#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"

#ifndef INCmenuSimulationH
#define INCmenuSimulationH
typedef enum {
	menuSimulationNONE,
	menuSimulationVSM,
	menuSimulationFAST,
	menuSimulationFULL
}menuSimulation;
#endif /*INCmenuSimulationH*/

#ifndef INCmenuCarstatesH
#define INCmenuCarstatesH
typedef enum {
	menuCarstatesIDLE,
	menuCarstatesPAUSED,
	menuCarstatesBUSY,
	menuCarstatesERROR
}menuCarstates;
#endif /*INCmenuCarstatesH*/
#ifndef INCcarH
#define INCcarH
typedef struct carRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	epicsEnum16	val;	/*State*/
	epicsInt32		clid;	/*Client ID*/
	char		omss[40]; /*Message (out)*/
	epicsInt32		oerr;	/*Error Code (out)*/
	epicsEnum16	ival;	/*State (in)*/
	DBLINK		icid;	/*Client ID (in)*/
	char		imss[40]; /*Message (in)*/
	epicsInt32		ierr;	/*Error Code (in)*/
	epicsInt32		aval;	/*Last State Archived*/
	epicsInt32		mval;	/*Last State Monitored*/
	epicsInt32		acid;	/*Last Client ID Archived*/
	epicsInt32		mcid;	/*Last Client ID Monitore*/
	char		amss[40]; /*Last Message Archived*/
	char		mmss[40]; /*Last Message Monitored*/
	epicsInt32		aerr;	/*Last Error Code Archive*/
	epicsInt32		merr;	/*Last Error Code Monitor*/
	epicsEnum16	ersv;	/*Error Alarm Severity*/
	DBLINK		siol;	/*Simulation Error Link*/
	epicsInt32		sval;	/*Simulation Error*/
	DBLINK		siml;	/*Simulation Mode Link*/
	epicsEnum16	simm;	/*Simulation Mode*/
	epicsEnum16	sims;	/*Sim Mode Alarm Severity*/
} carRecord;
#define carRecordNAME	0
#define carRecordDESC	1
#define carRecordASG	2
#define carRecordSCAN	3
#define carRecordPINI	4
#define carRecordPHAS	5
#define carRecordEVNT	6
#define carRecordTSE	7
#define carRecordTSEL	8
#define carRecordDTYP	9
#define carRecordDISV	10
#define carRecordDISA	11
#define carRecordSDIS	12
#define carRecordMLOK	13
#define carRecordMLIS	14
#define carRecordDISP	15
#define carRecordPROC	16
#define carRecordSTAT	17
#define carRecordSEVR	18
#define carRecordNSTA	19
#define carRecordNSEV	20
#define carRecordACKS	21
#define carRecordACKT	22
#define carRecordDISS	23
#define carRecordLCNT	24
#define carRecordPACT	25
#define carRecordPUTF	26
#define carRecordRPRO	27
#define carRecordASP	28
#define carRecordPPN	29
#define carRecordPPNR	30
#define carRecordSPVT	31
#define carRecordRSET	32
#define carRecordDSET	33
#define carRecordDPVT	34
#define carRecordRDES	35
#define carRecordLSET	36
#define carRecordPRIO	37
#define carRecordTPRO	38
#define carRecordBKPT	39
#define carRecordUDF	40
#define carRecordTIME	41
#define carRecordFLNK	42
#define carRecordVERS	43
#define carRecordVAL	44
#define carRecordCLID	45
#define carRecordOMSS	46
#define carRecordOERR	47
#define carRecordIVAL	48
#define carRecordICID	49
#define carRecordIMSS	50
#define carRecordIERR	51
#define carRecordAVAL	52
#define carRecordMVAL	53
#define carRecordACID	54
#define carRecordMCID	55
#define carRecordAMSS	56
#define carRecordMMSS	57
#define carRecordAERR	58
#define carRecordMERR	59
#define carRecordERSV	60
#define carRecordSIOL	61
#define carRecordSVAL	62
#define carRecordSIML	63
#define carRecordSIMM	64
#define carRecordSIMS	65
#endif /*INCcarH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int carRecordSizeOffset(dbRecordType *pdbRecordType)
{
    carRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->clid);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->clid - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->omss);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->omss - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->oerr);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->oerr - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->ival);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->ival - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->icid);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->icid - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->imss);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->imss - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->ierr);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->ierr - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->aval);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->aval - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->mval);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->mval - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->acid);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->acid - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->mcid);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->mcid - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->amss);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->amss - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->mmss);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->mmss - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->aerr);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->aerr - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->merr);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->merr - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->ersv);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->ersv - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->siol);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->siol - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->sval);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->sval - (char *)prec);
  pdbRecordType->papFldDes[63]->size=sizeof(prec->siml);
  pdbRecordType->papFldDes[63]->offset=(short)((char *)&prec->siml - (char *)prec);
  pdbRecordType->papFldDes[64]->size=sizeof(prec->simm);
  pdbRecordType->papFldDes[64]->offset=(short)((char *)&prec->simm - (char *)prec);
  pdbRecordType->papFldDes[65]->size=sizeof(prec->sims);
  pdbRecordType->papFldDes[65]->offset=(short)((char *)&prec->sims - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(carRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/
