#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"
#ifndef INCstatusH
#define INCstatusH
typedef struct statusRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	epicsInt32		val;	/*Current value*/
	DBLINK		inp;	/*Input Specification*/
	char		egu[16]; /*Units name*/
	epicsInt32		hopr;	/*High Operating Range*/
	epicsInt32		lopr;	/*Low Operating Range*/
	epicsInt32		lval;	/*Last value*/
	epicsEnum16	bi00;	/*bit 0 Value*/
	epicsEnum16	bi01;	/*bit 1 Value*/
	epicsEnum16	bi02;	/*bit 2 Value*/
	epicsEnum16	bi03;	/*bit 3 Value*/
	epicsEnum16	bi04;	/*bit 4 Value*/
	epicsEnum16	bi05;	/*bit 5 Value*/
	epicsEnum16	bi06;	/*bit 6 Value*/
	epicsEnum16	bi07;	/*bit 7 Value*/
	epicsEnum16	bi08;	/*bit 8 Value*/
	epicsEnum16	bi09;	/*bit 9 Value*/
	epicsEnum16	bi10;	/*bit 10 Value*/
	epicsEnum16	bi11;	/*bit 11 Value*/
	epicsEnum16	bi12;	/*bit 12 Value*/
	epicsEnum16	bi13;	/*bit 13 Value*/
	epicsEnum16	bi14;	/*bit 14 Value*/
	epicsEnum16	bi15;	/*bit 15 Value*/
	epicsEnum16	bi16;	/*bit 16 Value*/
	epicsEnum16	bi17;	/*bit 17 Value*/
	epicsEnum16	bi18;	/*bit 18 Value*/
	epicsEnum16	bi19;	/*bit 19 Value*/
	epicsEnum16	bi20;	/*bit 20 Value*/
	epicsEnum16	bi21;	/*bit 20 Value*/
	epicsEnum16	bi22;	/*bit 22 Value*/
	epicsEnum16	bi23;	/*bit 23 Value*/
	epicsEnum16	bi24;	/*bit 24 Value*/
	epicsEnum16	bi25;	/*bit 25 Value*/
	epicsEnum16	bi26;	/*bit 26 Value*/
	epicsEnum16	bi27;	/*bit 27 Value*/
	epicsEnum16	bi28;	/*bit 28 Value*/
	epicsEnum16	bi29;	/*bit 29 Value*/
	epicsEnum16	bi30;	/*bit 30 Value*/
	epicsEnum16	bi31;	/*bit 31 Value*/
	DBLINK		lk00;	/*Forward Link 0*/
	DBLINK		lk01;	/*Forward Link 1*/
	DBLINK		lk02;	/*Forward Link 2*/
	DBLINK		lk03;	/*Forward Link 3*/
	DBLINK		lk04;	/*Forward Link 4*/
	DBLINK		lk05;	/*Forward Link 5*/
	DBLINK		lk06;	/*Forward Link 6*/
	DBLINK		lk07;	/*Forward Link 7*/
	DBLINK		lk08;	/*Forward Link 8*/
	DBLINK		lk09;	/*Forward Link 9*/
	DBLINK		lk10;	/*Forward Link 10*/
	DBLINK		lk11;	/*Forward Link 11*/
	DBLINK		lk12;	/*Forward Link 12*/
	DBLINK		lk13;	/*Forward Link 13*/
	DBLINK		lk14;	/*Forward Link 14*/
	DBLINK		lk15;	/*Forward Link 15*/
	DBLINK		lk16;	/*Forward Link 16*/
	DBLINK		lk17;	/*Forward Link 17*/
	DBLINK		lk18;	/*Forward Link 18*/
	DBLINK		lk19;	/*Forward Link 19*/
	DBLINK		lk20;	/*Forward Link 20*/
	DBLINK		lk21;	/*Forward Link 21*/
	DBLINK		lk22;	/*Forward Link 22*/
	DBLINK		lk23;	/*Forward Link 23*/
	DBLINK		lk24;	/*Forward Link 24*/
	DBLINK		lk25;	/*Forward Link 25*/
	DBLINK		lk26;	/*Forward Link 26*/
	DBLINK		lk27;	/*Forward Link 27*/
	DBLINK		lk28;	/*Forward Link 28*/
	DBLINK		lk29;	/*Forward Link 29*/
	DBLINK		lk30;	/*Forward Link 30*/
	DBLINK		lk31;	/*Forward Link 31*/
	epicsInt32		hihi;	/*Hihi Alarm Limit*/
	epicsInt32		lolo;	/*Lolo Alarm Limit*/
	epicsInt32		high;	/*High Alarm Limit*/
	epicsInt32		low;	/*Low Alarm Limit*/
	epicsEnum16	hhsv;	/*Hihi Severity*/
	epicsEnum16	llsv;	/*Lolo Severity*/
	epicsEnum16	hsv;	/*High Severity*/
	epicsEnum16	lsv;	/*Low Severity*/
	epicsInt32		hyst;	/*Alarm Deadband*/
	epicsInt32		adel;	/*Archive Deadband*/
	epicsInt32		mdel;	/*Monitor Deadband*/
	epicsInt32		lalm;	/*Last Value Alarmed*/
	epicsInt32		alst;	/*Last Value Archived*/
	epicsInt32		mlst;	/*Last Val Monitored*/
	DBLINK		siol;	/*Sim Input Specifctn*/
	epicsInt32		sval;	/*Simulation Value*/
	DBLINK		siml;	/*Sim Mode Location*/
	epicsEnum16	simm;	/*Simulation Mode*/
	epicsEnum16	sims;	/*Sim mode Alarm Svrty*/
} statusRecord;
#define statusRecordNAME	0
#define statusRecordDESC	1
#define statusRecordASG	2
#define statusRecordSCAN	3
#define statusRecordPINI	4
#define statusRecordPHAS	5
#define statusRecordEVNT	6
#define statusRecordTSE	7
#define statusRecordTSEL	8
#define statusRecordDTYP	9
#define statusRecordDISV	10
#define statusRecordDISA	11
#define statusRecordSDIS	12
#define statusRecordMLOK	13
#define statusRecordMLIS	14
#define statusRecordDISP	15
#define statusRecordPROC	16
#define statusRecordSTAT	17
#define statusRecordSEVR	18
#define statusRecordNSTA	19
#define statusRecordNSEV	20
#define statusRecordACKS	21
#define statusRecordACKT	22
#define statusRecordDISS	23
#define statusRecordLCNT	24
#define statusRecordPACT	25
#define statusRecordPUTF	26
#define statusRecordRPRO	27
#define statusRecordASP	28
#define statusRecordPPN	29
#define statusRecordPPNR	30
#define statusRecordSPVT	31
#define statusRecordRSET	32
#define statusRecordDSET	33
#define statusRecordDPVT	34
#define statusRecordRDES	35
#define statusRecordLSET	36
#define statusRecordPRIO	37
#define statusRecordTPRO	38
#define statusRecordBKPT	39
#define statusRecordUDF	40
#define statusRecordTIME	41
#define statusRecordFLNK	42
#define statusRecordVERS	43
#define statusRecordVAL	44
#define statusRecordINP	45
#define statusRecordEGU	46
#define statusRecordHOPR	47
#define statusRecordLOPR	48
#define statusRecordLVAL	49
#define statusRecordBI00	50
#define statusRecordBI01	51
#define statusRecordBI02	52
#define statusRecordBI03	53
#define statusRecordBI04	54
#define statusRecordBI05	55
#define statusRecordBI06	56
#define statusRecordBI07	57
#define statusRecordBI08	58
#define statusRecordBI09	59
#define statusRecordBI10	60
#define statusRecordBI11	61
#define statusRecordBI12	62
#define statusRecordBI13	63
#define statusRecordBI14	64
#define statusRecordBI15	65
#define statusRecordBI16	66
#define statusRecordBI17	67
#define statusRecordBI18	68
#define statusRecordBI19	69
#define statusRecordBI20	70
#define statusRecordBI21	71
#define statusRecordBI22	72
#define statusRecordBI23	73
#define statusRecordBI24	74
#define statusRecordBI25	75
#define statusRecordBI26	76
#define statusRecordBI27	77
#define statusRecordBI28	78
#define statusRecordBI29	79
#define statusRecordBI30	80
#define statusRecordBI31	81
#define statusRecordLK00	82
#define statusRecordLK01	83
#define statusRecordLK02	84
#define statusRecordLK03	85
#define statusRecordLK04	86
#define statusRecordLK05	87
#define statusRecordLK06	88
#define statusRecordLK07	89
#define statusRecordLK08	90
#define statusRecordLK09	91
#define statusRecordLK10	92
#define statusRecordLK11	93
#define statusRecordLK12	94
#define statusRecordLK13	95
#define statusRecordLK14	96
#define statusRecordLK15	97
#define statusRecordLK16	98
#define statusRecordLK17	99
#define statusRecordLK18	100
#define statusRecordLK19	101
#define statusRecordLK20	102
#define statusRecordLK21	103
#define statusRecordLK22	104
#define statusRecordLK23	105
#define statusRecordLK24	106
#define statusRecordLK25	107
#define statusRecordLK26	108
#define statusRecordLK27	109
#define statusRecordLK28	110
#define statusRecordLK29	111
#define statusRecordLK30	112
#define statusRecordLK31	113
#define statusRecordHIHI	114
#define statusRecordLOLO	115
#define statusRecordHIGH	116
#define statusRecordLOW	117
#define statusRecordHHSV	118
#define statusRecordLLSV	119
#define statusRecordHSV	120
#define statusRecordLSV	121
#define statusRecordHYST	122
#define statusRecordADEL	123
#define statusRecordMDEL	124
#define statusRecordLALM	125
#define statusRecordALST	126
#define statusRecordMLST	127
#define statusRecordSIOL	128
#define statusRecordSVAL	129
#define statusRecordSIML	130
#define statusRecordSIMM	131
#define statusRecordSIMS	132
#endif /*INCstatusH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int statusRecordSizeOffset(dbRecordType *pdbRecordType)
{
    statusRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->inp);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->inp - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->egu);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->egu - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->hopr);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->hopr - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->lopr);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->lopr - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->lval);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->lval - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->bi00);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->bi00 - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->bi01);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->bi01 - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->bi02);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->bi02 - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->bi03);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->bi03 - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->bi04);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->bi04 - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->bi05);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->bi05 - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->bi06);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->bi06 - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->bi07);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->bi07 - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->bi08);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->bi08 - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->bi09);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->bi09 - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->bi10);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->bi10 - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->bi11);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->bi11 - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->bi12);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->bi12 - (char *)prec);
  pdbRecordType->papFldDes[63]->size=sizeof(prec->bi13);
  pdbRecordType->papFldDes[63]->offset=(short)((char *)&prec->bi13 - (char *)prec);
  pdbRecordType->papFldDes[64]->size=sizeof(prec->bi14);
  pdbRecordType->papFldDes[64]->offset=(short)((char *)&prec->bi14 - (char *)prec);
  pdbRecordType->papFldDes[65]->size=sizeof(prec->bi15);
  pdbRecordType->papFldDes[65]->offset=(short)((char *)&prec->bi15 - (char *)prec);
  pdbRecordType->papFldDes[66]->size=sizeof(prec->bi16);
  pdbRecordType->papFldDes[66]->offset=(short)((char *)&prec->bi16 - (char *)prec);
  pdbRecordType->papFldDes[67]->size=sizeof(prec->bi17);
  pdbRecordType->papFldDes[67]->offset=(short)((char *)&prec->bi17 - (char *)prec);
  pdbRecordType->papFldDes[68]->size=sizeof(prec->bi18);
  pdbRecordType->papFldDes[68]->offset=(short)((char *)&prec->bi18 - (char *)prec);
  pdbRecordType->papFldDes[69]->size=sizeof(prec->bi19);
  pdbRecordType->papFldDes[69]->offset=(short)((char *)&prec->bi19 - (char *)prec);
  pdbRecordType->papFldDes[70]->size=sizeof(prec->bi20);
  pdbRecordType->papFldDes[70]->offset=(short)((char *)&prec->bi20 - (char *)prec);
  pdbRecordType->papFldDes[71]->size=sizeof(prec->bi21);
  pdbRecordType->papFldDes[71]->offset=(short)((char *)&prec->bi21 - (char *)prec);
  pdbRecordType->papFldDes[72]->size=sizeof(prec->bi22);
  pdbRecordType->papFldDes[72]->offset=(short)((char *)&prec->bi22 - (char *)prec);
  pdbRecordType->papFldDes[73]->size=sizeof(prec->bi23);
  pdbRecordType->papFldDes[73]->offset=(short)((char *)&prec->bi23 - (char *)prec);
  pdbRecordType->papFldDes[74]->size=sizeof(prec->bi24);
  pdbRecordType->papFldDes[74]->offset=(short)((char *)&prec->bi24 - (char *)prec);
  pdbRecordType->papFldDes[75]->size=sizeof(prec->bi25);
  pdbRecordType->papFldDes[75]->offset=(short)((char *)&prec->bi25 - (char *)prec);
  pdbRecordType->papFldDes[76]->size=sizeof(prec->bi26);
  pdbRecordType->papFldDes[76]->offset=(short)((char *)&prec->bi26 - (char *)prec);
  pdbRecordType->papFldDes[77]->size=sizeof(prec->bi27);
  pdbRecordType->papFldDes[77]->offset=(short)((char *)&prec->bi27 - (char *)prec);
  pdbRecordType->papFldDes[78]->size=sizeof(prec->bi28);
  pdbRecordType->papFldDes[78]->offset=(short)((char *)&prec->bi28 - (char *)prec);
  pdbRecordType->papFldDes[79]->size=sizeof(prec->bi29);
  pdbRecordType->papFldDes[79]->offset=(short)((char *)&prec->bi29 - (char *)prec);
  pdbRecordType->papFldDes[80]->size=sizeof(prec->bi30);
  pdbRecordType->papFldDes[80]->offset=(short)((char *)&prec->bi30 - (char *)prec);
  pdbRecordType->papFldDes[81]->size=sizeof(prec->bi31);
  pdbRecordType->papFldDes[81]->offset=(short)((char *)&prec->bi31 - (char *)prec);
  pdbRecordType->papFldDes[82]->size=sizeof(prec->lk00);
  pdbRecordType->papFldDes[82]->offset=(short)((char *)&prec->lk00 - (char *)prec);
  pdbRecordType->papFldDes[83]->size=sizeof(prec->lk01);
  pdbRecordType->papFldDes[83]->offset=(short)((char *)&prec->lk01 - (char *)prec);
  pdbRecordType->papFldDes[84]->size=sizeof(prec->lk02);
  pdbRecordType->papFldDes[84]->offset=(short)((char *)&prec->lk02 - (char *)prec);
  pdbRecordType->papFldDes[85]->size=sizeof(prec->lk03);
  pdbRecordType->papFldDes[85]->offset=(short)((char *)&prec->lk03 - (char *)prec);
  pdbRecordType->papFldDes[86]->size=sizeof(prec->lk04);
  pdbRecordType->papFldDes[86]->offset=(short)((char *)&prec->lk04 - (char *)prec);
  pdbRecordType->papFldDes[87]->size=sizeof(prec->lk05);
  pdbRecordType->papFldDes[87]->offset=(short)((char *)&prec->lk05 - (char *)prec);
  pdbRecordType->papFldDes[88]->size=sizeof(prec->lk06);
  pdbRecordType->papFldDes[88]->offset=(short)((char *)&prec->lk06 - (char *)prec);
  pdbRecordType->papFldDes[89]->size=sizeof(prec->lk07);
  pdbRecordType->papFldDes[89]->offset=(short)((char *)&prec->lk07 - (char *)prec);
  pdbRecordType->papFldDes[90]->size=sizeof(prec->lk08);
  pdbRecordType->papFldDes[90]->offset=(short)((char *)&prec->lk08 - (char *)prec);
  pdbRecordType->papFldDes[91]->size=sizeof(prec->lk09);
  pdbRecordType->papFldDes[91]->offset=(short)((char *)&prec->lk09 - (char *)prec);
  pdbRecordType->papFldDes[92]->size=sizeof(prec->lk10);
  pdbRecordType->papFldDes[92]->offset=(short)((char *)&prec->lk10 - (char *)prec);
  pdbRecordType->papFldDes[93]->size=sizeof(prec->lk11);
  pdbRecordType->papFldDes[93]->offset=(short)((char *)&prec->lk11 - (char *)prec);
  pdbRecordType->papFldDes[94]->size=sizeof(prec->lk12);
  pdbRecordType->papFldDes[94]->offset=(short)((char *)&prec->lk12 - (char *)prec);
  pdbRecordType->papFldDes[95]->size=sizeof(prec->lk13);
  pdbRecordType->papFldDes[95]->offset=(short)((char *)&prec->lk13 - (char *)prec);
  pdbRecordType->papFldDes[96]->size=sizeof(prec->lk14);
  pdbRecordType->papFldDes[96]->offset=(short)((char *)&prec->lk14 - (char *)prec);
  pdbRecordType->papFldDes[97]->size=sizeof(prec->lk15);
  pdbRecordType->papFldDes[97]->offset=(short)((char *)&prec->lk15 - (char *)prec);
  pdbRecordType->papFldDes[98]->size=sizeof(prec->lk16);
  pdbRecordType->papFldDes[98]->offset=(short)((char *)&prec->lk16 - (char *)prec);
  pdbRecordType->papFldDes[99]->size=sizeof(prec->lk17);
  pdbRecordType->papFldDes[99]->offset=(short)((char *)&prec->lk17 - (char *)prec);
  pdbRecordType->papFldDes[100]->size=sizeof(prec->lk18);
  pdbRecordType->papFldDes[100]->offset=(short)((char *)&prec->lk18 - (char *)prec);
  pdbRecordType->papFldDes[101]->size=sizeof(prec->lk19);
  pdbRecordType->papFldDes[101]->offset=(short)((char *)&prec->lk19 - (char *)prec);
  pdbRecordType->papFldDes[102]->size=sizeof(prec->lk20);
  pdbRecordType->papFldDes[102]->offset=(short)((char *)&prec->lk20 - (char *)prec);
  pdbRecordType->papFldDes[103]->size=sizeof(prec->lk21);
  pdbRecordType->papFldDes[103]->offset=(short)((char *)&prec->lk21 - (char *)prec);
  pdbRecordType->papFldDes[104]->size=sizeof(prec->lk22);
  pdbRecordType->papFldDes[104]->offset=(short)((char *)&prec->lk22 - (char *)prec);
  pdbRecordType->papFldDes[105]->size=sizeof(prec->lk23);
  pdbRecordType->papFldDes[105]->offset=(short)((char *)&prec->lk23 - (char *)prec);
  pdbRecordType->papFldDes[106]->size=sizeof(prec->lk24);
  pdbRecordType->papFldDes[106]->offset=(short)((char *)&prec->lk24 - (char *)prec);
  pdbRecordType->papFldDes[107]->size=sizeof(prec->lk25);
  pdbRecordType->papFldDes[107]->offset=(short)((char *)&prec->lk25 - (char *)prec);
  pdbRecordType->papFldDes[108]->size=sizeof(prec->lk26);
  pdbRecordType->papFldDes[108]->offset=(short)((char *)&prec->lk26 - (char *)prec);
  pdbRecordType->papFldDes[109]->size=sizeof(prec->lk27);
  pdbRecordType->papFldDes[109]->offset=(short)((char *)&prec->lk27 - (char *)prec);
  pdbRecordType->papFldDes[110]->size=sizeof(prec->lk28);
  pdbRecordType->papFldDes[110]->offset=(short)((char *)&prec->lk28 - (char *)prec);
  pdbRecordType->papFldDes[111]->size=sizeof(prec->lk29);
  pdbRecordType->papFldDes[111]->offset=(short)((char *)&prec->lk29 - (char *)prec);
  pdbRecordType->papFldDes[112]->size=sizeof(prec->lk30);
  pdbRecordType->papFldDes[112]->offset=(short)((char *)&prec->lk30 - (char *)prec);
  pdbRecordType->papFldDes[113]->size=sizeof(prec->lk31);
  pdbRecordType->papFldDes[113]->offset=(short)((char *)&prec->lk31 - (char *)prec);
  pdbRecordType->papFldDes[114]->size=sizeof(prec->hihi);
  pdbRecordType->papFldDes[114]->offset=(short)((char *)&prec->hihi - (char *)prec);
  pdbRecordType->papFldDes[115]->size=sizeof(prec->lolo);
  pdbRecordType->papFldDes[115]->offset=(short)((char *)&prec->lolo - (char *)prec);
  pdbRecordType->papFldDes[116]->size=sizeof(prec->high);
  pdbRecordType->papFldDes[116]->offset=(short)((char *)&prec->high - (char *)prec);
  pdbRecordType->papFldDes[117]->size=sizeof(prec->low);
  pdbRecordType->papFldDes[117]->offset=(short)((char *)&prec->low - (char *)prec);
  pdbRecordType->papFldDes[118]->size=sizeof(prec->hhsv);
  pdbRecordType->papFldDes[118]->offset=(short)((char *)&prec->hhsv - (char *)prec);
  pdbRecordType->papFldDes[119]->size=sizeof(prec->llsv);
  pdbRecordType->papFldDes[119]->offset=(short)((char *)&prec->llsv - (char *)prec);
  pdbRecordType->papFldDes[120]->size=sizeof(prec->hsv);
  pdbRecordType->papFldDes[120]->offset=(short)((char *)&prec->hsv - (char *)prec);
  pdbRecordType->papFldDes[121]->size=sizeof(prec->lsv);
  pdbRecordType->papFldDes[121]->offset=(short)((char *)&prec->lsv - (char *)prec);
  pdbRecordType->papFldDes[122]->size=sizeof(prec->hyst);
  pdbRecordType->papFldDes[122]->offset=(short)((char *)&prec->hyst - (char *)prec);
  pdbRecordType->papFldDes[123]->size=sizeof(prec->adel);
  pdbRecordType->papFldDes[123]->offset=(short)((char *)&prec->adel - (char *)prec);
  pdbRecordType->papFldDes[124]->size=sizeof(prec->mdel);
  pdbRecordType->papFldDes[124]->offset=(short)((char *)&prec->mdel - (char *)prec);
  pdbRecordType->papFldDes[125]->size=sizeof(prec->lalm);
  pdbRecordType->papFldDes[125]->offset=(short)((char *)&prec->lalm - (char *)prec);
  pdbRecordType->papFldDes[126]->size=sizeof(prec->alst);
  pdbRecordType->papFldDes[126]->offset=(short)((char *)&prec->alst - (char *)prec);
  pdbRecordType->papFldDes[127]->size=sizeof(prec->mlst);
  pdbRecordType->papFldDes[127]->offset=(short)((char *)&prec->mlst - (char *)prec);
  pdbRecordType->papFldDes[128]->size=sizeof(prec->siol);
  pdbRecordType->papFldDes[128]->offset=(short)((char *)&prec->siol - (char *)prec);
  pdbRecordType->papFldDes[129]->size=sizeof(prec->sval);
  pdbRecordType->papFldDes[129]->offset=(short)((char *)&prec->sval - (char *)prec);
  pdbRecordType->papFldDes[130]->size=sizeof(prec->siml);
  pdbRecordType->papFldDes[130]->offset=(short)((char *)&prec->siml - (char *)prec);
  pdbRecordType->papFldDes[131]->size=sizeof(prec->simm);
  pdbRecordType->papFldDes[131]->offset=(short)((char *)&prec->simm - (char *)prec);
  pdbRecordType->papFldDes[132]->size=sizeof(prec->sims);
  pdbRecordType->papFldDes[132]->offset=(short)((char *)&prec->sims - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(statusRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/
