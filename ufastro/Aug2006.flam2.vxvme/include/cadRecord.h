#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"

#ifndef INCmenuSimulationH
#define INCmenuSimulationH
typedef enum {
	menuSimulationNONE,
	menuSimulationVSM,
	menuSimulationFAST,
	menuSimulationFULL
}menuSimulation;
#endif /*INCmenuSimulationH*/

#ifndef INCmenuDirectiveH
#define INCmenuDirectiveH
typedef enum {
	menuDirectiveMARK,
	menuDirectiveCLEAR,
	menuDirectivePRESET,
	menuDirectiveSTART,
	menuDirectiveSTOP
}menuDirective;
#endif /*INCmenuDirectiveH*/

#ifndef INCcadMFLGH
#define INCcadMFLGH
typedef enum {
	cadMFLG_THREE_STATES,
	cadMFLG_TWO_STATES
}cadMFLG;
#endif /*INCcadMFLGH*/
#ifndef INCcadH
#define INCcadH
typedef struct cadRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	epicsInt32		val;	/*Return Error Code*/
	char		snam[40]; /*Subroutine Name*/
	epicsInt32		sadr;	/*Subroutine Address*/
	char		inam[40]; /*Init Routine Name*/
	epicsEnum16	dir;	/*CAD Directive*/
	epicsEnum16	odir;	/*Output CAD Directive*/
	epicsEnum16	mflg;	/*Mark Flag*/
	epicsInt32		base;	/*Internal Base State*/
	epicsInt32		icid;	/*Client ID (In)*/
	char		mess[40]; /*Message*/
	char		omss[40]; /*Old Message*/
	short		ctyp;	/*Number of CAD Args*/
	short		prec;	/*Display Precision*/
	DBLINK		mlnk;	/*Mark   Link*/
	DBLINK		clnk;	/*Clear  Link*/
	DBLINK		plnk;	/*Preset Link*/
	DBLINK		stlk;	/*Start  Link*/
	DBLINK		splk;	/*Stop   Link*/
	epicsInt32		ocid;	/*Client ID (Out)*/
	epicsEnum16	osim;	/*Simulation Mode (Out)*/
	short		narg;	/*No. Inputs used*/
	short		mark;	/*Is Record Preset?*/
	epicsEnum16	ersv;	/*Error Alarm Severity*/
	DBLINK		siol;	/*Simulation Error Link*/
	epicsInt32		sval;	/*Simulation Error*/
	DBLINK		siml;	/*Simulation Mode Link*/
	epicsEnum16	simm;	/*Simulation Mode*/
	epicsEnum16	sims;	/*Sim Mode Alarm Svrity*/
	DBLINK		inpa;	/*Input Link A*/
	DBLINK		inpb;	/*Input Link B*/
	DBLINK		inpc;	/*Input Link C*/
	DBLINK		inpd;	/*Input Link D*/
	DBLINK		inpe;	/*Input Link E*/
	DBLINK		inpf;	/*Input Link F*/
	DBLINK		inpg;	/*Input Link G*/
	DBLINK		inph;	/*Input Link H*/
	DBLINK		inpi;	/*Input Link I*/
	DBLINK		inpj;	/*Input Link J*/
	DBLINK		inpk;	/*Input Link K*/
	DBLINK		inpl;	/*Input Link L*/
	DBLINK		inpm;	/*Input Link M*/
	DBLINK		inpn;	/*Input Link N*/
	DBLINK		inpo;	/*Input Link O*/
	DBLINK		inpp;	/*Input Link P*/
	DBLINK		inpq;	/*Input Link Q*/
	DBLINK		inpr;	/*Input Link R*/
	DBLINK		inps;	/*Input Link S*/
	DBLINK		inpt;	/*Input Link T*/
	DBLINK		outa;	/*Output Link A*/
	DBLINK		outb;	/*Output Link B*/
	DBLINK		outc;	/*Output Link C*/
	DBLINK		outd;	/*Output Link D*/
	DBLINK		oute;	/*Output Link E*/
	DBLINK		outf;	/*Output Link F*/
	DBLINK		outg;	/*Output Link G*/
	DBLINK		outh;	/*Output Link H*/
	DBLINK		outi;	/*Output Link I*/
	DBLINK		outj;	/*Output Link J*/
	DBLINK		outk;	/*Output Link K*/
	DBLINK		outl;	/*Output Link L*/
	DBLINK		outm;	/*Output Link M*/
	DBLINK		outn;	/*Output Link N*/
	DBLINK		outo;	/*Output Link O*/
	DBLINK		outp;	/*Output Link P*/
	DBLINK		outq;	/*Output Link Q*/
	DBLINK		outr;	/*Output Link R*/
	DBLINK		outs;	/*Output Link S*/
	DBLINK		outt;	/*Output Link T*/
	char		a[40]; /*Value of Input A*/
	char		b[40]; /*Value of Input B*/
	char		c[40]; /*Value of Input C*/
	char		d[40]; /*Value of Input D*/
	char		e[40]; /*Value of Input E*/
	char		f[40]; /*Value of Input F*/
	char		g[40]; /*Value of Input G*/
	char		h[40]; /*Value of Input H*/
	char		i[40]; /*Value of Input I*/
	char		j[40]; /*Value of Input J*/
	char		k[40]; /*Value of Input K*/
	char		l[40]; /*Value of Input L*/
	char		m[40]; /*Value of Input M*/
	char		n[40]; /*Value of Input N*/
	char		o[40]; /*Value of Input O*/
	char		p[40]; /*Value of Input P*/
	char		q[40]; /*Value of Input Q*/
	char		r[40]; /*Value of Input R*/
	char		s[40]; /*Value of Input S*/
	char		t[40]; /*Value of Input T*/
	void *vala;	/*Value of Output A*/
	void *valb;	/*Value of Output B*/
	void *valc;	/*Value of Output C*/
	void *vald;	/*Value of Output D*/
	void *vale;	/*Value of Output E*/
	void *valf;	/*Value of Output F*/
	void *valg;	/*Value of Output G*/
	void *valh;	/*Value of Output H*/
	void *vali;	/*Value of Output I*/
	void *valj;	/*Value of Output J*/
	void *valk;	/*Value of Output K*/
	void *vall;	/*Value of Output L*/
	void *valm;	/*Value of Output M*/
	void *valn;	/*Value of Output N*/
	void *valo;	/*Value of Output O*/
	void *valp;	/*Value of Output P*/
	void *valq;	/*Value of Output Q*/
	void *valr;	/*Value of Output R*/
	void *vals;	/*Value of Output S*/
	void *valt;	/*Value of Output T*/
	void *olda;	/*Old output value A*/
	void *oldb;	/*Old output value B*/
	void *oldc;	/*Old output value C*/
	void *oldd;	/*Old output value D*/
	void *olde;	/*Old output value E*/
	void *oldf;	/*Old output value F*/
	void *oldg;	/*Old output value G*/
	void *oldh;	/*Old output value H*/
	void *oldi;	/*Old output value I*/
	void *oldj;	/*Old output value J*/
	void *oldk;	/*Old output value K*/
	void *oldl;	/*Old output value L*/
	void *oldm;	/*Old output value M*/
	void *oldn;	/*Old output value N*/
	void *oldo;	/*Old output value O*/
	void *oldp;	/*Old output value P*/
	void *oldq;	/*Old output value Q*/
	void *oldr;	/*Old output value R*/
	void *olds;	/*Old output value S*/
	void *oldt;	/*Old output value T*/
	epicsEnum16	ftva;	/*Type of VALA*/
	epicsEnum16	ftvb;	/*Type of VALB*/
	epicsEnum16	ftvc;	/*Type of VALC*/
	epicsEnum16	ftvd;	/*Type of VALD*/
	epicsEnum16	ftve;	/*Type of VALE*/
	epicsEnum16	ftvf;	/*Type of VALF*/
	epicsEnum16	ftvg;	/*Type of VALG*/
	epicsEnum16	ftvh;	/*Type of VALH*/
	epicsEnum16	ftvi;	/*Type of VALI*/
	epicsEnum16	ftvj;	/*Type of VALJ*/
	epicsEnum16	ftvk;	/*Type of VALK*/
	epicsEnum16	ftvl;	/*Type of VALL*/
	epicsEnum16	ftvm;	/*Type of VALM*/
	epicsEnum16	ftvn;	/*Type of VALN*/
	epicsEnum16	ftvo;	/*Type of VALO*/
	epicsEnum16	ftvp;	/*Type of VALP*/
	epicsEnum16	ftvq;	/*Type of VALQ*/
	epicsEnum16	ftvr;	/*Type of VALR*/
	epicsEnum16	ftvs;	/*Type of VALS*/
	epicsEnum16	ftvt;	/*Type of VALT*/
} cadRecord;
#define cadRecordNAME	0
#define cadRecordDESC	1
#define cadRecordASG	2
#define cadRecordSCAN	3
#define cadRecordPINI	4
#define cadRecordPHAS	5
#define cadRecordEVNT	6
#define cadRecordTSE	7
#define cadRecordTSEL	8
#define cadRecordDTYP	9
#define cadRecordDISV	10
#define cadRecordDISA	11
#define cadRecordSDIS	12
#define cadRecordMLOK	13
#define cadRecordMLIS	14
#define cadRecordDISP	15
#define cadRecordPROC	16
#define cadRecordSTAT	17
#define cadRecordSEVR	18
#define cadRecordNSTA	19
#define cadRecordNSEV	20
#define cadRecordACKS	21
#define cadRecordACKT	22
#define cadRecordDISS	23
#define cadRecordLCNT	24
#define cadRecordPACT	25
#define cadRecordPUTF	26
#define cadRecordRPRO	27
#define cadRecordASP	28
#define cadRecordPPN	29
#define cadRecordPPNR	30
#define cadRecordSPVT	31
#define cadRecordRSET	32
#define cadRecordDSET	33
#define cadRecordDPVT	34
#define cadRecordRDES	35
#define cadRecordLSET	36
#define cadRecordPRIO	37
#define cadRecordTPRO	38
#define cadRecordBKPT	39
#define cadRecordUDF	40
#define cadRecordTIME	41
#define cadRecordFLNK	42
#define cadRecordVERS	43
#define cadRecordVAL	44
#define cadRecordSNAM	45
#define cadRecordSADR	46
#define cadRecordINAM	47
#define cadRecordDIR	48
#define cadRecordODIR	49
#define cadRecordMFLG	50
#define cadRecordBASE	51
#define cadRecordICID	52
#define cadRecordMESS	53
#define cadRecordOMSS	54
#define cadRecordCTYP	55
#define cadRecordPREC	56
#define cadRecordMLNK	57
#define cadRecordCLNK	58
#define cadRecordPLNK	59
#define cadRecordSTLK	60
#define cadRecordSPLK	61
#define cadRecordOCID	62
#define cadRecordOSIM	63
#define cadRecordNARG	64
#define cadRecordMARK	65
#define cadRecordERSV	66
#define cadRecordSIOL	67
#define cadRecordSVAL	68
#define cadRecordSIML	69
#define cadRecordSIMM	70
#define cadRecordSIMS	71
#define cadRecordINPA	72
#define cadRecordINPB	73
#define cadRecordINPC	74
#define cadRecordINPD	75
#define cadRecordINPE	76
#define cadRecordINPF	77
#define cadRecordINPG	78
#define cadRecordINPH	79
#define cadRecordINPI	80
#define cadRecordINPJ	81
#define cadRecordINPK	82
#define cadRecordINPL	83
#define cadRecordINPM	84
#define cadRecordINPN	85
#define cadRecordINPO	86
#define cadRecordINPP	87
#define cadRecordINPQ	88
#define cadRecordINPR	89
#define cadRecordINPS	90
#define cadRecordINPT	91
#define cadRecordOUTA	92
#define cadRecordOUTB	93
#define cadRecordOUTC	94
#define cadRecordOUTD	95
#define cadRecordOUTE	96
#define cadRecordOUTF	97
#define cadRecordOUTG	98
#define cadRecordOUTH	99
#define cadRecordOUTI	100
#define cadRecordOUTJ	101
#define cadRecordOUTK	102
#define cadRecordOUTL	103
#define cadRecordOUTM	104
#define cadRecordOUTN	105
#define cadRecordOUTO	106
#define cadRecordOUTP	107
#define cadRecordOUTQ	108
#define cadRecordOUTR	109
#define cadRecordOUTS	110
#define cadRecordOUTT	111
#define cadRecordA	112
#define cadRecordB	113
#define cadRecordC	114
#define cadRecordD	115
#define cadRecordE	116
#define cadRecordF	117
#define cadRecordG	118
#define cadRecordH	119
#define cadRecordI	120
#define cadRecordJ	121
#define cadRecordK	122
#define cadRecordL	123
#define cadRecordM	124
#define cadRecordN	125
#define cadRecordO	126
#define cadRecordP	127
#define cadRecordQ	128
#define cadRecordR	129
#define cadRecordS	130
#define cadRecordT	131
#define cadRecordVALA	132
#define cadRecordVALB	133
#define cadRecordVALC	134
#define cadRecordVALD	135
#define cadRecordVALE	136
#define cadRecordVALF	137
#define cadRecordVALG	138
#define cadRecordVALH	139
#define cadRecordVALI	140
#define cadRecordVALJ	141
#define cadRecordVALK	142
#define cadRecordVALL	143
#define cadRecordVALM	144
#define cadRecordVALN	145
#define cadRecordVALO	146
#define cadRecordVALP	147
#define cadRecordVALQ	148
#define cadRecordVALR	149
#define cadRecordVALS	150
#define cadRecordVALT	151
#define cadRecordOLDA	152
#define cadRecordOLDB	153
#define cadRecordOLDC	154
#define cadRecordOLDD	155
#define cadRecordOLDE	156
#define cadRecordOLDF	157
#define cadRecordOLDG	158
#define cadRecordOLDH	159
#define cadRecordOLDI	160
#define cadRecordOLDJ	161
#define cadRecordOLDK	162
#define cadRecordOLDL	163
#define cadRecordOLDM	164
#define cadRecordOLDN	165
#define cadRecordOLDO	166
#define cadRecordOLDP	167
#define cadRecordOLDQ	168
#define cadRecordOLDR	169
#define cadRecordOLDS	170
#define cadRecordOLDT	171
#define cadRecordFTVA	172
#define cadRecordFTVB	173
#define cadRecordFTVC	174
#define cadRecordFTVD	175
#define cadRecordFTVE	176
#define cadRecordFTVF	177
#define cadRecordFTVG	178
#define cadRecordFTVH	179
#define cadRecordFTVI	180
#define cadRecordFTVJ	181
#define cadRecordFTVK	182
#define cadRecordFTVL	183
#define cadRecordFTVM	184
#define cadRecordFTVN	185
#define cadRecordFTVO	186
#define cadRecordFTVP	187
#define cadRecordFTVQ	188
#define cadRecordFTVR	189
#define cadRecordFTVS	190
#define cadRecordFTVT	191
#endif /*INCcadH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int cadRecordSizeOffset(dbRecordType *pdbRecordType)
{
    cadRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->snam);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->snam - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->sadr);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->sadr - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->inam);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->inam - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->dir);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->dir - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->odir);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->odir - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->mflg);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->mflg - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->base);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->base - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->icid);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->icid - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->mess);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->mess - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->omss);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->omss - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->ctyp);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->ctyp - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->prec);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->prec - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->mlnk);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->mlnk - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->clnk);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->clnk - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->plnk);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->plnk - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->stlk);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->stlk - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->splk);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->splk - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->ocid);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->ocid - (char *)prec);
  pdbRecordType->papFldDes[63]->size=sizeof(prec->osim);
  pdbRecordType->papFldDes[63]->offset=(short)((char *)&prec->osim - (char *)prec);
  pdbRecordType->papFldDes[64]->size=sizeof(prec->narg);
  pdbRecordType->papFldDes[64]->offset=(short)((char *)&prec->narg - (char *)prec);
  pdbRecordType->papFldDes[65]->size=sizeof(prec->mark);
  pdbRecordType->papFldDes[65]->offset=(short)((char *)&prec->mark - (char *)prec);
  pdbRecordType->papFldDes[66]->size=sizeof(prec->ersv);
  pdbRecordType->papFldDes[66]->offset=(short)((char *)&prec->ersv - (char *)prec);
  pdbRecordType->papFldDes[67]->size=sizeof(prec->siol);
  pdbRecordType->papFldDes[67]->offset=(short)((char *)&prec->siol - (char *)prec);
  pdbRecordType->papFldDes[68]->size=sizeof(prec->sval);
  pdbRecordType->papFldDes[68]->offset=(short)((char *)&prec->sval - (char *)prec);
  pdbRecordType->papFldDes[69]->size=sizeof(prec->siml);
  pdbRecordType->papFldDes[69]->offset=(short)((char *)&prec->siml - (char *)prec);
  pdbRecordType->papFldDes[70]->size=sizeof(prec->simm);
  pdbRecordType->papFldDes[70]->offset=(short)((char *)&prec->simm - (char *)prec);
  pdbRecordType->papFldDes[71]->size=sizeof(prec->sims);
  pdbRecordType->papFldDes[71]->offset=(short)((char *)&prec->sims - (char *)prec);
  pdbRecordType->papFldDes[72]->size=sizeof(prec->inpa);
  pdbRecordType->papFldDes[72]->offset=(short)((char *)&prec->inpa - (char *)prec);
  pdbRecordType->papFldDes[73]->size=sizeof(prec->inpb);
  pdbRecordType->papFldDes[73]->offset=(short)((char *)&prec->inpb - (char *)prec);
  pdbRecordType->papFldDes[74]->size=sizeof(prec->inpc);
  pdbRecordType->papFldDes[74]->offset=(short)((char *)&prec->inpc - (char *)prec);
  pdbRecordType->papFldDes[75]->size=sizeof(prec->inpd);
  pdbRecordType->papFldDes[75]->offset=(short)((char *)&prec->inpd - (char *)prec);
  pdbRecordType->papFldDes[76]->size=sizeof(prec->inpe);
  pdbRecordType->papFldDes[76]->offset=(short)((char *)&prec->inpe - (char *)prec);
  pdbRecordType->papFldDes[77]->size=sizeof(prec->inpf);
  pdbRecordType->papFldDes[77]->offset=(short)((char *)&prec->inpf - (char *)prec);
  pdbRecordType->papFldDes[78]->size=sizeof(prec->inpg);
  pdbRecordType->papFldDes[78]->offset=(short)((char *)&prec->inpg - (char *)prec);
  pdbRecordType->papFldDes[79]->size=sizeof(prec->inph);
  pdbRecordType->papFldDes[79]->offset=(short)((char *)&prec->inph - (char *)prec);
  pdbRecordType->papFldDes[80]->size=sizeof(prec->inpi);
  pdbRecordType->papFldDes[80]->offset=(short)((char *)&prec->inpi - (char *)prec);
  pdbRecordType->papFldDes[81]->size=sizeof(prec->inpj);
  pdbRecordType->papFldDes[81]->offset=(short)((char *)&prec->inpj - (char *)prec);
  pdbRecordType->papFldDes[82]->size=sizeof(prec->inpk);
  pdbRecordType->papFldDes[82]->offset=(short)((char *)&prec->inpk - (char *)prec);
  pdbRecordType->papFldDes[83]->size=sizeof(prec->inpl);
  pdbRecordType->papFldDes[83]->offset=(short)((char *)&prec->inpl - (char *)prec);
  pdbRecordType->papFldDes[84]->size=sizeof(prec->inpm);
  pdbRecordType->papFldDes[84]->offset=(short)((char *)&prec->inpm - (char *)prec);
  pdbRecordType->papFldDes[85]->size=sizeof(prec->inpn);
  pdbRecordType->papFldDes[85]->offset=(short)((char *)&prec->inpn - (char *)prec);
  pdbRecordType->papFldDes[86]->size=sizeof(prec->inpo);
  pdbRecordType->papFldDes[86]->offset=(short)((char *)&prec->inpo - (char *)prec);
  pdbRecordType->papFldDes[87]->size=sizeof(prec->inpp);
  pdbRecordType->papFldDes[87]->offset=(short)((char *)&prec->inpp - (char *)prec);
  pdbRecordType->papFldDes[88]->size=sizeof(prec->inpq);
  pdbRecordType->papFldDes[88]->offset=(short)((char *)&prec->inpq - (char *)prec);
  pdbRecordType->papFldDes[89]->size=sizeof(prec->inpr);
  pdbRecordType->papFldDes[89]->offset=(short)((char *)&prec->inpr - (char *)prec);
  pdbRecordType->papFldDes[90]->size=sizeof(prec->inps);
  pdbRecordType->papFldDes[90]->offset=(short)((char *)&prec->inps - (char *)prec);
  pdbRecordType->papFldDes[91]->size=sizeof(prec->inpt);
  pdbRecordType->papFldDes[91]->offset=(short)((char *)&prec->inpt - (char *)prec);
  pdbRecordType->papFldDes[92]->size=sizeof(prec->outa);
  pdbRecordType->papFldDes[92]->offset=(short)((char *)&prec->outa - (char *)prec);
  pdbRecordType->papFldDes[93]->size=sizeof(prec->outb);
  pdbRecordType->papFldDes[93]->offset=(short)((char *)&prec->outb - (char *)prec);
  pdbRecordType->papFldDes[94]->size=sizeof(prec->outc);
  pdbRecordType->papFldDes[94]->offset=(short)((char *)&prec->outc - (char *)prec);
  pdbRecordType->papFldDes[95]->size=sizeof(prec->outd);
  pdbRecordType->papFldDes[95]->offset=(short)((char *)&prec->outd - (char *)prec);
  pdbRecordType->papFldDes[96]->size=sizeof(prec->oute);
  pdbRecordType->papFldDes[96]->offset=(short)((char *)&prec->oute - (char *)prec);
  pdbRecordType->papFldDes[97]->size=sizeof(prec->outf);
  pdbRecordType->papFldDes[97]->offset=(short)((char *)&prec->outf - (char *)prec);
  pdbRecordType->papFldDes[98]->size=sizeof(prec->outg);
  pdbRecordType->papFldDes[98]->offset=(short)((char *)&prec->outg - (char *)prec);
  pdbRecordType->papFldDes[99]->size=sizeof(prec->outh);
  pdbRecordType->papFldDes[99]->offset=(short)((char *)&prec->outh - (char *)prec);
  pdbRecordType->papFldDes[100]->size=sizeof(prec->outi);
  pdbRecordType->papFldDes[100]->offset=(short)((char *)&prec->outi - (char *)prec);
  pdbRecordType->papFldDes[101]->size=sizeof(prec->outj);
  pdbRecordType->papFldDes[101]->offset=(short)((char *)&prec->outj - (char *)prec);
  pdbRecordType->papFldDes[102]->size=sizeof(prec->outk);
  pdbRecordType->papFldDes[102]->offset=(short)((char *)&prec->outk - (char *)prec);
  pdbRecordType->papFldDes[103]->size=sizeof(prec->outl);
  pdbRecordType->papFldDes[103]->offset=(short)((char *)&prec->outl - (char *)prec);
  pdbRecordType->papFldDes[104]->size=sizeof(prec->outm);
  pdbRecordType->papFldDes[104]->offset=(short)((char *)&prec->outm - (char *)prec);
  pdbRecordType->papFldDes[105]->size=sizeof(prec->outn);
  pdbRecordType->papFldDes[105]->offset=(short)((char *)&prec->outn - (char *)prec);
  pdbRecordType->papFldDes[106]->size=sizeof(prec->outo);
  pdbRecordType->papFldDes[106]->offset=(short)((char *)&prec->outo - (char *)prec);
  pdbRecordType->papFldDes[107]->size=sizeof(prec->outp);
  pdbRecordType->papFldDes[107]->offset=(short)((char *)&prec->outp - (char *)prec);
  pdbRecordType->papFldDes[108]->size=sizeof(prec->outq);
  pdbRecordType->papFldDes[108]->offset=(short)((char *)&prec->outq - (char *)prec);
  pdbRecordType->papFldDes[109]->size=sizeof(prec->outr);
  pdbRecordType->papFldDes[109]->offset=(short)((char *)&prec->outr - (char *)prec);
  pdbRecordType->papFldDes[110]->size=sizeof(prec->outs);
  pdbRecordType->papFldDes[110]->offset=(short)((char *)&prec->outs - (char *)prec);
  pdbRecordType->papFldDes[111]->size=sizeof(prec->outt);
  pdbRecordType->papFldDes[111]->offset=(short)((char *)&prec->outt - (char *)prec);
  pdbRecordType->papFldDes[112]->size=sizeof(prec->a);
  pdbRecordType->papFldDes[112]->offset=(short)((char *)&prec->a - (char *)prec);
  pdbRecordType->papFldDes[113]->size=sizeof(prec->b);
  pdbRecordType->papFldDes[113]->offset=(short)((char *)&prec->b - (char *)prec);
  pdbRecordType->papFldDes[114]->size=sizeof(prec->c);
  pdbRecordType->papFldDes[114]->offset=(short)((char *)&prec->c - (char *)prec);
  pdbRecordType->papFldDes[115]->size=sizeof(prec->d);
  pdbRecordType->papFldDes[115]->offset=(short)((char *)&prec->d - (char *)prec);
  pdbRecordType->papFldDes[116]->size=sizeof(prec->e);
  pdbRecordType->papFldDes[116]->offset=(short)((char *)&prec->e - (char *)prec);
  pdbRecordType->papFldDes[117]->size=sizeof(prec->f);
  pdbRecordType->papFldDes[117]->offset=(short)((char *)&prec->f - (char *)prec);
  pdbRecordType->papFldDes[118]->size=sizeof(prec->g);
  pdbRecordType->papFldDes[118]->offset=(short)((char *)&prec->g - (char *)prec);
  pdbRecordType->papFldDes[119]->size=sizeof(prec->h);
  pdbRecordType->papFldDes[119]->offset=(short)((char *)&prec->h - (char *)prec);
  pdbRecordType->papFldDes[120]->size=sizeof(prec->i);
  pdbRecordType->papFldDes[120]->offset=(short)((char *)&prec->i - (char *)prec);
  pdbRecordType->papFldDes[121]->size=sizeof(prec->j);
  pdbRecordType->papFldDes[121]->offset=(short)((char *)&prec->j - (char *)prec);
  pdbRecordType->papFldDes[122]->size=sizeof(prec->k);
  pdbRecordType->papFldDes[122]->offset=(short)((char *)&prec->k - (char *)prec);
  pdbRecordType->papFldDes[123]->size=sizeof(prec->l);
  pdbRecordType->papFldDes[123]->offset=(short)((char *)&prec->l - (char *)prec);
  pdbRecordType->papFldDes[124]->size=sizeof(prec->m);
  pdbRecordType->papFldDes[124]->offset=(short)((char *)&prec->m - (char *)prec);
  pdbRecordType->papFldDes[125]->size=sizeof(prec->n);
  pdbRecordType->papFldDes[125]->offset=(short)((char *)&prec->n - (char *)prec);
  pdbRecordType->papFldDes[126]->size=sizeof(prec->o);
  pdbRecordType->papFldDes[126]->offset=(short)((char *)&prec->o - (char *)prec);
  pdbRecordType->papFldDes[127]->size=sizeof(prec->p);
  pdbRecordType->papFldDes[127]->offset=(short)((char *)&prec->p - (char *)prec);
  pdbRecordType->papFldDes[128]->size=sizeof(prec->q);
  pdbRecordType->papFldDes[128]->offset=(short)((char *)&prec->q - (char *)prec);
  pdbRecordType->papFldDes[129]->size=sizeof(prec->r);
  pdbRecordType->papFldDes[129]->offset=(short)((char *)&prec->r - (char *)prec);
  pdbRecordType->papFldDes[130]->size=sizeof(prec->s);
  pdbRecordType->papFldDes[130]->offset=(short)((char *)&prec->s - (char *)prec);
  pdbRecordType->papFldDes[131]->size=sizeof(prec->t);
  pdbRecordType->papFldDes[131]->offset=(short)((char *)&prec->t - (char *)prec);
  pdbRecordType->papFldDes[132]->size=sizeof(prec->vala);
  pdbRecordType->papFldDes[132]->offset=(short)((char *)&prec->vala - (char *)prec);
  pdbRecordType->papFldDes[133]->size=sizeof(prec->valb);
  pdbRecordType->papFldDes[133]->offset=(short)((char *)&prec->valb - (char *)prec);
  pdbRecordType->papFldDes[134]->size=sizeof(prec->valc);
  pdbRecordType->papFldDes[134]->offset=(short)((char *)&prec->valc - (char *)prec);
  pdbRecordType->papFldDes[135]->size=sizeof(prec->vald);
  pdbRecordType->papFldDes[135]->offset=(short)((char *)&prec->vald - (char *)prec);
  pdbRecordType->papFldDes[136]->size=sizeof(prec->vale);
  pdbRecordType->papFldDes[136]->offset=(short)((char *)&prec->vale - (char *)prec);
  pdbRecordType->papFldDes[137]->size=sizeof(prec->valf);
  pdbRecordType->papFldDes[137]->offset=(short)((char *)&prec->valf - (char *)prec);
  pdbRecordType->papFldDes[138]->size=sizeof(prec->valg);
  pdbRecordType->papFldDes[138]->offset=(short)((char *)&prec->valg - (char *)prec);
  pdbRecordType->papFldDes[139]->size=sizeof(prec->valh);
  pdbRecordType->papFldDes[139]->offset=(short)((char *)&prec->valh - (char *)prec);
  pdbRecordType->papFldDes[140]->size=sizeof(prec->vali);
  pdbRecordType->papFldDes[140]->offset=(short)((char *)&prec->vali - (char *)prec);
  pdbRecordType->papFldDes[141]->size=sizeof(prec->valj);
  pdbRecordType->papFldDes[141]->offset=(short)((char *)&prec->valj - (char *)prec);
  pdbRecordType->papFldDes[142]->size=sizeof(prec->valk);
  pdbRecordType->papFldDes[142]->offset=(short)((char *)&prec->valk - (char *)prec);
  pdbRecordType->papFldDes[143]->size=sizeof(prec->vall);
  pdbRecordType->papFldDes[143]->offset=(short)((char *)&prec->vall - (char *)prec);
  pdbRecordType->papFldDes[144]->size=sizeof(prec->valm);
  pdbRecordType->papFldDes[144]->offset=(short)((char *)&prec->valm - (char *)prec);
  pdbRecordType->papFldDes[145]->size=sizeof(prec->valn);
  pdbRecordType->papFldDes[145]->offset=(short)((char *)&prec->valn - (char *)prec);
  pdbRecordType->papFldDes[146]->size=sizeof(prec->valo);
  pdbRecordType->papFldDes[146]->offset=(short)((char *)&prec->valo - (char *)prec);
  pdbRecordType->papFldDes[147]->size=sizeof(prec->valp);
  pdbRecordType->papFldDes[147]->offset=(short)((char *)&prec->valp - (char *)prec);
  pdbRecordType->papFldDes[148]->size=sizeof(prec->valq);
  pdbRecordType->papFldDes[148]->offset=(short)((char *)&prec->valq - (char *)prec);
  pdbRecordType->papFldDes[149]->size=sizeof(prec->valr);
  pdbRecordType->papFldDes[149]->offset=(short)((char *)&prec->valr - (char *)prec);
  pdbRecordType->papFldDes[150]->size=sizeof(prec->vals);
  pdbRecordType->papFldDes[150]->offset=(short)((char *)&prec->vals - (char *)prec);
  pdbRecordType->papFldDes[151]->size=sizeof(prec->valt);
  pdbRecordType->papFldDes[151]->offset=(short)((char *)&prec->valt - (char *)prec);
  pdbRecordType->papFldDes[152]->size=sizeof(prec->olda);
  pdbRecordType->papFldDes[152]->offset=(short)((char *)&prec->olda - (char *)prec);
  pdbRecordType->papFldDes[153]->size=sizeof(prec->oldb);
  pdbRecordType->papFldDes[153]->offset=(short)((char *)&prec->oldb - (char *)prec);
  pdbRecordType->papFldDes[154]->size=sizeof(prec->oldc);
  pdbRecordType->papFldDes[154]->offset=(short)((char *)&prec->oldc - (char *)prec);
  pdbRecordType->papFldDes[155]->size=sizeof(prec->oldd);
  pdbRecordType->papFldDes[155]->offset=(short)((char *)&prec->oldd - (char *)prec);
  pdbRecordType->papFldDes[156]->size=sizeof(prec->olde);
  pdbRecordType->papFldDes[156]->offset=(short)((char *)&prec->olde - (char *)prec);
  pdbRecordType->papFldDes[157]->size=sizeof(prec->oldf);
  pdbRecordType->papFldDes[157]->offset=(short)((char *)&prec->oldf - (char *)prec);
  pdbRecordType->papFldDes[158]->size=sizeof(prec->oldg);
  pdbRecordType->papFldDes[158]->offset=(short)((char *)&prec->oldg - (char *)prec);
  pdbRecordType->papFldDes[159]->size=sizeof(prec->oldh);
  pdbRecordType->papFldDes[159]->offset=(short)((char *)&prec->oldh - (char *)prec);
  pdbRecordType->papFldDes[160]->size=sizeof(prec->oldi);
  pdbRecordType->papFldDes[160]->offset=(short)((char *)&prec->oldi - (char *)prec);
  pdbRecordType->papFldDes[161]->size=sizeof(prec->oldj);
  pdbRecordType->papFldDes[161]->offset=(short)((char *)&prec->oldj - (char *)prec);
  pdbRecordType->papFldDes[162]->size=sizeof(prec->oldk);
  pdbRecordType->papFldDes[162]->offset=(short)((char *)&prec->oldk - (char *)prec);
  pdbRecordType->papFldDes[163]->size=sizeof(prec->oldl);
  pdbRecordType->papFldDes[163]->offset=(short)((char *)&prec->oldl - (char *)prec);
  pdbRecordType->papFldDes[164]->size=sizeof(prec->oldm);
  pdbRecordType->papFldDes[164]->offset=(short)((char *)&prec->oldm - (char *)prec);
  pdbRecordType->papFldDes[165]->size=sizeof(prec->oldn);
  pdbRecordType->papFldDes[165]->offset=(short)((char *)&prec->oldn - (char *)prec);
  pdbRecordType->papFldDes[166]->size=sizeof(prec->oldo);
  pdbRecordType->papFldDes[166]->offset=(short)((char *)&prec->oldo - (char *)prec);
  pdbRecordType->papFldDes[167]->size=sizeof(prec->oldp);
  pdbRecordType->papFldDes[167]->offset=(short)((char *)&prec->oldp - (char *)prec);
  pdbRecordType->papFldDes[168]->size=sizeof(prec->oldq);
  pdbRecordType->papFldDes[168]->offset=(short)((char *)&prec->oldq - (char *)prec);
  pdbRecordType->papFldDes[169]->size=sizeof(prec->oldr);
  pdbRecordType->papFldDes[169]->offset=(short)((char *)&prec->oldr - (char *)prec);
  pdbRecordType->papFldDes[170]->size=sizeof(prec->olds);
  pdbRecordType->papFldDes[170]->offset=(short)((char *)&prec->olds - (char *)prec);
  pdbRecordType->papFldDes[171]->size=sizeof(prec->oldt);
  pdbRecordType->papFldDes[171]->offset=(short)((char *)&prec->oldt - (char *)prec);
  pdbRecordType->papFldDes[172]->size=sizeof(prec->ftva);
  pdbRecordType->papFldDes[172]->offset=(short)((char *)&prec->ftva - (char *)prec);
  pdbRecordType->papFldDes[173]->size=sizeof(prec->ftvb);
  pdbRecordType->papFldDes[173]->offset=(short)((char *)&prec->ftvb - (char *)prec);
  pdbRecordType->papFldDes[174]->size=sizeof(prec->ftvc);
  pdbRecordType->papFldDes[174]->offset=(short)((char *)&prec->ftvc - (char *)prec);
  pdbRecordType->papFldDes[175]->size=sizeof(prec->ftvd);
  pdbRecordType->papFldDes[175]->offset=(short)((char *)&prec->ftvd - (char *)prec);
  pdbRecordType->papFldDes[176]->size=sizeof(prec->ftve);
  pdbRecordType->papFldDes[176]->offset=(short)((char *)&prec->ftve - (char *)prec);
  pdbRecordType->papFldDes[177]->size=sizeof(prec->ftvf);
  pdbRecordType->papFldDes[177]->offset=(short)((char *)&prec->ftvf - (char *)prec);
  pdbRecordType->papFldDes[178]->size=sizeof(prec->ftvg);
  pdbRecordType->papFldDes[178]->offset=(short)((char *)&prec->ftvg - (char *)prec);
  pdbRecordType->papFldDes[179]->size=sizeof(prec->ftvh);
  pdbRecordType->papFldDes[179]->offset=(short)((char *)&prec->ftvh - (char *)prec);
  pdbRecordType->papFldDes[180]->size=sizeof(prec->ftvi);
  pdbRecordType->papFldDes[180]->offset=(short)((char *)&prec->ftvi - (char *)prec);
  pdbRecordType->papFldDes[181]->size=sizeof(prec->ftvj);
  pdbRecordType->papFldDes[181]->offset=(short)((char *)&prec->ftvj - (char *)prec);
  pdbRecordType->papFldDes[182]->size=sizeof(prec->ftvk);
  pdbRecordType->papFldDes[182]->offset=(short)((char *)&prec->ftvk - (char *)prec);
  pdbRecordType->papFldDes[183]->size=sizeof(prec->ftvl);
  pdbRecordType->papFldDes[183]->offset=(short)((char *)&prec->ftvl - (char *)prec);
  pdbRecordType->papFldDes[184]->size=sizeof(prec->ftvm);
  pdbRecordType->papFldDes[184]->offset=(short)((char *)&prec->ftvm - (char *)prec);
  pdbRecordType->papFldDes[185]->size=sizeof(prec->ftvn);
  pdbRecordType->papFldDes[185]->offset=(short)((char *)&prec->ftvn - (char *)prec);
  pdbRecordType->papFldDes[186]->size=sizeof(prec->ftvo);
  pdbRecordType->papFldDes[186]->offset=(short)((char *)&prec->ftvo - (char *)prec);
  pdbRecordType->papFldDes[187]->size=sizeof(prec->ftvp);
  pdbRecordType->papFldDes[187]->offset=(short)((char *)&prec->ftvp - (char *)prec);
  pdbRecordType->papFldDes[188]->size=sizeof(prec->ftvq);
  pdbRecordType->papFldDes[188]->offset=(short)((char *)&prec->ftvq - (char *)prec);
  pdbRecordType->papFldDes[189]->size=sizeof(prec->ftvr);
  pdbRecordType->papFldDes[189]->offset=(short)((char *)&prec->ftvr - (char *)prec);
  pdbRecordType->papFldDes[190]->size=sizeof(prec->ftvs);
  pdbRecordType->papFldDes[190]->offset=(short)((char *)&prec->ftvs - (char *)prec);
  pdbRecordType->papFldDes[191]->size=sizeof(prec->ftvt);
  pdbRecordType->papFldDes[191]->offset=(short)((char *)&prec->ftvt - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(cadRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/
