[schematic2]
uniq 4
[tools]
[detail]
[cell use]
use esirs 100 -400 100 0 esirs#1
xform 0 300 -200
p 200 -400 100 1024 1 name:$(top)AIRMASS
use esirs 700 -400 100 0 esirs#2
xform 0 900 -200
p 800 -400 100 1024 1 name:$(top)BIASGATE
use esirs 1300 -400 100 0 esirs#3
xform 0 1500 -200
p 1400 -400 100 1024 1 name:$(top)BIASPWR
use esirs 1900 -400 100 0 esirs#4
xform 0 2100 -200
p 2000 -400 100 1024 1 name:$(top)BIASVDD
use esirs 2500 -400 100 0 esirs#5
xform 0 2700 -200
p 2600 -400 100 1024 1 name:$(top)BIASVRES
use esirs 3100 -400 100 0 esirs#6
xform 0 3300 -200
p 3200 -400 100 1024 1 name:$(top)CAMBENC6
use esirs 3700 -400 100 0 esirs#7
xform 0 3900 -200
p 3800 -400 100 1024 1 name:$(top)CAMBENC7
use esirs 4300 -400 100 0 esirs#8
xform 0 4500 -200
p 4400 -400 100 1024 1 name:$(top)CAMBENC8
use esirs 4900 -400 100 0 esirs#9
xform 0 5100 -200
p 5000 -400 100 1024 1 name:$(top)CAMBENCA
use esirs 5500 -400 100 0 esirs#10
xform 0 5700 -200
p 5600 -400 100 1024 1 name:$(top)CAMCOLD5
use esirs 6100 -400 100 0 esirs#11
xform 0 6300 -200
p 6200 -400 100 1024 1 name:$(top)CAMDETCB
use esirs 6700 -400 100 0 esirs#12
xform 0 6900 -200
p 6800 -400 100 1024 1 name:$(top)CAMHTPWA
use esirs 7300 -400 100 0 esirs#13
xform 0 7500 -200
p 7400 -400 100 1024 1 name:$(top)CAMHTPWB
use esirs -500 0 100 0 esirs#14
xform 0 -300 200
p -400 0 100 1024 1 name:$(top)CAMSETPA
use esirs 100 0 100 0 esirs#15
xform 0 300 200
p 200 0 100 1024 1 name:$(top)CAMSETPB
use esirs 700 0 100 0 esirs#16
xform 0 900 200
p 800 0 100 1024 1 name:$(top)CAMVAC
use esirs 1300 0 100 0 esirs#17
xform 0 1500 200
p 1400 0 100 1024 1 name:$(top)CDSREADS
use esirs 1900 0 100 0 esirs#18
xform 0 2100 200
p 2000 0 100 1024 1 name:$(top)CYCLETYP
use esirs 2500 0 100 0 esirs#19
xform 0 2700 200
p 2600 0 100 1024 1 name:$(top)DACPRE00
use esirs 3100 0 100 0 esirs#20
xform 0 3300 200
p 3200 0 100 1024 1 name:$(top)DACPRE01
use esirs 3700 0 100 0 esirs#21
xform 0 3900 200
p 3800 0 100 1024 1 name:$(top)DACPRE02
use esirs 4300 0 100 0 esirs#22
xform 0 4500 200
p 4400 0 100 1024 1 name:$(top)DACPRE03
use esirs 4900 0 100 0 esirs#23
xform 0 5100 200
p 5000 0 100 1024 1 name:$(top)DACPRE04
use esirs 5500 0 100 0 esirs#24
xform 0 5700 200
p 5600 0 100 1024 1 name:$(top)DACPRE05
use esirs 6100 0 100 0 esirs#25
xform 0 6300 200
p 6200 0 100 1024 1 name:$(top)DACPRE06
use esirs 6700 0 100 0 esirs#26
xform 0 6900 200
p 6800 0 100 1024 1 name:$(top)DACPRE07
use esirs 7300 0 100 0 esirs#27
xform 0 7500 200
p 7400 0 100 1024 1 name:$(top)DACPRE08
use esirs -500 400 100 0 esirs#28
xform 0 -300 600
p -400 400 100 1024 1 name:$(top)DACPRE09
use esirs 100 400 100 0 esirs#29
xform 0 300 600
p 200 400 100 1024 1 name:$(top)DACPRE10
use esirs 700 400 100 0 esirs#30
xform 0 900 600
p 800 400 100 1024 1 name:$(top)DACPRE11
use esirs 1300 400 100 0 esirs#31
xform 0 1500 600
p 1400 400 100 1024 1 name:$(top)DACPRE12
use esirs 1900 400 100 0 esirs#32
xform 0 2100 600
p 2000 400 100 1024 1 name:$(top)DACPRE13
use esirs 2500 400 100 0 esirs#33
xform 0 2700 600
p 2600 400 100 1024 1 name:$(top)DACPRE14
use esirs 3100 400 100 0 esirs#34
xform 0 3300 600
p 3200 400 100 1024 1 name:$(top)DACPRE15
use esirs 3700 400 100 0 esirs#35
xform 0 3900 600
p 3800 400 100 1024 1 name:$(top)DACPRE16
use esirs 4300 400 100 0 esirs#36
xform 0 4500 600
p 4400 400 100 1024 1 name:$(top)DACPRE17
use esirs 4900 400 100 0 esirs#37
xform 0 5100 600
p 5000 400 100 1024 1 name:$(top)DACPRE18
use esirs 5500 400 100 0 esirs#38
xform 0 5700 600
p 5600 400 100 1024 1 name:$(top)DACPRE19
use esirs 6100 400 100 0 esirs#39
xform 0 6300 600
p 6200 400 100 1024 1 name:$(top)DACPRE20
use esirs 6700 400 100 0 esirs#40
xform 0 6900 600
p 6800 400 100 1024 1 name:$(top)DACPRE21
use esirs 7300 400 100 0 esirs#41
xform 0 7500 600
p 7400 400 100 1024 1 name:$(top)DACPRE22
use esirs -500 800 100 0 esirs#42
xform 0 -300 1000
p -400 800 100 1024 1 name:$(top)DACPRE23
use esirs 100 800 100 0 esirs#43
xform 0 300 1000
p 200 800 100 1024 1 name:$(top)DACPRE24
use esirs 700 800 100 0 esirs#44
xform 0 900 1000
p 800 800 100 1024 1 name:$(top)DACPRE25
use esirs 1300 800 100 0 esirs#45
xform 0 1500 1000
p 1400 800 100 1024 1 name:$(top)DACPRE26
use esirs 1900 800 100 0 esirs#46
xform 0 2100 1000
p 2000 800 100 1024 1 name:$(top)DACPRE27
use esirs 2500 800 100 0 esirs#47
xform 0 2700 1000
p 2600 800 100 1024 1 name:$(top)DACPRE28
use esirs 3100 800 100 0 esirs#48
xform 0 3300 1000
p 3200 800 100 1024 1 name:$(top)DACPRE29
use esirs 3700 800 100 0 esirs#49
xform 0 3900 1000
p 3800 800 100 1024 1 name:$(top)DACPRE30
use esirs 4300 800 100 0 esirs#50
xform 0 4500 1000
p 4400 800 100 1024 1 name:$(top)DACPRE31
use esirs 4900 800 100 0 esirs#51
xform 0 5100 1000
p 5000 800 100 1024 1 name:$(top)DATUMCNT
use esirs 5500 800 100 0 esirs#52
xform 0 5700 1000
p 5600 800 100 1024 1 name:$(top)DCKERPOS
use esirs 6100 800 100 0 esirs#53
xform 0 6300 1000
p 6200 800 100 1024 1 name:$(top)DCKRSTEP
use esirs 6700 800 100 0 esirs#54
xform 0 6900 1000
p 6800 800 100 1024 1 name:$(top)DECKERAC
use esirs 7300 800 100 0 esirs#55
xform 0 7500 1000
p 7400 800 100 1024 1 name:$(top)DECKERDC
use esirs -500 1200 100 0 esirs#56
xform 0 -300 1400
p -400 1200 100 1024 1 name:$(top)DECKERDS
use esirs 100 1200 100 0 esirs#57
xform 0 300 1400
p 200 1200 100 1024 1 name:$(top)DECKERHC
use esirs 700 1200 100 0 esirs#58
xform 0 900 1400
p 800 1200 100 1024 1 name:$(top)DECKERIV
use esirs 1300 1200 100 0 esirs#59
xform 0 1500 1400
p 1400 1200 100 1024 1 name:$(top)DECKERRC
use esirs 1900 1200 100 0 esirs#60
xform 0 2100 1400
p 2000 1200 100 1024 1 name:$(top)DECKERSV
use esirs 2500 1200 100 0 esirs#61
xform 0 2700 1400
p 2600 1200 100 1024 1 name:$(top)DHSLABEL
use esirs 3100 1200 100 0 esirs#62
xform 0 3300 1400
p 3200 1200 100 1024 1 name:$(top)EDTACTN
use esirs 3700 1200 100 0 esirs#63
xform 0 3900 1400
p 3800 1200 100 1024 1 name:$(top)EDTFRAME
use esirs 4300 1200 100 0 esirs#64
xform 0 4500 1400
p 4400 1200 100 1024 1 name:$(top)EDTTOTAL
use esirs 4900 1200 100 0 esirs#65
xform 0 5100 1400
p 5000 1200 100 1024 1 name:$(top)EXPTIME
use esirs 5500 1200 100 0 esirs#66
xform 0 5700 1400
p 5600 1200 100 1024 1 name:$(top)FIL1STEP
use esirs 6100 1200 100 0 esirs#67
xform 0 6300 1400
p 6200 1200 100 1024 1 name:$(top)FIL2STEP
use esirs 6700 1200 100 0 esirs#68
xform 0 6900 1400
p 6800 1200 100 1024 1 name:$(top)FILT1AC
use esirs 7300 1200 100 0 esirs#69
xform 0 7500 1400
p 7400 1200 100 1024 1 name:$(top)FILT1DC
use esirs -500 1600 100 0 esirs#70
xform 0 -300 1800
p -400 1600 100 1024 1 name:$(top)FILT1DS
use esirs 100 1600 100 0 esirs#71
xform 0 300 1800
p 200 1600 100 1024 1 name:$(top)FILT1HC
use esirs 700 1600 100 0 esirs#72
xform 0 900 1800
p 800 1600 100 1024 1 name:$(top)FILT1IV
use esirs 1300 1600 100 0 esirs#73
xform 0 1500 1800
p 1400 1600 100 1024 1 name:$(top)FILT1POS
use esirs 1900 1600 100 0 esirs#74
xform 0 2100 1800
p 2000 1600 100 1024 1 name:$(top)FILT1RC
use esirs 2500 1600 100 0 esirs#75
xform 0 2700 1800
p 2600 1600 100 1024 1 name:$(top)FILT1SV
use esirs 3100 1600 100 0 esirs#76
xform 0 3300 1800
p 3200 1600 100 1024 1 name:$(top)FILT2AC
use esirs 3700 1600 100 0 esirs#77
xform 0 3900 1800
p 3800 1600 100 1024 1 name:$(top)FILT2DC
use esirs 4300 1600 100 0 esirs#78
xform 0 4500 1800
p 4400 1600 100 1024 1 name:$(top)FILT2DS
use esirs 4900 1600 100 0 esirs#79
xform 0 5100 1800
p 5000 1600 100 1024 1 name:$(top)FILT2HC
use esirs 5500 1600 100 0 esirs#80
xform 0 5700 1800
p 5600 1600 100 1024 1 name:$(top)FILT2IV
use esirs 6100 1600 100 0 esirs#81
xform 0 6300 1800
p 6200 1600 100 1024 1 name:$(top)FILT2POS
use esirs 6700 1600 100 0 esirs#82
xform 0 6900 1800
p 6800 1600 100 1024 1 name:$(top)FILT2RC
use esirs 7300 1600 100 0 esirs#83
xform 0 7500 1800
p 7400 1600 100 1024 1 name:$(top)FILT2SV
use esirs -500 2000 100 0 esirs#84
xform 0 -300 2200
p -400 2000 100 1024 1 name:$(top)FILTER
use esirs 100 2000 100 0 esirs#85
xform 0 300 2200
p 200 2000 100 1024 1 name:$(top)FOCUSAC
use esirs 700 2000 100 0 esirs#86
xform 0 900 2200
p 800 2000 100 1024 1 name:$(top)FOCUSDC
use esirs 1300 2000 100 0 esirs#87
xform 0 1500 2200
p 1400 2000 100 1024 1 name:$(top)FOCUSDS
use esirs 1900 2000 100 0 esirs#88
xform 0 2100 2200
p 2000 2000 100 1024 1 name:$(top)FOCUSHC
use esirs 2500 2000 100 0 esirs#89
xform 0 2700 2200
p 2600 2000 100 1024 1 name:$(top)FOCUSIV
use esirs 3100 2000 100 0 esirs#90
xform 0 3300 2200
p 3200 2000 100 1024 1 name:$(top)FOCUSPOS
use esirs 3700 2000 100 0 esirs#91
xform 0 3900 2200
p 3800 2000 100 1024 1 name:$(top)FOCUSRC
use esirs 4300 2000 100 0 esirs#92
xform 0 4500 2200
p 4400 2000 100 1024 1 name:$(top)FOCUSSV
use esirs 4900 2000 100 0 esirs#93
xform 0 5100 2200
p 5000 2000 100 1024 1 name:$(top)FOCUSTEP
use esirs 5500 2000 100 0 esirs#94
xform 0 5700 2200
p 5600 2000 100 1024 1 name:$(top)FRMMODE
use esirs 6100 2000 100 0 esirs#95
xform 0 6300 2200
p 6200 2000 100 1024 1 name:$(top)GRISMAC
use esirs 6700 2000 100 0 esirs#96
xform 0 6900 2200
p 6800 2000 100 1024 1 name:$(top)GRISMDC
use esirs 7300 2000 100 0 esirs#97
xform 0 7500 2200
p 7400 2000 100 1024 1 name:$(top)GRISMDS
use esirs -500 2400 100 0 esirs#98
xform 0 -300 2600
p -400 2400 100 1024 1 name:$(top)GRISMHC
use esirs 100 2400 100 0 esirs#99
xform 0 300 2600
p 200 2400 100 1024 1 name:$(top)GRISMIV
use esirs 700 2400 100 0 esirs#100
xform 0 900 2600
p 800 2400 100 1024 1 name:$(top)GRISMRC
use esirs 1300 2400 100 0 esirs#101
xform 0 1500 2600
p 1400 2400 100 1024 1 name:$(top)GRISMSV
use esirs 1900 2400 100 0 esirs#102
xform 0 2100 2600
p 2000 2400 100 1024 1 name:$(top)GRSMPOS
use esirs 2500 2400 100 0 esirs#103
xform 0 2700 2600
p 2600 2400 100 1024 1 name:$(top)GRSMSTEP
use esirs 3100 2400 100 0 esirs#104
xform 0 3300 2600
p 3200 2400 100 1024 1 name:$(top)HEIGHT
use esirs 3700 2400 100 0 esirs#105
xform 0 3900 2600
p 3800 2400 100 1024 1 name:$(top)INTERLCK
use esirs 4300 2400 100 0 esirs#106
xform 0 4500 2600
p 4400 2400 100 1024 1 name:$(top)LOGFILE
use esirs 4900 2400 100 0 esirs#107
xform 0 5100 2600
p 5000 2400 100 1024 1 name:$(top)LOGHOST
use esirs 5500 2400 100 0 esirs#108
xform 0 5700 2600
p 5600 2400 100 1024 1 name:$(top)LOGSIZE
use esirs 6100 2400 100 0 esirs#109
xform 0 6300 2600
p 6200 2400 100 1024 1 name:$(top)LVDTDISP
use esirs 6700 2400 100 0 esirs#110
xform 0 6900 2600
p 6800 2400 100 1024 1 name:$(top)LVDTVLTS
use esirs 7300 2400 100 0 esirs#111
xform 0 7500 2600
p 7400 2400 100 1024 1 name:$(top)LYOTAC
use esirs -500 2800 100 0 esirs#112
xform 0 -300 3000
p -400 2800 100 1024 1 name:$(top)LYOTDC
use esirs 100 2800 100 0 esirs#113
xform 0 300 3000
p 200 2800 100 1024 1 name:$(top)LYOTDS
use esirs 700 2800 100 0 esirs#114
xform 0 900 3000
p 800 2800 100 1024 1 name:$(top)LYOTHC
use esirs 1300 2800 100 0 esirs#115
xform 0 1500 3000
p 1400 2800 100 1024 1 name:$(top)LYOTIV
use esirs 1900 2800 100 0 esirs#116
xform 0 2100 3000
p 2000 2800 100 1024 1 name:$(top)LYOTPOS
use esirs 2500 2800 100 0 esirs#117
xform 0 2700 3000
p 2600 2800 100 1024 1 name:$(top)LYOTRC
use esirs 3100 2800 100 0 esirs#118
xform 0 3300 3000
p 3200 2800 100 1024 1 name:$(top)LYOTSTEP
use esirs 3700 2800 100 0 esirs#119
xform 0 3900 3000
p 3800 2800 100 1024 1 name:$(top)LYOTSV
use esirs 4300 2800 100 0 esirs#120
xform 0 4500 3000
p 4400 2800 100 1024 1 name:$(top)MILLISEC
use esirs 4900 2800 100 0 esirs#121
xform 0 5100 3000
p 5000 2800 100 1024 1 name:$(top)MOSAC
use esirs 5500 2800 100 0 esirs#122
xform 0 5700 3000
p 5600 2800 100 1024 1 name:$(top)MOSBARCD
use esirs 6100 2800 100 0 esirs#123
xform 0 6300 3000
p 6200 2800 100 1024 1 name:$(top)MOSBENC2
use esirs 6700 2800 100 0 esirs#124
xform 0 6900 3000
p 6800 2800 100 1024 1 name:$(top)MOSBENC3
use esirs 7300 2800 100 0 esirs#125
xform 0 7500 3000
p 7400 2800 100 1024 1 name:$(top)MOSBENC4
use esirs -500 3200 100 0 esirs#126
xform 0 -300 3400
p -400 3200 100 1024 1 name:$(top)MOSCIRC1
use esirs 100 3200 100 0 esirs#127
xform 0 300 3400
p 200 3200 100 1024 1 name:$(top)MOSCIRC2
use esirs 700 3200 100 0 esirs#128
xform 0 900 3400
p 800 3200 100 1024 1 name:$(top)MOSCOLD1
use esirs 1300 3200 100 0 esirs#129
xform 0 1500 3400
p 1400 3200 100 1024 1 name:$(top)MOSDC
use esirs 1900 3200 100 0 esirs#130
xform 0 2100 3400
p 2000 3200 100 1024 1 name:$(top)MOSDS
use esirs 2500 3200 100 0 esirs#131
xform 0 2700 3400
p 2600 3200 100 1024 1 name:$(top)MOSHC
use esirs 3100 3200 100 0 esirs#132
xform 0 3300 3400
p 3200 3200 100 1024 1 name:$(top)MOSIV
use esirs 3700 3200 100 0 esirs#133
xform 0 3900 3400
p 3800 3200 100 1024 1 name:$(top)MOSPLT01
use esirs 4300 3200 100 0 esirs#134
xform 0 4500 3400
p 4400 3200 100 1024 1 name:$(top)MOSPLT02
use esirs 4900 3200 100 0 esirs#135
xform 0 5100 3400
p 5000 3200 100 1024 1 name:$(top)MOSPLT03
use esirs 5500 3200 100 0 esirs#136
xform 0 5700 3400
p 5600 3200 100 1024 1 name:$(top)MOSPLT04
use esirs 6100 3200 100 0 esirs#137
xform 0 6300 3400
p 6200 3200 100 1024 1 name:$(top)MOSPLT05
use esirs 6700 3200 100 0 esirs#138
xform 0 6900 3400
p 6800 3200 100 1024 1 name:$(top)MOSPLT06
use esirs 7300 3200 100 0 esirs#139
xform 0 7500 3400
p 7400 3200 100 1024 1 name:$(top)MOSPLT07
use esirs -500 3600 100 0 esirs#140
xform 0 -300 3800
p -400 3600 100 1024 1 name:$(top)MOSPLT08
use esirs 100 3600 100 0 esirs#141
xform 0 300 3800
p 200 3600 100 1024 1 name:$(top)MOSPLT09
use esirs 700 3600 100 0 esirs#142
xform 0 900 3800
p 800 3600 100 1024 1 name:$(top)MOSPLT10
use esirs 1300 3600 100 0 esirs#143
xform 0 1500 3800
p 1400 3600 100 1024 1 name:$(top)MOSPLT11
use esirs 1900 3600 100 0 esirs#144
xform 0 2100 3800
p 2000 3600 100 1024 1 name:$(top)MOSPLTYP
use esirs 2500 3600 100 0 esirs#145
xform 0 2700 3800
p 2600 3600 100 1024 1 name:$(top)MOSPOS
use esirs 3100 3600 100 0 esirs#146
xform 0 3300 3800
p 3200 3600 100 1024 1 name:$(top)MOSRC
use esirs 3700 3600 100 0 esirs#147
xform 0 3900 3800
p 3800 3600 100 1024 1 name:$(top)MOSSTEP
use esirs 4300 3600 100 0 esirs#148
xform 0 4500 3800
p 4400 3600 100 1024 1 name:$(top)MOSSV
use esirs 4900 3600 100 0 esirs#149
xform 0 5100 3800
p 5000 3600 100 1024 1 name:$(top)MOSVAC
use esirs 5500 3600 100 0 esirs#150
xform 0 5700 3800
p 5600 3600 100 1024 1 name:$(top)OBJNAME
use esirs 6100 3600 100 0 esirs#151
xform 0 6300 3800
p 6200 3600 100 1024 1 name:$(top)OBSDEC
use esirs 6700 3600 100 0 esirs#152
xform 0 6900 3800
p 6800 3600 100 1024 1 name:$(top)OBSERVER
use esirs 7300 3600 100 0 esirs#153
xform 0 7500 3800
p 7400 3600 100 1024 1 name:$(top)OBSID
use esirs -500 4000 100 0 esirs#154
xform 0 -300 4200
p -400 4000 100 1024 1 name:$(top)OBSMODE
use esirs 100 4000 100 0 esirs#155
xform 0 300 4200
p 200 4000 100 1024 1 name:$(top)OBSNAME
use esirs 700 4000 100 0 esirs#156
xform 0 900 4200
p 800 4000 100 1024 1 name:$(top)OBSPROGM
use esirs 1300 4000 100 0 esirs#157
xform 0 1500 4200
p 1400 4000 100 1024 1 name:$(top)OBSRA
use esirs 1900 4000 100 0 esirs#158
xform 0 2100 4200
p 2000 4000 100 1024 1 name:$(top)PIXBSCLK
use esirs 2500 4000 100 0 esirs#159
xform 0 2700 4200
p 2600 4000 100 1024 1 name:$(top)POSTSETS
use esirs 3100 4000 100 0 esirs#160
xform 0 3300 4200
p 3200 4000 100 1024 1 name:$(top)PRESETS
use esirs 3700 4000 100 0 esirs#161
xform 0 3900 4200
p 3800 4000 100 1024 1 name:$(top)SECONDS
use esirs 4300 4000 100 0 esirs#162
xform 0 4500 4200
p 4400 4000 100 1024 1 name:$(top)TELESCOP
use esirs 4900 4000 100 0 esirs#163
xform 0 5100 4200
p 5000 4000 100 1024 1 name:$(top)UTEND
use esirs 5500 4000 100 0 esirs#164
xform 0 5700 4200
p 5600 4000 100 1024 1 name:$(top)UTSTART
use esirs 6100 4000 100 0 esirs#165
xform 0 6300 4200
p 6200 4000 100 1024 1 name:$(top)WIDTH
use esirs 6700 4000 100 0 esirs#166
xform 0 6900 4200
p 6800 4000 100 1024 1 name:$(top)WINDOWAC
use esirs 7300 4000 100 0 esirs#167
xform 0 7500 4200
p 7400 4000 100 1024 1 name:$(top)WINDOWDC
use esirs -500 4400 100 0 esirs#168
xform 0 -300 4600
p -400 4400 100 1024 1 name:$(top)WINDOWDS
use esirs 100 4400 100 0 esirs#169
xform 0 300 4600
p 200 4400 100 1024 1 name:$(top)WINDOWHC
use esirs 700 4400 100 0 esirs#170
xform 0 900 4600
p 800 4400 100 1024 1 name:$(top)WINDOWIV
use esirs 1300 4400 100 0 esirs#171
xform 0 1500 4600
p 1400 4400 100 1024 1 name:$(top)WINDOWRC
use esirs 1900 4400 100 0 esirs#172
xform 0 2100 4600
p 2000 4400 100 1024 1 name:$(top)WINDOWSV
use esirs 2500 4400 100 0 esirs#173
xform 0 2700 4600
p 2600 4400 100 1024 1 name:$(top)WINPOS
use esirs 3100 4400 100 0 esirs#174
xform 0 3300 4600
p 3200 4400 100 1024 1 name:$(top)WINSTEP
[comments]
Generated via Perl script version: uff2sadfits.pl,v 0.5 2006/06/22 15:59:26 hon Exp $
