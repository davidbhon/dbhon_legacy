;Positionsin for Decker Wheel
Decker
3 positions
;Offset Name
100    imaging
200    long_slit
300    mos 
;Positions for Mos Slit position
MOS
20 positions
;Offset Name
100    imaging
200    circle1
300    circle
400    1pix-slit
500    2pix-slit
600    3pix-slit
700    4pix-slit
800    5pix-slit
900    6pix-slit
1000   7pix-slit
1100   8pix-slit
1200   mos1
1300   mos2
1400   mos3
1500   mos4
1600   mos5
1700   mos6
1800   mos7
1900   mos8
2000   mos9
;Positions for grism Wheel
Grism
5 positions
;Offset Name
100    open
200    JH
300    HK
400    JHK
500    TBD1
;Positions for Filter Wheel 1
Filter1
9 positions
;Offset Name
100    open}
200    dark
300    J-lo
400    J
500    H
600    Ks
700    JH
800    HK
900    TBD1
;Positions for Lyot Wheel
Lyot
5 postions
;Offset Name
100    f/16
200    MCAO_over
300    MCAO_under
400    H1
500    H2
;Positions for Filter Wheel 2
Filter2
13 positions
;Offset Name
100    Blankoff
200    Open
300    K
400    L
500    M
600    N_wide
700    Qa
800    Qb
900    Q_wide
1000    Spare-1
1100    Spare-2
1200    Spare-3
1300    Spare-4
;Positions for detector Focus Wheel
Focus
2 positions
;Offset Name
100    Imaging
200    Spectroscopy
