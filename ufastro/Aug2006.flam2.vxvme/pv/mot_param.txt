;Motor Parameter File
; $Name:  $ $Id: mot_param.txt 14 2008-06-11 01:49:45Z hon $
;cc agent host IP number
000.000.000.000
;port number
52024
;Number of Motors
8
;# IS  TS  A   D  HC DC AN  HS  FHS HD  BL  PORT    EN       Name
 1 25 100 255 255  0 25  A  100  0  0   0  52024  Window   Window wheel
 2 25 100 255 255  0 25  B  100  0  0   0  52024  MOS      mos slit position
 3 25 100 255 255  0 25  C  100  0  0   0  52024  Decker   Decker Wheel
 4 25 600 255 255  0 25  D  600  0  0   0  52024  Filter1  Filter Wheel
 5 25 200 255 255  0 25  E  200  0  0   0  52024  Lyot     Lyot Wheel
 6 25 600 255 255  0 25  F  600  0  0   0  52024  Filter2  Filter Wheel 2
 7 25 600 255 255  0 25  G  600  0  0   0  52024  Grism    grism Wheel
 8 25 100 255 255  0 25  H  100  0  0   0  52024  Focus    detector Focus
;
;#  : Motor Number
;IS : Initial Speed
;TS : Terminal Speed
;A  : Acceleration
;D  : Deceleration
;HC : Hold Current
;DC : Drive Current
;AN : Axis Name (one character)
;HS : Homing Speed
;FHS: Final Homing Speed
;HD : Homing Direction 
;BL : Back-Lash correction param.
;EN : Epics Name
;Name : Motor name
;
; NOTE: There can be any number of comments between the above params
;       and the following positions,
;       but a line with keyword positions (in capitals)
;	must appear once (and only once in file) just before first ;Mot_# line.
;
;POSITIONS:
;Mot_#   #_Positions for Decker Wheel
1  4 Window
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 100     1.00         2.0     28.0      Closed
 2 200     1.00         2.0     28.0      Open
 3 300     1.00         2.0     28.0      Park
 4   0     1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Mos Slit
2  20 MOS
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0       0.50         2.0     28.0      imaging
 2 1417    0.97         0.5     18.3      mos1
 3 1958    0.97         0.5     18.3      2pix-slit
 4 2500    0.97         0.5     18.3      mos2
 5 3042    0.97         0.5     18.3      3pix-slit
 6 3583    0.97         0.5     18.3      mos3
 7 5000    0.97         0.5     18.3      circle1
 8 6417    0.97         0.5     18.3      mos4
 9 6958    0.97         0.5     18.3      4pix-slit
10 7500    0.97         0.5     18.3      mos5
11 8042    0.97         0.5     18.3      6pix-slit
12 8583    0.95         0.2     27.5      mos6
13 10000    0.95         0.2     27.5      circle2
14 11417    0.95         0.2     27.5      mos7
15 11958    0.95         0.2     27.5      8pix-slit
16 12500    0.95         0.2     27.5      mos8
17 13042    0.95         0.2     27.5      1pix-slit
18 13583    0.95         0.2     27.5      mos9
19 0    0.95         0.2     27.5      Park
20 0    0.95         0.2     27.5      Datum
;Mot_#   #_Positions for Decker Wheel
3  5 Decker
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0     1.00         2.0     28.0      Imaging
 2 1667  0.97         0.5     18.3      Long_slit
 3 3333  0.97         0.5     18.3      MOS
 4 0     0.97         0.5     18.3      Park
 5 0     0.97         0.5     18.3      Datum
;Mot_#   #_Positions for Filter Wheel 1
4  7 Filter1
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0     1.00         2.0     28.0      Open
 2 13800 0.71        17.60    25.38     J
 3 27600 0.95         7.39     8.08     H
 4 41400 0.77         8.39     8.81     Ks
 5 55200 0.95         8.35     9.13     J-lo
 6 0     1.00         2.0     28.0      Park
 7 0     0.92         9.22    10.15     Datum
;Mot_#   #_Positions for Lyot Wheel
5  5 Lyot
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0     1.00         2.0     28.0      f/16
 2 575   1.00         2.0     28.0      MCAO_over
 3 1150  1.00         2.0     28.0      MCAO_under
 4 1725  0.97         2.0     28.0      Hartmann1
 5 2300  1.00         2.0     28.0      Hartmann2
 6 0     1.00         2.0     28.0      Park
 7 0     0.92         9.22    10.15     Datum
;Mot_#   #_Positions for Filter Wheel 2
6  7 Filter2
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0     1.00         2.0     28.0      Open
 2 13800 0.71        17.60    25.38     JH
 3 27600 0.95         7.39     8.08     HK
 4 41400 0.77         8.39     8.81     Ks_bad
 5 55200 0.95         8.35     9.13     Dark
 6 0     1.00         2.0     28.0      Park
 7 0     0.92         9.22    10.15     Datum
7  3 Grism
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0      1.00         2.0     28.0      open
 2 13800     1.00         2.0     28.0      JH
 3 27600     1.00         2.0     28.0      HK
 4 41400     0.97         0.5     18.3      Open2
 5 55200     1.00         2.0     28.0      Dark
 6 0     1.00         2.0     28.0      Park
 7 0     0.92         9.22    10.15     Datum
;Mot_#   #_Positions detector focus Wheel
8  2 Focus
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 1000  1.00         2.0     28.0      f/16
 2 1500  1.00         2.0     28.0      MCAO_under
 3 1500  1.00         2.0     28.0      MCAO_over
 6 0     1.00         2.0     28.0      Park
 7 0     0.92         9.22    10.15     Datum
;Mot_#   #_Positions Window Wheel
