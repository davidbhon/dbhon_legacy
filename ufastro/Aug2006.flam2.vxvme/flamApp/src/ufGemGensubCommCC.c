#if !defined(__UFGEMGENSUBCOMMCC_C__)
#define __UFGEMGENSUBCOMMCC_C__ "RCS: $Name:  $ $Id: ufGemGensubCommCC.c,v 0.5 2006/06/28 20:25:24 rojas Exp $"
static const char rcsIdufGEMGENSUBCOMMCCC[] = __UFGEMGENSUBCOMMCC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>
#include <sysLib.h>
#include <unistd.h>

/* use  select on socfd -- hon */
#include <sockLib.h>

#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
/* #include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <dbStaticLib.h>
#include <dbBase.h>

#include "flam.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"
#include "ufLog.h"
/* #include "ufdbl.h" */

static char cc_DEBUG_MODE[6];
static char cc_STARTUP_DEBUG_MODE[6];
static int cc_initialized = 0;

static char cc_host_ip[20];
static long cc_port_no;

static long number_motors;
static int counter = 0;
static long updates_count;

void init_cc_config()
{
  FILE *ccFile = 0;
  char in_str[256];
  char *temp_str = 0;

  ccFile = fopen( ccconfig_filename, "r" ) ;
  if( NULL == ccFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s",
	       ccconfig_filename ) ;
      ufLog( _UFerrmsg ) ;
      return ;
    }

  NumFiles++;
  fgets (in_str, 256, ccFile);
  fgets (in_str, 256, ccFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (cc_STARTUP_DEBUG_MODE, in_str);
  fclose (ccFile);
  NumFiles--;

  if ((strcmp (cc_STARTUP_DEBUG_MODE, "NONE") != 0) ||
      (strcmp (cc_STARTUP_DEBUG_MODE, "MIN") != 0) ||
      (strcmp (cc_STARTUP_DEBUG_MODE, "FULL") != 0))
    strcpy (cc_STARTUP_DEBUG_MODE, "NONE");

  strcpy (cc_DEBUG_MODE, "FULL");

  cc_initialized = 1;
  fprintf( stderr, __HERE__ "> CC Initialized\n" ) ;
}

long
ufNamedPosGensub (genSubRecord * gsp)
{
  return OK;
}

long
ufmrgCarGensubInit (genSubRecord * gsp)
{
  size_t i = 0;
  int *last_command = 0;

  strcpy (gsp->a, "");	/* Sector Wheel */
  strcpy (gsp->b, "");	/* Window Changer */
  strcpy (gsp->c, "");	/* Aperture Wheel */
  strcpy (gsp->d, "");	/* Filter Wheel 1 */
  strcpy (gsp->e, "");	/* Lyot Wheel */
  strcpy (gsp->f, "");	/* Filter Wheel 2 */
  strcpy (gsp->g, "");	/* Pupil Image */
  strcpy (gsp->h, "");	/* Slit Wheel */
  strcpy (gsp->i, "");	/* Grating Wheel */
  strcpy (gsp->k, "");	/* Cold Clamp */
  strcpy (gsp->l, "");	/* Temperature Set */

  last_command = (int *) malloc (sizeof (int) * 10);
  for (i = 0; i <= 9; i++)
    last_command[i] = CAR_IDLE;

  strcpy (gsp->vald, "IDLE");
  strcpy (gsp->valc, "GOOD");

  gsp->dpvt = (void *) last_command;

  return OK ;
}

long
ufmrgCarGensubProc (genSubRecord * gsp)
{
  long status = 0;
  static char temp_str[40];
  static char out_message[40];
  long action = CAR_IDLE;
  int *last_command;

  strcpy (out_message, "");

  /* Check the Sector Wheel Car */
  strcpy (temp_str, gsp->a);

  /* Check the Window Changer Car */
  strcpy (temp_str, gsp->b);

  /* Check the Aperture Wheel Car */
  strcpy (temp_str, gsp->c);

  /* Check the Filter Wheel 1 Car */
  strcpy (temp_str, gsp->d);

  /* Check the Lyot Wheel Car */
  strcpy (temp_str, gsp->e);

  /* Check the Filter Wheel 2 Car */
  strcpy (temp_str, gsp->f);

  /* Check the Pupil Image Car */
  strcpy (temp_str, gsp->g);

  /* Check the Slit Wheel Car */
  strcpy (temp_str, gsp->h);

  /* Check the Grating Wheel Car */
  strcpy (temp_str, gsp->i);

  /* Check the Cold Clamp Car */
  strcpy (temp_str, gsp->k);

  if ((strcmp (gsp->a, "ERR") == 0) || (strcmp (gsp->b, "ERR") == 0)
      || (strcmp (gsp->c, "ERR") == 0) || (strcmp (gsp->d, "ERR") == 0)
      || (strcmp (gsp->e, "ERR") == 0) || (strcmp (gsp->f, "ERR") == 0)
      || (strcmp (gsp->g, "ERR") == 0) || (strcmp (gsp->h, "ERR") == 0)
      || (strcmp (gsp->i, "ERR") == 0) || (strcmp (gsp->k, "ERR") == 0))
    action = CAR_ERROR;

  if (action == CAR_IDLE)
    {
      if ((strcmp (gsp->a, "BUSY") == 0) || (strcmp (gsp->b, "BUSY") == 0)
	  || (strcmp (gsp->c, "BUSY") == 0) || (strcmp (gsp->d, "BUSY") == 0)
	  || (strcmp (gsp->e, "BUSY") == 0) || (strcmp (gsp->f, "BUSY") == 0)
	  || (strcmp (gsp->g, "BUSY") == 0) || (strcmp (gsp->h, "BUSY") == 0)
	  || (strcmp (gsp->i, "BUSY") == 0) || (strcmp (gsp->k, "BUSY") == 0))
	action = CAR_BUSY;
    }

  /* Let's figure out the state of the system and its health */

  if (action == CAR_IDLE)
    {
      strcpy (gsp->vald, "IDLE");
      strcpy (gsp->valc, "GOOD");
    }
  else
    {
      if (action == CAR_BUSY)
	{
	  strcpy (gsp->vald, "BUSY");
	  strcpy (gsp->valc, "GOOD");
	}
      else
	{
	  strcpy (gsp->vald, "ERROR");
	  strcpy (gsp->valc, "BAD");
	}
    }
  /* Let's see if we can figure out the status of the last command */
  /* first let's find out which one changed */

  last_command = (int *) gsp->dpvt;


  if (status_changed (gsp->a, last_command[0]))
    {
      if (strcmp (gsp->a, "IDLE") == 0)
	{
	  action = CAR_IDLE;
	  last_command[0] = CAR_IDLE;
	}
      else
	{
	  if (strcmp (gsp->a, "BUSY") == 0)
	    {
	      action = CAR_BUSY;
	      last_command[0] = CAR_BUSY;
	    }
	  else
	    {
	      action = CAR_ERROR;
	      last_command[0] = CAR_ERROR;
	      strcpy (out_message, gsp->l);
	      /* do we need 0.5 delay here if the CAR is
	         IDLE? or should we do a Callback? */
	      if (*(long *) gsp->valb == CAR_IDLE)
		{

		}
	    }
	}
    }
  else				/* Well link 'A' did not change, let keep going */
    {
      if (status_changed (gsp->b, last_command[1]))
	{
	  if (strcmp (gsp->b, "IDLE") == 0)
	    {
	      action = CAR_IDLE;
	      last_command[1] = CAR_IDLE;
	    }
	  else
	    {
	      if (strcmp (gsp->b, "BUSY") == 0)
		{
		  action = CAR_BUSY;
		  last_command[1] = CAR_BUSY;
		}
	      else
		{
		  action = CAR_ERROR;
		  last_command[1] = CAR_ERROR;
		  strcpy (out_message, gsp->m);
		  /* do we need 0.5 delay here if the CAR
		     is IDLE? or should we do a Callback? */
		  if (*(long *) gsp->valb == CAR_IDLE)
		    {

		    }
		}
	    }
	}
      else		/* Well link 'B' did not change, let keep going */
	{
	  if (status_changed (gsp->c, last_command[2]))
	    {
	      if (strcmp (gsp->c, "IDLE") == 0)
		{
		  action = CAR_IDLE;
		  last_command[2] = CAR_IDLE;
		}
	      else
		{
		  if (strcmp (gsp->c, "BUSY") == 0)
		    {
		      action = CAR_BUSY;
		      last_command[2] = CAR_BUSY;
		    }
		  else
		    {
		      action = CAR_ERROR;
		      last_command[2] = CAR_ERROR;
		      strcpy (out_message, gsp->n);
		      /* do we need 0.5 delay here if the
		         CAR is IDLE? or should we do a Callback? */
		      if (*(long *) gsp->valb == CAR_IDLE)
			{

			}
		    }
		}
	    }
	  else		/* Well link 'C' did not change, let keep going */
	    {
	      if (status_changed (gsp->d, last_command[3]))
		{
		  if (strcmp (gsp->d, "IDLE") == 0)
		    {
		      action = CAR_IDLE;
		      last_command[3] = CAR_IDLE;
		    }
		  else
		    {
		      if (strcmp (gsp->d, "BUSY") == 0)
			{
			  action = CAR_BUSY;
			  last_command[3] = CAR_BUSY;
			}
		      else
			{
			  action = CAR_ERROR;
			  last_command[3] = CAR_ERROR;
			  strcpy (out_message, gsp->o);
			  /* do we need 0.5 delay here if
			     the CAR is IDLE? or should we do a Callback? */
			  if (*(long *) gsp->valb == CAR_IDLE)
			    {

			    }
			}
		    }
		}
	      else	/* Well link 'D' did not change, let keep going */
		{
		  if (status_changed (gsp->e, last_command[4]))
		    {
		      if (strcmp (gsp->e, "IDLE") == 0)
			{
			  action = CAR_IDLE;
			  last_command[4] = CAR_IDLE;
			}
		      else
			{
			  if (strcmp (gsp->e, "BUSY") == 0)
			    {
			      action = CAR_BUSY;
			      last_command[4] = CAR_BUSY;
			    }
			  else
			    {
			      action = CAR_ERROR;
			      last_command[4] = CAR_ERROR;
			      strcpy (out_message, gsp->p);
			      /* do we need 0.5 delay here
			         if the CAR is IDLE? or
			         should we do a Callback? */
			      if (*(long *) gsp->valb == CAR_IDLE)
				{

				}
			    }
			}
		    }
		  else	/* Well link 'E' did not change, let keep going */
		    {
		      if (status_changed (gsp->f, last_command[5]))
			{
			  if (strcmp (gsp->f, "IDLE") == 0)
			    {
			      action = CAR_IDLE;
			      last_command[5] = CAR_IDLE;
			    }
			  else
			    {
			      if (strcmp (gsp->f, "BUSY") == 0)
				{
				  action = CAR_BUSY;
				  last_command[5] = CAR_BUSY;
				}
			      else
				{
				  action = CAR_ERROR;
				  last_command[5] = CAR_ERROR;
				  strcpy (out_message, gsp->q);
				  /* do we need 0.5 delay here if the CAR is
				     IDLE? or should we do a Callback? */
				  if (*(long *) gsp->valb == CAR_IDLE)
				    {

				    }
				}
			    }
			}
		      else /* Well link 'F' did not change, let keep going */
			{
			  if (status_changed (gsp->g, last_command[6]))
			    {
			      if (strcmp (gsp->g, "IDLE") == 0)
				{
				  action = CAR_IDLE;
				  last_command[6] = CAR_IDLE;
				}
			      else
				{
				  if (strcmp (gsp->g, "BUSY") == 0)
				    {
				      action = CAR_BUSY;
				      last_command[6] = CAR_BUSY;
				    }
				  else
				    {
				      action = CAR_ERROR;
				      last_command[7] = CAR_ERROR;
				      strcpy (out_message, gsp->r);
				      /* do we need 0.5 delay here if the CAR 
				      is IDLE? or should we do a Callback? */
				      if (*(long *) gsp->valb == CAR_IDLE)
					{

					}
				    }
				}
			    }
			  else /* Well link 'G' did not change,let keep going */
			    {
			      if (status_changed (gsp->h, last_command[7]))
				{
				  if (strcmp (gsp->h, "IDLE") == 0)
				    {
				      action = CAR_IDLE;
				      last_command[7] = CAR_IDLE;
				    }
				  else
				    {
				      if (strcmp (gsp->h, "BUSY") == 0)
					{
					  action = CAR_BUSY;
					  last_command[7] = CAR_BUSY;
					}
				      else
					{
					  action = CAR_ERROR;
					  last_command[7] = CAR_ERROR;
					  strcpy (out_message, gsp->s);
					  /* do we need 0.5 delay here if
					     the CAR is IDLE? or should we do a Callback? */
					  if (*(long *) gsp->valb == CAR_IDLE)
					    {
					    }
					}
				    }
				}
			      else	/* Well link 'H' did not change, let keep going */
				{
				  if (status_changed
				      (gsp->i, last_command[8]))
				    {
				      if (strcmp (gsp->i, "IDLE") == 0)
					{
					  action = CAR_IDLE;
					  last_command[8] = CAR_IDLE;
					}
				      else
					{
					  if (strcmp (gsp->i, "BUSY") == 0)
					    {
					      action = CAR_BUSY;
					      last_command[8] = CAR_BUSY;
					    }
					  else
					    {
					      action = CAR_ERROR;
					      last_command[8] = CAR_ERROR;
					      strcpy (out_message, gsp->t);
					      if (*(long *) gsp->valb == CAR_IDLE)
						{
						}
					    }
					}
				    }
				  else	/* Well link 'I' did not change, let keep going */
				    {
				      if (status_changed
					  (gsp->k, last_command[9]))
					{
					  if (strcmp (gsp->k, "IDLE") == 0)
					    {
					      action = CAR_IDLE;
					      last_command[9] = CAR_IDLE;
					    }
					  else
					    {
					      if (strcmp (gsp->k, "BUSY") == 0)
						{
						  action = CAR_BUSY;
						  last_command[9] = CAR_BUSY;
						}
					      else
						{
						  action = CAR_ERROR;
						  last_command[9] = CAR_ERROR;
						  strcpy (out_message,
							  gsp->u);
						  if (*(long *) gsp->valb == CAR_IDLE)
						    {
						    }
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }


  strcpy (gsp->vala, out_message);
  *(long *) gsp->valb = action;

  return status;
}

/******************** INAM for motorG *******************/
long
ufmotorGinit (genSubRecord * pgs)
{
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubMotPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;

  /* 
   *  Create a private control structure for this gensub record
   */

  pPriv = (genSubMotPrivate *) malloc (sizeof (genSubMotPrivate));
  if (pPriv == NULL)
    {
      trx_debug ("Can't create private structure", pgs->name, "NONE", "NONE");
      return -1;
    }

  /* 
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
   */

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status)
    {
      trx_debug ("can't locate CAR IVAL field", pgs->name, "NONE", "NONE");
      return -1;
    }

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status)
    {
      trx_debug ("can't locate CAR IMSS field", pgs->name, "NONE", "NONE");
      return -1;
    }

  /* trx_debug("Assigning initial parameters",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Assign private information needed */
  pPriv->port_no = cc_port_no;  
  pPriv->agent = 3;		/* motor agent */

  pPriv->CurrParam.init_velocity = -1.0;
  pPriv->CurrParam.slew_velocity = -1.0;
  pPriv->CurrParam.acceleration = -1.0;
  pPriv->CurrParam.deceleration = -1.0;
  pPriv->CurrParam.drive_current = -1.0;
  pPriv->CurrParam.datum_speed = -1;
  pPriv->CurrParam.datum_direction = -1;
  pPriv->CurrParam.command_mode = SIMM_NONE;	/* Simulation 
						   Mode
						   mode
						   (start
						   in real
						   mode) */
  strcpy (pPriv->mot_inp.home_switch, "enabled");
  pPriv->CurrParam.steps_to_add = 0.0;
  pPriv->CurrParam.homing = 0;
  pPriv->CurrParam.backlash = 0.0;
  pPriv->CurrParam.setting_time = -1.0;
  pPriv->CurrParam.jog_speed_slow = -1;
  pPriv->CurrParam.jog_speed_hi = -1;
  /* trx_debug("Reading initial value from file",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* trx_debug("Clearing Input links",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the Gensub inputs */
  *(long *) pgs->a = -1;	/* perform test */
  *(long *) pgs->c = -1;	/* Simulation Mode */
  *(long *) pgs->e = -1;	/* datum directive */
  *(double *) pgs->g = 0.0;	/* num steps */
  strcpy (pgs->i, "");		/* stop message */
  strcpy (pgs->j, "");		/* response from the agent
				   via CA */
  /* or the task that receives the response via TCP/IP */
  strcpy (pgs->k, "");		/* abort message */

  strcpy (pgs->m, "NONE");	/* debug mode start in NONE 
				 */
  *(long *) pgs->u = -1;	/* origin directive */

  /* *(double *)pgs->n = -1.0 ; *//* init velocity */
  /* *(double *)pgs->o = -1.0 ; *//* slew velocity */
  /* *(double *)pgs->p = -1.0 ; *//* acceleration */
  /* *(double *)pgs->q = -1.0 ; *//* deceleration */
  /* *(double *)pgs->r = -1.0 ; *//* drive current */
  /* *(double *)pgs->s = -1.0 ; *//* datum speed */
  /* *(long *)pgs->t = -1 ; *//* datum direction */


  /* trx_debug("Clearing output links",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL)
    {
      trx_debug ("can't create a callback", pgs->name, "NONE", "NONE");
      return -1;
    }

  pPriv->pCallback->pRecord = (struct dbCommon *) pgs;

  pPriv->commandState = TRX_GS_INIT;

  requestCallback (pPriv->pCallback, TRX_INIT_CC_AGENT_WAIT);

  /* Clear the input structure */
  pPriv->mot_inp.command_mode   = -1;
  pPriv->mot_inp.test_directive = -1;
  pPriv->mot_inp.datum_motor_directive = -1;
  pPriv->mot_inp.init_velocity = -1.0;
  pPriv->mot_inp.slew_velocity = -1.0;
  pPriv->mot_inp.acceleration  = -1.0;
  pPriv->mot_inp.deceleration  = -1.0;
  pPriv->mot_inp.drive_current = -1.0;
  pPriv->mot_inp.datum_speed   = -1.0;
  pPriv->mot_inp.datum_direction = -1;
  pPriv->mot_inp.num_steps       = -1.0;
  pPriv->mot_inp.setting_time    = -1.0;
  pPriv->mot_inp.jog_speed_slow  = -1;
  pPriv->mot_inp.jog_speed_hi    = -1;
  strcpy (pPriv->mot_inp.stop_mess, "");
  strcpy (pPriv->mot_inp.abort_mess, "");
  pPriv->mot_inp.origin_directive = -1;
  strcpy (pPriv->mot_inp.home_switch, "enabled");

  /* 
   *  Save the private control structure in the record's device
   *  private field.
   */

  pgs->dpvt = (void *) pPriv;

  /* 
   *  And do some last minute initialization stuff
   */

  return OK;
}

/********* function to read parameters from the motor file ********/
/********* calls UFReadPositions at end to load named positions ***/

long read_mot_file( char *rec_name, genSubMotPrivate * pPriv )
{
  FILE *motFile = 0;
  char in_str[256];
  double numnum = 0.0 ;
  double portNo;
  int found = 0;
  char short_name[40];
  char some_str[40];
  char *some_str2 = 0;
  long mot_num = 0;
  double init_vel = 0.0, slew_vel = 0.0, accel = 0.0, decel = 0.0;
  double hold_curr = 0.0, drive_curr = 0.0, backlash = 0.0;
  char indexer = 0;
  long home_speed = 0, final_home_speed = 0;
  long home_dir = 0;
  int i = 0;
  long nparmsRead=0;

  counter++;
  strcpy (some_str, rec_name);

  some_str2 = strrchr (some_str, ':');
  some_str2[0] = '\0';
  some_str2 = strrchr (some_str, ':');
  some_str2 = some_str2 + 1;
  strcpy (short_name, some_str2);

  motFile = fopen( mot_filename, "r" ) ;
  if( NULL == motFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s",
	       mot_filename ) ;
      ufLog( _UFerrmsg ) ;
      return -1 ;
    }

  /* Read the host IP number */
  NumFiles++;

  fgets (in_str, sizeof(in_str), motFile);	/* comment line */
  fgets (in_str, sizeof(in_str), motFile);	/* comment line */
  fgets (in_str, sizeof(in_str), motFile);	/* comment line */
  fgets (in_str, sizeof(in_str), motFile);	/* IP number */

  strcpy (cc_host_ip, in_str);
  i = 0;

  while( ((isdigit (cc_host_ip[i])) || (cc_host_ip[i] == '.')) && (i < 15) )
    i++;
  cc_host_ip[i] = '\0';

  /* Read the port number */
  fgets (in_str, sizeof(in_str), motFile);
  fscanf (motFile, "%lf", &numnum);

  cc_port_no = (long) numnum; 

  fgets (in_str, sizeof(in_str), motFile); 
  fgets (in_str, sizeof(in_str), motFile);	/* comment line */

  fscanf (motFile, "%lf", &numnum);
  number_motors = (long) numnum;


  fgets (in_str, sizeof(in_str), motFile);	/* read to end of line */
  fgets (in_str, sizeof(in_str), motFile);	/* comment line */
  found = FALSE;

  while( !found && (nparmsRead < number_motors) )
    {
      /* read the parameters for a motor */
      fscanf( motFile, "%ld %lf %lf %lf %lf %lf %lf %c %ld %ld %ld %lf %lf",
	      &mot_num, &init_vel, &slew_vel, &accel, &decel, &hold_curr,
	      &drive_curr, &indexer, &home_speed, &final_home_speed, &home_dir,
	      &backlash, &portNo );

      fgets( in_str, sizeof(in_str), motFile ); /*remainder of motor CAD name */
      ++nparmsRead;
				/* if corresponding motor then load params */
      if( strstr( in_str, short_name ) != NULL ) 
	{
	  found = TRUE;
	  pPriv->mot_num = mot_num;
	  pPriv->CurrParam.init_velocity = init_vel;
	  pPriv->CurrParam.slew_velocity = slew_vel;
	  pPriv->CurrParam.acceleration = accel;
	  pPriv->CurrParam.deceleration = decel;
	  pPriv->CurrParam.drive_current = drive_curr;
	  pPriv->indexer = indexer;
	  pPriv->CurrParam.datum_speed = (long) home_speed;
	  pPriv->CurrParam.datum_direction = home_dir;
	  pPriv->CurrParam.backlash = backlash;
	  pPriv->CurrParam.final_datum_speed = final_home_speed;
	  pPriv->CurrParam.setting_time = 0.0;
	  pPriv->CurrParam.jog_speed_slow = 0;
	  pPriv->CurrParam.jog_speed_hi = 0;

	  sprintf(_UFerrmsg,__HERE__"> %ld, %ld, %d, %d, %d, %d, %d, %d, %c, %ld, %ld, %ld, %d, %s %ld",
		  nparmsRead, mot_num, (int)init_vel, (int)slew_vel, (int)accel, (int)decel,
		  (int)hold_curr, (int)drive_curr, indexer, home_speed, final_home_speed,
		  home_dir, (int)backlash, short_name, portNo );
	  ufLog( _UFerrmsg ) ;
	}
    }

  if( found ) { 
     /* UFReadPositions( motFile, number_motors, mot_num );  */
  }
  else { 
    sprintf(_UFerrmsg,__HERE__">last:%ld,%ld,%d,%d,%d,%d,%d,%d,%c,%ld,%ld,%ld,%d, %s",
	   nparmsRead,mot_num,(int)init_vel,(int)slew_vel,(int)accel,(int)decel,
	   (int)hold_curr,(int)drive_curr,indexer,home_speed,final_home_speed,
	   home_dir,(int)backlash,in_str );
    ufLog( _UFerrmsg ) ;
  }
 printf("calling to UFReadPositions \n");
  UFReadPositions( motFile, number_motors, mot_num ); 
printf("After UFReadPositions \n"); 
  fclose( motFile );
printf("After fclose \n");
  NumFiles--;
printf("After NumFiles-- \n");

  return OK;
}

/******************** SNAM for motorG *******************/
long
ufmotorGproc (genSubRecord * pgs)
{
  genSubMotPrivate *pPriv;
  long status = OK;
  char response[40];
  long some_num;
  long ticksNow;

  char *endptr;
  int num_str = 0;
  static char **com;
  int i;
  char car_name[31];
  char sad_name[31];
  long timeout;
  double numnum;

  static struct dbAddr motorCarVal;
  static long motorCar;
  long options = 0;
  long nRequest = 1;

  strcpy (cc_DEBUG_MODE, pgs->m);


  pPriv = (genSubMotPrivate *) pgs->dpvt;
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */
  switch (pPriv->commandState)
    {
    case TRX_GS_INIT:

      debugPGS( __HERE__, pgs ) ;
      if (flam_initialized != 1) init_flam_config ();
      if (!cc_initialized) init_cc_config ();

      pPriv->CurrParam.steps_from_home = -1; /* assume motor position unknown */
      pPriv->socfd = -1;     /* socket */

      *(long *) pgs->vala = SIMM_NONE;	
      *(long *) pgs->valb = (long) pPriv->socfd; 
      strcpy(pgs->valc, "");	
      *(double *) pgs->vald = pPriv->CurrParam.init_velocity;
      *(double *) pgs->vale = pPriv->CurrParam.slew_velocity;
      *(double *) pgs->valf = pPriv->CurrParam.acceleration;
      *(double *) pgs->valg = pPriv->CurrParam.deceleration;
      *(double *) pgs->valh = pPriv->CurrParam.drive_current;
      *(double *) pgs->vali = pPriv->CurrParam.datum_speed;
      *(long *) pgs->valj = pPriv->CurrParam.datum_direction;
      *(double *) pgs->l = pPriv->CurrParam.backlash;
      *(double *) pgs->n = pPriv->CurrParam.init_velocity;
      *(double *) pgs->o = pPriv->CurrParam.slew_velocity;
      *(double *) pgs->p = pPriv->CurrParam.acceleration;
      *(double *) pgs->q = pPriv->CurrParam.deceleration;
      *(double *) pgs->r = pPriv->CurrParam.drive_current;
      *(double *) pgs->s = pPriv->CurrParam.datum_speed;
      *(long *) pgs->t = pPriv->CurrParam.datum_direction;

      /* Spawn a task to be the receiver from the agent */
      if ((pPriv->socfd > 0) && (RECV_MODE))
	{
	  trx_debug ("Spawning a task to receive TCP/IP", pgs->name, "FULL",
		     cc_STARTUP_DEBUG_MODE);
	}

      pPriv->commandState = TRX_GS_DONE;

      break;

      /* 
       *  In idle state we wait for one of the inputs to 
       *  change indicating that a new command has arrived. Status
       *  and updating from the Agent comes during BUSY state.
       */
    case TRX_GS_DONE:

      /* Check to see if the input has changed. */
      trx_debug ("Processing from DONE state", pgs->name, "FULL",
		 cc_DEBUG_MODE);

      pPriv->mot_inp.test_directive = *(long *) pgs->a;
      strcpy (pPriv->mot_inp.home_switch, pgs->b);

      pPriv->mot_inp.command_mode = *(long *) pgs->c;
      pPriv->mot_inp.datum_motor_directive = *(long *) pgs->e;
      pPriv->mot_inp.num_steps = *(double *) pgs->g;
      pPriv->mot_inp.origin_directive = *(long *) pgs->u;
      if (*(double *) pgs->n == pPriv->CurrParam.init_velocity)
	pPriv->mot_inp.init_velocity = -1.0;
      else
	pPriv->mot_inp.init_velocity = *(double *) pgs->n;
      if (*(double *) pgs->o == pPriv->CurrParam.slew_velocity)
	pPriv->mot_inp.slew_velocity = -1.0;
      else
	pPriv->mot_inp.slew_velocity = *(double *) pgs->o;
      if (*(double *) pgs->p == pPriv->CurrParam.acceleration)
	pPriv->mot_inp.acceleration = -1.0;
      else
	pPriv->mot_inp.acceleration = *(double *) pgs->p;
      if (*(double *) pgs->q == pPriv->CurrParam.deceleration)
	pPriv->mot_inp.deceleration = -1.0;
      else
	pPriv->mot_inp.deceleration = *(double *) pgs->q;
      if (*(double *) pgs->r == pPriv->CurrParam.drive_current)
	pPriv->mot_inp.drive_current = -1.0;
      else
	pPriv->mot_inp.drive_current = *(double *) pgs->r;
      if (*(double *) pgs->s == pPriv->CurrParam.datum_speed)
	pPriv->mot_inp.datum_speed = -1.0;
      else
	pPriv->mot_inp.datum_speed = *(double *) pgs->s;
      if (*(long *) pgs->t == pPriv->CurrParam.datum_direction)
	pPriv->mot_inp.datum_direction = -1;
      else
	pPriv->mot_inp.datum_direction = *(long *) pgs->t;
      if (*(double *) pgs->l == pPriv->CurrParam.backlash)
	pPriv->mot_inp.backlash = 0.0;
      else
	pPriv->mot_inp.backlash = *(double *) pgs->l;
      if ((long) pPriv->CurrParam.setting_time != -1)
	pPriv->mot_inp.setting_time = pPriv->CurrParam.setting_time;
      if ((long) pPriv->CurrParam.jog_speed_slow != -1)
	pPriv->mot_inp.jog_speed_slow = pPriv->CurrParam.jog_speed_slow;
      if ((long) pPriv->CurrParam.jog_speed_hi != -1)
	pPriv->mot_inp.jog_speed_hi = pPriv->CurrParam.jog_speed_hi;

      strcpy (pPriv->mot_inp.stop_mess, pgs->i);
      strcpy (pPriv->mot_inp.abort_mess, pgs->k);

      if ((pPriv->mot_inp.test_directive != -1) ||
	  (pPriv->mot_inp.command_mode != -1) ||
	  (pPriv->mot_inp.datum_motor_directive != -1) ||
	  (pPriv->mot_inp.origin_directive != -1) ||
	  ((long) pPriv->mot_inp.num_steps != 0) ||
	  ((long) pPriv->mot_inp.init_velocity != -1) ||
	  ((long) pPriv->mot_inp.slew_velocity != -1) ||
	  ((long) pPriv->mot_inp.acceleration != -1) ||
	  ((long) pPriv->mot_inp.deceleration != -1) ||
	  ((long) pPriv->mot_inp.drive_current != -1) ||
	  ((long) pPriv->mot_inp.datum_speed != -1) ||
	  (pPriv->mot_inp.datum_direction != -1) ||
	  (strcmp (pPriv->mot_inp.stop_mess, "") != 0) ||
	  (strcmp (pPriv->mot_inp.abort_mess, "") != 0))
	{
	  /* A new command has arrived so set the CAR to busy */
	  strcpy (pPriv->errorMessage, "");
 	  status = dbPutField (&pPriv->carinfo.carMessage,
			       DBR_STRING, pPriv->errorMessage, 1);
	  if (status)
	    {
	      trx_debug ("can't clear CAR message", pgs->name, "NONE",
			 cc_DEBUG_MODE);
	      return OK;
	    } 

	  trx_debug ("Setting the car to BUSY.",pgs->name,"FULL",cc_DEBUG_MODE);
	  some_num = CAR_BUSY;
	  status = dbPutField (&pPriv->carinfo.carState,
			       DBR_LONG, &some_num, 1);
	  if (status)
	    {
	      trx_debug ("can't set CAR to busy", pgs->name, "NONE",
			 cc_DEBUG_MODE);
	      return OK;
	    } 
	  /* set up a CALLBACK after 0.5 seconds in order
	     to send it.  set the Command state to SENDING */
	  trx_debug ("Setting Command State",pgs->name,"FULL",cc_DEBUG_MODE);

	  pPriv->commandState = TRX_GS_SENDING;
	  requestCallback (pPriv->pCallback, TRX_ERROR_DELAY);

	  /* Clear the input links */

	  *(long *) pgs->a = -1;	/* perform test */
	  *(long *) pgs->c = -1;	/* Simulation Mode */
	  *(long *) pgs->e = -1;	/* datum directive */
	  *(double *) pgs->g = 0.0;	/* num steps */
	  strcpy (pgs->i, "");	/* stop message */
	  strcpy (pgs->j, "");	/* response from the agent via CA */
	  /* or the task that receives the response via TCP/IP */
	  pgs->noj = 1;
	  strcpy (pgs->k, "");	/* abort message */
	  *(long *) pgs->u = -1;	/* stop directive */
	}
      else
	{
	}
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",cc_DEBUG_MODE);

      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */

      if (pPriv->mot_inp.command_mode != -1)
	{	
      	 printf ("ufmotorGProc> We have INIT Command focd = %ld\n",pPriv->socfd); 

	  /* Check first to see if we have a valid INIT directive */
	  if ((pPriv->mot_inp.command_mode == SIMM_NONE) ||
	      (pPriv->mot_inp.command_mode == SIMM_FAST) ||
	      (pPriv->mot_inp.command_mode == SIMM_FULL))
	    {
	      
	      if (pPriv->socfd >= 0)
		{
		  if (checkSoc (pPriv->socfd, 0.0) > 0)
		    printf("ufmotorGproc> socket is writable, no reconnect\n");
		  else
		    {
		      trx_debug ("Closing curr connection",pgs->name,"FULL",
				 	cc_DEBUG_MODE);
		      ufClose (pPriv->socfd);
		      pPriv->socfd = -1;
		      			
		      *(long *)pgs->valb = (long)pPriv->socfd; 
		    }
		}  
	      pPriv->CurrParam.command_mode = pPriv->mot_inp.command_mode;
	      *(long *) pgs->vala = (long) pPriv->CurrParam.command_mode;
	      pPriv->CurrParam.init_velocity   = -1.0;
	      pPriv->CurrParam.slew_velocity   = -1.0;
	      pPriv->CurrParam.acceleration    = -1.0;
	      pPriv->CurrParam.deceleration    = -1.0;
	      pPriv->CurrParam.drive_current   = -1.0;
	      pPriv->CurrParam.datum_speed     = -1;
	      pPriv->CurrParam.datum_direction = -1;
	      pPriv->CurrParam.setting_time    = -1.0;
	      pPriv->CurrParam.jog_speed_slow  = -1;
	      pPriv->CurrParam.jog_speed_hi    = -1;
	      pPriv->CurrParam.backlash        = 0.0;
	      printf("calling read_mot_file \n");
	      if (read_mot_file (pgs->name, pPriv) == OK)
		pPriv->send_init_param = 1;
	      printf("after read_mot_file \n");
	
	      pPriv->port_no = cc_port_no;
	      *(double *) pgs->l = 0.0;	 /* init velocity */
	      *(double *) pgs->n = -1.0; /* init velocity */
	      *(double *) pgs->o = -1.0; /* slew velocity */
	      *(double *) pgs->p = -1.0; /* acceleration */
	      *(double *) pgs->q = -1.0; /* deceleration */
	      *(double *) pgs->r = -1.0; /* drive current */
	      *(double *) pgs->s = -1.0; /* datum speed */
	      *(long *) pgs->t   = -1;	 /* datum direction */

	      if (pPriv->mot_inp.command_mode != SIMM_FAST)
		{
		  if (pPriv->socfd < 0)
		     {
			taskDelay(120);
  printf ("ufmotorGProc>nam:%s pt:%ld host:%ld\n",pgs->name,pPriv->port_no,cc_host_ip); 
		     pPriv->socfd=UFGetConnection(pgs->name,pPriv->port_no, 
					cc_host_ip); 
		     *(long *) pgs->valb = (long) pPriv->socfd; /* socket */
      	printf ("ufmotorGProc> After GETConn focd = %ld\n",pPriv->socfd); 
		     }
		  if (pPriv->socfd < 0)
		    {		       /* we have a bad socket connection */
		     pPriv->commandState = TRX_GS_DONE;
		     strcpy(pPriv->errorMessage,"Error Conn. to Agent ***");
		     printf("ufMotorGproc> Error Conn. to Agent *** rec: %s\n",				pgs->name);
		      
		      status = dbPutField (&pPriv->carinfo.carMessage,
					   DBR_STRING,pPriv->errorMessage,1);
		      if (status)
			{
			  trx_debug ("can't set CAR message",pgs->name,
				     "NONE",cc_DEBUG_MODE);
			  return OK;
			}
		      some_num = CAR_ERROR;
		      status = dbPutField(&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to ERROR", pgs->name,
				     "NONE", cc_DEBUG_MODE);
			  return OK;
			}
		    }
		}
	      printf(" before  == SIMM_FAST \n");
	      if ((pPriv->mot_inp.command_mode == SIMM_FAST)
		  || (pPriv->socfd > 0))
		{
		  *(double *) pgs->vald = pPriv->CurrParam.init_velocity;
		  *(double *) pgs->vale = pPriv->CurrParam.slew_velocity;
		  *(double *) pgs->valf = pPriv->CurrParam.acceleration;
		  *(double *) pgs->valg = pPriv->CurrParam.deceleration;
		  *(double *) pgs->valh = pPriv->CurrParam.drive_current;
		  *(double *) pgs->vali = pPriv->CurrParam.datum_speed;
		  *(long *) pgs->valj   = pPriv->CurrParam.datum_direction;
		  *(double *) pgs->vall = pPriv->CurrParam.backlash;

		  pPriv->commandState = TRX_GS_DONE;
		  some_num = CAR_IDLE;
		  status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				       &some_num, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR to IDLE", pgs->name, "NONE",
				 cc_DEBUG_MODE);
		      return OK;
		    }
		}
	      printf(" before  == SIMM_FAST \n");
	    }
	  else if (pPriv->mot_inp.command_mode == SIMM_VSM) 
	    {
	      printf("calling read_mot_file 2 \n");
	      if (read_mot_file (pgs->name, pPriv) == OK)
		pPriv->send_init_param = 1;

	      printf ("After read_mot_file  2\n");

	      pPriv->CurrParam.command_mode = pPriv->mot_inp.command_mode;

	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pPriv->errorMessage, "");
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug("can't set CAR message",
					pgs->name,"NONE",cc_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_IDLE;
	      status=dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
	      if (status)
		trx_debug("can't set CAR to ERROR",pgs->name,"NONE",
					cc_DEBUG_MODE);
	      return OK;
	    }
	  else /* we have an error in input of the init command */
	    {
	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pPriv->errorMessage, "Bad INIT Directive CC");
	      /* set the CAR to ERR */
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     cc_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status )
		{
		  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",
			     cc_DEBUG_MODE);
		  return OK;
		}
	    }
	}
      else  /* We a move command */
	{
	  trx_debug("We have motor Command",pgs->name,"FULL",cc_DEBUG_MODE);

	  if (pPriv->CurrParam.command_mode == SIMM_VSM) 
	    {
	      /* Reset Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pPriv->errorMessage, "");
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		  trx_debug("can't set CAR message",pgs->name,"NONE",
					cc_DEBUG_MODE);
	      some_num = CAR_IDLE;
	      status=dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
	      if (status)
		  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",
					cc_DEBUG_MODE);
	      return OK;
	    }

	  /* Save the current parameters */
	  if ((pPriv->mot_inp.datum_motor_directive == 1)
	      && (strcmp (pPriv->mot_inp.home_switch, "disabled") == 0))
	    {
	      /* we have a home directive but the home switch is disabled */

	      /* Clear the input structure */
	      pPriv->mot_inp.command_mode          = -1;
	      pPriv->mot_inp.test_directive        = -1;
	      pPriv->mot_inp.datum_motor_directive = -1;
	      pPriv->mot_inp.init_velocity         = -1.0;
	      pPriv->mot_inp.slew_velocity         = -1.0;
	      pPriv->mot_inp.acceleration          = -1.0;
	      pPriv->mot_inp.deceleration          = -1.0;
	      pPriv->mot_inp.drive_current         = -1.0;
	      pPriv->mot_inp.datum_speed           = -1.0;
	      pPriv->mot_inp.datum_direction       = -1;
	      pPriv->mot_inp.num_steps      	   = 0.0;
	      strcpy (pPriv->mot_inp.stop_mess, "");
	      strcpy (pPriv->mot_inp.abort_mess, "");
	      pPriv->mot_inp.origin_directive      = -1;
	      pPriv->mot_inp.setting_time    	   = -1.0;
	      pPriv->mot_inp.jog_speed_slow    	   = -1;
	      pPriv->mot_inp.jog_speed_hi  	   = -1;
	      pPriv->mot_inp.backlash  		   = 0.0;

	      pPriv->commandState = TRX_GS_DONE;

	      /* send an error message */
	      strcpy (pPriv->errorMessage, "Home switch is disabled");
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     		cc_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     cc_DEBUG_MODE);
		  return OK;
		}
	    }
	  else	/* go ahead */
	    {
	     pPriv->OldParam.init_velocity = pPriv->CurrParam.init_velocity;
	     pPriv->OldParam.slew_velocity = pPriv->CurrParam.slew_velocity;
	     pPriv->OldParam.acceleration  = pPriv->CurrParam.acceleration;
	     pPriv->OldParam.deceleration  = pPriv->CurrParam.deceleration;
	     pPriv->OldParam.drive_current = pPriv->CurrParam.drive_current;
	     pPriv->OldParam.datum_speed   = pPriv->CurrParam.datum_speed;
	     pPriv->OldParam.datum_direction=pPriv->CurrParam.datum_direction;
	     pPriv->OldParam.backlash       = pPriv->CurrParam.backlash;

	     /* get the input into the current parameters */
	     if((long) pPriv->mot_inp.init_velocity != -1)
	       pPriv->CurrParam.init_velocity = pPriv->mot_inp.init_velocity;
	      if((long) pPriv->mot_inp.slew_velocity != -1)
		pPriv->CurrParam.slew_velocity = pPriv->mot_inp.slew_velocity;
	      if ((long) pPriv->mot_inp.acceleration != -1)
		pPriv->CurrParam.acceleration = pPriv->mot_inp.acceleration;
	      if ((long) pPriv->mot_inp.deceleration != -1)
		pPriv->CurrParam.deceleration = pPriv->mot_inp.deceleration;
	      if ((long) pPriv->mot_inp.drive_current != -1)
		pPriv->CurrParam.drive_current = pPriv->mot_inp.drive_current;
	      if (pPriv->mot_inp.datum_speed != -1)
		pPriv->CurrParam.datum_speed = pPriv->mot_inp.datum_speed;
	      if (pPriv->mot_inp.datum_direction != -1)
		pPriv->CurrParam.datum_direction=pPriv->mot_inp.datum_direction;
	      if ((long) pPriv->mot_inp.backlash != 0)
		pPriv->CurrParam.backlash = pPriv->mot_inp.backlash;

	      /* see if this a command right after an init */
	      if (pPriv->send_init_param)
		{
		pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity;
		pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity;
		pPriv->mot_inp.acceleration  = pPriv->CurrParam.acceleration;
		pPriv->mot_inp.deceleration  = pPriv->CurrParam.deceleration;
		pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current;
		pPriv->mot_inp.datum_speed   = pPriv->CurrParam.datum_speed;
		pPriv->mot_inp.datum_direction=pPriv->CurrParam.datum_direction;
		pPriv->mot_inp.backlash      = pPriv->CurrParam.backlash;
		}
	      /* formulate the command strings */
	      com = malloc (50 * sizeof (char *));
	      for (i = 0; i < 50; i++)
		com[i] = malloc (80 * sizeof (char));

	      strcpy (car_name, pgs->name);
	      car_name[strlen (car_name)-1] = '\0';
	      strcat (car_name, "C.IVAL");
	      status = dbNameToAddr (car_name, &motorCarVal);
	      if (status)
		logMsg ("can't locate PV: %s", (int) &car_name, 0, 0, 0, 0, 0);

	      strcpy (sad_name, "flam2:sad:");
	      strcat (sad_name,namedPositionsTable[pPriv->mot_num-1].motorName);
	      strcat (sad_name,  "RawPos");


	      num_str =	UFcheck_motor_inputs (pPriv->mot_inp,com,pPriv->indexer,
					   car_name,sad_name,&pPriv->CurrParam,
					   pPriv->CurrParam.command_mode);

	      /* Clear the input structure */

	      pPriv->mot_inp.command_mode          = -1;
	      pPriv->mot_inp.test_directive        = -1;
	      pPriv->mot_inp.datum_motor_directive = -1;
	      pPriv->mot_inp.init_velocity         = -1.0;
	      pPriv->mot_inp.slew_velocity         = -1.0;
	      pPriv->mot_inp.acceleration          = -1.0;
	      pPriv->mot_inp.deceleration   	   = -1.0;
	      pPriv->mot_inp.drive_current 	   = -1.0;
	      pPriv->mot_inp.datum_speed 	   = -1.0;
	      pPriv->mot_inp.datum_direction       = -1;
	      pPriv->mot_inp.num_steps             = 0.0;
	      strcpy (pPriv->mot_inp.stop_mess, "");
	      strcpy (pPriv->mot_inp.abort_mess, "");
	      pPriv->mot_inp.origin_directive      = -1;
	      pPriv->mot_inp.setting_time          = -1.0;
	      pPriv->mot_inp.jog_speed_slow        = -1;
	      pPriv->mot_inp.jog_speed_hi          = -1;
	      pPriv->mot_inp.backlash              = 0.0;

	      /* do we have commands to send? */
	      if (num_str > 0)
		 {
		 /* if no Agent needed then go back to DONE 
		  * we are talking about SIMM_FAST or commands that do not 
		  * need the agent 
		  */

		  if ((pPriv->CurrParam.command_mode == SIMM_FAST))
		    {
		      for (i = 0; i < 50; i++)
			free (com[i]);
		      free (com);
		      /* set Command state to DONE */
		      pPriv->commandState = TRX_GS_DONE;
		      /* update the output links */
		      *(double *) pgs->vald = pPriv->CurrParam.init_velocity;
		      *(double *) pgs->vale = pPriv->CurrParam.slew_velocity;
		      *(double *) pgs->valf = pPriv->CurrParam.acceleration;
		      *(double *) pgs->valg = pPriv->CurrParam.deceleration;
		      *(double *) pgs->valh = pPriv->CurrParam.drive_current;
		      *(double *) pgs->vali = pPriv->CurrParam.datum_speed;
		      *(long *) pgs->valj = pPriv->CurrParam.datum_direction;
		      *(double *) pgs->vall = pPriv->CurrParam.backlash;
		      pPriv->CurrParam.setting_time = -1.0;
		      pPriv->CurrParam.jog_speed_slow = -1;
		      pPriv->CurrParam.jog_speed_hi = -1;
		      pPriv->send_init_param = 0;
		      some_num = CAR_IDLE;
		      status = dbPutField (&pPriv->carinfo.carState,
					   DBR_LONG, &some_num, 1);
		      if (status)
			{
			  logMsg ("can't set CAR to IDLE\n", 0, 0, 0, 0, 0, 0);
			  return OK;
			}
		    }
		  else		/* we are in SIMM_NONE or SIMM_FULL */
		    { /* See if you can send the command and go to BUSY state */
		     if (pPriv->socfd > 0)
			{
		       status=ufStringsSend(pPriv->socfd,com,num_str,pgs->name);
			}
		     else
		       status = 0;

		      for (i = 0; i < 50; i++) free (com[i]);
		      free (com);

		      if (status == 0)
			{/*          error sending the commands to the agent */
			pPriv->commandState = TRX_GS_DONE;
			strcpy(pPriv->errorMessage,"Bad socket connection CC1");
		  	/* set the CAR to ERR */
			status = dbPutField (&pPriv->carinfo.carMessage,
				       DBR_STRING, pPriv->errorMessage, 1);
			  if (status)
			    {
			      trx_debug ("can't set CAR message", pgs->name,
					 "NONE", cc_DEBUG_MODE);
			      return OK;
			    }
			  some_num = CAR_ERROR;
			  status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,
					&some_num, 1);
			  if (status)
			    {
			      trx_debug ("can't set CAR to ERROR", pgs->name,
					 "NONE", cc_DEBUG_MODE);
			      return OK;
			    }
			}
		      else
			{			/* send successful */
			updates_count = 1;
			requestCallback(pPriv->pCallback,TRX_CC_AGENT_TIMEOUT);
			pPriv->startingTicks = tickGet ();
			pPriv->commandState = TRX_GS_BUSY;
			}
		    }
		}
	      else  /* case of: num_str <= 0 */
		{
		  for (i = 0; i < 50; i++)
		    free (com[i]);
		  free (com);
		  
		  pPriv->commandState = TRX_GS_DONE;
		  if (num_str < 0)
		    {
		      /* we have an error in the Input */
		      strcpy (pPriv->errorMessage, "Error in input CC");
		  
		      status = dbPutField (&pPriv->carinfo.carMessage,
					   DBR_STRING, pPriv->errorMessage,1);
		      if (status)
			{
			  trx_debug ("can't set CAR message", pgs->name,
				     "NONE", cc_DEBUG_MODE);
			  return OK;
			}
		      some_num = CAR_ERROR;
		      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to ERROR", pgs->name,
				     "NONE", cc_DEBUG_MODE);
			  return OK;
			}
		    }
		  else	/* case of: num_str == 0 */
		    {
		      /* We have had a change in input. The inputs got sent 
		       * to be checked but no string got formulated, because 
		       * the motor is already at requested position, so just 
		       * pretend like a successful move and set CAR to IDLE. 
		       */

		      sprintf( _UFerrmsg, __HERE__ "> NOOP for %s",pgs->name );
		      ufLog( _UFerrmsg );
		      pPriv->commandState = TRX_GS_DONE;

		      /* update the output links */
		      *(double *) pgs->vald = pPriv->CurrParam.init_velocity;
		      *(double *) pgs->vale = pPriv->CurrParam.slew_velocity;
		      *(double *) pgs->valf = pPriv->CurrParam.acceleration;
		      *(double *) pgs->valg = pPriv->CurrParam.deceleration;
		      *(double *) pgs->valh = pPriv->CurrParam.drive_current;
		      *(double *) pgs->vali = pPriv->CurrParam.datum_speed;
		      *(long *) pgs->valj = pPriv->CurrParam.datum_direction;
		      *(double *) pgs->vall = pPriv->CurrParam.backlash;
		      pPriv->CurrParam.setting_time = -1.0;
		      pPriv->CurrParam.jog_speed_slow = -1;
		      pPriv->CurrParam.jog_speed_hi = -1;
		      pPriv->CurrParam.homing = 0;
		      pPriv->send_init_param = 0;

		      /* wait 1 sec before resetting CARo BUSY->IDLE is seen */
		      taskDelay(sysClkRateGet());
 		      some_num = CAR_IDLE;
		      status = dbPutField (&pPriv->carinfo.carState,
					   DBR_LONG, &some_num, 1);
		      if (status)
			{
			  logMsg ("can't set CAR to IDLE\n", 0, 0, 0, 0, 0, 0);
			  return OK;
			} 
		    }
		}
	    }
	}

      break;


    case TRX_GS_BUSY:

      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */

      if ((strcmp (pgs->i, "") != 0) || (strcmp (pgs->k, "") != 0))
	{
	  cancelCallback (pPriv->pCallback);
	  strcpy (pPriv->mot_inp.stop_mess, pgs->i);
	  strcpy (pPriv->mot_inp.abort_mess, pgs->k);
	  strcpy (pgs->i, "");
	  strcpy (pgs->k, "");
	  pPriv->send_init_param = 0;
	  requestCallback (pPriv->pCallback, 0);
	  pPriv->commandState = TRX_GS_DONE;
	  return OK;
	}

       status = dbGetField (&motorCarVal,
                             DBR_LONG, &motorCar, &options, &nRequest, NULL);
       if (status)
           logMsg ("can't read from motorC.IVAL", 0, 0, 0, 0, 0, 0);

      if (motorCar == CAR_IDLE)
	 {
	      cancelCallback (pPriv->pCallback);
	      pPriv->commandState = TRX_GS_DONE;
	      pPriv->CurrParam.homing = 0; 

	      *(double *) pgs->vald = pPriv->CurrParam.init_velocity;
	      *(double *) pgs->vale = pPriv->CurrParam.slew_velocity;
	      *(double *) pgs->valf = pPriv->CurrParam.acceleration;
	      *(double *) pgs->valg = pPriv->CurrParam.deceleration;
	      *(double *) pgs->valh = pPriv->CurrParam.drive_current;
	      *(double *) pgs->vali = pPriv->CurrParam.datum_speed;
	      *(long *) pgs->valj = pPriv->CurrParam.datum_direction;
	      *(double *) pgs->vall = pPriv->CurrParam.backlash;
	      pPriv->CurrParam.setting_time = -1.0;
	      pPriv->CurrParam.jog_speed_slow = -1;
	      pPriv->CurrParam.jog_speed_hi = -1;
	      pPriv->send_init_param = 0;
	}
	else
	{ 
	  /* is this a CALLBACK timeout? or someone pushed the apply? */
	  /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */

 	  ticksNow = tickGet ();
	  timeout = TRX_CC_AGENT_TIMEOUT;
	  if ((ticksNow >= (pPriv->startingTicks + timeout)) &&
	      (strcmp (pPriv->errorMessage, "CALLBACK") == 0))
	    {
	      pPriv->CurrParam.homing = 0;
	      pPriv->commandState = TRX_GS_DONE;

	      strcpy (pPriv->errorMessage, " RRO - Motor Command Timed out");
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     cc_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     cc_DEBUG_MODE);
		}
	    }

	} 
      break;
    }	     

  return OK;
}

/**********************************************************/

/* Function to distribue one input to more than one output */
void distribute_str_input (genSubRecord * pgs, char link, int num_links)
{

  char input_str[39];

  if (link == 'a')
    strcpy (input_str, pgs->a);
  else if (link == 'b')
    strcpy (input_str, pgs->b);
  else if (link == 'c')
    strcpy (input_str, pgs->c);
  else if (link == 'd')
    strcpy (input_str, pgs->d);
  else if (link == 'e')
    strcpy (input_str, pgs->e);
  else if (link == 'f')
    strcpy (input_str, pgs->f);
  else if (link == 'g')
    strcpy (input_str, pgs->g);
  else if (link == 'h')
    strcpy (input_str, pgs->h);
  else if (link == 'i')
    strcpy (input_str, pgs->i);
  else if (link == 'j')
    strcpy (input_str, pgs->j);
  else if (link == 'k')
    strcpy (input_str, pgs->k);
  else if (link == 'l')
    strcpy (input_str, pgs->l);
  else if (link == 'm')
    strcpy (input_str, pgs->m);
  else if (link == 'n')
    strcpy (input_str, pgs->n);
  else if (link == 'o')
    strcpy (input_str, pgs->o);
  else if (link == 'p')
    strcpy (input_str, pgs->p);
  else if (link == 'q')
    strcpy (input_str, pgs->q);
  else if (link == 'r')
    strcpy (input_str, pgs->r);
  else if (link == 's')
    strcpy (input_str, pgs->s);
  else if (link == 't')
    strcpy (input_str, pgs->t);
  else if (link == 'u')
    strcpy (input_str, pgs->u);
  else
    strcpy (input_str, "");

  strcpy (pgs->vala, input_str);
  if (num_links > 1)
    strcpy (pgs->valb, input_str);
  if (num_links > 2)
    strcpy (pgs->valc, input_str);
  if (num_links > 3)
    strcpy (pgs->vald, input_str);
  if (num_links > 4)
    strcpy (pgs->vale, input_str);
  if (num_links > 5)
    strcpy (pgs->valf, input_str);
  if (num_links > 6)
    strcpy (pgs->valg, input_str);
  if (num_links > 7)
    strcpy (pgs->valh, input_str);
  if (num_links > 8)
    strcpy (pgs->vali, input_str);
  if (num_links > 9)
    strcpy (pgs->valj, input_str);
  if (num_links > 10)
    strcpy (pgs->valk, input_str);
  if (num_links > 11)
    strcpy (pgs->vall, input_str);
  if (num_links > 12)
    strcpy (pgs->valm, input_str);
  if (num_links > 13)
    strcpy (pgs->valn, input_str);
  if (num_links > 14)
    strcpy (pgs->valo, input_str);
  if (num_links > 15)
    strcpy (pgs->valp, input_str);
  if (num_links > 16)
    strcpy (pgs->valq, input_str);
  if (num_links > 17)
    strcpy (pgs->valr, input_str);
  if (num_links > 18)
    strcpy (pgs->vals, input_str);
  if (num_links > 19)
    strcpy (pgs->valt, input_str);
  if (num_links > 20)
    strcpy (pgs->valu, input_str);
  return;
}

/******************** SNAM for ccdebug *******************/
long
ufccdebugproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'j', 10);
  return OK;
}

/******************** SNAM for ccInitS *******************/
long
ufccinitSproc (genSubRecord * pgs)
{
  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

/******************** SNAM for ccParkS *******************/
long
ufccparkSproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

/******************** SNAM for ccstopS *******************/
long
ufccstopSproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

/******************** SNAM for ccAbortS *******************/
long
ufccabortSproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

/******************** SNAM for ccTestS *******************/
long
ufcctestSproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

/******************** SNAM for ccTestS *******************/
long
ufccdaatumSproc (genSubRecord * pgs)
{

  if (strcmp (pgs->a, "") != 0)
    distribute_str_input (pgs, 'a', 10);
  return OK;
}

#endif /* __UFGEMGENSUBCOMMCC_C__ */
