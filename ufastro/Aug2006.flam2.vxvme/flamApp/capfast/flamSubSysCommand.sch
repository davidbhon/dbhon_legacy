[schematic2]
uniq 220
[tools]
[detail]
w -214 1627 -100 0 START inhier.START.P -304 1616 -64 1616 ebos.ebos#103.SLNK
w -94 1227 100 0 n#204 estringouts.estringouts#116.SLNK -304 80 -416 80 -416 1216 288 1216 288 1648 192 1648 ebos.ebos#103.FLNK
w 42 587 100 0 n#215 elongouts.elongouts#117.FLNK 320 224 352 224 352 576 -208 576 -208 912 -64 912 flamTimer.flamTimer#160.START
w 1954 331 100 0 n#165 estringouts.estringouts#126.OUT 1920 320 2048 320 2048 64 junction
w 1218 75 -100 0 n#165 estringouts.estringouts#116.OUT -48 64 2544 64 outhier.CAR_IMSS.p
w 754 331 100 0 n#165 estringouts.estringouts#167.OUT 736 320 832 320 832 64 junction
w 1234 331 100 0 n#165 estringouts.estringouts#168.OUT 1216 320 1312 320 1312 64 junction
w 2066 1131 100 0 n#201 efanouts.efanouts#136.LNK3 2368 1344 2528 1344 2528 1120 1664 1120 1664 512 1344 512 1344 336 1664 336 estringouts.estringouts#126.SLNK
w 2098 1099 100 0 n#200 efanouts.efanouts#121.LNK3 2368 1696 2560 1696 2560 1088 1696 1088 1696 800 2048 800 elongouts.elongouts#124.SLNK
w 1906 363 100 0 n#195 estringouts.estringouts#126.FLNK 1920 352 1952 352 1952 544 junction
w 1650 555 100 0 n#195 estringouts.estringouts#168.FLNK 1216 352 1312 352 1312 544 2048 544 elongouts.elongouts#125.SLNK
w 1042 555 100 0 n#195 estringouts.estringouts#167.FLNK 736 352 832 352 832 544 1312 544 junction
w 1218 587 100 0 n#198 efanouts.efanouts#155.LNK2 1504 848 1632 848 1632 576 864 576 864 336 960 336 estringouts.estringouts#168.SLNK
w 530 587 100 0 n#197 efanouts.efanouts#181.LNK2 608 848 736 848 736 576 384 576 384 336 480 336 estringouts.estringouts#167.SLNK
w 2322 523 -100 0 n#162 elongouts.elongouts#125.OUT 2304 512 2400 512 2400 160 junction
w 1402 171 -100 0 n#162 elongouts.elongouts#117.OUT 320 160 2544 160 outhier.CAR_IVAL.p
w 2322 779 100 0 n#162 junction 2400 512 2400 768 2304 768 elongouts.elongouts#124.OUT
w 2402 843 -100 0 DONE elongouts.elongouts#124.FLNK 2304 832 2560 832 outhier.DONE.p
w 2466 1771 -100 0 Reset efanouts.efanouts#121.LNK1 2368 1760 2624 1760 2624 1408 junction
w 1106 1035 -100 0 Reset efanouts.efanouts#136.LNK1 2368 1408 2624 1408 2624 1024 -352 1024 -352 1376 -64 1376 ebos.ebos#104.SLNK
w 642 891 -100 0 Reset efanouts.efanouts#181.LNK1 608 880 736 880 736 1024 junction
w 1538 891 -100 0 Reset efanouts.efanouts#155.LNK1 1504 880 1632 880 1632 1024 junction
w 914 1163 100 0 n#193 efanouts.efanouts#150.LNK1 1824 1648 2016 1648 2016 1152 -128 1152 -128 816 -64 816 flamTimer.flamTimer#160.STOP
w 1362 1195 100 0 n#192 efanouts.efanouts#150.LNK2 1824 1616 1984 1616 1984 1184 800 1184 800 912 864 912 flamTimer.flamTimer#170.START
w 2450 1387 100 0 n#109 efanouts.efanouts#136.LNK2 2368 1376 2592 1376 junction
w 1650 1067 100 0 n#109 efanouts.efanouts#121.LNK2 2368 1728 2592 1728 2592 1056 768 1056 768 816 864 816 flamTimer.flamTimer#170.STOP
w 282 811 100 0 n#184 flamTimer.flamTimer#160.TIMEOUT 192 912 256 912 256 800 368 800 efanouts.efanouts#181.SLNK
w 2402 587 -100 0 c#178 elongouts.elongouts#125.FLNK 2304 576 2560 576 outhier.FAILED.p
w 1194 811 100 0 n#176 flamTimer.flamTimer#170.TIMEOUT 1120 912 1184 912 1184 800 1264 800 efanouts.efanouts#155.SLNK
w 1466 1579 100 0 n#152 efanouts.efanouts#150.SLNK 1584 1568 1408 1568 efanouts.efanouts#91.LNK3
w -14 235 100 0 n#146 hwin.hwin#147.in -32 288 -32 224 64 224 elongouts.elongouts#117.DOL
w 1834 1339 100 0 n#138 efanouts.efanouts#91.LNK4 1408 1536 1600 1536 1600 1328 2128 1328 efanouts.efanouts#136.SLNK
w 1730 1803 100 0 n#153 efanouts.efanouts#91.LNK1 1408 1632 1504 1632 1504 1792 2016 1792 2016 1680 2128 1680 efanouts.efanouts#121.SLNK
w 1970 587 100 0 n#135 hwin.hwin#132.in 1952 672 1952 576 2048 576 elongouts.elongouts#125.DOL
w 1586 379 100 0 n#134 hwin.hwin#131.in 1568 416 1568 368 1664 368 estringouts.estringouts#126.DOL
w 1970 843 100 0 n#133 hwin.hwin#130.in 1952 928 1952 832 2048 832 elongouts.elongouts#124.DOL
w -14 203 100 0 n#119 estringouts.estringouts#116.FLNK -48 96 -32 96 -32 192 64 192 elongouts.elongouts#117.SLNK
w 1122 1499 100 0 n#107 ecalcouts.ecalcouts#218.VAL 1120 1488 1184 1488 1184 1632 1216 1632 efanouts.efanouts#91.SELL
w 738 1435 100 0 n#106 ebis.ebis#102.VAL 704 1456 736 1456 736 1424 800 1424 ecalcouts.ecalcouts#218.SDIS
w -136 1419 100 0 n#97 hwin.hwin#99.in -160 1472 -160 1408 -64 1408 ebos.ebos#104.DOL
w -136 1659 100 0 n#96 hwin.hwin#98.in -160 1712 -160 1648 -64 1648 ebos.ebos#103.DOL
w 376 1483 100 0 n#95 ebis.ebis#102.SLNK 448 1472 352 1472 junction
w 248 1595 100 0 n#95 ebos.ebos#103.OUT 192 1584 352 1584 352 1344 192 1344 ebos.ebos#104.OUT
w 736 1563 100 0 n#86 hwin.hwin#89.in 672 1712 720 1712 720 1552 800 1552 ecalcouts.ecalcouts#218.INPA
w 1120 1563 100 0 n#85 ecalcouts.ecalcouts#218.FLNK 1120 1552 1168 1552 efanouts.efanouts#91.SLNK
s 2624 2032 100 1792 2000/12/05
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2528 -240 100 1792 flamSubSysCommand.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/05
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 flam Sub-system Commadn
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
[cell use]
use ecalcouts 800 1367 100 0 ecalcouts#218
xform 0 960 1488
p 872 1400 100 0 -1 CALC:A+1
p 1632 2014 100 0 -1 DISV:0
p 888 1600 100 0 1 SCAN:Passive
p 864 1368 100 0 -1 name:$(top)$(cmd)$(sys)ActiveC
p 784 1560 75 0 -1 palrm(INPA):NMS
p 752 1560 75 0 -1 pproc(INPA):NPP
use inhier -400 1616 100 0 START
xform 0 -304 1616
use ebis 448 1399 100 0 ebis#102
xform 0 576 1472
p 560 1392 100 1024 1 name:$(top)$(cmd)$(sys)Active
use hwin 480 1671 100 0 hwin#89
xform 0 576 1712
p 432 1744 100 0 -1 val(in):$(top)$(sys):applyC.VAL CPP
use hwin -352 1671 100 0 hwin#98
xform 0 -256 1712
p -349 1704 100 0 -1 val(in):1
use hwin -352 1431 100 0 hwin#99
xform 0 -256 1472
p -349 1464 100 0 -1 val(in):0
use hwin 1760 887 100 0 hwin#130
xform 0 1856 928
p 1760 960 100 0 -1 val(in):$(CAR_IDLE)
use hwin 1376 375 100 0 hwin#131
xform 0 1472 416
p 1376 448 100 0 -1 val(in):$(top)$(sys):applyC.OMSS
use hwin 1760 631 100 0 hwin#132
xform 0 1856 672
p 1760 704 100 0 -1 val(in):$(CAR_ERROR)
use hwin -224 247 100 0 hwin#147
xform 0 -128 288
p -368 288 100 0 -1 val(in):$(CAR_BUSY)
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use efanouts 1584 1431 100 0 efanouts#150
xform 0 1704 1584
p 1792 1424 100 1024 1 name:$(top)$(cmd)$(sys)BusyF
use efanouts 2128 1191 100 0 efanouts#136
xform 0 2248 1344
p 2256 1184 100 1024 1 name:$(top)$(cmd)$(sys)ErrF
use efanouts 1168 1415 100 0 efanouts#91
xform 0 1288 1568
p 1216 1376 100 0 1 SELM:Specified
p 1392 1408 100 1024 1 name:$(top)$(cmd)$(sys)ActiveF
use efanouts 2128 1543 100 0 efanouts#121
xform 0 2248 1696
p 2272 1536 100 1024 1 name:$(top)$(cmd)$(sys)IdleF
use efanouts 1264 663 100 0 efanouts#155
xform 0 1384 816
p 1392 640 100 1024 1 name:$(top)$(cmd)$(sys)timeoutF
use efanouts 368 663 100 0 efanouts#181
xform 0 488 816
p 512 640 100 1024 1 name:$(top)$(cmd)$(sys)NoStartF
use outhier 2592 832 100 0 DONE
xform 0 2544 832
use outhier 2576 160 100 0 CAR_IVAL
xform 0 2528 160
use outhier 2576 64 100 0 CAR_IMSS
xform 0 2528 64
use outhier 2592 576 100 0 FAILED
xform 0 2544 576
use flamTimer -64 743 100 0 flamTimer#160
xform 0 64 864
p -64 704 100 0 1 setTimeout:timeout $(START_TIMEOUT)
p -64 736 100 0 1 setTimer:timer $(top)$(cmd)$(sys)S
use flamTimer 864 743 100 0 flamTimer#170
xform 0 992 864
p 864 704 100 0 1 setTimeout:timeout $(CMD_TIMEOUT)
p 864 736 100 0 1 setTimer:timer $(top)$(cmd)$(sys)R
use estringouts -304 7 100 0 estringouts#116
xform 0 -176 80
p -240 -32 100 0 1 VAL:
p -160 0 100 1024 1 name:$(top)$(cmd)$(sys)Erase
use estringouts 1664 263 100 0 estringouts#126
xform 0 1792 336
p 1728 224 100 0 1 OMSL:closed_loop
p 1776 256 100 1024 1 name:$(top)$(cmd)$(sys)ErrMessage
use estringouts 480 263 100 0 estringouts#167
xform 0 608 336
p 384 192 100 0 1 VAL:$(top)$(cmd)$(sys) command did not respond
p 592 256 100 1024 1 name:$(top)$(cmd)$(sys)NoStartMess
use estringouts 960 263 100 0 estringouts#168
xform 0 1088 336
p 944 224 100 0 1 VAL:$(sys) command timed out
p 1056 256 100 1024 1 name:$(top)$(cmd)$(sys)TimeoutMess
use elongouts 64 103 100 0 elongouts#117
xform 0 192 192
p 208 96 100 1024 1 name:$(top)$(cmd)$(sys)Busy
p 320 160 75 768 -1 pproc(OUT):PP
use elongouts 2048 711 100 0 elongouts#124
xform 0 2176 800
p 2160 704 100 1024 1 name:$(top)$(cmd)$(sys)Idle
p 2304 768 75 768 -1 pproc(OUT):PP
use elongouts 2048 455 100 0 elongouts#125
xform 0 2176 544
p 2176 448 100 1024 1 name:$(top)$(cmd)$(sys)Error
p 2304 512 75 768 -1 pproc(OUT):PP
use ebos -64 1527 100 0 ebos#103
xform 0 64 1616
p 48 1520 100 1024 1 name:$(top)$(cmd)$(sys)Start
p 192 1584 75 768 -1 pproc(OUT):PP
use ebos -64 1287 100 0 ebos#104
xform 0 64 1376
p 48 1280 100 1024 1 name:$(top)$(cmd)$(sys)Done
p 192 1344 75 768 -1 pproc(OUT):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSubSysCommand.sch,v 0.0 2006/06/21 15:20:21 hon Exp $
