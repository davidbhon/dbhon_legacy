# RCS: 
# $Name:  $ $Id: Makefile.flam,v 0.49 2006/10/13 21:12:43 hon Exp $
#
# Macros:
SHELL := /bin/tcsh -f

DOMAIN := $(shell domainname)
UNAME := $(shell uname | cut -d'_' -f1)
 
ifndef RCSMASTER
ifeq ($(UNAME),Linux)
# RCSMASTER := $(shell \ls -l | \grep RCS | \cut -d'>' -f2 | \cut -c2 | sed 's^/RCS*^^')
  RCSMASTER := $(shell \ls -l --time-style=locale | \grep RCS | awk '{print $$11}' | sed 's^/RCS*^^')
else
  RCSMASTER := $(shell \ls -l | \grep RCS | awk '{print $$11}' | sed 's^/RCS*^^')
endif
endif

foo := $(shell if( -e RCS ) co $(COFLAGS) .ufcshrc .makerules .maketargets .gccv whoco myco)

ifndef RCSDEV
  RCSDEV := $(shell \pwd)
endif

include .makerules

# make agents (inet apps.) first
FLAMDIR := desktopicons flam.inet flam2.epics flam2.scripts javaUFProtocol javaUFLib flam2.jec flam2.fatboy flam2.vxvme flam2.seqexec flam2.oiwfs flam2.jdd

STDLIBA := $(shell \find $(GCCLIB) -follow -type f -name "*std*.a" |tail -1)
STDLIBSO := $(shell \find $(GCCLIB) -follow -type f -name "*std*.so" |tail -1)


RCSLIST := ReadMe Makefile .makerules .maketargets .gccv .ufcshrc ufci ufco whoco myco

UFLIBDIR := libUF++ libUFProtocol++ libUFSerial++ libUFGem++ libUF libUFGem \
	    javaUFProtocol javaUFLib
 
#UFLIBDIR += libUFIDL

UFAPPDIR := unitTests scripts java perl

WEBDIR := www dhtml dhtml/cgi-bin dhtml/trecs \
	  dhtml/flam1.hdwrctrl dhtml/flam1.obsvctrl \
	  dhtml/flam1.obsvctrl/kp2m dhtml/flam1.obsvctrl/kp4m \
	  dhtml/flam1.obsvctrl/mmt dhtml/flam1.obsvctrl/gems 

#OTHRDIR := imglib kittpeak etc doc
#OTHRDIR := imglib etc kittpeak
OTHRDIR := scripts imglib etc
#OTHRDIR := scripts etc

FLAMDOC := flam2.doc flam2.doc/Electronics flam2.doc/Electronics/Printouts flam2.doc/ICD flam2.doc/ICD/Alt_Headers flam2.doc/ICD/Printouts flam2.doc/ICD/Old_Epics_db flam2.doc/ICD/Old_FPRD flam2.doc/ICD/Old_ICS1.9_3.1 flam2.doc/ICD/Old_OCDD flam2.doc/Optics flam2.doc/Optics/FilterScans flam2.doc/Figures/Tiffs flam2.doc/Figures/Jeffs_Tiffs/Cropped flam2.doc/Figures/Jeffs_Tiffs/Originals flam2.doc/Figures/Jeffs_Tiffs/Smaller flam2.doc/Figures/Johns_Images flam2.doc/Figures/Steve flam2.doc/ATdoc flam2.doc/ATdoc/Old_Versions flam2.doc/ATdoc/Printouts flam2.doc/JEI flam2.doc/JEI/Orig flam2.doc/JEI/Smaller flam2.doc/ManagementDocs flam2.doc/OIWFS flam2.doc/Software

# make instrument (flam) apps last:
DIR := $(OTHRDIR) $(UFLIBDIR) $(UFAPPDIR) $(FLAMDIR) 
MDIR := $(SGISTL) $(DIR)

UFRELEASE := UFF2PreAT0

ifeq ($(MAKEFILE_LIST), )
  FMAKE := $(MAKE)
else
  FMAKE := $(MAKE) -f $(MAKEFILE_LIST)
endif

# Targets:
all: show
	@true

flam:
	@echo make flamingos system

trecs:
	@echo make trecs system

instrum:
	@echo make geneic instrument system

show:
	-@echo JDATE= $(JDATE)
	-@echo RCSMASTER= $(RCSMASTER)
	-@echo JAR= $(JAR)
	-@echo LIB= $(LIB)
	-@echo EXE= $(EXE)
	-@echo STARGET= $(STARGET)
	-@echo TARGET= $(TARGET)
	-@echo JTARGET= $(JTARGET)
	-@echo COMPILER_ID= $(COMPILER_ID)
	-@echo OS= $(OS)
	-@echo OS_VERSION= $(OS_VERSION)
	-@echo VX_TARGET= $(VX_TARGET)
	-@echo RCSLIST = $(RCSLIST)
	-@echo PUBHDR = $(PUBHDR)
	-@echo PUBLIB = $(PUBLIB)
	-@echo PUBSLIB = $(PUBSLIB)
	-@echo PUBEXE = $(PUBEXE) 
	-@echo PUBBIN = $(PUBBIN)
	-@echo PUBSBIN = $(PUBSBIN)
	-@echo UFINSTALL = $(UFINSTALL)
	-@echo INSTALL_HDR = $(INSTALL_HDR)
	-@echo RELEASE = $(RELEASE)
	-@echo UFRELEASE = $(UFRELEASE)
	-@echo DIR = $(DIR)
	-@echo STL = $(STL)
	-@echo CXXFLAGS = $(CXXFLAGS)
	-@echo GCCIDM = $(GCCIDM)
	-@echo GCCLIB = $(GCCLIB)
	-@echo STDLIBA = $(STDLIBA)
	-@echo STDLIBSO = $(STDLIBSO)
	-@echo DOMAIN = $(DOMAIN)
	-@echo MAKEFILE_LIST = $(MAKEFILE_LIST)
	-@echo MAKEFILELS = $(MAKEFILES)
	-@echo FMAKE = $(FMAKE)

# force source of .ufcshrc before make
init: initco pubclean
	source .ufcshrc; $(FMAKE) -i -k .init

webco:
	$(foreach i, $(WEBDIR), \
	if ( ! -e $i ) mkdir -p $i; \
	if ( ! -e $i/RCS && -e $(RCSMASTER) ) ln -s $(RCSMASTER)/$i/RCS $i/RCS;)
	$(foreach i, $(WEBDIR), pushd $i; co -q $(COFLAGS) RCS/*,v; popd;)

.init:
	$(foreach i, $(DIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET);)
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k init; popd;)
	cd flam.inet; $(MAKE) -i -k initco
	cd imglib; make init
	cd unitTests; make .init
	cd flam.inet/portescap; make -ik .links

docco:
	$(foreach i, $(FLAMDOC), mkdir -p $i; pushd $i; \
	if ( ! -e $i/RCS && -e $(RCSMASTER) ) ln -s $(RCSMASTER)/$i/RCS; \
        co $(COFLAGS) RCS/*,v; popd;)

initco: 
	if( -e RCS ) co -q $(COFLAGS) RCS/*,v
	$(foreach i, $(DIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(TARGET)/ ) mkdir -p $i/$(TARGET); \
	if ( ! -e $i/$(TARGET)/.depend ) touch $i/$(TARGET)/.depend; \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET); \
	if ( ! -e $i/RCS && -e $(RCSMASTER) ) ln -s $(RCSMASTER)/$i/RCS $i/RCS;)
	$(foreach i, $(DIR), pushd $i; co -q $(COFLAGS) RCS/*,v; popd;)
	$(foreach i, $(DIR), pushd $i; $(MAKE) -ik initco; popd;)
#	cd flam.inet; $(MAKE) -ik initco
	cd flam2.vxvme; $(MAKE) -ik -f Makefile.uf initco

relco: 
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/*,v
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/.[A-z]*,v
	$(foreach i, $(DIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET); \
	if ( ! -e $i/RCS && -e $(RCSMASTER) ) ln -s $(RCSMASTER)/$i/RCS $i/RCS;)
	$(foreach i, $(DIR), pushd $i; if( -e RCS ) co -q -r$(UFRELEASE) RCS/*,v; \
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/.[A-z]*,v; popd;)
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k init; popd;)

ifeq ($(RCSMASTER),.)
sgistl: initpub
	if ( ! -e $(INSTALL_HDR)/sgistl/ ) mkdir -p $(INSTALL_HDR)/sgistl
	pushd $(SGISTL); \
	cp ./[a-z]* $(INSTALL_HDR)/sgistl; \
	chmod -R 775 $(INSTALL_HDR)/sgistl; \
	popd
else
sgistl: initpub
	if ( ! -e $(SGISTL) ) mkdir -p $(SGISTL);
	if ( ! -e $(SGISTL)/RCS ) csh -c "pushd $(SGISTL); ln -s $(RCSMASTER)/$(SGISTL)/RCS; co $(COFLAGS) RCS/[a-z]*,v"
	if ( ! -e $(INSTALL_HDR)/sgistl/ ) csh -c "mkdir -p $(INSTALL_HDR)/sgistl; cp $(SGISTL)/[a-z]* $(INSTALL_HDR)/sgistl/; chmod -R 775 $(INSTALL_HDR)/sgistl/"
endif

initpub: .ufcshrc .makerules .maketargets
	if ( ! -e $(INSTALL_SCRP)/ ) csh -c "mkdir -p $(INSTALL_SCRP) ; chmod -R 775 $(INSTALL_SCRP)/"
	if ( ! -e $(INSTALL_PERL)/ ) csh -c "mkdir -p $(INSTALL_PERL) ; chmod -R 775 $(INSTALL_PERL)/"
	if ( ! -e $(INSTALL_JAR)/ ) csh -c "mkdir -p $(INSTALL_JAR) ; chmod -R 775 $(INSTALL_JAR)/"
	if ( ! -e $(INSTALL_LIB)/ ) csh -c "mkdir -p $(INSTALL_LIB) ; chmod -R 775 $(INSTALL_LIB)/"
	if ( ! -e $(INSTALL_SLIB)/ ) csh -c "mkdir -p $(INSTALL_SLIB) ; chmod -R 775 $(INSTALL_SLIB)/"
	if ( ! -e $(INSTALL_BIN)/ ) csh -c "mkdir -p $(INSTALL_BIN) ; chmod -R 775 $(INSTALL_BIN)/"
	if ( ! -e $(INSTALL_SBIN)/ ) csh -c "mkdir -p $(INSTALL_SBIN) ; chmod -R 775 $(INSTALL_SBIN)/"
	if ( ! -e $(INSTALL_HDR)/ ) csh -c "mkdir -p $(INSTALL_HDR) ; chmod -R 775 $(INSTALL_HDR)/"
	if ( ! -e $(INSTALL_ETC)/ ) csh -c "mkdir -p $(INSTALL_ETC) ; chmod -R 775 $(INSTALL_ETC)/"
	if ( ! -e $(INSTALL_LIB)/libstdc++.so ) csh -c "cd $(INSTALL_LIB); ln -s $(STDLIBSO) libstdc++.so"
	if ( ! -e $(INSTALL_SLIB)/libstdc++.a ) csh -c "cd $(INSTALL_SLIB); ln -s $(STDLIBA) libstdc++.a"
	cp -f -p .ufcshrc $(UFINSTALL)/
#	cd annexApp; make .init

cleanpub:
	if( -e $(UFINSTALL)/ ) $(RM) -r $(UFINSTALL)/
	mkdir -p $(UFINSTALL)

ifeq ($(STL),sgi)
pubclean: cleanpub sgistl
	@echo cleaned and re-initialized $(UFINSTALL) using $(STL)
else
pubclean: cleanpub initpub 
	@echo cleaned and re-initialized $(UFINSTALL) using $(STL)
endif

sync:
	-@echo sync: full checkin and checkout ...
	ci -q RCS/*,v >& /dev/null ; co $(COFLAGS) RCS/*,v
	ci -q RCS/.[A-z]*,v >& /dev/null ; co $(COFLAGS) RCS/.[A-z]*,v
	$(foreach i, $(DIR), \
	pushd $i; if( -e RCS ) co $(COFLAGS) RCS/*,v; if( -e RCS ) co $(COFLAGS) RCS/.[A-z]*,v; $(MAKE) -i -k sync; popd;)
	-@echo sync: full checkin and checkout completed

syncinit:
	-@echo sync: full checkout ...
	co $(COFLAGS) RCS/*,v
	co $(COFLAGS) RCS/.[A-z]*,v
	$(foreach i, $(DIR), \
	pushd $i; if( -e RCS ) co $(COFLAGS) RCS/*,v; if( -e RCS ) co $(COFLAGS) RCS/.[A-z]*,v; $(MAKE) -i -k init; popd;)
	-@echo sync: full checkout completed

syncdiff:
	-@echo sync: full checkout ...
	$(foreach i, $shell(\ls RCS/*,v), rcsdiff $i >& /dev/null; if( $$? != 0 ) co $(COFLAGS) $i;)
	$(foreach i, $shell(\ls RCS/.[A-z]*,v), rcsdiff $i >& /dev/null; if( $$? != 0 ) co $(COFLAGS) $i;)
	$(foreach i, $(DIR), \
	pushd $i; $(MAKE) -i -k syncdiff; popd;)
	-@echo sync: full checkout completed

# only public headers, libraries, and executables, meant for public consumption
# should be "installed", nominally into /usr/local/...
# force source of .ufcshrc
install:
	source .ufcshrc; $(FMAKE) -i -k .install

.install: pubheader publib pubexe pubjar
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k install; popd;)
	cd flam2.epics; $(MAKE) -i -k .install

realclean: .init
	source .ufcshrc; $(FMAKE) -i -k .clean

clean:
	source .ufcshrc; $(FMAKE) -i -k .clean

.clean:
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k clean; popd;)

pubheader:
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k $@; popd;)

publib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k $@; popd;)

pubexe: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(DIR), pushd $i; $(MAKE) -i -k $@; popd;)

pubjar:
	@if( ! -e $(INSTALL_JAR)/ ) mkdir -p $(INSTALL_JAR)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

stripall: pubheader strippublib strippubexe
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

strippublib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(DIR), pushd $i; $(MAKE)  $@; popd;)

strippubexe: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

strip: pubheader stripslib stripsbin
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

stripslib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(DIR), pushd $i; $(MAKE)  $@; popd;)

stripsbin: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

jar:
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

shlib: pubheader
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

exe: shlib publib
	$(foreach i, $(DIR), pushd $i; $(MAKE) $@; popd;)

rcsclean:
	$(foreach i, $(DIR), pushd $i; rcsclean; popd;)
#	rcsclean

rcsinit:
	if( -o RCS ) $(RM) ./RCS/* ./RCS/.[A-z]*
	ci -l0.0 -N"Init" -s"Dev." -m"initial checkin" -t-"initial checkin" $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; rcsclean; popd;)

rcscheckin:
	ci -l -s"Developmental" -m"inremental checkin" $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcstest:
	ci -u -sTestable -m"current & testable" $(RCSLIST)
	rcs -N"Test": $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcsinstall:
	ci -u -sInstalled -m"current & installed" $(RCSLIST)
	rcs -N"Install": $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcsrelease: rcscheckin
	ci -u -sRelease -m"tested & released" $(RCSLIST)
	rcs -N"$(UFRELEASE)": $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

releaseco: rcsrelease
	rcsclean; co -q -r$(UFRELEASE) $(RCSLIST)
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

release: releaseco
	$(foreach i, $(DIR), pushd $i; $(MAKE $(MAKEFLAGS)) $@; popd;)
	@echo completeded installation of release $(UFRELEASE)

depend: pubheader
	$(foreach i, $(DIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

devrcs:
	$(foreach i, $(shell \find . -name RCS), $(RM) $i; ln -s $(RCSMASTER)/$i $i;)
