// portable cas -- already included from UFPVAttr.h
#include "UFPVAttr.h"

/// Brief (ONE LINE) description of class: TBD
/** The Process Variable class, Flam2PV. Provides the read() and write()
 * functions. The read() function calls the Flam2Server::read() function
 * which calls the appropriate function or functions from its function
 * table. The actual functions to be called are members of 
 * the Flam2PV class, readStatus() to readUnits().
 */ 
class UFPV : public casPV {
 public:
  /// Brief (ONE LINE) description of method: TBD
  /**
   * A constructor -- more detailed description.
   * @param cas a caServer object
   * @param attr a UFPVAttr object
   * @see ~UFPV()
   * @return Nothing - this is a constructor
   */
  UFPV(const caServer& cas, const UFPVAttr& attr);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   */ 
  inline ~UFPV() {}
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return const char* TBD
   */ 
  inline virtual const char* getName() const { return _attr.getName(); }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return caStatus TBD
   */ 
  inline virtual caStatus interestRegister() { _interest = aitTrue; return S_casApp_success; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   */ 
  inline virtual void interestDelete() { _interest = aitFalse; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return caStatus TBD
   */ 
  virtual caStatus beginTransaction();
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   */ 
  virtual void endTransaction();
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param ctx TBD
   * @param prototype TBD
   * @return caStatus TBD
   */ 
  virtual caStatus read(const casCtx& ctx, gdd& prototype);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param ctx TBD
   * @param val TBD
   * @return caStatus TBD
   */ 
  virtual caStatus write(const casCtx& ctx, const gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return aitEnum TBD
   */ 
  virtual aitEnum bestExternalType();
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readStatus(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readSeverity(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readPrecision(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readHighOperation(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readLowOperation(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readHighAlarm(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readHighWarn(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readLowWarn(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readLowAlarm(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readHighCtrl(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readLowCtrl(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readVal(gdd& val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param val TBD
   * @return gddAppFuncTableStatus
   */ 
  gddAppFuncTableStatus readUnits(gdd& val);
 private:
  //The LT char in doxygen comments indicates that the comment comes AFTER the code, instead of 
  //before the code
  static int _currentOps; /**< TBD static int*/
  const UFPVAttr& _attr;  /**< TBD UFPVAttr object
			   * @see UFPVAttr
			   */
  aitBool _interest; ///< TBD aitBool
                     ///
};
