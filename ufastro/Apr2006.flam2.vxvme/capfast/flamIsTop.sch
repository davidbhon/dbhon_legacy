[schematic2]
uniq 92
[tools]
[detail]
w 1928 299 100 0 n#71 elongouts.elongouts#81.OUT 1888 288 2016 288 hwout.hwout#86.outp
w 648 -53 100 0 n#70 estringouts.estringouts#72.OUT 608 -64 736 -64 hwout.hwout#83.outp
w 648 139 100 0 n#69 estringouts.estringouts#73.OUT 608 128 736 128 hwout.hwout#82.outp
w 648 331 100 0 n#68 estringouts.estringouts#74.OUT 608 320 736 320 hwout.hwout#79.outp
w 1928 75 100 0 n#67 eaos.eaos#75.OUT 1888 64 2016 64 hwout.hwout#87.outp
w 1064 1867 100 0 n#35 flamIsObserveCmds.flamIsObserveCmds#66.MESS 2368 1376 2496 1376 2496 1856 -320 1856 -320 1248 0 1248 eapply.eapply#23.INMC
w 1064 1835 100 0 n#34 flamIsObserveCmds.flamIsObserveCmds#66.VAL 2368 1408 2464 1408 2464 1824 -288 1824 -288 1280 0 1280 eapply.eapply#23.INPC
w 792 1803 100 0 n#33 flamIsSetupCmds.flamIsSetupCmds#65.MESS 1760 1376 1888 1376 1888 1792 -256 1792 -256 1312 0 1312 eapply.eapply#23.INMB
w 792 1771 100 0 n#32 flamIsSetupCmds.flamIsSetupCmds#65.VAL 1760 1408 1856 1408 1856 1760 -224 1760 -224 1344 0 1344 eapply.eapply#23.INPB
w 1304 651 100 0 n#31 eapply.eapply#23.OCLC 384 1248 640 1248 640 640 2016 640 2016 1376 2112 1376 flamIsObserveCmds.flamIsObserveCmds#66.CLID
w 1304 683 100 0 n#30 eapply.eapply#23.OUTC 384 1280 672 1280 672 672 1984 672 1984 1408 2112 1408 flamIsObserveCmds.flamIsObserveCmds#66.DIR
w 1032 715 100 0 n#29 eapply.eapply#23.OCLB 384 1312 704 1312 704 704 1408 704 1408 1376 1504 1376 flamIsSetupCmds.flamIsSetupCmds#65.CLID
w 1032 747 100 0 n#28 eapply.eapply#23.OUTB 384 1344 736 1344 736 736 1376 736 1376 1408 1504 1408 flamIsSetupCmds.flamIsSetupCmds#65.DIR
w 520 1739 100 0 n#27 flamIsSystemCmds.flamIsSystemCmds#64.MESS 1152 1376 1280 1376 1280 1728 -192 1728 -192 1376 0 1376 eapply.eapply#23.INMA
w 520 1707 100 0 n#26 flamIsSystemCmds.flamIsSystemCmds#64.VAL 1152 1408 1248 1408 1248 1696 -160 1696 -160 1408 0 1408 eapply.eapply#23.INPA
w 616 1387 100 0 n#25 eapply.eapply#23.OCLA 384 1376 896 1376 flamIsSystemCmds.flamIsSystemCmds#64.CLID
w 616 1419 100 0 n#24 eapply.eapply#23.OUTA 384 1408 896 1408 flamIsSystemCmds.flamIsSystemCmds#64.DIR
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos IS Top
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/08
s 2096 -272 100 1792 Author: RRO
s 2512 -240 100 1792 flamIsTop.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 RRO
s 2624 2064 100 1792 2000/12/08
[cell use]
use changeBar 1984 2023 100 0 changeBar#91
xform 0 2336 2064
use hwout 736 279 100 0 hwout#79
xform 0 832 320
p 960 320 100 0 -1 val(outp):$(sad)loRes10Blocker.VAL PP NMS
use hwout 736 87 100 0 hwout#82
xform 0 832 128
p 960 128 100 0 -1 val(outp):$(sad)loRes20Blocker.VAL PP NMS
use hwout 736 -105 100 0 hwout#83
xform 0 832 -64
p 960 -64 100 0 -1 val(outp):$(sad)hiRes10Blocker.VAL PP NMS
use hwout 2016 247 100 0 hwout#86
xform 0 2112 288
p 2240 288 100 0 -1 val(outp):$(sad)reconfigTimeout.VAL PP NMS
use hwout 2016 23 100 0 hwout#87
xform 0 2112 64
p 2240 64 100 0 -1 val(outp):$(sad)kBrRhLimit.VAL PP NMS
use elongouts 1632 231 100 0 elongouts#81
xform 0 1760 320
p 1696 224 100 0 1 name:$(top)reconfigTimeout
p 1888 288 75 768 -1 pproc(OUT):PP
use eaos 1632 7 100 0 eaos#75
xform 0 1760 96
p 1376 -50 100 0 0 PREC:3
p 1696 0 100 0 1 name:$(top)kBrRhLimit
p 1888 64 75 768 -1 pproc(OUT):PP
use estringouts 352 263 100 0 estringouts#74
xform 0 480 336
p 416 256 100 0 1 name:$(top)loRes10Blocker
p 608 320 75 768 -1 pproc(OUT):PP
use estringouts 352 71 100 0 estringouts#73
xform 0 480 144
p 416 64 100 0 1 name:$(top)loRes20Blocker
p 608 128 75 768 -1 pproc(OUT):PP
use estringouts 352 -121 100 0 estringouts#72
xform 0 480 -48
p 416 -128 100 0 1 name:$(top)hiRes10Blocker
p 608 -64 75 768 -1 pproc(OUT):PP
use flamIsObserveCmds 2112 807 100 0 flamIsObserveCmds#66
xform 0 2240 1168
use flamIsSetupCmds 1504 807 100 0 flamIsSetupCmds#65
xform 0 1632 1168
use flamIsSystemCmds 896 807 100 0 flamIsSystemCmds#64
xform 0 1024 1168
use eapply 0 871 100 0 eapply#23
xform 0 192 1232
p 176 848 100 1024 1 name:$(top)is:apply
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamIsTop.sch,v 0.1 2004/10/11 21:04:10 rojas Exp $
