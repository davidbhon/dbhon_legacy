[schematic2]
uniq 2
[tools]
[detail]
s 2624 2032 100 1792 YYYY/MM/DD
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 Filename
s 2096 -272 100 1792 Author: RRO
s 2096 -240 100 1792 YYYY/MM/DD
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Drawing Title
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: BorderC.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
