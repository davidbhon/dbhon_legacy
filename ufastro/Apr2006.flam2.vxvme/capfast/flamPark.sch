[schematic2]
uniq 97
[tools]
[detail]
w 56 1131 100 0 n#96 hwin.hwin#95.in 32 1120 128 1120 ecad2.ecad2#48.INPB
w 144 1547 -100 0 c#89 ecad2.ecad2#48.DIR 128 1408 64 1408 64 1536 272 1536 272 1856 160 1856 inhier.DIR.P
w 112 1579 -100 0 c#90 ecad2.ecad2#48.ICID 128 1376 32 1376 32 1568 240 1568 240 1728 160 1728 inhier.ICID.P
w 384 1547 -100 0 c#91 ecad2.ecad2#48.VAL 448 1408 512 1408 512 1536 304 1536 304 1856 448 1856 outhier.VAL.p
w 416 1579 -100 0 c#92 ecad2.ecad2#48.MESS 448 1376 544 1376 544 1568 336 1568 336 1728 448 1728 outhier.MESS.p
w 1564 427 100 0 n#83 flamSubSysCommand.flamSubSysCommand#57.CAR_IVAL 1472 320 1568 320 1568 544 1728 544 flamSubSysCombine.flamSubSysCombine#77.DC_VAL
w 1628 339 100 0 n#82 flamSubSysCommand.flamSubSysCommand#57.CAR_IMSS 1472 256 1632 256 1632 432 1728 432 1728 480 flamSubSysCombine.flamSubSysCombine#77.DC_MESS
w 1624 611 100 0 n#81 flamSubSysCommand.flamSubSysCommand#56.CAR_IMSS 1472 704 1568 704 1568 608 1728 608 flamSubSysCombine.flamSubSysCombine#77.CC_MESS
w 1528 771 100 0 n#80 flamSubSysCommand.flamSubSysCommand#56.CAR_IVAL 1472 768 1632 768 1632 672 1728 672 flamSubSysCombine.flamSubSysCombine#77.CC_VAL
w 2024 555 100 0 n#93 flamSubSysCombine.flamSubSysCombine#77.CAR_IMSS 1984 544 2112 544 ecars.ecars#53.IMSS
w 2024 619 100 0 n#78 flamSubSysCombine.flamSubSysCombine#77.CAR_IVAL 1984 608 2112 608 ecars.ecars#53.IVAL
w 920 923 100 0 n#76 efanouts.efanouts#13.LNK4 864 912 1024 912 1024 224 1184 224 flamSubSysCommand.flamSubSysCommand#57.START
w 952 987 100 0 n#71 efanouts.efanouts#13.LNK2 864 976 1088 976 1088 1232 1312 1232 estringouts.estringouts#68.SLNK
w 1560 1227 100 0 n#59 estringouts.estringouts#68.OUT 1568 1216 1600 1216 hwout.hwout#60.outp
w 952 955 100 0 n#52 efanouts.efanouts#13.LNK3 864 944 1088 944 1088 672 1184 672 flamSubSysCommand.flamSubSysCommand#56.START
w 1144 1451 100 0 n#94 efanouts.efanouts#13.LNK1 864 1008 1024 1008 1024 1440 1312 1440 elongouts.elongouts#14.SLNK
w 512 939 100 0 n#49 ecad2.ecad2#48.STLK 448 928 624 928 efanouts.efanouts#13.SLNK
w 1560 1419 100 0 n#19 elongouts.elongouts#14.OUT 1568 1408 1600 1408 hwout.hwout#18.outp
w 1240 1483 100 0 n#17 hwin.hwin#16.in 1216 1536 1216 1472 1312 1472 elongouts.elongouts#14.DOL
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 FLAMINGOS park Command
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/05
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamPark.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/05
[cell use]
use hwin 1024 1495 100 0 hwin#16
xform 0 1120 1536
p 864 1536 100 0 -1 val(in):$(CAD_MARK)
use hwin -160 1079 100 0 hwin#95
xform 0 -64 1120
p -320 1120 100 0 -1 val(in):$(top)state
use outhier 464 1728 100 0 MESS
xform 0 432 1728
use outhier 464 1856 100 0 VAL
xform 0 432 1856
use inhier 80 1728 100 0 ICID
xform 0 160 1728
use inhier 96 1856 100 0 DIR
xform 0 160 1856
use flamSubSysCombine 1728 391 100 0 flamSubSysCombine#77
xform 0 1856 576
p 1728 256 100 0 1 setCmd:cmd park
use estringouts 1312 1159 100 0 estringouts#68
xform 0 1440 1232
p 1392 1120 100 0 0 OMSL:supervisory
p 1376 1120 100 0 1 VAL:PARK
p 1504 1152 100 1024 1 name:$(top)parkDcCmd
use hwout 1600 1175 100 0 hwout#60
xform 0 1696 1216
p 1824 1216 100 0 -1 val(outp):$(top)dc:acqControl.A
use hwout 1600 1367 100 0 hwout#18
xform 0 1696 1408
p 1824 1408 100 0 -1 val(outp):$(top)cc:park.DIR PP NMS
use elongouts 1312 1351 100 0 elongouts#14
xform 0 1440 1440
p 1376 1312 100 0 0 OMSL:supervisory
p 1504 1344 100 1024 1 name:$(top)parkCcMark
p 1568 1408 75 768 -1 pproc(OUT):PP
use flamSubSysCommand 1184 39 100 0 flamSubSysCommand#57
xform 0 1328 224
p 1184 0 100 0 1 setCommand:cmd park
p 1184 32 100 0 1 setSystem:sys dc
use flamSubSysCommand 1184 487 100 0 flamSubSysCommand#56
xform 0 1328 672
p 1184 448 100 0 1 setCommand:cmd park
p 1184 480 100 0 1 setSystem:sys cc
use ecars 2112 327 100 0 ecars#53
xform 0 2272 496
p 2272 320 100 1024 1 name:$(top)parkC
use ecad2 128 839 100 0 ecad2#48
xform 0 288 1152
p 192 800 100 0 1 INAM:flamIsNullInit
p 192 752 100 0 1 SNAM:flamIsParkProcess
p 288 832 100 1024 1 name:$(top)park
p 464 928 75 1024 -1 pproc(STLK):PP
use efanouts 624 791 100 0 efanouts#13
xform 0 744 944
p 768 784 100 1024 1 name:$(top)parkFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamPark.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
