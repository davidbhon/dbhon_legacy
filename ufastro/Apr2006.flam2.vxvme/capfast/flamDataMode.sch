[schematic2]
uniq 110
[tools]
[detail]
w 1154 1803 100 0 n#108 elongouts.elongouts#104.DOL 1248 1792 1120 1792 1120 1840 hwin.hwin#109.in
w 1586 1739 100 0 n#106 elongouts.elongouts#104.OUT 1504 1728 1728 1728 hwout.hwout#107.outp
w 1154 1771 100 0 n#105 estringouts.estringouts#68.FLNK 1056 1520 1120 1520 1120 1760 1248 1760 elongouts.elongouts#104.SLNK
w 1544 1067 100 0 n#78 eseqs.eseqs#98.LNK2 1504 1056 1632 1056 1632 1088 junction
w 1672 1099 100 0 n#78 eseqs.eseqs#98.LNK1 1504 1088 1888 1088 ecars.ecars#53.IVAL
w 1090 1099 100 0 n#103 eseqs.eseqs#98.DOL1 1184 1088 1056 1088 1056 1136 hwin.hwin#100.in
w 1090 1067 100 0 n#102 eseqs.eseqs#98.DOL2 1184 1056 1056 1056 1056 1008 hwin.hwin#101.in
w 696 779 100 0 n#99 ecad2.ecad2#48.STLK 256 768 1184 768 eseqs.eseqs#98.SLNK
w -48 1387 -100 0 c#89 ecad2.ecad2#48.DIR -64 1248 -128 1248 -128 1376 80 1376 80 1696 -32 1696 inhier.DIR.P
w -80 1419 -100 0 c#90 ecad2.ecad2#48.ICID -64 1216 -160 1216 -160 1408 48 1408 48 1568 -32 1568 inhier.ICID.P
w 192 1387 -100 0 c#91 ecad2.ecad2#48.VAL 256 1248 320 1248 320 1376 112 1376 112 1696 256 1696 outhier.VAL.p
w 224 1419 -100 0 c#92 ecad2.ecad2#48.MESS 256 1216 352 1216 352 1408 144 1408 144 1568 256 1568 outhier.MESS.p
w 632 1547 100 0 n#73 ecad2.ecad2#48.VALA 256 1056 512 1056 512 1536 800 1536 estringouts.estringouts#68.DOL
w 392 811 100 0 n#71 ecad2.ecad2#48.PLNK 256 800 576 800 576 1504 800 1504 estringouts.estringouts#68.SLNK
w 1160 1499 100 0 n#59 estringouts.estringouts#68.OUT 1056 1488 1312 1488 hwout.hwout#60.outp
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 FLAMINGOS dataMode Command
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/05
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamDataMode.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/05
[cell use]
use hwin 928 1799 100 0 hwin#109
xform 0 1024 1840
p 768 1840 100 0 -1 val(in):$(CAD_CLEAR)
use hwin 864 1095 100 0 hwin#100
xform 0 960 1136
p 704 1136 100 0 -1 val(in):$(CAR_BUSY)
use hwin 864 967 100 0 hwin#101
xform 0 960 1008
p 704 1008 100 0 -1 val(in):$(CAR_IDLE)
use hwout 1728 1687 100 0 hwout#107
xform 0 1824 1728
p 1952 1728 100 0 -1 val(outp):$(top)dc:acqControl.DIR PP NMS
use hwout 1312 1447 100 0 hwout#60
xform 0 1408 1488
p 1536 1488 100 0 -1 val(outp):$(top)dc:acqControl.C
use elongouts 1248 1671 100 0 elongouts#104
xform 0 1376 1760
p 1360 1664 100 1024 1 name:$(top)dataModeCadClear
p 1504 1728 75 768 -1 pproc(OUT):PP
use eseqs 1184 679 100 0 eseqs#98
xform 0 1344 928
p 1248 640 100 0 1 DLY1:0.0e+00
p 1248 608 100 0 1 DLY2:0.5e+00
p 1392 672 100 1024 1 name:$(top)dataModeBusy
p 1520 1088 75 1024 -1 pproc(LNK1):PP
p 1520 1056 75 1024 -1 pproc(LNK2):PP
use outhier 272 1568 100 0 MESS
xform 0 240 1568
use outhier 272 1696 100 0 VAL
xform 0 240 1696
use inhier -112 1568 100 0 ICID
xform 0 -32 1568
use inhier -96 1696 100 0 DIR
xform 0 -32 1696
use estringouts 800 1431 100 0 estringouts#68
xform 0 928 1504
p 880 1392 100 0 1 OMSL:closed_loop
p 992 1424 100 1024 1 name:$(top)dataModeDcWrite
use ecars 1888 807 100 0 ecars#53
xform 0 2048 976
p 2048 800 100 1024 1 name:$(top)dataModeC
use ecad2 -64 679 100 0 ecad2#48
xform 0 96 992
p 0 640 100 0 1 INAM:flamIsNullInit
p 0 592 100 0 1 SNAM:flamIsDataModeProcess
p 96 672 100 1024 1 name:$(top)dataMode
p 272 768 75 1024 -1 pproc(STLK):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamDataMode.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
