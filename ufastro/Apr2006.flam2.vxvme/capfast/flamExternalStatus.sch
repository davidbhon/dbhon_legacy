[schematic2]
uniq 228
[tools]
[detail]
w 626 1723 100 0 n#226 hwin.hwin#145.in 608 1712 704 1712 elongins.elongins#224.INP
w 626 1467 100 0 n#225 hwin.hwin#52.in 608 1456 704 1456 ebis.ebis#223.INP
s 2528 -240 100 1792 flamExternalStatus.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2002/07/14
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 External System Status
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s 2624 2064 100 1792 2002/01/23
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2624 2032 100 1792 2002/07/14
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Removed TCS monitors
s 2016 2032 100 1792 B
[cell use]
use changeBar 1984 2023 100 0 changeBar#141
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#227
xform 0 2336 2032
use elongins 704 1607 100 0 elongins#224
xform 0 832 1680
p 752 1568 100 0 1 SCAN:1 second
p 752 1600 100 0 1 name:$(top)tcsHeartbeat
use ebis 704 1351 100 0 ebis#223
xform 0 832 1424
p 736 1312 100 0 1 SCAN:1 second
p 736 1344 100 0 1 name:$(top)telescopeInPosition
use hwin 416 1671 100 0 hwin#145
xform 0 512 1712
p 176 1712 100 0 -1 val(in):tcs:sad:heartbeat
use hwin 416 1415 100 0 hwin#52
xform 0 512 1456
p 176 1456 100 0 -1 val(in):tcs:inPosition
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamExternalStatus.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
