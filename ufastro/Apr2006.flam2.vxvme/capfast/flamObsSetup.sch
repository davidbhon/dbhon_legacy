[schematic2]
uniq 279
[tools]
[detail]
w 1938 491 100 0 n#278 hwout.hwout#277.outp 2144 480 1792 480 egenSub.egenSub#130.OUTI
w 818 491 100 0 n#276 ecad20.ecad20#227.VALP 192 480 1504 480 egenSub.egenSub#130.INPI
w 898 331 100 0 n#273 ecad20.ecad20#227.PLNK 192 32 352 32 352 320 1504 320 egenSub.egenSub#130.SLNK
w 1938 811 100 0 n#271 egenSub.egenSub#130.OUTD 1792 800 2144 800 hwout.hwout#272.outp
w 1938 683 100 0 n#270 egenSub.egenSub#130.OUTF 1792 672 2144 672 hwout.hwout#269.outp
w 1938 747 100 0 n#267 egenSub.egenSub#130.OUTE 1792 736 2144 736 hwout.hwout#268.outp
w 818 747 100 0 n#266 ecad20.ecad20#227.VALL 192 736 1504 736 egenSub.egenSub#130.INPE
w 818 683 100 0 n#265 ecad20.ecad20#227.VALM 192 672 1504 672 egenSub.egenSub#130.INPF
w 818 811 100 0 n#264 ecad20.ecad20#227.VALK 192 800 1504 800 egenSub.egenSub#130.INPD
w 818 555 100 0 n#262 ecad20.ecad20#227.VALO 192 544 1504 544 egenSub.egenSub#130.INPH
w 818 619 100 0 n#261 ecad20.ecad20#227.VALN 192 608 1504 608 egenSub.egenSub#130.INPG
w 482 -245 100 0 n#260 ecad20.ecad20#227.VALR 192 352 288 352 288 -256 736 -256 736 -96 800 -96 flamIsEcUpdate.flamIsEcUpdate#194.ENABLE
w 1938 555 100 0 n#238 egenSub.egenSub#130.OUTH 1792 544 2144 544 hwout.hwout#240.outp
w 1938 619 100 0 n#237 egenSub.egenSub#130.OUTG 1792 608 2144 608 hwout.hwout#239.outp
w 818 875 100 0 n#249 ecad20.ecad20#227.VALJ 192 864 1504 864 egenSub.egenSub#130.INPC
w 994 939 100 0 n#252 ecad20.ecad20#227.VALB 192 1376 544 1376 544 928 1504 928 egenSub.egenSub#130.INPB
w 1026 1003 100 0 n#242 ecad20.ecad20#227.VALA 192 1440 608 1440 608 992 1504 992 egenSub.egenSub#130.INPA
w 274 11 100 0 n#255 ecad20.ecad20#227.STLK 192 0 416 0 flamSubSysCommand.flamSubSysCommand#57.START
w 1938 875 100 0 n#212 egenSub.egenSub#130.OUTC 1792 864 2144 864 hwout.hwout#213.outp
w 1090 -21 100 0 n#202 flamIsEcUpdate.flamIsEcUpdate#194.FLNK 1088 -32 1152 -32 flamSubSysCommand.flamSubSysCommand#197.START
w 1106 203 100 0 n#201 flamSubSysCommand.flamSubSysCommand#57.CAR_IMSS 704 32 768 32 768 192 1504 192 1504 0 junction
w 1490 11 100 0 n#201 flamSubSysCommand.flamSubSysCommand#197.CAR_IMSS 1440 0 1600 0 ecars.ecars#53.IMSS
w 1106 235 100 0 n#200 flamSubSysCommand.flamSubSysCommand#57.CAR_IVAL 704 96 736 96 736 224 1536 224 1536 64 junction
w 1490 75 100 0 n#200 flamSubSysCommand.flamSubSysCommand#197.CAR_IVAL 1440 64 1600 64 ecars.ecars#53.IVAL
w 722 -21 100 0 n#191 flamSubSysCommand.flamSubSysCommand#57.DONE 704 -32 800 -32 flamIsEcUpdate.flamIsEcUpdate#194.START
w 1938 939 100 0 n#129 egenSub.egenSub#130.OUTB 1792 928 2144 928 hwout.hwout#132.outp
w 1938 1003 100 0 n#128 egenSub.egenSub#130.OUTA 1792 992 2144 992 hwout.hwout#131.outp
w -112 1739 -100 0 c#89 ecad20.ecad20#227.DIR -128 1632 -192 1632 -192 1728 16 1728 16 1920 -96 1920 inhier.DIR.P
w -144 1771 -100 0 c#90 ecad20.ecad20#227.ICID -128 1600 -224 1600 -224 1760 -16 1760 -16 1856 -96 1856 inhier.ICID.P
w 136 1739 -100 0 c#91 ecad20.ecad20#227.VAL 192 1632 256 1632 256 1728 64 1728 64 1920 192 1920 outhier.VAL.p
w 168 1771 -100 0 c#92 ecad20.ecad20#227.MESS 192 1600 288 1600 288 1760 96 1760 96 1856 192 1856 outhier.MESS.p
s 1968 496 100 0 nod PA
s 1968 560 100 0 nod distance
s 1968 880 100 0 number of coadds
s 1968 816 100 0 number of read
s 1968 752 100 0 tel. dither mode
s 1968 688 100 0 nod settle time
s 1968 624 100 0 offset settle time
s 1968 944 100 0 number of ND reads
s 1968 1008 100 0 exposure time
s 1184 496 100 0 nod PA
s 1184 560 100 0 nod distance
s 1184 880 100 0 number of coadds
s 1184 816 100 0 number of read
s 1184 752 100 0 tel. dither mode
s 1184 688 100 0 nod settle time
s 1184 624 100 0 offset settle time
s 288 624 100 0 offset settle time
s 288 688 100 0 nod settle time
s 288 752 100 0 tel. dither mode
s 288 816 100 0 number of read
s 288 880 100 0 number of coadds
s 288 560 100 0 nod distance
s 288 496 100 0 nod PA
s 1184 944 100 0 number of ND reads
s 1184 1008 100 0 exposure time
s 288 1392 100 0 number of ND reads
s 304 1456 100 0 exposure time
s -416 528 100 0 auto temp control
s -416 976 100 0 offset settle time
s -416 1040 100 0 nod settle time
s -416 1104 100 0 tel. dither mode
s -416 1168 100 0 number of read
s -416 1232 100 0 number of coadds
s 2512 -240 100 1792 flamObsSetup.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2003/02/18
s 2320 -240 100 1792 Rev: H
s 2432 -192 100 256 Flamingos observationSetup Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s -416 1424 100 0 exposure time
s -416 1360 100 0 number of ND reads
s -416 912 100 0 nod distance
s 272 368 100 0 auto temp control
s -416 848 100 0 nod PA
[cell use]
use hwout 2144 439 100 0 hwout#277
xform 0 2240 480
p 2368 480 100 0 -1 val(outp):S(top)dc:obsControl.J
use hwout 2144 759 100 0 hwout#272
xform 0 2240 800
p 2368 800 100 0 -1 val(outp):$(top)dc:obsControl.E
use hwout 2144 631 100 0 hwout#269
xform 0 2240 672
p 2368 672 100 0 -1 val(outp):$(top)dc:obsControl.G
use hwout 2144 695 100 0 hwout#268
xform 0 2240 736
p 2368 736 100 0 -1 val(outp):$(top)dc:obsControl.F
use hwout 2144 503 100 0 hwout#240
xform 0 2240 544
p 2368 544 100 0 -1 val(outp):$(top)dc:obsControl.I
use hwout 2144 567 100 0 hwout#239
xform 0 2240 608
p 2368 608 100 0 -1 val(outp):$(top)dc:obsControl.H
use hwout 2144 823 100 0 hwout#213
xform 0 2240 864
p 2368 864 100 0 -1 val(outp):$(top)dc:obsControl.D
use hwout 2144 887 100 0 hwout#132
xform 0 2240 928
p 2368 928 100 0 -1 val(outp):$(top)dc:obsControl.C
use hwout 2144 951 100 0 hwout#131
xform 0 2240 992
p 2368 992 100 0 -1 val(outp):$(top)dc:obsControl.B
use ecad20 -128 -89 100 0 ecad20#227
xform 0 32 800
p -32 1376 100 0 0 FTVA:LONG
p -32 1344 100 0 0 FTVB:LONG
p -32 1280 100 0 0 FTVD:LONG
p -32 1248 100 0 0 FTVE:LONG
p -32 1184 100 0 0 FTVG:LONG
p -32 1152 100 0 0 FTVH:LONG
p -32 1120 100 0 0 FTVI:DOUBLE
p -32 1088 100 0 0 FTVJ:DOUBLE
p -32 1024 100 0 0 FTVL:STRING
p -32 928 100 0 0 FTVO:STRING
p -32 832 100 0 0 FTVR:LONG
p -64 -128 100 0 1 INAM:flamIsNullInit
p -64 -160 100 0 1 SNAM:flamIsObsSetupProcess
p -64 -96 100 0 1 name:$(top)observationSetup
use flamSubSysCommand 416 -185 100 0 flamSubSysCommand#57
xform 0 560 0
p 416 -224 100 0 1 setCommand:cmd obsSetup
p 416 -192 100 0 1 setSystem:sys dc
use flamSubSysCommand 1152 -217 100 0 flamSubSysCommand#197
xform 0 1296 -32
p 1152 -256 100 0 1 setCommand:cmd obsSetup
p 1152 -224 100 0 1 setSystem:sys ec
use flamIsEcUpdate 800 -217 100 0 flamIsEcUpdate#194
xform 0 944 -32
use egenSub 1504 231 100 0 egenSub#130
xform 0 1648 656
p 1281 5 100 0 0 FTA:LONG
p 1281 5 100 0 0 FTB:LONG
p 1281 -27 100 0 0 FTC:LONG
p 1281 -59 100 0 0 FTD:LONG
p 1281 -91 100 0 0 FTE:STRING
p 1281 -155 100 0 0 FTF:LONG
p 1281 -155 100 0 0 FTG:LONG
p 1281 -187 100 0 0 FTH:DOUBLE
p 1281 -219 100 0 0 FTI:DOUBLE
p 1281 -251 100 0 0 FTJ:STRING
p 1281 5 100 0 0 FTVA:STRING
p 1281 5 100 0 0 FTVB:STRING
p 1281 -27 100 0 0 FTVC:STRING
p 1281 -59 100 0 0 FTVD:STRING
p 1281 -91 100 0 0 FTVE:STRING
p 1281 -155 100 0 0 FTVF:STRING
p 1281 -155 100 0 0 FTVG:STRING
p 1281 -187 100 0 0 FTVH:STRING
p 1281 -219 100 0 0 FTVI:STRING
p 1281 -251 100 0 0 FTVJ:STRING
p 1568 192 100 0 1 INAM:flamIsNullGInit
p 1568 160 100 0 1 SNAM:flamIsCopyGProcess
p 1568 224 100 768 1 name:$(top)obsSetupPreset2G
use outhier 208 1920 100 0 VAL
xform 0 176 1920
use outhier 208 1856 100 0 MESS
xform 0 176 1856
use inhier -160 1920 100 0 DIR
xform 0 -96 1920
use inhier -176 1856 100 0 ICID
xform 0 -96 1856
use ecars 1600 -217 100 0 ecars#53
xform 0 1760 -48
p 1760 -224 100 1024 1 name:$(top)observationSetupC
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamObsSetup.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
