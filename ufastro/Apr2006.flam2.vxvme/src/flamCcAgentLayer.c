

/*
 *
 *  Header for cc agent simulation interface layer code.
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <msgQLib.h>
#include <logLib.h>

#include <recSup.h>

#include <cad.h>
#include <cadRecord.h>

#include <genSubRecord.h>

#include <flamCcAgentLayer.h>
#include <flamCcAgentSim.h>


/*
 * Local defines
 */

#define MAX_MESSAGES                20	/* max messages per 
					   queue */
#define CAD_Q_WAIT_TIME             5	/* max time to wait 
					   for Q */


/*
 *  Private data
 */

static MSG_Q_ID ccCommandQ = NULL;	/* message queue
					   for agent */



/*
 *************************************
 *
 *  Header for ccCadInit
 *
 *
 *************************************
 */


long
ccCadInit (cadRecord * pcr	/* cad record structure */
  )
{
  long status;			/* function return status */



  /* 
   *  If a message queue has already been created then there is nothing
   *  to do since initialization is only done the first time this function
   *  is called
   */

  if (ccCommandQ)
    {
      return OK;
    }


  /* 
   *  Otherwise this is the first call, create a message queue 
   *  to communicate with the simulated CC agent then initialize
   *  the agent itself.
   */

  ccCommandQ = msgQCreate (MAX_MESSAGES, sizeof (ccAgentCommand), MSG_Q_FIFO);
  if (ccCommandQ == NULL)
    {
      status = -1;
      recGblRecordError (status, pcr, __FILE__ ":can't create message Queue");
      return status;
    }

  status = initCcAgentSim (ccCommandQ);

  return status;
}


 /* 
  *************************************
  *
  *  Header for ccCadSend
  *
  *
  *************************************
  */

long
ccCadSend (cadRecord * pcr	/* cad record structure */
  )
{
  char *pCommand;		/* command name pointer */
  ccAgentCommand command;	/* agent command structure */
  long status;			/* function return status */


  /* 
   *  Process according to the directive given
   */

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;


      /* 
       *  Preset and Start both check the input attributes before doing
       *  anything.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      if (strcmp (pcr->a, "ERROR") == 0)
	{
	  strncpy (pcr->mess, "Invalid attribute", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      /* 
       *  Preset just checks the attributes, so bail out here
       */

      if (pcr->dir == menuDirectivePRESET)
	{
	  break;
	}


      /* 
       *  Start executes the command.
       */


      /* 
       *  Copy record names into the command structure then
       *  place it on the message queue.
       */

      strcpy (command.carName, "flam:cc:applyC");
      strcpy (command.attributeA, pcr->a);

      /* 
       *  Isolate the command name from the record name
       */

      for (pCommand = pcr->name; *pCommand != ':'; pCommand++);
      for (pCommand++; *pCommand != ':'; pCommand++);
      pCommand++;

      strcpy (command.commandName, pCommand);

      command.executionTime = 20;

      status = msgQSend (ccCommandQ,
			 (char *) &command,
			 sizeof (command), CAD_Q_WAIT_TIME, MSG_PRI_NORMAL);
      if (status == ERROR)
	{
	  strncpy (pcr->mess, "Can not send agent message", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}


      /* 
       *  Message sent, so copy a to vala to make this the
       *  current value.
       */

      strcpy (pcr->vala, pcr->a);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


 /* 
  *************************************
  *
  *  Header for ccCadNoSend
  *
  *
  *************************************
  */

long
ccCadNoSend (cadRecord * pcr	/* cad record structure */
  )
{

  /* 
   *  Process according to the directive given
   */

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;


      /* 
       *  Preset and Start both check the input attributes before doing
       *  anything.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:


      if (strcmp (pcr->a, "ERROR") == 0)
	{
	  strncpy (pcr->mess, "Invalid attribute", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}


      /* 
       *  Preset just checks the attributes, so bail out here
       */

      if (pcr->dir == menuDirectivePRESET)
	{
	  break;
	}


      /* 
       *  Start simply copies A to VALA.
       */

      strcpy (pcr->vala, pcr->a);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}
