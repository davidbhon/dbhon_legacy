#if !defined(__FLAM_C__)
#define __FLAM_C__ "RCS: $Name:  $ $Id: flam.c,v 0.1 2004/10/11 21:04:10 rojas Exp $"
static const char rcsIdufFLAMC[] = __FLAM_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

/* #include <car.h>
#include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <dbStaticLib.h>
#include <dbBase.h>

#include "flam.h"
#include "ufdbl.h"

void
trx_debug (const char *message, const char *rec_name,
	   const char *mess_level, const char *debug_level)
{
  bool log_it = 0;
  int imess_level = 0;
  int idebug_level = 0;

  if (strcmp (debug_level, "FULL") == 0)
    idebug_level = 2;
  if (strcmp (debug_level, "MID") == 0)
    idebug_level = 1;
  if (strcmp (debug_level, "NONE") == 0)
    idebug_level = 0;

  if (strcmp (mess_level, "FULL") == 0)
    imess_level = 2;
  if (strcmp (mess_level, "MID") == 0)
    imess_level = 1;
  if (strcmp (mess_level, "NONE") == 0)
    imess_level = 0;

  if (imess_level <= idebug_level)
    log_it = true;

  if (log_it)
    {
      char out_mess[ 256 ] ;
      strcpy (out_mess, "<%ld> ");
      strcat (out_mess, rec_name);
      strcat (out_mess, ":");
      strcat (out_mess, message);
      strcat (out_mess, "\n");
      logMsg (out_mess, (int) tickGet (), 0, 0, 0, 0, 0);
    }
}

int
init_flam_config ()
{
  FILE *trxFile = 0;
  char in_str[80];
  char *temp_str = 0;

  trxFile = fopen( "./pv/flam_cfg.txt", "r" ) ;
  if( NULL == trxFile )
    {
      fprintf( stderr, __HERE__ "> Unable to open file: ./pv/flam_cfg.txt" ) ;
      return -1 ;
    }

  NumFiles++;
  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (ccconfig_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (mot_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (dcconfig_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (ecconfig_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (env_gs_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (env_hb_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (env_array_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (env_tempMon_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (env_press_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (temp_cont_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (ch_param_filename, in_str);

  fgets (in_str, 40, trxFile);
  fgets (in_str, 40, trxFile);
  temp_str = strchr (in_str, '\n');
  if (temp_str != NULL)
    temp_str[0] = '\0';
  strcpy (vac_param_filename, in_str);

  fclose (trxFile);
  NumFiles--;

  flam_initialized = 1;
  fprintf( stderr,__HERE__ "> flamingos-2 Initialized\n" ) ;

  return OK;
}

#endif
