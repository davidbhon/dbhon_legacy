package ufjdd;
/**
 * Title:        TelescopePanel.java  (CORBA interface)
 * Version:      (see rcsID)
 * Authors:      Craig Warner, Ziad Saleh, Frank Varosi
 * Company:      University of Florida
 * Description:  For sending position offset requests to telescope and monitoring pointing.
 */
import javaUFLib.*;
import javaUFProtocol.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import CIS.*;
import DAF.*;
import TelescopeServer.*;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;

public class TelescopePanel extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: TelescopePanel.java,v 1.3 2005/11/29 21:08:10 drashkin Exp $";

    public String status;
    Telescope_ifce telescope_ifce;
    public double currentRA = 0, currentDec = 0, offset = 0.1;
    private JLabel labelRA, labelDec, labelOffset;
    private JTextField otherOffset;
    private JComboBox selectOffset;
    public String[] offsetOptions = {"0.1 arcsec", "0.5 arcsec", "1.0 arcsec", "5.0 arcsec", "10 arcsec", "20 arcsec", "Other (below)"};

    private int xSize = 500, ySize = 400;

    public TelescopePanel( String tcshost, String tcsport, String[] args ) {
        initComponents();
        this.setTitle("Telescope  Offset  Control");
        this.setSize( xSize, ySize );
        this.setLocation(5, 5);
        this.setResizable(false);
        this.setForeground(Color.blue);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        String[] arg = new String[2];;
        arg[0] = "-ORBInitRef";
        arg[1] = "NameService=corbaloc:iiop:" + tcshost + ":" + tcsport + "/NameService" ;
 
        try {
            telescope_ifce = getTelescopeServerReference(arg);
            status = "Connected to CORBA Telescope Server";
            currentRA = telescope_ifce.getRightAscention();
            currentDec = telescope_ifce.getDeclination();
        } catch (Exception ex) {
            status = "Failed connecting to CORBA Telescope Server";
        }
        
        System.out.println(status);
	labelRA.setText("RA:  " + decimalToDegrees(currentRA));
        labelDec.setText("Dec: " + decimalToDegrees(currentDec));
    }
    
    private void initComponents() {
        JButton downButton = new JButton();
        JButton upButton = new JButton();
        JButton leftButton = new JButton();
        JButton rightButton = new JButton();
        JButton doneButton = new JButton("Done");
	labelRA = new JLabel("RA:  " + decimalToDegrees(currentRA));
	labelDec = new JLabel("Dec: " + decimalToDegrees(currentDec));
	labelOffset = new JLabel("Offset: ");
	selectOffset = new JComboBox(offsetOptions);
	selectOffset.setSelectedIndex(0);
	otherOffset = new JTextField(5);

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                hide();
            }
        });
                
        upButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (selectOffset.getSelectedIndex() == offsetOptions.length-1)
                    try {
                        String offsetValue = otherOffset.getText();
                        offset = Double.parseDouble(offsetValue);
                    } catch(NumberFormatException e) {
                        System.out.println("Invalid Offset");
                        offset = 0.0;
                    }
                try {
                    telescope_ifce.requestTelescopeOffset(0.0,offset/3600.0);
                    //System.out.println("Telescope moved up " + offset + " arcsec.");
		    currentDec = telescope_ifce.getDeclination(); 
        	    labelDec.setText("Dec: " + decimalToDegrees(currentDec));
                } catch(DAF.GCSException gcse) {
                    gcse.printStackTrace();
                    System.out.println("Can't move telescope ");
                }
            }
        });
        
        downButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (selectOffset.getSelectedIndex() == offsetOptions.length-1)
                    try {
                        String offsetValue = otherOffset.getText();
                        offset = Double.parseDouble(offsetValue);
                    } catch(NumberFormatException e) {
                        System.out.println("Invalid Offset");
                        offset = 0.0;
                    }
                try {
                    telescope_ifce.requestTelescopeOffset(0.0,-1.0*offset/3600.0);
                    //System.out.println("Telescope moved down " + offset + " arcsec.");
                    currentDec = telescope_ifce.getDeclination();
                    labelDec.setText("Dec: " + decimalToDegrees(currentDec));
                } catch(DAF.GCSException gcse) {
                    gcse.printStackTrace();
                    System.out.println("Can't move telescope ");
                }
            }
        });
        
        leftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (selectOffset.getSelectedIndex() == offsetOptions.length-1)
                    try {
                        String offsetValue = otherOffset.getText();
                        offset = Double.parseDouble(offsetValue);
                    } catch(NumberFormatException e) {
                        System.out.println("Invalid Offset");
			offset = 0.0;
                    }
                try {
                    telescope_ifce.requestTelescopeOffset(-1.0*offset/3600.0,0.0);
                    //System.out.println("Telescope moved left " + offset + " arcsec.");
                    currentRA = telescope_ifce.getRightAscention();
         	    labelRA.setText("RA:  " + decimalToDegrees(currentRA));
                } catch(DAF.GCSException gcse) {
                    gcse.printStackTrace();
                    System.out.println("Can't move telescope ");
                }
            }
        });
        
        rightButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (selectOffset.getSelectedIndex() == offsetOptions.length-1)
                    try {
                        String offsetValue = otherOffset.getText();
                        offset = Double.parseDouble(offsetValue);
                    } catch(NumberFormatException e) {
                        System.out.println("Invalid Offset");
                        offset = 0.0;
                    }
                try {
                    telescope_ifce.requestTelescopeOffset(offset/3600.0,0.0);
                    //System.out.println("Telescope moved right " + offset + " arcsec.");
                    currentRA = telescope_ifce.getRightAscention();
                    labelRA.setText("RA:  " + decimalToDegrees(currentRA));
                } catch(DAF.GCSException gcse) {
                    gcse.printStackTrace();
                    System.out.println("Can't move telescope ");
                }
            }
        });
        
	selectOffset.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ev) {
		int offsetIndex = selectOffset.getSelectedIndex();
                double[] offsetValues = {0.1,0.5,1.0,5.0,10.0,30.0};
    		if (offsetIndex < offsetValues.length) {
            	    offset = offsetValues[offsetIndex];
        	} else {
		    String offsetValue = otherOffset.getText();
		    try {
			offset = Double.parseDouble(offsetValue);
		    } catch(NumberFormatException e) {
			System.out.println("Invalid Offset");
		    }
            	}
                System.out.println("The Offset has been changed to " + offset + " arcsec.");
	    }
	});
        
	doneButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { done(); } });

        Properties props = new Properties(System.getProperties());
	String data_path = props.getProperty("jdd.data_path");
        System.out.println("jdd.data_path := " + data_path);

        downButton.setIcon(new ImageIcon(data_path + "/Down.gif"));
        upButton.setIcon(new ImageIcon(data_path + "/Up.gif"));
        leftButton.setIcon(new ImageIcon(data_path + "/Left.gif"));
	rightButton.setIcon(new ImageIcon(data_path + "/Right.gif"));

        getContentPane().setLayout(new RatioLayout());

        getContentPane().add("0.42,0.80;0.15,0.15", downButton );
        getContentPane().add("0.42,0.08;0.15,0.15", upButton );
        getContentPane().add("0.05,0.42;0.15,0.15", leftButton );
        getContentPane().add("0.80,0.42;0.15,0.15", rightButton );
        getContentPane().add("0.80,0.80;0.15,0.15", doneButton );

	getContentPane().add("0.35,0.35;0.30,0.08", labelRA );
        getContentPane().add("0.35,0.42;0.30,0.08", labelDec );
	getContentPane().add("0.35,0.52;0.10,0.08", labelOffset );
	getContentPane().add("0.45,0.52;0.20,0.08", selectOffset );
	getContentPane().add("0.35,0.62;0.30,0.08", otherOffset );

        pack();
    }
    
    private void done() { this.dispose(); }

    public static Telescope_ifce getTelescopeServerReference(String args[]) {
        try {
            // create and initialize the ORB
            ORB orb = ORB.init(args,null);
            
            // get the root naming context
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

            // Use NamingContextExt instead of NamingContext.
	    // This is part of the Interoperable naming Service.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            
            // resolve the Object Refernce in Naming
            String name = "Telescope.GCS";
            
            Telescope_ifce telescope_ifce = Telescope_ifceHelper.narrow(ncRef.resolve_str(name));
            return telescope_ifce;
        }
	catch (NotFound nfe) {
            System.out.println(nfe.toString());
            return null;
        }
	catch (Exception e) {
            System.out.println( e.toString());
            e.printStackTrace(System.out);
            return null;
        }
    }
   
    public static String decimalToDegrees(double dec) {
        String deg = "";
        int d,m,s,t;
        if (dec >= 0) {
            d = (int)dec;
            if (d < 10) deg+= "0" + d + ":"; else deg+= d + ":";
            m = (int)(dec*60-d*60);
            if (m < 10) deg+= "0" + m + ":"; else deg+= m + ":";
            s = (int)(dec*3600-d*3600-m*60+0.005);
            if (s < 10) deg+= "0" + s; else deg+= s;
	    t = (int)(dec*360000-d*360000-m*6000-s*100+0.5);
	    if (t < 10) deg+= ".0" + t; else deg+= "." + t;
        } else {
            deg+= "-";
            d = (int)(Math.abs(dec));
            if (d < 10) deg+= "0" + d + ":"; else deg+= d + ":";
            m = (int)Math.abs(dec*60+d*60);
            if (m < 10) deg+= "0" + m + ":"; else deg+= m + ":";
            s = (int)Math.abs(dec*3600+d*3600+m*60-0.005);
            if (s < 10) deg+= "0" + s; else deg+= s;
	    t = (int)Math.abs(dec*360000+d*360000+m*6000+s*100-0.5);
            if (t < 10) deg+= ".0" + t; else deg+= "." + t;
        }
        return deg;
    }
}
