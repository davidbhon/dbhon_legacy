package ufjdd;
/**
 * Title:        ColorBar.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh and Frank Varosi
 * Company:      University of Florida
 * Description:  Object for displaying vertical color bar for image color scale.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.util.*;
import java.awt.event.*;

public class ColorBar extends JPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: ColorBar.java,v 1.1 2005/05/05 15:26:12 amarin Exp $";

    byte colorBar[];
    int width = 30;
    int Ncolors = 256;
    IndexColorModel colorModel;
    Image cbarImage;
    
    public ColorBar(IndexColorModel colorModel, int width, int Ncolors) {
        
        this.colorModel = colorModel;
        this.width = width;
	this.Ncolors = Ncolors;
        this.setPreferredSize( new Dimension( width, Ncolors ) );

        int index = Ncolors*width;
        colorBar = new byte[Ncolors*width];

        for( int i=0; i < Ncolors; i++ ) {
            for( int j=0; j < width; j++ ) colorBar[--index] = (byte)i;
        }

        cbarImage = createImage(new MemoryImageSource(width, Ncolors, colorModel, colorBar, 0, width));
    }
    
    public void paintComponent( Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g.drawImage(cbarImage, 0, 0, this);
    }
    
    public void updateColorMap(IndexColorModel colorModel) {
        cbarImage = createImage(new MemoryImageSource(width, Ncolors, colorModel, colorBar, 0, width));
        repaint();
    }
}
