package ufjdd;
/**
 * Title:        Java Data Display (JDD): ImageDisplayPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Ziad Saleh, Frank Varosi, & Craig Warner
 * Company:      University of Florida
 * Description:  For single image display of contents of a frame buffer from NIR Acq. Server
 */
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;
import javaUFProtocol.*;

class ImageDisplayPanel extends JPanel implements FocusListener, KeyListener {
    public static final
	String rcsID = "$Name:  $ $Id: ImageDisplayPanel.java,v 1.22 2006/09/07 16:26:46 flam Exp $";

    public int width, height;
    protected int xZoom, yZoom;
    protected int zoomRect;
    //protected int zoomFactor=0;
    protected float zoomFactor=0;
    protected int oMin, oMax;
    protected float newMin, newMax;
    protected boolean showZoom = false;
    protected int zoomPixels[][];
    protected byte scaledPixels[];
    protected Image pixImage = null;
    protected ImageBuffer imageBuffer = null;
    protected ImageBuffer imageBuffer1k = null;
    protected ImageBuffer bgdBuffer = null;
    protected ImageBuffer bgdPreviewBuffer = null;
    protected ImageBuffer srcPreviewBuffer = null;
    protected ImageBuffer savedImgBuff = null;
    protected boolean displDif = true;
    protected boolean displBgd = false;
    protected boolean displSrc = false;
    protected IndexColorModel colorModel;
    protected Color transLucent = new Color(128, 255, 128, 33);
    protected Color transLucent2 = new Color(255, 128, 128, 33);
    protected JComboBox zoomFacSelect;
    protected ZoomPanel zoomPanel;
    protected ImagePanel[] imagePanels;
    protected int xRulerInit, xRulerFin;
    protected int yRulerInit, yRulerFin;
    protected int xisav, yisav;
    protected String[] dragObjectStatus = {"Hidden", "Hidden", "Hidden", "Hidden"};
    protected Color[] dragObjectColor = {Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN};
    protected int[] dragObjectX = {100, 150, 200, 250};
    protected int[] dragObjectY = {100, 150, 200, 250};
    protected boolean[] dragObject = {false, false, false, false};
    protected boolean showRuler= false;
    protected int mouseButton = 0;
    protected int xInit = 0, yInit = 0;
    protected float minInit = 0, maxInit = 0;	
    protected boolean showMOS = false;   
    boolean doLinear = true;   
    boolean doLog    = false;	
    boolean doPower  = false;	
    protected String srcFile = "fileName";
    protected String bgdFile = "";
    protected int oldWidth = 0;
    protected int oldHeight = 0;
    protected MOSMask[] mosMasks;
    protected boolean drawCoords = false;

    public ImageDisplayPanel( ImagePanel[] imagePanelsPtr, IndexColorModel colorModelPtr,
			      UFFrameConfig frameConfig, ZoomPanel zoomPanelPtr )
    {
        this.colorModel = colorModelPtr;
	this.imagePanels = imagePanelsPtr;
        this.zoomPanel = zoomPanelPtr;
        this.zoomFacSelect = zoomPanel.zoomFacSelect;
	this.width = frameConfig.width;
	this.height = frameConfig.height;
        this.setMinimumSize(new Dimension(width, height));
        this.scaledPixels = new byte[ width * height ];

	this.zoomPanel.zoomImage.addImageDisplayPanel(this);
        
	if(oldWidth != width || oldHeight != height) {
	repaint();
	int oldWidth = width;
	int oldHeight = height;
	}

	this.addKeyListener(this);
	this.addFocusListener(this);
	
	zoomFactor = 2;
        zoomFacSelect.setSelectedItem("2");
	zoomRect = (int)(zoomPanel.zoomImage.xySize/zoomFactor);

        zoomFacSelect.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) {

		    String zfitem = (String)zoomFacSelect.getSelectedItem();

		    if( zfitem.length() == 0 )
			zoomFactor = 1;
		    else
			//zoomFactor = Integer.parseInt( zfitem );
			zoomFactor = Float.parseFloat(zfitem);

		    zoomRect = (int)(zoomPanel.zoomImage.xySize/zoomFactor);

		    if( showZoom ) {
			ZoomImage();
			repaint();
		    }
		}
	    });
        
        addMouseListener(new MouseAdapter() {
	
		public void mousePressed(MouseEvent mev)
		{
		    if( (mev.getModifiers() & InputEvent.BUTTON2_MASK) != 0 ) 
		    {		    
			mouseButton = 2;
			xInit = mev.getX();
			yInit = mev.getY();

			for (int j = 0; j < 4; j++) {
			   if (dragObjectStatus[j].equals("V Line") && Math.abs(xInit-dragObjectX[j]) < 50) {
			      xisav = dragObjectX[j];
			      yisav = 0;
			      for (int l = 0; l < 4; l++) {
				if (l == j) dragObject[l] = true; else dragObject[l] = false;
			      }
			   } else if (dragObjectStatus[j].equals("H Line") && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = 0;
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("+") && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("X") && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("Circle")  && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
                           } else dragObject[j] = false; 
			}
	            }
		    if( (mev.getModifiers() & InputEvent.BUTTON1_MASK) != 0 ) 
		    {
			requestFocus();
			mouseButton = 1;
			xInit = mev.getX();
			yInit = mev.getY();
			
		 	if( showRuler ) {
			   xisav = xInit;
			   yisav = yInit;      
			}
		        else {
			   ZoomEvent(mev);
			}
	            }  

		    if( (mev.getModifiers() & InputEvent.BUTTON3_MASK) != 0 ) 
		    {
			mouseButton = 3;
			xInit = mev.getX();
			yInit = mev.getY();
			minInit = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
			maxInit = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
	            }  
  
		}
		public void mouseEntered(MouseEvent mev) {
		   if( showZoom ) requestFocus();
		}
    		public void mouseExited(MouseEvent mev) {}
    		public void mouseReleased(MouseEvent mev) {
		   if (drawCoords) {
			drawCoords = false;
			repaint();
		   }
		}
    		public void mouseClicked(MouseEvent mev) {}
	    });
	    
        addMouseMotionListener(new MouseMotionAdapter() {
		
		public void mouseMoved(MouseEvent mev) {
		    int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
		    int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
		    double scale = 2048.0/(Math.min(width, height));
		    int xz = mev.getX();
		    int yz = mev.getY();
		    int xLoc = (int)(xz*scale);
		    int yLoc = (int)(2047-yz*scale);
		    if (yLoc < 0) yLoc = 0;
		    double data = 0;
		    if (imageBuffer != null) data = imageBuffer.pixels[xLoc+2048*(2047-yLoc)];
                    InitDataDisplay.jddFullFrame.pixelDataVal.setText(" x="+xLoc+", y="+yLoc+", Data = "+data);
		    if (zoomFactor > 0 && showZoom) {
                	//int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
                	//int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
	                //double scale = 2048.0/(Math.min(width, height));
                        //int xz = mev.getX();
                        //int yz = mev.getY();
	                int xiz = xZoom - (int)(zoomRect/(2*scale));
        	        int yiz = yZoom - (int)(zoomRect/(2*scale));
			int w = (int)(zoomRect/scale);
			if (xz >= xiz && xz <= xiz+w && yz >= yiz && yz <= yiz+w) {
			   setToolTipText("X: "+(int)(xZoom*scale)+"; Y: "+(int)(2047-yZoom*scale)+"; width: "+480/zoomFactor);
			} else setToolTipText(null);
		    }
		}

		public void mouseDragged(MouseEvent mev) 
		{
                        int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
                        int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
			int xz = mev.getX();
			int yz = mev.getY();
                    	if (zoomFactor > 0 && showZoom) {
                            double scale = 2048.0/(Math.min(width, height));
                            int xiz = xZoom - (int)(zoomRect/(2*scale));
                            int yiz = yZoom - (int)(zoomRect/(2*scale));
                            int w = (int)(zoomRect/scale);
                            if (xz >= xiz && xz <= xiz+w && yz >= yiz && yz <= yiz+w) {
				setToolTipText("X: "+(int)(xz*scale+0.5)+"; Y: "+(int)(2047.5-yz*scale)+"; width: "+480/zoomFactor);
                            } else setToolTipText(null);
			    drawCoords = true;
                    	}
			int xOffset = mev.getX()-xInit;
			int yOffset = mev.getY()-yInit;
			int xi = xisav + xOffset;
			int yi = yisav + yOffset;

		    if( mouseButton == 2 ) {
			for (int j = 0; j < 4; j++) {
			    if (dragObject[j]) {
				dragObjectX[j] = xi;
				dragObjectY[j] = yi;
			    }
			}
 		    }
		    else if( mouseButton == 1 ) {
		 
		 	if( showRuler) {
			  xRulerInit = xisav;
			  yRulerInit = yisav;
			  xRulerFin  = xi;
			  yRulerFin  = yi;
			}
			else {
			   ZoomEvent(mev);
			};
		    }
		    else if( mouseButton == 3 ) {
	                String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
	                String scaleBoxSel = (String)(InitDataDisplay.jddFullFrame.scaleBox.getSelectedItem());
	               if (zModeBoxSel.indexOf("Auto") >= 0) { //nothing to do
 	               } else if (zModeBoxSel.indexOf("Manual") >= 0) {
			 imagePanels[0].adjustPanel.updateMinMax();
		         int imMin = (int)imagePanels[0].adjustPanel.fmin;
			 int imMax = (int)imagePanels[0].adjustPanel.fmax;
                         int cx = xz - xInit;
                         int cy = yz - yInit;
		         if (scaleBoxSel.indexOf("Linear") >= 0) {
                           int min = (int)(minInit-cy*(0.3*(imMax-imMin))/height);
                           int max = (int)(maxInit+cx*(imMax-imMin)/width);
			   InitDataDisplay.jddFullFrame.zMax.setText(String.valueOf(max));
			   InitDataDisplay.jddFullFrame.zMin.setText(String.valueOf(min));
			   imagePanels[0].imageDisplay.applyLinearScale(min, max);
			   InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale(min, max);
                           if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                                InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max);
			   //Update scaling on Zoomed Image
			   if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
			      zoomPanel.zoomImage.applyLinearScale(min,max);
			   }
			 } else if (scaleBoxSel.indexOf("Log") >= 0) {
	                   int min = (int)(maxInit+cx*(0.1*(imMax-imMin))/width);
			   InitDataDisplay.jddFullFrame.zMax.setText(String.valueOf(min));
			   imagePanels[0].imageDisplay.applyLogScale(min);
                           InitDataDisplay.jddFullFrame.srcPreviewImage.applyLogScale(min);
                           if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                                InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLogScale(min);
			   if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
			      zoomPanel.zoomImage.applyLogScale(min);
			   }
			 } else if (scaleBoxSel.indexOf("Power") >= 0) {
	                   float power = minInit-(float)cy*5/height;
	                   int min = (int)(maxInit+cx*(0.1*(imMax-imMin))/width);
			   InitDataDisplay.jddFullFrame.zMax.setText(String.valueOf(min));
			   String powerString = String.valueOf(power);
			   if (powerString.length() > 5)
			      powerString = powerString.substring(0,5);
			   InitDataDisplay.jddFullFrame.zMin.setText(powerString);
			   imagePanels[0].imageDisplay.applyPowerScale(min, power);
                           InitDataDisplay.jddFullFrame.srcPreviewImage.applyPowerScale(min, power);
			   if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
				InitDataDisplay.jddFullFrame.bgdPreviewImage.applyPowerScale(min, power);
                           if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
                              zoomPanel.zoomImage.applyPowerScale(min, power);
                           }
			 }
                       }
		    }
		    repaint();
		}
	    });      
    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void updateImage(ImageBuffer imgBuff)
    {
        if( imgBuff == null ) {
	    imageBuffer1k = null;
	    pixImage = null;
	}
	else {
	    savedImgBuff = imgBuff;
	    int width = InitDataDisplay.jddFullFrame.srcPreviewImage.getWidth();
	    int height = InitDataDisplay.jddFullFrame.srcPreviewImage.getHeight();
	    int size = Math.min(width, height);
	    srcPreviewBuffer = imgBuff.rescale(size);
            //srcPreviewBuffer   = imgBuff.rescale256();
	    
	    InitDataDisplay.jddFullFrame.srcPreviewImage.updateImage( srcPreviewBuffer, colorModel );
            InitDataDisplay.jddFullFrame.srcPreviewImage.updateSize(size);

            String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
            float min = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
            float max = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
	    
            if (zModeBoxSel.indexOf("Auto") >= 0) {
               InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale();
	       if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
		   InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale();
            } else if (zModeBoxSel.indexOf("Manual") >= 0) { 
               InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale(min, max);
               if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                   InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max);
            } else if (zModeBoxSel.indexOf("Zoom") >= 0) {
               InitDataDisplay.jddFullFrame.srcPreviewImage.setZoomScale(zoomPanel);
               if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                   InitDataDisplay.jddFullFrame.bgdPreviewImage.setZoomScale(zoomPanel);
            } else if (zModeBoxSel.startsWith("Z")) {
	       InitDataDisplay.jddFullFrame.srcPreviewImage.applyZScale(zModeBoxSel);
               if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                   InitDataDisplay.jddFullFrame.bgdPreviewImage.applyZScale(zModeBoxSel);
            }

	    width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
	    height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
	    size = Math.min(width, height);
	    
	    if (displSrc) 
	    {
                imageBuffer        = imgBuff;
		imageBuffer1k = imageBuffer.rescale(size);
	        //imageBuffer1k      = imageBuffer.rescale1k();
	        oMax = imageBuffer1k.max;
	        oMin = imageBuffer1k.min;
		
	    } else if( displBgd )
	    {
	        if (bgdBuffer==null) 
		{
	           imageBuffer1k = null;
	           pixImage = null;
		} 
                else 
		{
		   imageBuffer = bgdBuffer;
                   imageBuffer1k = imageBuffer.rescale(size);
	           //imageBuffer1k = imageBuffer.rescale1k();
	           oMax = imageBuffer1k.max;
	           oMin = imageBuffer1k.min;
		}
	    
	    } else if (displDif)
	    {
		if (bgdBuffer==null) {
		   imageBuffer = imgBuff;
		} 
		else {
		   imageBuffer = imgBuff.subtractReference( bgdBuffer );
		}
                imageBuffer1k = imageBuffer.rescale(size);
	        //imageBuffer1k = imageBuffer.rescale1k();
	        oMax = imageBuffer1k.max;
	        oMin = imageBuffer1k.min;
            }
	    	    
	    if( showZoom ) ZoomImage();
	    
	}
    }
    
    public synchronized void updateImage( IndexColorModel icm )
    {
        this.colorModel = icm;
        
        if( imageBuffer1k == null )
	    pixImage = null;
        else
            pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        
	String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
	float min = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
	float max = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
	
	InitDataDisplay.jddFullFrame.srcPreviewImage.updateImage( srcPreviewBuffer, colorModel );
	InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale();	
	if (bgdPreviewBuffer != null) {
	   InitDataDisplay.jddFullFrame.bgdPreviewImage.updateImage( bgdPreviewBuffer, colorModel );
           InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale();
	}
	/*if (zModeBoxSel.indexOf("Auto") >= 0) {
	   InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale();
	} else if (zModeBoxSel.indexOf("Manual") >= 0) { 
	   InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale(min, max);
	}
	
	if (bgdPreviewBuffer != null) {
	   InitDataDisplay.jddFullFrame.bgdPreviewImage.updateImage( bgdPreviewBuffer, colorModel );
           if (zModeBoxSel.indexOf("Auto") >= 0) {
              InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale();
           } else if (zModeBoxSel.indexOf("Manual") >= 0) { 
              InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max);
           }
	}*/

        repaint();
    }
    
//------------------------------------------------------------------------------------------

    public synchronized void applyLinearScale() { applyLinearScale( oMin, oMax ); }

    public synchronized void applyLinearScale(float minv, float maxv)
    {
        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

        newMin = minv;
        newMax = maxv;
        float range = maxv - minv;
        
        for(int i=0; i < imageBuffer1k.pixels.length; i++)
	    {
		float f = (float)( imageBuffer1k.pixels[i] - minv )/range;

		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }
	String s = String.valueOf(maxv);
	int epos = s.indexOf("E");
	if (epos != -1) {
	   s = s.substring(0,Math.min(5,epos))+s.substring(epos);
	}
        InitDataDisplay.jddFullFrame.zMax.setText(s);
        s = String.valueOf(minv);
        epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
        InitDataDisplay.jddFullFrame.zMin.setText(s);
	pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height,	colorModel, scaledPixels, 0, imageBuffer1k.width));

        repaint();

    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void applyLogScale(double threshold) {

        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
	double maxv = Math.log( (double)oMax );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer1k.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer1k.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.log( pval ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();
    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void applyPowerScale(double threshold, double power) {

        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
	if( power <= 0 ) power = 0.5;
	if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
	double maxv = Math.pow( (double)oMax, power );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer1k.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer1k.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.pow( pval, power ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();
    }
//-------------------------------------------------------------------------------------------------------

    public synchronized void applyZScale() {
	this.applyZScale("Zscale");
    }

    public synchronized void applyZScale(String zMode)
    {
        if( imageBuffer1k == null ) {
            pixImage = null;
            repaint();
            return;
        }
	imageBuffer.Zscale();
	float minv, maxv;
	if (zMode.equals("Zmax")) {
	    minv = (float)imageBuffer.ZscaleRange[0];
	    maxv = oMax;
	} else if (zMode.equals("Zmin")) {
	    minv = oMin;
	    maxv = (float)imageBuffer.ZscaleRange[1];
	} else {
	    minv = (float)imageBuffer.ZscaleRange[0];
            maxv = (float)imageBuffer.ZscaleRange[1];
	}

	newMin = minv;
	newMax = maxv;
        float range = maxv - minv;
        for(int i=0; i < imageBuffer1k.pixels.length; i++)
            {
                float f = (float)( imageBuffer1k.pixels[i] - minv )/range;

                if( f <= 0 )
                    scaledPixels[i] = 0;
                else if( f >= 1 )
                    scaledPixels[i] = (byte)255;
                else
                    scaledPixels[i] = (byte)Math.round(f*255);
            }


        String s = String.valueOf(newMax);
        int epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
	else if (s.length() > 6 && s.indexOf(".") != -1) {
	   s = s.substring(0,s.indexOf(".")+2);
	}
        InitDataDisplay.jddFullFrame.zMax.setText(s);
        s = String.valueOf(newMin);
        epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
        else if (s.length() > 6 && s.indexOf(".") != -1) {
           s = s.substring(0,s.indexOf(".")+2);
        }
        InitDataDisplay.jddFullFrame.zMin.setText(s);
        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();

    }

//-------------------------------------------------------------------------------------------------------

    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        if( pixImage == null ) {
	    g.setColor( Color.lightGray );
	    g.fillRect( 0, 0, width, height );
	    g.setColor( Color.BLACK );
            g.drawString("Buffer  is  Empty", 20, height/2);
        }
	else {
            g.drawImage( pixImage, 0, 0, null );
	    g.setColor( Color.GREEN );
	    g.drawLine( 20, 20, 20, 100);
 	    g.drawLine( 20, 20, 100, 20);
	    g.drawString("S", 15, 120);
	    g.drawString("W", 115, 25);

	    g.drawLine( 100, 20, 90, 10);
	    g.drawLine( 100, 20, 90, 30);
	    g.drawLine( 20, 100, 10, 90);
	    g.drawLine( 20, 100, 30, 90);

	    if( zoomFactor > 0 && showZoom ) {
		// Paint a rectangle with a translucent color
	        int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
	        int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
	        double scale = 2048.0/(Math.min(width, height));
                g.setColor( transLucent );
		int xi = xZoom - (int)(zoomRect/(2*scale));
		int yi = yZoom - (int)(zoomRect/(2*scale));
		g.fillRect( xi, yi, (int)(zoomRect/scale), (int)(zoomRect/scale));
		// Paint a double rectangular outline
                g.setColor( Color.WHITE );
		g.drawRect( xi, yi, (int)(zoomRect/scale), (int)(zoomRect/scale));
		g.setColor( Color.BLACK );
		g.drawRect( xi+1, yi+1, (int)(zoomRect/scale)-2, (int)(zoomRect/scale)-2);
		String x = getToolTipText();
		if (x != null && !x.equals("") && drawCoords) {
		   x = x.substring(0, x.indexOf("; w"));
		   g.drawString(x, xi+8, yZoom);
		}
	    }
	    for (int j = 0; j < 4; j++) {
                g.setColor(dragObjectColor[j]);
		if (dragObjectStatus[j].equals("V Line")) {
		   g.drawLine(dragObjectX[j], 0, dragObjectX[j], height);
		} else if (dragObjectStatus[j].equals("H Line")) {
		   g.drawLine(0, dragObjectY[j], width, dragObjectY[j]);
		} else if (dragObjectStatus[j].equals("+")) {
		    int xc = dragObjectX[j];
		    int yc = dragObjectY[j];
		    g.drawLine(xc,yc-20,xc,yc+20);
		    g.drawLine(xc-20,yc,xc+20,yc);
		    g.setColor(Color.BLACK);
		    g.drawLine(xc-1,yc-20,xc-1,yc+20);
		    g.drawLine(xc+1,yc-20,xc+1,yc+20);
		    g.drawLine(xc-20,yc-1,xc+20,yc-1);
		    g.drawLine(xc-20,yc+1,xc+20,yc+1);
		    g.setColor(dragObjectColor[j]);
		} else if (dragObjectStatus[j].equals("X")) {
                    int xc = dragObjectX[j];
                    int yc = dragObjectY[j];
                    g.drawLine(xc-20,yc-20,xc+20,yc+20);
                    g.drawLine(xc+20,yc-20,xc-20,yc+20);
                    g.setColor(Color.BLACK);
                    g.drawLine(xc-21,yc-20,xc+19,yc+20);
                    g.drawLine(xc-19,yc-20,xc+21,yc+20);
                    g.drawLine(xc+21,yc-20,xc-19,yc+20);
                    g.drawLine(xc+19,yc-20,xc-21,yc+20);
                    g.setColor(dragObjectColor[j]);
		} else if (dragObjectStatus[j].equals("Circle")) {
		    g.drawOval(dragObjectX[j]-10, dragObjectY[j]-10, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawOval(dragObjectX[j]-11, dragObjectY[j]-11, 22, 22);
                    g.drawOval(dragObjectX[j]-9, dragObjectY[j]-9, 18, 18);
                    g.setColor(dragObjectColor[j]);
		}
	    }
	    if( showRuler ) {
	        int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
	        int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
                g.setColor( transLucent2 );
		int x1 = xRulerInit;
		int y1 = yRulerInit;
		int x2 = xRulerFin;
		int y2 = yRulerInit;
		int x3 = xRulerFin;
		int y3 = yRulerFin;
		
		String xEW = new String();
		String yNS = new String();		
		double xShift = (368.64/height)*(x1-x3); // 0.18 arcsec/pix
		double yShift = (368.64/width)*(y1-y3); // 0.18 arcsec/pix
		float xpxShift = (int)(10*(2048.0/width)*(x1-x3))/10; //pixel shift
                float ypxShift = (int)(10*(2048.0/height)*(y1-y3))/10; //pixel shift
		if ( xShift < 0 ) xEW = "W"; 
		   else xEW = "E";
		if ( yShift < 0 ) yNS = "S"; 
		   else yNS = "N";
		   
		float xShiftLabel = (int)(10*Math.abs(xShift))/10;
		float yShiftLabel = (int)(10*Math.abs(yShift))/10;
		
		String ylabel = new String(" (" + ypxShift+" px; "+ yShiftLabel + "'' " + yNS + ","); 
		String xlabel = new String(" " + xpxShift+" px; "+ xShiftLabel + "'' " + xEW + ")" );
		int[] xPoints = {x1, x2, x3};
		int[] yPoints = {y1, y2, y3};
		g.fillPolygon( xPoints, yPoints, 3 );
                g.setColor( Color.WHITE );
		g.drawPolygon( xPoints, yPoints, 3 );
		g.drawString(ylabel, x3, y3);
		g.drawString(xlabel, x3, y3+12);
	    }
	    if( showMOS && mosMasks != null) {
	        g.setColor( Color.ORANGE );
		for (int i=0; i < mosMasks.length; i++) {
		   if (mosMasks[i] != null) {
		      mosMasks[i].setScale((float)imageBuffer1k.width/this.width,(float)imageBuffer1k.height/this.height, (float)this.height);
		      mosMasks[i].drawMOSMask(g);
		   }
		}
	    }
        }
    }
//-------------------------------------------------------------------------------------------------------
    
    private void ZoomEvent( java.awt.event.MouseEvent mev ) {   

	if( !showZoom )
	{
	    for( int i=0; i < imagePanels.length; i++ ) {
	    	if( imagePanels[i].imageDisplay.showZoom ) {
	    	    imagePanels[i].imageDisplay.showZoom = false;
	    	    imagePanels[i].imageDisplay.repaint();
	    	}
	    }
	    showZoom = true;
	}
        xZoom = mev.getX();
        yZoom = mev.getY();
	ZoomImage();
        repaint();
    }
//-------------------------------------------------------------------------------------------------------
    
    public void ZoomImage() {

        if( imageBuffer == null ) return;
        if( imageBuffer1k == null ) return;
	
	int width = InitDataDisplay.jddFullFrame.imPanelsHolder.getWidth();
	int height = InitDataDisplay.jddFullFrame.imPanelsHolder.getHeight();
	double scale = 2048.0/(Math.min(width, height));

        if( xZoom < (int)(zoomRect/(2*scale)) ) xZoom = (int)(zoomRect/(2*scale));
        if( yZoom < (int)(zoomRect/(2*scale)) ) yZoom = (int)(zoomRect/(2*scale));
        if( (imageBuffer1k.width - xZoom) < (int)(zoomRect/(2*scale)) )  xZoom = imageBuffer1k.width - (int)(zoomRect/(2*scale)) ;
        if( (imageBuffer1k.height - yZoom) < (int)(zoomRect/(2*scale)) ) yZoom = imageBuffer1k.height - (int)(zoomRect/(2*scale)) ;
        
        zoomPanel.zoomImage.updateImage( imageBuffer.zoom( (int)(scale*xZoom), (int)(scale*yZoom), zoomRect, zoomFactor ), colorModel );
    }
//-------------------------------------------------------------------------------------------------------

    public void updateZoomImage(int dx, int dy) {
        if( imageBuffer == null ) return;
        if( imageBuffer1k == null ) return;
	double scale = 2048./Math.min(imageBuffer1k.width, imageBuffer1k.height);
	xZoom+=(int)Math.floor(dx/scale);
	yZoom+=(int)Math.floor(dy/scale);
	ZoomImage();
	repaint();
    }
//-------------------------------------------------------------------------------------------------------

    public void setZoomImage() {
	if (showZoom == false) {
	    xZoom = imageBuffer1k.width/2;
	    yZoom = imageBuffer1k.height/2;
	}
	showZoom = true;
	ZoomImage();
    }

//-------------------------------------------------------------------------------------------------------
    
    public void sendToBgd(ImageBuffer imgBuff) {

    bgdBuffer = imgBuff;
    bgdPreviewBuffer=srcPreviewBuffer;

    if (InitDataDisplay.jddtype.equals("NIRjdd"))
	InitDataDisplay.jddFullFrame.bgdFileName.setNewState(InitDataDisplay.obsMonitor.archiveFile().getNewText());
    
    String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
    float min = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
    float max = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
     
    if (bgdPreviewBuffer != null) {
	int width = InitDataDisplay.jddFullFrame.bgdPreviewImage.getWidth();
        int height = InitDataDisplay.jddFullFrame.bgdPreviewImage.getHeight();
        int size = Math.min(width, height);

	InitDataDisplay.jddFullFrame.bgdPreviewImage.updateImage( bgdPreviewBuffer, colorModel );
        InitDataDisplay.jddFullFrame.bgdPreviewImage.updateSize(size);
      // if (zModeBoxSel.indexOf("Auto") >= 0) {
          InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale();
      // } else if (zModeBoxSel.indexOf("Manual") >= 0) { 
      //    InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max);
      // }
    }
    System.out.println("ufjdd.sendToBgd> Current image sent to bgd buffer");

    }

    public void updateBackgroundPreview(ImageBuffer bgdBuff) {
	int width = InitDataDisplay.jddFullFrame.bgdPreviewImage.getWidth();
	int height = InitDataDisplay.jddFullFrame.bgdPreviewImage.getHeight();
	int size = Math.min(width, height);
	bgdPreviewBuffer = bgdBuff.rescale(size);
	InitDataDisplay.jddFullFrame.bgdPreviewImage.updateImage( bgdPreviewBuffer, colorModel );
	InitDataDisplay.jddFullFrame.bgdPreviewImage.updateSize(size);
	InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(); 
    }

    public void keyPressed(KeyEvent kev) { 
	ZoomEvent(kev);
    }
    public void keyTyped(KeyEvent kev) {}
    public void keyReleased(KeyEvent kev) {}

    public void focusGained(FocusEvent fev) {
	if( showZoom ) requestFocus(); }
    public void focusLost(FocusEvent fev) {}

    private void ZoomEvent(KeyEvent kev) {
	if (!showZoom) return;
	switch (kev.getKeyCode()) {
            case KeyEvent.VK_LEFT:  --xZoom; break;
            case KeyEvent.VK_RIGHT: ++xZoom; break;
            case KeyEvent.VK_UP:    --yZoom; break;
            case KeyEvent.VK_DOWN:  ++yZoom; break;
            case KeyEvent.VK_KP_LEFT:  --xZoom; break;
            case KeyEvent.VK_KP_RIGHT: ++xZoom; break;
            case KeyEvent.VK_KP_UP:    --yZoom; break;
            case KeyEvent.VK_KP_DOWN:  ++yZoom; break;
            default: return;
	}
	ZoomImage();
	repaint();
    }

    public void updateZoomImage(String mode) {
	if (mode.equals("Auto") || mode.equals("Linear")) {
	    if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
		float max = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
                float min = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
                zoomPanel.zoomImage.applyLinearScale(min, max);
            }
	}
	else if (mode.equals("Power")) {
	    if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
		double thresh = Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText());
		double power = Double.parseDouble(InitDataDisplay.jddFullFrame.zMin.getText());
		zoomPanel.zoomImage.applyPowerScale(thresh, power);
	    }
	}
	else if (mode.equals("Log")) {
	    if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Tied")) {
		double thresh = Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText());
		zoomPanel.zoomImage.applyLogScale(thresh);
	    }
	}
    }

    public ZoomPanel setZoomScale() {
	if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Linear")) {
	   float min = zoomPanel.adjustZoom.newMin;
	   float max = zoomPanel.adjustZoom.newMax;
	   imagePanels[0].imageDisplay.applyLinearScale(min, max);
	} else if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Log")) {
	   double thresh = Double.parseDouble(zoomPanel.adjustZoom.thresholdTextField.getText());
	   imagePanels[0].imageDisplay.applyLogScale(thresh);
	} else if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Power")) {
           double thresh = Double.parseDouble(zoomPanel.adjustZoom.thresholdTextField.getText());
           double power = Double.parseDouble(zoomPanel.adjustZoom.powerTextField.getText());
	   imagePanels[0].imageDisplay.applyPowerScale(thresh, power);
	}
	return zoomPanel;
    }

    public void setZoomBoxColor(int i) {
	if (i == 1) transLucent = new Color(128,255,128,16);
	else if (i == 2) transLucent = new Color(128,255,128,0);
	else transLucent = new Color(128,255,128,33);
	repaint();
    }

    public int getZoomBoxColor() {
	if (transLucent == new Color(128,255,128,16)) return 1;
	else if (transLucent == new Color(128,255,128,0)) return 2;
	else return 0;
    }
}
