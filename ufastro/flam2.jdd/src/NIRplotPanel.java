package ufjdd;
/**
 * Title:        NIRplotPanel.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Craig Warner, Antonio Marin-Franch, Frank Varosi
 * Company:      University of Florida
 * Description:  A UFPlotPanel for f2jdd 
 */

import javaUFLib.*;
import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class NIRplotPanel extends UFPlotPanel { 
   JMenuItem printItem, gatorPlotItem;
   GatorPlot gp;
   boolean first, isHist;
   float[] x;
   float[] y;
   float[] xFit;
   float[] yFit;
   boolean overplot = false;
   String s, overs;
   Container theContainer;
   Rectangle bounds, gpBounds;
   int oldxdim, oldydim;

   public NIRplotPanel(int xdim1, int ydim1) {
      this.xdim = xdim1;
      this.ydim = ydim1;
      this.xpos2 = xdim - 20;
      this.ypos2 = ydim - 47;
      this.setBackground(Color.black);
      this.setForeground(Color.white);
      this.setPreferredSize(new Dimension(xdim, ydim));
      first=true;

      setNIRPlotMenu();
      
      addMouseListener(new java.awt.event.MouseListener() {
	public void mouseClicked(java.awt.event.MouseEvent evt) {
        }

        public void mousePressed(java.awt.event.MouseEvent ev) {
           if ((ev.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
              if(ev.isPopupTrigger()) {
		menu.show(ev.getComponent(), ev.getX(), ev.getY());
              }
           }

	   if ((ev.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
	      if (xyFrame != null) {
		xyFrame.xField.setText(""+ev.getX());
		xyFrame.yField.setText(""+ev.getY());
		xyFrame.deviceButton.setSelected(true);
	      }
	   }
	}

        public void mouseReleased(java.awt.event.MouseEvent evt) {
	}

        public void mouseEntered(java.awt.event.MouseEvent evt) {
        }

        public void mouseExited(java.awt.event.MouseEvent evt) {
        }
      });
   }

   public void setNIRPlotMenu() {
      menu = new JPopupMenu();

      printItem = new JMenuItem("Print or Save");
      menu.add(printItem);
      printItem.addActionListener(this);

      gp = new GatorPlot();
      gp.autoSetVisible(false);
      gatorPlotItem = new JMenuItem("Send to GatorPlot");
      menu.add(gatorPlotItem);
      gatorPlotItem.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
	     if (gp == null) {
		gp = new GatorPlot();
		gp.autoSetVisible(false);
	     }
	     gp.addWindowListener(new WindowListener() {
		public void windowActivated(WindowEvent ev) {
		}
		public void windowDeactivated(WindowEvent ev) {
		}
                public void windowDeiconified(WindowEvent ev) {
                }
                public void windowIconified(WindowEvent ev) {
                }
                public void windowOpened(WindowEvent ev) {
                }
                public void windowClosed(WindowEvent ev) {
		   gp.remove(NIRplotPanel.this);
      		   xdim = oldxdim;
      		   ydim = oldydim;
      		   gpBounds = getBounds();
      		   setBounds(bounds);
      		   setNIRPlotMenu();
      		   theContainer.add(NIRplotPanel.this);
      		   repaint();
                }
                public void windowClosing(WindowEvent ev) {
                }
	     });
             theContainer = getParent();
             bounds = getBounds();
	     if (gpBounds != null) setBounds(gpBounds);
             oldxdim = xdim;
             oldydim = ydim;
	     xdim = gp.xdim;
	     ydim = gp.ydim;
             if( ! first ) {
                 gp.setVisible(true);
                 gp.setState( Frame.NORMAL );
             }
             if (first) {
                first = false;
                gp.showPlot(true);
                gp.setState( Frame.NORMAL );
             }
             setGatorPlotMenu();
             gp.addPanel(NIRplotPanel.this);
             gp.resetOPlots();
             if (overplot) {
                if (isHist) {
                   gp.hist(x, s);
                   gp.overplot(xFit, yFit, overs, true);
                } else {
                   gp.plot(x, y, s);
                   gp.overplot(xFit, yFit, overs, true);
                }
                //overplot = false;
             } else {
                gp.plot(x, y, s);
             }
          }
      });
   }

   public void setGatorPlotMenu() {
      menu = new JPopupMenu();
      exportItem = new JMenuItem("Export as PNG/JPEG");
      menu.add(exportItem);
      exportItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           JFileChooser jfc = new JFileChooser(saveDir);
           int returnVal = jfc.showSaveDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
              String filename = jfc.getSelectedFile().getAbsolutePath();
              saveDir = jfc.getCurrentDirectory();
              File f = new File(filename);
              if (f.exists()) {
                String[] saveOptions = {"Overwrite","Cancel"};
                int n = JOptionPane.showOptionDialog(NIRplotPanel.this, filename+" already exists.", "File exists!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, saveOptions, saveOptions[1]);
                if (n == 1) {
                   return;
                }
              }
              String format = "png";
              if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg")) format = "jpeg";
              NIRplotPanel upp = NIRplotPanel.this;
              try {
                BufferedImage image = new BufferedImage(upp.xdim, upp.ydim, BufferedImage.TYPE_INT_BGR);
                image.createGraphics().drawImage(offscreenImg,0,0,upp.xdim,upp.ydim,upp);
                ImageIO.write(image, format, f);
              } catch(IOException e) {
                System.err.println("UFPlotPanel error > could not create JPEG image!");
              }
           }
        }
      });
      printItem = new JMenuItem("Print or Save");
      menu.add(printItem);
      printItem.addActionListener(this);
      resetSizeItem = new JMenuItem("Reset Size");
      menu.add(resetSizeItem);
      resetSizeItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           resizePlot(ufpxdim, ufpydim);
           if (gp.getClass().getName().indexOf("GatorPlot") != -1) {
              if (gp.showPanels)
                gp.setSize(ufpxdim + 164, ufpydim + 154);
              else gp.setSize(ufpxdim + 4, ufpydim + 26);
           }
           else gp.setSize(ufpxdim + 4, ufpydim + 26);
        }
      });
      if (gp.getClass().getName().indexOf("GatorPlot") != -1) {
        //final GatorPlot gp = (GatorPlot)ufp;
        resetRangeItem = new JMenuItem("Reset Range");
        resetRangeItem.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              gp.xMinField.setText("");
              gp.yMinField.setText("");
              gp.xMaxField.setText("");
              gp.yMaxField.setText("");
              gp.plotButton.doClick();
           }
        });
        menu.add(resetRangeItem);
        if (gp.showPanels) gpPanelItem = new JMenuItem("Hide Option Panels");
        else gpPanelItem = new JMenuItem("Show Option Panels");
        gpPanelItem.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              if (gp.showPanels) {
                gp.showPanels = false;
                gpPanelItem.setText("Show Option Panels");
                gp.remove(gp.leftPanel);
                gp.remove(gp.bottomPanel);
                gp.setSize(ufpxdim + 4, ufpydim + 26);
                gp.validate();
                gp.repaint();
              } else {
                gp.showPanels = true;
                gpPanelItem.setText("Hide Option Panels");
                gp.getContentPane().add(gp.leftPanel, BorderLayout.WEST);
                gp.getContentPane().add(gp.bottomPanel, BorderLayout.SOUTH);
                gp.setSize(ufpxdim + 164, ufpydim + 154);
                gp.validate();
                gp.repaint();
              }
           }
        });
        menu.add(gpPanelItem);
      }

      quitItem = new JMenuItem("Quit");
      menu.add(quitItem);
      quitItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           gp.dispose();
        }
      });

   }

   public void initPlot() {
      offscreenImg = createImage(xdim,ydim);
      if( offscreenImg == null ) {
	  offscreenImg = createImage(xdim,ydim);
      }
      offscreenG = (Graphics2D)offscreenImg.getGraphics();
      offscreenG.setColor(backColor);
      offscreenG.fillRect(0,0,xdim,ydim);
      offscreenG.setColor(plotColor);
      offscreenG.setFont(mainFont);
   }

   public void plot(float[] x, float[] y, String s) {
      doHist = false;
      isHist = false;
      overplot = false;
      this.x=x;
      this.y=y;
      this.s=s;
      super.plot(x,y,s);
   }

   public void plotCont(float[] x, float[] y, String s) {
      doHist = false;
      this.drawAxes(x, y, s);
      this.x=x;
      this.y=y;
      this.s=s;
      multi[0]++;
      if (multi[0] >= multi[1]*multi[2]) multi[0] = 0;
      if (!noData) this.makePlot(x, y);
   }

   public void overplot(float[] x, float[] y, String s) {
      overplot=true;
      this.xFit=x;
      this.yFit=y;
      this.overs = s;
      super.overplot(x,y,s);
   }

   public float[] hist(float[] x, String s) {
      overplot = false;
      doHist = true;
      isHist = true;
      this.x=x;
      this.s=s;
      return super.hist(x,s);
   }

   public void calcZoom() {
      if (sxinit==0 || syinit==0 || sxfin==0 || syfin==0 ) return;
      float x1 = (Math.min( sxinit, sxfin )-xoff)/xscale;
      float y1 = (Math.min( yoff-syinit, yoff-syfin ))/yscale;
      float x2 = (Math.max( sxinit, sxfin )-xoff)/xscale;
      float y2 = (Math.max( yoff-syinit, yoff-syfin ))/yscale;
      x1 = (float)(Math.floor(x1*1000)*.001);
      x2 = (float)(Math.floor(x2*1000)*.001);
      y1 = (float)(Math.floor(y1*1000)*.001);
      y2 = (float)(Math.floor(y2*1000)*.001);
      if ((""+x1).equals("NaN") || (""+y1).equals("NaN") || (""+x2).equals("NaN") || (""+y2).equals("NaN")) return;
      //String s = "*xrange=["+(int)x1+","+(int)x2+"], *yrange=["+(int)y1+","+(int)y2+"],"+plotOpts;
      if (gp != null) {
        gp.xMinField.setText(""+x1);
        gp.xMaxField.setText(""+x2);
        gp.yMinField.setText(""+y1);
        gp.yMaxField.setText(""+y2);
        gp.plotButton.doClick();
      }
    }

}
