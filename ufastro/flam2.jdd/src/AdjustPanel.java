package ufjdd;
/**
 * Title:       AdjustPanel.java
 * Version:     (see rcsID)
 * Authors:     Ziad Saleh, Frank Varosi, and Craig Warner
 * Created:     March 19, 2004, 12:49 AM
 * Company:     University of Florida
 * Description: Object for adjusting the display scaling of image in an ImagePanel object.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import javaUFLib.*;

public class AdjustPanel extends JPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: AdjustPanel.java,v 1.5 2006/02/17 22:53:27 warner Exp $";

    private ImageDisplayPanel imgDisPan;
    protected String className = this.getClass().getName();
    int CoAdds = 1;
    double scaleFactor = 1.0;
    boolean useScaleFactor = false;
    public boolean setNewMin = false, setNewMax = false;
    public float newMin, newMax;
    float fmin, fmax;
    int imin, imax;
    String scaleMode = "Linear";
    UFColorButton origMinButton, origMaxButton, newMinButton, newMaxButton;
    public JLabel minValLabel;
    public JLabel maxValLabel;
    JTextField newMinTextField;
    JTextField newMaxTextField;
    ButtonModel buLinModel;
    JTextField thresholdTextField, powerTextField;
    ButtonGroup scalingModeBuGrp;
    Color origButtonColor;
    
    public AdjustPanel(ImageDisplayPanel imgDisPan)
    {
        this.imgDisPan = imgDisPan;
        
        origMinButton = new UFColorButton("Min *");
	origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
        origMinButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        origMaxButton = new UFColorButton("Max *");
	origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
        origMaxButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        minValLabel = new JLabel("", JLabel.LEFT);
        minValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        maxValLabel = new JLabel("", JLabel.LEFT);
        maxValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        newMinTextField = new JTextField();
        newMaxTextField = new JTextField();
        minValLabel.setText("" + 0);
        maxValLabel.setText("" + 0);
        
        newMinButton = new UFColorButton("Fix Min");
        newMaxButton = new UFColorButton("Fix Max");
        
        thresholdTextField = new JTextField("0.1");
        thresholdTextField.setEditable(false);
        powerTextField = new JTextField("0.33");
        powerTextField.setEditable(false);
        
        
        JRadioButton LinScaleModeButton = new JRadioButton("Linear");
	JRadioButton LogScaleModeButton = new JRadioButton("Log");
	JRadioButton PowScaleModeButton = new JRadioButton("Power");
	JRadioButton TiedScaleModeButton = new JRadioButton("Tied");

        scalingModeBuGrp = new ButtonGroup();
        scalingModeBuGrp.add(LinScaleModeButton);
        scalingModeBuGrp.add(LogScaleModeButton);
        scalingModeBuGrp.add(PowScaleModeButton);
	scalingModeBuGrp.add(TiedScaleModeButton);

        RadioListener scalingModeListener = new RadioListener( scalingModeBuGrp );
        LinScaleModeButton.addActionListener(scalingModeListener);
        LogScaleModeButton.addActionListener(scalingModeListener);
        PowScaleModeButton.addActionListener(scalingModeListener);
	TiedScaleModeButton.addActionListener(scalingModeListener);
        
        buLinModel = LinScaleModeButton.getModel();
        scalingModeBuGrp.setSelected(buLinModel, true);
        
        origMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMinButton.setText("Min *");
                newMinButton.setText("Fix Min");
		origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
		newMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                scaleAndDraw();
            }
        });
        
        origMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMaxButton.setText("Max *");
                newMaxButton.setText("Fix Max");
                origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
                newMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                scaleAndDraw();
            }
        });
        
        newMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if( newMinTextField.getText().trim().length() > 0 ) {
                    origMinButton.setText("Min");
                    newMinButton.setText("Fix Min *");
                    origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                    newMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
                    scaleAndDraw();
                }
            }
        });
        
        newMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if( newMaxTextField.getText().trim().length() > 0 ) {
                    origMaxButton.setText("Max");
                    newMaxButton.setText("Fix Max *");
                    origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                    newMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
                    scaleAndDraw();
                }
            }
        });
        
        newMinTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if( c == KeyEvent.VK_ENTER ) {
		    if( newMinTextField.getText().trim().length() > 0 ) {
			origMinButton.setText("Min");
			newMinButton.setText("Fix Min *");
                        origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                        newMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
			scaleAndDraw();
		    }
		}
		else newMinTextField.setBackground( Color.white );
            }
        });
        
        newMaxTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if( c == KeyEvent.VK_ENTER ) {
		    if( newMaxTextField.getText().trim().length() > 0 ) {
			origMaxButton.setText("Max");
			newMaxButton.setText("Fix Max *");
                        origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
                        newMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
			scaleAndDraw();
		    }
                }
		else newMaxTextField.setBackground( Color.white );
            }
        });
        
        thresholdTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                if( e.getKeyChar() == KeyEvent.VK_ENTER ) {
                    scaleAndDraw();
                }
		else thresholdTextField.setBackground( Color.white );
            }
        });
        
        powerTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                 if( e.getKeyChar() == KeyEvent.VK_ENTER ) {
                    scaleAndDraw();
                }
		else powerTextField.setBackground( Color.white );
            }
        });
        
        this.setLayout(new RatioLayout());
        this.add("0.00, 0.0; 0.25, 0.32", origMinButton);
        this.add("0.25, 0.0; 0.25, 0.32", minValLabel);
        this.add("0.50, 0.0; 0.25, 0.32", origMaxButton);
        this.add("0.75, 0.0; 0.25, 0.32", maxValLabel);
        
        this.add("0.00, 0.33; 0.25, 0.32", newMinButton);
        this.add("0.25, 0.33; 0.25, 0.32", newMinTextField);
        this.add("0.50, 0.33; 0.25, 0.32", newMaxButton);
        this.add("0.75, 0.33; 0.25, 0.32", newMaxTextField);
        
        this.add("0.00, 0.67; 0.20, 0.32", LinScaleModeButton);
        this.add("0.20, 0.67; 0.12, 0.32", LogScaleModeButton);
        this.add("0.32, 0.67; 0.05, 0.32", new JLabel(">"));
        this.add("0.37, 0.67; 0.10, 0.32", thresholdTextField);
        this.add("0.50, 0.67; 0.18, 0.32", PowScaleModeButton);
        this.add("0.68, 0.67; 0.04, 0.32", new JLabel("="));
        this.add("0.72, 0.67; 0.10, 0.32", powerTextField);
	this.add("0.85, 0.67; 0.15; 0.32", TiedScaleModeButton);
        
        setBorder(BorderFactory.createTitledBorder(""));
    }

    public void updateMinMax() {
	if( imgDisPan.imageBuffer == null ) return;
        this.CoAdds = imgDisPan.imageBuffer.CoAdds;
        this.scaleFactor = imgDisPan.imageBuffer.scaleFactor;
	if( CoAdds <= 0 ) CoAdds = 1;
	if( scaleFactor <= 0 ) scaleFactor = 1;
	this.imin = imgDisPan.imageBuffer.min;
	this.imax = imgDisPan.imageBuffer.max;
	this.fmin = imgDisPan.imageBuffer.s_min;
	this.fmax = imgDisPan.imageBuffer.s_max;
	displayMinMax();
    }

    public void displayMinMax()
    {
	if( useScaleFactor ) {
	    minValLabel.setText( UFLabel.truncFormat( fmin * scaleFactor ) );
	    maxValLabel.setText( UFLabel.truncFormat( fmax * scaleFactor ) );
	}
	else {
	    minValLabel.setText( Integer.toString( imin ) );
	    maxValLabel.setText( Integer.toString( imax ) );
	}
    }

    void useScaleFactor( boolean useit ) { useScaleFactor = useit; }

    public void scaleAndDraw( String scaleMode )
    {
	this.scaleMode = scaleMode;
	scaleAndDraw();
    }

    public void scaleAndDraw()
    {
	setNewMin = false;
	setNewMax = false;
	newMin = fmin;
	newMax = fmax;

	if( useScaleFactor ) { // min/max values are expressed in per frame time units.
	    newMin *= scaleFactor;
	    newMax *= scaleFactor;
	}

        if( scaleMode.equalsIgnoreCase("Linear") ) {

            if( origMinButton.getText().indexOf("*") > 0 && origMaxButton.getText().indexOf("*") > 0 )
                reDrawImage();
            else {

		if( newMinButton.getText().indexOf("*") > 0 ) {
		    if( newMinTextField.getText().trim().length() == 0 ) {
			origMinButton.setText("Min *");
			newMinButton.setText("Fix Min");
	                origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
        	        newMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
		    }
		    else {
			try{
			    newMin = Float.parseFloat( newMinTextField.getText().trim() );
			    setNewMin = true;
			}
			catch(NumberFormatException nfe) {
			    origMinButton.setText("Min *");
			    newMinButton.setText("Fix Min");
        	            origMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
	                    newMinButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
			    newMinTextField.setBackground( Color.yellow );
			    Toolkit.getDefaultToolkit().beep();
			}
		    }
                }

		if( newMaxButton.getText().indexOf("*") > 0 ) {
		    if( newMaxTextField.getText().trim().length() == 0 ) {
			origMaxButton.setText("Max *");
			newMinButton.setText("Fix Max");
	                origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
        	        newMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
		    }
		    else {
			try{
			    newMax = Float.parseFloat( newMaxTextField.getText().trim() );
			    setNewMax = true;
			}
			catch(NumberFormatException nfe) {
			    origMaxButton.setText("Max *");
			    newMaxButton.setText("Fix Max");
        	            origMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
	                    newMaxButton.updateColorGradient(UFColorButton.COLOR_SCHEME_BLUE);
			    newMaxTextField.setBackground( Color.yellow );
			    Toolkit.getDefaultToolkit().beep();
			}
		    }
		}

                reDrawImage( newMin, newMax );
            }
        }
        else if( scaleMode.equalsIgnoreCase("Power") ) {
	    try {
		double threshHold = Double.parseDouble( thresholdTextField.getText() );
		newMin = (float)threshHold;
		setNewMin = true;
		try {
		    double power = Double.parseDouble( powerTextField.getText() );
		    reDrawImage( threshHold, power );
		}
		catch(NumberFormatException nfe) {
		    powerTextField.setBackground( Color.yellow );
		    Toolkit.getDefaultToolkit().beep();
		}
	    }
	    catch(NumberFormatException nfe) {
		thresholdTextField.setBackground( Color.yellow );
		Toolkit.getDefaultToolkit().beep();
	    }
	}
	else if (scaleMode.equalsIgnoreCase("Tied")) {
	   String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
	   String scaleBoxSel = (String)(InitDataDisplay.jddFullFrame.scaleBox.getSelectedItem());
	   if (zModeBoxSel.indexOf("Auto") >= 0 || scaleBoxSel.indexOf("Linear") >= 0) {
	      newMin = Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText());
	      newMax = Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText());
	      reDrawImage(newMin, newMax);
	   }
	   else if (zModeBoxSel.indexOf("Manual") >= 0 && scaleBoxSel.indexOf("Power") >= 0) {
              double thresh = Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText());
              double power = Double.parseDouble(InitDataDisplay.jddFullFrame.zMin.getText());
	      reDrawImage(thresh, power);
	   }
	   else if (zModeBoxSel.indexOf("Manual") >= 0 && scaleBoxSel.indexOf("Log") >= 0){
	      double thresh = Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText());
	      reDrawImage(thresh);
	   }
	   
	}
        else {
	    try {
		double threshHold = Double.parseDouble( thresholdTextField.getText() );
		newMin = (float)threshHold;
		setNewMin = true;
		reDrawImage( threshHold );
	    }
	    catch(NumberFormatException nfe) {
		thresholdTextField.setBackground( Color.yellow );
		Toolkit.getDefaultToolkit().beep();
	    }
        }
    }

    protected void reDrawImage() { imgDisPan.applyLinearScale(); }

    protected void reDrawImage(float min, float max)
    {
	if( useScaleFactor )
	    imgDisPan.applyLinearScale( (int)Math.round( min * CoAdds ), (int)Math.round( max * CoAdds ) );
	else
	    imgDisPan.applyLinearScale( (int)Math.round( min ), (int)Math.round( max ) );
    }

    protected void reDrawImage(double threshHold)
    {
	if( useScaleFactor )
	    imgDisPan.applyLogScale( threshHold * CoAdds );
	else
	    imgDisPan.applyLogScale( threshHold );
    }

    protected void reDrawImage(double threshHold, double power)
    {
	if( useScaleFactor )
	    imgDisPan.applyPowerScale( threshHold * CoAdds, power );
	else
	    imgDisPan.applyPowerScale( threshHold, power );
    }
//------------------------------------------------------------------------------------------------------------

    class RadioListener implements ActionListener
    {
	ButtonGroup buttonGroup;

	RadioListener( ButtonGroup buttonGroup ) {
	    this.buttonGroup = buttonGroup;
	}

        public void actionPerformed(ActionEvent e) {
           
            String scaleMode = getSelection().getText();

            if( scaleMode.equalsIgnoreCase("Linear") ) {
                thresholdTextField.setEditable(false);
                powerTextField.setEditable(false);
                newMinTextField.setEditable(true);
                newMaxTextField.setEditable(true);
                newMinButton.setEnabled(true);
                newMaxButton.setEnabled(true);
            }
	    else if( scaleMode.equalsIgnoreCase("Power") ) {
                thresholdTextField.setEditable(true);
                powerTextField.setEditable(true);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
                newMinButton.setEnabled(false);
                newMaxButton.setEnabled(false);
	    }
	    else if (scaleMode.equalsIgnoreCase("Tied")) {
                thresholdTextField.setEditable(false);
                powerTextField.setEditable(false);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
                newMinButton.setEnabled(false);
                newMaxButton.setEnabled(false);
	    }
            else {
                thresholdTextField.setEditable(true);
                powerTextField.setEditable(false);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
                newMinButton.setEnabled(false);
                newMaxButton.setEnabled(false);
            }
            
            scaleAndDraw( scaleMode );
        }

	JRadioButton getSelection() {
	    for( Enumeration e = buttonGroup.getElements(); e.hasMoreElements(); )
		{
		    JRadioButton b = (JRadioButton)e.nextElement();
		    if( b.getModel() == buttonGroup.getSelection() ) return b;
		}
	    return null;
	}
    }
}


