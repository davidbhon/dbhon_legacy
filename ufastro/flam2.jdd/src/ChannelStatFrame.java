package ufjdd;
/**
 * Title:        ChannelStatFrame
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Craig Warner
 * Company:      University of Florida
 * Description:  View statistics on individual channels
 */
import javaUFLib.*;
import javaUFProtocol.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class ChannelStatFrame extends JFrame {

   int[][] data;
   UFPlotPanel plotPanel;
   JLabel lChan, lMin, lMax, lMean, lSdev, lWhereMin, lWhereMax;
   JLabel[] channels, mins, maxs, means, sdevs, whereMins, whereMaxs;
   JButton[] statButtons;

   public ChannelStatFrame(int[] pixels, UFPlotPanel plot) {
      super("Channel Statistics");
      this.plotPanel = plot;
      data = new int[2048][2048];
      for (int j = 0; j < 2048; j++) {
	for (int l = 0; l < 2048; l++) {
	   data[j][l] = pixels[j*2048+l];
	}
      }
      Container content = getContentPane();
      SpringLayout layout = new SpringLayout();
      content.setLayout(layout);
      lChan = new JLabel("Channel #");
      content.add(lChan);
      layout.putConstraint(SpringLayout.WEST, lChan, 10, SpringLayout.WEST, content);
      layout.putConstraint(SpringLayout.NORTH, lChan, 10, SpringLayout.NORTH, content);
      lMin = new JLabel("Min");
      content.add(lMin);
      layout.putConstraint(SpringLayout.WEST, lMin, 70, SpringLayout.EAST, lChan);
      layout.putConstraint(SpringLayout.NORTH, lMin, 10, SpringLayout.NORTH, content);
      lWhereMin = new JLabel("Min Location");
      content.add(lWhereMin);
      layout.putConstraint(SpringLayout.WEST, lWhereMin, 70, SpringLayout.EAST, lMin);
      layout.putConstraint(SpringLayout.NORTH, lWhereMin, 10, SpringLayout.NORTH, content);
      lMax = new JLabel("Max");
      content.add(lMax);
      layout.putConstraint(SpringLayout.WEST, lMax, 70, SpringLayout.EAST, lWhereMin);
      layout.putConstraint(SpringLayout.NORTH, lMax, 10, SpringLayout.NORTH, content);
      lWhereMax = new JLabel("Max Location");
      content.add(lWhereMax);
      layout.putConstraint(SpringLayout.WEST, lWhereMax, 70, SpringLayout.EAST, lMax);
      layout.putConstraint(SpringLayout.NORTH, lWhereMax, 10, SpringLayout.NORTH, content);
      lMean = new JLabel("Mean");
      content.add(lMean);
      layout.putConstraint(SpringLayout.WEST, lMean, 70, SpringLayout.EAST, lWhereMax);
      layout.putConstraint(SpringLayout.NORTH, lMean, 10, SpringLayout.NORTH, content);
      lSdev = new JLabel("Std Dev");
      content.add(lSdev);
      layout.putConstraint(SpringLayout.WEST, lSdev, 70, SpringLayout.EAST, lMean);
      layout.putConstraint(SpringLayout.NORTH, lSdev, 10, SpringLayout.NORTH, content);
      channels = new JLabel[17];
      for (int j = 0; j < 16; j++) channels[j] = new JLabel(""+(j+1));
      channels[16] = new JLabel("All");
      mins = new JLabel[17];
      maxs = new JLabel[17];
      whereMins = new JLabel[17];
      whereMaxs = new JLabel[17];
      means = new JLabel[17];
      sdevs = new JLabel[17];
      statButtons = new JButton[17];
      for (int j = 0; j < 17; j++) {
	mins[j] = new JLabel();
        maxs[j] = new JLabel();
        whereMins[j] = new JLabel();
        whereMaxs[j] = new JLabel();
	means[j] = new JLabel();
	sdevs[j] = new JLabel();
	statButtons[j] = new JButton("Histogram of Channel "+(j+1));
        statButtons[j].setActionCommand(""+j);
	statButtons[j].addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      int x = Integer.parseInt(ev.getActionCommand());
	      float[] histData;
	      String title;
	      if (x < 16) {
		title = "Channel "+(x+1);
		histData = new float[128*2048];
		int c = 0;
		for (int j = 0; j < data.length; j++) {
		   for (int l = x*128; l < (x+1)*128; l++) {
		      histData[c] = data[j][l];
		      c++;
		   }
		}
	      } else {
		title = "All Channels";
		histData = new float[2048*2048];
		int c = 0;
		for (int j = 0; j < data.length; j++) {
		   for (int l = 0; l < 2048; l++) {
		      histData[c] = data[j][l];
		      c++;
		   }
		}
	      }
	      float[] histY = plotPanel.hist(histData, "*xtitle=Data Values, *ytitle=Frequency, *title="+title);
	   }
	});
      }
      statButtons[16].setText("Histogram of All");
      content.add(channels[0]);
      layout.putConstraint(SpringLayout.WEST, channels[0], 10, SpringLayout.WEST, content);
      layout.putConstraint(SpringLayout.NORTH, channels[0], 15, SpringLayout.SOUTH, lChan);
      content.add(mins[0]);
      layout.putConstraint(SpringLayout.WEST, mins[0], 70, SpringLayout.EAST, lChan);
      layout.putConstraint(SpringLayout.NORTH, mins[0], 15, SpringLayout.SOUTH, lChan);
      content.add(whereMins[0]);
      layout.putConstraint(SpringLayout.WEST, whereMins[0], 70, SpringLayout.EAST, lMin);
      layout.putConstraint(SpringLayout.NORTH, whereMins[0], 15, SpringLayout.SOUTH, lChan);
      content.add(maxs[0]);
      layout.putConstraint(SpringLayout.WEST, maxs[0], 70, SpringLayout.EAST, lWhereMin);
      layout.putConstraint(SpringLayout.NORTH, maxs[0], 15, SpringLayout.SOUTH, lChan);
      content.add(whereMaxs[0]);
      layout.putConstraint(SpringLayout.WEST, whereMaxs[0], 70, SpringLayout.EAST, lMax);
      layout.putConstraint(SpringLayout.NORTH, whereMaxs[0], 15, SpringLayout.SOUTH, lChan);
      content.add(means[0]);
      layout.putConstraint(SpringLayout.WEST, means[0], 70, SpringLayout.EAST, lWhereMax);
      layout.putConstraint(SpringLayout.NORTH, means[0], 15, SpringLayout.SOUTH, lChan);
      content.add(sdevs[0]);
      layout.putConstraint(SpringLayout.WEST, sdevs[0], 70, SpringLayout.EAST, lMean);
      layout.putConstraint(SpringLayout.NORTH, sdevs[0], 15, SpringLayout.SOUTH, lChan);
      content.add(statButtons[0]);
      layout.putConstraint(SpringLayout.WEST, statButtons[0], 70, SpringLayout.EAST, lSdev);
      layout.putConstraint(SpringLayout.NORTH, statButtons[0], 15, SpringLayout.SOUTH, lChan);
      for (int j = 1; j < 17; j++) {
	content.add(channels[j]);
        layout.putConstraint(SpringLayout.WEST, channels[j], 10, SpringLayout.WEST, content);
        layout.putConstraint(SpringLayout.NORTH, channels[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(mins[j]);
        layout.putConstraint(SpringLayout.WEST, mins[j], 70, SpringLayout.EAST, lChan);
        layout.putConstraint(SpringLayout.NORTH, mins[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(whereMins[j]);
        layout.putConstraint(SpringLayout.WEST, whereMins[j], 70, SpringLayout.EAST, lMin);
        layout.putConstraint(SpringLayout.NORTH, whereMins[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(maxs[j]);
        layout.putConstraint(SpringLayout.WEST, maxs[j], 70, SpringLayout.EAST, lWhereMin);
        layout.putConstraint(SpringLayout.NORTH, maxs[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(whereMaxs[j]);
        layout.putConstraint(SpringLayout.WEST, whereMaxs[j], 70, SpringLayout.EAST, lMax);
        layout.putConstraint(SpringLayout.NORTH, whereMaxs[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(means[j]);
        layout.putConstraint(SpringLayout.WEST, means[j], 70, SpringLayout.EAST, lWhereMax);
        layout.putConstraint(SpringLayout.NORTH, means[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(sdevs[j]);
        layout.putConstraint(SpringLayout.WEST, sdevs[j], 70, SpringLayout.EAST, lMean);
        layout.putConstraint(SpringLayout.NORTH, sdevs[j], 15, SpringLayout.SOUTH, channels[j-1]);
	content.add(statButtons[j]);
        layout.putConstraint(SpringLayout.WEST, statButtons[j], 70, SpringLayout.EAST, lSdev);
        layout.putConstraint(SpringLayout.NORTH, statButtons[j], 15, SpringLayout.SOUTH, channels[j-1]);
      }
      layout.putConstraint(SpringLayout.EAST, content, 10, SpringLayout.EAST, statButtons[15]);
      layout.putConstraint(SpringLayout.SOUTH, content, 10, SpringLayout.SOUTH, statButtons[16]);
      updateStats(data);
      pack();
      setVisible(true);
   }

   public void updateStats(int[] pixels, boolean visible) {
      data = new int[2048][2048];
      for (int j = 0; j < 2048; j++) {
        for (int l = 0; l < 2048; l++) {
           data[j][l] = pixels[j*2048+l];
        }
      }
      updateStats(data);
      if (visible) setVisible(true);
   }

   public void updateStats(int[][] data) {
      int[] b;
      for (int j = 0; j < 16; j++) {
	mins[j].setText(""+UFArrayOps.minValue(data, 0, data.length-1, j*128, j*128+127));
	maxs[j].setText(""+UFArrayOps.maxValue(data, 0, data.length-1, j*128, j*128+127));
	b = UFArrayOps.whereMinValue(data, 0, data.length-1, j*128, j*128+127);
	whereMins[j].setText("X:"+b[0]+"  Y:"+(2047-b[1]));
	b = UFArrayOps.whereMaxValue(data, 0, data.length-1, j*128, j*128+127);
        whereMaxs[j].setText("X:"+b[0]+"  Y:"+(2047-b[1]));
	means[j].setText(""+UFArrayOps.avgValue(data, 0, data.length-1, j*128, j*128+127));
	sdevs[j].setText(""+UFArrayOps.stddev(data, 0, data.length-1, j*128, j*128+127));
      }
      mins[16].setText(""+UFArrayOps.minValue(data));
      maxs[16].setText(""+UFArrayOps.maxValue(data));
      b = UFArrayOps.whereMinValue(data);
      whereMins[16].setText("X:"+b[0]+"  Y:"+(2047-b[1]));
      b = UFArrayOps.whereMaxValue(data);
      whereMaxs[16].setText("X:"+b[0]+"  Y:"+(2047-b[1]));
      means[16].setText(""+UFArrayOps.avgValue(data));
      sdevs[16].setText(""+UFArrayOps.stddev(data));
   }

}
