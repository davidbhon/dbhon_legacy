package ufjdd;
/**
 * Title:        AperturePanel.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner
 * Company:      University of Florida
 * Description:  For plotting histogram of aperture statistics 
 */
import javaUFLib.*;
import java.awt.*;

public class AperturePanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: AperturePanel.java,v 1.4 2006/11/01 20:31:31 warner Exp $";

    boolean first;
    NIRplotPanel plotPanel;

    public AperturePanel(NIRplotPanel plotPanel)
    {
        this.plotPanel = plotPanel;
	first = true;
    }

    public void show()
    {
        if( ! first ) {
	    plotPanel.setVisible(true);
	}
    }

    public void update(double[][] data, float xcen, float ycen, float xrad, float yrad, float bsize, float isize, double scaleFac, int Ndifs, String name)
    {
	float[] sourceData = new float[(int)(xrad*yrad*4)];
	float oxrad = xrad+bsize;
	float oyrad = yrad+bsize;
	float ixrad = xrad+isize;
	float iyrad = yrad+isize;
	int xmin = Math.max(0, (int)(xcen-oxrad));
	float xmax = Math.min(data.length-1, xcen+oxrad);
        int ymin = Math.max(0, (int)(ycen-oyrad));
        float ymax = Math.min(data[0].length-1, ycen+oyrad);
	int n = 0, nbg = 0;
	double z, zi, zo;
	float totalbg = 0;
	for (int j = xmin; j < xmax; j++) {
	   for (int l = ymin; l < ymax; l++) {
	      z = Math.pow((j-xcen),2)/Math.pow(xrad,2)+Math.pow((l-ycen),2)/Math.pow(yrad,2);
	      if (z <= 1) {
		sourceData[n++] = (float)(data[l][j]*scaleFac);
	      } else {
		zo = Math.pow((j-xcen),2)/Math.pow(oxrad,2)+Math.pow((l-ycen),2)/Math.pow(oyrad,2);
		zi = Math.pow((j-xcen),2)/Math.pow(ixrad,2)+Math.pow((l-ycen),2)/Math.pow(iyrad,2);
		if (zo <= 1 && zi > 1) {
		   totalbg+=(float)(data[l][j]*scaleFac);
		   nbg++;
		}
	      }
	   }
	}
	float meanbg = totalbg/nbg;
	if (n == 0) return;
	plotHistogram( UFArrayOps.extractValues(sourceData,0,n-1), scaleFac, Ndifs, name, meanbg, xrad, yrad, bsize-isize);
    }

    public void update( double[][] data, float xcen, float ycen, float xrad, float yrad, float bsize, float isize, float min, float max, double scaleFac, int Ndifs, String name )
    {
        float[] sourceData = new float[(int)(xrad*yrad*4)];
        float oxrad = xrad+bsize;
        float oyrad = yrad+bsize;
        float ixrad = xrad+isize;
        float iyrad = yrad+isize;
        int xmin = Math.max(0, (int)(xcen-oxrad));
        float xmax = Math.min(data.length-1, xcen+oxrad);
        int ymin = Math.max(0, (int)(ycen-oyrad));
        float ymax = Math.min(data[0].length-1, ycen+oyrad);
        int n = 0, nbg = 0;
        double z, zi, zo;
        float totalbg = 0;
        for (int j = xmin; j < xmax; j++) {
           for (int l = ymin; l < ymax; l++) {
              z = Math.pow((j-xcen),2)/Math.pow(xrad,2)+Math.pow((l-ycen),2)/Math.pow(yrad,2);
              if (z <= 1) {
		float tempData = (float)(data[l][j]*scaleFac);
		if (tempData >= min && tempData <= max) sourceData[n++] = tempData;
              } else {
                zo = Math.pow((j-xcen),2)/Math.pow(oxrad,2)+Math.pow((l-ycen),2)/Math.pow(oyrad,2);
                zi = Math.pow((j-xcen),2)/Math.pow(ixrad,2)+Math.pow((l-ycen),2)/Math.pow(iyrad,2);
                if (zo <= 1 && zi > 1) {
		   float tempData = (float)(data[l][j]*scaleFac);
		   if (tempData >= min && tempData <= max) {
		      totalbg+=tempData;
		      nbg++;
		   }
                }
              }
           }
        }
        float meanbg = totalbg/nbg;
        if (n == 0) return;
        plotHistogram( UFArrayOps.extractValues(sourceData,0,n-1), scaleFac, Ndifs, name, meanbg, xrad, yrad, bsize-isize);
    }

    private void plotHistogram( float[] data, double scaleFac, int Ndifs, String name, float meanbg, float xrad, float yrad, float asize)
    {
        double mean = UFArrayOps.avgValue( data );
	double stddev = UFArrayOps.stddev( data );
	if( Ndifs < 1 ) Ndifs = 1;
	double stdevFT = stddev/Math.sqrt( scaleFac * Ndifs );
	int npts = data.length;
	int nbins = 10*(int)(Math.floor( Math.sqrt( npts )/42 ) + 1 );
	float[] histY = plotPanel.hist( data, "*nbins=" + nbins + ", *xtitle=Data Values, *ytitle=Frequency, *title=" + name + "  :  Npts = " + npts);
	plotPanel.xyouts(0.15f,0.11f,"Mean = " + UFLabel.truncFormat(mean)+"  :  Std. Dev. = " + UFLabel.truncFormat( stddev ) +"  :  Std. Dev. / Frame = " + UFLabel.truncFormat( stdevFT ), "*charsize=12,*normal");
	float max = (float)UFArrayOps.maxValue(data);
	float min = (float)UFArrayOps.minValue(data);
        //System.out.println("AperturePanel.plotHistogram> min=" + min + ", max=" + max);
	float rng = max - min;
	int npg = 200;
	float[] x = new float[npg];
	float[] y = new float[npg];
	float nplot = (float)npg;
	for (int j = 0; j < npg; j++) x[j] = min + j*rng/nplot;
	float[] z = UFArrayOps.divArrays( UFArrayOps.subArrays( x, (float)mean ), (float)stddev );
	float ymax = (float)UFArrayOps.maxValue( histY ); 
	for (int j = 0; j < npg; j++) y[j] = ymax * (float)Math.exp( -z[j]*z[j]/2 );
	for (int j = 0; j < npg; j++) x[j] = j*nbins/nplot;
	plotPanel.overplot(x, y, ""); 
	float temp = (meanbg-min)*nbins/(max-min);
	float[] bgx = {temp, temp};
	float[] bgy = {0, 2*(float)npts};
	plotPanel.overplot(bgx,bgy,"*color=144,172,0");
	plotPanel.xyouts(0.15f,0.15f,"Xr = "+UFLabel.truncFormat(xrad)+" : Yr = "+UFLabel.truncFormat(yrad)+" : Annulus = "+UFLabel.truncFormat(asize), "*charsize=12,*normal");
	plotPanel.xyouts(0.15f,0.19f,"Background = "+UFLabel.truncFormat(meanbg), "*charsize=12,*normal,*color=144,172,0");
    }
}
