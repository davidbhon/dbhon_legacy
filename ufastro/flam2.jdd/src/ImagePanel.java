package ufjdd;
/**
 * Title:        Java Data Display (JDD): ImagePanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For single image display of contents of a frame buffer from NIR Acq. Server
 */
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

public class ImagePanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ImagePanel.java,v 1.4 2005/11/29 21:08:10 drashkin Exp $";

    //public ControlPanel controlPanel;
    public ImageDisplayPanel imageDisplay;
    public AdjustPanel adjustPanel;

    public ImagePanel(ImagePanel[] imgPanels, IndexColorModel colorModel,
		      DataAccessPanel dataAccess, ZoomPanel zoomPanel)
    {
	imageDisplay = new ImageDisplayPanel( imgPanels, colorModel, dataAccess.frameConfig, zoomPanel );
        adjustPanel = new AdjustPanel(imageDisplay);
	//controlPanel = new ControlPanel(imageDisplay, adjustPanel, dataAccess);

	this.setLayout(new RatioLayout());
	//this.add("0.0,0.00;1.0,0.03", controlPanel);
	this.add("0.0,0.0;1.0,1.0", imageDisplay);
	//this.add("0.0,0.93;1.0,0.07", adjustPanel);
	//this.setBorder(BorderFactory.createLineBorder(Color.green, 1));
    }
}
