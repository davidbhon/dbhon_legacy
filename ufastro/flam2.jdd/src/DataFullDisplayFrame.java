package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Marin-Franch, Frank Varosi and Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class DataFullDisplayFrame extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: DataFullDisplayFrame.java,v 1.24 2006/07/03 21:45:11 warner Exp $";

    String bufferViewSelect;

    GraphicsConfiguration gc;
    //JScrollPane fullDispl;
    static JPanel imPanelsHolder;
    static JTextField zMax = new JTextField("0");
    static JTextField zMin = new JTextField("0");
    static JComboBox scaleBox;
    static UFColorCombo zModeBox;
    static previewImage srcPreviewImage;
    static previewImage bgdPreviewImage;
    static UFTextField bgdFileName = new UFTextField("bgdFileName", "none");
    static UFTextField srcFileName = new UFTextField("srcFileName", "none");
    final JComboBox bufferView;
    final ImagePanel[] imagePanels;
    protected final JLabel pixelDataVal = new JLabel("x=0, y=0, Data = 0");
    
    public DataFullDisplayFrame(GraphicsConfiguration gc, IndexColorModel colorModel, 
				final ImagePanel[] imagePanels,
				DataAccessPanel fullDisplPanel, String args[])
    {
        super(gc);
	this.setTitle("Full Display");
        this.setSize( 1280, 1024 );	
        this.setLocation( 0, 0 );
	this.setUndecorated(false);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.imagePanels = imagePanels;

	bufferViewSelect = "Src Buffer";

	srcPreviewImage = new previewImage( 256, colorModel);
	bgdPreviewImage = new previewImage( 256, colorModel);
	
	//DataFullDisplayFrame components:
	fullDisplPanel.setLayout(new RatioLayout());
	
	imPanelsHolder = new JPanel();
	imPanelsHolder.setLayout(new RatioLayout());
	imPanelsHolder.add( "0.0,0.0;1.0,1.0", imagePanels[0] );

	imPanelsHolder.setBorder(BorderFactory.createLineBorder(Color.green, 1));
	
        final JLabel zMaxLabel = new JLabel();
        final JLabel zMinLabel = new JLabel();
        zMaxLabel.setText("Z max");
        zMinLabel.setText("Z min");

	String zMode[] = {"Auto","Manual","Zoom Box","Zscale","Zmax","Zmin"}; 
	//zModeBox = new JComboBox(zMode);
	zModeBox = new UFColorCombo(zMode, UFColorCombo.COLOR_SCHEME_LIGHT_GRAY);
	zModeBox.setMaximumRowCount(zMode.length);
	//zModeBox.setBackground(Color.lightGray);
	  
	zMax.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        zMax.setEditable(false);
	zMin.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        zMin.setEditable(false);
	
	String scaleValues[] = {"Linear","Log","Power"}; 
	scaleBox = new JComboBox(scaleValues);
	scaleBox.setMaximumRowCount(scaleValues.length);
	scaleBox.setSelectedItem("Linear");
	scaleBox.setVisible(false);

	final UFColorButton applyZ = new UFColorButton("Apply");
	applyZ.setVisible(false);
	applyZ.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
	applyZ.setForeground(Color.blue);
        applyZ.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String zModeBoxSelect = (String)zModeBox.getSelectedItem();
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(zModeBoxSelect.indexOf("Manual") >= 0) {
	      if (scaleBoxSelect.indexOf("Linear") >= 0) {
	        imagePanels[0].imageDisplay.applyLinearScale(
	                                   Float.parseFloat(zMin.getText()), 
	   		  	           Float.parseFloat(zMax.getText())) ;
	        imagePanels[0].imageDisplay.doLinear=true;
	        imagePanels[0].imageDisplay.doLog   =false;
	        imagePanels[0].imageDisplay.doPower =false;
                imagePanels[0].imageDisplay.updateZoomImage("Linear");
                srcPreviewImage.applyLinearScale(
                                           Double.parseDouble(zMin.getText()),
                                           Double.parseDouble(zMax.getText()));
                if (bgdPreviewImage != null) bgdPreviewImage.applyLinearScale(
                                           Double.parseDouble(zMin.getText()),
                                           Double.parseDouble(zMax.getText()));
	      } else if (scaleBoxSelect.indexOf("Log") >= 0) {
	        imagePanels[0].imageDisplay.applyLogScale(
	                                   Double.parseDouble(zMax.getText())) ;
	      
	        imagePanels[0].imageDisplay.doLinear=false;
	        imagePanels[0].imageDisplay.doLog   =true;
	        imagePanels[0].imageDisplay.doPower =false;
		imagePanels[0].imageDisplay.updateZoomImage("Log");
                srcPreviewImage.applyLogScale(
                                           Double.parseDouble(zMax.getText())) ;
                if (bgdPreviewImage != null) bgdPreviewImage.applyLogScale(
                                           Double.parseDouble(zMax.getText())) ;
	      } else if (scaleBoxSelect.indexOf("Power") >= 0) {
	        imagePanels[0].imageDisplay.applyPowerScale(
	                                   Double.parseDouble(zMax.getText()), 
	   		  	           Double.parseDouble(zMin.getText())) ;
	        imagePanels[0].imageDisplay.doLinear=false;
	        imagePanels[0].imageDisplay.doLog   =false;
	        imagePanels[0].imageDisplay.doPower =true;
                imagePanels[0].imageDisplay.updateZoomImage("Power");
		srcPreviewImage.applyPowerScale(
                                           Double.parseDouble(zMax.getText()),
                                           Double.parseDouble(zMin.getText()));
		if (bgdPreviewImage != null) bgdPreviewImage.applyPowerScale(
                                           Double.parseDouble(zMax.getText()),
                                           Double.parseDouble(zMin.getText()));
	      }
           } else if(zModeBoxSelect.indexOf("Zoom Box") >= 0) {
	      ZoomPanel tempzp = imagePanels[0].imageDisplay.setZoomScale();
              srcPreviewImage.setZoomScale(tempzp);
              if (bgdPreviewImage != null) bgdPreviewImage.setZoomScale(tempzp);
	   }
	  }
        });

        zModeBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String zModeBoxSelect = (String)zModeBox.getSelectedItem();
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(zModeBoxSelect.indexOf("Manual") >= 0) {
	      if (scaleBoxSelect.indexOf("Log") >= 0) {
	        zMin.setVisible(false);
	        zMinLabel.setVisible(false);
                zMax.setText("10");
              }
	      if (scaleBoxSelect.indexOf("Power") >= 0) {
                zMax.setText("10");
                zMin.setText("0.1");
             }
	      zMax.setEditable(true);
              zMin.setEditable(true);
	      applyZ.setVisible(true);
	      //zModeBox.setBackground(Color.orange);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_ORANGE);
	      scaleBox.setVisible(true);
           } else if (zModeBoxSelect.indexOf("Auto") >= 0) {
              zMaxLabel.setText("Z max");
              zMinLabel.setText("Z min");
              zMinLabel.setVisible(true);;
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
	      applyZ.setVisible(false);
	      //zModeBox.setBackground(Color.lightGray);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_LIGHT_GRAY);
	      scaleBox.setVisible(false);
	      imagePanels[0].imageDisplay.applyLinearScale( ) ;
	      srcPreviewImage.applyLinearScale( ) ;
	      if (bgdPreviewImage != null) bgdPreviewImage.applyLinearScale( ) ;
	      imagePanels[0].imageDisplay.updateZoomImage("Auto");
	   } else if (zModeBoxSelect.indexOf("Zoom Box") >= 0) {
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(true);
              //zModeBox.setBackground(Color.CYAN);
              zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_CYAN);
	      scaleBox.setVisible(false);
              ZoomPanel tempzp = imagePanels[0].imageDisplay.setZoomScale();
              srcPreviewImage.setZoomScale(tempzp);
              if (bgdPreviewImage != null) bgdPreviewImage.setZoomScale(tempzp);
	   } else if (zModeBoxSelect.indexOf("Zscale") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.GREEN);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_GREEN);
              scaleBox.setVisible(false);
              imagePanels[0].imageDisplay.applyZScale() ;
	      srcPreviewImage.applyZScale();
	      if (bgdPreviewImage != null) bgdPreviewImage.applyZScale();
              imagePanels[0].imageDisplay.updateZoomImage("Auto");
	   } else if (zModeBoxSelect.indexOf("Zmax") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.YELLOW);
              zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_YELLOW);
              scaleBox.setVisible(false);
              imagePanels[0].imageDisplay.applyZScale("Zmax");
              if (bgdPreviewImage != null) bgdPreviewImage.applyZScale("Zmax");
              if (srcPreviewImage != null) srcPreviewImage.applyZScale("Zmax");
              imagePanels[0].imageDisplay.updateZoomImage("Auto");
	   } else if (zModeBoxSelect.indexOf("Zmin") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.MAGENTA);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_MAGENTA);
              scaleBox.setVisible(false);
              imagePanels[0].imageDisplay.applyZScale("Zmin");
              srcPreviewImage.applyZScale("Zmin");
              if (bgdPreviewImage != null) bgdPreviewImage.applyZScale("Zmin");
              imagePanels[0].imageDisplay.updateZoomImage("Auto");
           }
	  }
        });
	
        scaleBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(scaleBoxSelect.indexOf("Linear") >= 0) {
              zMaxLabel.setText("Z max");
              zMinLabel.setText("Z min");
              zMinLabel.setVisible(true);;
              zMin.setVisible(true);
            } else if (scaleBoxSelect.indexOf("Log") >= 0) {
              zMaxLabel.setText("       >");
              zMinLabel.setVisible(false);
              zMax.setText("10");
              zMin.setVisible(false);
            } else if (scaleBoxSelect.indexOf("Power") >= 0) {
              zMaxLabel.setText("       >");
              zMinLabel.setText("Power");
              zMinLabel.setVisible(true);;
              zMax.setText("10");
              zMin.setText("0.1");
              zMin.setVisible(true);
	    }
	  }
        });

	String[] dragObjectOptions = {"Hidden", "H Line", "V Line", "+", "X", "Circle"};
	int[] dragObjectColor = {UFColorCombo.COLOR_SCHEME_RED, UFColorCombo.COLOR_SCHEME_YELLOW, UFColorCombo.COLOR_SCHEME_GREEN, UFColorCombo.COLOR_SCHEME_CYAN}; 
	final UFColorCombo[] dragObjectBox = new UFColorCombo[4];
	for (int j = 0; j < 4; j++) {
	    dragObjectBox[j] = new UFColorCombo(dragObjectOptions, dragObjectColor[j]);
	    //dragObjectBox[j].setBackground(dragObjectColor[j]);
	    final int objectNum = j;
	    dragObjectBox[j].addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    imagePanels[0].imageDisplay.dragObjectStatus[objectNum]=(String)dragObjectBox[objectNum].getSelectedItem(); 
		    imagePanels[0].imageDisplay.repaint();
		}
	    });
	}

	final UFColorButton rulerButton = new UFColorButton("Show ruler");
	rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
	rulerButton.setText("Show ruler");
        rulerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	     if(rulerButton.getText().indexOf("Show") >= 0) {
	        rulerButton.setText("Hide ruler");
	        rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_ORANGE);
	        imagePanels[0].imageDisplay.showRuler = true;
	        imagePanels[0].imageDisplay.repaint();
             }
	     else if(rulerButton.getText().indexOf("Hide") >= 0) {
	        rulerButton.setText("Show ruler");
	        rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
	        imagePanels[0].imageDisplay.showRuler = false;
	        imagePanels[0].imageDisplay.repaint();
	     }
            }
        });	

        JPanel toolsPanel = new JPanel();
	JLabel scale = new JLabel("Scale:", JLabel.CENTER);
	scale.setForeground(Color.darkGray);
	
	toolsPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
	toolsPanel.setLayout(new RatioLayout());
	toolsPanel.add("0.02,0.02, 0.96, 0.10", new JLabel("Display tools"));
		
	toolsPanel.add("0.02,0.15, 0.26, 0.14", scale);
	toolsPanel.add("0.02,0.31, 0.28, 0.14", zModeBox);
	toolsPanel.add("0.32,0.15, 0.16, 0.14", zMaxLabel);
	toolsPanel.add("0.48,0.15, 0.22, 0.14", zMax);
	toolsPanel.add("0.32,0.31, 0.16, 0.14", zMinLabel);
	toolsPanel.add("0.48,0.31, 0.22, 0.14", zMin);
	
	toolsPanel.add("0.71,0.15, 0.27, 0.14", scaleBox);
	toolsPanel.add("0.71,0.31, 0.27, 0.14", applyZ);

        toolsPanel.add("0.02,0.49, 0.29, 0.14", dragObjectBox[0]);
        toolsPanel.add("0.35,0.49, 0.29, 0.14", dragObjectBox[1]);
        toolsPanel.add("0.67,0.49, 0.29, 0.14", dragObjectBox[2]);
        toolsPanel.add("0.06,0.66, 0.38, 0.14", dragObjectBox[3]);

	toolsPanel.add("0.51,0.66, 0.40, 0.14", rulerButton);
	toolsPanel.add("0.02,0.83, 0.90, 0.14", pixelDataVal);
	
	JButton loadMOSmask = new JButton("Load MOS");
	final FileDialog fd = new FileDialog(this, "Please choose a file:", FileDialog.LOAD);
		
	final JButton showMOSmask = new JButton();	
	showMOSmask.setText("Show MOS");
        showMOSmask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	     if(showMOSmask.getText().indexOf("Show") >= 0) {
	        showMOSmask.setText("Hide MOS");
	        imagePanels[0].imageDisplay.showMOS = true;
	        imagePanels[0].imageDisplay.repaint();
             }
	     else if(showMOSmask.getText().indexOf("Hide") >= 0) {
	        showMOSmask.setText("Show MOS");
	        imagePanels[0].imageDisplay.showMOS = false;
	        imagePanels[0].imageDisplay.repaint();
	     }
            }
        });	
 
        JPanel MOSPanel = new JPanel();
	MOSPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
	MOSPanel.setLayout(new RatioLayout());
	MOSPanel.add("0.02,0.02, 0.96, 0.20", new JLabel("MOS mask"));	
	MOSPanel.add("0.10,0.24, 0.80, 0.32", loadMOSmask);
	MOSPanel.add("0.10,0.62, 0.80, 0.32", showMOSmask);
	
        loadMOSmask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		fd.setVisible(true);
	        String fileMOS = fd.getFile();
                if (fileMOS == null) {
	           // no file selected
	        } else {
		   Vector v = new Vector();
		   String currLine = " ";
		   String mosColor = "";
		   String mosText = "";
		   int i1, i2;
		   boolean hasMoreTokens;
		   boolean displayText = true;
		   try {
		      BufferedReader r = new BufferedReader(new FileReader(fd.getDirectory() + File.separator+fileMOS));
		      while (currLine != null) {
			currLine = r.readLine();
			if (currLine == null) break;
			if (currLine.startsWith("#")) continue;
			if (currLine.startsWith("global")) {
			   i1 = currLine.indexOf("color=")+6;
			   i2 = currLine.indexOf(" ",i1+1);
			   if (i1 != 5 && i2 > 0) mosColor = currLine.substring(i1,i2).trim();
			   else if (i1 != 5 && i2 == -1) mosColor = currLine.substring(i1).trim();
			   i1 = currLine.indexOf("text=")+5;
			   i2 = currLine.indexOf(" ",i1+1);
			   if (i1 != 4 && i2 > 0) {
			      if (currLine.substring(i1,i2).trim().equals("0"))
				displayText = false;
			   }  else if (i1 != 4 && i2 == -1) {
			      if (currLine.substring(i1).trim().equals("0"))
				displayText = false;
			   }
			   continue;
			}
			v.add(currLine);
		      }
		   } catch (IOException exc) {
		     System.out.println("Error Reading From File.");
		   } 
		   MOSMask[] mosMasks = new MOSMask[v.size()];
		   for (int j = 0; j < v.size(); j++) {
		      currLine = (String)v.elementAt(j);
		      if (currLine.startsWith("image;box")) {
			float[] tokens = new float[5];
			mosText = "";
			i1 = 10;
			hasMoreTokens = true;
			int l = 0;
			while (hasMoreTokens && l < 5) {
			   i2 = currLine.indexOf(",",i1);
			   if (i2 < 0) i2 = 10000;
			   if (i2 > currLine.indexOf(")",i1)) {
			      i2 = currLine.indexOf(")",i1);
			      hasMoreTokens = false;
			   }
			   tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
			   l++;
			   i1 = i2+1;
			}
			i1 = currLine.indexOf("text={")+6;
			if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
			mosMasks[j] = new RectMOSMask(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4],mosText);
			mosMasks[j].setTextMode(displayText);
			if (mosColor != "") mosMasks[j].setColor(mosColor);
		      } else if (currLine.startsWith("image;circle")) {
                        float[] tokens = new float[3];
                        mosText = "";
                        i1 = 13;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 3) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2
));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],tokens[2],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
		      } else if (currLine.startsWith("image;ellipse")) {
                        float[] tokens = new float[4];
                        mosText = "";
                        i1 = 14;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 4) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],tokens[2],tokens[3],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
                      } else if (currLine.startsWith("image;point")) {
                        float[] tokens = new float[2];
                        mosText = "";
                        i1 = 12;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 2) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
                      }
		   }
		   imagePanels[0].imageDisplay.mosMasks = mosMasks;
	       }
	       showMOSmask.setText("Hide MOS");
	       imagePanels[0].imageDisplay.showMOS = true;
	       imagePanels[0].imageDisplay.repaint();

	    }
        });	

        JPanel mvZoomPanel = new JPanel();
	mvZoomPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
	mvZoomPanel.setLayout(new RatioLayout());
	
	UFColorButton up    = new UFColorButton("u");
	   up.updateColorGradient(UFColorButton.COLOR_SCHEME_GRAY);
	UFColorButton down  = new UFColorButton("d");
	   down.updateColorGradient(UFColorButton.COLOR_SCHEME_GRAY);
	UFColorButton left  = new UFColorButton("l");
	   left.updateColorGradient(UFColorButton.COLOR_SCHEME_GRAY);
	UFColorButton right = new UFColorButton("r");
	   right.updateColorGradient(UFColorButton.COLOR_SCHEME_GRAY);
	final JTextField pixInc = new JTextField("10");
	pixInc.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
	
        up.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	       if(imagePanels[0].imageDisplay.showZoom==true){
	          int offset = Integer.parseInt(pixInc.getText());
                  imagePanels[0].imageDisplay.yZoom-=(int)(offset/2);
                  imagePanels[0].imageDisplay.ZoomImage();
                  imagePanels[0].imageDisplay.repaint();
	       }
	    }
        });
        down.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	       if(imagePanels[0].imageDisplay.showZoom==true){
	          int offset = Integer.parseInt(pixInc.getText());
                  imagePanels[0].imageDisplay.yZoom+=(int)(offset/2);
                  imagePanels[0].imageDisplay.ZoomImage();
                  imagePanels[0].imageDisplay.repaint();
	       }
	    }
        });
        left.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	       if(imagePanels[0].imageDisplay.showZoom==true){
	          int offset = Integer.parseInt(pixInc.getText());
                  imagePanels[0].imageDisplay.xZoom-=(int)(offset/2);
                  imagePanels[0].imageDisplay.ZoomImage();
                  imagePanels[0].imageDisplay.repaint();
	       }
	    }
        });
/*
	right.addMouseListener(new MouseAdapter() {
	   boolean isPressed = false;
	   public void mousePressed(MouseEvent mev) {
	      if (imagePanels[0].imageDisplay.showZoom==true){
		this.isPressed = true;
		int offset = Integer.parseInt(pixInc.getText());
		while(this.isPressed && b < 30) {
                   imagePanels[0].imageDisplay.xZoom+=(int)(offset/2);
                   imagePanels[0].imageDisplay.ZoomImage();
                   imagePanels[0].imageDisplay.repaint();
		   long currTime = System.currentTimeMillis();
		   while(System.currentTimeMillis() < currTime+50) {
		   }
		}
	      }
	   }
           public void mouseReleased(MouseEvent mev) {
              if (imagePanels[0].imageDisplay.showZoom==true){
                this.isPressed = false;
              }
           }
	});
*/
        right.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	       if(imagePanels[0].imageDisplay.showZoom==true){
	          int offset = Integer.parseInt(pixInc.getText());
                  imagePanels[0].imageDisplay.xZoom+=(int)(offset/2);
                  imagePanels[0].imageDisplay.ZoomImage();
                  imagePanels[0].imageDisplay.repaint();
	       }
	    }
        });	

	mvZoomPanel.add("0.02,0.02, 0.96, 0.20", new JLabel("Move Zoom"));	
	mvZoomPanel.add("0.325,0.24, 0.35, 0.20", up);
	mvZoomPanel.add("0.325,0.78, 0.35, 0.20", down);
	mvZoomPanel.add("0.01,0.50, 0.30, 0.20", left);
	mvZoomPanel.add("0.69,0.50, 0.30, 0.20", right);
	mvZoomPanel.add("0.40,0.50, 0.20, 0.20", pixInc);
	
	UFColorButton sendToBgdButton = new UFColorButton("Send to bgd");
	sendToBgdButton.updateColorGradient(UFColorButton.COLOR_SCHEME_CYAN);
	sendToBgdButton.setForeground(Color.black);
	
        sendToBgdButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	       String zModeBoxSel = (String)zModeBox.getSelectedItem();
               imagePanels[0].imageDisplay.sendToBgd(imagePanels[0].imageDisplay.savedImgBuff);
	       setBgdFileName(srcFileName.getText());
	       updateBuffer(imagePanels, zModeBoxSel);
/*
	       if (zModeBoxSel.indexOf("Auto") >= 0) {
	          imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	          imagePanels[0].adjustPanel.updateMinMax();
	          imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	       } else if (zModeBoxSel.indexOf("Manual") >= 0) {
	          imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	          imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	       }
*/
	    }
        });
	
	JPanel srcBufDispl = new JPanel();
	srcBufDispl.setBackground(Color.gray);
	srcBufDispl.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
	srcBufDispl.setLayout(new RatioLayout());
	srcBufDispl.add("0.01, 0.015,0.98,0.975", srcPreviewImage);
	
	JPanel bgdBufDispl = new JPanel();
	bgdBufDispl.setBackground(Color.gray);
	bgdBufDispl.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
	bgdBufDispl.setLayout(new RatioLayout());
	bgdBufDispl.add("0.01, 0.015,0.98,0.975", bgdPreviewImage);
	
	String buffers[] = {"Src Buffer","Bgd Buffer","Dif Buffer"}; 
	bufferView = new JComboBox(buffers);
	//final JComboBox bufferView = new JComboBox(buffers);
	bufferView.setMaximumRowCount(buffers.length);
	bufferView.setSelectedItem("Dif Buffer");
	
	bufferView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	        bufferViewSelect = (String)bufferView.getSelectedItem();
	       String zModeBoxSel = (String)zModeBox.getSelectedItem();
	       if (bufferViewSelect.indexOf("Src") >= 0 ) {
	         imagePanels[0].imageDisplay.displDif=false;
	         imagePanels[0].imageDisplay.displSrc=true;
	         imagePanels[0].imageDisplay.displBgd=false;
/*
	  	 if (zModeBoxSel.indexOf("Auto") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.updateMinMax();
	  	    imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	  	 } else if (zModeBoxSel.indexOf("Manual") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	  	 } else {
                    imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
                    imagePanels[0].imageDisplay.applyZScale();
		 }
*/
	       }
	       if (bufferViewSelect.indexOf("Bgd") >= 0 ) {
	    	 imagePanels[0].imageDisplay.displDif=false;
	    	 imagePanels[0].imageDisplay.displSrc=false;
	    	 imagePanels[0].imageDisplay.displBgd=true;
/*
	  	 if (zModeBoxSel.indexOf("Auto") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.updateMinMax();
	  	    imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	  	 } else if (zModeBoxSel.indexOf("Manual") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	  	 }
*/
	       }
	       if (bufferViewSelect.indexOf("Dif") >= 0 ) {
	    	 imagePanels[0].imageDisplay.displDif=true;
	    	 imagePanels[0].imageDisplay.displSrc=false;
	    	 imagePanels[0].imageDisplay.displBgd=false;
/*
	  	 if (zModeBoxSel.indexOf("Auto") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.updateMinMax();
	  	    imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	  	 } else if (zModeBoxSel.indexOf("Manual") >= 0) {
	            imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	  	    imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	  	 } else {
                    imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
		    imagePanels[0].imageDisplay.applyZScale();
		 }
*/
	       }
	       updateBuffer(imagePanels, zModeBoxSel);
	       setTitleBarText();
            }
        });

	bgdFileName.setEditable(false);
	bgdFileName.setBackground(Color.white);
        srcFileName.setEditable(false);
        srcFileName.setBackground(Color.white);
	
	fullDisplPanel.add("0.005, 0.01, 0.21, 0.24", toolsPanel);
	fullDisplPanel.add("0.005, 0.255, 0.10, 0.11", MOSPanel);
	fullDisplPanel.add("0.110, 0.255, 0.105, 0.11", mvZoomPanel);
	fullDisplPanel.add("0.005, 0.37,  0.03, 0.02", new JLabel("Src:")) ;
	if (InitDataDisplay.jddtype.equals("NIRjdd")) {
	   fullDisplPanel.add("0.035, 0.37,  0.18, 0.02", InitDataDisplay.obsMonitor.archiveFile()) ;
	} else if (InitDataDisplay.jddtype.equals("flam2jdd")) {
	   fullDisplPanel.add("0.035, 0.37,  0.18, 0.02", srcFileName);
	}
	fullDisplPanel.add("0.005, 0.39,  0.21, 0.27", srcBufDispl);
	fullDisplPanel.add("0.005, 0.66,  0.03, 0.02", new JLabel("Bgd:"));
	fullDisplPanel.add("0.035, 0.66,  0.18, 0.02", bgdFileName);
	fullDisplPanel.add("0.005, 0.68,  0.21, 0.27", bgdBufDispl);

	fullDisplPanel.add("0.01, 0.955, 0.08, 0.04", sendToBgdButton);
	fullDisplPanel.add("0.105, 0.96, 0.03, 0.03", new JLabel("View:"));
	fullDisplPanel.add("0.14, 0.96,  0.07, 0.03", bufferView);

        fullDisplPanel.add("0.22, 0.00, 0.78, 1.00", imPanelsHolder);
	
        this.getContentPane().setLayout(new RatioLayout());
        this.getContentPane().add( "0,0,1,1", fullDisplPanel );
 	
	addComponentListener(new ComponentAdapter() {
	   public void componentResized(ComponentEvent ev) {
	      imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	      if (imagePanels[0].imageDisplay.bgdBuffer != null) {
		imagePanels[0].imageDisplay.updateBackgroundPreview(imagePanels[0].imageDisplay.bgdBuffer);
	      }
              String zModeBoxSel = (String)zModeBox.getSelectedItem();
	      updateScale(imagePanels[0], zModeBoxSel);
	      double ratio = 0.8;
	      int h = getHeight();
	      int w = (int)(h/ratio);
              setSize((int)(h/ratio), h);
	   }
	});
    }  

    public void updateScale(ImagePanel imPanel, String mode) {
	if (mode.indexOf("Auto") >= 0) {
	    imPanel.adjustPanel.updateMinMax();
            imPanel.adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	} else if (mode.indexOf("Manual") >= 0) {
            imPanel.imageDisplay.updateImage(imPanel.imageDisplay.savedImgBuff);
            imPanel.adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	} else if (mode.indexOf("Zoom") >= 0) {
            imPanel.imageDisplay.setZoomScale();
	} else if (mode.startsWith("Z")) {
	    imPanel.imageDisplay.applyZScale(mode);
	}
    }

    public void setTitleBarText() {
	String title = "You're looking at: ";
	if (bufferViewSelect == null || bufferViewSelect.trim().equals("")) {
	    System.err.println("DataFullDisplayFrame.setTitleBarText> bufferViewSelect ComboBox text is null!!!");
	    return;
	}
	if (srcFileName == null) {
	    System.err.println("DataFullDisplayFrame.setTitleBarText> srcFileName TextField is null!!!");
	    return;
	}
	if (bgdFileName == null) {
	    System.err.println("DataFullDisplayFrame.setTitleBarText> bgdFileName TextField is null!!!");
	    return;
	}
	if (bufferViewSelect.indexOf("Src") != -1) {
	    setTitle(title+" Src buffer: "+srcFileName.getText());
	} else if (bufferViewSelect.indexOf("Bgd") != -1) {
	    setTitle(title+" Bgd buffer: "+bgdFileName.getText());
	} else if (bufferViewSelect.indexOf("Dif") != -1) {
	    setTitle(title+" Dif buffer: "+srcFileName.getText()+" - "+bgdFileName.getText());
	}
    }

    public void setBgdFileName (String newName) {
	if (bgdFileName != null) { 
            int idx = newName.lastIndexOf("/");
            if (idx > 0) {
                newName = newName.substring(idx+1);
            }
	    bgdFileName.setText(newName);
	}
	else 
	    System.err.println("DataFullDisplayFrame.setBgdFileName> bgdFileName TextField is null!!!");
	
    }

    public void setSrcFileName(String newName) {
	if (srcFileName != null) {
	    int idx = newName.lastIndexOf("/");
	    if (idx > 0) {
		newName = newName.substring(idx+1);
	    }
	    srcFileName.setText(newName);
	}
	else
	    System.err.println("DataFullDisplayFrame.setSrcFileName> srcFileName TextField is null!!!");
    }

    public void updateBuffer(ImagePanel[] imagePanels, String zModeBoxSel) {
	imagePanels[0].imageDisplay.updateImage(imagePanels[0].imageDisplay.savedImgBuff);
	if (zModeBoxSel.indexOf("Auto") >= 0) {
	    imagePanels[0].adjustPanel.updateMinMax();
	    imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
	} else if (zModeBoxSel.indexOf("Manual") >= 0) {
	    imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(zMin.getText()), Float.parseFloat(zMax.getText()));
	} else if (zModeBoxSel.indexOf("Zoom") >= 0) {
	    imagePanels[0].imageDisplay.setZoomScale();
	} else if (zModeBoxSel.startsWith("Z")) {
	    imagePanels[0].imageDisplay.applyZScale(zModeBoxSel);
	}
    }

    protected void processWindowEvent(WindowEvent wev) {
        if (wev.getID() == WindowEvent.WINDOW_CLOSING ) {
            Object[] exitOptions = {"Exit","Cancel"};
            int n = JOptionPane.showOptionDialog(this, "Are you sure you want to quit?", "Exit JDD?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
            if (n == 0) {
                System.out.println("JDD exiting...");
                System.exit(0);
            }
        }
        else
            super.processWindowEvent(wev);
    }

}
