package ufjdd;
/**
 * Title:        Java Data Display (JDD) : ChannelStatsFrame.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Frank Varosi
 * Company:      University of Florida
 * Description:  For displaying values of detector channel statistics.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
//===================================================================================

public class ChannelStatsFrame extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: ChannelStatsFrame.java,v 1.10 2005/08/02 18:20:36 amarin Exp $";

    protected UFLabel bufferInfo = new UFLabel("Buffer Name : ");
    //protected UFLabel coaddsInfo = new UFLabel("# CoAdds =");
    //protected UFLabel diffsInfo = new UFLabel("# Diffs =");
    protected ChanStat[] chanStats;
    private int xSize = 330, xSizeWide = 400, ySize = 420;
    private boolean _withMinMax = false;

    ChannelStatsFrame( int Nchannels, boolean withMinMax )
    {
	super("Channel Statistics");
	JPanel titPan = new JPanel( new GridLayout(1,0) );
	if( withMinMax ) {
	    this.setSize( xSizeWide, ySize );
	    String[] columnTits = {"Channel","Mean","Min","Max","St. Dev.","Skew","Curtosis"};
	    for( int i=0; i < columnTits.length; i++ ) titPan.add( new JLabel(columnTits[i],JLabel.CENTER) );
	} else {
	    this.setSize( xSize, ySize );
	    String[] columnTits = {"Channel","Mean","St. Dev.","Skew","Curtosis"};
	    for( int i=0; i < columnTits.length; i++ ) titPan.add( new JLabel(columnTits[i],JLabel.CENTER) );
	}
	_withMinMax = withMinMax;
	Container content = this.getContentPane();
	content.setLayout(new GridLayout(0,1));
	content.add( bufferInfo );
	//content.add( coaddsInfo );
	content.add( titPan );
	chanStats = new ChanStat[Nchannels];

	for( int i=0; i < Nchannels; i++ )
	    {
		chanStats[i] = new ChanStat( i+1, withMinMax );
		content.add( chanStats[i] );
	    }

	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent wev) { wev.getWindow().dispose(); } });
    }

    public void display( ImageBuffer imageBuffer )
    {
	bufferInfo.setText( imageBuffer.nameNum );
	//coaddsInfo.setText( imageBuffer.CoAdds );
	int nchan = chanStats.length;
	if( imageBuffer.chanMeans.length < nchan ) nchan = imageBuffer.chanMeans.length;

	for( int i=0; i < nchan; i++ )
	    chanStats[i].setValues( imageBuffer.chanMeans[i], imageBuffer.chanStdevs[i],
				    imageBuffer.chanSkews[i], imageBuffer.chanCurts[i] );

	if( _withMinMax )
	    for( int i=0; i < nchan; i++ )
		chanStats[i].setMinMax( imageBuffer.chanMins[i], imageBuffer.chanMaxs[i] );
    }

    public int Nchannels() { return chanStats.length; }
    public boolean withMinMax() { return _withMinMax; }

//===================================================================================

    class ChanStat extends JPanel {

	JLabel number;
	JTextField mean;
	JTextField min;
	JTextField max;
	JTextField sigma;
	JTextField skew;
	JTextField curtosis;

	ChanStat( int num, boolean withMinMax )
	{
	    this.number = new JLabel( num + " ", JLabel.RIGHT );
	    this.mean = new JTextField();
	    this.sigma = new JTextField();
	    this.skew = new JTextField();
	    this.curtosis = new JTextField();
	    this.mean.setHorizontalAlignment( JTextField.RIGHT );
	    this.sigma.setHorizontalAlignment( JTextField.RIGHT );
	    this.skew.setHorizontalAlignment( JTextField.RIGHT );
	    this.curtosis.setHorizontalAlignment( JTextField.RIGHT );
	    this.mean.setEditable(false);
	    this.sigma.setEditable(false);
	    this.skew.setEditable(false);
	    this.curtosis.setEditable(false);
	    Color LightGray = new Color(222,222,222);
	    this.mean.setBackground( LightGray );
	    this.sigma.setBackground( LightGray );
	    this.skew.setBackground( LightGray );
	    this.curtosis.setBackground( LightGray );
	    this.setLayout( new RatioLayout() );

	    if( withMinMax ) {
		this.min = new JTextField();
		this.max = new JTextField();
		this.min.setHorizontalAlignment( JTextField.RIGHT );
		this.max.setHorizontalAlignment( JTextField.RIGHT );
		this.min.setEditable(false);
		this.max.setEditable(false);
		this.min.setBackground( LightGray );
		this.max.setBackground( LightGray );
		this.add("0.00,0.0;0.06,1.0", number );
		this.add("0.06,0.0;0.19,1.0", mean );
		this.add("0.25,0.0;0.17,1.0", min );
		this.add("0.42,0.0;0.17,1.0", max );
		this.add("0.59,0.0;0.15,1.0", sigma );
		this.add("0.74,0.0;0.13,1.0", skew );
		this.add("0.87,0.0;0.13,1.0", curtosis );
	    }
	    else {
		this.add("0.00,0.0;0.10,1.0", number );
		this.add("0.10,0.0;0.26,1.0", mean );
		this.add("0.36,0.0;0.24,1.0", sigma );
		this.add("0.60,0.0;0.20,1.0", skew );
		this.add("0.80,0.0;0.20,1.0", curtosis );
	    }
	}

	public void setValues( double mean, double sigma, double skew, double curtosis )
	{
	    this.mean.setText( twoDecimDigits( mean ) );
	    this.sigma.setText( twoDecimDigits( sigma ) );
	    this.skew.setText( twoDecimDigits( skew ) );
	    this.curtosis.setText( twoDecimDigits( curtosis ) );
	}

	public void setMinMax( double min, double max )
	{
	    this.min.setText( twoDecimDigits( min ) );
	    this.max.setText( twoDecimDigits( max ) );
	}

	private String twoDecimDigits( double value )
	{
	    String valTxt = Double.toString( value );
	    int ip = valTxt.indexOf(".");
	    if( ip <= 0 ) return valTxt;
	    int nchar = valTxt.length();
	    int ndigits = ip + 3;

	    if( ndigits < nchar ) {
		String vText = valTxt.substring( 0, ndigits );
		int ie = valTxt.indexOf("E");
		if( ie > 0 ) {
		    String sign = valTxt.substring( ie+1, ie+2 );
		    if( sign.equals("-") )
			return("0.00");
		    else
			return( vText + valTxt.substring( ie, nchar ) );
		}
		else return vText;
	    }
	    else if( ndigits > nchar )
		return( valTxt + "0" );
	    else
		return valTxt;
	}
    }
}
