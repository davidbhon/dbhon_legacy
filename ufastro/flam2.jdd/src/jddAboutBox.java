package ufjdd;

/**
 * Title:        Java Data Display  (JDD).
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.ImageIcon;
import java.applet.*;


//===============================================================================
/**
 * Window to display "About" contents
 */
public class jddAboutBox extends JDialog implements ActionListener {
  public static final String rcsID = "$Name:  $ $Id: jddAboutBox.java,v 1.3 2005/11/29 21:08:10 drashkin Exp $";

  JPanel panel1 = new JPanel();
  JPanel panel2 = new JPanel();
  JPanel insetsPanel1 = new JPanel();
  JPanel insetsPanel2 = new JPanel();
  JPanel insetsPanel3 = new JPanel();
  JButton button1 = new JButton();
  JLabel imageControl1 = new JLabel();
  ImageIcon imageIcon;
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();
  String product = "CIRCE Java Data Display (JDD)";
  String copyright = "Copyright (c) 2005";
  String comments = "CIRCE near-IR instrument";
  String authors = "Written by Marin-Franch, Warner, Rashkin, Varosi and Saleh";

//-------------------------------------------------------------------------------
  /**
   *Constructor from a given parent frame
   *@param parent Frame: parent frame
   */
  public jddAboutBox(Frame parent) {
      super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    pack();
  }

//-------------------------------------------------------------------------------
  /**
   *Overriden so that we can close on system exit
   *@param e TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
  } //end of processWindowEvent

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    imageIcon = new ImageIcon("./data/logo.gif");
    imageControl1 = new JLabel(imageIcon) ;
    this.setTitle("About");
    setResizable(false);
    panel1.setLayout(new BorderLayout());
    panel2.setLayout(new BorderLayout());
    insetsPanel1.setLayout(new FlowLayout());
    insetsPanel2.setLayout(new FlowLayout());
    insetsPanel3.setLayout(new GridLayout(5,1));
    insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
    insetsPanel3.setBorder(new EmptyBorder(1, 10, 10, 10));
    button1.setText("OK");
    button1.addActionListener(this);
    insetsPanel2.add(imageControl1, null);
    panel2.add(insetsPanel2, BorderLayout.WEST);
    insetsPanel3.add(new JLabel(product), null);
    insetsPanel3.add(new JLabel("Version 2005/08/09"), null);
    insetsPanel3.add(new JLabel(copyright), null);
    insetsPanel3.add(new JLabel(comments), null);
    insetsPanel3.add(new JLabel(authors), null);
    panel2.add(insetsPanel3, BorderLayout.CENTER);
    insetsPanel1.add(button1, null);
    panel1.add(panel2, BorderLayout.WEST);
    panel1.add(insetsPanel3, BorderLayout.CENTER);
    panel1.add(insetsPanel1, BorderLayout.SOUTH);
    this.getContentPane().add(panel1, null);
  } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   *method to dispose (close) this dialog window
   */
  void cancel() { dispose(); }

//-------------------------------------------------------------------------------
  /**
   *ActionPerformed method calls the 'cancel' method when the button is pressed.
   *@param e TBD
   */
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == button1) {
      cancel();
    }

  } //end of actionPerformed

} //end of class jddAboutBox

