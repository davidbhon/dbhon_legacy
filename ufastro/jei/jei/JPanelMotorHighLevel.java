package ufjei;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;

//===============================================================================
/**
 * Handles the Motor High Level tabbed pane
 */
public class JPanelMotorHighLevel extends JPanel{
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorHighLevel.java,v 0.9 2002/12/04 23:04:04 varosi beta $";

  public static JPanelMotorNames jPanelMotorNames  ;
  public static JPanelNamedLocation jPanelNamedLocation ;
  public static JPanelMove jPanelMove ;
  public static JPanelHome jPanelHome  ;
  public static JPanelMotorAbort jPanelAbort  ;
  public static JPanelMotorStatus jPanelStatus  ;
  public static JPanelMotorLock jPanelMotorLock ;

    // save fiducial parameters for controlling the High-Resolution Grating.
    public String fiducialAngle;
    public String fiducialSteps;

  RatioLayout panelayout = new RatioLayout();
  GridLayout buttonlayout = new GridLayout();
  JPanel jPanelButtons = new JPanel();
  JButton jButtonConnect = new JButton("Connect  EPICS  to  Motor  Agent");
  JButton jButtonQueryPos = new JButton("Query Status");
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "cc:heartbeat",
					     EPICS.prefix + "cc:HeartBeat:");
  EPICSLabel monitorCAR = new EPICSLabel(EPICS.prefix + "cc:applyC.VAL", "CAR:");
  EPICSLabel monitorIMSS = new EPICSLabel(EPICS.prefix + "cc:applyC.IMSS", "Msg:");

    // set and monitor parameters for controlling the High-Resolution Grating.
    EPICSTextField jTextFiducialAngle = new EPICSTextField(EPICS.prefix + "sad:fiducialAngle",
							   "Fiducial Angle of High-Res grating", true);
    EPICSTextField jTextFiducialSteps = new EPICSTextField(EPICS.prefix + "sad:fiducialSteps",
							   "Fiducial Steps of High-Res grating", true);
    EPICSTextField jTextCentralWavelen = new EPICSTextField(EPICS.prefix + "sad:wavelength",
							    "Desired Central wavelength", true);
    JTextField jTextActualWavelen = new JTextField();
    JButton jButtonGetActualWavelen = new JButton("Get  Actual  Wavelength =");
    JButton jButtonResetFidAngle = new JButton("Reset");
    JButton jButtonResetFidSteps = new JButton("Reset");
    JCheckBox jCheckBoxFidAngle = new JCheckBox("fiducial  Angle =");
    JCheckBox jCheckBoxFidSteps = new JCheckBox("fiducial  Steps =");
//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param jeiMotorParameters jeiMotorParameters: motor parameters object
   */
  public JPanelMotorHighLevel(jeiMotorParameters jeiMotorParameters) {
    fiducialAngle = EPICS.get(EPICS.prefix + "sad:fiducialAngle");
    fiducialSteps = EPICS.get(EPICS.prefix + "sad:fiducialSteps");
    this.setLayout(panelayout);
    buttonlayout.setColumns(0);
    buttonlayout.setRows(1);
    jPanelButtons.setLayout(buttonlayout);
    JPanel jPanelHighResGrating = new JPanel();
    jPanelHighResGrating.setLayout( new RatioLayout() );
    jPanelHighResGrating.setBorder(BorderFactory.createEtchedBorder());
    jPanelMotorNames = new JPanelMotorNames(jeiMotorParameters);
    jPanelNamedLocation = new JPanelNamedLocation(jeiMotorParameters);
    jPanelMove = new JPanelMove(jeiMotorParameters);
    jPanelHome = new JPanelHome(jeiMotorParameters);
    jPanelAbort = new JPanelMotorAbort(jeiMotorParameters);
    jPanelStatus = new JPanelMotorStatus(jeiMotorParameters);
    jPanelMotorLock = new JPanelMotorLock(jeiMotorParameters);
    jPanelButtons.add(jPanelMove);
    jPanelButtons.add(jPanelHome);
    jPanelButtons.add(jPanelAbort);

    this.add("0.0,0.02;0.015,0.66", jPanelMotorLock);
    this.add("0.02,0.02;0.15,0.66", jPanelMotorNames);
    this.add("0.17,0.02;0.15,0.66", jPanelNamedLocation);
    this.add("0.32,0.02;0.34,0.66", jPanelButtons);
    this.add("0.66,0.02;0.33,0.66", jPanelStatus);
    this.add("0.05,0.80;0.30,0.10", jButtonConnect);
    HeartBeatLabel.setHorizontalAlignment(JLabel.LEFT);
    this.add("0.60,0.70;0.35,0.10", HeartBeatLabel);
    this.add("0.05,0.70;0.15,0.10", monitorCAR);
    this.add("0.20,0.70;0.40,0.10", monitorIMSS);
    this.add("0.85,0.70;0.14,0.09", jButtonQueryPos);
    this.add("0.38,0.80;0.61,0.19", jPanelHighResGrating );

    jPanelHighResGrating.add("0.00,0.00;0.30,0.25", new JLabel("High  Resolution  Grating:") );
    jPanelHighResGrating.add("0.00,0.30;0.35,0.35", new JLabel("Desired  Central  Wavelength =") );
    jPanelHighResGrating.add("0.35,0.30;0.10,0.35", jTextCentralWavelen );
    jPanelHighResGrating.add("0.45,0.30;0.05,0.35", new JLabel("um") );
    jPanelHighResGrating.add("0.53,0.30;0.22,0.35", jCheckBoxFidAngle );
    jPanelHighResGrating.add("0.75,0.30;0.11,0.35", jTextFiducialAngle );
    jPanelHighResGrating.add("0.86,0.30;0.14,0.35", jButtonResetFidAngle );
    jPanelHighResGrating.add("0.00,0.65;0.35,0.35", jButtonGetActualWavelen );
    jPanelHighResGrating.add("0.35,0.65;0.10,0.35", jTextActualWavelen );
    jPanelHighResGrating.add("0.45,0.65;0.05,0.35", new JLabel("um") );
    jPanelHighResGrating.add("0.53,0.65;0.22,0.35", jCheckBoxFidSteps );
    jPanelHighResGrating.add("0.75,0.65;0.11,0.35", jTextFiducialSteps );
    jPanelHighResGrating.add("0.86,0.65;0.14,0.35", jButtonResetFidSteps );

    jButtonGetActualWavelen.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		jTextActualWavelen.setText( EPICS.get( EPICS.prefix + "sad:adjWaveLength" ).substring(0,6) );
	    }
	});

    jTextFiducialAngle.setEnabled(false);
    jTextFiducialSteps.setEnabled(false);

    jCheckBoxFidAngle.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
	  if( jCheckBoxFidAngle.isSelected() )
	      jTextFiducialAngle.setEnabled(true);
	  else
	      jTextFiducialAngle.setEnabled(false);
      }
    });

    jCheckBoxFidSteps.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
	  if( jCheckBoxFidSteps.isSelected() )
	      jTextFiducialSteps.setEnabled(true);
	  else
	      jTextFiducialSteps.setEnabled(false);
      }
    });

    jButtonResetFidAngle.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		if( jCheckBoxFidAngle.isSelected() )
		    jTextFiducialAngle.set_and_putcmd( fiducialAngle );
	    }
	});

    jButtonResetFidSteps.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		if( jCheckBoxFidSteps.isSelected() )
		    jTextFiducialSteps.set_and_putcmd( fiducialSteps );
	    }
	});

    jButtonQueryPos.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		jPanelStatus.queryMotorPositions();
	    }
	});

    jButtonConnect.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) {
		monitorCAR.setText("CAR: BUSY");
		monitorIMSS.setText("Msg: Epics connecting to Motor Agent...");
		try { Thread.sleep(100); } catch (Exception _e) {}
		jPanelStatus.connectEPICStoMotorAgent();
	    }
	});
  } //end of JPanelMotorHighLevel
} //end of class JPanelMotorHighLevel

//===============================================================================
/**
 * Location name object
 */
class JPanelNamedLocation extends JPanel{
  public static EPICSComboBox NamedLocation[] ;

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelNamedLocation(jeiMotorParameters mot_param) {
    NamedLocation = new EPICSComboBox[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel("Named Location"), null);
    int i;
    for (i=0; i < mot_param.getNumberMotors(); i++) {
       String pv_name = mot_param.getPVmotorPrefix(i+1) + "namedPos.A";
       String mon_name = mot_param.getPVmotorPrefix(i+1) + "namedPos.VALC";
       String item_names[] = new String[mot_param.getNumLocations(i+1)+1];
       for (int j=1;j<=mot_param.getNumLocations(i+1);j++) {
         item_names[j] = new String(mot_param.getNamedLocations(i+1,j));
       }
       item_names[0] = "";
       NamedLocation[i] = new EPICSComboBox(pv_name, mon_name, item_names, EPICSComboBox.INDEX);
       NamedLocation[i].setLightWeightPopupEnabled(true);
       add (NamedLocation[i],null);
       //System.out.println(i + " " + pv_name);
    }
  } //end of JPanelNamedLocation

} //end of class JPanelNamedLocation

//===============================================================================
/**
 * Array of move buttons
 */
class JPanelMove extends JPanel implements ActionListener {
  public static JButton b[] ;

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMove(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton ("Move");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       add( b[i] );
    }
  } //end of JPanelMove

//-------------------------------------------------------------------------------
  /**
   * TBD
   */
  public void actionPerformed(ActionEvent e) {
    String mot_num_str = e.getActionCommand();
    int midx = Integer.parseInt(mot_num_str);
    int mot_num = midx+1;
    //even though EPICSMotorStatus class will lock these (and unlock when not BUSY),
    // lock them now first before the user can press another button:
    jeiFrame.jPanelLowLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelMoveLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelHomeLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelOrigin.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelHome.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMove.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[midx].setEnabled(false);
    //    jeiFrame.jPanelParameters.putParams(mot_num);
    int pos_idx = jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[midx].getSelectedIndex();
    String pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "namedPos.A";
    String cmd = "PUT " + pv_name + " " + pos_idx + " ;named position";
    jeiCmd command = new jeiCmd();
    command.execute(cmd);
    pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "motorApply.DIR";
    cmd = "PUT " + pv_name + " 3 ;apply the high level move";
    command.execute(cmd);
    
  } //end of actionPerformed

} //end of class JPanelMove

//===============================================================================
/**
 * Array of Home buttons
 */
class JPanelHome extends JPanel implements ActionListener {
  public static JButton b[] ;

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public JPanelHome(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton ("Datum");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       add( b[i] );
    }
  } //end of JPanelHome

//-------------------------------------------------------------------------------
  /**
   * TBD
   */
  public void actionPerformed(ActionEvent e) {
   // need to add datum speed and datum direction (??)
    String mot_num_str = e.getActionCommand();
    int midx = Integer.parseInt(mot_num_str);
    int mot_num = midx+1;
    //even though EPICSMotorStatus class will lock these (and unlock when not BUSY),
    // lock them now first before the user can press another button:
    jeiFrame.jPanelLowLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelMoveLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelHomeLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelOrigin.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelHome.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMove.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[midx].setEnabled(false);
    //send out the motor parameters
    //    jeiFrame.jPanelParameters.putParams(mot_num);
    jeiCmd command = new jeiCmd();
    String pvName = jeiMotorParameters.getPVmotorPrefix(mot_num);
    String cmd = "PUT " + pvName + "motorApply.DIR CLEAR ;clear inputs";
    command.execute(cmd);
    cmd = "PUT " + pvName + "datum.A 1 ;mark datum";
    command.execute(cmd);
    cmd = "PUT " + pvName + "motorApply.DIR 3 ;start datum move";
    command.execute(cmd);
  } //end of actionPerformed

} //end of class JPanelHome
