package ufjei;

import java.util.*;
import java.io.*;
import jca.*;
import jca.dbr.*;
import jca.event.*;


//===============================================================================
/**
 * Class for storing record field information
 * Contains the rcsID and the prefix
 */
public class EPICS {
  public static final String rcsID = "$Name:  $ $Id: EPICS.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  static public String prefix;


//-------------------------------------------------------------------------------
  /**
   *Put the val into the rec Record field
   *@param rec String: record field
   *@param val String: value putting into the field
   */
  static public void put(String rec, String val) {
    try {
      PV pv = new PV(rec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      pv.put(val);
      Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception exp) {
      jeiError.show(exp.toString() + " " + rec);
    }
  } //end of put


//-------------------------------------------------------------------------------
  /**
   *TBD
   *@param pv PV: TBD
   *@param val String: value putting into the field
   */
  static public void put(PV pv, String val) {
    try {
     Ca.pendIO(jeiFrame.TIMEOUT);
     pv.put(val);
     Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception exp) {
      //System.out.println(exp.toString());
      jeiError.show(exp.toString() + " " + pv.name());
      //JOptionPane.showMessageDialog(this,exp.toString());
    }
  } //end of put


//-------------------------------------------------------------------------------
  /**
   *Returns the String of the value in the rec Record Field
   *@param rec String: Record field
   */
  static public String get(String rec) {
    String val = "";
    try {
      PV pv = new PV(rec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      DBR_DBString dbr = new DBR_DBString("");
      pv.get(dbr);
      Ca.pendIO(jeiFrame.TIMEOUT);
      val = dbr.valueAt(0);
    }
    catch (Exception exp) {
      jeiError.show(exp.toString() + " " + rec);
       //System.out.println(exp.toString());
    }
    return val;
  } //end of get

/*
put epicsSim here someday

  public static boolean in_simulation_mode = true;
  private static Properties recs = new Properties();

  static {
    load();
  }

  synchronized static String get(String rec) {
    return recs.getProperty(rec);
  }

  synchronized static void put(String rec, String val) {
    recs.put(rec, val);
    save();
  }

  synchronized static void load() {
    try {
      FileInputStream input = new FileInputStream( "sim_recs.txt" );
      recs.load( input );
      input.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  synchronized static void save() {
    try {
      FileOutputStream output = new FileOutputStream( "sim_recs.txt" );
      recs.store( output, "simulated EPICS records" );
      output.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  static void monitor(PV pv, String rec, jca.event.MonitorListener l, int mask) {
    // Start a thread that checks to see if rec changes value.
    // In the thread, call arg.monitorChanged if it does.
    mon_thread aThread = new mon_thread(pv, rec, l, mask);
    aThread.start();
  }

}

class mon_thread extends Thread {
  PV pv;
  String rec;
  jca.event.MonitorListener l;
  int mask;
  String val;

  public mon_thread(PV _pv, String _rec, jca.event.MonitorListener _l, int _mask) {
    super(_rec);
    pv = _pv;
    rec = _rec;
    l = _l;
    mask = _mask;
    val = epicsSim.get(rec);
  }

  public void run() {
    while (true) {
      System.out.println( "checking " + val );
      if (val.compareTo(epicsSim.get(rec)) != 0) {
        val = epicsSim.get(rec);
        MonitorEvent event = new MonitorEvent(pv, new jca.dbr.DBR_DBString(), null, l);
        event.fire();
      }
      try {
        Thread.currentThread().sleep(1000);
      } catch (InterruptedException e) { }
    }
  }
*/
} //end of class Epics

