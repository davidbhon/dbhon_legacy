package ufjei;

import jca.*;
import jca.event.*;
import jca.dbr.*;


//===============================================================================
/**
 *
 */
public class jca_test implements MonitorListener{
  public static final String rcsID = "$Name:  $ $Id: jca_test.java,v 0.0 2002/06/03 17:44:36 hon beta $";

  static String args[];


//-------------------------------------------------------------------------------
  /**
   *
   */
  public void monitorChanged (MonitorEvent e) {
    System.out.println("Monitor Happened");
  } //end of monitorChanged


//-------------------------------------------------------------------------------
  /**
   *
   */
  public jca_test () {
    try {
      PV pv = new PV(args[0]);
      Ca.pendIO(0.5);
      pv.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
      Ca.pendIO(0.5);
      pv.put(args[1]);
      Ca.pendIO(0.5);

    }
    catch (Exception e) {
      System.out.println("Error: " + e.toString());

    }
    System.exit(0);
  } //end of jca_test


//-------------------------------------------------------------------------------
  /**
   *
   */
  public static void main (String [] _args) {

    if (_args.length != 2) {
      System.out.println("incorrect number of arguments");
      return;
    }
    else {
      args = _args;
      new jca_test();
    }

  } //end of main

} //end of class jca_test

