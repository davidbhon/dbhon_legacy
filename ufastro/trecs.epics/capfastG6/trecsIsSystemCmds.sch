[schematic2]
uniq 110
[tools]
[detail]
w 738 1867 100 0 n#109 eapply.eapply#21.INPB 0 1056 -224 1056 -224 1856 1760 1856 1760 1664 1664 1664 trecsTest.trecsTest#101.VAL
w 328 1419 -100 0 MESS eapply.eapply#21.MESS 384 1184 480 1184 480 1408 224 1408 224 1536 320 1536 outhier.MESS.p
w 296 1355 -100 0 VAL eapply.eapply#21.VAL 384 1216 448 1216 448 1344 192 1344 192 1664 320 1664 outhier.VAL.p
w -8 1419 -100 0 CLID eapply.eapply#21.CLID 0 1184 -96 1184 -96 1408 128 1408 128 1536 64 1536 inhier.CLID.P
w 24 1355 -100 0 DIR eapply.eapply#21.DIR 0 1216 -64 1216 -64 1344 160 1344 160 1664 64 1664 inhier.DIR.P
w 520 779 100 0 n#94 eapply.eapply#21.OCLF 384 768 704 768 704 640 832 640 trecsReboot.trecsReboot#108.ICID
w 840 843 100 0 n#93 eapply.eapply#21.OCLE 384 832 1344 832 1344 640 1472 640 trecsDebug.trecsDebug#104.ICID
w 1160 907 100 0 n#92 eapply.eapply#21.OCLD 384 896 1984 896 1984 640 2112 640 trecsPark.trecsPark#103.ICID
w 1176 971 100 0 n#91 eapply.eapply#21.OCLC 384 960 2016 960 2016 1600 2112 1600 trecsDatum.trecsDatum#102.ICID
w 856 1035 100 0 n#90 eapply.eapply#21.OCLB 384 1024 1376 1024 1376 1600 1472 1600 trecsTest.trecsTest#101.ICID
w 536 1099 100 0 n#89 eapply.eapply#21.OCLA 384 1088 736 1088 736 1600 832 1600 trecsInit.trecsInit#100.ICID
w 1032 -53 100 0 n#88 eapply.eapply#21.INPD 0 928 -320 928 -320 -64 2432 -64 2432 704 2304 704 trecsPark.trecsPark#103.VAL
w 1032 -21 100 0 n#87 eapply.eapply#21.INMD 0 896 -288 896 -288 -32 2400 -32 2400 640 2304 640 trecsPark.trecsPark#103.MESS
w 744 11 100 0 n#86 eapply.eapply#21.INPE 0 864 -256 864 -256 0 1792 0 1792 704 1664 704 trecsDebug.trecsDebug#104.VAL
w 744 43 100 0 n#85 eapply.eapply#21.INME 0 832 -224 832 -224 32 1760 32 1760 640 1664 640 trecsDebug.trecsDebug#104.MESS
w 456 75 100 0 n#84 eapply.eapply#21.INPF 0 800 -192 800 -192 64 1152 64 1152 704 1024 704 trecsReboot.trecsReboot#108.VAL
w 456 107 100 0 n#83 eapply.eapply#21.INMF 0 768 -160 768 -160 96 1120 96 1120 640 1024 640 trecsReboot.trecsReboot#108.MESS
w 1032 1963 100 0 n#82 eapply.eapply#21.INMC 0 960 -320 960 -320 1952 2432 1952 2432 1600 2304 1600 trecsDatum.trecsDatum#102.MESS
w 1032 1931 100 0 n#81 eapply.eapply#21.INPC 0 992 -288 992 -288 1920 2400 1920 2400 1664 2304 1664 trecsDatum.trecsDatum#102.VAL
w 744 1899 100 0 n#80 eapply.eapply#21.INMB 0 1024 -256 1024 -256 1888 1792 1888 1792 1600 1664 1600 trecsTest.trecsTest#101.MESS
w 456 1835 100 0 n#78 eapply.eapply#21.INMA 0 1088 -192 1088 -192 1824 1152 1824 1152 1600 1024 1600 trecsInit.trecsInit#100.MESS
w 456 1803 100 0 n#77 eapply.eapply#21.INPA 0 1120 -160 1120 -160 1792 1120 1792 1120 1664 1024 1664 trecsInit.trecsInit#100.VAL
w 536 811 100 0 n#76 eapply.eapply#21.OUTF 384 800 736 800 736 704 832 704 trecsReboot.trecsReboot#108.DIR
w 856 875 100 0 n#75 eapply.eapply#21.OUTE 384 864 1376 864 1376 704 1472 704 trecsDebug.trecsDebug#104.DIR
w 1176 939 100 0 n#74 eapply.eapply#21.OUTD 384 928 2016 928 2016 704 2112 704 trecsPark.trecsPark#103.DIR
w 1160 1003 100 0 n#73 trecsDatum.trecsDatum#102.DIR 2112 1664 1984 1664 1984 992 384 992 eapply.eapply#21.OUTC
w 520 1131 100 0 n#72 trecsInit.trecsInit#100.DIR 832 1664 704 1664 704 1120 384 1120 eapply.eapply#21.OUTA
w 840 1067 100 0 n#71 trecsTest.trecsTest#101.DIR 1472 1664 1344 1664 1344 1056 384 1056 eapply.eapply#21.OUTB
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS System Commands
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/08
s 2096 -272 100 1792 Author: WNR
s 2528 -240 100 1792 trecsIsSystemCmds.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/08
[cell use]
use trecsReboot 832 167 100 0 trecsReboot#108
xform 0 928 480
use trecsDebug 1472 167 100 0 trecsDebug#104
xform 0 1568 480
use trecsPark 2112 167 100 0 trecsPark#103
xform 0 2208 480
use trecsDatum 2112 1127 100 0 trecsDatum#102
xform 0 2208 1440
use trecsTest 1472 1127 100 0 trecsTest#101
xform 0 1568 1440
use trecsInit 832 1127 100 0 trecsInit#100
xform 0 928 1440
use inhier 48 1623 100 0 DIR
xform 0 64 1664
use inhier 48 1495 100 0 CLID
xform 0 64 1536
use outhier 288 1623 100 0 VAL
xform 0 304 1664
use outhier 288 1495 100 0 MESS
xform 0 304 1536
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use eapply 0 583 100 0 eapply#21
xform 0 192 944
p 192 576 100 1024 1 name:$(top)apply1
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsIsSystemCmds.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
