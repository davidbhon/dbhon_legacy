[schematic2]
uniq 405
[tools]
[detail]
w 1938 971 100 0 n#402 egenSub.egenSub#289.OUTB 1840 960 2096 960 hwout.hwout#403.outp
w 1178 971 100 0 n#401 egenSub.egenSub#289.INPB 1552 960 864 960 864 1088 448 1088 ecad20.ecad20#93.VALF
w 1922 1963 100 0 n#398 egenSub.egenSub#319.OUTA 1824 1952 2080 1952 hwout.hwout#399.outp
w 1106 1963 100 0 n#397 egenSub.egenSub#319.INPA 1536 1952 736 1952 736 1408 448 1408 ecad20.ecad20#93.VALA
w 706 203 100 0 n#394 ecad20.ecad20#93.PLNK 448 0 576 0 576 192 896 192 896 320 1024 320 elongouts.elongouts#387.SLNK
w 1003 416 100 0 n#393 hwin.hwin#391.in 992 352 1024 352 elongouts.elongouts#387.DOL
w 1386 363 100 0 n#392 elongouts.elongouts#387.FLNK 1280 352 1552 352 egenSub.egenSub#289.SLNK
w 1706 155 100 0 n#389 hwout.hwout#388.outp 2000 144 1472 144 1472 288 1280 288 elongouts.elongouts#387.OUT
w 50 171 100 0 n#381 hwin.hwin#380.in 32 160 128 160 ecad20.ecad20#93.INPT
w 50 235 100 0 n#378 hwin.hwin#373.in 32 224 128 224 ecad20.ecad20#93.INPS
w 50 299 100 0 n#377 hwin.hwin#372.in 32 288 128 288 ecad20.ecad20#93.INPR
w 50 363 100 0 n#376 hwin.hwin#371.in 32 352 128 352 ecad20.ecad20#93.INPQ
w 50 427 100 0 n#375 hwin.hwin#370.in 32 416 128 416 ecad20.ecad20#93.INPP
w 1170 203 100 0 n#354 efanouts.efanouts#96.LNK1 912 0 1024 0 1024 192 1376 192 1376 1280 1536 1280 egenSub.egenSub#319.SLNK
w 1922 1899 100 0 n#341 egenSub.egenSub#319.OUTB 1824 1888 2080 1888 hwout.hwout#346.outp
w 1922 1835 100 0 n#340 egenSub.egenSub#319.OUTC 1824 1824 2080 1824 hwout.hwout#347.outp
w 1922 1771 100 0 n#339 egenSub.egenSub#319.OUTD 1824 1760 2080 1760 hwout.hwout#178.outp
w 1922 1707 100 0 n#338 egenSub.egenSub#319.OUTE 1824 1696 2080 1696 hwout.hwout#348.outp
w 1922 1643 100 0 n#337 egenSub.egenSub#319.OUTF 1824 1632 2080 1632 hwout.hwout#349.outp
w 1922 1579 100 0 n#336 egenSub.egenSub#319.OUTG 1824 1568 2080 1568 hwout.hwout#350.outp
w 1922 1515 100 0 n#335 egenSub.egenSub#319.OUTH 1824 1504 2080 1504 hwout.hwout#351.outp
w 1922 1451 100 0 n#334 egenSub.egenSub#319.OUTI 1824 1440 2080 1440 hwout.hwout#352.outp
w 1922 1387 100 0 n#333 egenSub.egenSub#319.OUTJ 1824 1376 2080 1376 hwout.hwout#353.outp
w 1138 1899 100 0 n#358 egenSub.egenSub#319.INPB 1536 1888 800 1888 800 1344 448 1344 ecad20.ecad20#93.VALB
w 1170 1835 100 0 n#359 egenSub.egenSub#319.INPC 1536 1824 864 1824 864 1152 448 1152 ecad20.ecad20#93.VALE
w 970 1035 100 0 n#360 egenSub.egenSub#289.INPA 1552 1024 448 1024 ecad20.ecad20#93.VALG
w 1202 1771 100 0 n#360 junction 928 1024 928 1760 1536 1760 egenSub.egenSub#319.INPD
w 970 779 100 0 n#364 egenSub.egenSub#289.INPE 1552 768 448 768 ecad20.ecad20#93.VALK
w 1234 1707 100 0 n#364 junction 992 768 992 1696 1536 1696 egenSub.egenSub#319.INPE
w 970 715 100 0 n#365 egenSub.egenSub#289.INPF 1552 704 448 704 ecad20.ecad20#93.VALL
w 1266 1643 100 0 n#365 junction 1056 704 1056 1632 1536 1632 egenSub.egenSub#319.INPF
w 970 651 100 0 n#366 egenSub.egenSub#289.INPG 1552 640 448 640 ecad20.ecad20#93.VALM
w 1298 1579 100 0 n#366 junction 1120 640 1120 1568 1536 1568 egenSub.egenSub#319.INPG
w 970 587 100 0 n#367 egenSub.egenSub#289.INPH 1552 576 448 576 ecad20.ecad20#93.VALN
w 1330 1515 100 0 n#367 junction 1184 576 1184 1504 1536 1504 egenSub.egenSub#319.INPH
w 970 523 100 0 n#368 egenSub.egenSub#289.INPI 1552 512 448 512 ecad20.ecad20#93.VALO
w 1362 1451 100 0 n#368 junction 1248 512 1248 1440 1536 1440 egenSub.egenSub#319.INPI
w 970 459 100 0 n#369 egenSub.egenSub#289.INPJ 1552 448 448 448 ecad20.ecad20#93.VALP
w 1394 1387 100 0 n#369 junction 1312 448 1312 1376 1536 1376 egenSub.egenSub#319.INPJ
w 970 843 100 0 n#363 egenSub.egenSub#289.INPD 1552 832 448 832 ecad20.ecad20#93.VALJ
w 970 907 100 0 n#362 egenSub.egenSub#289.INPC 1552 896 448 896 ecad20.ecad20#93.VALI
w 1930 459 100 0 n#308 egenSub.egenSub#289.OUTJ 1840 448 2080 448 hwout.hwout#140.outp
w 1930 523 100 0 n#307 egenSub.egenSub#289.OUTI 1840 512 2080 512 hwout.hwout#139.outp
w 1930 587 100 0 n#306 egenSub.egenSub#289.OUTH 1840 576 2080 576 hwout.hwout#138.outp
w 1930 651 100 0 n#305 egenSub.egenSub#289.OUTG 1840 640 2080 640 hwout.hwout#137.outp
w 1930 715 100 0 n#304 egenSub.egenSub#289.OUTF 1840 704 2080 704 hwout.hwout#136.outp
w 1930 779 100 0 n#303 egenSub.egenSub#289.OUTE 1840 768 2080 768 hwout.hwout#135.outp
w 1930 843 100 0 n#302 egenSub.egenSub#289.OUTD 1840 832 2080 832 hwout.hwout#134.outp
w 1930 907 100 0 n#301 egenSub.egenSub#289.OUTC 1840 896 2080 896 hwout.hwout#133.outp
w 1930 1035 100 0 n#299 egenSub.egenSub#289.OUTA 1840 1024 2080 1024 hwout.hwout#131.outp
w 1474 75 100 0 n#385 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IVAL 1408 64 1600 64 ecars.ecars#53.IVAL
w 1474 11 100 0 n#386 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IMSS 1408 0 1600 0 ecars.ecars#53.IMSS
w 986 -21 100 0 n#384 efanouts.efanouts#96.LNK2 912 -32 1120 -32 trecsSubSysCommand.trecsSubSysCommand#56.START
w 482 -21 100 0 n#356 ecad20.ecad20#93.STLK 448 -32 576 -32 576 -80 672 -80 efanouts.efanouts#96.SLNK
w 144 1739 -100 0 c#89 ecad20.ecad20#93.DIR 128 1600 64 1600 64 1728 272 1728 272 1984 160 1984 inhier.DIR.P
w 112 1771 -100 0 c#90 ecad20.ecad20#93.ICID 128 1568 32 1568 32 1760 240 1760 240 1856 160 1856 inhier.ICID.P
w 384 1739 -100 0 c#91 ecad20.ecad20#93.VAL 448 1600 512 1600 512 1728 304 1728 304 1984 448 1984 outhier.VAL.p
w 416 1771 -100 0 c#92 ecad20.ecad20#93.MESS 448 1568 544 1568 544 1760 336 1760 336 1856 448 1856 outhier.MESS.p
s 2000 -80 100 1536 F
s 2224 -80 100 1792 Added wavelength SAD update
s 2464 -80 100 1536 WNR
s 2560 -80 100 1536 2003/09/06
s 1952 976 100 0 central wavelength
s 2000 16 100 1536 C
s 2224 16 100 1792 Mark obsSetup on preset
s 2464 16 100 1536 WNR
s 2560 16 100 1536 2002/07/14
s 576 320 100 0 lambdaMin
s -128 768 100 0 window
s -128 832 100 0 slit
s -128 896 100 0 sector
s -128 960 100 0 lyot
s -128 1024 100 0 lens
s -128 1088 100 0 wavelength
s -128 1152 100 0 grating
s -128 1216 100 0 filter
s -128 1280 100 0 aperture
s -128 1344 100 0 imaging
s -128 1408 100 0 camera
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 Trecs instrumentSetup Command
s 2320 -240 100 1792 Rev: F
s 2096 -240 100 1792 2003/09/06
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsInsSetup.sch
s 1936 1392 100 0 window name
s 1936 1776 100 0 aperture name
s 1936 1712 100 0 grating name
s 1936 1648 100 0 lyot stop name
s 1936 1584 100 0 pupil name
s 1936 1520 100 0 sector name
s 1936 1456 100 0 slit name
s 1952 464 100 0 window pos
s 1952 1040 100 0 aperture pos
s 1952 912 100 0 filter 1 pos
s 1952 848 100 0 filter 2 pos
s 1952 784 100 0 grating pos
s 1952 720 100 0 lyot stop pos
s 1952 656 100 0 pupil pos
s 1952 592 100 0 sector pos
s 1952 528 100 0 slit pos
s 576 1424 100 0 camera mode
s 576 1360 100 0 imaging mode
s 1936 1840 100 0 filter name
s 1936 1904 100 0 imaging mode
s 576 528 100 0 slit name
s 576 592 100 0 sector name
s 576 656 100 0 pupil name
s 576 720 100 0 lyot stop name
s 576 784 100 0 grating name
s 576 848 100 0 filter 2 name
s 576 912 100 0 filter 1 name
s 576 1040 100 0 aperture name
s 576 464 100 0 window name
s 576 1104 100 0 central wavelength
s 576 1168 100 0 filter Name
s 576 384 100 0 throughput
s -128 576 100 0 lens override
s -128 640 100 0 filter override
s -128 704 100 0 aperture override
s -128 512 100 0 window override
s 2000 80 100 1536 A
s 2000 48 100 1536 B
s 2144 80 100 1536 Initial Layout
s 2128 48 100 1536 Rework CAD inputs
s 2464 80 100 1536 WNR
s 2464 48 100 1536 WNR
s 2560 80 100 1536 2000/12/05
s 2560 48 100 1536 2001/02/03
s 576 256 100 0 lambdaMax
s 2000 -16 100 1536 D
s 2224 -16 100 1792 Correct CAD input labels
s 2464 -16 100 1536 WNR
s 2560 -16 100 1536 2002/09/10
s 1936 1968 100 0 camera mode
s 2000 -48 100 1536 E
s 2224 -48 100 1792 Changed SAD outputs
s 2464 -48 100 1536 WNR
s 2560 -48 100 1536 2003/02/17
[cell use]
use changeBar 1984 -121 100 0 changeBar#404
xform 0 2336 -80
use hwout 2096 919 100 0 hwout#403
xform 0 2192 960
p 2320 960 100 0 -1 val(outp):$(sad)wavelength.VAL PP NMS
use changeBar 1984 -25 100 0 changeBar#395
xform 0 2336 16
use changeBar 1984 39 100 0 changeBar#382
xform 0 2336 80
use changeBar 1984 7 100 0 changeBar#383
xform 0 2336 48
use changeBar 1984 -57 100 0 changeBar#396
xform 0 2336 -16
use changeBar 1984 -89 100 0 changeBar#400
xform 0 2336 -48
use hwout 2000 103 100 0 hwout#388
xform 0 2096 144
p 2224 144 100 0 -1 val(outp):$(top)observationSetup.DIR PP NMS
use hwout 2080 983 100 0 hwout#131
xform 0 2176 1024
p 2304 1024 100 0 -1 val(outp):$(top)cc:aprtrWhl:namedPos.A
use hwout 2080 855 100 0 hwout#133
xform 0 2176 896
p 2304 896 100 0 -1 val(outp):$(top)cc:fltrWhl1:namedPos.A
use hwout 2080 791 100 0 hwout#134
xform 0 2176 832
p 2304 832 100 0 -1 val(outp):$(top)cc:fltrWhl2:namedPos.A
use hwout 2080 727 100 0 hwout#135
xform 0 2176 768
p 2304 768 100 0 -1 val(outp):$(top)cc:grating:namedPos.A
use hwout 2080 663 100 0 hwout#136
xform 0 2176 704
p 2304 704 100 0 -1 val(outp):$(top)cc:lyotWhl:namedPos.A
use hwout 2080 599 100 0 hwout#137
xform 0 2176 640
p 2304 640 100 0 -1 val(outp):$(top)cc:pplImg:namedPos.A
use hwout 2080 535 100 0 hwout#138
xform 0 2176 576
p 2304 576 100 0 -1 val(outp):$(top)cc:sectWhl:namedPos.A
use hwout 2080 471 100 0 hwout#139
xform 0 2176 512
p 2304 512 100 0 -1 val(outp):$(top)cc:slitWhl:namedPos.A
use hwout 2080 407 100 0 hwout#140
xform 0 2176 448
p 2304 448 100 0 -1 val(outp):$(top)cc:winChngr:namedPos.A
use hwout 2080 1719 100 0 hwout#178
xform 0 2176 1760
p 2304 1760 100 0 -1 val(outp):$(sad)apertureName.VAL PP NMS
use hwout 2080 1847 100 0 hwout#346
xform 0 2176 1888
p 2304 1888 100 0 -1 val(outp):$(sad)imagingMode.VAL PP NMS
use hwout 2080 1783 100 0 hwout#347
xform 0 2176 1824
p 2304 1824 100 0 -1 val(outp):$(sad)filterName.VAL PP NMS
use hwout 2080 1655 100 0 hwout#348
xform 0 2176 1696
p 2304 1696 100 0 -1 val(outp):$(sad)gratingName.VAL PP NMS
use hwout 2080 1591 100 0 hwout#349
xform 0 2176 1632
p 2304 1632 100 0 -1 val(outp):$(sad)lyotName.VAL PP NMS
use hwout 2080 1527 100 0 hwout#350
xform 0 2176 1568
p 2304 1568 100 0 -1 val(outp):$(sad)lensName.VAL PP NMS
use hwout 2080 1463 100 0 hwout#351
xform 0 2176 1504
p 2304 1504 100 0 -1 val(outp):$(sad)sectorName.VAL PP NMS
use hwout 2080 1399 100 0 hwout#352
xform 0 2176 1440
p 2304 1440 100 0 -1 val(outp):$(sad)slitName.VAL PP NMS
use hwout 2080 1335 100 0 hwout#353
xform 0 2176 1376
p 2304 1376 100 0 -1 val(outp):$(sad)windowName.VAL PP NMS
use hwout 2080 1911 100 0 hwout#399
xform 0 2176 1952
p 2304 1952 100 0 -1 val(outp):$(sad)cameraMode.VAL PP NMS
use hwin 800 311 100 0 hwin#391
xform 0 896 352
p 784 384 100 0 -1 val(in):$(CAD_MARK)
use hwin -160 119 100 0 hwin#380
xform 0 -64 160
p -416 160 100 0 -1 val(in):$(top)state
use hwin -160 375 100 0 hwin#370
xform 0 -64 416
p -416 416 100 0 -1 val(in):$(top)loRes10Blocker
use hwin -160 311 100 0 hwin#371
xform 0 -64 352
p -416 352 100 0 -1 val(in):$(top)loRes20Blocker
use hwin -160 247 100 0 hwin#372
xform 0 -64 288
p -416 288 100 0 -1 val(in):$(top)hiRes10Blocker
use hwin -160 183 100 0 hwin#373
xform 0 -64 224
p -416 224 100 0 -1 val(in):$(top)rhTooHigh
use elongouts 1024 231 100 0 elongouts#387
xform 0 1152 320
p 976 224 100 0 1 name:$(top)obsSetupAutomark
p 1280 288 75 768 -1 pproc(OUT):PP
use egenSub 1552 263 100 0 egenSub#289
xform 0 1696 688
p 1329 37 100 0 0 FTA:STRING
p 1329 37 100 0 0 FTB:STRING
p 1329 5 100 0 0 FTC:STRING
p 1329 -27 100 0 0 FTD:STRING
p 1329 -59 100 0 0 FTE:STRING
p 1329 -123 100 0 0 FTF:STRING
p 1329 -123 100 0 0 FTG:STRING
p 1329 -155 100 0 0 FTH:STRING
p 1329 -187 100 0 0 FTI:STRING
p 1329 -219 100 0 0 FTJ:STRING
p 1329 37 100 0 0 FTVA:STRING
p 1329 37 100 0 0 FTVB:STRING
p 1329 5 100 0 0 FTVC:STRING
p 1329 -27 100 0 0 FTVD:STRING
p 1329 -59 100 0 0 FTVE:STRING
p 1329 -123 100 0 0 FTVF:STRING
p 1329 -123 100 0 0 FTVG:STRING
p 1329 -155 100 0 0 FTVH:STRING
p 1329 -187 100 0 0 FTVI:STRING
p 1329 -219 100 0 0 FTVJ:STRING
p 1616 224 100 0 1 INAM:trecsIsNullGInit
p 1616 192 100 0 1 SNAM:trecsIsTranslateGProcess
p 1792 256 100 1024 1 name:$(top)insSetupTranslateG
p 1840 970 75 0 -1 pproc(OUTB):PP
use egenSub 1536 1191 100 0 egenSub#319
xform 0 1680 1616
p 1313 965 100 0 0 FTA:STRING
p 1313 965 100 0 0 FTB:STRING
p 1313 933 100 0 0 FTC:STRING
p 1313 901 100 0 0 FTD:STRING
p 1313 869 100 0 0 FTE:STRING
p 1313 805 100 0 0 FTF:STRING
p 1313 805 100 0 0 FTG:STRING
p 1313 773 100 0 0 FTH:STRING
p 1313 741 100 0 0 FTI:STRING
p 1313 709 100 0 0 FTJ:STRING
p 1313 965 100 0 0 FTVA:STRING
p 1313 965 100 0 0 FTVB:STRING
p 1313 933 100 0 0 FTVC:STRING
p 1313 901 100 0 0 FTVD:STRING
p 1313 869 100 0 0 FTVE:STRING
p 1313 805 100 0 0 FTVF:STRING
p 1313 805 100 0 0 FTVG:STRING
p 1313 773 100 0 0 FTVH:STRING
p 1313 741 100 0 0 FTVI:STRING
p 1313 709 100 0 0 FTVJ:STRING
p 1600 1152 100 0 1 INAM:trecsIsNullGInit
p 1600 1120 100 0 1 SNAM:trecsIsCopyGProcess
p 1744 1184 100 1024 1 name:$(top)insSetupCopyG
p 1824 1962 75 0 -1 pproc(OUTA):PP
p 1824 1898 75 0 -1 pproc(OUTB):PP
p 1824 1834 75 0 -1 pproc(OUTC):PP
p 1824 1770 75 0 -1 pproc(OUTD):PP
p 1824 1706 75 0 -1 pproc(OUTE):PP
p 1824 1642 75 0 -1 pproc(OUTF):PP
p 1824 1578 75 0 -1 pproc(OUTG):PP
p 1824 1514 75 0 -1 pproc(OUTH):PP
p 1824 1450 75 0 -1 pproc(OUTI):PP
p 1824 1386 75 0 -1 pproc(OUTJ):PP
use efanouts 672 -217 100 0 efanouts#96
xform 0 792 -64
p 816 -224 100 1024 1 name:$(top)insSetupFanout
use ecad20 128 -121 100 0 ecad20#93
xform 0 288 768
p 192 -160 100 0 1 INAM:trecsIsNullInit
p 192 -192 100 0 1 SNAM:trecsIsInsSetupProcess
p 352 -128 100 1024 1 name:$(top)instrumentSetup
p 448 10 75 0 -1 pproc(PLNK):PP
p 448 -22 75 0 -1 pproc(STLK):PP
use outhier 464 1856 100 0 MESS
xform 0 432 1856
use outhier 464 1984 100 0 VAL
xform 0 432 1984
use inhier 80 1856 100 0 ICID
xform 0 160 1856
use inhier 96 1984 100 0 DIR
xform 0 160 1984
use trecsSubSysCommand 1120 -217 100 0 trecsSubSysCommand#56
xform 0 1264 -32
p 1120 -256 100 0 1 setCommand:cmd insSetup
p 1120 -224 100 0 1 setSystem:sys cc
use ecars 1600 -217 100 0 ecars#53
xform 0 1760 -48
p 1760 -224 100 1024 1 name:$(top)instrumentSetupC
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsInsSetup.sch,v 0.5 2003/09/08 19:55:38 varosi beta $
