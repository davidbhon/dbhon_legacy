[schematic2]
uniq 43
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-Recs SAD Overview
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 trecsSadMain.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2000/11/18
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Added EC status
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/01/25
[cell use]
use trecsSadEcEngineering 896 135 100 0 trecsSadEcEngineering#42
xform 0 1040 288
use trecsSadEcStatus 896 487 100 0 trecsSadEcStatus#41
xform 0 1040 640
use trecsSadEcConfig 896 839 100 0 trecsSadEcConfig#40
xform 0 1040 992
use changeBar 1984 2023 100 0 changeBar#38
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#39
xform 0 2336 2032
use trecsSadWcs 2048 1479 100 0 trecsSadWcs#37
xform 0 2192 1632
use trecsSadDcStatus 1664 487 100 0 trecsSadDcStatus#36
xform 0 1808 640
use trecsSadCcStatus 128 487 100 0 trecsSadCcStatus#35
xform 0 272 640
use trecsSadCcEngineering 128 135 100 0 trecsSadCcEngineering#34
xform 0 272 288
use trecsSadCcConfig 128 839 100 0 trecsSadCcConfig#33
xform 0 272 992
use trecsSadDcEngineering 1664 135 100 0 trecsSadDcEngineering#32
xform 0 1808 288
use trecsSadDcConfig 1664 839 100 0 trecsSadDcConfig#31
xform 0 1808 992
use trecsSadInsStatus 1152 1223 100 0 trecsSadInsStatus#30
xform 0 1296 1376
use trecsSadObsStatus 640 1223 100 0 trecsSadObsStatus#29
xform 0 784 1376
use trecsSadInsConfig 1152 1575 100 0 trecsSadInsConfig#28
xform 0 1296 1728
use trecsSadObsConfig 640 1575 100 0 trecsSadObsConfig#27
xform 0 784 1728
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadMain.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
