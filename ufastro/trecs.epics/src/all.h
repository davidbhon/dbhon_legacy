
/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * trecsBogusLookup.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument dummy lookup
 * table module
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2001/01/14  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


#define NAME_SIZE  16

typedef struct
{
  char metaName[NAME_SIZE];
  char cameraMode[NAME_SIZE];
  char imagingMode[NAME_SIZE];
  char aperture[NAME_SIZE];
  char filter[NAME_SIZE];
  char grating[NAME_SIZE];
  char wavelength[NAME_SIZE];
  char lyot[NAME_SIZE];
  char lens[NAME_SIZE];
  char sector[NAME_SIZE];
  char slit[NAME_SIZE];
  char window[NAME_SIZE];
  char filter1[NAME_SIZE];
  char filter2[NAME_SIZE];
  char clamp[NAME_SIZE];
  float efficiency;
}
TRECS_CONFIG;

/*
 *
 *  Public function prototypes
 *
 */

long trecsInitBogusLookup (void);
long trecsReloadBogusLookup (void);
long trecsBogusMetaLookup (TRECS_CONFIG *);
long trecsBogusFilterLookup (TRECS_CONFIG *);
long trecsBogusNameLookup (char *, char *, float *);



/*
 *
 *  Header for trecsCcAgentLayer.h
 *
 */




/*
 *
 *  Public function prototypes
 *
 */

long ccCadInit (cadRecord * pcr);
long ccCadSend (cadRecord * pcr);
long ccCadNoSend (cadRecord * pcr);

long ccGensubInit (genSubRecord * pgs);
long ccGensubSend (genSubRecord * pgs);



/*
 *
 *  Header file for ccAgentSim.c module.
 *
 */

#define MAX_STRING_SIZE 40

/*
 *
 *  Cc agent command structure
 *
 */

typedef struct
{
  char commandName[MAX_STRING_SIZE];
  char carName[MAX_STRING_SIZE];
  char attributeA[MAX_STRING_SIZE];
  char attributeB[MAX_STRING_SIZE];
  char attributeC[MAX_STRING_SIZE];
  char attributeD[MAX_STRING_SIZE];
  char attributeE[MAX_STRING_SIZE];
  char attributeF[MAX_STRING_SIZE];
  char attributeG[MAX_STRING_SIZE];
  char attributeH[MAX_STRING_SIZE];
  char attributeI[MAX_STRING_SIZE];
  char attributeJ[MAX_STRING_SIZE];
  long executionTime;
}
ccAgentCommand;

/*
 *
 *  Public function prototypes
 *
 */

long initCcAgentSim (MSG_Q_ID ccCommandQueue);



/*
 *
 *  Header file for dcAgentSim.c module.
 *
 */

#define MAX_STRING_SIZE 40

/*
 *
 *  Dc agent command structure
 *
 */

typedef struct
{
  char commandName[MAX_STRING_SIZE];
  char carName[MAX_STRING_SIZE];
  char attributeA[MAX_STRING_SIZE];
  char attributeB[MAX_STRING_SIZE];
  char attributeC[MAX_STRING_SIZE];
  char attributeD[MAX_STRING_SIZE];
  char attributeE[MAX_STRING_SIZE];
  char attributeF[MAX_STRING_SIZE];
  char attributeG[MAX_STRING_SIZE];
  char attributeH[MAX_STRING_SIZE];
  char attributeI[MAX_STRING_SIZE];
  char attributeJ[MAX_STRING_SIZE];
  char attributeK[MAX_STRING_SIZE];
  char attributeL[MAX_STRING_SIZE];
  char attributeM[MAX_STRING_SIZE];
  char attributeN[MAX_STRING_SIZE];
  char attributeO[MAX_STRING_SIZE];
  char attributeP[MAX_STRING_SIZE];
  long executionTime;
}
dcAgentCommand;

/*
 *
 *  Public function prototypes
 *
 */

long initDcAgentSim (MSG_Q_ID dcCommandQueue);


/*
 *
 *  Header for trecsDcCadSim.h
 *
 */




/*
 *
 *  Public function prototypes
 *
 */

long dcCadInit (cadRecord * pcr);
long dcCadProcess (cadRecord * pcr);

/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * trecsIsCad.h
 *
 * PURPOSE
 * -------
 * Declare public CAD record support functions
 * 
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/11  WNR  Initial coding
 */

/* INDENT ON */

/* ===================================================================== */





/*
 *
 *  Public function prototypes
 *
 */

long trecsIsAbortProcess (cadRecord * pcr);
long trecsIsDataModeProcess (cadRecord * pcr);
long trecsIsDatumProcess (cadRecord * pcr);
long trecsIsDebugProcess (cadRecord * pcr);
long trecsIsInitProcess (cadRecord * pcr);
long trecsIsInsSetupProcess (cadRecord * pcr);
long trecsIsNullInit (cadRecord * pcr);
long trecsIsObsSetupProcess (cadRecord * pcr);
long trecsIsObserveProcess (cadRecord * pcr);
long trecsIsParkProcess (cadRecord * pcr);
long trecsIsRebootInit (cadRecord * pcr);
long trecsIsRebootProcess (cadRecord * pcr);
long trecsIsSetDhsInfoProcess (cadRecord * pcr);
long trecsIsSetWcsProcess (cadRecord * pcr);
long trecsIsStopProcess (cadRecord * pcr);
long trecsIsTestProcess (cadRecord * pcr);

/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * trecsIsGensub.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument sequencer gensub
 * record support code.
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/12  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


/*
 *
 *  Public function prototypes
 *
 */

long trecsCmdCombineGProcess (genSubRecord * pgs);
long trecsIsCopyGProcess (genSubRecord * pgs);
long trecsIsInitGInit (genSubRecord * pgs);
long trecsIsInitGProcess (genSubRecord * pgs);
long trecsIsInsSetupCopy (genSubRecord * pgs);
long trecsIsInsSetupTranslate (genSubRecord * pgs);
long trecsIsNullGInit (genSubRecord * pgs);
long trecsIsRebootGInit (genSubRecord * pgs);
long trecsIsRebootGProcess (genSubRecord * pgs);
long trecsIsSubSysCombineProcess (genSubRecord * pgs);
long trecsIsTranslateGProcess (genSubRecord * pgs);
long trecsStateGProcess (genSubRecord * pgs);




/*
 *
 *  Header file for tcAgentSim.c module.
 *
 */

#define MAX_STRING_SIZE 40

/*
 *
 *  Tc agent command structure
 *
 */

typedef struct
{
  char commandName[MAX_STRING_SIZE];
  char carName[MAX_STRING_SIZE];
  char attributeA[MAX_STRING_SIZE];
  char attributeE[MAX_STRING_SIZE];
  long executionTime;
}
tcAgentCommand;

/*
 *
 *  Public function prototypes
 *
 */

long initTcAgentSim (MSG_Q_ID tcCommandQueue);


/*
 *
 *  Header for trecsTcCadSim.h
 *
 */




/*
 *
 *  Public function prototypes
 *
 */

long tcCadInit (cadRecord * pcr);
long tcCadProcess (cadRecord * pcr);
