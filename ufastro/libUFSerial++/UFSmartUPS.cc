#include <cstdio>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>

#include "UFSmartUPS.h"

UFSmartUPS::UFSmartUPS() : UFClientSocket(),_params(),_message(""),_alarm(false) { }

void UFSmartUPS::reset() {
  _message = ""; _alarm = false; 
}

int UFSmartUPS::requestPage(const char * pagename, const char * hostname) {
  int sent = 0;
  if (isAlarm()) return -1; // If alarm condition, bail out quickly
  /* create and send the http GET request */
  string buffer = "GET ";
  buffer+=pagename;
  buffer+=" HTTP/1.1\r\nHost: ";
  buffer+=hostname;
  buffer+="\r\n\r\n";
  sent = sendHTTP( buffer );
  if (sent <= 0) _alarm = true; // Some problem sending over the socket, sound the alarm
  return sent;
}

int UFSmartUPS::postLogon(const char * username, const char * password, const char * hostname) {
  int sent = 0;
  if (isAlarm()) return -1; // If alarm condition, bail out quickly
  /* try to post username and password */
  string buffer = "POST /PROCESS-LOGON HTTP/1.1\r\nHost: ";
  buffer+=hostname;
  buffer+="\r\nUser-Agent: Mozilla/4.7(X11;Linux)\r\nAccept: */*\r\nContent-type: application/x-www-form-urlencoded\r\nContent-length: 28\r\n\r\nUserName=";
  buffer+=username;
  buffer+="&PassWord=";
  buffer+=password;
  buffer+="\r\n\r\n";
  sent =  sendHTTP( buffer );
  if (sent <= 0) _alarm = true; // Some problem sending over the socket, sound the alarm
  return sent;
}

int UFSmartUPS::getResponse() {
  int count = 0;
  _message = ""; // reset message
  if (isAlarm()) return -1; // If alarm condition, bail out quickly
  count = recvHTTP(_message);
  if (count <= 0) {
    _alarm = true;
    return -1;
  }
  return count;
}

void UFSmartUPS::parseMessage() {
  unsigned int i=0,j=0;
  if (isAlarm()) return; // If alarm condition, bail out quickly
  if ( (i = _message.find("Input Voltage:",0)) != string::npos)
    if ( (j = _message.find("V",i+92)) != string::npos)
      _params.inpVolt = atoi(_message.substr(i+92,i+92-j).c_str());
  if ( (i = _message.find("Battery Voltage:",0)) != string::npos)
    if ( (j = _message.find("V",i+94)) != string::npos)
      _params.battVolt = (double)atof(_message.substr(i+94,i+94-j).c_str());
  if ( (i = _message.find("Input Frequency:",0)) != string::npos)
    if ( (j = _message.find("Hz",i+94)) != string::npos)
      _params.inpFreq = (double)atof(_message.substr(i+94,i+94-j).c_str());
  if ( (i = _message.find("Battery Capacity:",0)) != string::npos)
    if ( (j = _message.find("%",i+95)) != string::npos)
      _params.battCap = atoi(_message.substr(i+95,i+95-j).c_str());
  if ( (i = _message.find("Load Percent:",0)) != string::npos)
    if ( (j = _message.find("%",i+91)) != string::npos)
      _params.load = atoi(_message.substr(i+91,i+91-j).c_str());
  if (_params.battCap <= 20) //sound the alarm
    _alarm = true;
}

void UFSmartUPS::printParams() {
  cout.setf(ios::fixed);
  cout.precision(1);
  cout << "Input Voltage:    " << _params.inpVolt  << " V" << endl 
       << "Battery Voltage:  " << _params.battVolt << " V" << endl 
       << "Input Frequency:  " << _params.inpFreq  << " Hz"<< endl  
       << "Battery Capacity: " << _params.battCap  << " %" << endl
       << "Load Percent:     " << _params.load     << " %" << endl;
}

//functions to inspect message
bool UFSmartUPS::loginRequired() {
  if (_message.find("Unauthorised",0) != string::npos)
    return true;
  else return false;
}
bool UFSmartUPS::loginFailed() {
  if (_message.find("Login Error",0) != string::npos)
    return true;
  else return false;
}

// static funcs;
int UFSmartUPS::fetchBatteryHealth(const string & host_or_IP) {
  static UFSmartUPS us;
  if (!us.validConnection()) {
    if ((us.connect(host_or_IP,80,false)) < 0) {
      fprintf(stderr,"Could not connect to %s on port 80\n",host_or_IP.c_str());
      return -1;
    }
    if (!us.validConnection()) {
      fprintf(stderr,"Invalid Connection\n");
      return -1;
    }
  }
  us.setTimeOut(10);
  us.requestPage("/status.htm",host_or_IP.c_str());
  us.getResponse();
  if (us.loginRequired()) {
    us.postLogon("admin","1234",host_or_IP.c_str());
    us.getResponse();
    if (us.loginFailed()) {
      fprintf(stderr,"Login failed.  Aborting...\n");
      return -1;
    } 
    us.requestPage("/status.htm",host_or_IP.c_str());
    us.getResponse();
  }
  us.parseMessage();
  if (us.isAlarm()) return -2;
  return us._params.battCap;
}

int UFSmartUPS::main(int argc, char ** argv) {
  //char HOST_NAME[] = "192.168.111.231";
  char HOST_NAME[] = "192.168.111.231";
  int i = 0;
  while (true) { // enter polling loop
    i = UFSmartUPS::fetchBatteryHealth(HOST_NAME);
    if (i == -2) {
      cout << "Alarm!!" << endl;
      return i;
    } else if (i == -1) {
      cout << "Bad Connection" << endl;
      return i;
    }
    cout << i << " " << time(NULL) << endl;
    sleep(10);//sleep for 10 seconds
  }
  return i;
}
