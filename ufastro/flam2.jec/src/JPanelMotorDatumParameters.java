package uffjec;
   import javax.swing.*;
   import java.awt.*;


//===============================================================================
/**
 *Datum Parameters tabbed pane
 */
public class JPanelMotorDatumParameters extends JPanel {
	static final long serialVersionUID = 0;
	
    /**
     *Default constructor
     *@param fjecmotors FJECMotor[]: Array of FJECMotors
     */
    public JPanelMotorDatumParameters(FJECMotor [] fjecmotors) {
	JPanel internalPanel = new JPanel();
	internalPanel.setLayout(new GridLayout(0,1));
	internalPanel.add(FJECMotor.getMotorDatumParameterLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) {
	    internalPanel.add(fjecmotors[i].getMotorDatumParameterPanel());	  
	    //JPanel j = new JPanel();
	    //final FJECMotor [] jcm = fjecmotors;
	    //final int k = i;
	    //j.add(new JButton("Connect to Agent") {
	    //  public void actionPerformed(ActionEvent ae) {
	    //      jcm[k].connect();
	    //  }
	    //  });
	    //add(j);
	}
	setLayout(new RatioLayout());


	JPanel [] barPan = new JPanel[7];
        //JPanel [] barPan = new JPanel[5];
	double xVal = 0.2725;
	//double xVal = 0.17+0.1021*1.34;
	for (int i=0; i<barPan.length; i++) {
	    barPan[i] = new JPanel(){
	    	static final long serialVersionUID = 0;
		    public void paint(Graphics g) {
			g.setColor(Color.black);
			g.fillRect(0,0,getWidth(),getHeight());
		    }
		};
	    add(xVal+",0.095;0.005,0.75",barPan[i]);
	    //xVal += 0.1021*1.34;
	    xVal += 0.1021;
	}
	add("0.01,0.01;0.99,0.85",internalPanel);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("cc:"));
    }
      
}
