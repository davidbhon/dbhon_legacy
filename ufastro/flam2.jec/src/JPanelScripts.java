package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import javaUFLib.*;

public class JPanelScripts extends JPanel {
	
	static final long serialVersionUID = 0;
	
	private Vector recvec,pvec;
	private JList recordList;
	private JTextArea runArea;
	private JList runList;
	private JButton recordButton;
	private JButton createScriptButton;
	private JButton monitoredDelayButton;
	private String script_filename;
	private Process psh;
	private JScrollPane jsa;
	private JScrollPane jsb;
	
	private int mousePressedIndex;
	
	private JFrame scriptsPopup;
	
	private void recordListMousePressed() {
		mousePressedIndex = recordList.getSelectedIndex();
	}
	
	private void recordListMouseReleased() {
		int x = recordList.getSelectedIndex();
		if (x == mousePressedIndex) return;
		Object o = recvec.remove(mousePressedIndex);
		recvec.insertElementAt(o,x);
		recordList.setListData(recvec);
	}
	
	public void detachRecordListPanel() {
		JPanel retPan = new JPanel();
		retPan.setLayout(new BorderLayout());
		retPan.add(jsa);
		scriptsPopup = new JFrame("Scripts");
		scriptsPopup.setContentPane(retPan);
		scriptsPopup.setSize(400,400);
		scriptsPopup.setVisible(true);
		EPICS.recordScript = true;
	}
	
	public void attachRecordListPanel(){
		add("0.10,0.20;0.35,0.40",jsa);
		scriptsPopup.setVisible(false);
		scriptsPopup = null;
	}
	
	public JPanelScripts() {
		setLayout(new RatioLayout());
		script_filename = "EPICS_script_0.pl";
		recordList = new JList();
		recordList.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent me) {}
			public void mouseEntered(MouseEvent me) {}
			public void mouseExited(MouseEvent me) {}
			public void mousePressed(MouseEvent me) {
				recordListMousePressed();
			}
			public void mouseReleased(MouseEvent me) {
				recordListMouseReleased();
			}
		});
		//	runList.addMouseMotionListener(new MouseMotionListener(){
		//public void mouseMoved(MouseEvent me) {}
		//public void mouseDragged(MouseEvent me) {}
		//  });
		runList = new JList();
		runArea = new JTextArea();
		recvec = new Vector();
		pvec = new Vector();
		clearRunScript();
		recordButton = new JButton("Turn on script recording");
		EPICS.recordScript = false;
		jsa = new JScrollPane(recordList);
		jsb = new JScrollPane(runList);
		
		recordButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				recordButtonActionPerformed();
			}
		});
		createScriptButton = new JButton("Create ufca Perl Script");
		createScriptButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				createScriptButtonActionPerformed();
			}
		});
		monitoredDelayButton = new JButton("Add Monitored Delay");
		monitoredDelayButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				monitoredDelayButtonActionPerformed();
			}
		});
		JPanel recButtonPanel = new JPanel();
		recButtonPanel.setLayout(new RatioLayout());
		JButton recClearButton = new JButton("Clear All");
		recClearButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				recvec.clear();
				recordList.setListData(recvec);
			}
		});
		JButton recDelButton = new JButton("Clear Selected");
		recDelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				int [] sel = recordList.getSelectedIndices();
				for (int i=0; i<sel.length; i++)
					recvec.removeElementAt(sel[i]-i);
				recordList.setListData(recvec);
			}
		});
		recButtonPanel.add("0.01,0.01;0.49,0.25",recClearButton);
		recButtonPanel.add("0.51,0.01;0.49,0.25",recDelButton);
		recButtonPanel.add("0.01,0.31;0.49,0.25",createScriptButton);
		recButtonPanel.add("0.51,0.31;0.49,0.25",monitoredDelayButton);
		
		JPanel runButtonPanel = new JPanel();
		runButtonPanel.setLayout(new RatioLayout());
		JButton runClearButton = new JButton("Clear");
		runClearButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				clearRunScript();
			}
		});
		final JButton runSaveButton = new JButton("Save");
		runSaveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				saveScript();
			}
		});
		final JButton runLoadButton = new JButton("Load");
		runLoadButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				loadScript();
			}
		});
		final JButton runRunButton = new JButton("Run Selected");
		runRunButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				runScript();
			}
		});
		final JButton runRunAllButton = new JButton("Run All");
		runRunAllButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				int [] sel = new int[pvec.size()];
				for (int i=0; i<sel.length; i++) sel[i]=i;
				runList.setSelectedIndices(sel);
				runScript();
			}
		});
		final JRadioButton editModeButton = new JRadioButton("Edit Mode");
		editModeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				runSaveButton.setEnabled(editModeButton.isSelected());
				runLoadButton.setEnabled(editModeButton.isSelected());
				runRunButton.setEnabled(!editModeButton.isSelected());
				runRunAllButton.setEnabled(!editModeButton.isSelected());
				copyEditToRun();
				if (editModeButton.isSelected()){
					jsb.setViewportView(runArea);
				} else {
					jsb.setViewportView(runList);
				}
			}
		});
		final JRadioButton runModeButton = new JRadioButton("Run Mode",true);
		runModeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				runSaveButton.setEnabled(editModeButton.isSelected());
				runLoadButton.setEnabled(editModeButton.isSelected());
				runRunButton.setEnabled(!editModeButton.isSelected());
				runRunAllButton.setEnabled(!editModeButton.isSelected());
				copyEditToRun();
				if (runModeButton.isSelected()) {
					jsb.setViewportView(runList);
				} else {
					jsb.setViewportView(runArea);
				}
			}
		});
		runSaveButton.setEnabled(false);
		runLoadButton.setEnabled(false);
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(editModeButton);
		buttonGroup.add(runModeButton);
		JPanel radioPanel = new JPanel();
		radioPanel.setLayout(new RatioLayout());
		radioPanel.add("0.01,0.01;0.49,0.99",runModeButton);
		radioPanel.add("0.51,0.01;0.49,0.99",editModeButton);
		
		runButtonPanel.add("0.01,0.01;0.32,0.25",runClearButton);
		runButtonPanel.add("0.33,0.01;0.32,0.25",runSaveButton);
		runButtonPanel.add("0.66,0.01;0.32,0.25",runLoadButton);
		runButtonPanel.add("0.01,0.30;0.49,0.25",runRunButton);
		runButtonPanel.add("0.51,0.30;0.49,0.25",runRunAllButton);
		
		final JButton tachButton = new JButton("Detach");
		tachButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				if (tachButton.getText().equals("Detach")){
					tachButton.setText("Re-attach");
					detachRecordListPanel();
					repaint();
				}else{
					tachButton.setText("Detach");
					attachRecordListPanel();
				}
			}
		});
		
		add("0.10,0.10;0.35,0.08",new JLabel("Recorded Commands",JLabel.CENTER));
		add("0.10,0.20;0.35,0.40",jsa);
		add("0.01,0.20;0.08,0.08",tachButton);
		add("0.50,0.10;0.35,0.08",new JLabel("Created Script",JLabel.CENTER));
		add("0.50,0.20;0.35,0.40",jsb);
		add("0.50,0.60;0.35,0.08",radioPanel);
		add("0.10,0.70;0.35,0.20",recButtonPanel);
		add("0.50,0.70;0.35,0.20",runButtonPanel);
		add("0.10,0.90;0.20,0.08",recordButton);
	}
	
	public void destroyPsh()  {	
		if (psh != null)
			psh.destroy();
	}
	
	private void runScript(){
		try {
			UFExecCommand.getEnvVar("UFINSTALL");
			
			
			try {
				if (psh == null)
					psh = Runtime.getRuntime().exec("psh -F");
			} catch (IOException ioe) {
				System.err.println("JPanelScripts.runScript> Unable to open psh shell for scripting: "+
						ioe.toString());
			} 
			int [] sel = runList.getSelectedIndices();
			BufferedInputStream es = new BufferedInputStream(psh.getErrorStream());
			OutputStream os = psh.getOutputStream();
			
			if (sel.length == 1) {
				os.write((((String)pvec.elementAt(sel[0]))+"\n").getBytes());
				os.flush();
				Thread.sleep(100); // wait a little for any error messages
				if (es.available() > 0) {
					byte [] b  = new byte[es.available()];
					es.read(b,0,b.length);
					System.err.println("JPanelScripts.runScript (Error from psh)>  "+new String(b));
				}
				runList.setSelectedIndex(sel[0]+1);
			} else if (sel.length > 1) {
				
				String s = "";
				for (int i=0; i<sel.length; i++) s += (String)pvec.elementAt(sel[i])+"\n";
				//s += "exit\n";
				os.write(s.getBytes());
				os.flush();
				Thread.sleep(100); // wait a little for any error messages
				
				if (es.available() > 0) {
					byte [] b  = new byte[es.available()];
					es.read(b,0,b.length);
					System.err.println("JPanelScripts.runScript (Error from psh)>  "+new String(b));
				}
				/*
				 if (is.available() > 0) {
				 byte [] b  = new byte[is.available()];
				 is.read(b,0,b.length);
				 System.out.println(new String(b));
				 }
				 System.out.flush();
				 System.err.flush();
				 */
			} else {
				System.err.println("JPanelScripts.runScript> Nothing selected");
			}
		} catch (Exception e) {
			System.err.println("JPanelScripts.runScript> "+e.toString());
		}
		
	}
	
	private void copyEditToRun() {
		String s = runArea.getText();
		pvec.clear();
		while (s != null && s.length() > 1 && s.indexOf("\n") != -1) {
			pvec.add(s.substring(0,s.indexOf("\n")));
			s = s.substring(s.indexOf("\n")+1,s.length());
			//System.out.println(s);
		}
		if (s != null && s.length() > 0) 
			pvec.add(s);
		runList.setListData(pvec);
	}
	
	private void clearRunScript(){
		pvec.clear();
		pvec.add("#!/usr/local/bin/perl");
		pvec.add("use UFCA;");
		runList.setListData(pvec);	
		runArea.setText("#!/usr/local/bin/perl\n use UFCA;\n");
		
	}
	
	private void monitoredDelayButtonActionPerformed() {
		
	}
	
	private void loadScript() {
		runArea.setText("Scripidydoodah");
	}
	
	private void saveScript() {
		JFileChooser fc = new JFileChooser(fjec.data_path);
		ExtensionFilter extf = new ExtensionFilter (
				"EPICS Pezca Script Files", new String[] {".pl"});
		fc.setFileFilter (extf); // Initial filter setting
		fc.setDialogTitle("Save script data to");
		// try to update number of previous filename,
		// assuming that the last characters before the 
		// final extension represent a number
		int i = script_filename.lastIndexOf(".");
		int j = i;
		try {
			while (--j > 0)
				Integer.parseInt(script_filename.substring(j,i));
		} catch (NumberFormatException nfe) {}
		try {
			if (j > 0 && j < i-1)
				script_filename = script_filename.substring(0,j+1) +
				(Integer.parseInt(script_filename.substring(j+1,i))+1) +
				script_filename.substring(i,script_filename.length());
		} catch (NumberFormatException nfe) {}
		fc.setSelectedFile(new File(script_filename));
		fc.setApproveButtonText("Save");
		int returnVal = fc.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			script_filename = fc.getSelectedFile().getAbsolutePath();
			if (!script_filename.endsWith(".pl")) script_filename += ".pl";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(script_filename)));
				//for (int ii=0; ii<pvec.size(); ii++) {
				String s = runArea.getText();//(String)pvec.elementAt(ii);
				bw.write(s,0,s.length());
				bw.newLine();
				//}
				bw.close();
			} catch (IOException ioe) {
				System.err.println("JPanelScripts.createScriptButtonActionPerformed> "+ioe.toString());
			}
		}
	}
	
	public void createScriptButtonActionPerformed(){
		clearRunScript();
		for (int i=0; i<recvec.size(); i++) {
			StringTokenizer st = new StringTokenizer((String)recvec.elementAt(i)," ");
			if (st.countTokens() < 2) {
				System.err.println("JPanelScripts.createScriptButtonActionPerformed> Malformed "+
				"command line: not enough tokens");
			}
			String cmd = st.nextToken();
			String rec = st.nextToken();
			pvec.add("$pv = UFCA::connectPV(\""+rec+"\","+EPICS.TIMEOUT+" );");
			runArea.append("$pv = UFCA::connectPV(\""+rec+"\","+EPICS.TIMEOUT+" );\n");
			if (cmd.equals("put")) {
				if (st.countTokens() < 2) {
					System.err.println("JPanelScripts.createScriptButtonActionPerformed> Malformed "+
					"command line: not enough tokens");
				}
				st.nextToken();
				String val = st.nextToken();
				pvec.add("UFCA::putString($pv, \""+val+"\");");		
				runArea.append("UFCA::putString($pv, \""+val+"\");\n");		
			} else if (cmd.equals("get")) {
				pvec.add("$val = UFCA::getString($pv);");		
				runArea.append("$val = UFCA::getString($pv);\n");		
			} else {
				System.err.println("JPanelScripts.createScriptButtonActionPerformed> Unknown script "+
						"command: "+cmd);
			}	    
			
		}
		runList.setListData(pvec);
	}
	
	private void recordButtonActionPerformed() {
		EPICS.recordScript = !EPICS.recordScript;
		if (EPICS.recordScript)
			recordButton.setText("Turn off script recording");
		else
			recordButton.setText("Turn on script recording");
	}
	
	public void addLine(String line) {
		recvec.add(line);
		recordList.setListData(recvec);
	}
	
}
