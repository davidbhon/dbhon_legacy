package uffjec;

import javax.swing.*;

public class JPanelMaster extends JPanel {

	static final long serialVersionUID = 0;	
	
    JButton abortButton;
    JButton continueButton;
    JButton datumButton;
    JButton initButton;
    JButton observeButton;
    JButton parkButton;
    JButton rebootButton;    
    JButton stopButton;
    JButton testButton;
    

    public JPanelMaster() {
    }
    
    public static void main(String [] args) {
	JFrame j = new JFrame("JPanelMaster Unit Test");
	j.setSize(800,600);
	j.setContentPane(new JPanelMaster());
	j.setDefaultCloseOperation(3);
	j.setVisible(true);
    }

}
