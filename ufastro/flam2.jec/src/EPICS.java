package uffjec;

import ufjca.*;

//===============================================================================
/**
 * Class for storing record field information
 * Contains the rcsID and the prefix
 * Note that UFCAToolkit is contained in the UFCA module
 * (currently located in ufrcs/flam2.scripts/UFCA
 */
public class EPICS {
    public static final String rcsID = "$Name:  $ $Id: EPICS.java,v 0.30 2006/11/02 20:43:30 warner Exp $";
    static public String prefix = "null:";
    static public String OIWFSprefix = "null:";
    public static final int MARK = 0;
    public static final int CLEAR= 1;
    public static final int PRESET=2;
    public static final int START =3;
    public static final int IDLE = 0;
    public static final int BUSY = 2;
    public static final int ERROR= 3;
    public static final int NULL = Integer.MIN_VALUE;
    public static double TIMEOUT = fjecFrame.TIMEOUT;
    public static boolean recordScript;
    public static boolean nullDB,nullOIWFS;
    public static String applyPrefix;
    public static boolean PORTABLE;
    public static EPICSRecs recs;

//-------------------------------------------------------------------------------
/**
 *setup static variables that must exist inside a try/catch block
 */

    static public void setupStaticVars() {
	try {
	    if (!prefix.toLowerCase().equals("null") && !prefix.toLowerCase().equals("null:"))
		nullDB = false;
	    else
		nullDB = true;
	    if (!OIWFSprefix.toLowerCase().equals("null") && !OIWFSprefix.toLowerCase().equals("null:"))
		nullOIWFS = false;
	    else
		nullOIWFS = true;
	    recordScript = false;
	    PORTABLE = true;
	    applyPrefix = prefix;//+"eng:";
	    recs = new EPICSRecs(PORTABLE, prefix);
	} catch (Exception e) {
	    System.err.println("EPICS::setupStaticVars> Big Problem: " + e.toString());
	}
    }

    //---------------------------------------------------------------------------

    static  public void put(String rec,int val) {
	put(rec,val+"");
    }

//-------------------------------------------------------------------------------
  /**
   *Put the val into the rec Record field
   *@param rec String: record field
   *@param val String: value putting into the field
   */
  static  public void put(String rec, String val) {
      if (rec.endsWith("null")) return;
      if (recordScript) {
	  fjecFrame.jPanelScripts.addLine("put "+rec+" -- "+val);
      }
      if (nullDB && nullOIWFS) return;
      if (nullOIWFS && rec.startsWith(OIWFSprefix)) return;
      if (nullDB && rec.startsWith(prefix) && !rec.startsWith(OIWFSprefix)) return; 
      try {
	  UFCAToolkit.put(rec,val);
      }
      catch (Exception exp) {
	  System.err.println("EPICS::put> "+exp.toString() + " " + rec);
      }
  } //end of put

//-------------------------------------------------------------------------------
  /**
   *Returns the String of the value in the rec Record Field
   *@param rec String: Record field
   */
    static public String get(String rec) {
	if (recordScript) {
	    fjecFrame.jPanelScripts.addLine("get "+rec);
	}
	if (nullDB && nullOIWFS) return "null";
	if (nullOIWFS && rec.startsWith(OIWFSprefix)) return "null";
	if (nullDB && rec.startsWith(prefix) && !rec.startsWith(OIWFSprefix)) return "null"; 
	String val = "";
	try {
	    val = UFCAToolkit.get(rec);	    
	}
	catch (Exception exp) {
	    //fjecError.show(exp.toString() + " " + rec);
	    System.err.println("EPICS::get> "+exp.toString() + " " + rec);
	}
	return val;
    }
    
    static public boolean removeMonitor(String monitorRec,UFCAToolkit.MonitorListener listener) {
	if (nullDB && nullOIWFS) return true;
	if (nullOIWFS && monitorRec.startsWith(OIWFSprefix)) return true;
	if (nullDB && monitorRec.startsWith(prefix) && !monitorRec.startsWith(OIWFSprefix)) return true; 
	try {
	    return UFCAToolkit.removeMonitor(monitorRec,listener);
	} catch (Exception e) {
	    System.err.println("EPICS::addMonitor> Problem establishing monitor for recName "+
			       monitorRec+" : "+e.toString());
	    return false;
	}
    }

    static  public boolean addMonitor(String monitorRec,UFCAToolkit.MonitorListener listener) {
        //System.out.println(monitorRec);
	if (nullDB && nullOIWFS) return true;
	if (nullOIWFS && monitorRec.startsWith(OIWFSprefix)) return true;
	if (nullDB && monitorRec.startsWith(prefix) && !monitorRec.startsWith(OIWFSprefix)) return true; 
	try {
	    return UFCAToolkit.addMonitor(monitorRec,listener);
	} catch (Exception e) {
	    System.err.println("EPICS::addMonitor> Problem establishing monitor for recName "+
			       monitorRec+" : "+e.toString());
	    return false;
	}
    }

    /*
    static public Thread monitorCheat(final Monitor mon, final MonitorListener ml) {
	return new Thread () {
		public void run() {
		    setName("KeepGoing");
		    if (mon == null) return;
		    Channel theChannel = mon.getChannel();
		    String oldStringVal = " ";
		    while (getName().equals("KeepGoing")) {
			try {
			    String newStringVal = EPICS.get(theChannel.getName());
			    //theContext.flushIO();
			    if (newStringVal.compareTo(oldStringVal) != 0) {
				ml.monitorChanged(new MonitorEvent(theChannel,new DBR_String(newStringVal),CAStatus.NORMAL));
				oldStringVal = new String(newStringVal);
			    }
			} catch (Exception e) {
			    System.err.println("EPICS::monitorCheat2> " + e.toString());
			    break;
			}
			try { Thread.sleep(5000);}
			catch (Exception e) { System.err.println("EPICS::monitorCheat3> " + e.toString()); }
		    }
		    
		}
	    };
    }
    */
/*
put epicsSim here someday

  public static boolean in_simulation_mode = true;
  private static Properties recs = new Properties();

  static {
    load();
  }

  synchronized static String get(String rec) {
    return recs.getProperty(rec);
  }

  synchronized static void put(String rec, String val) {
    recs.put(rec, val);
    save();
  }

  synchronized static void load() {
    try {
      FileInputStream input = new FileInputStream( "sim_recs.txt" );
      recs.load( input );
      input.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  synchronized static void save() {
    try {
      FileOutputStream output = new FileOutputStream( "sim_recs.txt" );
      recs.store( output, "simulated EPICS records" );
      output.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  static void monitor(PV pv, String rec, jca.event.MonitorListener l, int mask) {
    // Start a thread that checks to see if rec changes value.
    // In the thread, call arg.monitorChanged if it does.
    mon_thread aThread = new mon_thread(pv, rec, l, mask);
    aThread.start();
  }

}

class mon_thread extends Thread {
  PV pv;
  String rec;
  jca.event.MonitorListener l;
  int mask;
  String val;

  public mon_thread(PV _pv, String _rec, jca.event.MonitorListener _l, int _mask) {
    super(_rec);
    pv = _pv;
    rec = _rec;
    l = _l;
    mask = _mask;
    val = epicsSim.get(rec);
  }

  public void run() {
    while (true) {
      System.out.println( "checking " + val );
      if (val.compareTo(epicsSim.get(rec)) != 0) {
        val = epicsSim.get(rec);
        MonitorEvent event = new MonitorEvent(pv, new jca.dbr.DBR_DBString(), null, l);
        event.fire();
      }
      try {
        Thread.currentThread().sleep(1000);
      } catch (InterruptedException e) { }
    }
  }
*/
} //end of class Epics

