package uffjec;

import java.util.Vector;
import ufjca.*;

public class EPICSCallback implements UFCAToolkit.MonitorListener{

    EPICSCallbackListener callbackListener = null;
    private String myName = null;
    public String hashKey = null;

    public static Vector ecVector = new Vector();

    protected EPICSCallback(){}

    public EPICSCallback(String caName,EPICSCallbackListener cl) {
	try {
	    if (caName.indexOf(".") == -1 && caName.indexOf(":") == -1) {
		hashKey = caName;
          	caName = EPICS.recs.get(hashKey);
	    }
	    if (!caName.equals("null")) EPICS.addMonitor(caName,this);
	    callbackListener = cl;
	    myName = caName;
	    ecVector.add(this);
	} catch (Exception ex) {
	    System.err.println("EPICSCallback> Problem setting up monitor:"
			       +ex.toString());
	}
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(myName,this);
        if (hashKey != null) {
	    myName = EPICS.recs.get(hashKey);
        } else {
	    myName = dbname + myName.substring(myName.indexOf(":")+1);
        }
	if (!myName.equals("null")) EPICS.addMonitor(myName,this);
    }

  

    public void monitorChanged(String value) {
	try {
	    callbackListener.valueChanged(value);
	} catch (Exception e) {
	    System.err.println("EPICSCallback::monitorChanged> "+
			       myName+" : "+e.toString());
	}
    }

}
