package uffjec;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class JPanelDetectorLowLevel extends JPanel {
    static final long serialVersionUID = 0;
	
    EPICSTextField [] biaVals = new EPICSTextField[4];
    EPICSComboBox cycleTypeBox;
    EPICSComboBox integrationTime;
    EPICSLabel numPreResets;
    EPICSLabel numPostResets;
    EPICSComboBox numFramesOut;
    EPICSTextField pixelClockPeriod;
    
    EPICSTextField mceRawActionField;
    EPICSTextField mceRawQueryField;

    EPICSRadioButton[] cts;
    JCheckBox cbCDSRead, cbMultRead;

    //Old High level stuff
    JButton lvdtOnOffButton;
    EPICSLabel lvdtPosition;
    EPICSLabel lvdtVoltage;

    public JPanelDetectorLowLevel() {

	//old high level stuff
        lvdtOnOffButton = new JButton("Turn On LVDT");
        lvdtOnOffButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    lvdtButtonActionPerformed();
                }
            });
/*
	Old hard-coded values
        lvdtPosition = new EPICSLabel(EPICS.prefix+"sad:LVDTDISP","");
        lvdtVoltage = new EPICSLabel(EPICS.prefix+"sad:LVDTVLTS","");
*/

        //lvdtPosition = new EPICSLabel(EPICSRecs.lvdtPosition,"");
        //lvdtVoltage = new EPICSLabel(EPICSRecs.lvdtVoltage,"");

        //JPanelLVDT lvdtPanel = new JPanelLVDT();
        //lvdtPanel.setBorder(new EtchedBorder(0));

        /*
        //////////////////////////////////////
        ////////////////////////////////////////
        ////////////////////////////////////////
        JPanel lvdtPanel = new JPanel();
        lvdtPanel.setBorder(new EtchedBorder(0));
        lvdtPanel.add(new JLabel("LVDT Displacement:"));
        lvdtPanel.add(lvdtPosition);
        lvdtPanel.add(new JLabel("LVDT Voltage:"));
        lvdtPanel.add(lvdtVoltage);
        lvdtPanel.add(lvdtOnOffButton);
        */

	//Low-level
	//Bias panel
	JPanel biaPanel = new JPanel();
	biaPanel.setLayout(new RatioLayout());
	JPanel biaLabelPanel = new JPanel();
	biaLabelPanel.setLayout(new RatioLayout());

        biaVals[0] = new EPICSTextField(EPICSRecs.biasGate,"",true);
        biaVals[1] = new EPICSTextField(EPICSRecs.biasPwr,"",true);
        biaVals[2] = new EPICSTextField(EPICSRecs.biasVReset,"",true);
        biaVals[3] = new EPICSTextField(EPICSRecs.biasVCC,"",true);

	biaLabelPanel.add("0.01,0.01;0.99,0.49", new JLabel("DETECTOR BIASES", JLabel.CENTER));

	biaLabelPanel.add("0.01,0.51;0.33,0.49",new JLabel("Name",JLabel.CENTER));
	biaLabelPanel.add("0.34,0.51;0.22,0.49",new JLabel("Set",JLabel.CENTER));
	biaLabelPanel.add("0.56,0.51;0.22,0.49",new JLabel("Last",JLabel.CENTER));
	biaLabelPanel.add("0.78,0.51;0.22,0.49",new JLabel("Current",JLabel.CENTER));

	biaPanel.add("0.01,0.01;0.33,0.19",new JLabel("BIAS GATE"));
	biaPanel.add("0.34,0.01;0.22,0.19",biaVals[0]);
        biaPanel.add("0.56,0.01;0.22,0.19",new EPICSLabel(EPICSRecs.valBiasGate,""));
        biaPanel.add("0.78,0.01;0.22,0.19",new EPICSLabel(EPICSRecs.sadBiasGate,""));

	biaPanel.add("0.01,0.20;0.33,0.19",new JLabel("BIAS POWER"));
	biaPanel.add("0.34,0.20;0.22,0.19",biaVals[1]);
        biaPanel.add("0.56,0.20;0.22,0.19",new EPICSLabel(EPICSRecs.valBiasPwr,""));
        biaPanel.add("0.78,0.20;0.22,0.19",new EPICSLabel(EPICSRecs.sadBiasPwr,""));

        biaPanel.add("0.01,0.40;0.33,0.19",new JLabel("BIAS VRESET"));
        biaPanel.add("0.34,0.40;0.22,0.19",biaVals[2]);
        biaPanel.add("0.56,0.40;0.22,0.19",new EPICSLabel(EPICSRecs.valBiasVReset,""));
        biaPanel.add("0.78,0.40;0.22,0.19",new EPICSLabel(EPICSRecs.sadBiasVReset,""));
	
	biaPanel.add("0.01,0.60;0.33,0.19",new JLabel("Array VCC"));
	biaPanel.add("0.34,0.60;0.22,0.19",biaVals[3]);
        biaPanel.add("0.56,0.60;0.22,0.19",new EPICSLabel(EPICSRecs.valBiasVCC,""));
        biaPanel.add("0.78,0.60;0.22,0.19",new EPICSLabel(EPICSRecs.sadBiasVCC,""));

	JRadioButton biasDigital = new JRadioButton("Digital",true);
	JRadioButton biasVolts = new JRadioButton("Volts");
	biaPanel.add("0.01,0.80;0.33,0.19", new JLabel("Units"));
	ButtonGroup biasbg = new ButtonGroup();
	biasbg.add(biasDigital);
	biasbg.add(biasVolts);
	biaPanel.add("0.34,0.80;0.33,0.19", biasDigital);
        biaPanel.add("0.67,0.80;0.33,0.19", biasVolts);
	

	//Detector Timing
        JPanel dtPanel = new JPanel();
        dtPanel.setLayout(new RatioLayout());
        dtPanel.setBorder(new EtchedBorder(0));

        JPanel pbcPanel = new JPanel();
        pbcPanel.setLayout(new RatioLayout());
        pbcPanel.setBorder(new EtchedBorder(0));
        pixelClockPeriod = new EPICSTextField(EPICSRecs.pixelBaseClock,"");

        pbcPanel.add("0.01,0.01;0.33,0.49",new JLabel("PIXEL BASE CLOCK",JLabel.CENTER));
        pbcPanel.add("0.34,0.01;0.22,0.49",new JLabel("Set",JLabel.CENTER));
        pbcPanel.add("0.56,0.01;0.22,0.49",new JLabel("Last",JLabel.CENTER));
        pbcPanel.add("0.78,0.01;0.22,0.49",new JLabel("Current",JLabel.CENTER));

        pbcPanel.add("0.34,0.51;0.22,0.49",pixelClockPeriod);
        pbcPanel.add("0.56,0.51;0.22,0.49",new EPICSLabel(EPICSRecs.valPixelBaseClock,""));
        pbcPanel.add("0.78,0.51;0.22,0.49",new EPICSLabel(EPICSRecs.sadPixelBaseClock,""));

        dtPanel.add("0.01,0.01;0.99,0.06", new JLabel("DETECTOR TIMING", JLabel.CENTER));

	JPanel ldvarPanel = new JPanel();
        ldvarPanel.setLayout(new RatioLayout());
        ldvarPanel.setBorder(new EtchedBorder(0));

        ldvarPanel.add("0.01,0.01;0.33,0.19",new JLabel("LOAD VARIABLES",JLabel.CENTER));
        ldvarPanel.add("0.34,0.01;0.22,0.19",new JLabel("Set",JLabel.CENTER));
        ldvarPanel.add("0.56,0.01;0.22,0.19",new JLabel("Last",JLabel.CENTER));
        ldvarPanel.add("0.78,0.01;0.22,0.19",new JLabel("Current",JLabel.CENTER));

        ldvarPanel.add("0.01,0.21;0.33,0.19",new JLabel("Pre Resets"));
        ldvarPanel.add("0.34,0.21;0.22,0.19",new EPICSTextField(EPICSRecs.preResets,"",true));
        ldvarPanel.add("0.56,0.21;0.22,0.19",new EPICSLabel(EPICSRecs.valPreResets,""));
        ldvarPanel.add("0.78,0.21;0.22,0.19",new EPICSLabel(EPICSRecs.sadNumPreResets,""));

        ldvarPanel.add("0.01,0.41;0.33,0.19",new JLabel("Post Resets"));
        ldvarPanel.add("0.34,0.41;0.22,0.19",new EPICSTextField(EPICSRecs.postResets,"",true));
        ldvarPanel.add("0.56,0.41;0.22,0.19",new EPICSLabel(EPICSRecs.valPostResets,""));
        ldvarPanel.add("0.78,0.41;0.22,0.19",new EPICSLabel(EPICSRecs.sadNumPostResets,""));

        ldvarPanel.add("0.01,0.61;0.33,0.19",new JLabel("Num Reads"));
        ldvarPanel.add("0.34,0.61;0.22,0.19",new EPICSTextField(EPICSRecs.cdsReads,"",true));
        ldvarPanel.add("0.56,0.61;0.22,0.19",new EPICSLabel(EPICSRecs.valCDSReads,""));
        ldvarPanel.add("0.78,0.61;0.22,0.19",new EPICSLabel(EPICSRecs.sadCDSReads,""));

        ldvarPanel.add("0.01,0.81;0.33,0.19",new JLabel("Num Frames"));
        ldvarPanel.add("0.34,0.81;0.22,0.19",new EPICSTextField(EPICSRecs.expCnt,"",true));
        ldvarPanel.add("0.56,0.81;0.22,0.19",new EPICSLabel(EPICSRecs.valExpCnt,""));
        //Sad for # frames
        //ldvarPanel.add("0.78,0.81;0.22,0.19",new EPICSLabel(EPICSRecs.sadCDSReads,""));

	JPanel ctPanel = new JPanel();
        ctPanel.setLayout(new RatioLayout());
        ctPanel.setBorder(new EtchedBorder(0));

        ctPanel.add("0.01,0.01;0.33,0.27",new JLabel("CYCLE TYPES",JLabel.CENTER));
        ctPanel.add("0.34,0.01;0.22,0.27",new JLabel("1 Frame",JLabel.CENTER));
        ctPanel.add("0.56,0.01;0.22,0.27",new JLabel("<html>Many<br>Frames</html>",JLabel.CENTER));
        ctPanel.add("0.78,0.01;0.22,0.27",new JLabel("<html>Both Reads<br>as Frames</html>", JLabel.CENTER));

	cts = new EPICSRadioButton[7];
	cts[0] = new EPICSRadioButton(EPICSRecs.cycleType, null, "10", "null");
        cts[1] = new EPICSRadioButton(EPICSRecs.cycleType, null, "30", "null");
        cts[2] = new EPICSRadioButton(EPICSRecs.cycleType, null, "20", "null");
        cts[3] = new EPICSRadioButton(EPICSRecs.cycleType, null, "40", "null");
        cts[4] = new EPICSRadioButton(EPICSRecs.cycleType, null, "22", "null");
        cts[5] = new EPICSRadioButton(EPICSRecs.cycleType, null, "42", "null");
        cts[6] = new EPICSRadioButton(EPICSRecs.cycleType, null, "0", "null");
	cts[0].setText("CT 10");
        cts[1].setText("CT 30");
        cts[2].setText("CT 20");
        cts[3].setText("CT 40");
        cts[4].setText("CT 22");
        cts[5].setText("CT 42");
	cts[6].setText("CT 0");
	cts[6].setSelected(true);

        ctPanel.add("0.01,0.28;0.33,0.18",new JLabel("Single Read"));
        ctPanel.add("0.34,0.28;0.22,0.18", cts[0]);
        ctPanel.add("0.56,0.28;0.22,0.18", cts[1]);

        ctPanel.add("0.01,0.46;0.33,0.18",new JLabel("CDS Read"));
        ctPanel.add("0.34,0.46;0.22,0.18", cts[2]);
        ctPanel.add("0.56,0.46;0.22,0.18", cts[3]);

	cbCDSRead = new JCheckBox();
	cbCDSRead.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      if (cbCDSRead.isSelected()) {
		cts[2].updateValToMark("21");
		cts[2].setText("CT 21");
		cts[3].updateValToMark("41");
		cts[3].setText("CT 41");
	      } else {
                cts[2].updateValToMark("20");
                cts[2].setText("CT 20");
                cts[3].updateValToMark("40");
                cts[3].setText("CT 40");
	      }
              if (cts[2].isSelected()) {
                cts[2].doClick();
              } else cts[3].doClick();
	   }
	});
	cbCDSRead.setEnabled(false);
	
        ctPanel.add("0.78,0.46;0.22,0.18", cbCDSRead);

        ctPanel.add("0.01,0.64;0.33,0.18",new JLabel("Multiple Reads"));
        ctPanel.add("0.34,0.64;0.22,0.18", cts[4]);
        ctPanel.add("0.56,0.64;0.22,0.18", cts[5]);

        cbMultRead = new JCheckBox();
        cbMultRead.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              if (cbMultRead.isSelected()) {
                cts[4].updateValToMark("23");
                cts[4].setText("CT 23");
                cts[5].updateValToMark("43");
                cts[5].setText("CT 43");
              } else {
                cts[4].updateValToMark("22");
                cts[4].setText("CT 22");
                cts[5].updateValToMark("42");
                cts[5].setText("CT 42");
              }
	      if (cts[4].isSelected()) {
		cts[4].doClick();
	      } else cts[5].doClick();
           }
        });
        cbMultRead.setEnabled(false);

        ctPanel.add("0.78,0.64;0.22,0.18", cbMultRead);

        ctPanel.add("0.01,0.82;0.33,0.18",new JLabel("Continuous Reset"));
        ctPanel.add("0.34,0.82;0.22,0.18", cts[6]);

	ActionListener cbAction = new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              cbCDSRead.setEnabled(cts[2].isSelected() || cts[3].isSelected());
	      cbMultRead.setEnabled(cts[4].isSelected() || cts[5].isSelected());
           }
	};
        ButtonGroup ctButtons = new ButtonGroup();
        for (int j = 0; j < cts.length; j++) {
	   ctButtons.add(cts[j]);
	   cts[j].addActionListener(cbAction);
	}

	dtPanel.add("0.01,0.08;0.99,0.12", pbcPanel);
	dtPanel.add("0.01,0.21;0.99,0.29", ldvarPanel);
        dtPanel.add("0.01,0.51;0.99,0.31", ctPanel);

	setLayout(new RatioLayout());
	biaPanel.setBorder(new EtchedBorder(0));
	biaLabelPanel.setBorder(new EtchedBorder(0));

	//Integration time
	JPanel iTimePanel = new JPanel();
	iTimePanel.setBorder(new EtchedBorder(0));
        iTimePanel.setLayout(new RatioLayout());

        iTimePanel.add("0.01,0.01;0.33,0.33",new JLabel("INTEGRATION TIME",JLabel.CENTER));
        iTimePanel.add("0.34,0.01;0.22,0.33",new JLabel("Set",JLabel.CENTER));
        iTimePanel.add("0.56,0.01;0.22,0.33",new JLabel("Last",JLabel.CENTER));
        iTimePanel.add("0.78,0.01;0.22,0.33",new JLabel("Current",JLabel.CENTER));

        iTimePanel.add("0.01,0.34;0.33,0.33",new JLabel("Time Value"));
        iTimePanel.add("0.34,0.34;0.22,0.33",new EPICSTextField(EPICSRecs.expTime,"",true));
        iTimePanel.add("0.56,0.34;0.22,0.33",new EPICSLabel(EPICSRecs.valExpTime,""));
        iTimePanel.add("0.78,0.34;0.22,0.33",new EPICSLabel(EPICSRecs.exposureTime,""));

        JRadioButton iTimeSec = new JRadioButton("Seconds",true);
        JRadioButton iTimeMilliSec = new JRadioButton("MilliSec");
        iTimePanel.add("0.01,0.67;0.33,0.33", new JLabel("Time Units"));
        ButtonGroup iTimebg = new ButtonGroup();
        iTimebg.add(iTimeSec);
        iTimebg.add(iTimeMilliSec);
        iTimePanel.add("0.34,0.67;0.33,0.33", iTimeSec);
        iTimePanel.add("0.67,0.67;0.33,0.33", iTimeMilliSec);

        dtPanel.add("0.01,0.83;0.99,0.17", iTimePanel);

	JPanel rawMcePanel = new JPanel();
	rawMcePanel.setBorder(new EtchedBorder(0));
	rawMcePanel.setLayout(new RatioLayout());
	//mceRawActionField = new EPICSTextField(EPICS.prefix+"dc:mcecmd.Action");
	//mceRawQueryField = new EPICSTextField(EPICS.prefix+"dc:mcecmd.Query");
        mceRawActionField = new EPICSTextField(EPICSRecs.mceAction);
        mceRawQueryField = new EPICSTextField(EPICSRecs.mceQuery);

	rawMcePanel.add("0.01,0.01;0.99,0.25",new JLabel("MCE Raw Commands",JLabel.CENTER));
	rawMcePanel.add("0.01,0.26;0.20,0.37",new JLabel("Action:"));
	rawMcePanel.add("0.21,0.26;0.79,0.37",mceRawActionField);
	rawMcePanel.add("0.01,0.63;0.20,0.37",new JLabel("Query:"));
	rawMcePanel.add("0.21,0.63;0.79,0.37",mceRawQueryField);
	//EPICSCheckBox datumLabDef = new EPICSCheckBox(EPICS.prefix+"dc:datum.LabDef","1","0");
        EPICSCheckBox datumLabDef = new EPICSCheckBox(EPICSRecs.labDef,"1","0");
	datumLabDef.setText("LabDef");
	/*
    -- Low Level Detector Panel
	*/
	add("0.01,0.01;0.28,0.08",biaLabelPanel);
	add("0.01,0.09;0.28,0.29",biaPanel);
        add("0.30,0.01;0.38,0.85",dtPanel);
	add("0.70,0.75;0.25,0.15",rawMcePanel);
	add("0.47,0.87;0.08,0.05",datumLabDef);
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("dc:"));
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
    
    }

    public void lvdtButtonActionPerformed() {
        if (lvdtOnOffButton.getText().indexOf("On") != -1)
            lvdtOnOffButton.setText("Turn Off LVDT");
        else
            lvdtOnOffButton.setText("Turn On LVDT");
    }
}
