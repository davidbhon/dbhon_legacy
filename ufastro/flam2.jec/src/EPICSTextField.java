
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Modified:     by Frank Varosi, 2001 (to make it work right with epics).
//Company:      University of Florida
//Description:

package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JTextField;
import javax.swing.*;
import javaUFLib.*;
import java.util.*;
//===============================================================================
/**
 * Handles text fields related to EPICS
 */
public class EPICSTextField extends JTextField implements UFCAToolkit.MonitorListener,FocusListener, KeyListener, MouseListener {
    public static final String rcsID = "$Name:  $ $Id: EPICSTextField.java,v 0.32 2006/10/31 21:07:01 warner Exp $";
    static final long serialVersionUID = 0;
    
    String putRec;
    String hashKey = null;
    String monHashKey = null;
    Vector shadowHash = new Vector();
    String monitorRec;
    String prev_text = "";
    String new_text = "";
    String desc;
    String outVal = "";
    boolean dual = true; // true if putRec and monitorRec are two different recs
    boolean doPuts = true;
    boolean userEvent = false;
    long timeStamp = 0;
    boolean keepYellow = false;
    int yellowState = 0;
    boolean isPreamp = false;

    UFMessageLog _log;
    
    Vector shadowVec,clearVec;
    HashMap shadowMap,shadowValMap;

    public static final Color CHANGED_COLOR = new Color(0,255,255);

    //-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public EPICSTextField() {
	try  {
	    jbInit("", "", "");
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
	}
    } //end of EPICSTextField

    //-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param rec String: record field
     */
    public EPICSTextField(String rec) {
	try  {
	    if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
		hashKey = rec;
		monHashKey = rec;
		rec = EPICS.recs.get(hashKey);
	    }
	    jbInit(rec, rec, "");
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
	}
    } //end of EPICSTextField
    //-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param rec String: record field
     *@param description String: Information regarding the record field
     */
    public EPICSTextField(String rec, String description) {
	try  {
            if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
                hashKey = rec;
		monHashKey = rec;
                rec = EPICS.recs.get(hashKey);
            }
	    jbInit(rec, rec, description);
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
	}
    } //end of EPICSTextField

    //-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param rec String: record field
     *@param description String: Information regarding the record field
     */
    public EPICSTextField(String rec, String description, boolean doPuts) {
	try  {
	    this.doPuts = doPuts;
            if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
                hashKey = rec;
		monHashKey = rec;
                rec = EPICS.recs.get(hashKey);
            }
	    jbInit(rec, rec, description);
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
	}
    } //end of EPICSTextField

    //-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param putRec String: Input record field for monitoring
     *@param monitorRec String: Output record field for monitoring
     *@param description String: Information regarding the record field
     */
    public EPICSTextField(String putRec, String monitorRec, String description, boolean doPuts) {
	try  {
	    this.doPuts = doPuts;
            if (putRec.indexOf(".") == -1 && putRec.indexOf(":") == -1) {
                hashKey = putRec;
                putRec = EPICS.recs.get(hashKey);
            }
            if (monitorRec.indexOf(".") == -1 && monitorRec.indexOf(":") == -1) {
                monHashKey = monitorRec;
                monitorRec = EPICS.recs.get(monHashKey);
            }
	    jbInit(putRec, monitorRec, description);
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
	}
    } //end of EPICSTextField

    //-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param putRec String: Input record field for monitoring
     *@param monitorRec String: Output record field for monitoring
     *@param description String: Information regarding the record field
     */
    public EPICSTextField(String putRec, String monitorRec, String description) {
	try  {
            if (putRec.indexOf(".") == -1 && putRec.indexOf(":") == -1) {
                hashKey = putRec;
                putRec = EPICS.recs.get(hashKey);
            }
            if (monitorRec.indexOf(".") == -1 && monitorRec.indexOf(":") == -1) {
                monHashKey = monitorRec;
                monitorRec = EPICS.recs.get(monHashKey);
            }
	    jbInit(putRec, monitorRec, description);
	}
	catch(Exception ex) {
	    fjecError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
	}
    } //end of EPICSTextField

    //-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param putRec String: Input record field for monitoring
     *@param monitorRec String: Output record field for monitoring
     *@param description String: Information regarding the record field
     */
    private void jbInit(String putRec, String monitorRec, String description) throws Exception {
	shadowVec = new Vector();
	shadowMap = new HashMap();
	shadowValMap = new HashMap();
	clearVec = new Vector();
	_log = new UFMessageLog(1000);
	this.dual = !putRec.equals(monitorRec);
	this.putRec = putRec;
	this.monitorRec = monitorRec;
	this.desc = description;
	this.setToolTipText();
	this.timeStamp = 0;
	this.addMouseListener(this);

	if( dual || doPuts ) {
	    this.addFocusListener(this);
	    this.addKeyListener(this);
	}
      
	if( putRec.trim().equals("") || monitorRec.trim().equals("") ) return;
      
	if (!EPICS.addMonitor(monitorRec,this))
	    setBackground(Color.red);
	/*
	// setup monitor(s)
	int mask = Monitor.VALUE;
	try {
	gov.aps.jca.Channel theChannel = EPICS.theContext.createChannel(monitorRec);
	EPICS.theContext.pendIO(EPICS.TIMEOUT);
	outMon = theChannel.addMonitor(mask,this);
	EPICS.theContext.flushIO();
	//EPICS.monitorCheat(outMon,this).start();
	//theContext.destroy();
	//theChannel.destroy();
	//	outMon = pv.addMonitor(new DBR_DBString(), this, new Integer(0), mask);
	}
	catch (Exception ex) { this.setBackground( Color.red ); }
	*/
    } //end of jbInit

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
	if (hashKey != null) {
	    putRec = EPICS.recs.get(hashKey);
	} else {
	    putRec = dbname + putRec.substring(putRec.indexOf(":")+1);
	    return;
	}
	if (monHashKey != null) {
	    monitorRec = EPICS.recs.get(monHashKey);
	} else {
	    monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
	    return;
	}
	EPICS.addMonitor(monitorRec,this);
	Object [] shadows = shadowVec.toArray();
	Object [] shadowHashArray = shadowHash.toArray();
	shadowVec.clear();
	shadowHash.clear();
	shadowValMap.clear();
	for (int i=0; i<shadows.length; i++) {
	    String shady;
	    if (shadowHashArray[i] != null) {
		shady = EPICS.recs.get((String)shadowHashArray[i]);
		shadowHash.add((String)shadowHashArray[i]);
	    } else {
		shady = (String)shadows[i];
		shady = dbname + shady.substring(shady.indexOf(":")+1);
	    }
	    EPICS.removeMonitor(shady,(UFCAToolkit.MonitorListener)shadowMap.get(shady));
	    shadowVec.add(shady);
	    shadowMap.put(shady,shadowMap.remove(shadows[i]));
	    if (shady != null) {
		EPICS.addMonitor(shady,(UFCAToolkit.MonitorListener)shadowMap.get(shady));
	    }
	}
	Object [] clears = clearVec.toArray();
	clearVec.clear();
	for (int i=0; i<clears.length; i++){
	    String clearEyed = (String)clears[i];
            if (clearEyed.indexOf(".") != -1 || clearEyed.indexOf(":") != -1) {
		clearEyed = dbname + clearEyed.substring(clearEyed.indexOf(":")+1);
	    }
	    clearVec.add(clearEyed);
	}
	setToolTipText();
    }


    public long getTimeStamp() { return timeStamp; }

    public String getPutRec() { return putRec; }
    public String getMonRec() { return monitorRec; }

    public String toString() { return getText();}

    /**
     * EPICS puts are put to this record as well;
     * monitors are established for this record, and
     * monitor values are added to mulit-line tooltips
     */
    public void addShadowRecord(String shadowRecName) {
        if (shadowRecName.indexOf(".") == -1 && shadowRecName.indexOf(":") == -1) {
            shadowHash.add(shadowRecName);
            shadowRecName = EPICS.recs.get(shadowRecName);
        } else {
	    shadowHash.add(null);
	}
	shadowVec.add(shadowRecName);
	final String localName = shadowRecName;
	shadowMap.put(shadowRecName,new UFCAToolkit.MonitorListener(){
		public void monitorChanged(String val) {
		    shadowValMap.put(EPICS.prefix+localName.substring(localName.indexOf(":")+1),val);
		    setToolTipText();
		}
		public void reconnect(String newPrefix) {
		    
		}
	    });
	EPICS.addMonitor(shadowRecName,(UFCAToolkit.MonitorListener)shadowMap.get(shadowRecName));
    }

    public void removeShadowRecord(String shadowRecName) {
	shadowVec.remove(shadowRecName);
	EPICS.removeMonitor(shadowRecName,(UFCAToolkit.MonitorListener)shadowMap.get(shadowRecName));
	shadowMap.remove(shadowRecName);
    }
    
    /**
     * In case you don't want to have a record
     * processed on the next top-level apply
     */
    public void doClearOnPut(String putRecName) {
	clearVec.add(putRecName);
    }

    //-------------------------------------------------------------------------------
    /**
     * Sets the text for the tool tip -- returns no arguments
     */
    protected void setToolTipText() {
	if( dual )
	    this.setToolTipText( putRec + " -> " + monitorRec + " = " + outVal );
	//this.setToolTipText( monitorRec + " = " + outVal
	//		     + "  : old=" + prev_text
	//		     + "  : new=" + new_text );
	else {
	    String tip = "<html>"+monitorRec + " = " + outVal;
	    Object [] shadows = shadowVec.toArray();
	    for (int i=0; i<shadows.length; i++) {
		tip += "<br>"+(String)shadows[i]+" = "+(String)shadowValMap.get((String)shadows[i]);
	    }
	    if (isPreamp && !outVal.equals("null") && !outVal.equals("")) {
		tip += " volts<br>"+(int)(Float.parseFloat(outVal)*255/9.4+0.0005)+" bits";
	    }
	    tip += "</html>";
	    //this.setToolTipText( monitorRec + " = " + outVal );
	    this.setToolTipText(tip);
	}
    }

    public void setText(String newText) {
	super.setText(newText);
	_log.addMessage(newText);
    }

    //-------------------------------------------------------------------------------
    /**
     * Event Handling Method: overloads the virtual method in abstract MonitorListener class (jca)
     *@param event MonitorEvent: TBD
     */
    public void monitorChanged(String value) {
	try {
	    //dbstr.printInfo();
	    //Ca.pendIO(EPICS.TIMEOUT);

	    //	if( event.getChannel().getName().equals(this.monitorRec) ) {
	    outVal = value.trim(); 
	    setText( outVal.trim() );
	    prev_text = value.trim();
	    if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
	    if( this.getText().indexOf("ERR") >= 0 ) {
		Toolkit.getDefaultToolkit().beep();
		Toolkit.getDefaultToolkit().beep();
	    }
	    //outMon.clear();
	    //outMon = event.pv().addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
	    //}
	
	    setToolTipText();
	    if (keepYellow) {
		yellowState++;
		if (yellowState > 1) yellowState = 0;
	    }
	
	    if( dual && doPuts && userEvent )
		{
		    userEvent = false;
		    if( new_text.equals( getText() ) )
			this.setBackground( Color.white );
		    else 
			this.setBackground( Color.yellow );
		} 
	    else if (!keepYellow || yellowState != 1) {
		this.setBackground( Color.white );
	    }
	    this.timeStamp = System.currentTimeMillis();
	    this.invalidate();
	}
	catch (Exception ex) {
	    fjecError.show("Error in monitor changed for EPICSTextField: " + monitorRec + " " + ex.toString());
	}
    } //end of monitorChanged

    //------------------------------------------------------------------
    public void mouseReleased(MouseEvent me) {}
    public void mousePressed(MouseEvent me) {}
    public void mouseExited(MouseEvent me) {}
    public void mouseEntered(MouseEvent me) {}
    public void mouseClicked(MouseEvent me) {
	if (me.getClickCount() == 2 && me.getButton() == MouseEvent.BUTTON3) {
	    String inp = "";
	    inp = JOptionPane.showInputDialog(this,"New monitor rec for this "+
					      "component? (Current pvName = "+
					      monitorRec+")");
	    if (inp != null && inp.trim() != null) {
		try {
		    EPICS.removeMonitor(monitorRec,this);
		    monitorRec = inp.trim();
		    if (!dual)
			putRec = monitorRec;
		    EPICS.addMonitor(monitorRec,this);
		}  catch (Exception e) {
		    System.err.println("EPICSTextField::mouseClicked> "+
				       e.toString());
		}
	    }
	} else if (me.getButton() == MouseEvent.BUTTON3) {
	    JPopupMenu popupMenu = new JPopupMenu();
	    JMenuItem viewLogSmall = new JMenuItem("View Log (small)");
	    JMenuItem viewLogLarge = new JMenuItem("View Log (bigger)");
	    JMenuItem viewLogHuge = new JMenuItem("View Log (full size)");
	    popupMenu.add(viewLogSmall);
	    popupMenu.add(viewLogLarge);
	    popupMenu.add(viewLogHuge);
	    
	    viewLogSmall.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ev) { _log.viewFrame(monitorRec,600,200); } });
	    
	    viewLogLarge.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ev) { _log.viewFrame(monitorRec,650,500); } });
	    
	    viewLogHuge.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ev) { _log.viewFrame(monitorRec,700,900); } });
	    
	    
	    popupMenu.show(me.getComponent(), me.getX(), me.getY());
	}
    }

    //-------------------------------------------------------------------------------

    public void focusGained(FocusEvent e) 
    { 
	if( prev_text.trim().equals("") )
	    prev_text = this.getText().trim();
    }

    //-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	new_text = getText().trim();

	if( ! new_text.equals( prev_text ) ) {
	    setBackground( CHANGED_COLOR );
	    //if( doPuts ) putcmd( new_text );
	}
	else if (!keepYellow) setBackground( Color.white );
    }
    //-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ke KeyEvent: TBD
     */
    public void keyTyped (KeyEvent ke) {
	userEvent = true;
    }

    //-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ke KeyEvent: TBD
     */
    public void keyReleased (KeyEvent ke) {
	userEvent = true;
    }

    //-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ke KeyEvent: TBD
     */
    public void keyPressed (KeyEvent ke)
    {
	userEvent = true;

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		new_text = getText().trim();

		//if( ! new_text.equals( prev_text ) ) {
		if( doPuts ) {
		    putcmd( new_text );
		    setBackground( Color.yellow );
		}
		//}
		else setBackground( Color.white );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prev_text );
		setBackground(Color.white);
	    }
	//else
	//  setBackground(new Color(0,255,255));
    }

    public String getPutrecName() { return putRec;}
    public String getMonval() { return outVal;}

    //-------------------------------------------------------------------------------
    /**
     * Puts the current contents of the textfield to the EPICS record field
     *@param val String: Value to be put
     */
    public void forcePut() {
	putcmd( this.getText() );
    }

    //-------------------------------------------------------------------------------
    /**
     * Puts the val to the EPICS record field
     *@param val String: Value to be put
     */
    void putcmd(String val) {
	//if( val.trim().equals("") || putRec.trim().equals("") ) return;
	//String cmd = new String("PUT " + putRec + " " + "\""+val+ "\"" + " ;" + desc) ;
	//command.execute(cmd);
	EPICS.put(putRec,val);
	Object [] shadows = shadowVec.toArray();
	for (int i=0; i<shadows.length; i++) {
	    EPICS.put((String)shadows[i],val);
	}
	Object [] clears = clearVec.toArray();
	for (int i=0; i<clears.length; i++) {
            String clearEyed = (String)clears[i];
            if (clearEyed.indexOf(".") == -1 && clearEyed.indexOf(":") == -1) {
		EPICS.put(EPICS.recs.get((String)clears[i]), EPICS.CLEAR+"");
	    } else { 
		EPICS.put((String)clears[i],EPICS.CLEAR+"");
	    }
	}
	prev_text = val.trim();
	yellowState = 0;
    } //end of putcmd

    //-------------------------------------------------------------------------------
    /**
     * Sets the text and puts the value in EPICS
     *@param val String: value to be set and put
     */
    public void set_and_putcmd(String val) {
	if (keepYellow) {
	   yellowState++;
	   setBackground(Color.YELLOW);
	}
	setText( val );
	putcmd( val );
    } //end of set_and_putcmd

    //-------------------------------------------------------------------------------
    /**
     * Window Event Handling Method
     *@param e WindowEvent: TBD
     */
    protected void processWindowEvent(WindowEvent e) {
	if(e.getID() == WindowEvent.WINDOW_CLOSING) {
	    try {
		EPICS.removeMonitor(monitorRec,this);
	    }
	    catch (Exception ee) {
		fjecError.show(e.toString());
	    }
	    ///dispose();
	}
	///super.processWindowEvent(e);
    } //end of processWindowEvent

    //-------------------------------------------------------------------------------
    /**
     *  This method allows the user to reset the
     *  EPICS record names and re-establish
     *  monitors to the new record names.
     *@param putRec String: Input record field for monitoring
     *@param monRec String: Output record field for monitoring
     *@param desc String: description of the record field
     */
    public void setEPICSRecords(String putRec, String monRec, String desc) {
	this.setText("");
	this.setBackground(Color.white);
	this.putRec = putRec;
	this.monitorRec = monRec;
	this.desc = desc;
	this.prev_text = this.getText();
	this.doPuts = true;

	try {
	    //EPICS.removeMonitor(putRec,this);
	    EPICS.removeMonitor(monRec,this);
	    // setup monitor
	    //EPICS.addMonitor(putRec,this);
	    EPICS.addMonitor(monRec,this);
	    this.setToolTipText();
	}
	catch (Exception ee) {
	    this.setBackground(Color.red);
	}

    } // end of setEPICSRecords

    //-------------------------------------------------------------------------------
    /**
     *  This method allows the user to reset the
     *  EPICS record names and re-establish
     *  monitors to the new record names.
     *@param rec String: Record field
     *@param desc String: Record description
     */
    public void setEPICSRecords(String rec, String desc) {
	this.setEPICSRecords(rec,rec,desc);
    } // end of setEPICSRecords

    public void setYellowState(boolean b) {
	this.keepYellow = b;
    }

  public void setPreamp(boolean b) {
    this.isPreamp = b;
  }

} //end of class EPICSTextField

