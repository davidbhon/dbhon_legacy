package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class EPICSCheckBox extends JCheckBox implements UFCAToolkit.MonitorListener {

    static final long serialVersionUID = 0;
    
    String hashKey = null;
    String monitorRec;
    String outVal;

    String markVal;
    String clearVal;

    public EPICSCheckBox(String epicsName,  String valToMark,  String valToClear) {
	monitorRec = epicsName;
        if (monitorRec.indexOf(".") == -1 && monitorRec.indexOf(":") == -1) {
            hashKey = monitorRec;
            monitorRec = EPICS.recs.get(hashKey);
        }
	markVal = valToMark;
	clearVal = valToClear;

	if (monitorRec.trim().equals("")) return;
 
	if (!EPICS.addMonitor(monitorRec,this)) 
	    setBackground(Color.red);
	else
	    setForeground(new JCheckBox().getForeground());
	
	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (isSelected()) {
			EPICS.put(monitorRec,markVal);
		    } else {
			EPICS.put(monitorRec,clearVal);
		    }
		}
	    });	
    }

    public EPICSCheckBox(String epicsName) {
	this(epicsName, EPICS.MARK+"", "null");
    }
    
    public void monitorChanged(String value) {
	try {
	    outVal = value;
	    setToolTipText();
	    if (value.trim().toLowerCase().equals("null") || value.equals("")) {
		setSelected(false);
		
	    }else {
		int val = Integer.parseInt(value);
		if (val == EPICS.MARK || val == EPICS.PRESET) 
		    setSelected(true);
		else
		    setSelected(false);
		if (value.trim().equals(markVal.trim()))
		    setSelected(true);
		else if (value.trim().equals(clearVal.trim()))
		    setSelected(false);
	    }
	} catch (Exception e) {
	    System.err.println("EPICSCheckBox.monitorChanged> "+e.toString());
	}
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
        if (hashKey != null) {
            monitorRec = EPICS.recs.get(hashKey);
        } else {
            monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
        }
	EPICS.addMonitor(monitorRec,this);
    }

    private void setToolTipText() {
	this.setToolTipText(monitorRec + " = " + outVal);
    }

}
