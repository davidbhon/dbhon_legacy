package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.UFColorButton;

public class JPanelMindReader extends JPanel {

	static final long serialVersionUID = 0;
    static JPanel me;

    public  JPanelMindReader() {
	me = this;
	UFColorButton b = new UFColorButton("Do what I want you to do");
	b.setColorGradient(new Color(100,238,100), new Color(100,255,100), new Color(100,100,255), new Color(0,80,150));
	b.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    //System.err.println("JPanelMindReader> Insufficient data. Think harder and try again.");
		    JOptionPane.showMessageDialog(me,"You have angered FLAMINGOS-2 with your impure thoughts!  Prepare for the spectrometric wrath of FLAMINGOS-2!!");
		    String text = "<html>FJEC:<br>Uh oh, looks like you're in trouble...<br>FLAMINGOS-2 is using my psychic connection with you<br>to channel its cryogenic fury.<br>Quick, either shut your brain down now, or initiate the self-destruct sequence<br><br>Good luck </html>";
		    //JOptionPane.showMessageDialog(null,"FJEC: Uh oh, looks like you're in trouble");
		    JOptionPane.showMessageDialog(me,text);
		    UFColorButton desButton = new UFColorButton("Self-Destruct",UFColorButton.COLOR_SCHEME_RED);
		    me.add("0.25,0.50;0.50,0.30",desButton);
		    desButton.invalidate();
		    me.repaint();
		    me.invalidate();
		    desButton.addActionListener(new ActionListener(){
			    public void actionPerformed(ActionEvent ae) {
				JOptionPane.showMessageDialog(me,"Now formatting hard drive");
				System.exit(0);
			    }
			});
		}
	    });
	setLayout(new RatioLayout());
	add("0.40,0.01;0.20,0.06",b);
    }

}
