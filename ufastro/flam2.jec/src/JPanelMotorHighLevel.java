package uffjec;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import javaUFLib.*;
//===============================================================================
/**
 * Handles the Motor High Level tabbed pane
 */
public class JPanelMotorHighLevel extends JPanel{
	static final long serialVersionUID = 0;
	
	/**
     * Default constructor
     *@param fjecmotors FJECMotor []: Array of FJECMotors
     */
    public JPanelMotorHighLevel(FJECMotor [] fjecmotors) {
	setLayout(new RatioLayout());
	JPanel highPanel = new JPanel();
	highPanel.setLayout(new GridLayout(0,1));
	highPanel.add(FJECMotor.getMotorHighLevelLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) highPanel.add(fjecmotors[i].getMotorHighLevelPanel());

	//EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.prefix+"cc:abort.All",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.recs.get(EPICSRecs.abortAll),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);

	//highPanel.add(new FJECSubSystemPanel("cc:"));
	add("0.01,0.01;0.99,0.80",highPanel);
	add("0.90,0.82;0.10,0.16",abortAllButton);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("cc:"));
	/*
	JPanel scrollPanel = new JPanel();
	scrollPanel.setLayout(new GridLayout(0,1));
	for (int i=0; i<fjecmotors.length; i++)
	    if (fjecmotors[i] instanceof FJECMotorGrating) {
		scrollPanel.add(((FJECMotorGrating)fjecmotors[i]).getMotorHiResGratingPanel());
	    }
	add("0.33,0.81;0.66,0.20",new JScrollPane(scrollPanel));
	*/
    }
}
