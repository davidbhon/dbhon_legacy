package uffjec;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.border.*;

import javaUFLib.*;
import javaUFProtocol.*;
import java.io.*;
import ufjca.*;

public class JPanelOIWFS extends JPanel {

    static final long serialVersionUID = 0;
    JScrollPane imgScrollPane;
    JPanel imgDisplayPanel;
    MemoryImageSource mis;
    Image fitsImage;

    JTextField fitsPath;
    JButton fitsMovieButton;
    JCheckBox stretchBox, ratioBox;

    boolean keepGoing,stillRunning;
    IndexColorModel icm;
	
    public JPanelOIWFS() {
	byte [] r = new byte[256];byte [] g = new byte[256];byte [] b = new byte[256];
	for (int i=0; i<256; i++) { r[i] = b[i] = g[i] = (byte) i; }
	icm =new IndexColorModel(8,256,r,g,b);

	String [] tmp = {"QUIET","NONE","MIN","FULL","MAX"};
	EPICSComboBox debugLevelBox = new EPICSComboBox(EPICS.OIWFSprefix+"debug.A",tmp,EPICSComboBox.ITEM);

	EPICSTextField moveXField = new EPICSTextField(EPICS.OIWFSprefix+"move.A","");
	EPICSTextField moveYField = new EPICSTextField(EPICS.OIWFSprefix+"move.B","");
	EPICSTextField tolXYField = new EPICSTextField(EPICS.OIWFSprefix+"tolerance.A","");
	EPICSTextField tolZField  = new EPICSTextField(EPICS.OIWFSprefix+"tolerance.B","");

	JPanel debugPanel = new JPanel();
	debugPanel.setLayout(new GridLayout(0,1));
	debugPanel.add(new JLabel("Debug Level",JLabel.CENTER));
	debugPanel.add(debugLevelBox);
	debugPanel.setBorder(new EtchedBorder(0));

	JPanel moveXPanel = new JPanel();
	moveXPanel.setLayout(new RatioLayout());
	moveXPanel.add("0.01,0.01;0.40,0.99",new JLabel("X-axis position:"));
	moveXPanel.add("0.41,0.01;0.20,0.99",moveXField);
	moveXPanel.add("0.61,0.01;0.20,0.99",new JLabel("(mm)"));
	moveXPanel.add("0.81,0.01;0.20,0.99",new EPICSLabel(EPICS.OIWFSprefix+"move.VALA",""));
	JPanel moveYPanel = new JPanel();
	moveYPanel.setLayout(new RatioLayout());
	moveYPanel.add("0.01,0.01;0.40,0.99",new JLabel("Y-axis position:"));
	moveYPanel.add("0.41,0.01;0.20,0.99",moveYField);
	moveYPanel.add("0.61,0.01;0.20,0.99",new JLabel("(mm)"));
	moveYPanel.add("0.81,0.01;0.20,0.99",new EPICSLabel(EPICS.OIWFSprefix+"move.VALB",""));
	JPanel movePanel = new JPanel();
	movePanel.setLayout(new GridLayout(0,1));
	movePanel.add(new JLabel("Probe Movement",JLabel.CENTER));
	movePanel.add(moveXPanel);
	movePanel.add(moveYPanel);
	movePanel.setBorder(new EtchedBorder(0));

	JPanel tolXYPanel = new JPanel();
	tolXYPanel.setLayout(new RatioLayout());
	tolXYPanel.add("0.01,0.01;0.40,0.99",new JLabel("Tolerance in X&Y"));
	tolXYPanel.add("0.41,0.01;0.20,0.99",tolXYField);
	tolXYPanel.add("0.61,0.01;0.20,0.99",new JLabel("(mm)"));	
	tolXYPanel.add("0.81,0.01;0.20,0.99",new EPICSLabel(EPICS.OIWFSprefix+"tolerance.VALA",""));
	JPanel tolZPanel = new JPanel();
	tolZPanel.setLayout(new RatioLayout());
	tolZPanel.add("0.01,0.01;0.40,0.99",new JLabel("Tolerance in Z"));
	tolZPanel.add("0.41,0.01;0.20,0.99",tolZField);
	tolZPanel.add("0.61,0.01;0.20,0.99",new JLabel("(mm)"));	
	tolZPanel.add("0.81,0.01;0.20,0.99",new EPICSLabel(EPICS.OIWFSprefix+"tolerance.VALB",""));
	JPanel tolPanel = new JPanel();
	tolPanel.setLayout(new GridLayout(0,1));
	tolPanel.add(new JLabel("Tolerance",JLabel.CENTER));
	tolPanel.add(tolXYPanel);
	tolPanel.add(tolZPanel);
	tolPanel.setBorder(new EtchedBorder(0));

	UFColorButton datumButton = new UFColorButton("Datum",UFColorButton.COLOR_SCHEME_GREEN);
	datumButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"datum.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"datum.DIR",EPICS.START);
		}
	    });
	UFColorButton initButton = new UFColorButton("Init",UFColorButton.COLOR_SCHEME_YELLOW);
	initButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"init.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"init.DIR",EPICS.START);
		}
	    });
	UFColorButton parkButton = new UFColorButton("Park",UFColorButton.COLOR_SCHEME_GREEN);
	parkButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"park.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"park.DIR",EPICS.START);
		}
	    });
	UFColorButton testButton = new UFColorButton("Test",UFColorButton.COLOR_SCHEME_GREEN);
	testButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"test.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"test.DIR",EPICS.START);
		}
	    });
	UFColorButton stopButton = new UFColorButton("Stop",UFColorButton.COLOR_SCHEME_RED);
	stopButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"stop.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"stop.DIR",EPICS.START);
		}
	    });
	UFColorButton rebootButton = new UFColorButton("Reboot",UFColorButton.COLOR_SCHEME_RED);
	rebootButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"reboot.DIR",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"reboot.DIR",EPICS.START);
		}
	    });
	UFColorButton followButton = new UFColorButton("Follow",UFColorButton.COLOR_SCHEME_AQUA);
	followButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    
		}
	    });
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(3,3,10,10));
	buttonPanel.add(initButton);	
	buttonPanel.add(datumButton);
	buttonPanel.add(parkButton);
	buttonPanel.add(testButton);
	buttonPanel.add(stopButton);
	buttonPanel.add(rebootButton);
	buttonPanel.add(followButton);

	JPanel statusPanel = new JPanel();
	statusPanel.setLayout(new RatioLayout());
	//statusPanel.add("0.49,0.05;0.02,0.99",new JSeparator(JSeparator.VERTICAL));
	double yVal = 0.10;
	statusPanel.add("0.01,0.01;0.99,0.04",new JLabel("Status",JLabel.CENTER));

	statusPanel.add("0.01,0.10;0.49,0.04",new JLabel("name:",JLabel.LEFT));
	statusPanel.add("0.50,0.10;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"name",""));
	statusPanel.add("0.01,0.15;0.49,0.04",new JLabel("probeInit:",JLabel.LEFT));
	statusPanel.add("0.50,0.15;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeInit",""));
	statusPanel.add("0.01,0.20;0.49,0.04",new JLabel("probeIndex:",JLabel.LEFT));
	statusPanel.add("0.50,0.20;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeIndex",""));
	statusPanel.add("0.01,0.25;0.49,0.04",new JLabel("probePark:",JLabel.LEFT));
	statusPanel.add("0.50,0.25;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probePark",""));
	/*statusPanel.add("0.01,0.30;0.49,0.04",new JLabel("historylog:",JLabel.LEFT));
	statusPanel.add("0.50,0.30;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"historylog",""));
	*/
	statusPanel.add("0.01,0.30;0.49,0.04",new JLabel("probeC:",JLabel.LEFT));
	statusPanel.add("0.50,0.30;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeC",""));
	statusPanel.add("0.01,0.35;0.49,0.04",new JLabel("activeC:",JLabel.LEFT));
	statusPanel.add("0.50,0.35;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"activeC",""));
	statusPanel.add("0.01,0.40;0.49,0.04",new JLabel("health:",JLabel.LEFT));
	statusPanel.add("0.50,0.40;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"health",""));
	statusPanel.add("0.01,0.50;0.49,0.04",new JLabel("inPosition:",JLabel.LEFT));
	statusPanel.add("0.50,0.50;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"inPosition",""));
	statusPanel.add("0.01,0.55;0.49,0.04",new JLabel("arrayS:",JLabel.LEFT));
	statusPanel.add("0.50,0.55;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"arrayS",""));
	statusPanel.add("0.01,0.60;0.49,0.04",new JLabel("followS:",JLabel.LEFT));
	statusPanel.add("0.50,0.60;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"followS",""));
	
	
	statusPanel.add("0.01,0.65;0.49,0.04",new JLabel("present:",JLabel.LEFT));
	statusPanel.add("0.50,0.65;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"present",""));
	statusPanel.add("0.01,0.70;0.49,0.04",new JLabel("state:",JLabel.LEFT));
	statusPanel.add("0.50,0.70;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"state",""));
	
	statusPanel.add("0.01,0.75;0.49,0.04",new JLabel("probeDebug:",JLabel.LEFT));
	statusPanel.add("0.50,0.75;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeDebug",""));
	statusPanel.add("0.01,0.80;0.49,0.04",new JLabel("probeSimulate:",JLabel.LEFT));
	statusPanel.add("0.50,0.80;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeSimulate",""));
	statusPanel.add("0.01,0.85;0.49,0.04",new JLabel("toleranceC:",JLabel.LEFT));
	statusPanel.add("0.50,0.85;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"toleranceC",""));
	//statusPanel.add("0.01,0.85;0.49,0.04",new JLabel("probeX:",JLabel.LEFT));
	//statusPanel.add("0.50,0.85;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeX",""));
	//statusPanel.add("0.01,0.90;0.49,0.04",new JLabel("probeY:",JLabel.LEFT));
	//statusPanel.add("0.50,0.90;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeY",""));
	
	statusPanel.setBorder(new EtchedBorder(0));

		
	imgDisplayPanel = new JPanel(){
		public void paint(Graphics g) {
		    super.paint(g);
		    if (fitsImage != null) {
			int wd = fitsImage.getWidth(null);
			int ht = fitsImage.getHeight(null);
			if (stretchBox.isSelected()) {
			    if (ratioBox.isSelected()) {
				int x = imgDisplayPanel.getWidth();
				int y = imgDisplayPanel.getHeight();
				double r = Math.min(x,y);
				int widthX = (int)( r * wd / (double) ht);
				int widthY = (int)( r * ht / (double) wd);
				int startX = (x/2 - widthX/2);
				int startY = (y/2 - widthY/2);
				g.drawImage(fitsImage,startX,startY,widthX+startX,widthY+startY,0,0,wd,ht,this);
			    } else {
				g.drawImage(fitsImage,0,0,imgDisplayPanel.getWidth(),imgDisplayPanel.getHeight(),0,0,wd,ht,this);
			    }
			} else {
			    g.drawImage(fitsImage,imgDisplayPanel.getWidth()/2 - wd/2,imgDisplayPanel.getHeight()/2 - ht/2,this);
			}
		    }
		}
	    };
	
	imgScrollPane = new JScrollPane(imgDisplayPanel);
	

	fitsMovieButton = new JButton("Start");
	fitsMovieButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (fitsMovieButton.getText().trim().equals("Start")) {
			startMovieThread();
		    } else {
			stopMovieThread();
		    }
		}
	    });

	fitsPath = new JTextField();

	stretchBox = new JCheckBox("Stretch To Fit",true);
	ratioBox = new JCheckBox("Keep Aspect Ratio",true);
	ratioBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    repaint();
		}
	    });
	stretchBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    if (stretchBox.isSelected()){
			ratioBox.setEnabled(true);
			repaint();
		    } else {
			ratioBox.setSelected(true);
			ratioBox.setEnabled(false);
			repaint();
		    }
		}
	    });

	JButton browseButton = new JButton("Browse");
	browseButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    JFileChooser jfc = new JFileChooser();
		    if (jfc.showOpenDialog(null) == jfc.APPROVE_OPTION) {
			fitsPath.setText(jfc.getSelectedFile().getAbsolutePath());
		    }
		}
	    });

	JPanel moviePanel = new JPanel();
	moviePanel.setBorder(new EtchedBorder(0));
	moviePanel.setLayout(new RatioLayout());
	moviePanel.add("0.01,0.01;0.99,0.04",new JLabel("FITS Movie",JLabel.CENTER));
	moviePanel.add("0.01,0.05;0.10,0.05",new JLabel("File:"));
	moviePanel.add("0.11,0.05;0.69,0.05",fitsPath);
	moviePanel.add("0.80,0.05;0.20,0.05",browseButton);
	moviePanel.add("0.01,0.10;0.33,0.05",stretchBox);
	moviePanel.add("0.34,0.10;0.33,0.05",ratioBox);
	moviePanel.add("0.67,0.10;0.33,0.05",fitsMovieButton);
	moviePanel.add("0.01,0.15;0.99,0.85",imgDisplayPanel);
	

	setLayout(new RatioLayout());
	add("0.05,0.30;0.25,0.20",movePanel);
	add("0.05,0.50;0.25,0.20",tolPanel);
	add("0.05,0.10;0.10,0.10",debugPanel);
	add("0.30,0.10;0.45,0.75",moviePanel);
	add("0.75,0.10;0.24,0.75",statusPanel);
	add("0.01,0.75;0.29,0.25",buttonPanel);
	//add imgdisplaypanel using bogus numbers, then call
	// the resize callback function to maintain an 80x80
	// pixel size for the panel
	//add("0.65,0.85;0.01,0.01",imgDisplayPanel);
    }

    public void setImageData(UFProtocol ufp, UFFrameConfig uffc) {
	if (uffc.depth == 16) {
	    if (ufp instanceof UFShorts) {
		UFShorts ufs = (UFShorts)ufp;
		short [] vals = ufs.values();
		int [] ivals = new int[vals.length];
		int max = -1;
		int min = 65536;
		for (int i=0; i<vals.length; i++) { ivals[i] = vals[i]; if (ivals[i] > max) max = ivals[i]; if (ivals[i] < min) min = ivals[i]; }
		for (int i=0; i<vals.length; i++) { ivals[i] -= min; ivals[i] = (int)( ivals[i] / ((double)(max - min)) * 255.0); }
		fitsImage = createImage(new MemoryImageSource(uffc.width,uffc.height,icm,ivals,0,uffc.width));
	    } else {
		System.err.println("JPanelOIWFS.setImageData> FrameConfig reports depth of "+uffc.depth+", but UFProtocol object is "+ ufp.description());
	    }
	} else if (uffc.depth == 32) {
	    if (ufp instanceof UFInts) {
		UFInts ufi = (UFInts)ufp;
		fitsImage = createImage(new MemoryImageSource(uffc.width,uffc.height,icm,ufi.values(),0,uffc.width));
	    } else {
		System.err.println("JPanelOIWFS.setImageData> FrameConfig reports depth of "+uffc.depth+", but UFProtocol object is "+ ufp.description());
	    }
	} else {
	    System.err.println("JPanelOIWFS.setImageData> Unsupported FITS file pixel depth? depth = " + uffc.depth);
	}	
	imgDisplayPanel.repaint();
    }

    public void startMovieThread() {
	new Thread() {
	    public void run() {
		keepGoing = true;
		stillRunning = true;
		fitsMovieButton.setText("Stop");
		String fullFitsPath = fitsPath.getText();
		while (keepGoing) {
		    if (fullFitsPath == null || fullFitsPath.trim().equals("")) break;
		    File f = new File(fullFitsPath);
		    if (f.exists() && f.length() >= 12800){ 
			UFFITSheader ufh = new UFFITSheader();
			setImageData(ufh.readFITSfile(fullFitsPath),ufh.getFrameCon());
		    }
		    try { sleep(1000); } catch (Exception e ) {}
		}
		stillRunning = false;
		keepGoing = false; // (just in case)
		fitsMovieButton.setText("Start");
	    }
	}.start();
    }

    public void stopMovieThread() {
	keepGoing = false;
	while (stillRunning);
    }

    public static void main(String [] args) {
	EPICS.OIWFSprefix = "f2:wfs:";
	for (int i=0; i<args.length; i++) 
	    if (args[i] != null && !args[i].trim().equals("")) {
		if (args[i].trim().toLowerCase().equals("-epics")){
		    if (args.length > (i+1))
			EPICS.OIWFSprefix = args[i+1];
		} else
		    EPICS.OIWFSprefix = args[i];
	    }
	if (EPICS.OIWFSprefix == null || EPICS.OIWFSprefix.trim().equals("")) EPICS.OIWFSprefix = "null:";
	if (!EPICS.OIWFSprefix.trim().endsWith(":")) EPICS.OIWFSprefix = EPICS.OIWFSprefix.trim()+":";
	EPICS.setupStaticVars();
	JFrame j = new JFrame("FJEC OIWFS Panel");
	j.setContentPane(new JPanelOIWFS());
	j.setSize(950,600);
	j.setDefaultCloseOperation(3);
	j.setVisible(true);
	UFCAToolkit.startMonitorLoop();
    }
    
}
