package uffjec;

import javaUFLib.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

public class JPanelDetectorPreamp extends JPanel {
    static final long serialVersionUID = 0;
	
    EPICSTextField [] preVals = new EPICSTextField[32];
    EPICSLabel [] preOutVals = new EPICSLabel[32];
    EPICSLabel [] preSads = new EPICSLabel[32];
    JButton[] incButtons = new JButton[32];
    JButton[] decButtons = new JButton[32];
    JTextField globalVals, oddVals, evenVals;
    JButton globalInc, oddInc, evenInc;
    JButton globalDec, oddDec, evenDec;
    JTextField incVal, decVal;
    JRadioButton bVolts, bBits;
    ButtonGroup units;
    float vbits = 9.4f/255.f;

    public JPanelDetectorPreamp() {
	JPanel prePanel = new JPanel();
	prePanel.setLayout(new RatioLayout());
	JPanel prePanel2 =new JPanel();
	prePanel2.setLayout(new RatioLayout());
	JPanel preLabelPanel = new JPanel();
	preLabelPanel.setLayout(new RatioLayout());
	JPanel preLabelPanel2 =new JPanel();
	preLabelPanel2.setLayout(new RatioLayout());

	JPanel prePanel3 = new JPanel();
	prePanel3.setLayout(new RatioLayout());

	preLabelPanel.add("0.01,0.01;0.20,0.99",new JLabel("DAC",JLabel.CENTER));
	preLabelPanel.add("0.21,0.01;0.16,0.99",new JLabel("Desired",JLabel.CENTER));
	preLabelPanel.add("0.37,0.01;0.16,0.99",new JLabel("Set CMD",JLabel.CENTER));
	preLabelPanel.add("0.53,0.01;0.16,0.99",new JLabel("Status",JLabel.CENTER));
	preLabelPanel2.add("0.01,0.01;0.20,0.99",new JLabel("DAC",JLabel.CENTER));
	preLabelPanel2.add("0.21,0.01;0.16,0.99",new JLabel("Desired",JLabel.CENTER));
	preLabelPanel2.add("0.37,0.01;0.16,0.99",new JLabel("Set CMD",JLabel.CENTER));
	preLabelPanel2.add("0.53,0.01;0.16,0.99",new JLabel("Status",JLabel.CENTER));
	
 	for (int i=0; i<preVals.length/2; i++) {
	    final int myI = i;
 	    //preVals[i] = new EPICSTextField(EPICS.prefix + "dc:setup.Preamp" + (i<9?"0"+(i+1):""+(i+1)),"",true);
            preVals[i] = new EPICSTextField("preAmp"+i,"",true);
	    preVals[i].setYellowState(true);
	    preVals[i].setPreamp(true);
 	    //preOutVals[i] = new EPICSLabel(EPICS.prefix + "dc:setup.VALPreamp" + (i<9?"0"+(i+1):""+(i+1)),"");
            preOutVals[i] = new EPICSLabel("valPreAmp"+i,"");
	    preOutVals[i].setPreamp(true);
 	    //preSads[i] = new EPICSLabel(EPICS.prefix + "sad:DACPRE" + (i<=9?"0"+(i):""+(i)),"");
            preSads[i] = new EPICSLabel("sadPreAmp"+i,"");
	    preSads[i].setPreamp(true);
	    incButtons[i] = new JButton("+");
            decButtons[i] = new JButton("-");
	    incButtons[i].addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    float oldval = 0;
                    float inc = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        inc = Float.parseFloat(incVal.getText());
			if (bBits.isSelected()) inc*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval+inc;
		    preVals[myI].set_and_putcmd(""+newval);
		}
	    });
            decButtons[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    float oldval = 0;
                    float dec = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        dec = Float.parseFloat(decVal.getText());
                        if (bBits.isSelected()) dec*=vbits;
                    } catch (NumberFormatException e) { }
		    float newval = oldval-dec;
                    preVals[myI].set_and_putcmd(""+newval);
                }
            });
	    prePanel.add("0.01,"+(i/(preVals.length/2.0))+";0.20,"+(1.0/(preVals.length/2.0)),new JLabel("Preamp "+(i+1)));
 	    prePanel.add("0.21,"+(i/(preVals.length/2.0))+";0.16,"+(1.0/(preVals.length/2.0)),preVals[i]);
 	    prePanel.add("0.37,"+(i/(preVals.length/2.0))+";0.18,"+(1.0/(preVals.length/2.0)),preOutVals[i]);
 	    prePanel.add("0.55,"+(i/(preVals.length/2.0))+";0.18,"+(1.0/(preVals.length/2.0)),preSads[i]);
	    prePanel.add("0.73,"+(i/(preVals.length/2.0))+";0.13,"+(1.0/(preVals.length/2.0)),incButtons[i]);
            prePanel.add("0.86,"+(i/(preVals.length/2.0))+";0.13,"+(1.0/(preVals.length/2.0)),decButtons[i]);
 	}
 	for (int i=preVals.length/2; i<preVals.length; i++) {
            final int myI = i;
 	    //preVals[i] = new EPICSTextField(EPICS.prefix + "dc:setup.Preamp" +(i<9?"0"+(i+1):""+(i+1)),"",true);
 	    //preOutVals[i] = new EPICSLabel(EPICS.prefix + "dc:setup.VALPreamp" +(i<9?"0"+(i+1):""+(i+1)),"");
 	    //preSads[i] = new EPICSLabel(EPICS.prefix + "sad:DACPRE" + (i<=9?"0"+(i):""+(i)),"");
            preVals[i] = new EPICSTextField("preAmp"+i,"",true);
            preVals[i].setYellowState(true);
            preVals[i].setPreamp(true);
            preOutVals[i] = new EPICSLabel("valPreAmp"+i,"");
            preOutVals[i].setPreamp(true);
            preSads[i] = new EPICSLabel("sadPreAmp"+i,"");
            preSads[i].setPreamp(true);
            incButtons[i] = new JButton("+");
            decButtons[i] = new JButton("-");
            incButtons[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    float oldval = 0;
                    float inc = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        inc = Float.parseFloat(incVal.getText());
                        if (bBits.isSelected()) inc*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval+inc;
                    preVals[myI].set_and_putcmd(""+newval);
                }
            });
            decButtons[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    float oldval = 0;
                    float dec = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[myI].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        dec = Float.parseFloat(decVal.getText());
                        if (bBits.isSelected()) dec*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval-dec;
                    preVals[myI].set_and_putcmd(""+newval);
                }
            });
 	    prePanel2.add("0.01,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.20,"+(1.0/(preVals.length/2.0)),new JLabel("Preamp "+(i+1)));
 	    prePanel2.add("0.21,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.16,"+(1.0/(preVals.length/2.0)),preVals[i]);
 	    prePanel2.add("0.37,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.18,"+(1.0/(preVals.length/2.0)),preOutVals[i]);
 	    prePanel2.add("0.55,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.18,"+(1.0/(preVals.length/2.0)),preSads[i]);
            prePanel2.add("0.73,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.13,"+(1.0/(preVals.length/2.0)),incButtons[i]);
            prePanel2.add("0.86,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.13,"+(1.0/(preVals.length/2.0)),decButtons[i]);
 	}

        globalVals = new JTextField();
	globalInc = new UFColorButton("+");
        globalDec = new UFColorButton("-");
	globalVals.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ev) {
                for (int i = 0; i < preVals.length; i++) {
                    preVals[i].set_and_putcmd(globalVals.getText());
                }
	    }
	});
	globalInc.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ev) {
		for (int i = 0; i < preVals.length; i++) {
                    float oldval = 0;
		    float inc = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        inc = Float.parseFloat(incVal.getText());
                        if (bBits.isSelected()) inc*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval+inc;
                    preVals[i].set_and_putcmd(""+newval);
                }
	    }
	});
        globalDec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 0; i < preVals.length; i++) {
                    float oldval = 0;
                    float dec = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        dec = Float.parseFloat(decVal.getText());
                        if (bBits.isSelected()) dec*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval-dec;
                    preVals[i].set_and_putcmd(""+newval);
                }
            }
        });
	prePanel3.add("0.01,0.00;0.25,"+(1.0/(preVals.length/2.0)), new JLabel("Global: "));
	prePanel3.add("0.26,0.00;0.24,"+(1.0/(preVals.length/2.0)), globalVals);
        prePanel3.add("0.50,0.00;0.25,"+(1.0/(preVals.length/2.0)), globalInc);
        prePanel3.add("0.75,0.00;0.25,"+(1.0/(preVals.length/2.0)), globalDec);

        oddVals = new JTextField();
        oddInc = new UFColorButton("+");
        oddDec = new UFColorButton("-");
        oddVals.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 0; i < preVals.length; i+=2) {
                    preVals[i].set_and_putcmd(oddVals.getText());
                }
            }
        });
        oddInc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 0; i < preVals.length; i+=2) {
                    float oldval = 0;
                    float inc = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        inc = Float.parseFloat(incVal.getText());
                        if (bBits.isSelected()) inc*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval+inc;
                    preVals[i].set_and_putcmd(""+newval);
                }
            }
        });
        oddDec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 0; i < preVals.length; i+=2) {
                    float oldval = 0;
                    float dec = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        dec = Float.parseFloat(decVal.getText());
                        if (bBits.isSelected()) dec*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval-dec;
                    preVals[i].set_and_putcmd(""+newval);
                }
            }
        });
        prePanel3.add("0.01,"+(1.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), new JLabel("Odd:    "));
	prePanel3.add("0.26,"+(1.0/(preVals.length/2.0))+";0.24,"+(1.0/(preVals.length/2.0)), oddVals);
        prePanel3.add("0.50,"+(1.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), oddInc);
        prePanel3.add("0.75,"+(1.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), oddDec);

        evenVals = new JTextField();
        evenInc = new UFColorButton("+");
        evenDec = new UFColorButton("-");
        evenVals.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 1; i < preVals.length; i+=2) {
                    preVals[i].set_and_putcmd(evenVals.getText());
                }
            }
        });
        evenInc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 1; i < preVals.length; i+=2) {
                    float oldval = 0;
                    float inc = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        inc = Float.parseFloat(incVal.getText());
                        if (bBits.isSelected()) inc*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval+inc;
                    preVals[i].set_and_putcmd(""+newval);
                }
            }
        });
        evenDec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                for (int i = 1; i < preVals.length; i+=2) {
                    float oldval = 0;
                    float dec = 1;
                    try {
                        oldval = Float.parseFloat(preOutVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        oldval = Float.parseFloat(preVals[i].getText());
                    } catch (NumberFormatException e) { }
                    try {
                        dec = Float.parseFloat(decVal.getText());
                        if (bBits.isSelected()) dec*=vbits;
                    } catch (NumberFormatException e) { }
                    float newval = oldval-dec;
                    preVals[i].set_and_putcmd(""+newval);
                }
            }
        });
        prePanel3.add("0.01,"+(2.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), new JLabel("Even:   "));
        prePanel3.add("0.26,"+(2.0/(preVals.length/2.0))+";0.24,"+(1.0/(preVals.length/2.0)), evenVals);
        prePanel3.add("0.50,"+(2.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), evenInc);
        prePanel3.add("0.75,"+(2.0/(preVals.length/2.0))+";0.25,"+(1.0/(preVals.length/2.0)), evenDec);

	incVal = new JTextField();
	incVal.setText("1");
	decVal = new JTextField();
	decVal.setText("1");

        prePanel3.add("0.01,"+(4.0/(preVals.length/2.0))+";0.4,"+(1.0/(preVals.length/2.0)), new JLabel("Increment:"));
        prePanel3.add("0.41,"+(4.0/(preVals.length/2.0))+";0.24,"+(1.0/(preVals.length/2.0)), incVal);

        prePanel3.add("0.01,"+(5.0/(preVals.length/2.0))+";0.4,"+(1.0/(preVals.length/2.0)), new JLabel("Decrement:"));
        prePanel3.add("0.41,"+(5.0/(preVals.length/2.0))+";0.24,"+(1.0/(preVals.length/2.0)), decVal);

	bVolts = new JRadioButton("Volts", true);
	bBits = new JRadioButton("Bits", true);
	units = new ButtonGroup();
	units.add(bVolts);
	units.add(bBits);
	prePanel3.add("0.01,"+(6.0/(preVals.length/2.0))+";0.4,"+(1.0/(preVals.length/2.0)), bVolts);
        prePanel3.add("0.51,"+(6.0/(preVals.length/2.0))+";0.4,"+(1.0/(preVals.length/2.0)), bBits);

	setLayout(new RatioLayout());
	prePanel.setBorder(new EtchedBorder(0));
	prePanel2.setBorder(new EtchedBorder(0));
        prePanel3.setBorder(new EtchedBorder(0));
	
	add("0.01,0.01;0.35,0.05",preLabelPanel);
	add("0.01,0.06;0.35,0.80",prePanel);
	add("0.36,0.01;0.35,0.05",preLabelPanel2);
	add("0.36,0.06;0.35,0.80",prePanel2);
        add("0.71,0.06;0.20,0.80",prePanel3);
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("dc:"));
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
    
    }
}
