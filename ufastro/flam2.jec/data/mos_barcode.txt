; This is the MOS barcode parameter file
; this should contain the named positions 
; of the MOS wheel (copied from the 
; motor parameter file), as well as
; the database of MOS wheel plates and
; (maybe?) barcode numbers
;
;EPICS prefix
sad:
; Number of positions
13
; position data
; steps from home : name
 100                MOSCIRC1
 152                MOSCIRC2
 195                MOSPLT01
 225                MOSPLT02
 350                MOSPLT03
 445                MOSPLT04
 495		    MOSPLT05
 570		    MOSPLT06
 635		    MOSPLT07
 670		    MOSPLT08
 690		    MOSPLT09
 710                MOSPLT10
 730                MOSPLT11
; barcode data
