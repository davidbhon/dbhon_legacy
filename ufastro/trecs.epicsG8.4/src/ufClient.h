#if !defined(__UFClient_h__)
#define __UFClient_h__ "$Name:  $ $Id: ufClient.h,v 0.2 2002/07/11 16:55:14 dan beta $"
#define __UFClient_H__(arg) static const char arg##Client_h__rcsId[] = __UFClient_h__;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)

#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "fcntl.h"
#include "ioLib.h"
#include "fioLib.h"
#include "in.h"
#include <sys/ioctl.h>
#include "stdio.h"
#include "time.h"
#include "hostLib.h"
#include "math.h"
#include "inetLib.h"
#include "ctype.h"
#include "string.h"

#else

#include "sys/types.h"
#include "arpa/inet.h"

#endif

typedef struct
{
  int length, type, elem;
  short seqCnt, seqTot;
  float duration;
  char timestamp[25];
  char name[83];
}
ufProtocolHeader;

typedef struct
{
  ufProtocolHeader hdr;
  /* note there are now 15 (int) elements */
  int w, h, d, littleEnd, dmaCnt, imageCnt, coAdds,
    pixelSortID, frameObsSeqNo, frameObsSeqTot,
    chopBeam, saveSet, nodBeam, nodSet, frameWriteCnt;
}
ufFrameConfig;

typedef struct
{
  ufProtocolHeader hdr;
  int nodBeams, chopBeams, saveSets, nodSets;
}
ufObsConfig;

/**
 * ufObsConfig struct is not sent as defined, instead the hdr is sent and then
 * the contiguous vector of 16bit flags that indicate the complete observation sequence is sent.
 * (see obsFlags in ufObsConfSend/Recv).
 */

/* basic os functions */

/** posix nanosleep */
extern void ufSleep (float time);

extern const char *ufHostName ();
extern const char *ufHostTime (char *tz);

/** sockets or fifos */
extern int ufAvailable (int fd);

/** named pipe/fifos */
extern int ufFifoAvailable (const char *fifoname);

/**
 * Agent (service) names, server IP addresse, default ports
 * (should be in /etc/services file)
 * return int number of list entrees, entry string looks like:
 * "AgentName@ServerHostName:nnn.nnn.nnn.nnn~PortNo"
 */
extern int ufServices (char **infolist);

/* basic socket functions */

extern int ufIsIPAddress (const char *host);
extern int ufSetSocket (int socFd, struct sockaddr_in *addr, int portNo);
extern int ufConnect (const char *host, int portNo);
extern int ufClose (int socFd);

/** raw byte array output */

extern int ufSend (int socFd, const unsigned char *sndbuf, int nb);

extern int ufSendShort (int socFd, short val);	/* htons */
extern int ufSendShorts (int socFd, short *vals, int cnt);	/* htons 
								 */
extern int ufSendInt (int socFd, int val);	/* htonl */
extern int ufSendInts (int socFd, int *vals, int cnt);	/* htonl 
							 */
extern int ufSendFloat (int socFd, float val);	/* htonl */
extern int ufSendFloats (int socFd, float *vals, int cnt);	/* htonl 
								 */
extern int ufSendCstr (int socFd, const char *s);

/** raw byte array input */

extern int ufRecv (int socFd, unsigned char *rbuf, int len);

extern int ufSetTimeout (float timeout);	/* timeout
						   (sec) in 
						   ufRecv */

extern short ufRecvShort (int socFd);	/* ntohs */
extern int ufRecvShorts (int socFd, short *vals, int cnt);	/* ntohs 
								 */
extern int ufRecvInt (int socFd);	/* ntohl */
extern int ufRecvInts (int socFd, int *vals, int cnt);	/* ntohl 
							 */
extern float ufRecvFloat (int socFd);	/* ntohl */
extern int ufRecvFloats (int socFd, float *vals, int cnt);	/* ntohl 
								 */

/** reset null terminated string */
extern int ufRecvCstr (int socFd, char *rbuf, int len);

/** allocate new null terminated string */
extern int ufRecvNewCstr (int socFd, char **rbuf);


/** uf protocol object header socket i/o */

extern int ufHeaderSend (int socFd, ufProtocolHeader * hdr);
extern int ufHeaderRecv (int socFd, ufProtocolHeader * hdr);
extern int ufHeaderLength (const ufProtocolHeader * hdr);

/* ufStrings socket i/o *********/

extern int ufStringsSend (int socFd, char **strings, int Nstrings,
			  const char *name);
extern char **ufStringsRecv (int socFd, int *Nstring, ufProtocolHeader * hdr,
			     int *nbtot);

/*** all the func. below are by Agent name, rather than socFd */

extern int ufConnectAgent (const char *agent, const char *host, int portNo);
extern int ufCloseAgent (const char *agent);
extern int ufAgentSocket (const char *agent);

/******** uf protocol object header i/o **********/

extern int ufHeaderSendAgent (const char *agent, ufProtocolHeader * hdr,
			      int *socFd);
extern int ufHeaderRecvAgent (const char *agent, ufProtocolHeader * hdr,
			      int *socFd);

/*** ufStrings i/o *********/

extern int ufStringsSendAgent (const char *agent, char **strings,
			       int Nstrings, const char *name);
extern char **ufStringsRecvAgent (const char *agent, int *Nstring,
				  ufProtocolHeader * hdr, int *nbtot);

/*** ufInts (32-bit) i/o: BufferName is optional input, ufProtocolHeader* hdr is output. */

extern int ufIntsSend (const char *agent, int *bufptr, int bufsiz,
		       const char *BufferName);
extern int ufIntsRecv (const char *agent, int *bufptr, int bufsiz,
		       ufProtocolHeader * hdr);

/*** ufFrameConfig & ufObsConfig i/o *********/

extern int ufFrameConfSend (const char *agent, ufFrameConfig * uffc);
extern int ufFrameConfRecv (const char *agent, ufFrameConfig * uffc);
extern int ufObsConfSend (const char *agent, ufObsConfig * ufoc,
			  short *obsFlags);
extern int ufObsConfRecv (const char *agent, ufObsConfig * ufoc,
			  short **obsFlags);

/* ufRequest:
 *    request = "buffer name" for Frame from a buffer,
 * or request = "BN" for buffer names (ufStrings),
 * or request = "FC" for ufFrameConfig,
 * or request = "OC" for ufObsConfig.
 *
 * handshake: client sends UFTimeStamp with request in "name" field, 
 * server replies with UFLIB object (Hdr and data array).
 */

extern int ufRequest (const char *agent, const char *request);

#endif /* __UFClient_h__ */
