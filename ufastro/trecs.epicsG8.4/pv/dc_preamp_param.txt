;Detector PreAmp Offsets Parameter File
;
;dc agent host IP number
000.000.000.000
;dc agent port number
52008
Name(no spaces)  Register  LatchOrder  Bias(volts)  DacScaleFactor(#/volts)  DacOffset(#)

Offset_01          0         1         9.00            25.5                  0
Offset_02          1         1         9.00            25.5                  0
Offset_03          2         1         9.00            25.5                  0
Offset_04          3         1         9.00            25.5                  0
Offset_05          4         1         9.00            25.5                  0
Offset_06          5         1         9.00            25.5                  0
Offset_07          6         1         9.00            25.5                  0
Offset_08          7         1         9.00            25.5                  0
Offset_09          8         1         9.00            25.5                  0
Offset_10          9         1         9.00            25.5                  0
Offset_11         10         1         9.00            25.5                  0
Offset_12         11         1         9.00            25.5                  0
Offset_13         12         1         9.00            25.5                  0
Offset_14         13         1         9.00            25.5                  0
Offset_15         14         1         9.00            25.5                  0
Offset_16         15         1         9.00            25.5                  0

