#!/bin/tcsh -f
# rudimentary script to count rcs files via grep \,v
set file = "cvs_ls.list"
if( "$1" != "" ) set file = "$1"
echo '$file == ' $file 
grep CVSROOT $file | sort -u
echo -n 'grep akefile\,v $file | sort -u | wc -l == '
grep 'akefile\,v' $file | sort -u | wc -l
echo -n 'grep \.gz $file | sort -u | grep -v tar | wc -l == '
grep '\.gz' $file | sort -u | grep -v tar | wc -l
echo -n 'grep \.bz2 $file | sort -u | wc -l == '
grep '\.bz2' $file | sort -u | wc -l
echo -n 'grep \.tar $file | sort -u | wc -l == '
grep '\.tar' $file | sort -u | wc -l
echo -n 'grep \.tgz\,v $file | sort -u | wc -l == '
grep '\.tgz\,v' $file | sort -u | wc -l
echo -n 'grep \.cfg\,v $file | sort -u | wc -l == '
grep '\.cfg\,v' $file | sort -u | wc -l
echo -n 'grep \.conf\,v $file | sort -u | wc -l == '
grep '\.conf\,v' $file | sort -u | wc -l
echo -n 'grep \.png\,v $file | sort -u | wc -l == '
grep '\.png\,v' $file | sort -u | wc -l
echo -n 'grep \.jpg\,v $file | sort -u | wc -l == '
grep '\.jpg\,v' $file | sort -u | wc -l
echo -n 'grep \.jpeg\,v $file | sort -u | wc -l == '
grep '\.jpeg\,v' $file | sort -u | wc -l
echo -n 'grep \.htm\,v $file | sort -u | wc -l == '
grep '\.htm\,v' $file | sort -u | wc -l
echo -n 'grep \.html\,v $file | sort -u | wc -l == '
grep '\.html\,v' $file | sort -u | wc -l
echo -n 'grep \.cgi\,v $file | sort -u | wc -l == '
grep '\.cgi\,v' $file | sort -u | wc -l
echo -n 'grep \.php\,v $file | sort -u | wc -l == '
grep '\.php\,v' $file | sort -u | wc -l
echo -n 'grep \.js\,v $file | sort -u | wc -l == '
grep '\.js\,v' $file | sort -u | wc -l
echo -n 'grep \.jsp\,v $file | sort -u | wc -l == '
grep '\.jsp\,v' $file | sort -u | wc -l
echo -n 'grep \.jsl\,v $file | sort -u | wc -l == '
grep '\.jsl\,v' $file | sort -u | wc -l
echo -n 'grep \.csh\,v $file | sort -u | wc -l == '
grep '\.csh\,v' $file | sort -u | wc -l
echo -n 'grep \.ksh\,v $file | sort -u | wc -l == '
grep '\.ksh\,v' $file | sort -u | wc -l
echo -n 'grep \.sh\,v $file | sort -u | wc -l == '
grep '\.sh\,v' $file | sort -u | wc -l
echo -n 'grep \.h\,v $file | sort -u | wc -l == '
grep '\.h\,v' $file | sort -u | wc -l
echo -n 'grep \.H\,v $file | sort -u | wc -l == '
grep '\.H\,v' $file | sort -u | wc -l
echo -n 'grep \.c\,v $file | sort -u | wc -l == '
grep '\.c\,v' $file | sort -u | wc -l
echo -n 'grep \.C\,v $file | sort -u | wc -l == '
grep '\.C\,v' $file | sort -u | wc -l
echo -n 'grep \.cc\,v $file | sort -u | wc -l == '
grep '\.cc\,v' $file | sort -u | wc -l
echo -n 'grep \.cpp\,v $file | sort -u | wc -l == '
grep '\.cpp\,v' $file | sort -u | wc -l
echo -n 'grep \.f77\,v $file | sort -u | wc -l == '
grep '\.f77\,v' $file | sort -u | wc -l
echo -n 'grep \.f\,v $file | sort -u | wc -l == '
grep '\.f\,v' $file | sort -u | wc -l
echo -n 'grep \.java\,v $file | sort -u | wc -l == '
grep '\.java\,v' $file | sort -u | wc -l
echo -n 'grep \.pl\,v $file | sort -u | wc -l == '
grep '\.pl\,v' $file | sort -u | wc -l
echo -n 'grep \.pm\,v $file | sort -u | wc -l == '
grep '\.pm\,v' $file | sort -u | wc -l
echo -n 'grep \.py\,v $file | sort -u | wc -l == '
grep '\.py\,v' $file | sort -u | wc -l
echo -n 'grep \.tcl\,v $file | sort -u | wc -l == '
grep '\.tcl\,v' $file | sort -u | wc -l
echo -n 'grep \,v $file | sort -u | grep -v \. | grep -v akefile\,v | wc -l == '
grep '\,v' $file | sort -u | grep -v '\.' | grep -v 'akefile\,v' | wc -l
echo -n 'grep \,v $file | sort -u | wc -l == '
grep '\,v' $file | sort -u | wc -l
exit
