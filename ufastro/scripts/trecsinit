#!/bin/tcsh
# RCS: "$Name:  $ $Id: trecsinit,v 1.2 2002/10/08 19:57:17 dan beta $"

# directive enum from /gemini/epics/base/include/cad.h
# if these change this script is broken.
set CAD_ABORT = 0
set CAD_MARK = 0
set CAD_CLEAR  = 1
set CAD_PRESET = 2
set CAD_START = 3
set CAD_STOP = 4
set CAD_ACCEPT = 0 
set CAD_REJECT = -1

# $epics':stop.DIR' $CAD_MARK
set doHelp = "false"
set epics = "trecs"

# Do a simple traversal of the command line to determine if -help or -h
# have been issued
set index = 1

while( $index <= $#argv )
    if( "$argv[$index]" == "-h" || "$argv[$index]" == "-help" ) then
	set doHelp = "true"
    endif
    @ index++
end

# Set UFINSTALL if necessary
if( ! $?UFINSTALL ) then
    echo "No default UFINSTALL found, using /usr/local/uf"
    echo ""
    echo "This may be changed by adding this environment variable"
    echo "to your .cshrc/.tcshrc file.  Note that simply setting this"
    echo "variable in your current environment will not work, since"
    echo "this script forks a new shell."
    echo ""
    setenv UFINSTALL /usr/local/uf
endif

source $UFINSTALL/.ufcshrc

if( "$doHelp" == "true" ) then
  echo ""
  echo "Usage: $0 [epicsdb] [-help|-h] [-init] [-stop] [-clear]"
  echo ""
  echo "The purpose of this script is to perform one or more top level"
  echo "database functions.  The database name may be specified as"
  echo "as the first command line argument.  If no database"
  echo "is specified, then trecs is assumed."
  echo ""
  echo "Here are the available database functions:"
  echo " -stop:   Halt the current observation"
  echo " -clear:  Clears the epics inputs"
  echo " -init:   Read param files, connect to agents"
  echo ""
  echo "Database functions may be given more than once, and order of"
  echo "the command line arguments is honored in executing the database"
  echo "functions."
  echo ""
  exit
endif

set ufcaput = $UFINSTALL/bin/ufcaput
set ufcaget = $UFINSTALL/bin/ufcaget
set ufsleep = $UFINSTALL/bin/ufsleep

if( ! -f $ufcaput ) then
    echo ""
    echo "ERROR: Unable to locate $ufcaput"
    echo "The ufcaput program is necessary for this script to
    echo "function properly."
    echo ""
    exit
endif

if( ! -f $ufcaget ) then
    echo ""
    echo "ERROR: Unable to locate $ufcaget"
    echo "The ufcaget program is necessary for this script to
    echo "function properly."
    echo ""
    exit
endif

if( ! -f $ufsleep ) then
    echo ""
    echo "ERROR: Unable to locate $ufsleep"
    echo "The ufsleep program is necessary for this script to
    echo "function properly."
    echo ""
    exit
endif

echo ""
echo "Using EPICS database: $epics"
echo "Using UFINSTALL directory: $UFINSTALL"

# This is a bit ugly, but was the only way I could come up with to allow
# for multiple command to be run in the order given on the command line,
# without using external scripts.
#
while( $#argv >= 1 )
    set ack = ""
    set msg = ""

    echo ""

    if( "$argv[1]" == "miri" || "$argv[1]" == "trecs" ) then
	set epics = $argv[1]
	echo "Using epics database $epics"
    else if( "$argv[1]" == "-init" ) then
	echo "Performing init..."

	# (re)Init directive
	echo -n $epics':init.A=NONE' | $ufcaput
	echo -n $epics':apply.DIR='$CAD_START | $ufcaput

	echo "Init sent, waiting for reply."
	echo "This could take a while..."

	# Read back the success/failure state
	set ack = "BUSY"

	# Check the CAR here since the apply.VAL seems to generate
	# timeout messages
	while( "$ack" == "BUSY" )
	    $UFINSTALL/bin/ufsleep 1.0
	    set ack = `$ufcaget $epics':'applyC`
	end

	# CAR is now no longer BUSY (hopefully IDLE), check to see how
	# the init request terminated by checking the apply.VAL
	set ack = `$ufcaget $epics':'apply`

	if( "$ack" == "-1" ) then
		# There was a problem, check the error message
		set msg = `$ufcaget $epics':'apply.MESS`
		echo "Init failed/rejected: $msg"
		exit
	endif

	echo "Init successful"
    else if( "$argv[1]" == "-clear" ) then
	echo "Clearing the configuration..."

	# First, clear the system
	echo -n $epics':apply.DIR='$CAD_CLEAR | $ufcaput

	# Check if the command succeeded
	while( "$ack" == "" ) 
	    $ufsleep 1.0
	    set ack = `$ufcaget $epics':'apply.VAL`
	end

	if( "$ack" == "-1" ) then
		set msg = `$ufcaget $epics':'apply.MESS`
		echo Clear failed/rejected':' $msg
		exit
	endif

	echo "Clear successful"
    else if( "$argv[1]" == "-stop" ) then
	echo "Performing stop..."

	echo -n $epics':stop.DIR'=$CAD_MARK | $ufcaput
	echo -n $epics':apply.DIR='$CAD_START | $ufcaput

	# Check if the command succeeded
	while( "$ack" == "" ) 
	    $ufsleep 1.0
	    set ack = `$ufcaget $epics':'apply.VAL`
	end

	if( "$ack" == "-1" ) then
		set msg = `$ufcaget $epics':'apply.MESS`
		echo Stop failed/rejected':' $msg
		exit
	endif

	echo "Stop successful."
    else
	echo "Unknown argument: $argv[1]"
    endif

    shift
end

echo ""
exit
