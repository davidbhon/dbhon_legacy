setenv INSTALL_LOCATION /usr/local/epics3.14.2
setenv INSTALL_DIR $INSTALL_LOCATION
#setenv EPICS_HOST_ARCH `./startup/EpicsHostArch.pl`
setenv EPICS_HOST_ARCH `$INSTALL_LOCATION/startup/EpicsHostArch.pl`
setenv LD_LIBRARY_PATH /share/local/lib:${INSTALL_LOCATION}/lib/${EPICS_HOST_ARCH}
