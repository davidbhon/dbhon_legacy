#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufenv;

$ufenv::rcsId = q($Name:  $ $Id: ufenvcgi.pl 14 2008-06-11 01:49:45Z hon $);
$ufenv::cgi = new CGI;
$ufenv::organization = "University of Florida";
$ufenv::department = "Department of Astronomy";
$ufenv::refresh = 10;

print $ufenv::cgi->header( -type => "text/html", -expires => "now", -refresh => "$ufenv::refresh" );
print $ufenv::cgi->start_html( -title=> "$ufenv::doctitle", -bgcolor=> "#ffffff" );
($ufenv::s,$ufenv::m,$ufenv::h,$ufenv::md,$ufenv::mo,$ufenv::y,$ufenv::wd,$ufenv::yd,$ufenv::isd) = localtime(time());
$ufenv::y = $ufenv::y + 1900; $ufenv::yd = $ufenv::yd + 1;
if( $ufenv::s < 10 ) { $ufenv::s = "0$ufenv::s"; }
if( $ufenv::m < 10 ) { $ufenv::m = "0$ufenv::m"; }
if( $ufenv::h < 10 ) { $ufenv::h = "0$ufenv::h"; }
$ufenv::output = "$ufenv::y:$ufenv::yd:$ufenv::h:$ufenv::m:$ufenv::s TReCS Environment (10sec. Updates):\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "=================================\n";
print $ufenv::cgi->p("$ufenv::output");

$ufenv::run = "env LD_LIBRARY_PATH=/usr/local/uf/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv /usr/local/uf/bin";
$ufenv::gp354 = `$ufenv::run/ufvac -port 52004 -host newton -q -raw 1`;
chomp $ufenv::gp354;
@ufenv::vac = split / /,$ufenv::gp354;
$ufenv::output = "Cryostat Vacuum:     $ufenv::vac[1]";
print $ufenv::cgi->p("$ufenv::output");
#
$ufenv::ls340 = `$ufenv::run/uflsc -port 52003 -host newton -q -raw 'krdg?a;krdg?b'`;
chomp $ufenv::ls340; chomp $ufenv::ls340;
@ufenv::cryotemp2 = split /;/,$ufenv::ls340;
$ufenv::output = "Array MUX:           $ufenv::cryotemp2[0]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Finger:         $ufenv::cryotemp2[1]\n";
print $ufenv::cgi->p("$ufenv::output");
#
$ufenv::ls218 = `$ufenv::run/uflsc -port 52002 -host newton -q -raw 'krdg?0'`;
chomp $ufenv::ls218;
@ufenv::cryotemp8 = split /,/,$ufenv::ls218;
$ufenv::output = "Cold Plate #1:       $ufenv::cryotemp8[0]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Plate #2:       $ufenv::cryotemp8[1]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Active Shield:       $ufenv::cryotemp8[2]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Passive Shield:      $ufenv::cryotemp8[3]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Window Turret:       $ufenv::cryotemp8[4]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Strap:  $ufenv::cryotemp8[5]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Edge:   $ufenv::cryotemp8[6]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Center: $ufenv::cryotemp8[7]";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "=================================\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::cgi->end_html();
