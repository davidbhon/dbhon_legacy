#!/bin/csh -f
set nfsmnt = /nfs/flam2sparc/data/Apr02-June26,2006
if( "$1" != "" ) set nfsmnt = "$1"
pushd $nfsmnt
set dl = `find . -type d -name "[0-Z]*"`
echo $dl
foreach d ($dl)
  pushd $d
  pwd
  mkdir -p /share/data/2006/$d
  set fl = `find . -type f`
  echo $fl
  foreach file ($fl)
    set f = $file:t
    echo $f
    gzip -c $f > /share/data/2006/$d/$f'.gz'
  end
  popd
end
