// sequence for manual PID entry
// set the control loop to "1", input "A", units "1" for Kelvin, turn on "1" ("0" off)
CSET 1, A, 1, 1

// set P, I, D for control loop "1", P (float) value, I (float) value, D value
PID 1, 12.0, 15.0, 0.0      
// get PID values
PID?

// set heater range (integer between 0 and 5)
RANGE 5
// get heater range 
RANGE?

// set the desired temperature setpoint for control loop "1" 
SETP 1, 8.0     
// get setpoint value
SETP? 1

// change mode for control loop 1, to "1" for manual PID (via PID command)
// (2 for zone, 3, for openloop, 4 autotune PID, 5 autotune PI, 5, autotune P)
CMODE 1, 1



// Query commands
KRDG? A // query sensor A for temperature in Kelvin
SRDG? A // query sensor A for reading in sensor units (hopefully volts)



