package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.tree.*;
import java.util.*;
import java.io.*;

//===============================================================================
/**
 *Master tabbed pane
 */
public class JPanelMaster extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelMaster.java,v 0.113 2003/10/23 22:52:19 varosi beta $";

    jeiCmd jeiCommand = new jeiCmd();

    String cfg_file_name = "T-ReCS.cfg";
    boolean ok_to_edit = true;
    boolean initMarked = false;

  JButton jButton_MoreObsParms = new JButton("Additional Observation Parameters");
  JmoreObsParms MoreObsParms_Window = new JmoreObsParms();

    JButton jButtonTempControl = new JButton("Det.Temp.Control");
    JButton jButtonTCindicator = new JButton("?");

  JPanel jPanelObsSetup = new JPanel();
  GridLayout gridLayoutObsSetup = new GridLayout();
  JPanel jPanelOverrides = new JPanel();
  GridLayout gridLayoutOverrides = new GridLayout();
  JPanel jPanelInstrSetup = new JPanel();
  GridLayout gridLayoutInstrSetup = new GridLayout();

  JLabel jLabelObsSetup = new JLabel("Observation Setup");
  JLabel jLabelInstrSetup = new JLabel("Instrument Setup");
  JLabel jLabelOverrides = new JLabel("Overrides");
  JLabel jLabelCameraMode = new JLabel("Camera mode");
  JLabel jLabelImagingMode = new JLabel("Imaging Mode");
  JLabel jLabelDataMode = new JLabel("Data mode");
  JLabel jLabelObservingMode = new JLabel("Observing mode");
  JLabel jLabelSkyBackgrnd = new JLabel("Sky Background");
  JLabel jLabelSkyNoise = new JLabel("Sky Noise");
  JLabel jLabelSlitWidth = new JLabel("Slit Width");
  JLabel jLabelGrating = new JLabel("Grating");
  JLabel jLabelCentralWavelength = new JLabel("Central Wavelength");

  final JButton jButtonFilter = new JButton("Filter"); //button toggles Filter override T/F

  EPICSComboBox jComboBoxCameraMode;
  EPICSComboBox jComboBoxObservingMode;
  EPICSComboBox jComboBoxImagingMode;
  EPICSComboBox jComboBoxDataMode;
  EPICSTextField jTextFieldCameraMode;
  EPICSTextField jTextFieldObservingMode;
  EPICSTextField jTextFieldImagingMode;
  EPICSTextField jTextFieldDataMode;
    JButton jButtonPutObsSetup = new JButton("rePUT>Epics");

  EPICSComboBox jComboBoxSkyBackgrnd;
  EPICSComboBox jComboBoxSkyNoise;
  EPICSComboBox jComboBoxAirMass;
  EPICSTextField jTextFieldSkyBackgrnd;
  EPICSTextField jTextFieldSkyNoise;
  EPICSTextField jTextFieldAirMass;

  EPICSComboBox jComboBoxFilter;
  EPICSComboBox jComboBoxSlitWidth;
  EPICSComboBox jComboBoxGrating;
  EPICSTextField jTextFieldFilter;
  EPICSTextField jTextFieldSlitWidth;
  EPICSTextField jTextFieldGrating;
    JButton jButtonPutInstrSetup = new JButton("rePUT>Epics");

  EPICSTextField jTextFieldCentralWavelength = new EPICSTextField(EPICS.prefix + "instrumentSetup.F",
								  EPICS.prefix + "instrumentSetup.VALF",
								  "Enter Central wavelength", true);
  EPICSTextField jTextFieldCenWaveCurrVal = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALF",
								  "Current Central wavelength");
  EPICSComboBox jComboBoxSector;
  EPICSComboBox jComboBoxAperture;
  EPICSComboBox jComboBoxPupil;
  EPICSComboBox jComboBoxLyotStop;
  EPICSComboBox jComboBoxWindow;
  EPICSTextField jTextFieldSector;
  EPICSTextField jTextFieldAperture;
  EPICSTextField jTextFieldPupil;
  EPICSTextField jTextFieldLyotStop;
  EPICSTextField jTextFieldWindow;
    JButton jButtonPutOverrides = new JButton("rePUT>Epics");
 
  JLabel jLabelOnSrcTime = new JLabel("On Source Time (mins)");
  EPICSTextField jTextFieldOnSrcTime = new EPICSTextField(EPICS.prefix + "observationSetup.B",
							  EPICS.prefix + "observationSetup.VALB",
							  jLabelOnSrcTime.getText(), true);
  JLabel jLabelChopThrow = new JLabel("Chop Throw (arcsec)");
  EPICSTextField jTextFieldChopThrow = new EPICSTextField(EPICS.prefix + "observationSetup.C",
							  EPICS.prefix + "observationSetup.VALC",
							  jLabelChopThrow.getText(), true);

  EPICSTextField jTextFieldObsDataLabel = new EPICSTextField(EPICS.prefix + "observe.A",
							     EPICS.prefix + "observe.VALA",
							     "Observation DHS Label", true);

  EPICSTextField jTextFieldObsNote = new EPICSTextField(EPICS.prefix + "dc:acqControl.J",
							EPICS.prefix + "dc:acqControlG.VALQ",
							"ObsNote for FITS header", true);

  EPICSLabel monitorApplyVAL = new EPICSLabel(EPICS.prefix + "apply.VAL", "CAD:");
  EPICSLabel monitorApplyCAR = new EPICSLabel(EPICS.prefix + "applyC.VAL", "CAR:");
  EPICSLabel monitorApplyMESS = new EPICSLabel(EPICS.prefix + "apply.MESS", "CAD-Msg:");
  EPICSLabel monitorApplyOMSS = new EPICSLabel(EPICS.prefix + "applyC.OMSS", "CAR-Msg:");

  EPICSLabel monitorObsConCAR = new EPICSLabel(EPICS.prefix + "dc:obsControlC.VAL", "ObsCntrl:");
  EPICSLabel monitorObsConOMSS = new EPICSLabel(EPICS.prefix + "dc:obsControlC.OMSS", "Msg:");

  EPICSLabel monitorAcqConCAR = new EPICSLabel(EPICS.prefix + "dc:acqControlC.VAL", "AcqCntrl:");
  EPICSLabel monitorAcqConOMSS = new EPICSLabel(EPICS.prefix + "dc:acqControlC.OMSS", "Msg:");

  EPICSLabel monitorCompConCAR = new EPICSLabel(EPICS.prefix + "cc:applyC.VAL", "MotCntrl:");
  EPICSLabel monitorCompConIMSS = new EPICSLabel(EPICS.prefix + "cc:applyC.IMSS", "Msg:");

  EPICSLabel monitorEnvConCAR = new EPICSLabel(EPICS.prefix + "ec:applyC.VAL", "EnvCntrl:");
  EPICSLabel monitorEnvConOMSS = new EPICSLabel(EPICS.prefix + "ec:applyC.OMSS", "Msg:");

  EPICSLabel monitorOnSrcTime = new EPICSLabel(EPICS.prefix + "dc:physOutG.VALC", "Adjusted Time=");
  EPICSLabel monitorObsTime = new EPICSLabel(EPICS.prefix + "sad:photonTime", "Total=");

//EPICSLabel monitorState = new EPICSLabel(EPICS.prefix + "sad:state", "Instrument State:");
  EPICSLabel monitorObserveCAR = new EPICSLabel(EPICS.prefix + "observeC.VAL", "Observe CAR:");
  EPICSTextField jTextFieldObsStatus = new EPICSTextField(EPICS.prefix + "sad:observationStatus","ObsStatus");
  EPICSTextField jTextFieldNodCycle = new EPICSTextField(EPICS.prefix + "sad:currentNodCycle","NodCycle");
  EPICSTextField jTextFieldNodBeam = new EPICSTextField(EPICS.prefix + "sad:currentBeam","NodBeam");
  EPICSTextField jTextFieldBgWellpc = new EPICSTextField(EPICS.prefix + "sad:bgWellCurrent","Well");
//EPICSTextField jTextFieldBgADUs = new EPICSTextField(EPICS.prefix + "sad:bgAdcCurrent","ADC");
  EPICSTextField jTextFieldBgSigma = new EPICSTextField(EPICS.prefix + "sad:sigmaPerFrameRead","Sigma Noise");
  EPICSTextField jTextFieldFrameCount = new EPICSTextField(EPICS.prefix + "dc:acqControlG.VALI","Frame Count");
  JLabel jLabelTotalFrames = new JLabel("out of  ?  Save Frames");
  JLabel jLabelTotalNodSets = new JLabel("of  ?");

  JCheckBox jCheckBoxSector = new JCheckBox("Sector");
  JCheckBox jCheckBoxAperture = new JCheckBox("Aperture");
  JCheckBox jCheckBoxPupil = new JCheckBox("Pupil");
  JCheckBox jCheckBoxLyotStop = new JCheckBox("Lyot Stop");
  JCheckBox jCheckBoxWindow = new JCheckBox("Window");

  JPanel jPanelStatus = new JPanel();
  JLabel jLabelObsStatus = new JLabel("Obs Status");
  JLabel jLabelNodCycle = new JLabel("Nod Cycle");
  JLabel jLabelNodBeam = new JLabel("Beam");
  JLabel jLabelProgress = new JLabel("Acquired ");
    JObsProgressBar jObsProgressBar;
    JLabel jLabelBgWellpc = new JLabel("Backgrnd: % Well");
    JLabel jLabelBgSigma = new JLabel("Noise Std.Dev.");

  JLabel jLabelConfiguration = new JLabel("Configurations");
  JScrollPane jScrollPane1 = new JScrollPane();
  JTree jTree1 = new JTree();
  DefaultTreeModel treeModel;
  JButton jButtonLoadCfg = new JButton("Load Cfg Set");
  JButton jButtonEditCfg = new JButton("Enable Cfg Edits");
  JButton jButtonDoCfg = new JButton("Do Selected Cfg");
  JButton jButtonAddCfg = new JButton("Add Cfg");
  JButton jButtonDelCfg = new JButton("Delete Cfg");
  JButton jButtonMoveCfgUp = new JButton("Move Up");
  JButton jButtonMoveCfgDown = new JButton("Move Down");
  JButton jButtonSaveCfg = new JButton("Save Cfg Set");
  JButton jButtonBlankAll = new JButton("Blank all inputs");

  JPanel jPanelButtons = new JPanel();
  JButton jButtonOpticalPath = new JButton("Show  Optical  Path");
  JButton jButtonCancel = new JButton("CLEAR");
  JButton jButtonDatum = new JButton("DATUM");
  JButton jButtonInit = new JButton("I N I T");
  JButton jButtonPark = new JButton("PARK");
  JButton jButtonObserve = new JButton("OBSERVE");
  JButton jButtonStop = new JButton("STOP");
  JButton jButtonAbort = new JButton("ABORT");
  JButton jButtonTest = new JButton("TEST");
  JButton jButtonApply = new JButton("APPLY");

  GridLayout gridLayoutButtons = new GridLayout();
  GridLayout gridLayoutCfgCmds = new GridLayout();
  JPanel jPanelCfg = new JPanel();
  JPanel jPanelTree = new JPanel();
  JPanel jPanelCfgCmds = new JPanel();
  BorderLayout borderLayoutCfg = new BorderLayout();
  BorderLayout borderLayoutTree = new BorderLayout();

    JPanel jPanelArchiveData = new JPanel();
    EPICSLabel jLabelArchiveHost = new EPICSLabel(EPICS.prefix + "sad:localFileDir","Archive File on Host =");
    EPICSTextField jTextFieldArchiveFileName = new EPICSTextField(EPICS.prefix + "sad:localFileName",
								  "Archive Filename");
    // Master panel needs access to functions in
    //  detector panel class, passed from jeiFrame:
    JPanelDetector jPanelDetector;
    EPICSLabel mainHeartBeat;

//-------------------------------------------------------------------------------
  /**
   *Construct the frame
   */
  public JPanelMaster( JPanelDetector DetectorPanel, EPICSLabel heartbeat ) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jPanelDetector = DetectorPanel;
      mainHeartBeat = heartbeat;
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of JPanelMaster

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {

    jObsProgressBar = new JObsProgressBar( 1000, jTextFieldFrameCount,
					   jPanelDetector, jLabelTotalFrames, jLabelTotalNodSets );

    String [] combobox_items_1 = {" ", "chop-nod", "chop", "nod", "stare"};
    jComboBoxObservingMode = new EPICSComboBox(EPICS.prefix + "observationSetup.A",
					       EPICS.prefix + "observationSetup.VALA",
					       combobox_items_1, EPICSComboBox.ITEM);
    jTextFieldObservingMode = new EPICSTextField(EPICS.prefix + "observationSetup.VALA","observing mode");
    jTextFieldObservingMode.setEditable(false);

    String[] dataModes = {"","save","discard","discard-all","no-dhs"};
    jComboBoxDataMode = new EPICSComboBox(EPICS.prefix + "dataMode.A",
					  EPICS.prefix + "dataMode.VALA",
					  dataModes, EPICSComboBox.ITEM);
    jTextFieldDataMode = new EPICSTextField(EPICS.prefix + "dataMode.VALA","data mode");
    jTextFieldDataMode.setEditable(false);
	
    jComboBoxDataMode.addItemListener( new java.awt.event.ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if( !e.getItem().equals("save") && e.getStateChange() == ItemEvent.SELECTED ) {
		    if( JOptionPane.showConfirmDialog(null,"Are you sure you want discard data ?")
			!= JOptionPane.YES_OPTION )
			jComboBoxDataMode.set_and_putcmd("save");
		}
	    }
	});

    jPanelObsSetup.add(jLabelObsSetup, null);
    jLabelObsSetup.setForeground(Color.black);
    jPanelObsSetup.add(jButtonPutObsSetup, null);
    jPanelObsSetup.add(new JLabel("Current Values:"));
    jPanelObsSetup.add(jLabelObservingMode, null);
    jPanelObsSetup.add(jComboBoxObservingMode, null);
    jPanelObsSetup.add(jTextFieldObservingMode);
    jPanelObsSetup.add(jLabelDataMode, null);
    jPanelObsSetup.add(jComboBoxDataMode, null);
    jPanelObsSetup.add(jTextFieldDataMode);

    jLabelSkyBackgrnd.setText("Sky Background");
    String [] combo_items_bkgr = {" ", "20 % (Low)", "50 % (Moderate)", "70 % (High)", "90 % (Unusable)"};
    jComboBoxSkyBackgrnd = new EPICSComboBox( EPICS.prefix + "observationSetup.E",
					      EPICS.prefix + "observationSetup.VALE",
					      combo_items_bkgr, EPICSComboBox.ITEM);
    jTextFieldSkyBackgrnd = new EPICSTextField(EPICS.prefix + "dc:MetaEnvG.VALB","background");
    jTextFieldSkyBackgrnd.setEditable(false);

    jLabelSkyNoise.setText("Sky Noise");
    String [] combo_items_noise = {" ", "20 % (Low)", "50 % (Moderate)", "70 % (High)", "90 % (Unusable)"};
    jComboBoxSkyNoise = new EPICSComboBox( EPICS.prefix + "observationSetup.D",
					   EPICS.prefix + "observationSetup.VALD",
					   combo_items_noise, EPICSComboBox.ITEM);
    jTextFieldSkyNoise = new EPICSTextField(EPICS.prefix + "dc:MetaEnvG.VALD","sky noise");
    jTextFieldSkyNoise.setEditable(false);

    String[] airMasses = {" ","1.0","1.5","2.0","2.5","3.0"};
    jComboBoxAirMass = new EPICSComboBox(EPICS.prefix + "observationSetup.F",
					 EPICS.prefix + "observationSetup.VALF",
					 airMasses, EPICSComboBox.ITEM);
    jTextFieldAirMass = new EPICSTextField(EPICS.prefix + "dc:MetaEnvG.VALC", "AirMass");
    jTextFieldAirMass.setEditable(false);

    jPanelObsSetup.add(jLabelSkyNoise, null);
    jPanelObsSetup.add(jComboBoxSkyNoise, null);
    jPanelObsSetup.add(jTextFieldSkyNoise);
    jPanelObsSetup.add(jLabelSkyBackgrnd, null);
    jPanelObsSetup.add(jComboBoxSkyBackgrnd, null);
    jPanelObsSetup.add(jTextFieldSkyBackgrnd);
    jPanelObsSetup.add(new JLabel("Air Mass"));
    jPanelObsSetup.add(jComboBoxAirMass);
    jPanelObsSetup.add(jTextFieldAirMass);

    jButtonPutObsSetup.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent e) {
		jComboBoxObservingMode.forcePut();
		jComboBoxDataMode.forcePut();
		jComboBoxSkyBackgrnd.forcePut();
		jComboBoxSkyNoise.forcePut();
		jComboBoxAirMass.forcePut();
	    }
	});

    String [] combobox_items_CameraMode = {" ", "imaging", "spectroscopy"};
    jComboBoxCameraMode = new EPICSComboBox(EPICS.prefix + "instrumentSetup.A",
					    EPICS.prefix + "instrumentSetup.VALA",
					    combobox_items_CameraMode, EPICSComboBox.ITEM);
    jTextFieldCameraMode = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALA","camera mode");
    jTextFieldCameraMode.setEditable(false);

    String [] combobox_items_ImagingMode = {" ", "field", "window", "pupil"};
    jComboBoxImagingMode = new EPICSComboBox( EPICS.prefix + "instrumentSetup.B",
					      EPICS.prefix + "instrumentSetup.VALB",
					      combobox_items_ImagingMode, EPICSComboBox.ITEM);
    jTextFieldImagingMode = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALB","imaging mode");
    jTextFieldImagingMode.setEditable (false);

    // Filter
    LinkedList lnklst = new LinkedList();
    lnklst.add(" ");
    TextFile f = new TextFile(jei.data_path + "FilterLookup.txt", TextFile.IN);
    String line, filter_name;
    StringTokenizer st;
    if (!f.ok()) {
      System.out.println("error loading FilterLookup.txt");
    }
    else {
      line = f.readLine();
      if (!f.ok()) jeiError.show("error reading FilterLookup.txt");
      while (line != null && f.ok()) {
        if (line.length() > 0) if (line.charAt(0) != '#') {
          st = new StringTokenizer(line, "\t");
          if (st.hasMoreTokens()) filter_name = st.nextToken();
          else filter_name = "";
          if (filter_name.length() > 0) lnklst.add(filter_name);
        }
        line = f.readLine();
        if (!f.ok()) jeiError.show("error reading FilterLookup.txt");
      }
      f.close();
    }
    lnklst.add("OFFLINE");
    int i;
    String [] combobox_items = new String[lnklst.size()];
    for( i = 0; i < lnklst.size(); i++) combobox_items[i] = (String)lnklst.get(i);
    jComboBoxFilter = new EPICSComboBox( EPICS.prefix + "instrumentSetup.D",
					 EPICS.prefix + "instrumentSetup.VALE",
					 combobox_items, EPICSComboBox.ITEM);
    jTextFieldFilter = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALE","filter");
    jTextFieldFilter.setEditable(false);

    String filterOverride = EPICS.get( EPICS.prefix + "instrumentSetup.M" );
    jButtonFilter.setText("Filter (ov=" + filterOverride.substring(0,1) + ")");
    jButtonFilter.setForeground(new JLabel().getForeground());

    jButtonFilter.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent e) {
		String filtOver = EPICS.get( EPICS.prefix + "instrumentSetup.M" );
		if( filtOver.trim().equals("FALSE") )
		    putEpics( EPICS.prefix + "instrumentSetup.M", "TRUE" );
		else
		    putEpics( EPICS.prefix + "instrumentSetup.M", "FALSE" );
		filtOver = EPICS.get( EPICS.prefix + "instrumentSetup.M" );
		jButtonFilter.setText("Filter (ov=" + filtOver.substring(0,1) + ")");
	    }
	});

    // SlitWidth
    combobox_items = new String[jeiMotorParameters.getNumLocations(8)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(8); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(8,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxSlitWidth = new EPICSComboBox( EPICS.prefix + "instrumentSetup.J",
					    EPICS.prefix + "instrumentSetup.VALO",
					    combobox_items, EPICSComboBox.ITEM);
    jTextFieldSlitWidth = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALO","slit width");
    jTextFieldSlitWidth.setEditable (false);

    // Grating
    combobox_items = new String[jeiMotorParameters.getNumLocations(9)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(9); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(9,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxGrating = new EPICSComboBox( EPICS.prefix + "instrumentSetup.E",
					  EPICS.prefix + "instrumentSetup.VALK",
					  combobox_items, EPICSComboBox.ITEM);
    jTextFieldGrating = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALK","grating");
    jTextFieldGrating.setEditable (false);

    jButtonPutInstrSetup.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent e) {
		jComboBoxCameraMode.forcePut();
		jComboBoxImagingMode.forcePut();
		jComboBoxFilter.forcePut();
		jComboBoxSlitWidth.forcePut();
		jComboBoxGrating.forcePut();
		jTextFieldCentralWavelength.forcePut();
	    }
	});
    jPanelInstrSetup.add(jLabelInstrSetup, null);
    jLabelInstrSetup.setForeground(Color.black);
    jPanelInstrSetup.add(jButtonPutInstrSetup, null);
    jPanelInstrSetup.add(new JLabel("Current Values:"));
    jPanelInstrSetup.add(jLabelCameraMode, null);
    jPanelInstrSetup.add(jComboBoxCameraMode, null);
    jPanelInstrSetup.add(jTextFieldCameraMode);
    jPanelInstrSetup.add(jLabelImagingMode, null);
    jPanelInstrSetup.add(jComboBoxImagingMode, null);
    jPanelInstrSetup.add(jTextFieldImagingMode);
    jPanelInstrSetup.add(jButtonFilter, null);
    jPanelInstrSetup.add(jComboBoxFilter, null);
    jPanelInstrSetup.add(jTextFieldFilter);
    jPanelInstrSetup.add(jLabelSlitWidth, null);
    jPanelInstrSetup.add(jComboBoxSlitWidth, null);
    jPanelInstrSetup.add(jTextFieldSlitWidth);
    jPanelInstrSetup.add(jLabelGrating, null);
    jPanelInstrSetup.add(jComboBoxGrating, null);
    jPanelInstrSetup.add(jTextFieldGrating);
    jPanelInstrSetup.add(jLabelCentralWavelength, null);
    jPanelInstrSetup.add(jTextFieldCentralWavelength, null);
    jPanelInstrSetup.add(jTextFieldCenWaveCurrVal, null);
    jTextFieldCenWaveCurrVal.setEditable(false);

    // Sector
    combobox_items = new String[jeiMotorParameters.getNumLocations(1)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(1); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(1,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxSector = new EPICSComboBox( EPICS.prefix + "instrumentSetup.I",
					 EPICS.prefix + "instrumentSetup.VALN",
					 combobox_items, EPICSComboBox.ITEM);
    jTextFieldSector = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALN","sector");
    jTextFieldSector.setEditable (false);

    //Aperture
    combobox_items = new String[jeiMotorParameters.getNumLocations(3)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(3); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(3,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxAperture = new EPICSComboBox( EPICS.prefix + "instrumentSetup.C",
					   EPICS.prefix + "instrumentSetup.VALG",
					   combobox_items, EPICSComboBox.ITEM);
    jTextFieldAperture = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALG","aperture");
    jTextFieldAperture.setEditable(false);

    // Pupil
    combobox_items = new String[jeiMotorParameters.getNumLocations(7)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(7); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(7,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxPupil = new EPICSComboBox( EPICS.prefix + "instrumentSetup.G",
				       EPICS.prefix + "instrumentSetup.VALM",
				       combobox_items, EPICSComboBox.ITEM);
    jTextFieldPupil = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALM","pupil");
    jTextFieldPupil.setEditable (false);

    // LyotStop
    int listsize = jeiMotorParameters.getNumLocations(5)+2;
    combobox_items = new String[listsize];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(5); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(5,i);
      if (combobox_items[i].indexOf("Blank-Off") != -1) listsize--;
    }
    combobox_items[i] = "OFFLINE";
    String [] temp = combobox_items;
    combobox_items = new String[listsize];
    int k=0;
    for (int j=0; j<temp.length; j++) {
    	if (temp[j].indexOf("Blank-Off") == -1) combobox_items[k++] = temp[j];
    }
    jComboBoxLyotStop = new EPICSComboBox( EPICS.prefix + "instrumentSetup.H",
					   EPICS.prefix + "instrumentSetup.VALL",
					   combobox_items, EPICSComboBox.ITEM);
    jTextFieldLyotStop = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALL","lyot stop");
    jTextFieldLyotStop.setEditable (false);

    // Window
    combobox_items = new String[jeiMotorParameters.getNumLocations(2)+2];
    combobox_items[0] = " ";
    for( i = 1; i <= jeiMotorParameters.getNumLocations(2); i++) {
      combobox_items[i] = jeiMotorParameters.getNamedLocations(2,i);
    }
    combobox_items[i] = "OFFLINE";
    jComboBoxWindow = new EPICSComboBox( EPICS.prefix + "instrumentSetup.K",
					 EPICS.prefix + "instrumentSetup.VALP",
					 combobox_items, EPICSComboBox.ITEM);
    jTextFieldWindow = new EPICSTextField(EPICS.prefix + "instrumentSetup.VALP","window");
    jTextFieldWindow.setEditable (false);

    jButtonPutOverrides.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent e) {
		jComboBoxSector.forcePut();
		jComboBoxAperture.forcePut();
		jComboBoxPupil.forcePut();
		jComboBoxLyotStop.forcePut();
		jComboBoxWindow.forcePut();
	    }
	});

    // Check Boxes for Overrides:
    jComboBoxSector.setEnabled(false);
    jComboBoxAperture.setEnabled(false);
    jComboBoxPupil.setEnabled(false);
    jComboBoxLyotStop.setEnabled(false);
    jComboBoxWindow.setEnabled(false);

    jCheckBoxSector.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jCheckBoxSector_action(e); } });

    jCheckBoxAperture.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jCheckBoxAperture_action(e); } });

    jCheckBoxPupil.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jCheckBoxPupil_action(e); } });

    jCheckBoxLyotStop.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jCheckBoxLyotStop_action(e); } });

    jCheckBoxWindow.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jCheckBoxWindow_action(e); } });

    jPanelOverrides.add(jLabelOverrides, null);
    jPanelOverrides.add(jButtonPutOverrides, null);
    jPanelOverrides.add(new JLabel("Current Values:"));
    jPanelOverrides.add(jCheckBoxSector, null);
    jPanelOverrides.add(jComboBoxSector, null);
    jPanelOverrides.add(jTextFieldSector);
    jPanelOverrides.add(jCheckBoxAperture, null);
    jPanelOverrides.add(jComboBoxAperture, null);
    jPanelOverrides.add(jTextFieldAperture);
    jPanelOverrides.add(jCheckBoxPupil, null);
    jPanelOverrides.add(jComboBoxPupil, null);
    jPanelOverrides.add(jTextFieldPupil);
    jPanelOverrides.add(jCheckBoxLyotStop, null);
    jPanelOverrides.add(jComboBoxLyotStop, null);
    jPanelOverrides.add(jTextFieldLyotStop);
    jPanelOverrides.add(jCheckBoxWindow, null);
    jPanelOverrides.add(jComboBoxWindow, null);
    jPanelOverrides.add(jTextFieldWindow);

    //the Big Buttons:
    jButtonApply.setBackground(Color.cyan);
    jButtonApply.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonApply_action(e); } });

    jButtonDatum.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonDatum_action(e); } });

    jButtonCancel.addActionListener(new ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonCancel_action(e); } });

    jButtonObserve.setBackground(Color.green);
    jButtonObserve.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonObserve_action(e); } });

    jButtonStop.setBackground(Color.red);
    jButtonStop.setForeground(Color.white);
    jButtonStop.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonStop_action(e); }	});

    jButtonInit.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonInit_action(e); }	});

    jButtonPark.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonPark_action(e); }	});

    jButtonAbort.setBackground(Color.yellow);
    jButtonAbort.setForeground(Color.black);
    jButtonAbort.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonAbort_action(e); } });

    jButtonTest.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonTest_action(e); }	});

    jPanelButtons.add(jButtonInit, null);
    jPanelButtons.add(jButtonDatum, null);
    jPanelButtons.add(jButtonCancel, null);
    jPanelButtons.add(jButtonPark, null);
    jPanelButtons.add(jButtonTest, null);
    jPanelButtons.add(jButtonAbort, null);
    jPanelButtons.add(jButtonObserve, null);
    jPanelButtons.add(jButtonStop, null);

    //button to toggle on/off the Temperature Control option of Instrument Sequencer:

    jButtonTempControl.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonTempControl_action(); } });

    jButtonTCindicator.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonTempControl_action(); } });

    //set the current status:
    String TempControl = EPICS.get( EPICS.prefix + "observationSetup.O" );
    if( TempControl.trim().toUpperCase().equals("TRUE") ) {
	jButtonTCindicator.setBackground(Color.green);
	jButtonTCindicator.setForeground(Color.black);
	jButtonTCindicator.setText("is  Enabled");
    } else {
	jButtonTCindicator.setBackground(Color.red);
	jButtonTCindicator.setForeground(Color.white);
	jButtonTCindicator.setText("is  Disabled");
    }

    //buttons which pop-up new windows for showing optical path and environment params:

    jButtonOpticalPath.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonOpticalPath_action(e); } });

    jButton_MoreObsParms.addActionListener(new ActionListener()
	{ public void actionPerformed(ActionEvent e) { jMoreObsParms_action(e); } });

    //Saved Configurations Buttons:

    jButtonDoCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonDoCfg_action(e); } });

    jButtonBlankAll.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonBlankAll_action(e); } });

    jButtonLoadCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonLoadCfg_action(e); } });

    jButtonEditCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonEditCfg_action(e); } });

    jButtonAddCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonAddCfg_action(e); } });

    jButtonDelCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonDelCfg_action(e); } });

    jButtonMoveCfgUp.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonMoveCfgUp_action(e); } });

    jButtonMoveCfgDown.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonMoveCfgDown_action(e); } });

    jButtonSaveCfg.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { jButtonSaveCfg_action(e); } });

    jPanelCfgCmds.add(jButtonLoadCfg, null);
    jPanelCfgCmds.add(jButtonDoCfg, null);
    jPanelCfgCmds.add(jButtonEditCfg, null);
    jPanelCfgCmds.add(jButtonBlankAll, null);
    jPanelCfgCmds.add(jButtonAddCfg, null);
    jPanelCfgCmds.add(jButtonMoveCfgUp, null);
    jPanelCfgCmds.add(jButtonMoveCfgDown, null);
    jPanelCfgCmds.add(jButtonDelCfg, null);
    jPanelCfgCmds.add(jButtonSaveCfg, null);

    jPanelObsSetup.setBorder(BorderFactory.createEtchedBorder());
    gridLayoutObsSetup.setColumns(3);
    gridLayoutObsSetup.setRows(0);
    jPanelObsSetup.setLayout(gridLayoutObsSetup);
    this.setLayout(new RatioLayout());

    jPanelInstrSetup.setBorder(BorderFactory.createEtchedBorder());
    jPanelInstrSetup.setLayout(gridLayoutInstrSetup);
    jLabelConfiguration.setFont(new java.awt.Font("Dialog", 1, 12));
    gridLayoutInstrSetup.setColumns(3);
    gridLayoutInstrSetup.setRows(0);
    jPanelOverrides.setLayout(gridLayoutOverrides);
    gridLayoutOverrides.setColumns(3);
    gridLayoutOverrides.setRows(0);
    jPanelOverrides.setBorder(BorderFactory.createEtchedBorder());

    jLabelObsSetup.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabelInstrSetup.setFont(new java.awt.Font("Dialog", 1, 12));
    jLabelOverrides.setFont(new java.awt.Font("Dialog", 1, 12));

    jPanelButtons.setLayout(gridLayoutButtons);
    gridLayoutButtons.setColumns(2);
    gridLayoutButtons.setHgap(1);
    gridLayoutButtons.setRows(0);
    ///gridLayoutButtons.setVgap(4);
    jPanelButtons.setBorder(BorderFactory.createEtchedBorder());

    jPanelCfg.setLayout(borderLayoutCfg);
    jPanelCfgCmds.setLayout(gridLayoutCfgCmds);
    gridLayoutCfgCmds.setColumns(1);
    gridLayoutCfgCmds.setRows(0);
    ///gridLayoutCfgCmds.setVgap(4);
    jPanelTree.setLayout(borderLayoutTree);
    jPanelCfg.setBorder(BorderFactory.createEtchedBorder());

    jPanelObsSetup.setBounds(5,6,232,130);
    jPanelInstrSetup.setBounds(4,134,232,180);
    jPanelOverrides.setBounds(4,320,232,120);
    jPanelCfg.setBounds(239, 74, 471, 244);
    jPanelCfg.add(jPanelCfgCmds, BorderLayout.EAST);
    jPanelCfg.add(jPanelTree, BorderLayout.CENTER);
    jPanelTree.add(jLabelConfiguration, BorderLayout.NORTH);
    jPanelTree.add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jTree1, null);

    jTextFieldObsStatus.setEditable(false);
    jTextFieldNodCycle.setEditable(false);
    jTextFieldNodBeam.setEditable(false);
    jTextFieldFrameCount.setText("0");
    jTextFieldFrameCount.setEditable(false);
    jTextFieldBgWellpc.setEditable(false);
    jTextFieldBgSigma.setEditable(false);

    jPanelStatus.setLayout(new RatioLayout());
    jPanelStatus.setBorder(BorderFactory.createEtchedBorder());
    jPanelStatus.add("0.01,0.00;0.21,0.20", jLabelObsStatus);
    jPanelStatus.add("0.22,0.00;0.77,0.20", jTextFieldObsStatus);
    jPanelStatus.add("0.01,0.20;0.18,0.20", jLabelNodCycle);
    jPanelStatus.add("0.19,0.20;0.07,0.20", jTextFieldNodCycle);
    jPanelStatus.add("0.26,0.20;0.11,0.20", jLabelTotalNodSets);
    jPanelStatus.add("0.38,0.20;0.10,0.20", jLabelNodBeam);
    jPanelStatus.add("0.48,0.20;0.07,0.20", jTextFieldNodBeam);
    jPanelStatus.add("0.57,0.20;0.43,0.20", monitorObserveCAR);
    jPanelStatus.add("0.01,0.40;0.18,0.20", jLabelProgress);
    jPanelStatus.add("0.19,0.40;0.20,0.20", jTextFieldFrameCount);
    jPanelStatus.add("0.41,0.40;0.59,0.20", jLabelTotalFrames);
    jPanelStatus.add("0.01,0.60;0.98,0.20", jObsProgressBar);
    jPanelStatus.add("0.01,0.80;0.32,0.20", jLabelBgWellpc);
    jPanelStatus.add("0.33,0.80;0.20,0.20", jTextFieldBgWellpc);
    jPanelStatus.add("0.54,0.80;0.26,0.20", jLabelBgSigma);
    jPanelStatus.add("0.80,0.80;0.20,0.20", jTextFieldBgSigma);

    jButtonApply.setFont(new java.awt.Font("Dialog", 1, 12));

    jPanelArchiveData.setBorder(BorderFactory.createEtchedBorder());
    jPanelArchiveData.setLayout(new GridLayout(0,1,3,3));
    jPanelArchiveData.add(jLabelArchiveHost);
    jPanelArchiveData.add(jTextFieldArchiveFileName);
    jTextFieldArchiveFileName.setEditable(false);
    JPanel jPanelArchDataLabel = new JPanel();
    jPanelArchDataLabel.setLayout( new RatioLayout() );
    jPanelArchDataLabel.add("0.0,0.0;0.3,1.0", new JLabel("DHS Label:"));
    jPanelArchDataLabel.add("0.3,0.0;0.7,1.0", jTextFieldObsDataLabel);
    jPanelArchiveData.add(jPanelArchDataLabel);
    JPanel jPanelArchObsNote = new JPanel();
    jPanelArchObsNote.setLayout( new RatioLayout() );
    jPanelArchObsNote.add("0.0,0.0;0.3,1.0", new JLabel("User Note:"));
    jPanelArchObsNote.add("0.3,0.0;0.7,1.0", jTextFieldObsNote);
    jPanelArchiveData.add(jPanelArchObsNote);

    this.add("0.01,0.01;0.35,0.36", jPanelInstrSetup);
    this.add("0.01,0.38;0.35,0.27", jPanelOverrides);
    this.add("0.01,0.66;0.35,0.29", jPanelObsSetup);
    this.add("0.01,0.95;0.35,0.05", jButton_MoreObsParms);
    this.add("0.37,0.01;0.40,0.39", jPanelCfg);
//  this.add("0.37,0.41;0.40,0.03", monitorState);
    this.add("0.37,0.40;0.12,0.04", monitorCompConCAR);
    this.add("0.49,0.40;0.30,0.04", monitorCompConIMSS);
    this.add("0.37,0.44;0.12,0.04", monitorEnvConCAR);
    this.add("0.49,0.44;0.30,0.04", monitorEnvConOMSS);
    this.add("0.37,0.48;0.12,0.04", monitorObsConCAR);
    this.add("0.49,0.48;0.30,0.04", monitorObsConOMSS);
    this.add("0.37,0.52;0.12,0.04", monitorAcqConCAR);
    this.add("0.49,0.52;0.30,0.04", monitorAcqConOMSS);
    this.add("0.37,0.56;0.38,0.25", jPanelStatus);
    this.add("0.80,0.85;0.10,0.11", jButtonApply);
    this.add("0.91,0.86;0.10,0.05", monitorApplyVAL);
    this.add("0.91,0.90;0.10,0.05", monitorApplyCAR);
    this.add("0.64,0.81;0.36,0.05", monitorApplyMESS);
    this.add("0.64,0.96;0.36,0.04", monitorApplyOMSS);
    this.add("0.64,0.85;0.15,0.05", jButtonTempControl);
    this.add("0.64,0.91;0.15,0.05", jButtonTCindicator);
    this.add("0.37,0.84;0.16,0.05", jLabelOnSrcTime);
    this.add("0.53,0.84;0.05,0.05", jTextFieldOnSrcTime);
    this.add("0.37,0.89;0.17,0.05", monitorOnSrcTime);
    this.add("0.54,0.89;0.10,0.05", monitorObsTime);
    this.add("0.37,0.94;0.16,0.05", jLabelChopThrow);
    this.add("0.53,0.94;0.05,0.05", jTextFieldChopThrow);
    this.add("0.78,0.01;0.22,0.46", jPanelButtons);
    this.add("0.78,0.48;0.22,0.07", jButtonOpticalPath);
    this.add("0.75,0.56;0.25,0.25", jPanelArchiveData);

    jObsProgressBar.start();
    load_cfg_tree();
    jButtonEditCfg_action(null);
  } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#cancel button action performed
   *@param e ActionEvent: not used
   */
  void jButtonCancel_action(ActionEvent e) {
    String cmd = "PUT " + EPICS.prefix + "apply.DIR CLEAR";
    jeiCommand.execute(cmd);
    if( EPICS.get( EPICS.prefix + "sad:observationStatus" ).indexOf("MARK") >= 0 ) {
	cmd = "PUT " + EPICS.prefix + "sad:observationStatus  CLEARed";
	jeiCommand.execute(cmd);
    }
  }

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#save configuration button action performed
   *@param e ActionEvent: not used
   */
  void jButtonSaveCfg_action(ActionEvent e) {
    JFileChooser chooser = new JFileChooser(jei.data_path);
    ExtensionFilter cfg_type = new ExtensionFilter (
          "T-ReCS cfg files", new String[] {".cfg"});
    chooser.setFileFilter (cfg_type); // Initial filter setting
    chooser.setSelectedFile(new File(cfg_file_name));
    chooser.setDialogTitle("Save configuration tree to");
    chooser.setApproveButtonText("Save");
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.CANCEL_OPTION ) return;
    if(returnVal == JFileChooser.APPROVE_OPTION) {
      cfg_file_name = chooser.getSelectedFile().getName();
      if (!cfg_file_name.endsWith(".cfg")) cfg_file_name = cfg_file_name + ".cfg";
    }
    ///take a look at this
    TextFile out_file = new TextFile(jei.data_path + cfg_file_name, TextFile.OUT) ;
    Object root = treeModel.getRoot();
    Object cfg;
    Object child;
    int n = treeModel.getChildCount(root);
    if (!out_file.ok()) {
      jeiError.show("error in opening the file");
    }
    else {
      for (int i = 0; i < n; i++) {
        cfg = treeModel.getChild(root, i);
        if (!treeModel.isLeaf(cfg)) {
          //System.out.println(cfg);
          if (!out_file.ok()) {
            jeiError.show("error in writing to the file");
          }
          else {
            out_file.println(cfg.toString());
          }
          int m = treeModel.getChildCount(cfg);
          for (int j = 0; j < m; j++) {
            child = treeModel.getChild(cfg, j);
            if (treeModel.isLeaf(child)) {
              //System.out.println("." + child);
              if (!out_file.ok()) {
                jeiError.show("error in writing to the file");
              }
              else {
                out_file.println("." + child);
              }
            }
          }
        }
      }
      out_file.close();
    }
  } //end of jButtonSaveCfg_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#load configuration button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonLoadCfg_action(ActionEvent e) {
    JFileChooser chooser = new JFileChooser(jei.data_path);
    ExtensionFilter cfg_type = new ExtensionFilter (
          "T-ReCS cfg files", new String[] {".cfg"});
    chooser.setFileFilter (cfg_type); // Initial filter setting
    chooser.setSelectedFile(new File(cfg_file_name));
    chooser.setDialogTitle("Load configuration tree from");
    chooser.setApproveButtonText("Load");
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.CANCEL_OPTION ) return;
    if(returnVal == JFileChooser.APPROVE_OPTION) {
      cfg_file_name = chooser.getSelectedFile().getName();
      if (!cfg_file_name.endsWith(".cfg")) cfg_file_name = cfg_file_name + ".cfg";
    }

    load_cfg_tree();
  } //end of jButtonLoadCfg_action


//-------------------------------------------------------------------------------
  /**
   * Loads the configuration tree
   */
  void load_cfg_tree() {
    DefaultMutableTreeNode root = new DefaultMutableTreeNode(cfg_file_name);
    String line;
    DefaultMutableTreeNode parent = null;
    TextFile in_file = new TextFile(jei.data_path + cfg_file_name, TextFile.IN) ;

    // read the file
    while ((line = in_file.readLine()) != null && in_file.ok()) {
      if (line.trim().equals("")) {
        // ignore it
      }
      else if (line.charAt(0) == '.') { // add child
        if (parent == null) {
          jeiError.show("reading from " + cfg_file_name + " - no parent for child");
          return;
        }
        else parent.add(new DefaultMutableTreeNode (line.substring(1)));
      }
      else { // add parent
        parent = new DefaultMutableTreeNode(line);
        root.add(parent);
      }
    }
    in_file.close();

    treeModel = new DefaultTreeModel(root);
    jTree1.setModel(treeModel);
  } //end of load_cfg_tree


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#delete configuration button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonDelCfg_action(ActionEvent e) {
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      jeiError.show("nothing selected to delete");
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
      if (JOptionPane.showConfirmDialog(this,
          "Are you sure you want DELETE the selected configutation, " + treeNode + " ?") == JOptionPane.YES_OPTION) {
        treeModel.removeNodeFromParent(treeNode);
      }
    }
  } //end of jButtonDelCfg_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#add configuration button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonAddCfg_action(ActionEvent e) {
    int i;
    DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      i = root.getChildCount()+1;
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
      i = root.getIndex(treeNode) + 2;
    }
    String cfg_name = JOptionPane.showInputDialog("Enter the name of the new configuration");  // a get string dialog
    if (cfg_name != null) {
      DefaultMutableTreeNode parent = new DefaultMutableTreeNode(cfg_name);
      String val;
      val = (String)jComboBoxCameraMode.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Camera mode : " + val));
      val = (String)jComboBoxObservingMode.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Observing mode : " + val));
      val = (String)jComboBoxImagingMode.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Imaging mode : " + val));
      val = (String)jComboBoxDataMode.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Data mode : " + val));
      val = (String)jComboBoxSkyBackgrnd.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Sky Background : " + val));
      val = (String)jComboBoxSkyNoise.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Sky Noise : " + val));
      val = (String)jComboBoxSector.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Sector : " + val));
      val = (String)jComboBoxFilter.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Filter : " + val));
      val = (String)jComboBoxLyotStop.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Lyot stop : " + val));
      val = (String)jComboBoxSlitWidth.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Slit width : " + val));
      val = (String)jComboBoxGrating.getSelectedItem();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Grating : " + val));
      val = (String)jTextFieldCentralWavelength.getText();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Central wavelength : " + val));

      if (jCheckBoxSector.isSelected()) {
        val = (String)jComboBoxSector.getSelectedItem();
        if (has_data(val)) parent.add(new DefaultMutableTreeNode("Sector : " + val));
      }
      if (jCheckBoxAperture.isSelected()) {
        val = (String)jComboBoxAperture.getSelectedItem();
        if (has_data(val)) parent.add(new DefaultMutableTreeNode("Aperture : " + val));
      }
      if (jCheckBoxPupil.isSelected()) {
        val = (String)jComboBoxPupil.getSelectedItem();
        if (has_data(val)) parent.add(new DefaultMutableTreeNode("Pupil : " + val));
      }
      if (jCheckBoxWindow.isSelected()) {
        val = (String)jComboBoxWindow.getSelectedItem();
        if (has_data(val)) parent.add(new DefaultMutableTreeNode("Window : " + val));
      }
      val = (String)jTextFieldOnSrcTime.getText();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("On source time : " + val));
      val = (String)jTextFieldObsDataLabel.getText();
      if (has_data(val)) parent.add(new DefaultMutableTreeNode("Observation data label : " + val));
      treeModel.insertNodeInto(parent, root, i-1);
      jTree1.setSelectionPath(path);
    }
  } //end of jButtonAddCfg_action


//-------------------------------------------------------------------------------
  /**
   *TBD
   *@param s String: TBD
   */
  boolean has_data(String s) {
    if (s == null) return false;
    if (s.trim().equals("")) return false;
    return true;
  } //end of has_data


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#move configuration up button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveCfgUp_action(ActionEvent e) {
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      jeiError.show("nothing selected to move");
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
      DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
      int i = root.getIndex(treeNode);
      if (i > 0) {
        treeModel.removeNodeFromParent(treeNode);
        treeModel.insertNodeInto(treeNode, root, i-1);
        jTree1.setSelectionPath(path);
      }
    }
  } //end of jButtonMoveCfgUp_action


//-------------------------------------------------------------------------------
  /**
   * JPanelMaster#move configuration down button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveCfgDown_action(ActionEvent e) {
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      jeiError.show("nothing selected to move");
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
      DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
      int i = root.getIndex(treeNode);
      if (i < root.getChildCount()-1) {
        treeModel.removeNodeFromParent(treeNode);
        treeModel.insertNodeInto(treeNode, root, i+1);
        jTree1.setSelectionPath(path);
      }
    }
  } //end of jButtonMoveCfgDown_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#do configuration button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonDoCfg_action(ActionEvent e) {
    Object cfg;
    Object child;
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      jeiError.show("nothing selected to do");
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
      jeiCommand.execute(";cfg " + treeNode.toString());
      cfg = treeNode;
      if (!treeModel.isLeaf(cfg)) {
        int m = treeModel.getChildCount(cfg);
        for (int j = 0; j < m; j++) {
          child = treeModel.getChild(cfg, j);
          if (treeModel.isLeaf(child)) {
            jeiCommand.execute("." + child.toString());
          }
        }
      }
      jeiCommand.execute(";end " + treeNode.toString());
    }
  } //end of jButtonDoCfg_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#edit configuration button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonEditCfg_action(ActionEvent e) {
    if (ok_to_edit) {
      ok_to_edit = false;
      jButtonEditCfg.setText("Enable Edits");
      jButtonBlankAll.setEnabled(false);
      jButtonAddCfg.setEnabled(false);
      jButtonDelCfg.setEnabled(false);
      jButtonMoveCfgUp.setEnabled(false);
      jButtonMoveCfgDown.setEnabled(false);
      jButtonSaveCfg.setEnabled(false);
    }
    else {
	//      String pw = JOptionPane.showInputDialog("Enter the edit authorization password");
	//      if (pw != null) {
	//        if (pw.equals("gators")) {
	ok_to_edit = true;
	jButtonEditCfg.setText("Disable Edits");
	jButtonBlankAll.setEnabled(true);
	jButtonAddCfg.setEnabled(true);
	jButtonDelCfg.setEnabled(true);
	jButtonMoveCfgUp.setEnabled(true);
	jButtonMoveCfgDown.setEnabled(true);
	jButtonSaveCfg.setEnabled(true);
	//        }
	//      }
    }
  } //end of jButtonEditCfg_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#init button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonInit_action(ActionEvent e) {
    String cmd = "PUT " + EPICS.prefix + "init.A NONE";
    jeiCommand.execute(cmd);
    initMarked = true;
  } //end of jButtonInit_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#apply button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonApply_action(ActionEvent e) {
      if( initMarked ) {
	  monitorApplyCAR.setText("CAR: BUSY");
	  mainHeartBeat.setText("trecs: EPICS is now connecting to Agents...");
	  initMarked = false;
      }
    String cmd = "PUT " + EPICS.prefix + "apply.DIR START";
    jeiCommand.execute(cmd);
  } //end of jButtonApply_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#observe button action performed
   *@param e ActionEvent: TBD
   */
    void jButtonObserve_action(ActionEvent e)
    {
	if( checkDataMode() )
	    {
		jObsProgressBar.stop();
		jTextFieldFrameCount.setText("0");
		jObsProgressBar.start();

		// make sure FASTMODE is not set in comment record:
		String comment = EPICS.get( EPICS.prefix + "dc:acqControlG.VALQ" );
		int fmindex = comment.indexOf("FAST");

		if( fmindex >= 0 ) {
		    int mdindex = comment.indexOf("MODE");
		    if( fmindex > 0 )
			comment = comment.substring(0,fmindex) + comment.substring(mdindex+4);
		    else
			comment = comment.substring(mdindex+4);
		    if( comment.trim().equals("") ) comment = ".";
		    putEpics( EPICS.prefix + "dc:acqControl.J", comment );
		}

		putEpics( EPICS.prefix + "observe.DIR", "MARK" );
		putEpics( EPICS.prefix + "sad:observationStatus", "MARKed CAD for new obs..." );
	    }
    } //end of jButtonObserve_action

//-------------------------------------------------------------------------------
    int resetObsStatusCounters()
    {
	int totalFrames = jPanelDetector.TotalFrames();
	jLabelTotalFrames.setText("out of  " + totalFrames + " save Frames");
	jLabelTotalNodSets.setText("of "+jPanelDetector.CTcTextGroup[jPanelDetector.ctiNodSets].getText()+".");
	jPanelDetector.jTextFieldTotalFrames.setText( totalFrames + " " );
	jPanelDetector.totFramesLabel.setText( "out of  " + totalFrames );
	return totalFrames;
    }
//-------------------------------------------------------------------------------
    /**
     *JPanelMaster#checkDataMode
     * Gets the current data mode from EPICS, and if data mode = save, returns true.
     * If data mode = discard then confirms with user to continue, otherwise returns false.
     * If data mode does not agree with engineering low-level db, then re-put data mode.
     */
    boolean checkDataMode()
    {
	String DataMode = EPICS.get( EPICS.prefix + "dataMode.VALA" ).trim().toUpperCase();

	if( ! DataMode.equals("SAVE") )
	    {
		if( JOptionPane.NO_OPTION ==
		    JOptionPane.showConfirmDialog(this,"Data will not be saved, continue ?","Warning",
						  JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) )
		    return false;
	    }

	String acqDataMode = EPICS.get( EPICS.prefix + "dc:acqControlG.VALE" ).trim().toUpperCase();
	if( ! DataMode.equals( acqDataMode ) )
	    jComboBoxDataMode.set_and_putcmd( DataMode.toLowerCase() );
	return true;
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val to the specified EPICS record field
   *@param putRec String: the EPICS record
   *@param value String: Value to be put
   */
    void putEpics( String putRec, String value ) {
	if( value.trim().equals("") || putRec.trim().equals("") ) return;
	String cmd = new String("PUT " + putRec + "  \""+value+ "\"") ;
	jeiCommand.execute(cmd);
    } //end of putEpics

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#TempControl button action performed
   */
  void jButtonTempControl_action() {
      String TempControl = EPICS.get( EPICS.prefix + "observationSetup.O" );
      if( TempControl.trim().toUpperCase().equals("FALSE") ) {
	  putEpics( EPICS.prefix + "observationSetup.O", "TRUE" );
	  jButtonTCindicator.setBackground(Color.green);
	  jButtonTCindicator.setForeground(Color.black);
	  jButtonTCindicator.setText("is  Enabled");
      } else {
	  putEpics( EPICS.prefix + "observationSetup.O", "FALSE" );
	  jButtonTCindicator.setBackground(Color.red);
	  jButtonTCindicator.setForeground(Color.white);
	  jButtonTCindicator.setText("is  Disabled");
      }
  } //end of jButtonPark_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#park button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonPark_action(ActionEvent e) {
    String cmd = "PUT " + EPICS.prefix + "park.DIR MARK";
    jeiCommand.execute(cmd);
  } //end of jButtonPark_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#datum button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonDatum_action(ActionEvent e) {
    String cmd = "PUT " + EPICS.prefix + "datum.DIR MARK";
    jeiCommand.execute(cmd);
  } //end of jButtonDatum_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#test button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonTest_action(ActionEvent e) {
    String cmd = "PUT " + EPICS.prefix + "test.DIR MARK";
    jeiCommand.execute(cmd);
  } //end of jButtonTest_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#stop button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonStop_action(ActionEvent e) {
    if (JOptionPane.showConfirmDialog(this,
        "Are you sure you want to STOP the observation?") == JOptionPane.YES_OPTION) {
      String cmd = "PUT " + EPICS.prefix + "stop.DIR MARK";
      jeiCommand.execute(cmd);
      cmd = "PUT " + EPICS.prefix + "apply.DIR START";
      jeiCommand.execute(cmd);
    }
  } //end of jButtonStop_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#abort button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonAbort_action(ActionEvent e) {
    if (JOptionPane.showConfirmDialog(this,
        "Are you sure you want to ABORT the observation?") == JOptionPane.YES_OPTION) {
      String cmd = "PUT " + EPICS.prefix + "abort.DIR MARK";
      jeiCommand.execute(cmd);
      cmd = "PUT " + EPICS.prefix + "apply.DIR START";
      jeiCommand.execute(cmd);
    }
  } //end of jButtonAbort_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#sector check box action performed
   *@param e ActionEvent: TBD
   */
  void jCheckBoxSector_action(ActionEvent e) {
    if (jCheckBoxSector.isSelected()) {
	jComboBoxSector.setEnabled(true);
    }
    else {
      jComboBoxSector.setEnabled(false);
    }
  } //end of jCheckBoxSector_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#aperture check box action performed
   *@param e ActionEvent: TBD
   */
  void jCheckBoxAperture_action(ActionEvent e) {
    if (jCheckBoxAperture.isSelected()) {
	putEpics( EPICS.prefix + "instrumentSetup.L", "TRUE" );
	jComboBoxAperture.setEnabled(true);
    } else {
	jComboBoxAperture.setEnabled(false);
	putEpics( EPICS.prefix + "instrumentSetup.L", "FALSE" );
    }
  } //end of jCheckBoxAperture_action


//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#pupil check box action performed
   *@param e ActionEvent: TBD
   */
  void jCheckBoxPupil_action(ActionEvent e) {
    if (jCheckBoxPupil.isSelected()) {
	putEpics( EPICS.prefix + "instrumentSetup.N", "TRUE" );
	jComboBoxPupil.setEnabled(true);
    } else {
	jComboBoxPupil.setEnabled(false);
	putEpics( EPICS.prefix + "instrumentSetup.N", "FALSE" );
    }
  } //end of jCheckBoxPupil_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#Lyot-stop check box action performed
   *@param e ActionEvent: TBD
   */
  void jCheckBoxLyotStop_action(ActionEvent e) {
    if (jCheckBoxLyotStop.isSelected()) {
	jComboBoxLyotStop.setEnabled(true);
    } else {
	jComboBoxLyotStop.setEnabled(false);
    }
  } //end of jCheckBoxLyotStop_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#window check box action performed
   *@param e ActionEvent: TBD
   */
  void jCheckBoxWindow_action(ActionEvent e) {
    if (jCheckBoxWindow.isSelected()) {
	putEpics( EPICS.prefix + "instrumentSetup.O", "TRUE" );
	jComboBoxWindow.setEnabled(true);
    } else {
	putEpics( EPICS.prefix + "instrumentSetup.O", "FALSE" );
	jComboBoxWindow.setEnabled(false);
    }
  } //end of jCheckBoxWindow_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#optical path button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonOpticalPath_action(ActionEvent e) {
    String cmd = "PUSH Optical Path";
    jeiCommand.execute(cmd);
    jObsProgressBar.start();
  } //end of jButtonOpticalPath_action

//-------------------------------------------------------------------------------
  /**
   *JPanelMaster#blank all button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonBlankAll_action(ActionEvent e) { // clear all cfg fields
    if (JOptionPane.showConfirmDialog(this,
        "Are you sure you want to CLEAR all inputs ?") == JOptionPane.YES_OPTION)
	{
	    jComboBoxCameraMode.setSelectedIndex(0);
	    jComboBoxObservingMode.setSelectedIndex(0);
	    jComboBoxImagingMode.setSelectedIndex(0);
	    jComboBoxDataMode.setSelectedIndex(0);
	    jComboBoxSkyBackgrnd.setSelectedIndex(0);
	    jComboBoxSkyNoise.setSelectedIndex(0);
	    jComboBoxAirMass.setSelectedIndex(0);
	    jComboBoxSector.setSelectedIndex(0);
	    jComboBoxFilter.setSelectedIndex(0);
	    jComboBoxLyotStop.setSelectedIndex(0);
	    jComboBoxSlitWidth.setSelectedIndex(0);
	    jComboBoxGrating.setSelectedIndex(0);
	    jTextFieldCentralWavelength.setText("");
	    jComboBoxAperture.setSelectedIndex(0);
	    jComboBoxPupil.setSelectedIndex(0);
	    jComboBoxWindow.setSelectedIndex(0);
	    jTextFieldOnSrcTime.setText("");
	    jTextFieldChopThrow.setText("");
	}
  } //end of jButtonBlankAll_action

//--------------------------------------------------------
    
    void jMoreObsParms_action(ActionEvent e) {
	Point loc = getLocation();
	Dimension frmSize = new Dimension(400,400);
	MoreObsParms_Window.setSize( frmSize );
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	MoreObsParms_Window.setLocation( 10, (int)(screenSize.getHeight() - frmSize.getHeight()) - 7 );
	MoreObsParms_Window.show();
	MoreObsParms_Window.setState( Frame.NORMAL );
	jObsProgressBar.start();
    } //end of jMoreObsParms_action

} //end of class JPanelMaster

//===============================================================================
/* 
 * This class creates a window for overriding/modifying additional observation parameters.
 * Originally written by Ziad Saleh (2002) to mod environment params,
 * re-written by Frank Varosi (2003).
 */

class JmoreObsParms extends JFrame
{
    jeiCmd jeiCommand = new jeiCmd();
    JCheckBox jCheckBoxReadMode = new JCheckBox("Readout Mode:");
    EPICSComboBox eComboBoxReadMode;

    JCheckBox jCheckBoxNodMode = new JCheckBox("Nod Handshake:");
    EPICSComboBox eComboBoxNodMode;

    JCheckBox jCheckBoxFrameTime = new JCheckBox("Frame Time (ms)");
    EPICSTextField eTFinFrameTime = new EPICSTextField(EPICS.prefix + "observationSetup.N",
						       EPICS.prefix + "observationSetup.VALO","frmtime",true);

    JCheckBox jCheckBoxSaveFreq = new JCheckBox("Save Frequency");
    EPICSTextField eTFinSaveFreq = new EPICSTextField(EPICS.prefix + "observationSetup.M",
						      EPICS.prefix + "observationSetup.VALN","savefreq",true);

    JCheckBox jCheckBoxNodDwell = new JCheckBox("Nod Dwell (s)");
    EPICSTextField eTFinNodDwell = new EPICSTextField(EPICS.prefix + "observationSetup.J",
						      EPICS.prefix + "observationSetup.VALL","noddwell",true);

    JCheckBox jCheckBoxNodSettle = new JCheckBox("Nod Settle (s)");
    EPICSTextField eTFinNodSettle = new EPICSTextField(EPICS.prefix + "observationSetup.K",
						       EPICS.prefix + "observationSetup.VALM","nodsettle",true);

    JCheckBox jCheckBoxEmissivity = new JCheckBox("Emissivity");
    EPICSTextField eTFinEmissivity = new EPICSTextField(EPICS.prefix + "observationSetup.H",
							EPICS.prefix + "observationSetup.VALH","Emissivity", true);

    JCheckBox jCheckBoxTemperature = new JCheckBox("Temperature (C)");
    EPICSTextField eTFinTemperature = new EPICSTextField(EPICS.prefix + "observationSetup.G",
							 EPICS.prefix+"observationSetup.VALG","Temperature", true);

    JCheckBox jCheckBoxRotator = new JCheckBox("Rotator Rate");
    EPICSTextField eTFinRotator = new EPICSTextField(EPICS.prefix + "observationSetup.I",
						     EPICS.prefix + "observationSetup.VALI","Rotator Rate", true);
//------------------------------------------------

    public JmoreObsParms()
    {
	super("Additional Observation Parameters");
	this.getContentPane().setLayout(new RatioLayout());

	String [] combobox_items = {"", "S1R3", "S1", "Automatic"};
	eComboBoxReadMode = new EPICSComboBox( EPICS.prefix + "observationSetup.L",
					       EPICS.prefix + "observationSetup.VALK",
					       combobox_items, EPICSComboBox.ITEM );
	if( jCheckBoxReadMode.isSelected() )
	    eComboBoxReadMode.setEnabled(true);
	else eComboBoxReadMode.setEnabled(false);

	String [] combobox_TF = {"", "TRUE", "FALSE"};
	eComboBoxNodMode = new EPICSComboBox( EPICS.prefix + "dc:acqControl.G",
					      EPICS.prefix + "dc:acqControlG.VALH",
					      combobox_TF, EPICSComboBox.ITEM );
	if( jCheckBoxNodMode.isSelected() )
	    eComboBoxNodMode.setEnabled(true);
	else eComboBoxNodMode.setEnabled(false);

	if( jCheckBoxFrameTime.isSelected() )
	    eTFinFrameTime.setEnabled(true);
	else eTFinFrameTime.setEnabled(false);

	if( jCheckBoxSaveFreq.isSelected() )
	    eTFinSaveFreq.setEnabled(true);
	else eTFinSaveFreq.setEnabled(false);

	if( jCheckBoxNodDwell.isSelected() )
	    eTFinNodDwell.setEnabled(true);
	else eTFinNodDwell.setEnabled(false);

	if( jCheckBoxNodSettle.isSelected() )
	    eTFinNodSettle.setEnabled(true);
	else eTFinNodSettle.setEnabled(false);

	if( jCheckBoxEmissivity.isSelected() )
	    eTFinEmissivity.setEnabled(true);
	else eTFinEmissivity.setEnabled(false);

	if( jCheckBoxTemperature.isSelected() )
	    eTFinTemperature.setEnabled(true);
	else eTFinTemperature.setEnabled(false);

	if( jCheckBoxRotator.isSelected() )
	    eTFinRotator.setEnabled(true);
	else eTFinRotator.setEnabled(false);

	JPanel LeftPanel = new JPanel();
	LeftPanel.setLayout(new GridLayout(9,1));
	    
	LeftPanel.add(new JLabel(""));
	LeftPanel.add(jCheckBoxReadMode);
	LeftPanel.add(jCheckBoxNodMode);
	LeftPanel.add(jCheckBoxFrameTime);
	LeftPanel.add(jCheckBoxSaveFreq);
	LeftPanel.add(jCheckBoxNodDwell);
	LeftPanel.add(jCheckBoxNodSettle);
	LeftPanel.add(jCheckBoxEmissivity);
	LeftPanel.add(jCheckBoxTemperature);

//	LeftPanel.add(jCheckBoxRotator);
//	LeftPanel.add(new JLabel("Humidity (real)"));
//	LeftPanel.add(new JLabel("Air Mass (real)"));
//	LeftPanel.add(new JLabel("Telescope In Position"));

//	String tcs = EPICS.prefix.substring(0,1) + ":tc:";
//	EPICSTextField Humidity_TF = new EPICSTextField(tcs + "sad:currentRH", "Humidity");
//	EPICSTextField AirMass_TF = new EPICSTextField(tcs + "sad:airMassNow", "Environment");
//	EPICSTextField inPosition_TF = new EPICSTextField(EPICS.prefix + "telescopeInPosition","TCS in Position");

	JPanel MiddlePanel = new JPanel();
	MiddlePanel.setLayout(new GridLayout(9,1));
	MiddlePanel.add(new JLabel("Set Value",JLabel.CENTER));
	MiddlePanel.add(eComboBoxReadMode);
	MiddlePanel.add(eComboBoxNodMode);
	MiddlePanel.add(eTFinFrameTime);
	MiddlePanel.add(eTFinSaveFreq);
	MiddlePanel.add(eTFinNodDwell);
	MiddlePanel.add(eTFinNodSettle);
	MiddlePanel.add(eTFinEmissivity);
	MiddlePanel.add(eTFinTemperature);
//	MiddlePanel.add(eTFinRotator);
//	MiddlePanel.add(Humidity_TF);
//	MiddlePanel.add(AirMass_TF);
//	MiddlePanel.add(inPosition_TF);
	    
	EPICSTextField eTFoutReadMode = new EPICSTextField(EPICS.prefix + "sad:readoutMode","rdoutmode");
	EPICSTextField eTFoutNodMode = new EPICSTextField(EPICS.prefix + "dc:acqControlG.VALH","nodmode");
	EPICSTextField eTFoutFrameTime = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALA","frametime");
	EPICSTextField eTFoutSaveFreq = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALB","savefreq");
	EPICSTextField eTFoutNodDwell = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALF","noddwell");
	EPICSTextField eTFoutNodSettle = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALG","nodsettle");
	EPICSTextField eTFoutEmissivity = new EPICSTextField(EPICS.prefix+"observationSetup.VALH","Emissivity");
	EPICSTextField eTFoutTemperature = new EPICSTextField(EPICS.prefix + "observationSetup.VALG","Temp.");

	eTFoutReadMode.setEditable(false);
	eTFoutNodMode.setEditable(false);
	eTFoutFrameTime.setEditable(false);
	eTFoutSaveFreq.setEditable(false);
	eTFoutNodDwell.setEditable(false);
	eTFoutNodSettle.setEditable(false);
	eTFoutEmissivity.setEditable(false);
	eTFoutTemperature.setEditable(false);

//	EPICSTextField Current_Temperature = new EPICSTextField(tcs + "sad:currentExternalTemp","real T");
//	EPICSTextField Current_Rotator = new EPICSTextField(EPICS.prefix + "observationSetup.VALI","Rot.Rate");
//	EPICSTextField Current_Humidity = new EPICSTextField(tcs + "sad:currentRH", "Humidity");
//	EPICSTextField Current_AirMass = new EPICSTextField(tcs + "sad:airMassNow","Environment");
//	EPICSTextField Current_inPosition = new EPICSTextField(EPICS.prefix + "telescopeInPosition", "inPos");
//	Current_Rotator.setEditable(false);
//	Current_Humidity.setEditable(false);
//	Current_AirMass.setEditable(false);
//	Current_inPosition.setEditable(false);

	JPanel RightPanel = new JPanel();
	RightPanel.setLayout(new GridLayout(9,1));
	RightPanel.add(new JLabel("Current Value",JLabel.CENTER));
	RightPanel.add(eTFoutReadMode);
	RightPanel.add(eTFoutNodMode);
	RightPanel.add(eTFoutFrameTime);
	RightPanel.add(eTFoutSaveFreq);
	RightPanel.add(eTFoutNodDwell);
	RightPanel.add(eTFoutNodSettle);
	RightPanel.add(eTFoutEmissivity);
	RightPanel.add(eTFoutTemperature);
//	RightPanel.add(Current_Rotator);
//	RightPanel.add(Current_Humidity);
//	RightPanel.add(Current_AirMass);
//	RightPanel.add(Current_inPosition);

	EPICSLabel metaFrameTime = new EPICSLabel(EPICS.prefix + "dc:DCMetaOutG.VALC", "meta-FrameTime___=");
	EPICSLabel monChopFreq = new EPICSLabel(EPICS.prefix + "dc:physOutG.VALD",     "Chop Frequency____=");
	EPICSLabel monDutyCycle = new EPICSLabel(EPICS.prefix + "dc:physOutG.VALH",    "Duty Cycle________=");
	EPICSLabel monFrameCoadds = new EPICSLabel(EPICS.prefix + "dc:hardwareG.VALC", "Frame Coadds_____=");
	EPICSLabel monSkipFrames = new EPICSLabel(EPICS.prefix + "dc:hardwareG.VALD",  "Skip Frames______=");
	EPICSLabel monWellDepth = new EPICSLabel(EPICS.prefix + "dc:DCBiasG.VALI",     "Well Depth_______=");

	JPanel BottomPanel = new JPanel();
	BottomPanel.setLayout(new GridLayout(6,1));
	BottomPanel.add( metaFrameTime );
	BottomPanel.add( monChopFreq );
	BottomPanel.add( monDutyCycle );
	BottomPanel.add( monFrameCoadds );
	BottomPanel.add( monSkipFrames );
	BottomPanel.add( monWellDepth );

	this.getContentPane().add("0.01,0;0.39,0.6",LeftPanel);
	this.getContentPane().add("0.40,0;0.29,0.6",MiddlePanel);
	this.getContentPane().add("0.70,0;0.29,0.6",RightPanel);
	this.getContentPane().add("0.05,0.62;0.6,0.33",BottomPanel);

//	EPICSLabel TopAcq = new EPICSLabel(EPICS.prefix + "acq", "acq=");
//	EPICSLabel TopRdout = new EPICSLabel(EPICS.prefix + "rdout", "rdout=");
//	EPICSLabel TopPrep = new EPICSLabel(EPICS.prefix + "prep", "prep=");
//	EPICSLabel tcsHeartbeat = new EPICSLabel(EPICS.prefix+"tcsHeartbeat",EPICS.prefix+"tcsHeartbeat=");
	JButton buttonDONE = new JButton ("DONE");

//	this.getContentPane().add("0.05,0.96;0.3,0.04",TopAcq);
//	this.getContentPane().add("0.36,0.96;0.3,0.04",TopRdout);
//	this.getContentPane().add("0.67,0.96;0.3,0.04",TopPrep);
//	this.getContentPane().add("0.1,0.94;0.5,0.05",tcsHeartbeat);
	this.getContentPane().add("0.7,0.8;0.3,0.1",buttonDONE);

	buttonDONE.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonDONE_action(); } });

	jCheckBoxReadMode.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxReadMode_action(); } });

	jCheckBoxNodMode.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxNodMode_action(); } });

	jCheckBoxFrameTime.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxFrameTime_action(); } });

	jCheckBoxSaveFreq.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxSaveFreq_action(); } });

	jCheckBoxNodDwell.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxNodDwell_action(); } });

	jCheckBoxNodSettle.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxNodSettle_action(); } });

 	jCheckBoxEmissivity.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxEmissivity_action(); } });

 	jCheckBoxTemperature.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { jCheckBoxTemperature_action(); } });
    }

//--------------------------------------------------------
    void buttonDONE_action() { this.dispose(); }

//-------------------------------------------------------------------------------

    void jCheckBoxReadMode_action() {
	if( jCheckBoxReadMode.isSelected() )
	    eComboBoxReadMode.setEnabled(true);
	else
	    eComboBoxReadMode.setEnabled(false);
    } //end of jCheckBoxReadMode_action

//-------------------------------------------------------------------------------

    void jCheckBoxNodMode_action() {
	if( jCheckBoxNodMode.isSelected() )
	    eComboBoxNodMode.setEnabled(true);
	else
	    eComboBoxNodMode.setEnabled(false);
    } //end of jCheckBoxNodMode_action

//-------------------------------------------------------------------------------

    void jCheckBoxFrameTime_action() {
	if( jCheckBoxFrameTime.isSelected() ) {
	    putEpics( EPICS.prefix + "observationSetup.R", "TRUE" );
	    eTFinFrameTime.setEnabled(true);
	} else {
	    eTFinFrameTime.setEnabled(false);
	    putEpics( EPICS.prefix + "observationSetup.R", "FALSE" );
	}
    } //end of jCheckBoxFrameTime_action

//-------------------------------------------------------------------------------

    void jCheckBoxSaveFreq_action() {
	if( jCheckBoxSaveFreq.isSelected() ) {
	    putEpics( EPICS.prefix + "observationSetup.Q", "TRUE" );
	    eTFinSaveFreq.setEnabled(true);
	} else {
	    eTFinSaveFreq.setEnabled(false);
	    putEpics( EPICS.prefix + "observationSetup.Q", "FALSE" );
	}
    } //end of jCheckBoxSaveFreq_action

//-------------------------------------------------------------------------------

    void jCheckBoxNodDwell_action() {
	if( jCheckBoxNodDwell.isSelected() ) {
	    putEpics( EPICS.prefix + "observationSetup.P", "TRUE" );
	    eTFinNodDwell.setEnabled(true);
	} else {
	    eTFinNodDwell.setEnabled(false);
	    putEpics( EPICS.prefix + "observationSetup.P", "FALSE" );
	}
    } //end of jCheckBoxNodDwell_action

//-------------------------------------------------------------------------------

    void jCheckBoxNodSettle_action() {
	if( jCheckBoxNodSettle.isSelected() ) {
	    putEpics( EPICS.prefix + "observationSetup.P", "TRUE" );
	    eTFinNodSettle.setEnabled(true);
	} else {
	    eTFinNodSettle.setEnabled(false);
	    putEpics( EPICS.prefix + "observationSetup.P", "FALSE" );
	}
    } //end of jCheckBoxNodSettle_action

//-------------------------------------------------------------------------------

    void jCheckBoxEmissivity_action() {
	if( jCheckBoxEmissivity.isSelected() )
	    eTFinEmissivity.setEnabled(true);
	else
	    eTFinEmissivity.setEnabled(false);
    } //end of jCheckBoxEmissivity_action

//-------------------------------------------------------------------------------

    void jCheckBoxTemperature_action() {
	if( jCheckBoxTemperature.isSelected() )
	    eTFinTemperature.setEnabled(true);
	else
	    eTFinTemperature.setEnabled(false);
    } //end of jCheckBoxTemperature_action

//-------------------------------------------------------------------------------

    void jCheckBoxRotator_action() {
	if( jCheckBoxRotator.isSelected() )
	    eTFinRotator.setEnabled(true);
	else
	    eTFinRotator.setEnabled(false);
    } //end of jCheckBoxRotator_action

//-------------------------------------------------------------------------------
  /**
   * Puts the val to the specified EPICS record field
   *@param putRec String: the EPICS record
   *@param value String: Value to be put
   */
    void putEpics( String putRec, String value ) {
	if( value.trim().equals("") || putRec.trim().equals("") ) return;
	String cmd = new String("PUT " + putRec + "  \""+value+ "\"") ;
	jeiCommand.execute(cmd);
    } //end of putEpics

}//end of class JmoreObsParms******************************************

//===============================================================================
//Varosi 4/15/02:
//The timer will generate events that will trigger actionPerformed to update obs progress bar:

class JObsProgressBar extends JProgressBar implements ActionListener
{
    Timer progressTimer;
    int timerInterval, totalFrames, prevFrmCnt;
    JPanelDetector DetectorParams;
    EPICSTextField frameCount;
    JLabel LabTotFrames, LabTotNodSets;

    public JObsProgressBar( int timeDelay, EPICSTextField frmCntMonitor,
			    JPanelDetector DetectorPanel, JLabel jLabelTotalFrames, JLabel jLabelTotalNodSets )
    {
	timerInterval = timeDelay;
	frameCount = frmCntMonitor;
	DetectorParams = DetectorPanel;
	LabTotFrames = jLabelTotalFrames;
	LabTotNodSets = jLabelTotalNodSets;
	init();
    }

    public void init()
    {
	setMinimum(0);
	setMaximum(100);
	setValue(0);
	totalFrames = 2;
	prevFrmCnt = 999999;
    }

    public void start()
    {
	if( progressTimer == null )
	    progressTimer = new Timer( timerInterval, this );
	else
	    progressTimer.stop();

	prevFrmCnt = 999999; //this will cause resetObsStatusCounters() to be invoked by actionPerformed().
	progressTimer.start();
    }

    public void stop()
    {
	progressTimer.stop();
    }

    public void actionPerformed( ActionEvent e )
    {
	String fcs="";
	try {
	    fcs = frameCount.getText().trim();
	    int fci = 0;
	    if( fcs.length() > 0 ) fci = Integer.parseInt( fcs );
	    if( fci < prevFrmCnt || fci < 2 ) resetObsStatusCounters();
	    if( fci != prevFrmCnt ) {
		if( totalFrames < fci ) totalFrames = fci + 1;
		setValue( (100*fci)/totalFrames );
		prevFrmCnt = fci;
	    }
	}
	catch(Exception x) {
	    System.out.println(">in jObsProgressBar: " + x.toString() + ": " + fcs + "<");
	    x.printStackTrace();
	}
    }

    void resetObsStatusCounters()
    {
	totalFrames = DetectorParams.TotalFrames();
	LabTotFrames.setText("out of  " + totalFrames + " save Frames");
	LabTotNodSets.setText("of " + DetectorParams.CTcTextGroup[DetectorParams.ctiNodSets].getText() + ".");
	DetectorParams.jTextFieldTotalFrames.setText( totalFrames + " " );
	DetectorParams.totFramesLabel.setText( "out of  " + totalFrames );
    }
}
