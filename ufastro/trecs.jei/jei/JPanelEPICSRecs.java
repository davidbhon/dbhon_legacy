package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;                // Object serialization streams.
import java.net.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import jca.*;
import jca.dbr.*;

//===============================================================================
/**
 * Handles the Records tabbed pane
 */
public class JPanelEPICSRecs extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelEPICSRecs.java,v 0.2 2003/05/27 12:03:54 varosi beta $";
  static Vector EPICSRecListFrame_names = new Vector();
  static Vector EPICSRecListFrames = new Vector();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JPanel jPanelSouthUpper = new JPanel();
  JPanel jPanelSouthLower = new JPanel();
  BorderLayout borderLayout1 = new BorderLayout();
  JTree jTree1 = new JTree();
  DefaultTreeModel treeModel;
  EPICSRecsTreeCellRenderer TreeCellRenderer = new EPICSRecsTreeCellRenderer();
  JScrollPane jScrollPane1 = new JScrollPane(jTree1);
  JButton jButtonAdd = new JButton();
  JComboBox jComboBoxRecName = new JComboBox();
  JTextField jTextFieldDesc = new JTextField();
  JTextField jTextFieldWin = new JTextField();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel3 = new JLabel();
  BorderLayout borderLayout2 = new BorderLayout();
  EPICSTextField currentRecTextField = new EPICSTextField();
  String recName_prev_val = "";

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public JPanelEPICSRecs() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      jeiError.show("jbInit exception in JPanelEPICSRecs");
    }
  } //end of JPanelEPICSRecs


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    jButtonAdd.setText("Add to specified window");
    jButtonAdd.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonAdd_actionPerformed(e);
      }
    });

    jComboBoxRecName.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
      public void focusGained(FocusEvent fe) {
        jComboBoxRecName_focusGained();
      }
      public void focusLost(FocusEvent fe) {
        jComboBoxRecName_focusLost();
      }
    });

    jComboBoxRecName.getEditor().getEditorComponent().addKeyListener(new KeyListener() {
      public void keyTyped (KeyEvent ke) {
      }

      public void keyReleased (KeyEvent ke) {
      }
      public void keyPressed (KeyEvent ke) {
        jComboBoxRecName_keyPressed(ke);
      }
    });

    this.setLayout(borderLayout1);
    jTextFieldDesc.setColumns(40);
    jTextFieldWin.setColumns(20);
    jTextFieldWin.setText("Custom List 1");
    jPanel1.setLayout(borderLayout2);
    this.add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jScrollPane1, BorderLayout.CENTER);
    this.add(jPanel2, BorderLayout.SOUTH);
    jPanel2.setLayout(new GridLayout(2,1));
    jPanel2.add(jPanelSouthUpper);
    jPanel2.add(jPanelSouthLower);

    jPanelSouthUpper.setLayout( new RatioLayout() );
    jPanelSouthUpper.add("0.02,0.1;0.05,0.8", new JLabel("record:"));
    jPanelSouthUpper.add("0.08,0.1;0.27,0.8", jComboBoxRecName);
    jComboBoxRecName.setEditable(true);
    jPanelSouthUpper.add("0.40,0.1;0.05,0.8", new JLabel("value="));
    jPanelSouthUpper.add("0.46,0.1;0.25,0.8", currentRecTextField);
    jPanelSouthUpper.add("0.75,0.1;0.22,0.8", jButtonAdd);

    jPanelSouthLower.add(new JLabel("desc:"), null);
    jPanelSouthLower.add(jTextFieldDesc, null);
    jPanelSouthLower.add(new JLabel("to window:"), null);
    jPanelSouthLower.add(jTextFieldWin, null);
    jScrollPane1.getViewport().add(jTree1, null);
    //    jTree1.setCellRenderer(TreeCellRenderer);
    load_rec_tree();
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   * JPanelEPICSRecs#record name focus gained
   */
  public void jComboBoxRecName_focusGained() {
    recName_prev_val = (String)jComboBoxRecName.getSelectedItem();
  } //end of jComboBoxRecName


//-------------------------------------------------------------------------------
  /**
   * JPanelEPICSRecs#record name focus lost
   */
  public void jComboBoxRecName_focusLost() {
    if (recName_prev_val == null) return;
    if (!recName_prev_val.equals((String)jComboBoxRecName.getSelectedItem())){
      currentRecTextField.setEPICSRecords((String)jComboBoxRecName.getSelectedItem(),
          jTextFieldDesc.getText());
      setHistory(jComboBoxRecName, (String)jComboBoxRecName.getSelectedItem());
    }
  } //end of jComboBoxRecName_focusLost


//-------------------------------------------------------------------------------
  /**
   * JPanelEPICSRecs#record name key pressed
   * @param ke TBD
   */
  public void jComboBoxRecName_keyPressed(KeyEvent ke) {
    if (ke.getKeyChar() == '\n') { // we got the enter key
      //System.out.println(recName_prev_val);
      //if (recName_prev_val == null) recName_prev_val = "";
      //if (!recName_prev_val.equals((String)jComboBoxRecName.getSelectedItem()))
      currentRecTextField.setEPICSRecords(
         (String)jComboBoxRecName.getSelectedItem(),jTextFieldDesc.getText());
      recName_prev_val = (String)jComboBoxRecName.getSelectedItem();
      // System.out.println(recName_prev_val);
    }
  } //end of jComboBoxRecName_keyPressed


//-------------------------------------------------------------------------------
  /**
   * Loads the record
   */
  void load_rec_tree() {
    DefaultMutableTreeNode parent[] = new DefaultMutableTreeNode[10];
    DefaultMutableTreeNode node;
    EPICSRecsNode enode;
    String line;
    int lev = 0;
    int curr_lev = 0;

    enode = new EPICSRecsNode("sect", "EPICS Records");
    parent[0] = new DefaultMutableTreeNode(enode);
    for (int i = 1; i <= 9; i++) {
      parent[i] = null;
    }

    // open rec file
    TextFile in_file = new TextFile(jei.data_path + "jpdf.txt", TextFile.IN);

    // read the file
    while (!(line = in_file.readLine()).equals("eod")) {
      if (line.trim().equals("")) ;// ignore it
      else if (line.charAt(0) == ';') ; // ignore it
      else if (line.charAt(0) >= '1' && line.charAt(0) <= '9' ) { // new section
        lev = line.charAt(0) - '0';
        if (parent[lev - 1] == null) {
          jeiError.show("no parent, level too big?. line = " + line);
          return;
        }
        else {
          curr_lev = lev;
          enode = new EPICSRecsNode("sect", line.substring(2));
          parent[curr_lev] = new DefaultMutableTreeNode (enode);
          parent[curr_lev-1].add(parent[curr_lev]);
        }
      }
      else if (line.charAt(0) == ' ') { // a field
        enode = new EPICSRecsNode("field", "." + line.trim());
        node = new DefaultMutableTreeNode(enode);
        parent[curr_lev + 1].add(node);
      }
      else { // new rec
        enode = new EPICSRecsNode("rec", line);
        parent[curr_lev + 1] = new DefaultMutableTreeNode(enode);
        parent[curr_lev].add(parent[curr_lev + 1]);
      }
    }
    in_file.close();

    treeModel = new DefaultTreeModel(parent[0]);
    jTree1.setModel(treeModel);
    jTree1.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
      public void valueChanged(TreeSelectionEvent e) {
        jTree1_valueChanged(e);
      }
    });
  } // end of load_rec_tree


//-------------------------------------------------------------------------------
  /**
   *Add identified record:field to an EPICSRecListFrame
   *@param e not used
   */
  void jButtonAdd_actionPerformed(ActionEvent e) {
    jeiCmd command = new jeiCmd();
    String rec = (String)jComboBoxRecName.getSelectedItem();
    String desc = jTextFieldDesc.getText();
    String win = jTextFieldWin.getText();
    if (rec == null) jeiError.show("No record or field selected");
    else if (rec.trim().equals("")) jeiError.show("No record or field specified");
    else if (win.trim().equals("")) jeiError.show("No window name specified");
    else command.execute("ADD_REC " + rec + "&&" + desc + " &&" + win);
  } //end of jButtonAdd_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *show newly selected rec in text boxes below
   *@param e TreeSelectionEvent: ??TBD
   */
  void jTree1_valueChanged(TreeSelectionEvent e) {
    TreePath path = jTree1.getSelectionPath();
    if (path == null) {
      ///jeiError.show("nothing selected");
    }
    else {
      MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(path.getPathCount()-1);
      String node_text = treeNode.toString();
      if (treeModel.isLeaf(treeNode)) {
        int p = node_text.indexOf(',');
        if (node_text.charAt(0) == '.') { // a field node
          //check to see if we shouldn't monitor this record
          StringTokenizer st = new StringTokenizer(node_text,",");
          st.nextToken();
          if (st.nextToken().endsWith("X")) {
            jeiError.show("Cannot monitor this record");
          }
          else {
            String field_name = node_text.substring(0, p);
            String desc = node_text.substring(p + 1);
            MutableTreeNode recNode = (MutableTreeNode)path.getPathComponent(path.getPathCount()-2);
 ///         jComboBoxRecName.setSelectedItem(field_name);
            String rec_text = recNode.toString();
            p = rec_text.indexOf(',');
            String rec_name = rec_text.substring(0, p);
            jComboBoxRecName.setSelectedItem(EPICS.prefix + rec_name + field_name);
            setHistory(jComboBoxRecName, (String)jComboBoxRecName.getSelectedItem());
            jTextFieldDesc.setText(desc);
            currentRecTextField.setEPICSRecords((String)jComboBoxRecName.getSelectedItem(),
                    jTextFieldDesc.getText());
          }
        }
        else {  // a record, not a field
          String rec_name = node_text.substring(0, p);
          String desc = node_text.substring(p + 1);
          jComboBoxRecName.setSelectedItem(EPICS.prefix + rec_name);
          setHistory(jComboBoxRecName, (String)jComboBoxRecName.getSelectedItem());
          jTextFieldDesc.setText(desc);
          currentRecTextField.setEPICSRecords((String)jComboBoxRecName.getSelectedItem(),
                    jTextFieldDesc.getText());
        }
      }
    }
  } // end of jTree1_valueChanged


//-------------------------------------------------------------------------------
  /**
   * set the combobox history
   *@param j TBD
   *@param s TBD
   */
  private void setHistory (JComboBox j, String s) {
    int count = j.getItemCount();
    for (int i=0; i<count; i++) {
      if (((String)j.getItemAt(i)).compareTo(s) == 0) return;
    }
    j.addItem(s);
    if (j.getItemCount() > 10) j.removeItemAt(0);
  } //end of setHistory

} //end of class JPanelEPICSRecs

