package ufjei;


/**
 *Handles the current version of the package
 *Date is hard coded in the version field
 *Date is six digits starting with the year then month then day
 *This string is used for the name in the title bar
 */
  public class version {
  public static final String rcsID = "$Name:  $ $Id: version.java,v 0.102 2003/10/23 22:06:10 varosi beta $";
  public static final String version = "2003/10/23";
}
