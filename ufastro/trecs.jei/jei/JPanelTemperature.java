package ufjei;
   import java.awt.event.*;
   import javax.swing.*;
   import javax.swing.event.*;
   import java.awt.*;
   import java.net.*;
   import java.applet.*;
   import java.io.*;
   import java.util.*;
   import java.util.Date.*;
   import java.sql.Timestamp;
   import jca.*;
   import jca.event.*;
   import jca.dbr.*;

//===============================================================================
/**
 *This Class handles the Temperature tabbed pane
 */
public class JPanelTemperature extends JPanel implements MonitorListener{
  TextFile pw;
  public static final String rcsID = "$Name:  $ $Id: JPanelTemperature.java,v 0.19 2003/07/16 22:44:17 varosi beta $";
  public String fileName;                 // file containing temperature sensors config.
  public String td_filename = "trecs.td"; // default temperature graph data filename is trecs.td

  jeiCmd jeiCommand = new jeiCmd();

  // do NOT automatically put set point,
  // can cause bad temperature setting, so use set point PUT button.

  EPICSTextField set_Point = new EPICSTextField(EPICS.prefix + "ec:tempSet.A",
                                                EPICS.prefix + "ec:tempSetG.VALC"," desired Temp. set point");
  JButton jButtonClear = new JButton("CLEAR  epics");

  EPICSTextField p_Text = new EPICSTextField(EPICS.prefix + "ec:tempSetG.D",
                                                EPICS.prefix + "ec:tempSetG.VALE","P value",true);
  EPICSTextField i_Text = new EPICSTextField(EPICS.prefix + "ec:tempSetG.E",
                                                EPICS.prefix + "ec:tempSetG.VALF","I value",true);
  EPICSTextField d_Text = new EPICSTextField(EPICS.prefix + "ec:tempSetG.F",
                                                EPICS.prefix + "ec:tempSetG.VALG","D value",true);
  JLabel p_Label = new JLabel("     P =");
  JLabel i_Label = new JLabel("     I =");
  JLabel d_Label = new JLabel("     D =");

  JButton jButtonApplySetp = new JButton("APPLY");
  JButton jButtonConnect = new JButton("Connect  EPICS  to  Agent");
  JButton jButtonPID = new JButton("Hide  PID");
  JButton jButtonHeaterOff = new JButton("Emergency Heater OFF");
  JButton jButtonColdhead = new JButton("Turn Coldhead On");
  JButton jButtonPressure = new JButton("Turn Pressure Sensor On");
  EPICSTextField actualTdet = new EPICSTextField(EPICS.prefix+"ec:arrayG.VALA",";actual array temperature");
  JEIsensorParameters SensorParms[];
  GridLayout gridLayout1 = new GridLayout();
  GridLayout gridLayout2 = new GridLayout();
  GridLayout gridLayout3 = new GridLayout();
  JPanel jPanelLeft = new JPanel();
  JPanel jColorPanel = new JPanel();
  JPanel jSetPUnitPanel = new JPanel();
  JPanel jPanelSensorLabel = new JPanel();
  JPanel jPanelSensorTemp = new JPanel();
  JPanel jPanelSensorUnits = new JPanel();
  JPanel jPanelTempControl = new JPanel();
  JPanel jRatePanel = new JPanel();
  JPanel jKeyPanel = new JPanel();
  JPanel jButtonPanel = new JPanel();
  JPanel jPanelHeater = new JPanel();
  JPanel jRateUnitPanel = new JPanel();
  JPanel jCheckBoxPanel = new JPanel();
  JPanel jGraphButtonPanel = new JPanel();
  JGraphPanel jGraphPanel;
  JButton jGraphStartButton = new JButton("Start Graph");
  JButton jGraphParamButton = new JButton("Graph Parameters");
  JButton jGraphClearButton = new JButton("Clear Graph Data");
  JButton jGraphLoadButton = new JButton("Load Graph Data");
  GraphThread graphThread;
  RateThread rateThread;
  EPICSComboBox heaterComboBox;
  JEIsensorParameters Pressure = new JEIsensorParameters("Pressure",EPICS.prefix +"ec:pressMonG.VALA");
  JDialogGraphParameters dialogBox; // create dialog box after getting
                                    // graph parameters from file or UFlib or EPICS
  JCheckBox jCheckBoxGraphGrid = new JCheckBox("Show Grid");
  Monitor monitorColdhead = null;
  Monitor monitorPressure = null;
  EPICSLabel monitorCAD = new EPICSLabel(EPICS.prefix + "ec:apply.VAL", "CAD= ");
  EPICSLabel monitorCADmess = new EPICSLabel(EPICS.prefix + "ec:apply.MESS", "Message:");
  EPICSLabel monitorCAR = new EPICSLabel(EPICS.prefix + "ec:applyC.VAL", "CAR= ",jButtonApplySetp);
  EPICSLabel monitorCARmess = new EPICSLabel(EPICS.prefix + "ec:applyC.OMSS", "Message:",jButtonConnect);
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "ec:heartbeat",
					     EPICS.prefix + "ec:HeartBeat:");
//-------------------------------------------------------------------------------
  /**
   *Constructs the frame
   *@param fileName string that contains the file name
   */
  public JPanelTemperature(String fileName) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      this.fileName = fileName;
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of JPanelTemperature

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception
  {
    this.setLayout(new RatioLayout());

    String [] items = {"Off (0)","Low (1)","Medium (2)","High (3)"};
    heaterComboBox = new EPICSComboBox( EPICS.prefix + "ec:tempSet.B",
				        EPICS.prefix + "ec:tempSetG.VALD",
					items, EPICSComboBox.INDEX );
    ReadFile( fileName );
    Pressure.hotThreshold = 1.e-4;
    Pressure.coldThreshold = 1.3e-6;
    graphThread = new GraphThread();
    dialogBox = new JDialogGraphParameters();

    actualTdet.setEditable(false);
    actualTdet.setBackground(new Color(175,175,175));
    this.setMaximumSize(new Dimension(870, 470));
    this.setMinimumSize(new Dimension(870, 470));
    this.setPreferredSize(new Dimension(870, 470));
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    gridLayout1.setVgap(0);
    gridLayout2.setColumns(1);
    gridLayout2.setRows(0);
    gridLayout2.setVgap(0);
    gridLayout3.setColumns(1);
    gridLayout3.setRows(0);
    gridLayout3.setVgap(0);
    jPanelLeft.setLayout(gridLayout1);
    jSetPUnitPanel.setLayout(gridLayout1);
    jColorPanel.setLayout(gridLayout2);
    jPanelSensorLabel.setLayout(gridLayout2);
    jPanelSensorTemp.setLayout(gridLayout2);
    jPanelSensorUnits.setLayout(gridLayout2);
    jPanelTempControl.setLayout(gridLayout1);
    jRateUnitPanel.setLayout(gridLayout2);
    jCheckBoxPanel.setLayout(gridLayout2);
    jRatePanel.setLayout(gridLayout2);
    jKeyPanel.setLayout(gridLayout2);
    jGraphPanel.setLayout(gridLayout1);
    jButtonPanel.setLayout(gridLayout3);

    jButtonConnect.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) {
		jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR CLEAR");
		jeiCommand.execute("PUT " + EPICS.prefix + "ec:init.A 0");
		jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR START");
		try {
		    Thread.currentThread().sleep(1000);
		}
		catch(Exception e) {
                  JOptionPane.showMessageDialog(null,e.toString(),"RateThread Error",JOptionPane.ERROR_MESSAGE);
		}
		checkConnection(true);
	    }
	});

    jButtonClear.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) {
		jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR CLEAR");
		set_Point.setText("");
	    }
	});

    jButtonApplySetp.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) {
		set_Point.forcePut();
		jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR START");
	    }
	});

    jButtonPID.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonPID_action(); } });

    jGraphLoadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphLoadButton_action(e); } });

    jGraphStartButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphStartButton_action(e); } });

    jGraphClearButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {     
		if (graphThread.isAlive()) 
		    jGraphStartButton_action(e); // stop the graph thread before clearing data! 
		jGraphPanel.eraseGraphData(); 
	    }
	});

    jGraphParamButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphParamButton_action(e); } });

    jButtonColdhead.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonColdhead_action(e); } });

    jButtonPressure.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonPressure_action(e); } });

    jButtonHeaterOff.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonHeaterOff_action(e); } });

    jCheckBoxGraphGrid.setSelected(true);
    jCheckBoxGraphGrid.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { repaint(); } });

    jColorPanel.add(new JLabel(""));
    jPanelSensorLabel.add(new JLabel("Sensor Name"));
    jPanelSensorTemp.add(new JLabel("Temperature"));
    jPanelSensorUnits.add(new JLabel(""));
    jRatePanel.add(new JLabel(" Rate"));
    jRateUnitPanel.add(new JLabel(""));
    jCheckBoxPanel.add(new JLabel(""));

    for (int i=0; i<SensorParms.length; i++) {
      jColorPanel.add(SensorParms[i].statusColor);
      jPanelSensorLabel.add(SensorParms[i].label);
      jPanelSensorTemp.add(SensorParms[i].temp);
      jPanelSensorUnits.add(SensorParms[i].units);
      jRatePanel.add(SensorParms[i].graphElement.rate);
      jRateUnitPanel.add(SensorParms[i].graphElement.units);
      jCheckBoxPanel.add(SensorParms[i].graphElement.showme);
    }

    jColorPanel.add(Pressure.statusColor);
    Pressure.units.setText("Torr");
    jPanelSensorLabel.add(Pressure.label);
    jPanelSensorTemp.add(Pressure.temp);
    jPanelSensorUnits.add(Pressure.units);
    jRatePanel.add( new JLabel());
    jRateUnitPanel.add(new JLabel());
    jCheckBoxPanel.add(new JLabel());

    jKeyPanel.add(new JLabel("Red -- Warm, OK to open"));
    jKeyPanel.add(new JLabel("Yellow -- In Transition"));
    jKeyPanel.add(new JLabel("Blue -- Cold, OK for Observation"));

    jPanelLeft.add(new JLabel()); //add dummy components for spacing
    jPanelTempControl.add(new JLabel("Detector  T"));
    jSetPUnitPanel.add(new JLabel()); // add dummy components for spacing

    jPanelLeft.add(new JLabel("Set Point ="));
    jPanelTempControl.add(set_Point);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(new JLabel("Actual T ="));
    jPanelTempControl.add(actualTdet);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(p_Label);
    jPanelTempControl.add(p_Text);

    jPanelLeft.add(i_Label);
    jPanelTempControl.add(i_Text);

    jPanelLeft.add(d_Label);
    jPanelTempControl.add(d_Text);

    jPanelHeater.add(new JLabel("Heater Power:"));
    jPanelHeater.add(heaterComboBox);
    jPanelHeater.add(jButtonHeaterOff);

    jButtonPanel.add(jButtonApplySetp);
    jButtonPanel.add(jButtonPID);
    jButtonPanel.add(jButtonPressure);
    jButtonPanel.add(jButtonClear);
    jGraphButtonPanel.setLayout(new GridLayout(0,1));
    jGraphButtonPanel.add(jGraphStartButton);
    jGraphButtonPanel.add(jGraphParamButton);
    jGraphButtonPanel.add(jGraphClearButton);
//      jGraphButtonPanel.add(jGraphSaveButton);
    jGraphButtonPanel.add(jGraphLoadButton);
    for (int h=0; h<3; h++) jSetPUnitPanel.add(new JLabel()); //add dummy components for spacing
    HeartBeatLabel.setHorizontalAlignment(JLabel.LEFT);
    this.add("0.01,0.02;0.08,0.40", jPanelLeft);
    this.add("0.09,0.02;0.08,0.40", jPanelTempControl);
    this.add("0.17,0.02;0.02,0.40", jSetPUnitPanel);
    this.add("0.01,0.46;0.20,0.19", jPanelHeater);
    this.add("0.01,0.65;0.20,0.34", jButtonPanel);
    this.add("0.22,0.02;0.03,0.44", jColorPanel);
    this.add("0.26,0.02;0.10,0.44", jPanelSensorLabel);
    this.add("0.36,0.02;0.085,0.44", jPanelSensorTemp);
    this.add("0.45,0.02;0.03,0.44", jPanelSensorUnits);
    //    this.add("0.47,0.02;0.05,0.44", jRatePanel);
    //    this.add("0.525,0.02;0.04,0.44", jRateUnitPanel);
    this.add("0.48,0.02;0.02,0.44", jCheckBoxPanel);
    this.add("0.52,0.02;0.47,0.73", jGraphPanel);
    this.add("0.25,0.51;0.21,0.15", jKeyPanel);
    this.add("0.25,0.70;0.20,0.07", jButtonConnect);
    this.add("0.60,0.78;0.10,0.05", jCheckBoxGraphGrid);
    this.add("0.77,0.78;0.18,0.20", jGraphButtonPanel);
    this.add("0.25,0.80;0.10;0.05", monitorCAD);
    this.add("0.35,0.80;0.50;0.05", monitorCADmess);
    this.add("0.25,0.85;0.10;0.05", monitorCAR);
    this.add("0.35,0.85;0.50;0.05", monitorCARmess);
    this.add("0.25,0.90;0.50;0.05", HeartBeatLabel);

    //setup monitor for Pressure Sensor On button:
    PV pv2 = new PV(EPICS.prefix + "ec:vacPowerG.A");
    Ca.pendIO(jeiFrame.TIMEOUT);
    monitorPressure = pv2.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
    Ca.pendIO(jeiFrame.TIMEOUT);

    //setup monitor for Coldhead button:
    //PV pv1 = new PV(EPICS.prefix + "ec:chPowerG.A");
    //Ca.pendIO(jeiFrame.TIMEOUT);
    //monitorColdhead = pv1.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
    //Ca.pendIO(jeiFrame.TIMEOUT);

    p_Label.setVisible(false);
    i_Label.setVisible(false);
    d_Label.setVisible(false);
    p_Text.setVisible(false);
    i_Text.setVisible(false);
    d_Text.setVisible(false);
    jButtonPID.setText("Show PID");
    checkConnection(true);
  } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   *Event handler for monitor events
   *@param e not used
   */
  public void monitorChanged(MonitorEvent me) {
    String value = "";
    if( me.pv().name().equals(EPICS.prefix + "ec:chPowerG.VALC") )
	{
	    value = EPICS.get( me.pv().name() ).trim();
	    if (value.trim().equals("on"))
		value = "Off";
	    else
		value = "On";
	    jButtonColdhead.setText("Turn Coldhead "+value);
	}
    else if( me.pv().name().equals(EPICS.prefix+ "ec:vacPowerG.VALC") )
	{
	    value = EPICS.get( me.pv().name() ).trim();
	    if (value.equals("on"))
		value = "Off";
	    else
		value = "On";
	    jButtonPressure.setText("Turn Pressure Sensor "+value);
	}
  }

//-------------------------------------------------------------------------------

    void checkConnection( boolean indicateOK )
    {
	String socTset = EPICS.get(EPICS.prefix + "ec:tempSetG.VALB"); 

	if( socTset.equals("-1") || 
	    checkforBadSocket( monitorCARmess.getText() ) )
	    { 
		jButtonConnect.setBackground(Color.red);
		jButtonConnect.setForeground(Color.white);
		jButtonConnect.setText("Connect  EPICS  to  Agent");
	    }
	else if( indicateOK ) {
	    jButtonConnect.setBackground(Color.green);
	    jButtonConnect.setForeground(Color.black);
	    jButtonConnect.setText("Connected  to  Agent");
	}
    }
//-------------------------------------------------------------------------------

    boolean checkforBadSocket( String EPICSmessage )
    {
	if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	    EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	    EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
	    return true;
	else
	    return false;
    }

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonHeaterOff
   *JPanelTemperature emergency heater off button action performed
   *@param e not used
   */
  void jButtonHeaterOff_action (ActionEvent e) {
      heaterComboBox.setSelectedIndex(0);
      jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR CLEAR");
      jeiCommand.execute("PUT " + EPICS.prefix + "ec:tempSet.A 6 ;put a valid set point");
      jeiCommand.execute("PUT " + EPICS.prefix + "ec:tempSet.B 0 ;heater off panic button");
      jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR START ;apply it...");
  } //end of jButtonHeater Off

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonColdhead
   *JPanelTemperature cold head button action performed
   *@param e not used
   */
  void jButtonColdhead_action (ActionEvent e) {
    String value = "";
    if (jButtonColdhead.getText().indexOf("On") != -1)
	value = "on";
    else
	value = "off";
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR CLEAR");
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:chPower.A "+value+" ;Coldhead button");
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR START ;apply it...");
  } //end of jButtonColdhead Off


//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonPressure
   *JPanelTemperature Pressure button action performed
   *@param e not used
   */
  void jButtonPressure_action (ActionEvent e) {
    String value = "";
    if (jButtonPressure.getText().indexOf("On") != -1)
	value = "on";
    else
	value = "off";
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR CLEAR");
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:vacPower.A "+value+" ;Pressure sensor button");
    jeiCommand.execute("PUT " + EPICS.prefix + "ec:apply.DIR START ;apply it...");
  } //end of jButtonPressure

//-------------------------------------------------------------------------------
 /**
  *Event Handler for jGraphLoadButton
  *JPanelTemperature Graph load button action performed
  *@param e not used
  */
 void jGraphLoadButton_action (ActionEvent e) {
    int counter =0;
    try {
      JFileChooser fc = new JFileChooser(new File(jei.data_path));
      ExtensionFilter t_graph_type = new ExtensionFilter (
		     "T-ReCS graph data files", new String[] {".td"});
      fc.setFileFilter (t_graph_type); // Initial filter setting
      fc.setDialogTitle("Load graph data from");
      fc.setSelectedFile(new File(td_filename));
      fc.setApproveButtonText("Load");
      int returnVal = fc.showOpenDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        String t_file_name = fc.getSelectedFile().getAbsolutePath();
      	if (!t_file_name.endsWith(".td")) t_file_name += ".td";
        TextFile inp = new TextFile(t_file_name,TextFile.IN);
        jGraphPanel.pollPeriod = Integer.parseInt(inp.nextUncommentedLine());
        counter = 0;
        jGraphPanel.size = 0;
        String line = "";
        for (int i=0; i<SensorParms.length; i++) {
	    jGraphPanel.xValues[i].clear();
	    jGraphPanel.yValues[i].clear();
	}
        while((line = inp.nextUncommentedLine()) != null) {
	  if (line.trim().equals("")) break;
          StringTokenizer stY = new StringTokenizer(line);
          for (int i=0; i<SensorParms.length; i++) {
            if (jGraphPanel.size > jGraphPanel.maxDataPoints) {
		jGraphPanel.yValues[i].removeFirst();
		jGraphPanel.xValues[i].removeLast();
            }
	    jGraphPanel.yValues[i].addLast( new Integer((int)(Double.parseDouble(stY.nextToken())*100)) );
	    jGraphPanel.xValues[i].addFirst( new Integer( jGraphPanel.pollPeriod*counter*100 ) );
          }
          counter++;
          jGraphPanel.size++;
        }
        inp.close();
        td_filename = t_file_name;
	repaint();
      }
    }
    catch (Exception ee) {
       //jeiFrameErrorLog.addError("JPanelTemperature","jGraphLoadButton_action()",ee.toString());
       jeiError.show("problem reading temperature data file : " + counter + " " +ee.toString());
    }
  } //end of jGraphLoadButton_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for JGraphParamButton
   *JPanelTemperature Graph parameters button action performed
   *@param e not used
   */
  void jGraphParamButton_action (ActionEvent e) {
    Point origin = getLocation();
    Dimension totalSize = getSize();
    dialogBox.graphParams[8].setText(jGraphPanel.pollPeriod + "");
    dialogBox.graphParams[9].setText(jGraphPanel.maxDataPoints + "");
    dialogBox.setLocation((int)(origin.getX() + totalSize.getWidth()/2),
                           (int)(origin.getY() + totalSize.getHeight()/2));
    dialogBox.setVisible(true);
    repaint();
  } //end of jGraphParamButton

//-------------------------------------------------------------------------------
  /**
   *Sounds the alarm
   *@param e not used
   */
  void alarm_action(ActionEvent e){
    SensorParms[1].soundAlarm();
  } //end of alarm_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonPID
   *JPanelTemperature PID button action performed
   *@param e not used
   */
  void jButtonPID_action() {
    if (p_Label.isShowing()){
      p_Label.setVisible(false);
      i_Label.setVisible(false);
      d_Label.setVisible(false);
      p_Text.setVisible(false);
      i_Text.setVisible(false);
      d_Text.setVisible(false);
      jButtonPID.setText("Show PID");
    }
    else {
      p_Label.setVisible(true);
      i_Label.setVisible(true);
      d_Label.setVisible(true);
      p_Text.setVisible(true);
      i_Text.setVisible(true);
      d_Text.setVisible(true);
      jButtonPID.setText("Hide  PID");
    }
    jPanelLeft.repaint();
    jPanelTempControl.repaint();
  } //end of jButtonPID_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for jGraphStartButton
   *JPanelTemperature start button action performed
   *@param e not used
   */
  void jGraphStartButton_action(ActionEvent e) {
    if (graphThread.isAlive()) {
      graphThread.keepRunning = false;
      jGraphStartButton.setText("Start Graph");
      pw.close();
    }
    else {
      try {
        JFileChooser fc = new JFileChooser(jei.data_path);
  //      fc.setSelectedFile("T-ReCS.tgd");
        ExtensionFilter t_graph_type = new ExtensionFilter (
          "T-ReCS graph data files", new String[] {".td"});
        fc.setFileFilter (t_graph_type); // Initial filter setting
        fc.setDialogTitle("Save graph data to");
        fc.setSelectedFile(new File(td_filename));
        fc.setApproveButtonText("Save");
        int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          graphThread = new GraphThread();
          String t_file_name = fc.getSelectedFile().getAbsolutePath();
          if (!t_file_name.endsWith(".td")) t_file_name += ".td";
          pw = new TextFile(t_file_name,TextFile.OUT);
          pw.println("; Temperature Graph Data file");
          pw.println(";");
          pw.println("; Poll Rate");
          pw.println(jGraphPanel.pollPeriod+"");
//          pw.println("; Max number of data points");
//          pw.println(jGraphPanel.maxDataPoints+"");
//          pw.println("; Actual number of data points");
//          pw.println(jGraphPanel.size+"");
//          pw.println(";");
//          pw.println("; Data points have been scaled by 100 to preserve precision");
//          pw.println(";");
          pw.println(";");
          td_filename = t_file_name;

          graphThread.start();
          jGraphStartButton.setText("Stop Graph");
        }
          /*for (int i=0; i<SensorParms.length; i++) {
            ListIterator lit = jGraphPanel.xValues[i].listIterator(0);
            String s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            pw.println(s);
            lit = jGraphPanel.yValues[i].listIterator(0);
            s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            pw.println(s);
          }
          pw.close();
        }   */
      }
      catch (Exception ee) {
        //jeiFrameErrorLog.addError("JPanelTemperature","jGraphStartButton_action()",ee.toString());
      }
    }
  } //end of jGraphStartButton_action

//===============================================================================
  /**
   *TBD
   */
  public class RateThread extends Thread {

    boolean keepRunning = true;
    int sleepamount = jGraphPanel.pollPeriod;
    double []oldValues = new double[SensorParms.length];

//-------------------------------------------------------------------------------
    /**
     *TBD
     */
    public void run() {
      for (int i=0; i<SensorParms.length; i++) oldValues[i] = 0.0;
      while (keepRunning) {
        for (int i=0; i<SensorParms.length; i++) {
          double currRate = (SensorParms[i].currentTemp - oldValues[i]) / sleepamount;
          double oldRate = 0.0;
          try {
            oldRate = Double.parseDouble(SensorParms[i].graphElement.rate.getText());
          }
          catch (Exception e){
            oldRate = 0.0;
          }
          SensorParms[i].graphElement.rate.setText(( (oldRate+currRate) / 2.0) + "");
          oldValues[i] = SensorParms[i].currentTemp;
        }// end of for loop
        try {
          this.sleep(sleepamount*1000);
        }
        catch (Exception e) {
          JOptionPane.showMessageDialog(null,e.toString(),"RateThread Error",JOptionPane.ERROR_MESSAGE);
        }
      }
    }//end of run
  }//end of class RateThread

//===============================================================================
  /**
   *TBD
   */
  public class GraphThread extends Thread {
    Timestamp timestamp;
    boolean keepRunning = true;
    int sleepamount = jGraphPanel.pollPeriod;
    int counter = 0;

//-------------------------------------------------------------------------------
    /**
     *Displays line on the graph when start graph is clicked
     *logs the graph data in the .tgd file
     *
     */
    public void run() {
      counter = 0;
      while( keepRunning && (counter<32768) )
        try {
	    if (jGraphPanel.size < jGraphPanel.maxDataPoints)
		jGraphPanel.size++;
	    for (int i=0; i<SensorParms.length; i++) {
		pw.print( SensorParms[i].currentTemp + " ");
		if (jGraphPanel.size == jGraphPanel.maxDataPoints && counter >= jGraphPanel.size) {
		    //jGraphPanel.xValues[i].removeLast();
		    jGraphPanel.yValues[i].removeFirst();
		}
		// Add x values in reverse order because of strip chart style graph
		// Add Integers since linkedlists can only hold objects, not primitives.
		// Scale by 100 in order to preserve precision
                if (jGraphPanel.size > counter)
		  jGraphPanel.xValues[i].addFirst( new Integer( sleepamount*counter*100 ) );
		jGraphPanel.yValues[i].addLast( new Integer((int)(SensorParms[i].currentTemp*100)) );
	    }
	    timestamp = new Timestamp((new Date()).getTime());
	    pw.println(" " + timestamp.toString());
	    repaint();
	    if( sleepamount <= 0 )
		keepRunning = false;
	    else
		this.sleep(sleepamount*1000);
	    counter++;
	    //System.out.println(jGraphPanel.size+"");
	}
      catch (Exception e) {
	  JOptionPane.showMessageDialog(null,e.toString(),"GraphThread Error",JOptionPane.ERROR_MESSAGE);
      }
    }
  } //end of class GraphThread

//-------------------------------------------------------------------------------
  /**
   *Reads the temperature parameter file
   *@param filename String: contains the name of the file name used
   */
  public void ReadFile(String filename) {

    TextFile in_file = new TextFile(jei.data_path + filename,TextFile.IN);

    try {
      String line;
      StringTokenizer st;

      line = in_file.nextUncommentedLine();
      int numSensors=0; int maxLabelSize = 0;
      try {  numSensors = Integer.parseInt(line);
             SensorParms = new JEIsensorParameters[numSensors]; }
      catch (Exception e) {jeiError.show("Bad number of sensors parameter"); }
      for (int i=0; i<numSensors; i++) {
        line = in_file.nextUncommentedLine();
        st = new StringTokenizer(line," ");
        // assume that all words are part of the sensor name
        // except the last three, which should be the epics record name for
	// this particular sensor and the Hot/Cold Threshold Values
        int numTokens = st.countTokens();
        String s = "";
        for (int j=0; j<numTokens-3; j++)
          s += st.nextToken() + " ";
        s = s.substring(0,s.length()-1); // strip off the last space
        if (s.length() > maxLabelSize) maxLabelSize = s.length();
	String recName = "";
	try { recName = st.nextToken(); }
	catch (Exception e) { jeiError.show("Couldn't get sensor epics name:" + e.toString()); }
        SensorParms[i] = new JEIsensorParameters (s,EPICS.prefix+recName);
        try { SensorParms[i].hotThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { jeiError.show("Bad Hot Threshold Value: "+e.toString()); }
        try { SensorParms[i].coldThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { jeiError.show("Bad Cold Threshold Value: "+e.toString()); }
      }
      // make label spacing uniform
      for (int i=0; i<numSensors; i++) {
        String s = SensorParms[i].label.getText();
        while (s.length() < maxLabelSize) s += " ";
        SensorParms[i].label.setText(s);
      }

      //graph paramters
      line = in_file.nextUncommentedLine();
      st = new StringTokenizer(line," ");
      // create the graph panel object
      jGraphPanel = new JGraphPanel();
      try {
        jGraphPanel.timeMin = Double.parseDouble(st.nextToken());
        jGraphPanel.timeMax = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMin = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMax = Double.parseDouble(st.nextToken());
        jGraphPanel.xTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.xLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.pollPeriod = Integer.parseInt(st.nextToken());
        jGraphPanel.maxDataPoints = Integer.parseInt(st.nextToken());
      } catch (Exception e) {jeiError.show("Bad graph Parameters"); }

      in_file.close();
    }
    catch (Exception e) {
      jeiError.show("Couldn't read temperature data file: "+e.toString());
    }
  } //end of ReadFile

//===============================================================================
  /**
   *Handles the Graph on the JPanelTemperature
   */
  class JGraphPanel extends JPanel {
    // array of linked lists to store data points for the sensors.
    LinkedList [] xValues = new LinkedList[SensorParms.length];
    LinkedList [] yValues = new LinkedList[SensorParms.length];
    //number of points currently stored.
    int size;
    //maximum number of points
    int maxDataPoints;
    int panelWidth, panelHeight, panelMinX, panelMinY;
    double timeMin, timeMax, tempMin, tempMax;
    double xTickDelta, xLabelDelta, yTickDelta, yLabelDelta;
    int pollPeriod;

//-------------------------------------------------------------------------------
    /**
     *Constructor Function for JGraphPanel
     */
    JGraphPanel () {
      size = 0; maxDataPoints = 2000;
      xTickDelta = 60; yTickDelta = 10;
      xLabelDelta = 120; yLabelDelta = 20;
      panelMinX = 10; panelMinY = 10;
      tempMin = 0; tempMax = 100;
      timeMin = 0; timeMax = 600;
      pollPeriod = 5;
      for (int i=0; i<SensorParms.length; i++) {
        xValues[i] = new LinkedList();
        yValues[i] = new LinkedList();
      }
    } //end of JGraphPanel constructor fn

//-------------------------------------------------------------------------------
      /**
       *Clears the xValues array and the yValues array for the graph data
       */
      public void eraseGraphData () {
         size = 0;
         for (int i=0; i<SensorParms.length; i++) {
           xValues[i].clear();
           yValues[i].clear();
         }
         jeiFrame.jPanelTemperature.repaint();
      } //end of eraseGraphData

//-------------------------------------------------------------------------------
      /**
       *reverses ??
       *@param arrayit TBD
       */
      public int[] reverse (int [] arrayit) {
        int [] temper = new int[arrayit.length];
        int k = arrayit.length-1;
        for (int i=0; i<arrayit.length; i++) {
            temper[k--] = arrayit[i];
        }
        return temper;
      } //end of reverse

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform2(double x, double y)
      {
         final double scr_org_x = 0;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = (panelWidth) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY) / (tempMax-tempMin);
         y -= tempMin;// x += timeMin;
         Point scr_loc = new Point();
         scr_loc.x = (int)(scr_org_x + x * scr_x_per_x);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform2

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param g Graphics TBD
       */
      public synchronized void paint(Graphics g) {
        panelWidth = (int)this.getWidth() - 38;
        panelHeight = (int)this.getHeight() - 18;
        g.setFont(new Font(g.getFont().getName(),g.getFont().getStyle(),10));
        g.drawLine( panelMinX, panelHeight, panelWidth, panelHeight );
        g.drawLine( panelWidth, panelMinY, panelWidth, panelHeight );

        double d = timeMin;
        while (d <= timeMax) {
	    Point p1 = xform(d,1); Point p2 = xform(d,-1);
	    if( jCheckBoxGraphGrid.isSelected() ) {
		g.setColor( Color.gray );
		g.drawLine( p1.x, panelHeight, p2.x, 0 );
	    }
	    g.setColor( Color.black );
	    g.drawLine( p1.x, panelHeight+1, p2.x, panelHeight-1 );
	    d+=xTickDelta;
        }

        d = timeMin;
        while (d <=timeMax) {
          Point p1 = xform(d,-15);
	  g.drawString( String.valueOf(-d), p1.x-9, panelHeight+15 );
          d+=xLabelDelta;
        }

        double y = tempMin;
        while (y <= tempMax) {
	    if( jCheckBoxGraphGrid.isSelected() ) {
		g.setColor( Color.gray );
		g.drawLine( panelMinX, xform2(panelMinX+1,y).y, panelWidth, xform2(panelMinX-1,y).y );
	    }
	    g.setColor( Color.black );
	    g.drawLine( panelWidth-1, xform2(0,y).y, panelWidth+1, xform2(0,y).y );
	    y+=yTickDelta;
        }

        y = tempMin;
        while (y <= tempMax) {
          g.drawString( String.valueOf(y), panelWidth+6, xform2(0,y).y+3 );
          y+=yLabelDelta;
        }

        int []tempx = new int[size];
        int []tempy = new int[size];
	Point srcLoc;
	// Compute screen Locations and use polylines to draw the graphs.

        for (int i=0; i<SensorParms.length; i++) {
          if (SensorParms[i].graphElement.showme.isSelected()) {
             for (int j=0; j<size; j++){
                srcLoc = xform( ((Integer)xValues[i].get(j)).intValue()/100.0,
				((Integer)yValues[i].get(j)).intValue()/100.0 );
                tempx[j] = srcLoc.x;
                tempy[j] = srcLoc.y;
             }
	     g.setColor( SensorParms[i].statusColor.getBackground() );
             g.drawPolyline(tempx,tempy,size);
          }
        }

	g.setColor( Color.black );
      } //end of paint
   } //end of class JGraphPanel

//===============================================================================
   /**
    *TBD
    */
   public class GraphElement {
     JTextField rate = new JTextField ("0",5);
     JLabel units = new JLabel ("K/min");
     JCheckBox showme = new JCheckBox ();

//-------------------------------------------------------------------------------
     /**
      *GraphElement constructor
      */
     GraphElement () {
       showme.setSelected(false);
       rate.setEditable(false);
       rate.setBackground(new Color(175,175,175));
       showme.addActionListener(
           new java.awt.event.ActionListener() {
              public void actionPerformed(ActionEvent e) {
                 repaint();
           }
       });

     } //end of GraphElement

   } //end of class GraphElement

//===============================================================================
  /**
   *Object storing and handling temperature sensor parameters
   */
  public class JEIsensorParameters
  {
    EPICSTextField temp;
    JLabel label = new JLabel();
    JLabel units = new JLabel("K");
    JTextField statusColor = new JTextField("",2);
    GraphElement graphElement = new GraphElement();
    double currentTemp;
    double hotThreshold;
    double coldThreshold;
    String channel;
    int model;

//-------------------------------------------------------------------------------
      /**
       *Constructor for JEIsensorParameters
       */
      JEIsensorParameters( String name, String rec ) {
         label.setText(name);
         temp = new EPICSTextField( rec, "; temperature sensor" );
         statusColor.setBackground(new Color(255,0,0));
         statusColor.setEditable(false);
         temp.setEditable(false);
         temp.setBackground(new Color(175,175,175));

         temp.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
              setTemperature (e);
            }
            public void removeUpdate(DocumentEvent e) {
              //setTemperature (e);
            }
            public void changedUpdate(DocumentEvent e) {
              //setTemperature (e);
            }
         });

         currentTemp = 0;
         hotThreshold = 0;
         coldThreshold = 0;
         channel = "0";
         model = 0;
      } //end of JEIsensorParameters

//-------------------------------------------------------------------------------
      /**
       *Sets the temperature
       *@param e not used
       */
      public void setTemperature(DocumentEvent e) {
        String s = temp.getText();
	if( s.trim().length() > 0 ) {
	    try { currentTemp = Double.parseDouble(s); }
	    catch (Exception ex) { } //jeiError.show("Bad Temperature Reading"); }
	    checkThreshold();
	}
      } //end of setTemperature

//-------------------------------------------------------------------------------
      /**
       *TBD
       */
      public void checkThreshold()
      {
	  if (currentTemp > hotThreshold)
	      statusColor.setBackground( Color.red );
	  else
	      if (currentTemp < coldThreshold)
		  statusColor.setBackground( Color.blue );
	      else statusColor.setBackground( Color.yellow );
      } //end of checkThreshold

//-------------------------------------------------------------------------------
      /**
       *Sounds the Alarm
       */
      public void soundAlarm () {
        URL codeBase;
        try {
          codeBase = new URL("file:" + System.getProperty("user.dir") + "/alert.au");
          AudioClip aa = Applet.newAudioClip(codeBase);
          aa.play();
        }
        catch (MalformedURLException e) {
          System.err.println(e.getMessage());
        }
      } //end of soundAlarm
   } //end of class JEITemperaturParameters

//===============================================================================
   /**
    *TBD
    */
  public class JDialogGraphParameters extends JDialog {
    JPanel labelpanel = new JPanel();
    JPanel textpanel = new JPanel();
    JPanel buttonpanel = new JPanel();
    JButton buttonOK = new JButton ("OK");
    JButton buttonCancel = new JButton ("Cancel");
    JTextField [] graphParams = new JTextField[10];
    double timeMin,timeMax,tempMin,tempMax;
    double xTickDelta,xLabelDelta,yTickDelta,yLabelDelta;

//-------------------------------------------------------------------------------
      /**
       *Default Constructor
       */
      JDialogGraphParameters () {
        super();
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        this.setModal(true);
        try  {
          jbInit();
        }
        catch(Exception e) {
          e.printStackTrace();
        }
      } //end of JDialogGraphParameters

//-------------------------------------------------------------------------------
      /**
       *Component Initialization
       */
      private void jbInit () {
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonOK_action(e);
          }
        });
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonCancel_action(e);
          }
        });
        labelpanel.setLayout(new GridLayout(0,1,0,10));
        textpanel.setLayout(new GridLayout(0,1,0,10));
        buttonpanel.setLayout(new FlowLayout());
        this.getContentPane().setLayout (new BorderLayout());
        labelpanel.add(new JLabel());
        labelpanel.add(new JLabel("Min Time"));
        labelpanel.add(new JLabel("Max Time"));
        labelpanel.add(new JLabel("Min Temp"));
        labelpanel.add(new JLabel("Max Temp"));
        labelpanel.add(new JLabel("x Tick Delta"));
        labelpanel.add(new JLabel("x Label Delta"));
        labelpanel.add(new JLabel("y Tick Delta"));
        labelpanel.add(new JLabel("y Label Delta"));
        labelpanel.add(new JLabel("Graph Poll Period"));
        labelpanel.add(new JLabel("Max Data Points"));
        textpanel.add(new JLabel());
        for (int i=0; i<10; i++) {
           graphParams[i] = new JTextField("0.0",5);
           textpanel.add(graphParams[i]);
        }
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
        graphParams[9].setText(jGraphPanel.maxDataPoints + "");
        buttonpanel.add(buttonOK);
        buttonpanel.add(buttonCancel);
        this.getContentPane().add(labelpanel,BorderLayout.WEST);
        this.getContentPane().add(textpanel,BorderLayout.EAST);
        this.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
        this.setTitle("Graph Parameters");
        buttonOK.grabFocus();
        this.setSize(new Dimension(200,350));
      } //end of jbInit

//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonOK
       *JDialogGraphParameters ok button action performed
       *@param e not used
       */
      void buttonOK_action(ActionEvent e) {
        int oldRate = jGraphPanel.pollPeriod;
        int newRate = Integer.parseInt(graphParams[8].getText());
        int oldMaxPoints = jGraphPanel.maxDataPoints;
        int newMaxPoints = Integer.parseInt(graphParams[9].getText());
        int i = -1;
        if (oldRate != newRate || oldMaxPoints != newMaxPoints) {
          i = JOptionPane.showConfirmDialog(null,"These changes will erase graph data",
					    "Warning",JOptionPane.OK_CANCEL_OPTION);
          if (i == JOptionPane.CANCEL_OPTION) return;
          else jGraphPanel.eraseGraphData();
        }
        jGraphPanel.timeMin = Double.parseDouble(graphParams[0].getText());
        jGraphPanel.timeMax = Double.parseDouble(graphParams[1].getText());
        jGraphPanel.tempMin = Double.parseDouble(graphParams[2].getText());
        jGraphPanel.tempMax = Double.parseDouble(graphParams[3].getText());
        jGraphPanel.xTickDelta = Double.parseDouble(graphParams[4].getText());
        jGraphPanel.xLabelDelta = Double.parseDouble(graphParams[5].getText());
        jGraphPanel.yTickDelta = Double.parseDouble(graphParams[6].getText());
        jGraphPanel.yLabelDelta = Double.parseDouble(graphParams[7].getText());
        jGraphPanel.pollPeriod = Integer.parseInt(graphParams[8].getText());
        jGraphPanel.maxDataPoints = Integer.parseInt(graphParams[9].getText());
        this.setVisible(false);
      } //end of buttonOK_action

//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonCancel
       *JDialogGraphParameters cancel button action performed
       *@param e not used
       */
      void buttonCancel_action(ActionEvent e) {
        this.setVisible(false);
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
        graphParams[9].setText(jGraphPanel.maxDataPoints + "");
      } //end of buttonCancel_action

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param e WindowEvent: TBD
       */
      protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
          this.dispose();
        }
        super.processWindowEvent(e);
      } //end of processWindowEvent
   } //end of class JDialogGraphParameters
} //end of class JPanelTemperature





