package ufjei;

import jca.*;
import jca.event.*;
import jca.dbr.*;
import java.awt.*;
import java.util.StringTokenizer;

//===============================================================================
/**
 * Handles the status of the Motors given by EPICS
 */
public class EPICSMotorStatus implements MonitorListener{

  public static double currPos[] = new double[10];
  private String locMonitors[];
  private String statMonitors[];
  private boolean errorFlag;

//-------------------------------------------------------------------------------
  /**
   * Default constructor
   */
  public EPICSMotorStatus() {
    statMonitors = new String[jeiMotorParameters.getNumberMotors()];
    locMonitors = new String[jeiMotorParameters.getNumberMotors()];
    String pv_name = "";
    errorFlag = false;
    for (int i=0; i<jeiMotorParameters.getNumberMotors(); i++) {
      try {
        // set up monitors to listen to the motor CAR records
        pv_name = jeiMotorParameters.getPVmotorPrefix(i+1) + "motorC.VAL";
        PV pv = new PV(pv_name);
        Ca.pendIO(jeiFrame.TIMEOUT);
        statMonitors[i] = pv_name;
        pv.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
        Ca.pendIO(jeiFrame.TIMEOUT);

        // set up monitors to listen to the motor positions
        pv_name = jeiMotorParameters.getPVmotorPrefix(i+1) + "motorG.VALC";
        pv = new PV(pv_name);
        Ca.pendIO(jeiFrame.TIMEOUT);
        locMonitors[i] = pv_name;
        pv.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
        Ca.pendIO(jeiFrame.TIMEOUT);
      }
      catch (Exception e) {
        jeiError.show("error setting up monitor " + pv_name + " " + e.toString());
      }
    }
  }// end of EPICSMotorStatus

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param me MonitorEvent: TBD
   */
  public void monitorChanged(MonitorEvent me) {
    int i = -1;
    try {
      for (i = 0; i < jeiMotorParameters.getNumberMotors(); i++) {
        if (me.pv().name().equals(statMonitors[i])) {
          statMonitorChanged(me,i);
          break;
        }
        else if (me.pv().name().equals(locMonitors[i])) {
          locMonitorChanged(me,i);
          break;
        }
      }
    }
    catch (Exception e) {
      jeiError.show("error processing monitorChanged. i = " + i + "; "+ e.toString() + "; " + me.pv().name());
    }
  }// end of monitorChanged

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param me MonitorEvent: TBD
   *@param i int: TBD
   */
  public void statMonitorChanged(MonitorEvent me, int i) {
    final String BUSY = "BUSY"; final String IDLE = "IDLE"; final String ERROR = "ERR";
    DBR_DBString val = new DBR_DBString();
    String statusStr = "";
    try {
      me.pv().get(val);
      Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception e) {
      jeiError.show("error in EPICSMotorStatus: " +me.pv().name()+ " " + e.toString());
    }
    if (val.valueAt(0) == null) return;
    if (val.valueAt(0).equals(IDLE)) {
	if( jeiFrame.jPanelHighLevel.jPanelMotorLock.b[i].isSelected() ) {
	    jeiFrame.jPanelLowLevel.jPanelMoveLow.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelHomeLow.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelOrigin.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelHome.b[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelMove.b[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[i].setEnabled(true);
	}
	errorFlag = false;
	jeiFrame.jPanelLowLevel.jPanelMotorLock.b[i].setEnabled(true);
	jeiFrame.jPanelHighLevel.jPanelMotorLock.b[i].setEnabled(true);
    }
    else if (val.valueAt(0).equals(BUSY)){
      jeiFrame.jPanelLowLevel.jPanelMotorLock.b[i].setEnabled(false);
      jeiFrame.jPanelLowLevel.jPanelMoveLow.b[i].setEnabled(false);
      jeiFrame.jPanelLowLevel.jPanelHomeLow.b[i].setEnabled(false);
      jeiFrame.jPanelLowLevel.jPanelOrigin.b[i].setEnabled(false);
      jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[i].setEnabled(false);
      jeiFrame.jPanelHighLevel.jPanelMotorLock.b[i].setEnabled(false);
      jeiFrame.jPanelHighLevel.jPanelHome.b[i].setEnabled(false);
      jeiFrame.jPanelHighLevel.jPanelMove.b[i].setEnabled(false);
      jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[i].setEnabled(false);
      errorFlag = false;
    }
    else if (val.valueAt(0).equals(ERROR)) {
	statusStr = "Unknown";
	if( jeiFrame.jPanelHighLevel.jPanelMotorLock.b[i].isSelected() ) {
	    jeiFrame.jPanelLowLevel.jPanelMoveLow.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelHomeLow.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelOrigin.b[i].setEnabled(true);
	    jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelHome.b[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelMove.b[i].setEnabled(true);
	    jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[i].setEnabled(true);
	}
	jeiFrame.jPanelHighLevel.jPanelStatus.Status[i].setText(statusStr);
	jeiFrame.jPanelLowLevel.jPanelStatus.Status[i].setText(statusStr);
	errorFlag = true;
	jeiFrame.jPanelLowLevel.jPanelMotorLock.b[i].setEnabled(true);
	jeiFrame.jPanelHighLevel.jPanelMotorLock.b[i].setEnabled(true);
    }
  } //end of statMonitorChanged

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param me MonitorEvent: TBD
   *@param i int: TBD
   */
  public void locMonitorChanged(MonitorEvent me, int i) {
    DBR_DBString val = new DBR_DBString();
    try {
      me.pv().get(val);
      Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception e) {
      jeiFrame.jPanelHighLevel.jPanelStatus.Status[i].setBackground(Color.red);
      jeiFrame.jPanelLowLevel.jPanelStatus.Status[i].setBackground(Color.red);
    }
    currPos[i] = -999999; // unknown
    String statusStr = "";
    if (errorFlag){
      statusStr = "Error"; // or get the ERROR message from the CAR
      String pv_name = "";
      StringTokenizer st = new StringTokenizer(me.pv().name(),":");
      pv_name += st.nextToken() +":"+ st.nextToken() +":"+ st.nextToken() +":"+ "motorC.OMSS";
      DBR_DBString errVal = new DBR_DBString();
      try {
        PV pv = new PV(pv_name);
        Ca.pendIO(jeiFrame.TIMEOUT);
        pv.get(errVal);
        Ca.pendIO(jeiFrame.TIMEOUT);
        statusStr += ": "+errVal.valueAt(0);
      }
      catch (Exception ee) {
	  jeiError.show("Cannot read Error Message from " + pv_name + " : " + ee.toString());
      }
      //motorC.VAL
    }
    else if (val.valueAt(0).trim().equals("")) {
      statusStr = "Unknown (position record is empty)";
    }
    else {
      try {
        currPos[i] = (int) Double.parseDouble(val.valueAt(0));
        statusStr = jeiFrame.jPanelHighLevel.jPanelStatus.getStatus(i + 1, currPos[i]);
      }
      catch (Exception ee) {
        jeiError.show("parseInt error in EPICSMotorStatus: " + me.pv().name() + " " + ee.toString());
        statusStr = "ERROR parsing";
      }
    }
    jeiFrame.jPanelHighLevel.jPanelStatus.Status[i].setText(statusStr);
    jeiFrame.jPanelHighLevel.jPanelStatus.Status[i].setBackground(jeiFrame.jPanelTemperature.getBackground());

    jeiFrame.jPanelLowLevel.jPanelStatus.Status[i].setText(statusStr);
    jeiFrame.jPanelLowLevel.jPanelStatus.Status[i].setBackground(jeiFrame.jPanelTemperature.getBackground());
    if (jeiFrame.Opathframe != null) {
      jeiFrame.Opathframe.update_motor_status(i + 1);
    }
  }// end of locMonitorChanged

} //end of class EPICSMotorStatus
