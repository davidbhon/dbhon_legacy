package ufjei;
   import java.awt.event.*;
   import javax.swing.*;
   import java.awt.*;
   import java.net.*;
   import java.applet.*;
   import java.io.*;
   import java.util.*;

//===============================================================================
/**
 *Low Level tabbed pane
 */
public class JPanelMotorLowLevel extends JPanel{
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorLowLevel.java,v 0.3 2002/12/04 23:04:31 varosi beta $";

  public static JPanelMotorNames jPanelMotorNames ;
  public static JPanelStepsToMove jPanelStepsToMove  ;
  public static JPanelMoveLow jPanelMoveLow;
  public static JPanelHomeLow jPanelHomeLow ;
  public static JPanelMotorOrigin jPanelOrigin ;
  public static JPanelMotorAbort jPanelAbort  ;
  public static JPanelMotorStatus jPanelStatus  ;
  public static JPanelMotorLock jPanelMotorLock ;

  public static JPanelMultiMotor jPanelMultiMotor ;
  RatioLayout layout = new RatioLayout();
  GridLayout buttonLayout = new GridLayout();
  JPanel jPanelButtons = new JPanel();
  JButton jButtonQueryPos = new JButton("Query Status");
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "cc:heartbeat",
					     EPICS.prefix + "cc:HeartBeat:");
//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public JPanelMotorLowLevel(jeiMotorParameters jeiMotorParameters) {
    this.setLayout(layout);
    buttonLayout.setColumns(0);
    buttonLayout.setRows(1);
    jPanelButtons.setLayout(buttonLayout);
    jPanelMotorNames = new JPanelMotorNames(jeiMotorParameters);
    jPanelStepsToMove = new JPanelStepsToMove(jeiMotorParameters);
    jPanelMoveLow = new JPanelMoveLow(jeiMotorParameters);
    jPanelHomeLow = new JPanelHomeLow(jeiMotorParameters);
    jPanelOrigin = new JPanelMotorOrigin(jeiMotorParameters);
    jPanelAbort = new JPanelMotorAbort(jeiMotorParameters);
    jPanelStatus = new JPanelMotorStatus(jeiMotorParameters);
    jPanelMotorLock = new JPanelMotorLock(jeiMotorParameters);
    jPanelButtons.add(jPanelMoveLow);
    jPanelButtons.add(jPanelHomeLow);
    jPanelButtons.add(jPanelAbort);
    jPanelButtons.add(jPanelOrigin);
    jPanelMultiMotor = new JPanelMultiMotor(jeiMotorParameters);
    this.add("0.0,0.02;0.015,0.66", jPanelMotorLock);
    this.add("0.02,0.02;0.15,0.66", jPanelMotorNames);
    this.add("0.17,0.02;0.14,0.66", jPanelStepsToMove);
    this.add("0.32,0.02;0.34,0.66", jPanelButtons);
    this.add("0.66,0.02;0.33,0.66", jPanelStatus);
    this.add("0.01,0.80;0.60,0.15", jPanelMultiMotor);
    HeartBeatLabel.setHorizontalAlignment(JLabel.LEFT);
    this.add("0.60,0.70;0.35,0.10", HeartBeatLabel);
    this.add("0.85,0.70;0.14,0.10", jButtonQueryPos);

    jButtonQueryPos.addActionListener(new ActionListener() {
       public void actionPerformed(ActionEvent ae) {
	   jPanelStatus.queryMotorPositions();
       }
    });
  } //end of JPanelMotorLowLevel
} //end of class JPanelMotorLowLevel

//===============================================================================
/**
 *MultiMotor panel of components for handling multiple motors
 */
class JPanelMultiMotor extends JPanel implements ActionListener {
  public static JTextField jTextField = new JTextField(10);
  public static JButton jButtonMove = new JButton("Move");
  public static JButton jButtonHome = new JButton("Home");
  public static JButton jButtonAbort = new JButton("Abort");
  public static JButton jButtonSelectAll = new JButton("Chk All");
  public static JButton jButtonSelectNone = new JButton("Chk None");
  public static JCheckBox jCheckBox [];
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "cc:heartbeat",
					     EPICS.prefix + "cc:HeartBeat:");
//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMultiMotor(jeiMotorParameters mot_param) {
    this.setLayout(new BorderLayout());
    jCheckBox = new JCheckBox[mot_param.getNumberMotors()];
    JPanel topPanel = new JPanel();
    JPanel botPanel = new JPanel();
    for (int i=0; i<mot_param.getNumberMotors(); i++) {
      jCheckBox[i] = new JCheckBox((i+1)+"");
      botPanel.add(jCheckBox[i]);
    }
    botPanel.add(jButtonSelectAll);
    jButtonSelectAll.addActionListener(this);
    jButtonSelectAll.setActionCommand("selectall");
    botPanel.add(jButtonSelectNone);
    jButtonSelectNone.addActionListener(this);
    jButtonSelectNone.setActionCommand("selectnone");
    topPanel.add(new JLabel("Multi Motor -----  Steps To Move: "));
    topPanel.add(jTextField);
    topPanel.add(jButtonMove);
    jButtonMove.addActionListener(this);
    jButtonMove.setActionCommand("move");
    topPanel.add(jButtonHome);
    jButtonHome.addActionListener(this);
    jButtonHome.setActionCommand("home");
    topPanel.add(jButtonAbort);
    jButtonAbort.addActionListener(this);
    jButtonAbort.setActionCommand("abort");
    this.add(topPanel,BorderLayout.NORTH);
    this.add(botPanel,BorderLayout.SOUTH);
  } // end of JPanelMultiMotor constructor

//-------------------------------------------------------------------------------
  /**
   *TBD
   *@param ae ActionEvent: TBD
   */
  public void actionPerformed(ActionEvent ae) {
    jeiCmd command = new jeiCmd();
    String theCmd = ae.getActionCommand();
    if (theCmd.equals("selectall")) {
      for (int i=0; i<jCheckBox.length; i++)
          jCheckBox[i].setSelected(true);
    }
    else if (theCmd.equals("selectnone")) {
        for (int i=0; i<jCheckBox.length; i++)
            jCheckBox[i].setSelected(false);
    }
    else if (theCmd.equals("move")) {
      for (int i=0; i<jCheckBox.length; i++)
          if (jCheckBox[i].isSelected()) {
	      //      jeiFrame.jPanelParameters.putParams(i+1);
            String pvName = jeiMotorParameters.getPVmotorPrefix(i+1) + "steps.A";
            String cmd = new String("PUT " + pvName + " " + jTextField.getText() + " ;steps to move") ;
            command.execute(cmd);
            pvName = jeiMotorParameters.getPVmotorPrefix(i+1) + "motorApply.DIR";
            cmd = new String("PUT " + pvName + " 3 ;start low level move") ;
            command.execute(cmd);
          }
    }
    else if (theCmd.equals("home")) {
      for (int i=0; i<jCheckBox.length; i++)
          if (jCheckBox[i].isSelected()) {
            int mot_num = i+1;
	    //	    jeiFrame.jPanelParameters.putParams(mot_num);
            String pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "datum.A";
            String cmd = new String("PUT " + pvName + " 1" + " ;mark datum") ;
            command.execute(cmd);
            pvName = jeiMotorParameters.getPVmotorPrefix(mot_num)+"motorApply.DIR";
            cmd = "PUT " + pvName + " 3" + " ;start datum move";
            command.execute(cmd);
          }
    }
    else if (theCmd.equals("abort")) {
      for (int i=0; i<jCheckBox.length; i++)
          if (jCheckBox[i].isSelected()) {
            int mot_num = i+1;
            String pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "abort.A";
            String cmd = "PUT " + pv_name + " ABORT ;abort motor movement";
            command.execute(cmd);
            pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "motorApply.DIR";
            cmd = "PUT " + pv_name + " 3 ;apply the abort";
            command.execute(cmd);
          }
    }
  } // end of actionPerformed
} // end of class JPanelMultiMotor

//===============================================================================
/**
 * Array of steps to move text fields
 */
class JPanelStepsToMove extends JPanel {
  public static JTextField stepsToMove[] ;

//-------------------------------------------------------------------------------
  /**
   * Default constructor
   */
  public JPanelStepsToMove(jeiMotorParameters mot_param) {
    stepsToMove = new JTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel("Steps to Move"), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       stepsToMove[i] = new JTextField();
       stepsToMove[i].setText("0");
       add( stepsToMove[i] );
    }
  } //end of JPanelStepsToMove

} //end of class JPanelStepsToMove

//===============================================================================
/**
 * Array of move low buttons
 */
class JPanelMoveLow extends JPanel implements ActionListener {
  public JButton b[] ;

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMoveLow(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel(" "), null);

    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton ("Move");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       b[i].setDefaultCapable(false);
       add( b[i] );
    }
  } //end of JPanelMoveLow

//-------------------------------------------------------------------------------
  /**
   * TBD
   *@param e ActionEvent: TBD
   */
  public void actionPerformed(ActionEvent e) {
    String mot_num_str = e.getActionCommand();
    int midx = Integer.parseInt( mot_num_str );
    int mot_num = midx + 1;
    String stepsToMove = JPanelStepsToMove.stepsToMove[midx].getText();
    if( Integer.parseInt( stepsToMove ) != 0 ) {
	//even though EPICSMotorStatus class will lock these (and unlock when not BUSY),
	// lock them now first before the user can press another button:
	jeiFrame.jPanelLowLevel.jPanelMotorLock.b[midx].setEnabled(false);
	jeiFrame.jPanelLowLevel.jPanelMoveLow.b[midx].setEnabled(false);
	jeiFrame.jPanelLowLevel.jPanelHomeLow.b[midx].setEnabled(false);
	jeiFrame.jPanelLowLevel.jPanelOrigin.b[midx].setEnabled(false);
	jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[midx].setEnabled(false);
	jeiFrame.jPanelHighLevel.jPanelMotorLock.b[midx].setEnabled(false);
	jeiFrame.jPanelHighLevel.jPanelHome.b[midx].setEnabled(false);
	jeiFrame.jPanelHighLevel.jPanelMove.b[midx].setEnabled(false);
	jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[midx].setEnabled(false);
    }
    //    jeiFrame.jPanelParameters.putParams(mot_num);
    // put the steps to move
    String pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "steps.A";
    String cmd = "PUT " + pvName + " " + stepsToMove + " ;steps to move";
    jeiCmd command = new jeiCmd();
    command.execute(cmd);
    pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "motorApply.DIR";
    cmd = "PUT " + pvName + " 3 ;start low level move";
    command.execute(cmd);
  } //end of actionPerformed

} //end of class JPanelMoveLow

//===============================================================================
/**
 *Array of home low buttons
 */
class JPanelHomeLow extends JPanel implements ActionListener {
  public JButton b[] ;

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelHomeLow(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton ("Datum");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       b[i].setDefaultCapable(false);
       add (b[i]);
    }
  } //end of JPanelHomeLow

//-------------------------------------------------------------------------------
  /**
   * TBD
   *@param e ActionEvent: TBD
   */
  public void actionPerformed(ActionEvent e) {
   // need to add datum speed and datum direction (??)
    String mot_num_str = e.getActionCommand();
    int midx = Integer.parseInt(mot_num_str);
    int mot_num = midx+1;
    //even though EPICSMotorStatus class will lock these (and unlock when not BUSY),
    // lock them now first before the user can press another button:
    jeiFrame.jPanelLowLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelMoveLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelHomeLow.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelOrigin.b[midx].setEnabled(false);
    jeiFrame.jPanelLowLevel.jPanelStepsToMove.stepsToMove[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMotorLock.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelHome.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelMove.b[midx].setEnabled(false);
    jeiFrame.jPanelHighLevel.jPanelNamedLocation.NamedLocation[midx].setEnabled(false);
    //    jeiFrame.jPanelParameters.putParams(mot_num);
    String pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "datum.A";
    String cmd = "PUT " + pvName + " 1" + " ;mark datum";
    jeiCmd command = new jeiCmd();
    command.execute(cmd);
    pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "motorApply.DIR";
    cmd = "PUT " + pvName + " 3" + " ;start datum move";
    command.execute(cmd);
  } //end of actionPerformed

} //end of class JPanelHomeLow

//===============================================================================
/**
 *Array of low level origin buttons
 */
class JPanelMotorOrigin extends JPanel implements ActionListener {
  public JButton b[] ;

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMotorOrigin(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton("Origin");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       b[i].setDefaultCapable(false);
       add (b[i]);
    }
  } //end of JPanelMotorOrigin

//-------------------------------------------------------------------------------
  /**
   * TBD
   *@param e ActionEvent: TBD
   */
  public void actionPerformed(ActionEvent e) {
   // need to add datum speed and datum direction (??)
    String mot_num_str = e.getActionCommand();
    int mot_num = Integer.parseInt(mot_num_str)+1;
    String pvName = jeiMotorParameters.getPVmotorPrefix(mot_num) + "origin.A";
    String cmd = "PUT " + pvName + " 1" + " ;origin";
    jeiCmd command = new jeiCmd();
    command.execute(cmd);
    pvName = jeiMotorParameters.getPVmotorPrefix(mot_num)+"motorApply.DIR";
    cmd = "PUT " + pvName + " 3" + " ;start Origin";
    command.execute(cmd);
  } //end of actionPerformed

} //end of class JPanelMotorOrigin
