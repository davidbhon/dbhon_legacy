#if !defined(__ufg354vacd_cc__)
#define __ufg354vacd_cc__ "$Name:  $ $Id: ufg354vacd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufg354vacd_cc__;

#include "UFGemGranPhil354Agent.h"

int main(int argc, char** argv) {
  try {
    UFGemGranPhil354Agent::main(argc, argv);
  }
  catch( std::exception& e ) {
    clog<<"ufg354vacd> exception in stdlib: "<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufg354vacd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufg354vacd_cc__
