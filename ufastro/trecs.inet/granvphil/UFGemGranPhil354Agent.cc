#if !defined(__UFGemGranPhil354Agent_cc__)
#define __UFGemGranPhil354Agent_cc__ "$Name:  $ $Id: UFGemGranPhil354Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemGranPhil354Agent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemGranPhil354Agent.h"
#include "UFGP354Config.h"

string UFGemGranPhil354Agent::_currentval;

// helper for ctors:
void UFGemGranPhil354Agent::_setDefaults(const string& instrum) {
  UFDeviceAgent::_setDefaults(instrum);
  if( instrum != "" && instrum != "false" ) {
    _epics = instrum;
    _confGenSub = _epics + ":ec:configG";
    _heart = _epics + ":ec:gp354Heartbeat";
    _statRec = _epics + ":ec:pressMonG.J";
  }
  else {
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
}

// ctors:
UFGemGranPhil354Agent::UFGemGranPhil354Agent(int argc,
					     char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFGemGranPhil354Agent::UFGemGranPhil354Agent(const string& name,
					     int argc,
					     char** argv) : UFGemDeviceAgent(name, argc, argv){
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

// static funcs:
int UFGemGranPhil354Agent::main(int argc, char** argv) {
  UFGemGranPhil354Agent ufs("UFGemGranPhil354Agent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemGranPhil354Agent::options(string& servlist) {
  _config = new UFGP354Config(); // needs to be proper device subclass (GP345)
  // g-p defaults for trecs:
  _config->_tsport = 7004;
  _config->_tshost = "ufannex"; // "192.168.111.101";

  int port = UFDeviceAgent::options(servlist);

  _setDefaults("false");

  // _epics may have been reset by cmd line:
  string arg = findArg("-noepics"); // epics host/db name
  if( arg == "true"  ) {
    _setDefaults("false");
  }

  arg = findArg("-epics"); // epics db name
  if( arg != "false" &&  arg != "true" ) {
    _setDefaults(arg);
  }

  arg = findArg("-sim"); // sim mode
  if( arg != "false" ) {
    _sim = true;
  }

  arg = findArg("-heart"); // override heartbeat channel name
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf"); // override config channel name
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());
  else 
    _config->_tsport = 7004;

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    UFTermServ::_verbose = _verbose;
    UFSocket::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52008;
  }
  if( _verbose ) {
    clog<<"UFGemGranPhil354Agent::options> _epics: "<< _epics<<endl;
    clog<<"UFGemGranPhil354Agent::options> set port to GemGranPhil354 port "<<port<<endl;
  }
  return port;
}

// override base class startup here to avoid connect to term. serv
// if no motors are specified, assume simulation is desired,
// then do server listen stuff
void UFGemGranPhil354Agent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemGranPhil354Agent::startup> established signal handler."<<endl;

  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemLakeShore340Agent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemLakeShoreAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemGranPhil354Agent::init(const string& tshost, int tsport) {
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemGranPhil354Agent> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemGranPhil354Agent> Ecpics CA child proc. failed to start"<<endl;
  }
  if( !_sim ) {
    // connect to terminal server:
    _config->connect(tshost, tsport);
    if( _config->_devIO == 0 ) {
      clog<<"UFGemGranPhil354Agent> failed connection to annex/iocom."<<endl;
      exit(-1);
    }
  }
  // test connectivity to lakeshore:
  string gp354; // cmd & reply strings 
  time_t clck = time(0);
  if( clck % 2 == 0 )
    gp354 = "*01 9.99E+09";
  else
    gp354 = "*01 0.00E+00";

  string gprd = "RD"; // should be function in UFDeviceConfig
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.granvilphil354."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemGranPhil354Agent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  */
  if( _sim ) {
    clog<<"UFGemGranPhil354Agent> simulating connection to granville-phillips 354."<<endl;
  }
  else {
    clog<<"UFGemGranPhil354Agent> checking availiblity of granville-phillips 354."<<endl;
    int nc = _config->_devIO->submit(gprd, gp354, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemGranPhil354Agent> gravilphil reading failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
  }
  clog<<"UFGemGranPhil354Agent> reply: "<<gp354<<endl;
  _DevHistory.push_back(gp354);
  /*
  int nc = ::write(fd, gp354.c_str(), gp354.length());
  if( nc <= 0 ) {
    clog<<"UFGemGranPhil354Agent> status log failed..."<<endl;
  }
  ::close(fd);
  */
  return _config->_devIO;
}

void* UFGemGranPhil354Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  // since we cannot assume the epics db is available at startup,
  // attempt this one-time init each time we are invoked:
  /*
  if( _epics != "false" && _StatList.size() == 0 ) {
    string conf = _confGenSub + ".VALC";
    clog<<"UFGemGranPhil354Agent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    int nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemGranPhil354Agent::ancillary> epics initialization succeeded."<<endl;
      _heart = _StatList[2]; //  GP354 heartbeat rec. name is 3nd entry in this list
    }
    else {
      clog<<"UFGemGranPhil354Agent::ancillary> epics initialization failed."<<endl;
    }
    conf = _confGenSub + ".VALA";
    clog<<"UFGemGranPhil354Agent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemGranPhil354Agent::ancillary> epics initialization succeeded."<<endl;
      _statRec = _StatList[2]; // GP354 status rec. name is 3d entry in this list
    }
  }
  */
  string reply354 = "unknown@" + UFRuntime::currentTime();
  time_t clck = time(0);
  if( clck % 2 == 0 )
    reply354 = "*01 9.99E+09";
  else
    reply354 = "*01 0.00E+00";

  string gprd = "RD"; // should be function in UFDeviceConfig
  // assume blocking i/o for now...
  if( !_sim && _config && _config->_devIO ) {
    int nc =_config->_devIO->submit(gprd, reply354, _flush); // expect single line reply
    if( nc <= 0 ) {
      clog<<"UFGemGranPhil354Agent> gravilphil (1st try) reading failed, try reconnect?..."<<endl;
      //_config->_devIO->reconnect();
      if( _config->_devIO == 0 ) {
        clog<<"UFGemGranPhil354Agent> failed connection to annex/iocom."<<endl;
        exit(-1);
      }
      nc =_config->_devIO->submit(gprd, reply354, _flush); // expect single line reply
      if( nc <= 0 )
        clog<<"UFGemGranPhil354Agent> gravilphil reading failed 2nd try... quite and try later"<<endl;
    }
  } // !_sim
  _currentval = reply354;

  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();
  _DevHistory.push_back(reply354);

  if( _verbose )
    clog<<"UFGemGranPhil354Agent::ancillary> reading: "<<reply354<<endl;

  // update the heartbeat:
  clck = time(0);
  if( _capipe && _epics != "false" && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str();
    if( _verbose )
      clog<<"UFGemGranPhil354Agent::ancillary> heart= "<<val<<", fd: "<<fileno(_capipe)<<endl;
    updateEpics(_heart, val);
    delete s.str();
  }
  if( reply354 == "" || reply354 == " " ) {
    clog<<"UFGemGranPhil354Agent::ancillary> no vac. value now: "<<currentTime()<<endl;
    return p;
  }
  if( reply354.find("unknown") != string::npos ) {
    clog<<"UFGemGranPhil354Agent::ancillary> vac. value: "<<reply354<<endl;
    return p;
  }

  // reply must be parsed and formatted
  // eliminate "*01 " part: "*01 9.99E+09"
  string delim = " ";
  int pos0= 1 + reply354.find(delim);
  // presumably this is the only pressure value
  string vac= reply354.substr(pos0);

  // update the status value(s):
  if( _capipe && _epics != "false" && _statRec != "" ) {
    if( _verbose )
      clog<<"UFGemGranPhil354Agent::ancillary> vac= "<<vac<<", fd: "<<fileno(_capipe)<<endl;
    vector < string > v;
    v.push_back(vac);
    updateEpics(_statRec, v);
  }
   
  return p;
} //ancillary

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// the Gemini/Epics version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error/alarm condition should be indicated
int UFGemGranPhil354Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_expected = false;
    clog<<"UFGemGranPhil354Agent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemGranPhil354Agent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemGranPhil354Agent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car, errmsg, hist = "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= _sim;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];
    if( _verbose ) {
      clog<<"UFGemGranPhil354Agent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemGranPhil354Agent::action> cmdimpl: "<<cmdimpl<<endl;
    }
    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int)replies.size();
      }
      else {
        replies.push_back(_config->status(this)); 
	return (int)replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemGranPhil354Agent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemGranPhil354Agent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      //sim = false;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemGranPhil354Agent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") != 0 ||
	     cmdname.find("Sim") != 0 || 
	     cmdname.find("SIM") != 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemGranPhil354Agent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemGranPhil354Agent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
        return (int)replies.size();
      }
      else {
	delete rep; rep = 0;
        return 0;      
      }
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    string reply354;
    if( !sim && _config != 0 && _config->_devIO ) {
      int nr = _config->validCmd(cmdimpl);
      if( nr > 0 ) { // valid device cmd
        if( _verbose ) clog<<"UFGemGranPhil354Agent::action> submit raw: "<<cmdimpl<<endl;
        stat = _config->_devIO->submit(cmdimpl, reply354, _flush); // expect single line reply
        if( _verbose ) clog<<"UFGemGranPhil354Agent::action> reply: "<<reply354<<endl;
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply354);
        act->cmd_reply.push_back(reply354);
      }
      else { // default to history, but get new reading first
        string gprd = "RD"; // should be function in UFDeviceConfig
	if( _config != 0 && _config->_devIO ) 
          stat = _config->_devIO->submit(gprd, reply354, _flush); // expect single line reply
        if( stat >= 0 ) {
	  if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply354);
	}
        int last = (int)_DevHistory.size() - 1;
	int cnt = atoi(cmdimpl.c_str());
	if( cnt <= 0 ) cnt = 1;
	if( cnt > last+1 ) cnt = last+1;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
    }
    else if( hist != "" ) {
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(cmdimpl.c_str());
      if( cnt <= 0 ) cnt = 1;
      if( cnt > last+1 ) cnt = last+1;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      errmsg += cmdimpl;
      reply354 = "Sim OK, sleeping 1/2 sec...";
      act->cmd_reply.push_back(reply354);
      UFPosixRuntime::sleep(0.5);
      if( car != "" && _epics != "false" && _capipe ) sendEpics(car, errmsg);
      if( _verbose ) {
        clog<<"UFGemGranPhil354Agent::action> device reply: "<<reply354<<endl;
	clog<<"UFGemGranPhil354Agent::action> simulating error condition..."<<endl;
      }
      delete rep; rep = 0;
      return 0;
    }
    else if( sim ) {
      time_t clck = time(0);
      if( clck % 2 == 0 )
        reply354 = "*01 9.99E+09";
      else
        reply354 = "*01 0.00E+00";
      act->cmd_reply.push_back(reply354);
    }
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( car != "" && _epics != "false" && _capipe ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( car != "" && _epics != "false" && _capipe ) sendEpics(car, "OK");
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size();
  }

  delete rep; rep = 0;
  return 0;
} 

int UFGemGranPhil354Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemGranPhil354Agent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    sleep(_Update);
  }
  else
    UFSocket::waitOnAll(_Update);
    // sleep(_Update);
}

#endif // UFGemGranPhil354Agent
