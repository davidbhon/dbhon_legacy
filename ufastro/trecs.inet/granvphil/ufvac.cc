#if !defined(__ufvac_cc__)
#define __ufvac_cc__ "$Name:  $ $Id: ufvac.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

static bool _quiet = false;

class ufvac : public UFDaemon, public UFClientSocket {
public:
  ~ufvac();
  inline ufvac(int argc, char** argv, int port= -1) : UFDaemon(argc, argv),
						     UFClientSocket(port), _fs(0) {
    rename("ufvac@" + hostname());
  }

  static int main(int argc, char** argv);
  // return 0 on connection failure:
  FILE* init(const string& host, int port);

  // submit cmd & par (only), return immediately without fetching reply
  int submit(const string& c, const string& p, float flush= -1.0); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  //int submit(const string& raw, UFStrings& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& raw, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& c, const string& p, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.

  virtual string description() const { return __ufvac_cc__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
};

int ufvac::main(int argc, char** argv) {
  ufvac vac(argc, argv);
  string arg, host(hostname()), raw("true"), hist("false"), stat("false"); // default is raw command mode
  int port= 52004;
  float flush= -1.0;

  arg = vac.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      raw = arg;
  }

  arg = vac.findArg("-his");
  if( arg != "false" ) {
    raw = "false";
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      hist = arg;
    else
      hist = "2";
  }

  arg = vac.findArg("-hist");
  if( arg != "false" ) {
    raw = "false";
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      hist = arg;
    else
      hist = "2";
  }

  arg = vac.findArg("-stat");
  if( arg != "false" ) {
    raw = "false";
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      stat = arg;
    else
      stat = "fits";
  }

  arg = vac.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = vac.findArg("-raw");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = vac.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = vac.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufvac> usage: 'ufvac -flush sec. -host host -port port -raw raw-command'"<<endl;
    return 0;
  }

  FILE* f  = vac.init(host, port);
  if( f == 0 ) {
    clog<<"ufvac> unable to connect to Granville-Phillips vacuum agent..."<<endl;
    return 1;
  }

  UFStrings* reply_p= 0;
  if( raw != "false" ) {
    // raw (explicit) command should be executed once
    int ns = vac.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufvac> failed to submit raw= "<<raw<<endl;

    if( reply_p == 0 ) {
      clog<<"ufvac> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  else if( hist != "false" ) {
    // raw (explicit) command should be executed once
    int ns = vac.submit("his", hist, reply_p, flush);
    if( ns <= 0 )
      clog << "ufvac> failed to submit hist= "<<hist<<endl;

    if( reply_p == 0 ) {
      clog<<"ufvac> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  else if( stat != "false" ) {
    // raw (explicit) command should be executed once
    int ns = vac.submit("stat", stat, reply_p, flush);
    if( ns <= 0 )
      clog << "ufvac> failed to submit stat= "<<stat<<endl;

    if( reply_p == 0 ) {
      clog<<"ufvac> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  // enter command loop
  string line;
  while( true ) {
    clog<<"ufvac(raw)> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;

    raw = line;
    int ns = vac.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufvac> failed to submit raw= "<<raw<<endl;

    if( reply_p == 0 ) {
      clog<<"ufvac> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  return 0; 
}


// public static func:
// return < 0 on connection failure:
FILE* ufvac::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !_quiet )
    clog<<"ufvac> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// public
ufvac::~ufvac() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

// submit string (only), return immediately without fetching reply
int ufvac::submit(const string& c, const string& p, float flush) {
  vector<string> cmd;
  cmd.push_back(c);
  cmd.push_back(p);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufvac::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
//int ufvac::submit(const string& raw, UFStrings& r, float flush) {
int ufvac::submit(const string& raw, UFStrings*& r, float flush) {
  int ns = submit("raw", raw, flush);
  if( ns <= 0 )
    return ns;

  /*
  ns = recv(r);
  if( ns <= 0 )
    return ns;

  return r.elements();
  */

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

// submit string, recv reply
int ufvac::submit(const string& c, const string& p, UFStrings*& r, float flush) {
  int ns = submit(c, p, flush);
  if( ns <= 0 )
    return ns;

  /*
  ns = recv(r);
  if( ns <= 0 )
    return ns;

  return r.elements();
  */

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

int main(int argc, char** argv) {
  return ufvac::main(argc, argv);
}

#endif // __ufvac_cc__
