#if !defined(__UFGPConfig_cc__)
#define __UFGPConfig_cc__ "$Name:  $ $Id: UFGPConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGPConfig_cc__;

#include "UFGPConfig.h"
#include "UFGranvilPhilAgent.h"
#include "UFFITSheader.h"

UFGPConfig::UFGPConfig(const string& name) : UFDeviceConfig(name) {}

UFGPConfig::UFGPConfig(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

vector< string >& UFGPConfig::queryCmds() {
  return _queries;
}

vector< string >& UFGPConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("RD"); _actions.push_back("rd");
  return _actions;
}

int UFGPConfig::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      if( c.find("?") == string::npos )
        return 0;
      else
	return 1;
    }
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }
  clog<<"UFGPConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFGPConfig::status(UFDeviceAgent* da) {
  UFGranvilPhilAgent* dagp = dynamic_cast< UFGranvilPhilAgent* > (da);
  if( dagp->_verbose )
    clog<<"UFLSConfig::status> "<<dagp->name()<<endl;
  string g = dagp->getCurrent(); 
  string delim = " ";
  int pos0= 1 + g.find(delim);
  // presumably this is the only pressure value
  string vac= "Vaccum == ";
  vac += g.substr(pos0);
  vac += " Torr";
  return new UFStrings(dagp->name(), (string*) &vac);
}

UFStrings* UFGPConfig::statusFITS(UFDeviceAgent* da) {
  UFGranvilPhilAgent* dagp = dynamic_cast< UFGranvilPhilAgent* > (da);
  if( dagp->_verbose )
    clog<<"UFLSConfig::status> "<<dagp->name()<<endl;
  string g = dagp->getCurrent(); 
  string delim = " ";
  int pos0= 1 + g.find(delim);
  // presumably this is the only pressure value
  string vac= g.substr(pos0);

  map< string, string > valhash, comments;
  valhash["Vacuum"] = vac;
  comments["Vacuum"] = "Torr";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(dagp->name());

  return ufs;
}

#endif // __UFGPConfig_cc__
