#if !defined(__ufdc_cc__)
#define __ufdc_cc__ "$Name:  $ $Id: ufdc.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"
#include "UFMCE4Config.h"

class ufdc :  public UFClientApp {
public:
  inline ufdc(const string& name, int argc, char** argv, char** envp, int port= -1);
  inline ~ufdc() {}
  static int main(const string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufdc_cc__; }

  // parse & insert into string vector assuming
  // format: -par "hard:0,1,2,3,4,5,6,7,8,9"
  // or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
  // or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
  // also truncate to directive name par ==> "hard/phys/meta"
  int parsePar(string& par, vector < string >& pars);

  // parse & insert into string vector assuming
  // format: -conf "ObsId,Host,RootDir,File"
  int parseConf(string& conf, vector < string >& acqconf);

  // submit parameter bundle, flush output and sleep a bit.
  // recv reply and return reply as string.
  int submitPar(vector < string >& par, const string& obsmode, const string& rdoutmode,
		UFStrings*& reply, float flush= -1.0);

  // test dac bias & preamp commands
  int testDACBias(UFStrings*& reply, int well= 0, int det= 0, float flush= -1.0);
  int testDACPreamp(UFStrings*& reply, float flush= -1.0);
};

ufdc::ufdc(const string& name, int argc,
	   char** argv, char** envp, int port) : UFClientApp(name, argc, argv, envp, port) {}

int ufdc::main(const string& name, int argc, char** argv, char** envp) {
  ufdc dc(name, argc, argv, envp);
  string arg, host(hostname());
  string acq("false"), conf("false"), par("false"), sim("false"), status("false");
  string cmdline("false"), raw("true"); // default is raw command mode
  int port= 52008;
  float flush= -1.0;
  int dac= -1;
  string usage = "usage: 'ufdc [-flush sec.] [-host host] [-port port] [-q/-raw [raw-command]] [-acq [start/stop]] [-conf \"ObsId,Host,RootDir,File\"] [-obsmode stare/nod/chop/chop-nod] [-rdoutmode default] [-par [hard:1,2,...] [phys:1.0,2.0,...] [meta:a,b,...]] [-dac 0/1/2]'";
  
  string obsmode= "Stare", rdoutmode= "Default";

  arg = dc.findArg("-h");
  if( arg != "false" ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  arg = dc.findArg("-help");
  if( arg != "false" ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  arg = dc.findArg("-obsmode");
  if( arg != "false" && arg != "true" )
    obsmode = arg;

  arg = dc.findArg("-rdoutmode");
  if( arg != "false" && arg != "true" )
    rdoutmode = arg;

  arg = dc.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" )
      raw = cmdline = arg;
  }
  arg = dc.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = dc.findArg("-raw");
  if( arg != "false" && arg != "true" )
    raw = cmdline = arg;

  arg = dc.findArg("-status");
  if( arg != "false" ) {
    raw = "false";
    _quiet = true;
    if( arg != "true" ) 
      status = cmdline = arg;
    else
      status = cmdline = "All";
  }

  arg = dc.findArg("-stat");
  if( arg != "false" ) { 
    raw = "false";
    _quiet = true;
    if( arg != "true" ) 
      status = cmdline = arg;
    else
      status = cmdline = "All";
  }

  arg = dc.findArg("-sim");
  if( arg != "false" && arg != "true" ) {
    sim = cmdline = arg;
    raw = "false";
  }
  /*
  arg = dc.findArg("-car");
  if( arg != "false" && arg != "true" ) {
   car = cmdline = arg;
     raw = "false";
  }
  */
  // format: -par "hard:0,1,2,3,4,5,6,7,8,9"
  // or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
  // or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
  // or:     -par clear
  arg = dc.findArg("-parm");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    par = cmdline = arg;
    raw = "false";
  }
  arg = dc.findArg("-par");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    par = cmdline = arg;
    raw = "false";
  }

  arg = dc.findArg("-acq");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    acq = cmdline = arg;
    raw = "false";
  }

  arg = dc.findArg("-conf");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    conf = cmdline = arg;
    raw = "false";
  }

  arg = dc.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = dc.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  arg = dc.findArg("-dac");
  if( arg != "false" && arg != "true" ) {
    dac = atoi(arg.c_str());
    cmdline = arg;
    raw = "false";
  }

  if( port <= 0 || host.empty() ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  FILE* f  = dc.init(host, port);
  if( f == 0 ) {
    clog<<"ufdc> unable to connect to MCE4 command server agent..."<<endl;
    return -2;
  }

  UFStrings* reply_p;
  if( cmdline != "false" ) {
    // command should be executed once
    vector< string > mult;
    if( par != "false" ) {
      int npar = dc.parsePar(par, mult);
      if( npar <= 0 ) {
        clog << "ufdc> failed to parse parameters: "<<cmdline<<endl;
        return -1;
      }
    }
    else if( conf != "false" ) {
      int nconf = dc.parseConf(conf, mult);
      if( nconf <= 0 ) {
        clog << "ufdc> failed to parse config option: "<<cmdline<<endl;
        return -1;
      }
    }
    else if( dac < 0 ) {
      int nraw = dc.parse(cmdline, mult);
      if( nraw <= 0 ) {
        clog << "ufdc> failed to parse cmd(s): "<<cmdline<<endl;
        return -1;
      }
    }

    int ns= 0;
    if( raw != "false" ) {
      ns = dc.submit("raw", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( status != "false" ) {
      ns = dc.submit("status", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      ns = dc.submit("sim", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<sim<<endl;
        return -3;
      }
    }
    else if( acq != "false" ) {
      ns = dc.submit("acq", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( conf != "false" ) {
      ns = dc.submit("conf", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( par != "false" ) {
      ns = dc.submitPar(mult, obsmode, rdoutmode, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }  
    else if( dac == 0 ) {
      ns = dc.testDACBias(reply_p, 0, 0, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }
    else if( dac == 1 ) {
      ns = dc.testDACBias(reply_p, 1, 1, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }
    else if( dac >= 2 ) {
      ns = dc.testDACBias(reply_p, 1, 2, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }

    UFStrings& reply = *reply_p;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return dc.close();
  }

  string line;
  while( true ) {
    clog<<"ufdc> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" ) return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    if( line.find("par") != string::npos ||
	line.find("Par") != string::npos || 
	line.find("PAR") != string::npos )
      par = line;
    vector< string > mult;
    if( par != "false" ) {
      int npar = dc.parsePar(par, mult);
      if( npar <= 0 ) {
        clog << "ufdc> failed to parse parameters: "<<cmdline<<endl;
        return -2;
      }
    }
    else {
      int nraw = dc.parse(line, mult);
      if( nraw <= 0 ) {
        clog << "ufdc> failed to parse cmd(s): "<<cmdline<<endl;
        return -2;
      }
    }

    int ns= 0;
    if( raw != "false" ) {
      ns = dc.submit("raw", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      ns = dc.submit("sim", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<sim<<endl;
        return -3;
      }
    }
    else if( acq != "false" ) {
      ns = dc.submit("acq", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( par != "false" ) {
      ns = dc.submitPar(mult, obsmode, rdoutmode, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  return dc.close(); 
}

// parse mutil-cmd line assuming
// format: -par "hard:0,1,2,3,4,5,6,7,8,9"
// or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
// or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
// insert into string vector
int ufdc::parsePar(string& par, vector <string>& pars) {
  pars.clear();
  pars.push_back("Par");
  if( par.find("cle") != string::npos ||
      par.find("Cle") != string::npos || 
      par.find("CLE") != string::npos ) {
    pars.push_back("Clear");
    return (int)pars.size();
  }

  size_t colp =  par.find(":");
  if( colp == string::npos ) {
    pars.clear();
    return 0;
  }
  string parlist = par.substr(1+colp);

  if( par.find("har") != string::npos ||
      par.find("Har") != string::npos || 
      par.find("HAR") != string::npos ) {
    pars.push_back("Har");
    UFMCE4Config::HdwrParms hp;
    UFMCE4Config::clear(hp);
    UFMCE4Config::HdwrParms::iterator itr = hp.begin();
    size_t com = 0, pos = 0;
    string sval;
    do {
      string key = itr->first;
      pars.push_back(key);
      com = parlist.find(",", pos);
      if( com == string::npos ) // assume it's final val
	sval = parlist.substr(pos);
      else
        sval = parlist.substr(pos, com-pos);
      pars.push_back(sval);
      //clog<<"ufdc::parsePar> key: "<<key<<", val: "<<sval<<endl;
      pos = 1+com;
    } while( ++itr != hp.end());
    return (int)pars.size();
  }

  if( par.find("phy") != string::npos ||
      par.find("Phy") != string::npos || 
      par.find("PHY") != string::npos ) {
    pars.push_back("Phy");
    UFMCE4Config::PhysParms pp;
    UFMCE4Config::clear(pp);
    UFMCE4Config::PhysParms::iterator itr = pp.begin();
    size_t com = 0, pos = 0;
    string sval;
    do {
      string key = itr->first;
      pars.push_back(key);
      com = parlist.find(",", pos);
      if( com == string::npos ) // assume it's final val
	sval = parlist.substr(pos);
      else
        sval = parlist.substr(pos, com-pos);
      pars.push_back(sval);
      //clog<<"ufdc::parsePar> key: "<<key<<", val: "<<sval<<endl;
      pos = 1+com;
    } while( ++itr != pp.end());
    return (int)pars.size();
  }

  if( par.find("met") != string::npos ||
      par.find("Met") != string::npos || 
      par.find("MET") != string::npos ) {
    pars.push_back("Met");
    UFMCE4Config::TextParms mp;
    UFMCE4Config::clear(mp);
    UFMCE4Config::TextParms::iterator itr = mp.begin();
    size_t com = 0, pos = 0;
    string sval;
    do {
      string key = itr->first;
      pars.push_back(key);
      com = parlist.find(",", pos);
      if( com == string::npos ) // assume it's final val
	sval = parlist.substr(pos);
      else
        sval = parlist.substr(pos, com-pos);
      pars.push_back(sval);
      if( !_quiet )
        clog<<"ufdc::parsePar> key: "<<key<<", val: "<<sval<<endl;
      pos = 1+com;
    } while( ++itr != mp.end());
    return (int)pars.size();
  }
  return -1;
}

int ufdc::parseConf(string& conf, vector <string>& acqconf) {
  acqconf.clear();
  size_t comp =  conf.find(",");
  if( comp == string::npos ) {
    return 0;
  }
  acqconf.push_back("Acq");
  acqconf.push_back("Conf");
  string obsId = conf.substr(0, comp);
  acqconf.push_back("ObsID"); acqconf.push_back(obsId);
 
  size_t prev = 1+comp;
  comp =  conf.find(",", prev);
  if( comp == string::npos ) {
    return acqconf.size();
  }
  string host = conf.substr(prev, comp-prev);
  acqconf.push_back("Host"); acqconf.push_back(host);

  prev = 1+comp;
  comp =  conf.find(",", prev);
  if( comp == string::npos ) {
    return acqconf.size();
  }
  string rootdir = conf.substr(prev, comp-prev);
  acqconf.push_back("RootDir"); acqconf.push_back(rootdir);

  // last value, filename, should not be followed by a comma:
  prev = 1+comp;
  if( prev >= conf.length() ) {
    return acqconf.size();
  }
  string filenm = conf.substr(prev);
  acqconf.push_back("File"); acqconf.push_back(filenm);

  return acqconf.size();
}

int ufdc::submitPar(vector< string >& vs, const string& obsmode,
		    const string& rdoutmode, UFStrings*& r, float flush) {

  vs.push_back("obs_mode"); vs.push_back(obsmode);
  vs.push_back("readout_mode"); vs.push_back(rdoutmode);

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufdc::testDACBias(UFStrings*& r, int well, int det, float flush) {
  vector< string > vs;

  vs.push_back("BiasPark"); vs.push_back("ARRAY_POWER_DOWN_BIAS");
  vs.push_back("BiasDatum"); vs.push_back("ARRAY_POWER_DAC_DEFAULTS_BIAS");
  vs.push_back("BiasPower"); vs.push_back("ARRAY_POWER_UP_BIAS");
  strstream sw;
  sw<<"ARRAY_WELL "<<well<<ends;
  vs.push_back("BiasvWell"); vs.push_back(sw.str()); delete sw.str();
  strstream sd;
  sd<<"ARRAY_SET_DET_BIAS "<<det<<ends;
  vs.push_back("BiasvBias"); vs.push_back(sd.str()); delete sd.str();

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufdc::testDACPreamp(UFStrings*& r, float flush) {
  vector< string > vs;

  vs.push_back("BiasPark"); vs.push_back("ARRAY_POWER_DOWN_BIAS");
  vs.push_back("BiasDatum"); vs.push_back("ARRAY_POWER_DAC_DEFAULTS_BIAS");
  vs.push_back("BiasPower"); vs.push_back("ARRAY_POWER_UP_BIAS");

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int main(int argc, char** argv, char** envp) {
  return ufdc::main("ufdc", argc, argv, envp);
}

#endif // __ufdc_cc__
