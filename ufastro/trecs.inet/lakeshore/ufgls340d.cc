#if !defined(__ufgls340d_cc__)
#define __ufgls340d_cc__ "$Name:  $ $Id: ufgls340d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgls340d_cc__;

#include "UFGemLakeShore340Agent.h"

int main(int argc, char** argv) {
  try {
    UFGemLakeShore340Agent::main(argc, argv);
  }
  catch( std::exception& e ) {
    clog<<"ufgls340d> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgls340d> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgls340d_cc__
