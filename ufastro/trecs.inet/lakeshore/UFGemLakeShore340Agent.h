#if !defined(__UFGemLakeShore340Agent_h__)
#define __UFGemLakeShore340Agent_h__ "$Name:  $ $Id: UFGemLakeShore340Agent.h,v 0.5 2003/07/25 21:16:55 hon beta $"
#define __UFGemLakeShore340Agent_H__(arg) const char arg##UFGemLakeShore340Agent_h__rcsId[] = __UFGemLakeShore340Agent_h__;

#include "UFGemDeviceAgent.h"
#include "UFLS340Config.h"

#include "string"
#include "vector"
#include "deque"
#include "iostream.h"

class UFGemLakeShore340Agent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemLakeShore340Agent(int argc, char** argv);
  UFGemLakeShore340Agent(const string& name, int argc, char** argv);
  inline virtual ~UFGemLakeShore340Agent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  virtual void startup();

  virtual string newClient(UFSocket* client);

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in addition to establishing the iocomm/annex connection tp lakeshore,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);
  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);
  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline void getCurrent(string& chanA, string& chanB) {
    chanA = _currentvalA; chanB = _currentvalB;
    return;
  }

  inline float getSetPnt() { return _setpval; }
  inline float getSetPnt(string& sval) { strstream s; s<<_setpval<<ends; sval = s.str(); delete s.str(); return _setpval; }

protected:
  static string _currentvalA;
  static string _currentvalB;
  static float  _setpval;
  static string _setpvalCmd;
  static string _setPointRec;
  static string _simkelvin1, _simkelvin2;
  static time_t _timeOut; // in seconds 
  static time_t _clock;
  static bool _setcrv, _setintyp;

  static string _simsetp();
};

#endif // __UFGemLakeShore340Agent_h__
      
