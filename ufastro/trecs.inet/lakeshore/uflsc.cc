#if !defined(__uflsc_cc__)
#define __uflsc_cc__ "$Name:  $ $Id: uflsc.cc 14 2008-06-11 01:49:45Z hon $";
#include "unistd.h"
#include "stdio.h"
#include "iostream.h"
#if defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#include "sys/uio.h"
#endif
#include "termio.h"
#include "string"
#include "vector"

#include "UFClientApp.h"

// although technically not a daemon, a client can make use of the runtime funcs:
class uflsc : public UFClientApp {
public:
  uflsc(const string& name, int argc, char** argv, char** envp, int port= -1);
  inline ~uflsc() {}

  static int main(const string& name, int argc, char** argv, char** envp);
  virtual string description() const { return __uflsc_cc__; }
};

uflsc::uflsc(const string& name, 
	     int argc, char** argv, char** envp,
	     int port) : UFClientApp(name, argc, argv, envp, port) {};

int uflsc::main(const string& name, int argc, char** argv, char** envp) {
  uflsc lsc("uflsc", argc, argv, envp);
  string arg, host(hostname());
  string raw = "true"; // default is raw command mode
  string status = "", his= ""; 
  int port= 52003;
  float flush= -1.0;

  arg = lsc.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = lsc.findArg("-raw");
  if( arg != "false" && arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = lsc.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = lsc.findArg("-port");
  if( arg != "false" && arg != "false" )
    port = atoi(arg.c_str());

  arg = lsc.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) 
      raw = arg;
  }

  arg = lsc.findArg("-his");
  if( arg != "false" ) { // history
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      his = arg;
    else
      his = "1";
      //status = "FITS"; // only two allowed values 
  }

  arg = lsc.findArg("-stat");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  arg = lsc.findArg("-status");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  if( port <= 0 || host.empty() ) {
    clog<<"uflsc> usage: 'uflsc -flush sec. -host host -port port -q raw-command'"<<endl;
    return 0;
  }

  FILE* f  = lsc.init(host, port);
  if( f == 0 ) {
    clog<<"uflsc> unable to connect to LakeShore command server/agent..."<<endl;
    return 1;
  }

  UFStrings* reply_p = 0;
     
  if( raw != "true" && raw != "false" ) {
    // raw (explicit) command should be executed once
    string req = "raw";
    int ns = lsc.submit(req, raw, reply_p, flush);
    if( ns <= 0 || reply_p == 0 ) {
      clog << "uflsc> failed to submit or get reply for raw: "<<raw<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }

  if( his != "" ) {
    // history (explicit) command should be executed once
    string req = "his";
    int ns = lsc.submit(req, his, reply_p, flush);
    if( ns <= 0 || reply_p == 0 ) {
      clog << "uflsc> failed to submit or get reply for raw: "<<raw<<endl;
      return -1;
    }

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }

  if( status != "" ) {
    // stat (explicit) command should be executed once
    string req = "status";
    int ns = lsc.submit(req, status, reply_p, flush);
    if( ns <= 0 || reply_p == 0 ) {
      clog << "uflsc> failed to submit or get reply for raw: "<<raw<<endl;
      return -1;
    }

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }

  // enter command loop
  // first reset keyboard input behavior
  _orig = lsc.setRawIO();
  string line, prompt= "uflsc> ";
  vector< string > history;
  while( true ) {
    clog<<prompt<<ends;
    line = lsc.getLine(prompt, history);
    
    if( line == "exit" || line == "quit" || line == "q" )
      break;

    if( line.rfind("history") != string::npos ||
	line.rfind("History") != string::npos ||
	line.rfind("HISTORY") != string::npos ) { // print history
      for( int i = 0; i < (int)history.size(); ++i )
	clog<<i<<": "<<history[i]<<endl;
      continue;
    }
    else {
      history.push_back(line);
    }

    int ns= 0;
    size_t spos = line.rfind("stat");
    if( spos == string::npos )
      spos = line.rfind("Stat");
    if( spos == string::npos )
      spos = line.rfind("STAT");
    if( spos != string::npos ) {
      status = line.substr(1+line.find(" ", spos));
      if( status.length() > 0 )
        ns = lsc.submit("status", status, reply_p, flush);
    }
    spos = line.rfind("his");
    if( spos == string::npos )
      spos = line.rfind("His");
    if( spos == string::npos )
      spos = line.rfind("HIS");
    if( spos != string::npos ) {
      status = line.substr(1+line.find(" ", spos));
      if( status.length() > 0 )
        ns = lsc.submit("his", status, reply_p, flush);
    }
    else {
      raw = line;
      ns = lsc.submit("raw", raw, reply_p, flush);
      if( ns <= 0 || reply_p == 0 ) {
        clog << "uflsc> failed to submit or get reply for raw: "<<raw<<endl;
        return -1;
      }
    }
    UFStrings& reply = *reply_p;
    cout<<"\n"<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  } // end while

  lsc.restoreIO(_orig);
  return 0; 
}

int main(int argc, char** argv, char** envp) {
  return uflsc::main("uflsc", argc, argv, envp);
}

#endif // __uflsc_cc__
