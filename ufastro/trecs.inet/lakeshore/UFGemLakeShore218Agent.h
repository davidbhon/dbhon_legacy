#if !defined(__UFGemLakeShore218Agent_h__)
#define __UFGemLakeShore218Agent_h__ "$Name:  $ $Id: UFGemLakeShore218Agent.h,v 0.1 2002/06/26 20:34:04 trecs beta $"
#define __UFGemLakeShore218Agent_H__(arg) const char arg##UFGemLakeShore218Agent_h__rcsId[] = __UFGemLakeShore218Agent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFLS218Config.h"

class UFGemLakeShore218Agent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemLakeShore218Agent(int argc, char** argv);
  UFGemLakeShore218Agent(const string& name, int argc, char** argv);
  inline virtual ~UFGemLakeShore218Agent() {}

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in adition to establishing the iocomm/annex connection to,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);
  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline string getCurrent() { return _currentval; }

protected:
  static string _currentval;

};

#endif // __UFGemLakeShore218Agent_h__
      
