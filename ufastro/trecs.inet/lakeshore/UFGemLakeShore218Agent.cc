#if !defined(__UFGemLakeShore218Agent_cc__)
#define __UFGemLakeShore218Agent_cc__ "$Name:  $ $Id: UFGemLakeShore218Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemLakeShore218Agent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemLakeShore218Agent.h"
#include "UFLS218Config.h"

string UFGemLakeShore218Agent::_currentval;

// ctors:
UFGemLakeShore218Agent::UFGemLakeShore218Agent(int argc,
					       char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve LS218 readings
  _flush =  0.0; // 0.35; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls218Heartbeat";
  _statRec = _epics + ":ec:tempMonG.J"; // 
}

UFGemLakeShore218Agent::UFGemLakeShore218Agent(const string& name,
					       int argc,
					       char** argv) : UFGemDeviceAgent(name, argc, argv){
  // use ancillary function to retrieve LS218 readings
  _flush =  0.0; // 0.35; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls218Heartbeat";
  _statRec = _epics + ":ec:tempMonG.J"; // 
}

// static funcs:
int UFGemLakeShore218Agent::main(int argc, char** argv) {
  UFGemLakeShore218Agent ufs("UFGemLakeShore218Agent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemLakeShore218Agent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  // _epics may have been reset:
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls218Heartbeat";
  _statRec = _epics + ":ec:tempMonG.J"; // 

  _config = new UFLS218Config(); // needs to be proper device subclass (LS218)
  // 218 defaults for trecs:
  _config->_tsport = 7002;
  _config->_tshost = "192.168.111.101";

  string arg;
  arg = findArg("-sim"); // don't connect to epics db
  if( arg == "true" ) {
    _sim = true;
  }

  arg = findArg("-noepics"); // don't connect to epics db
  if( arg == "true" ) {
    _epics = "false";
    _heart = "";
    _statRec = "";
  }

  arg = findArg("-epics"); // epics host/db name
  if( arg != "false" && arg != "true" ) {
    _epics = arg;
    _confGenSub = _epics + ":ec:configG";
    _heart = _epics + ":ec:ls218Heartbeat";
    _statRec = _epics + ":ec:tempMonG.J"; // 
  }

  arg = findArg("-heart");
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52002;
  }
  if( _verbose ) 
    clog<<"UFGemLakeShore218Agent::options> set port to GemLakeShore218 port "<<port<<endl;

  return port;
}

// override base class startup here to avoid connect to term. serv
// if no conection is specified, assume simulation is desired,
// then do server listen stuff
void UFGemLakeShore218Agent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemLakeShore218Agent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemLakeShore218Agent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else
    init(_config->_tshost, _config->_tsport);

  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemLakeShoreAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemLakeShore218Agent::init(const string& tshost, int tsport) {
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild("trecs");
    if( _capipe )
      clog<<"UFGemLakeShore218Agent> Epics CA child proc. started"<<endl;
    else
      clog<<"UFGemLakeShore218Agent> Ecpics CA child proc. failed to start"<<endl;
  }

  // connect to terminal server:
  if( !_sim ) {
    _config->connect(tshost, tsport);
    if( _config->_devIO == 0 )
      return 0;
  }
  // test connectivity to lakeshore:
  string lakeshore; // lakeshore cmd & reply strings 
  lakeshore = "+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0";
  string allkelvin = "krdg?0"; // should be function in UFDeviceConfig
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.lakeshore218."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemLakeShore218Agent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  */
  if( !_sim ) {
    clog<<"UFGemLakeShore218Agent> checking availiblity of lakeshore 218 channels 1 - 8."<<endl;
    int nc = _config->_devIO->submit(allkelvin, lakeshore, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemLakeShore218Agent> lakeshore218 querry all kelvin failed..."<<endl;
    }
    clog<<"UFGemLakeShore218Agent> reply: "<<lakeshore<<ends;
  }
  else
    clog<<"UFGemLakeShore218Agent> simulate lakeshore 218 channels 1 - 8."<<endl;

  /*
  int nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
  if( nc <= 0 ) {
    clog<<"UFGemLakeShore218Agent> status log failed..."<<endl;
  }
  ::close(fd);
  */
  return _config->_devIO;
}


void* UFGemLakeShore218Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  //clog<<"UFGemLakeShore208Agent::ancillary> get new LS218 reading..."<<endl;

  string reply218 = "+000.00,+000.00,+000.00,+000.00,+000.00,+000.00,+000.00,+000.00";
  if( _DevHistory.size() % 2 == 0 )
    reply218 = "+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0";
  else
    reply218 = "+298.7,+298.7,+298.7,+298.7,+298.7,+298.7,+298.7,+298.7";
  // assume blocking i/o for now...

  if( _config && _config->_devIO ) { // not simulation
    _config->_devIO->submit("krdg?0", reply218, _flush); // expect single line reply
  }

  _currentval = reply218;

  // check the max history count, and decrement by two if limit has been reached
  if( _DevHistory.size() > _DevHistSz ) { // should be configurable size
    _DevHistory.pop_front();
    _DevHistory.pop_front();
  }

  // insert value into history
  _DevHistory.push_back(reply218);

  if( _verbose )
    clog<<"UFGemLakeShore218Agent::ancillary> new reading: "<<reply218<<endl;
  // reply must be parsed and formatted into vector
  // eliminate all "+" parts: 
  // "+365.00,+475.00,+475.00,+475.00,+475.00,+475.00,+475.00,+475.00"
  string delim = ",+";
  int dlen = delim.length();
  vector< string > v;
  int pos0= 1 + reply218.find("+");
  int pos= reply218.find(delim, pos0);
  string t= reply218.substr(pos0, pos-1);
  // presumably this is the 1st of 8 temperature vals
  v.push_back(t);
  //if( _verbose )
  // clog<<"UFGemLakeShore218Agent::ancillary> t= "<<t<<endl;
  while( true ) {
    pos0 = dlen + pos;
    pos = reply218.find(delim, pos0);
    if( pos == (int)string::npos )
      break;
    t = reply218.substr(pos0, pos-pos0);
    v.push_back(t);
    //if( _verbose )
    //clog<<"UFGemLakeShore218Agent::ancillary> t= "<<t<<endl;
  }
  t = reply218.substr(pos0, reply218.length()-pos0-1);
  v.push_back(t); // and this is the 8th
  //if( _verbose )
  //clog<<"UFGemLakeShore218Agent::ancillary> t= "<<t<<endl;

  // since we cannot assume the epics db is available at startup,
  // attempt this one-time init each time we are invoked:
  /*
  if( _epics != "false" && _StatList.size() == 0 ) {
    string conf = _confGenSub + ".VALC";
    int nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ){
      clog<<"UFGemLakeShore218Agent::ancillary> epics initialization succeeded."<<endl;
      _heart = _StatList[1]; //  LS218 heartbeat rec. name is 2nd entry in this list
    }
    else 
      clog<<"UFGemLakeShore218Agent::ancillary> epics initialization failed."<<endl;

    conf = _confGenSub + ".VALA";
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ){
      clog<<"UFGemLakeShore218Agent::ancillary> epics initialization succeeded."<<endl;
      _statRec = _StatList[1]; // LS218 status rec. name is 2nd entry in this list
    }
    else 
      clog<<"UFGemLakeShore218Agent::ancillary> epics initialization failed."<<endl;
  } 
  */ 
  // update the heartbeat:
  time_t clck = time(0);
  if( _epics != "false" && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
  }
    
  // update the status values:
  if( _epics != "false" && _statRec != "" )
    updateEpics(_statRec, v);

  return p;
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// this version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error /alarm condition should be indicated
int UFGemLakeShore218Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_expected = false;
    clog<<"UFGemLakeShore218Agent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemLakeShore218Agent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemLakeShore218Agent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car, errmsg, hist= "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= _sim;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];

    if( _verbose ) {
      clog<<"UFGemLakeShore218Agent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemLakeShore218Agent::action> cmdimpl: "<<cmdimpl<<endl;
    }

    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int) replies.size();
      }
      else {
        replies.push_back(_config->status(this));
	return (int) replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemLakeShore218Agent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("caput") == 0 ||
	     cmdname.find("Caput") == 0 ||
	     cmdname.find("CaPut") == 0 || 
             cmdname.find("CAPUT") == 0 ) {
      if( _verbose ) clog<<"UFGemLakeShore218Agent::action> "<<cmdname<<" "<<cmdimpl<<endl;
      if( _epics != "false" && car != "" ) sendEpics(cmdimpl);
      delete rep; rep = 0;
      return 0;      
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemLakeShore218Agent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemLakeShore218Agent::action> History: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") == 0 ||
	     cmdname.find("Sim") == 0 || 
	     cmdname.find("SIM") == 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemLakeShore218Agent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemLakeShore218Agent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
	return (int) replies.size();
      }
      else {
        delete rep; rep = 0;
        return 0;
      }      
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    string reply218;
    if( sim ) { // sim history
      if( _DevHistory.size() % 2 == 0 )
        reply218 = "+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0,+300.0";
      else
        reply218 = "+298.7,+298.7,+298.7,+298.7,+298.7,+298.7,+298.7,+298.7";
      if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply218);
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(hist.c_str());
      if( cnt <= 0 )
	cnt = 1;
      else if( cnt > last )
	cnt = 1+last;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( !sim && _config != 0 && _config->_devIO ) {
      int nr = _config->validCmd(cmdimpl);
      if( nr > 0 ) { // valid device cmd
        if( _verbose ) clog<<"UFGemLakeShore218Agent::action> submit raw: "<<cmdimpl<<endl;
        stat = _config->_devIO->submit(cmdimpl, reply218, _flush); // expect single line reply?
        if( _verbose ) clog<<"UFGemLakeShore218Agent::action> reply: "<<reply218<<endl;
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply218);
      } // legitimate ls218 cmd
      else if( cmdname.find("sta") != string::npos || cmdname.find("Sta") != string::npos ||
	       cmdname.find("STA") != string::npos ) { // pseudo cmd: status FITS or Full
	string allkelvin = "krdg?0"; // should be function in UFDeviceConfig
        stat = _config->_devIO->submit(allkelvin, reply218, _flush); // expect single line reply?
        if( stat > 0 ) { // update _current
	  _currentval = reply218;
          if( _verbose ) clog<<"UFGemLakeShore218Agent::action> reply: "<<reply218<<endl;
          if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply218);
	}
	if( cmdimpl.find("fit") != string::npos || cmdimpl.find("Fit") != string::npos ||
	    cmdimpl.find("FIT") != string::npos ) {
          replies.push_back(_config->statusFITS(this));
	  return (int) replies.size();
	}
	else {
          replies.push_back(_config->statusFITS(this));
	  return (int) replies.size();
	}
      }	
      else { // assume history (psuedo cmd) request:
        int last = (int)_DevHistory.size() - 1;
	int cnt = atoi(cmdimpl.c_str());
	if( cnt <= 0 )
	  cnt = 1;
        else if( cnt > last )
	  cnt = 1+last;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
    }
    else if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      errmsg += cmdimpl;
      reply218 = "Sim OK, sleeping 1/2 sec...";
      UFPosixRuntime::sleep(0.5);
      if( _epics != "false" && car != "" ) sendEpics(car, errmsg);
      if( _verbose ) {
        clog<<"UFGemLakeShore218Agent::action> device reply: "<<reply218<<endl;
	clog<<"UFGemLakeShore218Agent::action> simulating error condition..."<<endl;
      }
      delete rep; rep = 0;
      return 0;
    }
    else if( sim ) {
      reply218 = "Sim OK, sleeping 1/2 sec...";
      UFPosixRuntime::sleep(0.5);
    }
    if( hist == "" && reply218 != "" ) // history req. already dealt with...
      act->cmd_reply.push_back(reply218);
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( _epics != "false" &&  car != "" ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( _epics != "false" && car != "" ) sendEpics(car, "OK");
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_expected ) {
    delete rep; rep = 0;
    return 0;
  }
  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int) replies.size();
}

int UFGemLakeShore218Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemLakeShore218Agent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    sleep(_Update);
  }
  else
    UFSocket::waitOnAll(_Update);
    // sleep(_Update);
}

#endif // UFGemLakeShore218Agent
