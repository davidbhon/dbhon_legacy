#if !defined(__UFLS340Config_h__)
#define __UFLS340Config_h__ "$Name:  $ $Id: UFLS340Config.h,v 0.4 2003/02/07 21:39:31 hon beta $"
#define __UFLS340Config_H__(arg) const char arg##UFLS340Config_h__rcsId[] = __UFLS340Config_h__;

#include "UFDeviceConfig.h"

class UFLS340Config : public UFDeviceConfig {
public:
  static const float _MaxSetPnt, _NominalSetPnt;
  static const int _BadSetPnt, _BadPID;
  static const string _MaxAllowedCmd, _NominalSetPntCmd;
  UFLS340Config(const string& name= "UnknownLS340@DefaultConfig");
  UFLS340Config(const string& name, const string& tshost, int tsport);
  inline virtual ~UFLS340Config() {}

  // override these virtuals:
  virtual vector< string >& UFLS340Config::queryCmds();
  virtual vector< string >& UFLS340Config::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  // set TReCS default PID:
  static int setPID(float P= 1000.0, float I= 500.0, float D= 1000.0);

  // set TReCS standard calibration curve
  static int setCrvTReCS();
  static int setInTypTReCS();
};

#endif // __UFLS340Config_h__
