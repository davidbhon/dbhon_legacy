#if !defined(__ufgls218d_cc__)
#define __ufgls218d_cc__ "$Name:  $ $Id: ufgls218dExc.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgls218d_cc__;

#include	<exception>
#include	<stdexcept>
#include	<typeinfo>

#include "UFGemLakeShore218Agent.h"

using namespace std ;

int main(int argc, char** argv) {
  try {
    UFGemLakeShore218Agent::main(argc, argv);
  }
  catch( std::bad_alloc& e ) {
    clog<<"ufgls218d> caught bad_alloc: "<<e.what()<<endl;
    return -1;
  }
  catch( std::bad_cast& e ) {
    clog<<"ufgls218d> caught bad_cast: "<<e.what()<<endl;
    return -1;
  }
  catch( std::bad_typeid& e ) {
    clog<<"ufgls218d> caught bad_typeid: "<<e.what()<<endl;
    return -1;
  }
//  catch( std::ios_base::failure& e ) {
//    clog<<"ufgls218d> caught ios_base::failure: "<<e.what()<<endl;
//    return -1;
//  }
  catch( std::bad_exception& e ) {
    clog<<"ufgls218d> caught bad_exception: "<<e.what()<<endl;
    return -1;
  }
  catch( std::range_error& e ) {
    clog<<"ufgls218d> caught range_error: "<<e.what()<<endl;
    return -1;
  }
  catch( std::overflow_error& e ) {
    clog<<"ufgls218d> caught overflow_error: "<<e.what()<<endl;
    return -1;
  }
  catch( std::underflow_error& e ) {
    clog<<"ufgls218d> caught underflow_error: "<<e.what()<<endl;
    return -1;
  }
  catch( std::exception& e ) {
    clog<<"ufgls218d> caught STD exception: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgls218d> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgls218d_cc__
