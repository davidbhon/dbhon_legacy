
#include	"string"
#include	"strstream"
#include	"iostream"
#include	"algorithm"

#include	"cstdlib"

#include	"UFStringTokenizer.h"
#include	"UFNamedPosition.h"

using std::string ;
using std::strstream ;
using std::ends ;
using std::clog ;
using std::endl ;

UFNamedPosition::UFNamedPosition( const UFStringTokenizer& st )
{
positionNumber = static_cast< unsigned int >(
	atoi( st[ 0 ].c_str() ) ) ;
offset = atoi( st[ 1 ].c_str() ) ;

strstream s1 ;
s1	<< st[ 2 ] << ends ;
s1	>> throughput ;
delete[] s1.str() ;

strstream s2 ;
s2	<< st[ 3 ] << ends ;
s2	>> lambdaLo ;
delete[] s2.str() ;

strstream s3 ;
s3	<< st[ 4 ] << ends ;
s3	>> lambdaHi ;
delete[] s3.str() ;

positionName = st[ 5 ] ;

// The comment field can be multiple tokens
if( st.size() >= 7 )
	{
	// (comment) included
	comment = st.assemble( 6 ) ;
	if( comment.empty() )
		{
		clog	<< "UFNamedPosition> Empty comment field"
			<< endl ;
		return ;
		}

	// comment must be of this form: "(comment)"
	// Verify that there is exactly one ( and exactly one )
	if( 1 != std::count( comment.begin(), comment.end(), '(' ) )
		{
		clog	<< "UFNamedPosition> Invalid syntax for "
			<< "named position comment: "
			<< comment
			<< endl ;
		return ;
		}
	if( 1 != std::count( comment.begin(), comment.end(), ')' ) )
		{
		clog	<< "UFNamedPosition> Invalid syntax for "
			<< "named position comment: "
			<< comment
			<< endl ;
		return ;
		}

	// Verify that the ( and the ) are in the proper locations
	if( 0 != comment.find( '(' ) )
		{
		clog	<< "UFNamedPosition> Invalid syntax for "
			<< "named position comment: "
			<< comment
			<< endl ;
		return ;
		}

	if( comment.find( ')' ) != (comment.size() - 1) )
		{
		clog	<< "UFNamedPosition> Invalid syntax for "
			<< "named position comment: "
			<< comment
			<< endl ;
		return ;
		}

	// The comment appears to be of the correct form
	// Trim the ( and )
	comment.erase( comment.begin() ) ;
	comment.erase( comment.size() - 1 ) ;

	} // if( 7 == st.size() )
} // UFNamedPosition()

UFNamedPosition::UFNamedPosition( const UFNamedPosition& rhs )
 : positionNumber( rhs.positionNumber ),
   offset( rhs.offset ),
   throughput( rhs.throughput ),
   lambdaLo( rhs.lambdaLo ),
   lambdaHi( rhs.lambdaHi ),
   positionName( rhs.positionName ),
   comment( rhs.comment )
{}

UFNamedPosition::~UFNamedPosition()
{
/* No heap space allocated */
//clog	<< "~UFNamedPosition" << endl ;
}

UFNamedPosition& UFNamedPosition::operator=( const UFNamedPosition& rhs )
{
positionNumber = rhs.positionNumber ;
offset = rhs.offset ;
throughput = rhs.throughput ;
lambdaLo = rhs.lambdaLo ;
lambdaHi = rhs.lambdaHi ;
positionName = rhs.positionName ;
comment = rhs.comment ;

return *this ;
}
