#if !defined(__ufmotord_cc__)
#define __ufmotord_cc__ "$Name:  $ $Id: ufmotord.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufmotord_cc__;

#include "UFPortescapAgent.h"

int main(int argc, char** argv) {
  return UFPortescapAgent::main(argc, argv);
}
      
#endif // __ufmotord_cc__
