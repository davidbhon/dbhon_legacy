#if !defined(__ufgmotord_cc__)
#define __ufgmotord_cc__ "$Name:  $ $Id: ufgmotord.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgmotord_cc__;

#include "UFGemPortescapAgent.h"

int main(int argc, char** argv) {
  try {
    UFGemPortescapAgent::main(argc, argv);
  }
  catch( std::exception& e ) {
    clog<<"ufgmotord> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgmotord> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgmotord_cc__
