#!/bin/tcsh -f
# rcsId = $Name:  $ $Id: ufsimagents,v 0.26 2003/10/24 21:53:31 varosi Exp $
#
# note that this script should be used by the TReCS Executive daemon (ufgtrecs) for starting/stopping, i.e.
# performiing "soft reboots" of the system
# start a specific agent via first cmd line parameter: "ufsimagents ufagentname ...usual params..."
# start all agents, including trecs executive via d cmd line parameter: "ufsimagents -all ...usual params..."
# start all agents, excluding trecs executive via first cmd line parameter: "ufsimagents -ex ..usual params...."
if( ! $?UFINSTALL ) setenv UFINSTALL /usr/local/uf
#setenv LD_LIBRARY_PATH ${UFINSTALL}/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv
#set path = (./ ~/bin $UFINSTALL/bin $UFINSTALL/sbin /usr/local/bin /opt/EDTpdv /usr/X11R6/bin /usr/java/bin /bin /etc /usr/bin /sbin /usr/sbin)
source $UFINSTALL/.ufcshrc
set slp = 0.7
# just list agents this can start:
if( "$1" == "-h" || "$1" == "-help" ) then
  echo '"ufsimagents [-all]" -- starts all agents with default runtime options'
  echo '"ufsimagents -l/ls/list" -- list installed agents'
#  echo '"ufsimagents -ex" -- start all agents except the executive'
  echo '"ufsimagents ufagentname" -- start only specified agent'
  exit
endif
if( "$1" == "-l" || "$1" == "-ls" || "$1" == "-list" ) then
  \ls -lqF $UFINSTALL/bin/ufacqframed
  \ls -lqF $UFINSTALL/bin/ufgdhsd
  \ls -lqF $UFINSTALL/bin/ufgls218d
  \ls -lqF $UFINSTALL/bin/ufgls340d
  \ls -lqF $UFINSTALL/bin/ufg354vacd
  \ls -lqF $UFINSTALL/bin/ufgmotord
  \ls -lqF $UFINSTALL/bin/ufgmce4d
  \ls -lqF $UFINSTALL/bin/ufgtrecsd
  exit
endif
if( "$1" == "" || "$1" == "-all" ) then
  set epics = "-sim -epics miri "
else if( "$1" == "-noepics" ) then
  set epics = "-sim -noepics"
else
  set epics = "-sim"
endif
set jd = `date "+%Y:%j:%H:%M"`
set osname = `uname`
set agent = "unknown"
# presumably all agent/daeomn names start with uf and end with d:
if( "$1" == "ufacqframed" ) then 
  mkdir -p /data/trecs
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "$argv[2-]"
endif
if( "$1" == "ufgdhsd" ) then 
  $UFINSTALL/bin/dhsClientBoot start
  set agent = ${UFINSTALL}/bin/"$1"
  set params = -dhs reggie "$argv[2-]"
endif
if( "$1" == "ufgls218d" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost trecsperle -tsport 7002 $argv[2-]"
endif
if( "$1" == "ufgls340d" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost trecsperle -tsport 7003 $argv[2-]"
endif
if( "$1" == "ufg354vacd" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost trecsperle -tsport 7004 $argv[2-]"
endif
if( "$1" == "ufgmotord" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost trecsperle -tsport 7005 $argv[2-]"
endif
if( "$1" == "ufgmce4d" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost trecsperle -tsport 7008 $argv[2-]"
endif
if( -x $agent && "$agent" != "unknown" ) then
  $agent $epics $params >& /usr/tmp/$1.${jd} &
  echo UF agents currently running:
  if( "$osname" == "SunOS" ) then
    ps -ef | grep $UFINSTALL | grep uf | grep d | grep -v jei
  else
    ps -efw | grep $UFINSTALL | grep uf | grep d | grep -v jei
  endif
  exit
endif
# is there is no leading dash, assume this is not an option, rather
# just a typo or mispelled arf
set arg = `echo $1 | egrep -v '\-'`
if( "$arg" != "" ) then
  echo Sorry, "$1" '(' "$arg" ')' is ambiguous...
  exit
endif
# if we get here start all or all but executive
# frame acquisition daemon (in sim mode, ignore the EDT card):
mkdir -p /data/trecs
$UFINSTALL/bin/ufacqframed -s $argv[1-] >& /usr/tmp/ufacqframed.${jd} &
#
# lakeshore 340 (array temp. controller) daemon:
ufsleep $slp
$UFINSTALL/bin/ufgls340d $epics $argv[1-] -tshost trecsperle -tsport 7003 >& /usr/tmp/ufgls340d.${jd} &
#
# lakeshore 218 daemon:
ufsleep $slp
$UFINSTALL/bin/ufgls218d $epics $argv[1-] -tshost trecsperle -tsport 7002 >& /usr/tmp/ufgls218d.${jd} &
#
# granville-philips vacuum monitor daemon:
ufsleep $slp
$UFINSTALL/bin/ufg354vacd $epics $argv[1-] -tshost trecsperle -tsport 7004 >& /usr/tmp/ufg354vacd.${jd} &
#
# potescap motor indexor daemon:
ufsleep $slp
$UFINSTALL/bin/ufgmotord $epics $argv[1-] -tshost trecsperle -tsport 7005 -motors "A B C D E F G H I" >& /usr/tmp/ufgmotord.${jd} &
#
# detector controller (mce4) daemon:
# this should come up after all other device agents because it does a FITS connect on startup:
ufsleep $slp
$UFINSTALL/bin/ufgmce4d $epics $argv[1-] -tshost trecsperle -tsport 7008 >& /usr/tmp/ufgmce4d.${jd} &
# optionally start the trecs executive:
# note that the trecs executive communicates with the baytech rpc-3
# and handles requests for powerOn-Off of each/all instrument hardware component(s)
# so it is responsible for turning the cryo-cooler "coldhead" on or off:
pgrep -lf ufgtrecsd
if( $status != 0 ) then
  ufsleep $slp
  $UFINSTALL/bin/ufgtrecsd $epics $argv[1-] -baytech irbaytech -tsport 7001 >& /usr/tmp/ufgtrecsd.${jd} &
endif
#
# gemini dhs client daemon is last (since it will attempt to test the FITS transactions:
$UFINSTALL/bin/dhsClientBoot start
$UFINSTALL/bin/ufgdhsd $epics -dhs reggie $argv[1-] >& /usr/tmp/ufgdhsd.${jd} &
#
echo UF agents started:
ufsleep $slp
if( "$osname" == "SunOS" ) then
  ps -ef | grep $UFINSTALL | grep uf | grep d | grep -v jei
else
  ps -efw | grep $UFINSTALL | grep uf | grep d | grep -v jei
endif
ufpurge /data/trecs
ufpurge /usr/tmp
exit
