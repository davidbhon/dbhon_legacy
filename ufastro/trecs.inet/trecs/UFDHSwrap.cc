#if !defined(__UFDHSwrap_cc__)
#define __UFDHSwrap_cc__ "$Name:  $ $Id: UFDHSwrap.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDHSwrap_cc__;

#include "UFDHSwrap.h"
__UFDHSwrap_H__(__UFDHSwrap_cc);

#if !defined(sparc) || defined(linux)
void dhsAvAdd( DHS_AV_LIST, const char *, DHS_DATA_TYPE, int, const unsigned long *, ... ){ return; }
void dhsAvDelete( DHS_AV_LIST, const char *, DHS_STATUS * ){ return; }
DHS_AV_LIST dhsAvListNew( DHS_STATUS * ){ return DHS_S_NULL; }
extern void dhsAvListFree( DHS_AV_LIST, DHS_STATUS * ){ return; }
extern void dhsBdCtl( DHS_CONNECT, DHS_BD_CTL, ... ){ return; }
DHS_BD_FRAME dhsBdFrameNew( DHS_BD_OBJECT, const char *, int, DHS_DATA_TYPE, int, const unsigned long *, 
                            const void **, DHS_STATUS * ){ return DHS_S_NULL; }
DHS_TAG dhsBdGet( DHS_CONNECT, const char *, DHS_BD_GET_TYPE, void *, DHS_STATUS * ){ return DHS_S_NULL; }
char *dhsBdName( DHS_CONNECT, DHS_STATUS * ){ return 0; }
DHS_TAG dhsBdPut( DHS_CONNECT, const char *, DHS_BD_PUT_TYPE, DHS_BOOLEAN, ... ){ return DHS_S_NULL; }
DHS_CB_FN_PTR dhsCallbackSet( DHS_CB_TYPE, DHS_CB_FN_PTR, DHS_STATUS * ){ return 0; }
DHS_CONNECT dhsConnect( const char *, const char *, void *,  DHS_STATUS * ){ return DHS_S_NULL; }
void dhsDisconnect( DHS_CONNECT, DHS_STATUS * ) { return; }
void dhsEventLoop( DHS_EL_TYPE , ... ){ return; }
void dhsEventLoopEnd( DHS_STATUS * ){ return; }
void dhsExit( DHS_STATUS * ){ return; }
void dhsInit( const char *, const int, DHS_STATUS * ){ return; }
DHS_BOOLEAN dhsIsConnected( DHS_CONNECT, DHS_STATUS * ){ return DHS_FALSE; }
DHS_CMD_STATUS dhsStatus( DHS_TAG, char **, DHS_STATUS * ){ return DHS_CS_ERROR; }
DHS_BOOLEAN dhsTagDone( DHS_TAG, DHS_STATUS * ){ return DHS_FALSE; }
void dhsTagFree( DHS_TAG, DHS_STATUS * ){ return; }
void dhsWait( int, const DHS_TAG *, DHS_STATUS * ){ return; }
#endif

#include "UFRuntime.h"
#include "strings.h"
#include "iostream"
#include "strstream"

// statics/globals:
DHS_BD_FRAME UFDHSwrap::_dataFrame0 = DHS_AV_LIST_NULL;
int UFDHSwrap::_width;
int UFDHSwrap::_height;
string UFDHSwrap::_instrum;
string UFDHSwrap::_obsinfo;
string UFDHSwrap::_serverIp;
string UFDHSwrap::_service;

bool UFDHSwrap::_skip= true;
bool UFDHSwrap::_wait= true; // wait for completion success/failure of dhs transaction
bool UFDHSwrap::_verbose= false;
bool UFDHSwrap::_writeDD= false;
DHS_STATUS UFDHSwrap::_status = DHS_S_SUCCESS;
DHS_CONNECT UFDHSwrap::_connection= 0;
int UFDHSwrap::_archvFrmCnt= 0;
int UFDHSwrap::_totalArchvFrms= 1;
int UFDHSwrap::_qlookFrmCnt= 0; // sum over all qlook streams
string UFDHSwrap::_archvDataLabel;
// note static string initialization works on Solaris, but not always on Linux 
string UFDHSwrap::_finalKeyExt;
vector< string > UFDHSwrap::_QlookNames;
map< string, int > UFDHSwrap::_QlookFrmCnt;
map< string, UFDHSwrap::Stream* > UFDHSwrap::_QlookStrms; // for all q-look streams
map< string, string > UFDHSwrap::_DataTypes; // for FITS datatypes
// these globals/statics are for the _get callback:
void* UFDHSwrap::_DataSetRaw= 0;
unsigned char* UFDHSwrap::_DataSetFITS= 0;
int UFDHSwrap::_DataSize= 0;

// hiden mutex to try to deal with thread-safety in stdlib
static pthread_mutex_t _attribmutex;
static pthread_mutex_t _framemutex;
//static pthread_mutex_t _datasetmutex;

UFDHSwrap::UFDHSwrap(const string& instrum, int w, int h) {
  _width= w; _height = h; _instrum = instrum; _archvDataLabel = ""; _finalKeyExt = "END";
}

// error callback:
void UFDHSwrap::_dhsError(DHS_CONNECT connect, DHS_STATUS errorNum, DHS_ERR_LEVEL errorLev,
                          char *msg, DHS_TAG tag, void *userData) {
  clog<<"UFDHSwrap::_dhsError> errorNum: "<<errorNum<<", errorLev: "<<errorLev<<", msg: "<<msg<<endl;
}

// convenience funcs.
int UFDHSwrap::setQlNames(const vector< string >& qlnames) {
  for( int i= 0; i < (int)qlnames.size(); ++i ) {
    string qnam = qlnames[i];
    _QlookNames.push_back(qnam); _QlookFrmCnt[qnam] = 0;
  }
  return (int)_QlookNames.size();
}

int UFDHSwrap::setQlNames(const string& instrum, const vector< string >& qlnames) {
  for( int i= 0; i < (int)qlnames.size(); ++i ) {
    string qnam = qlnames[i];
    if( qnam.find(instrum) == string::npos ) qnam = instrum + ":" + qnam;
    _QlookNames.push_back(qnam); _QlookFrmCnt[qnam] = 0;
  }
  return (int)_QlookNames.size();
}

void UFDHSwrap::setArchvFrmCnt(int frmcnt, int total, const string& epicsChan) {
  if( epicsChan == "" ) {
    _archvFrmCnt = frmcnt;
    _totalArchvFrms = total;
    return;
  }
  // otherwise, assume the epics db rec. exists, and caput...
  // TBD
  return;
}

int UFDHSwrap::getArchvFrmCnt(const string& epicsChan) { 
  if( epicsChan == "" )
    return _archvFrmCnt;

  // otherwise, assume the epics db rec. exists, and contains the right info, and caget...
  // TBD
  return _archvFrmCnt;
}

void UFDHSwrap::setQlookFrmCnt(int frmcnt, const string& qname) {
  map< string, int >::iterator q = _QlookFrmCnt.find(qname);
  if( q == _QlookFrmCnt.end() ) // not found
    return;
  _QlookFrmCnt[qname] = frmcnt; 
  _qlookFrmCnt += frmcnt;
  return;
}

int UFDHSwrap::getQlookFrmCnt(const string& qname) {
  map< string, int >::iterator q = _QlookFrmCnt.find(qname);
  if( q == _QlookFrmCnt.end() ) // not found
    return -1;
  return _QlookFrmCnt[qname];
}

DHS_STATUS UFDHSwrap::open(const string& serverIp, const string& service) {
  ::pthread_mutex_init(&_attribmutex, 0);
  //  ::pthread_mutex_init(&_datasetmutex, 0);
  ::pthread_mutex_init(&_framemutex, 0);
  char clienthost[MAXNAMLEN + 1]; ::memset(clienthost, 0, sizeof(clienthost));
  ::gethostname(clienthost, sizeof(clienthost));

  if( _verbose )
    clog<<"UFDHSwrap::open> dhsInit "<<serverIp<<", "<<service<<endl;

  ::dhsInit(clienthost, 16, &_status);
  if( _status != DHS_S_SUCCESS ) {
    clog<<"UFDHSwrap::open> dhsInit failed, status= "<<_status<<endl;
    return _status;
  }

  // set error callback
#if defined(LINUX)
  ::dhsCallbackSet(DHS_CBT_ERROR, (void (*)())UFDHSwrap::_dhsError, &_status);
#else
  ::dhsCallbackSet(DHS_CBT_ERROR, (void (*)(...))UFDHSwrap::_dhsError, &_status);
#endif

  // start dhs event loop
  ::dhsEventLoop(DHS_ELT_THREADED, 0, &_status);
  if( _status != DHS_S_SUCCESS )
    return _status;
  
  // connect
  int cnt = 0, maxtry= 3;
  do {
    if( _verbose )
      clog<<"UFDHSwrap::open> dhsConnect "<<serverIp<<", "<<service<<endl;
    _connection = ::dhsConnect(serverIp.c_str(), service.c_str(), 0, &_status);

    DHS_BOOLEAN connected = ::dhsIsConnected(_connection, &_status);
    if( !connected && maxtry > 1 ) {
      clog<<"UFDHSwrap::open> connection failed; try again..."<<endl;
      ::sleep(1);
    }
    else if( connected ) {
      if( _verbose )
        clog<<"UFDHSwrap::open> connection succeeded."<<endl;
      _serverIp = serverIp; _service = service;
    }
  } while ( _connection <= 0 && ++cnt < maxtry );
  if( _connection <= 0 )
    clog<<"UFDHSwrap::open> failed to connect after "<<cnt<<" tries, giving up..."<<endl;

  cnt = UFDHSwrap::_initDataTypes(_instrum);
  if( _verbose )
    clog<<"UFDHSwrap::open> found "<<cnt<<" keywords in libdd.config, datatypes initialized..."<<endl;

  return _status;
} // open

DHS_STATUS UFDHSwrap::reconnect(int tryCnt) {
  // connection lost? 
  _connection = ::dhsConnect(_serverIp.c_str(), _service.c_str(), 0, &_status);
  DHS_BOOLEAN connected = ::dhsIsConnected(_connection, &_status);

  for( int ntry= 0; ntry < tryCnt && !connected; ++ntry ) {
    clog<<"UFDHSwrap::open> reconnect failed. sleep a bit and try again; try/cnt= "<<ntry<<" / "<<tryCnt<<endl;
    sleep(5);
    _connection = ::dhsConnect(_serverIp.c_str(), _service.c_str(), 0, &_status);
    connected = dhsIsConnected(_connection, &_status);
  }

  return _status;
} // reconnect

DHS_STATUS UFDHSwrap::close() {
  _status = DHS_S_SUCCESS;
  ::dhsDisconnect(_connection, &_status);
  ::dhsEventLoopEnd(&_status);
  ::dhsExit(&_status);
  if(_verbose )
    clog<<"UFDHSwrap::close> disconnected from DHS and exited dhsEventLoop..."<<endl;
  return _status;
} // close

string UFDHSwrap::newlabel() {
  string label = "";
  if( _connection == 0 )
    return label;

  char *name = ::dhsBdName(_connection, &_status);
  if( name != 0  && _status == DHS_S_SUCCESS )
    label = name; // memory leak?

  return label;
} // newlabel

DHS_STATUS UFDHSwrap::setAttributes(const vector< const string* >& fitsHdr, DHS_AV_LIST& av, int hdrtyp) {
  _status = DHS_S_SUCCESS;
  if( ::pthread_mutex_lock(&_attribmutex) < 0 ) {
    _status = DHS_S_NULL;
    return _status;
  }

  // Mandatory dataSet header -- still needed?
  string key, val;
  key= "instrument"; val = _instrum.c_str();
  //if(_verbose )
   // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", val= "<<val<<endl;
  ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
  if( _status != DHS_S_SUCCESS )
    clog<<"UFDHSwrap::setAttributes> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  else if(_verbose )
    clog<<"UFDHSwrap::setAttributes> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

  // decode header provided as string* vec
  map< string, string > svals, comments;
  map< string, int > ivals;
  map< string, float > fvals;
  int cnt= 0;
  if( hdrtyp == UFDHSwrap::Final ) {
    cnt = decodeFinalFITS(fitsHdr, svals, ivals, fvals, comments);
  }
  else if( hdrtyp == UFDHSwrap::Initial ) {
    cnt = decodeInitialFITS(fitsHdr, svals, ivals, fvals, comments);
  }
  else { // assume All or Extension
    cnt = decodeFITS(fitsHdr, svals, ivals, fvals, comments);
  }
  if( _verbose )
    clog<<"UFDHSwrap::setAttributes> cnt= "<<cnt<<", svals: "<<svals.size()<<", ivals: "<<ivals.size()<<", fvals: "<<fvals.size()<<endl;
  if( cnt <= 0 ) {
    clog<<"UFDHSwrap::setAttributes> empty FITS header ? No AttribAdd ..."<<endl;
    ::pthread_mutex_unlock(&_attribmutex);
    return _status;
  }

  map< string, int >::iterator iti = ivals.begin();
  while( iti != ivals.end() ) {
    string key = iti->first;
    int val = iti->second;
    //if(_verbose )
     // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", int val= "<<val<<endl;
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_INT32, 0, 0, val, &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"UFDHSwrap::setAttributese> Failed AttribAdd of key= "<<key<<", int val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if( _verbose )
      clog<<"UFDHSwrap::setAttributes> Succeeded AttribAdd of key= "<<key<<", int val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    
    // write out data dictionary:
    if( _writeDD )
      cout<<"OK "<<_instrum<<"\t"<<key<<"\t\tINT\t"<<key<<"\tF\tNULL\tNONE\tNULL\t\""<<comments[key]<<"\""<<endl;
    ++iti;
  }

  map< string, float >::iterator itf = fvals.begin();
  while( itf != fvals.end() ) {
    string key = itf->first;
    float val = itf->second;
    //if(_verbose )
     // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", float/double val= "<<val<<endl;
    //::dhsBdAttribAdd( av, key.c_str(), DHS_DT_FLOAT, 0, 0, val, &_status );
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_DOUBLE, 0, 0, (double)val, &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"UFDHSwrap::setAttributes> Failed AttribAdd of key= "<<key<<", float/double val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if( _verbose )
      clog<<"UFDHSwrap::setAttributes> Succeeded AttribAdd of key= "<<key<<", float/double val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    
    // write out data dictionary:
    if( _writeDD )
      cout<<"OK "<<_instrum<<"\t"<<key<<"\t\tFLOAT\t"<<key<<"\tF\tNULL\tNONE\tNULL\t\""<<comments[key]<<"\""<<endl;
    ++itf;
  }

  map< string, string >::iterator its = svals.begin();
  while( its != svals.end() ) {
    string key = its->first;
    string val = its->second;
    //if(_verbose )
     // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", string val= "<<val<<endl;
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"UFDHSwrap::setAttributes> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if( _verbose )
      clog<<"UFDHSwrap::setAttributes> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

    // write out data dictionary:
    if( _writeDD )
      cout<<"OK "<<_instrum<<"\t"<<key<<"\t\tSTRING\t"<<key<<"\tF\tNULL\tNONE\tNULL\t\""<<comments[key]<<"\""<<endl;
    ++its;
  }

  ::pthread_mutex_unlock(&_attribmutex);
  return _status;
} // setAttributes

DHS_STATUS UFDHSwrap::resetAttributes(DHS_AV_LIST& av, map< string, string >& svals,
				      map< string, int >& ivals, map< string, float >& fvals) {
  if( ::pthread_mutex_lock(&_attribmutex) < 0 ) {
    _status = DHS_S_NULL;
    return _status;
  }
  _status = DHS_S_SUCCESS;
  map< string, int >::iterator iti = ivals.begin();
  while( iti != ivals.end() ) {
    string key = iti->first;
    int val = iti->second;
    //if(_verbose )
      //clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", int val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    ::dhsBdAttribDelete( av, key.c_str(), &_status );
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_INT32, 0, 0, val, &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"setAttributese> Failed AttribAdd of key= "<<key<<", int val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if(_verbose )
      clog<<"setAttributes> Succeeded AttribAdd of key= "<<key<<", int val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

    ++iti;
  }

  map< string, float >::iterator itf = fvals.begin();
  while( itf != fvals.end() ) {
    string key = itf->first;
    float val = itf->second;
    //if(_verbose )
     // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", float/double val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    ::dhsBdAttribDelete( av, key.c_str(), &_status );
    //::dhsBdAttribAdd( av, key.c_str(), DHS_DT_FLOAT, 0, 0, val, &_status );
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_DOUBLE, 0, 0, (double)val, &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"setAttributes> Failed AttribAdd of key= "<<key<<", float/double val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if(_verbose )
      clog<<"setAttributes> Succeeded AttribAdd of key= "<<key<<", float/double val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

    ++itf;
  }

  map< string, string >::iterator its = svals.begin();
  while( its != svals.end() ) {
    string key = its->first;
    string val = its->second;
    //if(_verbose )
     // clog<<"UFDHSwrap::setAttributes> Attempt AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    ::dhsBdAttribDelete( av, key.c_str(), &_status );
    ::dhsBdAttribAdd( av, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
    if( _status != DHS_S_SUCCESS )
      clog<<"setAttributes> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
    else if(_verbose )
      clog<<"setAttributes> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

    ++its;
  }

  ::pthread_mutex_unlock(&_attribmutex);
  return _status;
} // resetAttributes

DHS_STATUS UFDHSwrap::finalDataset(const vector< const string* >& fitsHdr, string& dataLabel,
				   DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, Final); // set final value attrib.
  }
  return _status;
} // finalDataset with final attributes

DHS_STATUS UFDHSwrap::newDataset(const vector< const string* >&  fitsHdr, string& dataLabel,
				 DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = DHS_S_SUCCESS;
  _obsinfo = obsInfoFrom(fitsHdr, "OBSERVER", "OBJECT");
  newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, Initial);
  }
  return _status;
} // newDataset with initial attributes

DHS_STATUS UFDHSwrap::newDataset(string& dataLabel, DHS_BD_DATASET& dataSet, const string* quicklook) {
  /*
  if( ::pthread_mutex_lock(&_datasetmutex) < 0 ) {
    _status = DHS_S_NULL;
    return _status;
  }
  */
  _status = DHS_S_SUCCESS;
  // Setup for new frame by getting data label if none has been provided:
  bool externlabel = true;
  if( dataLabel == "" ) {
    externlabel = false;
    //char* label = ::dhsBdName( _connection, &_status ); // presumably a C stdlib malloc...
    // dataLabel = label; //::free(label); // copy and free (possible memory leak?)
    dataLabel = UFDHSwrap::newlabel();
    if( _verbose )
      clog<<"UFDHSwrap::newDataset> got label from DHS: " <<dataLabel<<endl;
  }
  else if( _verbose ) {
    clog<<"UFDHSwrap::newDataset> using externally provided label: " <<dataLabel<<endl;
  }
  if( _status != DHS_S_SUCCESS || dataLabel == "" ) {
    //::pthread_mutex_unlock(&_datasetmutex);
    clog<<"UFDHSwrap::newDataset> No label from DataSet? " <<dataLabel<<endl;
    return _status;
  }

  if( quicklook ) {
    if( _verbose )
      clog<<"UFDHSwrap::newDataset> (TEMP) Quicklook stream: "<<*quicklook<<", using label: " <<dataLabel<<endl;
    ::dhsBdCtl( _connection, DHS_BD_CTL_LIFETIME, dataLabel.c_str(), DHS_BD_LT_TEMPORARY, &_status );
    //clog<<"newDataset> (TRANS) Quicklook stream: "<<*quicklook<<", using label: " <<dataLabel<<endl;
    //::dhsBdCtl( _connection, DHS_BD_CTL_LIFETIME, dataLabel.c_str(), DHS_BD_LT_TRANSIENT, &_status );
    //clog<<"newDataset> (PERM) Quicklook stream: "<<*quicklook<<", using label: " <<dataLabel<<endl;
    //::dhsBdCtl( _connection, DHS_BD_CTL_LIFETIME, dataLabel.c_str(), DHS_BD_LT_PERMANENT, &_status );
  }
  else {
    if( _verbose )
      clog<<"UFDHSwrap::newDataset> Archive stream, using label: " <<dataLabel<<endl;
    if( !externlabel )
      ::dhsBdCtl( _connection, DHS_BD_CTL_LIFETIME, dataLabel.c_str(), DHS_BD_LT_PERMANENT, &_status );
  }
  if( _status != DHS_S_SUCCESS ) {
    //::pthread_mutex_unlock(&_datasetmutex);
    return _status;
  }

  //The CONTRIB call to dhsBdCtl gets rejected, and is not needed, so skip it.
  //  char* instrum = (char*)_instrum.c_str();
  //  char *contrib[1] = { instrum };
  //  if( !externlabel )
  //    ::dhsBdCtl( _connection, DHS_BD_CTL_CONTRIB, dataLabel.c_str(), 1, contrib, &_status  ); 

  if( quicklook ) { // quicklook stream name not null implies...
    char *quick[1]; quick[0] = (char*)quicklook->c_str();
    if( _verbose )
      clog<<"UFDHSwrap::newDataset> ::dhsBdCtl of quicklook: "<<quick[0]<<", using label: " <<dataLabel<<endl;
    ::dhsBdCtl( _connection, DHS_BD_CTL_QLSTREAM, dataLabel.c_str(), 1, quick, &_status  ); 
  }

  // create new dataSet 
  dataSet = ::dhsBdDsNew( &_status );
  if( _status != DHS_S_SUCCESS ) {
    //::pthread_mutex_unlock(&_datasetmutex);
    return _status;
  }

  // Mandatory dataSet headers
  // set instrum for every dataset
  string key, val;
  key= "instrument"; val = _instrum.c_str();
  ::dhsBdAttribAdd( dataSet, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
  if( _status != DHS_S_SUCCESS )
    clog<<"UFDHSwrap::newDataset> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  else if(_verbose )
    clog<<"UFDHSwrap::newDataset> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

  if( _archvFrmCnt > 0  )
    return _status;

  // set telescope for init. dataset
  key= "telescope"; val = "GeminiSouth";
  ::dhsBdAttribAdd( dataSet, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
  if( _status != DHS_S_SUCCESS )
    clog<<"UFDHSwrap::newDataset> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  else if( _verbose )
    clog<<"UFDHSwrap::newDataset> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;

  //::pthread_mutex_unlock(&_datasetmutex);
  return _status;
} // newDataset with (minimal) mandatory attributes

// create new frame for new dataset with minimal (mandatory?) attributes
// return ptr to data/frame allocation buffer
int* UFDHSwrap::newFrame(DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 const map< string, int >& extradims,
			 int index, int total, const string* quicklook) {
  if( ::pthread_mutex_lock(&_framemutex) < 0 ) {
    _status = DHS_S_NULL;
    return 0;
  }
  _status = DHS_S_SUCCESS;
  strstream frmname; // if available, include some information about the observer & object (obsinfo)
  frmname<<_obsinfo<<" "<<(1+index)<<" of "<<total<<ends;
  size_t extradim = extradims.size();
  if( extradim > 5 ) {
    clog<<"UFDHSwrap::newFrame> dhs supports NAXIS1-7 only, extradim: "<<extradim<<endl;
    extradim = 5;
  }  
  //size_t dim = 2; // NAXIS1,2 always present
  size_t dim = 2 + extradim; // NAXIS1,2 always present, extra axes/dims fails?
  unsigned long axisSize[dim];
  unsigned long origin[dim];
  char* axisLabel[dim];
  axisSize[0] = _width; origin[0] = 1; axisLabel[0] = "Xaxis";
  axisSize[1] = _height; origin[1] = 1; axisLabel[1] = "Yaxis";
  int totpix = _width*_height;
  map< string, int >::const_iterator eit = extradims.begin();
  for( size_t i = 0; i < extradim && eit != extradims.end(); ++i, ++eit) {
    string elbl = eit->first;
    int edim = eit->second;
    totpix = totpix * edim;
    //axisSize[1] = axisSize[1] * edim;
    axisSize[2+i] = edim; origin[2+i] = 1; axisLabel[2+i] = (char*)elbl.c_str();
  }
  int *pDhsData= 0;						
  // create frame for the given dataset; this performs the 'C' Heap malloc of the image data buffer:
  dataFrame = ::dhsBdFrameNew( dataSet, frmname.str(), index, DHS_DT_INT32, dim, axisSize, 
	                       (const void**) &pDhsData, &_status );
  /*
  if( index == 0 ) { // || _dataFrame0 == DHS_AV_LIST_NULL ) {
    dataFrame = _dataFrame0 = ::dhsBdFrameNew( dataSet, frmname.str(), index, DHS_DT_INT32, dim, axisSize, 
			                       (const void**) &pDhsData, &_status );
    if( _verbose )
      clog<<"UFDHSwrap::newFrame> frame 0 of dataSet: "<<dataSet<<", _dataFrame0: "<< _dataFrame0<<endl;
  }
  else {
    dataFrame = ::dhsBdFrameNew( _dataFrame0, frmname.str(), index-1, DHS_DT_INT32, dim, axisSize, 
    			         (const void**) &pDhsData, &_status );
    if( _verbose )
      clog<<"UFDHSwrap::newFrame> SubFrame of dataFrame0: "<< _dataFrame0<<", dataFrame: "<< dataFrame<<endl;
  }
  */
  if( _status != DHS_S_SUCCESS ) {
    delete frmname.str();
    if( pDhsData != 0 ) ::free(pDhsData);
    ::pthread_mutex_unlock(&_framemutex);
    return 0;
  }
  else {
    memset(pDhsData, 0, totpix*sizeof(int));
  }
  if( _verbose ) {
    clog<<"UFDHSwrap::newFrame> new 32bit frame allocated: "<<frmname.str()<<endl;
    size_t tot= 1;
    for( size_t i = 0; i < dim; ++i ) {
      tot = tot * axisSize[i];
      clog<<"UFDHSwrap::newFrame> Axes size(s) are: "<<axisLabel[i]<<" -- "<<axisSize[i]<<endl;
    }
    clog<<"UFDHSwrap::newFrame> total size of frame buffer (32 bit pixels): "<<tot<<endl;
    int* buf =(int*) pDhsData;
    clog<<"UFDHSwrap::newFrame> first, middle, last: "<<buf[0]<<", "<<buf[tot/2]<<", "<<buf[tot-1]<<endl;
  }
  delete frmname.str();

  // Mandatory headers?
  string key, val;
  key= "instrument"; val = _instrum.c_str();
  ::dhsBdAttribAdd( dataFrame, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
  if( _status != DHS_S_SUCCESS )
    clog<<"UFDHSwrap::newFrame> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  else if(_verbose )
    clog<<"UFDHSwrap::newFrame> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  /*
  key= "telescope"; val = "GeminiSouth";
  ::dhsBdAttribAdd( dataFrame, key.c_str(), DHS_DT_STRING, 0, 0, val.c_str(), &_status );
  if( _status != DHS_S_SUCCESS )
    clog<<"UFDHSwrap::newFrame> Failed AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  else if( _verbose )
    clog<<"UFDHSwrap::newFrame> Succeeded AttribAdd of key= "<<key<<", string val= "<<val<<", datalabel: "<<_archvDataLabel<<endl;
  */
  // Frame (extension?) header info. 
  unsigned long axisDims[1] = { dim };
  ::dhsBdAttribAdd( dataFrame, "dataType", DHS_DT_STRING, 0, 0, "Intensity", &_status );
  ::dhsBdAttribAdd( dataFrame, "origin", DHS_DT_INT32, 1, axisDims, origin, &_status );
  ::dhsBdAttribAdd( dataFrame, "axisLabel", DHS_DT_STRING, 1, axisDims, axisLabel, &_status ); 
  //if( quicklook )
    ::dhsBdAttribAdd( dataFrame, "axisSize", DHS_DT_INT32, 1, axisDims, axisSize, &_status );

  ::pthread_mutex_unlock(&_framemutex);
  return pDhsData;
} // newFrame with (minimal) mandatory attributes for dataset

// create new (2-D) frame with (minimal) mandatory attributes for existing dataset datalabel; 
int* UFDHSwrap::newFrame(string& dataLabel, DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 int index, int total, const string* quicklook) {
  // (re)create dataSet for given label
  newDataset(dataLabel, dataSet, quicklook);
  map< string, int > extradims; // none
  return newFrame(dataSet, dataFrame, extradims, index, total, quicklook);
} // newFrame for (minimal) mandatory attributes  datalabel

// create new (2to7-D) frame with (minimal) mandatory attributes for existing dataset datalabel; 
int* UFDHSwrap::newFrame(string& dataLabel, DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 const map< string, int >& extradims,
			 int index, int total, const string* quicklook) {
  // (re)create dataSet for given label
  newDataset(dataLabel, dataSet, quicklook);
  return newFrame(dataSet, dataFrame, extradims, index, total, quicklook);
} // newFrame for (minimal) mandatory attributes  datalabel

// newFrame multi-dimensional (up to 7, according to bulkdata icd)), with full attributes for datalabel
int* UFDHSwrap::newFrame(const vector< const string* >& fitsExtHdr,
                         string& dataLabel, 
			 DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 const map< string, int >& extradims,
			 int index, int total, const string* quicklook) {
  // (re)create dataSet for given label
  newDataset(dataLabel, dataSet, quicklook);
  int* pix = newFrame(dataSet, dataFrame, extradims, index, total, quicklook);
  if( fitsExtHdr.size() > 0 ) {
    clog<<"UFDHSwrap::newFrame> setting extension header attributes for new frame..."<<endl;
    setAttributes(fitsExtHdr, dataFrame, Extension);
  }
  return pix;
} // newFrame with full attributes for datalabel

// set pDhsData with data values and send to dhs
// newFrame multi-dimensional (up to 7), with full attributes for datalabel
int* UFDHSwrap::newFrame(const vector< const string* >& fitsPrmHdr,
                         const vector< const string* >& fitsExtHdr,
                         string& dataLabel, 
			 DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 const map< string, int >& extradims,
			 int index, int total, const string* quicklook) {
  // (re)create dataSet for given label
  newDataset(dataLabel, dataSet, quicklook);
  // set the _obsinfo from the primary header keys:
  _obsinfo = obsInfoFrom(fitsPrmHdr, "OBSERVER", "OBJECT");
  int* pix = newFrame(dataSet, dataFrame, extradims, index, total, quicklook);
  // need to deal with the case where the first frame is also the final frame:
  if( !quicklook && fitsPrmHdr.size() > 0) {
    if( index == 0 ) {
      setAttributes(fitsPrmHdr, dataFrame, Initial);
      //setAttributes(fitsPrmHdr, dataSet, Initial);
    }
    if( index >= total - 1 ) {
      setAttributes(fitsPrmHdr, dataFrame, Final);
      //setAttributes(fitsPrmHdr, dataSet, Final);
    }
  }
  if( fitsExtHdr.size() > 0 ) 
    setAttributes(fitsExtHdr, dataFrame, Extension);
    //setAttributes(fitsExtHdr, dataSet, Extension);
  return pix;
} // newFrame with full attributes for datalabel

string UFDHSwrap::obsInfoFrom(const vector< const string* >& fitsHdr, const string& observer, const string& target) {
  string name = "Unknown";
  map< string, string > svals, comments;
  map< string, int > ivals;
  map< string, float > fvals;
  int cnt = decodeInitialFITS(fitsHdr, svals, ivals, fvals, comments);
  string obs = svals[observer];
  obs = obs.substr(0, obs.find(" "));
  string targ = svals[target];
  targ = targ.substr(0, targ.find(" "));
  if( cnt > 2 )
   name = obs + ":" + targ;
  return name;
}

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSwrap::putArchiveFrame(const int* data, int elem, const string& dataLabel,
                	              DHS_BD_DATASET& dataSet, int* pDhsData,
			              int index, bool finalframe) {
  if( data == 0 ) {
    clog<<"UFDHSwrap::putArchiveFrame> data == 0!"<<endl;
    return DHS_E_NULLVALUE;
  }
  if( pDhsData == 0 ) {
    clog<<"UFDHSwrap::putArchiveFrame> pDhsData == 0!"<<endl;
    return DHS_E_NULLVALUE;
  }
  // copy data into dhs buff:
  ::memcpy(pDhsData, data, elem*sizeof(int));
  return _putArchiveFrame(dataLabel, dataSet, pDhsData, index, finalframe);
}

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSwrap::putArchiveFrame(const vector< int* > data, int elem, const string& dataLabel,
                	              DHS_BD_DATASET& dataSet, int* pDhsData,
			              int index, bool finalframe) {
  if( pDhsData == 0 ) { 
    clog<<"UFDHSwrap::putArchiveFrame> pDhsData == 0!"<<endl;
    return DHS_E_NULLVALUE;
  }
  // copy data into dhs buff:
  for( int i = 0; i < (int)data.size(); ++i ) {
    int* pDhs = pDhsData + i*elem;
    if( data[i] != 0 )
      ::memcpy(pDhs, data[i], elem*sizeof(int));
  }
  return _putArchiveFrame(dataLabel, dataSet, pDhsData, index, finalframe);
}

// once the images have been copied into pDhsData, use this:
DHS_STATUS UFDHSwrap::_putArchiveFrame(const string& dataLabel,
                	               DHS_BD_DATASET& dataSet, int* pDhsData,
			               int index, bool finalframe) { // send the data to the dhs
  strstream s;
  s << dataLabel; // and append group labels if desired ...?
  // this works -- i.e. all mef frames get inserted/merged into single fits file
  //s << ".0.0"<<ends; // all put examples show this for 'group numbers' 

  // use the index to set the group label?
  // what does this do? -- same as below!
  //s << ".0.0:" << index << ends;

  // this results on only the final mef frame in fits file!
  //s << ".0." << index << ends;

  // this results on only the final mef frame in fits file!
  //s << "." << index << ends;

  s << ends;  // just the unmodified dataset label, no group numbers
  string dataFrameLabel = s.str(); delete s.str();
  string t = UFRuntime::currentTime();
  DHS_TAG putTag;
  if( finalframe ) {
    if( _verbose )
      clog<<"UFDHSwrap::_putArchiveFrame> "<<t<<", Sending FINAL frame (index):"<<index<<", dataFrameLabel: "<<dataFrameLabel<<endl;
    putTag = ::dhsBdPut( _connection, dataFrameLabel.c_str(), DHS_BD_PT_DS, DHS_TRUE, dataSet, 0, &_status );
  }
  else {
    if( _verbose )
      clog<<"UFDHSwrap::_putArchiveFrame> "<<t<<", Sending a frame (index):"<<index<<", dataFrameLabel: "<<dataFrameLabel<<endl;
    putTag = ::dhsBdPut( _connection, dataFrameLabel.c_str(), DHS_BD_PT_DS, DHS_FALSE, dataSet, 0, &_status );
  }
  if( _status != DHS_S_SUCCESS ) {
    clog<<"UFDHSwrap::_putArchiveFrame> failed dhsBdPut, dhsStatus= "<<_status<<endl;
    ::dhsTagFree(putTag, &_status);
    return _status;
  }

  // wait for completion
  if( _wait ) {
    ::dhsWait( 1, &putTag, &_status );
    if( _status != DHS_S_SUCCESS ) {
      ::dhsTagFree(putTag, &_status);
      return _status;
    }
    char* statmsg;
    DHS_CMD_STATUS sendStatus = ::dhsStatus (putTag, &statmsg, &_status );
    if( sendStatus != DHS_CS_DONE ) {
      clog<<"UFDHSwrap::putArchiveFrame> Error(s) exporting "<<dataFrameLabel<<",  sendStatMsg: "<<statmsg<<endl;
    }
    else if( _verbose ) {
      clog<<"UFDHSwrap::putArchiveFrame> Sucessfully exported "<<dataFrameLabel<<", sendStatuMsg: "<<statmsg<<endl;
    }
  }
  ::dhsTagFree(putTag, &_status);

  t = UFRuntime::currentTime();
  if( _verbose )
    clog<<"UFDHSwrap::_putArchiveFrame> "<<t<<", completed transmission of frame, dhs(Wait)Status= "<<_status<<endl;

  return _status;
} // _putArchiveFrame


// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSwrap::putQlookFrame(const int* data, int elem,
				    const string& dataLabel, DHS_BD_DATASET& dataSet, int* pDhsData,
			            int index, bool finalframe) {
  if( data == 0 ) {
    clog<<"UFDHSwrap::putQlookFrame> data == 0!"<<endl;
    return DHS_E_NULLVALUE;
  }
  if( pDhsData == 0 ) { 
    clog<<"UFDHSwrap::putQlookFrame> pDhsData == 0!"<<endl;
    return DHS_E_NULLVALUE;
  }
  strstream s;
  // this works -- i.e. all mef frames get inserted/merged into single fits file
  s << dataLabel;
  //s << ".0.0"; // all put examples show this for 'group numbers' 
  s <<ends;
  string dataFrameLabel = s.str(); delete s.str();

  // elem indicates total in data, but really only want to send one image at a time
  int numpix = width() * height();
  if( numpix > elem ) {
    clog<<"UFDHSwrap::putQlookFrame> mismatch in data & imag dimensions: numpix= "<<numpix<<", elem= "<<elem<<endl;
    numpix = elem;
  }
  ::memcpy(pDhsData, data, numpix*sizeof(int));

  // print to stdout in readable form!
  //::dhsBdDsPrint(dataSet, &_status);
  // send the data to the dhs
  string t = UFRuntime::currentTime();
  DHS_TAG putTag;
  if( finalframe ) {
    if( _verbose )
      clog<<"UFDHSwrap::putQlookFrame> "<<t<<", Sending FINAL frame (index):"<<index<<", dataFrameLabel: "<<dataFrameLabel<<endl;
    putTag = ::dhsBdPut( _connection, dataFrameLabel.c_str(), DHS_BD_PT_DS_QL, DHS_TRUE, dataSet, 0, &_status );
  }
  else {
    if( _verbose )
      clog<<"UFDHSwrap::putQlookFrame> "<<t<<", Sending frame (index):"<<index<<", dataFrameLabel: "<<dataFrameLabel<<endl;
    putTag = ::dhsBdPut( _connection, dataFrameLabel.c_str(), DHS_BD_PT_DS_QL, DHS_FALSE, dataSet, 0, &_status );
  }
  if( _status != DHS_S_SUCCESS ) {
    clog<<"UFDHSwrap::_putQlookFrame> failed dhsBdPut, dhsStatus= "<<_status<<endl;
    ::dhsTagFree(putTag, &_status);
    return _status;
  }
 
  // wait for completion
  if( _wait ) {
    ::dhsWait( 1, &putTag, &_status );
    if( _status != DHS_S_SUCCESS ) {
      ::dhsTagFree(putTag, &_status);
      return _status;
    } 
    char* statmsg;
    DHS_CMD_STATUS sendStatus = ::dhsStatus(putTag, &statmsg, &_status );
    if( sendStatus != DHS_CS_DONE ) {
      clog<<"UFDHSwrap::putQlookFrame> Error(s) exporting "<<dataFrameLabel<<",  sendStatMsg: "<<statmsg<<endl;
    }
    else if( _verbose ) {
      clog<<"UFDHSwrap::putQuicklookFrame> sucessfully exported "<<dataFrameLabel<<",  sendStatuMsg: "<<statmsg<<endl;
    }
  }
  ::dhsTagFree(putTag, &_status);
 
  return _status;
} // putQlookFrame

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSwrap::putQlookFrame(const int* data, int elem, const vector< int >& offsets,
                                    const string& dataLabel, DHS_BD_DATASET& dataSet, int* pDhsData,
                                    int index, bool finalframe) {
  // elem indicates total in data, but really only want to send one image at a time
  for( int i= 0; i < (int) offsets.size(); ++i ) {
    // copy data into dhs buff:
    int offset = offsets[i];
    putQlookFrame(&data[offset], elem, dataLabel, dataSet, pDhsData, index+i, finalframe);
  }
  return _status;
} // putQlookFrame (vec)

void UFDHSwrap::_getDataSetFITS(DHS_CONNECT connect, DHS_TAG tag, char *label, DHS_BD_GET_TYPE type,
                                DHS_CMD_STATUS cmdStatus, char *string, DHS_AV_LIST avList,
                                void *data, unsigned long length, void *userDataFunc) {
  clog<<"UFDHSwrap::_getDataSetFITS> processing FITS get request ..."<<endl;
  if( UFDHSwrap::_DataSetFITS )
    free(UFDHSwrap::_DataSetFITS);
  // clear the globals
  UFDHSwrap::_DataSetFITS = 0;
  UFDHSwrap::_DataSize = 0; 

  if( cmdStatus == DHS_CS_DONE ) {
    clog<<"UFDHSwrap::_getDataSetFITS> got data for label: "<<label<<endl;
    UFDHSwrap::_DataSetFITS = (unsigned char*) data;
    UFDHSwrap::_DataSize = (int) length;
    if( userDataFunc )
      ( ( void (*) ( void *, unsigned long ) ) userDataFunc ) ( data, length );
  }
  else if( cmdStatus == DHS_CS_ERROR ) {
    clog<<"UFDHSwrap::_getDataSetFITS> Failed to get data for label: "<<label<<ends;
    if( string )
      clog<<" "<<string<<endl;
    else
      clog<<endl;
  }
  else if( cmdStatus == DHS_CS_BUSY ) {
    clog<<"UFDHSwrap::_getDataSetFITS> DHS indicates BUSY for get callback of label: "<<label<<ends;
    if( string )
      clog<<" "<<string<<endl;
    else
      clog<<endl;
  }
  else if( cmdStatus == DHS_CS_PENDING ) {
    clog<<"UFDHSwrap::_getDataSetFITS> DHS indicates PENDING for get callback of label: "<<label<<ends;
    if( string )
      clog<<" "<<string<<endl;
    else
      clog<<endl;
  }
  //::dhsTagFree( tag, &status );
} // _getDataSetFITS callback

unsigned char* UFDHSwrap::getData(const string& name, int& size, int tryCnt, bool hdrOnly) { 
  //DHS_BD_GET_RAW
  //DHS_BD_GET_FITS
  //DHS_BD_GET_FITS_HEADER
  // set the get callback
  //::dhsCallbackSet(DHS_CBT_GET, (DHS_CB_FN_PTR)UFDHSwrap::_getDataSetRaw, &_status);
  ::dhsCallbackSet(DHS_CBT_GET, (DHS_CB_FN_PTR)UFDHSwrap::_getDataSetFITS, &_status);

  //DHS_TAG getTag = dhsBdGet( _connection, name.c_str(), DHS_BD_GT_RAW, 0, &_status );
  DHS_TAG getTag = ::dhsBdGet( _connection, name.c_str(), DHS_BD_GT_FITS, 0, &_status );
  if( _status != DHS_S_SUCCESS ) {
    clog<<"getData> dhsBdGet call failed, status=  "<<_status<<endl;
    return 0;
  }

  DHS_CMD_STATUS getStatus;
  int tries= 0; 
  do {
    // Wait for the request to complete -- callback executed
    ::dhsWait( 1, &getTag, &_status );
    char* msg= 0;
    getStatus = ::dhsStatus( getTag, &msg, &_status );
    if( getStatus == DHS_CS_PENDING ) {
      clog<<"UFDHSwrap::getData> DHS status: PENDING "<<ends;
      if( msg )
        clog<<msg<<endl;
      else
        clog<<endl;
    }
    if( getStatus == DHS_CS_BUSY ) {
      clog<<"UFDHSwrap::getData> DHS status: BUSY "<<ends;
      if( msg )
        clog<<msg<<endl;
      else
        clog<<endl;
    }
    if( getStatus == DHS_CS_BUSY || getStatus == DHS_CS_PENDING )
      ::sleep(5); // wait a bit?
    else
      break;
  } while( ++tries <= tryCnt );

  ::dhsTagFree( getTag, &_status );

  if( _status != DHS_S_SUCCESS || UFDHSwrap::_DataSetFITS == 0 || UFDHSwrap::_DataSize <= 0 )
    return 0;

  /*
  DHS_BD_DATASET dataSet = UFDHSwrap::_DataSetRaw;
  DHS_BD_FRAME frame = ::dhsBdFrameIndex(dataSet, index, &_status);
  char *name;
  DHS_DATA_TYPE type;
  int naxis;
  unsigned long *naxes;
  void *value;
  ::dhsBdFrameInfo(frame, &name, &type, &naxis, &naxes, &value, &_status);
  */

  // if we get here, the get callback has set the globals
  size = UFDHSwrap::_DataSize;
  return (unsigned char*) UFDHSwrap::_DataSetFITS;
} // getData

// decode header provided as a string
int UFDHSwrap::decodeFITS(const string& hdr,
			  map< string, string >& svals,
			  map< string, int >& ivals,
			  map< string, float >& fvals,
			  map< string, string >& comments,
			  bool append) {
  if( !append ) {
    svals.clear();  ivals.clear(); fvals.clear();  comments.clear(); 
  }
  //if( _verbose )
  // clog<<"UFDHSwrap::decodeFITS> "<<hdr<<endl;

  int len = hdr.length();
  for( int cnt = 0; cnt < len; cnt += 80 ) {
    string s = hdr.substr(cnt, 80);
    string key = s.substr(0,8);
    char* cs = (char*)key.c_str();
    char* css = index(cs, ' ');
    if( css ) *css = '\0'; // eliminate trailing white spaces?
    key = cs;
    if( key.length() <= 1 || key.find("SIMPLE") == 0 || key.find("END") == 0 ||
	key.find("TELESCOP") == 0 || key.find("INSTRUM") == 0 || key.find("ORIGIN") == 0 ||
	key.find("NAXIS") == 0 || key.find("BITPIX") == 0 )
      continue; // skip these keys
    string val = s.substr(9, 21);
    cs = (char*)val.c_str();
    bool isString = false;
    while( *cs == '\'' || *cs == '\"' || *cs == ' ' || *cs == '\t' ) {
      if( *cs == '\'' || *cs == '\"' ) isString = true; 
      ++cs; // skip whites or quotes
    }
    val = cs; cs = (char*)val.c_str(); size_t q = val.find("\'");
    if( q != string::npos ) // eliminate final quote
      val = val.substr(0, q);
    string comment = s.substr(32);
    comments[key] = comment;
    DHS_DATA_TYPE dtyp = _dataType(key, _instrum);
    if( dtyp == DHS_DT_NONE && _skip ) // datatype not found in table, skip?
      continue;
    if( dtyp == DHS_DT_NONE && isString ) // datatype not found in table?
      dtyp = DHS_DT_STRING; // ok to assume string
    // text ?
    if( dtyp == DHS_DT_STRING ) {
      svals[key] = val;
      //clog<<"UFDHSwrap::decodeFITS> string key: "<<key<<", val: "<<val<<endl;
    }
    // is it a floating point?
    else if( dtyp == DHS_DT_DOUBLE || dtyp == DHS_DT_FLOAT ) {
      fvals[key] = atof(val.c_str());
      //clog<<"UFDHSwrap::decodeFITS> float key: "<<key<<", val: "<<val<<endl;
    }
    // or it integer?
    else if( dtyp == DHS_DT_INT32 || dtyp == DHS_DT_UINT32 ) {
      ivals[key] = atoi(val.c_str());
      //clog<<"UFDHSwrap::decodeFITS> int key: "<<key<<", val: "<<val<<endl;
    }
    else { // make a guess ?
      if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
      	  val.find(".") != string::npos && val.find(":") == string::npos )
	// digit(s) followed by . indicates float (but no : or -)
        fvals[key] = atof(val.c_str());
      else if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
               val.find("-") != 0 && val.find(":") == string::npos )
        // digit(s) not followed by . indicates int (but no other -)
        ivals[key] = atoi(val.c_str());
      else  // assume string (any value, or date, etc.)
        svals[key] = val;
    }
  }
  //if( _verbose )
  //clog<<"UFDHSwrap::decodeFITS> strings: "<< svals.size()<<", ints: "<<ivals.size()<<", floats: "<<fvals.size()<<endl;

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode FITS from string

// decode header provided as const vector< const string* >
int UFDHSwrap::decodeFITS(const vector< const string* >& hdr,
			  map< string, string >& svals,
			  map< string, int >& ivals,
			  map< string, float >& fvals,
			  map< string, string >& comments) {
  for( size_t i= 0; i < hdr.size(); ++i )
    decodeFITS(*hdr[i], svals, ivals, fvals, comments); // default here should be to append

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode FITS from UFStrings

int UFDHSwrap::decodeInitialFITS(const vector< const string* >& hdr,
				 map< string, string >& svals,
				 map< string, int >& ivals,
				 map< string, float >& fvals,
				 map< string, string>& comments)  {
  svals.clear();  ivals.clear(); fvals.clear(); 
  comments.clear(); 
  size_t cnt = hdr.size();
  for( size_t i = 0; i < cnt; ++i ) {
    const string& s = *hdr[i];
    //if( _verbose )
    //clog<<"UFDHSwrap::decodeInitialFITS> "<<s<<endl;
    string key = s.substr(0,8);
    char* cs = (char*)key.c_str();
    char* css = index(cs, ' ');
    if( css ) *css = '\0'; // eliminate trailing white spaces?
    key = cs;
    if( key.length() <= 1 || key.find("SIMPLE") == 0 || key.find("END") == 0 ||
	key.find("TELESCOP") == 0 || key.find("INSTRUM") == 0 || key.find("ORIGIN") == 0 ||
	key.find("NAXIS") == 0 || key.find("BITPIX") == 0 )
      continue; // skip these keys
    if( key.rfind(_finalKeyExt) != string::npos && 
	key.rfind(_finalKeyExt) > 0 && key.rfind(_finalKeyExt) < 8 )
      continue; // skip final keys
    string val = s.substr(9, 21);
    cs = (char*)val.c_str();
    bool isString = false;
    while( *cs == '\'' || *cs == '\"' || *cs == ' ' || *cs == '\t' ) {
      if( *cs == '\'' || *cs == '\"' ) isString = true; 
      ++cs; // skip whites or quotes
    }
    val = cs; cs = (char*)val.c_str(); size_t q = val.find("\'");
    if( q != string::npos ) // eliminate final quote
      val = val.substr(0, q);
    string comment = s.substr(32);
    comments[key] = comment;
    DHS_DATA_TYPE dtyp = _dataType(key, _instrum);
    if( dtyp == DHS_DT_NONE && _skip ) // datatype not found in table, skip?
      continue;
    if( dtyp == DHS_DT_NONE && isString ) // datatype not found in table, use it anyway?
      dtyp = DHS_DT_STRING; // ok to assume string
    // text ?
    if( dtyp == DHS_DT_STRING ) {
      svals[key] = val;
      //clog<<"UFDHSwrap::decodeInitialFITS> string key: "<<key<<", val: "<<val<<endl;
    }
    // is it a floating point?
    else if( dtyp == DHS_DT_DOUBLE || dtyp == DHS_DT_FLOAT ) {
      fvals[key] = atof(val.c_str());
      //clog<<"UFDHSwrap::decodeInitialFITS> float key: "<<key<<", val: "<<val<<endl;
    }
    // or it integer?
    else if( dtyp == DHS_DT_INT32 || dtyp == DHS_DT_UINT32 ) {
      ivals[key] = atoi(val.c_str());
      //clog<<"UFDHSwrap::decodeInitialFITS> int key: "<<key<<", val: "<<val<<endl;
    }
    else { // make a guess
      if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
      	  val.find(".") != string::npos && val.find(":") == string::npos )
	// digit(s) followed by . indicates float (but no : or -)
        fvals[key] = atof(val.c_str());
      else if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
               val.find("-") != 0 && val.find(":") == string::npos )
        // digit(s) not followed by . indicates int (but no other -)
        ivals[key] = atoi(val.c_str());
      else  // assume string (any value, or date, etc.)
        svals[key] = val;
    }
  }
  //if( _verbose )
  //  clog<<"UFDHSwrap::decodeInitialFITS> strings: "<< svals.size()<<", ints: "<<ivals.size()<<", floats: "<<fvals.size()<<endl;

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode (OSCIR type) primary header initial attributes from UFStrings

// decode header provided as a UFStrings
int UFDHSwrap::decodeFinalFITS(const vector< const string* >& hdr,
			       map< string, string >& svals,
			       map< string, int >& ivals,
			       map< string, float >& fvals,
			       map< string, string>& comments)  {
  svals.clear();  ivals.clear(); fvals.clear(); 
  comments.clear(); 
  size_t cnt = hdr.size();
  for( size_t i = 0; i < cnt; ++i ) {
    const string& s = *hdr[i];
    //if( _verbose )
    //clog<<"UFDHSwrap::decodeFinalFITS> "<<s<<endl;
    string key = s.substr(0,8);
    char* cs = (char*)key.c_str();
    char* css = index(cs, ' ');
    if( css ) *css = '\0'; // eliminate trailing white spaces?
    key = cs;
    if( key.length() <= 1 || key.find("SIMPLE") == 0 || key.find("END") == 0 ||
	key.find("TELESCOP") == 0 || key.find("INSTRUM") == 0 || key.find("ORIGIN") == 0 ||
	key.find("NAXIS") == 0 || key.find("BITPIX") == 0 )
      continue; // skip these two keys
    if( key.rfind(_finalKeyExt) == string::npos )
      continue; // skip non-final keys
    string val = s.substr(9, 21);
    cs = (char*)val.c_str();
    bool isString = false;
    while( *cs == '\'' || *cs == '\"' || *cs == ' ' || *cs == '\t' ) {
      if( *cs == '\'' || *cs == '\"' ) isString = true; 
      ++cs; // skip whites or quotes
    }
    val = cs; cs = (char*)val.c_str(); size_t q = val.find("\'");
    if( q != string::npos ) // eliminate final quote
      val = val.substr(0, q);
    string comment = s.substr(32);
    comments[key] = comment;
    DHS_DATA_TYPE dtyp = _dataType(key, _instrum);
    if( dtyp == DHS_DT_NONE && _skip ) // datatype not found in table, skip?
      continue;
    if( dtyp == DHS_DT_NONE && isString ) // datatype not found in table?
      dtyp = DHS_DT_STRING; // ok to assume string
    // text ?
    if( dtyp == DHS_DT_STRING ) {
      svals[key] = val;
      //clog<<"UFDHSwrap::decodeFinalFITS> string key: "<<key<<", val: "<<val<<endl;
    }
    // is it a floating point?
    else if( dtyp == DHS_DT_DOUBLE || dtyp == DHS_DT_FLOAT ) {
      fvals[key] = atof(val.c_str());
      //clog<<"UFDHSwrap::decodeFinalFITS> float key: "<<key<<", val: "<<val<<endl;
    }
    // or it integer?
    else if( dtyp == DHS_DT_INT32 || dtyp == DHS_DT_UINT32 ) {
      ivals[key] = atoi(val.c_str());
      //clog<<"UFDHSwrap::decodeFinalFITS> int key: "<<key<<", val: "<<val<<endl;
    }
    else { // make a guess
      if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
      	  val.find(".") != string::npos && val.find(":") == string::npos )
	// digit(s) followed by . indicates float (but no : or -)
        fvals[key] = atof(val.c_str());
      else if( ( isdigit(*cs) || (cs[0] == '-' || cs[0] == '+' && isdigit(cs[1])) ) &&
               val.find("-") != 0 && val.find(":") == string::npos )
        // digit(s) not followed by . indicates int (but no other -)
        ivals[key] = atoi(val.c_str());
      else  // assume string (any value, or date, etc.)
        svals[key] = val;
    }
  }
  //if( _verbose )
  //  clog<<"UFDHSwrap::decodeFinalFITS> strings: "<< svals.size()<<", ints: "<<ivals.size()<<", floats: "<<fvals.size()<<endl;

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode (OSCIR type) primary header finaal attributes from UFStrings

DHS_DATA_TYPE UFDHSwrap::_dataType(const string& key, const string& instrum) {
  // only TReCS supported now...
  if( instrum.find("*") != string::npos ) {
    if( _verbose ) 
      clog<<"UFDHSwrap::_dataTypes> Ok, support: "<<instrum<<", key: "<<key<<endl;
  }
  else if( instrum.find("trecs") != string::npos || instrum.find("Trecs") != string::npos ||
           instrum.find("TReCS") != string::npos || instrum.find("TRECS") != string::npos  ) {
    if( _verbose ) 
      clog<<"UFDHSwrap::_dataTypes> Ok, support: "<<instrum<<", key: "<<key<<endl;
  }
  else if( instrum.find("flam") != string::npos || instrum.find("Flam") != string::npos ||
           instrum.find("FLAM") != string::npos  ) {
    if( _verbose ) 
      clog<<"UFDHSwrap::_dataTypes> Ok, support: "<<instrum<<", key: "<<key<<endl;
  }
  else {
    clog<<"UFDHSwrap::_dataTypes> instrum not supported: "<<instrum<<", key: "<<key<<endl;
    return DHS_DT_NONE;
  }
  // locate entree in libdd for TReCS:
  if( _DataTypes.empty() ) // need to init...
    _initDataTypes(instrum);

  if(  _DataTypes.find(key) != _DataTypes.end() ) {
    string dt = _DataTypes[key];
    if( dt.find("STRING") != string::npos ) {
      //clog<<"UFDHSwrap::_dataTypes> dt is "<<dt<<endl;
      return DHS_DT_STRING;
    }
    else if( dt.find("INT") != string::npos ) { 
      //clog<<"UFDHSwrap::_dataTypes> dt is "<<dt<<endl;
      return DHS_DT_INT32;
    }
    else if( dt.find("FLOAT") != string::npos ) {
      //clog<<"UFDHSwrap::_dataTypes> dt is "<<dt<<endl;
      return DHS_DT_FLOAT;
    }
    else if( dt.find("DOUBLE") != string::npos ) {
      //clog<<"UFDHSwrap::_dataTypes> dt is "<<dt<<endl; 
      return DHS_DT_DOUBLE;
    }
    else {
      //clog<<"UFDHSwrap::_dataTypes> assume dt is STRING"<<endl;
      return DHS_DT_STRING;
    }
  }
  // not found...
  //clog<<"UFDHSwrap::_dataTypes> key: "<<key<<" not found in DataTypes table, assume string..."<<endl;
  //return DHS_DT_STRING;
  if( _verbose )
    clog<<"UFDHSwrap::_dataTypes> key: "<<key<<" not found in DataTypes table..."<<endl;
  return DHS_DT_NONE;
}

int UFDHSwrap::_initDataTypes(const string& instrum) {
  string dlist = "grep -i ";
  dlist += instrum + " ";
  dlist += getenv("UFINSTALL");
  dlist += "/etc/libdd.config | awk '{print $5\":\"$4}' | sort -u";
  if( _verbose ) 
    clog<<"UFDHSwrap::_initDataTypes> parsing libdd.conf via popen of: "<<dlist<<endl;
  FILE* fp = ::popen(dlist.c_str(), "r");
  if( fp == 0 )
    return -1;

  char buff[BUFSIZ]; memset(buff, 0, sizeof(buff));
  while (fgets(buff, BUFSIZ, fp) != 0) {
    string s = buff;
    if( _verbose ) 
      clog<<"UFDHSwrap::_initDataTypes> libdd: "<<s<<endl;
    size_t coln = s.find(':');
    if( coln == string::npos )
      continue;
    string key = s.substr(0, coln);
    string typ = s.substr(1+coln);
    if( key.length() > 0 && typ.length() > 0 ) {
      _DataTypes[key] = typ;
      if( _verbose ) 
        clog<<"UFDHSwrap::_initDataTypes> key: "<<key<<", typ: "<<typ<<endl;
    }
  }

  ::pclose(fp);
  return (int)_DataTypes.size();
}
#endif // __UFDHSwrap_cc__
