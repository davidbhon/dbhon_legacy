/* Tatiane,
 *   In order to determine that my DHS installation is fully finctional,
 *   I'm simply tryimg to test with this minimal example client I got
 *   long ago from Richard Wolf, with minimal modifcations...
 *   It's not making past the dhsConnect call.
 *   --david
 */
/*
Delivered-To: hon@polaris.astro.ufl.edu
Date: Wed, 8 Dec 1999 09:01:38 -0700 (MST)
From: rwolff@noao.edu (Richard Wolff)
To: hon@polaris.astro.ufl.edu
Subject: Re: dhs

----------
X-Sun-Data-Type: text
X-Sun-Data-Description: text
X-Sun-Data-Name: text
X-Sun-Charset: us-ascii
X-Sun-Content-Lines: 23

The attached C file is what Dayle Kotturi sent to me for a dhs test.
You'll need to change 'mordred' to whatever machine you're running it from.
I believe you also need to get Dayle to authorize connection from that
machine.  But you can try it without and see what happens.  There will be
an error when the DHS complains it doesn't know about some attribute/value
pair, but that's ignorable if you're mainly concerned about connectivity
and getting things done in the right order.

We're using dhs-0.16 libraries.

You can check on the server by telnet'ing to the IP address in the file
and logging in as 'dhs' with pw '468imp@'.  cd to /var/dhs and look in
dhsData.log  (Dayle sent out some email about all this, addressed to
several people including Tom Kisko so if you don't have the email, ask him.)


If you can get this to work, then it's time to move on to real data!  I've
been writing multiple frames per dataset, and multiple datasets per data label
and I believe it all works aside from a deep bug that means I can only do
a couple of dhsPut calls before I have to do a dhsWait on those puts.  That's
being looked into.

Richard
----------
X-Sun-Data-Type: c-file
X-Sun-Data-Description: c-file
X-Sun-Data-Name: dhstests.c
X-Sun-Charset: us-ascii
X-Sun-Content-Lines: 555
*/

/*      kdk
 *      28sep98: started numbering new frames at "1" instead of "0"
 *               took out AttribAdd with "axisLabel" (JD says may be a bug)
 *               put checkRet wrapper around calls in export.
 *               included "logLib.h" so it could know about logMsg
 *               had to make export, quicklook and slowlook return
 *               a status (not void) since checkRet returns a val.
 *      29sep98: initialized mydhsStatus to DHS_S_SUCCESS prior to
 *               each lib call in routine "export". Do for the rest.
 *               took out all the '\'s at the end of the lines.
 *
 */

#ifndef VxWorks
#define logMsg  printf
#else  /* !VxWorks */
#include "vxWorks.h"
#include "taskLib.h"
#include "sysLib.h"
#include "stdio.h"
#define __PROTOTYPE_5_0
#include "logLib.h"
#endif  /* !VxWorks */
/*
#include "gemTypes.h"
*/

#include "unistd.h"
#include "stdio.h"
#include "stdlib.h"
#include "signal.h"
#include "dirent.h"
/*
 *  sds.h is only required so some SDS functions can be used directly.
 */
#include "sds.h"
#include "gen_types.h"
#include "dhs.h"

#define VOID

/* TReCS-like attributes as globals -- david */
static const int _width= 320;
static const int _height= 240;
static char* _client_hostname= 0;
static DHS_CONNECT dhsConnection;
static DHS_STATUS mydhsStatus = DHS_S_SUCCESS;

#define checkRet( fn, st, val )                                         \
        fn;                                                             \
        if ( st != DHS_S_SUCCESS )                                      \
        {                                                               \
            DHS_STATUS          stat2, errorNum;                        \
            DHS_ERR_LEVEL       errorLev;                               \
            char                *string;                                \
            stat2 = DHS_S_SUCCESS;                                      \
            while ( stat2 == DHS_S_SUCCESS )                            \
            {                                                           \
                string = dhsMessage( &errorNum, &errorLev, &stat2 );    \
                if ( stat2 == DHS_S_SUCCESS )                           \
                {                                                       \
                    logMsg( "status: %d message: %s\n", st, string );   \
                }                                                       \
                else if ( stat2 == DHS_S_NO_MESSAGE )                   \
                {                                                       \
                    stat2 = DHS_S_SUCCESS;                              \
                    break;                                              \
                }                                                       \
                else                                                    \
                {                                                       \
                    logMsg(                                             \
                         "status: %d dhsMessage failed with status: %d",\
                          st, stat2 );                                  \
                }                                                       \
                dhsMessageClear( &stat2 );                              \
            }                                                           \
            if ( dhsConnection != DHS_CONNECT_NULL )                    \
            {                                                           \
                dhsDisconnect( dhsConnection, &stat2 );                 \
                dhsConnection = DHS_CONNECT_NULL;                       \
            }                                                           \
            dhsExit( &stat2 );                                          \
            return (val);                                               \
        }


int export ()
{
    char *dataLabel;
    char label[50];
    char *axisLabel[2] = {"None", "None"};
    DHS_BD_DATASET dataset;
    DHS_BD_FRAME dataFrame;
    unsigned long dims[1];
    unsigned long axisSize[2];
    unsigned long origin[2];
    DHS_TAG putTag;
    DHS_CMD_STATUS sendStatus;
    char *msg;    
    unsigned long axisDims[1];
/*  FILE	*fp;			 File pointer to raw image 
    uint16 	*pDhsData;						*/

    float 	*pDhsData;						

    /* New, for the quick look 08oct98 dk */
    char			*qlStreams[1];
    char 			*contrib[1];

    /* Setup for new frame by getting data label */
    mydhsStatus = DHS_S_SUCCESS;
    dataLabel = dhsBdName (dhsConnection, &mydhsStatus);
    /* sprintf (label, "%s.0.0", dataLabel); */
    sprintf (label, "%s", dataLabel); 
    printf ("With dhsStatus = %d, got label = %s\n", mydhsStatus, label);

    /* Set up for the quick look */
    contrib[0] = "2DIRS";
    qlStreams[0] = "myScience";
    dhsBdCtl(dhsConnection, DHS_BD_CTL_LIFETIME, 
    dataLabel, DHS_BD_LT_PERMANENT, &mydhsStatus);
    /* dhsBdCtl(dhsConnection, DHS_BD_CTL_CONTRIB, 
    dataLabel, 1, contrib); */
    dhsBdCtl(dhsConnection, DHS_BD_CTL_QLSTREAM,
    dataLabel, 1, qlStreams, &mydhsStatus);

    /* create new dataset */
    dims[0] = 2;
    axisDims[0] = 1;
    axisSize[0] = _width;
    axisSize[1] = _height;
    origin[0] = 1;
    origin[1] = 1;
    printf ("Axis labels are '%s' and  '%s'\n", axisLabel[0], axisLabel[1]);

    dataset = dhsBdDsNew (&mydhsStatus);
    printf("And after dhsBdDsNew, dhsStatus=%d\n", mydhsStatus);

    /* Mandatory dataset headers */
    dhsBdAttribAdd (dataset, "instrument", DHS_DT_STRING, 0, NULL, "t-recs", 
		    &mydhsStatus);
    dhsBdAttribAdd (dataset, "telescope", DHS_DT_STRING, 0, NULL, "GeminiNorth", 
		    &mydhsStatus);
    printf ("After telescope attribadd, dhsStatus=%d\n", mydhsStatus);

    /* Create frame */
    dataFrame = dhsBdFrameNew (dataset, "dataArray", 1, DHS_DT_FLOAT, 2, axisSize, 
			       (const void **)&pDhsData, &mydhsStatus);
    printf ("After framenew, dhsStatus=%d\n", mydhsStatus);

    /* add frame header info. */
    dhsBdAttribAdd (dataFrame, "dataType", DHS_DT_STRING, 0, NULL, "Electrons", 
		    &mydhsStatus);
    dhsBdAttribAdd (dataFrame, "origin", DHS_DT_INT32, 1, dims, origin, 
		    &mydhsStatus);

    dhsBdAttribAdd (dataFrame, "axisSize", DHS_DT_INT32, 1, dims, axisSize, 
		    &mydhsStatus);
/*  
    dhsBdAttribAdd (dataFrame, "axisLabel", DHS_DT_STRING, 1, dims, axisLabel, 
    &mydhsStatus); 
*/
    printf ("After attribadds, dhsStatus=%d\n", mydhsStatus);

    /*
    fp = fopen("029.raw", "r");
    fread(pDhsData, 2, 480*640, fp);
    fclose(fp);
    */
    /*printf ("Create the data by reading a raw image file...\n");
    printf ("Create the data by making your own data...\n");
    for (i = 0; i < 640 * 480; i++) {
        *pDhsData++ = (float)i;
    } 

    dhsBdDsPrint (dataset, &mydhsStatus);
    */

    printf ("Sending it...\n");

    /* send the data to the dhs */
    checkRet (putTag = dhsBdPut (dhsConnection, label, DHS_BD_PT_DS, DHS_TRUE, dataset, NULL, &mydhsStatus), mydhsStatus, -21);
    printf ("After dhsBdPut, dhsStatus=%d\n", mydhsStatus);

    /* wait for completion */
    checkRet (dhsWait (1, &putTag, &mydhsStatus), mydhsStatus, -13);
    printf ("After dhsWait, dhsStatus=%d\n", mydhsStatus);

    checkRet (sendStatus = dhsStatus (putTag, &msg, &mydhsStatus), mydhsStatus, -12);
    if (sendStatus != DHS_CS_DONE) {
	printf ("Failed to export the data!\n, %s\n", msg);
	printf ("sendStatus=%d\n", sendStatus);
	printf ("%s\n", msg);
    }
    checkRet (dhsTagFree (putTag, &mydhsStatus), mydhsStatus, -13);

    logMsg("about to free dataset\n");
    checkRet (dhsBdDsFree(dataset, &mydhsStatus), mydhsStatus, -13);

    if (mydhsStatus != DHS_S_SUCCESS) {
	printf ("Bad dhs status %d \n", mydhsStatus);
        return 1;
    } else {
	printf ("Frame exported\n");
    }
    return 0;
}

void quicklook ( int nloop )
{

    int i, k, l;

/*  int             j; */
    unsigned short *pDhsData;
    unsigned short *ptr;
    char *dataLabel;
    char label[50];
    char *axisLabel[2] =
    {"None", "None"};
    DHS_BD_DATASET dataset;
    DHS_BD_FRAME dataFrame;
    unsigned long dims[1];
    unsigned long axisSize[2];
    unsigned long origin[2];
    DHS_TAG putTag;
    DHS_CMD_STATUS sendStatus;
    DHS_BOOLEAN dhsLastFlag;
    char msg[80];
    unsigned long axisDims[1];

    /* setup for new frame */
    /* get data label */
    dataLabel = dhsBdName (dhsConnection, &mydhsStatus);
    /* sprintf (label, "%s.0.0", dataLabel); */
    sprintf (label, "%s", dataLabel); */
    printf ("label = %s, dhs status = %d\n", label, mydhsStatus);

    /* create new dataset */
    dims[0] = 2;
    axisDims[0] = 1;
    axisSize[0] = _width;
    axisSize[1] = _height;
    origin[0] = 1;
    origin[1] = 1;
    printf ("Xlabel=%s Ylabel=%s\n", axisLabel[0], axisLabel[1]);

    l = 1;
    for (k = 0; k < nloop; k++) {

	l = l * (-1);

        /* Create a dataset */

	printf ("\n\nCreating dataset %d\n", k);

	dataset = dhsBdDsNew (&mydhsStatus);
	printf ("After dhsBdDsNew, dhsStatus=%d\n", mydhsStatus);

	/* Mandatory dataset headers */
	/*
	dhsBdAttribAdd (dataset, "instrument", DHS_DT_STRING, 0, NULL, "agwps0", 
			&mydhsStatus);
	*/
	dhsBdAttribAdd (dataset, "instrument", DHS_DT_STRING, 0, NULL, "t-recs", 
			&mydhsStatus);

	if (k == 0) {
	  /*
	    dhsBdAttribAdd (dataset, "telescope", DHS_DT_STRING, 0, NULL, "UKATC", 
			    &mydhsStatus);
	  */
	    dhsBdAttribAdd (dataset, "telescope", DHS_DT_STRING, 0, NULL, "GeminiSouth", 
			    &mydhsStatus);
	    printf ("After first calls to dhsBdAttribAdd, dhsStatus=%d\n", mydhsStatus);
	}
	/* Create frame */
	dataFrame = dhsBdFrameNew (dataset, "dataArray", 1, DHS_DT_UINT16, 2, axisSize, 
				   (const void **)&pDhsData, &mydhsStatus);
	printf ("After dhsBdFrameNew, dhsStatus=%d\n", mydhsStatus);

	dhsBdAttribAdd (dataFrame, "origin", DHS_DT_INT32, 1, dims, origin, 
			&mydhsStatus);
	dhsBdAttribAdd (dataFrame, "axisSize", DHS_DT_INT32, 1, dims, axisSize, 
			&mydhsStatus);

	if (k == 0) {

	    /* add frame header info. */
	    dhsBdAttribAdd (dataFrame, "dataType", DHS_DT_STRING, 0, NULL, "Electrons", 
			    &mydhsStatus);
	    dhsBdAttribAdd (dataFrame, "frameName", DHS_DT_STRING, 0, NULL, "A&G QL Test", 
			    &mydhsStatus);
	    dhsBdAttribAdd (dataFrame, "frameId", DHS_DT_STRING, 0, NULL, "1", 
			    &mydhsStatus);
/*
	    dhsBdAttribAdd (dataFrame, "axisLabel", DHS_DT_STRING, 1, dims, axisLabel, 
			    &mydhsStatus);
*/
	    printf ("After second calls to dhsBdAttribAdd, dhsStatus=%d\n", mydhsStatus);

	}
	/* Send data frame */

	printf ("Sending data frame %d...\n", k + 1);

	ptr = pDhsData;
	for (i = 0; i < _width * _height; i++) {
	    *ptr = (unsigned short)(17000 + (i + k) * l / 64);
	    /* printf ("Data[%d %d %d] = %d\n", i, k, l, (int) *ptr); */
	    ptr++;
	}

	if ((k == 0) || (k >= (nloop - 1)))
	    dhsBdDsPrint (dataset, &mydhsStatus);

	/* send the data to the dhs */
	if (k < (nloop - 1))
	    dhsLastFlag = DHS_FALSE;
	else
	    dhsLastFlag = DHS_TRUE;
    
	putTag = dhsBdPut (dhsConnection, label, DHS_BD_PT_DS_QL, dhsLastFlag, dataset, 
			   NULL, &mydhsStatus);
	printf ("After %s dhsBdPut, dhsStatus=%d\n",
	  ((dhsLastFlag == DHS_TRUE) ? "LAST" : "intermediate"), mydhsStatus);

	/* wait for completion */
	dhsWait (1, &putTag, &mydhsStatus);
	printf ("After dhsWait, dhsStatus=%d\n", mydhsStatus);

	sendStatus = dhsStatus (putTag, (char **)&msg, &mydhsStatus);
	if (sendStatus != DHS_CS_DONE) {
	    printf ("Failed to export the data!\n, %s\n", msg);
	    printf ("sendStatus=%d, dhsStatus=%d\n", sendStatus, mydhsStatus);
	    printf ("Message=%s\n", msg);
	}
	dhsTagFree (putTag, &mydhsStatus);
	printf ("After dhsTagFree, dhsStatus=%d\n", mydhsStatus);

	dhsBdDsFree (dataset, &mydhsStatus);
	printf ("After dhsBdDsFree, dhsStatus=%d\n", mydhsStatus);

    }				/* Next k */

    if (mydhsStatus != DHS_S_SUCCESS) {
	printf ("Bad dhs status %d \n", mydhsStatus);
	mydhsStatus = DHS_S_SUCCESS;
    }
    return;
}

void slowlook (
		 int nloop
)
{

    int i, j, k, l;
    float localData[3][2];
    float *pDhsData;
    float *ptr;
    char *dataLabel;
    char label[50];
    char *axisLabel[2] =
    {"None", "None"};
    DHS_BD_DATASET dataset;
    DHS_BD_FRAME dataFrame;
    unsigned long dims[1];
    unsigned long axisSize[2];
    unsigned long origin[2];

/*  float           intData[3][2]; */
    DHS_TAG putTag;
    DHS_CMD_STATUS sendStatus;

/*  DHS_BOOLEAN     dhsLastFlag; */
    char msg[80];
    unsigned long axisDims[1];

    for (i = 0; i < 3; i++) {
	for (j = 0; j < 2; j++) {
	    localData[i][j] = 1.0;
	}
    }

    l = 1;
    for (k = 0; k < nloop; k++) {

	l = l * (-1);

	/* setup for new frame */
	/* get data label */
	dataLabel = dhsBdName (dhsConnection, &mydhsStatus);
	/* sprintf (label, "%s.0.0", dataLabel); */
	sprintf (label, "%s", dataLabel);
	printf ("label %d = %s, dhs status = %d\n", k, label, mydhsStatus);

	/* create new dataset */
	dims[0] = 2;
	axisDims[0] = 1;
	axisSize[0] = 3;
	axisSize[1] = 2;
	origin[0] = 1;
	origin[1] = 1;
	printf ("Xlabel=%s Ylabel=%s\n", axisLabel[0], axisLabel[1]);

    /* Create a dataset */

	printf ("\n\nCreating dataset %d\n", k + 1);

        mydhsStatus = DHS_S_SUCCESS;
	dataset = dhsBdDsNew (&mydhsStatus);
	printf ("After dhsBdDsNew, dhsStatus=%d\n", mydhsStatus);

	/* Mandatory dataset headers */

        mydhsStatus = DHS_S_SUCCESS;
	dhsBdAttribAdd (dataset, "instrument", DHS_DT_STRING, 0, NULL, "gnaac", 
			&mydhsStatus);

        dhsBdAttribAdd (dataset, "telescope", DHS_DT_STRING, 0, NULL, "UKATC",
                            &mydhsStatus);
	printf ("After first calls to dhsBdAttribAdd, dhsStatus=%d\n", mydhsStatus);

	/* Create frame */
	dataFrame = dhsBdFrameNew (dataset, "dataArray", 1, DHS_DT_FLOAT, 2, axisSize, 
				   (const void **)&pDhsData, &mydhsStatus);
	printf ("After dhsBdFrameNew, dhsStatus=%d\n", mydhsStatus);

	dhsBdAttribAdd (dataFrame, "origin", DHS_DT_INT32, 1, dims, origin, 
			&mydhsStatus);

	/* add frame header info. */
	dhsBdAttribAdd (dataFrame, "dataType", DHS_DT_STRING, 0, NULL, "Electrons", 
			&mydhsStatus);
	dhsBdAttribAdd (dataFrame, "frameName", DHS_DT_STRING, 0, NULL, "A&G SL Test", 
			&mydhsStatus);
	dhsBdAttribAdd (dataFrame, "frameId", DHS_DT_STRING, 0, NULL, "1", 
			&mydhsStatus);
	dhsBdAttribAdd (dataFrame, "axisSize", DHS_DT_INT32, 1, dims, axisSize, 
			&mydhsStatus);
/*
	dhsBdAttribAdd (dataFrame, "axisLabel", DHS_DT_STRING, 1, dims, axisLabel, 
			&mydhsStatus); 
*/
	printf ("After second calls to dhsBdAttribAdd, dhsStatus=%d\n", mydhsStatus);

	/* Send data frame */

	printf ("Sending data frame %d...\n", k + 1);

	ptr = pDhsData;
	for (i = 0; i < 3 * 2; i++) {
	    *ptr = (float)((i + k) * l);
	    printf ("Data[%d %d %d] = %f\n", i, k, l, *ptr);
	    ptr++;
	}

	if ((k == 0) || (k >= (nloop - 1)))
	    dhsBdDsPrint (dataset, &mydhsStatus);

	/* send the data to the dhs */
	putTag = dhsBdPut (dhsConnection, label, DHS_BD_PT_DS_QL, DHS_FALSE, dataset, 
			   NULL, &mydhsStatus);
	printf ("After dhsBdPut, dhsStatus=%d\n", mydhsStatus);

	/* wait for completion */
	dhsWait (1, &putTag, &mydhsStatus);
	printf ("After dhsWait, dhsStatus=%d\n", mydhsStatus);

	sendStatus = dhsStatus (putTag, (char **)&msg, &mydhsStatus);
	if (sendStatus != DHS_CS_DONE) {
	    printf ("Failed to export the data!\n, %s\n", msg);
	    printf ("sendStatus=%d, dhsStatus=%d\n", sendStatus, mydhsStatus);
	    printf ("Message=%s\n", msg);
	}
	dhsTagFree (putTag, &mydhsStatus);
	printf ("After dhsTagFree, dhsStatus=%d\n", mydhsStatus);

	dhsBdDsFree (dataFrame, &mydhsStatus);
	printf ("After dhsBdDsFree of dataFrame, dhsStatus=%d\n", mydhsStatus);

	dhsBdDsFree (dataset, &mydhsStatus);
	printf ("After dhsBdDsFree of dataset, dhsStatus=%d\n", mydhsStatus);

    }				/* Next k */

    if (mydhsStatus != DHS_S_SUCCESS) {
	printf ("Bad dhs status %d \n", mydhsStatus);
	mydhsStatus = DHS_S_SUCCESS;
    }
    return;
}

void daqDhsErrorCallback (DHS_CONNECT connect, DHS_STATUS errorNum, 
			  DHS_ERR_LEVEL errorLev, char *msg, DHS_TAG tag, 
			  void *userData)
{
#ifdef VxWorks
  logMsg ("In dhs error callback: errorNum=%d errorLev=%d msg=%s\n", errorNum, errorLev, msg, 0, 0, 0);
#else
  logMsg ("In dhs error callback: errorNum=%d errorLev=%d msg=%s\n", errorNum, errorLev, msg);
#endif
}

void daqDhsGetCallback ()
{
#ifdef VxWorks
  logMsg ("In dhs get callback\n", 0, 0, 0, 0, 0, 0);
#else
  logMsg ("In dhs get callback\n");
#endif
}

void daqDhsPutCallback ()
{
#ifdef VxWorks
  logMsg ("In dhs put callback\n", 0, 0, 0, 0, 0, 0);
#else
  logMsg ("In dhs put callback\n");
#endif
}

void initDhs ( const char *serverIp, const char *server )
{
    /* do dhs initialise things here... */
    mydhsStatus = DHS_S_SUCCESS;

    /* need to put a unique identifying name in here! */
    /* dhsInit ("mordred.tuc.noao.edu", 16, &mydhsStatus); */
    dhsInit (_client_hostname, 16, &mydhsStatus);
    printf ("After init dhStatus %d \n", mydhsStatus);
    if (mydhsStatus != DHS_S_SUCCESS) {
        return;
    }

    /* should I use callbacks? If so set them up here */
    dhsCallbackSet (DHS_CBT_ERROR, daqDhsErrorCallback, &mydhsStatus);
    printf ("After cb error dhs status %d \n", mydhsStatus);
    if (mydhsStatus != DHS_S_SUCCESS) {
        return;
    }

    dhsCallbackSet (DHS_CBT_GET, daqDhsGetCallback, &mydhsStatus);
    printf ("After cb get dhs status %d \n", mydhsStatus);
    dhsCallbackSet (DHS_CBT_PUT, daqDhsPutCallback, &mydhsStatus);
    printf ("After cbput dhsStatus %d \n", mydhsStatus);
    if (mydhsStatus != DHS_S_SUCCESS) {
        return;
    }

    /* connect to the server */
    printf ("Connecting to server %s...\n", server);
    mydhsStatus = DHS_S_SUCCESS;
    dhsConnection = dhsConnect (serverIp, server, NULL, &mydhsStatus);
    if (mydhsStatus != DHS_S_SUCCESS) {
#ifdef VxWorks
	logMsg ("Failed to connect to DHS server!\n", 0, 0, 0, 0, 0, 0);
#else
	logMsg ("Failed to connect to DHS server!\n");
#endif
        return;
    }
    else {
        printf("Established connection '%lu'\n", dhsConnection);
    }
    printf ("After connect dhsStatus %d \n", mydhsStatus);

    /* start dhs event loop */
    dhsEventLoop (DHS_ELT_THREADED, NULL, &mydhsStatus);
    printf ("After event loop dhs status %d \n", mydhsStatus);
    return;
}

void exitDhs ( void )
{
    mydhsStatus = DHS_S_SUCCESS;
    dhsDisconnect(dhsConnection, &mydhsStatus);
    mydhsStatus = DHS_S_SUCCESS;
    dhsExit (&mydhsStatus);
    return;
}

int dodhstest ( char *name )   
{
/* change this for TReCS/UF DHS -- david
    initDhs("128.171.188.193", "dataServerNT");
*/
    initDhs("128.227.184.39", "dataServerNT");
    printf("initDhs return code = %d\n", mydhsStatus);
    if (mydhsStatus == DHS_S_SUCCESS)
    {
        export();
        printf("export return code = %d\n", mydhsStatus);
    }
    if (mydhsStatus == DHS_S_SUCCESS)
    {
        exitDhs();
        printf ("After exitDhs return code = %d \n", mydhsStatus);
    }
    printf("I am done now\n");

  return mydhsStatus;
}

#ifndef VxWorks
int main(int argc, char** argv) {
  char info[MAXNAMLEN + 1];
  memset(info,0,sizeof(info));
  gethostname(info, sizeof(info));
  _client_hostname = (char*) calloc(1+strlen(info), sizeof(char));
  strcpy(_client_hostname, info);
  return dodhstest(_client_hostname);
}
#endif
