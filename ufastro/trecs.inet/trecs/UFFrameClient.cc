#if !defined(__UFFrameClient_cc__)
#define __UFFrameClient_cc__ "$Name:  $ $Id: UFFrameClient.cc 14 2008-06-11 01:49:45Z hon $";
const char rcsId[] = __UFFrameClient_cc__;

#include "UFFrameClient.h"
#include "UFClientSocket.h"
#include "UFFrameConfig.h"
#include "UFTimeStamp.h"
#include "UFFITSheader.h"

// globals:
float UFFrameClient::_timeout = -1;
bool UFFrameClient::_verbose = false;
bool UFFrameClient::_observatory = true;
UFStrings* UFFrameClient::_bufnames= 0;
UFClientSocket UFFrameClient::_frmcmdsoc;

int UFFrameClient::startObs(UFObsConfig& obscfg, const string& host, int port) {
  if( _verbose )
    clog<<"UFFrameClient::startObs> datalabel: "<<obscfg.datalabel()<<endl;
  if( !_frmcmdsoc.validConnection() ) {
    bool block = false;
    int fd = _frmcmdsoc.connect(host, port, block);
    if( fd <= 0 ) {
       clog<<"UFFrameClient::startObs> unable to connect to ufacqframed"<<endl;
      return fd;
    }
  }
  obscfg.setReadoutMode("S1");  //keep it simple otherwise need pixel maps

  _frmcmdsoc.send(obscfg);
  UFStrings* frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(_frmcmdsoc));
  if( frmreply == 0 ) {
    clog<<"UFFrameClient::startObs> null error on send obscfg"<<endl;
    return -1;
  }
  else if( (frmreply->name()).find("ERROR") != string::npos ) {
    clog<<"UFFrameClient::startObs> error on send ObsConfigg"<<frmreply->name()<<endl;
    delete frmreply;
    return -2;
  }
  delete frmreply;

  UFTimeStamp start("START");
  _frmcmdsoc.send(start); // send the start obs. directive, recv. reply.
  frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(_frmcmdsoc));
  if( frmreply == 0 ) {
    clog<<"UFFrameClient::startObs> null error on send START"<<endl;
    return -3;
  }
  else if( (frmreply->name()).find("ERROR") != string::npos ) {
    clog<<"UFFrameClient::startObs> error on send START"<<frmreply->name()<<endl;
    delete frmreply;
    return -4;
  }
  delete frmreply;

  return _frmcmdsoc.getInfo().fd;
}

// connect to ufacqframed and start observation
int UFFrameClient::replConnectAndStartObs(UFObsConfig& obscfg, const string& host, int port) {
  bool block = false;
  int datafd = connect(host, port, block); // connect for replicated data frames
  if( datafd <= 0 ) {
    clog<<"UFFrameClient::replConnectAndStartObs> error on data soc. connect to acqframed."<<endl;
    return datafd;
  }

  UFTimeStamp replica("REPLICA");
  int nb = send(replica); // send the directive, no reply to recv.
  if( nb <= 0 ) {
    clog<<"UFFrameClient::replConnectAndStartObs> error on data soc. greeting to acqframed."<<endl;
    close();
    return nb;
  }

  int cmdfd = startObs(obscfg, host, port);
  /*
  if( cmdfd <= 0 ) {
    return cmdfd;
  }
  UFObsConfig* curObsConf = dynamic_cast<UFObsConfig*>(UFProtocol::createFrom(*this));
  if( curObsConf == 0 ) {
    clog<<"UFFrameClient::replConnectAndStartObs> null error on data replication stream"<<endl;
  }
  else if( (curObsConf->name()).find("ERROR") != string::npos ) {
    clog<<"UFFrameClient::replConnectAndStartObs> error on data replication stream"<<curObsConf->name()<<endl;
    delete curObsConf; curObsConf = 0;
  }
  return curObsConf;
  */

  return cmdfd;
}


// connect to ufedtd:
int UFFrameClient::replConnect(const string& host, int port) {
  if( host != "" ) _frmhost = host;
  if( port > 0  ) _frmport = port;

  bool block = false;
  int fd = connect(_frmhost, _frmport, block);
  if( fd <= 0 )
    return 0;
 
  if( port == 52000 ) { // ufacqframed
    UFTimeStamp replica("REPLICA");
    int nb = send(replica); // send the directive, no reply to recv.
    if( nb <= 0 ) {
      clog<<"UFFrameClient::replConnect> error on data soc. greeting to acqframed."<<endl;
      close();
      return nb;
    }
    else {
      if( _verbose )
        clog<<"UFFrameClient::replConnect> connected to acqframed."<<endl;
      return fd;
    }
  }
  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet("dhsclient::replication");
  int ns = send(greet);
  ns = recv(greet);
  if( _verbose )
    clog<<"UFFrameClient> connected to ufedtd, greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return fd;
}

UFProtocol* UFFrameClient::replFrame(int& frmcnt, int& total) {
  UFClientSocket* csoc = static_cast< UFClientSocket* > (this);
  if( csoc == 0 )
     return 0;

  //clog<<"UFFrameClient::replFrame> waiting to recv. ufprotocol object, _timeout= "<<_timeout<<endl;
  // in order to be able to interrupt this...
  int na = 0;
  int cnt = 2 * ((int)_timeout); // negative timeout means forever
  if( cnt < 0 ) cnt = 0; // forever
  while( cnt >= 0 && na == 0 ) {
    na = csoc->available();
    UFPosixRuntime::sleep(0.5); // this is why cnt is doubled, this sleep allows procees interrupt
    if( _timeout > 0 ) --cnt;
  }
  if( na <= 0 ) {
    clog<<"UFFrameClient::replFrame> lost connection? socket avail: "<<na<<", socptr: "<<csoc<<endl;
    return 0;
  }
  //clog<<"UFFrameClient::replFrame> socket avail: "<<na<<", socptr: "<<csoc<<endl;
  //  createFrom now supports timeout, default is < 0, i.e. infinite... 
  UFProtocol* ufp = 0;
  try {
    ufp = UFProtocol::createFrom(*csoc);
    if( ufp == 0 )
      clog<<"UFFrameClient::replFrame> recv/create failed..."<<endl;
    else if( _verbose )
      clog<<"UFFrameClient::replFrame> recv'd: "<<ufp->name()<<" "<<ufp->timeStamp()<<endl;
  }
  catch(std::exception& e) {
    clog<<"UFFrameClient::replFrame> stdlib exception occured: "<<e.what()<<endl;
  }
  catch(...) {
    clog<<"UFFrameClient::replFrame> unknown exception occured..."<<endl;
  }
  
  if( ufp == 0 )
    return 0;

  if( ufp->typeId() == UFProtocol::_ObsConfig_ ) {
    frmcnt = 0;
    //return dynamic_cast<UFObsConfig*> (ufp);
    return ufp;
  }
  ++frmcnt; ++total;
  //return dynamic_cast<UFInts*> (ufp);
  //clog<<"UFFrameClient::replFrame> recv'd "<<ufp->name()<<endl;
  return ufp;
}

UFInts* UFFrameClient::_simFrame(const string& name, int index) {
  int cnt = _w*_h;
  int *tstimg = new int[_w*_h];

  if( index % 2 == 0 )
    for( int i = 0; i < cnt; ++i )
      tstimg[i] = i + index;
  else
    for( int i = 0; i < cnt; ++i )
      tstimg[i] = (cnt-i) + index;

  return new UFInts(name, (int*) tstimg, cnt); // deep copy
}

bool UFFrameClient::getFileStoreName(string& readyfile, string& filename, string& prevfile,
				     int& frmcnt, int& frmtotal) {
  // get the named buffer's current data frame:
  filename = "";
  if( !validConnection() ) {
    close();
    int fd = connect(_frmhost, _frmport);
    if( fd <= 0 )
      return false;
    if( _verbose ) clog<<"UFFrameClient::getUpdatedBufNames> connected to frame service. "<<_frmhost<<endl;
  }

  UFTimeStamp req("FC"); // request frame config.
  send(req);
  // current frameconfig is sent back first:
  UFFrameConfig* ufc = dynamic_cast< UFFrameConfig* > (UFProtocol::createFrom(*this));
  if( ufc == 0 ) {
    clog<<"UFFrameClient::getFileStoreName> bad frame config"<<endl;
    return 0;
  }
  frmcnt = ufc->frameWriteCnt();
  frmtotal = ufc->frameObsSeqTot();
  delete ufc;

  // request file name
  req.rename("FileName");
  send(req);
  // local file storage full path is sent back:
  UFStrings* ufs = dynamic_cast< UFStrings* > (UFProtocol::createFrom(*this));
  if( ufs == 0 ) {
    clog<<"UFFrameClient::getFileStoreName> bad file name reply"<<endl;
    return false;
  }
  if( ufs->elements() <= 0 ) {
    delete ufs;
    return false;
  }
  filename = (*ufs)[0]; delete ufs;
  if( filename.find("none") != string::npos || // still writing first file
      filename.find("None") != string::npos || filename.find("NONE") != string::npos ) {
    return false; // not done!
  }
  if( prevfile == "" ) { // first-time query for  observation
    prevfile = filename;
    readyfile = "";
    return false;
  }
  if( prevfile != filename ) {
    readyfile = prevfile;  // file name has changed, new observation started, frmcnt < total
    prevfile = filename;
    return true;
  }
  if( frmcnt == frmtotal ) { // observation completed, new one starting up
    readyfile = filename;
    prevfile = "";
  }

  return true;
}

// get the current frameconfig info. formatted with instrument quicklook
// stream names
int UFFrameClient::getUpdatedQlNames(const string& instrum,
				     vector< string >& qlbufnames,
				     int& frmcnt, int& frmtotal) {
  // get the named buffer's current data frame:
  if( !validConnection() ) {
    close();
    int fd = connect(_frmhost, _frmport);
    if( fd <= 0 )
      return 0;
    else
      if( _verbose ) clog<<"UFFrameClient::getUpdatedBufNames> connected to frame service. "<<_frmhost<<endl;
  }
  UFTimeStamp req("FC"); // request frame config.
  send(req);
  // current frameconfig is sent back first:
  UFFrameConfig* ufc = dynamic_cast< UFFrameConfig* > (UFProtocol::createFrom(*this));
  if( ufc == 0 ) {
    clog<<"UFFrameClient::getUpdatedBufNames> bad frame config"<<endl;
    return 0;
  }
  frmcnt = ufc->frameObsSeqNo();
  frmtotal = ufc->frameObsSeqTot();
  int cnt = ufc->getUpdatedBuffNames(instrum, qlbufnames);
  delete ufc;
  return cnt;
}

UFInts* UFFrameClient::fetchFrame(const string& name, int& frmcnt, int& frmtotal) {
  if( name == "sim" || name == "trecssim" || name == "simtrecs" ) 
    return _simFrame(name, frmcnt);

  if( name.find("trecs:") == 0 ) {
    int slen = strlen("trecs:");
    string bufname = name.substr(slen);
    if( bufname.length() <= 2 ) // all buffer names should be 3 or more chars
      return _simFrame(name, frmcnt); // just do sim
  }
  // get the named buffer's current data frame:
  if( !validConnection() ) {
    close();
    int fd = connect(_frmhost, _frmport);
    if( fd <= 0 )
      return 0;
    else
      if( _verbose ) clog<<"UFFrameClient::fetchFrame> connected to frame service. "<<_frmhost<<endl;
  }
  UFTimeStamp req("BN"); // request buffer names
  if( _bufnames == 0 ) { // first connection? get list of buffernames:
    send(req);
    _bufnames = dynamic_cast< UFStrings* > (UFProtocol::createFrom(*this));
    if( _bufnames == 0 ) {
      clog<<"UFFrameClient::fetchFrame> unable to get buffer names from frame service. "<<_frmhost<<endl;
      close();
      return 0;
    }
    if( _verbose ) 
      for( int i = 0; i < _bufnames->numVals(); ++i ) {
        clog<<"UFFrameClient::fetchFrame> buffer # "<<i<<" is: "<<(*_bufnames)[i]<<endl;
      }
  }

  bool nameOk= false;
  for( int i = 0; i < _bufnames->numVals(); ++i ) {
    //clog<<"UFFrameClient::fetchFrame> buffer # "<<i<<" is: "<<(*_bufnames)[i]<<endl;
    if( name == (*_bufnames)[i] ) {
      nameOk = true;
      break;
    }
  }
  if( !nameOk ) {
    clog<<"UFFrameClient::fetchFrame> ? bad buffer name: "<<name<<endl;
    return 0;
  }

  req.rename(name); 
  send(req); // request the frame buffer by name

  // current frameconfig is sent back first:
  UFFrameConfig* ufc = dynamic_cast< UFFrameConfig* > (UFProtocol::createFrom(*this));
  if( ufc == 0 ) {
    clog<<"UFFrameClient::fetchFrame> ? bad frame config?"<<endl;
    return 0;
  }

  frmcnt = ufc->frameObsSeqNo();
  frmtotal = ufc->frameObsSeqTot();

  if( ufc->name() != name ) {
    clog<<"UFFrameClient::fetchFrame> ? bad buffer name: "<<ufc->name()<<endl;
    return 0;
  }

  // frame data follows
  UFInts* ufi = dynamic_cast< UFInts* > (UFProtocol::createFrom(*this));
  if( ufi == 0 ) {
    clog<<"UFFrameClient::fetchFrame> ? bad frame?"<<endl;
    return 0;
  }

  return ufi;
}

int UFFrameClient::fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

// return FITS header from opened file, seeking to start of first data frame
int UFFrameClient::openFITS(const string& filename, UFStrings*& fitsHdr, 
			    int& width, int& height, int& frmtotal) {
  fitsHdr = 0;
  frmtotal = 0;
  struct stat st;
  int rs = ::stat(filename.c_str(), &st);
  if( rs < 0 ) {
    clog<<"UFFrameClient::openFITS> unable to stat: "<<filename<<endl;
    return 0;
  }
  int totalbytes = st.st_size;

  int fd = ::open(filename.c_str(), O_RDONLY);
  if( fd <= 0 ) {
    clog<<"UFFrameClient::openFITS> unable to open: "<<filename<<endl;
    return 0;
  }

  char fitshd1[80];
  char end[4]; ::memset(end, 0, sizeof(end));
  char naxis[7]; ::memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;

  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  int fitsbytes= 0; // allow 10x2880 (10 headers)?
  UFFITSheader ufits;
  do {
    ::memset(fitshd1, 0, sizeof(fitshd1));
    fitsbytes += ::read(fd, fitshd1, 80);
    memcpy(end, fitshd1, ne);
    s = end;
    memcpy(naxis, fitshd1, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, fitshd1);
    if( key == "NAXIS2" )
      h = fitsInt(key, fitshd1);
    // add entry to header
    string fitshdr = fitshd1;
    ufits.add(fitshdr);
  } while( s != "END" && fitsbytes < 10*2880 );
  if( fitsbytes >= 10*2880 && s != "END" ) {
    clog<<"UFFrameClient::openFITS> sorry, improper or no FITS header?"<<endl;
    ::close(fd);
    return 0;
  }
  if( w <= 0 || h <= 0 ) {
    clog<<"UFFrameClient::openFITS> sorry FITS header lacks NAXIS1,2"<<endl;
    ::close(fd);
    return 0;
  }
  // fits image data is Big-Endian:
  if( _verbose )
    clog<<"UFFrameClient::openFITS> header fitbytes= "<<fitsbytes<<", total= "<<totalbytes<<endl;
  int frmbytes = totalbytes - fitsbytes;
  frmtotal = frmbytes / (w*h*sizeof(int));
  width = w; height = h;
  if( _verbose )
    clog<<"UFFrameClient::openFITS> header indicates: w= "<<w<<", h= "<<h
        <<", total frames= "<<frmtotal<<endl;

  fitsHdr = ufits.Strings(filename);
  return fd;
}
  
// return FITS data from open file, seeking to next data frame
UFInts* UFFrameClient::seekFITSData(const int fd, const int width, const int height) {
  int* frame = new int[width*height];
  int nb = ::read(fd, frame, width*height*sizeof(int));
  if( nb < width*height*(int)sizeof(int) ) {
    clog<<"UFFrameClient::seekFITSData> unable to read full image data, nb= "<<nb
        <<", expected "<< width*height*sizeof(int)<<endl;
    delete frame;
    return 0;
  }    
  return new UFInts("ImageFrame", (const int*) frame);
}

#endif // __UFFrameClient_cc__
