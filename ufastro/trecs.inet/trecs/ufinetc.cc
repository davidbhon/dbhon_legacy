#if !defined(__ufinetc_cc__)
#define __ufinetc_cc__ "$Name:  $ $Id: ufinetc.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufinetc_cc__;

#include "UFClientSocket.h"
__UFSocket_H__(ufinetc_cc);

#include "UFTimeStamp.h"
__UFTimeStamp_H__(ufinetc_cc);

#include "UFRuntime.h"
__UFRuntime_H__(ufinetc_cc);

#include "cstdio"
#include "cstring"
const bool _verbose = true;

int main(int argc, char** argv, char** envp) {
  // /usr/local/sbin/{ufinetc,ufstartup}
  // preumably this is exec'd by the inetd listening on port 52222
  // and so it is expected to associate file desc. 0, 1, and 2 with
  // client connection:
  string usage = "ufinetc -shutdown OR -boot [-host hostname]";
  if( argc <= 1 ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }

  string host = UFRuntime::hostname();
  UFRuntime::Argv args(argc, argv);
  string arg = UFRuntime::argVal("-host", args);
  if( arg != "true" && arg != "false" )
    host = arg;
  
  arg = UFRuntime::argVal("-h", args);
  if( arg == "true" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }
    
  arg = UFRuntime::argVal("-help", args);
  if( arg == "true" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }

  string clname = "UFTReCSShutDown@" + host;
  string boot = UFRuntime::argVal("-boot", args);
  if( boot == "true" )
    clname = "UFTReCSBoot@" + host;

  string shutdown = UFRuntime::argVal("-shutdown", args);
  if( boot != "true"  && shutdown != "true" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }

  UFClientSocket clsoc;
  if( clsoc.connect(host, 52222, false) < 0 ) {
    clog<<"ufinetc> failed to connect to ufinetboot"<<endl;
    return 1;
  }
  clog<<"ufinetc> connected to ufinetboot: "<<clsoc.description()<<endl;
  clog<<"ufinetc> send request= "<<clname<<endl;
  // client connected:
  int ns = clsoc.send(clname); // send boot request
  if( ns <= 0 ) {
    clog<<"ufinetc> failed to send request. "<<clname<<endl;
    clsoc.close();
    return 2;
  }
  else {
    clog<<"ufinetc> sent request, ns= "<<ns<<", "<<clname<<endl;
  }
  // expect single string reply:
  string reply;
  int nr = clsoc.recv(reply);
  if( nr <= 0 ) {
    clog<<"ufinetc> failed to recv boot request reply"<<endl;
    clsoc.close();
    return 3;
  }
  clog<<"ufinetc> reply, nr: "<<nr<<" -- "<<reply<<endl;
  clsoc.close();
  return 0;
}

#endif // __ufinetc_cc__
