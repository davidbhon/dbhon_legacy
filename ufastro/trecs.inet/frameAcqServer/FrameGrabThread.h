#if !defined(__FrameGrabThread_h__)
#define __FrameGrabThread_h__ "$Name:  $ $Id: FrameGrabThread.h,v 1.3 2003/07/10 23:54:11 varosi beta $"

/*
 * The FrameGrabThread grabs frames from EDT/PCI device:
 *   copies EDT DMA buffer into UFInts frames and pushes pointers onto the grabbedFramesList,
 *   then accessed by FrameProcessThread for writing to FITS file, etc.
 * Processing multi-sample readout modes and mapping pixels from detector to display order
 *   is done by FrameProcessThread, by calling local method popGrabbedFrames().
 *
 * When EDT/PDV device cannot be opened the server reverts to simulation mode:
 *   a FrameServerThread recvs UFInts frames and puts pointers in the receivedFramesList,
 *   then accessed by the FrameGrabThread which copies them into the grabbedFramesList,
 *   then accessed by the FrameProcessThread, which moves them into frameBuffers.
 *
 * Frank Varosi, July 9, 2003.
 */

extern void* FrameGrabThread( void* ) ;
extern bool createFrameGrabThread( int Nbuf_EDT, bool simulate=false, int MaxTimeOuts=1 ) ;
extern bool ObsInProgress() ;
extern bool EDTdeviceExists() ;
extern vector< UFInts* > popGrabbedFrames() ;
extern void clearGrabbedList() ;
extern bool grabListEmpty() ;
extern bool recvdListEmpty() ;
extern bool abortDHS() ;
extern UFStrings* FrameGrabStart( string command, int TimeOut=0 ) ;
extern UFStrings* FrameGrabAbort( string command ) ;
extern UFStrings* FrameGrabKill( string command ) ;
extern UFStrings* delayProcessing() ;
extern UFTimeStamp* getCameraType() ;
extern void FrameGrabShutdown() ;
extern void FrameGrabSimulate( UFInts* receivedFrame ) ;
extern void checkEDTdevFrameConf( string Caller="FrameGrabThread" ) ;
extern void FrameGrabSetNbuffers( int Nbuf ) ;
extern void FrameGrabSetMaxTimeOuts( int maxTouts ) ;
extern int totalFramesToGrab() ;
extern int FrameGrabMaxDMALag() ;
extern int FrameGrabNobsError() ;
extern int FrameGrabNobsAbort() ;
extern int FrameGrabNobsComp() ;
extern int FrameGrabNbuffers() ;
extern int FrameGrabNframes() ;
extern int FrameGrabTimeOuts() ;
extern int FrameGrabMaxTimeOuts() ;
extern UFInts* copyPixelMap( string& message, string name="" ) ;
extern string definePixelMap( UFInts* PixelMap ) ;

#endif /* __FrameGrabThread_h__ */
