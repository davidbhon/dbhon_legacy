#if !defined(__FrameWriteThread_h__)
#define __FrameWriteThread_h__ "$Name:  $ $Id: FrameWriteThread.h,v 1.2 2003/07/11 22:46:33 varosi beta $"

/*
 * The FrameWriteThread writes all raw data frames to the FITS file, if one has been opened
 *  by a client request. FrameWriteThread.cc contains all relevant FITS file methods.
 *
 * Frank Varosi, July 9, 2003.
 */

extern void* FrameWriteThread( void* ) ;
extern bool createFrameWriteThread( string Version, int MaxFrmWaitWrite, string HostOfAgents ) ;
extern bool nowSavingData() ;
extern bool writeFrame( UFInts* writeMe, int SaveSetCount ) ;
extern bool writeFrameToFile( UFInts* frame ) ;
extern bool writeListEmpty() ;
extern bool writeAccumSigImage( UFInts* accSig ) ;
extern int writeFITSheader( UFStrings* FITS_header, int fileDesc ) ;
extern int NfilesCreated() ;
extern bool AgentsConnected( string FSThreadName = "FrameWriteThread" ) ;
extern string extractDirectoryName( string FileName, string& ErrMess );
extern void clearWriteList() ;
extern void FrameWriteStart() ;
extern void FrameWriteShutdown() ;
extern UFStrings* defineFITSheader( UFStrings* newFITSheader ) ;
extern UFStrings* appendFITSheader( UFStrings* partialFITSheader ) ;
extern UFStrings* createFITSheader( bool addEND=true ) ;
extern UFStrings* fetchFITSheader( string FSThreadName = "FrameWriteThread" ) ;
extern UFStrings* closeFITSfile( UFStrings* FileCommand ) ;
extern UFStrings* openFITSfile( UFStrings* FileCommand, bool autoClose=false ) ;
extern UFStrings* autoCreateFITSfile( UFStrings* FileCommand ) ;
extern UFStrings* getFITSheader();
extern UFStrings* getLastFileName();

#endif /* __FrameWriteThread_h__ */
