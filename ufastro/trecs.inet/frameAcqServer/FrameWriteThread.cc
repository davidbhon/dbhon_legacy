#if !defined(__FrameWriteThread_cc__)
#define __FrameWriteThread_cc__ "$Name:  $ $Id: FrameWriteThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameWriteThread_cc__;

#include "FrameAcqServer.h"
#include "UFFITSClient.h"
#include "FrameWriteThread.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"

// The thread ID and blocking mutex for starting/pausing the FrameWriteThread.

pthread_t FrameWriteThreadID = 0 ;
pthread_mutex_t FrameWriteMutex = PTHREAD_MUTEX_INITIALIZER ;
static bool _keepRunning = true ;
string FrameAcqVersion;  //added to comment of first record in FITS file.

// The List used to store pointers to frames that are to be written to FITS file or DHS stream.

frameListType _writeFramesList ;
pthread_mutex_t _writeFramesListMutex = PTHREAD_MUTEX_INITIALIZER ;

int _saveFile = -1; // file descriptore unit for the FITS file and saving frames.
int _Nfiles = 0 ;
int _NfhRecs = 0 ; // # of FITS header records (including blanks) written to file, set in openFITSfile().
UFStrings* _FITSheader = 0 ;
pthread_mutex_t _FITSheaderMutex = PTHREAD_MUTEX_INITIALIZER ;
UFStrings* FITS_FileWrittenStatus = new UFStrings("UNKNOWN", "none") ;
pthread_mutex_t _fileStatusMutex = PTHREAD_MUTEX_INITIALIZER ;

string _FITS_fileName ;
string _saveDirectory ; //current directory in which FITS file is created.
int _saveDirCount = 0 ; //current version number appended to directory (zero results in blank).
int _NfilinDir = 0 ;    //current number of files created in current directory.

bool _autoCloseFile = false ;  // true means FrameWriteThread will automatically write final header
                               //                 and close FITS file when all frames are written.
bool _frameWriteError = false; // true means FrameWriteThread has encountered a repeated error
                               //         while attempting to write frames to FITS file and so
                               //    will honor any request to close the FITS file even if incomplete.
int _maxFrmWaitWrite = 300;    // if write error occurs, will keep trying to write the frame
                               //  until # of frames waiting in write List exceeds _maxFrmWaitWrite.

UFFITSClient* ufFITS = 0; //object used to gather FITS header from device agents (optional).
UFFITSClient::AgentLoc agentLocs;
UFSocket::ConnectTable agentConnections;
string agentHost; //the hostname of system on which device agents are running (rarely used option).

// If non-zero, this accum(sig) image is written to separate FITS file (see closeFITSfile() code):
UFInts* _accumSigImage = 0;
pthread_mutex_t _writeAccSigMutex = PTHREAD_MUTEX_INITIALIZER ;

//******************************************************************************************

bool createFrameWriteThread( string Version, int MaxFramesWaitWrite, string HostOfAgents )
{
  FrameAcqVersion = Version;
  agentHost = HostOfAgents;
  if( MaxFramesWaitWrite > 1 ) _maxFrmWaitWrite = MaxFramesWaitWrite;
  _keepRunning = true;

  if( ( FrameWriteThreadID = UFPosixRuntime::newThread( FrameWriteThread ) ) <= 0 )
    return false;
  else
    return true;
}
//---------------------------------------------------------------------------------

void FrameWriteShutdown() {
  _keepRunning = false;
  ::pthread_mutex_unlock( &_fileStatusMutex );
  ::pthread_mutex_unlock( &_writeFramesListMutex );
  ::pthread_mutex_unlock( &FrameWriteMutex );
}

void FrameWriteStart() { ::pthread_mutex_unlock( &FrameWriteMutex ); }
bool nowSavingData() { if(_saveFile < 0) return false; else return true; }
int NfilesCreated() { return _Nfiles; }

//--------------------------------------------------------------------------------------------------

void* FrameWriteThread( void* )
{
  // This method runs as a thread,
  // popping frames from the writeFramesList, and writing them to the FITS file.
  clog << "FrameWriteThread> when errors: max # frames allowed waiting write before obs/write abort = "
       << _maxFrmWaitWrite << endl;

  while( _keepRunning )
    {
      //mutex must be locked before checking if writeFramesList is empty!
      ::pthread_mutex_lock( &FrameWriteMutex );

      if( writeListEmpty() )
	{
	  ::pthread_mutex_lock( &_fileStatusMutex );
	  bool autoClose = _autoCloseFile;
	  ::pthread_mutex_unlock( &_fileStatusMutex );

	  if( autoClose && _saveFile>=0 )
	    {
	      UFPosixRuntime::sleep( 1.0 );
	      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	      int FrameWriteCount = theFrameConfig->frameWriteCnt();
	      int FrameTotal = theFrameConfig->frameObsSeqTot();
	      if( FrameTotal <= 0 ) FrameTotal = theFrameConfig->frameGrabCnt();
	      theFrameConfig = accessFrameConfig( false );

	      if( FrameWriteCount >= FrameTotal )   //all frames must be written.
		{
		  UFPosixRuntime::sleep( 0.5 );
		  closeFITSfile( new UFStrings("AUTO_CLOSE","AUTO_CLOSE") );
		}
	    }

	  ::pthread_mutex_lock( &FrameWriteMutex );
	  //Since a lock was already obtained above, this second try to lock mutex will block and wait.
	  //First mutex is unlocked by FrameProcessThread when new frame is processed (see writeFrame()).
	}

      ::pthread_mutex_unlock( &FrameWriteMutex ); //Unlock mutex to reset for next time.
      if( !_keepRunning ) break;

      ::pthread_mutex_lock( &_writeFramesListMutex );
      UFInts* frameToWrite = _writeFramesList.back();
      _writeFramesList.pop_back();
      ::pthread_mutex_unlock( &_writeFramesListMutex );

      if( _saveFile>=0 && !_frameWriteError )
	{
	  int NtoWrite=0, Ntry=0, Maxtry=30;
	  float sleepTime = 10.0;
	  string error;

          while( ! writeFrameToFile( frameToWrite ) )
            {
	      error = strerror( errno );
	      error = "ERROR: FrameWriteThread> Failed write (" + error + ") ";
	      if( frameToWrite ) error += frameToWrite->name();
	      clog << error << endl;
	      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	      theFrameConfig->rename(error);
	      NtoWrite = theFrameConfig->frameObsSeqNo() - theFrameConfig->frameWriteCnt();
	      //if obs is complete, starting countdown until write abort:
	      if( theFrameConfig->frameObsSeqNo() >= theFrameConfig->frameObsSeqTot() )
		++Ntry;
	      theFrameConfig = accessFrameConfig( false );

	      if( NtoWrite < _maxFrmWaitWrite && Ntry < Maxtry ) {
		clog << "......................will try to write frame again after "
		     << sleepTime << " sec sleep...";
		if( Ntry > 0 ) 
		  clog << sleepTime*(Maxtry-Ntry) << " secs until write abort!";
		clog <<endl;
		UFPosixRuntime::sleep( sleepTime );
	      }
	      else {                     //Abort obs. if write error is persistent:
		_frameWriteError = true; //this will negate any further write attempts and allow closing.
		clog << "FrameWriteThread> forcing observation and file writing to ABORT..."<<endl;
		FrameGrabAbort( error );
		FrameGrabKill( error );
		break;
	      }
            }
	}

      delete frameToWrite;
    }

  clearWriteList();
  _frameWriteError = true; //this will allow closing.
  closeFITSfile( new UFStrings("CLOSE","CLOSE") );
  clog << "FrameWriteThread> terminated."<<endl;
  return 0;
}
//----------------------------------------------------------

bool writeListEmpty()
{
  ::pthread_mutex_lock( &_writeFramesListMutex );
  bool empty_status = _writeFramesList.empty();
  ::pthread_mutex_unlock( &_writeFramesListMutex );

  return empty_status;
}
//----------------------------------------------------------

void clearWriteList()
{
  ::pthread_mutex_lock( &_writeFramesListMutex );
  if( !_writeFramesList.empty() ) {
    clog<<"FrameAcqServer> "<<_writeFramesList.size()
	<<" frames still in _writeFramesList, deleting them..."<<endl;
    for( frameListType::iterator pf = _writeFramesList.begin(); pf != _writeFramesList.end(); ++pf )
      delete *pf;
  }
  _writeFramesList.clear();
  ::pthread_mutex_unlock( &_writeFramesListMutex );
}
//---------------------------------------------------------------------------------

bool writeFrame( UFInts* writeMe, int SaveSetCnt )
{
  // Copy frame and push it onto the front of writeFramesList for the FrameWriteThread.
  // Swap bytes Little to Big endian if needed to comply with FITS standard.

  UFInts* frameToWrite = new (nothrow) UFInts( writeMe->name(), writeMe->valInts(), writeMe->numVals() );
  string newName = "SaveSet# " + UFRuntime::numToStr( SaveSetCnt ) + " <" + writeMe->name() + ">";

  if( UFProtocol::validElemCnt( frameToWrite ) )
    {
#ifdef LittleEndian
      frameToWrite->reverseByteOrder();
#endif
      ::pthread_mutex_lock( &_writeFramesListMutex );
      _writeFramesList.push_front( frameToWrite );
      frameToWrite->rename( newName );
      ::pthread_mutex_unlock( &_writeFramesListMutex );
      ::pthread_mutex_unlock( &FrameWriteMutex );
      return true;
    }
  else {
    string status = "ERROR: writeFrame> FAILED mem. alloc. for copy of " + newName;
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(status);
    theFrameConfig = accessFrameConfig( false );
    clog << status << ". " <<endl;
    return false;
  }
}
//---------------------------------------------------------------------------------

bool writeFrameToFile( UFInts* writeMe )
{
  if( ! UFProtocol::validElemCnt( writeMe ) ) {
    clog << "writeFrameToFile> arg UFInts* writeMe is NOT valid object!" <<endl;
    return false;
  }

  int nbtowrite = writeMe->numVals() * sizeof(int);
  int nbwritten = writeMe->writeValues( _saveFile );

  if( nbwritten < nbtowrite ) {
    clog << "writeFrameToFile> wrote " << nbwritten << " out of " << nbtowrite
	 << " bytes in " << writeMe->name() << " (" << strerror(errno) << ")." << endl;
    return false;
  }
  
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int FrameWriteCount = theFrameConfig->frameWriteCnt();
  theFrameConfig->setFrameWriteCnt( ++FrameWriteCount );
  theFrameConfig = accessFrameConfig( false );

  return true ;
}
//---------------------------------------------------------------------------------

bool writeAccumSigImage( UFInts* accSig )
{
  // Copy accumulated signal image to new UFInts which will be written to separate FITS file
  // after the main raw data FITS file is closed.
  // Swap bytes Little to Big endian if needed to comply with FITS standard.

  if( ! UFProtocol::validElemCnt( accSig ) ) {
    clog << "ERROR: writeAccumSigImage> input arg UFInts* accSig is not valid object!" <<endl;
    return false;
  }

  ::pthread_mutex_lock( &_writeAccSigMutex );
  if( _accumSigImage ) delete _accumSigImage;

  _accumSigImage = new (nothrow) UFInts( accSig->name(), accSig->valInts(), accSig->numVals() );

  if( UFProtocol::validElemCnt( _accumSigImage ) ) {
#ifdef LittleEndian
    _accumSigImage->reverseByteOrder();
#endif
    clog<<"writeAccumSigImage> copied and queued "<<_accumSigImage->name()<<" for filing."<<endl;
    ::pthread_mutex_unlock( &_writeAccSigMutex );
    return true;
  }
  else {
    string status = "ERROR: writeAccumSigImage> FAILED alloc. for copy of " + accSig->name();
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(status);
    theFrameConfig = accessFrameConfig( false );
    clog << status << ". " <<endl;
    delete _accumSigImage; _accumSigImage = 0;
    ::pthread_mutex_unlock( &_writeAccSigMutex );
    return false;
  }
}
//---------------------------------------------------------------------------------

bool writeAccumSigToFile( string accSigFileName )
{
  ::pthread_mutex_lock( &_writeAccSigMutex );
  bool valid = UFProtocol::validElemCnt( _accumSigImage );

  if( !valid ) {
    for( int i=0; i<2; i++ ) {
      ::pthread_mutex_unlock( &_writeAccSigMutex );
      clog << "writeAccumSigToFile> waiting 1 sec for _accumSigImage ..."<<endl;
      UFPosixRuntime::sleep( 1.0 );
      ::pthread_mutex_lock( &_writeAccSigMutex );
      if( valid = UFProtocol::validElemCnt( _accumSigImage ) )
	break;
    }
    if( !valid ) {
      clog << "writeAccumSigToFile> _accumSigImage is NULL object!"<<endl;
      ::pthread_mutex_unlock( &_writeAccSigMutex );
      return false;
    }
  }

  clog << "writeAccumSigToFile> opening file: " << accSigFileName <<endl;
  mode_t UserMask = ::umask(0);
  int accSigFD = ::open( accSigFileName.c_str(), O_RDWR|O_CREAT|O_EXCL,
			 S_IRUSR|S_IRGRP|S_IROTH|S_IXUSR|S_IXGRP|S_IXOTH );
  ::umask(UserMask);
  string errmsg;
  bool success = true;

  if( accSigFD >= 0 )
    {
      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
      UFFITSheader* frameFH = new (nothrow) UFFITSheader( *theFrameConfig,
							  "Written by " + FrameAcqVersion );
      frameFH->add("FRMSPROC",
		   theFrameConfig->frameObsSeqNo()," # of frames actually processed");
      theFrameConfig = accessFrameConfig( false );
      //extract part of main FITS header and use with accsig file:
      int LocFHinfo = 0;
      while( (*_FITSheader->stringAt(LocFHinfo)).find("DATE_FH") == string::npos &&
	     LocFHinfo < _FITSheader->elements()-2 )
	++LocFHinfo;
      UFStrings* accSigFITSheader = new (nothrow) UFStrings( frameFH->Strings("accum(sig)"),
							     _FITSheader, ++LocFHinfo );

      if( writeFITSheader( accSigFITSheader, accSigFD ) > 0 )
	clog << "writeAccumSigToFile> FITS header written to file: " << accSigFileName <<endl;
      else
	clog << "writeAccumSigToFile> ERROR writing FITS header to file: " << accSigFileName <<endl;

      int nbtowrite = _accumSigImage->numVals() * sizeof(int);
      int nbwritten = _accumSigImage->writeValues( accSigFD );
      ::close( accSigFD );

      if( nbwritten < nbtowrite ) {
	string error = strerror(errno);
	errmsg = "ERROR: writeAccumSigToFile> wrote " + UFRuntime::numToStr( nbwritten )
	  + " out of " + UFRuntime::numToStr( nbtowrite ) + " bytes (" + error + ").";
	success = false;
      }
      else clog << "writeAccumSigToFile> wrote and closed file: " << accSigFileName <<endl;
    }
  else {
    string error = strerror(errno);
    errmsg = "ERROR: writeAccumSigToFile> Failed open FITS file (" + error + "): " + accSigFileName;
    success = false;
  }

  delete _accumSigImage; _accumSigImage = 0;
  ::pthread_mutex_unlock( &_writeAccSigMutex );
  
  if( errmsg.length() > 1 ) {
    clog << errmsg <<endl;
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(errmsg);
    theFrameConfig = accessFrameConfig( false );
  }

  return success;
}

//---------------------------------------------------------------------------------
// Give other threads access to status and counters:

UFStrings* getLastFileName()
{
  ::pthread_mutex_lock( &_fileStatusMutex );
  UFStrings* Fstat = new (nothrow) UFStrings( FITS_FileWrittenStatus );  //makes a copy.
  ::pthread_mutex_unlock( &_fileStatusMutex );
  string message;

  if( Fstat == 0 ) {
    clog << "ERROR: ";
    message = "getLastFileName> memory allocation failure!";
    Fstat = new (nothrow) UFStrings( "ERROR", &message ) ;
  }
  else {
    message = "FrameAcqServer> sending copy of status of last FITS file written...";
    Fstat->stampTime( FITS_FileWrittenStatus->timeStamp() );   //original timestamp of file closing.
  }
  clog << message <<endl;
  return Fstat;
}
//------------------------------------------------------FITS header and file methods:---------

UFStrings* defineFITSheader( UFStrings* newFITSheader )
{
  if( newFITSheader == 0 ) {
    string message = "FrameAcqServer> rejected NULL new FITS header.";
    clog << message << endl;
    return new UFStrings( "ERROR", &message ) ;
  }

  if( newFITSheader->name() == "ERROR" ) return newFITSheader;   //pass on ERROR message to client.

  string message = "FrameAcqServer>";
  string LastRec = *(newFITSheader->stringAt( newFITSheader->elements()-1 ));

  if( LastRec.find("END") != 0 && LastRec.find("   ") != 0 ) {
    UFStrings* EndRec =  new (nothrow) UFStrings( "END", "END" );
    UFStrings* tempFITSheader = new (nothrow) UFStrings( newFITSheader, EndRec ); //concatenate "END".
    delete EndRec;
    delete newFITSheader;
    newFITSheader = tempFITSheader;
    message += " Added END record to FITS header.";
  }

  if( _saveFile>=0 )  // _NfhRecs is current # of FITS header records, set in openFITSfile().
    {
      if( newFITSheader->elements() >= _FITSheader->elements() && newFITSheader->elements() <= _NfhRecs )
	newFITSheader->rename( "FINAL " + newFITSheader->name() );
      else {
	message = "FrameAcqServer> Invalid final FITS header (wrong # recs)";
	clog << message << endl;
	return new UFStrings( "ERROR", &message ) ;
      }
    }

  ::pthread_mutex_lock( &_FITSheaderMutex );

  if( _FITSheader != 0 ) delete _FITSheader;
  _FITSheader = newFITSheader;
  _FITSheader->stampTime( UFRuntime::currentTime() );

  message += " Accepted new FITS header.";

  ::pthread_mutex_unlock( &_FITSheaderMutex );
  clog << message << endl;
  return new UFStrings( _FITSheader->name(), &message ) ;
}
//---------------------------------------------------------------

UFStrings* appendFITSheader( UFStrings* partialFITSheader )
{
  UFStrings* basicFITSheader = createFITSheader( false );  //false == no END record.

  if( basicFITSheader->name() == "ERROR" ) 
    {
      delete partialFITSheader;
      return basicFITSheader;   //pass on ERROR message to client.
    }

  UFStrings* combinedFITSheader = new (nothrow) UFStrings( basicFITSheader, partialFITSheader );

  if( combinedFITSheader != 0 )
    combinedFITSheader->rename("HEADER (basic FITS + extra_info)");

  delete basicFITSheader;
  delete partialFITSheader;

  return combinedFITSheader;
}
//-------------------------------------------------------------------

UFStrings* createFITSheader( bool addEND )
{
  // create a default basic FITS header using the FrameConfig and the ObsConfig.
  string message;

  UFObsConfig* theObsConfig = accessObsConfig( true );

  if( theObsConfig == 0 )
    {
      theObsConfig = accessObsConfig( false );
      message = "createFITSheader> working ObsConfig is NOT defined! ";
      clog << "ERROR: " << message << endl;
      return new UFStrings( "ERROR", &message ) ;
    }

  UFFrameConfig* theFrameConfig = accessFrameConfig( true );

  if( theFrameConfig == 0 )
    {
      theFrameConfig = accessFrameConfig( false );
      theObsConfig = accessObsConfig( false );
      message = "createFITSheader> working FrameConfig is NOT defined! ";
      clog << "ERROR: " << message << endl;
      return new UFStrings( "ERROR", &message ) ;
    }

  UFFITSheader* FITSheader = new (nothrow) UFFITSheader(*theFrameConfig,
							*theObsConfig,
							"Written by " + FrameAcqVersion);
  theFrameConfig = accessFrameConfig( false );
  theObsConfig = accessObsConfig( false );

  if( FITSheader == 0 ) {
    message = "createFITSheader> memory allocation failure!";
    clog << "ERROR: " << message <<endl;
    return new (nothrow) UFStrings( "ERROR", &message ) ;
  }

  if( addEND ) FITSheader->end();

  return FITSheader->Strings("HEADER (basic FITS)");
}
//-------------------------------------------

UFStrings* getFITSheader()
{
  string message;

  if( _FITSheader != 0 )   // make copy of the FITS header for a FrameServerThread:
    {
      ::pthread_mutex_lock( &_FITSheaderMutex );

      UFStrings* FHcopy = new (nothrow) UFStrings( _FITSheader );

      if( FHcopy == 0 ) {
	clog << "ERROR: ";
	message = "getFITSheader> memory allocation failure!";
	FHcopy = new (nothrow) UFStrings( "ERROR", &message ) ;
      }
      else {
	message = "FrameAcqServer> sending copy of _FITSheader...";
	if( _FITS_fileName.length() > 0 )
	  FHcopy->rename( FHcopy->name() + ": " + _FITS_fileName );
	FHcopy->stampTime( _FITSheader->timeStamp() );
      }
      ::pthread_mutex_unlock( &_FITSheaderMutex );
      clog << message <<endl;
      return FHcopy;
    }
  else
    {
      message = "FrameAcqServer> FITS header not yet defined!";
      clog << "ERROR: " << message << endl;
      return( new (nothrow) UFStrings( "ERROR", &message ) );
    }
}
//--------------------------------------------------------------------------

int writeFITSheader( UFStrings* FITS_header, int fileDesc )
{
  if( FITS_header == 0 ) {
    clog << "ERROR: writeFITSheader> UFStrings* FITS_header == NULL pointer!" <<endl;
    return(0);
  }

  char* Line = new char[80];
  for( int i=0; i < 80; i++ ) Line[i] = ' ';
  const char* BlankLine = (const char* )Line;
  int nbw = 0;

  for( int i=0; i < FITS_header->elements(); i++ )
    {
      nbw += ::write( fileDesc, FITS_header->valData(i), min( FITS_header->valSize(i), 80 ) );

      if( FITS_header->valSize(i) < 80 )
	nbw += ::write( fileDesc, BlankLine, 80 - FITS_header->valSize(i) );
    }

  int NheadRecs = FITS_header->elements();
  int NoverRecs = FITS_header->elements() % 36 ;

  if( NoverRecs > 0 )
    {
      int NblankRecs = 36 - NoverRecs;
      NheadRecs += NblankRecs;
      for( int i=0; i < NblankRecs; i++ ) nbw += ::write( fileDesc, BlankLine, 80 );
    }

  if( nbw < (NheadRecs*80) ) {
    clog << "writeFITSheader> wrote " << nbw << " out of "
	 << NheadRecs*80 << " bytes in FITS header (" << strerror(errno) << ")." <<endl;
    return(0);
  }

  return NheadRecs;
}
//---------------------------------------------------------------

string extractDirectoryName( string FileName, string& ErrMess )
{
  if( FileName.find("/") != 0 )
    {
      ErrMess = "FrameAcqServer> must give full absolute pathname of FITS file: " + FileName;
      return( "ERROR" );
    }

  int dirpos = FileName.find_last_of("/");

  if( dirpos < 2 )
    {
      ErrMess = "FrameAcqServer> invalid (no directory) FITS file name: " + FileName;
      return( "ERROR" );
    }

  if( FileName.length() < 6 )
    {
      ErrMess = "FrameAcqServer> invalid (too short) FITS file name: " + FileName;
      return( "ERROR" );
    }

  return FileName.substr(0,dirpos);
}
//---------------------------------------------------------------

UFStrings* openFITSfile( UFStrings* FileCommand, bool autoClose )
{
  string message;
  string Status = "OK ?";

  UFObsConfig* theObsConfig = accessObsConfig( true );
  accessObsConfig( false );   //release the lock but keep the pointer just to check it.

  if( theObsConfig == 0 )
    {
      message = "FrameAcqServer> working ObsConfig is NOT defined! ";
      Status = "ERROR" ;
    }
  else if( _saveFile>=0 )
    {
      message = "FrameAcqServer> FITS file allready open: " + _FITS_fileName;
      Status = "NOOP" ;
    }
  else if( _FITSheader == 0 )
    {
      message = "FrameAcqServer> FITS header is NOT defined.";
      Status = "ERROR" ;
    }
  else
    {
      _FITS_fileName = *(FileCommand->stringAt(0));
      int remHostPos = _FITS_fileName.find(":");

      if( remHostPos > 0 )
	{
	  string remHostName = _FITS_fileName.substr( 0, remHostPos );
	  _FITS_fileName = _FITS_fileName.substr( remHostPos+1 );
	  if( extractDirectoryName( _FITS_fileName, message ) != "ERROR" )
	    {
	      if( remHostName != UFRuntime::hostname() )
		_FITS_fileName = "/net/" + remHostName + _FITS_fileName;
	    }
	}
      else if( remHostPos == 0 ) _FITS_fileName = _FITS_fileName.substr( 1 );

      string Directory = extractDirectoryName( _FITS_fileName, message );

      if( Directory == "ERROR" )
	{
	  _FITS_fileName.erase();
	  Status = "ERROR";
	}
      else {
	mode_t UserMask = ::umask(0);

	if( ::mkdir( Directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH ) < 0 )
	  {
	    if( errno != EEXIST )
	      {
		::umask(UserMask);
		string error = strerror(errno);
		message = "FrameAcqServer> Failed create directory (" + error + "): " + Directory;
		clog << "ERROR: " << message << " . " << endl;
		delete FileCommand;
		return new UFStrings( "ERROR", &message ) ;
	      }
	  }
	else clog << "FrameAcqServer> created directory: " << Directory << " . " << endl;

	_saveFile = ::open( _FITS_fileName.c_str(), O_RDWR|O_CREAT|O_EXCL, S_IRUSR|S_IRGRP|S_IROTH );
	::umask(UserMask);
	clearWriteList();
	_frameWriteError = false; //make sure to reset this!!!!

	if( _saveFile>=0 )
	  {
	    ::pthread_mutex_lock( &_FITSheaderMutex );
	    _FITSheader->stampTime( UFRuntime::currentTime() );

	    if( (_NfhRecs = writeFITSheader( _FITSheader, _saveFile )) > 0 ) {
	      Status = "OPEN" ;
	      message = "FrameAcqServer> FITS header written to file: " + _FITS_fileName;
	    }
	    else { //indicate write error occurred but proceed anyway to see what happens...
	      Status = "ERROR" ;
	      string error = strerror(errno);
	      message = "FrameAcqServer> ERROR (" + error + ") writing FITS header to file: "
		+ _FITS_fileName;
	    }

	    ::pthread_mutex_unlock( &_FITSheaderMutex );
	    _Nfiles++;
	    ::pthread_mutex_lock( &_fileStatusMutex );
	    _autoCloseFile = autoClose;
	    ::pthread_mutex_unlock( &_fileStatusMutex );

	    // reset the write count to zero indicating this is a new file:
	    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	    theFrameConfig->setFrameWriteCnt( 0 );
	    theFrameConfig = accessFrameConfig( false );
	  }
	else {
	  string error = strerror(errno);
	  message = "FrameAcqServer> Failed open FITS file (" + error + "): " + _FITS_fileName;
	  _FITS_fileName.erase();
	  Status = "ERROR" ;
	}
      }
    }

  delete FileCommand;
  clog << Status << ": " << message << endl;
  return new UFStrings( Status, &message ) ;
}
//---------------------------------------------------------------

UFStrings* autoCreateFITSfile( UFStrings* FileCommand )
{
  if( FileCommand->elements() < 1 ) {
    string message = "FrameAcqServer> missing the root directory name!";
    delete FileCommand;
    clog << "ERROR: " << message << endl;
    return new UFStrings( "ERROR", &message ) ;
  }

  string rootDir = *(FileCommand->stringAt(0));
  string Directory = rootDir + "/" + UFRuntime::currentTime();  //use year and day of year.
  size_t cpos = Directory.find(":");
  Directory = Directory.substr( 0, cpos ) + "-" + Directory.substr( cpos+1, 3 );

  if( _saveDirectory != Directory ) { //new day, new directory...
    _saveDirectory = Directory;
    _saveDirCount = 0;
    _NfilinDir = 0;
  }

  if( _saveDirCount > 0 ) Directory = Directory + "." + UFRuntime::numToStr( _saveDirCount );
  mode_t UserMask = ::umask(0);
  int stat;

  if( (stat = ::mkdir( Directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH )) < 0 )
    {
      if( errno == EEXIST )
	{
	  if( _NfilinDir < 1 ) {
	    clog << "FrameAcqServer> directory [" << Directory << "] exists...try new version..."<<endl;
	    while( errno == EEXIST && _saveDirCount < 999 ) {
	      errno = 0;
	      Directory = _saveDirectory + "." + UFRuntime::numToStr( ++_saveDirCount );
	      stat = ::mkdir( Directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH );
	    }
	  }
	  else {
	    stat = 0;
	    clog << "FrameAcqServer> directory [" << Directory << "] exists...reusing it..." << endl;
	  }
	}
      if( stat < 0 ) {
	::umask(UserMask);
	string error = strerror(errno);
	string message = "FrameAcqServer> Failed create directory (" + error + "): " + Directory;
	clog << "ERROR: " << message << " . " << endl;
	delete FileCommand;
	return new UFStrings( "ERROR", &message ) ;
      }
    }
  else clog << "FrameAcqServer> created directory: " << Directory << " . " << endl;

  ::umask(UserMask);
  string fileName = UFRuntime::numToStr( ++_NfilinDir );
  while( fileName.length() < 3 ) fileName = "0" + fileName;
  fileName = Directory + "/S" + fileName + ".fits";
  UFStrings* openReply = openFITSfile( new UFStrings("OPEN", &fileName) );

  if( openReply->name() == "OPEN" )
    {
      vector< string > archinfo;
      archinfo.push_back( fileName );
      archinfo.push_back( UFRuntime::hostname() );
      return new UFStrings( openReply, new UFStrings("FILE", archinfo) ) ;
    }
  else {
    _NfilinDir--;
    return openReply;
  }
}
//---------------------------------------------------------------

UFStrings* closeFITSfile( UFStrings* FileCommand )
{
  string message;
  string Status = "OK ?";

  if( _saveFile>=0 )
    {
      //first check if strings in recvd object are intended to be the final FITS header:

      if( FileCommand->elements() >= _FITSheader->elements() && FileCommand->elements() <= _NfhRecs )
	{
	  ::pthread_mutex_lock( &_FITSheaderMutex );
	  delete _FITSheader;
	  _FITSheader = FileCommand;
	  _FITSheader->stampTime( UFRuntime::currentTime() );
	  _FITSheader->rename("FINAL HEADER");
	  ::pthread_mutex_unlock( &_FITSheaderMutex );
	}
      else {
	if( FileCommand->elements() > 9 ) //warn of attempted final FITS header but wrong size.
	  clog << "WARNING: FrameAcqServer> invalid final FITS header (wrong # recs)"<<endl;
	delete FileCommand;
      }

      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
      int FrameTotal = theFrameConfig->frameObsSeqTot();
      if( FrameTotal <= 0 ) FrameTotal = theFrameConfig->frameGrabCnt();
      int FrameCount = theFrameConfig->frameObsSeqNo();
      theFrameConfig = accessFrameConfig( false );

      //if writing of frames is not finished then mark file to be closed when done.
      //if a write error occurred then allow closing file.

      if( ( FrameCount < FrameTotal || !grabListEmpty() || !writeListEmpty() ) && !_frameWriteError )
	{
	  ::pthread_mutex_lock( &_fileStatusMutex );
	  _autoCloseFile = true;
	  ::pthread_mutex_unlock( &_fileStatusMutex );
	  Status = "AUTO_CLOSE" ;
	  message = "FrameAcqServer> will ";
	  ::pthread_mutex_lock( &_FITSheaderMutex );
	  if( (_FITSheader->name()).find("FINAL") != string::npos )
	    message += "write final FITS header and ";
	  ::pthread_mutex_unlock( &_FITSheaderMutex );
	  message += "close FITS file: ";
	  message += _FITS_fileName;
	  clog << Status << ": " << message << endl;
	}
      else //re-write FITS header and close the file:
	{
	  ::pthread_mutex_lock( &_FITSheaderMutex );
	  message = "FrameAcqServer> ";

	  if( (_FITSheader->name()).find("FINAL") != string::npos )
	    {
	      if( ::lseek( _saveFile, 0, SEEK_SET ) == 0 )
		{
		  if( writeFITSheader( _FITSheader, _saveFile ) > 0 ) {
		    Status = "CLOSE" ;
		    message += "wrote final FITS header and ";
		  }
		  else {
		    Status = "ERROR?" ;
		    message += "ERROR writing final FITS header; ";
		  }
		}
	      else {
		Status = "ERROR?" ;
		message += "could NOT write final FITS header (lseek failed); ";
	      }
	    }
	  else Status = "CLOSE" ;

	  ::close(_saveFile); _saveFile = -1;
	  message += "closed FITS file: ";
	  message += _FITS_fileName;
	  clog << Status << ": " << message << endl;

	  theFrameConfig = accessFrameConfig( true );
	  string ErrMess = theFrameConfig->name();
	  theFrameConfig = accessFrameConfig( false );
	  if( ErrMess.find("ERROR") == string::npos )
	    ErrMess = "";
	  else
	    ErrMess += ": ";

	  vector< string > filenames;
	  filenames.push_back(_FITS_fileName );
	  string accSigFileName = _FITS_fileName + ".accsig";

	  if( writeAccumSigToFile( accSigFileName ) ) //write final accumulated signal image
	    filenames.push_back( accSigFileName );    // and keep track of files written.

	  ::pthread_mutex_lock( &_fileStatusMutex );
	  delete FITS_FileWrittenStatus;
	  FITS_FileWrittenStatus = new UFStrings( ErrMess + Status + ": " + message, filenames );
	  _autoCloseFile = false;
	  ::pthread_mutex_unlock( &_fileStatusMutex );
	  _FITS_fileName.erase();
	  ::pthread_mutex_unlock( &_FITSheaderMutex );
	}
    }
  else
    {
      message = "FrameAcqServer> currently NOT saving to any file.";
      Status = "NOOP" ;
      clog << Status << ": " << message << endl;
      int minfhrecs = 9;
      if( _FITSheader ) minfhrecs = _FITSheader->elements();

      //check if strings are intended to be final FITS header:
      if( FileCommand->elements() >= minfhrecs ) {
	FileCommand->rename("FINAL HEADER");
	return new UFStrings( defineFITSheader( FileCommand ), new UFStrings( Status, &message ) );
      }
      else delete FileCommand;
    }

  return new UFStrings( Status, &message ) ;
}
//------------------------------------------Setup Agent Connections for query of FITS header---

bool AgentsConnected( string FSThreadName )
{
  if( !ufFITS ) ufFITS = new UFFITSClient( FSThreadName );

  if( agentConnections.size() <=0 )
    {
      if( agentHost.length() < 2 ) agentHost = UFRuntime::hostname();
      clog << FSThreadName << "> Locating & connecting to Device Agents on host: "<<agentHost<<endl;

      if( ufFITS->locateAgents( agentHost, agentLocs ) <= 0 )
	{
	  clog<<"ERROR: " << FSThreadName << "> no Device Agents located on host: "<<agentHost<<endl;
	  return false;
	}
  
      if( ufFITS->connectAgents( agentLocs, agentConnections ) <= 0 )
	{
	  clog<<"ERROR: "<< FSThreadName<<"> NO connections to device agents on host: "<<agentHost<<endl;
	  return false;
	}
    }
  return true;
}
//-------------------------------------------------------------------

UFStrings* fetchFITSheader( string FSThreadName )
{
  // fetch all parts of FITS header from device agents:
  string message;

  if( AgentsConnected( FSThreadName ) )
    {
      UFStrings* agentFITSheaders = ufFITS->fetchAllFITS( agentConnections, 7.0, true );

      if( agentFITSheaders == 0 ) {
	agentConnections.clear();
	message = "fetchFITSheader> UFFITSClient::fetchAllFITS from agents failed!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
      }

      if( agentFITSheaders->elements() < 10 ) {
	agentConnections.clear();
	message = "fetchFITSheader> FITS headers from agents are NOT valid!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
      }

      agentFITSheaders->rename("HEADER (from agents)");
      return agentFITSheaders;
    }
  else {
	message = "fetchFITSheader> failed to connect to agents!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
  }
}

#endif /* __FrameWriteThread_cc__ */
