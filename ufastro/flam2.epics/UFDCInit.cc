#if !defined(__UFDCINIT_DC__)
#define __UFDCINIT_DC__ "$Name:  $ $Id: UFDCInit.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCINIT_DC__;

#include "UFDCInit.h"
#include "UFInit.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"
#include "UFRuntime.h"
#include "UFTimeStamp.h"

// ctors:

// public static class funcs can be used everywhere
int UFDCInit::reconnect(const string& name, string& errmsg, int& err) {
  int port= 0;
  string host = UFDCSetup::agentHostAndPort(port);
  if( host.empty() ) {
    clog<<"UFDCInit::(re)connect> no hostname? host: "<<host<<", using hostname() ..."<<endl;
    host = UFRuntime::hostname();
    UFDCSetup::setHost(host);
  }
  if( port <= 0) {
    clog<<"UFDCInit::(re)connect> no port? poer: "<<port<<", using 52008 ..."<<endl;
    UFDCSetup::setPort(port);
  }

  //if( _verbose )
  clog<<"UFDCInit::(re)connect> "<<name<<" to UFMCE Agent host: "<<host<<", on port: "<<port<<endl;

  err= 1;
  errmsg = "Failed DCAgent Connect/Greeting";
  if( UFDCSetup::_connection == 0 ) {
    UFDCSetup::_connection = new UFClientSocket;
  }
  else {
    clog<<"UFDCInit::reconnect> close prior connection..."<<endl;
    UFDCSetup::_connection->close();
    delete UFDCSetup::_connection;
    UFDCSetup::_connection = new UFClientSocket;
  }
  bool block= false, validsoc= true;
  int socfd = UFDCSetup::_connection->connect(host, port, block);
  if( socfd < 0 ) {
    validsoc = false;
    clog<<"UFDCInit::reconnect> (nonblocking) to UFMCE Agent failed, port: "<<port<<", host: "<<host<<endl;
    //return socfd;
  }
  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  validsoc = UFDCSetup::_connection->validConnection();
  int cnt = 3;
  while( --cnt > 0 && !validsoc ) { // try a few times
    validsoc = UFDCSetup::_connection->validConnection();
    if( !validsoc ) {
      clog<<"UFDCInit::reconnect> UFMCE Agent failed validation? trycnt: "<<cnt<<endl;
      UFRuntime::mlsleep(0.25);
    }
  }

  int ns = -1;
  if( validsoc ) {
    UFTimeStamp greet(name);
    ns = UFDCSetup::_connection->send(greet);
    ns = UFDCSetup::_connection->recv(greet);
    //if( _verbose )
      clog<<"UFDCInit::connect> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;
  }
  if( ns <= 0 ) {
    clog<<"UFDCInit::connect> unable to transact greeting with UFMCE agent, invalid connection..."<<endl;
    UFDCSetup::_connection->close();
    delete UFDCSetup::_connection; UFDCSetup::_connection = 0;
    return -1;
  }                                                                             
  return socfd;
}

// pthread static func:
void* UFDCInit::connection(void* p) {
  UFInit::_DCthrdId = pthread_self();
  // just connects, if needed, to mce4 agent
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFGem* rec = arg->_rec;
  UFCAR* car = arg->_car;
  string name = "anon";
  if( rec )
    name = rec->name(); 

  int err= 0, fdsoc= 0;
  string errmsg;
  if( !_sim )
    fdsoc = reconnect(name, errmsg, err);

  // simple simulation...
  // set car idle after 1/2 sec:
  if( _sim ) UFPosixRuntime::sleep(0.5);
  // set car idle 
  if( car ) {
    if( fdsoc < 0 )
      car->setErr(errmsg, err);
    else
      car->setIdle();
  }
  else if( rec ) {
    rec->setIdle();
  }

  delete arg;
  UFInit::_DCthrdId = 0;

  // exit daemon thread
  return p;
}

/// arg:
int UFDCInit::connect(UFDCSetup::DetParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCInit::connection, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCInit::_create() {
  UFPVAttr* pva= 0;
  string host = UFRuntime::hostname();
  string pvname = _name + ".host";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);
  string s = pva->valString();
  clog<<"UFDCInit::_create> "<<pvname<<" == "<<s<<", hostname == "<<host<<endl;

  pvname = _name + ".port";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52008); 
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform initial connection to portescap motor agent
/// virtual funcs:
/// perform initial connection too all agents

int UFDCInit::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".host";
  UFPVAttr* pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) {
      string host = pva->valString();
      clog<<"UFDCInit::genExec> "<<pvname<<" == "<<host<<", hostname == "<<UFRuntime::hostname()<<endl;
      if( !host.empty() ) UFDCSetup::setHost(host);
      //pva->clearVal(); // clear?
    }
  }
  pvname = _name + ".port";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) {
      int port = (int)pva->getNumeric(); // input is anything non-null
      clog<<"UFDCInit::genExec> "<<pvname<<" == "<<port<<endl;
      if( port > 0 ) UFDCSetup::setPort(port);
      //pva->clearVal(); // clear?
    }
  }
  UFDCSetup::DetParm* p = new UFDCSetup::DetParm;
  int stat = connect(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFDCInit::genExec> failed to start connection pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCInit::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFDCINIT_DC__
