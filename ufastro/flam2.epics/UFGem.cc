#if !defined(__UFGEM_CC__)
#define __UFGEM_CC__ "$Name:  $ $Id: UFGem.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGEM_CC__;
  
#include "UFGem.h"
#include "UFSIR.h"
#include "UFPVAttr.h"
#include "UFPV.h"
#include "UFCAServ.h"
#include "UFPosixRuntime.h"

// IS:
#include "UFAbort.h" 
#include "UFApply.h" 
#include "UFContinue.h"
#include "UFDatum.h"
#include "UFDebug.h"
#include "UFEndGuide.h"
#include "UFEndObserve.h"
#include "UFEndVerify.h"
#include "UFGuide.h"
#include "UFInit.h"
#include "UFInstrumSetup.h"
#include "UFObserve.h"
#include "UFObsSetup.h"
#include "UFPark.h"
#include "UFPause.h"
#include "UFReboot.h"
#include "UFSetDHS.h"
#include "UFSetWCS.h"
#include "UFSIR.h"
#include "UFStop.h"
#include "UFTest.h"
#include "UFVerify.h"

// Eng:
#include "UFEngObserve.h"
#include "UFFocus.h"

// CC:
#include "UFCCAbort.h"
#include "UFCCDatum.h"
#include "UFCCInit.h"
#include "UFCCPark.h"
#include "UFCCSetup.h"
#include "UFCCStop.h"
#include "UFCCTest.h"

// DC:
#include "UFDCAbort.h"
#include "UFDCDatum.h"
#include "UFDCInit.h"
#include "UFDCPark.h"
#include "UFDCSetup.h"
#include "UFDCStop.h"
#include "UFDCTest.h"
#include "UFDCMCECmd.h"

// EC:
#include "UFECAbort.h"
#include "UFECDatum.h"
#include "UFECInit.h"
#include "UFECPark.h"
#include "UFECSetup.h"
#include "UFECStop.h"
#include "UFECTest.h"

bool UFGem::_fullDB = true;
bool UFGem::_paused = false;
bool UFGem::_sim = false;
bool UFGem::_shutdown = false;
// verbosity:
bool UFGem::_xml = false;
bool UFGem::_verbose = false;
bool UFGem::_vverbose = false;

// global ctor mutex:
pthread_mutex_t* UFGem::_mutex= 0;

// the full set of records in the database
std::map< string, UFGem* > UFGem::_theRecList;
// record name aliases
std::map< string, string > UFGem::_recAlias;

// these 2 lists should be examined every pulse of the heartbeat thread:
// the set of records with pending timeouts:
std::map< string, UFGem* > UFGem::_timeOutList;
pthread_mutex_t* UFGem::_TOmutex= 0; // global mutex for TOList

// the set of records (SIRs) requiring periodic processing:
std::map< string, UFGem* > UFGem::_periodicList;

/// dtor
UFGem::~UFGem() { 
  // since the cas thread may attempt to access these before we exit,
  // do not delete, but set _indbr and _outdbr to 0
  std::map< string, UFPVAttr* >::const_iterator it;
  for( it = begin(); it != end(); ++it ) {
    UFPVAttr* pva = it->second;
    pva->clearDBRec();
    //delete pva; cas thread my be accessing this
  }
}

// ctors:
UFGem::UFGem(const string& recname) :  _name(recname), _type(_Gem), _mark(false), _timeout(0) {
  deque< UFPVAttr* > inpva, outpva; // supplemental inputs & outputs, if any
  _create(inpva, outpva);
};

// ctor for additional input & outputs
UFGem::UFGem(const string& name, const deque< UFPVAttr* >& inpva, const deque< UFPVAttr* >& outpva) : 
  _name(name), _type(_Gem), _mark(false), _timeout(0) {
  _create(inpva, outpva);
}

// static class funcs:
void UFGem::shutdown() {
  clog<<"UFGem::shutdown> ..."<<endl;
  /*
  std::map< string, UFGem* >::const_iterator it;
  for( it = _theRecList.begin(); it != _theRecList.end(); ++it ) {
    UFGem* rec = it->second;
    delete rec;
  }
  _theRecList.clear();
  */
  ::exit(0); // assume shutdown is called from cas::main on termination
}

void UFGem::sigHandler(int signum) {
  //clog<<"UFGem::_sigHandler> caught signal "<<signum<<endl;
  switch(signum) {
    case SIGABRT: _shutdown = true; clog<<"UFGem> caught signal "<<signum<<" SIGABRT"<<endl;
                  break;
    case SIGINT: _shutdown = true; clog<<"UFGem> caught signal "<<signum<<" SIGINT"<<endl;
                 break;
    case SIGTERM: _shutdown = true; clog<<"UFGem> caught signal "<<signum<<" SIGTERM"<<endl;
                  break;
    case SIGPIPE: clog<<"UFGem> caught signal "<<signum<<" SIGPIPE"<<endl;
                  break;
    case SIGUSR1: clog<<"UFGem> caught signal "<<signum<<" SIGUSR1"<<endl;
                  break;
    case SIGUSR2: clog<<"UFGem> caught signal "<<signum<<" SIGUSR2"<<endl;
                  break;
    case SIGALRM: clog<<"UFGem> caught signal "<<signum<<" SIGALRM"<<endl; break;
    case SIGCHLD: clog<<"UFGem> caught signal "<<signum<<" SIGCHLD"<<endl; break;
    case SIGCONT: clog<<"UFGem> caught signal "<<signum<<" SIGCONT"<<endl; break;
    case SIGFPE: clog<<"UFGem> caught signal "<<signum<<" SIGFPE"<<endl; break;
    //case SIGPWR: clog<<"UFGem> caught signal "<<signum<<" SIGPWR"<<endl; break;
    case SIGURG: clog<<"UFGem> caught signal "<<signum<<" SIGURG"<<endl; break;
    case SIGVTALRM: clog<<"UFGem> caught signal "<<signum<<" SIGVTALRM"<<endl; break;
    case SIGXFSZ: clog<<"UFGem> caught signal "<<signum<<" SIGXFSZ"<<endl; break;
    default: clog<<"UFGem::_sigHandler> ignoring signal "<<signum<<endl; break;
  }
}

void UFGem::unmark() { 
  _mark = false;
  UFPVAttr* mpva = pmark();
  if( mpva != 0 )
    mpva->setVal(0); // mpva->clearVal(); // clear is now == MIN_INT, not 0!
}
  
int UFGem::mark(UFPVAttr* pva) {
  if( pva == 0 ) {
    //if( pva->name().find("heart") == string::npos )
    // clog<<"UFGem::mark> "<<pva->name()<<endl;
    int val = validate(pva); 
    if( val < 0 ) { 
      clog<<"UFGem::mark> "<<pva->name()<<", bad validation: "<<val<<endl;
      //unmark();
      //return val;
    } 
  }
  _mark = true;
  //clog<<"UFGem::mark> "<<pva->name()<<"_mark: "<<_mark<<endl;
  UFPVAttr* mpva = pmark();
  if( mpva == 0 ) {
    if( type() == _CAD )
      clog<<"UFGem::mark> CAD "<<name()<<" has no MARK field?"<<endl; 
    return 0;
  }

  return mpva->setVal(1);
}

int UFGem::mark(UFPVAttr* pva, const deque< UFGem* >& dbrecs) {
  /// find the rec. this pv attrib. belongs to and mark it for processing 
  for( int i = 0; i < (int)dbrecs.size(); ++i ) {
    UFGem* rec = dbrecs[i];
    rec->mark();
  }
  return (int) dbrecs.size();
} 

// linux is returning stat == 0 with errno == EINTR from the cas thread; 
// while return stat == 0 with errno == Success from the heartbeat thread;
// which is a bit confusing...
int UFGem::lock() { 
  int stat = -1, err= EINTR, cnt= 3;
  while( (stat != 0 && (err ==  EINTR || err == EBUSY)) && --cnt >= 0 ) {
    //stat = ::pthread_mutex_lock(_mutex); err = errno;
    stat = ::pthread_mutex_trylock(_mutex); err = errno;
    if(  stat != 0 && err != 0 && err != EINTR && err != EBUSY )
      clog<<"UFGem::lock> pthread: "<<pthread_self()<<" attempt lock stat: "<<stat<<", "<<strerror(err)<<" errno= "<<err<<endl;
    if( stat != 0 )
      UFPosixRuntime::sleep(0.01);
  }
  if( stat != 0 ) {
    //if( _vverbose )
      clog<<"UFGem::lock> pthread: "<<pthread_self()<<" failed to lock stat: "<<stat<<", "<<strerror(err)<<" errno= "<<err<<endl;
    return stat;
  }
  //if( _vverbose )
  //clog<<"UFGem::lock> pthread: "<<pthread_self()<<" locked rec mutex: "<<strerror(err)<<" rec: "<<name()<<endl;

  return (int) size();
}

int UFGem::unlock() { // and return size
  int sz = (int) size();
  ::pthread_mutex_unlock(_mutex);
  if( errno != 0 ) {
    //if( _vverbose )
      clog<<"UFGem::unlock> pthread: "<<pthread_self()<<" failed to unlock rec mutex: "<<strerror(errno)<<" errno= "<<errno<<endl;
    sz = -1;
  }
  //if( _vverbose )
  //clog<<"UFGem::unlock> pthread: "<<pthread_self()<<" unlocked rec mutex: "<<strerror(errno)<<" rec: "<<name()<<endl;

  return sz;  
}

int UFGem::lockTO() { 
  int sz = (int) _timeOutList.size();
  if( _TOmutex == 0 ) return sz;
  int stat = -1, err= EINTR, cnt= 3;
  while( (stat != 0 && (err ==  EINTR || err == EBUSY)) && --cnt >= 0 ) {
    //stat = ::pthread_mutex_lock(_TOmutex); err = errno;
    stat = ::pthread_mutex_trylock(_TOmutex); err = errno;
    if(  stat != 0 && err != 0 && err != EINTR && err != EBUSY )
      clog<<"UFGem::lockTO> pthread: "<<pthread_self()<<" attempt lock stat: "<<stat<<", "<<strerror(err)<<" errno= "<<err<<endl;
    if( stat != 0 )
      UFPosixRuntime::sleep(0.01);
  }
  if( stat != 0 ) {
    //if( _vverbose )
      clog<<"UFGem::lockTO> pthread: "<<pthread_self()<<" failed to lock stat: "<<stat<<", "<<strerror(err)<<" errno= "<<err<<endl;
    //return -1;
  }
  //if( _vverbose )
  //clog<<"UFGem::lockTO> pthread: "<<pthread_self()<<" locked TOList mutex: "<<strerror(err)<<endl;

  return sz; //return 0; // allow testing around timeouts
}

int UFGem::unlockTO() { // and return size
  int sz = (int) _timeOutList.size();
  if( _TOmutex == 0 ) return sz;
  int stat = ::pthread_mutex_unlock(_TOmutex);
  if( stat != 0 ) {
      clog<<"UFGem::unlockTO> pthread: "<<pthread_self()<<" failed to unlock rec mutex: "<<strerror(errno)<<" errno= "<<errno<<endl;
    sz = -1;
  }
  //if( _vverbose )
  //clog<<"UFGem::unlockTO> pthread: "<<pthread_self()<<" unlocked rec mutex: "<<strerror(errno)<<endl;

  return sz;  
}

int UFGem::insertTO(UFGem* rec, bool lock) {
  if( rec == 0 )
    return 0;

  if( lock ) lockTO();
  string recname = rec->name();
  //if( _verbose )
  clog<<"UFGem::insertTO> "<<recname<<" timeout: "<<rec->timeout()<<endl;
  _timeOutList[recname] = rec;
  int sz = (int)_timeOutList.size();
  if( lock ) unlockTO();
  return sz;
}

int UFGem::removeTO(UFGem* rec, bool lock) {
  if( rec == 0 )
    return 0;

  int sz = (int)_timeOutList.size();
  string recname = rec->name();
  //if( _verbose )
    clog<<"UFGem::removeTO> "<<recname<<" timeout: "<<rec->timeout()<<" TO sz: "<<sz<<endl;
  if( lock ) lockTO();
  _timeOutList.erase(recname);
  sz = (int)_timeOutList.size();
  if( lock ) unlockTO();
  return sz; 
}

// set pointer to _theRecList if successfull and return size
int UFGem::lockRecList(std::map< string, UFGem* >*& reclist) { 
  reclist = 0;
  int stat = lock();
  if( stat < 0 ) 
    return stat;

  reclist = &_theRecList;
  return (int)reclist->size();
}

// set pointer to _timeOutList if successfull and return size
int UFGem::lockTOList(std::map< string, UFGem* >*& tolist) { 
  tolist = 0;
  int stat = lockTO();
  if( stat < 0 ) 
    return stat;

  tolist = &_timeOutList;
  return (int)tolist->size();
}

/// find all recs marked for processing (for use in apply?)
int UFGem::marked(deque< UFGem* >& marked, deque< int >& timeouts) {
  marked.clear();
  timeouts.clear();

  size_t nr = _theRecList.size();
  if( nr == 0 ) 
    return -1;

  std::map< string, UFGem* >::const_iterator it;
  for( it = _theRecList.begin(); it != _theRecList.end(); ++it ) {
    UFGem* rec = it->second;
    if( rec->marked() ) {
      marked.push_back(rec);
      timeouts.push_back(rec->timeout());
    }
  }

  return (int)marked.size();
}

/// find all attached/linked recs marked for processing (for use in apply)
int UFGem::markedLinks(deque< UFGem* >& marked, deque< int >& timeouts) {
  marked.clear();
  timeouts.clear();

  size_t nr = _reclinks.size();
  if( nr == 0 ) 
    return -1;

  for( size_t i = 0; i < nr; ++i ) {
    UFGem* rec = _reclinks[i];
    if( rec->marked() ) {
      marked.push_back(rec);
      timeouts.push_back(rec->timeout());
    }
  }

  return (int)marked.size();
}

/// process all marked records provided in list, with associated timeouts (for use in apply?)
/// if timeout list is empty use any/def. value provided in optional final arg...
int UFGem::execMarked(const deque< UFGem* >& dbrecs, const deque< int >& timeouts, int timeout) {
  /// process all marked records in list
  for( size_t i = 0; i < dbrecs.size(); ++i ) {
    UFGem* rec = dbrecs[i];
    if( timeouts.size() >= i + 1 ) timeout = timeouts[i];
    execMarked(rec, timeout);
  }    
  return (int) dbrecs.size();
}

/// process specific marked record provided with associated timeouts (for use on SAD/SIR?)
int UFGem::execMarked(UFGem* dbrec, int timeout) {
  /// process all marked records in list
  if( dbrec->marked() ) {
    if( _verbose )
      clog<<"UFGem::execMarked> marked: "<<dbrec->name()<<endl;
    return dbrec->genExec(timeout);
  }
  //if( _verbose )
  //clog<<"UFGem::execMarked> not marked! "<<dbrec->name()<<endl;
  return -1;
}

int UFGem::start(int timeout) {
  // note that CAR has overriden start() and setBusy(), etc.
  // but all other records still make use of this base class (for now)...
  UFGem::RecType rt = type();
  if( rt == UFGem::_SIR ) // always process SADs
    return genExec(0);

  if( _paused ) {
    clog<<"UFGem::start> "<<name()<<", system is paused, timeout: "<<_timeout<<endl;
    return 0;
  }

  if( !marked() && rt == UFGem::_CAD ) {
    clog<<"UFGem::start> "<<name()<<", CAD not marked."<<endl;
    return 0;
  }
  
  /*
  if( timeout > 0 && _timeout > 0 ) {
    clog<<"UFGem::start> busy rectype: "<<rt<<", "<<_name<<", timeout: "<<timeout<<", "<<_timeout<<endl;
    return -1;
  }
  */
  clog<<"UFGem::start> rectype: "<<rt<<", "<<name()<<", timeout: "<<timeout<<", "<<_timeout<<endl;
  // is suplied timeout is <= 0, force processing?
  // Ok to start processing, clear mark and call genExec
  // note these are virtual funcs...
  unmark();

  // marking this should have called validate, no need to do it here:
  /*
  if(timeout > 0) val = validate(); // otherwise assume valid input 
  if( val < 0 ) { // invalid input
    string msg = errMsg(val); setErr(msg, val);
    return val; 
  }
  */

  // calling base class setBusy before genExec allows all inputs to be copied to their 
  // outputs readily before genExec (optionally) clears input(s).
  setBusy(timeout);

  // lock(); int val = genExec(timeout); unlock(); // safer to wrap the genExec in mutex lock?
  int val = genExec(timeout); // don't wrap the genExec in mutex lock?

  if( timeout <= 0 && val >= 0 )
    setIdle();

  return val;
} // start

// this is appropriate for CAD, Apply should override to process linked CADs
// can also be overriden by SIRs and CARs (never unmark these)
int UFGem::directive(int d) {
  switch( d ) {
  case _Clear: clear(); setIdle(); //clog<<"UFGem::directive> clear: "<<name()<<endl; 
     break;
  case _Preset: preset(); setIdle(); //clog<<"UFGem::directive> preset: "<<name()<<endl;
    break;
  case _Start: start(); //clog<<"UFGem::directive> start: "<<name()<<endl;
    break;
  case _Stop: stop(); setIdle(); //clog<<"UFGem::directive> stop: "<<name()<<endl;
    break;
  case _Mark:
  default: mark(); setIdle(); //clog<<"UFGem::directive> mark: "<<name()<<endl;
    break;
  }
  return d;
}

// these are meant to be overriden by any record that has an associated _car (apply, cad, ?)?
// allow this to reset suggested timeout?
void UFGem::setBusy(size_t timeout) { 
  if( _verbose )
    clog<<"UFGem::setBusy> pending completion or timeout in heartbeat ticks: "<<timeout<<endl;
  _paused = false;
  if( timeout > 0 ) _timeout = timeout;
  //insertTO(this); only CARs in TOlist
}
  
// default behavior of base class is to simply set its _timeout and optional proxy's
void UFGem::setBusy(size_t timeout, UFGem* proxy) {
  if( proxy != 0 && this != proxy )
    proxy->setBusy(timeout);
  if( _timeout > 0 ) {
    clog<<"UFGem::setBusy> already busy, _timeout: "<<_timeout<<endl;
    return;
  }

  _timeout = timeout;

  if( _verbose )
    clog<<"UFGem::setBusy> pending completion or timeout in heartbeat ticks: "<<timeout<<endl;

  return;
}

void UFGem::setIdle() { 
  if( _timeout > 0 && _verbose )
    clog<<"UFGem::setIdle> "<<_name<<" clearing remaining timeout: "<<_timeout<<endl;
  _timeout = 0;
  return;
}

string UFGem::errMsg(int val) { 
  string msg = "generic error.";
  if( _verbose )
    clog<<"UFGem> errval: "<<val<<", msg: "<<msg<<endl;
  return msg; 
}

void UFGem::setErr(string& errmsg, int err) {
  if( _verbose )
    clog<<"UFGem::setErr> "<<_name<<"TO remaining timeout: "<<_timeout<<", err: "<< errmsg<<endl; 
  _timeout = 0;
  setPV(errmsg, pmess());
  setPV(err, poerr());
}

// use these to (re)set or (re)define/(re)allocate the UFPVAttr of a recond's field/element
// to the desired data type:
UFPVAttr* UFGem::setPVAttrOf(const string& pvname, const string& sval) {
  clog<<"UFGem::setPVAttrof> "<<pvname<<", sval: "<<sval<<endl;
  UFPVAttr* pva = 0;
  if( find(pvname) != end() ) {
    pva = (*this)[pvname];
    delete pva; // destroy previous
    erase(pvname); // and remove from self
    clog<<"UFGem::setPVAttrof> erased: "<<pvname<<endl;
  }
  pva = new (nothrow) UFPVAttr(pvname, sval);
  clog<<"UFGem::setPVAttrof> "<<pvname<<", sval: "<<sval<<endl;
  return pva;
} 

UFPVAttr* UFGem::setPVAttrOf(const string& pvname, double val) {
  UFPVAttr* pva = 0;
  if( find(pvname) != end() ) {
    pva = (*this)[pvname];
    delete pva; // destroy previous
    erase(pvname); // and remove from self
    clog<<"UFGem::setPVAttrof> erased: "<<pvname<<endl;
  }
  pva = new (nothrow) UFPVAttr(pvname, val);
  clog<<"UFGem::setPVAttrof> "<<pvname<<", dval: "<<val<<endl;
  return pva;
}

UFPVAttr* UFGem::setPVAttrOf(const string& pvname, int val) {
  UFPVAttr* pva = 0;
  if( find(pvname) != end() ) {
    pva = (*this)[pvname];
    delete pva; // destroy previous
    erase(pvname); // and remove from self
    clog<<"UFGem::setPVAttrof> erased: "<<pvname<<endl;
  }
  pva = new (nothrow) UFPVAttr(pvname, val);
  clog<<"UFGem::setPVAttrof> "<<pvname<<", ival: "<<val<<endl;
  return pva;
}

void UFGem::setPV(string& val, UFPVAttr* pva) {
  if( pva == 0 ) pva = pval();
  if( pva == 0 ) return;
  if( _verbose )
    clog<<"UFGem::setPV> "<<val<<", pva "<<pva->name()<<endl; 
  static gdd* gv= 0;
  if( gv == 0 ) gv = new gdd;
  gv->putConvert(val.c_str());
  UFPV::write(*gv, *pva);
  //gdd.unreference(); // dtor is not accessable (private or protected)
}

void UFGem::setPV(int val, UFPVAttr* pva) {
  if( pva == 0 ) pva = pval();
  if( pva == 0 ) return;
  if( _verbose )
    clog<<"UFGem::setPV> "<<val<<", pv: "<<pva->name()<<endl; 
  static gdd* gv= 0;
  if( gv == 0 ) gv = new gdd;
  gv->putConvert(val);
  UFPV::write(*gv, *pva);
  //gdd.unreference(); // dtor is not accessable (private or protected)
}

void UFGem::setPV(double val, UFPVAttr* pva) {
  if( pva == 0 ) pva = pval();
  if( pva == 0 ) return;
  if( _verbose )
    clog<<"UFGem::setPV> "<<val<<", pv: "<<pva->name()<<endl; 
  static gdd* gv= 0;
  if( gv == 0 ) gv = new gdd;
  gv->putConvert(val);
  UFPV::write(*gv, *pva);
}

// validate each pva or all together...
int UFGem::validate(UFGem* dbrec) {
  if( dbrec == 0 )
    return -1;

  string name = dbrec->name();
  clog<<"UFGem::validate> "<<name<<endl;
  std::map< string, UFPVAttr* >::const_iterator it = dbrec->begin(); // iterate thru list of pv's contained in dbrec 
  int val= 0;
  UFPVAttr* pva= 0;
  while( it != dbrec->end() ) {
    name = it->first;
    clog<<"UFGem::validate> rec: "<<dbrec->name()<<endl;
    pva = it->second;
    if( pva ) {
      clog<<"UFGem::validate> pva: "<<pva->name()<<endl;
      val = dbrec->validate(pva);
      if( val < 0 ) break;
    }
  }
  return val;
}

// non static funcs:

// virtuals 

// default genExec should be overriden in extensions
// in principle this can be used for 0 timeouts 
// (directive propogation regardless of mar) as
// well from a validated marked command record (start)
// with non 0 timeout:  
int UFGem::genExec(int timeout) {
  clog<<"UFGem::genExec> "<<_name<<", timeout: "<<timeout<<endl;
  // process fanouts first?
  size_t fanouts =  _pvfanouts.size();
  clog<<"UFGem::genExec> "<<_name<<"< fanouts: "<<fanouts<<endl;
  if( fanouts > 0 ) {
    std::map<string, UFPVAttr* >::const_iterator it;
    for( it = _pvfanouts.begin(); it != _pvfanouts.end(); ++it ) {
      string inpvname = it->first;
      if( find(inpvname) != end() ) { // ok, pv exists in record input list 
        UFPVAttr* outpv = it->second;
        UFPVAttr* inpv = (*this)[inpvname];
	outpv->copyVal(inpv); // will mark input if required
      }
    }
  }

  // process linked records nect?
  int nl = _reclinks.size(); // list of other records linked to this one
  clog<<"UFGem::genExec> "<<_name<<"< linkss: "<<nl<<endl;
  int val= 0;
  for( int i = 0; i < nl; ++i ) {
    UFGem* lnk = _reclinks[i];
    string name = lnk->name();
    if( lnk->marked() ) {
      clog<<"UFGem::genExec> "<< _name <<" processing attached/linked record: "<<name<<endl;
      val = lnk->genExec();
      if( val < 0 ) break;
    }
    else if( _verbose )
      clog<<"UFGem::genExec> not marked, record: "<<name<<endl;
  }

  setPV(val);
  if( val < 0 ) {
    string msg = errMsg(val);
    setErr(msg, val);
  }
  else if( timeout <= 0 ) {
    setIdle();
  }

  return val;
}

// allow optional record name alias
int UFGem::registerRec(const string& recalias) {
  if( _name.empty() ) {
    clog<<" UFGem::registerRec> unnamed record!"<<endl;
    return -1;
  }
  _theRecList[_name] = this;
  if( recalias != "" )
    _recAlias[_name] = recalias;
  //if( _verbose ) 
  //  clog<<" UFGem::registerRec> _name: "<<_name<<", recalias: "<<recalias<<endl;

  std::map< string, UFPVAttr* >::const_iterator it;
  for( it = begin(); it != end(); ++it ) {
    UFPVAttr* pva = it->second;
    UFPVAttr::registerPV(pva); // rec.field == pva name (principal pv name)
    if( recalias != "" ) {
      string pvname = pva->name();\
      size_t dotpos = pvname.find(".");
      if( dotpos != string::npos ) {
        string field = pvname.substr(1+dotpos); 
	string name = recalias; name += "."; name += field;
        UFPVAttr::registerPV(name, pva); // recalias.field
      }
    }
  }

  // each record should have at least one pv alias (recname == recname.val)
  for( it = _pvalias.begin(); it != _pvalias.end(); ++it ) {
    string name = it->first;
    UFPVAttr* pva = it->second;
    UFPVAttr::registerPV(name, pva);
    if( recalias != "" ) {
      string pvname = pva->name();
      size_t dotpos = pvname.find(".");
      if( dotpos != string::npos ) { // must be the recname == recname.val alias
	string name = recalias;
        UFPVAttr::registerPV(name, pva); // recalias == recalias.val == recname = recname.val alias?
      }
      else {
        string fieldalias = pvname.substr(1+dotpos); 
	string name = recalias; name += "."; name += fieldalias;
        UFPVAttr::registerPV(name, pva); // recalias.fieldalias
      }
    }
  }
  size_t npv = size() + _pvalias.size();
  return (int)npv;
}

/// attach/assoc. this record to (input) pva, and conversely, pva to this object
int UFGem::attachInputPV(UFPVAttr* pva) { 
  if( pva == 0 ) {
    clog<<"UFGem::attachInputPV> null pva..."<<endl;
    return size();
  }

  string pvname = pva->name();
  /*
  if( find(pvname) != end() ) {
    UFPVAttr* prevpva = (*this)[pvname];
    prevpva->setDBRecInput(0); // clear self from pva's rec ptr 
    erase(pvname); // remove any pre-existing pva by same name (but do not destroy it?)
  }
  */
  (*this)[pvname] = pva;
  pva->setDBRecInput(this); // attach this record to the pva input 
  //if( pvname.find("heartbeat") != string::npos )
  //clog<<"UFGem::attachInputPV> "<<pvname<<", type: "<<type()<<", _indbr: "<<(int)pva->getDBRecInput()<<endl;

  return size();
}

/// attach/assoc. this record to (input) pva, and conversely, pva to this object
int UFGem::attachOutputPV(UFPVAttr* pva, bool clear) { 
  if( pva == 0 ) {
    clog<<"UFGem::attachOutputPV> null pva..."<<endl;
    return size();
  }
  if( clear ) pva->clearVal();  
  string pvname = pva->name();
  /*
  if( find(pvname) != end() ) {
    UFPVAttr* prevpva = (*this)[pvname];
    prevpva->setDBRecOutput(0); // clear self from pva's rec ptr 
    erase(pvname); // remove any pre-existing pva by same name (but do not destroy it?)
  }
  */
  (*this)[pvname] = pva;
  pva->setDBRecOutput(this); // attach this record to the pva output
    
  size_t dotpos = pvname.find(".");
  if( dotpos != string::npos ) {
    string pvfield = pvname.substr(++dotpos);
    if( pvfield == "VAL") {
      // all gemini records (of interest here) have this field alias:
      //clog<<"UFGem::attachOutputPV> alias: "<<pvname<<" == "<<_name<<endl;
      _pvalias[_name] = pva;
    }
  }
  return size();
}

/// static attach func. makes use of above
UFPVAttr* UFGem::attachInOutPV(UFGem* rec, UFPVAttr* inpva, const string& prepend,
			 const string& append, UFPVAttr* outpva) {
  if( rec == 0 || inpva == 0 )
    return 0;

  rec->attachInputPV(inpva);
  if( outpva == 0 ) {
    string pvname = inpva->name();
    string outpvname = pvname;
    if( prepend.empty() ) { // use append (default) substring
      outpvname += append;
    }
    else { // use prepend instead of append substring 
      string recname = rec->name();
      size_t dot = pvname.find(".");
      if( dot == string::npos ) {
	clog<<"UFGem::attachInOutPV> "<<rec->name()<<", pvname lacks .:"<<pvname<<endl;
	return 0;
      }
      string field = pvname.substr(++dot);
      outpvname = recname + "." + prepend + field;
    }
    outpva = new UFPVAttr(outpvname, inpva);
  }
  outpva->setAssoc(inpva, 0);
  inpva->setAssoc(0, outpva);
  bool clear= true;
  rec->attachOutputPV(outpva, clear);
  return outpva;
}

/// indicate one or more external (i.e. other records's) pvas as output
/// of the (named) pva in this record
int UFGem::attachFanOut(const string& inpvname, UFPVAttr* outpva ) { 
  if( find(inpvname) == end() ) {
    clog<<"UFGem::fanoutPV> pv: "<<inpvname<<", not contained in record (yet): "<<_name<<endl;
    return -1;
  }
  _pvfanouts.insert(pair<string, UFPVAttr*>(inpvname, outpva));
  return _pvfanouts.size();
}

// protected funcs:
void UFGem::_create(const std::deque< UFPVAttr* >& inpva, const std::deque< UFPVAttr* >& outpva) {
  if( _mutex == 0 ) {  
    _mutex = new pthread_mutex_t;
    ::pthread_mutex_init(_mutex, 0);
    /*
    pthread_mutexattr_t mattr;
    ::pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_ERRORCHECK); //PTHREAD_MUTEX_ERRORCHECK_NP);
    ::pthread_mutex_init(_mutex, &mattr);
    */
  }
  if( _TOmutex == 0 ) {  
    _TOmutex = new pthread_mutex_t;
    ::pthread_mutex_init(_TOmutex, 0);
    /*
    pthread_mutexattr_t mattr;
    ::pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_ERRORCHECK); //PTHREAD_MUTEX_ERRORCHECK_NP);
    ::pthread_mutex_init(_TOmutex, &mattr);
    */
  }
  /// min set of pv's for all record types
  UFPVAttr* pva= 0;
  string recname = name();
  string pvname;

  // move this to sub-classes Apply, CAD, CAR, and SIR:
  /*
  pvname = recname + ".VAL";
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int outval
  attachOutputPV(pva);
  */
  // all but the SIR have this, so we'll do this here and ignore it in any SIR
  pvname = recname + ".CLID";
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int input client id
  attachInputPV(pva);

  pvname = recname + ".ICID";
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int input client id
  attachInputPV(pva);

  pvname = recname + ".MESS";
  pva = new (nothrow) UFPVAttr(pvname, "null"); // int input client id
  attachInputPV(pva);

  pvname = recname + ".OCID";
  // all but the SIR have this, so we'll do this here and ignore it in any SIR
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int input client id
  attachOutputPV(pva);

  pvname = recname + ".OMSS";
  // all but the SIR have this, so we'll do this here and ignore it in any SIR
  pva = new (nothrow) UFPVAttr(pvname, "null"); // int input client id
  attachOutputPV(pva);

  // allow user timout input value
  pvname = recname + ".timeout";
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int user timeout
  attachInOutPV(this, pva);

  /// ctor uses default proc. func. for all pvas
  for( int i = 0; i < (int)inpva.size(); ++i ) {
    UFPVAttr* pva = inpva[i];
    attachInputPV(pva);
  }
  for( int i = 0; i < (int)outpva.size(); ++i ) {
    UFPVAttr* pva = outpva[i];
    attachOutputPV(pva);
  }
  //registerRec(); this must be performed by each extension instance, not by base class...
};  

void UFGem::copyInOut(UFPVAttr* pin) {
  if( pin == 0 ) {
    clog<<"UFGem::copyInOut> null PVAttr* pin..."<<endl;
    return;
  }

  UFPVAttr* pout= pin->assocOut(); 
  if( pout == 0 ) {
    clog<<"UFGem::copyInOut> null PVAttr* pout, assoc. with input: "<<pin->name()<<endl;
    return;
  }

  if( pin->isSet() ) { // do not copy cleared (non-set) values?
    if( _verbose )
      clog<<"UFGem::copyInOut> "<<pin->name()<<" to: "<<pout->name()<<endl;
    pout->copyVal(pin);
  }
  else if( _verbose ) {
    clog<<"UFGem::copyInOut> "<<pin->name()<<" not set, no copy to : "<<pout->name()<<endl;
  }

  return;
}

int UFGem::copyAllInOut(UFGem* rec) {
  int nc = -1;
  if( rec == 0 )
    return nc;

  nc = 0;
  std::map< string, UFPVAttr* >::iterator it = rec->begin();
  while( it != rec->end() ) {
    string pvpar = it->first; // should be == pin->name()
    UFPVAttr* pin = it->second;
    if( pin != 0 ) {
      if( rec == pin->getDBRecInput() ) { // only copy from inputs (not outputs!)
        rec->copyInOut(pin);
        ++nc;
      }
    }
    ++it;
  }
  //if( _verbose )
    clog<<"UFGem::copyAllInOut> "<<rec->name()<<" input cnt: "<<nc<<", tot. field cnt: "<<rec->size()<<endl;
  return nc;
}

void UFGem::printRecs() {
  std::map< string, UFGem* >::const_iterator rcit;
  std::map< string, UFPVAttr* >::const_iterator pvit;
  int nr = (int) _theRecList.size(); // set pointer to it if successfull and return size
  cout<<"UFGem::printRecs> rec list size: "<<nr<<endl;
  if( nr <= 0 )
    return;

  for( rcit = _theRecList.begin(); rcit != _theRecList.end(); ++rcit ) {
    UFGem* rec = rcit->second;
    string recname = rcit->first;
    if( _verbose )
      cout<<"UFGem::printRecs> recname: "<<recname<<", "<<rec->name()<<endl;
    for( pvit = rec->begin(); pvit != rec->end(); ++pvit ) {
      string pvname = pvit->first;
      UFPVAttr* pva = pvit->second;
      cout<<"UFGem::printRecs> pvname: "<<pvname;
      if( pva != 0 ) 
         cout<<", "<<pva->name()<<endl;
      else
         cout<<", no pva allocated!"<<endl;
    }
  }
}

void UFGem::printXML() {
  std::map< string, UFGem* >::const_iterator rcit;
  std::map< string, UFPVAttr* >::const_iterator pvit;
  int nr = (int) _theRecList.size(); // set pointer to it if successfull and return size
  //cout<<"UFGem::printRecs> rec list size: "<<nr<<endl;
  if( nr <= 0 )
    return;

  for( rcit = _theRecList.begin(); rcit != _theRecList.end(); ++rcit ) {
    UFGem* rec = rcit->second;
    if( rec->type() != _CAD )
      continue;
    cout<<"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"<<endl;
    cout<<"<!-- Complete set of inputs for F2 Component Control setup."<<endl;
    cout<<"     Copy this and replace any/all 'null' to desired values -->"<<endl;
    cout<<"<UFAstroInstrumParam xmlns:ufastro=\"095799c6-f822-45d7-943e-fa3160050b6f\">"<<endl;
    string recname = rec->name();
    size_t colon = recname.find(":");
    recname = recname.substr(colon);
    cout<<"<GeminiCAD instrument=\""<<UFCAServ::_instrum<<"\" EPICSrecord=\""<<recname<<"\">"<<endl;
    for( pvit = rec->begin(); pvit != rec->end(); ++pvit ) {
      string pvname = pvit->first;
      UFPVAttr* pva = pvit->second;
      if( !pva->isCADInput() )
	continue;
      size_t dot = pvname.rfind(".");
      string pvfield = pvname.substr(dot);
      cout<<"  <input name=\""<<pvfield<<"\"> null </input>"<<endl;
    } // inputs
    cout<<"</GeminiCAD>"<<endl;
    cout<<"</UFAstroInstrumParam>"<<endl;
  } // CAD
}

void UFGem::_heartBeat(const string& instrum) {
  // wrap all calls to genExec() with lock & unlocks to protect against cas writes
  string recname = instrum;
  if( _fullDB )
    recname += ":heartbeat";
  else
    recname += ":eng:heartbeat";

  UFPVAttr *sysheart= 0, *ccheart= 0, *dcheart= 0, *echeart= 0;
  int ccprev= 0, cc= 0, dcprev= 0, dc= 0, ecprev= 0, ec= 0;

  UFGem* sysrec = _theRecList[recname];
  string pvname = recname; pvname += ".VAL"; 
  if( sysrec == 0 ) {
    clog<<"UFGem::_heartBeat> no record found with name: "<<recname<<endl;
    return;
  }
  sysheart = (*sysrec)[pvname];
  if( sysheart == 0 ) {
    clog<<"UFGem::_heartBeat> no PVAttr found with name: "<<pvname<<endl;
    return;
  }

  recname = instrum + ":cc:heartbeat";
  pvname = recname + ".INP"; // input pv of sys heartbeat rec.
  UFGem* rec = _theRecList[recname];
  if( rec == 0 ) {
    clog<<"UFGem::_heartBeat> no record found with name: "<<recname<<endl;
  }
  else {
    ccheart = (*rec)[pvname];
    cc = (int) ccheart->getNumeric();
  }
  recname = instrum + ":dc:heartbeat";
  pvname = recname + ".VAL";
  rec = _theRecList[recname];
  if( rec == 0 ) {
    clog<<"UFGem::_heartBeat> no record found with name: "<<recname<<endl;
  }
  else {
    dcheart = (*rec)[pvname];
    dc = (int) dcheart->getNumeric();
  }
  recname = instrum + ":ec:heartbeat";
  pvname = recname + ".VAL";
  rec = _theRecList[recname];
  if( rec == 0 ) {
    clog<<"UFGem::_heartBeat> no record found with name: "<<recname<<endl;
  }
  else {
    echeart = (*rec)[pvname];
    ec = (int) echeart->getNumeric();
  }
  //if( _vverbose )
  // clog<<"UFGem::_heartBeat> cc: "<<cc<<", dc: "<<dc<<", ec: "<<ec<<endl;
  int sys = (int)sysheart->getNumeric();
  if( cc > ccprev && dc > dcprev && ec > ecprev ) {
    sysheart->setVal(++sys); // set input pva
    rec = sysheart->getDBRecInput();
    if( _vverbose )
      clog<<"UFGem::_heartBeat> sys: "<<sysheart->name()<<", rec: "<<rec->name()<<endl;
    if( sysrec == rec ) { // make sure we are using the right rec. ptr.
     //sysrec->lock(); sysrec->genExec(); sysrec->unlock(); // lock & process record (to post out val) & unlock
      sysrec->genExec(); // process record (to post out val) without lock
    }
  }
  // need to updatre prev. ?
  if( cc > ccprev ) ccprev = cc;
  if( dc > dcprev ) dcprev = dc;
  if( ec > ecprev ) ecprev = ec;

  if( _paused ) // skip timeout logic...
    return;

  // lock timeout rec list mutex (in addition to general rec list mutex? a bit overkill)
  int nt = lockTO(); // prevent any insertions
  if( nt <= 0 ) { // no timeout logic...
    unlockTO();
    return;
  }

  //if( _verbose )
  //clog<<"UFGem::_heartBeat> timeout list size: "<<nt<<", sys. heartbeat: "<<sys<<endl;
  vector< UFGem* > completed;
  vector< UFGem* > timedout;
  std::map< string, UFGem* >::const_iterator it = _timeOutList.begin();
  for( ; it != _timeOutList.end(); ++it ) {
    UFGem* rec = it->second;
    if( rec == 0 ) break;
    //rec->lock(); 
    int t = rec->heartpulse(); // timeout when t < 0; completed when t forced to 0 (and idle or error)?
    string name = rec->name();
    //rec->unlock();
    if( t > 0 && t <= 3 ) 
      clog<<"UFGem::_heartBeat> "<<name<<", about to time-out in ticks "<<t<<endl;
    if( t < 0 ) { // timeout has expired (or if CAR completion to error or idle)
      //if( rec->status() == _Busy ) {
	clog<<"UFGem:_:heartBeat> timeout on: "<<name<<endl;
	timedout.push_back(rec);
      //}
    }
    else if( t >= 0 && rec->status() != _Busy && rec->status() != _Error ) {
      clog<<"UFGem::_heartBeat> t: "<<t<<", "<<name<<" completion with status: "<<rec->status()<<endl;
      completed.push_back(rec);
    } 
    else if( nt == 1 && name.find("applyC") != string::npos ) {
      // special case check if only remaining to is for top level Apply CAR:
      if( UFCAR::_ErrorCnt == 0) { // force sys idle:
	clog<<"UFGem::_heartBeat> "<<rec->name()<<" processing completed without errors..."<<endl;
	rec->setIdle();
        completed.push_back(rec); // timeout has expired (or if CAR completion to error or idle)
      }
    }
    /*
    else { // sys error:
      clog<<"UFGem::_heartBeat> "<<rec->name()<<" processing completed with error(s): "<<UFCAR::_ErrorCnt<<endl;
      string syserr = "Instrument System Error";
      rec->setErr(syserr, 0);
      timedout.push_back(rec);
    }
    */
  }
  unlockTO(); // assuming the removeTO below will lock & unlock the list...

  // timeouts
  int nr = (int) timedout.size();
  if( nr > 0 ) 
    clog<<"UFGem::_heartBeat> removing "<<nr<<" timeouts from TO list..."<<endl;
  for( int i = 0; i < nr; ++i ) {
    UFGem* rec = timedout[i];
    if( rec == 0 ) // huh?
      continue;
    if( rec->status() == _Busy ) { // still busy after timeout expired
      string to = "TimedOut";
      rec->setErr(to, 2);
    }
    // leave obs flags unchanged on timeouts?
    removeTO(rec); // default lock/unlock of TO list
  }

  // completions
  nr = (int) completed.size();
  if( nr > 0 ) 
    clog<<"UFGem::_heartBeat> removing "<<nr<<" completions from TO list..."<<endl;
  for( int i = 0; i < nr; ++i ) {
    UFGem* rec = completed[i];
    if( rec == 0 ) // huh?
      continue;
    removeTO(rec); // default lock/unlock TO list
    // IS logic for successful CAR completions
    string recname = rec->name();
    //UFRuntime::lowerCase(recname);
    // completion of focus or any setup indicates prep becomes false
    if( recname.find("focus") != string::npos ) UFSIR::setPrep(false);
    if( recname.find("instrumSetup") != string::npos ) UFSIR::setPrep(false);
    if( recname.find("obsSetup") != string::npos ) UFSIR::setPrep(false);
    // completion of obs and endobs or abort/stop indicates all flags become false
    if( recname.find("endObserve") != string::npos )
      UFSIR::setFlags(); // all false
    else if( recname.find("observe") != string::npos )
      UFSIR::setFlags(); // all false
    else if( recname.find("abort") != string::npos )
      UFSIR::setFlags(); // all false
    else if( recname.find("stop") != string::npos )
      UFSIR::setFlags(); // all false
    // completion of pause indicates continue?
    if( recname.find("pause") != string::npos ) UFSIR::setRdout(true); // false
    // completion of continue indicates end observe?
    if( recname.find("continue") != string::npos ) UFSIR::setFlags(); // all false
  }

  return;
}

// heartbeat thread func should scan _timeOutList and _periodicList, and for
// each rec. in the combined list, invoke heartbeat, which invokes heartpulse...
void* UFGem::heartThread(void* p) {
  string instrum = *((string *)p);
  string recname = instrum;
  if( _fullDB )
    recname += ":heartbeat";
  else
    recname += ":eng:heartbeat";

  // error if no records exist or if no heartbeats!
  int nr= (int) _theRecList.size(); 
  clog<<"UFGem::heartThread> started instrum DB: "<<instrum<<"; gem record count: "<<nr<<endl;
  if( nr <= 0 )
    return p;

  UFGem *rec = _theRecList[recname];
  if( rec == 0 ) {
    clog<<"UFGem::heartThread> no record found with name: "<<recname<<endl;
    return p;
  }
  while ( ! UFGem::_shutdown ) {
    try {
      _heartBeat(instrum);
    }
    catch(std::exception& e) {
      clog<<"UFCAServD> stdlib exception occured: "<<e.what()<<endl;
    }
    catch(...) {
      clog<<"UFCAServD> unknown exception occured..."<<endl;
    }
    UFRuntime::mlsleep(1); 
  } // forever until shutdown
  
  return p;
}

// start the heartbeat thread:
pthread_t UFGem::startHeartThread(const string& instrum) {
  // need this to be on the heap, unless called from application main thread?
  string* inst = new string(instrum);
  return UFPosixRuntime::newThread(UFGem::heartThread, inst);
}

// this allocates and registers all pvs & recs
int UFGem::createDB(const string& instrum, bool pvalloc, const string& applyname) {
  //_theRecList.reserve(10000);
  //_theRecList.resize(10000);
  // create the SAD first (some CAD funcs will want to look at SAD vals. 
  // for self consistency checks...)
  UFSIR::createAll(instrum);
  //clog<<"UFGem::createDB> all sads: "<<_theRecList.size()<<endl;
  //printRecs();

  // only one apply rec. for entire db
  // this registers itself and creates an associated default car:
  UFApply *apply= 0, *engapply= 0;
  if( applyname.empty() )
    apply = new UFApply(instrum);
  else
    apply = new UFApply(instrum, applyname);

  // always create engineering apply/car recs
  string engname = "eng:apply";
  if( applyname.find(engname) == string::npos )
    engapply = new UFApply(instrum, engname);   
  else
    engapply = apply;

  UFGem* dbrec= 0;
  // Each cad below also registers itself and creates an associated default car:
  // if we want the full db with is ... (-is or -full option or not -eng)
  // Instrument Sequencer Records (IS)

  // process any marked engineering level recs first?
  //if( engapply != apply ) apply->prependRec(engapply); 

  dbrec = new UFReboot(instrum);

  if( _fullDB || engapply != apply ) {
    // highest priority records appear first:
    apply->appendRec(dbrec); // reboot
    dbrec = new UFAbort(instrum); apply->appendRec(dbrec);
    dbrec = new UFStop(instrum); apply->appendRec(dbrec);
    dbrec = new UFPause(instrum); apply->appendRec(dbrec);
    dbrec = new UFContinue(instrum); apply->appendRec(dbrec);
    dbrec = new UFInit(instrum); apply->appendRec(dbrec);
    dbrec = new UFDatum(instrum); apply->appendRec(dbrec);
  }
  else {
    engapply->appendRec(dbrec); // reboot from eng. db level?
  }

  //CC, DC, EC control (engineering level) highest priority:
  dbrec = new UFCCAbort(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCAbort(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECAbort(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  dbrec = new UFCCStop(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCStop(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECStop(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  //dbrec = new UFCCContinue(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  //dbrec = new UFDCContinue(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  //dbrec = new UFECContinue(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  dbrec = new UFCCDatum(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCDatum(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECDatum(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFCCPark(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCPark(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECPark(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  // Remainder of engineering DB (lower priority)
  // engineering observe cad/car:
  dbrec = new UFEngObserve(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  
  // and engineering focus cad/car:
  dbrec = new UFFocus(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

   // Component control (engineering):
  dbrec = new UFCCInit(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFCCSetup(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFCCTest(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  // Detector control (engineering):
  dbrec = new UFDCInit(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCSetup(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCTest(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFDCMCECmd(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  // Environment control (engineering):
  dbrec = new UFECInit(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECSetup(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);
  dbrec = new UFECTest(instrum); engapply->appendRec(dbrec); if( engapply != apply ) apply->appendRec(dbrec);

  if( _fullDB || engapply != apply ) { // remainder of the IS DB (lower priority)
    dbrec = new UFDebug(instrum); apply->appendRec(dbrec);
    dbrec = new UFEndGuide(instrum); apply->appendRec(dbrec);
    dbrec = new UFEndObserve(instrum); apply->appendRec(dbrec);
    dbrec = new UFEndVerify(instrum); apply->appendRec(dbrec);
    dbrec = new UFGuide(instrum); apply->appendRec(dbrec);
    dbrec = new UFInstrumSetup(instrum); apply->appendRec(dbrec);
    dbrec = new UFObserve(instrum); apply->appendRec(dbrec);
    dbrec = new UFObsSetup(instrum); apply->appendRec(dbrec);
    dbrec = new UFPark(instrum); apply->appendRec(dbrec);
    dbrec = new UFSetDHS(instrum); apply->appendRec(dbrec);
    dbrec = new UFSetWCS(instrum); apply->appendRec(dbrec);
    dbrec = new UFTest(instrum); apply->appendRec(dbrec);
    dbrec = new UFVerify(instrum); apply->appendRec(dbrec);
  }

  //clog<<"UFGem::createDB> total record cnt: "<<_theRecList.size()<<endl;
  if( _verbose )
    printRecs();
  if( _xml )
    printXML();

  if( !pvalloc )
    return (int)_theRecList.size();

  // allocate all db pvs from list:
  std::map< string, UFGem* >::iterator itdb = UFGem::_theRecList.begin();
  for( ; itdb != UFGem::_theRecList.end(); ++itdb ) {
    string recname = itdb->first;
    if( recname.empty() ) {
      clog<<"UFGem::createDB> empty recname, skip..."<<endl;
      continue;
    }
    UFGem* rec = itdb->second;
    if( rec == 0 ) {
      clog<<"UFGem::createDB> null recptr for recname: "<<recname<<", skip..."<<endl;
      continue;
    }
    std::map< string, UFPVAttr* >::iterator itpv = rec->begin();
    for( ; itpv != rec->end(); ++itpv ) {
      string pvname = itpv->first;
      //UFPVAttr* pva = itpv->second;
      UFPV* pv = UFCAServ::createPV(pvname, UFCAServ::_instance);
      if( pv == 0 )
	clog<<"UFGem::createDB> failed to create PV: "<<pvname<<endl;
    }
  }
  return (int)_theRecList.size();

} // UFGem::createDB

#endif // __UFGEM_CC__
