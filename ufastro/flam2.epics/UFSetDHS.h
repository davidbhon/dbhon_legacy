#if !defined(__UFSetDHS_h__)
#define __UFSetDHS_h__ "$Name:  $ $Id: UFSetDHS.h,v 0.2 2005/05/25 16:19:00 hon Exp $"
#define __UFSetDHS_H__(arg) const char arg##SetDHS_h__rcsId[] = __UFSetDHS_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFSetDHS : public UFCAD {
public:
  struct Parm { UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(UFObserve::Parm* op= 0, UFObsSetup::Parm* osp= 0, UFInstrumSetup::Parm* isp= 0): _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFSetDHS(const string& instrum= "instrum") : UFCAD(instrum+":setDhsInfo") { _create(); }
  inline virtual ~UFSetDHS() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* datalabel(void* p);
  static int label(UFSetDHS::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFSetDHS_h__
