#if !defined(__UFSIR_cc__)
#define __UFEnvSIR_cc__ "$Name:  $ $Id: UFEnvSIR.cc 14 2008-06-11 01:49:45Z hon $"

#include "UFEnvSIR.h"
__UFEnvSIR_H__(__UFEnvSIR_cc);

#include "UFPVAttr.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"

#include "strstream"

// statics
// static method to create default instance
UFEnvSIR* UFEnvSIR::create(const string& instrum) {
  string recname = instrum + ":ec:heartbeat";
  vector< UFPVAttr* > inputs, outputs;
  string pvname = recname; pvname += ".vacuum"; // (pfieffer) vacuum monitor agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  pvname = recname; pvname += ".montemp"; // mos & optica cryostat temperature monitors (lakeshore 218) agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  pvname = recname; pvname += ".ctrltemp"; // (lakeshore 332) temperature controller agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  pvname = recname; pvname += ".barcd"; // (symbol) barcode controller agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  pvname = recname; pvname += ".lvdt"; // (schaevitz) lvdt controller agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  pvname = recname; pvname += ".plc"; // (automation plc) interlock sys. monitor agent heartbeat
  inputs.push_back(new UFPVAttr(pvname, 0));

  UFEnvSIR* esad = new UFEnvSIR(recname, 0, inputs, outputs);

  return esad;
}

// virtuals
// the processing & 'genSub' record processing entry point:
int UFEnvSIR::genExec(int timeout) {
  UFPVAttr *pin = pinp(), *pout = pval();
  if( pin == 0 || pout == 0 ) {
    if( _verbose )
      clog<<"UFEnvSIR::genExec> pin: "<<(size_t)pin<<", pout: "<<(size_t)pout<<endl;
    return -1;
  }
  if( _verbose )
    clog<<"UFEnvSIR::genExec> pin: "<<pin->name()<<", pout: "<<pout->name()<<endl;

  UFPVAttr* pbarcd = barcd();
  UFPVAttr* plvdt = lvdt();
  UFPVAttr* pplc = plc();
  UFPVAttr* pfeiffer = vacuum();
  UFPVAttr* ls218 = montemp();
  UFPVAttr* ls332 = ctrltemp();

  if( pfeiffer == 0 || ls218 == 0 || ls332 == 0 || pbarcd == 0 ||  plvdt == 0 || pplc == 0 ) {
    if( _verbose )
      clog<<"UFEnvSIR::genExec> no enviroment inputs?"<<endl;
    return -1;
  }
  static double _prev[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
  double pulse = pfeiffer->getNumeric();
  if( pulse > _prev[0] ) 
    _prev[0] = pulse;
  else
    return timeout;

  pulse = ls218->getNumeric();
  if( pulse > _prev[1] ) 
    _prev[1] = pulse;
  else
    return timeout;

  pulse = ls332->getNumeric();
  if( pulse > _prev[2] ) 
    _prev[2] = pulse;
  else
    return timeout;

  pulse = pbarcd->getNumeric();
  if( pulse > _prev[3] ) 
    _prev[3] = pulse;
  else
    return timeout;

  pulse = plvdt->getNumeric();
  if( pulse > _prev[4] ) 
    _prev[3] = pulse;
  else
    return timeout;

  pulse = pplc->getNumeric();
  if( pulse > _prev[5] ) 
    _prev[4] = pulse;
  else
    return timeout;

  pulse = pin->getNumeric();
  ++pulse;
  /*
  strstream s; s<<pulse<<ends;
  tring sp = s.str(); delete s.str();
  pin->setVal(sp); pout->copyVal(pin);
  */
  pin->setVal(pulse); pout->copyVal(pin);
  return timeout;
}

#endif // __UFEnvSIR_cc__
