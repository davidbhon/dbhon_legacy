#if !defined(__UFECDATUM_CC__)
#define __UFECDATUM_CC__ "$Name:  $ $Id: UFECDatum.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECDATUM_CC__;

#include "UFECDatum.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFECDatum::datumEnv(void* p) {
  // use the ECSetup thread func:
  return UFECSetup::envControl(p);
}

/// general purpose datum func. 
int UFECDatum::datum(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // datum (re)sets lakeshore 332 default PIDs and SetPoints (channel A & B)
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec!= 0 ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && c != 0 && car != c ) car->setBusy(timeout);
  }
  // create and run pthread to datum whatever is currently being performed
  // then either reset car to idle or error. thread should free the EnvParm* allocations...
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECDatum::datumEnv, arg);

  return (int) thrid;
}

/// default datum
int UFECDatum::datum(int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec!= 0 ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && c != 0 && car != c ) car->setBusy(timeout);
  }
  // create and run pthread to datum whatever is currently being performed 
  // then either reset car to idle or error. thread should free the EnvParm* allocations...
  UFECSetup::EnvParm* parms = new UFECSetup::EnvParm; // default ctor is datum parms
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECDatum::datumEnv, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECDatum::_create() {
  UFPVAttr* pva= 0;
  string pvname;

  pvname = _name + ".MOSKelvin";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 75.5);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSOnOff";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 3);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSRange";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 3);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 500); 
  attachInOutPV(this, pva);

  pvname = _name + ".MOSI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 100); 
  attachInOutPV(this, pva);

  pvname = _name + ".MOSD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 500); 
  attachInOutPV(this, pva);

  pvname = _name + ".AManualPcnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0.0);
  attachInOutPV(this, pva);

  pvname = _name +  ".CamAKelvin";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 75.5);
  attachInOutPV(this, pva);

  pvname = _name + ".CamARange";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); // range 1-3
  attachInOutPV(this, pva);

  pvname = _name + ".CamAOnOff";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); // range 0 (off) or 1-3 (on)
  attachInOutPV(this, pva);

  pvname = _name + ".CamAP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 30); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamAI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 5); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamAD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".BManualPcnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 60.0);
  attachInOutPV(this, pva);

  pvname = _name +  ".CamBKelvin";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 75.5);
  attachInOutPV(this, pva);

  pvname = _name + ".CamBOnOff";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1); // range 0 (off) or 1 (on)
  attachInOutPV(this, pva);

  pvname = _name + ".CamBP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 30); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamBI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 5); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamBD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  registerRec();

  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform environment control datum
int UFECDatum::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFECDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  double amanual = INT_MIN;
  string pvname = _name + ".AManualPcnt";
  UFPVAttr* pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      amanual = pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int mosonoff= INT_MIN;
  pvname = _name + ".MOSOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      mosonoff = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int rmos= INT_MIN;
  pvname = _name + ".MOSRange";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      rmos = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  double kmos = INT_MIN;
  pvname = _name + ".MOSSetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kmos = pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int pmos= INT_MIN;
  pvname = _name + ".MOSP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pmos = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int imos= INT_MIN;
  pvname = _name + ".MOSI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      imos = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int dmos= INT_MIN;
  pvname = _name + ".MOSD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dmos = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int aonoff= INT_MIN;
  pvname = _name + ".CAMAOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      aonoff = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int rcama= INT_MIN;
  pvname = _name + ".CAMARange";
  pva = (*this)[pvname]; //
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      rcama = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  double kcama = INT_MIN;
  pvname = _name + ".CAMASetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kcama = pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int pcama= INT_MIN;
  pvname = _name + ".CamAP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pcama = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int icama= INT_MIN;
  pvname = _name + ".CamAI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      icama = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int dcama= INT_MIN;
  pvname = _name + ".CamAD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dcama = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int bonoff= INT_MIN;
  pvname = _name + ".CAMBOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      bonoff = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  double bmanual = INT_MIN;
  pvname = _name + ".BManualPcnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      bmanual = pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  double kcamb = INT_MIN;
  pvname = _name + ".CAMBSetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kcamb = pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int pcamb= INT_MIN;
  pvname = _name + ".CamBP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pcamb = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int icamb= INT_MIN;
  pvname = _name + ".CamBI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      icamb = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int dcamb= INT_MIN;
  pvname = _name + ".CamBD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dcamb = (int) pva->getNumeric();
      //pva->clearVal(); // clear
    }
  }

  int lvdt= 0; // datum should turn off the lvdt
  UFECSetup::EnvParm* parms = new UFECSetup::EnvParm(lvdt, kmos, kcama, kcamb, pmos, imos, dmos, rmos, mosonoff,
						     pcama, icama, dcama, rcama, aonoff, amanual,
						     pcamb, icamb, dcamb, bonoff, bmanual);
  int stat = datum(parms, timeout, this, _car);  // perform requested datum

  if( stat < 0 ) { 
    clog<<"UFECDatum::genExec> failed to start datum pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECDatum::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFDATUM_CC__
