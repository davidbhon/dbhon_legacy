#if !defined(__UFCCAbort_h__)
#define __UFCCAbort_h__ "$Name:  $ $Id: UFCCAbort.h,v 0.5 2005/07/26 21:38:11 hon Exp $"
#define __UFCCAbort_H__(arg) const char arg##CCAbort_h__rcsId[] = __UFCCAbort_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFCCAbort : public UFCAD {
public:
  inline UFCCAbort(const string& instrum= "instrum") : UFCAD(instrum+":cc:abort") { _create(); }
  inline virtual ~UFCCAbort() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* abortion(void* p);
  static int abort(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // available to IS
  static int abortAll(int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFCCAbort_h__
