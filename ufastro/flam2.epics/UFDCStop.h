#if !defined(__UFDCSTOP_h__)
#define __UFDCSTOP_h__ "$Name:  $ $Id: UFDCStop.h,v 0.4 2004/06/22 14:46:52 hon beta $"
#define __UFDCSTOP_H__(arg) const char arg##DCStop_h__rcsId[] = __UFDCSTOP_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

class UFDCStop : public UFCAD {
public:
  inline UFDCStop(const string& instrum= "instrum") : UFCAD(instrum+":dc:stop") { _create(); }
  inline virtual ~UFDCStop() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* stopit(void* p);
  static int stop(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFDCSTOP_h__
