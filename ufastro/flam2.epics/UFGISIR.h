#if !defined(__UFGISIR_h__)
#define __UFGISIR_h__ "$Name:  $ $Id: UFGISIR.h,v 0.5 2005/09/26 18:52:42 hon Exp $"
#define __UFGISIR_H__(arg) const char arg##GISIR_h__rcsId[] = __UFGISIR_h__;

#include "UFSIR.h"
#include "vector"

/// in principle a single sir class instance can accommodate all of the status
/// pvs for an IS, and ditto for the device layer sirs. but it is cleaner
/// parcel out the SAD into pieces. This Gemini Interlock specialization of the SIR
/// should override the genExec to deal with the additional inputs defined
/// by the ctor vector args below:
class UFGISIR : public UFSIR {
public:
  // all ctors use base class create ctor/helpers
  inline UFGISIR(const string& recname= "instrum:gis:heartbeat") : UFSIR(recname) {}
  //inline UFGISIR(const string& recname, double val) : UFSIR(recname, val) {}
  inline UFGISIR(const string& recname, const string& sval) : UFSIR(recname, sval) {}
  inline UFGISIR(const string& recname, const string& sval,
		 const vector< UFPVAttr* >& inputs,
	         vector< UFPVAttr* >& outputs) : UFSIR(recname, sval, inputs, outputs) {}
  inline virtual ~UFGISIR() {}

  virtual int genExec(int timeout= 0);

  // gemini interlock sir only
  inline UFPVAttr* packet() { string p= _name+".packet"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp01() { string p= _name+".inp01line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp02() { string p= _name+".inp02line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp03() { string p= _name+".inp03line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp04() { string p= _name+".inp04line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp05() { string p= _name+".inp05line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp06() { string p= _name+".inp06line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp07() { string p= _name+".inp07line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp08() { string p= _name+".inp08line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp09() { string p= _name+".inp09line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp10() { string p= _name+".inp10line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp11() { string p= _name+".inp11line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp12() { string p= _name+".inp12line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp13() { string p= _name+".inp13line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp14() { string p= _name+".inp14line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp15() { string p= _name+".inp15line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp16() { string p= _name+".inp16line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp17() { string p= _name+".inp17line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp18() { string p= _name+".inp18line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp19() { string p= _name+".inp19line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* inp20() { string p= _name+".inp20line"; if( find(p) != end() ) return (*this)[p]; return 0; }

  inline UFPVAttr* out01() { string p= _name+".out01line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* out02() { string p= _name+".out02line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* out03() { string p= _name+".out03line"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* out04() { string p= _name+".out04line"; if( find(p) != end() ) return (*this)[p]; return 0; }


  static UFGISIR* create(const string& instrum);

protected:
};

#endif // __UFGISIR_h__
