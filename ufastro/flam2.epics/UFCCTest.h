#if !defined(__UFCCTest_h__)
#define __UFCCTest_h__ "$Name:  $ $Id: UFCCTest.h,v 0.7 2006/08/29 20:16:46 hon Exp $"
#define __UFCCTest_H__(arg) const char arg##CCTest_h__rcsId[] = __UFCCTest_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFCCTest : public UFCAD {
public:
  inline UFCCTest(const string& instrum= "instrum") : UFCAD(instrum+":cc:test") { _create(); }
  inline virtual ~UFCCTest() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* thrdFunc(void* p);
  static int newThrd(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // available to IS
  static int statAll(int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFCCTest_h__
