#if !defined(__UFCCABORT_CC__)
#define __UFCCABORT_CC__ "$Name:  $ $Id: UFCCTest.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCABORT_CC__;

#include "UFCCTest.h"
#include "UFCCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// ctors:

// static class funcs:
void* UFCCTest::thrdFunc(void* p) {
  // test really just needs the mech. names
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string option = arg->_option;
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:testC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  int err= 0;
  string errmsg;
  bool validsoc= false;
  if( !_sim && UFCCSetup::_motoragntsoc != 0 ) {
    // test it to be sure non-blocking connectit is functital/selectable (connectit has completed)
    bool connected = ( UFCCSetup::_motoragntsoc != 0 );
    if( connected ) validsoc = UFCCSetup::_motoragntsoc->validConnection();
    int cnt = 3;
    while( --cnt > 0 && connected && !validsoc ) { // try a few times
      validsoc = UFCCSetup::_motoragntsoc->validConnection();
      if( !validsoc ) {
        //clog<<"UFCCTest::testit> failed validatit? trycnt: "<<cnt<<endl;
        UFPosixRuntime::sleep(0.1);
      }
    }
  }                                                                                           
  if( validsoc ) {
    UFCCSetup::cmdStatus(option, mparms, rec, car);
  }
  else {
    clog<<"UFCCTest::testit> unable to transact request with portescap motor agent due to invalid connectit..."<<endl;
    err = 1; errmsg = "Error Testing";
  }

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }

  // free arg
  delete arg;

  if( _sim || err ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && !err) 
	c->setIdle();
      else if( c != 0 && c != car && err ) {
        c->setErr(errmsg);
      }
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose test func. 

/// arg: mechName, motparm*
int UFCCTest::newThrd(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optital car's setBusy
  // while the optital supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocatits...
  //pthread_t pmot;
  // be sure to include 'statusall' option to force processing of entire mparms container
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car, "StatusAll");
  pthread_t thrid = UFPosixRuntime::newThread(UFCCTest::thrdFunc, arg);

  return (int) thrid;
}

int UFCCTest::statAll(int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optital car's setBusy
  // while the optital supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }
  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // this is a rather clumsy way to examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  // test these:
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  string mechName, indexor;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    clog<<"UFCCTest::genExec> "<<mechName<<", indexor: "<<indexor<<endl;
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
    (*mechs)[mechName] = mp;
  }
  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocatits...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mechs, rec, car, "StatusAll");
  pthread_t thrid = UFPosixRuntime::newThread(UFCCTest::thrdFunc, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFCCTest::_create() {
  /// pvs should look something like:
  /// flam:cc:test.mech1-N == anything non null
  // support 'all' option:
  string pvname = _name + ".All";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
  attachInOutPV(this, pva);
  // individual mech. test:

  int mcnt = UFCCSetup::setDefaults(_name); 
  deque< string >* mnames = UFCCSetup::getMechNames();
  for( int i = 0; i < mcnt; ++i ) {
    pvname = _name + "."; pvname += (*mnames)[i];
    pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
    attachInOutPV(this, pva);
  }

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform test mechanisms with non-null step cnts or named-positit inputs
int UFCCTest::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFCCTest::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // examine all the inputs and determine
  // which ones have been set for processing... 
  // is 'all' indicated:
  string pvname = name() + ".All";
  UFPVAttr* pva = (*this)[pvname]; // only interested in mechName
  if( pva != 0 ) {
    int input = (int)pva->getNumeric();
    if( _verbose )
      clog<<"UFCCDatum::genExec> "<<pvname<<", input: "<<input<<endl;
    if( input >= 0 ) {
      pva->clearVal();
      return statAll(timeout, this, _car);
    }
  }  

  // check individual mechs (assume all mech inputs are ints)
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  string mechName, indexor;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    pvname = name() + "." + mechName;
    pva = (*this)[pvname]; // only interested in mechName
    if( pva == 0 )
      continue;
    int input = (int)pva->getNumeric();
    if( _verbose )
      clog<<"UFCCDatum::genExec> "<<pvname<<", input: "<<input<<endl;
    if( input < 0 )
      continue;

    clog<<"UFCCTest::genExec> "<<mechName<<", indexor: "<<indexor<<endl;
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
    (*mechs)[mechName] = mp;
    pva->clearVal(); // clear mechName input
  }

  int stat= 0, nm = (int) mechs->size();
  if( nm > 0 )
    stat = newThrd(mechs, timeout, this, _car);  // perform requested test

  if( stat < 0 ) { 
    clog<<"UFCCTest::genExec> failed to start test pthread..."<<endl;
    return stat;
  }

  return nm;
} // test genExec

int UFCCTest::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();
  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      clog<<"UFCCTest::validate> "<<pvname<<" == "<<input<<endl;
    }
  }

  return 0;
} // validate


#endif // __UFCCABORT_CC__
