#if !defined(__UFSIR_cc__)
#define __UFSIR_cc__ "$Name:  $ $Id: UFSIR.cc 14 2008-06-11 01:49:45Z hon $"

#include "UFSIR.h"
__UFSIR_H__(__UFSIR_cc);

#include "UFEnvSIR.h"
#include "UFGISIR.h"
#include "UFCCInit.h"
#include "UFPVAttr.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"

#include "UFSADFITS.h"
#include "UFCCSetup.h" // for indexor step cnt stats use this static string sadChan(const string& mechname);
#include "ufflam2mech.h"

std::map< string, UFSIR* > UFSIR::_theSAD;

// datum, setups, observe, pause, continue, endobserve, stop, abort, can all make use 
// of this public statics:
UFSIR* UFSIR::_prep= 0;
UFSIR* UFSIR::_acq= 0;
UFSIR* UFSIR::_rdout= 0;

// and associated helpers:
int UFSIR::setPrep(bool prep) {
  UFPVAttr *pin = _prep->pinp();
  // if prep true, force other flags to false...
  if( prep )
    return _prep->setFlags(prep);

  pin->clearVal(); 
  return _prep->genExec();
}

int UFSIR::setAcq(bool acq) {
  UFPVAttr *pin = _acq->pinp();
  // if acq true, force prep flag to false and rdout to true...
  if( acq )
    return _acq->setFlags(false, acq, true);

  pin->clearVal();
  return _acq->genExec();
}

int UFSIR::setRdout(bool rdout) {
  UFPVAttr *pin = _rdout->pinp();
  // if rdout true, force prep flag to false and acq to true...
  if( rdout )
    return _rdout->setFlags(false, true, rdout);

  pin->clearVal();
  return _rdout->genExec();
}

int UFSIR::setFlags(bool prep, bool acq, bool rdout) {
  UFPVAttr *pin = _prep->pinp();
  if( prep ) pin->setVal(1); else pin->clearVal();

  pin = _acq->pinp();
  if( acq ) pin->setVal(1); else pin->clearVal();

  pin = _rdout->pinp();
  if( rdout ) pin->setVal(1); else pin->clearVal();

  _prep->genExec(); _acq->genExec(); _rdout->genExec();

  return 0;
}

// the processing & 'genSub' record processing entry point:
int UFSIR::genExec(int timeout) {
  UFPVAttr *pin = pinp(), *pout = pval();
  if( pin == 0 || pout == 0 ) {
    if( _verbose )
      clog<<"UFSIR::genExec> pin: "<<(size_t)pin<<", pout: "<<(size_t)pout<<endl;
    return -1;
  }

  pout->copyVal(pin);
 
  if( _verbose && _name.find("heartbeat") == string::npos )
    clog<<"UFSIR::genExec> pvin: "<<pin->name()<<", pvout: "<<pout->name()<<endl;
  
  return timeout;
}

// protected:
// ctor helper
void UFSIR::_create(bool reg) {
  // used by default ctor and also helper to all other _creates
  UFPVAttr* pva= 0;
  _type = UFGem::_SIR; _mark = true; // sirs are always marked for processing on any new input
  string recname = name();
  string pvname = recname + ".IMSS";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null");
  attachInputPV(pva);

  // outputs (none other than UFGem base class)
  if( reg ) { // conditional registration only true for default ctor
    registerRec();
  }
}

// ctors helper supports non-default (non int) input/output value for rec.
void UFSIR::_create(int val) {
  string recname = name();
  // check if this is a heartbeat
  // system heartbeat syntax: "instrum:heartbeat", not "instrum:sad:heartbeat"
  if( recname.find("heartbeat") != string::npos && recname.find(":sad:") != string::npos ) {
    size_t pos = recname.rfind(":");
    if( pos != string::npos ) {
      recname = recname.substr(0, pos);
      recname += ":heartbeat";
    }
  }
  UFPVAttr* pva= 0;
  string pvtype = recname + ".FTVL";
  pva = new (nothrow) UFPVAttr(pvtype, "LONG");
  attachOutputPV(pva); // indicate the type of sad is int
  string pvname = recname + ".INP";
  pva = new (nothrow) UFPVAttr(pvname, val);
  attachInputPV(pva); 
  pvname = recname + ".VAL";
  //attachOutputPV(setPVAttrOf(pvname, val));
  pva = new (nothrow) UFPVAttr(pvname, val); // int outval
  attachOutputPV(pva);
  _create(); // complete rec. i/o and register
}

// ctors helper supports non-default (non int) input/output value for rec.
void UFSIR::_create(double val) {
  string recname = name();
  UFPVAttr* pva= 0;
  string pvtype = recname + ".FTVL";
  pva = new (nothrow) UFPVAttr(pvtype, "DOUBLE");
  attachOutputPV(pva); // indicate the type of sad is double
  string pvname = recname + ".INP";
  pva = new (nothrow) UFPVAttr(pvname, val);
  attachInputPV(pva); 
  pvname = recname + ".VAL";
  //attachOutputPV(setPVAttrOf(pvname, val));// reset from default int to double
  pva = new (nothrow) UFPVAttr(pvname, val); // double outval
  attachOutputPV(pva);
  _create(); // complete rec. i/o and register
}

// ctors helper supports non-default (non int) input/output value for rec.
void UFSIR::_create(const string& sval) {
  string recname = name();
  UFPVAttr* pva= 0;
  string pvtype = recname + ".FTVL";
  pva = new (nothrow) UFPVAttr(pvtype, "STRING");
  attachOutputPV(pva); // inidcate the type of sad is string
  string pvname = recname + ".INP";
  pva = new (nothrow) UFPVAttr(pvname, sval);
  attachInputPV(pva); 
  pvname = recname + ".VAL";
  //attachOutputPV(setPVAttrOf(pvname, sval)); // reset from default int to string
  pva = new (nothrow) UFPVAttr(pvname, sval); // string outval
  attachOutputPV(pva);
  _create(); // complete rec. i/o and register
}

// ctor helper supports optional input/outputs for rec.
void UFSIR::_create(const string& sval, const vector< UFPVAttr* >& inputs, vector< UFPVAttr* >& outputs) {
  size_t outs = outputs.size(); // if output list is empty, create default '.VALoutputs' fields from inputs 
 
  for( size_t i =  0; i < inputs.size(); ++i ) {
    UFPVAttr* pvi = inputs[i];
    if( pvi != 0 ) {
      if( outs == 0 )
	attachInOutPV(this, pvi);
      else
        attachInputPV(pvi);
    }
  }

  for( size_t i =  0; i < outs; ++i )
    attachOutputPV(outputs[i]);

  _create(sval); // complete rec. i/o and register
}

// ctor helper supports optional input/outputs for rec.
void UFSIR::_create(int val, const vector< UFPVAttr* >& inputs, vector< UFPVAttr* >& outputs) {
  size_t outs = outputs.size(); // if output list is empty, create default '.VALoutputs' fields from inputs 
 
  for( size_t i =  0; i < inputs.size(); ++i ) {
    UFPVAttr* pvi = inputs[i];
    if( pvi != 0 ) {
      if( outs == 0 )
	attachInOutPV(this, pvi);
      else
        attachInputPV(pvi);
    }
  }

  for( size_t i =  0; i < outs; ++i )
    attachOutputPV(outputs[i]);

  _create(val); // complete rec. i/o and register
}

// static public:
int UFSIR::createAll(const string& instrum) {
  // test db:
  // heartbeats are special and don't need ':sad:'
  string recname;
  string sad = instrum + ":sad:";
  std::vector< string > vsad;

  // minimal tcs:
  recname =  instrum + ":tcs:airMassNow"; _theSAD[recname] = new UFSIR(recname, 0.0);
  recname = "tcs:sad:airMassNow"; _theSAD[recname] = new UFSIR(recname, 0.0);

  if( UFGem::_fullDB ) { 
    recname = instrum  + ":heartbeat"; _theSAD[recname] = new UFSIR(recname, 0);
  }
  else {
    recname = instrum  + ":eng:heartbeat"; _theSAD[recname] = new UFSIR(recname, 0);
  }

  // EC heartbeat rec need additional inputs for vac. and temp. agents
  // so let's provide a specialized environment SIR subclass
  recname = instrum + ":ec:heartbeat"; 
  _theSAD[recname] = UFEnvSIR::create(instrum); // use specialized static creation func.
  // and a few more standard sirs for compatibility with the vme db:
  recname = instrum + ":ec:Barheartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  recname = instrum + ":ec:L218heartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  recname = instrum + ":ec:L332heartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  recname = instrum + ":ec:LVDTheartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  recname = instrum + ":ec:PLCheartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  recname = instrum + ":ec:Vacheartbeat"; _theSAD[recname] = new UFSIR(recname, 0); 
  
  // GIS --provide a specialized gemini interlock system (gis) SIR subclass
  recname = instrum + ":gis:heartbeat";
  _theSAD[recname] = UFGISIR::create(instrum); // use specialized static creation func.
 
  // CC
  recname = instrum + ":cc:heartbeat"; _theSAD[recname] = new UFSIR(recname, 0);
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();
  UFFLAM2MechPos* mech= 0;
  for(int m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    string indexor = infoFLAM2MechOf(m, &mech); // sets mech ptr
    string mname = mech->name; string sadrecname = UFCCSetup::sadChan(mname);
    _theSAD[sadrecname] = new UFSIR(sadrecname, 0.0);
    if( _verbose )
      clog<<"UFSIR::createAll> indexor: "<<indexor<<", mechname: "<<mname<<", sad: "<<sadrecname<<endl; 
  }

  // DC
  recname = instrum + ":dc:heartbeat"; _theSAD[recname] = new UFSIR(recname, 0);
  // prep, acq & rdout booleans are special sads that also need not include :sad:
  // prep == true during datums and any of the setups, up until observe, and false
  // during observations
  recname = instrum + ":prep"; _prep = _theSAD[recname] = new UFSIR(recname, 0);
  // acq == true during observation until endobserve or abort or stop:
  recname = instrum + ":acq"; _acq = _theSAD[recname] = new UFSIR(recname, 0);
  // rdout is true during observation, false during pause, true on continue;
  // and false al times before and after observation ends/stops/aborts:
  recname = instrum + ":rdout"; _rdout = _theSAD[recname] = new UFSIR(recname, 0);
  // and an MCEBooted state:
  recname = instrum + ":sad:MCEBooted"; _theSAD[recname] = new UFSIR(recname, INT_MIN);
  // and an MCEReply from raw command(s):
  recname = instrum + ":sad:MCEReply"; _theSAD[recname] = new UFSIR(recname, "null");
  
  // all FITS keyword sads:
  int ns = UFSADFITS::allSAD(vsad, instrum);
  if( ns <= 0 ) vsad.push_back(instrum + ":sad:DATUMCNT");
  recname = vsad[0]; // first sad is datumcnt!
  _theSAD[recname] = new UFSIR(recname, "-1");
  if( UFCCInit::_datumCnt == 0 ) // allocate string* here, if needed
    UFCCInit::_datumCnt = new string(recname);
  // assume SAD records that contain FITS header values conform to FITS keyword specs.
  for( int i = 1; i < (int)vsad.size(); ++i ) {
    recname = vsad[i];
    _theSAD[recname] = new UFSIR(recname); // default ctor is now type string with init. val. "null"
  }

  /*
  // LVDT status:
  recname = instrum + ":sad:LVDTVolts"; _theSAD[recname] = new UFSIR(recname, 0.0);
  recname = instrum + ":sad:LVDTDisplacement"; _theSAD[recname] = new UFSIR(recname, 0.0);

  // need a datum counter, whose name may or may not already be set in UFCCInit::_datumCnt string ptr:
  recname = instrum + ":sad:DatumCnt";
  // init should set this to 0 and datum should increment it...
  _theSAD[recname] = new UFSIR(recname, -1);

  // interlock (plc) heartbeat rec has 24 input fields (the 20 input lines and 4 output lines),

  // get mos bard code names from common header:
  UFFLAM2MechPos* mech;
  char* indexor = infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor != 0 && mech != 0 ) {
    int pcnt = mech->posCnt;
    for( int i = 0; i < pcnt; ++i ) {
      string mos = mech->positions[i];
      recname = sad + mos;
      _theSAD[recname] = new UFSIR(recname, "null");
      recname += "MDF";
      _theSAD[recname] = new UFSIR(recname, "null");
   }
  }
  else {
    recname = sad + "MOSPos1BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos2BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos3BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos4BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos5BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos6BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos7BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos8BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos9BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos10BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "circMOSPos1BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "circMOSPos2BarCode"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos1MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos2MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos3MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos4MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos5MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos6MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos7MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos8MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos9MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "MOSPos10MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "circMOSPos1MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
    recname = sad + "circMOSPos2MDFString"; _theSAD[recname] = new UFSIR(recname, "null");
  }

  // and wcs stuff for miri (to allow simultaneous testing of multiple instruments on same host):
  string miri = "miri";
  recname = miri + ":wcs:mjdobs"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:radecsys"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:equinox"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:ctype1"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:crpix1"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:crval1"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:ctype2"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:crpix2"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:crval2"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:cd11"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:cd12"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:cd21"; _theSAD[recname] = new UFSIR(recname);
  recname = miri + ":wcs:cd22"; _theSAD[recname] = new UFSIR(recname);
  */

  // (flam) wcs:
  string wcs = instrum + ":wcs:";
  // old trecs names:
  recname = wcs + "mjdobs"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "radecsys"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "equinox"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "ctype1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "crpix1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "crval1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "ctype2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "crpix2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "crval2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "cd11"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "cd12"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "cd21"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "cd22"; _theSAD[recname] = new UFSIR(recname);
  // new names
  recname = wcs + "RA_used"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "DEC_used"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Pixscale"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "InstrumentPA"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "DIM"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Ctype1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Ctype2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Crval1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Crval2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Crpix1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Crpix2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "CD1_1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "CD1_2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "CD2_1"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "CD2_2"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "RA"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Dec"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "Tai"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "11"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "12"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "13"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "21"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "22"; _theSAD[recname] = new UFSIR(recname);
  recname = wcs + "23"; _theSAD[recname] = new UFSIR(recname);

  // the following was inserted via:
  /*
    grep -i sad FLAMINGOS-2_EPICS_databaselist_2003Aug15.txt | awk '{print $1}' | 
    cat flam2sad.db1 | grep -v '#' | cut -d':' -f3 | sed 's/^/  recname \= sad \+ "/g' |
    sed 's/$/"\;/g' | sed 's/$/"\;_theSAD[recname] = new UFSIR(recname)\;/g' 
  */
  /*
  recname = sad + "bancomTime"; _theSAD[recname] = new UFSIR(recname, "yyyy:ddd:hh:mm:ss.000");
  recname = sad + "beamMode"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "cameraMode"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "dataMode"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "obsID"; _theSAD[recname] = new UFSIR(recname, "Richard Elston");
  recname = sad + "obsMode"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "obsType"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "expTime"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "numCoadds"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "nodDelay"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "netExpTime"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "numReads"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "offsetDelay"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerPosName"; _theSAD[recname] = new UFSIR(recname, "Decker Position");
  recname = sad + "slitWidth"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSPosName"; _theSAD[recname] = new UFSIR(recname, "MOS Position");
  recname = sad + "MOSPlateBarCode"; _theSAD[recname] = new UFSIR(recname, "null");
  recname = sad + "MOSPlateMDFString"; _theSAD[recname] = new UFSIR(recname, "MOS MDF");
  recname = sad + "MOSPlateType"; _theSAD[recname] = new UFSIR(recname, "null");
  recname = sad + "filterName"; _theSAD[recname] = new UFSIR(recname, "Filter Name");
  recname = sad + "lyotPosName"; _theSAD[recname] = new UFSIR(recname, "Lyot Position");
  recname = sad + "grismName"; _theSAD[recname] = new UFSIR(recname, "Grism Name");
  recname = sad + "detectorPositionName"; _theSAD[recname] = new UFSIR(recname, "Detector Position");
  recname = sad + "currentBeam"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "observationStatus"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "datummed"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "health"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "historyLog"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "initialized"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "issPort"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "name"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "parked"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "state"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "windowRH"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preResets"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "postResets"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "pixelBaseClock"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "edtTimeout"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lutTimeout"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue1"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue2"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue3"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue4"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue5"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue6"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue7"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue8"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue9"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue10"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue11"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue12"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue13"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue14"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue15"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue16"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue17"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue18"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue19"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue20"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue21"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue22"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue23"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue24"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue25"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue26"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue27"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue28"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue29"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue30"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue31"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "preampDacValue32"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "acq"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "bunit"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "datalabel"; _theSAD[recname] = new UFSIR(recname, "null");
  recname = sad + "prep"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "rdout"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "utend"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "utnow"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "utstart"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detErrorBand"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "vBias"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detID"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detType"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "dcHealth"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "dcName"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "dcState"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerWhlPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSWhlPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filterhWhl1Pos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filterWhl2Pos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotWhlPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismWhlPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detFocusPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerWhlRawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSWhlRawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filterWhl1RawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filterWhl2RawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotWhlRawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismWhlRawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detfocusRawPos"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerOrigin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSOrigin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter1Origin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter2Origin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotOrigin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismOrigin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detectorOrigin"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerTotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSTotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter1TotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter2TotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotTotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismTotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detectorTotalSteps"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerInitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSInitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter1InitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter2InitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotInitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismInitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detectorInitVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerTerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSTerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter1TerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter2TerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotTerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismTerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detectorTerminalVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "deckerHomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSHomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter1HomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "filter2HomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "lyotHomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "grismHomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "detectorHomeVel"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ccHealth"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ccName"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ccState"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "wfsBeam"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSHeaterPower"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "MOSHeaterSetpoint"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "cameraHeaterPower"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "cameraHeaterSetpoint"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSColdhead"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSColdheadOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSWSurface"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSWSurfaceOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSSpare"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSMOSSpareOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamColdhead"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamColdheadOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamWSurface1"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamWSurface1OK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamWSurface2"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamWSurface2OK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSDetFanout"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSDetFanoutOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamSpare"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "tSCamSpareOK"; _theSAD[recname] = new UFSIR(recname);
  //recname = sad + "MOSVacuum"; _theSAD[recname] = new UFSIR(recname, "unknown");
  recname = sad + "MOSVacuumOK"; _theSAD[recname] = new UFSIR(recname);
  //recname = sad + "CamVacuum"; _theSAD[recname] = new UFSIR(recname, "unknown");
  recname = sad + "CamVacuumOK"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ecHealth"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ecName"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "ecState"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "barCodeReaderTriggerScan"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "bcrHealth"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "bcrName"; _theSAD[recname] = new UFSIR(recname);
  recname = sad + "bcrState"; _theSAD[recname] = new UFSIR(recname);
  */
  // just test with this for now...
  /*
  if( _theSAD.size() > 0 ) {
    clog<<"UFSIR::createAll> test with: "<<_theSAD.size()<<endl;
    return (int) _theSAD.size();
  }
  */

  return (int) _theSAD.size();
}

#endif // __UFSIR_cc__
