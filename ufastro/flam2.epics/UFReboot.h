#if !defined(__UFReboot_h__)
#define __UFReboot_h__ "$Name:  $ $Id: UFReboot.h,v 0.9 2005/08/05 16:33:00 hon Exp $"
#define __UFReboot_H__(arg) const char arg##Reboot_h__rcsId[] = __UFReboot_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h" // provides exising connection to executive, if available
#include "UFInit.h" // provides exising connections to device agents, if available

// forward declaration of UFCAwrapper class
class UFCAwrap;

// either abort an instrument setup or an obssetup or the ongoing observation
class UFReboot : public UFCAD {
public:
  struct Parm { string _host, _subsys, _mode; int _inetport, _execport;
    inline Parm(const string& f2host, const string& subsys= "full", const string& mode= "reboot",
		int inetport= 3720, int execport= 52000) :
      _host(f2host), _subsys(subsys), _mode(mode),_inetport(inetport), _execport(execport) {}
    inline ~Parm() {}
  };

  // thread arg; note this relies on the Parm copy ctor:
  struct ThrdArg { Parm _parm; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(const Parm& p, UFGem* r= 0, UFCAR* c= 0) : _parm(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFReboot(const string& instrum= "instrum") : UFCAD(instrum+":reboot") { _create(); }
  inline virtual ~UFReboot() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* shutdown(void* p);
  static void* reboot(void* p);
  static int haltstart(UFReboot::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // IS init should connect to executive
  static UFClientSocket *_inetd, *_execd;

protected:
  // ctor helper
  void _create();

  static string _oiwfschan;
  static UFCAwrap* _ufca;
};

#endif // __UFReboot_h__
