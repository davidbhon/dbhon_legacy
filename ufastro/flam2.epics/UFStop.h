#if !defined(__UFStop_h__)
#define __UFStop_h__ "$Name:  $ $Id: UFStop.h,v 0.4 2005/05/10 20:29:11 hon Exp $"
#define __UFStop_H__(arg) const char arg##Stop_h__rcsId[] = __UFStop_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFStop : public UFCAD {
public:
  struct Parm { UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(UFObserve::Parm* op= 0, UFObsSetup::Parm* osp= 0, UFInstrumSetup::Parm* isp= 0): _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFStop(const string& instrum= "instrum") : UFCAD(instrum+":stop") { _create(); }
  inline virtual ~UFStop() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* stopAll(void* p);
  static int stop(Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFStop_h__
