#if !defined(__UFSIR_h__)
#define __UFSIR_h__ "$Name:  $ $Id: UFSIR.h,v 0.15 2005/09/30 22:53:12 flam Exp $"
#define __UFSIR_H__(arg) const char arg##SIR_h__rcsId[] = __UFSIR_h__;

#include "UFGem.h"
#include "vector"

/// in principle a single sir class instance can accommodate all of the status
/// pvs for an IS, and ditto for the device layer sirs. but it is cleaner
/// parcel out status into small pieces...
class UFSIR : public UFGem {
public:
  inline UFSIR(const string& recname, const string& sval= "null") : UFGem(recname) { _create(sval); }
  inline UFSIR(const string& recname, int val) : UFGem(recname) { _create(val); }
  inline UFSIR(const string& recname, double val) : UFGem(recname) { _create(val); }
  inline UFSIR(const string& recname, const string& sval,
	       const vector< UFPVAttr* >& inputs,
	       vector< UFPVAttr* >& outputs) : UFGem(recname) { _create(sval, inputs, outputs); }
  inline UFSIR(const string& recname, int val,
	       const vector< UFPVAttr* >& inputs,
	       vector< UFPVAttr* >& outputs) : UFGem(recname) { _create(val, inputs, outputs); }
  inline virtual ~UFSIR() {}

  // always marked:
  inline virtual void mark() {}
  inline virtual void unmark() {}
  // no need for idle->busy-idle/err transitions (don't put into timeout list)?:
  inline virtual void setBusy(size_t timeout= 0) { _timeout = timeout; }
  //inline virtual int start(int timeout= 0) { return UFGem::start(timeout); }


  // effectively obviates need for genSub record
  // the processing & 'genSub' record processing entry point:
  virtual int genExec(int timeout= 0);

  // default sad db list
  static int createAll(const string& instrum= "instrum");

  // allow public access to sad
  static std::map< string, UFSIR* > _theSAD;

  // and these specific observation status (flag) records:
  // prep, acq & rdout booleans are special sads that also do not need include ':sad:'
  // prep == true during datums and any of the setups, up until observe, and false
  // during observations
  // acq == true during observation until endobserve or abort or stop:
  // rdout is true during observation, false during pause, true on continue;
  // and false al times before and after observation ends/stops/aborts:
  static UFSIR *_prep, *_acq, *_rdout;
  static int setPrep(bool prep= false);
  static int setAcq(bool acq= false);
  static int setRdout(bool rdout= false);
  static int setFlags(bool prep= false, bool acq= false, bool rdout= false);

protected:
  // ctor helpers
  void _create(bool reg= true);
  void _create(int val);
  void _create(double val);
  void _create(const string& sval);
  void _create(const string& sval, const vector< UFPVAttr* >& inputs, vector< UFPVAttr* >& outputs);
  void _create(int val, const vector< UFPVAttr* >& inputs, vector< UFPVAttr* >& outputs);
};

#endif // __UFSIR_h__
