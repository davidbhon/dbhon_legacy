#if !defined(__UFInstrumSetup_h__)
#define __UFInstrumSetup_h__ "$Name:  $ $Id: UFInstrumSetup.h,v 0.15 2005/10/12 18:51:45 hon Exp $"
#define __UFInstrumSetup_H__(arg) const char arg##InstrumSetup_h__rcsId[] = __UFInstrumSetup_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

//class UFClientSocket;

class UFInstrumSetup : public UFCAD {
public:
  struct Parm {
    map< string, UFCCSetup::MotParm* >* _mparms;
    UFECSetup::EnvParm* _eparms;
    inline Parm(map< string, UFCCSetup::MotParm* >* mp= 0,
	        UFECSetup::EnvParm* ep= 0) : _mparms(mp),  _eparms(ep) {}
    inline ~Parm() {} // assume CCSetup and ECSetup threads delete these...
    /*
    inline ~Parm() {
      if( _mparms != 0 ) {
        std::map< string, UFCCSetup::MotParm* >::const_iterator it = _mparms->begin();
	while( it != _mparms->end() ) { delete it->second; ++it; }
      }
      delete _mparms; delete _eparms;
    }
    */
  };
  // thread arg:
  struct ISThrdArg { UFInstrumSetup::Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ISThrdArg(UFInstrumSetup::Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ISThrdArg() { delete _parms; }
  };

  // all allowed named configurations: 
  static std::map< string, std::deque< string >* > _allowed;
  static bool allowable(const string& pvname, const string& val);
  static bool allowable(const string& pvname, double val);

  static UFInstrumSetup::Parm* allocParm(const map< string, string >& cconf,
					 const map< string, double >& ecsetp);

  inline UFInstrumSetup(const string& instrum, const string& host= "",
			int ccport= 52024, int ecport= 52003) : UFCAD(instrum+":instrumSetup") 
    { _create(host, ccport, ecport); }
  inline virtual ~UFInstrumSetup() {}

  // override this to NOT pre-process/copy inputs to outputs, differ to genExec.
  virtual void setBusy(size_t timeout, UFGem* proxy); // may a cad ever have another proxy? 
  inline virtual void setBusy(size_t timeout= _UFCADTO) { return setBusy(timeout, 0); }

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  // thread entry func. (used just for simulation?)
  static void* simAll(void* p= 0);
  /// general instrument seq. setup  func. (non datum)
  /// IS. starts a new daemon thread for every propper sequence,
  /// making use of static thread functions provided by CC, DC, and EC setups:
  static int config(UFInstrumSetup::Parm* p, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  // to instrument cc and ec agents
  inline static void setHost(const string& host) { _host = host; }
  inline static void setPorst(int cc= 52024, int ec= 52003) { _ccport = cc; _ecport= ec; }
  inline static string agentHostAndPorts(int& ccport, int& ecport) 
  { ccport = _ccport; ecport = _ecport; return _host; }
 
  // ObsSetup also sets its own version of this, which may override this,
  // this is 'cleared/nulled' on processing the Observe CAD
  static string _BiasMode; // this value is set by selection of WheelBiasMode

  // use this to auto-select the Lyot and DetFocus positions:
  // this is also 'cleared/nulled' on processing the Observe CAD
  static string _BeamMode; // this value is set by selection of BeamMode

protected:
  static UFClientSocket* _connection; // to instrument executive agent
  static string _host;
  static int _ccport, _ecport;

  // ctor helper
  void _create(const string& host, int ccport= 52024, int ecport= 52003);
};

#endif // __UFInstrumSetup_h__
