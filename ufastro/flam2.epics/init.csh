#!/bin/tcsh -f
# gemini cad/car enumerations:
set Mark = 0
set Clear = 1
set Preset = 2
set Start = 3
set Idle = 0
set Busy = 2
set Error = 3
#
#set db = 'flam:eng'
set db = flam
if( "$1" != "" ) set db = "$1"
#set apply = ${db}:eng:apply
set apply = ${db}:apply
ufcaput ${db}:cc:init.dir=$Preset
ufcaput ${apply}.dir=$Start
