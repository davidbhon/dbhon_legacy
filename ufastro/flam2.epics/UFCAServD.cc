#if !defined(__UFCASERVD_CC__)
#define __UFCASERVD_CC__ "$Name:  $ $Id: UFCAServD.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCASERVD_CC__;

#include "UFCAServD.h"
#include "UFGem.h"

// static funcs:
int UFCAServD::main(int argc, char** argv, char** env, char* name) {
  // Create server object.
  UFCAServD* cas = new UFCAServD(argc, argv, env, name);
  UFPVAttr::_cas = UFCAServ::_instance = cas;

  bool incIS = true; // no instrument sequencer records, just engineering (cc, dc, ec) and sads
  string applyrecname = "apply";
  bool dblist = false;
  string dbfile, appname = argv[0];
  if( appname.rfind("f2") != string::npos ) UFCAServ::_instrum = "f2";
  if( appname.rfind("fu") != string::npos ) UFCAServ::_instrum = "fu";
  if( appname.rfind("foo") != string::npos ) UFCAServ::_instrum = "foo";
  
  string arg = cas->findArg("-instrum"); // epics db name
  if( arg != "false" && arg != "true" ) {
     UFCAServ::_instrum = arg;
  }

  arg = cas->findArg("-eng"); 
  if( arg != "false" ) {
  // not full db: use enginering db but exclude instrument sequencer records
    UFGem::_fullDB = false;
    incIS = false;
    applyrecname = "eng:apply";
    if( arg != "true" )
      UFCAServ::_instrum = arg;
  }

  if( UFCAServ::_instrum.empty() && UFCAServD::_instrum.empty() ) {
    UFCAServ::_instrum = "flam";
  }
  clog<<"UFCAServD::main> _instrum: "<<UFCAServ::_instrum<<endl;

  arg = cas->findArg("-dbfile"); // epics db name
  if( arg != "false" && arg != "true" ) {
    dbfile = arg;
    struct stat s;
    if( stat(dbfile.c_str(), &s) != 0 ) // assume instrument name is also db file name:
      dbfile = "";
  }

  arg = cas->findArg("-l"); // list
  if( arg != "false" ) {
    dblist = true; UFGem::_verbose = true;
  }
  arg = cas->findArg("-xml"); // list CADs in xml format
  if( arg != "false" ) {
    dblist = true; UFGem::_xml = true;
  }
  arg = cas->findArg("-lv"); 
  if( arg != "false" ) {
    dblist = true;  UFGem::_verbose = true;
    UFCAServ::_verbose = true;
  }
  arg = cas->findArg("-lvv");
  if( arg != "false" ) {
    dblist = true;
    UFCAServ::_verbose = true; UFGem::_verbose = true;
    UFPV::_vverbose = UFGem::_vverbose = true; //cas->setDebugLevel(0u);
  }

  arg = cas->findArg("-v"); 
  if( arg != "false" ) {
    UFCAServ::_verbose = UFPV::_verbose = UFGem::_verbose = true;
  }
  arg = cas->findArg("-vv");
  if( arg != "false" ) {
    UFCAServ::_verbose = UFPV::_verbose = UFGem::_verbose = UFPV::_vverbose = UFGem::_vverbose = true;
    //cas->setDebugLevel(0u);
  }

  arg = cas->findArg("-vsend"); // print send transactions
  if( arg != "false" ) {
    UFProtocol::_sendverbose = true;
  }
  arg = cas->findArg("-vrecv"); // print recv transactions
  if( arg != "false" ) {
    UFProtocol::_recvverbose = true;
  }

  // by default, only create enginerring DB, unless cmdline opt -is or -full indicate otherwise:
  if( incIS )
    UFGem::_fullDB = true;

  int nrec = 0;
  if( !dbfile.empty() ) {
    //UFCAServD::createPVsfrom(file, UFPVAttr::_thePVList);
    nrec = UFGem::createDBfrom(dbfile);
  }
  else { // internal test DB -- each ctor registers rec & pvs
    nrec = UFGem::createDB(UFCAServD::_instrum, !dblist, applyrecname);
  }

  if( dblist || nrec == 0 ) // just list the db rec & optionally fields (-v or -vv) and exit
    return 0;

  // start sighandler thread:
  cas->sigWaitThread(UFGem::sigHandler);

  // start heart thread:
  UFGem::startHeartThread(UFCAServD::_instrum);

  // forever until shutdown:
  double delay = 1000;
  while( ! UFGem::_shutdown ) {
    try {
      // fileDescriptorManager is a predeclared object found in fdMgr.h
      fileDescriptorManager.process(delay);
    }
    catch(std::exception& e) {
      clog<<"UFCAServD> stdlib exception occured: "<<e.what()<<endl;
    }
    catch(...) {
      clog<<"UFCAServD> unknown exception occured..."<<endl;
    }
  } // forever while

  UFGem::shutdown();
  return 0;
}

#endif // __UFCASERVD_CC__
