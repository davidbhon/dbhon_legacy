#if !defined(__UFINIT___)
#define __UFINIT___ "$Name:  $ $Id: UFInit.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFINIT___;

#include "UFInit.h"
#include "UFReboot.h"
#include "UFCCInit.h"
#include "UFDCInit.h"
#include "UFECInit.h"
#include "UFPVAttr.h"
#include "UFRuntime.h"

pthread_t UFInit::_CCthrdId= 0;
pthread_t UFInit::_DCthrdId= 0;
pthread_t UFInit::_ECthrdId= 0;
std::map< string, string>* UFInit::_hostmap= 0; // default hostmap
std::map< string, int>* UFInit::_portmap= 0; // default portmap

// ctors:

// static class funcs:
void* UFInit::connectAll(void* p) {
  pthread_t self = pthread_self();
  // not connecting in separate trehads just yet...
  _CCthrdId= 0;
  _DCthrdId= 0;
  _ECthrdId= 0;
  UFInit::ThrdArg* arg = (UFInit::ThrdArg*) p;
  string option = arg->_option;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  int timeout = arg->_timeout;
  string name = "anon";
  if( rec )
    name = rec->name();

  int stat= 0, port= 0;
  string errmsg= "FailedConnect: ", host= agentHostAndPort(port);
  clog<<"UFInit::connecting> "<<name<<", to agents on: "<<host<<endl;
  bool blockcon = true; // block until connection completes?
  // simple simulation...
  if( !_sim ) {
    if( UFReboot::_execd != 0 ) {
      string agnthost = UFRuntime::hostname();
      int port= 52000;
      clog<<"UFCCInit::connectAll> connect to executive agent host: "<<agnthost
	  <<", port: "<<port<<endl;
      UFReboot::_execd->connect(agnthost, port, blockcon);
    }
    std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
    //if( _verbose )
      clog<<"UFInit::connectAll> start CC connection thread..."<<endl;
      stat = UFCCInit::connect(mechs, timeout, rec, car, option); 
    if( stat < 0 ) {
      clog<<"UFInit::connectAll> failed to start CC connection pthread..."<<endl;
      errmsg += " CC";
      //delete mechs;
    }
    int ls332= 52003, lvdt= 52006;
    UFECSetup::setPorts(ls332, lvdt);
    UFECSetup::EnvParm* ep = new UFECSetup::EnvParm;
    //if( _verbose )
      clog<<"UFInit::connectAll> start EC connection thread..."<<endl;
    stat = UFECInit::connect(ep, timeout, rec, car);
    if( stat < 0 ) {
      clog<<"UFCCInit::connectAll> failed to start EC connection pthread..."<<endl;
      errmsg += " EC";
      //delete ep;
    }
    UFDCSetup::DetParm* dp = new UFDCSetup::DetParm;
    //if( _verbose )
      clog<<"UFInit::connectAll> start DC connection thread..."<<endl;
    stat = UFDCInit::connect(dp, timeout, rec, car);
    if( stat < 0 ) {
      clog<<"UFCCInit::connectAll> failed to start DC connection pthread..."<<endl;
      errmsg += " DC";
      //delete dp;
    }
  }
  // free arg
  delete arg;
  // set car idle after 1/2 sec:
  if( _sim ) {
    UFPosixRuntime::sleep(0.5);
  }
  else {
    clog<<"UFCCInit::connectAll> self: "<<self<<" waiting for all connect threads to complete..."<<endl;
    // it would be safer to mutex protect this (later):
    while( (_CCthrdId > 0 || _CCthrdId == self) && 
	   (_DCthrdId > 0 || _DCthrdId == self) && 
	   (_ECthrdId > 0 || _DCthrdId == self) ) {
      clog<<"CC: "<< _CCthrdId<<", DC: "<<_DCthrdId<<", EC: "<<_ECthrdId<<" ... "<<ends;
      UFRuntime::mlsleep(0.5);
    }
  }
  if( car ) {
    if( stat < 0 )
      car->setErr(errmsg);
    else
      car->setIdle();
  }
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if(c != 0 && c != car ) {
      if( stat < 0 )
        car->setErr(errmsg);
      else
        car->setIdle();
    }
  }
  return p;
}

/// general purpose abort func. 

/// arg: mechName, motparm*
int UFInit::connect(UFInit::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFInit::ThrdArg* arg = new UFInit::ThrdArg(timeout, parms, rec, car);
  //pthread_t thrid = UFPosixRuntime::newThread(UFInit::connectAll, arg);
  UFInit::connectAll(arg);
  //return (int) thrid;
  return 1;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFInit::_create(const string& host, int port) {
  UFPVAttr* pva= 0;
  // executive:
  string pvname = _name + ".host"; // executve
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);
  /*
  pvname = _name + ".Origin";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52001);
  attachInOutPV(this, pva);

  pvname = _name + ".Status";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52001);
  attachInOutPV(this, pva);
  */

  pvname = _name + ".port";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52001);
  attachInOutPV(this, pva);

  // lakeshore 218
  pvname = _name + ".hostls218";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);

  pvname = _name + ".portls218";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52002);
  attachInOutPV(this, pva);

  // lakeshore 332
  pvname = _name + ".hostls332";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);

  pvname = _name + ".portls332";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52003);
  attachInOutPV(this, pva);

  // pfeiffer vac
  pvname = _name + ".hostpfvac";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);

  pvname = _name + ".portpfvac";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52004);
  attachInOutPV(this, pva);

  // mce4
  pvname = _name + ".hostmce4";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);

  pvname = _name + ".portmce4";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52008);
  attachInOutPV(this, pva);

  // portescap indexors
  pvname = _name + ".hostportescap";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host);
  attachInOutPV(this, pva);

  pvname = _name + ".portportescap";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52024);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform initial connection too all agents

int UFInit::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".host";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    clog<<"UFInit::genExec> connect host: "<<input<<endl;
    //pva->clearVal(); // clear?
  }
  UFInit::Parm* p = new UFInit::Parm;
  int stat = connect(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFInit::genExec> failed to start connection pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFInit::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFINIT___
