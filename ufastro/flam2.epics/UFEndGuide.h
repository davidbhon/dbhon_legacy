#if !defined(__UFEndGuide_h__)
#define __UFEndGuide_h__ "$Name:  $ $Id: UFEndGuide.h,v 0.2 2005/05/25 16:18:56 hon Exp $"
#define __UFEndGuide_H__(arg) const char arg##EndGuide_h__rcsId[] = __UFEndGuide_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFEndGuide : public UFCAD {
public:
  struct Parm { UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(UFObserve::Parm* op= 0, UFObsSetup::Parm* osp= 0, UFInstrumSetup::Parm* isp= 0): _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFEndGuide(const string& instrum= "instrum") : UFCAD(instrum+":endGuide") { _create(); }
  inline virtual ~UFEndGuide() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* loseAll(void* p);
  static int getlost(UFEndGuide::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFEndGuide_h__
