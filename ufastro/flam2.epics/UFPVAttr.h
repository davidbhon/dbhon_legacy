#if !defined(__UFPVAttr_h__)
#define __UFPVAttr_h__ "$Name:  $ $Id: UFPVAttr.h,v 0.20 2005/10/18 20:54:50 hon Exp $"
#define ____(arg) const char arg##PVAttr_h__rcsId[] = __UFPVAttr_h__;

// Portable Epics headers:
#include "aitTypes.h"
#include "casdef.h" // all cas class declarations
#include "epicsTimer.h"
#include "gddApps.h"
#include "gddAppFuncTable.h"
#include "gddScalar.h"
#include "fdManager.h"

// std
#include "string"
#include "map"

// foreard declarations:
class UFCAServ;
class UFGem;

using namespace std;
// (hon) note the comment below is from the original pcas simple example...
/// PV attributes and containee of db rec container object
/** This class is not part of the server interface, but is only used
 * here in order to keep track of PV values. It contains members for
 * storing and accessing all the application types needed to satisfy a
 * DBR_CTRL request. DBR_STS requests can also be satisfied. Since DM
 * makes DBR_CTRL requests for each controller or monitor initially,
 * and then makes DBR_STS requests subsequently,this class contains
 * the attributes needed to work with DM clients.
 */
class UFPVAttr {
public:
  /// ctors
  /* old
  UFPVAttr(const char* pName, char* val= 0);
  UFPVAttr(const char* pName, int val);
  UFPVAttr(const char* pName, double val);
  */
  UFPVAttr(const string& pName, const string& val= "");
  UFPVAttr(const string& pName, int val);
  UFPVAttr(const string& pName, double val);
  // create/init new pv from old (output from input)
  UFPVAttr(const string& pvname, const UFPVAttr* pv);

  inline virtual ~UFPVAttr() {}

  /// accessors
  inline const aitString& getName () const { return _name; }
  inline string name() const { string ns; const char* cs = _name.string(); if( cs != 0 ) ns = cs; return ns;}
  inline string valString() {
    int val = (int) getNumeric();
    aitString s; _pVal->getConvert(s);
    string vs; const char* cs = s.string();
    if( cs != 0 ) 
      vs = cs;
    else if( val == INT_MIN )
      vs = "null";
    return vs;
  }

  string setVal(const string& val, bool writepost= true);
  int setVal(int val, bool writepost= true);
  double setVal(double val, bool writepost= true);
  void clearVal(bool writepost= true); // indicate cleared-value should write/post monitor or not
  bool isSet();
  bool isClear();

  // provide means to copy the value (but not the other attributes),
  // this performs a UFPV::write to insure any monitors are posted:
  virtual void copyVal(const UFPVAttr* inpva);

  /// if gdd val is a string i have no idea what this will do...
  inline double getHighOperation () const { aitFloat64 d; _pVal->getConvert(d); return _highoperat * d; }
  inline double getLowOperation ()  const { aitFloat64 d; _pVal->getConvert(d); return _lowoperat * d; }
  inline double getHighAlarm () const { aitFloat64 d; _pVal->getConvert(d); return _high_alarm * d; }
  inline double getHighWarn () const { aitFloat64 d; _pVal->getConvert(d); return _high_warn * d; }
  inline double getLowWarn() const { aitFloat64 d; _pVal->getConvert(d); return _low_warn * d; }
  inline double getLowAlarm () const { aitFloat64 d; _pVal->getConvert(d); return _low_alarm * d; }
  inline double getHighCtrl () const { aitFloat64 d; _pVal->getConvert(d); return _high_ctrl_lim * d; }
  inline double getLowCtrl () const  { aitFloat64 d; _pVal->getConvert(d); return _low_ctrl_lim * d; }
  inline short getPrec () const { return _precision; }
  inline gdd* getVal () const { return _pVal; }
  inline aitString getUnits () const { return _units; }

  // return reference to numeric val...
  inline double& getNumeric() { return _numeric; }

  /// gemini spedific funcs.
  // set this pv's container rec.
  inline void setDBRecInput(UFGem* dbr) { _indbr = dbr; }
  inline void setDBRecOutput(UFGem* dbr) { _outdbr = dbr; }
  inline UFGem* getDBRecInput() const { return _indbr; }
  inline UFGem* getDBRecOutput() const { return _outdbr; }
  inline void clearDBRec() { _indbr = _outdbr = 0; }

  /// validate the new value
  int validate() const;

  // allow immediate processing of SAD and CARs on new input (but not on new output):
  // differ processing of CAD and Apply until start directive 
  UFGem* isCADInput();
  UFGem* isCARInput();
  /// allow automatice processing of SAD on new input:
  UFGem* isSADInput();
  /// allow automatice processing of CAD or Apply on new Directive input:
  UFGem* isDirectiveInput();

  /// on new value, if valid input (not output!), mark this pv's container record (presumable a CAD)
  int mark();
  void clear() const;
  int preset() const;
  int directive(int d) const;
  int start(int timeout= 0) const;
 
  inline static int registerPV(UFPVAttr* pva)
    { if( pva ) { string key = pva->name(); _thePVList[key] = pva; } return (int) _thePVList.size(); }
  inline static int registerPV(const string& pvname, UFPVAttr* pva) // allow aliases
    { if( pva && pvname != "" ) { _thePVList[pvname] = pva; } return (int) _thePVList.size(); }
  
  // will this ever change once set?
  //inline caServer* getCas() { return _cas; } 
  //inline caServer* setCas(caServer* cas) { caServer* ocas = _cas; _cas = cas; return ocas; }

  /// the full set of PVs also fully visible to public:
  static std::map<string, UFPVAttr*> _thePVList;

  // as is the ptr to the caserver (singleton?)
  static UFCAServ* _cas; /// to allow posting of cas events on PV writes via PV ctors

  inline UFPVAttr* assocOut() { return _assocOut; }
  inline UFPVAttr* assocIn() { return _assocIn; }
  inline string assocInName() { string s; return (_assocIn == 0) ? s : _assocIn->name(); }
  inline string assocOutName() { string s; return (_assocOut == 0) ? s : _assocOut->name(); }
  inline void setAssoc(UFPVAttr* pin= 0, UFPVAttr* pout= 0) { _assocIn = pin; _assocOut = pout; }

protected:
  /// numeric value of string/text pVal, if approp., fully visible to public:
  double _numeric; 
  double _highoperat,    /// 
         _lowoperat,     /// 
         _high_alarm,    /// 
         _low_alarm,     /// 
         _high_warn,     /// 
         _low_warn,      /// 
         _high_ctrl_lim, /// 
         _low_ctrl_lim;  ///
  short _precision;      ///
  aitString _name, /// 
            _units;/// 
  gdd* _pVal;            ///

  UFGem* _indbr; /// gemini record container to which this pva belongs as input 
  UFGem* _outdbr; /// gemini record container to which this pva belongs as output

  /// if this is an input PVAttr, it might have an associated output PVAttr;
  /// if this is an output PVAttr, it might have an associated input PVAttr;
  /// in either case, the pointer to the assoc. PV is of use 
  UFPVAttr *_assocIn, *_assocOut;
};

#endif // __UFPVAttr_h__
