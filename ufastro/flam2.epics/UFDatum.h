#if !defined(__UFDatum_h__)
#define __UFDatum_h__ "$Name:  $ $Id: UFDatum.h,v 0.5 2005/10/24 20:15:51 hon Exp $"
#define __UFDatum_H__(arg) const char arg##Datum_h__rcsId[] = __UFDatum_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFInit.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFDatum : public UFCAD {
public:
  struct Parm { string _subsys; UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(const string& subsys,
		UFObserve::Parm* op= 0,
		UFObsSetup::Parm* osp= 0,
		UFInstrumSetup::Parm* isp= 0): _subsys(subsys), _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFDatum(const string& instrum= "instrum") : UFCAD(instrum+":datum") { _create(); }
  inline virtual ~UFDatum() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* datumAll(void* p);
  static int datum(UFDatum::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  static int _DatumCnt;

protected:
  // ctor helper
  void _create();
};

#endif // __UFDatum_h__
