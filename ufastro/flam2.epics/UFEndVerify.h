#if !defined(__UFEndVerify_h__)
#define __UFEndVerify_h__ "$Name:  $ $Id: UFEndVerify.h,v 0.2 2005/05/25 16:18:57 hon Exp $"
#define __UFEndVerify_H__(arg) const char arg##EndVerify_h__rcsId[] = __UFEndVerify_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFEndVerify : public UFCAD {
public:
  struct Parm { UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(UFObserve::Parm* op= 0, UFObsSetup::Parm* osp= 0, UFInstrumSetup::Parm* isp= 0): _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFEndVerify(const string& instrum= "instrum") : UFCAD(instrum+":endVerify") { _create(); }
  inline virtual ~UFEndVerify() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* endAll(void* p);
  static int end(UFEndVerify::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFEndVerify_h__
