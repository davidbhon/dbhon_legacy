#if !defined(__UFVerify_CC__)
#define __UFVerify_CC__ "$Name:  $ $Id: UFVerify.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFVerify_CC__;

#include "UFVerify.h"
#include "UFCAR.h"
//#include "UFCCVerify.h"
//#include "UFDCVerify.h"
//#include "UFECVerify.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFVerify::checkit(void* p) {
  // check really just needs the mech. names
  UFVerify::ThrdArg* arg = (UFVerify::ThrdArg*) p;
  UFVerify::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFVerify::checking>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFVerify::checking> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFVerify::checking> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFVerify::checking> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFVerify::checking> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose check func. 

/// arg: mechName, motparm*
int UFVerify::check(UFVerify::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to check whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFVerify::ThrdArg* arg = new UFVerify::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFVerify::checkit, arg);
    return (int) thrid;
  }

  //UFCCVerify::checkAll(timeout, rec, car);
  //UFDCVerify::checkAll(timeout, rec, car);
  //UFECVerify::checkAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFVerify::_create() {
  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate check action
  attachInputPV(pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform check mechanisms with non-null step cnts or named-position inputs
int UFVerify::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFVerify::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFVerify::genExec> mode: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFVerify::Parm* p = new UFVerify::Parm;
  int stat = check(p, timeout, this, _car);  // perform requested check

  if( stat < 0 ) { 
    clog<<"UFVerify::genExec> failed to start check pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFVerify::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFVerify_CC__
