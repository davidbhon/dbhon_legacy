#if !defined(__UFECSetup_h__)
#define __UFECSetup_h__ "$Name:  $ $Id: UFECSetup.h,v 0.15 2006/10/10 14:21:08 hon Exp $"
#define __UFECSetup_H__(arg) const char arg##ECSetup_h__rcsId[] = __UFECSetup_h__;

#include "UFCAD.h"
#include "UFClientSocket.h"

#include "vector"

class UFECSetup : public UFCAD {
public:
  // environment control paramaters
  // lvdtonoff < 0 do not send any command to lvdt, == 0 power lvdt off, == 1 power lvdt on...
  // camA control loop supports ranges, camB does not. MOS is not currently controlled...
  struct EnvParm { int _OnOffLVDT, _PMOS, _IMOS, _DMOS, _RMOS, _OnOffMOS, _PCamA, _ICamA, _DCamA, _RCamA, _OnOffCamA, 
		     _PCamB, _ICamB, _DCamB, _OnOffCamB; double _MOSKelv, _CamAKelv, _AManual, _CamBKelv, _BManual, _Lock;
    /*
    inline EnvParm(int lvdton= -1, double kmos=71.1, double ka=72.2, double kb=72.2,
		   int pmos=1000, int imos=200, int dmos=100, int rmos= 3, int onoffmos= -1,
		   int pa=1000, int ia=200, int da=100, int ra= 3, int onoffa= -1,
		   int pb=1000, int ib=200, int db=100, int onoffb= -1, double manual= 38.0) : 
    */
    inline EnvParm(int lvdton= -1, double kmos=75.5, double ka=75.5, double kb=75.5,
		   int pmos=30, int imos=2, int dmos=0, int rmos= 3, int onoffmos= -1,
		   int pa=30, int ia=2, int da=0, int ra= 3, int onoffa= -1, double amanual= 0.0,
		   int pb=30, int ib=2, int db=0, int onoffb= -1, double bmanual= 38.0, int lock=0) : // manual == 38% ??
      _OnOffLVDT(lvdton),
      _PMOS(pmos), _IMOS(imos), _DMOS(dmos), _RMOS(rmos),
      _PCamA(pa), _ICamA(ia), _DCamA(da), _RCamA(ra), _OnOffCamA(onoffa),
      _PCamB(pb), _ICamB(ib), _DCamB(db), _OnOffCamB(onoffb),
      _MOSKelv(kmos), _CamAKelv(ka), _AManual(amanual), _CamBKelv(kb), _BManual(bmanual), _Lock(lock) {}
    // inline void pwrOnLVDT() { _OnOffLVDT = 1; }
    // inline void pwrOffLVDT() { _OnOffLVDT = 0; }
  };

  // thread arg:
  struct ECThrdArg { EnvParm* _parms; UFGem* _rec; UFCAR* _car;
    inline ECThrdArg( EnvParm* p, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ECThrdArg() { delete _parms; }
  };

  inline UFECSetup(const string& instrum= "instrum", const string& host= "", UFCAR* car= 0) : UFCAD(instrum+":ec:setup", car) 
    { _create(host); }

  inline virtual ~UFECSetup() {}

  virtual int validate(UFPVAttr* pva= 0);

  /// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
  virtual int genExec(int timeout= 0);

  ///general env. ctl. func. (non datum)
  // of use here and from IS. starts a new daemn thread for every propper seq.
  static int environ(UFECSetup::EnvParm* p, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  // thread entry func.
  static void* envControl(void* p= 0);

  // default SAD channel names
  static int sadChans(const string& instrum, vector< string>& sadrecs);

  inline static void setHost(const string& host) { _host = host; }
  inline static void setPorts(int ls332= 52003, int lvdt= 52006)
    { _ls332port= ls332; _lvdtport= lvdt; }
  inline static string agentHostAndPorts(int& ls332, int& lvdt)
    { ls332 = _ls332port; lvdt = _lvdtport;  return _host; }

  // assume we only need to command Lakeshore 332 (PID & set point), and LVDT readout parms.
  // the PLC and the Pfeifer and the Lakeshore 218 need no commands, they passively
  // provide hearbeat env. measurements to the SAD:
  static string _host; // agent(s) location
  static int _ls332port, _lvdtport;
  static UFClientSocket *_ls332d, *_lvdtd;
  
protected:
  // ctor helper
  void _create(const string& host, int ls332port= 52003, int lvdtport= 52006);
};

#endif // __UFECSetup_h__
