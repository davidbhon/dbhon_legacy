#if !defined(__UFPV_h__)
#define __UFPV_h__ "$Name:  $ $Id: UFPV.h,v 0.5 2005/09/30 18:19:34 hon Exp $"
#define __UFPV_H__(arg) const char arg##PV_h__rcsId[] = __UFPV_h__;

#include "UFPVAttr.h"
class UFCAServ; // forward declaration

/// Brief (ONE LINE) description of class: TBD
/** The Process Variable class, Flam2PV. Provides the read() and write()
 * functions. The read() function calls the Flam2Server::read() function
 * which calls the appropriate function or functions from its function
 * table. The actual functions to be called are members of 
 * the Flam2PV class, readStatus() to readUnits().
 */ 
class UFPV : public casPV {
  // this only supports scalars now
public:
  /*
  UFPV();
  UFPV(const UFPV& rhs);
  */
  UFPV(const caServer& cas, const UFPVAttr& attr, aitBool insterest= aitTrue);
  inline ~UFPV() {}

  // key virtuals:
  inline virtual const char* getName() const { return _attr.getName(); }
  inline virtual caStatus interestRegister() { _interest = aitTrue; return S_casApp_success; }
  inline virtual void interestDelete() { _interest = aitFalse; }
  virtual caStatus read(const casCtx& ctx, gdd& prototype);
  virtual caStatus beginTransaction();
  virtual void endTransaction();
  virtual aitEnum bestExternalType();
  // require virtual (calls static sig below):
  virtual caStatus write(const casCtx& ctx, const gdd& val);

  // essential write functionality:
  static caStatus UFPV::write(const gdd& val, UFPV& pv, UFCAServ* cas= 0);

  // support writes from external object (like genExec of UFGem rec.)
  static caStatus write(const gdd& val, UFPVAttr& pva, UFCAServ* cas= 0);

  //static caStatus write(const string& sval, UFPVAttr& pva);
  //static caStatus write(int ival, UFPVAttr& pva);
  //static caStatus write(double dval, UFPVAttr& pva);

  // for read func. table:
  gddAppFuncTableStatus readStatus(gdd& val);
  gddAppFuncTableStatus readSeverity(gdd& val);
  gddAppFuncTableStatus readPrecision(gdd& val);
  gddAppFuncTableStatus readHighOperation(gdd& val);
  gddAppFuncTableStatus readLowOperation(gdd& val);
  gddAppFuncTableStatus readHighAlarm(gdd& val);
  gddAppFuncTableStatus readHighWarn(gdd& val);
  gddAppFuncTableStatus readLowWarn(gdd& val);
  gddAppFuncTableStatus readLowAlarm(gdd& val);
  gddAppFuncTableStatus readHighCtrl(gdd& val);
  gddAppFuncTableStatus readLowCtrl(gdd& val);
  gddAppFuncTableStatus readVal(gdd& val);
  gddAppFuncTableStatus readUnits(gdd& val);

  inline const UFPVAttr& getAttr() const { return _attr; }
  inline const aitBool interest() const { return _interest; }

  static bool _verbose, _vverbose;

protected:
  static int _currentOps; /// ?
  const UFPVAttr& _attr;  /// see UFPVAttr
  aitBool _interest; /// monitor trigger
};

#endif // __UFPV_h__
