#if !defined(__UFDCInit_h__)
#define __UFDCInit_h__ "$Name:  $ $Id: UFDCInit.h,v 0.5 2006/02/15 21:48:57 hon Exp $"
#define __UFDCInit_H__(arg) const char arg##DCInit_h__rcsId[] = __UFDCInit_h__;

#include "UFCAD.h"

// init the _connection socket here
#include "UFDCSetup.h"

class UFDCInit : public UFCAD {
public:
  inline UFDCInit(const string& instrum= "instrum") : UFCAD(instrum+":dc:init") { _create(); }
  inline virtual ~UFDCInit() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // thread that performs portescap agent connection
  static void* connection(void* p);

  // start connection thread
  static int connect(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  static int reconnect(const string& name, string& errmsg, int& err);

protected:
  // ctor helper
  void _create();
};

#endif//  __UFDCInit_h__
