#if !defined(__UFObsSetup_h__)
#define __UFObsSetup_h__ "$Name:  $ $Id: UFObsSetup.h,v 0.10 2006/02/21 22:31:31 hon Exp $"
#define __UFObsSetup_H__(arg) const char arg##ObsSetup_h__rcsId[] = __UFObsSetup_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

class UFObsSetup : public UFCAD {
public:
  enum { _Imaging= 0, _Spect= 1 };
  struct Parm {
    int _obsmode; // wheelbias mode imaging or spectroscopy
    int _expcnt; // sci/frame-mode == 1; eng/frame-mode == 2 aka readoutmode
    int _numreads; // internal mce4 coadds?
    float _exptime;
    string _nfshost; // local host file-system or nfs mount?
    string _datadir; // local/nfs file-sys. directory
    string _filename; // non-dhs fits file name
    UFDCSetup::DetParm* _dparms;
    inline Parm(int obsmod= _Imaging, int nfrm=1, int nr=1, double t= 1.0,
		const string& file= "Flamingos2.fits" ,const string& dir= "", const string& nfshost= "") : 
      _obsmode(obsmod), _expcnt(nfrm), _numreads(nr), _exptime(t),
      _nfshost(nfshost), _datadir(dir), _filename(file), _dparms(0) {}
    inline ~Parm() {}
  };
  // thread arg:
  struct OSThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline OSThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~OSThrdArg() { delete _parms; }
  };

  inline UFObsSetup(const string& instrum= "instrum") : UFCAD(instrum+":obsSetup") { _create(); }
  inline virtual ~UFObsSetup() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // thread entry func.
  static void* sim(void* p= 0);
  /// general instrument seq. obs setup  func. (non datum)
  /// IS. starts a new daemon thread for every propper seq.
  static int config(UFObsSetup::Parm* p, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  // InstrumSetup also sets its own version of this, which may be overriden here,
  // then 'cleared/nulled' on processing this CAD
  static string _BiasMode; // this value may override UFInstrumSetup::_BiasMode value...
  // return list of established bias and preamp values for "imaging" or "spect" modes
  // values should be 0 -- 255:
  static int biasModeVals(const string& modename, map< string, int >& biases, map< string, int >& preamps);

protected:
  // ctor helper
  void _create();
};

#endif // __UFObsSetup_h__
