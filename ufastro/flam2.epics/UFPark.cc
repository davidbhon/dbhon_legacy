#if !defined(__UFPark_CC__)
#define __UFPark_CC__ "$Name:  $ $Id: UFPark.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPark_CC__;

#include "UFPark.h"
#include "UFCAR.h"
#include "UFCCPark.h"
#include "UFDCPark.h"
#include "UFECPark.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFPark::seekit(void* p) {
  // seek really just needs the mech. names
  UFPark::ThrdArg* arg = (UFPark::ThrdArg*) p;
  UFPark::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFPark::seeking>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFPark::seeking> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFPark::seeking> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFPark::seeking> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFPark::seeking> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose seek func. 

/// arg: mechName, motparm*
int UFPark::seek(UFPark::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to seek whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFPark::ThrdArg* arg = new UFPark::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFPark::seekit, arg);
    return (int) thrid;
  }

  UFCCPark::seekAll(timeout, rec, car);
  UFDCPark::mce4(timeout, rec, car);
  UFECPark::env(timeout, rec, car);

  return 3;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFPark::_create() {
  string pvname = _name + ".subsys";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate seek action mark
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
int UFPark::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFPark::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".subsys";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFPark::genExec> subsys: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFPark::Parm* p = new UFPark::Parm;
  int stat = seek(p, timeout, this, _car);  // perform requested seek

  if( stat < 0 ) { 
    clog<<"UFPark::genExec> failed to start seek park pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFPark::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFPark_CC__
