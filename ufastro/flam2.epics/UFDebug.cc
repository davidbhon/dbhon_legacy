#if !defined(__UFDebug_CC__)
#define __UFDebug_CC__ "$Name:  $ $Id: UFDebug.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDebug_CC__;

#include "UFDebug.h"
#include "UFCAR.h"
//#include "UFCCDebug.h"
//#include "UFDCDebug.h"
//#include "UFECDebug.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFDebug::debuging(void* p) {
  // debug really just needs the mech. names
  UFDebug::ThrdArg* arg = (UFDebug::ThrdArg*) p;
  UFDebug::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulating...
  clog<<"UFDebug::debuging>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFDebug::debuging> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFDebug::debuging> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFDebug::debuging> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFDebug::debuging> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose debug func. 

/// arg: mechName, motparm*
int UFDebug::debug(UFDebug::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optingal car's setBusy
  // while the optingal supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to debug whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocatings...
  if( _sim ) {
    UFDebug::ThrdArg* arg = new UFDebug::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFDebug::debuging, arg);
    return (int) thrid;
  }

  //UFCCDebug::debugAll(timeout, rec, car);
  //UFDCDebug::debugAll(timeout, rec, car);
  //UFECDebug::debugAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDebug::_create() {
  string pvname = _name + ".loglevel";
  // any non null string should indicate debug acting
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
int UFDebug::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDebug::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".loglevel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDebug::genExec> debug level: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFDebug::Parm* p = new UFDebug::Parm;
  int stat = debug(p, timeout, this, _car);  // perform requested debug

  if( stat < 0 ) { 
    clog<<"UFDebug::genExec> failed to start debug pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDebug::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFDebug_CC__
