#if !defined(__UFDCSETUP_DC__)
#define __UFDCSETUP_DC__ "$Name:  $ $Id: UFDCSetup.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCSETUP_DC__;

#include "UFDCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFCAServ.h"

// global (static) attributes:
string UFDCSetup::_host; // agent location
int UFDCSetup::_port= 52008;
UFClientSocket* UFDCSetup::_connection= 0;

// ctors:

// static class funcs:
int UFDCSetup::_indexOfMCE4Bias(const string& name) {
  const int nb= 4; // note the order below is the current MCE4 indexing (June 2006):
  const char* biasnames[] = { "BiasGATE", "BiasPWR", "BiasVRESET", "BiasVCC" };
  for( int i = 0; i < nb; ++i ) {
    if( name.find(biasnames[i]) != string::npos )
      return i;
  }
  return 0;
}

struct UFDCSetup::BiasPreampCnt
UFDCSetup::allocBiasAndPreamps(const string& modename, map<string, double>*& biases, map<string, double>*& preamps) {
  const int nb= 4;
  const char* biasnames[] = { "BiasGATE", "BiasPWR", "BiasVRESET", "BiasVCC" };
  biases = new map<string, double>;
  if( modename.find("imaging") != string::npos || 
      modename.find("Imaging") != string::npos ||
      modename.find("IMAGING") != string::npos ) {
    for( int i = 0; i < nb; ++i )
      (*biases)[biasnames[i]] = 1.0; //INT_MIN;
  }
  else {
    for( int i = 0; i < nb; ++i )
      (*biases)[biasnames[i]] = 0.75; //INT_MIN;
  }

  const int np= 32;
  const char* preampnames[] = { 
    "PreAmp01", "PreAmp02", "PreAmp03", "PreAmp04", "PreAmp05", "PreAmp06", "PreAmp07", "PreAmp08",
    "PreAmp09", "PreAmp10", "PreAmp11", "PreAmp12", "PreAmp13", "PreAmp14", "PreAmp15", "PreAmp16",
    "PreAmp17", "PreAmp18", "PreAmp19", "PreAmp20", "PreAmp21", "PreAmp22", "PreAmp23", "PreAmp24",
    "PreAmp25", "PreAmp26", "PreAmp27", "PreAmp28", "PreAmp29", "PreAmp30", "PreAmp31", "PreAmp32" };

  preamps = new map<string, double>;
  if( modename.find("imaging") != string::npos || 
      modename.find("Imaging") != string::npos ||
      modename.find("IMAGING") != string::npos ) {
    for( int i = 0; i < np; ++i )
      //(*preamps)[preampnames[i]] = 1.0; //INT_MIN;
      (*preamps)[preampnames[i]] = INT_MIN;
  }
  else {
    for( int i = 0; i < np; ++i )
      (*preamps)[preampnames[i]] = 0.75; //INT_MIN;
  }
  struct BiasPreampCnt bpcnt(biases->size(), preamps->size());
  return bpcnt;
}

void* UFDCSetup::configMCE4(void* p) {
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parm = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = UFCAServ::_instrum + ":dc:setupC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";
  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  bool validsoc = connected(UFDCSetup::_connection);
  if( !_sim && !validsoc ) {
    clog<<"UFDCSetup::configMCE4> not connected to UFMCE agent, need to (re)init..."<<endl;
    // and free the arg & parms
    delete arg;
    return p;
  }
  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  if( _sim ) {
    clog<<"UFDCSetup::configMCE4> sim... "<<endl;
    if( parm ) {
      clog<<"UFDCSetup::configMCE4> detector pixels: "<<parm->_w * parm->_h<<endl;
    }
    // set cad/car idle after 1/2:
    UFPosixRuntime::sleep(0.5);
    if( car ) {
      if( err != 0 )
        car->setErr(errmsg);
      else
        car->setIdle();
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) {
        if( err != 0 )
	  c->setErr(errmsg);	
        else
          c->setIdle();
      }
    }
    delete arg;
    return p;
  }

  string cmd = "CAR";
  string cmdpar = carname;
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "OBSCONF";
  cmdpar = "Preset";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  if( !parm->_passwd.empty() && parm->_passwd != "null" ) {
    cmd = "Raw "; cmd += "ExpCnt!!ErrSetup ";
    cmdpar = "APWB ENGINEERING";
    strstream s; s<<"APWB "<<parm->_passwd<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  if( parm->_expcnt > 0 ) {
    cmd = "Raw "; cmd += "ExpCnt!!ErrSetup ";
    cmdpar = "LDVAR 10 1";
    strstream s; s<<"LDVAR 10 "<<parm->_expcnt<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  if( parm->_pbc >= 0 ) {
    cmd = "Raw "; cmd += "PixBaseClck!!ErrSetup ";
    cmdpar = "PBC 10";
    strstream s; s<<"PBC "<<parm->_pbc<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  if( parm->_cdsreads > 0 ) {
    cmd = "Raw "; cmd += "CDSReadout!!ErrSetup ";
    cmdpar = "LDVAR 2 4";
    strstream s; s<<"LDVAR 2 "<<parm->_cdsreads<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  if( parm->_sec > 0 ) {
    cmd = "SAD";
    cmdpar = UFCAServ::_instrum + ":sad:EXPTIME";
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
    cmd = "Raw "; cmd += "ExpTime!!ErrSetup";
    cmdpar = "TIMER 0 1 S";
    strstream s; s<<"TIMER 0 "<<parm->_sec<<" S"<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }
  /*
  cmd = "Raw "; cmd += "Sim!!ErrSetup ";
  cmdpar = "SIM 0"; // turn off simulation
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  */

  if( parm->_presets >= 0 ) {
    cmd = "Raw "; cmd += "Presets!!ErrSetup ";
    cmdpar = "LDVAR 0 10"; // 10 presets
    strstream s; s<<"LDVAR 0 "<<parm->_presets<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  if( parm->_postsets >= 0 ) {
    cmd = "Raw "; cmd += "Postsets!!ErrSetup ";
    cmdpar = "LDVAR 1 1000"; // 1000 postsets
    strstream s; s<<"LDVAR 1 "<<parm->_postsets<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  // set & latch all biases and preamps
  map< string, double >::const_iterator itb = parm->_bias->begin();
  for( int i = 0; itb != parm->_bias->end(); ++i, ++itb ) {
    string name = itb->first;
    double val = itb->second;
    clog<<"UFDCSetup::configMCE4> name: "<<name<<", val: "<<val<<endl;
    if( (long)val == INT_MIN ) continue;
    // set all biases...
    int ival = (int)219; // VReset?
    if( name.find("GATE") != string::npos ) // bias GATE default on mce4 power-up == 179
      ival = (int)((val*255) / 4.970); // according to kevin's mce4 code
    else if( name.find("PWR") != string::npos ) // bias Power default on mce4 power-up == 255
      ival = (int)((val*255) / 4.960); // according to kevin's mce4 code
    else if( name.find("Reset") != string::npos ) // bias VReset default on mce4 power-up == 128
      ival = (int)((val*255) / 1.055); // according to kevin's mce4 code
    else if( name.find("VCC") != string::npos ) // bias Array Vcc default on mce4 power-up == 233
      ival =(int)( (val*255) / 5.47); // according to kevin's mce4 code
    cmd = "Raw "; cmd += "Bias!!ErrSetup";
    strstream bs; bs<<"DAC_SET_SINGLE_BIAS "<<_indexOfMCE4Bias(name); // mce4 cmd uses integer index id
    if( ival >= 0 && ival <= 255 )
      bs<<" "<<ival<<ends;
    else if( ival > 255 ) 
      bs<<" 255"<<ends;
    else
      bs<<" 0"<<ends;
    cmdpar = bs.str(); delete bs.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  map< string, double >::const_iterator itp = parm->_preamp->begin();
  int same = 1;
  double valzero;
  for (int i= 0; itp != parm->_preamp->end(); ++i, ++itp ) {
    string name = itp->first;
    double val = itp->second;
    if (i == 0) valzero = val;
    if (valzero != val) same=0;
  }
  itp = parm->_preamp->begin();
  if (same == 1) {
    int ival = (int)((valzero*255)/9.4);
    cmd = "Raw "; cmd += "Preamp!!ErrSetup";
    strstream ps; ps <<"DSAP ";
    if( ival >= 0 && ival <= 255 )
      ps<<" "<<ival<<ends;
    else if( ival > 255 )
      ps<<" 255"<<ends;
    else
      ps<<" 0"<<ends;
    cmdpar = ps.str(); delete ps.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  } else for( int i= 0; itp != parm->_preamp->end(); ++i, ++itp ) {
  //for( int i= 0; itp != parm->_preamp->end(); ++i, ++itp ) {
    string name = itp->first;
    double val = itp->second;
    clog<<"UFDCSetup::configMCE4> name: "<<name<<", val: "<<val<<endl;
    if( (long)val == INT_MIN ) continue;
    // set all preamps...
    int ival = (int)((val*255) / 9.4); // PreAmp offset default on mce4 power up == 229
    cmd = "Raw "; cmd += "Preamp!!ErrSetup";
    //strstream ps; ps <<"DAC_SET_SINGLE_PREAMP "<<i;
    strstream ps; ps <<"DSSP "<<i;
    if( ival >= 0 && ival <= 255 )
      ps<<" "<<ival<<ends;
    else if( ival > 255 ) 
      ps<<" 255"<<ends;
    else
      ps<<" 0"<<ends;
    cmdpar = ps.str(); delete ps.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  // latch all
  cmd = "Raw "; cmd += "LatchBias!!ErrSetup";
  cmdpar = "DAC_LAT_ALL_BIAS";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "Raw "; cmd += "LatchBias!!ErrSetup";
  cmdpar = "DAC_LAT_ALL_PREAMP";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);

  if( parm->_cycletype >= 0 ) {
    cmd = "SAD";
    cmdpar = UFCAServ::_instrum + ":sad:CYCLETYP";
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
    cmd = "Raw "; cmd += "CycleType 0!!ErrSetup";
    cmdpar = "CT 0"; // idle
    strstream s; s<<"CT "<<parm->_cycletype<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::configMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  // defer START directive to Observe CAD...
  UFStrings req(name, cmdvec);
  int ns = UFDCSetup::_connection->send(req);
  if( ns <= 0 ) {
    clog<<"UFDCSetup::configMCE4> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    err = 1;
    errmsg = "Failed MCE4 Setup";
  }
  //if( _verbose )
    clog<<"UFDCSetup::configMCE4> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
  //int nr = UFDCSetup::_connection->recv(req);

  // free arg (dtor deletes parms)
  delete arg; 

  // exit daemon thread
  return p;
} // configMCE4

/// general mce4 config func. (non datum)
int UFDCSetup::config(UFDCSetup::DetParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
  UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  // create and run pthread to configure mce4 and then either reset car to idle or error
  // thread should free DetParm* allocations...
  DCThrdArg* arg = new DCThrdArg(parms, rec, car);
  clog<<"UFDCSetup::config> start new MCE4 thread, detector pixels: "<<parms->_w * parms->_h<<endl;
  pthread_t thrid = UFPosixRuntime::newThread(UFDCSetup::configMCE4, arg);

  return (int) thrid;
}

// non static, non virtual funcs: 
// protected ctor helper:
void UFDCSetup::_create(const string& host, int port) {
  _host = host; _port = port; // set global statics
  if( _host == "" ) _host = UFRuntime::hostname();
  UFPVAttr* pva= 0;

  string pvname = _name + ".CycleType"; // CT 0 == datum, 10, 20, 40, ?
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".Passwd"; // "ARRAY_PW_BIAS ENGINEERING" or "APWB ENGINEERING"
  pva = (*this)[pvname] = new UFPVAttr(pvname, "");
  attachInOutPV(this, pva);

  pvname = _name + ".ExpCnt"; 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".Seconds"; // Timer 0 # (1->?)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".MilliSec"; // Timer 0 # 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".CDSReads"; // LDVAR 2 # (4-64)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 4);
  attachInOutPV(this, pva);

  pvname = _name + ".Presets"; // LDVAR 0 # (10)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 10);
  attachInOutPV(this, pva);

  pvname = _name + ".Postsets"; // LDVAR 1 # (1000)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1000);
  attachInOutPV(this, pva);

  pvname = _name + ".PixelBaseClock"; // PBC 10 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 10);
  attachInOutPV(this, pva);

  std::map<string, double>* biases; std::map<string, double>* preamps;
  struct BiasPreampCnt cnt = allocBiasAndPreamps("imaging", biases, preamps);
  if( _verbose )
    clog<<"UFDCSetup::_create> bias and preamps cnts: "<<cnt._nb<<", "<<cnt._np<<endl;
 
  // F2 only has 4 array bias channels (same as F1)
  map<string, double>::const_iterator itb = biases->begin();
  for( ; itb != biases->end(); ++itb ) {
    pvname = _name + "." + itb->first;
    double val = itb->second;
    pva = (*this)[pvname] = new UFPVAttr(pvname, val);
    attachInOutPV(this, pva);
    clog<<"UFDCSetup::_create> attached: "<<pvname<<endl;
  }

  // although the preamp has 32 setable dc offsets, not all may be used, so this may change...
  map<string, double>::const_iterator itp = preamps->begin();
  for( ; itp != preamps->end(); ++itp ) {
    pvname = _name + "." + itp->first;
    double val = itp->second;
    pva = (*this)[pvname] = new UFPVAttr(pvname, val);
    attachInOutPV(this, pva);
    clog<<"UFDCSetup::_create> attached: "<<pvname<<endl;
  }
  
  delete biases; delete preamps;

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
} // _create

/// virtual funcs:
int UFDCSetup::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string input = pva->valString();
  char* cs = (char*) input.c_str();
  while( *cs == ' ' && *cs == '\t' )
    ++cs;

  if( isdigit(cs[0]) )
    return 0;

  return -1;
}

/// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
int UFDCSetup::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCSetup::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // which pvs are non null (are set vs. cleared)?
  int lab = -1;
  int w= 2048, h= 2048, d= 32, ct= -1;
  string pvname = _name + ".CycleType";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    ct = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> ct: "<<ct<<endl;
    pva->clearVal(); // clear
  }

  string passwd= "";
  pvname = _name + ".Passwd";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    passwd = pva->valString();
    clog<<"UFDCSetup::genExec> passwd: "<<passwd<<endl;
    pva->clearVal(); // clear
  }

  int expcnt= -1;
  pvname = _name + ".ExpCnt";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    expcnt = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> excnt: "<<expcnt<<endl;
    pva->clearVal(); // clear
  }

  int s= -1;
  pvname = _name + ".Seconds";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    s = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> s: "<<s<<endl;
    pva->clearVal(); // clear
  } 
  
  int ms= -1;
  pvname = _name + ".MilliSec";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    ms = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> ms: "<<ms<<endl;
    pva->clearVal(); // clear
  }

  int rds= -1;
  pvname = _name + ".CDSReads";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    rds = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> rds: "<<rds<<endl;
    pva->clearVal(); // clear
  }

  int presets= -1;
  pvname = _name + ".Presets";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    presets = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> presets: "<<presets<<endl;
    pva->clearVal(); // clear
  }

  int postsets= -1;
  pvname = _name + ".Postsets";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    postsets = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> postsets: "<<postsets<<endl;
    pva->clearVal(); // clear
  }

  int pbc= -1;
  pvname = _name + ".PixelBaseClock";
  pva = (*this)[pvname];
  if( pva != 0 && pva->isSet() ) {
    pbc = (int)pva->getNumeric();
    clog<<"UFDCSetup::genExec> pbc: "<<pbc<<endl;
    pva->clearVal(); // clear
  }

  std::map< string, double >* biases= 0; std::map< string, double >* preamps= 0;
  struct BiasPreampCnt cnt = allocBiasAndPreamps("imaging", biases, preamps);
  if( biases ) clog<<"UFDCSetup::genExec> bias cnt: "<<biases->size()<<endl;
  if( preamps ) clog<<"UFDCSetup::genExec> preamp cnt: "<<preamps->size()<<endl;
  //if( _verbose )
    clog<<"UFDCSetup::genExec> bias and preamps cnts: "<<cnt._nb<<", "<<cnt._np<<endl;
 
  //F2 only has 4 array bias channelks (same as F1)
  map< string, double >::const_iterator itb = biases->begin();
  for( int i= 0; itb != biases->end(); ++itb, ++i ) {
    string biasname = itb->first;
    pvname = _name + "." + biasname;
    clog<<"UFDCSetup::genExec> bias name: "<<biasname<<", pvname: "<<pvname<<endl;
    pva = (*this)[pvname];
    if( pva == 0 ) {
      clog<<"UFDCSetup::genExec> pvname: "<<pvname<<" does not exist!"<<endl;
      continue;
    }
    if( pva->isClear() ) {
      clog<<"UFDCSetup::genExec> pvname: "<<pvname<<" is NOT Set."<<endl;
      continue;
    }
    double val = (*biases)[biasname] = pva->getNumeric();
    clog<<"UFDCSetup::genExec> "<<i<<", "<<biasname<<": "<<val<<endl;
    pva->clearVal(); // clear
  }
 
  map< string, double >::const_iterator itp = preamps->begin();
  for( int i= 0; itp != preamps->end(); ++itp, ++i ) {
    string preampname = itp->first;
    pvname = _name + "." + preampname;
    pva = (*this)[pvname];
    clog<<"UFDCSetup::genExec> preamp name: "<<preampname<<", pvname: "<<pvname<<endl;
    if( pva == 0 ) {
      clog<<"UFDCSetup::genExec> pvname: "<<pvname<<" does not exist!"<<endl;
      continue;
    }
    if( pva->isClear() ) {
      clog<<"UFDCSetup::genExec> pvname: "<<pvname<<" is NOT Set."<<endl;
      continue;
    }
    double val = (*preamps)[preampname] = pva->getNumeric();
    clog<<"UFDCSetup::genExec> "<<i<<", "<<preampname<<": "<<val<<endl;
    pva->clearVal(); // clear
  }
  // allocate full parm struct:
  int sim= INT_MIN, bootq= INT_MIN; 
  double kelvin= INT_MIN;
  UFDCSetup::DetParm* p = new UFDCSetup::DetParm(sim, bootq, lab, ct, expcnt, s, ms, rds, w, h, d,
						 pbc, presets, postsets, kelvin, passwd, biases, preamps);
  int stat = config(p, timeout, this, _car);
  if( stat < 0 )
    clog<<"UFDCSetup::genExec> failed to start mce4 pthread..."<<endl;

  return stat;
} // genExec

#endif // __UFDCSETUP_DC__
