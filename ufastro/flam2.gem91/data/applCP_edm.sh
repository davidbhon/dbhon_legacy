#!/bin/bash

# Example script for starting Gemini South engineering screens

if [ -z "$EPICS" ]; then
  echo "EPICS environment variables have not been defined."
  echo "Execute your local EPICS startup script and try again"
  exit 1
fi

# Make sure we are talking to the telescope on Cerro Pachon!
export EPICS_CA_ADDR_LIST=172.17.2.255

dir=$(dirname $0)

# $dir now holds the pathname to the location of the script.

export EDMFILES=$dir
export EDMCOLORFILE=colorsCP.list
export EDMFONTFILE=fontsCP.list
export EDMPRINTDEF=$EDMFILES/edmPrintCP.def
export EDMCALC=$EDMFILES/calcCP.list

# These environment variables should default to the location
# of the GEM9.0 installation tree

GEM9=/usr/software/dev/packages/epics/epics3.14.6GEM9.0/extensions

export EDMOBJECTS=$GEM9/src/edm/setup
export EDMPVOBJECTS=$GEM9/src/edm/setup
export EDMHELPFILES=$GEM9/src/edm/helpFiles

$GEM9/bin/${EPICS_HOST_ARCH}/edm -m "top=example:" -x template.edl &
