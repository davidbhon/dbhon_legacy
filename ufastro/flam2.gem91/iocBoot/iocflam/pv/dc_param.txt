;Detector Parameter File
;
;dc agent host IP number
192.168.111.222
;dc agent port number
52008
; Sky Transparency - Cloud Cover
;  Percentile  Chop Freq   Conditions
      20          3        Low Sky Noise
      50          3        Moderate Sky Noise
      70          3        High Sky Noise
      90          3        Unusable
; Sky Transparency - Water Vapour
;  Percentile      Conditions (mm)
     20                   2.3
     50                   4.3
     70                   7.6
     90                   9.0
;Fiducial Frame Time
 26
; Number of Readout Modes
3
; Readout Modes
S1
S1R1
S1R3
; Near Infrared Frame Time
; K Filter
; Default S1  S1R1  S1R3  
  45      40  45    50  
; L Filter
; Default S1  S1R1  S1R3  
  45      40  45    50
; M Filter
; Default S1  S1R1  S1R3  
  45      40  45    50
; Spectroscopy Frame Time
; LoRes10
; Default S1  S1R1  S1R3  
  100     100  100   100
; HiRes10
; Default S1  S1R1  S1R3  
   70      70   70    70
; LoRes20
; Default S1  S1R1  S1R3  
  200     200  200   200

