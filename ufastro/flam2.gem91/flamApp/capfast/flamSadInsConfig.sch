[schematic2]
uniq 58
[tools]
[detail]
s 2624 2032 100 1792 2001/02/11
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Revised to match ICD changes
s 2016 2032 100 1792 B
s 2624 2064 100 1792 2000/11/11
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamSadInsConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/02/11
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Flamingos Instrument Configuration
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs 1280 1671 100 0 esirs#50
xform 0 1488 1824
p 1216 1376 100 0 0 FTVL:LONG
p 1344 1632 100 0 1 SCAN:Passive
p 1472 1472 100 0 0 SNAM:
p 1440 1664 100 1024 -1 name:$(top)mosPlateBarCode
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)DeckerPos
use esirs -192 1255 100 0 esirs#27
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)mosPlateMDFString
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -256 544 100 0 0 FTVL:FLOAT
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)slitWidth
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)grismName
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)MOSName
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)mosPlateType
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)FilterName
use esirs 544 423 100 0 esirs#34
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)detectorPosName
use esirs 1280 1255 100 0 esirs#54
xform 0 1488 1408
p 1344 1216 100 0 1 SCAN:Passive
p 1344 1248 100 0 1 name:$(top)agenthostIP
use esirs 1280 839 100 0 esirs#55
xform 0 1488 992
p 1472 640 100 0 0 SNAM:
p 1360 832 100 0 1 name:$(top)LyotPos
use changeBar 1984 1991 100 0 changeBar#52
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#51
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadInsConfig.sch,v 0.0 2005/09/01 20:20:10 drashkin Exp $
