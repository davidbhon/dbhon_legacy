[schematic2]
uniq 104
[tools]
[detail]
w 648 1227 100 0 n#73 junction 512 1216 832 1216 832 1136 992 1136 estringouts.estringouts#102.DOL
w 728 1371 100 0 n#73 estringouts.estringouts#69.DOL 992 1360 512 1360 junction
w 728 1595 100 0 n#73 ecad2.ecad2#48.VALA 160 1216 512 1216 512 1584 992 1584 estringouts.estringouts#68.DOL
w 674 955 100 0 n#103 efanouts.efanouts#13.LNK3 576 944 832 944 832 1104 992 1104 estringouts.estringouts#102.SLNK
w 1240 1099 100 0 n#100 estringouts.estringouts#102.OUT 1248 1088 1280 1088 hwout.hwout#101.outp
w 1864 779 100 0 n#99 eseqs.eseqs#93.LNK2 1664 768 2112 768 ecars.ecars#53.IVAL
w 1704 811 100 0 n#99 eseqs.eseqs#93.LNK1 1664 800 1792 800 1792 768 junction
w 1016 483 100 0 n#98 efanouts.efanouts#13.LNK4 576 912 736 912 736 480 1344 480 eseqs.eseqs#93.SLNK
w 1272 779 100 0 n#97 eseqs.eseqs#93.DOL2 1344 768 1248 768 1248 736 1216 736 hwin.hwin#95.in
w 1272 811 100 0 n#96 eseqs.eseqs#93.DOL1 1344 800 1248 800 1248 832 1216 832 hwin.hwin#94.in
w -144 1547 -100 0 c#89 ecad2.ecad2#48.DIR -160 1408 -224 1408 -224 1536 -16 1536 -16 1856 -128 1856 inhier.DIR.P
w -176 1579 -100 0 c#90 ecad2.ecad2#48.ICID -160 1376 -256 1376 -256 1568 -48 1568 -48 1728 -128 1728 inhier.ICID.P
w 96 1547 -100 0 c#91 ecad2.ecad2#48.VAL 160 1408 224 1408 224 1536 16 1536 16 1856 160 1856 outhier.VAL.p
w 128 1579 -100 0 c#92 ecad2.ecad2#48.MESS 160 1376 256 1376 256 1568 48 1568 48 1728 160 1728 outhier.MESS.p
w 856 1339 100 0 n#74 efanouts.efanouts#13.LNK2 576 976 768 976 768 1328 992 1328 estringouts.estringouts#69.SLNK
w 840 1563 100 0 n#71 efanouts.efanouts#13.LNK1 576 1008 736 1008 736 1552 992 1552 estringouts.estringouts#68.SLNK
w 1240 1323 100 0 n#64 estringouts.estringouts#69.OUT 1248 1312 1280 1312 hwout.hwout#65.outp
w 1240 1547 100 0 n#59 estringouts.estringouts#68.OUT 1248 1536 1280 1536 hwout.hwout#60.outp
w 224 939 100 0 n#49 ecad2.ecad2#48.STLK 160 928 336 928 efanouts.efanouts#13.SLNK
s 2624 2032 100 1792 2000/12/05
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 flamDebug.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/05
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos debug Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use estringouts 992 1031 100 0 estringouts#102
xform 0 1120 1104
p 1056 992 100 0 1 OMSL:closed_loop
p 1056 1024 100 0 1 name:$(top)ecDebugLevel
p 1248 1088 75 768 -1 pproc(OUT):PP
use estringouts 992 1255 100 0 estringouts#69
xform 0 1120 1328
p 1056 1216 100 0 1 OMSL:closed_loop
p 1056 1248 100 0 1 name:$(top)dcDebugLevel
p 1248 1312 75 768 -1 pproc(OUT):PP
use estringouts 992 1479 100 0 estringouts#68
xform 0 1120 1552
p 1056 1440 100 0 1 OMSL:closed_loop
p 1056 1472 100 0 1 name:$(top)ccDebugLevel
p 1248 1536 75 768 -1 pproc(OUT):PP
use hwout 1280 1047 100 0 hwout#101
xform 0 1376 1088
p 1504 1088 100 0 -1 val(outp):$(top)ec:debug PP NMS
use hwout 1280 1271 100 0 hwout#65
xform 0 1376 1312
p 1504 1312 100 0 -1 val(outp):$(top)dc:debug.J PP NMS
use hwout 1280 1495 100 0 hwout#60
xform 0 1376 1536
p 1504 1536 100 0 -1 val(outp):$(top)cc:debug.J PP NMS
use hwin 1024 791 100 0 hwin#94
xform 0 1120 832
p 864 832 100 0 -1 val(in):$(CAR_BUSY)
use hwin 1024 695 100 0 hwin#95
xform 0 1120 736
p 864 736 100 0 -1 val(in):$(CAR_IDLE)
use eseqs 1344 391 100 0 eseqs#93
xform 0 1504 640
p 1408 352 100 0 1 DLY1:0.0e+00
p 1408 320 100 0 1 DLY2:0.5e+00
p 1536 384 100 1024 1 name:$(top)debugBusy
p 1312 800 75 1280 -1 pproc(DOL1):NPP
p 1312 768 75 1280 -1 pproc(DOL2):NPP
p 1680 800 75 1024 -1 pproc(LNK1):PP
p 1680 768 75 1024 -1 pproc(LNK2):PP
use outhier 176 1856 100 0 VAL
xform 0 144 1856
use outhier 176 1728 100 0 MESS
xform 0 144 1728
use inhier -192 1856 100 0 DIR
xform 0 -128 1856
use inhier -208 1728 100 0 ICID
xform 0 -128 1728
use ecars 2112 487 100 0 ecars#53
xform 0 2272 656
p 2272 480 100 1024 1 name:$(top)debugC
use ecad2 -160 839 100 0 ecad2#48
xform 0 0 1152
p -96 800 100 0 1 INAM:flamIsNullInit
p -96 752 100 0 1 SNAM:flamIsDebugProcess
p 0 832 100 1024 1 name:$(top)debug
p 176 928 75 1024 -1 pproc(STLK):PP
use efanouts 336 791 100 0 efanouts#13
xform 0 456 944
p 480 784 100 1024 1 name:$(top)debugFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamDebug.sch,v 0.0 2005/09/01 20:20:10 drashkin Exp $
