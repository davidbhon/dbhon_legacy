#if !defined(__UFClientAgent_c__)
#define __UFClientAgent_c__ "$Name:  $ $Id: ufClientAgent.c,v 0.0 2005/09/01 20:24:46 drashkin Exp $"
static const char rcsIdUFClientAgent[] = __UFClientAgent_c__;

#include "ufClient.h"
__UFClient_H__ (ClientAgent_c)
#include "ufLog.h"
  __UFLog_H__ (ClientAgent_c)
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
#define  MAXNAMLEN  512
#else
#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "inttypes.h"
#include "math.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "string.h"
#endif
#define _UFStandardAgentList_ 9
#define _UFStandardAnnextPorts_ 8

/* global socket file descriptor for frame/image server */
     static int _theUFFrameSockFd = 0;

/* global socket file descriptor for command/sequence server (agent) */
     static int _theUFCmdSockFd = 0;

/* global socket file descriptor for status/alarms server (agent) */
     static int _theUFStatSockFd = 0;

/* global socket file descriptor for telescope server (agent) */
     static int _theUFTeleSockFd = 0;

/* global socket file descriptor for mce4 detector agent */
     static int _theUFMCE4AgentSockFd = 0;

/* global socket file descriptor for motors agent */
     static int _theUFPortescapAgentSockFd = 0;

/* global socket file descriptor for temperature agent */
     static int _theUFLakeshoreAgentSockFd = 0;

/* global socket file descriptor for vacuum agent */
     static int _theUFGranPhilAgentSockFd = 0;

/* global socket file descriptor for barcode agent */
     static int _theUFBarCodeAgentSockFd = 0;

/* pointers to all open socket Fds */
     static int *_theAgentSockFdVec[_UFStandardAgentList_];

     static const char *_theAgentNames[_UFStandardAgentList_] =
       { "frame", "status", "command", "telescope", "MCE4",
       "Portescap", "LakeShore", "GranPhil", "BarCode"
     };

/******************************** agent/service info *************************/
     int ufServices (char **infolist)
{
  static char thelist[] = "frame@newton:128.227.184.138~5557, "	/* 1 
									 */
    "status@newton:128.227.184.138~55014, "	/* 2 */
    "command@newton:128.227.184.138~55013, "	/* 3 */
    "telescope@newton:128.227.184.138~55012, "	/* 4 */
    "MCE4@newton:128.227.184.138~55008, "	/* 5 */
    "PortescapMotors@newton:128.227.184.138~55007, "	/* 6 
							 */
    "LakeShore@newton:128.227.184.138~55006, "	/* 7 */
    "GranPhil@newton:128.227.184.138~55005, "	/* 8 */
    "BarCode@newton:128.227.184.138~55004, "	/* 9 */
    "MCE4@annex:192.168.111.101~7008, "	/* 10 */
    "PortescapMotors@annex:192.168.111.101~7007, "	/* 11 
							 */
    "LakeShore@annex:192.168.111.101~7006, "	/* 12 */
    "GranPhil@annex:192.168.111.101~7005, "	/* 13 */
    "BarCode@annex:192.168.111.101~7004, "	/* 14 */
    "Telescope@annex:192.168.111.101~7003, "	/* 15 */
    "Aux2@annex:192.168.111.101~7002, "	/* 16 */
    "Aux1@annex:192.168.111.101~7001";	/* 17 */

  *infolist = thelist;
  return _UFStandardAgentList_ + _UFStandardAnnextPorts_;
}

int
ufCloseAgent (const char *agent)
{
  int status, i = _UFStandardAgentList_;

  while (--i >= 0)
    {
      if (strcmp (agent, _theAgentNames[i]) == 0
	  && *_theAgentSockFdVec[i] > 0)
	{
	  sprintf (_UFerrmsg, "ufCloseAgent> closing agent: %s.", agent);
	  ufLog (_UFerrmsg);
	  status = close (*_theAgentSockFdVec[i]);
	  *_theAgentSockFdVec[i] = 0;
	  return status;
	}
    }
  sprintf (_UFerrmsg, "ufCloseAgent> unknown agent name= %s.", agent);
  ufLog (_UFerrmsg);

  return -1;
}

int
ufConnectAgent (const char *agent, const char *host, int portNo)
{
  int *socFd = 0;
  int i = _UFStandardAgentList_;

  if (_theAgentSockFdVec[0] == 0)
    {				/* first-time init: */
      _theAgentSockFdVec[0] = &_theUFFrameSockFd;
      _theAgentSockFdVec[1] = &_theUFCmdSockFd;
      _theAgentSockFdVec[2] = &_theUFStatSockFd;
      _theAgentSockFdVec[3] = &_theUFTeleSockFd;
      _theAgentSockFdVec[4] = &_theUFMCE4AgentSockFd;
      _theAgentSockFdVec[5] = &_theUFPortescapAgentSockFd;
      _theAgentSockFdVec[6] = &_theUFLakeshoreAgentSockFd;
      _theAgentSockFdVec[7] = &_theUFGranPhilAgentSockFd;
      _theAgentSockFdVec[8] = &_theUFBarCodeAgentSockFd;
    }

  while (--i >= 0)
    {
      if (strcmp (agent, _theAgentNames[i]) == 0)
	{
	  socFd = _theAgentSockFdVec[i];
	  if (*socFd > 0)
	    {
	      sprintf (_UFerrmsg,
		       "ufConnectAgent> already connected to agent: %s, on socFd=%d",
		       agent, *socFd);
	      ufLog (_UFerrmsg);
	      return *socFd;
	    }
	  *socFd = ufConnect (host, portNo);
	  if (*socFd <= 0)
	    sprintf (_UFerrmsg, "ufConnectAgent> Failed connect to agent: %s",
		     agent);
	  else
	    sprintf (_UFerrmsg, "ufConnectAgent> connected to agent: %s.",
		     agent);
	  ufLog (_UFerrmsg);
	  return *socFd;
	}
    }

  sprintf (_UFerrmsg, "ufConnectAgent> unknown agent name= %s.", agent);
  ufLog (_UFerrmsg);
  return -1;
}

int
ufAgentSocket (const char *agent)	/* return socket Fd 
					   of an agent */
{
  int *socFd = 0;
  int i = _UFStandardAgentList_;

  while (--i >= 0)
    {
      if (strcmp (agent, _theAgentNames[i]) == 0)
	{
	  socFd = _theAgentSockFdVec[i];
	  return (*socFd);
	}
    }
  sprintf (_UFerrmsg, "ufAgentSocket> unknown agent name= %s.", agent);
  ufLog (_UFerrmsg);
  return -1;
}

#endif /* __UFClientAgent_c__ */
