#if !defined(__UFDBL_H__)
#define __UFDBL_H__ "$Name:  $ $Id: ufdbl.h,v 0.0 2005/09/01 20:24:46 drashkin Exp $"

#include "genSubRecord.h"
#include "cadRecord.h"
#include "sirRecord.h"

/* uf modification of "dbl" func. in epics 3.12Gem */
extern long ufdbl (char *precdesname);

/* latif stuff: */
extern long ufHeartbeat ();
extern long ufuptime ();
extern long ufmemShow ();
extern long ufAlive ();

/* more (hon) stuff: */
extern long ufdbStrGet (char *name, char *str);
extern long ufdbStrTransfer (char *input, char *output);
extern const char *ufAgentHostIP ();
extern char *ufdbName ();

/* dan stuff */

#if !defined(HAVE_BOOL) && !defined(__cplusplus)
#define HAVE_BOOL
typedef enum
{ false, true }
bool;
#endif

/**
 * Verifies validity of the given genSubRecord, and optionally
 * outputs debugging information.
 */
bool debugPGS (const char *funcName, const genSubRecord * pgs);
bool debugCAD (const char* funcName, const cadRecord* pcr ) ;
/**
 * Returns true if a < b, false otherwise.
 */
bool
ufmin2i (int a, int b)
{
  return ((a < b) ? a : b);
}

bool
status_changed (const char *current, int last) ;

/**
 * Dump a record's fields to the stdout.
 * If useVal is true, then print the recordPtr->val[a..z], otherwise
 * recordPtr->[a..z]
 */
void dumpPGS( const char* funcName, const genSubRecord* pgs, bool useVal ) ;
void dumpPCR( const char* funcName, const cadRecord* pcr, bool useVal );

/* more (hon) stuff: */
extern double ufGetWaveLength();
extern void ufSetCentralWavelength(const char* w);
extern void ufSetWaveLength(const sirRecord* sPtr);
extern double ufGratingOffsetCalc();
extern bool ufdbStrPut( char* recName, char* val );
extern double ufGetFiducialAngle();
extern double ufGetFiducialSteps();
extern void ufStoreAdjWaveLength(double val);
extern int ufClearDatumCnt(char* id);
extern int ufGetDatumCnt(char* id);
extern int ufDatumCnt(char* id);
extern void ufSetDatumCnt(const sirRecord* sPtr);

#endif /* __UFDBL_H__ */
