#if !defined(__UFGEMGENSUBCOMMDC_C__)
#define __UFGEMGENSUBCOMMDC_C__ "RCS: $Name:  $ $Id: ufGemGensubCommDC.c,v 0.0 2005/09/01 20:24:46 drashkin Exp $"
static const char rcsIdufGEMGENSUBCOMMCDC[] = __UFGEMGENSUBCOMMDC_C__ ;

#include <stdioLib.h>
#include <string.h>
#include <stdio.h>
#include <sysLib.h>

/* use select (checkSoc) on socfd -- hon */
#include <sockLib.h>

#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
/* #include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>


#include <dbStaticLib.h> 
#include <dbBase.h> 

#include "flam.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"
#include "ufClient.h"
#include "ufLog.h"
#include "ufdbl.h"

static int _verbose = 1;

static char dc_DEBUG_MODE[6]={"FULL"} ;
static char dc_STARTUP_DEBUG_MODE[6]={"FULL"} ;
static int dc_initialized  = 0 ;

static cloud_cover_element cloud_cover_table[4] ;
static water_vapour_element water_vapour_table[4];
static double fiducial_frame_time ;

static char dc_host_ip[20] ;

static char adjPhysParm[25][40] ;
static char meta_phys_parm[25][40] ;
static char phys_phys_parm[25][40] ;
static char hrdware_parm[12][40] ;
static char adjMetaParm[28][40] ;

static double dc_bias_M[NVBIAS] ;
static double dc_bias_volt[NVBIAS];
static long dc_bias_B[NVBIAS] ;
static long dc_bias_control[NVBIAS] ;
static long dc_bias_def[NVBIAS] ;
static long dc_bias_min[NVBIAS] ;
static long dc_bias_max[NVBIAS];
static double dc_env_param[8] ;

static double preAmp_M[NVPAMP] ;
static double preAmp_volt[NVPAMP];
static long preAmp_B[NVPAMP] ;
static long preAmp_default[NVPAMP] ;
static long preAmp_control[NVPAMP] ;

static long Num_Readout_Modes ;

/* Array to hold the readout mode names */

static char **Readout_Modes ;

/* Look up tables for K, L, M filters and Spectroscopy */

static double *K_Filter ;
static double *L_Filter ;
static double *M_Filter ;
static double *LoRes10_Grating ;
static double *HiRes10_Grating ;
static double *LoRes20_Grating ;

/********************** DC  CODING ************************/

void init_dc_config () {
  FILE *dcFile = 0 ;
  char in_str[80] ;
  char *temp_str ;

  dcFile = fopen( dcconfig_filename, "r" ) ;
  if( NULL == dcFile )
    {
      sprintf(_UFerrmsg, __HERE__ "> Unable to open file:%s",dcconfig_filename);
      ufLog( _UFerrmsg ) ;
      /* RRO
      strcpy(dc_STARTUP_DEBUG_MODE,"NONE") ;
      strcpy(dc_DEBUG_MODE,"NONE") ;
      */
      strcpy(dc_STARTUP_DEBUG_MODE,"FULL") ;
      strcpy(dc_DEBUG_MODE,"FULL") ;
    }
  else
    {
      NumFiles++ ;
      fgets(in_str,40,dcFile);
      fgets(in_str,40,dcFile);
      temp_str = strchr(in_str,'\n') ;
      if (temp_str != NULL) temp_str[0] = '\0' ;
      strcpy(dc_STARTUP_DEBUG_MODE,in_str) ;
      fclose(dcFile) ;
      NumFiles-- ;

      if ( (strcmp(dc_STARTUP_DEBUG_MODE,"NONE") != 0) ||
	   (strcmp(dc_STARTUP_DEBUG_MODE,"MIN") != 0) ||
	   (strcmp(dc_STARTUP_DEBUG_MODE,"FULL") != 0) ) 
	strcpy(dc_STARTUP_DEBUG_MODE,"FULL") ;
    
      strcpy(dc_DEBUG_MODE,"FULL") ;
    }

  dc_initialized  = 1 ;
  fprintf( stderr, __HERE__ "> DC Initialized\n" ) ;
}

long ufdcStatusGInit (genSubRecord *gsp) {
  size_t i = 0;
  int *last_command = 0;

  debugPGS( __HERE__, gsp ) ;

  strcpy(gsp->a, "") ;
  strcpy(gsp->b, "") ;
  strcpy(gsp->c, "") ;
  strcpy(gsp->d, "") ;

  last_command = (int *) malloc (sizeof(int)*2);
  for (i = 0; i<=1;i++)
    {
      last_command[i] = CAR_IDLE ;
    }
  strcpy(gsp->valc,"IDLE") ;
  strcpy(gsp->vald,"GOOD") ;

  gsp->dpvt = (void *) last_command ;

  return OK ;
} 

/**********************************************************/
long ufdcStatusGProc (genSubRecord *gsp) {
  long status = 0 ;
  static char out_message[40];
  long action = CAR_IDLE;
  int *last_command ;

  debugPGS( __HERE__, gsp ) ;

  strcpy(out_message, "") ;
   
  if ( (strcmp(gsp->a,"ERR") == 0) ||  (strcmp(gsp->c,"ERR") == 0) ) 
    action = CAR_ERROR ;

  if (action == CAR_IDLE) {
    if ( (strcmp(gsp->a,"BUSY") == 0) ||  (strcmp(gsp->c,"BUSY") == 0) ) 
      action = CAR_BUSY ;
  } 
  
  /*Let's figure out the state of the system and its health */
  if (action == CAR_IDLE) {
    strcpy(gsp->vald,"IDLE") ;
    strcpy(gsp->valc,"GOOD") ;
  } else {
    if (action == CAR_BUSY) {
      strcpy(gsp->vald,"BUSY") ;
      strcpy(gsp->valc,"GOOD") ;
    } else {
      strcpy(gsp->vald,"ERROR") ;
      strcpy(gsp->valc,"BAD") ;
    }
  }
  /* Let's see if we can figure out the status of the last command */
  /* first let's find out which one changed */

  last_command = (int *) gsp->dpvt ;
  
  if (status_changed (gsp->a, last_command[0])) {
    if (strcmp(gsp->a,"IDLE") == 0) {
      action = CAR_IDLE ;
      last_command[0] = CAR_IDLE ;
    } else {
      if (strcmp(gsp->a,"BUSY") == 0) {
        action = CAR_BUSY ;
        last_command[0] = CAR_BUSY ;
      } else {
        action = CAR_ERROR ;
        last_command[0] = CAR_ERROR ;
        strcpy(out_message,gsp->b) ;
        /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
        if (*(long *)gsp->valb == CAR_IDLE) {

        }
      }
    }
  } else { /* Well link 'A' did not change, let keep going */
    if (status_changed (gsp->c, last_command[1])) {
      if (strcmp(gsp->c,"IDLE") == 0) {
        action = CAR_IDLE ;
        last_command[1] = CAR_IDLE ;
      } else {
        if (strcmp(gsp->c,"BUSY") == 0) {
          action = CAR_BUSY ;
          last_command[1] = CAR_BUSY ;
        } else {
          action = CAR_ERROR ;
          last_command[1] = CAR_ERROR ;
          strcpy(out_message,gsp->d) ;
          /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
          if (*(long *)gsp->valb == CAR_IDLE) {
          
          }
        }
      }
    } 
  }
  
  strcpy(gsp->vala,out_message); 
  *(long *)gsp->valb = action; 
 
  return status ;
} 

/******************** INAM for adjHrdwrG ******************/
long ufadjHrdwrGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy(pgs->a,"") ;
  pgs->noa = 12 ;
  return OK ;
}

/******************** SNAM for adjHrdwrG ******************/
long ufadjHrdwrGproc (genSubRecord *pgs)
{
  char yy[40] ; 
  char *original ;
  char *zz ;

  debugPGS( __HERE__, pgs ) ;

  if (strcmp(pgs->a,"") != 0) {
    /* status = unpack_any_array (pgs, (char *)pgs->a, pgs->noa,remainder) ; */
  
    original = pgs->a ; zz = pgs->a ; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 5 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 8 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 9 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valj,yy) ; /* 10 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valk,yy) ; /* 11 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 */

    strcpy(pgs->valm, "1") ;
    strcpy(pgs->valn, "START") ;
    strcpy(pgs->a,"") ;
    pgs->noa = 12 ;
  } else {
    strcpy(pgs->valn, "CLEAR") ;
  }
  return OK ;
}

/* function to read the det.control params file: returns port # */

long read_dc_file(char *rec_name, genSubCommPrivate *pPriv, char paramLevel)
{
  FILE *dcFile = 0;
  char in_str[80] ;
  double numnum ;
  int i;
  int done  ;
  char *temp_str ;
  long dc_port_no = 0;

/*  ufLog( __HERE__ ) ; */

  dcFile = fopen( dc_param_filename, "r" ) ;
  if( NULL == dcFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s", dc_param_filename ) ;
      ufLog( _UFerrmsg ) ;
      return -1 ;
    }

  /* Read the host IP number */
  NumFiles++ ;

  fgets(in_str,80,dcFile);  /* comment line */
  fgets(in_str,80,dcFile);  /* comment line */
  fgets(in_str,80,dcFile);  /* comment line */
  fgets(in_str,40,dcFile) ; /* IP number */

  strcpy (dc_host_ip,in_str) ;
  i = 0;
  while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
  dc_host_ip[i] = '\0' ;

     /* Read the port number */
  fgets(in_str,80,dcFile); /* comment line */ 
  fscanf(dcFile,"%ld",&dc_port_no) ; /* port number */

  sprintf(_UFerrmsg, __HERE__ "> host IP addr: %s,  port # =%ld", dc_host_ip, dc_port_no);
  ufLog( _UFerrmsg ) ;

  /* see if this is ObsControl and read the look up tables for sky transparency */
  if (paramLevel == 'M') {
    fgets(in_str,80,dcFile);  /* read till the end of line */
    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i = 0; i < 4; i++) {
      fscanf(dcFile,"%lf %lf",
	     &cloud_cover_table[i].percentile,
	     &cloud_cover_table[i].chop_freq) ;
      fgets(in_str,40,dcFile);  /* read the condition */
      /* trim leading spaces */
      temp_str = strchr(in_str,' ') ;
      done = 0;
      while ((temp_str[0] == ' ') && (!done)) {
	if (temp_str == in_str) {
	  strcpy(in_str, temp_str+1);
	} else {
	  done = 1;
	  /* if (temp_str != NULL) temp_str[0] = '\0' ; */
	}
	temp_str = strchr(in_str,' ') ;
      }
      /* trim trailing spaces */
      temp_str = in_str + strlen(in_str) - 1 ;
      done = 0 ;
      while (isspace(temp_str[0]) && (!done)) {
	temp_str[0] = '\0' ;
	if(!isspace(in_str[strlen(in_str)-1]))   done = 1;
	temp_str = in_str + strlen(in_str) - 1 ;
      }
      strcpy(cloud_cover_table[i].conditions, in_str) ;
      /*printf("%f  %f  @%s@\n",cloud_cover_table[i].chop_freq, cloud_cover_table[i].percentile,
	cloud_cover_table[i].conditions) ; */
    }
    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i = 0; i < 4; i++) {
      fscanf(dcFile,"%lf  %lf",&water_vapour_table[i].percentile,
	     &water_vapour_table[i].conditions) ;
      fgets(in_str,80,dcFile); /* read till the end of the line */
    }
    fgets(in_str,80,dcFile); /* comment line */ 
    fscanf(dcFile,"%lf",&numnum) ;
    fiducial_frame_time = numnum ;
    fgets(in_str,80,dcFile); /* read till the end of the line */
    /* printf("%f\n",fiducial_frame_time) ; */
    /* Read the number of readout modes */
    fgets(in_str,80,dcFile); /* comment line */ 
    fscanf(dcFile,"%lf",&numnum) ;
    Num_Readout_Modes = (long) numnum ;
    fgets(in_str,80,dcFile); /* read till the end of the line */
    /* allocate the necessary memory */
    Readout_Modes = malloc(Num_Readout_Modes*sizeof(char *)) ;
    for (i=0;i<Num_Readout_Modes;i++) Readout_Modes[i] = malloc(10*sizeof(char)) ;

    K_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
    L_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
    M_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
    LoRes10_Grating = malloc(Num_Readout_Modes*sizeof(double)) ;
    HiRes10_Grating = malloc(Num_Readout_Modes*sizeof(double)) ;
    LoRes20_Grating = malloc(Num_Readout_Modes*sizeof(double)) ;

    /* Read the readout Modes */
    fgets(in_str,80,dcFile); /* comment line */ 

    for (i=0;i<Num_Readout_Modes;i++) {
      fgets(in_str,40,dcFile);   /* read the readout mode */
      /* trim leading spaces */		
      done = 0;
      temp_str = strchr(in_str,' ') ;
      while ((temp_str[0] == ' ') && (!done)) {
	if (temp_str == in_str) {
	  strcpy(in_str, temp_str+1);
	} else {
	  done = 1; 
           
	}
	temp_str = strchr(in_str,' ') ;
      } 
      /* trim trailing spaces */
      temp_str = in_str + strlen(in_str) - 1 ;
      done = 0 ;
      while (isspace(temp_str[0]) && (!done)) {
	temp_str[0] = '\0' ;
	if(!isspace(in_str[strlen(in_str)-1]))   done = 1;
	temp_str = in_str + strlen(in_str) - 1 ;
      }  
        
      while ((temp_str != NULL) && (!done)) { 
	if (temp_str == in_str) {  
	  strcpy(in_str, temp_str+1); 
	} else {  
	  done = 1; 
	  if (temp_str != NULL) temp_str[0] = '\0' ; 
	} 
	temp_str = strchr(in_str,' ') ; 
      } 
        
      strcpy(Readout_Modes[i], in_str) ;
    }

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      K_Filter[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      L_Filter[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      M_Filter[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      LoRes10_Grating[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      HiRes10_Grating[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 

    fgets(in_str,80,dcFile); /* comment line */ 
    fgets(in_str,80,dcFile); /* comment line */ 
    for (i=0;i<Num_Readout_Modes;i++) {
      fscanf(dcFile,"%lf",&numnum) ;
      LoRes20_Grating[i] = numnum ;
    }
    fgets(in_str,80,dcFile); /* read until the end of the line */ 
  }
  fclose(dcFile) ; 
  NumFiles-- ;

  return dc_port_no;
}

/******************** INAM for hardwareG *******************/
long ufhardwareGinit (genSubRecord *pgs)
{
  /* Gensub Private Structure Declaration */
  genSubDCHrdwrPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;

  debugPGS( __HERE__, pgs ) ;

  /* Create a private control structure for this gensub record */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCHrdwrPrivate *) malloc (sizeof(genSubDCHrdwrPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* Locate the associated CAR record fields and save addresses in the private structure */
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  pPriv->CurrParam.FrmCoadds= -1 ;
  pPriv->CurrParam.ChpSettleReads= -1 ;
  pPriv->CurrParam.ChpCoadds= -1 ;
  pPriv->CurrParam.Savesets= -1 ;
  pPriv->CurrParam.NodSettleReads= -1 ;
  pPriv->CurrParam.NodSets= -1 ;
  pPriv->CurrParam.NodSettleChops= -1 ;
  pPriv->CurrParam.PreValidChops= -1 ;
  pPriv->CurrParam.PostValidChops= -1 ;
  pPriv->CurrParam.pixclock= -1 ;
  strcpy(pPriv->CurrParam.obs_mode,"") ;
  strcpy(pPriv->CurrParam.readout_mode,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /*  */
  *(long *)pgs->b = -1 ; /*  */
  *(long *)pgs->c = -1 ; /*  */
  *(long *)pgs->d = -1 ; /*  */
  *(long *)pgs->e = -1 ; /*  */
  *(long *)pgs->f = -1 ; /*  */
  *(long *)pgs->g = -1 ; /*  */
  *(long *)pgs->h = -1 ; /*  */
  *(long *)pgs->i = -1 ; /*  */
  *(long *)pgs->k = -1 ; /*  */
  *(long *)pgs->l = -1 ; /*  */
  strcpy(pgs->j,"") ;
  strcpy(pgs->m,"") ; /*  */
  strcpy(pgs->n,"") ; /*  */
  *(long *)pgs->o = -1 ; /*  */
  *(long *)pgs->p = -1 ; /*  */
  *(long *)pgs->q = -1 ; /*  */
  *(long *)pgs->r = -1 ; /*  */
  *(long *)pgs->s = -1 ; /*  */
  *(long *)pgs->t = -1 ; /*  */
  *(long *)pgs->u = -1 ; /*  */

  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->dcHrdwr_inp.command_mode = -1  ;
  pPriv->dcHrdwr_inp.FrmCoadds = -1  ;
  pPriv->dcHrdwr_inp.ChpSettleReads = -1  ;
  pPriv->dcHrdwr_inp.ChpCoadds = -1  ;
  pPriv->dcHrdwr_inp.Savesets = -1  ;
  pPriv->dcHrdwr_inp.NodSettleReads = -1  ;
  pPriv->dcHrdwr_inp.NodSets = -1  ;
  pPriv->dcHrdwr_inp.NodSettleChops = -1  ;
  pPriv->dcHrdwr_inp.PreValidChops = -1  ;
  pPriv->dcHrdwr_inp.PostValidChops = -1  ;
  pPriv->dcHrdwr_inp.pixclock = -1  ;
  strcpy(pPriv->dcHrdwr_inp.readout_mode,"") ;
  strcpy(pPriv->dcHrdwr_inp.obs_mode,"") ;
  pPriv->dcHrdwr_inp.hFlag = ADJUSTED_INPUT ;
  

  pPriv->paramLevel = 'H' ;
  pgs->dpvt = (void *) pPriv;

  /* And do some last minute initialization stuff */
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/******************** SNAM for hardwareG *******************/
long ufhardwareGproc (genSubRecord *pgs)
{
  genSubDCHrdwrPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 
  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;

  debugPGS( __HERE__, pgs ) ;

  strcpy(dc_DEBUG_MODE,pgs->m) ; 
/*  strcpy(dc_DEBUG_MODE,"FULL") ; */

  pPriv = (genSubDCHrdwrPrivate *)pgs->dpvt ;

  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      if (flam_initialized != 1)  init_flam_config() ;
      if (!dc_initialized)  init_dc_config() ;

      /* Clear outpus A-J for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      *(long *)pgs->valc = -1 ;
      *(long *)pgs->vald = -1 ;
      *(long *)pgs->vale = -1 ;
      *(long *)pgs->valf = -1 ;
      *(long *)pgs->valg = -1 ;
      *(long *)pgs->valh = -1 ;
      *(long *)pgs->vali = -1 ;
      *(long *)pgs->valj = -1 ;
      *(long *)pgs->valk = -1 ;
      *(long *)pgs->vall = -1 ;
      strcpy(pgs->valm,"") ;
      strcpy(pgs->valn,"") ;

      trx_debug("Attempting to establish a connection to the agent",
		pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      pPriv->socfd = -1 ; 
      *(long *)pgs->valb = (long)pPriv->socfd ;   /* current socket number */  

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to change indicating that a
     *  new command has arrived. Status and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;

      pPriv->dcHrdwr_inp.command_mode   = *(long *)pgs->a  ;
      pPriv->dcHrdwr_inp.FrmCoadds      = *(long *)pgs->b  ;
      pPriv->dcHrdwr_inp.ChpSettleReads = *(long *)pgs->c  ;
      pPriv->dcHrdwr_inp.ChpCoadds      = *(long *)pgs->d  ;
      pPriv->dcHrdwr_inp.NodSettleChops = *(long *)pgs->e  ;
      pPriv->dcHrdwr_inp.NodSettleReads = *(long *)pgs->f  ;
      pPriv->dcHrdwr_inp.Savesets       = *(long *)pgs->g  ;
      pPriv->dcHrdwr_inp.NodSets        = *(long *)pgs->h  ;
      pPriv->dcHrdwr_inp.pixclock       = *(long *)pgs->i  ;
      pPriv->dcHrdwr_inp.PreValidChops  = *(long *)pgs->k  ; 
      pPriv->dcHrdwr_inp.PostValidChops = *(long *)pgs->l  ;
      strcpy(pPriv->dcHrdwr_inp.obs_mode,pgs->m) ;
      strcpy(pPriv->dcHrdwr_inp.readout_mode, pgs->n) ;
      pPriv->dcHrdwr_inp.hFlag = *(long *)pgs->o  ;

      if ( (pPriv->dcHrdwr_inp.command_mode != -1) ||
	   (pPriv->dcHrdwr_inp.FrmCoadds != -1) ||
	   (pPriv->dcHrdwr_inp.ChpSettleReads != -1) ||
	   (pPriv->dcHrdwr_inp.ChpCoadds != -1) ||
	   (pPriv->dcHrdwr_inp.Savesets != -1) ||
	   (pPriv->dcHrdwr_inp.NodSettleReads != -1) ||
	   (pPriv->dcHrdwr_inp.NodSets != -1) ||
	   (pPriv->dcHrdwr_inp.NodSettleChops != -1) ||
	   (pPriv->dcHrdwr_inp.PreValidChops != -1) ||
	   (pPriv->dcHrdwr_inp.PostValidChops != -1) ||
	   (pPriv->dcHrdwr_inp.pixclock  != -1) ||
           (strcmp(pPriv->dcHrdwr_inp.obs_mode,"") != 0) ||
           (strcmp(pPriv->dcHrdwr_inp.readout_mode,"") != 0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        *(long *)pgs->a = -1 ; /*  */
        *(long *)pgs->b = -1 ; /*  */
        *(long *)pgs->c = -1 ; /*  */
        *(long *)pgs->d = -1 ; /*  */
        *(long *)pgs->e = -1 ; /*  */
        *(long *)pgs->f = -1 ; /*  */
        *(long *)pgs->g = -1 ; /*  */
        *(long *)pgs->h = -1 ; /*  */
        *(long *)pgs->i = -1 ; /*  */
        *(long *)pgs->k = -1 ; /*  */
        *(long *)pgs->l = -1 ; /*  */
        strcpy(pgs->m,"") ;
        strcpy(pgs->n,"") ;

      } else { /* Check to see if the agent response input has changed. */

        strcpy(response,pgs->j) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J field from the Agent
	   * unless the agent is late with something.
	   * So Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the GENSUB.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed. */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;

      /* Is this a CALLBACK to send a command or do an INIT or process adjusted input? */
      if (pPriv->dcHrdwr_inp.hFlag == 1) {
        /* get the input into the current parameters */
        if (pPriv->dcHrdwr_inp.FrmCoadds != -1)  
          pPriv->CurrParam.FrmCoadds = pPriv->dcHrdwr_inp.FrmCoadds  ;
        if (pPriv->dcHrdwr_inp.ChpSettleReads != -1) 
          pPriv->CurrParam.ChpSettleReads = pPriv->dcHrdwr_inp.ChpSettleReads; 
        if (pPriv->dcHrdwr_inp.ChpCoadds != -1) 
          pPriv->CurrParam.ChpCoadds = pPriv->dcHrdwr_inp.ChpCoadds ;
        if (pPriv->dcHrdwr_inp.Savesets != -1) 
          pPriv->CurrParam.Savesets = pPriv->dcHrdwr_inp.Savesets ;
        if (pPriv->dcHrdwr_inp.NodSettleReads != -1) 
          pPriv->CurrParam.NodSettleReads = pPriv->dcHrdwr_inp.NodSettleReads ;
        if (pPriv->dcHrdwr_inp.NodSets != -1) 
          pPriv->CurrParam.NodSets = pPriv->dcHrdwr_inp.NodSets ;
        if (pPriv->dcHrdwr_inp.NodSettleChops != -1) 
          pPriv->CurrParam.NodSettleChops = pPriv->dcHrdwr_inp.NodSettleChops ;
        if (pPriv->dcHrdwr_inp.PreValidChops != -1) 
          pPriv->CurrParam.PreValidChops = pPriv->dcHrdwr_inp.PreValidChops ;
        if (pPriv->dcHrdwr_inp.PostValidChops != -1) 
          pPriv->CurrParam.PostValidChops = pPriv->dcHrdwr_inp.PostValidChops ;
        if (pPriv->dcHrdwr_inp.pixclock != -1) 
          pPriv->CurrParam.pixclock = pPriv->dcHrdwr_inp.pixclock ;
        if(strcmp(pPriv->dcHrdwr_inp.obs_mode,"") != 0)
          strcpy(pPriv->CurrParam.obs_mode,pPriv->dcHrdwr_inp.obs_mode) ;
        if(strcmp(pPriv->dcHrdwr_inp.readout_mode,"") != 0)
          strcpy(pPriv->CurrParam.readout_mode,pPriv->dcHrdwr_inp.readout_mode) ;


        *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
        *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
        *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
        *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
        *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
        *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
        *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
        *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
        *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
        *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
        strcpy(pgs->valm,pPriv->CurrParam.obs_mode) ;
        strcpy(pgs->valn,pPriv->CurrParam.readout_mode) ;

        /* Reset Command state to DONE */
        pPriv->commandState = TRX_GS_DONE;
        some_num = CAR_IDLE ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
      }
      else {    /* Is this a CALLBACK to send a command or do an INIT? */
        if (pPriv->dcHrdwr_inp.command_mode != -1) { /* We have an init command */
          trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Check first to see if we have a valid INIT directive */
          if ((pPriv->dcHrdwr_inp.command_mode == SIMM_NONE) ||
              (pPriv->dcHrdwr_inp.command_mode == SIMM_FAST) ||
              (pPriv->dcHrdwr_inp.command_mode == SIMM_FULL) ) {
            /* need to reconnect to the agent ? -- hon */
	    if( pPriv->socfd >= 0 ) {
	      if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	        ufLog("ufhardwareGproc> socket is writable, no need to reconnect...\n");
	      }
	      else {
                trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
                ufClose(pPriv->socfd) ;
                pPriv->socfd = -1;
                *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
	    }
            pPriv->CurrParam.command_mode = pPriv->dcHrdwr_inp.command_mode; /* simulation level */
            *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
            /* Re-Connect if needed */
            /* Read some initial parameters from a file */

            pPriv->port_no = read_dc_file( pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel );

            if (pPriv->dcHrdwr_inp.command_mode != SIMM_FAST) {
	      if( pPriv->socfd < 0 ) {
                trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
                pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no, dc_host_ip );
                trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
 	        *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
              if (pPriv->socfd < 0) { /* we have a bad socket connection */
                pPriv->commandState = TRX_GS_DONE;
                strcpy (pPriv->errorMessage, "Error Connecting to Agent");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            }
            /* update output links if connection is good or we are in SIMM_FAST*/
            if ( (pPriv->dcHrdwr_inp.command_mode == SIMM_FAST) ||
                 (pPriv->socfd > 0) ) {
              /* update the output links */

              *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
              *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
              *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
              *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
              *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
              *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
              *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
              *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
              *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
              *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
              strcpy(pgs->valm,pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valn,pPriv->CurrParam.readout_mode) ;

              /* Reset Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
              if (status) {
                trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              } 
            }
	  } else { /* we have an error in input of the init command */
            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "Bad INIT Directive DC 1");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
        } else { /* We have a configuration command */
          trx_debug("We have a configuration Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Save the current parameters */
 
          pPriv->OldParam.FrmCoadds      = pPriv->CurrParam.FrmCoadds ;
          pPriv->OldParam.ChpSettleReads = pPriv->CurrParam.ChpSettleReads ;
          pPriv->OldParam.ChpCoadds      = pPriv->CurrParam.ChpCoadds ;
          pPriv->OldParam.Savesets       = pPriv->CurrParam.Savesets ;
          pPriv->OldParam.NodSettleReads = pPriv->CurrParam.NodSettleReads ;
          pPriv->OldParam.NodSets        = pPriv->CurrParam.NodSets ;
          pPriv->OldParam.NodSettleChops = pPriv->CurrParam.NodSettleChops ;
          pPriv->OldParam.PreValidChops  = pPriv->CurrParam.PreValidChops ;
          pPriv->OldParam.PostValidChops = pPriv->CurrParam.PostValidChops ;
          pPriv->OldParam.pixclock       = pPriv->CurrParam.pixclock ;
          strcpy(pPriv->OldParam.obs_mode,pPriv->CurrParam.obs_mode) ;
          strcpy(pPriv->OldParam.readout_mode,pPriv->CurrParam.readout_mode) ;

          /* get the input into the current parameters */
          if (pPriv->dcHrdwr_inp.FrmCoadds != -1)  
            pPriv->CurrParam.FrmCoadds = pPriv->dcHrdwr_inp.FrmCoadds  ;
          if (pPriv->dcHrdwr_inp.ChpSettleReads != -1) 
            pPriv->CurrParam.ChpSettleReads = pPriv->dcHrdwr_inp.ChpSettleReads; 
          if (pPriv->dcHrdwr_inp.ChpCoadds != -1) 
            pPriv->CurrParam.ChpCoadds = pPriv->dcHrdwr_inp.ChpCoadds ;
          if (pPriv->dcHrdwr_inp.Savesets != -1) 
            pPriv->CurrParam.Savesets = pPriv->dcHrdwr_inp.Savesets ;
          if (pPriv->dcHrdwr_inp.NodSettleReads != -1) 
            pPriv->CurrParam.NodSettleReads = pPriv->dcHrdwr_inp.NodSettleReads ;
          if (pPriv->dcHrdwr_inp.NodSets != -1) 
            pPriv->CurrParam.NodSets = pPriv->dcHrdwr_inp.NodSets ;
          if (pPriv->dcHrdwr_inp.NodSettleChops != -1) 
            pPriv->CurrParam.NodSettleChops = pPriv->dcHrdwr_inp.NodSettleChops ;
          if (pPriv->dcHrdwr_inp.PreValidChops != -1) 
            pPriv->CurrParam.PreValidChops = pPriv->dcHrdwr_inp.PreValidChops ;
          if (pPriv->dcHrdwr_inp.PostValidChops != -1) 
            pPriv->CurrParam.PostValidChops = pPriv->dcHrdwr_inp.PostValidChops ;
          if (pPriv->dcHrdwr_inp.pixclock != -1) 
            pPriv->CurrParam.pixclock = pPriv->dcHrdwr_inp.pixclock ;
          if(strcmp(pPriv->dcHrdwr_inp.obs_mode,"") != 0)
            strcpy(pPriv->CurrParam.obs_mode,pPriv->dcHrdwr_inp.obs_mode) ;
          if(strcmp(pPriv->dcHrdwr_inp.readout_mode,"") != 0)
            strcpy(pPriv->CurrParam.readout_mode,pPriv->dcHrdwr_inp.readout_mode) ;

          /* formulate the command strings */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;

          num_str = UFprocess_dc_Hrdwr_inputs( pPriv->dcHrdwr_inp,com, pPriv->paramLevel,
					       rec_name,&pPriv->CurrParam, 
					       pPriv->CurrParam.command_mode ) ;
          /* Clear the input structure */

          pPriv->dcHrdwr_inp.command_mode = -1  ;
          pPriv->dcHrdwr_inp.FrmCoadds = -1  ;
          pPriv->dcHrdwr_inp.ChpSettleReads = -1  ;
          pPriv->dcHrdwr_inp.ChpCoadds = -1  ;
          pPriv->dcHrdwr_inp.Savesets = -1  ;
          pPriv->dcHrdwr_inp.NodSettleReads = -1  ;
          pPriv->dcHrdwr_inp.NodSets = -1  ;
          pPriv->dcHrdwr_inp.NodSettleChops = -1  ;
          pPriv->dcHrdwr_inp.PreValidChops = -1  ;
          pPriv->dcHrdwr_inp.PostValidChops = -1  ;
          pPriv->dcHrdwr_inp.pixclock = -1  ;
          strcpy(pPriv->dcHrdwr_inp.readout_mode,"") ;
          strcpy(pPriv->dcHrdwr_inp.obs_mode,"") ;

          if (num_str > 0) {   /* we have commands to send... */

            for (i=0;i<num_str;i++) {
	      sprintf(_UFerrmsg, __HERE__ "> %s",com[i]);
	      ufLog( _UFerrmsg );
	    }

            /* if no Agent needed then go back to DONE */
            /* in the case of SIMM_FAST or commands that do not need the agent */

            if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* update the output links */

              *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
              *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
              *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
              *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
              *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
              *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
              *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
              *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
              *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
              *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
              strcpy(pgs->valm, pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valn, pPriv->CurrParam.readout_mode) ;
 
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, 
                                   DBR_LONG, &some_num, 1);
              if (status) {
                logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
                return OK;
              }
            }
	    else { /* we are in SIMM_NONE or SIMM_FULL */
              /* See if you can send the command and go to BUSY state */
              if (pPriv->socfd > 0) {
		if( _verbose )
		  {
		    sprintf(_UFerrmsg, __HERE__"> sending to agent, genSub record: %s", pgs->name); 
		  ufLog( _UFerrmsg ) ;
		  }
		status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
              }
	      else {
		sprintf( _UFerrmsg, __HERE__ "> send socfd bad? , genSub record: %s", pgs->name); 
		ufLog( _UFerrmsg ) ;
		status = 0;
	      }
	      
	      for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;

              if (status == 0) { /* there was an error sending the commands to the agent */
                /* set Command state to DONE */
                pPriv->commandState = TRX_GS_DONE;
                /* set the CAR to ERR with appropriate message like 
                 * "Bad socket"  and go to DONE state*/
                 strcpy (pPriv->errorMessage, "Bad socket connection DC1");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else { /* send successful */
                /* establish a CALLBACK */
                requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
                pPriv->startingTicks = tickGet() ;
                /* set Command state to BUSY */
                pPriv->commandState = TRX_GS_BUSY;
              }
            }
          }
	  else {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (num_str < 0) {
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* we have an error in the Input */
              strcpy (pPriv->errorMessage, "Error in input DC1");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* num_str == 0 */
              /* we have a change in input. The inputs got checked but no string got formulated. */
              trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
            }
          }
        }
      
        /*Is this a J field processing? */
        strcpy(response,pgs->j) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something.
	   * Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
      }
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;

      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) {
        trx_debug(response,pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        ufLog("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "DC H Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then cancel call back, go to DONE state and set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
        
          /* update the output links */
          *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
          *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
          *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
          *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
          *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
          *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
          *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
          *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
          *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
          *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
          strcpy(pgs->valm, pPriv->CurrParam.obs_mode) ;
          strcpy(pgs->valn, pPriv->CurrParam.readout_mode) ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;

          if (*endptr != '\0') { /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            pPriv->CurrParam.FrmCoadds      = pPriv->OldParam.FrmCoadds ;
            pPriv->CurrParam.ChpSettleReads = pPriv->OldParam.ChpSettleReads ;
            pPriv->CurrParam.ChpCoadds      = pPriv->OldParam.ChpCoadds ;
            pPriv->CurrParam.Savesets       = pPriv->OldParam.Savesets ;
            pPriv->CurrParam.NodSettleReads = pPriv->OldParam.NodSettleReads ;
            pPriv->CurrParam.NodSets        = pPriv->OldParam.NodSets ;
            pPriv->CurrParam.NodSettleChops = pPriv->OldParam.NodSettleChops ;
            pPriv->CurrParam.PreValidChops  = pPriv->OldParam.PreValidChops ;
            pPriv->CurrParam.PostValidChops = pPriv->OldParam.PostValidChops ;
            pPriv->CurrParam.pixclock       = pPriv->OldParam.pixclock ;
            strcpy(pPriv->CurrParam.obs_mode,pPriv->OldParam.obs_mode) ;
            strcpy(pPriv->CurrParam.readout_mode,pPriv->OldParam.readout_mode) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/******************** INAM for adjPhysG ******************/
long ufadjPhysGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy(pgs->a,"") ;
  pgs->noa = 25 ;
  return OK ;
}

/******************** SNAM for adjPhysG ******************/
long ufadjPhysGproc(genSubRecord *pgs)
{
  char yy[40] ; 
  char *original ;
  char *zz ;

  debugPGS( __HERE__, pgs ) ;

  if (strcmp(pgs->a,"") != 0) {
    /* status = unpack_any_array (pgs, (char *)pgs->a, pgs->noa,remainder) ; */
    original = pgs->a ; zz = pgs->a ; 
                strcpy(phys_phys_parm[0],zz) ;/* 1 */
    zz = zz+40; strcpy(phys_phys_parm[1],zz) ;/* 2 */
    zz = zz+40; strcpy(phys_phys_parm[2],zz) ;/* 3 */
    zz = zz+40; strcpy(phys_phys_parm[3],zz) ;/* 4 */
    zz = zz+40; strcpy(phys_phys_parm[4],zz) ;/* 5 */
    zz = zz+40; strcpy(phys_phys_parm[5],zz) ;/* 6 */
    zz = zz+40; strcpy(phys_phys_parm[6],zz) ;/* 7 */
    zz = zz+40; strcpy(phys_phys_parm[7],zz) ;/* 8 */
    zz = zz+40; strcpy(phys_phys_parm[8],zz) ;/* 9 */
    zz = zz+40; strcpy(phys_phys_parm[9],zz) ;/* 10 */
    zz = zz+40; strcpy(phys_phys_parm[10],zz) ;/* 11 */
    zz = zz+40; strcpy(phys_phys_parm[11],zz) ;/* 12 */
    zz = zz+40; strcpy(phys_phys_parm[12],zz) ;/* 13 */
    zz = zz+40; strcpy(phys_phys_parm[13],zz) ;/* 14 */
    zz = zz+40; strcpy(phys_phys_parm[14],zz) ;/* 15 */
    zz = zz+40; strcpy(phys_phys_parm[15],zz) ;/* 16 */
    zz = zz+40; strcpy(phys_phys_parm[16],zz) ;/* 17 */
    zz = zz+40; strcpy(phys_phys_parm[17],zz) ;/* 18 */
    zz = zz+40; strcpy(phys_phys_parm[18],zz) ;/* 19 */
    zz = zz+40; strcpy(phys_phys_parm[19],zz) ;/* 20 */
    zz = zz+40; strcpy(phys_phys_parm[20],zz) ;/* 21 */
    zz = zz+40; strcpy(phys_phys_parm[21],zz) ;/* 22 */
    zz = zz+40; strcpy(phys_phys_parm[22],zz) ;/* 23 */
    zz = zz+40; strcpy(phys_phys_parm[23],zz) ;/* 24 */
    zz = zz+40; strcpy(phys_phys_parm[24],zz) ;/* 25 */
    
    pgs->valu = phys_phys_parm[0] ;

    zz = pgs->a ; 
                strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 Frame Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 Save Frequency*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 Exposure Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 Chop Frequency*/
    zz = zz+40; /* 5 Chop settle time */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6 Nod Dwel time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 Nod Settle Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 8 SCS Duty Cycle MCE4 Chop Duty Cycle*/
    zz = zz+40; /* 9 MCE4 Nod Duty Cycle */
    zz = zz+40; /* 10 SCS Frequency */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valk,yy) ; /* 11 Pre Valid Chop Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 Post Valid Chop time*/
    zz = zz+40; /* 13 Frame Duty Cycle*/
    zz = zz+40; /* 14 Frame Coadd */
    zz = zz+40; /* 15 Chop Settle Read */
    zz = zz+40; /* 16 Chop Coadd Chop */
    zz = zz+40; /* 17 Nod Settle Chop */
    zz = zz+40; /* 18 Nod Settle Read */
    zz = zz+40; /* 19 Save Sets */
    zz = zz+40; /* 20 Nod Sets */
    zz = zz+40; /* 21 Pix Clock */
    zz = zz+40; /* 22 Pre Chops */
    zz = zz+40; /* 23 Post Chops */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 24 Obs Mode*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 25 Readout Mode*/

    strcpy(pgs->valj, "1") ;
    strcpy(pgs->valt, "START") ;
    strcpy(pgs->a,"") ;
    pgs->noa = 25 ;
  } else {
    strcpy(pgs->valj, "0") ;
    strcpy(pgs->valt, "CLEAR") ;
  }
  return OK ;
}

/***************** Unpacks array of strings ***************/
long assign_adj_phys_param(genSubRecord *pgs, char *zz) {

  char *original ;
  original = zz ; 

  debugPGS( __HERE__, pgs ) ;
   
  strcpy(adjPhysParm[0],zz) ;/* 1 */
  zz = zz+40; strcpy(adjPhysParm[1],zz) ;/* 2 */
  zz = zz+40; strcpy(adjPhysParm[2],zz) ;/* 3 */
  zz = zz+40; strcpy(adjPhysParm[3],zz) ;/* 4 */
  zz = zz+40; strcpy(adjPhysParm[4],zz) ;/* 5 */
  zz = zz+40; strcpy(adjPhysParm[5],zz) ;/* 6 */
  zz = zz+40; strcpy(adjPhysParm[6],zz) ;/* 7 */
  zz = zz+40; strcpy(adjPhysParm[7],zz) ;/* 8 */
  zz = zz+40; strcpy(adjPhysParm[8],zz) ;/* 9 */
  zz = zz+40; strcpy(adjPhysParm[9],zz) ;/* 10 */
  zz = zz+40; strcpy(adjPhysParm[10],zz) ;/* 11 */
  zz = zz+40; strcpy(adjPhysParm[11],zz) ;/* 12 */
  zz = zz+40; strcpy(adjPhysParm[12],zz) ;/* 13 */
  zz = zz+40; strcpy(adjPhysParm[13],zz) ;/* 14 */
  zz = zz+40; strcpy(adjPhysParm[14],zz) ;/* 15 */
  zz = zz+40; strcpy(adjPhysParm[15],zz) ;/* 16 */
  zz = zz+40; strcpy(adjPhysParm[16],zz) ;/* 17 */
  zz = zz+40; strcpy(adjPhysParm[17],zz) ;/* 18 */
  zz = zz+40; strcpy(adjPhysParm[18],zz) ;/* 19 */
  zz = zz+40; strcpy(adjPhysParm[19],zz) ;/* 20 */
  zz = zz+40; strcpy(adjPhysParm[20],zz) ;/* 21 */
  zz = zz+40; strcpy(adjPhysParm[21],zz) ;/* 22 */
  zz = zz+40; strcpy(adjPhysParm[22],zz) ;/* 23 */
  zz = zz+40; strcpy(adjPhysParm[23],zz) ;/* 24 */
  zz = zz+40; strcpy(adjPhysParm[24],zz) ;/* 25 */

  return OK ;
}

/***************** Unpacks array of strings ***************/
long assign_adj_meta_param(genSubRecord *pgs, char *zz)
{
  int i ;
  char *original ;
  original = zz ;

  debugPGS( __HERE__, pgs ) ;

  for( i=0; i<28; i++ ) {
    strcpy(adjMetaParm[i],zz);
    adjMetaParm[i][strlen(adjMetaParm[i])] = '\0';
    zz = zz+40;
  }

  if (bingo) 
    for (i=0;i<28;i++) {
      adjMetaParm[i][39] = '\0' ;
      printf("Meta item # %d is: @%s@\n",i,adjMetaParm[i]) ;
      
    }
  return OK ;
}

/***************** Unpacks array of strings ***************/
long unpack_phys_param (genSubRecord *pgs, char *zz, char **remainder) {

  char yy[40] ;
  char *original ;
  int i;
  original = zz ; 

  debugPGS( __HERE__, pgs ) ;

              strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 5 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6  */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 8 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 9 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valj,yy) ; /* 10 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valk,yy) ; /* 11 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valm,yy) ; /* 13 */

  zz = zz+40; strcpy(hrdware_parm[0],zz) ;/* 14 */
  zz = zz+40; strcpy(hrdware_parm[1],zz) ;/* 15 */
  zz = zz+40; strcpy(hrdware_parm[2],zz) ;/* 16 */
  zz = zz+40; strcpy(hrdware_parm[3],zz) ;/* 17 */
  zz = zz+40; strcpy(hrdware_parm[4],zz) ;/* 18 */
  zz = zz+40; strcpy(hrdware_parm[5],zz) ;/* 19 */
  zz = zz+40; strcpy(hrdware_parm[6],zz) ;/* 20 */
  zz = zz+40; strcpy(hrdware_parm[7],zz) ;/* 21 */
  zz = zz+40; strcpy(hrdware_parm[8],zz) ;/* 22 */
  zz = zz+40; strcpy(hrdware_parm[9],zz) ;/* 23 */
  zz = zz+40; strcpy(hrdware_parm[10],zz) ;/* 24 */
  zz = zz+40; strcpy(hrdware_parm[11],zz) ;/* 25 */
  if (bingo) for (i=0;i<12;i++) printf("Hardware item # %d is: %s\n",i,hrdware_parm[i]) ;
  return OK ;
}

/******************** INAM for physicalG ******************/
long ufphysicalGinit (genSubRecord *pgs)
{
  /* Gensub Private Structure Declaration */
  genSubDCPhysicalPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;  

  debugPGS( __HERE__, pgs ) ;

  /* Create a private control structure for this gensub record */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCPhysicalPrivate *) malloc (sizeof(genSubDCPhysicalPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* Locate associated CAR record fields and save their addresses in private structure... */

  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* Assign private information needed */
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  pPriv->CurrParam.FrameTime = -1.0;
  pPriv->CurrParam.saveFrq= -1.0 ;
  pPriv->CurrParam.exposureTime= -1.0 ;
  pPriv->CurrParam.ChopFreq= -1.0 ;
  pPriv->CurrParam.SCSDutyCycle= -1.0 ;
  pPriv->CurrParam.nodDwelTime= -1.0 ;
  pPriv->CurrParam.nodStlTime= -1.0 ;
  pPriv->CurrParam.preValidChopTime = -1.0;
  pPriv->CurrParam.postValidChopTime = -1.0;
  /* pPriv->CurrParam.chopDutyCycle = -1.0; */
  
  strcpy(pPriv->CurrParam.obs_mode,"") ;
  strcpy(pPriv->CurrParam.readout_mode,"") ;

  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /*  */
  *(double *)pgs->b = -1.0 ; /*  */
  *(double *)pgs->c = -1.0 ; /*  */
  *(double *)pgs->d = -1.0 ; /*  */
  *(double *)pgs->e = -1.0 ; /*  */
  *(double *)pgs->f = -1.0 ; /*  */
  *(double *)pgs->g = -1.0 ; /*  */
  *(double *)pgs->h = -1.0 ; /*  */
  *(long *)pgs->l = -1 ;
  strcpy(pgs->i,"") ;
  strcpy(pgs->j,"") ;
  strcpy(pgs->k,"") ; /*  */
  *(double *)pgs->m = -1.0 ; /*  */
  *(double *)pgs->n = -1.0 ; /*  */
  *(double *)pgs->o = -1.0 ; /*  */
  *(double *)pgs->p = -1.0 ; /*  */
  /* strcpy(pgs->l,"") ;*/ /*  */
  /* *(long *)pgs->m = -1 ;  */

  pPriv->dcPhysical_inp.command_mode = -1;
  pPriv->dcPhysical_inp.FrameTime = -1.0;
  pPriv->dcPhysical_inp.saveFrq= -1.0 ;
  pPriv->dcPhysical_inp.exposureTime= -1.0 ;
  pPriv->dcPhysical_inp.ChopFreq= -1.0 ;
  pPriv->dcPhysical_inp.SCSDutyCycle= -1.0 ;
  pPriv->dcPhysical_inp.nodDwelTime= -1.0 ;
  pPriv->dcPhysical_inp.nodStlTime= -1.0 ;
  pPriv->dcPhysical_inp.preValidChopTime = -1.0;
  pPriv->dcPhysical_inp.postValidChopTime =-1.0 ;
  /* pPriv->dcPhysical_inp.chopDutyCycle = -1.0; */
  
  strcpy(pPriv->dcPhysical_inp.obs_mode,"") ;
  strcpy(pPriv->dcPhysical_inp.readout_mode,"") ;

  pPriv->dcPhysical_inp.pFlag = -1 ;
  
  pPriv->paramLevel = 'P' ;
  pgs->noj = 25 ;

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pgs->dpvt = (void *) pPriv;
  
  return OK ;
}

/******************** SNAM for physicalG *******************/
long ufphysicalGproc (genSubRecord *pgs) {

  genSubDCPhysicalPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 
  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;

  debugPGS( __HERE__, pgs ) ;

  /* strcpy(dc_DEBUG_MODE,pgs->m) ; */

  pPriv = (genSubDCPhysicalPrivate *)pgs->dpvt ;

  /* How the record is processed depends on the current command execution state... */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      if (flam_initialized != 1)  init_flam_config() ;
      if (!dc_initialized)  init_dc_config() ;

      /* Clear outpus A-J for */
      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      *(double *)pgs->valc = -1.0 ;
      *(double *)pgs->vald = -1.0 ;
      *(double *)pgs->vale = -1.0 ;
      *(double *)pgs->valf = -1.0 ;
      *(double *)pgs->valg = -1.0 ;
      *(double *)pgs->valh = -1.0 ;
      *(double *)pgs->valk = -1.0 ;
      *(double *)pgs->vall = -1.0 ;
      strcpy(pgs->vali,"") ;
      strcpy(pgs->valj,"") ;
      strcpy(pgs->valm,"") ;
      strcpy(pgs->valn,"") ;
      strcpy(pgs->valo,"") ;
      strcpy(pgs->valp,"") ;
      strcpy(pgs->valq,"") ;
      strcpy(pgs->valr,"") ;
      strcpy(pgs->vals,"") ;
      strcpy(pgs->valt,"") ;

      trx_debug("Attempting to establish a connection to the agent",
		pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      pPriv->socfd = -1 ;  /* ZZZ */
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to change indicating that a
     *  new command has arrived. Status and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      
      pPriv->dcPhysical_inp.command_mode = *(long *)pgs->a;
      pPriv->dcPhysical_inp.FrameTime = *(double *)pgs->b ;
      pPriv->dcPhysical_inp.saveFrq= *(double *)pgs->c ; 
      pPriv->dcPhysical_inp.exposureTime= *(double *)pgs->d ;
      pPriv->dcPhysical_inp.ChopFreq= *(double *)pgs->e ;
      pPriv->dcPhysical_inp.SCSDutyCycle= *(double *)pgs->f  ;
      pPriv->dcPhysical_inp.nodDwelTime= *(double *)pgs->g ;
      pPriv->dcPhysical_inp.nodStlTime= *(double *)pgs->h ;
      /* pPriv->dcPhysical_inp.chopDutyCycle =*(double *)pgs->l ; */
  
      strcpy(pPriv->dcPhysical_inp.obs_mode,pgs->i) ;
      strcpy(pPriv->dcPhysical_inp.readout_mode,pgs->k) ;

      pPriv->dcPhysical_inp.pFlag = *(long *)pgs->l ;
      pPriv->dcPhysical_inp.preValidChopTime = *(double *)pgs->m;
      pPriv->dcPhysical_inp.postValidChopTime = *(double *)pgs->n ;

      if ( (pPriv->dcPhysical_inp.command_mode != -1) ||
	   (pPriv->dcPhysical_inp.FrameTime != -1.0) ||
	   (pPriv->dcPhysical_inp.saveFrq != -1.0) ||
	   (pPriv->dcPhysical_inp.exposureTime != -1.0) ||
	   (pPriv->dcPhysical_inp.ChopFreq != -1.0) ||
	   (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0) ||
	   (pPriv->dcPhysical_inp.nodDwelTime != -1.0) ||
           (pPriv->dcPhysical_inp.preValidChopTime != -1.0) ||
           (pPriv->dcPhysical_inp.postValidChopTime != -1.0) ||
	   (pPriv->dcPhysical_inp.nodStlTime != -1.0) ||
           (strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0) ||
           (strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        *(long *)pgs->a = -1 ; /*  */
        *(double *)pgs->b = -1.0 ; /*  */
        *(double *)pgs->c = -1.0 ; /*  */
        *(double *)pgs->d = -1.0 ; /*  */
        *(double *)pgs->e = -1.0 ; /*  */
        *(double *)pgs->f = -1.0 ; /*  */
        *(double *)pgs->g = -1.0 ; /*  */
        *(double *)pgs->h = -1.0 ; /*  */
        *(double *)pgs->i = -1.0 ; /*  */
        *(double *)pgs->m = -1.0 ; /*  */
        *(double *)pgs->n = -1.0 ; /*  */
        strcpy(pgs->j,"") ;
        strcpy(pgs->k,"") ; /*  */
        pgs->noj = 25 ;
        strcpy(pgs->l,"") ; /*  */
        *(long *)pgs->m = -1 ; /*  */

      } else {  /* Check to see if the agent response input has changed. */

        strcpy(response,pgs->j) ;
        pgs->noj = 25 ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J field
	   * from the Agent unless the agent is late with something.
	   * Log a message that the agent responded unexpectedly
           */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the GENSUB.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 25 ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */

      /* Is this a CALLBACK to send a command or do an INIT or process adjusted input? */
      if (pPriv->dcPhysical_inp.pFlag == 1) {
        /* get the input into the current parameters */
        if (pPriv->dcPhysical_inp.FrameTime != -1.0)  
          pPriv->CurrParam.FrameTime = pPriv->dcPhysical_inp.FrameTime  ;
        if (pPriv->dcPhysical_inp.saveFrq != -1.0)  
          pPriv->CurrParam.saveFrq = pPriv->dcPhysical_inp.saveFrq  ;
        if (pPriv->dcPhysical_inp.exposureTime != -1.0)  
          pPriv->CurrParam.exposureTime = pPriv->dcPhysical_inp.exposureTime  ;
        if (pPriv->dcPhysical_inp.ChopFreq != -1.0)  
          pPriv->CurrParam.ChopFreq = pPriv->dcPhysical_inp.ChopFreq  ;
        if (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0)  
          pPriv->CurrParam.SCSDutyCycle = pPriv->dcPhysical_inp.SCSDutyCycle  ;
        if (pPriv->dcPhysical_inp.nodDwelTime != -1.0)  
          pPriv->CurrParam.nodDwelTime = pPriv->dcPhysical_inp.nodDwelTime  ;
        if (pPriv->dcPhysical_inp.nodStlTime != -1.0)  
          pPriv->CurrParam.nodStlTime = pPriv->dcPhysical_inp.nodStlTime  ;
        if (pPriv->dcPhysical_inp.preValidChopTime != -1.0)
          pPriv->CurrParam.preValidChopTime = pPriv->dcPhysical_inp.preValidChopTime ;
        if (pPriv->dcPhysical_inp.postValidChopTime != -1.0)
          pPriv->CurrParam.preValidChopTime = pPriv->dcPhysical_inp.postValidChopTime ;

        if(strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0)
          strcpy(pPriv->CurrParam.obs_mode,pPriv->dcPhysical_inp.obs_mode) ;
        if(strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0)
          strcpy(pPriv->CurrParam.readout_mode,pPriv->dcPhysical_inp.readout_mode) ;

        /* update the output links */
        *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
        *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
        *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
        *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
        /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
        *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
        *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ; 
        *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
        *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;

        strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
        strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;

        /* Reset Command state to DONE */
        pPriv->commandState = TRX_GS_DONE;
        some_num = CAR_IDLE ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                     &some_num,1);
        if (status) {
          trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
      }
      else {
        if (pPriv->dcPhysical_inp.command_mode != -1) { /* We have an init command */
          trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ; 
          /* Check first to see if we have a valid INIT directive */
          if ((pPriv->dcPhysical_inp.command_mode == SIMM_NONE) ||
              (pPriv->dcPhysical_inp.command_mode == SIMM_FAST) ||
              (pPriv->dcPhysical_inp.command_mode == SIMM_FULL) ) {
            /* need to reconnect to the agent ? -- hon */
	    if( pPriv->socfd >= 0 ) {
	      if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	        ufLog( __HERE__ "> socket is writable, no need to reconnect");
	      }
	      else {
                trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
                ufClose(pPriv->socfd) ;
                pPriv->socfd = -1;
                *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
	    }
             pPriv->CurrParam.command_mode = pPriv->dcPhysical_inp.command_mode; /* sim level */
            *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
            /* Re-Connect if needed */
            /* Read some initial parameters from a file */

            pPriv->port_no = read_dc_file( pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel );
 
            if (pPriv->dcPhysical_inp.command_mode != SIMM_FAST) {
	      if( pPriv->socfd < 0 ) {
                trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
                pPriv->socfd = UFGetConnection( pgs->name, pPriv->port_no, dc_host_ip );
                trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
	        *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
              if (pPriv->socfd < 0) { /* we have a bad socket connection */
                pPriv->commandState = TRX_GS_DONE;
                strcpy (pPriv->errorMessage, "Error Connecting to Agent");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            }
            /* update output links if connection is good or we are in SIMM_FAST*/
            if ( (pPriv->dcPhysical_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
              /* update the output links */
              /* update the output links */
              *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
              *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
              *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
              *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
              /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
              *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
              *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;
              strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;
              *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
              *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;  
              /* Reset Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
              if (status) {
                trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              } 
            }
        } else  if (pPriv->dcPhysical_inp.command_mode == SIMM_VSM) {

	    printf("ufacqControlGproc>: SETTING command mode to %d curr %d \n", 
				(int)pPriv->dcPhysical_inp.command_mode, 
				(int)pPriv->CurrParam.command_mode);
            pPriv->CurrParam.command_mode = pPriv->dcPhysical_inp.command_mode;

            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "");
            status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, 
					pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_IDLE;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) 
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE);

            return OK;

	  } else { /* we have an error in input of the init command */
            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "Bad INIT Directive DC 2");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
          strcpy(pgs->j,"") ;
          strcpy(pgs->valu,"") ;
          pgs->novu = 25 ;
          pgs->noj = 25 ; 
        }
	else {    /* We have a configuration command */
          trx_debug("We have a configuration Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Save the current parameters */

          pPriv->OldParam.FrameTime     = pPriv->CurrParam.FrameTime ;
          pPriv->OldParam.saveFrq       = pPriv->CurrParam.saveFrq ;
          pPriv->OldParam.exposureTime  = pPriv->CurrParam.exposureTime ;
          pPriv->OldParam.ChopFreq      = pPriv->CurrParam.ChopFreq ;
          pPriv->OldParam.SCSDutyCycle   = pPriv->CurrParam.SCSDutyCycle ;
          pPriv->OldParam.nodDwelTime   = pPriv->CurrParam.nodDwelTime ;
          pPriv->OldParam.nodStlTime    = pPriv->CurrParam.nodStlTime ;
          /* pPriv->OldParam.chopDutyCycle = pPriv->CurrParam.chopDutyCycle ; */
          pPriv->OldParam.preValidChopTime = pPriv->CurrParam.preValidChopTime ;
          pPriv->OldParam.postValidChopTime = pPriv->CurrParam.postValidChopTime ;
          strcpy(pPriv->OldParam.obs_mode,pPriv->CurrParam.obs_mode) ;
          strcpy(pPriv->OldParam.readout_mode,pPriv->CurrParam.readout_mode) ;

          /* get the input into the current parameters */
          if (pPriv->dcPhysical_inp.FrameTime != -1.0)  
            pPriv->CurrParam.FrameTime = pPriv->dcPhysical_inp.FrameTime  ;
          if (pPriv->dcPhysical_inp.saveFrq != -1.0)  
            pPriv->CurrParam.saveFrq = pPriv->dcPhysical_inp.saveFrq  ;
          if (pPriv->dcPhysical_inp.exposureTime != -1.0)  
            pPriv->CurrParam.exposureTime = pPriv->dcPhysical_inp.exposureTime  ;
          if (pPriv->dcPhysical_inp.ChopFreq != -1.0)  
            pPriv->CurrParam.ChopFreq = pPriv->dcPhysical_inp.ChopFreq  ;
          if (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0)  
            pPriv->CurrParam.SCSDutyCycle = pPriv->dcPhysical_inp.SCSDutyCycle  ;
          if (pPriv->dcPhysical_inp.nodDwelTime != -1.0)  
            pPriv->CurrParam.nodDwelTime = pPriv->dcPhysical_inp.nodDwelTime  ;
          if (pPriv->dcPhysical_inp.nodStlTime != -1.0)  
            pPriv->CurrParam.nodStlTime = pPriv->dcPhysical_inp.nodStlTime  ;
          if (pPriv->dcPhysical_inp.preValidChopTime != -1.0)
            pPriv->CurrParam.preValidChopTime= pPriv->dcPhysical_inp.preValidChopTime ;
          if (pPriv->dcPhysical_inp.postValidChopTime != -1.0)
            pPriv->CurrParam.postValidChopTime = pPriv->dcPhysical_inp.postValidChopTime ;

          if(strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0)
            strcpy(pPriv->CurrParam.obs_mode,pPriv->dcPhysical_inp.obs_mode) ;
          if(strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0)
            strcpy(pPriv->CurrParam.readout_mode,pPriv->dcPhysical_inp.readout_mode) ;

          /* formulate the command strings */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;
  
          num_str = UFcheck_dc_Phys_inputs( pPriv->dcPhysical_inp, com,
					    pPriv->paramLevel, rec_name,
					    &pPriv->CurrParam, pPriv->CurrParam.command_mode );
          /* Clear the input structure */
          pPriv->dcPhysical_inp.command_mode = -1;
          pPriv->dcPhysical_inp.FrameTime = -1.0;
          pPriv->dcPhysical_inp.saveFrq= -1.0 ;
          pPriv->dcPhysical_inp.exposureTime= -1.0 ;
          pPriv->dcPhysical_inp.ChopFreq= -1.0 ;
          pPriv->dcPhysical_inp.SCSDutyCycle= -1.0 ;
          pPriv->dcPhysical_inp.nodDwelTime= -1.0 ;
          pPriv->dcPhysical_inp.nodStlTime = -1.0 ;
          pPriv->dcPhysical_inp.preValidChopTime = -1.0 ;
          pPriv->dcPhysical_inp.postValidChopTime = -1.0 ;
          /* pPriv->dcPhysical_inp.chopDutyCycle = -1.0; */
  
          strcpy(pPriv->dcPhysical_inp.obs_mode,"") ;
          strcpy(pPriv->dcPhysical_inp.readout_mode,"") ;

          pPriv->dcPhysical_inp.pFlag = 1 ;

          /* do we have commands to send? */
          if (num_str > 0) {
            /* if no Agent needed then go back to DONE */
            /* we are talking about SIMM_FAST or commands that do not need the agent */

	     if (bingo)
	       {
		 for (i=0;i<num_str;i++)
		   {
		     sprintf( _UFerrmsg, __HERE__ "> com[ %d ]: %s", i, com[ i ] ) ;
		     ufLog( _UFerrmsg ) ;
		   }
	       }

            if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* update the output links */
              *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
              *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
              *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
              *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
              /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
              *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
              *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;

              strcpy(pgs->vali, pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valj, pPriv->CurrParam.readout_mode) ;
              *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
              *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, 
                                   DBR_LONG, &some_num, 1);
              if (status) {
                logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
                return OK;
              }
            } else { /* we are in SIMM_NONE or SIMM_FULL */
              /* See if you can send the command and go to BUSY state */
              if (pPriv->socfd > 0) {
		if( _verbose )
		  {
		    sprintf(_UFerrmsg,__HERE__"> sending to agent, genSub record: %s",pgs->name); 
		    ufLog( _UFerrmsg ) ;
		  }

		status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
              }
	      else {
		sprintf( _UFerrmsg, __HERE__ "> send socfd bad? , genSub record: %s",pgs->name); 
		ufLog( _UFerrmsg ) ;
		status = 0;
	      } 

              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              if (status == 0) { /* there was an error sending the commands to the agent */
                /* set Command state to DONE */
                pPriv->commandState = TRX_GS_DONE;
                /* set the CAR to ERR with appropriate message like 
                 * "Bad socket"  and go to DONE state*/
                strcpy (pPriv->errorMessage, "Bad socket connection DC2");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else { /* send successful */
                /* establish a CALLBACK */
                requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
                pPriv->startingTicks = tickGet() ;
                /* set Command state to BUSY */
                pPriv->commandState = TRX_GS_BUSY;
              }
            }
          } else {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (num_str < 0) {
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* we have an error in the Input */
              strcpy (pPriv->errorMessage, "Error in input DC2");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* num_str == 0 */
              /* is it possible to get here? */
              /* here we have a change in input.  The inputs though got sent
               * to be checked but no string got formulated.  */
              /* this can only happen if somehow the memory got corrupted ? */
              trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
            }
          }
        }
      
        /*Is this a J field processing? */
        strcpy(response,pgs->j) ;
        pgs->noj = 25 ;
        if (strcmp(response,"") != 0)
	  {
	  }
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J field from the Agent
	   * unless the agent is late with something
           * Log a message that the agent responded unexpectedly
	   */
          trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 25 ;
      }
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */

      strcpy(response,pgs->j) ;
      pgs->noj = 25 ;
      if (strcmp(response,"") != 0) {
        trx_debug(response,pgs->name,"FULL",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/

        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "DC P Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then cancel the call back go to DONE state and set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
        
          /* update the output links */
          *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
          *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
          *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
          *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
          /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
          *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
          *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;
          strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
          strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;
          *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
          *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */

            pPriv->CurrParam.FrameTime     = pPriv->OldParam.FrameTime ;
            pPriv->CurrParam.saveFrq       = pPriv->OldParam.saveFrq ;
            pPriv->CurrParam.exposureTime  = pPriv->OldParam.exposureTime ;
            pPriv->CurrParam.ChopFreq      = pPriv->OldParam.ChopFreq ;
            pPriv->CurrParam.SCSDutyCycle   = pPriv->OldParam.SCSDutyCycle ;
            pPriv->CurrParam.nodDwelTime   = pPriv->OldParam.nodDwelTime ;
            pPriv->CurrParam.nodStlTime    = pPriv->OldParam.nodStlTime ;
            /* pPriv->CurrParam.chopDutyCycle = pPriv->OldParam.chopDutyCycle ; */
            pPriv->CurrParam.preValidChopTime = pPriv->OldParam.preValidChopTime;
            pPriv->CurrParam.postValidChopTime = pPriv->OldParam.postValidChopTime;

            strcpy(pPriv->CurrParam.obs_mode,pPriv->OldParam.obs_mode) ;
            strcpy(pPriv->CurrParam.readout_mode,pPriv->OldParam.readout_mode) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            /* unpack_phys_param (pgs, (char *)pgs->j,(char **)hrdware_parm) ; */
            /* for (i = 0; i<12; i++) printf("Element %d is: %s \n",i,hrdware_parm[i]) ; */
            assign_adj_phys_param( pgs, (char *)pgs->j) ;
            pgs->novu = 25 ;
            pgs->valu = adjPhysParm[0] ;
	    pgs->noj = 25 ;
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 25 ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/***************** INAM for physOutGGinit *****************/
long ufphysOutGGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy (pgs->a,"") ;
  pgs->noa = 25 ;
  
  return OK ;
}

/***************** INAM for physOutGGinit *****************/
long ufphysOutGGproc (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  unpack_phys_param (pgs, (char *)pgs->a,(char **)hrdware_parm ) ;
  pgs->novu = 12;
  pgs->valu = hrdware_parm[0] ;
  strcpy (pgs->a,"") ;
  pgs->noa = 25 ;
  
  return OK ;
}

/***************** INAM for acqControlG *******************/
long ufacqControlGinit (genSubRecord *pgs)
{
  /* Gensub Private Structure Declaration */
  genSubDCAcqContPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;

  debugPGS( __HERE__, pgs ) ;

  printf("I'm in ufacqControlGinit \n");

  /* Create a private control structure for this gensub record  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCAcqContPrivate *) malloc (sizeof(genSubDCAcqContPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->commandState = TRX_GS_INIT;

  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  if (strstr(pgs->name,"miri") != NULL) strcpy (buffer, "miri:dc:observe"); 
  else strcpy (buffer, "flam:dc:observe"); 
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->ObserveCcarinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  if (strstr(pgs->name,"miri") != NULL) strcpy (buffer, "miri:dc:observe"); 
  else strcpy (buffer, "flam:dc:observe"); 
  /* strcpy (buffer, pgs->name);
     buffer[strlen(buffer) - 1] = '\0'; */
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->ObserveCcarinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* Assign private information needed */
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  strcpy(pPriv->CurrParam.command,"") ;
  strcpy(pPriv->CurrParam.obs_id,"") ;
  strcpy(pPriv->CurrParam.dhs_write,"") ;
  strcpy(pPriv->CurrParam.qckLk_id,"") ;
  strcpy(pPriv->CurrParam.local_archive,"") ;
  strcpy(pPriv->CurrParam.nod_handshake,"") ;
  strcpy(pPriv->CurrParam.archive_path,"") ;
  strcpy(pPriv->CurrParam.archive_file_name,"") ;
  strcpy(pPriv->CurrParam.comment,"") ;
  strcpy(pPriv->CurrParam.remote_archive,"") ;
  strcpy(pPriv->CurrParam.rem_archive_host,"") ;
  strcpy(pPriv->CurrParam.rem_archive_path,"") ;
  strcpy(pPriv->CurrParam.rem_archive_file_name,"") ;

  /* Clear the Gensub inputs */
  strcpy((char *)pgs->a,"") ; /*  */
  strcpy((char *)pgs->b,"") ; /*  */
  strcpy((char *)pgs->c,"") ; /*  */
  strcpy((char *)pgs->d,"") ; /*  */
  strcpy((char *)pgs->e,"") ; /*  */
  strcpy((char *)pgs->f,"") ; /*  */
  strcpy((char *)pgs->g,"") ; /*  */
  strcpy((char *)pgs->h,"") ; /*  */
  strcpy((char *)pgs->i,"") ; /*  */
  strcpy((char *)pgs->k,"") ; /*  */
  strcpy((char *)pgs->l,"") ; /*  */ 
  strcpy((char *)pgs->m,"") ; /*  */
  strcpy((char *)pgs->n,"") ; /*  */
  strcpy((char *)pgs->o,"") ; /*  */
  strcpy((char *)pgs->p,"") ; /*  */
  strcpy((char *)pgs->q,"") ; /*  */
  strcpy((char *)pgs->r,"") ; /*  */
  strcpy((char *)pgs->s,"") ; /*  */

  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;

  pPriv->dcAcqCont_inp.command_mode = -1  ;
  strcpy(pPriv->dcAcqCont_inp.command,"") ;
  strcpy(pPriv->dcAcqCont_inp.obs_id,"") ;
  strcpy(pPriv->dcAcqCont_inp.dhs_write,"") ;
  strcpy(pPriv->dcAcqCont_inp.qckLk_id,"") ;
  strcpy(pPriv->dcAcqCont_inp.local_archive,"") ;
  strcpy(pPriv->dcAcqCont_inp.nod_handshake,"") ;
  strcpy(pPriv->dcAcqCont_inp.archive_path,"") ;
  strcpy(pPriv->dcAcqCont_inp.archive_file_name,"") ;
  strcpy(pPriv->dcAcqCont_inp.comment,"") ;
  strcpy(pPriv->dcAcqCont_inp.remote_archive,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_host,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_path,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,"") ;

  pPriv->paramLevel = 'A' ;
  pPriv->observing = 0 ;
  pPriv->obs_time_out = 3600 ;
  pgs->dpvt = (void *) pPriv;

  return OK ;
}

/******************** SNAM for acqControlG *******************/
long ufacqControlGproc (genSubRecord *pgs) {

  genSubDCAcqContPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 
  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;
  char obs_mode[40] ;
  double onSrc_time , ChopDutyCycle, NodDutyCycle, Total_Time, FrameDutyCycle ;
  char temp_str[40];

  pPriv = (genSubDCAcqContPrivate *)pgs->dpvt ;

  printf("ufacqControlGproc> Setting debug mode to FULL \n");
  strcpy(dc_STARTUP_DEBUG_MODE,"FULL");
  strcpy(dc_DEBUG_MODE,"FULL");

  /* How the record is processed depends on the current command execution state... */

  printf("ufacqControlGproc> I'm in ufacqControlGproc> command state = %d \n",
		(int)pPriv->commandState);

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      printf("ufacqControlGproc>: Processing from TRX_GS_INIT: state \n");
      debugPGS( __HERE__, pgs ) ;
      if (flam_initialized != 1)  init_flam_config() ;
      if (!dc_initialized)  init_dc_config() ;

      /* Clear outpus A-J for temp Gensub */
      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy((char *)pgs->valc,"") ;
      strcpy((char *)pgs->vald,"") ;
      strcpy((char *)pgs->vale,"") ;
      strcpy((char *)pgs->valf,"") ;
      strcpy((char *)pgs->valg,"") ;
      strcpy((char *)pgs->valh,"") ;
      strcpy((char *)pgs->vali,"") ;
      strcpy((char *)pgs->valj,"") ;
      strcpy((char *)pgs->valk,"") ;
      strcpy((char *)pgs->vall,"") ;
      strcpy((char *)pgs->valm,"") ;
      strcpy((char *)pgs->valn,"") ;

      trx_debug("Attempting to establish a connection to the agent",
		pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);

      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      printf("ufacqControlGproc>: Processing from TRX_GS_DONE: state \n");
      
      strcpy(pPriv->dcAcqCont_inp.command,pgs->a) ;
      strcpy(pPriv->dcAcqCont_inp.obs_id,pgs->b) ;
      strcpy(pPriv->dcAcqCont_inp.dhs_write,pgs->c) ;
      strcpy(pPriv->dcAcqCont_inp.qckLk_id,pgs->d) ;
      if (strcmp((char *)pgs->e,"" ) == 0)  pPriv->dcAcqCont_inp.command_mode = -1 ;
      else {
        pPriv->dcAcqCont_inp.command_mode = (long)strtod(pgs->e,&endptr) ;
        if (*endptr != '\0'){
          pPriv->dcAcqCont_inp.command_mode = -1 ;
        }
      }
      strcpy(pPriv->dcAcqCont_inp.local_archive,pgs->f) ;
      strcpy(pPriv->dcAcqCont_inp.nod_handshake,pgs->g) ;
      strcpy(pPriv->dcAcqCont_inp.archive_path,pgs->h) ;
      strcpy(pPriv->dcAcqCont_inp.archive_file_name,pgs->i) ;
      strcpy(pPriv->dcAcqCont_inp.comment,pgs->k) ;
      strcpy(pPriv->dcAcqCont_inp.remote_archive,pgs->l) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_host,pgs->m) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_path,pgs->n) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,pgs->o) ;
      
      if ( (strcmp(pPriv->dcAcqCont_inp.command,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.obs_id,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.dhs_write,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.qckLk_id,"") != 0) ||
           (pPriv->dcAcqCont_inp.command_mode != -1) ||
           (strcmp(pPriv->dcAcqCont_inp.local_archive,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.nod_handshake,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.archive_path,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.archive_file_name,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.comment,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.remote_archive,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_host,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_path,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_file_name,"")) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        strcpy((char *)pgs->a,"") ; /*  */
        strcpy((char *)pgs->b,"") ; /*  */
        strcpy((char *)pgs->c,"") ; /*  */
        strcpy((char *)pgs->d,"") ; /*  */
        strcpy((char *)pgs->e,"") ; /*  */
        strcpy((char *)pgs->f,"") ; /*  */
        strcpy((char *)pgs->g,"") ; /*  */
        strcpy((char *)pgs->h,"") ; /*  */
        strcpy((char *)pgs->i,"") ; /*  */
        strcpy((char *)pgs->k,"") ; /*  */
        strcpy((char *)pgs->l,"") ; /*  */
        strcpy((char *)pgs->m,"") ; /*  */
        strcpy((char *)pgs->n,"") ; /*  */
        strcpy((char *)pgs->o,"") ; /*  */

      } else {  /*  Check to see if the agent response input has changed. */

        strcpy(response,pgs->j) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J field
	   * from the Agent unless the agent is late with something
	   * Log a message that the agent responded unexpectedly.
           */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the GENSUB.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      printf("ufacqControlGproc>: Processing from SENDING state \n");

      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */

      if (pPriv->dcAcqCont_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        printf("ufacqControlGproc>: INIT Command comm. mode = %d\n",(int)pPriv->dcAcqCont_inp.command_mode);

        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->dcAcqCont_inp.command_mode == SIMM_NONE) ||
            (pPriv->dcAcqCont_inp.command_mode == SIMM_FAST) ||
            (pPriv->dcAcqCont_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      ufLog( __HERE__ "> socket is writable, no need to reconnect");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->dcAcqCont_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          pPriv->port_no = read_dc_file( pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel);

          if (pPriv->dcAcqCont_inp.command_mode != SIMM_FAST) {
	    if(  pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection( pgs->name, pPriv->port_no, dc_host_ip );
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->dcAcqCont_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            strcpy(pgs->valc,pPriv->CurrParam.command);
            strcpy(pgs->vald,pPriv->CurrParam.obs_id);
            strcpy(pgs->vale,pPriv->CurrParam.dhs_write);
            strcpy(pgs->valf,pPriv->CurrParam.qckLk_id);
            strcpy(pgs->valg,pPriv->CurrParam.local_archive);
            strcpy(pgs->valh,pPriv->CurrParam.nod_handshake);
            strcpy(pgs->valo,pPriv->CurrParam.archive_path);
            strcpy(pgs->valp,pPriv->CurrParam.archive_file_name);
            strcpy(pgs->valq,pPriv->CurrParam.comment);
            strcpy(pgs->valr,pPriv->CurrParam.remote_archive);
            strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host);
            strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path);
            strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name);

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
        } else  if (pPriv->dcAcqCont_inp.command_mode == SIMM_VSM) {

	    printf("ufacqControlGproc>: SETTING command mode to %d curr %d \n", 
				(int)pPriv->dcAcqCont_inp.command_mode, 
				(int)pPriv->CurrParam.command_mode);
            pPriv->CurrParam.command_mode = pPriv->dcAcqCont_inp.command_mode;

            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "");
            status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, 
					pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_IDLE;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) 
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE);

            return OK;

        } else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive DC3");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, 
					pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      } else { /* We have a configuration command */
        trx_debug("We have a MCE4 Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        printf("ufacqControlGproc>: We have a MCE4 Command curr=%d \n",(
			int)pPriv->CurrParam.command_mode);

        if (pPriv->CurrParam.command_mode == SIMM_VSM) {

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_IDLE;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) 
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
        }

        /* Save the current parameters */

        strcpy(pPriv->OldParam.command,pPriv->CurrParam.command) ;
        strcpy(pPriv->OldParam.obs_id,pPriv->CurrParam.obs_id) ;
        strcpy(pPriv->OldParam.dhs_write,pPriv->CurrParam.dhs_write) ;
        strcpy(pPriv->OldParam.qckLk_id,pPriv->CurrParam.qckLk_id) ;
        strcpy(pPriv->OldParam.local_archive,pPriv->CurrParam.local_archive) ;
        strcpy(pPriv->OldParam.nod_handshake,pPriv->CurrParam.nod_handshake) ;
        strcpy(pPriv->OldParam.archive_path,pPriv->CurrParam.archive_path) ;
        strcpy(pPriv->OldParam.archive_file_name,pPriv->CurrParam.archive_file_name) ;
        strcpy(pPriv->OldParam.comment,pPriv->CurrParam.comment) ;
        strcpy(pPriv->OldParam.remote_archive,pPriv->CurrParam.remote_archive) ;
        strcpy(pPriv->OldParam.rem_archive_host,pPriv->CurrParam.rem_archive_host) ;
        strcpy(pPriv->OldParam.rem_archive_path,pPriv->CurrParam.rem_archive_path) ;
        strcpy(pPriv->OldParam.rem_archive_file_name,pPriv->CurrParam.rem_archive_file_name) ;

        /* get the input into the current parameters */
        if (strcmp(pPriv->dcAcqCont_inp.command,"") != 0)  
          strcpy(pPriv->CurrParam.command, pPriv->dcAcqCont_inp.command)  ;
        if (strcmp(pPriv->dcAcqCont_inp.obs_id,"") != 0)  
          strcpy(pPriv->CurrParam.obs_id,pPriv->dcAcqCont_inp.obs_id)  ;
        if (strcmp(pPriv->dcAcqCont_inp.dhs_write,"") != 0)  
          strcpy(pPriv->CurrParam.dhs_write, pPriv->dcAcqCont_inp.dhs_write)  ;
        if (strcmp(pPriv->dcAcqCont_inp.qckLk_id,"") != 0)  
          strcpy(pPriv->CurrParam.qckLk_id ,pPriv->dcAcqCont_inp.qckLk_id)  ;
        if (strcmp(pPriv->dcAcqCont_inp.local_archive,"") != 0)  
          strcpy(pPriv->CurrParam.local_archive,pPriv->dcAcqCont_inp.local_archive)  ;
        if (strcmp(pPriv->dcAcqCont_inp.nod_handshake,"") != 0)  
          strcpy(pPriv->CurrParam.nod_handshake, pPriv->dcAcqCont_inp.nod_handshake)  ;
        if (strcmp(pPriv->dcAcqCont_inp.archive_path,"") != 0)  
          strcpy(pPriv->CurrParam.archive_path, pPriv->dcAcqCont_inp.archive_path)  ;
        if (strcmp(pPriv->dcAcqCont_inp.archive_file_name,"") != 0)  
          strcpy(pPriv->CurrParam.archive_file_name, pPriv->dcAcqCont_inp.archive_file_name)  ;
        if (strcmp(pPriv->dcAcqCont_inp.comment,"") != 0)  
          strcpy(pPriv->CurrParam.comment,pPriv->dcAcqCont_inp.comment)  ;
        if (strcmp(pPriv->dcAcqCont_inp.remote_archive,"") != 0)  
          strcpy(pPriv->CurrParam.remote_archive,pPriv->dcAcqCont_inp.remote_archive)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_host,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_host,pPriv->dcAcqCont_inp.rem_archive_host)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_path,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_path,pPriv->dcAcqCont_inp.rem_archive_path)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_file_name,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_file_name,pPriv->dcAcqCont_inp.rem_archive_file_name);

        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;

        num_str = UFcheck_dc_Acq_inputs( pPriv->dcAcqCont_inp,com, pPriv->paramLevel,
					 rec_name, &pPriv->CurrParam, 
					 pPriv->CurrParam.command_mode) ;

        /* Clear the input structure */
        pPriv->dcAcqCont_inp.command_mode = -1  ;
        strcpy(pPriv->dcAcqCont_inp.command,"") ;
        strcpy(pPriv->dcAcqCont_inp.obs_id,"") ;
        strcpy(pPriv->dcAcqCont_inp.dhs_write,"") ;
        strcpy(pPriv->dcAcqCont_inp.qckLk_id,"") ;
        strcpy(pPriv->dcAcqCont_inp.local_archive,"") ;
        strcpy(pPriv->dcAcqCont_inp.nod_handshake,"") ;
        strcpy(pPriv->dcAcqCont_inp.archive_path,"") ;
        strcpy(pPriv->dcAcqCont_inp.archive_file_name,"") ;
        strcpy(pPriv->dcAcqCont_inp.comment,"") ;
        strcpy(pPriv->dcAcqCont_inp.remote_archive,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_host,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_path,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,"") ;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */

          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.command) ;
            strcpy(pgs->vald,pPriv->CurrParam.obs_id) ;
            strcpy(pgs->vale,pPriv->CurrParam.dhs_write) ;
            strcpy(pgs->valf,pPriv->CurrParam.qckLk_id) ;
            strcpy(pgs->valg,pPriv->CurrParam.local_archive) ;
            strcpy(pgs->valh,pPriv->CurrParam.nod_handshake) ;
            strcpy(pgs->valo,pPriv->CurrParam.archive_path) ;
            strcpy(pgs->valp,pPriv->CurrParam.archive_file_name) ;
            strcpy(pgs->valq,pPriv->CurrParam.comment) ;
            strcpy(pgs->valr,pPriv->CurrParam.remote_archive) ;
            strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host) ;
            strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path) ;
            strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name) ;

            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          }
	  else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            if (pPriv->socfd > 0) {
	      if( _verbose )
		{
		  sprintf(_UFerrmsg, __HERE__ "> sending to agent, genSub record: %s",pgs->name); 
		  ufLog( _UFerrmsg ) ;
		}

	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      sprintf(_UFerrmsg, __HERE__ "> send socfd bad? genSub record: %s",pgs->name); 
	      ufLog( _UFerrmsg ) ;
	      status = 0;
	    } 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message and go to DONE state*/
               strcpy (pPriv->errorMessage, "Bad socket connection DC3");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {  /* num_str <= 0 */
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input DC3");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }
      }
     
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;

      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J field
	 * from the Agent unless the agent is late with something
	 * Log a message that the agent responded unexpectedly
	 */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      
      if ( (strcmp(pgs->a,"ABORT") == 0) || (strcmp(pgs->a,"STOP") == 0)) { 
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }

	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        cancelCallback (pPriv->pCallback);
        strcpy(pPriv->dcAcqCont_inp.command,pgs->a) ;
        strcpy(pgs->a,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;   
        pPriv->commandState = TRX_GS_SENDING;    
        return OK ;
      } 

      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) {
        trx_debug(response,pgs->name,"MID",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        if (!pPriv->observing) {
          ticksNow = tickGet() ;
          if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
               (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "DC Acq Command Timed out");
            
            status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }          
          } else {
            /* Quit playing with the buttons. Can't you see I am BUSY? */
            trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
	} else { /* The observation timed out pPriv->obs_time_out */
          ticksNow = tickGet() ;
          if ( (ticksNow >= (pPriv->startingTicks + pPriv->obs_time_out)) &&
               (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
            pPriv->commandState = TRX_GS_DONE;

            strcpy (pPriv->errorMessage, "Observation Timed out");
            status = dbPutField(&pPriv->ObserveCcarinfo.carMessage,DBR_STRING,pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

          } else {
            /* Quit playing with the buttons. Can't you see I am BUSY? */
            trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }      
      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then cancel the call back go to DONE state and set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          pPriv->observing = 0 ;
          /* update the output links */
          strcpy(pgs->valc,pPriv->CurrParam.command);
          strcpy(pgs->vald,pPriv->CurrParam.obs_id);
          strcpy(pgs->vale,pPriv->CurrParam.dhs_write);
          strcpy(pgs->valf,pPriv->CurrParam.qckLk_id);
          strcpy(pgs->valg,pPriv->CurrParam.local_archive);
          strcpy(pgs->valh,pPriv->CurrParam.nod_handshake);
          strcpy(pgs->valo,pPriv->CurrParam.archive_path);
          strcpy(pgs->valp,pPriv->CurrParam.archive_file_name);
          strcpy(pgs->valq,pPriv->CurrParam.comment);
          strcpy(pgs->valr,pPriv->CurrParam.remote_archive);
          strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host);
          strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path);
          strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name);

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }

          if ( (strcmp(pPriv->CurrParam.command,"ABORT") == 0) ||
               (strcmp(pPriv->CurrParam.command,"STOP") == 0))  {
            pPriv->observing = 0 ;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
          if (strcmp(pPriv->CurrParam.command,"START") == 0) {
            pPriv->commandState = TRX_GS_BUSY;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            some_num = CAR_BUSY ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            /* Get the exposure time and setup a call back*/
            strcpy(obs_mode,pgs->p) ;
            if (strcmp(pgs->q,"") != 0) onSrc_time = strtod(pgs->q, &endptr) ;
            if (strcmp(pgs->r,"") != 0) ChopDutyCycle = strtod(pgs->r, &endptr)/100.0 ;
            if (strcmp(pgs->s,"") != 0) NodDutyCycle = strtod(pgs->s, &endptr)/100.0 ;
            if (strcmp(pgs->t,"") != 0) FrameDutyCycle = strtod(pgs->t, &endptr)/100.0 ;

            if (strcmp(obs_mode,"stare") == 0) Total_Time = onSrc_time ; 

            if (strcmp(obs_mode,"chop-nod") == 0) { 
              if ( (ChopDutyCycle != 0) && (NodDutyCycle != 0) ) 
                Total_Time = 2 * onSrc_time / ChopDutyCycle / NodDutyCycle / FrameDutyCycle;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zero DC1\n") ;
	    }
            if (strcmp(obs_mode,"chop") == 0) { 
              if ( (ChopDutyCycle != 0) ) Total_Time = 2*onSrc_time/ChopDutyCycle/FrameDutyCycle;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zero DC2\n") ;
            }
            if (strcmp(obs_mode,"nod") == 0) { 
              if ( (NodDutyCycle != 0) ) Total_Time = onSrc_time / NodDutyCycle / FrameDutyCycle ;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zero DC3 \n") ;
            }
            /* printf ("@@@@@@@@ Total experiment time is %f\n",Total_Time) ;  */
            /* Calculate Time out period */
            /*
            if (Total_Time > 50.0) pPriv->obs_time_out = (long)((Total_Time + 5.0)*3600.0) ; 
            else { 
              if (Total_Time < 8.0) pPriv->obs_time_out = (long)((Total_Time + 1.0)*3600.0) ; 
              else pPriv->obs_time_out = (long) ((Total_Time + 3.0)*3600.0) ; 
            } 
            */
            pPriv->observing = 1 ;            
            /* update output links */
            strcpy(pgs->valj,pgs->p) ;
            sprintf(temp_str,"%7.3f",Total_Time) ;
            strcpy(pgs->valk,temp_str) ;
            strcpy(pgs->vall,pgs->q) ;
            strcpy(pgs->valm,pgs->r) ;
            strcpy(pgs->valn,pgs->s) ;
            strcpy(pgs->valr,pgs->t) ;
            strcpy(pPriv->errorMessage, "CALLBACK") ;
            /* requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ; */
            pPriv->obs_time_out = (long) 3600 ;
            requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ;
            pPriv->startingTicks = tickGet() ;
            /* Stay in a busy state */
            pPriv->commandState = TRX_GS_BUSY;
          }
        } else { 
          /* do we have and end of observation ? */
          if (strcmp(response,"COMPLETE") == 0) {
            cancelCallback (pPriv->pCallback);
            pPriv->observing = 0 ;
            pPriv->commandState = TRX_GS_DONE; 
            
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else {    /* else do an update if you can and exit */
            numnum = strtod(response,&endptr) ;
            if (*endptr != '\0'){ /* we do not have a number */
              cancelCallback (pPriv->pCallback);
              pPriv->commandState = TRX_GS_DONE;
              /* restore the old parameters */
              if (pPriv->observing) {
                strcpy (pPriv->errorMessage, response);
                status = dbPutField( &pPriv->ObserveCcarinfo.carMessage, DBR_STRING,
				     pPriv->errorMessage, 1 );
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else {
                strcpy(pPriv->CurrParam.command,pPriv->OldParam.command) ;
                strcpy(pPriv->CurrParam.obs_id,pPriv->OldParam.obs_id) ;
                strcpy(pPriv->CurrParam.dhs_write,pPriv->OldParam.dhs_write) ;
                strcpy(pPriv->CurrParam.qckLk_id,pPriv->OldParam.qckLk_id) ;
                strcpy(pPriv->CurrParam.local_archive,pPriv->OldParam.local_archive) ;
                strcpy(pPriv->CurrParam.nod_handshake,pPriv->OldParam.nod_handshake) ;
                strcpy(pPriv->CurrParam.archive_path,pPriv->OldParam.archive_path) ;
                strcpy(pPriv->CurrParam.archive_file_name,pPriv->OldParam.archive_file_name) ;
                strcpy(pPriv->CurrParam.comment,pPriv->OldParam.comment) ;
                strcpy(pPriv->CurrParam.remote_archive,pPriv->OldParam.remote_archive) ;
                strcpy(pPriv->CurrParam.rem_archive_host,pPriv->OldParam.rem_archive_host) ;
                strcpy(pPriv->CurrParam.rem_archive_path,pPriv->OldParam.rem_archive_path) ;
                strcpy(pPriv->CurrParam.rem_archive_file_name,pPriv->OldParam.rem_archive_file_name);

                /* set the CAR to ERR with whatever the Agent sent you */
                strcpy (pPriv->errorMessage, response);
                /* set the CAR to ERR */
                status = dbPutField(&pPriv->carinfo.carMessage,DBR_STRING,pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            } else {
              if (strcmp(pgs->vali,response) != 0) {
                cancelCallback (pPriv->pCallback);
                requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ;
                /* printf("Requesting a Callback with %ld ticks for frame count of %s\n",
                   pPriv->obs_time_out,response) ; */
                pPriv->startingTicks = tickGet() ;
                strcpy (pgs->vali,response) ;
              } else {
                strcpy (pgs->vali,response) ;
              }
            }
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  printf("ufacqControlGproc> Exiting \n");
  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/***************** Unpacks array of strings ***************/
long unpack_meta_param (genSubRecord *pgs, char *zz, char **remainder) {

  char yy[40] ;
  char *original ;
  int i;
  original = zz ; 

  debugPGS( __HERE__, pgs ) ;

  strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */

  for( i=0; i<25; i++ ) {
    zz = zz+40;
    strcpy(meta_phys_parm[i],zz);
  }

  if (bingo) for (i=0;i<25;i++) printf("Meta item # %d is: %s\n",i,meta_phys_parm[i]) ;
  return OK ;
}

/***************** INAM for DCMetaOutG    *****************/
long ufDCMetaOutGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy (pgs->a,"") ;
  pgs->noa = 28 ;
  
  return OK ;
}

/***************** SNAM for DCMetaOutG    *****************/
long ufDCMetaOutGproc (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  if (strcmp(pgs->a,"") != 0) {
    unpack_meta_param (pgs, (char *)pgs->a,(char **)meta_phys_parm ) ;
    pgs->novu = 25;
    pgs->valu = meta_phys_parm[0] ;
    strcpy (pgs->a,"") ;
    pgs->noa = 28 ;
  }
  return OK ;
}

/******************** INAM for ObsControlG *******************/
long ufobsControlGinit (genSubRecord *pgs)
{
  genSubDCObsContPrivate *pPriv ;
  char buffer[MAX_STRING_SIZE];
  long status = OK;

  debugPGS( __HERE__, pgs ) ;

  /* Create a private control structure for this gensub record  */

  pPriv = (genSubDCObsContPrivate *) malloc (sizeof(genSubDCObsContPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* Locate associated CAR record fields and save their addresses in pPriv */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");
  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");
  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* Assign private information needed */
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE; /* Sim. Mode. Start in real mode */
  pPriv->CurrParam.expTime         = -1;
  pPriv->CurrParam.nonDestReadouts = -1;
  pPriv->CurrParam.numberCoadds    = -1;
  pPriv->CurrParam.numberReads     = -1;
  strcpy(pPriv->CurrParam.telDitherMode,"");
  pPriv->CurrParam.nodSettleTime = -1;
  pPriv->CurrParam.offSettleTime = -1;
  pPriv->CurrParam.nodDistance   = -1.0;
  pPriv->CurrParam.nodPA         = -1.0;

  /* Clear the Gensub inputs */
  *(long *)pgs->a   = -1;
  *(long *)pgs->b   = -1;
  *(long *)pgs->c   = -1;
  *(long *)pgs->d   = -1;
  *(long *)pgs->e   = -1;
  strcpy((char *)pgs->f,"");
  *(long *)pgs->g   = -1;
  *(long *)pgs->h   = -1;
  *(double *)pgs->i = -1.0;
  *(double *)pgs->k = -1.0;
  pgs->nou = 8 ;
 
  /* Create a callback for this gensub record */
  
  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;

  pPriv->dcObsCont_inp.command_mode = -1;    
  pPriv->dcObsCont_inp.expTime         = -1;
  pPriv->dcObsCont_inp.nonDestReadouts = -1;
  pPriv->dcObsCont_inp.numberCoadds    = -1;
  pPriv->dcObsCont_inp.numberReads     = -1;
  strcpy(pPriv->dcObsCont_inp.telDitherMode,"");
  pPriv->dcObsCont_inp.nodSettleTime = -1;
  pPriv->dcObsCont_inp.offSettleTime = -1;
  pPriv->dcObsCont_inp.nodDistance   = -1.0;
  pPriv->dcObsCont_inp.nodPA         = -1.0;

  pPriv->paramLevel = 'M' ;
  
  pgs->dpvt = (void *) pPriv;

  pgs->noj = 28 ;
  
  return OK ;
}

/******************** SNAM for ObsControlG *******************/
long ufobsControlGproc (genSubRecord *pgs)
{
  genSubDCObsContPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;

  debugPGS( __HERE__, pgs ) ;

  pPriv = (genSubDCObsContPrivate *)pgs->dpvt ;

  printf("ufobsControlGproc commandState %d \n",(int)pPriv->commandState);

  /* How the record is processed depends on the curr command execution state. */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      if (flam_initialized != 1)  init_flam_config();
      if (!dc_initialized)        init_dc_config();

      /* Clear outpus A-J for temp Gensub */
      *(long *)pgs->vala = SIMM_NONE; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1;        /* socket Number 			   */
      *(long *)pgs->valc = -1;        
      *(long *)pgs->vald = -1;        
      *(long *)pgs->vale = -1;        
      *(long *)pgs->valf = -1;        
      strcpy((char *)pgs->valg,"");
      *(long *)pgs->valh = -1;        
      *(long *)pgs->vali = -1;        
      *(long *)pgs->valj = -1.0;        
      *(long *)pgs->valk = -1.0;        

      trx_debug("Attempting to establish a connection to the agent",
				pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      pPriv->socfd =  -1;
      *(long *)pgs->valb = (long)pPriv->socfd; /* current socket number */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", 
			pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
      } 
      pPriv->commandState = TRX_GS_DONE;
      break;

    /*
     *  In idle state we wait for one of the inputs to change indicating that a
     *  new command has arrived. Status and updating from the Agent comes 
     *  during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE);
      trx_debug("Getting inputs A-I",pgs->name,"FULL",dc_DEBUG_MODE);

      pPriv->dcObsCont_inp.command_mode    = *(long *)pgs->a;
      pPriv->dcObsCont_inp.expTime         = *(long *)pgs->b;
      pPriv->dcObsCont_inp.nonDestReadouts = *(long *)pgs->c;
      pPriv->dcObsCont_inp.numberCoadds    = *(long *)pgs->d;
      pPriv->dcObsCont_inp.numberReads     = *(long *)pgs->e;
      strcpy(pPriv->dcObsCont_inp.telDitherMode,pgs->f);
      pPriv->dcObsCont_inp.nodSettleTime = *(long *)pgs->g;
      pPriv->dcObsCont_inp.offSettleTime = *(long *)pgs->h;
      pPriv->dcObsCont_inp.nodDistance   = *(double *)pgs->i;
      pPriv->dcObsCont_inp.nodPA         = *(double *)pgs->j;

      if ( (pPriv->dcObsCont_inp.expTime != -1) ||
           (pPriv->dcObsCont_inp.nonDestReadouts != -1) ||
           (pPriv->dcObsCont_inp.numberCoadds != -1) ||
           (pPriv->dcObsCont_inp.numberReads != -1) ||
           (strcmp(pPriv->dcObsCont_inp.telDitherMode,"") != 0) ||
           (pPriv->dcObsCont_inp.nodSettleTime != -1) ||
           (pPriv->dcObsCont_inp.offSettleTime != -1) ||
           (pPriv->dcObsCont_inp.nodDistance != -1.0) ||
           (pPriv->dcObsCont_inp.nodPA != -1.0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
				DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }

        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE);
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY );
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE);

        *(long *)pgs->a = -1 ; 
        *(long *)pgs->b = -1;
        *(long *)pgs->c = -1;
        *(long *)pgs->d = -1;
        *(long *)pgs->e = -1;
        strcpy((char *)pgs->f,"");
        *(long *)pgs->g   = -1;
        *(long *)pgs->h   = -1;
        *(double *)pgs->i = -1.0;
        *(double *)pgs->k = -1.0;

      } else {   /* Check to see if the agent response input has changed. */
        strcpy(response,pgs->j) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          trx_debug("Unexpected response from Agent:DONE",
			pgs->name,"NONE",dc_DEBUG_MODE);
        } else {
          /* OK. So someone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           */
         trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE);
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those values are PUSHED to the
	   * GENSUB. So to fix that, the CAD's are programmed to write clear 
	   * values (-1, 0 or "") on their output links when they receive an 
	   * empty string as input and the START directive is executed.
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 28 ;
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE);
      /* is this a processing with STOP or ABORT? 
       * if so then send the abort command to the Agent 
       * Is this a CALLBACK to send a command or do an INIT? 
       * clear the output link 
       */
      strcpy(pgs->valu,"");
      if (pPriv->dcObsCont_inp.command_mode != -1) { 
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;

        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->dcObsCont_inp.command_mode == SIMM_NONE) ||
            (pPriv->dcObsCont_inp.command_mode == SIMM_FAST) ||
            (pPriv->dcObsCont_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      ufLog( __HERE__ "> socket is writable, no need to reconnect");
	    } 
	    else {
              trx_debug("Closing current connection",
				pgs->name,"FULL",dc_DEBUG_MODE);
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd; /* curr. socket number */
	    }
	  }
          /* simulation level */
          pPriv->CurrParam.command_mode = pPriv->dcObsCont_inp.command_mode; 
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;

          /* Re-Connect if needed */
          pPriv->port_no=read_dc_file(pgs->name,
			(genSubCommPrivate *)pPriv, pPriv->paramLevel );

          if (pPriv->dcObsCont_inp.command_mode != SIMM_FAST) {
  	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",
				pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd=UFGetConnection(pgs->name,pPriv->port_no,dc_host_ip);
              trx_debug("came back from connect",
				pgs->name,"FULL",dc_DEBUG_MODE) ;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* curr. socket numb */
	    }

            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",
				pgs->name,"NONE",dc_DEBUG_MODE);
                return OK;
              }
              some_num = CAR_ERROR ;
              status=dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->dcObsCont_inp.command_mode == SIMM_FAST) || 
	       (pPriv->socfd > 0) ) {

            /* update the output links */
            *(long *)pgs->valc = pPriv->CurrParam.expTime;
            *(long *)pgs->vald = pPriv->CurrParam.nonDestReadouts;
            *(long *)pgs->vale = pPriv->CurrParam.numberCoadds;
            *(long *)pgs->valf = pPriv->CurrParam.numberReads;
            strcpy(pgs->valg,pPriv->CurrParam.telDitherMode) ;
            *(long *)pgs->valh = pPriv->CurrParam.nodSettleTime;
            *(long *)pgs->vali = pPriv->CurrParam.offSettleTime;
            *(double *)pgs->valj = pPriv->CurrParam.nodDistance;
            *(double *)pgs->valk = pPriv->CurrParam.nodPA;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
        } else {
          if (pPriv->dcObsCont_inp.command_mode == SIMM_VSM) {

             /* set simulation level */
             pPriv->CurrParam.command_mode=pPriv->dcObsCont_inp.command_mode; 
             pPriv->port_no=read_dc_file(pgs->name,
				(genSubCommPrivate *)pPriv, pPriv->paramLevel );

             pPriv->commandState = TRX_GS_DONE;
             strcpy (pPriv->errorMessage, "");
             status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
             if (status) {
               trx_debug("can't set CAR message",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
               return OK;
             }
             some_num = CAR_IDLE;
             status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
             if (status) {
               trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
             }
             return OK;
	  }
	  else {
             pPriv->commandState = TRX_GS_DONE;
             strcpy (pPriv->errorMessage, "Bad INIT Directivei DC4");
             status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
             if (status) {
               trx_debug("can't set CAR message",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
               return OK;
             }
             some_num = CAR_ERROR;
             status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
             if (status) {
               trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE);
               return OK;
             }
	  }
        }
      } else { /* We have a configuration command */
        trx_debug("We have a ObsControl Command",
			pgs->name,"FULL",dc_DEBUG_MODE) ;

        if (pPriv->CurrParam.command_mode == SIMM_VSM) {
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "");
            status = dbPutField (&pPriv->carinfo.carMessage,
                               DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE);
              return OK;
            }
            some_num = CAR_IDLE;
            status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
            }
            return OK;
        }

        /* Save the current parameters */
        pPriv->OldParam.expTime         = pPriv->CurrParam.expTime;
        pPriv->OldParam.nonDestReadouts = pPriv->CurrParam.nonDestReadouts;
        pPriv->OldParam.numberCoadds    = pPriv->CurrParam.numberCoadds;
        pPriv->OldParam.numberReads     = pPriv->CurrParam.numberReads;
        strcpy(pPriv->OldParam.telDitherMode,pPriv->CurrParam.telDitherMode);
        pPriv->OldParam.nodSettleTime   = pPriv->CurrParam.nodSettleTime;
        pPriv->OldParam.offSettleTime   = pPriv->CurrParam.offSettleTime;
        pPriv->OldParam.nodDistance     = pPriv->CurrParam.nodDistance;
        pPriv->OldParam.nodPA           = pPriv->CurrParam.nodPA;

        /* get the input into the current parameters */

        if (pPriv->dcObsCont_inp.expTime != -1)  
          pPriv->CurrParam.expTime = pPriv->dcObsCont_inp.expTime;

        if (pPriv->dcObsCont_inp.nonDestReadouts != -1)  
          pPriv->CurrParam.nonDestReadouts=pPriv->dcObsCont_inp.nonDestReadouts;

        if (pPriv->dcObsCont_inp.numberCoadds != -1)  
          pPriv->CurrParam.numberCoadds = pPriv->dcObsCont_inp.numberCoadds;

        if (pPriv->dcObsCont_inp.numberReads != -1)  
          pPriv->CurrParam.numberReads = pPriv->dcObsCont_inp.numberReads;

        if (strcmp(pPriv->dcObsCont_inp.telDitherMode,"") != 0)  
          strcpy(pPriv->CurrParam.telDitherMode,
				pPriv->dcObsCont_inp.telDitherMode);

        if (pPriv->dcObsCont_inp.nodSettleTime != -1)  
          pPriv->CurrParam.nodSettleTime = pPriv->dcObsCont_inp.nodSettleTime;

        if (pPriv->dcObsCont_inp.offSettleTime != -1)  
          pPriv->CurrParam.offSettleTime = pPriv->dcObsCont_inp.offSettleTime;

        if (pPriv->dcObsCont_inp.nodDistance != -1.0)  
          pPriv->CurrParam.nodDistance = pPriv->dcObsCont_inp.nodDistance;

        if (pPriv->dcObsCont_inp.nodPA != -1.0)  
          pPriv->CurrParam.nodPA = pPriv->dcObsCont_inp.nodPA;

        /* formulate the command strings */
        com = malloc(70*sizeof(char *)) ;
        for (i=0;i<70;i++) com[i] = malloc(70*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;

        num_str=UFcheck_dc_Obs_inputs( pPriv->dcObsCont_inp,com, 
			pPriv->paramLevel,rec_name,&pPriv->CurrParam,
			pPriv->CurrParam.command_mode);

        /* Clear the input structure */
        pPriv->dcObsCont_inp.command_mode = -1  ;
  	pPriv->dcObsCont_inp.expTime         = -1;
  	pPriv->dcObsCont_inp.nonDestReadouts = -1;
  	pPriv->dcObsCont_inp.numberCoadds    = -1;
  	pPriv->dcObsCont_inp.numberReads     = -1;
  	strcpy(pPriv->dcObsCont_inp.telDitherMode,"");
  	pPriv->dcObsCont_inp.nodSettleTime = -1;
  	pPriv->dcObsCont_inp.offSettleTime = -1;
  	pPriv->dcObsCont_inp.nodDistance   = -1.0;
  	pPriv->dcObsCont_inp.nodPA         = -1.0;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* it is using SIMM_FAST or commands that do not need the agent */

          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<70;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */
            *(long *)pgs->valc = pPriv->CurrParam.expTime;
            *(long *)pgs->vald = pPriv->CurrParam.nonDestReadouts;
            *(long *)pgs->vale = pPriv->CurrParam.numberCoadds;
            *(long *)pgs->valf = pPriv->CurrParam.numberReads;
            strcpy(pgs->valg,pPriv->CurrParam.telDitherMode) ;
            *(long *)pgs->valh = pPriv->CurrParam.nodSettleTime;
            *(long *)pgs->vali = pPriv->CurrParam.offSettleTime;
            *(double *)pgs->valj = pPriv->CurrParam.nodDistance;
            *(double *)pgs->valk = pPriv->CurrParam.nodPA;

            some_num = CAR_IDLE ;
            status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            if (pPriv->socfd > 0) {
	      if( _verbose )
		{
		sprintf(_UFerrmsg, __HERE__ ">sending to agent, genSub rec: %s",
					pgs->name); 
		ufLog( _UFerrmsg ) ;
		}
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      sprintf(_UFerrmsg, __HERE__ ">send socfd bad? , genSub rec: %s",
					pgs->name); 
	      ufLog( _UFerrmsg ) ;
	      status = 0;
	    } 
            for (i=0;i<70;i++) free(com[i]);
            free(com);
            if (status == 0) { /* error sending the commands to the agent */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Bad socket connection DC4");
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status=dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { 				/* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;

              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {
          for (i=0;i<70;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;

            strcpy (pPriv->errorMessage, "Error in input DC4");
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            trx_debug("Unexpected processing:SENDING",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }
      }
      
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;

      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J field
	 * from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 28 ;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */

      strcpy(response,pgs->j);
      if (strcmp(response,"") != 0) 
        trx_debug(response,pgs->name,"NONE",dc_DEBUG_MODE) ;
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "DC Obs Command Timed out");
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",
				pgs->name,"NONE",dc_DEBUG_MODE);
        }

      } else {
        /* What do we have from the Agent ? 
         * if Agent is done then cancel the call back go to DONE state and 
	 * set CAR to IDLE 
	 */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
         
          /* update the output links */
          *(long *)pgs->valc = pPriv->CurrParam.expTime;
          *(long *)pgs->vald = pPriv->CurrParam.nonDestReadouts;
          *(long *)pgs->vale = pPriv->CurrParam.numberCoadds;
          *(long *)pgs->valf = pPriv->CurrParam.numberReads;
          strcpy(pgs->valg,pPriv->CurrParam.telDitherMode) ;
          *(long *)pgs->valh = pPriv->CurrParam.nodSettleTime;
          *(long *)pgs->vali = pPriv->CurrParam.offSettleTime;
          *(double *)pgs->valj = pPriv->CurrParam.nodDistance;
          *(double *)pgs->valk = pPriv->CurrParam.nodPA;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */

            pPriv->CurrParam.expTime         = pPriv->OldParam.expTime;
            pPriv->CurrParam.nonDestReadouts = pPriv->OldParam.nonDestReadouts;
            pPriv->CurrParam.numberCoadds    = pPriv->OldParam.numberCoadds;
            pPriv->CurrParam.numberReads     = pPriv->OldParam.numberReads;
            strcpy(pPriv->CurrParam.telDitherMode,
						pPriv->OldParam.telDitherMode);
            pPriv->CurrParam.nodSettleTime   = pPriv->OldParam.nodSettleTime;
            pPriv->CurrParam.offSettleTime   = pPriv->OldParam.offSettleTime;
            pPriv->CurrParam.nodDistance     = pPriv->OldParam.nodDistance;
            pPriv->CurrParam.nodPA           = pPriv->OldParam.nodPA;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage,
					DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE);
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField(&pPriv->carinfo.carState,DBR_LONG,&some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",
				pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else {
            /* send the output to the output Gensub */
            assign_adj_meta_param( pgs, (char *)pgs->j);
            pgs->novu = 28;
            pgs->valu = adjMetaParm[0];
	    pgs->noj = 28;
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"");
      pgs->noj = 28;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

void parse_bias_values(char *in_str, long *cont_val, long *default_val, long *min_val, long *max_val)
{
  char *endptr ;
  sprintf( _UFerrmsg, __HERE__ "> Bias string=%s", in_str );
  ufLog( _UFerrmsg ) ;
  *cont_val = strtol(in_str,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *default_val = strtol(endptr,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *min_val = strtol(endptr,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *max_val = strtol(endptr,&endptr,10) ;

  sprintf( _UFerrmsg, __HERE__ "> Bias values parsed are %ld, %ld, %ld, %ld",
	   *cont_val,
	   *default_val,
	   *min_val,
	   *max_val) ;
  ufLog( _UFerrmsg ) ;
}

long ufDCBiasGinit (genSubRecord *pgs)
{
  /* Gensub Private Structure Declaration */
  genSubDCBiasPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;
  int i; 
  
  debugPGS( __HERE__, pgs ) ;

  /* Create a private control structure for this gensub record */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCBiasPrivate *) malloc (sizeof(genSubDCBiasPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*Locate associated CAR record fields and save their addresses in the private structure  */
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* Assign private information needed */
  pPriv->agent = 3; /* dc agent */

  for (i = 0;i<NVBIAS;i++) {
    pPriv->CurrParam.M[i] = -1.0 ;
    pPriv->CurrParam.B[i] = -1.0 ;
    pPriv->CurrParam.dac_volts[i] = -999999.0 ;
    pPriv->CurrParam.dac_controls[i] = -1 ;
  }

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */

  strcpy(pPriv->CurrParam.power,"") ;
  strcpy(pPriv->CurrParam.vGate,"") ;
  strcpy( pPriv->CurrParam.vWell, "" ) ;
  strcpy( pPriv->CurrParam.vBias, "" ) ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* command mode  */
  *(long *)pgs->b = -1 ;
  strcpy(pgs->c,"") ;
  *(long *)pgs->d = -1 ;
  *(long *)pgs->e = -1 ;
  *(double *)pgs->f = -1.0 ;
  *(long *)pgs->g = -1 ;
  *(long *)pgs->h = -1 ;
   strcpy(pgs->i,"") ;
   *(long *)pgs->k = -1 ;
   strcpy(pgs->l,"") ;
   strcpy(pgs->m,"") ;
   strcpy(pgs->n,"") ;
   strcpy(pgs->o,"") ;

  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->bias_inp.command_mode = -1 ;
  pPriv->bias_inp.datum_directive = -1 ;
  strcpy(pPriv->bias_inp.power,"") ;
  pPriv->bias_inp.park_directive = -1 ;
  pPriv->bias_inp.dac_id = -1 ;
  pPriv->bias_inp.dac_volt = -99999.99 ;
  pPriv->bias_inp.latch_dac_id = -1;
  pPriv->bias_inp.read_all_directive = -1 ;
  strcpy(pPriv->bias_inp.vGate,"") ;
  strcpy( pPriv->bias_inp.vWell, "" ) ;
  strcpy( pPriv->bias_inp.vBias, "" ) ;
  strcpy(pPriv->bias_inp.password,"");
  pPriv->bias_inp.DetTemp = -1;
  strcpy(pPriv->bias_inp.PassEnable,"") ;

  /* Save the private control structure in the record's device private field. */
  pgs->noj = NVBIAS ; 
  pgs->dpvt = (void *) pPriv;

  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/* reads dc_bias_param.txt file and returns the port # given by file */

long read_Bias_file( char *rec_name, genSubDCBiasPrivate *pPriv )
{
  FILE *biasFile = 0 ;
  char in_str[90] ;
  float dac_volt,  dac_M ;
  long dac_B;
  int i ;
  long dacVal, dc_port_no = 0;
  long bias_num, latch_order ;
  double temp_double ;

  sprintf( _UFerrmsg, __HERE__ "> rec_name: %s", rec_name ) ;
  ufLog( _UFerrmsg ) ;

  biasFile = fopen( dc_bias_filename, "r" ) ;
  if( NULL == biasFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s", dc_bias_filename ) ;
      ufLog( _UFerrmsg ) ;
      return -1 ;
    }

  /* Read the host IP number */
  NumFiles++ ;
  fgets(in_str,90,biasFile);  /* comment line */
  fgets(in_str,90,biasFile);  /* comment line */
  fgets(in_str,90,biasFile);  /* comment line */
  fgets(in_str,90,biasFile) ; /* IP number */

  strcpy (dc_host_ip,in_str) ;
  i = 0;
  while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
  dc_host_ip[i] = '\0' ;
    
  /* Read the port number */
  fgets(in_str,90,biasFile); /* comment line */ 
   
  fscanf(biasFile,"%ld",&dc_port_no) ; /* port number */

  sprintf(_UFerrmsg, __HERE__ "> host IP addr: %s,  port # =%ld", dc_host_ip, dc_port_no);
  ufLog( _UFerrmsg ) ;
  fgets(in_str,90,biasFile); /* read till end of line */ 
  fgets(in_str,90,biasFile); /* comment line */ 
  fgets(in_str,90,biasFile); /* comment line */ 
  fgets(in_str,90,biasFile); /* comment line */ 

  for (i=0;i<NVBIAS;i++) {
    fgets(in_str,15,biasFile);  /* skip DAC name */
      
    fscanf(biasFile, "%ld %ld %f %f %ld", &bias_num, &latch_order, &dac_volt, &dac_M, &dac_B);
    sprintf(_UFerrmsg, __HERE__ "> %ld %ld %f %f %ld",
	    bias_num, latch_order, dac_volt, dac_M, dac_B);
    ufLog( _UFerrmsg );
    pPriv->CurrParam.M[i] = dac_M ;
    pPriv->CurrParam.B[i] = dac_B ;
    pPriv->CurrParam.dac_volts[i] = dac_volt ;

    temp_double = pPriv->CurrParam.dac_volts[i] * pPriv->CurrParam.M[i] + pPriv->CurrParam.B[i];
    dacVal = (long)temp_double;
    if( (temp_double - (double)dacVal) >= 0.5 ) dacVal++;
    if( dacVal < 0 ) dacVal = 0;
    if( dacVal > 255 ) dacVal = 255;
    pPriv->CurrParam.dac_controls[i] = dacVal ;
    pPriv->CurrParam.default_values[i] = dacVal ; /* must stay constant once computed */
      
    fgets(in_str,90,biasFile); /* read till end of line */ 
  }

  fclose(biasFile) ; 
  NumFiles-- ;

  return dc_port_no;
}

/******************** SNAM for DCBiasG *******************/
long ufDCBiasGproc (genSubRecord *pgs) {

  genSubDCBiasPrivate *pPriv;
  long status = OK;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  long timeout ;
  double numnum ;
  char *updated_volts ;
  long default_val, min_val, max_val ;

  debugPGS( __HERE__, pgs ) ;

  if (strcmp(pgs->n,"") != 0) strcpy(dc_DEBUG_MODE,pgs->n) ;

  pPriv = (genSubDCBiasPrivate *)pgs->dpvt ;

  /* How the record is processed depends on the current command execution state...  */

  switch (pPriv->commandState) {
    case TRX_GS_INIT:
      /*      ufLog( __HERE__ "> TRX_GS_INIT" ) ; */

      if (flam_initialized != 1)  init_flam_config() ;
      if (!dc_initialized)  init_dc_config() ;

      /* Clear outputs A-J for dc Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy(pgs->valc,"UNKNOWN") ; /* power */
      
      for (i=0;i<NVBIAS;i++) {
        dc_bias_M[i] = pPriv->CurrParam.M[i] ;
        dc_bias_B[i] = pPriv->CurrParam.B[i] ;
        dc_bias_volt[i] = pPriv->CurrParam.dac_volts[i] ;
        dc_bias_control[i] = pPriv->CurrParam.dac_controls[i];
      }
      
      pgs->novd = NVBIAS ;
      pgs->nove = NVBIAS ;
      pgs->novf = NVBIAS ;
      pgs->novg = NVBIAS ;

      pgs->vald = dc_bias_M ;
      pgs->vale = dc_bias_B;
      pgs->valf = dc_bias_volt ;
      pgs->valg = dc_bias_control ;
      strcpy( pgs->valh, pPriv->CurrParam.vGate ) ;
      strcpy( pgs->vali, pPriv->CurrParam.vWell ) ;
      strcpy( pgs->valj, pPriv->CurrParam.vBias ) ;

      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */

      pPriv->commandState = TRX_GS_DONE;
      break;

    /*
     *  In idle state we wait for one of the inputs to change indicating that a
     *  new command has arrived. Status and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
/*      sprintf( _UFerrmsg, __HERE__ "> (%s): TRX_GS_DONE", pgs->name ) ;
      ufLog( _UFerrmsg ) ;
*/

      /* dumpPGS( __HERE__, pgs, true ) ; */

      /* Check to see if the input has changed. */ 
      pPriv->bias_inp.command_mode = *(long *)pgs->a ;
      pPriv->bias_inp.datum_directive = *(long *)pgs->b ;
      strcpy(pPriv->bias_inp.power,pgs->c) ;
      pPriv->bias_inp.park_directive = *(long *)pgs->d ;
      pPriv->bias_inp.dac_id = *(long *)pgs->e ;
      pPriv->bias_inp.dac_volt = *(double *)pgs->f ;
      pPriv->bias_inp.latch_dac_id = *(long *)pgs->g ;
      pPriv->bias_inp.read_all_directive = *(long *)pgs->h ;
      strcpy(pPriv->bias_inp.password,pgs->i) ;
      pPriv->bias_inp.DetTemp = *(long *)pgs->k ;

      /**
       * Only process the vWell and vBias fields if they have changed.
       * Be sure to reset the field in the pPriv structure if item has not changed.
       */
      if( (*(long*) pgs->l) != -1 )
	sprintf( pPriv->bias_inp.vWell, "%ld", *(long*) pgs->l ) ;
      else
	strcpy( pPriv->bias_inp.vWell, "" ) ;

      if( (*(long*) pgs->m) != -1 )
	  sprintf( pPriv->bias_inp.vBias, "%ld", *(long*) pgs->m ) ;
      else
	  strcpy( pPriv->bias_inp.vBias, "" ) ;

      strcpy(pPriv->bias_inp.PassEnable,pgs->o) ;

      if ( ( pPriv->bias_inp.command_mode != -1 ) ||
           ( pPriv->bias_inp.datum_directive != -1 ) ||
           ( strcmp(pPriv->bias_inp.power,"") != 0 ) ||
           ( pPriv->bias_inp.park_directive != -1 ) ||
           ( pPriv->bias_inp.dac_id != -1 ) ||
           ( (long)pPriv->bias_inp.dac_volt != -1 ) ||
           ( pPriv->bias_inp.latch_dac_id != -1 ) ||
           ( pPriv->bias_inp.read_all_directive != -1 ) ||
	   ( strcmp( pPriv->bias_inp.vWell, "" ) != 0) ||
           ( strcmp(pPriv->bias_inp.password,"") != 0) ||
           ( pPriv->bias_inp.DetTemp != -1 ) ||
           ( strcmp(pPriv->bias_inp.PassEnable,"") != 0) ||
	   ( strcmp( pPriv->bias_inp.vBias, "" ) != 0 ) ) {
        /* A new command has arrived so set the CAR to busy */
	/*	  ufLog( __HERE__ "> Found a new command" ) ; */
	strcpy (pPriv->errorMessage, "");
        status = dbPutField( &pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage, 1 );
        if (status) {
	  sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to clear CAR", pgs->name ) ;
	  ufLog( _UFerrmsg ) ;
          return OK;
        }

        some_num = CAR_BUSY ;
        status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
        if (status) {
	  sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to busy", pgs->name ) ;
	  ufLog( _UFerrmsg ) ;
          return OK;
        }

        /* set up a CALLBACK after 0.5 seconds in order to send it.
	 *  set the Command state to SENDING
	 */
/*	ufLog( __HERE__ "> Setting up call back" ) ; */
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        /* Clear the Gensub inputs */
        *(long *)pgs->a = -1 ; /* command mode  */
        *(long *)pgs->b = -1 ;
        strcpy(pgs->c,"") ;
        *(long *)pgs->d = -1 ;
        *(long *)pgs->e = -1 ;
        *(double *)pgs->f = -1.0 ;
        *(long *)pgs->g = -1 ;
        *(long *)pgs->h = -1 ;
        strcpy(pgs->i,"") ;
        *(long *)pgs->k = -1 ;
	*(long*) pgs->l = -1 ;
	*(long*) pgs->m = -1 ;
        
        strcpy(pgs->o,"") ;

        strcpy(pgs->j,"") ;  /* response from the agent via CA */
                          /* or the task that receives the response via TCP/IP */
        pgs->noj = NVBIAS ; 
      }
      else {   /* Check to see if the agent response input has changed.  */
        pgs->noj = NVBIAS ;

        if (strcmp(pgs->j,"") != 0) {
          /* in this state we should not expect anything in the J field
	   * from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the GENSUB.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = NVBIAS ; 
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
/*      sprintf( _UFerrmsg, __HERE__ "> (%s) TRX_GS_SENDING,command_mode: %ld",
	       pgs->name, pPriv->bias_inp.command_mode ) ;
      ufLog( _UFerrmsg ) ;
*/
      /*
      dumpPGS( __HERE__, pgs, false ) ;
      dumpPGS( __HERE__, pgs, true ) ;
      */

      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */

      if (pPriv->bias_inp.command_mode != -1)
	{
	  /* We have an init command */
	  trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
	  /* Check first to see if we have a valid INIT directive */
	  if ((pPriv->bias_inp.command_mode == SIMM_NONE) ||
	      (pPriv->bias_inp.command_mode == SIMM_FAST) ||
	      (pPriv->bias_inp.command_mode == SIMM_FULL) ) {

	    /* need to reconnect to the agent ? -- hon */
	    if( pPriv->socfd >= 0 ) {
	      if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
		ufLog( __HERE__ "> socket is writable, no need to reconnect");
	      }
	      else {
		trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
		ufClose(pPriv->socfd) ;
		pPriv->socfd = -1;
		*(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
	    }
	    pPriv->CurrParam.command_mode = pPriv->bias_inp.command_mode; /* simulation level */
	    *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
	    /* Re-Connect if needed */
	    /* printf("My Command Mode is %ld \n",pPriv->mot_inp.command_mode) ; */
	    /* Read some initial parameters from a file */

	    if( (pPriv->port_no = read_Bias_file(pgs->name, pPriv)) > 0 ) pPriv->send_init_param=1;

	    if (pPriv->bias_inp.command_mode != SIMM_FAST) {
	      if( pPriv->socfd < 0 ) {
		trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
		pPriv->socfd = UFGetConnection( pgs->name, pPriv->port_no, dc_host_ip );
		trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
		*(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }

	      if (pPriv->socfd < 0)
		{
		  /* we have a bad socket connection */
		  pPriv->commandState = TRX_GS_DONE;
		  strcpy (pPriv->errorMessage, "Error Connecting to Agent");
		  /* set the CAR to ERR */
		  status = dbPutField (&pPriv->carinfo.carMessage, 
				       DBR_STRING, pPriv->errorMessage,1);
		  if (status) {
		    sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR",
			     pgs->name ) ;
		    ufLog( _UFerrmsg ) ;
		    return OK;
		  }
		  some_num = CAR_ERROR ;
		  status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
				       &some_num,1);
		  if (status) {
		    sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to ERROR", pgs->name );
		    ufLog( _UFerrmsg ) ;
		    return OK;
		  }
		}
	    }
	    /* update output links if connection is good or we are in SIMM_FAST*/
	    if ( (pPriv->bias_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
	      /* update the output links */

	      strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
      
	      for (i=0;i<NVBIAS;i++) {
		dc_bias_M[i] = pPriv->CurrParam.M[i] ;
		dc_bias_B[i] = pPriv->CurrParam.B[i] ;
		dc_bias_volt[i] = pPriv->CurrParam.dac_volts[i] ;
		dc_bias_control[i] = pPriv->CurrParam.dac_controls[i];
	      }

	      /*	    ufLog( __HERE__ "> Updating output links" ) ; */
	      pgs->novd = NVBIAS ;
	      pgs->nove = NVBIAS ;
	      pgs->novf = NVBIAS ;
	      pgs->novg = NVBIAS ;
	      pgs->vald = dc_bias_M ;
	      pgs->vale = dc_bias_B;
	      pgs->valf = dc_bias_volt ;
	      pgs->valg = dc_bias_control ;
	      strcpy(pgs->valh,pPriv->CurrParam.vGate) ;
	      strcpy( pgs->vali, pPriv->CurrParam.vWell ) ;
	      strcpy( pgs->valj, pPriv->CurrParam.vBias ) ;

	      /* Reset Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      some_num = CAR_IDLE ;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
				   &some_num,1);
	      if (status) {
		sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to IDLE", pgs->name );
		ufLog( _UFerrmsg ) ;
		return OK;
	      } 
	    }
	  } else {  /* we have an error in input of the init command */
	    /*	    ufLog( __HERE__ "> Error in input of init?" ) ; */

	    /* Reset Command state to DONE */
	    pPriv->commandState = TRX_GS_DONE;
	    strcpy (pPriv->errorMessage, "Bad INIT Directive DC5");
	    /* set the CAR to ERR */
	    status = dbPutField (&pPriv->carinfo.carMessage, 
				 DBR_STRING, pPriv->errorMessage,1);
	    if (status) {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR message",
		       pgs->name ) ;
	      ufLog( _UFerrmsg ) ;
	      return OK;
	    }
	    some_num = CAR_ERROR ;
	    status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
				 &some_num,1);
	    if (status) {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to ERROR", pgs->name );
	      ufLog( _UFerrmsg ) ;
	      return OK;
	    }
	  }
	}
      else       /* We have another Bias command */
	{
/*	  ufLog( __HERE__ "> We have a BIAS command" ) ; */
	  /* Save the current parameters */
	  if( strcmp(pPriv->bias_inp.password,"") != 0 ) {
	    /*	  ufLog( __HERE__ "> BIAS, password processing" ) ; */
	    /* construct the strings to send commands */
	    com = malloc(50*sizeof(char *)) ;
	    for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
	    strcpy(rec_name,pgs->name) ;
	    rec_name[strlen(rec_name)] = '\0' ;
	    strcat(rec_name,".J") ;

	    num_str = UFcheck_bias_inputs( pPriv->bias_inp, com, rec_name, &pPriv->CurrParam, 
					   pPriv->CurrParam.command_mode) ;

	    /* Clear the input structure */
	    pPriv->bias_inp.command_mode = -1 ;
	    pPriv->bias_inp.datum_directive = -1 ;
	    strcpy(pPriv->bias_inp.power,"") ;
	    pPriv->bias_inp.park_directive = -1 ;
	    pPriv->bias_inp.dac_id = -1 ;
	    pPriv->bias_inp.dac_volt = -99999.99 ;
	    pPriv->bias_inp.latch_dac_id = -1;
	    pPriv->bias_inp.read_all_directive = -1 ;
	    strcpy(pPriv->bias_inp.vGate,"") ;
	    strcpy( pPriv->bias_inp.vWell, "" ) ;
	    strcpy( pPriv->bias_inp.vBias, "" ) ;
	    strcpy(pPriv->bias_inp.password,"");
	    pPriv->bias_inp.DetTemp = -1;
	    strcpy(pPriv->bias_inp.PassEnable,"") ;
	  }
	  else {	  /* not password */
	    /*	  ufLog( __HERE__ "> BIAS, NOT a password" ) ; */

	    for (i=0;i<NVBIAS;i++) {
	      pPriv->OldParam.M[i] = pPriv->CurrParam.M[i] ;
	      pPriv->OldParam.B[i] = pPriv->CurrParam.B[i] ;
	      pPriv->OldParam.dac_volts[i] = pPriv->CurrParam.dac_volts[i] ;
	      pPriv->OldParam.dac_controls[i] = pPriv->CurrParam.dac_controls[i] ;
	    }

	    strcpy(pPriv->OldParam.power , pPriv->CurrParam.power) ;
	    strcpy(pPriv->OldParam.vGate , pPriv->CurrParam.vGate) ;
	    strcpy( pPriv->OldParam.vWell, pPriv->CurrParam.vWell ) ;
	    strcpy( pPriv->OldParam.vBias, pPriv->CurrParam.vBias ) ;

	    /* get the input into the current parameters */
	    if ( (pPriv->bias_inp.dac_id >= 0) && (pPriv->bias_inp.dac_id <=(NVBIAS-1)) )
	      {
		if (pPriv->bias_inp.dac_volt != -99999.99) 
		  {
		    pPriv->CurrParam.dac_volts[pPriv->bias_inp.dac_id] =
		      pPriv->bias_inp.dac_volt ;
		    pPriv->CurrParam.dac_controls[pPriv->bias_inp.dac_id] =  
		      pPriv->CurrParam.dac_volts[pPriv->bias_inp.dac_id]
		      * pPriv->CurrParam.M[pPriv->bias_inp.dac_id]
		      + pPriv->CurrParam.B[pPriv->bias_inp.dac_id];
		  }
	      }
	    if (strcmp(pPriv->bias_inp.power,"") != 0)  
	      strcpy(pPriv->CurrParam.power, pPriv->bias_inp.power)  ;
	    /* if (strcmp(pPriv->bias_inp.vGate,"") != 0)  
	       strcpy(pPriv->CurrParam.vGate, pPriv->bias_inp.vGate)  ; */
	    if( strcmp( pPriv->bias_inp.vWell, "" ) != 0 )
	      strcpy( pPriv->CurrParam.vWell, pPriv->bias_inp.vWell ) ;
	    if( strcmp( pPriv->bias_inp.vBias, "" ) != 0 )
	      strcpy( pPriv->CurrParam.vBias, pPriv->bias_inp.vBias ) ;

	    /* formulate the command strings */
	    com = malloc(50*sizeof(char *)) ;
	    for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
	    strcpy(rec_name,pgs->name) ;
	    rec_name[strlen(rec_name)] = '\0' ;
	    strcat(rec_name,".J") ;

	    sprintf( _UFerrmsg, __HERE__ "> (%s) rec_name: %s", pgs->name, rec_name ) ;
	    ufLog( _UFerrmsg ) ;

	    num_str = UFcheck_bias_inputs( pPriv->bias_inp, com, rec_name,
					   &pPriv->CurrParam, pPriv->CurrParam.command_mode );

	    /* Clear the input structure */
	    pPriv->bias_inp.command_mode = -1 ;
	    pPriv->bias_inp.datum_directive = -1 ;
	    strcpy(pPriv->bias_inp.power,"") ;
	    pPriv->bias_inp.park_directive = -1 ;
	    pPriv->bias_inp.dac_id = -1 ;
	    pPriv->bias_inp.dac_volt = -99999.99 ;
	    pPriv->bias_inp.latch_dac_id = -1;
	    pPriv->bias_inp.read_all_directive = -1 ;
	    strcpy(pPriv->bias_inp.vGate,"") ;
	    strcpy( pPriv->bias_inp.vWell, "" ) ;
	    strcpy( pPriv->bias_inp.vBias, "" ) ;
	  }

	  /* do we have commands to send? */
	  if (num_str > 0) {
	    /* if no Agent needed then go back to DONE */
	    /* we are talking about SIMM_FAST or commands that do not need the agent */

	    if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
	      for (i=0;i<50;i++) free(com[i]) ;
	      free(com) ;
	      /* set Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      /* update the output links */
  
	      strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
     
	      for (i=0;i<NVBIAS;i++) {
		dc_bias_M[i] = pPriv->CurrParam.M[i] ;
		dc_bias_B[i] = pPriv->CurrParam.B[i] ;
		dc_bias_volt[i] = pPriv->CurrParam.dac_volts[i] ;
		dc_bias_control[i] = pPriv->CurrParam.dac_controls[i];
	      }
 
	      pgs->novd = NVBIAS ;
	      pgs->nove = NVBIAS ;
	      pgs->novf = NVBIAS ;
	      pgs->novg = NVBIAS ;
	      pgs->vald = dc_bias_M ;
	      pgs->vale = dc_bias_B;
	      pgs->valf = dc_bias_volt ;
	      pgs->valg = dc_bias_control ;
	      strcpy(pgs->valh,pPriv->CurrParam.vGate) ;

	      /*	    ufLog( __HERE__ "> Copying CurrParam to val{i,j}" ) ; */

	      strcpy( pgs->vali, pPriv->CurrParam.vWell ) ;
	      strcpy( pgs->valj, pPriv->CurrParam.vBias ) ;

	      pPriv->send_init_param = 0 ;
	      some_num = CAR_IDLE ;
	      status = dbPutField (&pPriv->carinfo.carState,
				   DBR_LONG, &some_num, 1);
	      if (status) {
		sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to IDLE", pgs->name ) ;
		ufLog( _UFerrmsg ) ;
		return OK;
	      }
	    }
	    else {
	      /* we are in SIMM_NONE or SIMM_FULL */
	      /* See if you can send the command and go to BUSY state */
	      if (pPriv->socfd > 0) {
	        sprintf( _UFerrmsg, __HERE__ "> sending to agent, genSub record: %s", pgs->name); 
		ufLog( _UFerrmsg ) ;
		status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
	      }
	      else {
		sprintf( _UFerrmsg, __HERE__ "> send socfd bad?, genSub record: %s", pgs->name); 
		ufLog( _UFerrmsg ) ;
		status = 0;
	      } 
	      for (i=0;i<50;i++) free(com[i]) ;
	      free(com) ;
	      if (status == 0) { /* there was an error sending the commands to the agent */
		/* set Command state to DONE */
		pPriv->commandState = TRX_GS_DONE;
		/* set the CAR to ERR with appropriate message and go to DONE state*/
		strcpy (pPriv->errorMessage, "Bad socket connection DC5");
		/* set the CAR to ERR */
		status = dbPutField (&pPriv->carinfo.carMessage, 
				     DBR_STRING, pPriv->errorMessage,1);
		if (status) {
		  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
		  return OK;
		}
		some_num = CAR_ERROR ;
		status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
				     &some_num,1);
		if (status) {
		  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
		  return OK;
		}
	      } else { /* send successful */
		/* establish a CALLBACK */
		requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
		pPriv->startingTicks = tickGet() ;
		/* set Command state to BUSY */
		pPriv->commandState = TRX_GS_BUSY;
	      }
	    }
	  }
	  else { /* num_str <= 0 */
	    for (i=0;i<50;i++) free(com[i]) ;
	    free(com) ;

	    if (num_str < 0) {
	      /* set Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      /* we have an error in the Input */
	      strcpy (pPriv->errorMessage, "Error in input DC5");
	      /* set the CAR to ERR */

	      status = dbPutField (&pPriv->carinfo.carMessage, 
				   DBR_STRING, pPriv->errorMessage,1);
	      if (status) {
		sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR message", pgs->name );
		ufLog( _UFerrmsg ) ;
		return OK;
	      }
	      some_num = CAR_ERROR ;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
	      if (status) {
		sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to ERROR", pgs->name );
		ufLog( _UFerrmsg ) ;
		return OK;
	      }
	    } else { /* num_str == 0 */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
	      trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
	    }
	  } 
	}

      /*Is this a J field processing? */
      pgs->noj = NVBIAS ; 

      if (strcmp(pgs->j,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something.
	 * Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = NVBIAS ; 
      break;

    case TRX_GS_BUSY:
/*      ufLog( __HERE__ "> TRX_GS_BUSY" ) ; */
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      pgs->noj = NVBIAS ; 

      if (strcmp(pgs->j,"") != 0) {
        trx_debug(pgs->j,pgs->name,"MID",dc_DEBUG_MODE) ; 
      }
  
      if (strcmp(pgs->j,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        timeout = TRX_DC_TIMEOUT ;
        if ( (ticksNow >= (pPriv->startingTicks + timeout)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Bias Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
      }
      else { /* What do we have from the Agent? */
        /* if Agent is done then cancel the call back go to DONE state and set CAR to IDLE */
        if (strcmp(pgs->j,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the output links */
          strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
          strcpy( pgs->valh, pPriv->CurrParam.vGate ) ;
	  strcpy( pgs->vali, pPriv->CurrParam.vWell ) ;
	  strcpy( pgs->valj, pPriv->CurrParam.vBias ) ;
          pPriv->send_init_param = 0 ;
          some_num = CAR_IDLE ;
          status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(pgs->j,&endptr) ;

          if (numnum <= 0) { /*we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            for (i=0;i<NVBIAS;i++) {
              pPriv->CurrParam.M[i] = pPriv->OldParam.M[i] ;
              pPriv->CurrParam.B[i] = pPriv->OldParam.B[i] ;
              pPriv->CurrParam.dac_volts[i] = pPriv->OldParam.dac_volts[i] ;
              pPriv->CurrParam.dac_controls[i] = pPriv->OldParam.dac_controls[i] ;
            }
  
            strcpy( pPriv->CurrParam.power, pPriv->OldParam.power ) ;
            strcpy( pPriv->CurrParam.vGate, pPriv->OldParam.vGate ) ;
	    strcpy( pPriv->CurrParam.vWell, pPriv->OldParam.vWell ) ;
	    strcpy( pPriv->CurrParam.vBias, pPriv->OldParam.vBias ) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, pgs->j);
            /* set the CAR to ERR */
            status = dbPutField( &pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage, 1 );
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
	  else { /* update the out link */
            updated_volts = pgs->j ;

            for (i=0;i<NVBIAS;i++) {
              parse_bias_values( updated_volts, &pPriv->CurrParam.dac_controls[i],
				 &default_val, &min_val, &max_val );
              dc_bias_control[i] = pPriv->CurrParam.dac_controls[i]; 
              dc_bias_def[i] = default_val ;
              dc_bias_min[i] = min_val ;
              dc_bias_max[i] = max_val ;
              dc_bias_volt[i] = (pPriv->CurrParam.dac_controls[i] - dc_bias_B[i])/dc_bias_M[i] ;
              updated_volts += 40; /* epics has rearranged DC agent response into 40 char chunks */
            }

            pgs->novd = NVBIAS ;
            pgs->nove = NVBIAS ;
            pgs->novf = NVBIAS ;
            pgs->novg = NVBIAS ;
            pgs->novk = NVBIAS ;
            pgs->novl = NVBIAS ;
            pgs->novm = NVBIAS ;
 
            pgs->vald = dc_bias_M;
            pgs->vale = dc_bias_B;
            pgs->valf = dc_bias_volt;
            pgs->valg = dc_bias_control;
            pgs->valk = dc_bias_def;
            pgs->vall = dc_bias_min;
            pgs->valm = dc_bias_max;
          }
        }
      }
      pgs->noj = NVBIAS ; 
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/******************** INAM for preampG *******************/
long ufDCPreAmpGinit (genSubRecord *pgs)
{
  genSubDCPreAmpPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;
  int i; 

  debugPGS( __HERE__, pgs ) ;

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCPreAmpPrivate *) malloc (sizeof(genSubDCPreAmpPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->agent = 3; /* dc agent */

  for (i = 0;i<NVPAMP;i++) {
    pPriv->CurrParam.default_values[i] = -1 ;
    pPriv->CurrParam.dac_controls[i] = -1 ;
  }

  pPriv->CurrParam.adjust_value = 0;
  pPriv->CurrParam.global_set = -1;

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */

  strcpy(pPriv->CurrParam.power,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* command mode  */
  *(long *)pgs->b = -1 ;
  strcpy(pgs->c,"") ;
  *(long *)pgs->d = -1 ;
  *(long *)pgs->e = -1 ;
  *(double *)pgs->f = -99999.99 ;
  *(double *)pgs->g = -1 ;
  *(long *)pgs->h = -1 ;
  *(long *)pgs->k = 0 ;
  strcpy(pgs->i,"NONE") ;

  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->preAmp_inp.command_mode = -1 ;
  pPriv->preAmp_inp.datum_directive = -1 ;
  strcpy(pPriv->preAmp_inp.power,"") ;
  pPriv->preAmp_inp.park_directive = -1 ;
  pPriv->preAmp_inp.preAmp_id = -1 ;
  pPriv->preAmp_inp.preAmp_volt = -99999.99 ;
  pPriv->preAmp_inp.latch_preAmp_id = -1;
  pPriv->preAmp_inp.read_all_directive = -1 ;
  pPriv->preAmp_inp.global_set = -1;
  pPriv->preAmp_inp.adjust_value = 0;

  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
  pgs->noj = NVPAMP ; 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

long read_PreAmp_file( char *rec_name, genSubDCPreAmpPrivate *pPriv )
{
  FILE *preampFile = 0;
  char in_str[90] ;
  float dac_volt, dac_M ;
  long dac_B;
  int i ;
  long dacVal, dc_port_no = 0;
  long preamp_num, latch_order ;
  double temp_double ;
  
  ufLog( __HERE__ ) ;
  preampFile = fopen( dc_preamp_filename, "r" ) ;

  if( NULL == preampFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s", dc_preamp_filename ) ;
      ufLog( _UFerrmsg ) ;
      return -1 ;
    }

  NumFiles++ ;
  /* Read the host IP number */
  fgets(in_str,90,preampFile);  /* comment line */
  fgets(in_str,90,preampFile);  /* comment line */
  fgets(in_str,90,preampFile);  /* comment line */
  fgets(in_str,90,preampFile) ; /* IP number */

  strcpy (dc_host_ip,in_str) ;
  i = 0;
  while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
  dc_host_ip[i] = '\0' ;

  /* Read the port number */
  fgets(in_str,90,preampFile); /* comment line */ 
  fscanf(preampFile,"%ld",&dc_port_no) ; /* port number */

  sprintf(_UFerrmsg, __HERE__ "> host IP addr: %s,  port # =%ld", dc_host_ip, dc_port_no);
  ufLog( _UFerrmsg ) ;
  fgets(in_str,90,preampFile); /* read till end of line */ 
  fgets(in_str,90,preampFile); /* comment line */ 
  fgets(in_str,90,preampFile); /* comment line */ 
  fgets(in_str,90,preampFile); /* comment line */ 

  for (i=0;i<NVPAMP;i++) {
    fgets(in_str,15,preampFile);  /* dac name */
      
    fscanf(preampFile, "%ld %ld %f %f %ld",
	   &preamp_num, &latch_order, &dac_volt, &dac_M, &dac_B);
    sprintf(_UFerrmsg, __HERE__ "> %ld %ld %f %f %ld",
	    preamp_num, latch_order, dac_volt, dac_M, dac_B);
    ufLog( _UFerrmsg );
    pPriv->CurrParam.M[i] = dac_M ;
    pPriv->CurrParam.B[i] = dac_B ;
    pPriv->CurrParam.dac_volts[i] = dac_volt ;

    temp_double = pPriv->CurrParam.dac_volts[i]*pPriv->CurrParam.M[i] + pPriv->CurrParam.B[i];
    dacVal = (long)temp_double;
    if( (temp_double - (double)dacVal) >= 0.5 ) dacVal++;
    if( dacVal < 0 ) dacVal = 0;
    if( dacVal > 255 ) dacVal = 255;
    pPriv->CurrParam.dac_controls[i] = dacVal ;
    pPriv->CurrParam.default_values[i] = dacVal ; /* must stay constant once computed */
      
    fgets(in_str,90,preampFile); /* read till end of line */ 
  }

  fclose(preampFile) ; 
  NumFiles-- ;

  return dc_port_no;
}

void parse_preamp_values(char *in_str, long *cont_val )
{
  char *endptr ;
  long default_val = 0;

  sprintf(_UFerrmsg, __HERE__ "> preAmp string=%s", in_str);
  ufLog( _UFerrmsg ) ;
  *cont_val = strtol(in_str,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  default_val = strtol(endptr,&endptr,10) ;

  sprintf(_UFerrmsg, __HERE__ "> preAmp values parsed are %ld, %ld", *cont_val, default_val);
  ufLog( _UFerrmsg ) ;
}
/******************** SNAM for DCPreampG *******************/

long ufDCPreAmpGproc(genSubRecord *pgs)
{
  genSubDCPreAmpPrivate  *pPriv;
  long status = OK;
  long some_num;  
  long ticksNow ; 
  char *endptr ;
  int i, num_str = 0 ;
  static char **com ;
  char rec_name[31] ;
  long timeout ;
  double numnum ;
  char *updated_volts ;

  debugPGS( __HERE__, pgs ) ;
  strcpy(dc_DEBUG_MODE,pgs->i) ;
  pPriv = (genSubDCPreAmpPrivate *)pgs->dpvt ;
  /* 
   *  How the record is processed depends on the current command execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      if (flam_initialized != 1)  init_flam_config() ;
      if (!dc_initialized)  init_dc_config() ;

      /* Clear outpus A-J for bieas Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      pPriv->socfd = -1 ;
      *(long *)pgs->valb = -1 ; /* socket Number */
      strcpy(pgs->valc,"") ;

      for (i=0;i<NVPAMP;i++) {
        preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
        preAmp_control[i] = pPriv->CurrParam.dac_controls[i] ;
      }

      pgs->vale = preAmp_default ;
      pgs->vald = preAmp_control ;

      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */

      pPriv->commandState = TRX_GS_DONE;
      break;
    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived.
     *  Status and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
 /*     ufLog( __HERE__ "> TRX_GS_DONE" ) ; */
      dumpPGS( __HERE__, pgs, false ) ;

      pPriv->preAmp_inp.command_mode = *(long *)pgs->a ;
      pPriv->preAmp_inp.datum_directive = *(long *)pgs->b ;
      strcpy(pPriv->preAmp_inp.power,"") ;
      pPriv->preAmp_inp.park_directive = *(long *)pgs->d ;
      pPriv->preAmp_inp.preAmp_id = *(long *)pgs->e ;
      pPriv->preAmp_inp.preAmp_volt = *(double *)pgs->f ;
      pPriv->preAmp_inp.global_set = *(double *)pgs->g ;
      pPriv->preAmp_inp.read_all_directive = *(long *)pgs->h ;
      pPriv->preAmp_inp.adjust_value = *(float *)pgs->k ;

      if ( ( pPriv->preAmp_inp.command_mode != -1 ) ||
           ( pPriv->preAmp_inp.datum_directive != -1 ) ||
           ( strcmp(pPriv->preAmp_inp.power,"") != 0 ) ||
           ( pPriv->preAmp_inp.park_directive != -1 ) ||
           ( pPriv->preAmp_inp.preAmp_id != -1 ) ||
           ( pPriv->preAmp_inp.preAmp_volt > -99999 ) ||
           ( pPriv->preAmp_inp.global_set >= 0 ) ||
           ( pPriv->preAmp_inp.adjust_value != 0 ) ||
           ( pPriv->preAmp_inp.read_all_directive != -1 ) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField( &pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage, 1 );
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback( pPriv->pCallback, TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        /* Clear the Gensub inputs */
        *(long *)pgs->a = -1 ; /* command mode  */
        *(long *)pgs->b = -1 ;
        strcpy(pgs->c,"") ;
        *(long *)pgs->d = -1 ;
        *(long *)pgs->e = -1 ;
        *(double *)pgs->f = -99999.99 ;
        *(double *)pgs->g = -1 ;
        *(long *)pgs->h = -1 ;
        *(float *)pgs->k = 0 ;

        strcpy(pgs->j,"") ;  /* response from the agent via CA */
        pgs->noj = NVPAMP ; 
      }
      else {  /* Check to see if the agent response input has changed. */
        pgs->noj = NVPAMP ; 

        if (strcmp(pgs->j,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the GENSUB.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
   /*   ufLog( __HERE__ "> TRX_GS_SENDING" ) ; */

      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */

      if (pPriv->preAmp_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->preAmp_inp.command_mode == SIMM_NONE) ||
            (pPriv->preAmp_inp.command_mode == SIMM_FAST) ||
            (pPriv->preAmp_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd > 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      ufLog("ufDCPreAmpGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->preAmp_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          /* printf("My Command Mode is %ld \n",pPriv->mot_inp.command_mode) ; */
          /* Read some initial parameters from a file */

          if( (pPriv->port_no = read_PreAmp_file(pgs->name, pPriv)) > 0 ) pPriv->send_init_param=1;

          if (pPriv->preAmp_inp.command_mode != SIMM_FAST) {
  	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection( pgs->name, pPriv->port_no, dc_host_ip );
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
	      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->preAmp_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
      
            for (i=0;i<NVPAMP;i++) {
              preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
              preAmp_control[i] = pPriv->CurrParam.dac_controls[i];
            }

	    pgs->vale = preAmp_default ;
	    pgs->vald = preAmp_control ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive DC6");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      }
      else {
        trx_debug("We have a preamp Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Save the current parameters */
        for (i=0;i<NVPAMP;i++) {
          pPriv->OldParam.default_values[i] = pPriv->CurrParam.default_values[i] ;
          pPriv->OldParam.dac_controls[i] = pPriv->CurrParam.dac_controls[i] ;
        }

        /* get the input into the current parameters */
        
        if (pPriv->preAmp_inp.global_set >= 0) {
	  /* Scale the global set voltage to DAC control values */
          for (i=0 ; i<NVPAMP;i++)
	    {
	      pPriv->CurrParam.dac_controls[i] = 
		pPriv->preAmp_inp.global_set  * pPriv->CurrParam.M[i] + pPriv->CurrParam.B[i];
	      pPriv->CurrParam.dac_volts[i] = pPriv->preAmp_inp.global_set ;
	    }
        }

        if (pPriv->preAmp_inp.adjust_value != 0) {
          for (i=0 ; i<NVPAMP;i++) {
            pPriv->CurrParam.dac_volts[i] =
	      pPriv->CurrParam.dac_volts[i] + pPriv->preAmp_inp.adjust_value ;
	    pPriv->CurrParam.dac_controls[i] = 
		pPriv->CurrParam.dac_volts[i] * pPriv->CurrParam.M[i]
	      + pPriv->CurrParam.B[i];
	  }
        }

        pPriv->CurrParam.global_set = pPriv->preAmp_inp.global_set ;
        pPriv->CurrParam.adjust_value = pPriv->preAmp_inp.adjust_value ;

	if( (pPriv->preAmp_inp.preAmp_id >= 0) && (pPriv->preAmp_inp.preAmp_id <=(NVPAMP-1)) ) {
          if( pPriv->preAmp_inp.preAmp_volt > -99999 ) {
            pPriv->CurrParam.dac_volts[pPriv->preAmp_inp.preAmp_id] = pPriv->preAmp_inp.preAmp_volt;
	  }
        }

        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;

        num_str = UFcheck_preAmp_inputs (pPriv->preAmp_inp, 
					 com, 
					 rec_name, 
					 &pPriv->CurrParam, 
					 pPriv->CurrParam.command_mode) ;
        /* Clear the input structure */
        pPriv->preAmp_inp.command_mode = -1 ;
        pPriv->preAmp_inp.datum_directive = -1 ;
        strcpy(pPriv->preAmp_inp.power,"") ;
        pPriv->preAmp_inp.park_directive = -1 ;
        pPriv->preAmp_inp.preAmp_id = -1 ;
        pPriv->preAmp_inp.preAmp_volt = -99999.99 ;
        pPriv->preAmp_inp.adjust_value = 0;
        pPriv->preAmp_inp.read_all_directive = -1 ;
        pPriv->preAmp_inp.global_set = -1 ;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          /* printf("********** Thn number of strings is : %d\n",num_str) ; */

          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */

            for (i=0;i<NVPAMP;i++) {
              preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
              preAmp_control[i] = pPriv->CurrParam.dac_controls[i];
            }

            pgs->vale = preAmp_default;
	    pgs->vald = preAmp_control;

            pPriv->send_init_param = 0 ;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState,
                                 DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            if (pPriv->socfd > 0) {
	      sprintf( _UFerrmsg, __HERE__ "> sending to agent, genSub record: %s", pgs->name); 
	      ufLog( _UFerrmsg ) ;
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      sprintf( _UFerrmsg, __HERE__ "> send socfd bad? genSub record: %s", pgs->name); 
	      ufLog( _UFerrmsg ) ;
	      status = 0;
	    } 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
              strcpy (pPriv->errorMessage, "Bad socket connection DC6");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else { /* num_str <= 0 */
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input DC6");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
	}
      }
      
      /*Is this a J field processing? */
      pgs->noj = NVPAMP ; 

      if (strcmp(pgs->j,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something.
	 * Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = NVPAMP ; 
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      pgs->noj = NVPAMP ; 

      if (strcmp(pgs->j,"") != 0) {
        trx_debug(pgs->j,pgs->name,"MID",dc_DEBUG_MODE) ; 
      }
  
      if (strcmp(pgs->j,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        timeout = TRX_DC_TIMEOUT ;
        if ( (ticksNow >= (pPriv->startingTicks + timeout)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Preamp Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
      }
      else { /* What do we have from the Agent? */
        /* if Agent is done then cancel the call back go to DONE state and set CAR to IDLE */
        if (strcmp(pgs->j,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the output links ? */
          pPriv->send_init_param = 0 ;
          some_num = CAR_IDLE ;
          status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
	else { /* else do an update if you can and exit */
          numnum = strtod(pgs->j,&endptr) ;

          if (numnum <= 0) { /*we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            for (i=0;i<NVPAMP;i++) {
              pPriv->CurrParam.default_values[i] = pPriv->OldParam.default_values[i] ;
              pPriv->CurrParam.dac_controls[i] = pPriv->OldParam.dac_controls[i] ;
            }
  
            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, pgs->j);
            /* set the CAR to ERR */
            status = dbPutField( &pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage, 1 );
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField( &pPriv->carinfo.carState, DBR_LONG, &some_num, 1 );
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
	  else { /* parse response and update the out link */
            updated_volts = pgs->j ;

            for (i=0;i<NVPAMP;i++) {
              parse_preamp_values( updated_volts, &pPriv->CurrParam.dac_controls[i] );
	      preAmp_control[i] = pPriv->CurrParam.dac_controls[i];
              preAmp_M[i] = pPriv->CurrParam.M[i] ;
	      preAmp_B[i] = pPriv->CurrParam.B[i] ;
              preAmp_volt[i] = (preAmp_control[i] - preAmp_B[i])/preAmp_M[i] ;
	      sprintf(_UFerrmsg, __HERE__ "> C=%ld B=%ld M=%f V=%f",
		      preAmp_control[i], preAmp_B[i], preAmp_M[i], preAmp_volt[i] );
	      ufLog( _UFerrmsg );
              updated_volts += 40; /* epics has rearranged DC agent response into 40 char chunks */
	    }

            pgs->vale = preAmp_volt;
            pgs->vald = preAmp_control;

            pgs->novd = NVPAMP;
            pgs->nove = NVPAMP;
          }
        }
      }
      pgs->noj = NVPAMP ; 
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/****************** SNAM forTCSISnodComplG ****************/
long ufTCSISnodComplGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy(pgs->j,"") ;
  strcpy(pgs->vala,"") ;
  return OK ;
}

/****************** SNAM forTCSISnodCompl G ***************/
long ufTCSISnodComplGproc (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy(pgs->vala,pgs->j) ;
  return OK ;
}

/****************** SNAM for MetaEnvG ****************/
long ufMetaEnvGinit (genSubRecord *pgs)
{
  debugPGS( __HERE__, pgs ) ;

  strcpy ((char *)pgs->a, "-1.0");
  strcpy ((char *)pgs->b, "-1.0");
  strcpy ((char *)pgs->c, "-1.0");
  strcpy ((char *)pgs->d, "-1.0");
  strcpy ((char *)pgs->e, "-1.0");
  strcpy ((char *)pgs->f, "-1.0");
  *(long *)pgs->g = 0 ;
  strcpy ((char *)pgs->h, "-1.0");

  dc_env_param[0] =  -1.0;
  dc_env_param[1] =  -1.0;
  dc_env_param[2] =  -1.0;
  dc_env_param[3] =  -1.0;
  dc_env_param[4] =  -1.0;
  dc_env_param[5] =  -1.0;
  dc_env_param[6] =  -1.0;
  dc_env_param[7] =  -1.0;

  pgs->vala = dc_env_param ;
 
  return OK ;
}

/****************** SNAM for MetaEnvG ***************/
long ufMetaEnvGproc (genSubRecord *pgs)
{
  char *endPtr;

  debugPGS( __HERE__, pgs ) ;

  dc_env_param[0] = strtod ((char *)pgs->a, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[0] = -1.0 ;
  dc_env_param[1] = strtod ((char *)pgs->b, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[1] = -1.0 ;
  dc_env_param[2] = strtod ((char *)pgs->c, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[2] = -1.0 ;
  dc_env_param[3] = strtod ((char *)pgs->d, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[3] = -1.0 ;
  dc_env_param[4] = strtod ((char *)pgs->e, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[4] = -1.0 ;
  dc_env_param[5] = strtod ((char *)pgs->f, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[5] = -1.0 ;
  dc_env_param[6] = (double) (*(long *)pgs->g) ;
  dc_env_param[7] = strtod ((char *)pgs->h, &endPtr) ;
  if (*endPtr != '\0') dc_env_param[7] = -1.0 ;

  strcpy (pgs->valb, pgs->a) ;
  strcpy (pgs->valc, pgs->b) ;
  strcpy (pgs->vald, pgs->c) ;
  strcpy (pgs->vale, pgs->d) ;
  strcpy (pgs->valf, pgs->e) ;
  strcpy (pgs->valg, pgs->f) ;
  if (*(long *)pgs->g == 0) strcpy (pgs->valh, "FALSE") ;
  else strcpy (pgs->valh, "TRUE");
  strcpy (pgs->vali, pgs->h) ;

  return OK ;
}

/******************** SNAM for flamSeqexecCounter ******************/
long flamSeqexecCounters (genSubRecord *pgs)
{
  char buffer[40] ; 

/*  debugPGS( __HERE__, pgs ) ; */

    strcpy (buffer, pgs->b); 
    strcat (buffer, " of ");
    strcat (buffer, pgs->a);
    strcpy(pgs->vala,buffer); 

  return OK ;
}


#endif /* __UFGEMGENSUBCOMMDC_C__  */

