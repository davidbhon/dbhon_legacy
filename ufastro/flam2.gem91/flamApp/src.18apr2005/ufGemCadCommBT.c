#if !defined(__UFGEMCADCOMMBT_C__)
#define __UFGEMCADCOMMBT_C__ "RCS: $Name:  $ $Id: ufGemCadCommBT.c,v 0.0 2005/09/01 20:24:46 drashkin Exp $"
static const char rcsIdufGEMCADCOMMCBT[] = __UFGEMCADCOMMBT_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <cad.h>
#include <cadRecord.h>

#include "flam.h"
#include "ufGemComm.h"
#include "ufdbl.h"

/**************   Detector  Controller CAD's   **************/
long
btrebootCommand (cadRecord * pcr)
{
  long out_vala;
  char out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40], out_valo[40];
  char out_valp[40], out_valq[40], out_valr[40];
  char *endptr;
  long temp_long;

  out_vala = -1;
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  strcpy (out_valn, "");
  strcpy (out_valo, "");
  strcpy (out_valp, "");
  strcpy (out_valq, "");
  strcpy (out_valr, "");

  temp_long = 0;

  debugCAD( __HERE__, pcr ) ;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive BT1", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1;
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  strcpy (out_valf, pcr->f);
	  temp_long = 1;
	}

      if (strcmp (pcr->g, "") != 0)
	{
	  strcpy (out_valg, pcr->g);
	  temp_long = 1;
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);
	  temp_long = 1;
	}

      if (strcmp (pcr->o, "") != 0)
	{
	  strcpy (out_valo, pcr->o);
	  temp_long = 1;
	}

      if (strcmp (pcr->p, "") != 0)
	{
	  strcpy (out_valp, pcr->p);
	  temp_long = 1;
	}

      if (strcmp (pcr->q, "") != 0)
	{
	  strcpy (out_valq, pcr->q);
	  temp_long = 1;
	}

      if (strcmp (pcr->r, "") != 0)
	{
	  strcpy (out_valr, pcr->r);
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{
	  *(long *) pcr->vala = out_vala;
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  strcpy (pcr->valo, out_valo);
	  strcpy (pcr->valp, out_valp);
	  strcpy (pcr->valq, out_valq);
	  strcpy (pcr->valr, out_valr);

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	  strcpy (pcr->o, "");
	  strcpy (pcr->p, "");
	  strcpy (pcr->q, "");
	  strcpy (pcr->r, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


#endif /* __UFGEMCADCOMMBT_C__ */
