#if !defined(__UFGEMCOMM_C__)
#define __UFGEMCOMM_C__ "RCS: $Name:  $ $Id: ufGemComm.c,v 0.0 2005/09/01 20:25:30 drashkin Exp $"
static const char rcsIdufUFGEMCOMMC[] = __UFGEMCOMM_C__;

#include <sys/types.h>
#include <sys/socket.h>
#include <stdioLib.h>
#include <string.h>
#include <time.h>
#include <selectLib.h>
#include <sockLib.h>
#include <sys/stat.h>

#include <string.h>
#include <stdio.h>
#include <sirRecord.h>

#include "ufClient.h"
#include "ufLog.h"

#include "flam.h"
/* #include "ufdbl.h" */
#include "ufGemComm.h"

#define CAR_IDLE_Comm 0
#define CAR_ERROR_Comm 3

static int _verbose = 1;
static int namPosTableInit = 0;	/* false */

/**
 * Establish a connection to a UFAgent host, and sends a timestamp header.
 * Returns -1 on error, otherwise the newly opened socket descriptor.
 */
int
UFGetConnection (const char *name, int port_no, const char *host)
{
  /* Information to send to the agent */
  static ufProtocolHeader sendheader;
  char *temp_str = 0;
  char temp_str2[80];

  int bytesSent = 0;
  /* Integer to hold the socket number */
  int socfd = -1;

  /* construct the header */
  memset (&sendheader, 0, sizeof (ufProtocolHeader));
  strcpy (sendheader.name, name);
  sendheader.type = 0;
  sendheader.elem = 0;
  sendheader.seqCnt = 1;
  sendheader.seqTot = 1;

  strcpy (temp_str2, ufHostTime (NULL));
  temp_str = strchr (temp_str2, ':');
  temp_str = temp_str + 2;
  strcpy (sendheader.timestamp, temp_str);
  sendheader.timestamp[24] = 0;
  printf("The host/port %s %d \n",host,port_no); 

  sendheader.length = ufHeaderLength (&sendheader);
  /* attempt to establish the connection */
  socfd = ufConnect (host, port_no);

  /* send the header if we are connected */
  if (socfd < 0)
    {
      return -1;
    }

  /* Connect succeeded, attempt to send the header */
  bytesSent = ufHeaderSend (socfd, &sendheader);

  /* Was header send successful? */
  if (bytesSent < 0)
    {
      /* Error sending the header */
      close (socfd);

      /* Return an error */
      return -1;
    }

  /* All is well, return the newly connected socket */
  return socfd;
}

long
UFcheck_long (long value, long val1, long val2)
{
  long status = 0;
  if ((value != val1) && (value != val2))
    status = not_valid;
  return status;
}

long
UFcheck_long_r (long value, long loval1, long upval2)
{
  long status = 0;
  if ((value < loval1) || (value > upval2))
    status = not_valid;
  return status;
}

long
UFcheck_double (double value, double loval1, double upval2)
{
  long status = 0;
  if ((value < loval1) || (value > upval2))
    status = not_valid;
  return status;
}

int UFcheck_motor_inputs (ufMotorInput mot_inp, char **com, char indexer,
 			 char *car_name, char *sad_name, 
			ufMotorCurrParam * CurrParam, long com_mode)
{
  long temp_long;
  long tmp_steps;
  double acceleration;
  int status = 0;
  int i = 0;
  int num_str = 4;
  char temp_str[80];
  char sign;
  char command[10];


  if (!com_mode)
    sprintf (command, "%craw", indexer);
  else
    sprintf (command, "%csim", indexer);

  CurrParam->steps_to_add = 0.0;
  CurrParam->homing = 0;

  /* initital velocity */
  temp_long = (long) (mot_inp.init_velocity);
  if (temp_long != (long) link_clear1)
    {
      if( UFcheck_double( mot_inp.init_velocity, init_velocity_lo,
			  init_velocity_hi ) == not_valid )
	{
	  printf ("********** Initial velocity is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: initial velocity");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cI %6.2f", indexer, mot_inp.init_velocity);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->init_velocity = mot_inp.init_velocity;
	}
    }
  /* slew velocity */

  temp_long = (long) (mot_inp.slew_velocity);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.slew_velocity, slew_velocity_lo,
	   slew_velocity_hi) == not_valid)
	{
	  printf ("********** Slew velocity is not valid\n"); 
	  status = not_valid;
	  return status;
	}
      else
	{
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: slew velocity");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cV %6.2f", indexer, mot_inp.slew_velocity);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->slew_velocity = mot_inp.slew_velocity;
	}
    }
  /* printf("********** Acceleration\n") ; */
  /* acceleration */

  acceleration = (mot_inp.acceleration);
  if (acceleration != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.acceleration, acceleration_lo,
	   acceleration_hi) == not_valid)
	{
	  printf ("********** Acceleration is not valid\n");
	  status = not_valid;
	  return status;
	}
    }
  /* printf("********** Deceleration\n") ; */
  /* deceleration */
  temp_long = (long) (mot_inp.deceleration);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.deceleration, deceleration_lo,
	   deceleration_hi) == not_valid)
	{
	  printf ("********** Deceleration is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  if (acceleration != (long) link_clear1)
	    {
	      /* printf("********** Deceleration\n") ; */
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrSend: the acceleration");
	      strcpy (com[num_str++], temp_str);
	      sprintf (temp_str, "%cK %ld %ld", indexer, (long) acceleration,
		       (long) mot_inp.deceleration);
	      strcpy (com[num_str++], temp_str);
	      CurrParam->acceleration = mot_inp.acceleration;
	      CurrParam->deceleration = mot_inp.deceleration;
	    }
	}
    }
  /* printf("********** Drive Current\n") ; */
  /* drive current */

  temp_long = (long) (mot_inp.drive_current);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.drive_current, drive_current_lo,
	   drive_current_hi) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: the current");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cY 0 %5.2f", indexer, mot_inp.drive_current);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->drive_current = mot_inp.drive_current;
	}
    }

  /* pPriv->mot_inp.setting_time */
  temp_long = (long) (mot_inp.setting_time);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double (mot_inp.setting_time, 0, 255) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: setting time");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cE %5.2f", indexer, mot_inp.setting_time);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->setting_time = mot_inp.setting_time;
	}
    }
  /* 
     pPriv->mot_inp.jog_speed_slow
     pPriv->mot_inp.jog_speed_hi */

  temp_long = (long) (mot_inp.jog_speed_slow);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double (mot_inp.jog_speed_slow, 0, 255) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: jog_speed_slow");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cB %ld %ld", indexer, mot_inp.jog_speed_slow,
		   mot_inp.jog_speed_hi);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->jog_speed_slow = mot_inp.jog_speed_slow;
	  CurrParam->jog_speed_hi = mot_inp.jog_speed_hi;
	}
    }

  /* printf("********** Test Directive\n") ; */
  /* Test Directive */
  temp_long = (long) (mot_inp.test_directive);
  /* printf("********** Test Directive0\n") ; */
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_long (temp_long, 0, 1) == not_valid)
	{
	  printf ("********** Test Directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Test Directive1\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error testing the system");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%cZ 0", indexer);
	  strcpy (com[num_str++], temp_str);
	}
    }

  /* printf("********** Datum Speed\n") ; */
  /* datum speed */
  temp_long = (long) (mot_inp.datum_speed);
  if (temp_long != (long) link_clear1)
    {
      if ((UFcheck_double
	   (mot_inp.datum_speed, datum_speed_lo, datum_speed_hi) == not_valid)
	  && (CurrParam->datum_speed == not_valid))
	{
	  printf ("********** Datum Speed is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  CurrParam->datum_speed = temp_long;
	}
    }

  /* printf("********** Datum Direction\n"); */
  /* datum direction */
  temp_long = (long) (mot_inp.datum_direction);
  if (temp_long != (long) link_clear1)
    {
      if ((UFcheck_long (temp_long, 0, 1) == not_valid)
	  && (CurrParam->datum_direction == not_valid))
	{
	  printf ("********** Datum Direction is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	CurrParam->datum_direction = temp_long;
    }

  /* datum motor directive */

  if (mot_inp.datum_motor_directive != (long) link_clear1)
    {
      sprintf( _UFerrmsg, __HERE__ "> Datum?: input # steps=%f,  current # steps=%f",
	       mot_inp.num_steps, CurrParam->steps_from_home);
      ufLog( _UFerrmsg ) ;

      if( UFcheck_long( mot_inp.datum_motor_directive, 0, 1 ) == not_valid )
	{
	  printf ("********** Datum Motor directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else if( mot_inp.num_steps != CurrParam->steps_from_home ||
	       mot_inp.datum_motor_directive != 0 ) /*skip if already there and not real Datum*/
	{
	  if ((CurrParam->datum_speed > 0)
	      && (UFcheck_long_r (CurrParam->datum_direction, 0, 1) != not_valid))
	    {
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrDatum");
	      strcpy (com[num_str++], temp_str);

	      if (use_near_home)
		{
		  if (Hi_Level_Move[(int) indexer - 65])
		    {
		      if (use_backlash)
			near_home_margin = mot_inp.num_steps - CurrParam->backlash;
		      sprintf (temp_str, "%cN %d", indexer, near_home_margin);
		      strcpy (com[num_str++], temp_str);
		      if (mot_inp.num_steps == (long) link_clear0)
			{
			  sprintf (temp_str, "%s %s", command, "!!ErrSend: steps");
			  strcpy (com[num_str++], temp_str);
			  sign = ' ';
			  tmp_steps = -(mot_inp.num_steps - CurrParam->backlash);
			  if (tmp_steps >= 0)
			    sign = '+';
			  sprintf (temp_str, "%c%c%ld", indexer, sign, tmp_steps);
			  strcpy (com[num_str++], temp_str);
			}
		    }		/* high level move */
		  else
		    {		/* datum */
		      sprintf (temp_str, "%cF %ld %ld", indexer,
			       CurrParam->datum_speed,
			       CurrParam->datum_direction);
		      strcpy (com[num_str++], temp_str);
		      if (fabs (CurrParam->backlash) > 0.0)
			{
			  /* long signval = 1;  home switch wiring set to high-to-low transition */
			  long signval = -1; /* home switch wiring set to low-to-high transition */
			  sprintf (temp_str, "%s %s", command, "!!ErrSend: steps");
			  strcpy (com[num_str++], temp_str);
			  /* if home switch is wired low to high the indexor should not
			     perform final jog. The final home direction is exclusively
			     determined by the sign of this backlash and the datum
			     direction: home, then strep (+ or -) 2 * backlash away from home, 
			     then home for good. */
			  sign = ' ';
			  tmp_steps = 2 * (signval) * CurrParam->backlash;
			  if (tmp_steps >= 0)
			    sign = '+';
			  sprintf (temp_str, "%c%c%ld", indexer, sign, tmp_steps);
			  strcpy (com[num_str++], temp_str);
			  strcpy (temp_str, command);
			  strcat (temp_str, " !!ErrDatum");
			  strcpy (com[num_str++], temp_str);
			  sprintf (temp_str, "%c F %ld %ld", indexer,
				   CurrParam->final_datum_speed,
				   CurrParam->datum_direction);
			  strcpy (com[num_str++], temp_str);
			}
		      CurrParam->homing = 1;
		    }		/* datum */
		}     /* near-home */
	      else
		{		/* real-home */
		  long signval = -1;	/* home switch wiring set to
					   low-to-high transition */
		  sprintf (temp_str, "%cF %ld %ld", indexer,
			   CurrParam->datum_speed,
			   CurrParam->datum_direction);
		  strcpy (com[num_str++], temp_str);
		  sprintf (temp_str, "%s %s", command, "!!ErrSend: steps");
		  strcpy (com[num_str++], temp_str);
		  /* set the backlash/step-away-from home value & direction */
		  sign = ' ';
		  tmp_steps = 2 * (signval) * CurrParam->backlash;
		  if (tmp_steps >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%ld", indexer, sign, tmp_steps);
		  strcpy (com[num_str++], temp_str);
		  strcpy (temp_str, command);
		  strcat (temp_str, " !!ErrDatum");
		  strcpy (com[num_str++], temp_str);
		  sprintf (temp_str, "%c F %ld %ld", indexer,
			   CurrParam->final_datum_speed,
			   CurrParam->datum_direction);
		  strcpy (com[num_str++], temp_str);
		  CurrParam->homing = 1;
		}		/* real-home */
	    }
	  else			/* bad parameter */
	    {
	      return -1;
	    }
	}
    }		       	/* end of datum directive code block */

  /* num steps */

  if (mot_inp.num_steps != (long) link_clear0)
    {
      sprintf( _UFerrmsg, __HERE__ "> Step: input # steps=%f,  current # steps=%f",
	       mot_inp.num_steps, CurrParam->steps_from_home);
      ufLog( _UFerrmsg ) ;

      if (UFcheck_double (mot_inp.num_steps, num_steps_lo, num_steps_hi) == not_valid)
	{
	  printf ("********** Number of Steps is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  if( !(Hi_Level_Move[(int) indexer - 65]) ) /* always do low level move */
	    {
	      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
	      strcpy (com[num_str++], temp_str);
	      sign = ' ';
	      if( mot_inp.num_steps >= 0 )
		sign = '+';
	      sprintf (temp_str, "%c%c%8.2f", indexer, sign, mot_inp.num_steps);
	      strcpy (com[num_str++], temp_str);
	    }
	  else if( mot_inp.num_steps != CurrParam->steps_from_home ) /*skip if already there*/
	    {
	      if( use_near_home ) 
		mot_inp.num_steps = mot_inp.num_steps - (long) near_home_margin;

	      if( use_near_home && use_backlash )
		mot_inp.num_steps = mot_inp.num_steps - (long) CurrParam->backlash;

	      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
	      strcpy (com[num_str++], temp_str);
	      sign = ' ';
	      if( use_backlash )
		{
		  if( !use_near_home )
		    {
		      if (mot_inp.num_steps >= 0)
			sign = '+';
		      mot_inp.num_steps = mot_inp.num_steps - (long) CurrParam->backlash;
		      sprintf (temp_str, "%c%c%8.2f", indexer, sign, mot_inp.num_steps);
		      strcpy (com[num_str++], temp_str);
		      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
		      strcpy (com[num_str++], temp_str);
		      sign = ' ';
		    }
		  if (CurrParam->backlash >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%ld", indexer, sign, (long) CurrParam->backlash);
		  strcpy (com[num_str++], temp_str);
		}
	      else
		{
		  if (mot_inp.num_steps >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%8.2f", indexer, sign, mot_inp.num_steps);
		  strcpy (com[num_str++], temp_str);
		}
	    }
	}
    }

  /* stop message */
  if (strlen (mot_inp.stop_mess) > 0)
    {
      strcpy (temp_str, command);
      strcat (temp_str, " !!ErrStop");
      strcpy (com[num_str++], temp_str);
      sprintf (temp_str, "%c @", indexer);
      strcpy (com[num_str++], temp_str);
    }

  /* abort message */
  if (strlen (mot_inp.abort_mess) > 0)
    {
      strcpy (temp_str, command);
      strcat (temp_str, " !!ErrAbort");
      strcpy (com[num_str++], temp_str);
      sprintf (temp_str, "%c @", indexer);
      strcpy (com[num_str++], temp_str);
    }

  /* Origin Directive */
  temp_long = (long) (mot_inp.origin_directive);

  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_long (temp_long, 0, 1) == not_valid)
	{
	  printf ("********** origin Directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error origining motor");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%c o", indexer);
	  strcpy (com[num_str++], temp_str);
	}
    }

  if (num_str > 2)
    {
      sprintf(com[0], "%cCAR", indexer);
      strcpy (com[1], car_name);
      sprintf(com[2], "%cSAD", indexer);
      strcpy (com[3], sad_name);
    }
  else num_str = 0;

  printf("num_str : %d\n",num_str); 
  for (i = 0; i < num_str; i++)
  	printf ("%s ", com[i]);

  printf(" \n"); 
  Hi_Level_Move[(int) indexer - 65] = 0;
  return num_str;
}

int UFcheck_temp_inputs( ufTempInput temp_inp, char **com, char *rec_name,
			 ufTempCurrParam * CurrParam, long com_mode )
{

  long temp_long;
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  char car_name[30];
  char command[10];
  double P_value = -1.0;
  double I_value = -1.0;
  int heater_on = 0;

  strcpy (car_name, rec_name);
  /* car_name[strlen(car_name)-1] = 'C' ; */
  /* strcat(car_name,".I") ; */
  if (!com_mode)
    strcpy (command, "raw");
  else
    strcpy (command, "sim");

  temp_long = (long) (temp_inp.P);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.P, P_value_lo, P_value_hi) == -1)
	{
	  printf ("********** P value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** P value \n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: PID");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "PID 1, %6.2f", temp_inp.P);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->P = temp_inp.P;
	  P_value = temp_inp.P;
	}
    }

  /* I value */
  temp_long = (long) (temp_inp.I);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.I, I_value_lo, I_value_hi) == -1)
	{
	  printf ("********** I value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** I value \n"); */
	  if (P_value < 0.0)
	    {
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrSend: PID");
	      strcpy (com[num_str++], temp_str);
	      sprintf (temp_str, "PID 1, , %6.2f", temp_inp.I);
	    }
	  else sprintf (temp_str, "%s, %6.2f", com[--num_str], temp_inp.I);

	  strcpy (com[num_str++], temp_str);
	  CurrParam->I = temp_inp.I;
	  I_value = temp_inp.I;
	}
    }

  /* D value */
  temp_long = (long) (temp_inp.D);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.D, D_value_lo, D_value_hi) == -1)
	{
	  printf ("********** D value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** I value \n"); */
	  if (P_value < 0.0)
	    {
	      if (I_value < 0.0)
		{
		  strcpy (temp_str, command);
		  strcat (temp_str, " !!ErrSend: PID");
		  strcpy (com[num_str++], temp_str);
		  sprintf (temp_str, "PID 1, , , %6.2f", temp_inp.D);
		}
	      else sprintf (temp_str, "%s, %6.2f", com[--num_str], temp_inp.D);
	    }
	  else
	    {
	      if (I_value < 0.0)
		  sprintf (temp_str, "%s,, %6.2f", com[--num_str], temp_inp.D);
	      else
		  sprintf (temp_str, "%s, %6.2f", com[--num_str], temp_inp.D);
	    }
	  strcpy (com[num_str++], temp_str);
	  CurrParam->D = temp_inp.D;
	}
    }

  /* Temperature Set Point */
  /* printf("********** Set point\n") ; */
  temp_long = (long) (temp_inp.set_point);

  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.set_point, set_point_lo, set_point_hi) == -1)
	{
	  printf ("********** Set Point is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** Set Point\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error setting the temp");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "SETP 1, %6.3f", temp_inp.set_point);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->set_point = temp_inp.set_point;
	  heater_on = 1;
	}
    }

  /* printf("********** Heater Range\n") ; */
  temp_long = (long) (temp_inp.heater_range);
  if (temp_long != -1)
    {
      if (UFcheck_long_r(temp_inp.heater_range, heater_range_lo, heater_range_hi) == -1)
	{
	  printf ("********** Heater Range is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** Heater Range\n") ; */
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrHeaterRange");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "RANGE %ld", temp_inp.heater_range);
	  strcpy (com[num_str++], temp_str);
	  CurrParam->heater_range = temp_inp.heater_range;
	  /* Lakeshore agent configures heater on startup so following is commented out */
	  /* if (heater_on) {
	     strcpy(temp_str,command) ;
	     strcat(temp_str," !! Error turning the heater on") ; 
	     strcpy(com[num_str++],temp_str) ;
	     sprintf(temp_str,"CSET 1,A,1,1") ;
	     strcpy(com[num_str++],temp_str) ;
	     } */
	}
    }

  if (num_str > 2)
    {
      strcpy (com[0], "CAR");
      strcpy (com[1], car_name);
    }
  else num_str = 0;

  return num_str;
}

long UFAgentSend(char *name, int socFd, int Nstrings, char **strings)
{
  int nsent = 0, nb, nbh, nbt = 0;
  static ufProtocolHeader hdr;

  strcpy (hdr.name, name);
  hdr.seqCnt = 1;
  hdr.seqTot = 1;
  hdr.type = 1;
  hdr.elem = Nstrings;
  nbh = ufHeaderLength (&hdr);
  hdr.length = nbh + Nstrings * sizeof (int);	/* should add length of strings */

  nb = ufHeaderSend (socFd, &hdr);
  sprintf(_UFerrmsg,"UFAgentSend> hdr: nbsent=%d, nbhdr=%d", nb, nbh);
  ufLog(_UFerrmsg);

  if (nb <= 0)
    {
      ufLog ("UFAgentSend> failed to send header!");
      return nb;
    }
  if (nb < nbh)
    ufLog ("UFAgentSend> failed to send complete header?");

  do {
    nb = ufSendCstr (socFd, *strings);
    nsent++;
    strings++;
    sprintf(_UFerrmsg,"UFAgentSend> # bytes sent = %d, from string #: %d", nb, nsent);
    ufLog(_UFerrmsg);
    if (nb >= 4)
      nbt += (nb - 4);
  }
  while( (nsent < Nstrings) && (nb > 0) );

  sprintf(_UFerrmsg,"UFAgentSend> total # bytes sent (from strings) = %d", nbt);
  ufLog(_UFerrmsg);

  return nbt;
}

int UFprocess_dc_Hrdwr_inputs( ufdcHrdwrInput dcHrdwr_inp, char **com, char paramLevel,
			       char *rec_name, ufdcHrdwrCurrParam * CurrParam, long com_mode )
{
  int status = 0;
  int num_str = 4;
  char temp_str[80];
  char car_name[30];

  strcpy (car_name, rec_name);

  if (strcmp (dcHrdwr_inp.obs_mode, "") != 0)
    {
      if ((strcmp (dcHrdwr_inp.obs_mode, "chop-nod") != 0) &&
	  (strcmp (dcHrdwr_inp.obs_mode, "chop") != 0) &&
	  (strcmp (dcHrdwr_inp.obs_mode, "nod") != 0) &&
	  (strcmp (dcHrdwr_inp.obs_mode, "stare") != 0))
	{
	  printf ("********** Obs mode is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  strcpy (temp_str, "ObsMode");
	  strcat (temp_str, " !!ErrSend: ObsMode");
	  strcpy (com[num_str++], temp_str);
	  strcpy (temp_str, dcHrdwr_inp.obs_mode);
	  strcpy (com[num_str++], temp_str);
	  strcpy (CurrParam->obs_mode, dcHrdwr_inp.obs_mode);
	}
    }

  if (strcmp (dcHrdwr_inp.readout_mode, "") != 0)
    {
	strcpy (temp_str, "ReadoutMode");
	strcat (temp_str, " !!ErrSend: ReadoutMode");
	strcpy (com[num_str++], temp_str);
	strcpy (temp_str, dcHrdwr_inp.readout_mode);
	strcpy (com[num_str++], temp_str);
	strcpy (CurrParam->readout_mode, dcHrdwr_inp.readout_mode);
    }

  if (dcHrdwr_inp.FrmCoadds != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.FrmCoadds, 0, 9000) == not_valid)
	{
	  printf ("********** Frame Coadds is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "FrameCoadds");
	  strcat (temp_str, " !!ErrSend: FrameCoadds");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.FrmCoadds);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->FrmCoadds = dcHrdwr_inp.FrmCoadds;
	}
    }

  if (dcHrdwr_inp.ChpSettleReads != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.ChpSettleReads, 0, 9000) == not_valid)
	{
	  printf ("********** chop settle read is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopSettleReads");
	  strcat (temp_str, " !!ErrSend: ChopSettleReads");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.ChpSettleReads);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChpSettleReads = dcHrdwr_inp.ChpSettleReads;
	}
    }

  if (dcHrdwr_inp.ChpCoadds != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.ChpCoadds, 0, 9000) == not_valid)
	{
	  printf ("********** chop coadd is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopCoadds");
	  strcat (temp_str, " !!ErrSend: ChopCoadds");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.ChpCoadds);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChpCoadds = dcHrdwr_inp.ChpCoadds;
	}
    }

  if (dcHrdwr_inp.Savesets != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.Savesets, 0, 9000000) == not_valid)
	{
	  printf ("********** Save sets is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SaveSets");
	  strcat (temp_str, " !!ErrSend: SaveSets");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.Savesets);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->Savesets = dcHrdwr_inp.Savesets;
	}
    }

  if (dcHrdwr_inp.NodSettleReads != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.NodSettleReads, 0, 9000) == not_valid)
	{
	  printf ("********** Nod Settle Reads is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleReads");
	  strcat (temp_str, " !!ErrSend: NodSettleReads");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.NodSettleReads);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSettleReads = dcHrdwr_inp.NodSettleReads;
	}
    }

  if (dcHrdwr_inp.NodSets != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.NodSets, 0, 20000) == not_valid)
	{
	  printf ("********** Nod sets is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSets");
	  strcat (temp_str, " !!ErrSend: NodSets");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.NodSets);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSets = dcHrdwr_inp.NodSets;
	}
    }

  if (dcHrdwr_inp.NodSettleChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.NodSettleChops, 0, 2000) == not_valid)
	{
	  printf ("********** Nod Settle Chops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleChops");
	  strcat (temp_str, " !!ErrSend: NodSettleChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.NodSettleChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSettleChops = dcHrdwr_inp.NodSettleChops;
	}
    }

  if (dcHrdwr_inp.PreValidChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.PreValidChops, 0, 9000) == not_valid)
	{
	  printf ("********** PreValidChops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PreValidChops");
	  strcat (temp_str, " !!ErrSend: PreValidChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.PreValidChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->PreValidChops = dcHrdwr_inp.PreValidChops;
	}
    }

  if (dcHrdwr_inp.PostValidChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.PostValidChops, 0, 9000) == not_valid)
	{
	  printf ("********** Post Valid Chops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PostValidChops");
	  strcat (temp_str, " !!ErrSend: PostValidChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.PostValidChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->PostValidChops = dcHrdwr_inp.PostValidChops;
	}
    }

  if (dcHrdwr_inp.pixclock != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHrdwr_inp.pixclock, 0, 1000) == not_valid)
	{
	  printf ("********** pixclock is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PixClock");
	  strcat (temp_str, " !!ErrSend: PixClock");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHrdwr_inp.pixclock);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->pixclock = dcHrdwr_inp.pixclock;
	}
    }

  if (num_str > 4)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      strcpy (temp_str, "PARM");
      strcpy (com[2], temp_str);
      if (paramLevel == 'H')
	strcpy (temp_str, "HARDWARE");
      else
	{
	  if (paramLevel == 'P')
	    strcpy (temp_str, "PHYSICAL");
	  else
	    strcpy (temp_str, "META");
	}
      strcpy (com[3], temp_str);
    }
  else
    num_str = 0;
  return num_str;
}

int
UFcheck_dc_Phys_inputs (ufdcPhysicalInput dcPhysical_inp, char **com,
			char paramLevel, char *rec_name,
			ufdcPhysicalCurrParam * CurrParam, long com_mode)
{
  int status = 0;
  int num_str = 4;
  char temp_str[80];
  char car_name[30];

  ufLog( __HERE__ ) ;
  strcpy (car_name, rec_name);

  if (strcmp (dcPhysical_inp.obs_mode, "") != 0)
    {
      if ((strcmp (dcPhysical_inp.obs_mode, "chop-nod") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "chop") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "nod") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "stare") != 0))
	{
	  printf ("********** Obs mode is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ObsMode");
	  strcat (temp_str, " !!ErrSend: ObsMode");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcPhysical_inp.obs_mode);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->obs_mode, dcPhysical_inp.obs_mode);
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ObsMode");
      strcat (temp_str, " !!ErrSend: ObsMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, CurrParam->obs_mode);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (strcmp (dcPhysical_inp.readout_mode, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ReadoutMode");
      strcat (temp_str, " !!ErrSend: ReadoutMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcPhysical_inp.readout_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->readout_mode, dcPhysical_inp.readout_mode);
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ReadoutMode");
      strcat (temp_str, " !!ErrSend: ReadoutMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, CurrParam->readout_mode);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.FrameTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.FrameTime, 0, 9000) == not_valid)
	{
	  printf ("********** FrameTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "FrameTime");
	  strcat (temp_str, " !!ErrSend: FrameTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.FrameTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->FrameTime = dcPhysical_inp.FrameTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "FrameTime");
      strcat (temp_str, " !!ErrSend: FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->FrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.saveFrq != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.saveFrq, 0, 100) == not_valid)
	{
	  printf ("********** saveFreq is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SaveFrequency");
	  strcat (temp_str, " !!ErrSend: SaveFrequency");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.saveFrq);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->saveFrq = dcPhysical_inp.saveFrq;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SaveFrequency");
      strcat (temp_str, " !!ErrSend: SaveFrequency");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->saveFrq);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.exposureTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.exposureTime, 0, 1000) == not_valid)
	{
	  printf ("********** exposureTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "OnSourceTime");
	  strcat (temp_str, " !!ErrSend: OnSourceTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.exposureTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->exposureTime = dcPhysical_inp.exposureTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "OnSourceTime");
      strcat (temp_str, " !!ErrSend: OnSourceTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->exposureTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.ChopFreq != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.ChopFreq, 0, 100) == not_valid)
	{
	  printf ("********** ChopFrequency is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopFrequency");
	  strcat (temp_str, " !!ErrSend: ChopFrequency");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.ChopFreq);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChopFreq = dcPhysical_inp.ChopFreq;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ChopFrequency");
      strcat (temp_str, " !!ErrSend: ChopFrequency");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->ChopFreq);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.SCSDutyCycle != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.SCSDutyCycle, 0, 100) == not_valid)
	{
	  printf ("********** SCSDutyCycle is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SCSDutyCycle");
	  strcat (temp_str, " !!ErrSend: SCSDC");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.SCSDutyCycle);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->SCSDutyCycle = dcPhysical_inp.SCSDutyCycle;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SCSDutyCycle");
      strcat (temp_str, " !!ErrSend: SCSDC");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->SCSDutyCycle);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.nodDwelTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.nodDwelTime, 0, 1000) == not_valid)
	{
	  printf ("********** nodDwelTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodDwellTime");
	  strcat (temp_str, " !!ErrSend: NodDwellTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.nodDwelTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->nodDwelTime = dcPhysical_inp.nodDwelTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "NodDwellTime");
      strcat (temp_str, " !!ErrSend: NodDwellTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->nodDwelTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.nodStlTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.nodStlTime, 0, 100) == not_valid)
	{
	  printf ("********** nodStlTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleTime");
	  strcat (temp_str, " !!ErrSend: NodSettleTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.nodStlTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->nodStlTime = dcPhysical_inp.nodStlTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "NodSettleTime");
      strcat (temp_str, " !!ErrSend: NodSettleTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->nodStlTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.preValidChopTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.preValidChopTime, 0, 1000) == not_valid)
	{
	  printf ("********** preValidChopTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "preValidChopTime");
	  strcat (temp_str, " !!ErrSend: preValidChopTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.preValidChopTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->preValidChopTime = dcPhysical_inp.preValidChopTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "preValidChopTime");
      strcat (temp_str, " !!ErrSend: preValidChopTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->preValidChopTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.postValidChopTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.postValidChopTime, 0, 9000000) == not_valid)
	{
	  printf ("********** postValidChopTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "postValidChopTime");
	  strcat (temp_str, " !!ErrSend: postValidChopTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.postValidChopTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->postValidChopTime = dcPhysical_inp.postValidChopTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "postValidChopTime");
      strcat (temp_str, " !!ErrSend: postValidChopTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->postValidChopTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (num_str > 4)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      strcpy (temp_str, "PARM");
      strcpy (com[2], temp_str);
      if (paramLevel == 'H')
	strcpy (temp_str, "HARDWARE");
      else
	{
	  if (paramLevel == 'P')
	    strcpy (temp_str, "PHYSICAL");
	  else
	    strcpy (temp_str, "META");
	}
      strcpy (com[3], temp_str);
    }
  else
    num_str = 0;
  return num_str;
}

int
UFcheck_dc_Acq_inputs (ufdcAcqContInput dcAcqCont_inp, char **com,
		       char paramLevel, char *rec_name,
		       ufdcAcqContCurrParam * CurrParam, long com_mode)
{
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  char car_name[30];

  strcpy (car_name, rec_name);

  ufLog( __HERE__ ) ;

  if (strcmp (dcAcqCont_inp.command, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.command, "CONFIGURE") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "START") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "STOP") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "ABORT") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "INIT") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "PARK") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "DATUM") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "TEST") != 0))
	{
	  printf ("********** command is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ACQ");
	  strcat (temp_str, " !!ErrSend: ACQ");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.command);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->command, dcAcqCont_inp.command);
	}
    }

  if (strcmp (dcAcqCont_inp.obs_id, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ObsID");
      strcat (temp_str, " !!ErrSend: obs_id");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.obs_id);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->obs_id, dcAcqCont_inp.obs_id);
    }
  else
    {
      if (strcmp (CurrParam->obs_id, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ObsID");
	  strcat (temp_str, " !!ErrSend: obs_id");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->obs_id);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.dhs_write, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "DHSWrite");
      strcat (temp_str, " !!ErrSend: dhs_write");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.dhs_write);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->dhs_write, dcAcqCont_inp.dhs_write);
    }
  else
    {
      if (strcmp (CurrParam->dhs_write, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "DHSWrite");
	  strcat (temp_str, " !!ErrSend: dhs_write");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->dhs_write);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.local_archive, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.local_archive, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "false") != 0))
	{
	  printf ("********** local_archive is not valid &%s&\n",
		  dcAcqCont_inp.local_archive);
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Archive");
	  strcat (temp_str, " !!ErrSend: Archive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.local_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->local_archive, dcAcqCont_inp.local_archive);
	}
    }
  else
    {
      if (strcmp (CurrParam->local_archive, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Archive");
	  strcat (temp_str, " !!ErrSend: Archive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->local_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.nod_handshake, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.nod_handshake, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "false") != 0))
	{

	  printf ("********** nod_handshake is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodHandShake");
	  strcat (temp_str, " !!ErrSend: nod_handshake");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.nod_handshake);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->nod_handshake, dcAcqCont_inp.nod_handshake);
	}
    }
  else
    {
      if (strcmp (CurrParam->nod_handshake, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodHandShake");
	  strcat (temp_str, " !!ErrSend: nod_handshake");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->nod_handshake);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }
  /* 
     if (strcmp(dcAcqCont_inp.archive_host,"") != 0) {
     num_str = num_str + 1; strcpy(temp_str,"ArchiveHost") 
     ; strcat(temp_str," !!ErrSend: archive_host") ;
     strcpy(com[(int)num_str-1],temp_str) ; num_str =
     num_str + 1;
     strcpy(temp_str,dcAcqCont_inp.archive_host);
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->archive_host,dcAcqCont_inp.archive_host) 
     ; } */
  if (strcmp (dcAcqCont_inp.archive_path, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ArchivePath");
      strcat (temp_str, " !!ErrSend: archive_path");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.archive_path);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->archive_path, dcAcqCont_inp.archive_path);
    }
  else
    {
      if (strcmp (CurrParam->archive_path, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ArchivePath");
	  strcat (temp_str, " !!ErrSend: archive_path");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->archive_path);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.archive_file_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ArchiveFileName");
      strcat (temp_str, " !!ErrSend: archive_file_name");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.archive_file_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->archive_file_name, dcAcqCont_inp.archive_file_name);
    }
  else
    {
      if (strcmp (CurrParam->archive_file_name, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ArchiveFileName");
	  strcat (temp_str, " !!ErrSend: archive_file_name");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->archive_file_name);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.comment, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Comment");
      strcat (temp_str, " !!ErrSend: comment");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.comment);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->comment, dcAcqCont_inp.comment);
    }
  else
    {
      if (strcmp (CurrParam->comment, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Comment");
	  strcat (temp_str, " !!ErrSend: comment");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->comment);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.remote_archive, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.remote_archive, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "false") != 0))
	{
	  printf ("********** remote_archive is not valid &%s&\n",
		  dcAcqCont_inp.remote_archive);
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchive");
	  strcat (temp_str, " !!ErrSend: RemoteArchive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.remote_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->remote_archive, dcAcqCont_inp.remote_archive);
	}
    }
  else
    {
      if (strcmp (CurrParam->remote_archive, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchive");
	  strcat (temp_str, " !!ErrSend: RemoteArchive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->remote_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_host, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchiveHost");
      strcat (temp_str, " !!ErrSend: RemArchiveHost");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_host);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_host, dcAcqCont_inp.rem_archive_host);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_host, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchiveHost");
	  strcat (temp_str, " !!ErrSend: RemArchiveHost");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_host);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_path, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchivePath");
      strcat (temp_str, " !!ErrSend: rem_archive_path");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_path);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_path, dcAcqCont_inp.rem_archive_path);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_path, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchivePath");
	  strcat (temp_str, " !!ErrSend: rem_archive_path");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_path);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_file_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchiveFileName");
      strcat (temp_str, " !!ErrSend: rem_archive_file_name");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_file_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_file_name,
	      dcAcqCont_inp.rem_archive_file_name);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_file_name, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchiveFileName");
	  strcat (temp_str, " !!ErrSend: rem_archive_file_name");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_file_name);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (num_str > 2)
    {
      strcpy( com[0], "CAR" );
      strcpy( com[1], car_name );
    }
  else
    num_str = 0;

  return num_str;
}

int
UFcheck_dc_Obs_inputs (ufdcObsContParam dcObsCont_inp, char **com,
		       char paramLevel, char *rec_name,
		       ufdcObsContParam * CurrParam, long com_mode)
{
  int num_str = 4;
  char temp_str[80];
  char car_name[30];

  strcpy(car_name, rec_name);

  if (dcObsCont_inp.expTime != -1)
    {
      strcpy (temp_str, "expTime");
      strcat (temp_str, " !!ErrSend: expTime");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int )dcObsCont_inp.expTime);
      strcpy (com[num_str++], temp_str);
      CurrParam->expTime = dcObsCont_inp.expTime;
    }

  if (dcObsCont_inp.nonDestReadouts != -1)
    {
      strcpy (temp_str, "nonDestReadouts");
      strcat (temp_str, " !!ErrSend: nonDestReadouts");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int)dcObsCont_inp.nonDestReadouts);
      strcpy (com[num_str++], temp_str);
      CurrParam->nonDestReadouts = dcObsCont_inp.nonDestReadouts;
    }

  if (dcObsCont_inp.numberCoadds != -1)
    {
      strcpy (temp_str, "numberCoadds");
      strcat (temp_str, " !!ErrSend: numberCoadds");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int)dcObsCont_inp.numberCoadds);
      strcpy (com[num_str++], temp_str);
      CurrParam->numberCoadds = dcObsCont_inp.numberCoadds;
    }

  if (dcObsCont_inp.numberReads != -1)
    {
      strcpy (temp_str, "numberReads");
      strcat (temp_str, " !!ErrSend: numberReads");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int)dcObsCont_inp.numberReads);
      strcpy (com[num_str++], temp_str);
      CurrParam->numberReads = dcObsCont_inp.numberReads;
    }

  if (strcmp (dcObsCont_inp.telDitherMode, "") != 0)
    {
      strcpy (temp_str, "telDitherMode");
      strcat (temp_str, " !!ErrSend: telDitherMode");
      strcpy (com[num_str++], temp_str);
      strcpy (temp_str, dcObsCont_inp.telDitherMode);
      strcpy (com[num_str++], temp_str);
      strcpy(CurrParam->telDitherMode, dcObsCont_inp.telDitherMode);
    }

  if (dcObsCont_inp.nodSettleTime != -1)
    {
      strcpy (temp_str, "nodSettleTime");
      strcat (temp_str, " !!ErrSend: nodSettleTime");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int)dcObsCont_inp.nodSettleTime);
      strcpy (com[num_str++], temp_str);
      CurrParam->nodSettleTime = dcObsCont_inp.nodSettleTime;
    }

  if (dcObsCont_inp.offSettleTime != -1)
    {
      strcpy (temp_str, "offSettleTime");
      strcat (temp_str, " !!ErrSend: offSettleTime");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%d", (int)dcObsCont_inp.offSettleTime);
      strcpy (com[num_str++], temp_str);
      CurrParam->offSettleTime = dcObsCont_inp.offSettleTime;
    }

  if (dcObsCont_inp.nodDistance != -1.0)
    {
      strcpy (temp_str, "nodDistance");
      strcat (temp_str, " !!ErrSend: nodDistance");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%f", dcObsCont_inp.nodDistance);
      strcpy (com[num_str++], temp_str);
      CurrParam->nodDistance = dcObsCont_inp.nodDistance;
    }

  if (dcObsCont_inp.nodPA != -1.0)
    {
      strcpy (temp_str, "nodPA");
      strcat (temp_str, " !!ErrSend: nodPA");
      strcpy (com[num_str++], temp_str);
      sprintf(temp_str, "%f", dcObsCont_inp.nodPA);
      strcpy (com[num_str++], temp_str);
      CurrParam->nodPA = dcObsCont_inp.nodPA;
    }

  if (num_str > 4)
    {
      strcpy (com[0], "CAR");
      strcpy (com[1], car_name);
      strcpy (com[2], "PARM");
      strcpy (com[3], "META");
    }
  else
    num_str = 0;

  return num_str;
}

int UFcheck_ecVacCh_inputs( ufecVacChInput ecVacCh_inp, char **com,
			    char *rec_name, ufecVacChCurrParam * CurrParam,
			    long com_mode, int agent )
{
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  char car_name[30];

  strcpy (car_name, rec_name);

  if (strcmp (ecVacCh_inp.power, "") != 0)
    {
      if ((strcmp (ecVacCh_inp.power, "ON") != 0) &&
	  (strcmp (ecVacCh_inp.power, "OFF") != 0))
	{
	  printf ("********** command is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  if (agent == 2)
	    {
	      if (strcmp (ecVacCh_inp.power, "ON") == 0)
		{
		  strcpy (temp_str, "raw !!ErrSend: ON Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "IG1");
		}
	      else
		{
		  strcpy (temp_str, "raw !!ErrSend: OFF Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "IG0");
		}
	    }
	  else
	    {
	      if (strcmp (ecVacCh_inp.power, "ON") == 0)
		{
		  strcpy (temp_str, "raw !!ErrSend: ON Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "R 240");
		}
	      else
		{
		  strcpy (temp_str, "raw !!ErrSend: OFF Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "S");
		}
	    }
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->power, ecVacCh_inp.power);
	}
    }

  if (num_str > 2)
    {
      strcpy( com[0], "CAR");
      strcpy( com[1], car_name);
    }
  else
    num_str = 0;

  return num_str;
}

int UFcheck_bias_inputs( ufDCBiasInput bias_inp, char **com, char *rec_name,
			 ufDCBiasCurrParam * CurrParam, long com_mode )
{
  int num_str = 2;
  char car_name[30];
  int dacid, dacVal;

  sprintf( _UFerrmsg, __HERE__ "> rec_name: %s", rec_name ) ;
  ufLog( _UFerrmsg ) ;

  strcpy (car_name, rec_name);

  if (strcmp (bias_inp.password, "") != 0)
    {
      ufLog( __HERE__ "> Processing password" ) ;
      strcpy( com[num_str++], "Password  !!ErrSend: Password");
      sprintf( com[num_str++], "%s %s", "ARRAY_PW_BIAS", bias_inp.password );
      strcpy( com[num_str++], "EnableChange  !!ErrSend: EnableChange");
      if( strstr(rec_name, "miri") != NULL)
	strcpy( com[num_str++], "miri:dc:biPassEnblIn");
      else
	strcpy( com[num_str++], "flam:dc:biPassEnblIn");
    }
  else 	  /* not password here */
    {
      ufLog( __HERE__ "> NOT a password" ) ;

      if (bias_inp.park_directive != -1.0)
	{
	  ufLog( __HERE__ "> Processing park_directive" ) ;

	  if( UFcheck_double (bias_inp.park_directive, 0, 1) == not_valid)
	    {
	      printf ("********** park_directive is not valid\n");
	      return not_valid;
	    }
	  else
	    {
	      strcpy( com[num_str++], "BiasPark  !!ErrSend: in Park");
	      strcpy( com[num_str++], "ARRAY_POWER_DOWN_BIAS");
	      strcpy( com[num_str++], "BiasPark  !!ErrSend: in Park");
	      strcpy( com[num_str++], "ARRAY_INIT_STATE_BIAS");
	    }
	}

      if (bias_inp.datum_directive != -1.0)
	{
	  ufLog( __HERE__ "> Processing datum_directive" ) ;

	  if( UFcheck_double (bias_inp.datum_directive, 0, 1) == not_valid)
	    {
	      printf ("********** datum_directive is not valid\n");
	      return not_valid;
	    }
	  else
	    {
	      for( dacid=0; dacid < NVBIAS; dacid++ )
		{
		  strcpy( com[num_str++], "BiasDacId  !!ErrSend: BiasDacId");
		  dacVal = CurrParam->default_values[dacid];
		  if( dacVal < 0 ) dacVal = 0;
		  if( dacVal > 255 ) dacVal = 255;
		  sprintf( com[num_str++], "%s %d %d", "Dac_Set_Single_Bias", dacid, dacVal);
		}
	      /* now Latch all the set default biases: (com array is not allocated large enough)*/
	      /* strcpy( com[num_str++], "BiasLatchAll  !!ErrSend: BiasLatchAll");
		 strcpy( com[num_str++], "Dac_Lat_All_Bias" ) ; */
	    }
	}

      if (strcmp (bias_inp.power, "") != 0)
	{
	  ufLog( __HERE__ "> Processing power directive" ) ;

	  if ((strcmp (bias_inp.power, "ON") != 0) &&
	      (strcmp (bias_inp.power, "on") != 0) &&
	      (strcmp (bias_inp.power, "On") != 0) &&
	      (strcmp (bias_inp.power, "OFF") != 0) &&
	      (strcmp (bias_inp.power, "Off") != 0) &&
	      (strcmp (bias_inp.power, "off") != 0))
	    {
	      printf ("********** Power directive is not valid\n");
	      return not_valid;
	    }
	  else
	    {
	      strcpy( com[num_str++], "BiasPower  !!ErrSend: setting power");

	      if ((strcmp (bias_inp.power, "ON") == 0) ||
		  (strcmp (bias_inp.power, "On") == 0) ||
		  (strcmp (bias_inp.power, "on") == 0))
		{
		  strcpy( com[num_str++], "ARRAY_POWER_UP_BIAS");
		  strcpy( com[num_str++], "BiasVgate" ) ;
		  strcpy( com[num_str++], "ARRAY_VGATE 1" ) ;
		}
	      else
		{
		  strcpy( com[num_str++], "ARRAY_POWER_DOWN_BIAS");
		  strcpy( com[num_str++], "BiasVgate" ) ;
		  strcpy( com[num_str++], "ARRAY_VGATE 0" ) ;
		}
	    }
	}

      if (bias_inp.dac_id != (long) link_clear1)
	{
	  ufLog( __HERE__ "> Processing dac_id" ) ;

	  if( UFcheck_long_r (bias_inp.dac_id, 0, 23) == not_valid)
	    {
	      printf ("********** BiasDacId is not valid\n");
	      return not_valid;
	    }
	  else
	    {
	      strcpy( com[num_str++], "BiasDacId  !!ErrSend: BiasDacId");
	      dacid = bias_inp.dac_id;
	      dacVal = bias_inp.dac_volt * CurrParam->M[dacid] + CurrParam->B[dacid];
	      if( dacVal < 0 ) dacVal = 0;
	      if( dacVal > 255 ) dacVal = 255;
	      sprintf( com[num_str++], "%s %d %d", "Dac_Set_Single_Bias", dacid, dacVal);
	    }
	}

      if (bias_inp.latch_dac_id != (long) link_clear1)
	{
	  ufLog( __HERE__ "> Processing latch_dac_id (Latch All Bias)" ) ;
	  strcpy( com[num_str++], "BiasLatchAll  !!ErrSend: BiasLatchAll");
	  strcpy( com[num_str++], "Dac_Lat_All_Bias" ) ;
	}

      if (bias_inp.read_all_directive != (long) link_clear1)
	{
	  ufLog( __HERE__ "> Processing read_all_directive" ) ;
	  strcpy( com[num_str++], "ReadAllBias  !!ErrSend: ReadAllBias");
	  strcpy( com[num_str++], "ARRAY_DAC_READBACK_BIAS");
	}

      if (strcmp (bias_inp.vGate, "") != 0)
	{
	  ufLog( __HERE__ "> Processing vGate" ) ;
	  strcpy( com[num_str++], "BiasVGate  !!ErrSend: vGate");
	  sprintf( com[num_str++], "ARRAY_SET_VGATE_BIAS %s", bias_inp.vGate );
	}

      if (strcmp (bias_inp.vWell, "") != 0)
	{
	  ufLog( __HERE__ "> Processing vWell" ) ;
	  strcpy( com[num_str++], "BiasVWell  !!ErrSend: vWell");
	  sprintf( com[num_str++], "ARRAY_WELL %s", bias_inp.vWell );
	}

      if (strcmp (bias_inp.vBias, "") != 0)
	{
	  sprintf( _UFerrmsg, __HERE__ "> Processing vBias, num_str: %d", num_str ) ;
	  ufLog( _UFerrmsg ) ;
	  strcpy( com[num_str++], "BiasLevel  !!ErrSend: vBias");
	  sprintf( com[num_str++], "ARRAY_SET_DET_BIAS %s", bias_inp.vBias );
	}
    }

  sprintf( _UFerrmsg, __HERE__ "> num_str: %d", num_str ) ;
  ufLog( _UFerrmsg ) ;

  if (num_str > 2)
    {
      strcpy( com[0], "CAR");
      strcpy( com[1], car_name);
    }
  else
    num_str = 0;

  return num_str;
}

long UFcheck_preAmp_inputs( ufDCPreAmpInput preAmp_inp, char **com, char *rec_name,
			    ufDCPreAmpCurrParam *CurrParam, long command_mode )
{
  int num_str = 2;
  char temp_str[80];
  char car_name[30];
  int dac_control;
  int dac_id;

  sprintf( _UFerrmsg, __HERE__ "> rec_name: %s", rec_name ) ;
  ufLog( _UFerrmsg ) ;

  strcpy (car_name, rec_name);

  if (preAmp_inp.datum_directive != -1.0)
    {
      ufLog( __HERE__ "> Processing datum_directive" ) ;

      if( UFcheck_double (preAmp_inp.datum_directive, 0, 1) == not_valid)
	{
	  printf ("********** datum_directive is not valid\n");
	  return not_valid;
	}
      else
	{
	  strcpy( com[num_str++], "preAmpDatum  !!ErrSend: in Datum" );
	  strcpy( com[num_str++], "ARRAY_OFFSET_DEFAULTS_PREAMP" );
	  /* CurrParam->datum_directive = preAmp_inp.datum_directive ; */
	}
    }

  if (preAmp_inp.global_set >= 0)
    {
      sprintf( _UFerrmsg, __HERE__ "> Processing global_set: %f", preAmp_inp.global_set ) ;
      ufLog( _UFerrmsg ) ;
      dac_control = preAmp_inp.global_set * CurrParam->M[0] + CurrParam->B[0];
      strcpy( com[num_str++], "SetGlobal !!ErrSend: SetGlobal" ) ;
      sprintf( com[num_str++], "ARRAY_OFFSET_GLOBAL_PREAMP %d", dac_control );
    }

  if (preAmp_inp.adjust_value != 0)
    {
      sprintf( _UFerrmsg, __HERE__ "> Processing adjust_value: %f", preAmp_inp.adjust_value ) ;
      ufLog( _UFerrmsg ) ;
      dac_control = preAmp_inp.adjust_value * CurrParam->M[0] + CurrParam->B[0];
      strcpy( com[num_str++], "AdjustPreAmp  !!ErrSend: AdjustPreAmp" );
      sprintf( com[num_str++], "ARRAY_OFFSET_ADJUST_PREAMP %d", dac_control );
    }

  if (preAmp_inp.preAmp_id != (long) link_clear1)
    {
      ufLog( __HERE__ "> Processing preamp_id" ) ;

      if( UFcheck_long_r (preAmp_inp.preAmp_id, 0, 50) == not_valid)
	{
	  printf ("********** preAmpId is not valid\n");
	  return not_valid;
	}
      else
	{
	  strcpy (temp_str, "preAmpId");
	  strcat (temp_str, " !!ErrSend: preAmpId");
	  strcpy (com[num_str++], temp_str);
	  dac_id = preAmp_inp.preAmp_id;
	  dac_control = preAmp_inp.preAmp_volt * CurrParam->M[dac_id] + CurrParam->B[dac_id];
	  sprintf( com[num_str++], "%s %ld %d", "ARRAY_OFFSET_SINGLE_PREAMP",
		   preAmp_inp.preAmp_id, dac_control );
	  /* CurrParam->dac_id = preAmp_inp.dac_id ; */
	}
    }

  if (preAmp_inp.read_all_directive != (long) link_clear1)
    {
      ufLog( __HERE__ "> Processing read_all_directive" ) ;

      if( UFcheck_long_r (preAmp_inp.read_all_directive, 0, 1) == not_valid)
	{
	  printf ("********** read_all_directive is not valid\n");
	  return not_valid;
	}
      else
	{
	  strcpy (temp_str, "ReadAllpreAmp");
	  strcat (temp_str, " !!ErrSend: ReadAllpreAmp");
	  strcpy (com[num_str++], temp_str);
	  sprintf (temp_str, "%s", "ARRAY_OFFSET_READBACK_PREAMP");
	  strcpy (com[num_str++], temp_str);
	  /* CurrParam->read_all_directive =
	     dcHrdwr_inp.read_all_directive ; */
	}
    }

  sprintf( _UFerrmsg, __HERE__ "> num_str: %d", num_str ) ;
  ufLog( _UFerrmsg ) ;

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
    }
  else
    num_str = 0;

  return num_str;
}

/*
  char all_devices[40] ;
  char annex[40] ;
  char ls_218[40] ;
  char ls_340[40] ;
  char vacuum[40] ;
  char indexors[40] ;
  char cryocooler[40] ;
  char ppcVME[40] ;
  char mce4[40] ;
  char all_agents[40] ;
  char executive[40] ;
  char ufacqframed[40] ;
  char ufgdhsd[40] ;
  char ufgls218[40] ;
  char ufgls340[40] ;
  char ufg354vacd[40] ;
  char ufgmotord[40] ;
  char ufgmce4d[40] ;
*/

void
str_toUpper (char *target, const char *original)
{
  for( ; original && *original ; ++original, ++target )
    {
      *target = toupper( *original ) ;
    }
  *target = 0 ;
  
  /*
  int i;
  for (i = 0; i < strlen (original); i++)
    target[i] = toupper ((int) original[i]);
  target[strlen (original)] = '\0';
  return;
  */
}

long
UFcheck_bt_inputs (ufBTInput boot_inp, char **com, char *rec_name,
		   ufBTCurrParam * CurrParam, long command_mode)
{
  int num_str = 2;
  char temp_str[80];
  char car_name[30];
  char in_upper[40];

  strcpy (car_name, rec_name);

  if (strcmp (boot_inp.all_devices, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.all_devices);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "all_devices");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ALL");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->all_devices, in_upper);
	}
    }

  if (strcmp (boot_inp.annex, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.annex);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "annex");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "annex");
	  /* strcpy(temp_str,"1"); */
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->annex, in_upper);
	}
    }

  if (strcmp (boot_inp.ls_218, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ls_218);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "lakeshore218");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "lakeshore218");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ls_218, in_upper);
	}
    }

  if (strcmp (boot_inp.ls_340, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ls_340);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "lakeshore340");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "lakeshore340");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ls_340, in_upper);
	}
    }

  if (strcmp (boot_inp.vacuum, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.vacuum);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "vacuum");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "vacuum");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->vacuum, in_upper);
	}
    }

  if (strcmp (boot_inp.indexors, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.indexors);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "indexors");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "indexors");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->indexors, in_upper);
	}
    }

  if (strcmp (boot_inp.cryocooler, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.cryocooler);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "cryocooler");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "cryocooler");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->cryocooler, in_upper);
	}
    }

  if (strcmp (boot_inp.ppcVME, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ppcVME);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ppcVME");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ppcVME");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ppcVME, in_upper);
	}
    }

  if (strcmp (boot_inp.mce4, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.mce4);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "mce4");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "mce4");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->mce4, in_upper);
	}
    }

  if (strcmp (boot_inp.all_agents, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.all_agents);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "All Agents");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "All");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->all_agents, in_upper);
	}
    }

  if (strcmp (boot_inp.executive, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.executive);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "executive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "executive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->executive, in_upper);
	}
    }

  if (strcmp (boot_inp.ufacqframed, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufacqframed);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufacqframed");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufacqframed");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufacqframed, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgdhsd, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgdhsd);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgdhsd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgdhsd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgdhsd, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgls218, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgls218);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgls218d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgls218d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgls218, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgls340, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgls340);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgls340d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgls340d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgls340, in_upper);
	}
    }

  if (strcmp (boot_inp.ufg354vacd, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufg354vacd);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufg354vacd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufg354vacd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufg354vacd, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgmotord, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgmotord);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgmotord");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgmotord");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgmotord, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgmce4d, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgmce4d);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  return not_valid;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgmce4d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgmce4d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgmce4d, in_upper);
	}
    }

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
    }
  else
    num_str = 0;

  return num_str;
}

long UFReadPositions( FILE * posFile, int num_mot, int last_mot_num )
{
  long i, j;
  char in_str[256];
  double in_dbl, throughput, lambda_lo, lambda_hi;
  int num_pos;
  char *temp_str;
  int done = 0;
  int mot_num, pos_num;

  /* read the remaining motor parameter lines */
  for (i = last_mot_num; i < num_mot; i++)
    fgets (in_str, sizeof(in_str), posFile);

  /* Read the comment lines describing the table of parameters */
  /* The comments end with line containing keyword POSITIONS */

  while( strstr( in_str, "POSITIONS" ) == NULL )
    fgets( in_str, sizeof(in_str), posFile );

  for (i = 0; i < num_mot; i++)
    {
      /* read the comment line with the full motor name */
      fgets (in_str, sizeof(in_str), posFile);

      /* read the number of positions */
      fscanf (posFile, "%d %d", &mot_num, &num_pos);
      namedPositionsTable[i].num_pos = num_pos;

      /* read to the end of the line */
      fgets (in_str, sizeof(in_str), posFile);

      /* insert the abbreviated name in the table */
      temp_str = strchr (in_str, '\n');
      if (temp_str != NULL)
	temp_str[0] = '\0';

      /* trim the leading spaces */
      temp_str = strchr (in_str, ' ');

      while ((temp_str != NULL) && (!done))
	{
	  if (temp_str == in_str)
	    strcpy (in_str, temp_str + 1);
	  else {
	    done = 1;
	    if (temp_str != NULL)
	      temp_str[0] = '\0';
	  }
	  temp_str = strchr(in_str, ' ');
	}

      strcpy (namedPositionsTable[i].motorName, in_str);

      /* read the comment line */
      fgets (in_str, sizeof(in_str), posFile);	/* comment line */

      for (j = 0; j < num_pos; j++)
	{
	  /* read the offset and the name of the positions */
	  fscanf (posFile, "%d %lf %lf %lf %lf", &pos_num, &in_dbl,
		  &throughput, &lambda_lo, &lambda_hi);

	  fgets (in_str, sizeof(in_str), posFile);	/* read the
					   position name */
	  temp_str = strchr (in_str, '\n');

	  if (temp_str != NULL)
	    temp_str[0] = '\0';

	  /* trim the white spaces */
	  temp_str = strchr (in_str, ' ');

	  while ((temp_str != NULL) && (!done))
	    {
	      if (temp_str == in_str)
		{
		  strcpy (in_str, temp_str + 1);
		}
	      else
		{
		  done = 1;
		  if (temp_str != NULL)
		    temp_str[0] = '\0';
		}
	      temp_str = strchr (in_str, ' ');
	    }

	  /* insert them in the table */
	  strcpy (namedPositionsTable[i].the_list[j].name, in_str);
	  namedPositionsTable[i].the_list[j].offset = in_dbl;

	  namedPositionsTable[i].the_list[j].throughput = throughput;
	  namedPositionsTable[i].the_list[j].lambdaLo = lambda_lo;
	  namedPositionsTable[i].the_list[j].lambdaHi = lambda_hi;
	}
    }
  namPosTableInit = 1;

  return OK ;
}

long UFPrintPositionsTable ()
{
  long status = 0;
  int i, j;

  if (namPosTableInit == 0)
    {
      printf ("The Table has not been initialized yet.\n");
      return -1;
    }
  else
    {
      for (i = 0; i < 10; i++)
	{
	  /* print the abbreviated motor name */
	  printf ("%s\n", namedPositionsTable[i].motorName);
	  /* print the number of positions */
	  printf ("%d\n", namedPositionsTable[i].num_pos);
	  for (j = 0; j < namedPositionsTable[i].num_pos; j++)
	    {
	      /* print the offset and the name of the
	         positions */
	      printf ("%f %f %s\n", namedPositionsTable[i].the_list[j].offset,
		      namedPositionsTable[i].the_list[j].throughput,
		      namedPositionsTable[i].the_list[j].name);

	    }
	}
      return status;
    }
}

long flamAlive ()
{

  printf ("I am awake. I am awake. now quit bugging me.!!!!\n");
  return OK;
}

long UFGetSteps( const char *motorName, int posNum, double *offset )
{
  long imot = 0;
  int npos = 0;
  double currentWaveLength = 0.0 ;

  sprintf( _UFerrmsg, __HERE__ "> motor=%s,  posNum=%d", motorName, posNum);
  ufLog( _UFerrmsg ) ;

  /* Lookup the motor in the motor table */
  for( imot=0; imot < NMOTORS ; ++imot )
    {
      if( strstr( namedPositionsTable[imot].motorName, motorName ) != 0 )
	break ;  /* Found it */
    }

  /* Was the motor found? */
  if( imot >= NMOTORS )
    {
      /* Nope, return error */
      ufLog( __HERE__ "> motor name not found in table, returning zero for offset...");
      *offset = 0;
      return -1;
    }

  /* Motor found */
  npos = namedPositionsTable[imot].num_pos;

  if( posNum <= 0 )
    {
      ufLog( __HERE__ "> posNum <=0, selecting PARK." );
      *offset = namedPositionsTable[imot].the_list[npos-1].offset;
      sprintf( _UFerrmsg, __HERE__ "> Motor number = %ld,  offset=%f", imot, *offset);
      ufLog( _UFerrmsg ) ;
      return imot;
    }

  if( npos < posNum )
    {
      sprintf( _UFerrmsg, __HERE__ "> posNum exceeds %d, assuming PARK.", npos);
      ufLog( _UFerrmsg ) ;
      *offset = namedPositionsTable[imot].the_list[npos-1].offset;
      sprintf( _UFerrmsg, __HERE__ "> Motor number = %ld,  offset=%f", imot, *offset);
      ufLog( _UFerrmsg ) ;
      return imot;
    }

  *offset = namedPositionsTable[imot].the_list[posNum-1].offset;

  sprintf( _UFerrmsg, __HERE__ "> Motor number = %ld,  posName=%s,  offset=%f",
	   imot, namedPositionsTable[imot].the_list[posNum-1].name, *offset );
  ufLog( _UFerrmsg ) ;

  if( strstr( namedPositionsTable[imot].the_list[posNum-1].name, "HighRes-10" ) != NULL )
    {
      currentWaveLength = ufGetWaveLength() ;

      if( (currentWaveLength >= 7.0) && (currentWaveLength <= 14.0) )
	{
	  ufLog( __HERE__ "> adjusting HighRes-10 grating..." );
	  
	  *offset = ufGratingOffsetCalc() ;

	  sprintf( _UFerrmsg, __HERE__ "> New offset: %f", *offset ) ;
	  ufLog( _UFerrmsg ) ;
	}
    }

  return imot;
}

int checkSoc (int socfd, float timeOut) {
  struct timeval *infinit = 0;
  struct timeval *usetimeout = 0;
  struct timeval timeout;
  struct timeval poll = { 0, 0 };	/* do not block */
  int fdcnt = 0, cnt = 32;
  struct timeval to, *to_p = 0;
  float _timeOut = 0.0;
  fd_set writefds, readfds, excptfds;
  char recvBuf;
  struct sockaddr _theSockAddr;
  int _theSizeofSockAddrData = sizeof (_theSockAddr);
  int rcnt = 0;

  if( socfd <= 0 ) {
    sprintf( _UFerrmsg, __HERE__ "> Bad socket FD" ) ;
    ufLog( _UFerrmsg ) ;
    return socfd;
  }

  if( socfd > 0 ) {
    sprintf( _UFerrmsg, __HERE__ "> Always return -1 for now..." ) ;
    ufLog( _UFerrmsg ) ;
    return -1;
  }
  
  if( getpeername (socfd, &_theSockAddr, &_theSizeofSockAddrData) != OK ) {
     sprintf( _UFerrmsg, __HERE__ "> Unable to getpeername for socket fd= %d",
	      socfd );
     ufLog( _UFerrmsg ) ;
     return -1;
  }
  else /* if (_verbose) */ {
    sprintf( _UFerrmsg, __HERE__ "> socfd: %d", socfd);
    ufLog( _UFerrmsg ) ;
  }

  if (ERROR != ioctl (socfd, FIONREAD, (int) &rcnt)) {
    if (rcnt >= 0) {
     /* if( recv( socfd, &recvBuf, 1, MSG_DONTWAIT | 
	     MSG_PEEK ) <= 0 ) { */
      if (recv (socfd, &recvBuf, 1, MSG_PEEK) < 0) {
	sprintf( _UFerrmsg, __HERE__ "> peek-recv() returned error on "
	        "socfd %d: %s", socfd, strerror (errno));
	ufLog( _UFerrmsg ) ;
        return -3;
      }
    }
  }
  else {
    sprintf( _UFerrmsg, __HERE__ "> ioctl returned error on socfd %d: %s\n",
	     socfd, strerror (errno));
    ufLog( _UFerrmsg ) ;
    return -2;
  }

  FD_ZERO (&writefds);
  FD_SET (socfd, &writefds);
  FD_ZERO (&readfds);
  FD_SET (socfd, &readfds);
  FD_ZERO (&excptfds);
  FD_SET (socfd, &excptfds);

  if( timeOut > 0.00000001 ) {
    _timeOut = timeOut;
    timeout.tv_sec = (long) floor (_timeOut);
    timeout.tv_usec = (unsigned long) floor (1000000 * (_timeOut - timeout.tv_sec));
    usetimeout = &timeout;
  }
  else if( timeOut > -0.00000001 ) { /* ~zero timout */
    usetimeout = &poll;
    /* usetimeout = &tenthsec; try a short timeout (0.1 sec) */
  }
  else { /* negative timeout indicates infinite (wait forever, blocking) */
    usetimeout = infinit;
  }

  do {
    /* since select may modify this area of memory, always provide fresh input: */
    if (usetimeout) {
      to = *usetimeout;
      to_p = &to;
    }
    errno = 0;
    fdcnt = select (1 + socfd, &readfds, &writefds, &excptfds, to_p);
  } while (errno == EINTR && --cnt >= 0);

  if (_verbose) {
    sprintf( _UFerrmsg, __HERE__ "> fdcnt: %d", fdcnt ) ;
    ufLog( _UFerrmsg ) ;
  }

  if( errno == EBADF ) {
    sprintf( _UFerrmsg, __HERE__ "> Invalid (EBADF) socket fd: %d", socfd);

    printf ("checkSoc> invalid (EBADF) socket fd= %d\n", socfd);
    ufLog( _UFerrmsg ) ;
    return -2;
  }
  if( fdcnt < 0 ) {
    sprintf( _UFerrmsg, __HERE__ "> Error in select() : %s", strerror( errno ) );
    ufLog( _UFerrmsg ) ;
    return fdcnt;
  }
  if( fdcnt == 0 ) {
    sprintf( _UFerrmsg, __HERE__ "> select() time-out" ) ;
    ufLog( _UFerrmsg ) ;
    return fdcnt;
  }

  /**
   * If the OS has detected a closed socket connection, then both
   * read and write bits will be set for that fd.
   */
  if( FD_ISSET( socfd, &readfds ) )
    {
      char buf[ 16 ] ;
      int bytesRead = recv( socfd, buf, 1, MSG_PEEK ) ;
      if( bytesRead < 0 )
	{
	  /* Error on socket */
	  sprintf( _UFerrmsg, __HERE__ "> recv() returned error: %s",
		   strerror( errno ) ) ;
	  ufLog( _UFerrmsg ) ;

	  return -1 ;
	}
      else if( 0 == bytesRead )
	{
	  /* Disconnected */
	  sprintf( _UFerrmsg, __HERE__ "> Socket disconnected" ) ;
	  ufLog( _UFerrmsg ) ;

	  return -1 ;
	}
    }

  /* if writable, should return > 0 */
  if (FD_ISSET (socfd, &writefds))
    return socfd;
  else
    return -1;
}

int
reassign_bingo ()
{
  if (bingo == 1)
    bingo = 0;
  else
    bingo = 1;
  return OK;
}

int
near_home (int how_close)
{
  if (use_near_home == 1)
    {
      use_near_home = 0;
      printf ("NOT using near home algorithm\n");
    }
  else
    {
      use_near_home = 1;
      near_home_margin = how_close;
      printf ("Using near home algorithm at %d from 0 position\n",
	      near_home_margin);
    }
  return OK;
}


int
backlash (int how_much)
{
  if (use_backlash == 1)
    {
      use_backlash = 0;
      printf ("NOT using backlash\n");
    }
  else
    {
      use_backlash = 1;
      backlash_margin = how_much;
      printf ("Using backlash algorithm\n");
    }
  return OK;
}

int
testing ()
{
  int i;
  for (i = 0; i < 50; i++)
    printf ("***************** The socket %d is : %d \n", i,
	    checkSoc (i, 0.5));
  return 0;
}




/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/
int
bingo_test ()
{
  int socfd;			/* Integer to hold the
				   socket number */
  char **message;		/* Holds the string(s) to
				   be sent */
  char **response;		/* Holds the string(s)
				   received */
  int num_bytes_sent;
  int tot_bytes;
  int num_strings_received;
  int status;			/* status of closing the
				   connection */
  static ufProtocolHeader myheader;	/* Information
					   about the
					   receive */
  /* Initialize the message string(s) */
  char mymessage1[] = "Hello World";
  char mymessage2[] = "Goodbye World";
  message[0] = (char *) mymessage1;
  message[1] = (char *) mymessage2;
  printf ("The message is %s\n", message[0]);
  printf ("The message is %s\n", message[1]);
  /* open the connection */
  socfd = ufConnect ("128.227.184.153", 55555);
  printf ("The socket number is: %d\n", socfd);
  if (socfd >= 0)
    {
      /* send the message: Socket number, message string,
         number of strings being sent */
      num_bytes_sent = ufStringsSend (socfd, message, 2, "hello");
      printf ("The number of bytes sent is %d\n", num_bytes_sent);
      if (num_bytes_sent > 0)
	{
	  /* receive the response from the server */
	  response =
	    ufStringsRecv (socfd, &num_strings_received, &myheader,
			   &tot_bytes);
	  /* print the information from the header */
	  if (response != NULL)
	    {
	      printf ("The number of strings received is %d\n",
		      num_strings_received);
	      printf ("The number of total bytes is %d\n", tot_bytes);
	      printf ("The length of the header is %d\n", myheader.length);
	      printf ("The type of the header is %d\n", myheader.type);
	      printf ("The elem of the header is %d\n", myheader.elem);
	      printf ("The seqCnt of the header is %d\n", myheader.seqCnt);
	      printf ("The seqTot of the header is %d\n", myheader.seqTot);
	      printf ("The duration of the header is %f\n",
		      myheader.duration);
	      printf ("The timestamp of the header is %s\n",
		      myheader.timestamp);
	      printf ("The name of the header is %s\n", myheader.name);
	      /* Print the string(s) received */
	      printf ("The response is %s\n", response[0]);
	      printf ("The response is %s\n", response[1]);

	      /* Close the connection */
	      status = ufClose (socfd);
	      printf ("The status is %d\n", status);
	      if (status != 0)
		{
		  printf ("%s\n", "Error closing the connection");
		}
	    }
	  else
	    {
	      printf ("%s\n", "Unable to recieve response");
	    }
	}
      else
	{
	  printf ("%s\n", "Unable to send message");
	}
    }
  else
    {
      printf ("%s\n", "Unable to create socket");
    }
  return 0;
}

#endif
