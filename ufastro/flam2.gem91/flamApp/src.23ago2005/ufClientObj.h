#if !defined(__UFClientObj_h__)
#define __UFClientObj_h__ "$Name:  $ $Id: ufClientObj.h,v 0.0 2005/09/01 20:25:30 drashkin Exp $"
#define __UFClientObj_H__(arg) const char arg##ClientObj_h__rcsId[] = __UFClientObj_h__;

#include "sys/types.h"
#include "arpa/inet.h"

typedef struct
{
  int length, type, elem;
  unsigned short seqCnt, seqTot;
  float duration;
  char timestamp[25];
  char name[83];
}
ufProtocolHeader;

/* ufPixConfig* p = sizeof(ufPixConfig) + (LUTSize-1)*sizeof(unsigned int); */
typedef struct
{
  ufProtocolHeader hdr;
  int pixelSortID, LUTSize;
  unsigned int pixLUT[1];
}
ufPixConfig;

typedef struct
{
  ufProtocolHeader hdr;
  /* note there are currently 10 (int) elements */
  int w, h, d, littleEnd, dmaCnt, imageCnt, coAdds,
    pixelSortID, frmObsSeqNo, frmObsSeqTot;
}
ufFrmConfig;

/**
 * obsflag[1] is the place-holder for the contiguous vector
 * of 16bit flags that indicate the complete
 * observation sequence. must always used via pointer (this way):
 * int sz = sizeof(ufObsConfig) +
 * sizeof(short)*(nodbeams*chopbeams*savesets*nodsets - 1);
 * ufObsConfig* obsptr = (ufObsConfig*) malloc(sz);
 *
 * where ufFrmConfStatus.frmObsSeqTot == nodbeams*chopbeams*savesets*nodsets;
 * and the flags are accessible via index:
 * ufObsConfig.obsFlag[0] --> 
 *  ufObsConfig.obsFlag[nodbeams*chopbeams*savesets*nodsets - 1]
 */
typedef struct
{
  ufProtocolHeader hdr;
  int nodBeams, chopBeams, saveSets, nodSets;
  unsigned short obsFlags[1];
}
ufObsConfig;

/** uf protocol object header socket i/o */

extern int ufSendHeader (int socFd, ufProtocolHeader * hdr);
extern int ufRecvHeader (int socFd, ufProtocolHeader * hdr);
extern int ufHeaderLength (const ufProtocolHeader * hdr);

/* ufStrings socket i/o *********/

extern int ufSendStrings (int socFd, char **strings, int Nstrings);
extern char **ufRecvStrings (int socFd, int *Nstring,
			     ufProtocolHeader * hdr, int *nbtot);

/** all the func. below are by Agent name, rather than socFd */

/******** uf protocol object header i/o **********/

extern int ufSendHeaderAgent (const char *agent, ufProtocolHeader * hdr,
			      int *socFd);
extern int ufRecvHeaderAgent (const char *agent, ufProtocolHeader * hdr,
			      int *socFd);

/*** ufStrings i/o *********/

extern int ufSendStringsAgent (const char *agent, char **strings,
			       int Nstrings);
extern char **ufRecvStringsAgent (const char *agent, int *Nstring,
				  ufProtocolHeader * hdr, int *nbtot);

/*** ufFrmConfig & ufObsConfig i/o *********/

extern int ufSendFrmConf (const char *agent, ufFrmConfig * uffc);
extern int ufRecvFrmConf (const char *agent, ufFrmConfig * uffc);
extern int ufSendObsConf (const char *agent, ufObsConfig * ufoc,
			  short *obsFlags);
extern int ufRecvObsConf (const char *agent, ufObsConfig * ufoc,
			  short **obsFlags);

/*** Frames (image) i/o: (not ufFrames but ufInts (32-bit) class) ***/

extern int ufSendFrame (const char *agent, const char *bufname,
			int *bufptr, int bufsiz);

extern int ufRecvFrame (const char *agent, ufProtocolHeader * hdr,
			int *bufptr, int bufsiz);

/* req & recv (calls ufRecvFrame) */
extern int ufFetchFrame (const char *agent, const char *bufname,
			 ufProtocolHeader * hdr, int *bufptr, int bufsiz);

/* ufRequest:
 *    request = "buffer name" for image (Frame) from a buffer,
 * or request = "BN" for buffer names (ufStrings),
 * or request = "FC" for ufFrmConfig,
 * or request = "OC" for ufObsConfig.
 * or request = "PC" for ufPixConfig.
 * handshake: client sends standalone ProtoHdr with request
 * in "name" field, (bufnames should also encode type info ?,
 * e.g. "Sig1Accum:Byte" or "Sig2Accum:Int").
 * server replies with data object (Hdr and data array).
 */
extern int ufRequest (const char *agent, const char *request);

/**
 * 2 - 14 ? frame request server first replies with modified
 * name strings: name::timestamp::ByteOrInt (numbuf),
 * then with frame data
 */
extern char **ufReqMultiFrame (const char *agent, int numbuf,
			       char **bufnames);

/** Byte Frames */
extern int ufRecvBFrame (const char *agent, ufProtocolHeader * hdr,
			 unsigned char *bufptr);
extern int ufSendBFrame (const char *agent, ufProtocolHeader * hdr,
			 unsigned char *bufptr);

/** req & recv */
extern int ufFetchBFrame (const char *agent, const char *bufname,
			  ufProtocolHeader * hdr, unsigned char *bufptr);

#endif /* __UFClientObj_h__ */
