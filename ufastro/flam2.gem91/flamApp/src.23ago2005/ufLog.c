
#if !defined(__UFLog_c__)
#define __UFLog_c__ "$Name:  $ $Id: ufLog.c,v 0.0 2005/09/01 20:25:30 drashkin Exp $"
static const char rcsIdUFLog[] = __UFLog_c__;

#include "sys/types.h"
#include "fcntl.h"
#include "unistd.h"

#include "stdio.h"
#include "errno.h"
#include "string.h"
#include "stdlib.h"

#include "ufLog.h"
__UFLog_H__ (Log_c)

#define ARRAY_SIZE 100

static char* array[ ARRAY_SIZE ] ;
static size_t nextFreeSlot = 0 ;

static FILE* logFile = 0 ;

static unsigned short int _ufLogStdOut = 0 ;
static unsigned short int _ufLogFile = 0 ;

void ufLogStdOut()
{
  _ufLogStdOut = (0 == _ufLogStdOut) ? 1 : 0 ;

  if( 0 == _ufLogStdOut )
    {
      fprintf( stdout, __HERE__ "> Logging to stdout disabled\n" ) ;
    }
  else
    {
      fprintf( stdout, __HERE__ "> Logging to stdout enabled\n" ) ;
    }
}

void ufLogFile()
{
  _ufLogFile = (0 == _ufLogFile) ? 1 : 0 ;

  if( 0 == _ufLogFile )
    {
      fprintf( stdout, __HERE__ "> Logging to file disabled\n" ) ;
    }
  else
    {
      fprintf( stdout, __HERE__ "> Logging to file enabled\n" ) ;
    }
}

char* ufStrDup( const char* orig )
{
  char* retMe = 0 ;

  if( 0 == orig )
    {
      return 0 ;
    }

  retMe = malloc( strlen( orig ) + 1 ) ;
  strcpy( retMe, orig ) ;

  return retMe ;
}

void ufLog (const char *msg)
{
  size_t numBytes = 0 ;

  if( _ufLogStdOut != 0 )
    {
      /* Log to stdout */
      fprintf( stdout, "%s\n", msg ) ;
    }

  if( 0 == _ufLogFile )
    {
      /* Do NOT perform debugging to file */
      return ;
    }

  if( nextFreeSlot < ARRAY_SIZE )
    {
      /* Make sure this one isn't being duplicated */
      if( 0 != nextFreeSlot && 0 == strcmp( msg, array[ nextFreeSlot - 1 ] ) )
	{
	  return ;
	}

      /* Still have room for this string */
      array[ nextFreeSlot++ ] = ufStrDup( msg ) ;

      /*
      if( 0 == (nextFreeSlot % 10) )
	{
	  fprintf( stdout, __HERE__ "> Buffered element %d\n",
		   nextFreeSlot ) ;
	}
      */

      return ;
    }

  /* Time to flush the buffer */
  fprintf( stdout, __HERE__ "> Flushing %d elements from buffer\n",
	   ARRAY_SIZE ) ;

  logFile = fopen( "./data/epicsDB.log", "a" ) ;

  if( 0 == logFile )
    {
      fprintf( stdout, "ufLog> Unable to open log file: %s\n",
	       strerror( errno ) ) ;
      return ;
    }

  /* File is open for writing */
  for( nextFreeSlot = 0 ; nextFreeSlot < ARRAY_SIZE ; ++nextFreeSlot )
    {
      numBytes += fprintf( logFile, "%s\n", array[ nextFreeSlot ] ) ;
      free( array[ nextFreeSlot ] ) ;
      array[ nextFreeSlot ] = 0 ;
    }

  fclose( logFile ) ;

  logFile = 0 ;
  nextFreeSlot = 0 ;

  fprintf( stdout, "ufLog> Wrote %d bytes\n", numBytes ) ;
}

#endif /* __UFLog_c__ */
