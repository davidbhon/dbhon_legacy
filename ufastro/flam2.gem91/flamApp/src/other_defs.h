#if !defined(__OTHERDEFS_H__)
#define __OTHERDEFS_H__ "RCS: $Name:  $ $Id: other_defs.h,v 0.0 2005/09/01 20:23:14 drashkin Exp $"
static const char rcsIdufOTHERDEFSH[] = __OTHERDEFS_H__;

/*
 * Structures returned by network data base library.  All addresses are
 * supplied in host order, and returned in network order (suitable for
 * use in system calls).
 */
struct hostent
{
  char *h_name;			/* official name of host */
  char **h_aliases;		/* alias list */
  int h_addrtype;		/* host address type */
  int h_length;			/* length of address */
  char **h_addr_list;		/* list of addresses from
				   name server */
#define	h_addr	h_addr_list[0]	/* address, for backward
				   compatiblity */
  unsigned int h_ttl;		/* Time to Live in Seconds
				   for this entry */
};

typedef struct unix__dirdesc
{
  int dd_fd;			/* file descriptor */
  long d_loc;			/* buf offset of entry from 
				   last readddir() */
  long dd_size;			/* amount of valid data in
				   buffer */
  long dd_bsize;		/* amount of entries read
				   at a time */
  long dd_off;			/* Current offset in dir
				   (for telldir) */
  char *dd_buf;			/* directory data buffer */
}
UNIX_DIR;

#define   MSG_WAITALL     0x40	/* wait for full request or 
				   error */

#define	isnanf(x)	(((*(long *)&(x) & 0x7f800000L) == 0x7f800000L) && \
			    ((*(long *)&(x) & 0x007fffffL) != 0x00000000L))

typedef struct timespec timespec_t;
typedef unsigned char lock_t;

struct hostent *v_gethostbyname (const char *hostname);


struct tm *v_localtime_r (const time_t * time_tvalue, struct tm *tmvalue);

struct tm *v_gmtime_r (const time_t * time_tvalue, struct tm *tmvalue);

#endif
