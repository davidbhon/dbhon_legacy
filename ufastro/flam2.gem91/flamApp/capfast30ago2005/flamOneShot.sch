[schematic2]
uniq 141
[tools]
[detail]
w 946 1611 100 0 n#139 ecalcs.ecalcs#83.FLNK 1920 800 1984 800 1984 1600 -32 1600 -32 1280 160 1280 ebos.ebos#68.SLNK
w 1522 907 100 0 n#109 ecalcs.ecalcs#63.VAL 1376 896 1632 896 ecalcs.ecalcs#83.INPC
w 738 1419 100 0 n#109 junction 1472 896 1472 1408 64 1408 64 1312 160 1312 ebos.ebos#68.DOL
w 82 1819 100 0 n#137 hwin.hwin#138.in 64 1856 64 1808 160 1808 elongins.elongins#136.INP
w 1490 587 100 0 n#135 ecalcs.ecalcs#63.FLNK 1376 928 1408 928 1408 576 1632 576 ecalcs.ecalcs#83.SLNK
w -14 907 -100 0 n#117 inhier.RESET.P -32 896 64 896 64 992 160 992 ebos.ebos#64.SLNK
w 946 1771 100 0 n#131 elongins.elongins#136.VAL 416 1760 1536 1760 1536 960 1632 960 ecalcs.ecalcs#83.INPA
w 946 1099 100 0 n#140 ebis.ebis#79.VAL 864 1088 1088 1088 ecalcs.ecalcs#63.INPA
w 2012 779 -100 0 FAILED ecalcs.ecalcs#83.VAL 1920 768 2176 768 outhier.RUNNING.p
w 536 1115 100 0 n#80 ebis.ebis#79.SLNK 608 1104 512 1104 junction
w 440 1259 100 0 n#80 ebos.ebos#68.OUT 416 1248 512 1248 512 960 416 960 ebos.ebos#64.OUT
w 88 1035 100 0 n#72 hwin.hwin#66.in 64 1120 64 1024 160 1024 ebos.ebos#64.DOL
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos One-Shot Module
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2001/01/11
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamOneShot.sch
s 2016 2032 100 1792 A
s 2208 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/01/11
[cell use]
use hwin -128 1079 100 0 hwin#66
xform 0 -32 1120
p -125 1112 100 0 -1 val(in):0
use hwin -128 1815 100 0 hwin#138
xform 0 -32 1856
p -272 1856 100 0 -1 val(in):$(timeout)
use elongins 160 1703 100 0 elongins#136
xform 0 288 1776
p 272 1696 100 1024 1 name:$(timer)MaxTime
use outhier 2208 768 100 0 RUNNING
xform 0 2160 768
use inhier -128 896 100 0 RESET
xform 0 -32 896
use ecalcs 1632 487 100 0 ecalcs#83
xform 0 1776 752
p 1696 448 100 0 1 CALC:(C<A)?1:0
p 1808 480 100 1024 1 name:$(timer)Running
use ecalcs 1088 615 100 0 ecalcs#63
xform 0 1232 880
p 1152 576 100 0 1 CALC:A+1
p 1152 544 100 0 1 SCAN:.1 second
p 1264 608 100 1024 1 name:$(timer)Timer
use ebis 608 1031 100 0 ebis#79
xform 0 736 1104
p 784 1024 100 1024 1 name:$(timer)Base
use ebos 160 1191 100 0 ebos#68
xform 0 288 1280
p 224 1152 100 0 1 OMSL:closed_loop
p 336 1184 100 1024 1 name:$(timer)Last
p 128 1312 75 1280 -1 pproc(DOL):NPP
p 416 1248 75 768 -1 pproc(OUT):PP
use ebos 160 903 100 0 ebos#64
xform 0 288 992
p 224 864 100 0 1 OMSL:closed_loop
p 352 896 100 1024 1 name:$(timer)Reset
p 416 960 75 768 -1 pproc(OUT):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamOneShot.sch,v 0.0 2005/09/01 20:21:37 drashkin Exp $
