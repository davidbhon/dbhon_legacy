[schematic2]
uniq 54
[tools]
[detail]
s 2624 2064 100 1792 2001/01/25
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2528 -240 100 1792 flamSadEcConfigure.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos EC Configure
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs 384 1671 100 0 esirs#48
xform 0 592 1824
p 448 1632 100 0 1 SCAN:Passive
p 544 1664 100 1024 -1 name:$(top)MOSKELV
use esirs 960 1671 100 0 esirs#49
xform 0 1168 1824
p 1024 1632 100 0 1 SCAN:Passive
p 1120 1664 100 1024 -1 name:$(top)camHeaterPower2
use esirs 960 1255 100 0 esirs#50
xform 0 1168 1408
p 896 960 100 0 0 FTVL:FLOAT
p 1024 1216 100 0 1 SCAN:Passive
p 1120 1248 100 1024 -1 name:$(top)camHeaterSetpoint2
use esirs 368 1255 100 0 esirs#51
xform 0 576 1408
p 432 1216 100 0 1 SCAN:Passive
p 576 1536 100 1024 -1 name:$(top)CAMKELV
use esirs 1536 1671 100 0 esirs#52
xform 0 1744 1824
p 1600 1632 100 0 1 SCAN:Passive
p 1696 1664 100 1024 -1 name:$(top)CAMVAC
use esirs 1536 1239 100 0 esirs#53
xform 0 1744 1392
p 1600 1200 100 0 1 SCAN:Passive
p 1696 1232 100 1024 -1 name:$(top)MOSVAC
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadEcConfig.sch,v 0.0 2005/09/01 20:21:37 drashkin Exp $
