[schematic2]
uniq 46
[tools]
[detail]
s 2624 2032 100 1792 2000/11/11
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 flamSadWcs.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2000/11/11
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos World Coordinate System
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs -112 -89 100 0 esirs#36
xform 0 96 64
p -48 -128 100 0 1 SCAN:Passive
p 48 -96 100 1024 -1 name:$(top)wcs12
use esirs -112 327 100 0 esirs#37
xform 0 96 480
p -48 288 100 0 1 SCAN:Passive
p 48 320 100 1024 -1 name:$(top)wcs11
use esirs -112 743 100 0 esirs#38
xform 0 96 896
p -48 704 100 0 1 SCAN:Passive
p 48 736 100 1024 -1 name:$(top)wcsTai
use esirs -112 1159 100 0 esirs#39
xform 0 96 1312
p -48 1120 100 0 1 SCAN:Passive
p 48 1152 100 1024 -1 name:$(top)wcsRA
use esirs -112 1575 100 0 esirs#40
xform 0 96 1728
p -48 1536 100 0 1 SCAN:Passive
p 48 1568 100 1024 -1 name:$(top)wcsDec
use esirs 592 1559 100 0 esirs#41
xform 0 800 1712
p 656 1520 100 0 1 SCAN:Passive
p 752 1552 100 1024 -1 name:$(top)wcs13
use esirs 592 1143 100 0 esirs#42
xform 0 800 1296
p 656 1104 100 0 1 SCAN:Passive
p 752 1136 100 1024 -1 name:$(top)wcs21
use esirs 592 727 100 0 esirs#43
xform 0 800 880
p 656 688 100 0 1 SCAN:Passive
p 752 720 100 1024 -1 name:$(top)wcs22
use esirs 592 311 100 0 esirs#44
xform 0 800 464
p 656 272 100 0 1 SCAN:Passive
p 752 304 100 1024 -1 name:$(top)wcs23
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadWcs.sch,v 0.0 2005/09/01 20:21:37 drashkin Exp $
