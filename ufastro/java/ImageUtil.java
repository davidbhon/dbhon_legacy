import java.io.*;
import java.util.*;
import java.net.*;
import java.applet.*;
import java.awt.*;
import java.awt.image.*;
import java.math.*;// so that you can use the random number generator
public class ImageUtil {

  public static int gMin = 0;    // global min pixel value for current image
  public static int gMax = 0;    // global max pixel value for current image
  public static double gMean = 0;   // global mean value for current image
  public static double gRMS = 0;   // global RMS value for current image
    public static Random rand =new Random();// for generating random numbers

  // convert byte array to an int array for creating an image
  public static int[] byteToIntImage(byte[] byteImg) {
      int arrayLen = byteImg.length;
      int[] intImg = new int[arrayLen];
      for (int i = 0; i < arrayLen; i++) {
	  intImg[i] = 0xff000000 | (int)((byteImg[i]/255.0) * 256*256*256); // scale byte up to 24 bits
      }
      return intImg;  // return new int array
  }

  // convert byte array to an int array
  public static int[] byteToInt(byte[] byteImg) {
      int arrayLen = byteImg.length/4;
      System.out.println("byteToInt> arrayLen = " + arrayLen);
//       for (int k = 0; k < 8; k++)
// 	  System.out.println("byteToInt> byteImg[" + k + "] = " + byteImg[k]);
      int[] intImg = new int[arrayLen];
      int offset = 0;
      for (int i = 0; i < arrayLen; i++) {
	  offset = i*4;
	  intImg[i] = ((byteImg[offset]*256*256*256) & 0xff000000) |
	              ((byteImg[offset+1]*256*256) & 0x00ff0000) |
	              ((byteImg[offset+2]*256) & 0x0000ff00) |
                      (byteImg[offset+3] & 0x000000ff);
      }
//       for (int k = 0; k < 8; k++)
// 	  System.out.println("byteToInt> intImg[" + k + "] = " + intImg[k]);
      return intImg;  // return new int array
  }

  // convert byte array to an float array
  public static float[] byteToFloat(byte[] byteImg) {
      int arrayLen = byteImg.length/4;
      System.out.println("byteToFloat> arrayLen = " + arrayLen);
//       for (int k = 0; k < 8; k++)
// 	  System.out.println("byteToInt> byteImg[" + k + "] = " + byteImg[k]);
      float[] floatImg = new float[arrayLen];
      int offset = 0;
      for (int i = 0; i < arrayLen; i++) {
	  offset = i*4;
	  floatImg[i] = (float)( ((byteImg[offset] << 24) & 0xff000000) |
	              ((byteImg[offset+1] << 16) & 0x00ff0000) |
	              ((byteImg[offset+2] << 8) & 0x0000ff00) |
                      (byteImg[offset+3] & 0x000000ff) );
      }
//       for (int k = 0; k < 8; k++)
// 	  System.out.println("byteToInt> intImg[" + k + "] = " + intImg[k]);
      return floatImg;  // return new float array
  }

  // convert int array to a byte array
  public static byte[] intToByte(int[] intImg) {
      int arrayLen = intImg.length;
      byte[] byteImg = new byte[arrayLen*4];
      int offset = 0;
      for (int i = 0; i < arrayLen; i++) {
	  offset = i*4;
	  byteImg[offset]   = (byte)((intImg[i] & 0xff000000) >> 24);
	  byteImg[offset+1] = (byte)((intImg[i] & 0x00ff0000) >> 16);
	  byteImg[offset+2] = (byte)((intImg[i] & 0x0000ff00) >> 8);
	  byteImg[offset+3] = (byte)(intImg[i] & 0x000000ff);
      }
      return byteImg;  // return new byte array
  }


  // convert a 128x128 byte array to 256x256 byte array
  public static byte[] img128to256(byte[] img128) {
      byte[] img256 = new byte[256*256];
      for (int row = 0; row < 128; row++) {
	  for (int col = 0; col < 128; col++) {
	      img256[(row*2*256) + (col*2)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+1)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+256)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+1+256)] = img128[(row*128) + col];
	  }
      }
      return img256;  // return new 256x256 byte array
  }

  // convert a 128x128 int array to 256x256 int array
  public static int[] img128to256(int[] img128) {
      int[] img256 = new int[256*256];
      for (int row = 0; row < 128; row++) {
	  for (int col = 0; col < 128; col++) {
	      img256[(row*2*256) + (col*2)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+1)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+256)] = img128[(row*128) + col];
	      img256[(row*2*256) + (col*2+1+256)] = img128[(row*128) + col];
	  }
      }
      return img256;  // return new 256x256 int array
  }


  public static void findMeanAndRMS(int[] data) {
      gMin = Integer.MAX_VALUE;
      gMax = Integer.MIN_VALUE;

      double sum = 0;
      double sqrSum = 0;

      int arrayLength = data.length;
      for (int i = 0; i < arrayLength; i++) {
	  sum += data[i];
      }

      gMean = sum/arrayLength;

      for (int i = 0; i < arrayLength; i++) { 
          double v = data[i] - gMean;
	  sqrSum += v*v;
      }

      gRMS = Math.sqrt(sqrSum/(arrayLength-1));

      for (int i = 0; i < arrayLength; i++) {
         if (data[i] > gMax)
            gMax = data[i];
         if (data[i] < gMin)
            gMin = data[i];
     }
  }

  // convertors
  static public int indexOf(int x, int y, int D) {
    int unit_step = 0;
    if( x>=2 ) unit_step = 1;
    return 2*y+x%2+D*(x - x%2)*unit_step;
  }

  static public int frameToImage(int[] frm, int[] img, int D) {
    int x, y;
    for( y=0; y < D; y++ ) {
      for( x=0; x < D; x++ ) {
        int n = indexOf(x,y,D);
        img[x+y*D] = frm[n];
      }
    }
    return D;
  }

  static public int imageToFrame(int[] img, int[] frm, int D) {
    int x, y;
    for( y=0; y < D; y++ ) {
      for( x=0; x < D; x++ ) {
        int n = indexOf(x,y,D);
        frm[n] = img[x+y*D];
      }
    }
    return D;
  }
    
    static public int testimg(int[] tstimg, int D) {
	int r =rand.nextInt()%1024;
	for( int y=0; y<D; y++ )
	    for( int x=0; x<D; x++ )
		tstimg[x+y*D] = ((rand.nextInt()%65000) + y*r) | 0xff000000;  
	// OR top bits to set saturation/alpha level
	return 1; //(D-1) + (D-1)*w;
    }
    
    static public int testimg(byte[] tstimg, int D) {
	for( int y=0; y<D; y++ )
	    for( int x=0; x<D; x++ )
		tstimg[x+y*D] = (byte)((x + y*D)%255);
	return 1; //(D-1) + (D-1)*w;
    }


  static public int greyscale(float[] buff, float max, int[] rgbPixels, int w, int h) { 
    System.out.println("greyscale> float version: --> red|green|blue, max= "+max);
    int red = 0x00ff0000&Color.red.getRGB();
    int green = 0x0000ff00&Color.green.getRGB();
    int blue = 0x000000ff&Color.blue.getRGB();
    int grey = 0x000000ff;
    System.out.println("red= "+red+"; green= "+green+", blue= "+blue);
    int colorRGB = red|green|blue;
    System.out.println("colorRGB= "+colorRGB+"; max= "+max);
    int index = 0;
    for( int y=0; y<h; y++ ) {
      for( int x=0; x<w; x++, index++ ) {
	// greyscale
	int color = (int)(255 * (buff[index] / max));
        //color += 256*color + 256*256*color;
        red = 256*256*color;
        green = 256*color;
        blue = color;
        //System.out.println("x,y= "+x+","+y+"; color= "+color+"; data= "+buff[index]);
	rgbPixels[index] = 0xff000000|red|green|blue;
      }
    }
    --index;
    System.out.println("index= "+index+" rgbPixel= "+rgbPixels[index]+"; data= "+buff[index]);
    return index;
  }


  static public int greyscale(int[] buff, int max, int[] rgbPixels, int w, int h) { 
    System.err.println("greyscale> int version: red|green|blue");
    int red = 0x00ff0000&Color.red.getRGB();
    int green = 0x0000ff00&Color.green.getRGB();
    int blue = 0x000000ff&Color.blue.getRGB();
    int grey = 0x000000ff;
    System.out.println("red= "+red+"; green= "+green+", blue= "+blue);
    int colorRGB = red|green|blue;
    System.out.println("colorRGB= "+colorRGB+"; max= "+max);
    int index = 0;
    for( int y=0; y<h; y++ ) {
      for( int x=0; x<w; x++, index++ ) {
	  //System.out.println("y= "+y+" | x= "+x);
	// greyscale
	int color;
	if (max == 0)
	    color = 0;
	else
	    color = (int)Math.rint(255 * buff[index] / max);
//	int color = (255 * buff[index] / max);
        //color += 256*color + 256*256*color;
        red = 256*256*color;
        green = 256*color;
        blue = color;
        //System.out.println("x,y= "+x+","+y+"; color= "+color+"; data= "+buff[index]);
	rgbPixels[index] = 0xff000000|red|green|blue;
      }
    }
    --index;
    System.out.println("greyscale(int)> after index--");

    System.out.println("greyscale(int)> index= "+index+" rgbPixel= "+rgbPixels[index]+"; data= "+buff[index]);
    return index;
  }

//   static public int greyscale(int[] buff, int max, int[] rgbPixels, int w, int h) { 
//     System.err.println("greyscale> int version: red|green|blue");
//     int red = 0x00ff0000&Color.red.getRGB();
//     int green = 0x0000ff00&Color.green.getRGB();
//     int blue = 0x000000ff&Color.blue.getRGB();
//     int grey = 0x000000ff;
//     System.out.println("red= "+red+"; green= "+green+", blue= "+blue);
//     int colorRGB = red|green|blue;
//     System.out.println("colorRGB= "+colorRGB+"; max= "+max);
//     int index = 0;
//     for( int y=0; y<h; y++ ) {
//       for( int x=0; x<w; x++, index++ ) {
// 	// greyscale
// 	int color = (int)Math.rint(255 * buff[index] / max);
// //	int color = (255 * buff[index] / max);
//         //color += 256*color + 256*256*color;
//         red = 256*256*color;
//         green = 256*color;
//         blue = color;
//         //System.out.println("x,y= "+x+","+y+"; color= "+color+"; data= "+buff[index]);
// 	rgbPixels[index] = 0xff000000|red|green|blue;
//       }
//     }
//     --index;
//     System.out.println("greyscale(int)> index= "+index+" rgbPixel= "+rgbPixels[index]+"; data= "+buff[index]);
//     return index;
//   }

  static public int greyscale(byte[] buff, int[] rgbPixels, int w, int h) { 
    System.out.println("greyscale> byte version: red|green|blue");
    int red = 0x00ff0000&Color.red.getRGB();
    int green = 0x0000ff00&Color.green.getRGB();
    int blue = 0x000000ff&Color.blue.getRGB();
    int grey = 0x000000ff;
    System.out.println("red= "+red+"; green= "+green+", blue= "+blue);
    int colorRGB = red|green|blue;
    System.out.println("(byte) colorRGB= "+colorRGB);

    String s = new String();
    for (int i = 0; i < 10; i++) {
	s = s + buff[i] + " ";
    }
    System.out.println(s);

    s = new String();
    for (int i = 0; i < 10; i++) {
	s = s + rgbPixels[i] + " ";
    }
    System.out.println(s);

    int index = 0;
    for( int y=0; y<h; y++ ) {
      for( int x=0; x<w; x++, index++ ) {
	// greyscale
	int color = buff[index];
        if (color < 0) {
            color += 256;  // we don't want negative numbers!
        }
        //red = green = blue = color;
        //System.out.println("x,y= "+x+","+y+"; color= "+color+"; data= "+buff[index]);
        red = 256*256*color;
        green = 256*color;
        blue = color;
	rgbPixels[index] = 0x7f000000|red|green|blue;
      }
    }

    s = new String();
    for (int i = 0; i < 10; i++) {
	s = s + buff[i] + " ";
    }
    System.out.println(s);

    s = new String();
    for (int i = 0; i < 10; i++) {
	s = s + rgbPixels[i] + " ";
    }
    System.out.println(s);

    --index;
    System.out.println("red = "+red+" green = "+green+" blue = "+blue);
    System.out.println("greyscale(byte)> index= "+index+" rgbPixel= "+rgbPixels[index]+"; data= "+buff[index]);
    return index;
  }

  static public void clearPixels(int[] rgbPixels, int w, int h) { 
    Color color = new Color(255,255,255);
    int index = 0;
    for( int y=0; y<h; y++ ) {
      for( int x=0; x<w; x++, index++ ) {
	rgbPixels[index] = color.getRGB();
      }
    }
    System.out.println("set all pixels to: "+color.getRGB());
  }
 

  static public int getMax(int[] theArray) {
    int prevValue = 0;
    int arrayLength = theArray.length;
      for (int i = 0; i < arrayLength; i++)
	if (theArray[i] > prevValue)
	    prevValue = theArray[i];
	return prevValue;
    }

} // end of class ImageWithStatus


