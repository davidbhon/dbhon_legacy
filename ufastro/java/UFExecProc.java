import java.io.*;
import java.util.*; // LinkedList, Arrays, etc.

class UFExecProc {
  public final String rcsId = "$Name:  $ $Id: UFExecProc.java,v 0.2 2005/12/12 19:13:17 hon Exp $";
  public static int execCmd(String cmd) {
    Runtime r = Runtime.getRuntime();
    Process p = null;
    try {
      System.err.println("exec cmd: " + cmd);
      p = r.exec(cmd); // should return whatever output is printed by RegUtilStart
    }
    catch( IOException e ) {
      System.err.println(e);
      return -1;
    }
    if( p == null ) {
      System.err.println("No Process object returned from exec of" + cmd);
      return -2;
    }
    BufferedReader bro = new BufferedReader(new InputStreamReader(p.getInputStream()));
    BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
    if( bro == null || bre == null ) {
      System.err.println("No BufferedReader object returned from exec of" + cmd);
      return -3;
    }
    try {
      p.waitFor();
    }
    catch(InterruptedException e) {
      System.err.println(e);
      return -4;
    }

    String so= null, se= null;
    try {
      do {
        so = bro.readLine(); se = bre.readLine();
        if( so != null ) System.err.println("RegUtilStart stdout: " + so);
        if( se != null ) System.err.println("RegUtilStart stderr: " + se);
      } while(so != null || se != null);
    }
    catch(IOException e) {
      System.err.println(e);
      return -5;
    }
    return 0;
  }
}
