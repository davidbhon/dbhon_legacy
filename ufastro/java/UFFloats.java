import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

public class UFFloats extends UFTimeStamp {
  public UFFloats() {
    _length=_minLength();
    _type=MsgTyp._Floats_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _name = new String("");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _values = null;
  }

  public UFFloats(int length) {
    _length=length; //_MinLength_;
    _type=MsgTyp._Floats_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _name = new String("");
    _values = null;
  }

  public UFFloats(String name, float[] vals) {
    _name = new String(name);
    _type=MsgTyp._Floats_;
    _elem=vals.length;
    _values = vals;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _length = _minLength() + 4*vals.length;
  }

  // all methods declared abstract by UFProtocal 
  // can be defined here
  public String description() { return new String("UFFloats"); }
 
  // return size of the element's value (not it's name!)
  // either string length, or sizeof(float), sizeof(frame):
  public int valSize(int elemIdx) { 
    if( elemIdx >= _values.length )
      return 0;
    else
      return 4;
  }

  public float[] values() { return _values; }

  public float maxVal() {
    float max=0.0f;
    for( int i=0; i<_values.length; i++ )
      if( _values[i]>max ) max = _values[i];

    return max;
  }

  public int numVals() {
    return _values.length;
  }

  public float valData(int index) {
    return _values[index];
  }


///////////////////////////// protected: /////////////////////
  // additional attributes & methods (beyond base class's):
  protected float[] _values=null;

  protected void _copyNameAndVals(String s, float[] vals) {
    _name = new String(s);
    _elem = vals.length;
    _length = _minLength();
    _values = new float[vals.length];
    for( int i=0; i<_elem; i++ ) {
      _values[i] = vals[i];
      _length += 4;
    }
  }
 
  public void setNameAndVals(String s, float[] vals) {
    _name = new String(s);
    _elem = vals.length;
    _length = _minLength();
    _values = new float[vals.length];
    for( int i=0; i<_elem; i++ ) {
      _values[i] = vals[i];
      _length += 4;
    }
  }
 
  protected int _dataFrom(DataInputStream inp) {
    // restore everything but length & type
    int retval=0;
    try {
	//length and type have already been read
      _elem = inp.readInt(); // should be 0
      retval += 4;
      _seqCnt = inp.readShort(); // seqcnt and seqtot (both short ints)
      retval += 2;
      _seqTot = inp.readShort();
      retval += 2;
      _duration = inp.readFloat(); // read in 'duration'
      retval+=4;
      byte[] tbuf = new byte[_timestamp.length()];
      inp.readFully(tbuf, 0, _timestamp.length());
      _timestamp = new String(tbuf);
      int namelen = inp.readInt();
      retval += 4;
      byte[] namebuf = new byte[namelen];
      if( namelen > 0 ) {
	inp.readFully(namebuf, 0, namelen);
        retval +=  namelen;
	_name = new String(namebuf);
      }
      // 9/17/9 drn
      // this may not be right -- what if _values != null, but is smaller than _elem?
      //if( _values == null ) {
	_values = new float[_elem];
	//}
// COMMENT THIS OUT WHILE WE TRY TO READ IN AS BYTES AND CONVERT TO FLOATS
//       for( int elem=0; elem<_elem; elem++ ) {
// 	_values[elem] = inp.readFloat();
//         retval += 4;
//       }

//      System.out.println("UFFloats._dataFrom> Creating new byte array");
      byte[] temp = new byte[_elem*4];
//      System.out.println("UFFloats._dataFrom> About to readFully");
      inp.readFully(temp, 0, temp.length);

      inp = new DataInputStream(new ByteArrayInputStream(temp));
      for( int elem=0; elem<_elem; elem++ ) {
	_values[elem] = inp.readFloat();
        retval += 4;
      }

//      System.out.println("UFFloats._dataFrom> Converting byte array to float array");
//      _values = ImageUtil.byteToFloat(temp);
//      System.out.println("UFFloats._dataFrom> Byte array converted to float array");

    }

    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    // should compare retval to length-4
    return retval;
  }

  protected int _outputTo(DataOutputStream out) {
    // write out all attributes
    int retval=0;
    int slen=0;
    try {
      out.writeInt(_length);
      retval += 4;
      out.writeInt(_type);
      retval += 4;
      out.writeInt(_elem);
      retval += 4;
      out.writeShort(_seqCnt);//2 shorts -- seqcnt, seqtot
      retval += 2;
      out.writeShort(_seqTot);
      retval += 2;
      out.writeFloat(_duration);//duration
      retval += 4;
      out.writeBytes(_timestamp);
      retval += _timestamp.length();
      out.writeInt(_name.length());
      retval += 4;
      if( _name.length() > 0 ) {
	out.writeBytes(_name);
	retval += _name.length();
      }

//  Comment out while we try to create an array of bytes in memory and write
//  the floats to it.  Then send all bytes at once to socket
//       for( int elem=0; elem<_elem; elem++ ) {
//         out.writeFloat(_values[elem]);
//         retval += 4;
//       }

      ByteArrayOutputStream outArray = new ByteArrayOutputStream(_elem*4);
      DataOutputStream outToArray = new DataOutputStream(outArray);
      for( int elem=0; elem<_elem; elem++ ) {
	outToArray.writeFloat(_values[elem]);
        retval += 4;
      }

      outArray.writeTo(out); // write all floats as bytes in one chunk


    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    return retval;
  }

    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("description = " + description() + " | ");
	sb.append("_length = " + _length + " | ");
	sb.append("_minlength() = " + _minLength() + " | ");
	sb.append("_type = " + _type + " | ");
	sb.append("_timestamp = " + _timestamp + "\n");
	sb.append("_elem = " + _elem + " | ");
	sb.append("_name = " +  _name + " | ");
        sb.append("_values =\n");

	return new String(sb);
    }

}
