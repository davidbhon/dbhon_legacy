===========================================
Routing setup for UF IR Lab private network
===========================================

Setup routing on a gateway machine with two ethernet cards:
      1. For Linux, add to /etc/rc.d/rc.local:
	 (Sample setup taken from wolf.astro.ufl.edu where
	  192.168.111.0 is the VPN, 128.227.184.0 is the LAN,
	  and 128.227.184.153 is the gateway machine this is
	  being set up on)

	 route add -net 192.168.111.0 gw 192.168.111.1 netmask 255.255.255.0 eth1
	 route add -net 128.227.184.0 gw 128.227.184.153 netmask 255.255.255.0 eth0

	 # Make sure to enable ip forwarding before trying to set rules
	 echo 1 > /proc/sys/net/ipv4/ip_forward
	 #ipchains -P forward DENY
	 ipchains -A forward -i eth0 -j MASQ

      2. for Solaris, add to ??



Setup Annex for routing:
      1. Setup Annex IP Address

	 ???????


      2. Add route to default gateway

	 route add default 192.168.111.254