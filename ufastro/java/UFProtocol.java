import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

//public abstract class UFProtocol extends Object implements Externalizable {
public abstract class UFProtocol extends Object {
    
    // default ctor needed for containers and "virtual creation"
    public UFProtocol() {
	_length=_MinLength_;
	_type=MsgTyp._TimeStamp_;
	_timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
	_elem=0;
	_seqCnt = _seqTot = 0;
	_duration = 0.0f;
	_name = new String("");
    }
    
    // maybe useful
    protected UFProtocol(String name, int elem) {
	_length = _MinLength_; 
	_type = MsgTyp._TimeStamp_;
	_elem = elem;
	_timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
	_seqCnt = _seqTot = 0;
	_duration = 0.0f;
	_name = new String(name);
    }
    
    // run-time typing for client side:
    // since enum's are not part of the java language, use inner classes
     public static class MsgTyp {
	static final int _MsgError_=-1;
	static final int _TimeStamp_=0;
	static final int _Strings_=1;
	static final int _Bytes_=2;
        static final int _Shorts_=3;
	static final int _Ints_=4;
	static final int _Floats_=5;
        static final int _ObsConfig_=8;
	static final int _FrameConfig_=9;
	static final int _ByteFrames_=11;
	static final int _IntFrames_=13;
	static final int _FloatFrames_=14;
	static final int _BoeingFrames_=21;
	static final int _RaytheonFrames_=31;
    } // UFProtocol.MsgTyp
    
    public static class FrameDim { 
	static int _Boeing128_=128;
	static int _Boeing256_=256;
	static int _RaytheonW_=320;
	static int _RaytheonH_=240;
    } // UFProtocol.FrameDim
    
    // client default is push
    public static class ClientTyp {
	static int _Push_=0;
	static int _Pull_=1;
    } // UFProtocol.ClientTyp
    
    // for test support: operations, simulation, or 
    // test (use case/scenario) modes
    public static class ExecMode {
	static int _Sim_=-1;
	static int _Ops_=0;
	static int _Test1_=1;
	static int _Test2_=2;
	static int _Test3_=3;
    } // UFProtocol.ExecMode
    
    public static class LengthAndType {
	int length;
	int type;
	LengthAndType() { length = type = 0; }
    } // LengthAndType
    
    // should be congruent with _minlength() for empty name string:
    public static int _MinLength_=44;
    
    
    
    // return size of the element's value (not it's name!)
    // either string length, or sizeof(float), sizeof(frame):
    public abstract int valSize(int elemIdx); 
    
    // fetch type
    public int typeId() { return _type; }
    // fetch description
    public abstract String description();
    // fetch name
    public String name() { return _name; };
    // fetch timestamp
    public String timeStamp() { return _timestamp; };
    // fetch element count
    public int elements() { return _elem; }
    // total length of the message:
    public int length() { return _length; }

    //rename method
    public String rename(String newname) {
	String orig = _name;
	_length -= _name.length();
	_name = new String(newname);
	_length += _name.length();
	return orig;
    }
    
    // can support bundling a bunch of messages into one send/recv?:
    public static int sizeOf(Vector v) {
	int retval=0;
	for( Enumeration e = v.elements(); e.hasMoreElements(); ) {
	    retval += ((UFProtocol)e).length();
	}
	return retval;
    }
    
    public static int elementsOf(Vector v) {
      int retval=0;
      for( Enumeration e = v.elements(); e.hasMoreElements(); ) {
	  retval += ((UFProtocol)e).elements();
      }
      return retval;
  }
    
    // helper - read length and type from file DataInputStream
    public static UFProtocol.LengthAndType readLengthAndType(DataInputStream inp) {
	UFProtocol.LengthAndType retval = new UFProtocol.LengthAndType();
	try {
	    retval.length = inp.readInt();
      System.err.println("length= "+retval.length);
      retval.type = inp.readInt();
      System.err.println("type=  "+retval.type);
    }
	catch(EOFException eof) {
	System.err.println(eof);
    } 
    catch(IOException ioe) {
	System.err.println(ioe);
    }
    catch( Exception e ) {
	System.err.println(e);
    }
    return retval;
    }
    
    // helper - read length and type from file descriptor
    public static UFProtocol.LengthAndType readLengthAndType(File f) {
      UFProtocol.LengthAndType retval=null;
    try {
	DataInputStream inp = new DataInputStream(new FileInputStream(f));
      retval = readLengthAndType(inp);
    } 
    catch(IOException ioe) {
	System.err.println(ioe);
    }
    catch( Exception e ) {
	System.err.println(e);
    }
    return retval;
  }
    // restore data from file
    public abstract int readData(File f); 
    // write (serialize) msg out to file, returns 0 on failure, num. bytes output on success
    public abstract int writeTo(File f);
    
    // helper - read length and type from socket
    public static UFProtocol.LengthAndType recvLengthAndType(Socket soc) {
      UFProtocol.LengthAndType retval=null;
    try {
	DataInputStream inp = new DataInputStream(soc.getInputStream());
      retval = readLengthAndType(inp);
    }
    catch(IOException ioe) {
	System.err.println(ioe);
    }
    catch( Exception e ) {
	System.err.println(e);
    }
    return retval;
  }

  // restore data from socket
  public abstract int recvData(Socket soc); 
  // write msg out to socket, returns 0 on failure, num. bytes on success
  public abstract int sendTo(Socket soc); 

    // creates a new instance from a file, returns null on failure
    public static UFProtocol createFrom(File f) {
      Integer length=null;
      Integer msgtyp=null;
      UFProtocol.LengthAndType lt = readLengthAndType(f);
      
      UFProtocol retval = create(lt);
      // note that this must read length - sizeof(msgtyp) bytes!
      if( retval != null ) {
	  retval.readData(f);
	  System.err.println("retval=  "+retval.description());
      }
      return retval;
    }
    
    // creates a new instance on heap, from a socket, returns null on failure
    public static UFProtocol createFrom(Socket soc) {
       	UFProtocol.LengthAndType lt  = recvLengthAndType(soc);
	
	UFProtocol retval = create(lt);
	// note that this must read length - sizeof(msgtyp) bytes!
	if( retval != null ) {
	    retval.recvData(soc);
	    System.err.println("retval=  "+retval.description());
	}
	return retval;
    }
    

    // _length must be >= _minlength!
    // sizeof(namelen) + strlen(_name) (could be 0) +  
    // strlen(_timestamp) + sizeof(_type) + sizeof(_elem) + sizeof(_length)+... 
    // = 4+0+24+4+4+4+2+2+4 = 48
    protected int _minLength() {
	int minlength = 6*4+_timestamp.length()+_name.length();
	return minlength;
    }
    
    // helpers assume blocking (synchronous) i/o:
    protected static int writeFully(File f, byte[] buf, int sz) {
	int retval=0;
	try {
	    DataOutputStream out = new DataOutputStream(new FileOutputStream(f));
	    out.write(buf, 0, sz);
	}
	catch(EOFException eof) {
	    System.err.println(eof);
	} 
	catch(IOException ioe) {
	    System.err.println(ioe);
	}
	catch( Exception e ) {
	    System.err.println(e);
	}
	return retval;
    }
    protected static int readFully(File f, byte[] buf, int sz) {
    int retval=0;
    try {
      DataInputStream inp = new DataInputStream(new FileInputStream(f));
      inp.readFully(buf, 0, sz);
    }
    catch(EOFException eof) {
	System.err.println(eof);
    } 
    catch(IOException ioe) {
	System.err.println(ioe);
    }
    catch( Exception e ) {
	System.err.println(e);
    }
    return retval;
    }
    
    // must have knowledge of all possible descendents in hierarchy
    protected static UFProtocol create(UFProtocol.LengthAndType lt) {
	// lt.type = 12;
	// lt.length = 65591;
	System.err.println("create> type= "+lt.type+", length= "+lt.length);
	UFProtocol retval=null;
	switch(lt.type) {
	case MsgTyp._Strings_: retval = new UFStrings(lt.length);
	    System.err.println("create> After new UFStrings(lt.length);");
	    break;
	    
	case MsgTyp._Bytes_: retval = new UFBytes(lt.length);
	    break;
	    
	case MsgTyp._Ints_: retval = new UFInts(lt.length);
	    break;
	    
	case MsgTyp._Floats_: retval = new UFFloats(lt.length);
	    break;
	    
	    /*	case MsgTyp._ByteFrames_:
	    System.err.println("create> new UFByteFrames...");
	    retval = new UFByteFrames(lt.length);
	    break;
	    
	case MsgTyp._IntFrames_:
	    System.err.println("create> new UFIntFrames...");
	    retval = new UFIntFrames(lt.length);
	    break;
	    
	case MsgTyp._FloatFrames_:
	    System.err.println("create> new UFFloatFrames...");
	    retval = new UFFloatFrames(lt.length);
	    break;
	    */
	case MsgTyp._TimeStamp_: // default
	    
	default: retval = new UFTimeStamp(lt.length);
	    break;
	}
	
	return retval;
    }
      
    ///////////////////////// protected attributes and methods //////////////////////
    // all protocol message packets have a type, a name, a timestamp,
    // # of elements, and one or more values
    
    // total length of message (byte count) == 
    // strlen(name) + strlen(_timestamp) + 
    // sizeof(_type) + sizeof(_elem) + sizeof(_length) + lengths of value elements
    protected int _length;
    
    // type must be one enumation MsgTyp
    protected int _type;
    
    // timesamp: "yyyy:ddd:hh:mm:ss.uuuuuu" format is 18 char string
    protected String _timestamp="yyyy:ddd:hh:mm:ss.uuuuuu";
    
    // number of value elements (values)
    // _elem == 0 for timestamp
    // _elem >= 1 for Strings or Floats or Frames
    protected int _elem;
    
    // name is optional for timestamps, could indicate time zone
    protected String _name="";

    /**
     * Sequence count (1-n of Total) allows grouping of multiple
     * protocol objects 
     */
    protected short _seqCnt; 
    protected short _seqTot; 

    /**
     * In order to used shared memory, these need to be fixed length
     * (or we need to write a custom STL::Allocator for shared memory use)
     * timesamp: "yyyy:ddd:hh:mm:ss.uuuuuu" format is 24 char string
     * duration of data in seconds
     * normally duration is positive definite (>=0), however, we need
     * to support the concept of an "empty" protocol message with 
     * _elem > 0 to be used as a "client request" object for that protocol object
     * of _elem; so we'll just encode that as a negative _duration?
     */
    protected float _duration; //20
    

} // UFProtocol


