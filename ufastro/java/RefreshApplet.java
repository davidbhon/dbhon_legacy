import java.applet.*;
import java.awt.*;
import java.net.*;

// html for this applet:
//<applet code=RefreshApplet >
//<param name=AnimURL value="http://finance.yahhoo.com">
//</applet>

// this borrows from the nutshell example pg 66-67 SmoothCircle.java
                     
public class RefreshApplet extends Applet implements Runnable {
  URL _amex= null;
  Thread _anim= null;
  long _millisleep= 30000;
  volatile  boolean _stopped;
  long _cnt;

  public void init() {
    // Called once by the browser when it starts the applet.
    String s = getParameter("AnimURL");
    try {
      _amex = new URL(s);
    }
    catch( java.net.MalformedURLException e ) {
      showStatus(e.toString());
      System.err.println("RefreshApplet> "+e.toString());
    }
    s = getParameter("Period");
    Integer period = new Integer(s);
    _millisleep = 1000 * period.intValue(); 
  }

  public void refresh() {
    // Called by paint, repaint, update, etc.
    try {
      getAppletContext().showDocument(_amex);
      ++_cnt;
      showStatus(_amex.toString());
      System.err.println("RefreshApplet> "+_cnt+_amex.toString());
    }
    catch( Exception e ) {
      showStatus(e.toString());
    }
  }

  public void run() {
    // execution of _anim Thread
    while( !_stopped ) {
	repaint(); // calls update
      try {
	_anim.sleep(_millisleep); // milli-sec.
      }
      catch( InterruptedException e ) {
        showStatus(e.toString());
      }
    }
    //_anim = null;
  }

  public void start() {
    // Called whenever the page containing this applet is made visible.
    _stopped = false;
    _cnt = 0;
    if( _anim == null ) {
      _anim = new Thread(this);
      _anim.start();
    }
  }

  public void stop() {
    // Called whenever the page containing this applet is not visible.
    //_stopped = true;
  }

  public void destroyed() {
    // Called once when the browser destroys this applet.
    _stopped = true;
    _anim = null;
  }

  public void paint(Graphics g) {
    // Called by update or repaint whenever this applet needs to repaint itself.
    refresh();
  }

  public void update(Graphics g) {
    // Called whenever this applet needs to repaint itself.
    // overriden so to not bother with the screen clear...
    paint(g); 
  }
}


