#!/usr/bin/env python
rcsId = '$Name:  $ $Id: nisttime.py 14 2008-06-11 01:49:45Z hon $'
import errno, os, signal, socket, sys, time
from subprocess import *

ldpath = os.getenv("LD_LIBRARY_PATH")
if (ldpath == None) :
  print "LD_LIBRARY_PATH is not defined..."
#  sys.exit()
#end if
#print "ldpath= "+ldpath

pypath = os.getenv("PYTHONPATH")
if (pypath == None) :
  print "PYTHONPATH is not defined..."
#  sys.exit()
#end if
#print "pypath= "+pypath

terminate = False;
interruptcnt = 0;
def handler(signum, frame):
  print 'Signal handler called with signal', signum
  ++interruptcnt
  sys.stdout.write("exit? [y]: ")
  rep = "y"; rep = sys.stdin.readline(); rep = rep.strip()
  if len(rep) == 0 or rep == "y" :
    print "reply == "+rep
    terminate = True
    sys.exit()
#end def

signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
host = socket.gethostname()
#print "host= "+host+", argv0= "+arg

soc = True
if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    sys.exit()
  if arg == "-s" or arg == "-soc" :
    print "using socket connection"
    soc = True
  if arg == "-t" or arg == "-tel" :
    print "using Popen of telnet"
    soc = False
 #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg= " + arg
#end if

nisttime = "54151 yy-mm-dd hh:mm:ss 00 0 0 769.7 UTC(NIST) *"
nisthost = "time-a.nist.gov"
nistport = "13"
port = int(nistport)
#print nisttime
try:
  if not soc:
    nisttime = Popen(["/usr/bin/telnet", nisthost, nistport], stdout=PIPE).communicate()[0]
  else:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    nisttimeserv = (nisthost, port)
    s.connect(nisttimeserv)
    nisttime = s.recv(len(nisttime), socket.MSG_WAITALL)
    nisttime = nisttime.strip() # remove all leading and trailing whites/newlines/carriage-returns
except IOError, err:
  print "IO error..."
except OSError, err:
  print "OSError: errno= ",err.errno," , ",err.strerror
except (NameError, TypeError, ValueError), err:
  print "Name or Type error..."
except SystemExit:
  print "interrupted: ", interruptcnt
  print "sys.exit() invoked from signal handler, aborting..."
  terminate = True;
except:
  print "interrupted: ", interruptcnt
  if not soc:
    print "exception occured with Popen of telnet to time-a.nist.gov on port 13"
  else:
    print "exception occured with socket connect/recv ",nisthost," on port ", port
#finally:
#  print "finally...interrupted: ", interruptcnt
#  if soc:
#    s.close()
#  if terminate:
#   sys.exit()

print nisttime
