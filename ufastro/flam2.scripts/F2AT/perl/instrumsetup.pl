#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: instrumsetup.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
######################### main #########################
#dbprint();
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 0.5;
#
$instrum = "flam";
$setmos = "imaging";
$setfilt = "HK";
$setbeam = "f/16";
$setbias = "imaging";
$test = $setbias = "imaging";
#
# the following comes directly from the fjec IS panel java code:
#
@beamStrs = ( "f/16","MCAO_over","MCAO_under" );
@deckerStrs = ( "imaging","long_slit","mos" );
@detFocposStrs = ( "f/16","MCAO" );
@filterStrs = ( "open","dark","J-lo","J","H","Ks","JH","HK","TBD1" );
@grismStrs = ( "open","JH","HK","JHK","TBD1" );
@lyotStrs = ( "f/16","MCAO_over","MCAO_under","H1","H2" );
@mosslitStrs = ( "imaging","circle1","circle2","1pix-slit","2pix-slit","3pix-slit","4pix-slit",
		 "6pix-slit","8pix-slit","mos1","mos2","mos3","mos4","mos5","mos6","mos7","mos8","mos9" );
@wheelBiasStrs = ( "imaging","long_slit","mos" );
@windowCoverStrs = ( "open","closed" );


if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "instrumsetup.pl [-h[elp]] [instrum] [filt] [beam] [wheelbias] \n";
  }
  $instrum = $argval;
#  if( @ARGV == 0 ) { $test = "all"; }
  if( @ARGV > 0 ) { $setmos = shift; }
  if( @ARGV > 0 ) { $setfilt = shift; }
  if( @ARGV > 0 ) { $setbeam = shift; }
  if( @ARGV > 0 ) { $setbias = shift; }
}
#
#startup();
#

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
UFCA::putInt($apply, $Clear);

$applyC = UFCA::connectPV("$instrum:applyC", $to);
# instrument setup car:
$setupC = UFCA::connectPV("$instrum:instrumSetupC", $to);
$setup = UFCA::connectPV("$instrum:instrumSetup.DIR", $to);
$vsetup = UFCA::connectPV("$instrum:instrumSetup.VAL", $to);

# essential parameters:
$mos = UFCA::connectPV("$instrum:instrumSetup.MOSSlit", $to);
$vmos = UFCA::connectPV("$instrum:instrumSetup.VALMOSSlit", $to);
$beam = UFCA::connectPV("$instrum:instrumSetup.BeamMode", $to);
$vbeam = UFCA::connectPV("$instrum:instrumSetup.VALBeamMode", $to);
$whlbias = UFCA::connectPV("$instrum:instrumSetup.WheelBiasMode", $to);
$vwhlbias = UFCA::connectPV("$instrum:instrumSetup.VALWheelBiasMode", $to);
$filt = UFCA::connectPV("$instrum:instrumSetup.Filter", $to);
$vfilt = UFCA::connectPV("$instrum:instrumSetup.VALFilter", $to);

# Instrum Setup PV overrides:
$ovrdckr = UFCA::connectPV("$instrum:instrumSetup.OverrideDecker", $to);
$vovrdckr = UFCA::connectPV("$instrum:instrumSetup.VALOverrideDecker", $to);
$dckr = UFCA::connectPV("$instrum:instrumSetup.Decker", $to);
$vdckr = UFCA::connectPV("$instrum:instrumSetup.VALDecker", $to);

$ovrdetfoc = UFCA::connectPV("$instrum:instrumSetup.OverrideDetPos", $to);
$detfoc = UFCA::connectPV("$instrum:instrumSetup.DetPosFocus", $to);
$vdetfoc = UFCA::connectPV("$instrum:instrumSetup.VALDetPosFocus", $to);
$vovrdetfoc = UFCA::connectPV("$instrum:instrumSetup.VALOverrideDetPos", $to);

$ovrgrism = UFCA::connectPV("$instrum:instrumSetup.OverrideGrism", $to);
$vovrgrism = UFCA::connectPV("$instrum:instrumSetup.VALOverrideGrism", $to);
$grism = UFCA::connectPV("$instrum:instrumSetup.Grism", $to);
$vgrism = UFCA::connectPV("$instrum:instrumSetup.VALGrism", $to);

$ovrlyot = UFCA::connectPV("$instrum:instrumSetup.OverrideLyot", $to);
$vovrlyot = UFCA::connectPV("$instrum:instrumSetup.VALOverrideLyot", $to);
$lyot = UFCA::connectPV("$instrum:instrumSetup.Lyot", $to);
$vlyot = UFCA::connectPV("$instrum:instrumSetup.VALLyot", $to);

$ovrwindow = UFCA::connectPV("$instrum:instrumSetup.OverrideWindowCover", $to);
$vovrwindow = UFCA::connectPV("$instrum:instrumSetup.VALOverrideWindowCover", $to);
$window = UFCA::connectPV("$instrum:instrumSetup.WindowCover", $to);
$vwindow = UFCA::connectPV("$instrum:instrumSetup.VALWindowCover", $to);

$ovrcamsetpA = UFCA::connectPV("$instrum:instrumSetup.OverrideCamSetPointA", $to);
$vovrcamsetpA = UFCA::connectPV("$instrum:instrumSetup.VALOverrideCamSetPointA", $to);
$camsetpA = UFCA::connectPV("$instrum:instrumSetup.CamSetPointA", $to);
$vcamsetpA = UFCA::connectPV("$instrum:instrumSetup.VALCamSetPointA", $to);

$ovrcamsetpB = UFCA::connectPV("$instrum:instrumSetup.OverrideCamSetPointB", $to);
$vovrcamsetpB = UFCA::connectPV("$instrum:instrumSetup.VALOverrideCamSetPointB", $to);
$camsetpB = UFCA::connectPV("$instrum:instrumSetup.CamSetPointB", $to);
$vcamsetpB = UFCA::connectPV("$instrum:instrumSetup.VALCamSetPointB", $to);

$ovrmossetp = UFCA::connectPV("$instrum:instrumSetup.OverrideMOSSetPoint", $to);
$vovrmossetp = UFCA::connectPV("$instrum:instrumSetup.VALOverrideMOSSetPoint", $to);
$mossetp = UFCA::connectPV("$instrum:instrumSetup.MOSSetPoint", $to);
$vmossetp = UFCA::connectPV("$instrum:instrumSetup.VALMOSSetPoint", $to);

# mech. sad
$wsad = UFCA::connectPV("$instrum:sad:WindowSteps.INP", $to);
$msad = UFCA::connectPV("$instrum:sad:MOSSteps", $to);
$dsad = UFCA::connectPV("$instrum:sad:DeckerSteps", $to);
$f1sad = UFCA::connectPV("$instrum:sad:Filter1Steps", $to);
$lsad = UFCA::connectPV("$instrum:sad:LyotSteps", $to);
$f2sad = UFCA::connectPV("$instrum:sad:Filter2Steps", $to);
$gsad = UFCA::connectPV("$instrum:sad:GrismSteps", $to);
$fsad = UFCA::connectPV("$instrum:sad:FocusSteps", $to);

# take a look at current input parameter values:
#printPVs();

# check current state of setup CAR:
$c = UFCA::getInt($setupC); $pv = UFCA::getPVName($setupC); print "$pv == $c\n";
if( $c != $Idle ) { die "InstrumentSetup not Idle!\n"; }
# clear:
$p = UFCA::putInt($apply, $Clear);

print "set mos: $setmos, set filter: $setfilt, beammode: $setbeam, wheelbias: $setbias\n";
$p = UFCA::putString($mos, $setmos);
$p = UFCA::putString($filt, $setfilt);
$p = UFCA::putString($beam, $setbeam);
$p = UFCA::putString($whlbias, $setbias);

printPVs();

$p = UFCA::putInt($apply, $Start);
waitCAR($setupC);

#printPVs();

exit;

############################## subs #####################
sub waitCAR {
  my $car = shift;
  my $c = UFCA::getInt($car);
  my $pv = UFCA::getPVName($car);
  my $ca = UFCA::getInt($applyC);
  my $pva = UFCA::getPVName($applyC);
  my $cnt = 10;
  my $sad = UFCA::getPVName($wsad);
  my $sv = UFCA::getInt($wsad);
  while(  $c == $Busy && $cnt-- > 0 ) {
    print "waitCAR> $pv: $c, $pva: $ca\n";
    $sad = UFCA::getPVName($wsad);
    $sv = UFCA::getInt($wsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($msad);
    $sv = UFCA::getInt($msad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($dsad);
    $sv = UFCA::getInt($dsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($f1sad);
    $sv = UFCA::getInt($f1sad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($lsad);
    $sv = UFCA::getInt($lsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($f2sad);
    $sv = UFCA::getInt($f2sad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($gsad);
    $sv = UFCA::getInt($gsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($fsad);
    $sv = UFCA::getInt($fsad);
    print "waitCAR> $sad: $sv\n";
    sleep 1;
    $c = UFCA::getInt($car);
    $ca = UFCA::getInt($applyC);
  }
  $cnt = 10;
  while(  $ca == $Busy && $cnt-- > 0 ) {
    print "waitCAR> $pv: $c, $pva: $ca\n";
    $sad = UFCA::getPVName($wsad);
    $sv = UFCA::getInt($wsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($msad);
    $sv = UFCA::getInt($msad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($dsad);
    $sv = UFCA::getInt($dsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($f1sad);
    $sv = UFCA::getInt($f1sad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($lsad);
    $sv = UFCA::getInt($lsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($f2sad);
    $sv = UFCA::getInt($f2sad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($gsad);
    $sv = UFCA::getInt($gsad);
    print "waitCAR> $sad: $sv\n";
    $sad = UFCA::getPVName($fsad);
    $sv = UFCA::getInt($fsad);
    print "waitCAR> $sad: $sv\n";
    sleep 1;
    $c = UFCA::getInt($car);
    $ca = UFCA::getInt($applyC);
  }
  print "waitCAR> $pv: $c, $pva: $ca\n";
}

sub printPVs {
  print "printPVs> get and print all InstrumSetup input/outputs...\n";
  my $v = UFCA::getAsString($beam); my $pv = UFCA::getPVName($beam); print "$pv == $v\n";
  $v = UFCA::getAsString($vbeam); $pv = UFCA::getPVName($vbeam); print "$pv == $v\n";

  $v = UFCA::getAsString($filt); $pv = UFCA::getPVName($filt); print "$pv == $v\n";
  $v = UFCA::getAsString($vfilt); $pv = UFCA::getPVName($vfilt); print "$pv == $v\n";

  $v = UFCA::getAsString($mos); $pv = UFCA::getPVName($mos); print "$pv == $v\n";
  $v = UFCA::getAsString($vmos); $pv = UFCA::getPVName($vmos); print "$pv == $v\n";

  $v = UFCA::getAsString($whlbias); $pv = UFCA::getPVName($whlbias); print "$pv == $v\n";
  $v = UFCA::getAsString($vwhlbias); $pv = UFCA::getPVName($vwhlbias); print "$pv == $v\n";

  $v = UFCA::getAsString($window); $pv = UFCA::getPVName($window); print "$pv == $v\n";
  $v = UFCA::getAsString($vwindow); $pv = UFCA::getPVName($vwindow); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrwindow); $pv = UFCA::getPVName($ovrwindow); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrwindow); $pv = UFCA::getPVName($vovrwindow); print "$pv == $v\n";

  $v = UFCA::getAsString($dckr); $pv = UFCA::getPVName($dckr); print "$pv == $v\n";
  $v = UFCA::getAsString($vdckr); $pv = UFCA::getPVName($vdckr); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrdckr); $pv = UFCA::getPVName($ovrdckr); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrdckr); $pv = UFCA::getPVName($vovrdckr); print "$pv == $v\n";

  $v = UFCA::getAsString($lyot); $pv = UFCA::getPVName($lyot); print "$pv == $v\n";
  $v = UFCA::getAsString($vlyot); $pv = UFCA::getPVName($vlyot); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrlyot); $pv = UFCA::getPVName($ovrlyot); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrlyot); $pv = UFCA::getPVName($vovrlyot); print "$pv == $v\n";

  $v = UFCA::getAsString($grism); $pv = UFCA::getPVName($grism); print "$pv == $v\n";
  $v = UFCA::getAsString($vgrism); $pv = UFCA::getPVName($vgrism); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrgrism); $pv = UFCA::getPVName($ovrgrism); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrgrism); $pv = UFCA::getPVName($vovrgrism); print "$pv == $v\n";

  $v = UFCA::getAsString($detfoc); $pv = UFCA::getPVName($detfoc); print "$pv == $v\n";
  $v = UFCA::getAsString($vdetfoc); $pv = UFCA::getPVName($vdetfoc); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrdetfoc); $pv = UFCA::getPVName($ovrdetfoc); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrdetfoc); $pv = UFCA::getPVName($vovrdetfoc); print "$pv == $v\n";

  $v = UFCA::getAsString($mossetp); $pv = UFCA::getPVName($mossetp); print "$pv == $v\n";
  $v = UFCA::getAsString($vmossetp); $pv = UFCA::getPVName($vmossetp); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrmossetp); $pv = UFCA::getPVName($ovrmossetp); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrmossetp); $pv = UFCA::getPVName($vovrmossetp); print "$pv == $v\n";

  $v = UFCA::getAsString($camsetpA); $pv = UFCA::getPVName($camsetpA); print "$pv == $v\n";
  $v = UFCA::getAsString($vcamsetpA); $pv = UFCA::getPVName($vcamsetpA); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrcamsetpA); $pv = UFCA::getPVName($ovrcamsetpA); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrcamsetpA); $pv = UFCA::getPVName($vovrcamsetpA); print "$pv == $v\n";

  $v = UFCA::getAsString($camsetpB); $pv = UFCA::getPVName($camsetpB); print "$pv == $v\n";
  $v = UFCA::getAsString($vcamsetpB); $pv = UFCA::getPVName($vcamsetpB); print "$pv == $v\n";
  $v = UFCA::getAsString($ovrcamsetpB); $pv = UFCA::getPVName($ovrcamsetpB); print "$pv == $v\n";
  $v = UFCA::getAsString($vovrcamsetpB); $pv = UFCA::getPVName($vovrcamsetpB); print "$pv == $v\n";
}

sub startup {
  print "initdatum> clear: $clear, init: $init, datum: $datum, sys: $sys\n";
  if( $clear == 1 ) {
    $p = UFCA::putInt($apply, $Clear);
    $c = UFCA::getInt($applyC);
    print "sys clear car: $c\n";
    $cnt = 3;
    while(  $c == $Busy && $cnt-- > 0 ) {
      print "sys clear car: $c\n";
      sleep 1;
      $c = UFCA::getInt($applyC);
    }
  }
  if( $init == 1 ) {
    $p = UFCA::putInt($sysI, $Start);
    $c = UFCA::getInt($sysIC);
    print "sys init car: $c\n";
    $cnt = 3;
    while(  $c == $Busy && $cnt-- > 0 ) {
      print "sys init car: $c\n";
      sleep 1;
      $c = UFCA::getInt($sysIC);
    }
  }
  if( $datum == 1 ) {
    $p = UFCA::putInt($sysD, $Start);
    $c = UFCA::getInt($sysDC);
    while(  $c == $Busy && $cnt-- > 0 ) {
      print "sys datum car: $c\n";
      sleep 1;
      $c = UFCA::getInt($sysDC);
    }
  }
}

sub dbprint {
  my $cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$eng;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}

