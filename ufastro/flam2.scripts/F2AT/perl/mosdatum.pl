#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: mosdatum.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$setupR = UFCA::connectPV("flam:cc:setupC", $to);
$mosR = UFCA::connectPV("flam:cc:setupMOSC", $to);
$init = UFCA::connectPV("flam:cc:init.dir", $to);
$initR = UFCA::connectPV("flam:cc:initC", $to);
# sad .val:
$b0 = UFCA::connectPV("flam:sad:MOSPlateBarCode", $to);
$b1 = UFCA::connectPV("flam:sad:MOSPos1BarCode", $to);
$b2 = UFCA::connectPV("flam:sad:MOSPos2BarCode", $to);
$b3 = UFCA::connectPV("flam:sad:MOSPos3BarCode", $to);
$b4 = UFCA::connectPV("flam:sad:MOSPos4BarCode", $to);
$b5 = UFCA::connectPV("flam:sad:MOSPos5BarCode", $to);
$b6 = UFCA::connectPV("flam:sad:MOSPos6BarCode", $to);
$b7 = UFCA::connectPV("flam:sad:MOSPos7BarCode", $to);
$b8 = UFCA::connectPV("flam:sad:MOSPos8BarCode", $to);
$b9 = UFCA::connectPV("flam:sad:MOSPos9BarCode", $to);
@barcdsad = ($b1, $b2, $b3, $b4, $b5, $b6, $b7, $b8, $b9);
# sad .inp:
$b1i = UFCA::connectPV("flam:sad:MOSPos1BarCode.inp", $to);
$b2i = UFCA::connectPV("flam:sad:MOSPos2BarCode.inp", $to);
$b3i = UFCA::connectPV("flam:sad:MOSPos3BarCode.inp", $to);
$b4i = UFCA::connectPV("flam:sad:MOSPos4BarCode.inp", $to);
$b5i = UFCA::connectPV("flam:sad:MOSPos5BarCode.inp", $to);
$b6i = UFCA::connectPV("flam:sad:MOSPos6BarCode.inp", $to);
$b7i = UFCA::connectPV("flam:sad:MOSPos7BarCode.inp", $to);
$b8i = UFCA::connectPV("flam:sad:MOSPos8BarCode.inp", $to);
$b9i = UFCA::connectPV("flam:sad:MOSPos9BarCode.inp", $to);
@barcdisad = ($b1i, $b2i, $b3i, $b4i, $b5i, $b6i, $b7i, $b8i, $b9i);
#
$sad = UFCA::connectPV("flam:sad:MOSSteps", $to);
$dcnt = UFCA::connectPV("flam:sad:DatumCnt", $to);
print "connected to Barcode SAD I/O...\n";
#
#
sub setPV {
  my $ch = shift @_;
  my $val = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $i, $o;
  print "PV == $pv, type == $styp, val == $val\n";
  if( $typ == $PVInt ) {
    $i = UFCA::getInt($ch, $to); UFCA::putInt($ch, $val, $to); $o = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $i = UFCA::getDouble($ch, $to); UFCA::putDouble($ch, $val, $to); $o = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $i = UFCA::getString($ch, $to); UFCA::putString($ch, $val, $to); $o = UFCA::getString($ch, $to);
  }

  #print "initial $pv was $i ==> new $pv is $o\n";
}
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  print "$pv type == $styp, val == $val\n";

  return $val;
}

sub monPV {
  my $ch1 = shift @_;
  my $ch2 = shift @_;
  my $pv1 = UFCA::getPVName($ch1);
  my $pv2 = UFCA::getPVName($ch2);
  UFCA::subscribe($ch1, $to);
  UFCA::subscribe($ch2, $to);
  my $m1 = 0; $m2 = 0;
  while( $m1 == 0 || $m2 == 0 ) {
    $m1 = UFCA::eventMon($ch1); $m2 = UFCA::eventMon($ch2);
    sleep 1;
  }
  $val1 = getPV($ch1);
  $val2 = getPV($ch2);
  print "$pv1 == $val1; $pv2 == $val2\n";
}


# main:
getPV($b0);
for( $i = 0; $i < 9; $i++ ) {
  $n = 1 + $i;
  print "$n\. ";
  getPV($barcdisad[$i]);
  getPV($barcdsad[$i]);
}
getPV($sad);
$dc = getPV($dcnt);
$val = $Preset;
if( $dc < 0 ) {
  # init:
  setPV($initR, $Idle);
  setPV($init, $Preset);
  setPV($applyR, $Idle);
  setPV($apply, $Start);
  #monPV($applyR, $initR);
  $pv =  UFCA::getPVName($b0);
  print "sys.init... hit enter to continue."; getc;
}

$datum = UFCA::connectPV("flam:cc:datum.MOS", $to);
$datumR = UFCA::connectPV("flam:cc:datumC", $to);
$dbl = UFCA::connectPV("flam:cc:datum.MOSBacklash", $to);
$ddir = UFCA::connectPV("flam:cc:datum.MOSDirection", $to);
$dvel = UFCA::connectPV("flam:cc:datum.MOSVel", $to);

# datum the MOS wheel:
setPV($datum, $Preset);
setPV($dbl, 0);
setPV($ddir, 0);
setPV($dvel, 100);
setPV($datumR, $Idle);
setPV($applyR, $Idle);
setPV($apply, $Start);
#monPV($applyR, $datumR);
print "datuming... hit enter to continue."; getc;
getPV($sad);

exit;

