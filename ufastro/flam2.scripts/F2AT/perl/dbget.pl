#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dbget.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
#$db = "flam:cc:heartbeat.val";
$db = shift;
$to = 0.5;
$chan = UFCA::connectPV($db, $to);
$pv = UFCA::getPVName($chan);
$typ = UFCA::getType($chan, $to);
$styp = UFCA::getTypeStr($chan, $to);
#print "$pv type == $typ, $styp\n";
if( $typ == $PVString ) { $i = UFCA::getString($chan, $to); }
if( $typ == $PVInt ) { $i = UFCA::getInt($chan, $to); }
if( $typ == $PVDouble ) { $i = UFCA::getDouble($chan, $to); }
print "$pv == $i, type == $styp\n";
exit;

