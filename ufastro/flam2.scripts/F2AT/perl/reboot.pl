#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: reboot.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
######################### main #########################
#dbprint();
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;
$init = 0;
$reboot = 1;
$sys = "all";

if( @ARGV >= 1 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    print "focus.pl [-help] [-init[only]] [-reboot[only] [subsys]]\n";
    exit;
  }
  elsif( $argval eq "-init" ) {
    $init = 1; $reboot = 0;
  }
  elsif( $argval eq "-reboot" ) {
    $reboot = 1; $init = 0;
  }
  else {
    $sys = $argval;
  }
}
if( @ARGV >= 2 ) {
  $sys = shift;
}
print "initreboot> init: $init, reboot: $reboot, sys: $sys\n";

$apply = UFCA::connectPV("flam:apply.DIR", $to);
$applyC = UFCA::connectPV("flam:applyC", $to);
$sysI = UFCA::connectPV("flam:init.DIR", $to);
$sysIC = UFCA::connectPV("flam:initC", $to);
$sysR = UFCA::connectPV("flam:reboot.DIR", $to);
$sysRC = UFCA::connectPV("flam:rebootC", $to);
$sysRsub = UFCA::connectPV("flam:reboot.subsys", $to);
$sysRF2 = UFCA::connectPV("flam:reboot.F2host", $to);
$sysRWFS = UFCA::connectPV("flam:reboot.F2OIWFS", $to);
#
$name = UFCA::getPVName($sysRC);
$c = UFCA::getInt($sysRC);
print "sys reboot car: $name, VAL: $c\n";

if( $init == 1 ) {
  $p = UFCA::putInt($sysI, $Start);
  $c = UFCA::getInt($sysIC);
  print "sys init car: $c\n";
  $c = $Busy; $cnt = 3;
  while(  $c == $Busy && $cnt-- > 0 ) {
  print "sys init car: $c\n";
    sleep 1;
    $c = UFCA::getInt($sysIC);
  }
}
if( $reboot == 1 ) {
#  $p = UFCA::putString($sysRsub, "all");
#  $p = UFCA::putString($sysRF2, "irflam2a");
#  $p = UFCA::putString($sysRWFS, "f2:wfs:reboot");
  $subsys = UFCA::getString($sysRsub);
  $f2host = UFCA::getString($sysRF2);
  $wfs = UFCA::getString($sysRWFS);
  $p = UFCA::putInt($sysR, $Start);
  print "sys reboot F2host, subsys, wfs: $f2host, $subsys, $wfs\n";
  $c = $Busy; $cnt = 3;
  $c = UFCA::getInt($sysRC);
  while(  $c == $Busy && $cnt-- > 0 ) {
  print "sys reboot car: $c\n";
    sleep 1;
    $c = UFCA::getInt($sysRC);
  }
}

exit;

############################## subs #####################
sub dbprint {
  my $cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$eng;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}

