#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: focus.pl 14 2008-06-11 01:49:45Z hon $to6 20:33:37 hon Exp hon $';
use UFCA;
######################### main #########################
#dbprint();
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;
$init = 0;
$limit = 0;
$stepcnt = 0;
if( @ARGV < 1 ) {
  $argval = shift;
  print "focus.pl [-help] [-init] [-limit] (+/-)stepcnt\n";
  exit;
}
#
if( @ARGV >= 1 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    print "focus.pl [-help] [-init] [-limit] (+/-)stepcnt\n";
    exit;
  }
  elsif( $argval eq "-init" ) {
    $init = 1;
  }
  elsif( $argval eq "-lim" || $argval eq "-limit" ) {
    $limit = shift;
  }
  else {
    $stepcnt = $argval;
  }
}
if( @ARGV >= 2 ) {
  $stepcnt = shift;
}

$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyC = UFCA::connectPV("flam:eng:applyC", $to);

if( $init > 0 ) {
  print "focus> init...\n";
  $focusE = UFCA::connectPV("flam:ec:init.dir", $to);
  $focusEC = UFCA::connectPV("flam:ec:initC", $to);
  $focusC = UFCA::connectPV("flam:cc:init.dir", $to);
  $focusCC = UFCA::connectPV("flam:cc:initC", $to);
  $pc = UFCA::putInt($focusC, $Start);
  $cc = UFCA::getInt($focusCC);
  print "focus init cc car: $cc\n";
  $cnt = 3;
  while(  $cc == $Busy && $cnt-- > 0 ) {
    print "focus init cc car: $cc\n";
    sleep 1;
    $cc = UFCA::getInt($focusCC);
  }
  $pe = UFCA::putInt($focusE, $Start);
  $ec = UFCA::getInt($focusEC);
  print "focus init ec car: $ec\n";
  $cnt = 3;
  while( $ec == $Busy && $cnt-- > 0 ) {
    print "focus init ec car: $ec\n";
    sleep 1;
    $ec = UFCA::getInt($focusEC);
  }
# also datum the indexors:
  $datumC = UFCA::connectPV("flam:cc:datum.All", $to);
  $datumCC = UFCA::connectPV("flam:cc:datumC", $to);
  $pc = UFCA::putInt($datumC, $Start);
  $pc = UFCA::putInt($apply, $Start);
  $cc = UFCA::getInt($datumCC);
  print "datum cc car: $cc\n";
  $cnt = 30;
  while(  $cc == $Busy && $cnt-- > 0 ) {
    print "datum cc car: $cc\n";
    sleep 1;
    $cc = UFCA::getInt($focusCC);
  }
  exit;
}

print "focus> seek limit (0==no, +/-1==yes): $limit, stepcnt: $stepcnt\n";

$focusC = UFCA::connectPV("flam:eng:focusC", $to);
$focusI = UFCA::connectPV("flam:eng:focusC.ival", $to);
$g = UFCA::getAsString($focusC);
if( $g ne $Idle ) {
  print "flam:eng:focusC not Idle, force Idle? [y/n]";
  $a = getc;
  if( $a eq 'n' ) { exit; }
  $p = UFCA::putInt($focusI, $Idle);
}
$frq = UFCA::connectPV("flam:eng:focus.LVDTExcFreqHiLow", $to);
$vlt = UFCA::connectPV("flam:eng:focus.LVDTExcVoltsHiLow", $to);
$pwr = UFCA::connectPV("flam:eng:focus.LVDTPwrOnOff", $to);
$limvel = UFCA::connectPV("flam:eng:focus.LimitVel", $to);
$steps = UFCA::connectPV("flam:eng:focus.Step", $to);
$stepvel = UFCA::connectPV("flam:eng:focus.StepVel", $to);
$focsad = UFCA::connectPV("flam:sad:FOCUSTEP", $to);
# seek limit first?
if( $limit != 0 ) {
  $p = UFCA::putInt($pwr, 1);
  $p = UFCA::putInt($frq, 0);
  $p = UFCA::putInt($vlt, 0);
  $p = UFCA::putInt($limvel, -20);
  $p = UFCA::putInt($apply, $Start);
  $sad = UFCA::getInt($focsad);
  $app = UFCA::getInt($applyC);
  print "$app, $sad ... ";
  while( $app == $Busy ) {
    print "$app, $sad ... ";
    sleep 1;
    $app = UFCA::getInt($applyC);
    $sad = UFCA::getInt($focsad);
  }
}
#
if( $stepcnt == 0 ) { exit; }
#
# else perform requested step:
$p = UFCA::putInt($stepvel, 20);
$p = UFCA::putInt($steps, $stepcnt);
$p = UFCA::putInt($apply, $Start);
$sad = UFCA::getInt($focsad);
$app = UFCA::getInt($applyC);
print "$app, $sad ... ";
while( $app == $Busy ) {
  print "$app, $sad ... ";
  sleep 1;
  $app = UFCA::getInt($applyC);
  $sad = UFCA::getInt($focsad);
}
exit;

############################## subs #####################
sub dbprint {
  my $cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$eng;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}

