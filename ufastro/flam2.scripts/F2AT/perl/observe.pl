#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: observe.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# main
#dbprint();
$instrum = "flam";
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;
#
$label = "Test";
$user = "RichardElston"; # if bias override
$mode = "SaveAll"; #mce4 cds read 4-64?
$index = 1;

if( @ARGV >= 1 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    print "observe.pl [-h[elp]] [instrum] [datalabel] [user remark] [save/discard] [dither index]\n";
    exit;
  }
  else {
    $instrum = $argval;
  }
  if( @ARGV >= 1 ) {
    $index = shift;
  }
  if( @ARGV >= 1 ) {
    $mode = $shift;
  }
  if( @ARGV >= 1 ) {
    $label = $shift;
  }
  if( @ARGV >= 1 ) {
    $user = shift;
  }
}
print "observe> instrum: $instrum, dither index: $index, datalabel: $label, user remark: $user, save/discard mode: $mode\n";

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$obsC = UFCA::connectPV("$instrum:observeC", $to);
$vobs = UFCA::connectPV("$instrum:observe.VAL", $to);
$datalabel = UFCA::connectPV("$instrum:observe.DataLabel", $to);
$vdatalabel = UFCA::connectPV("$instrum:observe.VALDataLabel", $to);
$usrinfo = UFCA::connectPV("$instrum:observe.UserInfo", $to);
$vusrinfo = UFCA::connectPV("$instrum:observe.VALUserInfo", $to);
$datamode = UFCA::connectPV("$instrum:observe.DataMode", $to);
$vdatamode = UFCA::connectPV("$instrum:observe.VALDataMode", $to);
$ditheridx = UFCA::connectPV("$instrum:observe.DitherIndex", $to);
$vditheridx = UFCA::connectPV("$instrum:observe.VALDitherIndex", $to);
#
$p = UFCA::putInt($apply, $Clear);
$ac = UFCA::getInt($applyC);
print "observe> clear car: $ac\n";
#$vdlabel = UFCA::getString($vdatalabel);
#print "observe> current $instrum datalabel: $ddlabel\n";
$p = UFCA::putString($datalabel, $label);
$dl = UFCA::getString($datalabel);
print "observe> put datalabel: $dl\n";
$p = UFCA::putString($usrinfo, $user);
$ui = UFCA::getString($usrinfo);
print "observe> put user remark: $ui\n";
$p = UFCA::putString($datamode, $mode);
$dm = UFCA::getString($datamode);
print "observe> put save/discard mode: $dm\n";
$p = UFCA::putInt($ditheridx, $index);
$di = UFCA::getInt($ditheridx);
print "observe> put datalabel: $dl, user remark: $ui, save/discard mode: $dm, dither index: $di\n";
#
print "observe> clear car: $ac\n";
$p = UFCA::putInt($apply, $Start);
$ac = UFCA::getInt($applyC);
$c = UFCA::getInt($obsC);
print "observe> obsserve car: $c, apply car: $ac\n";
$cnt = 3;
while( $c == $Busy && $ac == $Busy  && $cnt-- > 0 ) {
  print "observe> $cnt, observe car: $c, apply car: $ac\n";
  sleep 1;
  $c = UFCA::getInt($obsC);
  $ac = UFCA::getInt($applyC);
}
exit;
