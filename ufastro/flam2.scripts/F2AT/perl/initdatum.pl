#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: initdatum.pl 14 2008-06-11 01:49:45Z hon $to6 20:33:37 hon Exp hon $';
use UFCA;
######################### main #########################
#dbprint();
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;
$init = 0;
$limit = 0;
$stepcnt = 0;
$init = 1;
$datum = 1;
$sys = "all";

if( @ARGV >= 1 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    print "focus.pl [-help] [-init[only]] [-datum[only] [subsys]]\n";
    exit;
  }
  elsif( $argval eq "-init" ) {
    $init = 1; $datum = 0;
  }
  elsif( $argval eq "-datum" ) {
    $datum = 1; $init = 0;
  }
  else {
    $sys = $argval;
  }
}
if( @ARGV >= 2 ) {
  $sys = shift;
}
print "initdatum> init: $init, datum: $datum, sys: $sys\n";

$apply = UFCA::connectPV("flam:apply.DIR", $to);
$applyC = UFCA::connectPV("flam:applyC", $to);
$sysI = UFCA::connectPV("flam:init.DIR", $to);
$sysIC = UFCA::connectPV("flam:initC", $to);
$sysD = UFCA::connectPV("flam:datum.DIR", $to);
$sysDC = UFCA::connectPV("flam:datumC", $to);
if( $init == 1 ) {
  $p = UFCA::putInt($sysI, $Start);
  $c = UFCA::getInt($sysIC);
  print "sys init car: $c\n";
  $cnt = 3;
  while(  $c == $Busy && $cnt-- > 0 ) {
  print "sys init car: $c\n";
    sleep 1;
    $c = UFCA::getInt($sysIC);
  }
}
if( $datum == 1 ) {
  $p = UFCA::putInt($sysD, $Start);
  $c = UFCA::getInt($sysDC);
  while(  $c == $Busy && $cnt-- > 0 ) {
  print "sys datum car: $c\n";
    sleep 1;
    $c = UFCA::getInt($sysDC);
  }
}

exit;

############################## subs #####################
sub dbprint {
  my $cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$eng;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}

