#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: ecinit.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$setupR = UFCA::connectPV("flam:cc:setupC", $to);
$mosR = UFCA::connectPV("flam:cc:setupMOSC", $to);
$init = UFCA::connectPV("flam:cc:init.dir", $to);
$initR = UFCA::connectPV("flam:cc:initC", $to);
$sad = UFCA::connectPV("flam:sad:MOSSteps", $to);
#
#
sub setPV {
  my $ch = shift @_;
  my $val = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $i, $o;
  #print "PV == $pv, type == $styp, val == $val\n";
  if( $typ == $PVInt ) {
    $i = UFCA::getInt($ch, $to); UFCA::putInt($ch, $val, $to); $o = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $i = UFCA::getDouble($ch, $to); UFCA::putDouble($ch, $val, $to); $o = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $i = UFCA::getString($ch, $to); UFCA::putString($ch, $val, $to); $o = UFCA::getString($ch, $to);
  }
  print "initial $pv was $i ==> new $pv is $o\n";
}
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  print "$pv type == $styp, val == $val\n";
  return $val;
}

sub monPV {
  my $ch1 = shift @_;
  my $ch2 = shift @_;
  my $pv1 = UFCA::getPVName($ch1);
  my $pv2 = UFCA::getPVName($ch2);
  UFCA::subscribe($ch1, $to);
  UFCA::subscribe($ch2, $to);
  my $m1 = 0; $m2 = 0;
  while( $m1 == 0 || $m2 == 0 ) {
    $m1 = UFCA::eventMon($ch1); $m2 = UFCA::eventMon($ch2);
    sleep 1;
  }
  $val1 = getPV($ch1);
  $val2 = getPV($ch2);
  print "$pv1 == $val1; $pv2 == $val2\n";
}


# main:
getPV($sad);
$val = $Preset;
# init:
setPV($initR, $Idle);
setPV($init, $Preset);
setPV($applyR, $Idle);
setPV($apply, $Start);
#monPV($applyR, $initR);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}
exit;
