#!/usr/local/bin/perl
use UFCA;
$val = shift;
$to = 0.5;
$db = "flam:cc:heartbeat";
$chan = UFCA::connectPV($db, $to);
$h = UFCA::getInt($chan, $to);
print "$db == $h\n";
UFCA::putInt($chan, $val, $to);
$h = UFCA::getInt($chan, $to);
print "$db == $h\n";
exit;

