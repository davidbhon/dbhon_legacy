#!/bin/env python
rcsId = '$Name:  $ $Id: dcdatum.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################

instrum= "flam"
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

#:instrum#end:dc:datum.VAL
#:instrum#end:dc:datum.VALAllBias
#:instrum#end:dc:datum.VALAllPreamp
#:instrum#end:dc:datum.VALBitDepth
#:instrum#end:dc:datum.VALCDSReads
#:instrum#end:dc:datum.VALExpCnt
#:instrum#end:dc:datum.VALHeight
#:instrum#end:dc:datum.VALLabDef
#:instrum#end:dc:datum.VALMilliSec
#:instrum#end:dc:datum.VALPixelBaseClock
#:instrum#end:dc:datum.VALPostsets
#:instrum#end:dc:datum.VALPresets
#:instrum#end:dc:datum.VALSeconds
#:instrum#end:dc:datum.VALWidth
#:instrum#end:dc:datum.VALtimeout

  #end
#end
print "dcdatum> instrum\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
dcD = UFCA.connectPV(instrum+":dc:datum.DIR", to)
dcDLab = UFCA.connectPV(instrum+":dc:datum.LabDef", to)
dcDC = UFCA.connectPV(instrum+":dc:datumC", to)
dcDM = UFCA.connectPV(instrum+":dc:datum.marked", to)
pv = UFCA.getPVName(apply)
print "dcdatum> clear: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
#p = UFCA.putInt(sysD, Mark)
pv = UFCA.getPVName(dcDLab)
p = UFCA.putInt(dcDLab, 1)
m = UFCA.getInt(dcDM)
print "dcdatum> set pv to 1. instrum:datum marked applying start...\n"
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(dcDC)
pv = UFCA.getPVName(dcDC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(apply)
cnt = 10
while  c == Busy and cnt-- > 0  :
  print "dcdatum> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(dcDC)
  ca = UFCA.getInt(applyC)
#end
cnt = 10
while ca == Busy and cnt-- > 0  :
  print "dcdatum> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(dcDC)
  ca = UFCA.getInt(applyC)
#end
print "dcdatum> pv: c, pva: ca\n"
exit

