#!/bin/env python
rcsId = '$Name:  $ $Id: dcstop.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

flam:dc:datum.val
flam:dc:datum.AllBias
flam:dc:datum.AllPreamp
flam:dc:datum.CDSReads
flam:dc:datum.CycleType
flam:dc:datum.MilliSec
flam:dc:datum.PixelBaseClock
flam:dc:datum.Postsets
flam:dc:datum.Presets
flam:dc:datum.Seconds
flam:dc:datum.dir
flam:dc:datum.icid
flam:dc:datum.mess
flam:dc:datum.ocid
flam:dc:datum.timeout
flam:dc:datum.val
