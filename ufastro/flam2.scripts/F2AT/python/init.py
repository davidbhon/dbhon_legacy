#!/bin/env python
rcsId = '$Name:  $ $Id: init.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

#
# load the Swig generated Epics channel access module:
#
######################### main #########################
#
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

instrum= "flam"
host = `hostname`

  #end
#end

print "init> instrum@host, device agents expect connections on host\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
sysI = UFCA.connectPV(instrum+":init.DIR", to)
sysIM = UFCA.connectPV(instrum+":init.MARK", to)
sysIhost = UFCA.connectPV(instrum+":init.host", to)
sysIC = UFCA.connectPV(instrum+":initC", to)
pv = UFCA.getPVName(apply)
print "init> clear system: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
pv = UFCA.getPVName(sysIhost)
#p = UFCA.putInt(sysI, Mark)
p = UFCA.putString(sysIhost, host)
m = UFCA.getInt(sysIM)
print "set pv to host host. instrum:init marked applying start...\n"
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(sysIC)
pv = UFCA.getPVName(sysIC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(applyC)
cnt = 10
while  c == Busy and cnt-- > 0  :
  print "init> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(sysIC)
  ca = UFCA.getInt(applyC)
#end
cnt = 10
while  ca == Busy and cnt-- > 0  :
  print "init> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(sysIC)
  ca = UFCA.getInt(applyC)
#end
print "init> pv: c, pva: ca\n"
exit

