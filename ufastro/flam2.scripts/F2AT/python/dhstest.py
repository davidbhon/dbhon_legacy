#!/bin/env python
rcsId = '$Name:  $ $Id: dhstest.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyR = UFCA.connectPV(instrum+":eng:applyC", to)
init = UFCA.connectPV(instrum+":cc:init.dir", to)
initR = UFCA.connectPV(instrum+":cc:initC", to)
datumR = UFCA.connectPV(instrum+":cc:datumC", to)
sadwin = UFCA.connectPV(instrum+":sad:WindowSteps", to)
saddck = UFCA.connectPV(instrum+":sad:DeckerSteps", to)
sadmos = UFCA.connectPV(instrum+":sad:MOSSteps", to)
sadf1 = UFCA.connectPV(instrum+":sad:Filter1Steps", to)
sadlyo = UFCA.connectPV(instrum+":sad:LyotSteps", to)
sadf2 = UFCA.connectPV(instrum+":sad:Filter2Steps", to)
sadgrsm = UFCA.connectPV(instrum+":sad:GrismSteps", to)
sadfoc = UFCA.connectPV(instrum+":sad:FocusSteps", to)
# inputs:
#dall = UFCA.connectPV(instrum+":cc:datum.All", to)
dck = UFCA.connectPV(instrum+":cc:datum.Decker", to)
dckbl = UFCA.connectPV(instrum+":cc:datum.DeckerBacklash", to)
dckdir = UFCA.connectPV(instrum+":cc:datum.DeckerDirection", to)
dckvel = UFCA.connectPV(instrum+":cc:datum.DeckerVel", to)
f1 = UFCA.connectPV(instrum+":cc:datum.Filter1", to)
f1bl = UFCA.connectPV(instrum+":cc:datum.Filter1Backlash", to)
f1dir = UFCA.connectPV(instrum+":cc:datum.Filter1Direction", to)
f1vel = UFCA.connectPV(instrum+":cc:datum.Filter1Vel", to)
f2 = UFCA.connectPV(instrum+":cc:datum.Filter2", to)
f2bl = UFCA.connectPV(instrum+":cc:datum.Filter2Backlash", to)
f2dir = UFCA.connectPV(instrum+":cc:datum.Filter2Direction", to)
f2vel = UFCA.connectPV(instrum+":cc:datum.Filter2Vel", to)
foc = UFCA.connectPV(instrum+":cc:datum.Focus", to)
fucbl = UFCA.connectPV(instrum+":cc:datum.FocusBacklash", to)
focdir = UFCA.connectPV(instrum+":cc:datum.FocusDirection", to)
focvel = UFCA.connectPV(instrum+":cc:datum.FocusVel", to)
grsm = UFCA.connectPV(instrum+":cc:datum.Grism", to)
grsmbl = UFCA.connectPV(instrum+":cc:datum.GrismBacklash", to)
grsmdir = UFCA.connectPV(instrum+":cc:datum.GrismDirection", to)
grsmvel = UFCA.connectPV(instrum+":cc:datum.GrismVel", to)
lyot = UFCA.connectPV(instrum+":cc:datum.Lyot", to)
lyotbl = UFCA.connectPV(instrum+":cc:datum.LyotBacklash", to)
lyotdir = UFCA.connectPV(instrum+":cc:datum.LyotDirection", to)
lyotvel = UFCA.connectPV(instrum+":cc:datum.LyotVel", to)
mos = UFCA.connectPV(instrum+":cc:datum.MOS", to)
mosbl = UFCA.connectPV(instrum+":cc:datum.MOSBacklash", to)
mosdir = UFCA.connectPV(instrum+":cc:datum.MOSDirection", to)
mosvel = UFCA.connectPV(instrum+":cc:datum.MOSVel", to)
win = UFCA.connectPV(instrum+":cc:datum.Window", to)
winbl = UFCA.connectPV(instrum+":cc:datum.WindowBacklash", to)
windir = UFCA.connectPV(instrum+":cc:datum.WindowDirection", to)
winvel = UFCA.connectPV(instrum+":cc:datum.WindowVel", to)
dir = UFCA.connectPV(instrum+":cc:datum.dir", to)
icid = UFCA.connectPV(instrum+":cc:datum.icid", to)
mess = UFCA.connectPV(instrum+":cc:datum.mess", to)
ocid = UFCA.connectPV(instrum+":cc:datum.ocid", to)
tmo = UFCA.connectPV(instrum+":cc:datum.timeout", to)
val = UFCA.connectPV(instrum+":cc:datum.val", to)
#
sub setPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my i, o
  #print "PV == pv, type == styp, val == val\n"
  if typ == PVInt  :
    i = UFCA.getInt(ch, to) UFCA.putInt(ch, val, to) o = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    i = UFCA.getDouble(ch, to) UFCA.putDouble(ch, val, to) o = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    i = UFCA.getString(ch, to) UFCA.putString(ch, val, to) o = UFCA.getString(ch, to)
  #end
  print "initial pv was i ==> new pv is o\n"
#end
#
sub getPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my val
  if typ == PVInt  :
    val = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    val = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    val = UFCA.getString(ch, to)
  #end
  print "pv type == styp, val == val\n"
  return val
#end
# main:
getPV(sad)
val = Preset
# init:
setPV(initR, Idle)
setPV(init, Preset)
setPV(applyR, Idle)
setPV(apply, Start)
done = 0
while  done == 0  :
  ir = getPV(initR)
  ar = getPV(applyR)
  if ir == Error || ar == Error  : done = -1 #end
  if ir == Idle and ar == Idle  : done = 1 #end
  time.sleep 1
#end
# datum:
setPV(datumR, Idle)
setPV(dck, Preset)
setPV(dckbl, 20)
setPV(dckdir, "+")
setPV(dckvel, 100)
setPV(f1, Preset)
setPV(f1bl, 20)
setPV(f1dir, "+")
setPV(f1vel, 100)
f2 = UFCA.connectPV(instrum+":cc:datum.Filter2", to)
f2bl = UFCA.connectPV(instrum+":cc:datum.Filter2Backlash", to)
f2dir = UFCA.connectPV(instrum+":cc:datum.Filter2Direction", to)
f2vel = UFCA.connectPV(instrum+":cc:datum.Filter2Vel", to)
setPV(f2, Preset)
setPV(f2bl, 20)
setPV(f2dir, "+")
setPV(f2vel, 100)
foc = UFCA.connectPV(instrum+":cc:datum.Focus", to)
focbl = UFCA.connectPV(instrum+":cc:datum.FocusBacklash", to)
focdir = UFCA.connectPV(instrum+":cc:datum.FocusDirection", to)
focvel = UFCA.connectPV(instrum+":cc:datum.FocusVel", to)
setPV(foc, Preset)
setPV(focbl, 20)
setPV(focdir, "+")
setPV(focvel, 100)
grsm = UFCA.connectPV(instrum+":cc:datum.Grism", to)
grsmbl = UFCA.connectPV(instrum+":cc:datum.GrismBacklash", to)
grsmdir = UFCA.connectPV(instrum+":cc:datum.GrismDirection", to)
grsmvel = UFCA.connectPV(instrum+":cc:datum.GrismVel", to)
setPV(grsm, Preset)
setPV(grsmbl, 20)
setPV(grsmdir, "+")
setPV(grsmvel, 100)
lyot = UFCA.connectPV(instrum+":cc:datum.Lyot", to)
lyotbl = UFCA.connectPV(instrum+":cc:datum.LyotBacklash", to)
lyotdir = UFCA.connectPV(instrum+":cc:datum.LyotDirection", to)
lyotvel = UFCA.connectPV(instrum+":cc:datum.LyotVel", to)
setPV(lyot, Preset)
setPV(lyotbl, 20)
setPV(lyotdir, "+")
setPV(lyotvel, 100)
mos = UFCA.connectPV(instrum+":cc:datum.MOS", to)
mosbl = UFCA.connectPV(instrum+":cc:datum.MOSBacklash", to)
mosdir = UFCA.connectPV(instrum+":cc:datum.MOSDirection", to)
mosvel = UFCA.connectPV(instrum+":cc:datum.MOSVel", to)
setPV(mos, Preset)
setPV(mosbl, 20)
setPV(mosdir, "+")
setPV(mosvel, 100)
win = UFCA.connectPV(instrum+":cc:datum.Window", to)
winbl = UFCA.connectPV(instrum+":cc:datum.WindowBacklash", to)
windir = UFCA.connectPV(instrum+":cc:datum.WindowDirection", to)
winvel = UFCA.connectPV(instrum+":cc:datum.WindowVel", to)
setPV(win, Preset)
setPV(winbl, 20)
setPV(windir, "+")
setPV(winvel, 100)
#dir = UFCA.connectPV(instrum+":cc:datum.dir", to)
#icid = UFCA.connectPV(instrum+":cc:datum.icid", to)
#mess = UFCA.connectPV(instrum+":cc:datum.mess", to)
#ocid = UFCA.connectPV(instrum+":cc:datum.ocid", to)
#tmo = UFCA.connectPV(instrum+":cc:datum.timeout", to)
#val = UFCA.connectPV(instrum+":cc:datum.val", to)
setPV(applyR, Idle)
setPV(apply, Start)
done = 0
while  done == 0  :
  ir = getPV(initR)
  ar = getPV(applyR)
  if ir == Error || ar == Error  : done = -1 #end
  if ir == Idle and ar == Idle  : done = 1 #end
  time.sleep 1
#end

exit
