#!/usr/bin/env python
rcsId = '$Name:  $ $Id: ccinit.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, socket, sys, time
from subprocess import *
ufpy = os.getenv("UFINSTALL") + "/python"
if not (ufpy == None) :
  print "ufpy= "+ufpy
else:
  print "UFINSTALL is not defined, abort..."
  sys.exit()
ldpath = os.getenv("LD_LIBRARY_PATH")
if not (ldpath == None) :
  print "ldpath= "+ldpath
else:
  print "LD_LIBRARY_PATH is not defined, abort..."
  sys.exit()
pypath = os.getenv("PYTHONPATH")
if not (pypath == None) :
  print "pypath= "+pypath
else:
  print "PYTHONPATH is not defined, abort..."
  sys.exit()

import UFCA
from UFF2 import *

#UFF2.ufF2printAllNamedPositions()
ufF2printAllNamedPositions()

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "fu" #"flam"
#host = `/usr/bin/env hostname`
#host = Popen(["/usr/bin/env", "hostname"], stdout=PIPE).communicate()[0]
host = socket.gethostname()
print "host= "+host+", argv0= "+arg+", instrum= "+instrum

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum= " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg= " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
applyR = UFCA.connectPV(instrum+":eng:apply.DIR", to)
applyC = UFCA.connectPV(instrum+":eng:applyC", to)
setupC = UFCA.connectPV(instrum+":cc:setupC", to)
ccIhost = UFCA.connectPV(instrum+":cc:init.host", to)
ccIM = UFCA.connectPV(instrum+":cc:init.MARK", to)
ccIC = UFCA.connectPV(instrum+":cc:initC", to)
# init:
pv = UFCA.getPVName(applyR)
print "ccinit> clear system: ",pv,""
p = UFCA.putInt(applyR, Clear)
ca = UFCA.getInt(applyC)
p = UFCA.putString(ccIhost, host)
m = UFCA.getInt(ccIM)
pv = UFCA.getPVName(ccIM)
if m <= 0 : print "ccinit> mark failed for",pv,"" #end
print "ccinit> set ",pv," to host ",host," , ",instrum,":init marked, applying start...",""
p = UFCA.putInt(applyR, Start)
ca = UFCA.getInt(applyC)
pv = UFCA.getPVName(applyC)
ci = UFCA.getInt(ccIC)
pvi = UFCA.getPVName(ccIC)
cnt = 10
while ca == Busy and ci == Busy and --cnt > 0 :
  print "ccinit> ",pv,": ",ca," ",pvi,": ",ci
  time.sleep(1)
  ci = UFCA.getInt(ccIC)
  ca = UFCA.getInt(applyC)
#end
print "ccinit> ",pv,": ",ca," ",pvi,": ",ci
sys.exit()
