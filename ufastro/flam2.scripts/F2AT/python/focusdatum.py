#!/bin/env python
rcsId = '$Name:  $ $Id: focusdatum.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
#dbprint()
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0
init = 0
limit = 0
stepcnt = 0
  print "focus.pl [-help] [-init] [-limit] (+/-)stepcnt\n"
  exit
#end
#
    print "focus.pl [-help] [-init] [-limit] (+/-)stepcnt\n"
    exit
  #end
    init = 1
  #end
  #end
  else :
  #end
#end
#end

apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyC = UFCA.connectPV(instrum+":eng:applyC", to)

if init > 0  :
  print "focus> init...\n"
  focusE = UFCA.connectPV(instrum+":ec:init.dir", to)
  focusEC = UFCA.connectPV(instrum+":ec:initC", to)
  focusC = UFCA.connectPV(instrum+":cc:init.dir", to)
  focusCC = UFCA.connectPV(instrum+":cc:initC", to)
  pc = UFCA.putInt(focusC, Start)
  cc = UFCA.getInt(focusCC)
  print "focus init cc car: cc\n"
  cnt = 3
  while  cc == Busy and cnt-- > 0  :
    print "focus init cc car: cc\n"
    time.sleep 1
    cc = UFCA.getInt(focusCC)
  #end
  pe = UFCA.putInt(focusE, Start)
  ec = UFCA.getInt(focusEC)
  print "focus init ec car: ec\n"
  cnt = 3
  while ec == Busy and cnt-- > 0  :
    print "focus init ec car: ec\n"
    time.sleep 1
    ec = UFCA.getInt(focusEC)
  #end
# also datum the indexors:
  datumC = UFCA.connectPV(instrum+":cc:datum.All", to)
  datumCC = UFCA.connectPV(instrum+":cc:datumC", to)
  pc = UFCA.putInt(datumC, Start)
  pc = UFCA.putInt(apply, Start)
  cc = UFCA.getInt(datumCC)
  print "datum cc car: cc\n"
  cnt = 30
  while  cc == Busy and cnt-- > 0  :
    print "datum cc car: cc\n"
    time.sleep 1
    cc = UFCA.getInt(focusCC)
  #end
  exit
#end

print "focus> seek limit (0==no, +/-1==yes): limit, stepcnt: stepcnt\n"

focusC = UFCA.connectPV(instrum+":eng:focusC", to)
focusI = UFCA.connectPV(instrum+":eng:focusC.ival", to)
g = UFCA.getAsString(focusC)
if g ne Idle  :
  print instrum+":eng:focusC not Idle, force Idle? [y/n]"
  a = getc
  if a eq 'n'  : exit #end
  p = UFCA.putInt(focusI, Idle)
#end
frq = UFCA.connectPV(instrum+":eng:focus.LVDTExcFreqHiLow", to)
vlt = UFCA.connectPV(instrum+":eng:focus.LVDTExcVoltsHiLow", to)
pwr = UFCA.connectPV(instrum+":eng:focus.LVDTPwrOnOff", to)
limvel = UFCA.connectPV(instrum+":eng:focus.LimitVel", to)
steps = UFCA.connectPV(instrum+":eng:focus.Step", to)
stepvel = UFCA.connectPV(instrum+":eng:focus.StepVel", to)
focsad = UFCA.connectPV(instrum+":sad:FOCUSTEP", to)
# seek limit first?
if limit != 0  :
  p = UFCA.putInt(pwr, 1)
  p = UFCA.putInt(frq, 0)
  p = UFCA.putInt(vlt, 0)
  p = UFCA.putInt(limvel, -20)
  p = UFCA.putInt(apply, Start)
  sad = UFCA.getInt(focsad)
  app = UFCA.getInt(applyC)
  print "app, sad ... "
  while app == Busy  :
    print "app, sad ... "
    time.sleep 1
    app = UFCA.getInt(applyC)
    sad = UFCA.getInt(focsad)
  #end
#end
#
if stepcnt == 0  : exit #end
#
# else perform requested step:
p = UFCA.putInt(stepvel, 20)
p = UFCA.putInt(steps, stepcnt)
p = UFCA.putInt(apply, Start)
sad = UFCA.getInt(focsad)
app = UFCA.getInt(applyC)
print "app, sad ... "
while app == Busy  :
  print "app, sad ... "
  time.sleep 1
  app = UFCA.getInt(applyC)
  sad = UFCA.getInt(focsad)
#end
exit

############################## subs #####################
sub dbprint :
  my cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`
  my @dball = split /\n/,cc
  foreach db (@dball) :
    print "db\n"
  #end
  dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,dc
  foreach db (@dball) :
    print "db\n"
  #end
  ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,ec
  foreach db (@dball) :
    print "db\n"
  #end
  eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,eng
  foreach db (@dball) :
    print "db\n"
  #end
#end

