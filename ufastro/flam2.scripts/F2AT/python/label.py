#!/bin/env python
rcsId = '$Name:  $ $Id: label.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

to = 0.5
dbi = instrum+":sad:datalabel.inp"
db = instrum+":sad:datalabel"
chan = UFCA.connectPV(db, to)
label = UFCA.getString(chan, to)
print "db == label\n"
ichan = UFCA.connectPV(dbi, to)
UFCA.putString(ichan, val, to)
h = UFCA.getInt(chan, to)
label = UFCA.getString(chan, to)
print "db == label\n"
exit

