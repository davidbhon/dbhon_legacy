#!/bin/env python
rcsId = '$Name:  $ $Id: ccsetup.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, socket, sys, time
from subprocess import *
ufpy = os.getenv("UFINSTALL") + "/python"
if not (ufpy == None) :
  print "ufpy= "+ufpy
else:
  print "UFINSTALL is not defined, abort..."
  sys.exit()
#endif
ldpath = os.getenv("LD_LIBRARY_PATH")
if not (ldpath == None) :
  print "ldpath= "+ldpath
else:
  print "LD_LIBRARY_PATH is not defined, abort..."
  sys.exit()
#endif
pypath = os.getenv("PYTHONPATH")
if not (pypath == None) :
  print "pypath= "+pypath
else:
  print "PYTHONPATH is not defined, abort..."
  sys.exit()
#endif

import UFCA
from UFF2 import *

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [y]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep == None or rep[0] == '' or rep[0] == 'y' or rep[0] == '\n' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

app = arg = sys.argv.pop(0)
instrument = "fu"
heartbeat = instrument + ":heartbeat"
chan = UFCA.connectPV(heartbeat, 1.0)
if chan == None or chan == 0 or UFCA.nullPV(chan) :
  print "epics heartbeat connection failed, abort..."
  sys.exit()
#endif
#print "connected to chan: ",chan
pvname = UFCA.getPVName(chan)
val = UFCA.getInt(chan)
print "connected to: ",pvname, ", and got val= ",val

xmlfile = "ccsetup.xml"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "ccsetup.py [-h[elp]] [epics/instrument] [-xml inputvals.xml]"
    sys.exit()
  elif arg == "-xml" and len(sys.argv) > 0 :
    xmlfile = arg = sys.argv.pop(0)
  else:
    instrument = arg
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
  if arg == "-xml" and len(sys.argv) > 0 :
    xmlfile = arg = sys.argv.pop(0)
  #end if
#end if

print "epics instrument name == " + instrument,", xmlfile == " + xmlfile

import xml.dom.minidom
from xml.dom.minidom import Node
 
doc = None
try:
  doc = xml.dom.minidom.parse(xmlfile)
#endtry
except:
  print "exception occured with minidom parse of xmlfile "+xmlfile
  sys.exit()
#endexcept  
finally:
  if not doc :
    print "parse of xmlfile "+xmlfile+" failed, abort..."
    sys.exit()
  #endif
#endfinally
  
hashmap = {}
for cad in doc.getElementsByTagName("GeminiCAD") :
  instrum = cad.getAttribute("instrument")
  epicsrec = cad.getAttribute("EPICSrecord")
  if not (epicsrec[0] == ':') :
    epicsrec = ":" + epicsrec
  #endif -- ensure mandatory colon is present
  print "instrum= ",instrum, ", epicsrec= ",epicsrec
  for node in doc.getElementsByTagName("input") :
    epicsfield = node.getAttribute("name")
    if not (epicsfield[0] == '.') :
      epicsrec = "." + epicsrec
    #endif -- ensure mandatory dot is present
    #inp = instrum + epicsrec + epicsfield
    inp = instrument + epicsrec + epicsfield
    #print "input (epics channel) name: ",inp
    for val in node.childNodes :
      if val.nodeType == Node.TEXT_NODE:
        hashmap[inp] = val.data
        #print "found text input: ", inp, " with value: ", hashmap[inp]
      else:
        print "found non-text input: ", inp, ", ignoring..."
      #end if/else
    #end for childNodes
  #end for node
#end for cad

for inp in hashmap :
  val = hashmap[inp]
  #print "connect to: ",inp, ", and put val= ",val
  chan = UFCA.connectPV(str(inp), 1.0)
  if chan == 0 :
    print "failed connect to "+inp
    continue
  #endif
  pvname = UFCA.getPVName(chan)
  UFCA.putString(chan, str(val))
  print "connected to: ",pvname, ", and put val= ",val
#end for

#sys.exit()
