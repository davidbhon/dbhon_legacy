#!/bin/env python
rcsId = '$Name:  $ $Id: mosdatum.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyR = UFCA.connectPV(instrum+":eng:applyC", to)
setupR = UFCA.connectPV(instrum+":cc:setupC", to)
mosR = UFCA.connectPV(instrum+":cc:setupMOSC", to)
init = UFCA.connectPV(instrum+":cc:init.dir", to)
initR = UFCA.connectPV(instrum+":cc:initC", to)
# sad .val:
b0 = UFCA.connectPV(instrum+":sad:MOSPlateBarCode", to)
b1 = UFCA.connectPV(instrum+":sad:MOSPos1BarCode", to)
b2 = UFCA.connectPV(instrum+":sad:MOSPos2BarCode", to)
b3 = UFCA.connectPV(instrum+":sad:MOSPos3BarCode", to)
b4 = UFCA.connectPV(instrum+":sad:MOSPos4BarCode", to)
b5 = UFCA.connectPV(instrum+":sad:MOSPos5BarCode", to)
b6 = UFCA.connectPV(instrum+":sad:MOSPos6BarCode", to)
b7 = UFCA.connectPV(instrum+":sad:MOSPos7BarCode", to)
b8 = UFCA.connectPV(instrum+":sad:MOSPos8BarCode", to)
b9 = UFCA.connectPV(instrum+":sad:MOSPos9BarCode", to)
@barcdsad = (b1, b2, b3, b4, b5, b6, b7, b8, b9)
# sad .inp:
b1i = UFCA.connectPV(instrum+":sad:MOSPos1BarCode.inp", to)
b2i = UFCA.connectPV(instrum+":sad:MOSPos2BarCode.inp", to)
b3i = UFCA.connectPV(instrum+":sad:MOSPos3BarCode.inp", to)
b4i = UFCA.connectPV(instrum+":sad:MOSPos4BarCode.inp", to)
b5i = UFCA.connectPV(instrum+":sad:MOSPos5BarCode.inp", to)
b6i = UFCA.connectPV(instrum+":sad:MOSPos6BarCode.inp", to)
b7i = UFCA.connectPV(instrum+":sad:MOSPos7BarCode.inp", to)
b8i = UFCA.connectPV(instrum+":sad:MOSPos8BarCode.inp", to)
b9i = UFCA.connectPV(instrum+":sad:MOSPos9BarCode.inp", to)
@barcdisad = (b1i, b2i, b3i, b4i, b5i, b6i, b7i, b8i, b9i)
#
sad = UFCA.connectPV(instrum+":sad:MOSSteps", to)
dcnt = UFCA.connectPV(instrum+":sad:DatumCnt", to)
print "connected to Barcode SAD I/O...\n"
#
#
sub setPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my i, o
  print "PV == pv, type == styp, val == val\n"
  if typ == PVInt  :
    i = UFCA.getInt(ch, to) UFCA.putInt(ch, val, to) o = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    i = UFCA.getDouble(ch, to) UFCA.putDouble(ch, val, to) o = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    i = UFCA.getString(ch, to) UFCA.putString(ch, val, to) o = UFCA.getString(ch, to)
  #end

  #print "initial pv was i ==> new pv is o\n"
#end
#
sub getPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my val
  if typ == PVInt  :
    val = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    val = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    val = UFCA.getString(ch, to)
  #end
  print "pv type == styp, val == val\n"

  return val
#end

sub monPV :
  my pv1 = UFCA.getPVName(ch1)
  my pv2 = UFCA.getPVName(ch2)
  UFCA.subscribe(ch1, to)
  UFCA.subscribe(ch2, to)
  my m1 = 0 m2 = 0
  while m1 == 0 || m2 == 0  :
    m1 = UFCA.eventMon(ch1) m2 = UFCA.eventMon(ch2)
    time.sleep 1
  #end
  val1 = getPV(ch1)
  val2 = getPV(ch2)
  print "pv1 == val1 pv2 == val2\n"
#end


# main:
getPV(b0)
for i = 0 i < 9 i++  :
  n = 1 + i
  print "n\. "
  getPV(barcdisad[i])
  getPV(barcdsad[i])
#end
getPV(sad)
dc = getPV(dcnt)
val = Preset
if dc < 0  :
  # init:
  setPV(initR, Idle)
  setPV(init, Preset)
  setPV(applyR, Idle)
  setPV(apply, Start)
  #monPV(applyR, initR)
  pv =  UFCA.getPVName(b0)
  print "sys.init... hit enter to continue." getc
#end

datum = UFCA.connectPV(instrum+":cc:datum.MOS", to)
datumR = UFCA.connectPV(instrum+":cc:datumC", to)
dbl = UFCA.connectPV(instrum+":cc:datum.MOSBacklash", to)
ddir = UFCA.connectPV(instrum+":cc:datum.MOSDirection", to)
dvel = UFCA.connectPV(instrum+":cc:datum.MOSVel", to)

# datum the MOS wheel:
setPV(datum, Preset)
setPV(dbl, 0)
setPV(ddir, 0)
setPV(dvel, 100)
setPV(datumR, Idle)
setPV(applyR, Idle)
setPV(apply, Start)
#monPV(applyR, datumR)
print "datuming... hit enter to continue." getc
getPV(sad)

exit

