#!/bin/env python
rcsId = '$Name:  $ $Id: mosstep.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyR = UFCA.connectPV(instrum+":eng:applyC", to)
setupR = UFCA.connectPV(instrum+":cc:setupC", to)
mosR = UFCA.connectPV(instrum+":cc:setupMOSC", to)
stepR = UFCA.connectPV(instrum+":cc:setupC", to)
sad = UFCA.connectPV(instrum+":sad:MOSSteps", to)
#print "connected to Barcode SAD I/O...\n"
#
sub setPV :
  my argc = @_
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my i, o
  if argc >= 3  : print "PV == pv, type == styp, val == val\n" #end
  if typ == PVInt  :
    i = UFCA.getInt(ch, to) UFCA.putInt(ch, val, to) o = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    i = UFCA.getDouble(ch, to) UFCA.putDouble(ch, val, to) o = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    i = UFCA.getString(ch, to) UFCA.putString(ch, val, to) o = UFCA.getString(ch, to)
  #end
  if argc >= 3  : print "initial pv was i ==> new pv is o\n" #end
#end
#
sub getPV :
  my argc = @_
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my val
  if typ == PVInt  :
    val = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    val = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    val = UFCA.getString(ch, to)
  #end
  if argc >= 2  : print "pv type == styp, val == val\n" #end
  return val
#end

sub monPV :
  my pv1 = UFCA.getPVName(ch1)
  my pv2 = UFCA.getPVName(ch2)
  UFCA.subscribe(ch1, to)
  UFCA.subscribe(ch2, to)
  my m1 = 0 m2 = 0
  while m1 == 0 || m2 == 0  :
    m1 = UFCA.eventMon(ch1) m2 = UFCA.eventMon(ch2)
    time.sleep 1
  #end
  val1 = getPV(ch1)
  val2 = getPV(ch2)
  print "pv1 == val1 pv2 == val2\n"
#end

# main:
stepcnt = 100
#print "\n"

# step to each barcode position and process barcode capture:
# set step parms?
bacc = UFCA.connectPV(instrum+":cc:setup.MOSBarCdAcc", to)
bdec = UFCA.connectPV(instrum+":cc:setup.MOSBarCdDec", to)
bbl = UFCA.connectPV(instrum+":cc:setup.MOSBarCdBacklash", to)
bivel = UFCA.connectPV(instrum+":cc:setup.MOSBarCdInitVel", to)
brunc = UFCA.connectPV(instrum+":cc:setup.MOSBarCdRunc", to)
bvel = UFCA.connectPV(instrum+":cc:setup.MOSBarCdSlewVel", to)
bdir = UFCA.connectPV(instrum+":cc:setup.MOSBarCdDirection", to)
acc = UFCA.connectPV(instrum+":cc:setup.MOSAcc", to)
dec = UFCA.connectPV(instrum+":cc:setup.MOSDec", to)
bl = UFCA.connectPV(instrum+":cc:setup.MOSBacklash", to)
ivel = UFCA.connectPV(instrum+":cc:setup.MOSInitVel", to)
runc = UFCA.connectPV(instrum+":cc:setup.MOSRunc", to)
vel = UFCA.connectPV(instrum+":cc:setup.MOSSlewVel", to)
dir = UFCA.connectPV(instrum+":cc:setup.MOSDirection", to)
print "connected to all pv channels ... \n"
setPV(bivel, 100)
setPV(bvel, 100)
setPV(bacc, 200)
setPV(bbl, 0)
setPV(brunc, 50)
setPV(bdir, "+")
setPV(ivel, 100)
setPV(vel, 100)
setPV(acc, 200)
setPV(bl, 0)
setPV(runc, 50)
setPV(dir, "+")
#step = UFCA.connectPV(instrum+":cc:setup.MOSBarCdStep", to)
step = UFCA.connectPV(instrum+":cc:setup.MOSStep", to)
setPV(mosR, Idle)
setPV(stepR, Idle)
setPV(applyR, Idle)
setPV(step, stepcnt, 1)
setPV(apply, Start)
print "stepping stepcnt ... \n"
pos = getPV(sad, 1)
newpos = pos
diff = abs(newpos - pos)
abstep = abs(stepcnt)
while diff < abstep  :
  newpos = getPV(sad)
  diff = abs(newpos - pos)
  print "orig. pos == pos, new pos. == newpos, diff == diff\n"
  time.sleep 1
#end
exit
