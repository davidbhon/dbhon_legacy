#!/bin/env python
rcsId = '$Name:  $ $Id: fuedt.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

chid = UFCA.connectPV('fu:sad:EDTFRAME', 0.5)
UFCA.putInt(chid, 200)
edt = UFCA.getInt(chid)
sedt = UFCA.getAsString(chid)
UFCA.putInt(chid, 500)
edt = UFCA.getInt(chid)
print "int edtframe == ",edt,", prev. edtframe value == ", sedt, "\n"
