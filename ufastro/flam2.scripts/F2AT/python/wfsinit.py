#!/bin/env python
rcsId = '$Name:  $ $Id: wfsinit.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyR = UFCA.connectPV(instrum+":eng:applyC", to)
setupR = UFCA.connectPV(instrum+":cc:setupC", to)
mosR = UFCA.connectPV(instrum+":cc:setupMOSC", to)
init = UFCA.connectPV(instrum+":cc:init.dir", to)
initR = UFCA.connectPV(instrum+":cc:initC", to)
sad = UFCA.connectPV(instrum+":sad:MOSSteps", to)
#
#
sub setPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my i, o
  #print "PV == pv, type == styp, val == val\n"
  if typ == PVInt  :
    i = UFCA.getInt(ch, to) UFCA.putInt(ch, val, to) o = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    i = UFCA.getDouble(ch, to) UFCA.putDouble(ch, val, to) o = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    i = UFCA.getString(ch, to) UFCA.putString(ch, val, to) o = UFCA.getString(ch, to)
  #end
  print "initial pv was i ==> new pv is o\n"
#end
#
sub getPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my val
  if typ == PVInt  :
    val = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    val = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    val = UFCA.getString(ch, to)
  #end
  print "pv type == styp, val == val\n"
  return val
#end

sub monPV :
  my pv1 = UFCA.getPVName(ch1)
  my pv2 = UFCA.getPVName(ch2)
  UFCA.subscribe(ch1, to)
  UFCA.subscribe(ch2, to)
  my m1 = 0 m2 = 0
  while m1 == 0 || m2 == 0  :
    m1 = UFCA.eventMon(ch1) m2 = UFCA.eventMon(ch2)
    time.sleep 1
  #end
  val1 = getPV(ch1)
  val2 = getPV(ch2)
  print "pv1 == val1 pv2 == val2\n"
#end


# main:
getPV(sad)
val = Preset
# init:
setPV(initR, Idle)
setPV(init, Preset)
setPV(applyR, Idle)
setPV(apply, Start)
#monPV(applyR, initR)
done = 0
while  done == 0  :
  ir = getPV(initR)
  ar = getPV(applyR)
  if ir == Error || ar == Error  : done = -1 #end
  if ir == Idle and ar == Idle  : done = 1 #end
  time.sleep 1
#end
exit
