#!/usr/local/bin/perl
use UFF2;  # the swig generated module with ufF2
use IO::Socket;  # standard Perl TCP/UDP/IP socket module
use Config;

defined $Config{sig_name} || die "No sigs?";

sub sighandler {
  my $sig = shift;
  print "sighandler> recv'd sig: $sig\n";
  if ($sig eq "INT") {
    $issueCmds = 0;
  } else {
    $issueCmds = 1;
  }
  while ($issueCmds == 0) {
    print "Enter a command (type exit to quit or ENTER to continue): ";
    $userCmd = <STDIN>;
    if ($userCmd eq "\n") {
        $issueCmds = 1;
        $isFinished = 1;
    } elsif ($userCmd eq "exit\n" or $userCmd eq "quit\n") {
        exit;
    } else {
        print "CMD: $userCmd\n";
        print $socG "$userCmd\r\n";
        $socG->flush();
        $reply = <$socG>;
        print "REPLY: $reply\n";
    }
  }
}

my $name = "ZERO";
foreach $name (split(' ', $Config{sig_name})) {
  #print "set sighandler for sig: $name\n";
  if( $name ne "ZERO" ) { $SIG{$name} = \&sighandler; }
}

$host = "flamperle";
#Grism
$portG = 7018;

$socG = IO::Socket::INET->new ( PeerAddr => $host, PeerPort => $portG, Proto => 'tcp', Type => SOCK_STREAM );
if (!defined($socG)) {
   die "failed to connect to $host on port $portG\n";
} else {
   print "connected to $host on port $portG\n";
   print "To escape, press CTRL+C\n";
}

#Login
$temp = "";
while (index($temp, "flam") < 0) {
   $temp = <$socG>;
}
$cmd = "ufastro\r\n";
print $socG "$cmd";
$socG->flush();
$login = <$socG>;
sleep(1);
print $socG "$cmd";
$socG->flush();
$passwd = <$socG>;

sleep(2);
print $socG " \r\n";
$socG->flush();
$temp = "";
while (index($temp, "#") == -1) {
   $temp = <$socG>;
   print $temp;
}

$cmd[0] = "A\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; 
$cmd[1] = "B\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
$cmd[2] = "C\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
$cmd[3] = "D\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

$timeout = 210;
$motor = "Grism";
$curPos = 0; 
$desPosName = "Open";
$blash = 10;
$fastVel = 400;
$slowVel = 200;

$status = 0;

if( @ARGV > 0 ) {
 while(@ARGV > 0) {
  $argval = shift;
  $desPosName = $argval;
  if ($desPosName eq "debug") {
     $debug = 1;
     $argval = shift;
     $desPosName = $argval;
  } elsif ($desPosName eq "-help") {
     $status = 2;
  } elsif ($desPosName eq "-backlash") {
     $argval = shift;
     $desPosName = $argval;
     $blash = int($argval);
  } else {
     $debug = 0;
  }
 }
} else {
  $status = 1;
}

if ($status == 2) {
   UFF2::getNamedPositions("Grism");
   exit;
}

#Read current position
$cmd = "Z\r\n"; #req. indexor A step count
print $socG "$cmd";
$socG->flush();
$curPos = <$socG>;
if (length($curPos) > 5) {
   chomp($curPos);
   @temp = split(/ /,$curPos);
   $curPos = $temp[-2];
   print "Current Position: $curPos\n";
   $curPosName = UFF2::getPositionName($motor, $curPos);
   print "Current Position: $curPosName\n";
} else {
   print "Error reading current position\n";
   print "Continue? (y/n): ";
   $userCmd = <STDIN>;
   if ($userCmd ne "y\n") {
      exit;
   }
}

if ($status == 1) {
   UFF2::getNamedPositions("Grism");
   exit;
}

$count = UFF2::ufF2mechMotion4Named($motor, $curPos, $desPosName, $blash, $fastVel, $slowVel, $cmd[0], $cmd[1], $cmd[2], $cmd[3]);
print "OUTPUT: $count \n";
print "To abort, press CTRL+C\n";
for ($j = 0; $j < $count; $j++) {
  print "$cmd[$j]\n";
  #Strip out indexor name
  $cmd[$j] = substr($cmd[$j], 1);
  #Strip out U
  while (index($cmd[$j], "U") != -1) {
      $upos = index($cmd[$j], "U");
      substr($cmd[$j], $upos, 1, "");
  }
  #Strip out leading spaces
  while (substr($cmd[$j], 0, 1) eq ' ') {
      $cmd[$j] = substr($cmd[$j], 1);
  }
  #Strip off position info after ::
  if (index($cmd[$j], "::") != -1) {
      $cmd[$j] = substr($cmd[$j], 0, index($cmd[$j], "::"));
  }
  $slowHomeCmd = "F $slowVel 0";
  if (substr($cmd[$j],0,length($slowHomeCmd)) eq $slowHomeCmd) {
      $doOrigin = 1;
  } else {
      $doOrigin = 0;
  }
  print "Doing command number $j:\n";
  print "$cmd[$j]\n";
  if (substr($cmd[$j], 0, 1) eq 'F') {
      $desPos = 0;
  } elsif (substr($cmd[$j], 0, 1) eq '+' or substr($cmd[$j], 0, 1) eq '-') {
      $desPos = $curPos+$cmd[$j];
  }
  print "Curr: $curPos   Dest: $desPos\n\n";
  if ($debug == 0) {
    print $socG "$cmd[$j]\r\n";
    $socG->flush();
    $reply = <$socG>;
    print "REPLY: $reply\n";
    $isFinished = 0;
    $time = 0;
    $status2 = $curPos;
    while ($isFinished == 0) {
      sleep(4);
      $time+=4;
      if ($time > $timeout) {
	$issueCmds = 0;
        print "Time exceeded $timeout sec!\n";
	while ($issueCmds == 0) {
	   print "Enter a command (or ENTER to exit): ";
	   $userCmd = <STDIN>;
	   print "CMD: $userCmd\n";
	   if ($userCmd eq "\n") {
	      $issueCmds = 1;
	      $isFinished = 1;
           } elsif ($userCmd eq "exit\n" or $userCmd eq "quit\n") {
              exit;
	   } else {
	      print $socG "$userCmd\r\n";
	      $socG->flush();
	      $reply = <$socG>;
	      print "REPLY: $reply\n";
	   }
	}
      }
      $oldPos = $status2;
      print $socG "^\r\n";
      $socG->flush();
      $status1 = <$socG>;
      $pos1 = rindex($status1, " ");
      $pos2 = rindex($status1, "\r");
      $status1 = substr($status1, $pos1+1, $pos2-$pos1-1);
      sleep(1);
      $time+=1;
      print $socG "Z\r\n";
      $socG->flush();
      $status2 = <$socG>;
      chomp($status2);
      @temp = split(/ /,$status2);
      $status2 = $temp[-2];
      print "TIME: $time;  ^ (motion status): $status1;  Z (step count): $status2\n";
      if ($desPos != 0) {
	if ($status1 == 0 and $status2 == $oldPos) {
	   $isFinished = 1;
	   $curPos = $status2;
	}
      } else {
	if ($status1 == 0 and $status2 == $oldPos) {
	   $isFinished = 1;
	   $curPos = 0;
	}
      }
    }
    if ($doOrigin == 1) {
      print $socG "o\r\n";
      $socG->flush();
      $reply = <$socG>;
      print "ORIGIN REPLY: $reply";
      sleep(1);
      print $socG "Z\r\n";
      $socG->flush();
      $reply = <$socG>;
      print "Current Position: $reply";
    }
  }
}
#Read current position
$cmd = "Z\r\n"; #req. indexor A step count
print $socG "$cmd";
$socG->flush();
$curPos = <$socG>;
chomp($curPos);
@temp = split(/ /,$curPos);
$curPos = $temp[-2];
$curPosName = UFF2::getPositionName($motor, $curPos);
print "Current Position: $curPosName\n";
print "Disconnected from $host\n";
exit;
