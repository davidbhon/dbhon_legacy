#!/usr/local/bin/perl
use CGI;
use CGI::Carp;
#use LWP::Simple;
#use HTML::Parse;
#use HTML::Element;
#use URI::URL;

my $cgi = new CGI;
# since i don't know how to navigate the password via LWP/HTML/URI, just
# make use of wget for now...
my $url = 'http://192.168.111.30/axis-cgi/jpg/image.cgi?camera=&resolution=640x480';
my $wget = '/usr/local/bin/wget -q --http-user=ufastro --http-passwd=ufastro -O - ';
my $img = `$wget $url`;
print $cgi->header( -type => "image/jpg", -expires => "now" );
binmode STDOUT;
print $img;

exit;
