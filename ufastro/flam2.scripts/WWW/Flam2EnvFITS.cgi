#!/usr/local/bin/perl
#
$rcsId = '$Name:  $ $Id: Flam2EnvFITS.cgi,v 0.1 2004/12/14 17:33:59 hon Exp $';
#
$LOCAL = "/usr/local";
use lib "/share/local/uf/perl";
use CGI;
use CGI::Carp qw(fatalsToBrowser);

$cgi = new CGI;
$organization = "University of Florida";
$department = "Department of Astronomy";
$refresh = 30;
$doctitle = "UF Flamingos-2 EnvironmenT Status as FITS";
$instrum = "flam";

print $cgi->header( -type => "text/html", -expires => "now", -refresh => "$refresh" );
print $cgi->start_html( -title=> "$doctitle", -bgcolor=> "#ffffff" );
($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime(time());
$y = $y + 1900; $yd = $yd + 1;
if( $s < 10 ) { $s = "0$s"; }
if( $m < 10 ) { $m = "0$m"; }
if( $h < 10 ) { $h = "0$h"; }
$output = "$y:$yd:$h:$m:$s $doctitle ($refresh sec. Updates):\n";
print $cgi->p("$output");
$separate = "=========================================================\n";
print $cgi->p("$separate");

agentfits();

print $cgi->p("$separate");
$output = "$rcsId\n";
print $cgi->p("$output");
$cgi->end_html();

############################################# sub agentfits() #########################################
sub agentfits {
#$agenthost = `/bin/hostname`;
#chomp $agenthost;
$agenthost = "192.168.111.222";
$run = "/bin/env LD_LIBRARY_PATH=/share/local/uf/lib:$LOCAL/lib:/gemini/epics/lib/solaris-sparc-gnu:/opt/EDTpdv /share/local/uf/bin";
$pfvac = `$run/ufvac -stat fits -host $agenthost -port 52004`;
chomp $pfvac;
@vac = split /\n/,$pfvac;
$output = @vac[0];
print $cgi->p("$output");
$output = @vac[1];
print $cgi->p("$output");
#
$ls332 = `$run/uflsc -stat fits -host $agenthost -port 52003`;
chomp $ls332;
$output = $ls332;
@ls332 = split /\n/,$ls332;
$output = @ls332[0];
print $cgi->p("$output");
$output = @ls332[1];
print $cgi->p("$output");
$output = @ls332[2];
print $cgi->p("$output");
$output = @ls332[3];
print $cgi->p("$output");
#
$ls218 = `$run/uflsc -stat fits -host $agenthost -port 52002`;
chomp $ls218;
@ls218 = split /\n/,$ls218;
$output = @ls218[0];
print $cgi->p("$output");
$output = @ls218[1];
print $cgi->p("$output");
$output = @ls218[2];
print $cgi->p("$output");
$output = @ls218[3];
print $cgi->p("$output");
$output = @ls218[4];
print $cgi->p("$output");
$output = @ls218[5];
print $cgi->p("$output");
$output = @ls218[7];
print $cgi->p("$output");
$output = @ls218[8];
print $cgi->p("$output");
#
}
