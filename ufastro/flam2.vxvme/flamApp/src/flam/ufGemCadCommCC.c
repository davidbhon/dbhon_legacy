#if !defined(__UFGEMCADCOMMCC_C__)
#define __UFGEMCADCOMMCC_C__ "RCS: $Name:  $ $Id: ufGemCadCommCC.c,v 0.0 2007/02/02 21:09:55 aaguayo Developmental $"
static const char rcsIdufGEMCADCOMMCCC[] = __UFGEMCADCOMMCC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <cad.h>
#include <cadRecord.h>
#include <sirRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

#include <dbAccess.h>

#include "flam.h"
#include "ufGemComm.h"
/*#include <flamPositionLookup.h>*/

long
motorInitCommand (cadRecord * pcr)
{
  long out_vala;
  char *endptr;
  long temp_long;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a); */
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else
	{
	  *(long *) pcr->vala = -1;
	  /* strncpy (pcr->mess, "Bad init command",
	     MAX_STRING_SIZE); return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
motorTestCommand (cadRecord * pcr)
{
  /* 
     double out_val ; char *endptr ; long temp_long ; */
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* 
         out_val = strtod(pcr->a,&endptr) ;

         if (*endptr != '\0'){ strncpy (pcr->mess, "Can't
         convert test directive", MAX_STRING_SIZE); return 
         CAD_REJECT; } temp_long = (long)out_val ; if
         (UFcheck_long(temp_long,0,1) == -1) {
         strncpy(pcr->mess, "test directive is not valid",
         MAX_STRING_SIZE) ; return CAD_REJECT ; } *(long 
         *)pcr->vala = out_val ; */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/
long
motorStepsCommand (cadRecord * pcr)
{

  double out_val;
  char *endptr;
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      out_val = strtod (pcr->a, &endptr);
      if (*endptr != '\0')
	{
	  strncpy (pcr->mess, "Can't convert steps value", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      if (UFcheck_double (fabs (out_val), 0.0, num_steps_hi) == -1)
	{
	  strncpy (pcr->mess, " Number of Steps is not valid",
		   MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      *(double *) pcr->vala = out_val;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
motorDatumCommand (cadRecord * pcr)
{
  /* 
     double out_val ; char *endptr ; long temp_long ; */
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* 
         out_val = strtod(pcr->a,&endptr) ;

         if (*endptr != '\0'){ strncpy (pcr->mess, "Can't
         convert datum directive", MAX_STRING_SIZE);
         return CAD_REJECT; } temp_long = (long)out_val ; 
         if (UFcheck_long(temp_long,0,1) == -1) {
         strncpy(pcr->mess, "datum directive is not valid", 
         MAX_STRING_SIZE) ; return CAD_REJECT ; } *(long 
         *)pcr->vala = temp_long ; */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
motorStopCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }
  return CAD_ACCEPT;
}

/**********************************************************/
static long motorAbortCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
long motorInitNamedPosCommand( cadRecord * pcr )
{
  long imot;
  for( imot=0; imot < NMOTORS ; ++imot ) Hi_Level_Move[imot] = 0;
  return CAD_ACCEPT;
}

/**********************************************************/
long motorNamedPosCommand( cadRecord * pcr )
{
  char rec_name[60], seps[]=":";
  char *motor_name, *ctemp;
  double motor_offset;
  int pos_num;
  char *endptr;
  long HL_index;
  long status = OK;

  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start: check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      if (strcmp (pcr->a, "PARK") == 0)
	pos_num = -1;
      else
	{
	  pos_num = strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	    strncpy (pcr->mess,"Can't convert position num",MAX_STRING_SIZE);
	    return CAD_REJECT;
	    }
	}

      /* motor name is the 3rd token separated by colons */
      strcpy( rec_name, pcr->name );
      motor_name = strtok( rec_name, seps );
      motor_name = strtok( NULL, seps );
      motor_name = strtok( NULL, seps );
      ctemp = strchr( motor_name, ':' );
      if( ctemp != NULL ) ctemp[0] = '\0';  /* remove the final ':' char */

      HL_index = UFGetSteps( motor_name, pos_num, &motor_offset );

      if (HL_index < 0)
	{
	  if( strstr( motor_name, "coldClmp" ) != 0 )/* Cold Clamp ignored */
	    motor_offset = 0;
	  else {
	    sprintf( _UFerrmsg,"Motor name: %s, not found in table",motor_name);
	    ufLog( _UFerrmsg ) ;
	    strncpy (pcr->mess, _UFerrmsg, MAX_STRING_SIZE);
	    return CAD_REJECT;
	  }
	}
      else {
	Hi_Level_Move[HL_index] = 1;
	strcpy( pcr->valc, pcr->a );  /* copy the input named pos to output */
		
	status = flamNameLookup (pcr->b, pcr->a, pcr->vald);
	if (status)
	  printf ("%s name translation failed\n",(char *)pcr->b);
	
      }

      *(long *) pcr->vala = 0; /* not a datum (1) so set with 0 (-1 is no)*/
      *(double *) pcr->valb = motor_offset;

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }
  return CAD_ACCEPT;
}

/**********************************************************/
long motorOriginCommand (cadRecord * pcr)
{
  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
long ccAbortCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ccTestCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ccDatumCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, "1");
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ccStopCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ccParkCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      strcpy (pcr->vala, "PARK");
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ccInitCommand (cadRecord * pcr)
{
  double out_val;
  char *endptr;
  long temp_long;
  char empty_str[40];
  char in_str[40];

  strcpy (empty_str, "");
  strcpy (in_str, pcr->a);
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      if (strcmp (in_str, empty_str) != 0)
	{
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert init directive",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = (long) out_val;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "Init directive is  not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (temp_long == SIMM_FAST)
	    *(long *) pcr->valb = 1;
	  else
	    *(long *) pcr->valb = 0;
	    /* *(long *) pcr->valb = 0; */
	  strcpy (pcr->vala, pcr->a);
	}
      else
	{
	  /* strncpy (pcr->mess, "Can't convert init
	     directive", MAX_STRING_SIZE); return
	     CAD_REJECT; */
	}

      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

 /* ===================================================================== */
 /* INDENT OFF */
 /*
  * Function name:
  * readBarcodeCommand
  * 
  * Purpose:
  * Check to see if a SIR record has been specified and, if so, trigger
  * the barcode scanner sequence.
  *
  * Invocation:
  * status = readBarcodeCommand (cadRecord *pcr);
  *
  * Parameters in:
  *      > pcr->dir   long    command directive
  *      > pcr-A      string  SIR record where barcode will go
  *      > pcr-B      string  Simulation mode
  * 
  * Parameters out:
  *      < pcr->mess  string  status message
  * 
  * Return value:
  *      < status     long    CAD_ACCEPT or CAD_REJECT
  * 
  * Globals: 
  *  External functions:
  *  None
  * 
  *  External variables:
  *  None
  * 
  * Requirements:
  * 
  * Author:
  *  William Rambold (wrambold@gemini.edu)
  * 
  * History:
  * 2006/09/12  WNR  Initial coding
  *
  */
 /* INDENT ON */
 /* ===================================================================== */
 
 long readBarcodeCommand
 (
     cadRecord *pcr              /* cad record structure             */
 )
{
   char sirName[MAX_STRING_SIZE];
   struct dbAddr sirState;
   long status = 0;
   long simulation = -1;
 
   switch (pcr->dir)
     {
       /* Mark, Clear and Stop do nothing so can be accepted immediately */
 
     case menuDirectiveMARK:
       break;
 
     case menuDirectiveCLEAR:
       break;
 
     case menuDirectiveSTOP:
       break;
 
       /* 
        *  Preset checks attribute A for a valid position name.
        */
 
     case menuDirectivePRESET:
     case menuDirectiveSTART:
       if (strcmp(pcr->b, "NONE") == 0) simulation = SIMM_NONE;
       else if (strcmp(pcr->b, "VSM") == 0) simulation = SIMM_VSM;
       else if (strcmp(pcr->b, "FAST") == 0) simulation = SIMM_FAST;
       else if (strcmp(pcr->b, "FULL") == 0) simulation = SIMM_FULL;
       else 
         {
           strncpy (pcr->mess, "invalid simulation mode", MAX_STRING_SIZE); 
           return CAD_REJECT;
         }

       strcpy (sirName, pcr->name);
       strtok (sirName, ":");
       strcat (sirName, ":sad:");
       strcat (sirName, pcr->a);
 
       status = dbNameToAddr (sirName, &sirState);
       if (status)
         {
           strncpy (pcr->mess, "Invalid SIR record name", MAX_STRING_SIZE);
           return CAD_REJECT;
         }

       if (pcr->dir == menuDirectivePRESET) return CAD_ACCEPT;

       strcpy (pcr->vala, pcr->a);
       *(long *)pcr->valb = simulation;
       break;

     default:
       strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
       return CAD_REJECT;
       break;
     }
 
   return CAD_ACCEPT;
 }
 

 /* ===================================================================== */
 /* INDENT OFF */
 /*
  * Function name:
  * scanMasksCommand
  * 
  * Purpose:
  * Trigger the MOS mask barcode reading sequence.
  *
  * Invocation:
  * status = scanMasksCommand (cadRecord *pcr);
  *
  * Parameters in:
  *      > pcr->dir   long    command directive
  * 
  * Parameters out:
  *      < pcr->mess  string  status message
  * 
  * Return value:
  *      < status     long    CAD_ACCEPT or CAD_REJECT
  * 
  * Globals: 
  *  External functions:
  *  None
  * 
  *  External variables:
  *  None
  * 
  * Requirements:
  * 
  * Author:
  *  William Rambold (wrambold@gemini.edu)
  * 
  * History:
  * 2006/09/12  WNR  Initial coding
  *
  */
 /* INDENT ON */
 /* ===================================================================== */
 
 
 long scanMasksCommand
 (
     cadRecord *pcr              /* cad record structure             */
 )
{ 
   switch (pcr->dir)
     {
       /* Mark, Clear and Stop do nothing so can be accepted immediately */
 
     case menuDirectiveMARK:
       *(long *)pcr->valb = 1;
       break;
 
     case menuDirectiveCLEAR:
       *(long *)pcr->valb = 1;
       break;
 
     case menuDirectiveSTOP:
       *(long *)pcr->valb = 1;
       break;
 
       /* 
        *  No attributes for this command so just accept it.
        */
 
     case menuDirectivePRESET:
       *(long *)pcr->valb = 1;
       break;
       
     case menuDirectiveSTART:
printf ("setting scan start flag\n");
       *(long *)pcr->valb = 0;
       *(long *)pcr->vala = 1;
       break;
       
     default:
       strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
       return CAD_REJECT;
     }
 
   return CAD_ACCEPT;
 }
 

#endif /* __UFGEMCADCOMMCC_C__ */

epicsRegisterFunction(motorAbortCommand);
