[schematic2]
uniq 116
[tools]
[detail]
w 1410 1355 100 0 n#113 elongouts.elongouts#107.OUT 1376 1344 1504 1344 hwout.hwout#111.outp
w 1410 1611 100 0 n#112 elongouts.elongouts#106.OUT 1376 1600 1504 1600 hwout.hwout#110.outp
w 1010 1387 100 0 n#109 eseqs.eseqs#40.LNK4 832 1760 960 1760 960 1376 1120 1376 elongouts.elongouts#107.SLNK
w 882 1803 100 0 n#108 eseqs.eseqs#40.LNK3 832 1792 992 1792 992 1632 1120 1632 elongouts.elongouts#106.SLNK
w 434 1771 100 0 n#105 hwin.hwin#100.in 352 1712 416 1712 416 1760 512 1760 eseqs.eseqs#40.DOL4
w 418 1803 100 0 n#104 hwin.hwin#101.in 352 1776 384 1776 384 1792 512 1792 eseqs.eseqs#40.DOL3
w 418 1835 100 0 n#103 hwin.hwin#46.in 352 1840 384 1840 384 1824 512 1824 eseqs.eseqs#40.DOL2
w 434 1867 100 0 n#102 hwin.hwin#45.in 352 1904 416 1904 416 1856 512 1856 eseqs.eseqs#40.DOL1
w 836 1835 100 2 n#96 eseqs.eseqs#40.LNK2 832 1824 832 1856 junction
w 952 1859 100 0 n#96 eseqs.eseqs#40.LNK1 832 1856 1120 1856 ebis.ebis#97.SLNK
w 328 1035 100 0 n#64 junction 288 1024 416 1024 esirs.esirs#81.INP
w 8 1131 100 0 n#64 ecalcs.ecalcs#63.VAL 224 832 288 832 288 1120 -224 1120 -224 1024 -64 1024 ecalcs.ecalcs#63.INPA
w 296 875 100 0 n#82 ecalcs.ecalcs#63.FLNK 224 864 416 864 esirs.esirs#81.SLNK
w 32 1547 100 0 n#42 hwin.hwin#38.in 0 1616 32 1616 32 1536 80 1536 ecalcouts.ecalcouts#114.INPA
w 426 1547 100 0 n#115 ecalcouts.ecalcouts#114.FLNK 400 1536 512 1536 eseqs.eseqs#40.SLNK
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Add more TCS status info
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/12/18
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos Gemini Systems Simulator
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/12/18
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamGeminiSim.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2000/22/25
[cell use]
use ecalcouts 80 1351 100 0 ecalcouts#114
xform 0 240 1472
p 152 1384 100 0 -1 CALC:1
p 912 1934 100 0 0 OOPT:Transition To Non-zero
p 168 1584 100 0 1 SCAN:Passive
p 144 1352 100 0 -1 name:tc1:nodTrigger
p 32 1544 75 0 -1 pproc(INPA):NPP
use hwout 1504 1303 100 0 hwout#111
xform 0 1600 1344
p 1728 1344 100 0 -1 val(outp):flam2:apply.DIR PP NMS
use hwout 1504 1559 100 0 hwout#110
xform 0 1600 1600
p 1728 1600 100 0 -1 val(outp):flam2:continue.DIR PP NMS
use elongouts 1120 1287 100 0 elongouts#107
xform 0 1248 1376
p 1184 1280 100 0 1 name:tc1:startApply
p 1376 1344 75 768 -1 pproc(OUT):PP
use elongouts 1120 1543 100 0 elongouts#106
xform 0 1248 1632
p 1184 1536 100 0 1 name:tc1:markContinue
p 1376 1600 75 768 -1 pproc(OUT):PP
use hwin 160 1735 100 0 hwin#101
xform 0 256 1776
p 128 1776 100 0 -1 val(in):0
use hwin 160 1671 100 0 hwin#100
xform 0 256 1712
p 128 1712 100 0 -1 val(in):3
use hwin -192 1575 100 0 hwin#38
xform 0 -96 1616
p -288 1664 100 0 -1 val(in):flam2:observeC.VAL CPP
use hwin 160 1863 100 0 hwin#45
xform 0 256 1904
p 128 1904 100 0 -1 val(in):1
use hwin 160 1799 100 0 hwin#46
xform 0 256 1840
p 128 1840 100 0 -1 val(in):0
use ebis 1120 1783 100 0 ebis#97
xform 0 1248 1856
p 1184 1760 100 0 1 name:tc1:inPosition
use esirs 1088 775 100 0 esirs#75
xform 0 1296 928
p 1120 704 100 0 1 FTVL:DOUBLE
p 1120 736 100 0 1 NELM:39
p 1120 768 100 0 1 name:tc1:ak:astCtx
use esirs 416 71 100 0 esirs#78
xform 0 624 224
p 448 32 100 0 1 FTVL:DOUBLE
p 448 64 100 0 1 name:tc1:sad:currentRH
use esirs 1088 71 100 0 esirs#80
xform 0 1296 224
p 1120 32 100 0 1 FTVL:DOUBLE
p 1120 64 100 0 1 name:tc1:sad:airMassNow
use esirs 416 775 100 0 esirs#81
xform 0 624 928
p 448 736 100 0 1 FTVL:LONG
p 448 768 100 0 1 name:tc1:sad:heartbeat
use esirs 1760 71 100 0 esirs#84
xform 0 1968 224
p 1792 32 100 0 1 FTVL:DOUBLE
p 1792 64 100 0 1 name:tc1:sad:rotatorRate
use changeBar 1984 1991 100 0 changeBar#72
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#71
xform 0 2336 2064
use ecalcs -64 551 100 0 ecalcs#63
xform 0 80 816
p 0 480 100 0 1 CALC:A+1
p 0 512 100 0 1 SCAN:1 second
p 0 544 100 0 1 name:tc1:hbCalc
use eseqs 512 1447 100 0 eseqs#40
xform 0 672 1696
p 576 1408 100 0 1 DLY1:0.0e+00
p 576 1376 100 0 1 DLY2:5.0e+00
p 576 1344 100 0 1 DLY3:0.0e+00
p 576 1312 100 0 1 DLY4:0.1e+00
p 672 1440 100 1024 1 name:tc1:nodFake
p 480 1856 75 1280 -1 pproc(DOL1):NPP
p 480 1824 75 1280 -1 pproc(DOL2):NPP
p 848 1856 75 1024 -1 pproc(LNK1):PP
p 848 1824 75 1024 -1 pproc(LNK2):PP
p 848 1792 75 1024 -1 pproc(LNK3):PP
p 848 1760 75 1024 -1 pproc(LNK4):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamGeminiSim.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
