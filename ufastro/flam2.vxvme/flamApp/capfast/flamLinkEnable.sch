[schematic2]
uniq 148
[tools]
[detail]
w 930 1515 100 0 n#147 efanouts.efanouts#91.LNK1 1888 1088 2048 1088 2048 1504 -128 1504 -128 992 480 992 ebos.ebos#64.SLNK
w 794 715 -100 0 SLINK inhier.SLINK.P 112 704 1536 704 1536 1008 1648 1008 efanouts.efanouts#91.SLNK
w 1490 1099 100 0 n#145 elongins.elongins#144.VAL 1344 1088 1696 1088 efanouts.efanouts#91.SELL
w 792 1259 100 0 n#80 elongouts.elongouts#143.OUT 736 1248 896 1248 896 960 736 960 ebos.ebos#64.OUT
w 968 1115 100 0 n#80 elongins.elongins#144.SLNK 1088 1104 896 1104 junction
w 2028 1067 -100 0 FAILED efanouts.efanouts#91.LNK2 1888 1056 2240 1056 outhier.FLINK.p
w 218 1227 -100 0 START inhier.ENABLE.P 112 1216 384 1216 384 1280 480 1280 elongouts.elongouts#143.SLNK
w 408 1323 100 0 n#76 hwin.hwin#77.in 384 1392 384 1312 480 1312 elongouts.elongouts#143.DOL
w 408 1035 100 0 n#72 hwin.hwin#66.in 384 1104 384 1024 480 1024 ebos.ebos#64.DOL
s 2624 2064 100 1792 2001/02/22
s 2480 2064 100 1792 WNR
s 2208 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2528 -240 100 1792 flamLinkEnable.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/02/22
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos Link Enable Gate
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use elongins 1088 1031 100 0 elongins#144
xform 0 1216 1104
p 1152 1024 100 0 1 name:$(system)lnkSel
use elongouts 480 1191 100 0 elongouts#143
xform 0 608 1280
p 544 1152 100 0 1 OMSL:closed_loop
p 544 1184 100 0 1 name:$(system)linkEnbl
p 736 1248 75 768 -1 pproc(OUT):PP
use changeBar 1984 2023 100 0 changeBar#142
xform 0 2336 2064
use hwin 192 1063 100 0 hwin#66
xform 0 288 1104
p 195 1096 100 0 -1 val(in):0
use hwin 192 1351 100 0 hwin#77
xform 0 288 1392
p 195 1384 100 0 -1 val(in):3
use outhier 2272 1056 100 0 FLINK
xform 0 2224 1056
use efanouts 1648 871 100 0 efanouts#91
xform 0 1768 1024
p 1712 832 100 0 1 SELM:Mask
p 1824 864 100 1024 1 name:$(system)lnkGate
p 1920 1088 75 1280 -1 pproc(LNK1):PP
p 1920 1056 75 1280 -1 pproc(LNK2):PP
use inhier 0 1216 100 0 ENABLE
xform 0 112 1216
use inhier 16 704 100 0 SLINK
xform 0 112 704
use ebos 480 903 100 0 ebos#64
xform 0 608 992
p 544 864 100 0 1 OMSL:closed_loop
p 544 896 100 0 1 name:$(system)linkDsbl
p 736 960 75 768 -1 pproc(OUT):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamLinkEnable.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
