[schematic2]
uniq 18
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS-3
s 2096 -176 200 1792 Flamingos-2
s 2432 -192 100 256 FLAMONGOS-2 Main Overview
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/03
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamMain.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/22/03
[cell use]
use flamLvdt 2048 327 100 0 flamLvdt#17
xform 0 2272 480
p 2068 300 100 0 1 setLvdt:lvdt $(top)lvdt:
use flamEngTop 400 295 100 0 flamEngTop#16
xform 0 608 464
p 420 268 100 0 1 setEng:eng $(top)eng:
use gem_timelibTop -64 71 100 0 gem_timelibTop#15
xform 0 96 336
p -16 336 100 0 1 seta:top $(top)
use flamDhsStatus 1472 295 100 0 flamDhsStatus#14
xform 0 1696 464
p 1492 268 100 0 1 setDhs:dhs $(top)dhs:
use flamBoot 1072 375 100 0 flamBoot#13
xform 0 1152 480
p 928 320 100 0 1 setExec:exec $(top)eg:
use flamEcTop 928 679 100 0 flamEcTop#12
xform 0 1152 848
p 928 672 100 0 1 setEc:ec $(top)ec:
use flamControl 0 679 100 0 flamControl#10
xform 0 128 1040
use flamStatus 2048 679 100 0 flamStatus#9
xform 0 2176 1040
use flamDcTop 1360 679 100 0 flamDcTop#6
xform 0 1696 848
p 1472 672 100 0 1 setDc:dc $(top)dc:
use flamCcTop 272 679 100 0 flamCcTop#4
xform 0 608 848
p 384 672 100 0 1 setCc:cc $(top)cc:
use flamIsTop 432 1143 100 0 flamIsTop#3
xform 0 1136 1232
p 1568 1376 100 0 1 setBegin:START_TIMEOUT 3000
p 1568 1312 100 0 1 setRun:CMD_TIMEOUT 4500
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamMain.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
