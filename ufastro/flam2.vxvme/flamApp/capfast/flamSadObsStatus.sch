[schematic2]
uniq 48
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos Observation Status
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/11
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamSadObsStatus.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2000/11/11
[cell use]
use esirs 128 -281 100 0 esirs#47
xform 0 336 -128
p 192 -320 100 0 1 SCAN:Passive
p 288 -288 100 1024 -1 name:$(top)LOGSIZE
use esirs 1952 7 100 0 esirs#46
xform 0 2160 160
p 2016 -32 100 0 1 SCAN:Passive
p 2128 0 100 1024 -1 name:$(top)tSWindowStart
use esirs 1952 407 100 0 esirs#44
xform 0 2160 560
p 2016 368 100 0 1 SCAN:Passive
p 2112 400 100 1024 -1 name:$(top)tSWindowEnd
use esirs 1952 823 100 0 esirs#43
xform 0 2160 976
p 2016 784 100 0 1 SCAN:Passive
p 2112 816 100 1024 -1 name:$(top)tSDetectorStart
use esirs 1952 1239 100 0 esirs#42
xform 0 2160 1392
p 2016 1200 100 0 1 SCAN:Passive
p 2112 1232 100 1024 -1 name:$(top)tSDetectorEnd
use esirs 1952 1655 100 0 esirs#41
xform 0 2160 1808
p 2016 1616 100 0 1 SCAN:Passive
p 2112 1648 100 1024 -1 name:$(top)utstart
use esirs 1248 1671 100 0 esirs#40
xform 0 1456 1824
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)acq
use esirs 1248 1255 100 0 esirs#39
xform 0 1456 1408
p 1312 1216 100 0 1 SCAN:Passive
p 1408 1248 100 1024 -1 name:$(top)prep
use esirs 1248 839 100 0 esirs#38
xform 0 1456 992
p 1312 800 100 0 1 SCAN:Passive
p 1408 832 100 1024 -1 name:$(top)rdout
use esirs 1248 423 100 0 esirs#37
xform 0 1456 576
p 1312 384 100 0 1 SCAN:Passive
p 1408 416 100 1024 -1 name:$(top)utend
use esirs 1248 7 100 0 esirs#36
xform 0 1456 160
p 1312 -32 100 0 1 SCAN:Passive
p 1408 0 100 1024 -1 name:$(top)utnow
use esirs 544 7 100 0 esirs#35
xform 0 752 160
p 608 -32 100 0 1 SCAN:Passive
p 704 0 100 1024 -1 name:$(top)sigmaPerFrameRead
use esirs 544 423 100 0 esirs#34
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)observationStatus
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)currentNodCycle
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)currentBeam
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)bgWellStart
use esirs -192 7 100 0 esirs#30
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)bgWellEnd
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)bgWellCurrent
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)bgAdcStart
use esirs -192 1255 100 0 esirs#27
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)bgAdcEnd
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)bgAdcCurrent
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadObsStatus.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
