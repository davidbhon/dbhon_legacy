[schematic2]
uniq 55
[tools]
[detail]
s 2624 2064 100 1792 2001/01/25
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamsSadEcStatus.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos EC Status
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs -320 1671 100 0 esirs#49
xform 0 -112 1824
p -256 1632 100 0 1 SCAN:Passive
p -160 1664 100 1024 -1 name:$(top)bcrName
use esirs 896 1671 100 0 esirs#48
xform 0 1104 1824
p 960 1632 100 0 1 SCAN:Passive
p 1056 1664 100 1024 -1 name:$(top)bcrHealth
use esirs 288 1671 100 0 esirs#47
xform 0 496 1824
p 352 1632 100 0 1 SCAN:Passive
p 448 1664 100 1024 -1 name:$(top)bcrState
use esirs 1504 1671 100 0 esirs#7
xform 0 1712 1824
p 1440 1376 100 0 0 FTVL:LONG
p 1568 1632 100 0 1 SCAN:Passive
p 1664 1664 100 1024 -1 name:$(top)bcrHeartbeat
use esirs 2112 1671 100 0 esirs#53
xform 0 2320 1824
p 2048 1376 100 0 0 FTVL:LONG
p 2304 1472 100 0 0 SNAM:ufSetDatumCnt
p 2224 1664 100 768 1 name:$(top)bcrDatumCnt
use esirs -320 1255 100 0 esirs#54
xform 0 -112 1408
p -384 960 100 0 0 FTVL:LONG
p -256 1216 100 0 1 SCAN:Passive
p -160 1248 100 1024 -1 name:$(top)bcrTriggerScan
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadBcrStatus.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
