[schematic2]
uniq 139
[tools]
[detail]
w 1048 1691 100 0 n#137 hwin.hwin#138.in 1024 1760 1024 1680 1120 1680 elongins.elongins#136.INP
w 1426 843 100 0 n#135 ecalcs.ecalcs#63.FLNK 1312 1184 1344 1184 1344 832 1568 832 ecalcs.ecalcs#83.SLNK
w 1458 1163 100 0 n#109 junction 1408 1152 1568 1152 ecalcs.ecalcs#83.INPC
w 1122 1483 100 0 n#109 ecalcs.ecalcs#63.VAL 1312 1152 1408 1152 1408 1472 896 1472 896 1344 1024 1344 ecalcs.ecalcs#63.INPA
w -182 1131 -100 0 n#117 inhier.STOP.P -272 1120 -32 1120 junction
w 1138 651 100 0 n#117 efanouts.efanouts#91.LNK1 2240 1024 2368 1024 2368 640 -32 640 -32 1184 96 1184 ebos.ebos#64.SLNK
w 1394 1643 100 0 n#131 elongins.elongins#136.VAL 1376 1632 1472 1632 1472 1216 1568 1216 ecalcs.ecalcs#83.INPA
w 888 1291 100 0 n#85 ebis.ebis#79.VAL 800 1280 1024 1280 ecalcs.ecalcs#63.INPC
w 1672 395 100 0 n#85 junction 896 1280 896 384 2496 384 outhier.RUNNING.p
w 2332 1003 -100 0 FAILED efanouts.efanouts#91.LNK2 2240 992 2496 992 outhier.TIMEOUT.p
w -166 1419 -100 0 START inhier.START.P -272 1408 0 1408 0 1472 96 1472 ebos.ebos#68.SLNK
w 1936 955 100 0 n#93 ecalcs.ecalcs#83.FLNK 1856 1056 1920 1056 1920 944 2000 944 efanouts.efanouts#91.SLNK
w 1928 1035 100 0 n#92 ecalcs.ecalcs#83.VAL 1856 1024 2048 1024 efanouts.efanouts#91.SELL
w 472 1307 100 0 n#80 ebis.ebis#79.SLNK 544 1296 448 1296 junction
w 376 1451 100 0 n#80 ebos.ebos#68.OUT 352 1440 448 1440 448 1152 352 1152 ebos.ebos#64.OUT
w 24 1515 100 0 n#76 hwin.hwin#77.in 0 1584 0 1504 96 1504 ebos.ebos#68.DOL
w 24 1227 100 0 n#72 hwin.hwin#66.in 0 1296 0 1216 96 1216 ebos.ebos#64.DOL
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos Timer Module
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/04
s 2096 -272 100 1792 Author: RRO
s 2512 -240 100 1792 flamTimer.sch
s 2016 2032 100 1792 A
s 2208 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/04
[cell use]
use hwin -192 1543 100 0 hwin#77
xform 0 -96 1584
p -189 1576 100 0 -1 val(in):1
use hwin -192 1255 100 0 hwin#66
xform 0 -96 1296
p -189 1288 100 0 -1 val(in):0
use hwin 832 1719 100 0 hwin#138
xform 0 928 1760
p 688 1760 100 0 -1 val(in):$(timeout)
use elongins 1120 1575 100 0 elongins#136
xform 0 1248 1648
p 1232 1568 100 1024 1 name:$(timer)MaxTime
use outhier 2528 384 100 0 RUNNING
xform 0 2480 384
use outhier 2528 992 100 0 TIMEOUT
xform 0 2480 992
use efanouts 2000 807 100 0 efanouts#91
xform 0 2120 960
p 2064 768 100 0 1 SELM:Mask
p 2176 800 100 1024 1 name:$(timer)Timedout
p 2272 1024 75 1280 -1 pproc(LNK1):PP
p 2272 992 75 1280 -1 pproc(LNK2):PP
use inhier -368 1120 100 0 STOP
xform 0 -272 1120
use inhier -384 1408 100 0 START
xform 0 -272 1408
use ecalcs 1568 743 100 0 ecalcs#83
xform 0 1712 1008
p 1632 704 100 0 1 CALC:(C<A)?0:3
p 1744 736 100 1024 1 name:$(timer)Timeout
use ecalcs 1024 871 100 0 ecalcs#63
xform 0 1168 1136
p 1088 832 100 0 1 CALC:C?(A+1):0
p 1088 800 100 0 1 SCAN:.1 second
p 1200 864 100 1024 1 name:$(timer)Timer
use ebis 544 1223 100 0 ebis#79
xform 0 672 1296
p 720 1216 100 1024 1 name:$(timer)Counting
use ebos 96 1383 100 0 ebos#68
xform 0 224 1472
p 160 1344 100 0 1 OMSL:closed_loop
p 272 1376 100 1024 1 name:$(timer)Enable
p 64 1504 75 1280 -1 pproc(DOL):NPP
p 352 1440 75 768 -1 pproc(OUT):PP
use ebos 96 1095 100 0 ebos#64
xform 0 224 1184
p 160 1056 100 0 1 OMSL:closed_loop
p 288 1088 100 1024 1 name:$(timer)Disable
p 352 1152 75 768 -1 pproc(OUT):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamTimer.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
