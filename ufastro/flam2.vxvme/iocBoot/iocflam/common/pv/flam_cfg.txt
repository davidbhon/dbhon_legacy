# CC configuration filename
./pv/ccconfig.txt
# motor parameters
./pv/mot_param.txt
# DC configuration filename
./pv/dcconfig.txt
# EC configuration filename
./pv/ecconfig.txt
# EC status gensub filename
./pv/env_gs.txt
# EC Heartbeat Filename
./pv/env_hb.txt
# EC Array Gensub filename
./pv/env_array.txt
# EC tempMon Gensub filename
./pv/env_tempMon.txt
# EC Pressure Status filename
./pv/env_press.txt
# Temperature controller Gensub filename
./pv/temp_cont.txt
# Cold Head controller Gensub filename
./pv/ch_param.txt
# Vacuum Gauge controller Gensub filename
./pv/vac_param.txt
# Barcode Reader controller Gensub filename
./pv/bcReader_param.txt

