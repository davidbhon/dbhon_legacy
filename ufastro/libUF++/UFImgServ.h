#if !defined(__UFImgServ_h__)
#define __UFImgServ_h__ "$Name:  $ $Id: UFImgServ.h,v 0.0 2002/06/03 17:42:18 hon beta $"
#define __UFImgServ_H__(arg) const char arg##ImgServ_h__rcsId[] = __UFImgServ_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFImgServ : public UFDaemon {
public:
  inline UFImgServ(const string& name, char** envp= 0) : UFDaemon(name, envp) {}
  inline UFImgServ(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp) {}
  inline UFImgServ(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp) {}
  inline UFImgServ(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp) {}

  inline virtual ~UFImgServ() {}

  inline virtual string description() const { return __UFImgServ_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p=0);
  // archive buff, co-add & diff buffs:
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; } 
};

#endif // __UFImgServ_h__
