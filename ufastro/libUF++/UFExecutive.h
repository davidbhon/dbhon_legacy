#if !defined(__UFExecutive_h__)
#define __UFExecutive_h__ "$Name:  $ $Id: UFExecutive.h,v 0.1 2004/09/17 19:19:10 hon Exp $"
#define __UFExecutive_H__(arg) const char arg##Executive_h__rcsId[] = __UFExecutive_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
	#include "vxWorks.h"
	#include "sockLib.h"
	#include "taskLib.h"
	#include "ioLib.h"
	#include "fioLib.h"
#endif // vx

#include "UFDaemon.h"
#include "UFRingBuff.h"
#include "UFCmdSeqAgent.h"

#include "vector"
#include "map"

// Process or Thread group Parent
// agents & Servers should sub-class this, override virtuals,
// and supplement with additional functions as necessary
class UFExecutive : public UFDaemon {
  friend class UFCmdSeqAgent;
  // support single or multi-threaded apps.
public:
  inline UFExecutive(const string& name) : UFDaemon(name) {}
  inline UFExecutive::UFExecutive(int argc, char** argv) : UFDaemon(argc, argv) {
    _daemonvec= new DaemonVec; _daemontable= new DaemonTable; _ringbuffs= new RingBuffTable;
  }
  inline UFExecutive::UFExecutive(const string& name, int argc, char** argv) : UFDaemon(name, argc, argv) { 
    _daemonvec= new DaemonVec; _daemontable= new DaemonTable; _ringbuffs= new RingBuffTable;
  }
  inline ~UFExecutive() { delete _daemonvec; delete _daemontable; delete _ringbuffs; }

  inline virtual string description() const { return __UFExecutive_h__; }
  // is this necessary?
  //inline static const UFExecutive* getInstance() { return _instance; }
  
  // the true "main" entry point, signature must support use in pthread:
  virtual void* exec(void* p= 0);

  // kill children or cancel threads
  // stub it for now:
  inline void killDaemons() { _daemonvec->clear(); _daemontable->clear(); }
  // start or restart one or some or all daemons
  bool startDaemons();

  // delegated to individual daemons?
  virtual int createSharedResources() {
    int n= 0;
    for( int i= 0; i < (int) _daemonvec->size(); i++ ) {
      UFDaemon* dp = (UFDaemon*)(*_daemonvec)[i];
      n += dp->createSharedResources();
    }
    return n;
  }

  virtual int deleteSharedResources() {
    int n= 0;
    for( int i= 0; i < (int) _daemonvec->size(); i++ ) {
      UFDaemon* dp = (UFDaemon*)(*_daemonvec)[i];
      n += dp->deleteSharedResources();
    }
    return n;
  }

  // for stand-alone use
  static int main(int argc, char** argv);

protected:
  static void _sigHandler(int signum);

   // daemon vector holds all daemon thread or pid Ids
  static UFDaemon::DaemonVec* _daemonvec;  
   // daemon table holds all daemon thread or pid Ids
  static UFDaemon::DaemonTable* _daemontable;  

  // ringbuff table holds all ring buffs
  static UFDaemon::RingBuffTable* _ringbuffs;
};

#endif // __UFExecutive_h__
