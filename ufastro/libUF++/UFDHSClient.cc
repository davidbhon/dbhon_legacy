#if !defined(__UFDHSClient_cc__)
#define __UFDHSClient_cc__ "$Name:  $ $Id: UFDHSClient.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDHSClient_cc__;

#include "UFDHSClient.h"
__UFDHSClient_H__(__UFDHSClient_cc);

#include "time.h"

// default behavior of virtuals:
string UFDHSClient::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFDHSClient::exec(void* p) {
  // this is the main function for the Image Server DHSClient
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFDHSClient::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );

  return p;
}

#endif // __UFDHSClient_cc__
