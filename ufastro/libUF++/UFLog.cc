#if !defined(__UFLog_cc__)
#define __UFLog_cc__ "$Name:  $ $Id: UFLog.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLog_cc__;

#include "UFLog.h"
__UFLog_H__(__UFLog_cc);

#include "time.h"

// default behavior of virtuals:
string UFLog::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFLog::exec(void* p) {
  // this is the main function for the Image Server Log
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFLog::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFLog_cc__
