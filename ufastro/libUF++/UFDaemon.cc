#if !defined(__UFDaemon_cc__)
#define __UFDaemon_cc__ "$Name:  $ $Id: UFDaemon.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDaemon_cc__;

#include "UFDaemon.h"
__UFDaemon_H__(__UFDaemon_cc);

#include "unistd.h"
#include "time.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

// static attributes
UFRuntime::Argv* UFDaemon::_args= 0;
char** UFDaemon::_envp= 0;

UFDaemon::UFDaemon(const string& name,
		   char** envp) : UFRuntime(), _name(name), _usage(" [-mt] [-thread] ") {
  _envp = envp;
}

UFDaemon::UFDaemon(int argc, char** argv,
		   char** envp) : UFRuntime(), _name("Anonymous"), _usage(" [-mt] [-thread] ") {
  _envp = envp;
  _args = new UFRuntime::Argv;
  int cnt = argVec(argc, argv, *_args);
  if( cnt != argc ) clog<<"UFDaemon> ? error parsing command line."<<endl;
  if( argVal("-mt", *_args) != "false"  || argVal("-threads", *_args) != "false" )
    UFPosixRuntime::_threaded = true;
  if( argVal("-dmn", *_args) != "false" ) daemonize();
}

UFDaemon::UFDaemon(const string& name, int argc,
		  char** argv, char** envp) : UFRuntime(), _name(name), _usage(" [-mt] [-threads] ") {
  _envp = envp;
  _args = new UFRuntime::Argv;
  int cnt = argVec(argc, argv, *_args);
  if( cnt != argc ) clog<<"UFDaemon> ? bad command line."<<endl;
  if( argVal("-mt", *_args) != "false"  || argVal("-threads", *_args) != "false" )
    UFPosixRuntime::_threaded = true;
  if( argVal("-dmn", *_args) != "false" ) daemonize();
}

UFDaemon::UFDaemon(const string& name,
		   UFRuntime::Argv* args,
		   char** envp) : UFRuntime(), _name(name),  _usage(" [-mt] [-threads] ") {
  _envp = envp;
  _args = args;
  if( argVal("-mt", *_args) != "false"  || argVal("-threads", *_args) != "false" )
    UFPosixRuntime::_threaded = true;
  if( argVal("-dmn", *_args) != "false" ) daemonize();
}
    

// UFArchive definitions for use by any daemon:
int UFArchive::applySort(const UFInts& sortLUT, const UFProtocol& in, UFProtocol& out) {
  int type = in.typeId();

  if( type != out.typeId() )
    return -1;

  int nval = in.numVals();
  if( nval != out.numVals() )
    return -2;

  int i;
  switch(type) {
  case UFProtocol::_Bytes_ :
    for( i = 0; i < nval; ++i ) {
      char* inval = (char*) in.valData(i);
      int* ilut = (int*) sortLUT.valData(i);
      char* outval = (char*) out.valData(*ilut);
      *outval = *inval;
    }
    break;

  case UFProtocol::_Shorts_ :
    for( i = 0; i < nval; ++i ) {
      short* inval = (short*) in.valData(i);
      int* ilut = (int*) sortLUT.valData(i);
      short* outval = (short*) out.valData(*ilut);
      *outval = *inval;
    }
    break;

  case UFProtocol::_Floats_ :
    for( i = 0; i < nval; ++i ) {
      float* inval = (float*) in.valData(i);
      int* ilut = (int*) sortLUT.valData(i);
      float* outval = (float*) out.valData(*ilut);
      *outval = *inval;
    }
    break;

  case UFProtocol::_Ints_ :
    // default 

  default:
    for( i = 0; i < nval; ++i ) {
      int* inval = (int*) in.valData(i);
      int* ilut = (int*) sortLUT.valData(i);
      int* outval = (int*) out.valData(*ilut);
      *outval = *inval;
    }

    break;
  }
  return nval;
}

// for use by any daemon for iterative file output --
// with sime-automated file-naming & optional fits header
int UFArchive::writeFile(UFArchive& arc, UFProtocol* ufp, int numvals, int filecnt, int modulo) {
  pid_t mypid = getpid();
  strstream s;
  s << arc.outfile;
  if( filecnt > 0 ) { // use counter in name
  if( filecnt < 10 )
    s <<".00000"<<filecnt<<ends;
  else if( filecnt < 100 )
    s <<".0000"<<filecnt<<ends;
  else if( filecnt < 1000 )
    s <<".000"<<filecnt<<ends;
  else if( filecnt < 10000 )
    s <<".00"<<filecnt<<ends;
  else if( filecnt < 100000 )
    s <<".0"<<filecnt<<ends;
  else
    s <<"."<<filecnt<<ends;
  }
  char* filenm = s.str();
  /*
  struct stat st;
  int file_exists = ::fstat(arc.fd, &st);
  */
  if( (filecnt != 0) && (filecnt % modulo == 0)  ) // creat or append
    arc.fd = ::open(filenm, O_RDWR|O_CREAT, 0644);
  else
    arc.fd = ::open(filenm, O_RDWR|O_APPEND|O_CREAT, 0644);

  clog<<"UFArchive::> ("<<mypid<<") Opened file: "<<filenm<<" fd= "<<arc.fd<<endl;

  unsigned char* buf = (unsigned char*) ufp->valData();

  int nb= 0;
  if( !arc.fitsHdr.empty() ) {
    nb = ::write(arc.fd, arc.fitsHdr.c_str(), arc.fitsHdr.length());
    clog<<"UFArchive::> ("<<mypid<<") wrote fitsHdr to "<<filenm<<endl;
  }
  if( numvals <= 0 ) // default is to access the entire buffer:
    numvals = ufp->numVals();

  nb += ::write(arc.fd, buf, numvals*ufp->valSize());

  clog<<"UFArchive::> ("<<mypid<<") wrote nb= "<<nb<<" "<<ufp->name()<<" to "<<filenm<<endl;
  delete filenm;

  // always close
  return ::close(arc.fd);
}

// this should be the very first thing invoked by 
// the application/server main, at initialization
int UFDaemon::daemonize() {
  pid_t pid = fork();

  if( pid < 0 ) return -1; // fork failed
  if( pid > 0 ) ::exit(0); // parent is done

  ::setsid(); // child process becomes session leader
  ::chdir("/"); // allow any other partion/mount point to be unmounted
  ::umask(0); // clear file mode creation mask

  return 0; // continue execution... 
}
  
string UFDaemon::pathToSelf() {
  string path;
  if( _args ) {
    string self = (*_args)[0];
    path = UFRuntime::pathToSelf(self);
  }
  return path;
}


// default behavior of virtuals:
string UFDaemon::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFDaemon::exec(void* p) {
  // this is the main function for the Image Server Daemon
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFDaemon::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

// static convenience func:
void* UFDaemon::execThread(void* p) {
  UFDaemon* instance = static_cast<UFDaemon*>(p);
  if( instance == 0 ) {
    clog<<"UFDaemon::execThread> cast void* to UFDaemon*failed! can't exec."<<endl;
    return (void*)0;
  }

  return instance->exec(p);
}

#endif // __UFDaemon_cc__
