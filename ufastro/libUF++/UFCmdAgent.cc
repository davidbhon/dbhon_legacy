#if !defined(__UFCmdAgent_cc__)
#define __UFCmdAgent_cc__ "$Name:  $ $Id: UFCmdAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCmdAgent_cc__;

#include "UFCmdAgent.h"
__UFCmdAgent_H__(__UFCmdAgent_cc);

#include "time.h"

// default behavior of virtuals:
string UFCmdAgent::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFCmdAgent::exec(void* p) {
  // this is the main function for the Image Server CmdAgent
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFCmdAgent::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFCmdAgent_cc__
