#if !defined(__UFImgPreProc_cc__)
#define __UFImgPreProc_cc__ "$Name:  $ $Id: UFImgPreProc.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFImgPreProc_cc__;

#include "UFImgPreProc.h"
__UFImgPreProc_H__(__UFImgPreProc_cc);

#include "UFFrameGrab.h"
#include "UFEdtDMA.h"

#include "time.h"

// statics:
UFRingBuff* UFImgPreProc::_theArchiveRingBuff= 0;
vector< UFRingBuff* > UFImgPreProc::_theImgRingBuffs;

// default behavior of virtuals:
string UFImgPreProc::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFImgPreProc::exec(void* p) {
  // this is the main function for the Image Server ImgPreProc
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process

  // 
  do {
    clog<<"UFImgPreProc::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

// archive buff, co-add & diff buffs:
int UFImgPreProc::createSharedResources() {
  vector< string > ringnames, bufnames;
  int numbuf = frameBufNames(ringnames); // use default Signal Image frame buff name
  UFEdtDMA::Config* edt = new UFEdtDMA::Config;
  edt = UFEdtDMA::getConfig(edt); // returns null if unable to find card
  int bufsz = UFFrameGrab::frameSize(*edt); // in bytes

  if( threaded() ) {
    for( int i = 0; i < numbuf; i++ ) {
      UFRingBuff* rb = new UFHeapRingBuff(ringnames[i]);
      rb->init(bufnames, bufsz); // defaults to 4 element ring-buff if bufnames is empty 
      _theImgRingBuffs.push_back(rb);
    }
    _theArchiveRingBuff= _theImgRingBuffs[0];
  }
  else {
    for( int i = 0; i < (int) numbuf; i++ ) {
      UFRingBuff* rb = new UFHeapRingBuff(ringnames[i]);
      rb->init(bufnames, bufsz);
      _theImgRingBuffs.push_back(rb);
    }
    _theArchiveRingBuff= _theImgRingBuffs[0];
  }

  return 1+_theImgRingBuffs.size();
}

int UFImgPreProc::deleteSharedResources() {
  for( int i = 0; i < (int) _theImgRingBuffs.size(); i++ )
    delete _theImgRingBuffs[i];

  _theImgRingBuffs.clear();
  _theArchiveRingBuff= 0;

  return 0;
} 

#endif // __UFImgPreProc_cc__
