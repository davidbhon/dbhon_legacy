#if !defined(__UFDeviceConfig_h__)
#define __UFDeviceConfig_h__ "$Name:  $ $Id: UFDeviceConfig.h,v 0.5 2004/09/17 19:19:09 hon Exp $"
#define __UFDeviceConfig_H__(arg) const char arg##UFDeviceConfig_h__rcsId[] = __UFDeviceConfig_h__;

#include "UFTermServ.h"
#include "UFStrings.h"
#include "UFObsConfig.h"
class UFDeviceAgent; // forward declatation to eliminate nested include

class UFDeviceConfig {
public:
  UFDeviceConfig(const string& name= "UnknownDevice@DefaultConfig");
  UFDeviceConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFDeviceConfig() {}

  virtual UFTermServ* connect(const string& host= "", int port= 0);

  // distinguish between query requests/commands (must reply)
  virtual vector< string >& UFDeviceConfig::queryCmds();
  // and actions (optional reply)
  virtual vector< string >& UFDeviceConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);
  // key/value cmds (name/impl)
  inline virtual int validCmd(const string& name, const string& c) { return 1; }

  // all device trasanctions have termination character(s)
  virtual string terminator();
  // and possible prefix character(s).
  virtual string prefix();

  // return (current) configuration status in hash format (key == value):
  // the default signature provides status from with the device config,
  // and optionally appends status from the device agent, if available
  virtual UFStrings* status(UFDeviceAgent* da= 0);
  // return configuration status in FITS format:
  virtual UFStrings* statusFITS(UFDeviceAgent* da= 0);

  // this sig. allows supplenting the status info. in the config
  // object with status from both the dev. agent and an obs. conf. 
  virtual UFStrings* status(UFDeviceAgent* da, UFObsConfig* oc);
  // return configuration status in FITS format:
  virtual UFStrings* statusFITS(UFDeviceAgent* da, UFObsConfig* oc);

  // is the device access via a terminal-server (annex or iocomm or perle)?
  // if so, what are the connection attributes
  virtual bool termServ(int& port, string& host);

  // public attributes
  static UFTermServ* _devIO;
  int _tsport;
  string _tshost;

  static bool _verbose;

protected:
  string _name;
  static vector< string > _queries;
  static vector< string > _actions;
};

#endif // __UFDeviceConfig_h__
