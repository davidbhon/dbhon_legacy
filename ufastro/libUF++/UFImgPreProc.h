#if !defined(__UFImgPreProc_h__)
#define __UFImgPreProc_h__ "$Name:  $ $Id: UFImgPreProc.h,v 0.1 2004/09/17 19:19:11 hon Exp $"
#define __UFImgPreProc_H__(arg) const char arg##ImgPreProc_h__rcsId[] = __UFImgPreProc_h__;

#include "UFDaemon.h"
#include "UFObsConfig.h"
#include "string"

class UFImgPreProc : public UFDaemon {
protected:
  vector< string >* _bufNames; // image diff & co-add buffernames
  UFObsConfig* _obs;

public:
  static UFRingBuff* _theArchiveRingBuff;
  static vector< UFRingBuff* > _theImgRingBuffs;

  inline UFImgPreProc(const string& name) : UFDaemon(name), _bufNames(0), _obs(0) {}
  inline UFImgPreProc(int argc, char** argv) : UFDaemon(argc, argv), _bufNames(0), _obs(0) {}
  inline UFImgPreProc(const string& name, 
			 int argc, char** argv) : UFDaemon(name, argc, argv), _bufNames(0), _obs(0) {}
  inline UFImgPreProc(const string& name, const UFRuntime::Argv* args) : UFDaemon(name, args), _bufNames(0), _obs(0) {}
  inline virtual ~UFImgPreProc() {}

  inline virtual string description() const { return __UFImgPreProc_h__; }

  virtual string heartbeat();

  inline virtual int setObservation(const UFObsConfig& obs) { 
    delete _obs; _obs = new UFObsConfig(obs); return _obs->elements();
 }

  // standard image frmae buffer names, default supports chop-nods:
  inline virtual int frameBufNames(vector< string >& names) {
    if( _bufNames == 0 ) {
      _bufNames= new vector< string >;
      _bufNames->push_back("Sig1"); _bufNames->push_back("Ref1"); _bufNames->push_back("Dif1"); //3
      _bufNames->push_back("Sig2"); _bufNames->push_back("Ref2"); _bufNames->push_back("Dif2"); //6
      _bufNames->push_back("Sig1Accum"); _bufNames->push_back("Ref1Accum"); _bufNames->push_back("Dif1Accum"); //9
      _bufNames->push_back("Sig2Accum"); _bufNames->push_back("Ref2Accum"); _bufNames->push_back("Dif2Accum"); //12
      _bufNames->push_back("Sig"); _bufNames->push_back("SigAccum"); //14
    }
    names.clear();
    for( int i = 0; i < (int) _bufNames->size(); i++ )
      names.push_back((*_bufNames)[i]);

    return  (int) names.size();
  }

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // archive buff, co-add & diff buffs:
  virtual int createSharedResources();
  virtual int deleteSharedResources(); 
};

#endif // __UFImgPreProc_h__
