#if !defined(__UFPSem_cc__)
#define __UFPSem_cc__ "$Name:  $ $Id: UFPSem.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPSem_cc__;

#include "UFPSem.h"
__UFPSem_H__(__UFPSem_cc);

union semun { int val; struct semid_ds *buf;
	      unsigned short* array; struct seminfo* __buf; };

#include "sys/types.h"
#include "sys/ipc.h"
#include "sys/sem.h"
#include "stdlib.h"


bool UFPSem::_verbose = false;

sem_t* UFPSem::open(const string& name, int flag, mode_t mode, int val) {
  int ret= 0;
  ret = ::umask(0);
  //mode_t md = S_IRUSR|S_IRGRP|S_IROTH|S_IWUSR|S_IWGRP|S_IWOTH;
  sem_t* sem = 0;
#if !defined(SYSVSEM)
  if( _verbose )
    clog<<"UFPSem::open> name: "<<name<<", flag:"<<flag<<endl;
  if( flag == 0 ) { // attach
    sem = ::sem_open(name.c_str(), 0);
  }
  else { // create & attach
    sem = ::sem_open(name.c_str(), flag, mode, val);
    /*
    if( sem != 0 && sem != SEM_FAILED ) {
      ::sem_post(sem); // test increment
      ::sem_trywait(sem); // test decrement
      ::sem_close(sem);
      sem = ::sem_open(name.c_str(), 0);
    }
    */
  }
#else
// implement these via simple file system logic under "/usr/tmp/ufpsem/name.val"
  string semfile = "/usr/tmp/.UFPSem";
  if( name.find("/") != 0 ) semfile += "/";
  ret = ::mkdir(semfile.c_str(), 0777);
  semfile += name;

  sem = new sem_t; memset(sem, 0, sizeof(sem_t));
#if defined(LINUX) // creat file for ftok
  //sem->__sem_lock.__status = ::open(semfile.c_str(), O_CREAT, mode); // store fd here
  sem->__sem_lock.__status = ::creat(semfile.c_str(), mode); // store fd here
  if( sem->__sem_lock.__status > 0 && sem->__sem_lock.__status < FOPEN_MAX )
    ret = ::close(sem->__sem_lock.__status);
#else // creat file for ftok (test Linux logic on Solaris)
  //sem->sem_type = ::open(semfile.c_str(), O_CREAT, mode); // store fd here
  sem->sem_type = ::creat(semfile.c_str(), mode); // store fd here
  if( sem->sem_type > 0 && sem->sem_type < FOPEN_MAX )
    ret = ::close(sem->sem_type);
#endif
  //mode_t md = S_IRUSR|S_IRGRP|S_IROTH|S_IWUSR|S_IWGRP|S_IWOTH;
  key_t key = ftok(semfile.c_str(), 0);
  
  if( flag == O_CREAT ) {
    flag = IPC_CREAT|mode;
  }
  else if( flag == (O_CREAT|O_EXCL) ) {
    flag = IPC_CREAT|IPC_EXCL|mode;
  }
  else { // attach only
    flag = mode;
  }
  ret = ::semget(key, 1, flag);
  if( ret < 0 ) {
    if( _verbose )
      clog<<"UFPSem::open> semget failed: "<<strerror(errno)<<endl;
    delete sem; sem = 0;
    return sem;
  }
#if defined(LINUX)
  sem->__sem_value = ret; // store SYSV sem Id in value field
#else
  sem->sem_count = ret; // store SYSV sem Id in value field
#endif
  if( flag != (int)mode ) { // this is a create, need to init. value
    union semun arg; arg.val = val;
#if defined(LINUX)
    ret = ::semctl(sem->__sem_value, 0, SETVAL, arg); 
#else
    ret = ::semctl(sem->sem_count, 0, SETVAL, arg); 
#endif
    if( ret < 0 ) {
      if( _verbose )
	clog<<"UFPSem::open> semctl failed: "<<strerror(errno)<<endl;
      delete sem; sem = 0;
      return sem;
    }
  }
#endif
  if( _verbose ) {
    //ret = getvalue(sem, val);
    clog<<"UFPSem::open>"<<name<<", val= "<<val<<endl;
  }
  return sem;
}

// close descriptor for named semaphore SEM.
int UFPSem::close(sem_t*& sem) {
  int ret= 0;
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

#if !defined(SYSVSEM)
  ret = ::sem_close(sem);
#else
#if defined(LINUX)
  if( sem->__sem_lock.__status > 0 && sem->__sem_lock.__status < FOPEN_MAX )
    ret = ::close(sem->__sem_lock.__status);
#else
  if( sem->sem_type > 0 && sem->sem_type < FOPEN_MAX )
    ret = ::close(sem->sem_type);
#endif
  delete sem;
#endif

  sem = 0;
  return ret;
}

// remove named semaphore.
int UFPSem::unlink(const string& name) {
  int ret= -1;
#if defined(LINUX) || defined(SOLARIS)
#if !defined(SYSVSEM)
  ret = ::sem_unlink(name.c_str());
#else
  sem_t* sem = UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

  semid_ds seminfo;
  union semun s; s.buf = &seminfo;
#if defined(LINUX)
  ret = ::semctl(sem->__sem_value, 0, IPC_STAT, s); 
  ret = ::semctl(sem->__sem_value, 0, IPC_RMID, s); 
#else
  ret = ::semctl(sem->sem_count, 0, IPC_STAT, s); 
  ret = ::semctl(sem->sem_count, 0, IPC_RMID, s); 
#endif
  delete sem;
#endif
#endif
  return ret;
}

// post (increment) SEM. 
int UFPSem::post(sem_t* sem) { // increment
  int ret= -1;
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

#if !defined(SYSVSEM)
  ret = ::sem_post(sem);
#else
  union semun arg;
  ret = getvalue(sem, arg.val);
  if( ret < 0 )
   return ret;
  arg.val++;
#if defined(LINUX)
  ret = ::semctl(sem->__sem_value, 0, SETVAL, arg); // set incremented value
#else
  ret = ::semctl(sem->sem_count, 0, SETVAL, arg); // set incremented value
#endif
#endif
  if( _verbose ) {
    int val= -1;
    ret = getvalue(sem, val);
    clog<<"UFPSem::post> incremented: "<<val<<endl;
  }

  return ret;
}

// wait for SEM being posted.
int UFPSem::wait(sem_t* sem) { // decrement
  int ret= -1;
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

#if !defined(SYSVSEM)
  ret = ::sem_wait(sem);
#else
  union semun arg;
  ret = getvalue(sem, arg.val);
  if( ret < 0 )
   return ret;
  arg.val--;
  if( arg.val <= 0 ) arg.val = 0;
#if defined(LINUX)
  ret = ::semctl(sem->__sem_value, 0, SETVAL, arg); // set decremented value
#else
  ret = ::semctl(sem->sem_count, 0, SETVAL, arg); // set incremented value
#endif
#endif
  if( _verbose ) {
    int val= -1;
    ret = getvalue(sem, val);
    clog<<"UFPSem::wait> decremented: "<<val<<endl;
  }

  return ret;
}

// Test whether SEM is posted.
int UFPSem::trywait(sem_t* sem) {
  int ret= -1;
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

#if !defined(SYSVSEM)
  ret = ::sem_trywait(sem);
#else
  union semun arg;
  ret = getvalue(sem, arg.val);
  if( ret < 0 )
   return ret;
  arg.val--;
  if( arg.val <= 0 ) arg.val = 0;
#if defined(LINUX)
  ret = ::semctl(sem->__sem_value, 0, SETVAL, arg); // set decremented value
#else
  ret = ::semctl(sem->sem_count, 0, SETVAL, arg); // set incremented value
#endif
#endif
  if( _verbose ) {
    int val= -1;
    ret = getvalue(sem, val);
    clog<<"UFPSem::trywait> decremented: "<<val<<endl;
  }

  return ret;
}

// get current value of SEM and store it in VAL. 
int UFPSem::getvalue(sem_t* sem, int& val) {
  int ret= -1;
  if( sem == 0 || sem == SEM_FAILED )
    return ret;

#if !defined(SYSVSEM)
  if( _verbose )
    clog<<"UFPSem::getvalue> sem: "<<(size_t) sem<<ends;
  ret = ::sem_getvalue(sem, &val);
  if( _verbose )
    clog<<"UFPSem::getvalue> sem: "<<(size_t) sem<<", val: "<<val<<endl;
#else
  union semun arg;
  arg.val = 0;
#if defined(LINUX)
  ret = ::semctl(sem->__sem_value, 0, GETVAL, arg); // -1 is return if this fails!
#else
  ret = ::semctl(sem->sem_count, 0, GETVAL, arg); // set incremented value
#endif
  if( ret < 0 )
    clog<<"UFPSem::getvalue> failed to get semvalue, "<<strerror(errno)<<endl;
  val = ret; // this is clearly a problem; negative values are not allowed for SYS5 sems.
#endif

  return ret;
}


// convenience funcs. use SEM name:
// post SEM. 
int UFPSem::post(const string& name) { // increment
  sem_t* sem = UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return -1;
  if( _verbose ) clog<<"("<<name<<") "<<ends;
  int ret = UFPSem::post(sem);
  UFPSem::close(sem);
  return ret;
}  

// wait for SEM being posted.
int UFPSem::wait(const string& name) { // decrement
  sem_t* sem =  UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return -1;
  if( _verbose ) clog<<"("<<name<<") "<<ends;
  int ret = UFPSem::wait(sem);
  UFPSem::close(sem);
  return ret;
}  

// test whether SEM is posted.
int UFPSem::trywait(const string& name) {
  sem_t* sem =  UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return -1;
  if( _verbose ) clog<<"("<<name<<") "<<ends;
  int ret = UFPSem::trywait(sem);
  UFPSem::close(sem);
  return ret;
}  

// get current value of SEM and store it in VAL. 
int UFPSem::getvalue(const string& name, int& val) {
  sem_t* sem =  UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return -1;
  if( _verbose ) clog<<"("<<name<<") "<<ends;
  int ret = UFPSem::getvalue(sem, val);
  UFPSem::close(sem);
  return ret;
}  

// get current value of SEM and store it in VAL. 
int UFPSem::setvalue(const string& name, int val) {
  if( val < 0 )
    return -1;

  sem_t* sem =  UFPSem::open(name, 0);
  if( sem == 0 || sem == SEM_FAILED )
    return -1;
  if( _verbose ) clog<<"("<<name<<") "<<ends;
  int semval, ret = UFPSem::getvalue(sem, semval);
  int cnt= 16;
  while( cnt > 0 && semval < val ) {
    ret = post(sem);
    if( ret < 0 ) {
      --cnt;
      UFPosixRuntime::sleep(0.1);
    }
    ret = UFPSem::getvalue(sem, semval);
  }
  while( cnt > 0 && semval > val ) {
    ret = trywait(sem);
    if( ret < 0 ) {
      --cnt;
      UFPosixRuntime::sleep(0.1);
    }
    ret = UFPSem::getvalue(sem, semval);
  }
  UFPSem::close(sem);
  return ret;
}  

#include "UFPosixRuntime.h"
int UFPSem::main(int argc, char** argv, char** envp) {
  string semname = "/testUFPSem";
  string usage = "ufpsem [-h[elp]] [-d[elete]] [[-cnt] +/-n] (no args does get, +/-n increments or decrement by n) [-n[ame] (use name interface)";

  bool done = false;
  int val= INT_MAX;
  // preserv old cmd-line option: (ufpsem [+/-]val)
  if( argc > 1 && (isdigit(argv[1][0]) || isdigit(argv[1][1])) ) // [+/-] val
    val = atoi(argv[1]);

  UFRuntime::Argv args;
  UFRuntime::argVec(argc, argv,args);
  string arg = UFRuntime::argVal("-h", args);
  if( arg != "false" ) {
    clog<<usage<<endl;
    return 0;
  }
  arg = UFRuntime::argVal("-help", args);
  if( arg != "false" ) {
    clog<<usage<<endl;
    return 0;
  }

  arg = UFRuntime::argVal("-v", args);
  if( arg != "false" ) {
    UFPSem::_verbose = true;
  }

  arg = UFRuntime::argVal("-d", args);
  if( arg != "false" ) {
    return UFPosixRuntime::semDelete(semname);
  }

  arg = UFRuntime::argVal("-cnt", args);
  if( arg != "false" ) {
    val = atoi(arg.c_str());
    clog<<"val: "<<arg<<" = "<<val<<endl;
  }

  string host = UFRuntime::hostname();
  // try attach, create if necessary
  sem_t* sem = UFPosixRuntime::semAttach(semname);
  if( val == INT_MAX && (sem == 0 || sem == SEM_FAILED) ) { // this is a getvalue (only) request that 
    // fails if sem does not request
    clog<<"unable to attach: "<<semname<<endl;
    return 1;
  }
  if( val != INT_MAX && (sem == 0 || sem == SEM_FAILED) ) {
    // only create it if this is a set request and it does not exist
    sem = UFPosixRuntime::semCreate(semname, abs(val));
    if( val >= 0 ) done = true;
  }
  if( sem == 0 || sem == SEM_FAILED) {
    clog<<"unable to attach or create: "<<semname<<endl;
    return 1;
  }
  UFPSem::close(sem);
  // ok sem exists, continue test:

  int semval= -1;
  int ret = UFPSem::getvalue(semname, semval);
  if( ret < 0 ) {
    clog<<"unable to getvalue of "<<host<<":"<<semname<<endl;
    return 1;
  }
  clog<<host<<":"<<semname<<"= "<<semval<<endl;
  if( done || val == INT_MAX ) { // got current value, no new value requested...
    return 0;
  }

  arg = UFRuntime::argVal("-n", args);
  if( arg != "false" ) { // use the name interface..
    if( val > 0 ) {
      for( int i = 0; i < val; ++i ) {
        ret = UFPSem::post(semname);
        if( ret < 0 ) {
          clog<<"unable to post: "<<semname<<endl;
          return 2;
        }
        ret = UFPSem::getvalue(semname, semval);
        if( ret < 0 ) {
          clog<<"unable to getvalue of: "<<semname<<endl;
          return 3;
        }
        clog<<semname<<": "<<semval<<endl;
        UFPosixRuntime::sleep(0.01);
      }
      return 0;
    }
    else if( val < 0 ) {
      for( int i = val; i < 0; ++i ) {
        ret = UFPSem::wait(semname);
        if( ret < 0 ) {
          clog<<"unable to wait: "<<semname<<endl;
          return 2;
        }
        ret = UFPSem::getvalue(semname, semval);
        if( ret < 0 ) {
          clog<<"unable to getvalue of: "<<semname<<endl;
          return 3;
        }
        clog<<semname<<": "<<semval<<endl;
        UFPosixRuntime::sleep(0.01);
      }
      return 0;
    }
  }

  // if we get here, assume sem has been created:
  sem = UFPSem::open(semname, 0);
  if( val > 0 ) {
    for( int i = 0; i < val; ++i ) {
      ret = UFPSem::post(sem);
      if( ret < 0 ) {
        clog<<"unable to post: "<<semname<<endl;
        return 2;
      }
      ret = UFPSem::getvalue(sem, semval);
      if( ret < 0 ) {
        clog<<"unable to getvalue of: "<<semname<<endl;
        return 3;
      }
      clog<<semname<<": "<<semval<<endl;
      UFPosixRuntime::sleep(0.01);
    }
  }
  else if( val < 0 ) {
    for( int i = val; i < 0; ++i ) {
      ret = UFPSem::wait(sem);
      if( ret < 0 ) {
        clog<<"unable to wait: "<<semname<<endl;
        return 2;
      }
      ret = UFPSem::getvalue(sem, semval);
      if( ret < 0 ) {
        clog<<"unable to getvalue of: "<<semname<<endl;
        return 3;
      }
      clog<<semname<<": "<<semval<<endl;
      UFPosixRuntime::sleep(0.01);
    }
  }

  return UFPSem::close(sem);
}

#endif // __UFPSem_cc__
