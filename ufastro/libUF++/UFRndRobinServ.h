#if !defined(__UFRndRobinServ_h__)
#define __UFRndRobinServ_h__ "$Name:  $ $Id: UFRndRobinServ.h,v 0.7 2006/02/17 23:35:30 hon Exp $"
#define __UFRndRobinServ_H__(arg) const char arg##UFRndRobinServ_h__rcsId[] = __UFRndRobinServh__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFDaemon.h"
#include "UFSocket.h"
#include "UFServerSocket.h"
#include "UFProtocol.h"
#include "UFStrings.h"

using std::map;

class UFRndRobinServ: public UFDaemon {
public:
  // req table key: client socket, value: protocol object request
  typedef std::map< UFSocket*, UFProtocol* > MsgTable;

  // globally accessed by sigHandler:
  static bool _lost_connection;
  static bool _shutdown;
  static bool _childdeath;
  static bool _verbose;

  // listener:
  static UFServerSocket _theServer;
  // client table key: client name string, value: client socket
  static UFSocket::ConnectTable* _theConnections;
  // allow partially threaded (of sends) service:
  static pthread_mutex_t* _theConMutex;
  static void threadedServ();

  // return all keys in the ConnectTable (which should be connectionID information)
  static int connectList(const string& name, const UFSocket::ConnectTable& ct, vector<string>& vs);
  // in a newly allocated UFStrings object
  static UFStrings* connectList(const string& name, const UFSocket::ConnectTable& ct);

  static int main(int argc, char** argv, char** envp= 0);
  static void sighandlerDefault(int sig);
  static void shutdown();
  static int lostConnection(const UFServerSocket& serv);
  //virtual int lostConnection(const UFServerSocket& serv);
  // delete protocol alocations & clear table:
  static int clearAllMsgs(UFRndRobinServ::MsgTable& mtbl);

  UFRndRobinServ(int argc= 0, char** argv= 0, char** envp= 0);
  UFRndRobinServ(const string& name, int argc= 0, char** argv= 0, char** envp= 0);
  UFRndRobinServ(const string& name, UFRuntime::Argv* args, char** envp= 0);
  inline virtual ~UFRndRobinServ() {}

  // start listening on port
  virtual void startup();

  // convenience func. uses UFSocket::available() 
  virtual int pendingReqs(vector<UFSocket*>& clients);
  UFSocket* acceptOn(UFServerSocket& server);
  UFSocket* accept();

  // sub-classes should override one or more of the following:
  virtual int options(string& servlist);

  // handle new client connection;
  // default behavior is recv a UFTimeStamp, and echo it back.
  // must return client's name:
  virtual string newClient(UFSocket* client, const string& agentname);
  virtual string greetClient(UFSocket* client, const string& serviceName);

  // send identical message to all clients:
  virtual int notifyAll(const UFProtocol& notice) const;
  // send identical message to all status clients:
  virtual int notifyAllStatus(const UFProtocol& notice) const;
  // send each client a unique message:
  virtual int notifyClients(const UFRndRobinServ::MsgTable& notices) const;

  //#########################
  // Added by amarin to include new funtions from cancam ufmotord
  // send message to client identified by notice.name():
  virtual int notifyStatusClient(const UFProtocol& notice);
  //##########################

  // recv specific client requests, uses createFrom to allocate protocol msgs;
  // default behavior is probobably sufficient:
  virtual int recvReqs(const vector<UFSocket*>& clients);

  // support accept/reject acknowledgement
  virtual bool acceptable(UFProtocol* req, string& a);
  virtual int acknowledge(UFSocket* soc, UFProtocol* req, const string& a);

  // default service is good-old-fashion ping/echo:
  int echoReq(UFRndRobinServ::MsgTable& reqtbl);

  // service each client with one or more "current/immediate"
  // unique message(s), deallocates protocol msgs;
  // default behavior simply echos back (modified) request:
  virtual int servImmed();

  // service each client's queued request(s) that have become "current",
  // deallocates protocol msgs;
  // default behavior simply echos back (modified) request:
  virtual int servQueued();

  // allow sub-class to perform any ancillary or supplemental work
  // on each iteration of main event loop:
  virtual void* ancillary(void *p= 0);

  // allow sub-class to do whatever else it wants/needs 
  // before going to sleep:
  virtual void hibernate();

  // override these UFDaemon virtuals:
  // exec event loop calls all the above virtuals:
  virtual void* exec(void* p);
  inline virtual string description() const { return name(); }
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }

protected:
  // command-line option to acknowledge all request with accept/reject UFStrings reply
  bool _ack;
  // ancillary or supplemental event-loop logic:
  void* _ancil;
  // security: host.allow:
  static string _allowsubnet;
  static const string _geminiCtrl;
  static const string _geminiData;
  static const string _ufirastronet;

  UFRndRobinServ::MsgTable _immed; // reqs. marked with "current/past" timestamps
  UFRndRobinServ::MsgTable _queued; // reqs. marked with "future" timestamps

  // (support optional) replication service -- send all data to this client
  static UFSocket* _replicate;

  // check subnet for something recognizable
  static bool _allowableClient(UFSocket* cs, const string& subnet = "192.168");
};

#endif // __UFRndRobinServ_h__
      
