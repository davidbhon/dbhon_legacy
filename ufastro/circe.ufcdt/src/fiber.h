#ifndef _fiber_h
#define _fiber_h

#ifdef INCLUDE_EDT
#include <libedt.h>
#endif

#define FOI_NWORDS 5    /* # of words in FOI packet for phgus213x.ttf and following */
#define FOI_NBYTES FOI_NWORDS*sizeof(int) 
#define FOI_SIGNAL 0
#define FOI_IMAGE  1

typedef union foi_packet {
   unsigned char c[FOI_NBYTES];
   unsigned int i[FOI_NWORDS];
}foi_packet;

/* Global variables */
#ifdef INCLUDE_EDT
extern EdtDev *EDT_fd;
#endif
extern int FOI_Pipe[];
extern foi_packet FOI_Buf;
extern foi_packet FOI_LastCmd, FOI_ContAcqCmd;

/* function definitions */
extern int   sdvfoi_open(int);
extern int   sdvfoi_close(void);
extern int   foi_delay(int);
extern int   foi_resetvars(void);
extern int   foi_send1byte(void);
extern int   foi_nullcmd(void);
extern int   foi_dmainit_loopback(void);
extern int   foi_dmainit(void);
extern int   foi_fiforeset(void);
extern int   foi_cmd(unsigned int, unsigned int, unsigned int, unsigned int);
extern void *foi_monitor(void*);
extern int   foi_monitor_create(void);
extern int   foi_monitor_cancel(void);
extern int   datamode_toggle(void);
extern void  flipbits(int*);
extern int   fpga_program(char*);
extern int   fpga_status(void);
extern int   fpga_abort(void);

#endif
