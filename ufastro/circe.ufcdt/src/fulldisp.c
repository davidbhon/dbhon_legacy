/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: fulldisp.c -- Full-detector display control routines.
 |
 | DESCRIPTION:
 |      This source code file contains the routines for
 |      displaying images on the full-detector display, usually shown
 |      on the second monitor.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <math.h>
#include <gtk/gtk.h>

#include "main.h"
#include "support.h"
#include "support2.h"
#include "fiber.h"
#include "fits.h"
#include "display.h"
#include "plot.h"

extern image FullIm, SubIm[];

/***** Full Display image manipulation functions *****/

void zerofullimage(void)
{
   char *dum;

   dum = (char*) memset((void*)FullIm.data, 0, FullIm.nbytes);
}

int formatfullimage(void)
{
   int i, j, nbytes;
   int detnrows = DET_NROWS, detncols = DET_NCOLS, nxhalf = QUAD_NCOLS;
   int foffset, qoffset, dmin, dmax, dbot, dtop;
   char *dum;
   char *bptr1, *bptr2, *bptr3, *bptr4;
   int  *fptr1, *fptr2, *fptr3, *fptr4;
   int  *iptr11, *iptr12, *iptr13, *iptr14, *iptr15, *iptr16, *iptr17, *iptr18;
   int  *iptr21, *iptr22, *iptr23, *iptr24, *iptr25, *iptr26, *iptr27, *iptr28;
   image *im1=NULL, *im2=NULL;
   static int oldncols=0, oldnrows=0;
   int hist[NHIST], hshift=NHIST/2;

   /* Construct data array to display */

   switch (DC.buf_mode) {
      case 0: im1 = &SrcImage;  break;
      case 1: im1 = &BgdImage;  break;
      case 2: im1 = &SrcImage; im2 = &BgdImage; break;
   }

   /* Return if Image has not yet been initialized */
   if (im1->init == FALSE) return(FALSE);

   /* Copy header data */
   bptr1 = (char *)(&FullIm.nendptframes);
   bptr2 = (char *)(&im1->nendptframes);
   nbytes = (char *)(&FullIm.data) - bptr1;
   for (i=0; i<nbytes; i++) *bptr1++ = *bptr2++;

   /* Clear entire array if size of image has changed */
   if (SrcImage.ncols != oldncols || SrcImage.nrows != oldnrows) {
      zerofullimage();
      oldncols = SrcImage.ncols;
      oldnrows = SrcImage.nrows;
   } 

   /* Zero the histogram array */
   dum = (char*) memset((void*)hist, 0, NHIST*sizeof(int));

   /* Initialize Pointers */
   foffset = im1->startrow*detncols + im1->startcol;
   fptr1 = FullIm.data + detncols/2 + foffset;
   fptr2 = fptr1 - detncols/2;
   fptr3 = fptr2 + detnrows*detncols/2;
   fptr4 = fptr3 + detncols/2;

   bptr1 = &BPM[0] + detncols/2 + foffset;
   bptr2 = bptr1 - detncols/2;
   bptr3 = bptr2 + detnrows*detncols/2;
   bptr4 = bptr3 + detncols/2;

   qoffset = im1->nquadpixels;
   iptr11 = im1->data;          /* First 4 quads (reference) */
   iptr12 = iptr11 + qoffset;
   iptr13 = iptr12 + qoffset;
   iptr14 = iptr13 + qoffset;
   iptr15 = iptr14 + qoffset;   /* Last 4 quads (signal) */
   iptr16 = iptr15 + qoffset;
   iptr17 = iptr16 + qoffset;
   iptr18 = iptr17 + qoffset;

   nxhalf = detncols - im1->ncols;

   /* Construct data array to display */

   if (DC.buf_mode <= BACKGROUND) {      /* Display source or background */

      if (DC.sigref_mode == SIGNAL) {
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr15++; if (*bptr1) hist[*fptr1+hshift]++;
               *fptr2 = *iptr16++; if (*bptr2) hist[*fptr2+hshift]++;
               *fptr3 = *iptr17++; if (*bptr3) hist[*fptr3+hshift]++;
               *fptr4 = *iptr18++; if (*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;  fptr4++;
               bptr1++;  bptr2++;  bptr3++;  bptr4++;
            }
            fptr1 += nxhalf;  fptr2 += nxhalf;
            fptr3 += nxhalf;  fptr4 += nxhalf;
            bptr1 += nxhalf;  bptr2 += nxhalf;
            bptr3 += nxhalf;  bptr4 += nxhalf;
         }
      }
      else if (DC.sigref_mode == REFERENCE) {
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr11++; if(*bptr1) hist[*fptr1+hshift]++;
               *fptr2 = *iptr12++; if(*bptr2) hist[*fptr2+hshift]++;
               *fptr3 = *iptr13++; if(*bptr3) hist[*fptr3+hshift]++;
               *fptr4 = *iptr14++; if(*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;  fptr4++;
               bptr1++;  bptr2++;  bptr3++;  bptr4++;
            }
            fptr1 += nxhalf;   fptr2 += nxhalf;
            fptr3 += nxhalf;   fptr4 += nxhalf;
            bptr1 += nxhalf;   bptr2 += nxhalf;
            bptr3 += nxhalf;   bptr4 += nxhalf;
         }
      }
      else if (DC.sigref_mode == DIFFERENCE) {     /* Difference */
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr15++ - *iptr11++; if(*bptr1) hist[*fptr1+hshift]++;
               *fptr2 = *iptr16++ - *iptr12++; if(*bptr2) hist[*fptr2+hshift]++;
               *fptr3 = *iptr17++ - *iptr13++; if(*bptr3) hist[*fptr3+hshift]++;
               *fptr4 = *iptr18++ - *iptr14++; if(*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;   fptr4++;
               bptr1++;  bptr2++;  bptr3++;   bptr4++;
            }
            fptr1 += nxhalf;  fptr2 += nxhalf;
            fptr3 += nxhalf;  fptr4 += nxhalf;
            bptr1 += nxhalf;  bptr2 += nxhalf;
            bptr3 += nxhalf;  bptr4 += nxhalf;
         }
      }
   }

   else if (DC.buf_mode == SB) {     /* Display Source - Bkgd */

      iptr21 = im2->data;
      iptr22 = iptr21 + qoffset;
      iptr23 = iptr22 + qoffset;
      iptr24 = iptr23 + qoffset;
      iptr25 = iptr24 + qoffset;
      iptr26 = iptr25 + qoffset;
      iptr27 = iptr26 + qoffset;
      iptr28 = iptr27 + qoffset;

      if (DC.sigref_mode == SIGNAL) {
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr15++ - *iptr25++; if(*bptr1) hist[*fptr1+hshift]++;
               *fptr2 = *iptr16++ - *iptr26++; if(*bptr2) hist[*fptr2+hshift]++;
               *fptr3 = *iptr17++ - *iptr27++; if(*bptr3) hist[*fptr3+hshift]++;
               *fptr4 = *iptr18++ - *iptr28++; if(*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;  fptr4++;
               bptr1++;  bptr2++;  bptr3++;  bptr4++;
            }
            fptr1 += nxhalf;  fptr2 += nxhalf;
            fptr3 += nxhalf;  fptr4 += nxhalf;
            bptr1 += nxhalf;  bptr2 += nxhalf;
            bptr3 += nxhalf;  bptr4 += nxhalf;
         }
      }
      else if (DC.sigref_mode == REFERENCE) {
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr11++ - *iptr21++; if(*bptr1) hist[*fptr1+hshift]++;
               *fptr2 = *iptr12++ - *iptr22++; if(*bptr2) hist[*fptr2+hshift]++;
               *fptr3 = *iptr13++ - *iptr23++; if(*bptr3) hist[*fptr3+hshift]++;
               *fptr4 = *iptr14++ - *iptr24++; if(*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;  fptr4++;
               bptr1++;  bptr2++;  bptr3++;  bptr4++;
            }
            fptr1 += nxhalf;   fptr2 += nxhalf;
            fptr3 += nxhalf;   fptr4 += nxhalf;
            bptr1 += nxhalf;   bptr2 += nxhalf;
            bptr3 += nxhalf;   bptr4 += nxhalf;
         }
      }
      else if (DC.sigref_mode == DIFFERENCE) {
         for (i=0; i<im1->nrows; i++ ) {
            for (j=0; j<im1->ncols; j++) {
               *fptr1 = *iptr15++ - *iptr11++ - *iptr25++ + *iptr21++;
               *fptr2 = *iptr16++ - *iptr12++ - *iptr26++ + *iptr22++;
               *fptr3 = *iptr17++ - *iptr13++ - *iptr27++ + *iptr23++;
               *fptr4 = *iptr18++ - *iptr14++ - *iptr28++ + *iptr24++;
               if(*bptr1) hist[*fptr1+hshift]++;
               if(*bptr2) hist[*fptr2+hshift]++;
               if(*bptr3) hist[*fptr3+hshift]++;
               if(*bptr4) hist[*fptr4+hshift]++;
               fptr1++;  fptr2++;  fptr3++;   fptr4++;
               bptr1++;  bptr2++;  bptr3++;   bptr4++;
            }
            fptr1 += nxhalf;  fptr2 += nxhalf;
            fptr3 += nxhalf;  fptr4 += nxhalf;
            bptr1 += nxhalf;  bptr2 += nxhalf;
            bptr3 += nxhalf;  bptr4 += nxhalf;
         }
      }
   }

   /* Find Data Min and Max from histogram */
   hist_minmax(hist, NHIST, &dmin, &dmax);
   dmin = dmin-hshift;  dmax = dmax-hshift;

   /* Find Top and Bottom Percentage values from histogram */
   hist_bottop(hist, NHIST, 0.001, &dbot, &dtop);
   dbot = dbot-hshift;  dtop = dtop-hshift;

   /* Write Data Min and Max to Text Entry widgets */
   set_entry(GUI.FullDataMax_entry, "%d", dmax);
   set_entry(GUI.FullDataMin_entry, "%d", dmin);
   
   FullDisp.datamax = dmax;
   FullDisp.datamin = dmin;

   FullDisp.datatop = dtop;
   FullDisp.databot = dbot;

   FullIm.init=TRUE;
   return(TRUE);
}


void scalefullimage(void)
{
   int i, j;
   int colorval, maxcolor=MaxColor[0], mincolor=MinColor[0];
   int *fptr, coffset;
   int dispmin, dispmax;
   char *dum;
   float scale, offset;
   int hist[NHIST], hshift = NHIST/2;

   guchar *cptr, *bptr;

   /* Autostretch to Data Bot and Top */
   if (FullDisp.stretchmode == 0 || FullDisp.dispmin >= FullDisp.dispmax) {
      FullDisp.dispmax = FullDisp.datatop;   FullDisp.dispmin = FullDisp.databot;
   }
     
   /* Autostretch to Zoom Box */
   else if (FullDisp.stretchmode == 1) {
       /* Zero the histogram array */
       dum = (char*) memset((void*)hist, 0, NHIST*sizeof(int));
       fptr = FullIm.data + FullIm.ncols*(FullIm.nrows-1-ZoomBox.scry1) +
                 ZoomBox.scrx0;
       bptr = &BPM[0] + FullIm.ncols*(FullIm.nrows-1-ZoomBox.scry1) +
                 ZoomBox.scrx0;
       dispmin = dispmax = *fptr;
       for (i=0; i<ZoomBox.nrows; i++) {
          for (j=0; j<ZoomBox.ncols; j++) {
             if (*bptr) hist[*fptr+hshift]++;
             fptr++;
             bptr++;
          }
          fptr += FullIm.ncols - ZoomBox.ncols;
          bptr += FullIm.ncols - ZoomBox.ncols;
       }
       hist_bottop(hist, NHIST, 0.003, &dispmin, &dispmax);
       FullDisp.dispmin = dispmin-hshift;
       FullDisp.dispmax = dispmax-hshift; 
   }
 
   /* Check limits */
   FullDisp.dispmax = MIN(FullDisp.dispmax, 65535);
   if (FullDisp.dispmin >= FullDisp.dispmax) {
      if (FullDisp.dispmax == 65535) FullDisp.dispmin = FullDisp.dispmax - 1;
      else FullDisp.dispmax = FullDisp.dispmin + 1;
   }

   /* Display limits */
   set_entry(GUI.FullDisplayMax_entry, "%d", FullDisp.dispmax);
   set_entry(GUI.FullDisplayMin_entry, "%d", FullDisp.dispmin);

   scale = (float)NColors[0] / (FullDisp.dispmax - FullDisp.dispmin);  
   offset = -FullDisp.dispmin;

   /* Set destination char array pointers */
   /* 0, 0 = Upper left corner of window */
   cptr = FullDisp.buf + (DET_NROWS-1)*DET_NCOLS;
   fptr = FullIm.data;
   coffset = 2*DET_NCOLS;

   for (i=0; i<DET_NROWS; i++) {
      for (j=0; j<DET_NCOLS; j++) {
         colorval = (int) ((*fptr++ + offset) * scale + 0.5);
         *cptr++ = MIN(maxcolor, MAX(mincolor, colorval));
      }
      cptr -= coffset;
   }
}

void clearfulldisp(void)
{
   gdk_window_clear(GUI.FullDisp_darea->window);
}

void putfullimage (void)
{
   int dst_x, dst_y;
   GtkWidget *wid;

   dst_x = dst_y = 0;

   wid = FullDisp.wid;

   if (!IC.remote)
      gdk_draw_indexed_image(wid->window, wid->style->fg_gc[GTK_STATE_NORMAL],
        dst_x, dst_y, FullDisp.width, FullDisp.height, GDK_RGB_DITHER_NORMAL, 
        FullDisp.buf, FullDisp.width, CMap);

}

void displayfullimage(void)
{   

   if (formatfullimage() == TRUE) {
      scalefullimage();
      putfullimage();
      draw_box(&FullDisp, GC_FD, &ZoomBox);
      draw_fiducials();
      
      draw_compass(0.);
   }
} 


/* Draw all currently displayed fiducial lines and crosses */

void draw_fiducials() 
{
   if (VerFidA.drawn) draw_line  (&FullDisp, GC_FD, &VerFidA);
   if (VerFidB.drawn) draw_line  (&FullDisp, GC_FD, &VerFidB);
   if (HorFidA.drawn) draw_line  (&FullDisp, GC_FD, &HorFidA);
   if (HorFidB.drawn) draw_line  (&FullDisp, GC_FD, &HorFidB);
   if (CrossA.drawn ) draw_cross (&FullDisp, GC_FD, &CrossA);
   if (CrossB.drawn ) draw_cross (&FullDisp, GC_FD, &CrossB);
}


/* Draw the compass in the compass window */
/* Theta = Pos Ang in degrees that is toward top of detector */

void draw_compass (float theta)
{
   static int first=1;
   static int width, height;
   static int x1, y1, len, labellen;
   static char label[2][8]={"N\0", "E\0"};
   static float rpd=3.14159/180.;      /* Char offset params */
   static float lasttheta=-1000.;

   float costheta, sintheta;

   if (theta < -720. || theta > 720.) {
      fprintf(stderr, "Warning: Compass Theta value = %f out of range\n", theta);
      return;
   }

   if (first == 1) {
/*      XtVaGetValues(Compass_da, XmNheight, &height, XmNwidth, &width, NULL); */
      x1 = width/2;
      y1 = height/2;
      len  = (int) (width * 0.3);
      labellen = strlen(label[0]);
   }

   if (first == 1 || fabs(theta - lasttheta) > 0.5) {
   
      costheta = cos((360.-theta)*rpd);
      sintheta = sin((360.-theta)*rpd);
/*
      XClearWindow(display2, Compass_da_win);

      XSetForeground(display2, GC2, Black[1]);
      XSetLineAttributes(display2, GC2, 1, LineSolid, CapRound, JoinRound);

      XDrawLine(display2, Compass_da_win, GC2, x1, y1, (int)(x1-len*sintheta), 
          (int)(y1-len*costheta));
      XDrawLine(display2, Compass_da_win, GC2, x1, y1, (int)(x1-len*costheta),
          (int)(y1+len*sintheta));

      XDrawString(display2, Compass_da_win, GC2, (int)(x1-chars*len*sintheta-charx), 
         (int)(y1-chars*len*costheta+chary), label[0], labellen);
      XDrawString(display2, Compass_da_win, GC2, (int)(x1-chars*len*costheta-charx), 
         (int)(y1+chars*len*sintheta+chary), label[1], labellen);
 
      XSetForeground(display2, GC2, (unsigned long)Green[1]);
*/
      lasttheta = theta;
      first=0;
   }
}

