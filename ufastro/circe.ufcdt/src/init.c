/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | File: init.c -- Initialization Routines
 |
 | DESCRIPTION
 |	This source code file contains several routines used
 |        to initialize XPHARO widgets, memory and hardware.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <gtk/gtk.h>

#include "main.h"
#include "interface.h"
#include "support.h"
#include "support2.h"
#include "callbacks.h"
#include "files.h"
#include "fits.h"
#include "fiber.h"

extern file_struct Log;
extern fitsfile MainFits, SubFits;
extern image FullIm;

/*** Callbacks not prototyped in callbacks.h ***/
extern void on_Shutter_move_clicked  (GtkButton *, gpointer);
extern void on_Carousel_move_clicked (GtkButton *, gpointer);


/* Initialize Main_window GUI variables and widgets upon program startup */ 

void init_main(void)
{
   char strbuf[128];

   SrcImage.init = FALSE;
   BgdImage.init = FALSE;

   /*** Initialize GUI elements ***/

   GUI.Pressed_but = NULL;
   
   /* FITSSelect Window default directory and file type */
   strcpy(strbuf, FITSDIR);
   gtk_file_selection_set_filename(GTK_FILE_SELECTION(GUI.FITSSelect_window),
      strbuf);
   gtk_file_selection_complete(GTK_FILE_SELECTION(GUI.FITSSelect_window), "*.fits");
   gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(GUI.FITSSelect_window));

}

void init_datadir(void)
{
   int index;
   char templatestr[NAMESTRLEN], subdir[NAMESTRLEN];
   char formatstr[32], filename[NAMESTRLEN];
   int fp=0;
   
   /* Create test file name from Data Directory string */
   sprintf(templatestr, "%s/XXXXXX", Data_Dir);
   fp = mkstemp(templatestr);
   strcpy(subdir, Data_Dir);
   strcat(subdir, "/subimages");

   /* Open a new data directory */
   if ( fp < 1 ) {        /* Does directory already exist? */
      logprintf("Creating data directory: %s", Data_Dir);
      if (mkdir(Data_Dir, 000)) {                              /* If not, create it */
         logprintf("Invalid data directory: %s", Data_Dir);  /* If error, print warning */
         /*XtManageChild(Init_form);*/                             /* and request new name */
         /*XtPopup(XtParent(Init_form), XtGrabNone);*/
         return;
      }
      if ( chmod(Data_Dir, (mode_t)0755) ) perror("init_datadir, chmod");
      mkdir(subdir, 000);
      chmod(subdir, (mode_t)0755);
   }
   /* Close and unlink test file if directory already exists */
   else {
      close(fp);
      unlink(templatestr);
   }

   strcpy(Log.dir, Data_Dir);
   strcpy(MainFits.dir, Data_Dir);
   strcpy(SubFits.dir, subdir);

   /* Initialize FITS file index */ 
   sprintf(formatstr, "%%s/%%s%%0%dd.%%s", MainFits.ndigits);
   index = 0;

   /* Search for unique file name */
   do {
      if (fp) close(fp);
      sprintf(filename, formatstr, MainFits.dir, MainFits.prefix,
         index++, MainFits.suffix);
      fp = open(filename, O_RDONLY);
   } while (fp != -1 && (index < MainFits.max_index));

   if ((index == MainFits.max_index) && fp)
      fprintf(stderr, "createFITS Error: Can't create unique file name");
   if (fp) close(fp);
   MainFits.index = index-2;

   /* Set the default directory in the ReadFITS file selection dialog */
   strcpy(templatestr, Data_Dir);
   strcat(templatestr, "/*.fits");
/*   XmFileSelectionDoSearch(ReadFITS_file, XmStringCreateLocalized(templatestr)); */

   /* Open Log File */
   openlog();

}


void init_final()
{
   int imstatus;
   size_t dsize;
   arrayclock *ac;

   /* Allocate and initialize image memory */
   dsize = RAWDATASIZE;
   Raw_Buf[0] = (char*) valloc(dsize);
   Raw_Buf[1] = (char*) valloc(dsize);
   dsize = SORTDATASIZE;
   Sort_Buf   = (int*)  valloc(dsize);

   dsize = IMDATASIZE;
   SrcImage.data = (int*) malloc(dsize);
   BgdImage.data = (int*) malloc(dsize);
   SrcImage.nbytes = BgdImage.nbytes = 0;

/*   
   FlatImage.startcol    = FlatImage.startrow = 0;
   FlatImage.ncols       = FlatImage.nrows    = DET_NCOLS;
   FlatImage.nplanes     =                      1;
   FlatImage.nquadpixels =                      0;
   FlatImage.npixels     =                      DET_NPIX;
   FlatImage.nbytes      =                      FlatImage.npixels * 4;
  
   if ( (FlatImage.data = (int *)malloc(FlatImage.nbytes)) == NULL) {
      FlatImage.nbytes = 0;
      msgprintf("Error allocating memory for flat field buffer");
   }
*/
   
   ac = &ArrayClock;
   imstatus = setimageformat(ac);

   if (Raw_Buf[0] == NULL || Raw_Buf[1] == NULL || imstatus != 0) {
      printf("imstatus = %d\n", imstatus);
      msgprintf("Error allocating memory for image data");
   }

   /* Initialize EDT FOI board or set Fake data mode */
   if (!IC.tryfake) {
      if (sdvfoi_open(0) < 0) {
         logprintf("EDT FOI board not found");
         IC.tryfake = 1;
      }
      else {
         IC.data_mode = REALDATA;
         logprintf("Data Mode = REAL");
      }
   }

   if (IC.tryfake) {
      if (fakedata_init() < 0) {
         fprintf(stderr, "Error: Fake Data Initialization failed.\n");
         exit(0);
      }
      else {
         IC.data_mode = FAKEDATA;
         logprintf("Data Mode = FAKE");
      }
   }

   /* Program FPGA board and initialize FOI DMA input */
   if (IC.data_mode == REALDATA) {
      sleep(1);
      foi_dmainit();
   }

   /* Start fiber monitor routine in separate thread */
   foi_monitor_create();


}
