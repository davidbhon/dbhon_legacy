#if !defined(__UFGEMCADCOMMDC_C__)
#define __UFGEMCADCOMMDC_C__ "RCS: $Name:  $ $Id: ufGemCadCommDC.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufGEMCADCOMMCDC[] = __UFGEMCADCOMMDC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <cad.h>
#include <cadRecord.h>

#include "trecs.h"
#include "ufGemComm.h"


/**
 * Detector  Controller CAD's
 */

long
DCInitCommand (cadRecord * pcr)
{

  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
	  temp_long = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  strcpy (pcr->vala, "INIT");
	  strcpy (pcr->valc, pcr->a);
	  strcpy (pcr->valb, pcr->a);
	}
      else
	{
	  strcpy (pcr->valc, "");
	  strcpy (pcr->valb, "");
	  /* strncpy (pcr->mess, "Bad init command", MAX_STRING_SIZE);
	     return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
dcHrdwrCommand (cadRecord * pcr)
{

  long out_vala, out_valb, out_valc, out_vald, out_vale;
  long out_valf, out_valg, out_valh, out_vali, out_valj;
  long out_valk, out_valn;
  char out_vall[40], out_valm[40];
  char *endptr;
  long temp_long;

  out_vala = -1;
  out_valb = -1;
  out_valc = -1;
  out_vald = -1;
  out_vale = -1;
  out_valf = -1;
  out_valg = -1;
  out_valh = -1;
  out_vali = -1;
  out_valj = -1;
  out_valk = -1;
  out_valn = -1;
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  temp_long = 0;


  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("@@@@@@@@ pcr->a %s\n",pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  /* printf("!!!!!!!!!!!! out_vala %ld\n",out_vala) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->b %s\n",pcr->b) ; */
      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = (long) strtod (pcr->b, &endptr);
	  /* printf("!!!!!!!!!!!! out_valb %ld\n",out_valb) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad frame coadd", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valb, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid  frame coadd", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->c %s\n",pcr->c) ; */
      if (strcmp (pcr->c, "") != 0)
	{
	  out_valc = (long) strtod (pcr->c, &endptr);
	  /* printf("!!!!!!!!!!!! out_valc %ld\n",out_valc) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop sett freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valc, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid chop sett freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->d %s\n",pcr->d) ; */
      if (strcmp (pcr->d, "") != 0)
	{
	  out_vald = (long) strtod (pcr->d, &endptr);
	  /* printf("!!!!!!!!!!!! out_vald %ld\n",out_vald) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop coad", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vald, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid chop coad", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->e %s\n",pcr->e) ; */
      if (strcmp (pcr->e, "") != 0)
	{
	  out_vale = (long) strtod (pcr->e, &endptr);
	  /* printf("!!!!!!!!!!!! out_vale %ld\n",out_vale) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod settl c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vale, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid nod settl c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->f %s\n",pcr->f) ; */
      if (strcmp (pcr->f, "") != 0)
	{
	  out_valf = (long) strtod (pcr->f, &endptr);
	  /* printf("!!!!!!!!!!!! out_valf %ld\n",out_valf) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod settl f", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valf, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid nod settl f", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->g %s\n",pcr->g) ; */
      if (strcmp (pcr->g, "") != 0)
	{
	  out_valg = (long) strtod (pcr->g, &endptr);
	  /* printf("!!!!!!!!!!!! out_valg %ld\n",out_valg) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad save sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valg, 0, 1000000) == -1)
	    {
	      strncpy (pcr->mess, "invalid save sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->h %s\n",pcr->h) ; */
      if (strcmp (pcr->h, "") != 0)
	{
	  out_valh = (long) strtod (pcr->h, &endptr);
	  /* printf("!!!!!!!!!!!! out_valh %ld\n",out_valh) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valh, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid nod sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->i %s\n",pcr->i) ; */
      if (strcmp (pcr->i, "") != 0)
	{
	  out_vali = (long) strtod (pcr->i, &endptr);
	  /* printf("!!!!!!!!!!!! out_vali %ld\n",out_vali) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad pixclock", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vali, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid pixclock", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->j %s\n",pcr->j) ; */
      if (strcmp (pcr->j, "") != 0)
	{
	  out_valj = (long) strtod (pcr->j, &endptr);
	  /* printf("!!!!!!!!!!!! out_valj %ld\n",out_valj) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad pre chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valj, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid pre chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->k %s\n",pcr->k) ; */
      if (strcmp (pcr->k, "") != 0)
	{
	  out_valk = (long) strtod (pcr->k, &endptr);
	  /* printf("!!!!!!!!!!!! out_valk %ld\n",out_valk) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad post chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valk, 0, 1000) == -1)
	    {
	      strncpy (pcr->mess, "invalid post chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->l %s\n",pcr->l) ; */
      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);
	  /* printf("!!!!!!!!!!!! out_vall %s\n",out_vall) ; */
	  if ((strcmp (out_vall, "chop-nod") != 0) &&
	      (strcmp (out_vall, "chop") != 0) &&
	      (strcmp (out_vall, "nod") != 0) &&
	      (strcmp (out_vall, "stare") != 0))
	    {
	      strncpy (pcr->mess, "invalid obs mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->m %s\n",pcr->m) ; */
      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);
	  /* printf("!!!!!!!!!!!! out_valm %s\n",out_valm) ; */
	  /* if ((strcmp(out_valm,"S1") != 0) &&
	     (strcmp(out_valm,"S1R1") != 0) && 
	     (strcmp(out_valm,"S1R3") != 0) && 
	     (strcmp(out_valm,"s1") != 0) && 
	     (strcmp(out_valm,"s1r1") != 0) && 
	     (strcmp(out_valm,"s1r3") != 0)) { 
	     strncpy(pcr->mess, "invalid readout mode", MAX_STRING_SIZE) ; 
	     return CAD_REJECT ; 
	     } else */
	  temp_long = 1;
	}
      /* printf("@@@@@@@@ pcr->n %s\n",pcr->n) ; */
      if (strcmp (pcr->n, "") != 0)
	{
	  out_valn = (long) strtod (pcr->n, &endptr);
	  /* printf("!!!!!!!!!!!! out_valn %ld\n",out_valn) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valn, 0, 1) == -1)
	    {
	      strncpy (pcr->mess, "invalid flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}
      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /* printf("KKKKKKKKKKKKKKKKKKKKKKKK Clearing CAD Input\n") ; */
	  /*       if (temp_long) { */
	  *(long *) pcr->vala = out_vala;
	  *(long *) pcr->valb = out_valb;
	  *(long *) pcr->valc = out_valc;
	  *(long *) pcr->vald = out_vald;
	  *(long *) pcr->vale = out_vale;
	  *(long *) pcr->valf = out_valf;
	  *(long *) pcr->valg = out_valg;
	  *(long *) pcr->valh = out_valh;
	  *(long *) pcr->vali = out_vali;
	  *(long *) pcr->valj = out_valj;
	  *(long *) pcr->valk = out_valk;
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  *(long *) pcr->valn = out_valn;

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
dcPhysicalCommand (cadRecord * pcr)
{

  long out_vala;
  double out_valb, out_valc, out_vald, out_vale;
  double out_valf, out_valg, out_valh, out_vall, out_valm;
  char out_vali[40], out_valj[40];
  long out_valk;
  char out_valp[40];
  char *endptr;
  long temp_long;

  out_vala = -1;
  out_valb = -1.0;
  out_valc = -1.0;
  out_vald = -1.0;
  out_vale = -1.0;
  out_valf = -1.0;
  out_valg = -1.0;
  out_valh = -1.0;
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  out_valk = -1;
  out_vall = -1.0;
  out_valm = -1.0;
  strcpy (out_valp, "");

  temp_long = 0;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("@@@@@@@@ pcr->a %s\n",pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  /* printf("!!!!!!!!!!!! out_vala %ld\n",out_vala) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->b %s\n",pcr->b) ; */
      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = strtod (pcr->b, &endptr);
	  /* printf("!!!!!!!!!!!! out_valb %f\n",out_valb) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad frame time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valb, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid  frame time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->c %s\n",pcr->c) ; */
      if (strcmp (pcr->c, "") != 0)
	{
	  out_valc = strtod (pcr->c, &endptr);
	  /* printf("!!!!!!!!!!!! out_valc %f\n",out_valc) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad save freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valc, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid  save freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->d %s\n",pcr->d) ; */
      if (strcmp (pcr->d, "") != 0)
	{
	  out_vald = strtod (pcr->d, &endptr);
	  /* printf("!!!!!!!!!!!! out_vald %f\n",out_vald) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad exposure time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_vald, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid  exposure time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->e %s\n",pcr->e) ; */
      if (strcmp (pcr->e, "") != 0)
	{
	  out_vale = strtod (pcr->e, &endptr);
	  /* printf("!!!!!!!!!!!! out_vale %f\n",out_vale) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_vale, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid chp freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->f %s\n",pcr->f) ; */
      if (strcmp (pcr->f, "") != 0)
	{
	  out_valf = strtod (pcr->f, &endptr);
	  strcpy (out_valp, pcr->f);
	  /* printf("!!!!!!!!!!!! out_valf %f\n",out_valf) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "bad SCS DtyCycl", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valf, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid SCS DtyCycl", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->g %s\n",pcr->g) ; */
      if (strcmp (pcr->g, "") != 0)
	{
	  out_valg = strtod (pcr->g, &endptr);
	  /* printf("!!!!!!!!!!!! out_valg %f\n",out_valg) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod dwel time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valg, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid nod dwel time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}


      /* printf("@@@@@@@@ pcr->h %s\n",pcr->h) ; */
      if (strcmp (pcr->h, "") != 0)
	{
	  out_valh = strtod (pcr->h, &endptr);
	  /* printf("!!!!!!!!!!!! out_valh %f\n",out_valh) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod stl time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valh, 0, 300) == -1)
	    {
	      strncpy (pcr->mess, "invalid nod stl time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->i %s\n",pcr->i) ; */
      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);
	  /* printf("!!!!!!!!!!!! out_vali %s\n",out_vali) ; */
	  if ((strcmp (out_vali, "chop-nod") != 0) &&
	      (strcmp (out_vali, "chop") != 0) &&
	      (strcmp (out_vali, "nod") != 0) &&
	      (strcmp (out_vali, "stare") != 0))
	    {
	      strncpy (pcr->mess, "invalid obs mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->j %s\n",pcr->j) ; */
      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);
	  /* printf("!!!!!!!!!!!! out_valj %s\n",out_valj) ; */
	  /* if ((strcmp(out_valj,"S1") != 0) &&
	     (strcmp(out_valj,"S1R1") != 0) && 
	     (strcmp(out_valj,"S1R3") != 0) && 
	     (strcmp(out_valj,"s1") != 0) && 
	     (strcmp(out_valj,"s1r1") != 0) && 
	     (strcmp(out_valj,"s1r3") != 0)) { 
	     strncpy(pcr->mess, "invalid readout mode", MAX_STRING_SIZE) ; 
	     return CAD_REJECT ; 
	     } else */
	  temp_long = 1;
	}
      /*
         printf("@@@@@@@@ pcr->k %s\n",pcr->k) ; 
         if (strcmp(pcr->k,"") != 0) { 
         out_valk = strtod(pcr->k,&endptr) ; 
         printf("!!!!!!!!!!!! out_valk %f\n",out_valk) ; 
         if (*endptr != '\0'){ 
         strncpy (pcr->mess, "Bad chpduty cycle", MAX_STRING_SIZE); 
         return CAD_REJECT; 
         }
         if (UFcheck_double(out_valg,0,300) == -1) { 
         strncpy(pcr->mess, "invalid chopduty cycle", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         } else temp_long = 1 ; 
         }
       */

      /* printf("@@@@@@@@ pcr->k %s\n",pcr->k) ;   */
      if (strcmp (pcr->k, "") != 0)
	{
	  out_valk = (long) strtod (pcr->k, &endptr);
	  /* printf("!!!!!!!!!!!! out_valk %ld\n",out_valk) ;   */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valk, 0, 1) == -1)
	    {
	      strncpy (pcr->mess, "invalid flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->l %s\n",pcr->l) ; */
      if (strcmp (pcr->l, "") != 0)
	{
	  out_vall = strtod (pcr->l, &endptr);
	  /* printf("!!!!!!!!!!!! out_vall %f\n",out_vall) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad preValid Chop Time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_vall, 0, 3000) == -1)
	    {
	      strncpy (pcr->mess, "invalid preValid Chop Time",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->m %s\n",pcr->m) ; */
      if (strcmp (pcr->m, "") != 0)
	{
	  out_valm = strtod (pcr->m, &endptr);
	  /* printf("!!!!!!!!!!!! out_valm %f\n",out_valm) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad postValid Chop Time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valm, 0, 3000) == -1)
	    {
	      strncpy (pcr->mess, "invalid postValid Chop Time",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /* printf("KKKKKKKKKKKKKKKKKKKKKKKK Clearing CAD Input\n") ; */
	  /*       if (temp_long) { */
	  *(long *) pcr->vala = out_vala;
	  *(double *) pcr->valb = out_valb;
	  *(double *) pcr->valc = out_valc;
	  *(double *) pcr->vald = out_vald;
	  *(double *) pcr->vale = out_vale;
	  *(double *) pcr->valf = out_valf;
	  *(double *) pcr->valg = out_valg;
	  *(double *) pcr->valh = out_valh;
	  *(long *) pcr->valk = out_valk;
	  *(double *) pcr->vall = out_vall;
	  *(double *) pcr->valm = out_valm;
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valp, out_valp);

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	  strcpy (pcr->o, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
dcAcqCommand (cadRecord * pcr)
{

  char out_vala[40], out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40];

  char *endptr;
  long temp_long, out_valo;

  strcpy (out_vala, "");
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  strcpy (out_valn, "");

  out_valo = *(long *) pcr->valo;

  temp_long = 0;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      /* printf("@@@@@@@@ pcr->a %s\n",pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  /* printf("!!!!!!!!!!!! out_vala %s\n",out_vala) ; */
	  if ((strcmp (out_vala, "CONFIGURE") != 0) &&
	      (strcmp (out_vala, "START") != 0) &&
	      (strcmp (out_vala, "STOP") != 0) &&
	      (strcmp (out_vala, "ABORT") != 0) &&
	      (strcmp (out_vala, "INIT") != 0) &&
	      (strcmp (out_vala, "PARK") != 0) &&
	      (strcmp (out_vala, "DATUM") != 0) &&
	      (strcmp (out_vala, "TEST") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->b %s\n",pcr->b) ; */
      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);	/*obs ID */
	  temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->c %s\n",pcr->c) ; */
      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  /* printf("!!!!!!!!!!!! out_valc %s\n",out_valc) ; */
	  if ((strcmp (out_valc, "SAVE") != 0) &&
	      (strcmp (out_valc, "DISCARD") != 0) &&
	      (strcmp (out_valc, "Save") != 0) &&
	      (strcmp (out_valc, "Discard") != 0) &&
	      (strcmp (out_valc, "save") != 0) &&
	      (strcmp (out_valc, "discard") != 0))
	    {
	      strncpy (pcr->mess, "invalid DHS Write", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->d %s\n",pcr->d) ; */
      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  /* printf("!!!!!!!!!!!! out_vald %s\n",out_vald) ; */
	  temp_long = 1;	/* Quicklook  ID */
	}

      /* printf("@@@@@@@@ pcr->e %s\n",pcr->e) ; */
      if (strcmp (pcr->e, "") != 0)
	{
	  temp_long = (long) strtod (pcr->e, &endptr);
	  /* printf("!!!!!!!!!!!! out_vale %s\n",out_vale) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      if (temp_long == 2)
		out_valo = 1;
	      else
		out_valo = 0;
	      temp_long = 1;
	      strcpy (out_vale, pcr->e);
	    }
	}

      /* printf("@@@@@@@@ pcr->f %s\n",pcr->f) ; */
      if (strcmp (pcr->f, "") != 0)
	{
	  strcpy (out_valf, pcr->f);
	  /* printf("!!!!!!!!!!!! out_valf %s\n",out_valf) ; */
	  if ((strcmp (out_valf, "TRUE") != 0) &&
	      (strcmp (out_valf, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valf, "true") != 0) &&
	      (strcmp (out_valf, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid local archive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->g %s\n",pcr->g) ; */
      if (strcmp (pcr->g, "") != 0)
	{
	  strcpy (out_valg, pcr->g);
	  /* printf("!!!!!!!!!!!! out_valg %s\n",out_valg) ; */
	  if ((strcmp (out_valg, "TRUE") != 0) &&
	      (strcmp (out_valg, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valg, "true") != 0) &&
	      (strcmp (out_valg, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid nod handshake", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->h %s\n",pcr->h) ; */
      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);	/* path */
	  /* printf("!!!!!!!!!!!! out_valh %s\n",out_valh) ; */
	  temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->i %s\n",pcr->i) ; */
      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);	/* file name */
	  /* printf("!!!!!!!!!!!! out_vali %s\n",out_vali) ; */
	  temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->j %s\n",pcr->j) ; */
      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);	/* comment */
	  /* printf("!!!!!!!!!!!! out_valj %s\n",out_valj) ; */
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  /* printf("!!!!!!!!!!!! out_valf %s\n",out_valf) ; */
	  if ((strcmp (out_valk, "TRUE") != 0) &&
	      (strcmp (out_valk, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valk, "true") != 0) &&
	      (strcmp (out_valk, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid remote archive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);	/* rem host */
	  /* printf("!!!!!!!!!!!! out_valk %s\n",out_valk) ; */
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);	/* rem path */
	  /* printf("!!!!!!!!!!!! out_valk %s\n",out_valk) ; */
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);	/* rem file name */
	  /* printf("!!!!!!!!!!!! out_valk %s\n",out_valk) ; */
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /* printf("KKKKKKKKKKKKKKKKKKKKKKKK Clearing CAD Input\n") ; */
	  /*       if (temp_long) { */
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  *(long *) pcr->valo = out_valo;

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
dcObsCommand (cadRecord * pcr)
{

  long out_vala = -1;
  char out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40], out_valo[40];
  char out_valp[40], out_valq[40], out_valr[40], out_vals[40], out_valt[40];
  char *endptr = 0;
  long temp_long = 0;

  memset( out_valb, 0, 40 ) ;
  memset( out_valc, 0, 40 ) ;
  memset( out_vald, 0, 40 ) ;
  memset( out_vale, 0, 40 ) ;
  memset( out_valf, 0, 40 ) ;
  memset( out_valg, 0, 40 ) ;
  memset( out_valh, 0, 40 ) ;
  memset( out_vali, 0, 40 ) ;
  memset( out_valj, 0, 40 ) ;
  memset( out_valk, 0, 40 ) ;
  memset( out_vall, 0, 40 ) ;
  memset( out_valm, 0, 40 ) ;
  memset( out_valn, 0, 40 ) ;
  memset( out_valo, 0, 40 ) ;
  memset( out_valp, 0, 40 ) ;
  memset( out_valq, 0, 40 ) ;
  memset( out_valr, 0, 40 ) ;
  memset( out_vals, 0, 40 ) ;
  memset( out_valt, 0, 40 ) ;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      /* printf("@@@@@@@@ pcr->a %s\n",pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  /* printf("!!!!!!!!!!!! out_vala %ld\n",out_vala) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);
	  temp_long = 1;
	}
      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1;
	}
      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;
	}
      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);
	  temp_long = 1;
	}
      if (strcmp (pcr->f, "") != 0)
	{
	  strcpy (out_valf, pcr->f);
	  temp_long = 1;
	}
      if (strcmp (pcr->g, "") != 0)
	{
	  strcpy (out_valg, pcr->g);
	  temp_long = 1;
	}
      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);
	  temp_long = 1;
	}
      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);
	  temp_long = 1;
	}
      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);
	  temp_long = 1;
	}
      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  temp_long = 1;
	}
      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);
	  temp_long = 1;
	}
      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);
	  temp_long = 1;
	}
      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);
	  temp_long = 1;
	}
      if (strcmp (pcr->o, "") != 0)
	{
	  strcpy (out_valo, pcr->o);
	  temp_long = 1;
	}
      if (strcmp (pcr->p, "") != 0)
	{
	  strcpy (out_valp, pcr->p);
	  temp_long = 1;
	}
      if (strcmp (pcr->q, "") != 0)
	{
	  strcpy (out_valq, pcr->q);
	  temp_long = 1;
	}
      if (strcmp (pcr->r, "") != 0)
	{
	  strcpy (out_valr, pcr->r);
	  strcpy (out_valt, pcr->r);
	  temp_long = 1;
	}
      if (strcmp (pcr->s, "") != 0)
	{
	  strcpy (out_vals, pcr->s);
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /* printf("KKKKKKKKKKKKKKKKKKKKKKKK Clearing CAD Input\n") ; */
	  /*       if (temp_long) { */
	  *(long *) pcr->vala = out_vala;
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  strcpy (pcr->valo, out_valo);
	  strcpy (pcr->valp, out_valp);
	  strcpy (pcr->valq, out_valq);
	  strcpy (pcr->valr, out_valr);
	  strcpy (pcr->vals, out_vals);
	  strcpy (pcr->valt, out_valt);

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	  strcpy (pcr->o, "");
	  strcpy (pcr->p, "");
	  strcpy (pcr->q, "");
	  strcpy (pcr->r, "");
	  strcpy (pcr->s, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaInitCommand (cadRecord * pcr)
{

  long out_vala;
  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else
	{
	  *(long *) pcr->vala = -1;
	  /* strncpy (pcr->mess, "Bad init command", MAX_STRING_SIZE);
	     return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaDatumCommand (cadRecord * pcr)
{
  /*
     double out_val ;
     char *endptr ;
     long temp_long ; */
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /*
         out_val = strtod(pcr->a,&endptr) ; 

         if (*endptr != '\0'){ 
         strncpy (pcr->mess, "Can't convert datum directive", MAX_STRING_SIZE); 
         return CAD_REJECT; 
         } 
         temp_long = (long)out_val ; 
         if (UFcheck_long(temp_long,0,1) == -1) { 
         strncpy(pcr->mess, "datum directive is  not valid", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         } 
         *(long *)pcr->vala = temp_long ; 
       */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaPowerCommand (cadRecord * pcr)
{
  char out_vala[40];
  /* char *endptr ; */
  long temp_long;

  strcpy (out_vala, "");

  temp_long = 0;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "ON") != 0) &&
	      (strcmp (out_vala, "on") != 0) &&
	      (strcmp (out_vala, "On") != 0) &&
	      (strcmp (out_vala, "off") != 0) &&
	      (strcmp (out_vala, "Off") != 0) &&
	      (strcmp (out_vala, "OFF") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /*       if (temp_long) { */
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaParkCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  if (strcmp (pcr->a, "PARK") != 0)
	    {
	      strncpy (pcr->mess, "PARK directive  not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = 1;
	}
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaReadAllCommand (cadRecord * pcr)
{
  /*
     double out_val ;
     char *endptr ;
     long temp_long ; */
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaLatchCommand (cadRecord * pcr)
{

  long out_vala;
  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Invalid Latch command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 23) == -1)
	    {
	      strncpy (pcr->mess, "invalid Latch command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else
	{
	  *(long *) pcr->vala = -1;
	  /* strncpy (pcr->mess, "Bad init command", MAX_STRING_SIZE);
	     return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid latch command received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaSetCommand (cadRecord * pcr)
{

  long out_vala;
  double out_valb;
  char *endptr;
  long temp_long;

  out_vala = -1;
  out_valb = -1.0;

  temp_long = 0;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("@@@@@@@@ pcr->a %s\n",pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  /* printf("!!!!!!!!!!!! out_vala %ld\n",out_vala) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad Bias ID", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 23) == -1)
	    {
	      strncpy (pcr->mess, "invalid Bias ID", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      /* printf("@@@@@@@@ pcr->b %s\n",pcr->b) ; */
      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = strtod (pcr->b, &endptr);
	  /* printf("!!!!!!!!!!!! out_valb %f\n",out_valb) ; */
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad Voltage", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valb, -300.0, 300.0) == -1)
	    {
	      strncpy (pcr->mess, "invalid voltage", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == CAD_START))
	{
	  /* printf("KKKKKKKKKKKKKKKKKKKKKKKK Clearing CAD Input\n") ; */
	  /*       if (temp_long) { */
	  *(long *) pcr->vala = out_vala;
	  *(double *) pcr->valb = out_valb;

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;

}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaGlobSetCommand (cadRecord * pcr)
{

  double out_val;
  char *endptr;
  double temp_val;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.   */

    case CAD_PRESET:
    case CAD_START:
      if (strcmp (pcr->a, "") != 0)
	{
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert Global Set Value",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_val = out_val;
	  if (UFcheck_double (temp_val, -300.0, 300.0) == -1)
	    {
	      strncpy (pcr->mess, "Global Set Value is not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(double *) pcr->vala = temp_val;
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaAdjustCommand (cadRecord * pcr)
{

  double out_val;
  char *endptr;
  double temp_val;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      if (strcmp (pcr->a, "") != 0)
	{
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert adjust Value",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_val = out_val;
	  if (UFcheck_double (temp_val, -300.0, 300.0) == -1)
	    {
	      strncpy (pcr->mess, "Adjust value is not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(double *) pcr->vala = temp_val;
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaVGateCommand (cadRecord * pcr)
{
  char out_vala[40];
  strcpy (out_vala, "");

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaVWellCommand (cadRecord * pcr)
{
  char out_vala[40];
  strcpy (out_vala, "");
  strcpy (pcr->vala, "");

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;
      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
DCbipaVBiasCommand (cadRecord * pcr)
{
  char out_vala[40];
  strcpy (out_vala, "");

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbiPasswordCommand (cadRecord * pcr)
{
  /* char *endptr ; */

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (pcr->vala, pcr->a);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

#endif /* __UFGEMCADCOMMDC_C__ */
