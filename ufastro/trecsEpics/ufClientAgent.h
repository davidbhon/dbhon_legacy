#if !defined(__UFClientAgent_h__)
#define __UFClientAgent_h__ "$Name:  $ $Id: ufClientAgent.h,v 0.3 2002/07/01 19:34:14 hon Developmental $"
#define __UFClientAgent_H__(arg) const char arg##ClientAgent_h__rcsId[] = __UFClientAgent_h__;

/**
 * Agent (service) names, server IP addresse, default ports
 * (should be in /etc/services file)
 * return int number of list entrees, entry string looks like:
 * "AgentName@ServerHostName:nnn.nnn.nnn.nnn~PortNo"
 */
extern int ufServices (char **infolist);

extern int ufConnectAgent (const char *agent, const char *host, int portNo);
extern int ufCloseAgent (const char *agent);
extern int ufAgentSocket (const char *agent);

#endif /* __UFClientAgent_h__ */
