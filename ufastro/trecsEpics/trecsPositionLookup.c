

/*
 *
 *  Header for dummy lookup table code.
 *
 */

#include <stdioLib.h>
#include <stdlib.h>
#include <ellLib.h>
#include <string.h>
#include <logLib.h>

#include <trecsPositionLookup.h>


/*
 * Local defines
 */

/*
 *  Private data
 */

static ELLLIST *pFilterList, *pDeviceList;


typedef struct
{
    ELLNODE    next;
    char       name[NAME_SIZE];
    char       filter1[NAME_SIZE];
    char       filter2[NAME_SIZE];
    char       window[NAME_SIZE];
} FILTER_NODE;
    
typedef struct
{
    ELLNODE    next;
    int        number;
    int        positions;
    char       name[NAME_SIZE];
    ELLLIST    *pPositionList;
} DEVICE_NODE;


typedef struct
{
    ELLNODE    next;
    char       position[8];
    int        offset;
    float      throughput;
    float      lambdaLow;
    float      lambdaHigh;
    char       name[32];
} POSITION_NODE;
    


/*
 *************************************
 *
 *  Header for trecsInitPositionLookup
 *
 *************************************
 */


long trecsInitPositionLookup
(
    void
)
{
    long status = OK;           /* function return status           */

    /*
     *  Create the two lookup table lists
     */

    pFilterList = (ELLLIST *) malloc (sizeof (ELLLIST));
    ellInit (pFilterList);

    pDeviceList = (ELLLIST *) malloc (sizeof (ELLLIST));
    ellInit (pDeviceList);

    
    return status;
}


/*
 *************************************
 *
 *  Header for trecsReloadFilterTable
 *
 *************************************
 */

long trecsReloadFilterTable
(
    char *filterFileName
)
{
    FILE        *fp = NULL;           /* generic file pointer             */
    char        dataLine[256];
    FILTER_NODE *pFilterNode = NULL;
    long        status = OK;           /* function return status           */



    /*
     *  Try to open the filter configuration lookup file 
     */

    fp = fopen (filterFileName, "r");
    if (fp == NULL)
    {
        logMsg ("can't open filter configuration file\n",0,0,0,0,0,0); 
        return ERROR;
    }


    /*
     *  Open successful, if an old filter list exists, erase it before
     *  creating the list from this file.
     */

    ellFree (pFilterList);

    pFilterNode = (FILTER_NODE *) ellFirst (pFilterList);    
    
    while (fgets (dataLine, sizeof (dataLine), fp) != NULL)
    {
        if (*dataLine == '#' || *dataLine == '\n')
        {
            continue;
        }

        if (pFilterNode == NULL)
        {
            pFilterNode = (FILTER_NODE *) malloc (sizeof (FILTER_NODE));
            ellAdd (pFilterList, &pFilterNode->next);
        }

        if (sscanf (dataLine, 
                    "%s %s %s %s",
                    pFilterNode->name,
                    pFilterNode->filter1,
                    pFilterNode->filter2,
                    pFilterNode->window) != 4)
        {
            ellFree (pFilterList);
            logMsg ("error reading filter translation file\n",0,0,0,0,0,0);
            fclose (fp);
            return ERROR;
        }

        pFilterNode = (FILTER_NODE *) ellNext (&pFilterNode->next);
    }


    /*
     *  All done, close the filter lookup file
     */

    fclose (fp);
    return status;
}



/*
 *************************************
 *
 *  Header for trecsReloadDeviceTable
 *
 *************************************
 */

long trecsReloadDeviceTable
(
    char *deviceFileName
)
{
    FILE          *fp = NULL;           /* generic file pointer             */
    char          dataLine[256];
    DEVICE_NODE   *pDeviceNode = NULL;
    POSITION_NODE *pPositionNode = NULL;
    ELLLIST       *pPositionList = NULL; 
    int           position;
    long          status = OK;           /* function return status           */


    /*
     *  Try to open the device position lookup file 
     */

    fp = fopen (deviceFileName, "r");
    if (fp == NULL)
    {
        logMsg ("can't open device information file\n",0,0,0,0,0,0);
        return ERROR;
    }


    /*
     *  Erase the old device position lists if they already exist 
     */

    for (pDeviceNode = (DEVICE_NODE *) ellFirst (pDeviceList); 
         pDeviceNode != NULL;
         pDeviceNode = (DEVICE_NODE *) ellNext (&pDeviceNode->next))
    {
        if (pDeviceNode != NULL) ellFree (pDeviceNode->pPositionList);
    }

    ellFree (pDeviceList);
   
    pDeviceNode = (DEVICE_NODE *) ellFirst (pDeviceList);    
   

    /*
     *  Cruise through the device definition file looking for
     *  position information... Each motor has a definition block
     *  that starts with a title line containing the string";Mot_#".
     */

    while (fgets (dataLine, sizeof (dataLine), fp) != NULL)
    {

        /*
         *  Throw out everything up to the next device 
         *  definition block.
         */

        if (strncmp (dataLine, ";Mot_#", 6)) continue;


        /*
         *  The first line holds the device number, the number 
         *  of named positions for the device and the device 
         *  mnemonic.
         */

        if (fgets (dataLine, sizeof (dataLine), fp) == NULL)
        {
            logMsg ("Ran out of data in position file\n",0,0,0,0,0,0);
            fclose (fp);
            return ERROR;
        }

        if (pDeviceNode == NULL)
        {
            pDeviceNode = (DEVICE_NODE *) malloc (sizeof (DEVICE_NODE));
            ellAdd (pDeviceList, &pDeviceNode->next);
            pPositionList = (ELLLIST *) malloc (sizeof (ELLLIST));
            ellInit (pPositionList);
            pDeviceNode->pPositionList = pPositionList;
        }

        if (sscanf (dataLine, "%d %d %s", 
                &pDeviceNode->number,
                &pDeviceNode->positions,
                pDeviceNode->name) != 3)
        {
            logMsg ("position file formatting error\n",0,0,0,0,0,0);
            fclose (fp);
            return ERROR;
        }


        /*
         *  The positions for the device are defined next, one per line.
         *  Throw out any blank or comment lines that may be lurking
         *  around in the data block.
         */

        pPositionNode = (POSITION_NODE *) ellFirst (pDeviceNode->pPositionList);

        for (position = 0; position < pDeviceNode->positions; position++)
        {
            while (fgets (dataLine, sizeof (dataLine), fp) != NULL)
            {
                if (*dataLine != ';' && *dataLine != '\n') break;
            } 

            if (pPositionNode == NULL)
            {
                pPositionNode = (POSITION_NODE *)malloc (sizeof(POSITION_NODE));
                ellAdd (pPositionList, &pPositionNode->next);
            }

            if (sscanf (dataLine, 
                        "%s %d %f %f %f %s",
                        pPositionNode->position,
                        &pPositionNode->offset,
                        &pPositionNode->throughput,
                        &pPositionNode->lambdaLow,
                        &pPositionNode->lambdaHigh,
                        pPositionNode->name) != 6)
            {
                logMsg ("position file formatting error\n",0,0,0,0,0,0);
                fclose (fp);
                return ERROR;
            }

            pPositionNode = (POSITION_NODE *) ellNext (&pPositionNode->next);
        }

        pDeviceNode = (DEVICE_NODE *) ellNext (&pDeviceNode->next);
    }


    /*
     *  All done, close the position lookup file again.
     */


    fclose (fp);
    return status;
}


/*
 *************************************
 *
 *  Header for trecsFilterLookup
 *
 *************************************
 */

long trecsFilterLookup
(
    char *filter, 
    char *filter1, 
    char *filter2, 
    char *window
)
{
    FILTER_NODE *pFilterNode;
    long status = OK;           /* function return status     */

    pFilterNode = (FILTER_NODE *) ellFirst (pFilterList);

    while (pFilterNode != NULL)
    {
        if (strcmp (pFilterNode->name, filter) == 0)
        {
            strcpy (filter1, pFilterNode->filter1);
            strcpy (filter2, pFilterNode->filter2);
            strcpy (window, pFilterNode->window);
            break;
        }

        pFilterNode = (FILTER_NODE *) ellNext (&pFilterNode->next);
    }

    if (pFilterNode == NULL)
    {
        status = ERROR;
    }

    return status;
}


/*
 *************************************
 *
 *  Header for trecsPositionLookup
 *
 *************************************
 */

long trecsPositionLookup
(
    char    *device,
    char    *name,
    char    *position,
    float   *efficiency,
    float   *lambdaLow,
    float   *lambdaHigh
)
{    
    DEVICE_NODE   *pDeviceNode = NULL;
    POSITION_NODE *pPositionNode = NULL;
    ELLLIST       *pPositionList = NULL;
    long           status = OK;           /* function return status       */

    pDeviceNode = (DEVICE_NODE *) ellFirst (pDeviceList);

    while (pDeviceNode != NULL)
    {
        if (strcmp (pDeviceNode->name, device) == 0)
        {
            pPositionList = (ELLLIST *) pDeviceNode->pPositionList;
            pPositionNode = (POSITION_NODE *) ellFirst (pPositionList);

            while (pPositionNode != NULL)
            {
                if (strcmp (pPositionNode->name, name) == 0)
                { 
                    strcpy (position, pPositionNode->position);
                    *efficiency = pPositionNode->throughput;
                    *lambdaLow = pPositionNode->lambdaLow;
                    *lambdaHigh= pPositionNode->lambdaHigh;
                    return OK;
                }

                pPositionNode = (POSITION_NODE *) ellNext (&pPositionNode->next);
            }

            if (pPositionNode == NULL)
            {
                strcpy (position, "");
                *efficiency = 0.0;
                *lambdaLow = 0.0;
                *lambdaHigh = 0.0;
                status = ERROR;
            }
        }

        pDeviceNode = (DEVICE_NODE *) ellNext (&pDeviceNode->next);
    }

    if (pDeviceNode == NULL)
    {
        strcpy (position, "");
        *efficiency = 0.0;
        *lambdaLow = 0.0;
        *lambdaHigh = 0.0;
        status = ERROR;
    }

    return status;
}

