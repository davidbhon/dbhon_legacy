#if !defined(__UFGEMGENSUBCOMMDC_C__)
#define __UFGEMGENSUBCOMMDC_C__ "RCS: $Name:  $ $Id: ufGemGensubCommDC.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufGEMGENSUBCOMMCDC[] = __UFGEMGENSUBCOMMDC_C__ ;

#include <stdioLib.h>
#include <string.h>
#include <stdio.h>
#include <sysLib.h>

/* use select (checkSoc) on socfd -- hon */
#include <sockLib.h>

#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
#include <genSub.h>
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>


#include <dbStaticLib.h> 
#include <dbBase.h> 

#include "trecs.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"
#include "ufClient.h"
#include "ufLog.h"

static int _verbose = 1;
/*
static int number_connections = 0 ; 
static int num_req = 0 ; 
*/
/* static char namedPosStrings[200][40] ; */

static char dc_DEBUG_MODE[6] ;
static char dc_STARTUP_DEBUG_MODE[6] ;
static int dc_initialized  = 0 ;

static cloud_cover_element cloud_cover_table[4] ;
static water_vapour_element water_vapour_table[4];
static double fiducial_frame_time ;

static char dc_host_ip[20] ;
static long dc_port_no ;

static char adjPhysParm[25][40] ;
static char meta_phys_parm[25][40] ;
static char phys_phys_parm[25][40] ;
static char hrdware_parm[12][40] ;
static char adjMetaParm[28][40] ;

static double dc_bias_M[24], dc_bias_volt[24];
static long dc_bias_B[24], dc_bias_control[24], dc_bias_def[24], dc_bias_min[24], dc_bias_max[24];
static long preAmp_default[24] ;
static long preAmp_control[24] ;
static double dc_env_param[8] ;

static long Num_Readout_Modes ;

/* Array to hold the readout mode names */

static char **Readout_Modes ;

/* Look up tables for K, L, M filters and Spectroscopy */

static double *K_Filter ;
static double *L_Filter ;
static double *M_Filter ;
static double *LoRes10_Filter ;
static double *HiRes10_Filter ;
static double *LoRes20_Filter ;

/********************** DC  CODING ************************/

void init_dc_config () {
  FILE *dcFile ;
  char in_str[80] ;
  char *temp_str ;
  char filename[40] ;
 
  strcpy(filename,dcconfig_filename);
  if ((dcFile = fopen(filename,"r")) == NULL) {
    trx_debug("dc Configuration file could not be opened","dcConfig function","NONE","NONE") ;
    strcpy(dc_STARTUP_DEBUG_MODE,"NONE") ;
    strcpy(dc_DEBUG_MODE,"NONE") ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,40,dcFile);
    fgets(in_str,40,dcFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(dc_STARTUP_DEBUG_MODE,in_str) ;
    fclose(dcFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    if ( (strcmp(dc_STARTUP_DEBUG_MODE,"NONE") != 0) ||
         (strcmp(dc_STARTUP_DEBUG_MODE,"MIN") != 0) ||
	 (strcmp(dc_STARTUP_DEBUG_MODE,"FULL") != 0) ) strcpy(dc_STARTUP_DEBUG_MODE,"NONE") ;
    
    strcpy(dc_DEBUG_MODE,"NONE") ;
  }
  dc_initialized  = 1 ;
  logMsg("DC Initialized ... \n",0,0,0,0,0,0) ;
  return ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long ufdcStatusGInit (genSubRecord *gsp) {
  long status = 0 ;
  char blank_str[] = "" ;
  int i ;
  int *last_command ;

  strcpy(gsp->a, blank_str) ; /*  */
  strcpy(gsp->b, blank_str) ; /*  */
  strcpy(gsp->c, blank_str) ; /*  */
  strcpy(gsp->d, blank_str) ; /*  */

  last_command = (int *) malloc (sizeof(int)*2);
  for (i = 0; i<=1;i++) last_command[i] = CAR_IDLE ;
  strcpy(gsp->valc,"IDLE") ;
  strcpy(gsp->vald,"GOOD") ;

  gsp->dpvt = (void *) last_command ;
  return status ;
} 


/**********************************************************/
/**********************************************************/
/**********************************************************/
int status_changed (char *current, int last) {
  int changed = 0 ;
  switch (last) {
    case CAR_IDLE:
      if (strcmp(current,"IDLE") != 0) changed = 1;
      break;
    case CAR_BUSY:
      if (strcmp(current,"BUSY") != 0) changed = 1;
      break;

    case CAR_ERROR:
      if (strcmp(current,"ERR") != 0) changed = 1;
      break;
  }
  return changed ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long ufdcStatusGProc (genSubRecord *gsp) {
  long status = 0 ;
  char blank_str[] = "" ;
  /* static char temp_str[40] ; */
  static char out_message[40];
  long action = CAR_IDLE;
  int *last_command ;

  strcpy(out_message, blank_str) ;
   
  if ( (strcmp(gsp->a,"ERR") == 0) ||  (strcmp(gsp->c,"ERR") == 0) ) action = CAR_ERROR ;

  if (action == CAR_IDLE) {
    if ( (strcmp(gsp->a,"BUSY") == 0) ||  (strcmp(gsp->c,"BUSY") == 0) ) action = CAR_BUSY ;
  } 
  
  /*Let's figure out the state of the system and its health */
  if (action == CAR_IDLE) {
    strcpy(gsp->vald,"IDLE") ;
    strcpy(gsp->valc,"GOOD") ;
  } else {
    if (action == CAR_BUSY) {
      strcpy(gsp->vald,"BUSY") ;
      strcpy(gsp->valc,"GOOD") ;
    } else {
      strcpy(gsp->vald,"ERROR") ;
      strcpy(gsp->valc,"BAD") ;
    }
  }
  /* Let's see if we can figure out the status of the last command */
  /* first let's find out which one changed */

  last_command = (int *) gsp->dpvt ;
  
  if (status_changed (gsp->a, last_command[0])) {
    if (strcmp(gsp->a,"IDLE") == 0) {
      action = CAR_IDLE ;
      last_command[0] = CAR_IDLE ;
    } else {
      if (strcmp(gsp->a,"BUSY") == 0) {
        action = CAR_BUSY ;
        last_command[0] = CAR_BUSY ;
      } else {
        action = CAR_ERROR ;
        last_command[0] = CAR_ERROR ;
        strcpy(out_message,gsp->b) ;
        /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
        if (*(long *)gsp->valb == CAR_IDLE) {

        }
      }
    }
  } else { /* Well link 'A' did not change, let keep going */
    if (status_changed (gsp->c, last_command[1])) {
      if (strcmp(gsp->c,"IDLE") == 0) {
        action = CAR_IDLE ;
        last_command[1] = CAR_IDLE ;
      } else {
        if (strcmp(gsp->c,"BUSY") == 0) {
          action = CAR_BUSY ;
          last_command[1] = CAR_BUSY ;
        } else {
          action = CAR_ERROR ;
          last_command[1] = CAR_ERROR ;
          strcpy(out_message,gsp->d) ;
          /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
          if (*(long *)gsp->valb == CAR_IDLE) {
          
          }
        }
      }
    } 
  }
  
  strcpy(gsp->vala,out_message); 
  *(long *)gsp->valb = action; 
 
  return status ;
} 


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for adjHrdwrG ******************/
long ufadjHrdwrGinit (genSubRecord *pgs) {
  strcpy(pgs->a,"") ;
  pgs->noa = 12 ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for adjHrdwrG ******************/
long ufadjHrdwrGproc (genSubRecord *pgs) {
  /* Variable Declarations */
  /* long status ; */
  /* char *remainder ; */
  char yy[40] ; 
  /* if (bingo) printf("I got %d elements \n",elem_cnt) ; */
  char *original ;
  /* int i ; */
  char *zz ;
  if (strcmp(pgs->a,"") != 0) {
    /* printf("@@@@@@@@@@@@@@@ I am getting %ld elements\n",pgs->noa) ;
       printf("@@@@@@@@@@@@@@@  The first element is %s\n",(char *)pgs->a) ; */
    /* status = unpack_any_array (pgs, (char *)pgs->a, pgs->noa,remainder) ; */
  
    original = pgs->a ; zz = pgs->a ; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 5 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 8 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 9 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valj,yy) ; /* 10 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valk,yy) ; /* 11 */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 */


    strcpy(pgs->valm, "1") ;
    strcpy(pgs->valn, "START") ;
    strcpy(pgs->a,"") ;
    pgs->noa = 12 ;
  } else {
    strcpy(pgs->valn, "CLEAR") ;
  }
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the motor file *****************/
long read_dc_file (char *rec_name, genSubCommPrivate *pPriv, char paramLevel) {
  /* Initialization File */
  FILE *dcFile ;
  char in_str[80] ;
  char filename[50] ;
  double numnum ; /*, numnum1, numnum2 ; */
  int i;
  int done  ;
  char *temp_str ;
 
  strcpy(filename,dc_param_filename);
  printf("#$#$#$#$#$#$#$#$#$#$  Reading DC File.\n") ;
  if ((dcFile = fopen(filename,"r")) == NULL) {
    trx_debug("DC file could not be opened", rec_name,"MID",dc_STARTUP_DEBUG_MODE);
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,80,dcFile);  /* comment line */
    fgets(in_str,80,dcFile);  /* comment line */
    fgets(in_str,80,dcFile);  /* comment line */
    fgets(in_str,40,dcFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    strcpy (dc_host_ip,in_str) ;
    i = 0;
    while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
    dc_host_ip[i] = '\0' ;

    /* Read the port number */
    fgets(in_str,80,dcFile); /* comment line */ 
   
    /* 
    fgets(in_str,40,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(dcFile,"%lf",&numnum) ;

    dc_port_no = (long) numnum ;
    /* see if this is ObsControl and read the look up tables for sky transparency */
    if (paramLevel == 'M') {
      fgets(in_str,80,dcFile);  /* read till the end of line */
      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i = 0; i < 4; i++) {
        fscanf(dcFile,"%lf %lf",&cloud_cover_table[i].percentile, &cloud_cover_table[i].chop_freq) ;
        fgets(in_str,40,dcFile);  /* read the condition */
        /* trim leading spaces */
        temp_str = strchr(in_str,' ') ;
        done = 0;
        while ((temp_str[0] == ' ') && (!done)) {
          if (temp_str == in_str) {
            strcpy(in_str, temp_str+1);
          } else {
            done = 1;
            /* if (temp_str != NULL) temp_str[0] = '\0' ; */
          }
          temp_str = strchr(in_str,' ') ;
	}
        /* trim trailing spaces */
        temp_str = in_str + strlen(in_str) - 1 ;
        done = 0 ;
        while (isspace(temp_str[0]) && (!done)) {
          temp_str[0] = '\0' ;
          if(!isspace(in_str[strlen(in_str)-1]))   done = 1;
          temp_str = in_str + strlen(in_str) - 1 ;
	}
        strcpy(cloud_cover_table[i].conditions, in_str) ;
        /*printf("%f  %f  @%s@\n",cloud_cover_table[i].chop_freq, cloud_cover_table[i].percentile,
          cloud_cover_table[i].conditions) ; */
      }
      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i = 0; i < 4; i++) {
        fscanf(dcFile,"%lf  %lf",&water_vapour_table[i].percentile,
                                 &water_vapour_table[i].conditions) ;
        fgets(in_str,80,dcFile); /* read till the end of the line */
        /* fgets(in_str,40,dcFile); */  /* read the condition */
        /* trim leading spaces */
		/*
        done = 0;
        temp_str = strchr(in_str,' ') ;
        while ((temp_str[0] == ' ') && (!done)) {
          if (temp_str == in_str) {
            strcpy(in_str, temp_str+1);
          } else {
            done = 1; 
           
          }
          temp_str = strchr(in_str,' ') ;
	} */
        /* trim trailing spaces */
		/*
        temp_str = in_str + strlen(in_str) - 1 ;
        done = 0 ;
        while (isspace(temp_str[0]) && (!done)) {
          temp_str[0] = '\0' ;
          if(!isspace(in_str[strlen(in_str)-1]))   done = 1;
          temp_str = in_str + strlen(in_str) - 1 ;
	} */
        /*
        while ((temp_str != NULL) && (!done)) { 
          if (temp_str == in_str) {  
            strcpy(in_str, temp_str+1); 
          } else {  
            done = 1; 
            if (temp_str != NULL) temp_str[0] = '\0' ; 
          } 
          temp_str = strchr(in_str,' ') ; 
        } 
        */
      }
      fgets(in_str,80,dcFile); /* comment line */ 
      fscanf(dcFile,"%lf",&numnum) ;
      fiducial_frame_time = numnum ;
      fgets(in_str,80,dcFile); /* read till the end of the line */
      /* printf("%f\n",fiducial_frame_time) ; */
      /* Read the number of readout modes */
      fgets(in_str,80,dcFile); /* comment line */ 
      fscanf(dcFile,"%lf",&numnum) ;
      Num_Readout_Modes = (long) numnum ;
      fgets(in_str,80,dcFile); /* read till the end of the line */
      /* allocate the necessary memory */
      Readout_Modes = malloc(Num_Readout_Modes*sizeof(char *)) ;
      for (i=0;i<Num_Readout_Modes;i++) Readout_Modes[i] = malloc(10*sizeof(char)) ;

      K_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
      L_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
      M_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
      LoRes10_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
      HiRes10_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;
      LoRes20_Filter = malloc(Num_Readout_Modes*sizeof(double)) ;

      /* Read the readout Modes */
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fgets(in_str,40,dcFile);   /* read the readout mode */
        /* trim leading spaces */
		
        done = 0;
        temp_str = strchr(in_str,' ') ;
        while ((temp_str[0] == ' ') && (!done)) {
          if (temp_str == in_str) {
            strcpy(in_str, temp_str+1);
          } else {
            done = 1; 
           
          }
          temp_str = strchr(in_str,' ') ;
		} 
        /* trim trailing spaces */
		
        temp_str = in_str + strlen(in_str) - 1 ;
        done = 0 ;
        while (isspace(temp_str[0]) && (!done)) {
          temp_str[0] = '\0' ;
          if(!isspace(in_str[strlen(in_str)-1]))   done = 1;
          temp_str = in_str + strlen(in_str) - 1 ;
		}  
        
        while ((temp_str != NULL) && (!done)) { 
          if (temp_str == in_str) {  
            strcpy(in_str, temp_str+1); 
          } else {  
            done = 1; 
            if (temp_str != NULL) temp_str[0] = '\0' ; 
          } 
          temp_str = strchr(in_str,' ') ; 
        } 
        
        strcpy(Readout_Modes[i], in_str) ;

      }

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        K_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        L_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        M_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        LoRes10_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        HiRes10_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 

      fgets(in_str,80,dcFile); /* comment line */ 
      fgets(in_str,80,dcFile); /* comment line */ 
      for (i=0;i<Num_Readout_Modes;i++) {
        fscanf(dcFile,"%lf",&numnum) ;
        LoRes20_Filter[i] = numnum ;
      }
      fgets(in_str,80,dcFile); /* read until the end of the line */ 
    }
    fclose(dcFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  printf("#$#$#$#$#$#$#$#$#$#$  Finished Reading DC File.\n") ;
  return OK ;
}

/****** converts enviromental paremters to strings ********/
void assign_sky_transparency( ufdcObsContInput *dcObsCont_inp ) {
  int found ;
  int i ;
  /* char temp_str[40] ; */

  /* lookup the chop freq corresponding to sky noise */
  /* printf("#$#$#$#$#$#$#$#$#$#$  Assigning Env Parameters.\n") ; */
  i = 0 ;
  while( cloud_cover_table[i].percentile < dcObsCont_inp->meta_env_data[2] && i < 3 ) {
    i++ ;
  }
  sprintf( dcObsCont_inp->chop_freq, "%f", cloud_cover_table[i].chop_freq );

  /* lookup the mm of water vapour corresponding to sky background */
  /*sprintf(temp_str,"%ld",(long)dcObsCont_inp->meta_env_data[0]) ; */
  i = 0 ;
  while ( water_vapour_table[i].percentile < dcObsCont_inp->meta_env_data[0] && i < 3) {
    i++ ;
  }
  sprintf( dcObsCont_inp->sky_background, "%f", water_vapour_table[i].conditions );

  sprintf(dcObsCont_inp->air_mass,"%f",dcObsCont_inp->meta_env_data[1]) ;
  sprintf(dcObsCont_inp->ambient_temp,"%f",dcObsCont_inp->meta_env_data[3]) ;
  sprintf(dcObsCont_inp->tel_emiss,"%f",dcObsCont_inp->meta_env_data[4]) ;
  sprintf(dcObsCont_inp->rotat_rate,"%f",dcObsCont_inp->meta_env_data[5]) ;
  sprintf(dcObsCont_inp->fid_frame_time,"%f",fiducial_frame_time) ;
  found = 0 ;
  i = 0 ;
  while ((strcmp(dcObsCont_inp->readout_mode,Readout_Modes[i]) != 0) && (!found)) {
    i++ ;
    if (i == Num_Readout_Modes) found = 1;
  }
  if (i == Num_Readout_Modes) i = 0; /* default frame time */
  else i = i + 1; 
  sprintf(dcObsCont_inp->K_FilterFrameTime,"%f",K_Filter[i]) ;
  sprintf(dcObsCont_inp->L_FilterFrameTime,"%f",L_Filter[i]) ;
  sprintf(dcObsCont_inp->M_FilterFrameTime,"%f",M_Filter[i]) ;
  sprintf(dcObsCont_inp->LoRes10_FilterFrameTime,"%f",LoRes10_Filter[i]) ;
  sprintf(dcObsCont_inp->HiRes10_FilterFrameTime,"%f",HiRes10_Filter[i]) ;
  sprintf(dcObsCont_inp->LoRes20_FilterFrameTime,"%f",LoRes20_Filter[i]) ;
  /* printf("#$#$#$#$#$#$#$#$#$#$  Finished Assigning Env Parameters.\n") ; */
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for hardwareG *******************/
long ufhardwareGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDCHrdwrPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCHrdwrPrivate *) malloc (sizeof(genSubDCHrdwrPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  pPriv->CurrParam.FrmCoadds= -1 ;
  pPriv->CurrParam.ChpSettleReads= -1 ;
  pPriv->CurrParam.ChpCoadds= -1 ;
  pPriv->CurrParam.Savesets= -1 ;
  pPriv->CurrParam.NodSettleReads= -1 ;
  pPriv->CurrParam.NodSets= -1 ;
  pPriv->CurrParam.NodSettleChops= -1 ;
  pPriv->CurrParam.PreValidChops= -1 ;
  pPriv->CurrParam.PostValidChops= -1 ;
  pPriv->CurrParam.pixclock= -1 ;
  strcpy(pPriv->CurrParam.obs_mode,"") ;
  strcpy(pPriv->CurrParam.readout_mode,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /*  */
  *(long *)pgs->b = -1 ; /*  */
  *(long *)pgs->c = -1 ; /*  */
  *(long *)pgs->d = -1 ; /*  */
  *(long *)pgs->e = -1 ; /*  */
  *(long *)pgs->f = -1 ; /*  */
  *(long *)pgs->g = -1 ; /*  */
  *(long *)pgs->h = -1 ; /*  */
  *(long *)pgs->i = -1 ; /*  */
  *(long *)pgs->k = -1 ; /*  */
  *(long *)pgs->l = -1 ; /*  */
  strcpy(pgs->j,"") ;
  strcpy(pgs->m,"") ; /*  */
  strcpy(pgs->n,"") ; /*  */
  *(long *)pgs->o = -1 ; /*  */
  *(long *)pgs->p = -1 ; /*  */
  *(long *)pgs->q = -1 ; /*  */
  *(long *)pgs->r = -1 ; /*  */
  *(long *)pgs->s = -1 ; /*  */
  *(long *)pgs->t = -1 ; /*  */
  *(long *)pgs->u = -1 ; /*  */

  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->dcHwrdwr_inp.command_mode = -1  ;
  pPriv->dcHwrdwr_inp.FrmCoadds = -1  ;
  pPriv->dcHwrdwr_inp.ChpSettleReads = -1  ;
  pPriv->dcHwrdwr_inp.ChpCoadds = -1  ;
  pPriv->dcHwrdwr_inp.Savesets = -1  ;
  pPriv->dcHwrdwr_inp.NodSettleReads = -1  ;
  pPriv->dcHwrdwr_inp.NodSets = -1  ;
  pPriv->dcHwrdwr_inp.NodSettleChops = -1  ;
  pPriv->dcHwrdwr_inp.PreValidChops = -1  ;
  pPriv->dcHwrdwr_inp.PostValidChops = -1  ;
  pPriv->dcHwrdwr_inp.pixclock = -1  ;
  strcpy(pPriv->dcHwrdwr_inp.readout_mode,"") ;
  strcpy(pPriv->dcHwrdwr_inp.obs_mode,"") ;
  pPriv->dcHwrdwr_inp.hFlag = ADJUSTED_INPUT ;
  

  pPriv->paramLevel = 'H' ;
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for motorG *******************/
long ufhardwareGproc (genSubRecord *pgs) {

  genSubDCHrdwrPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;

  
  double numnum ;
  

  /* strcpy(dc_DEBUG_MODE,pgs->m) ; */

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCHrdwrPrivate *)pgs->dpvt ;
  printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState);
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!dc_initialized)  init_dc_config() ;
      /* read_dc_file(pgs->name, (genSubCommPrivate *)pPriv) ; */ /* ZZZ */

      /* pPriv->port_no = dc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      *(long *)pgs->valc = -1 ;
      *(long *)pgs->vald = -1 ;
      *(long *)pgs->vale = -1 ;
      *(long *)pgs->valf = -1 ;
      *(long *)pgs->valg = -1 ;
      *(long *)pgs->valh = -1 ;
      *(long *)pgs->vali = -1 ;
      *(long *)pgs->valj = -1 ;
      *(long *)pgs->valk = -1 ;
      *(long *)pgs->vall = -1 ;
      strcpy(pgs->valm,"") ;
      strcpy(pgs->valn,"") ;

      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */ 
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */  /* ZZZ */
      pPriv->socfd = -1 ;  /* ZZZ */
      *(long *)pgs->valb = (long)pPriv->socfd ;   /* current socket number */  /* ZZZ */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
	} */ /* ZZZ */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      

      pPriv->dcHwrdwr_inp.command_mode   = *(long *)pgs->a  ;
      pPriv->dcHwrdwr_inp.FrmCoadds      = *(long *)pgs->b  ;
      pPriv->dcHwrdwr_inp.ChpSettleReads = *(long *)pgs->c  ;
      pPriv->dcHwrdwr_inp.ChpCoadds      = *(long *)pgs->d  ;
      pPriv->dcHwrdwr_inp.NodSettleChops = *(long *)pgs->e  ;
      pPriv->dcHwrdwr_inp.NodSettleReads = *(long *)pgs->f  ;
      pPriv->dcHwrdwr_inp.Savesets       = *(long *)pgs->g  ;
      pPriv->dcHwrdwr_inp.NodSets        = *(long *)pgs->h  ;
      pPriv->dcHwrdwr_inp.pixclock       = *(long *)pgs->i  ;
      pPriv->dcHwrdwr_inp.PreValidChops  = *(long *)pgs->k  ; 
      pPriv->dcHwrdwr_inp.PostValidChops = *(long *)pgs->l  ;
      strcpy(pPriv->dcHwrdwr_inp.obs_mode,pgs->m) ;
      strcpy(pPriv->dcHwrdwr_inp.readout_mode, pgs->n) ;
      pPriv->dcHwrdwr_inp.hFlag = *(long *)pgs->o  ;
      /*
      printf("My inputs are ......\n") ;
      printf("Link A %ld\n",*(long *)pgs->a ) ; 
      printf("Link B %ld\n",*(long *)pgs->b ) ; 
      printf("Link C %ld\n",*(long *)pgs->c ) ; 
      printf("Link D %ld\n",*(long *)pgs->d ) ; 
      printf("Link E %ld\n",*(long *)pgs->e ) ; 
      printf("Link F %ld\n",*(long *)pgs->f ) ; 
      printf("Link G %ld\n",*(long *)pgs->g ) ;
      printf("Link H %ld\n",*(long *)pgs->h ) ; 
      printf("Link I %ld\n",*(long *)pgs->i ) ; 
      printf("Link K %ld\n",*(long *)pgs->k ) ; 
      printf("Link L %ld\n",*(long *)pgs->l ) ; 
      printf("Link M %s\n",(char *)pgs->m ) ; 
      printf("Link N %s\n",(char *)pgs->n ) ; 
      printf("Link O %ld\n",*(long *)pgs->o ) ; 
      */
      if ( (pPriv->dcHwrdwr_inp.command_mode != -1) ||
	   (pPriv->dcHwrdwr_inp.FrmCoadds != -1) ||
	   (pPriv->dcHwrdwr_inp.ChpSettleReads != -1) ||
	   (pPriv->dcHwrdwr_inp.ChpCoadds != -1) ||
	   (pPriv->dcHwrdwr_inp.Savesets != -1) ||
	   (pPriv->dcHwrdwr_inp.NodSettleReads != -1) ||
	   (pPriv->dcHwrdwr_inp.NodSets != -1) ||
	   (pPriv->dcHwrdwr_inp.NodSettleChops != -1) ||
	   (pPriv->dcHwrdwr_inp.PreValidChops != -1) ||
	   (pPriv->dcHwrdwr_inp.PostValidChops != -1) ||
	   (pPriv->dcHwrdwr_inp.pixclock  != -1) ||
           (strcmp(pPriv->dcHwrdwr_inp.obs_mode,"") != 0) ||
           (strcmp(pPriv->dcHwrdwr_inp.readout_mode,"") != 0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        *(long *)pgs->a = -1 ; /*  */
        *(long *)pgs->b = -1 ; /*  */
        *(long *)pgs->c = -1 ; /*  */
        *(long *)pgs->d = -1 ; /*  */
        *(long *)pgs->e = -1 ; /*  */
        *(long *)pgs->f = -1 ; /*  */
        *(long *)pgs->g = -1 ; /*  */
        *(long *)pgs->h = -1 ; /*  */
        *(long *)pgs->i = -1 ; /*  */
        *(long *)pgs->k = -1 ; /*  */
        *(long *)pgs->l = -1 ; /*  */
        strcpy(pgs->m,"") ;
        strcpy(pgs->n,"") ;
      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /*  printf("My Flag is %ld and the ADJUSTED input thingy is %d\n",pPriv->dcHwrdwr_inp.hFlag, ADJUSTED_INPUT) ; */
      /* Is this a CALLBACK to send a command or do an INIT or process adjusted input? */
      if (pPriv->dcHwrdwr_inp.hFlag == 1) {
        /* get the input into the current parameters */
        if (pPriv->dcHwrdwr_inp.FrmCoadds != -1)  
          pPriv->CurrParam.FrmCoadds = pPriv->dcHwrdwr_inp.FrmCoadds  ;
        if (pPriv->dcHwrdwr_inp.ChpSettleReads != -1) 
          pPriv->CurrParam.ChpSettleReads = pPriv->dcHwrdwr_inp.ChpSettleReads; 
        if (pPriv->dcHwrdwr_inp.ChpCoadds != -1) 
          pPriv->CurrParam.ChpCoadds = pPriv->dcHwrdwr_inp.ChpCoadds ;
        if (pPriv->dcHwrdwr_inp.Savesets != -1) 
          pPriv->CurrParam.Savesets = pPriv->dcHwrdwr_inp.Savesets ;
        if (pPriv->dcHwrdwr_inp.NodSettleReads != -1) 
          pPriv->CurrParam.NodSettleReads = pPriv->dcHwrdwr_inp.NodSettleReads ;
        if (pPriv->dcHwrdwr_inp.NodSets != -1) 
          pPriv->CurrParam.NodSets = pPriv->dcHwrdwr_inp.NodSets ;
        if (pPriv->dcHwrdwr_inp.NodSettleChops != -1) 
          pPriv->CurrParam.NodSettleChops = pPriv->dcHwrdwr_inp.NodSettleChops ;
        if (pPriv->dcHwrdwr_inp.PreValidChops != -1) 
          pPriv->CurrParam.PreValidChops = pPriv->dcHwrdwr_inp.PreValidChops ;
        if (pPriv->dcHwrdwr_inp.PostValidChops != -1) 
          pPriv->CurrParam.PostValidChops = pPriv->dcHwrdwr_inp.PostValidChops ;
        if (pPriv->dcHwrdwr_inp.pixclock != -1) 
          pPriv->CurrParam.pixclock = pPriv->dcHwrdwr_inp.pixclock ;
        if(strcmp(pPriv->dcHwrdwr_inp.obs_mode,"") != 0)
          strcpy(pPriv->CurrParam.obs_mode,pPriv->dcHwrdwr_inp.obs_mode) ;
        if(strcmp(pPriv->dcHwrdwr_inp.readout_mode,"") != 0)
          strcpy(pPriv->CurrParam.readout_mode,pPriv->dcHwrdwr_inp.readout_mode) ;


        *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
        *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
        *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
        *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
        *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
        *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
        *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
        *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
        *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
        *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
        strcpy(pgs->valm,pPriv->CurrParam.obs_mode) ;
        strcpy(pgs->valn,pPriv->CurrParam.readout_mode) ;

        /* Reset Command state to DONE */
        pPriv->commandState = TRX_GS_DONE;
        some_num = CAR_IDLE ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                     &some_num,1);
        if (status) {
          trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
      }
      else { /* kkkkkk */
        /* Is this a CALLBACK to send a command or do an INIT? */
        if (pPriv->dcHwrdwr_inp.command_mode != -1) { /* We have an init command */
          trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Check first to see if we have a valid INIT directive */
          if ((pPriv->dcHwrdwr_inp.command_mode == SIMM_NONE) ||
              (pPriv->dcHwrdwr_inp.command_mode == SIMM_FAST) ||
              (pPriv->dcHwrdwr_inp.command_mode == SIMM_FULL) ) {
            /* need to reconnect to the agent ? -- hon */
	    if( pPriv->socfd >= 0 ) {
	      if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	        printf("ufhardwareGproc> socket is writable, no need to reconnect...\n");
	      }
	      else {
                trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
                ufClose(pPriv->socfd) ;
                pPriv->socfd = -1;
                *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
	    }
            pPriv->CurrParam.command_mode = pPriv->dcHwrdwr_inp.command_mode; /* simulation level */
            *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
            /* Re-Connect if needed */
            printf("My Command Mode is %ld \n",pPriv->dcHwrdwr_inp.command_mode) ;
            /* Read some initial parameters from a file */
            /*
            pPriv->CurrParam.init_velocity = -1.0 ; 
            pPriv->CurrParam.slew_velocity = -1.0 ; 
            pPriv->CurrParam.acceleration = -1.0 ; 
            pPriv->CurrParam.deceleration = -1.0 ; 
            pPriv->CurrParam.drive_current= -1.0 ; 
            pPriv->CurrParam.datum_speed = -1 ; 
            pPriv->CurrParam.datum_direction = -1 ; 
            */
            /* if (read_dc_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ; */
            read_dc_file(pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel) ;
            pPriv->port_no = dc_port_no ;

            if (pPriv->dcHwrdwr_inp.command_mode != SIMM_FAST) {
	      if( pPriv->socfd < 0 ) {
                trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
                pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
                trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
 	        *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
              if (pPriv->socfd < 0) { /* we have a bad socket connection */
                pPriv->commandState = TRX_GS_DONE;
                strcpy (pPriv->errorMessage, "Error Connecting to Agent");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            }
            /* update output links if connection is good or we are in SIMM_FAST*/
            if ( (pPriv->dcHwrdwr_inp.command_mode == SIMM_FAST) ||
                 (pPriv->socfd > 0) ) {
              /* update the output links */

              *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
              *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
              *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
              *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
              *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
              *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
              *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
              *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
              *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
              *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
              strcpy(pgs->valm,pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valn,pPriv->CurrParam.readout_mode) ;

              /* Reset Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
              if (status) {
                trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              } 
            }
	  } else { /* we have an error in input of the init command */
            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "Bad INIT Directive");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
        } else { /* We have a configuration command */
          trx_debug("We have a configuration Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Save the current parameters */
 
          pPriv->OldParam.FrmCoadds      = pPriv->CurrParam.FrmCoadds ;
          pPriv->OldParam.ChpSettleReads = pPriv->CurrParam.ChpSettleReads ;
          pPriv->OldParam.ChpCoadds      = pPriv->CurrParam.ChpCoadds ;
          pPriv->OldParam.Savesets       = pPriv->CurrParam.Savesets ;
          pPriv->OldParam.NodSettleReads = pPriv->CurrParam.NodSettleReads ;
          pPriv->OldParam.NodSets        = pPriv->CurrParam.NodSets ;
          pPriv->OldParam.NodSettleChops = pPriv->CurrParam.NodSettleChops ;
          pPriv->OldParam.PreValidChops  = pPriv->CurrParam.PreValidChops ;
          pPriv->OldParam.PostValidChops = pPriv->CurrParam.PostValidChops ;
          pPriv->OldParam.pixclock       = pPriv->CurrParam.pixclock ;
          strcpy(pPriv->OldParam.obs_mode,pPriv->CurrParam.obs_mode) ;
          strcpy(pPriv->OldParam.readout_mode,pPriv->CurrParam.readout_mode) ;

          /* get the input into the current parameters */
          if (pPriv->dcHwrdwr_inp.FrmCoadds != -1)  
            pPriv->CurrParam.FrmCoadds = pPriv->dcHwrdwr_inp.FrmCoadds  ;
          if (pPriv->dcHwrdwr_inp.ChpSettleReads != -1) 
            pPriv->CurrParam.ChpSettleReads = pPriv->dcHwrdwr_inp.ChpSettleReads; 
          if (pPriv->dcHwrdwr_inp.ChpCoadds != -1) 
            pPriv->CurrParam.ChpCoadds = pPriv->dcHwrdwr_inp.ChpCoadds ;
          if (pPriv->dcHwrdwr_inp.Savesets != -1) 
            pPriv->CurrParam.Savesets = pPriv->dcHwrdwr_inp.Savesets ;
          if (pPriv->dcHwrdwr_inp.NodSettleReads != -1) 
            pPriv->CurrParam.NodSettleReads = pPriv->dcHwrdwr_inp.NodSettleReads ;
          if (pPriv->dcHwrdwr_inp.NodSets != -1) 
            pPriv->CurrParam.NodSets = pPriv->dcHwrdwr_inp.NodSets ;
          if (pPriv->dcHwrdwr_inp.NodSettleChops != -1) 
            pPriv->CurrParam.NodSettleChops = pPriv->dcHwrdwr_inp.NodSettleChops ;
          if (pPriv->dcHwrdwr_inp.PreValidChops != -1) 
            pPriv->CurrParam.PreValidChops = pPriv->dcHwrdwr_inp.PreValidChops ;
          if (pPriv->dcHwrdwr_inp.PostValidChops != -1) 
            pPriv->CurrParam.PostValidChops = pPriv->dcHwrdwr_inp.PostValidChops ;
          if (pPriv->dcHwrdwr_inp.pixclock != -1) 
            pPriv->CurrParam.pixclock = pPriv->dcHwrdwr_inp.pixclock ;
          if(strcmp(pPriv->dcHwrdwr_inp.obs_mode,"") != 0)
            strcpy(pPriv->CurrParam.obs_mode,pPriv->dcHwrdwr_inp.obs_mode) ;
          if(strcmp(pPriv->dcHwrdwr_inp.readout_mode,"") != 0)
            strcpy(pPriv->CurrParam.readout_mode,pPriv->dcHwrdwr_inp.readout_mode) ;

          /* see if this a command right after an init */
          /*
          if (pPriv->send_init_param) {
            pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
            pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
            pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
            pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
            pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current;
            pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
            pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ; 
            pPriv->send_init_param = 0 ; 
          }
          */
          /* formulate the command strings */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;

          num_str = UFcheck_dc_inputs (pPriv->dcHwrdwr_inp,com, pPriv->paramLevel, rec_name,&pPriv->CurrParam, 
                                               pPriv->CurrParam.command_mode) ;

          /* Clear the input structure */

          pPriv->dcHwrdwr_inp.command_mode = -1  ;
          pPriv->dcHwrdwr_inp.FrmCoadds = -1  ;
          pPriv->dcHwrdwr_inp.ChpSettleReads = -1  ;
          pPriv->dcHwrdwr_inp.ChpCoadds = -1  ;
          pPriv->dcHwrdwr_inp.Savesets = -1  ;
          pPriv->dcHwrdwr_inp.NodSettleReads = -1  ;
          pPriv->dcHwrdwr_inp.NodSets = -1  ;
          pPriv->dcHwrdwr_inp.NodSettleChops = -1  ;
          pPriv->dcHwrdwr_inp.PreValidChops = -1  ;
          pPriv->dcHwrdwr_inp.PostValidChops = -1  ;
          pPriv->dcHwrdwr_inp.pixclock = -1  ;
          strcpy(pPriv->dcHwrdwr_inp.readout_mode,"") ;
          strcpy(pPriv->dcHwrdwr_inp.obs_mode,"") ;

          /* do we have commands to send? */
          if (num_str > 0) {
            /* if no Agent needed then go back to DONE */
            /* we are talking about SIMM_FAST or commands that do not need the agent */
            printf("********** Thn number of strings is : %d\n",num_str) ;
            for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;

            if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* update the output links */

              *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
              *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
              *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
              *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
              *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
              *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
              *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
              *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
              *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
              *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
              strcpy(pgs->valm, pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valn, pPriv->CurrParam.readout_mode) ;
 
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, 
                                   DBR_LONG, &some_num, 1);
              if (status) {
                logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
                return OK;
              }
            } else { /* we are in SIMM_NONE or SIMM_FULL */
              /* See if you can send the command and go to BUSY state */
              /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
              if (pPriv->socfd > 0) {
		if( _verbose )
		  printf("ufhardwareGproc> sending to agent, genSub record: %s\n", pgs->name); 
		status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
              }
	      else {
		printf("ufhardwareGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
		status = 0;
	      } 
             for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              if (status == 0) { /* there was an error sending the commands to the agent */
                /* set Command state to DONE */
                pPriv->commandState = TRX_GS_DONE;
                /* set the CAR to ERR with appropriate message like 
                 * "Bad socket"  and go to DONE state*/
                 strcpy (pPriv->errorMessage, "Bad socket connection");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else { /* send successful */
                /* establish a CALLBACK */
                requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
                pPriv->startingTicks = tickGet() ;
                /* set Command state to BUSY */
                pPriv->commandState = TRX_GS_BUSY;
              }
            }
          } else {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (num_str < 0) {
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* we have an error in the Input */
              strcpy (pPriv->errorMessage, "Error in input");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* num_str == 0 */
              /* is it possible to get here? */
              /* here we have a change in input.  The inputs though got sent
               * to be checked but no string got formulated.  */
              /* this can only happen if somehow the memory got corrupted */
              /* or the gensub is being processed by an alien. */
              /* Weird. */
              trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
            }
          }
        }
      
        /*Is this a J field processing? */
        strcpy(response,pgs->j) ;
        if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
      } /* kkkkk */
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /* 
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) { 
        cancelCallback (pPriv->pCallback);  
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ; 
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;  
        strcpy(pgs->i,"") ; 
        strcpy(pgs->k,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;   
        pPriv->commandState = TRX_GS_SENDING;    
        return OK ;   
	} */

      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "DC H Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
        
          /* update the output links */

          *(long *)pgs->valc = pPriv->CurrParam.FrmCoadds ;
          *(long *)pgs->vald = pPriv->CurrParam.ChpSettleReads ;
          *(long *)pgs->vale = pPriv->CurrParam.ChpCoadds ;
          *(long *)pgs->valf = pPriv->CurrParam.NodSettleChops ;
          *(long *)pgs->valg = pPriv->CurrParam.NodSettleReads ;
          *(long *)pgs->valh = pPriv->CurrParam.Savesets ;
          *(long *)pgs->vali = pPriv->CurrParam.NodSets ;
          *(long *)pgs->valj = pPriv->CurrParam.pixclock ;
          *(long *)pgs->valk = pPriv->CurrParam.PreValidChops ;
          *(long *)pgs->vall = pPriv->CurrParam.PostValidChops ;
          strcpy(pgs->valm, pPriv->CurrParam.obs_mode) ;
          strcpy(pgs->valn, pPriv->CurrParam.readout_mode) ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */

            pPriv->CurrParam.FrmCoadds      = pPriv->OldParam.FrmCoadds ;
            pPriv->CurrParam.ChpSettleReads = pPriv->OldParam.ChpSettleReads ;
            pPriv->CurrParam.ChpCoadds      = pPriv->OldParam.ChpCoadds ;
            pPriv->CurrParam.Savesets       = pPriv->OldParam.Savesets ;
            pPriv->CurrParam.NodSettleReads = pPriv->OldParam.NodSettleReads ;
            pPriv->CurrParam.NodSets        = pPriv->OldParam.NodSets ;
            pPriv->CurrParam.NodSettleChops = pPriv->OldParam.NodSettleChops ;
            pPriv->CurrParam.PreValidChops  = pPriv->OldParam.PreValidChops ;
            pPriv->CurrParam.PostValidChops = pPriv->OldParam.PostValidChops ;
            pPriv->CurrParam.pixclock       = pPriv->OldParam.pixclock ;
            strcpy(pPriv->CurrParam.obs_mode,pPriv->OldParam.obs_mode) ;
            strcpy(pPriv->CurrParam.readout_mode,pPriv->OldParam.readout_mode) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for adjPhysG ******************/
long ufadjPhysGinit (genSubRecord *pgs) {
  strcpy(pgs->a,"") ;
  pgs->noa = 25 ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for adjHrdwrG ******************/
long ufadjPhysGproc (genSubRecord *pgs) {
  /* Variable Declarations */
  /* long status ; */
  /* char *remainder ; */
  char yy[40] ; 
  /* if (bingo) printf("I got %d elements \n",elem_cnt) ; */
  char *original ;
  /* int i ; */
  char *zz ;
  if (strcmp(pgs->a,"") != 0) {
    /* printf("@@@@@@@@@@@@@@@ I am getting %ld elements\n",pgs->noa) ;
       printf("@@@@@@@@@@@@@@@  The first element is %s\n",(char *)pgs->a) ; */
    /* status = unpack_any_array (pgs, (char *)pgs->a, pgs->noa,remainder) ; */
    original = pgs->a ; zz = pgs->a ; 
                strcpy(phys_phys_parm[0],zz) ;/* 1 */
    zz = zz+40; strcpy(phys_phys_parm[1],zz) ;/* 2 */
    zz = zz+40; strcpy(phys_phys_parm[2],zz) ;/* 3 */
    zz = zz+40; strcpy(phys_phys_parm[3],zz) ;/* 4 */
    zz = zz+40; strcpy(phys_phys_parm[4],zz) ;/* 5 */
    zz = zz+40; strcpy(phys_phys_parm[5],zz) ;/* 6 */
    zz = zz+40; strcpy(phys_phys_parm[6],zz) ;/* 7 */
    zz = zz+40; strcpy(phys_phys_parm[7],zz) ;/* 8 */
    zz = zz+40; strcpy(phys_phys_parm[8],zz) ;/* 9 */
    zz = zz+40; strcpy(phys_phys_parm[9],zz) ;/* 10 */
    zz = zz+40; strcpy(phys_phys_parm[10],zz) ;/* 11 */
    zz = zz+40; strcpy(phys_phys_parm[11],zz) ;/* 12 */
    zz = zz+40; strcpy(phys_phys_parm[12],zz) ;/* 13 */
    zz = zz+40; strcpy(phys_phys_parm[13],zz) ;/* 14 */
    zz = zz+40; strcpy(phys_phys_parm[14],zz) ;/* 15 */
    zz = zz+40; strcpy(phys_phys_parm[15],zz) ;/* 16 */
    zz = zz+40; strcpy(phys_phys_parm[16],zz) ;/* 17 */
    zz = zz+40; strcpy(phys_phys_parm[17],zz) ;/* 18 */
    zz = zz+40; strcpy(phys_phys_parm[18],zz) ;/* 19 */
    zz = zz+40; strcpy(phys_phys_parm[19],zz) ;/* 20 */
    zz = zz+40; strcpy(phys_phys_parm[20],zz) ;/* 21 */
    zz = zz+40; strcpy(phys_phys_parm[21],zz) ;/* 22 */
    zz = zz+40; strcpy(phys_phys_parm[22],zz) ;/* 23 */
    zz = zz+40; strcpy(phys_phys_parm[23],zz) ;/* 24 */
    zz = zz+40; strcpy(phys_phys_parm[24],zz) ;/* 25 */
    
    pgs->valu = phys_phys_parm[0] ;

    zz = pgs->a ; 
                strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 Frame Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 Save Frequency*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 Exposure Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 Chop Frequency*/
    zz = zz+40; /* 5 Chop settle time */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6 Nod Dwel time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 Nod Settle Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 8 SCS Duty Cycle MCE4 Chop Duty Cycle*/
    zz = zz+40; /* 9 MCE4 Nod Duty Cycle */
    zz = zz+40; /* 10 SCS Frequency */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valj,yy) ; /* 11 Pre Valid Chop Time*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 Post Valid Chop time*/
    zz = zz+40; /* 13 Frame Duty Cycle*/
    zz = zz+40; /* 14 Frame Coadd */
    zz = zz+40; /* 15 Chop Settle Read */
    zz = zz+40; /* 16 Chop Coadd Chop */
    zz = zz+40; /* 17 Nod Settle Chop */
    zz = zz+40; /* 18 Nod Settle Read */
    zz = zz+40; /* 19 Save Sets */
    zz = zz+40; /* 20 Nod Sets */
    zz = zz+40; /* 21 Pix Clock */
    zz = zz+40; /* 22 Pre Chops */
    zz = zz+40; /* 23 Post Chops */
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 24 Obs Mode*/
    zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 25 Readout Mode*/

    strcpy(pgs->valk, "1") ;
    strcpy(pgs->valt, "START") ;
    strcpy(pgs->a,"") ;
    pgs->noa = 25 ;
  } else {
    strcpy(pgs->valk, "0") ;
    strcpy(pgs->valt, "CLEAR") ;
  }
  return OK ;
}



/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** Unpacks array of strings ***************/
long assign_adj_phys_param(genSubRecord *pgs, char *zz) {

  char *original ;
  original = zz ; 
   
  strcpy(adjPhysParm[0],zz) ;/* 1 */
  zz = zz+40; strcpy(adjPhysParm[1],zz) ;/* 2 */
  zz = zz+40; strcpy(adjPhysParm[2],zz) ;/* 3 */
  zz = zz+40; strcpy(adjPhysParm[3],zz) ;/* 4 */
  zz = zz+40; strcpy(adjPhysParm[4],zz) ;/* 5 */
  zz = zz+40; strcpy(adjPhysParm[5],zz) ;/* 6 */
  zz = zz+40; strcpy(adjPhysParm[6],zz) ;/* 7 */
  zz = zz+40; strcpy(adjPhysParm[7],zz) ;/* 8 */
  zz = zz+40; strcpy(adjPhysParm[8],zz) ;/* 9 */
  zz = zz+40; strcpy(adjPhysParm[9],zz) ;/* 10 */
  zz = zz+40; strcpy(adjPhysParm[10],zz) ;/* 11 */
  zz = zz+40; strcpy(adjPhysParm[11],zz) ;/* 12 */
  zz = zz+40; strcpy(adjPhysParm[12],zz) ;/* 13 */
  zz = zz+40; strcpy(adjPhysParm[13],zz) ;/* 14 */
  zz = zz+40; strcpy(adjPhysParm[14],zz) ;/* 15 */
  zz = zz+40; strcpy(adjPhysParm[15],zz) ;/* 16 */
  zz = zz+40; strcpy(adjPhysParm[16],zz) ;/* 17 */
  zz = zz+40; strcpy(adjPhysParm[17],zz) ;/* 18 */
  zz = zz+40; strcpy(adjPhysParm[18],zz) ;/* 19 */
  zz = zz+40; strcpy(adjPhysParm[19],zz) ;/* 20 */
  zz = zz+40; strcpy(adjPhysParm[20],zz) ;/* 21 */
  zz = zz+40; strcpy(adjPhysParm[21],zz) ;/* 22 */
  zz = zz+40; strcpy(adjPhysParm[22],zz) ;/* 23 */
  zz = zz+40; strcpy(adjPhysParm[23],zz) ;/* 24 */
  zz = zz+40; strcpy(adjPhysParm[24],zz) ;/* 25 */

  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** Unpacks array of strings ***************/
long assign_adj_meta_param(genSubRecord *pgs, char *zz) {

  int i ;
  char *original ;
  original = zz ;

  strcpy(adjMetaParm[0],zz) ;/* 1 */
  adjMetaParm[0][strlen(adjMetaParm[0])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[1],zz) ;/* 2 */
  adjMetaParm[1][strlen(adjMetaParm[1])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[2],zz) ;/* 3 */
  adjMetaParm[2][strlen(adjMetaParm[2])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[3],zz) ;/* 4 */
  adjMetaParm[3][strlen(adjMetaParm[3])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[4],zz) ;/* 5 */
  adjMetaParm[4][strlen(adjMetaParm[4])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[5],zz) ;/* 6 */
  adjMetaParm[5][strlen(adjMetaParm[5])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[6],zz) ;/* 7 */
  adjMetaParm[6][strlen(adjMetaParm[6])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[7],zz) ;/* 8 */
  adjMetaParm[7][strlen(adjMetaParm[7])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[8],zz) ;/* 9 */
  adjMetaParm[8][strlen(adjMetaParm[8])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[9],zz) ;/* 10 */
  adjMetaParm[9][strlen(adjMetaParm[9])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[10],zz) ;/* 11 */
  adjMetaParm[10][strlen(adjMetaParm[10])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[11],zz) ;/* 12 */
  adjMetaParm[11][strlen(adjMetaParm[11])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[12],zz) ;/* 13 */
  adjMetaParm[12][strlen(adjMetaParm[12])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[13],zz) ;/* 14 */
  adjMetaParm[13][strlen(adjMetaParm[13])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[14],zz) ;/* 15 */
  adjMetaParm[14][strlen(adjMetaParm[14])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[15],zz) ;/* 16 */
  adjMetaParm[15][strlen(adjMetaParm[15])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[16],zz) ;/* 17 */
  adjMetaParm[16][strlen(adjMetaParm[16])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[17],zz) ;/* 18 */
  adjMetaParm[17][strlen(adjMetaParm[17])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[18],zz) ;/* 19 */
  adjMetaParm[18][strlen(adjMetaParm[18])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[19],zz) ;/* 20 */
  adjMetaParm[19][strlen(adjMetaParm[19])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[20],zz) ;/* 21 */
  adjMetaParm[20][strlen(adjMetaParm[20])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[21],zz) ;/* 22 */
  adjMetaParm[21][strlen(adjMetaParm[21])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[22],zz) ;/* 23 */
  adjMetaParm[22][strlen(adjMetaParm[22])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[23],zz) ;/* 24 */
  adjMetaParm[23][strlen(adjMetaParm[23])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[24],zz) ;/* 25 */
  adjMetaParm[24][strlen(adjMetaParm[24])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[25],zz) ;/* 26 */
  adjMetaParm[25][strlen(adjMetaParm[25])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[26],zz) ;/* 27 */
  adjMetaParm[26][strlen(adjMetaParm[26])] = '\0' ;
  zz = zz+40; strcpy(adjMetaParm[27],zz) ;/* 28 */
  adjMetaParm[27][strlen(adjMetaParm[27])] = '\0' ;

  if (bingo) 
    for (i=0;i<28;i++) {
      adjMetaParm[i][39] = '\0' ;
      printf("Meta item # %d is: @%s@\n",i,adjMetaParm[i]) ;
      
    }
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** Unpacks array of strings ***************/
long unpack_phys_param (genSubRecord *pgs, char *zz, char **remainder) {

  char yy[40] ;
  char *original ;
  int i;
  original = zz ; 
              strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vald,yy) ; /* 4 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vale,yy) ; /* 5 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valf,yy) ; /* 6  */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valg,yy) ; /* 7 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valh,yy) ; /* 8 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vali,yy) ; /* 9 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valj,yy) ; /* 10 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valk,yy) ; /* 11 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vall,yy) ; /* 12 */
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valm,yy) ; /* 13 */

  zz = zz+40; strcpy(hrdware_parm[0],zz) ;/* 14 */
  zz = zz+40; strcpy(hrdware_parm[1],zz) ;/* 15 */
  zz = zz+40; strcpy(hrdware_parm[2],zz) ;/* 16 */
  zz = zz+40; strcpy(hrdware_parm[3],zz) ;/* 17 */
  zz = zz+40; strcpy(hrdware_parm[4],zz) ;/* 18 */
  zz = zz+40; strcpy(hrdware_parm[5],zz) ;/* 19 */
  zz = zz+40; strcpy(hrdware_parm[6],zz) ;/* 20 */
  zz = zz+40; strcpy(hrdware_parm[7],zz) ;/* 21 */
  zz = zz+40; strcpy(hrdware_parm[8],zz) ;/* 22 */
  zz = zz+40; strcpy(hrdware_parm[9],zz) ;/* 23 */
  zz = zz+40; strcpy(hrdware_parm[10],zz) ;/* 24 */
  zz = zz+40; strcpy(hrdware_parm[11],zz) ;/* 25 */
  if (bingo) for (i=0;i<12;i++) printf("Hardware item # %d is: %s\n",i,hrdware_parm[i]) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for physicalG ******************/
long ufphysicalGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDCPhysicalPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCPhysicalPrivate *) malloc (sizeof(genSubDCPhysicalPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */

  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  pPriv->CurrParam.FrameTime = -1.0;
  pPriv->CurrParam.saveFrq= -1.0 ;
  pPriv->CurrParam.exposureTime= -1.0 ;
  pPriv->CurrParam.ChopFreq= -1.0 ;
  pPriv->CurrParam.SCSDutyCycle= -1.0 ;
  pPriv->CurrParam.nodDwelTime= -1.0 ;
  pPriv->CurrParam.nodStlTime= -1.0 ;
  pPriv->CurrParam.preValidChopTime = -1.0;
  pPriv->CurrParam.postValidChopTime = -1.0;
  /* pPriv->CurrParam.chopDutyCycle = -1.0; */

  
  strcpy(pPriv->CurrParam.obs_mode,"") ;
  strcpy(pPriv->CurrParam.readout_mode,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /*  */
  *(double *)pgs->b = -1.0 ; /*  */
  *(double *)pgs->c = -1.0 ; /*  */
  *(double *)pgs->d = -1.0 ; /*  */
  *(double *)pgs->e = -1.0 ; /*  */
  *(double *)pgs->f = -1.0 ; /*  */
  *(double *)pgs->g = -1.0 ; /*  */
  *(double *)pgs->h = -1.0 ; /*  */
  *(long *)pgs->l = -1 ;
  strcpy(pgs->i,"") ;
  strcpy(pgs->j,"") ;
  strcpy(pgs->k,"") ; /*  */
  *(double *)pgs->m = -1.0 ; /*  */
  *(double *)pgs->n = -1.0 ; /*  */
  *(double *)pgs->o = -1.0 ; /*  */
  *(double *)pgs->p = -1.0 ; /*  */
  /* strcpy(pgs->l,"") ;*/ /*  */
  /* *(long *)pgs->m = -1 ;  */


  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
 

  pPriv->dcPhysical_inp.command_mode = -1;
  pPriv->dcPhysical_inp.FrameTime = -1.0;
  pPriv->dcPhysical_inp.saveFrq= -1.0 ;
  pPriv->dcPhysical_inp.exposureTime= -1.0 ;
  pPriv->dcPhysical_inp.ChopFreq= -1.0 ;
  pPriv->dcPhysical_inp.SCSDutyCycle= -1.0 ;
  pPriv->dcPhysical_inp.nodDwelTime= -1.0 ;
  pPriv->dcPhysical_inp.nodStlTime= -1.0 ;
  pPriv->dcPhysical_inp.preValidChopTime = -1.0;
  pPriv->dcPhysical_inp.postValidChopTime =-1.0 ;
  /* pPriv->dcPhysical_inp.chopDutyCycle = -1.0; */
  
  strcpy(pPriv->dcPhysical_inp.obs_mode,"") ;
  strcpy(pPriv->dcPhysical_inp.readout_mode,"") ;

  pPriv->dcPhysical_inp.pFlag = -1 ;
  
  pPriv->paramLevel = 'P' ;
  pgs->noj = 25 ;

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pgs->dpvt = (void *) pPriv;
  
  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for physicalG *******************/
long ufphysicalGproc (genSubRecord *pgs) {

  genSubDCPhysicalPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;

  double numnum ;
  

  /* strcpy(dc_DEBUG_MODE,pgs->m) ; */

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCPhysicalPrivate *)pgs->dpvt ;
  /* printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      if (!dc_initialized)  init_dc_config() ;
      /* read_dc_file(pgs->name, (genSubCommPrivate *)pPriv) ; */ /* ZZZ */

      /* pPriv->port_no = dc_port_no ; */  /* ZZZ */

      /* Clear outpus A-J for */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      *(double *)pgs->valc = -1.0 ;
      *(double *)pgs->vald = -1.0 ;
      *(double *)pgs->vale = -1.0 ;
      *(double *)pgs->valf = -1.0 ;
      *(double *)pgs->valg = -1.0 ;
      *(double *)pgs->valh = -1.0 ;
      *(double *)pgs->valk = -1.0 ;
      *(double *)pgs->vall = -1.0 ;
      strcpy(pgs->vali,"") ;
      strcpy(pgs->valj,"") ;
      strcpy(pgs->valm,"") ;
      strcpy(pgs->valn,"") ;
      strcpy(pgs->valo,"") ;
      strcpy(pgs->valp,"") ;
      strcpy(pgs->valq,"") ;
      strcpy(pgs->valr,"") ;
      strcpy(pgs->vals,"") ;
      strcpy(pgs->valt,"") ;

      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */ /* ZZZ */
      pPriv->socfd = -1 ;  /* ZZZ */
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
	} */ /* ZZZ */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      
      pPriv->dcPhysical_inp.command_mode = *(long *)pgs->a;
      pPriv->dcPhysical_inp.FrameTime = *(double *)pgs->b ;
      pPriv->dcPhysical_inp.saveFrq= *(double *)pgs->c ; 
      pPriv->dcPhysical_inp.exposureTime= *(double *)pgs->d ;
      pPriv->dcPhysical_inp.ChopFreq= *(double *)pgs->e ;
      pPriv->dcPhysical_inp.SCSDutyCycle= *(double *)pgs->f  ;
      pPriv->dcPhysical_inp.nodDwelTime= *(double *)pgs->g ;
      pPriv->dcPhysical_inp.nodStlTime= *(double *)pgs->h ;
      /* pPriv->dcPhysical_inp.chopDutyCycle =*(double *)pgs->l ; */
  
      strcpy(pPriv->dcPhysical_inp.obs_mode,pgs->i) ;
      strcpy(pPriv->dcPhysical_inp.readout_mode,pgs->k) ;

      pPriv->dcPhysical_inp.pFlag = *(long *)pgs->l ;
      pPriv->dcPhysical_inp.preValidChopTime = *(double *)pgs->m;
      pPriv->dcPhysical_inp.postValidChopTime = *(double *)pgs->n ;

      /*
      printf("My inputs are ......\n") ;
      printf("Link A %ld\n",*(long *)pgs->a ) ;
      printf("Link B %f\n",*(double *)pgs->b ) ;
      printf("Link C %f\n",*(double *)pgs->c ) ;
      printf("Link D %f\n",*(double *)pgs->d ) ;
      printf("Link E %f\n",*(double *)pgs->e ) ;
      printf("Link F %f\n",*(double *)pgs->f ) ;
      printf("Link G %f\n",*(double *)pgs->g ) ;
      printf("Link H %f\n",*(double *)pgs->h ) ;
      printf("Link I %s\n",(char *)pgs->i ) ;
      printf("Link K %s\n",(char *)pgs->k ) ;
      printf("Link L %ld\n",*(long *)pgs->l ) ; */
      /* printf("Link M %ld\n",*(long *)pgs->m ) ;*/


      if ( (pPriv->dcPhysical_inp.command_mode != -1) ||
	   (pPriv->dcPhysical_inp.FrameTime != -1.0) ||
	   (pPriv->dcPhysical_inp.saveFrq != -1.0) ||
	   (pPriv->dcPhysical_inp.exposureTime != -1.0) ||
	   (pPriv->dcPhysical_inp.ChopFreq != -1.0) ||
	   (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0) ||
	   (pPriv->dcPhysical_inp.nodDwelTime != -1.0) ||
           (pPriv->dcPhysical_inp.preValidChopTime != -1.0) ||
           (pPriv->dcPhysical_inp.postValidChopTime != -1.0) ||
	   (pPriv->dcPhysical_inp.nodStlTime != -1.0) ||
	   /*(pPriv->dcPhysical_inp.chopDutyCycle != -1.0) || */

           (strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0) ||
           (strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;


        *(long *)pgs->a = -1 ; /*  */
        *(double *)pgs->b = -1.0 ; /*  */
        *(double *)pgs->c = -1.0 ; /*  */
        *(double *)pgs->d = -1.0 ; /*  */
        *(double *)pgs->e = -1.0 ; /*  */
        *(double *)pgs->f = -1.0 ; /*  */
        *(double *)pgs->g = -1.0 ; /*  */
        *(double *)pgs->h = -1.0 ; /*  */
        *(double *)pgs->i = -1.0 ; /*  */
        *(double *)pgs->m = -1.0 ; /*  */
        *(double *)pgs->n = -1.0 ; /*  */
        strcpy(pgs->j,"") ;
        strcpy(pgs->k,"") ; /*  */
        pgs->noj = 25 ;
        strcpy(pgs->l,"") ; /*  */
        *(long *)pgs->m = -1 ; /*  */

      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        pgs->noj = 25 ;
        /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 25 ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* printf("My Flag is %ld and the ADJUSTED input thingy is %d\n",pPriv->dcPhysical_inp.pFlag, ADJUSTED_INPUT) ; */
      /* Is this a CALLBACK to send a command or do an INIT or process adjusted input? */
      if (pPriv->dcPhysical_inp.pFlag == 1) {
        /* get the input into the current parameters */
        if (pPriv->dcPhysical_inp.FrameTime != -1.0)  
          pPriv->CurrParam.FrameTime = pPriv->dcPhysical_inp.FrameTime  ;
        if (pPriv->dcPhysical_inp.saveFrq != -1.0)  
          pPriv->CurrParam.saveFrq = pPriv->dcPhysical_inp.saveFrq  ;
        if (pPriv->dcPhysical_inp.exposureTime != -1.0)  
          pPriv->CurrParam.exposureTime = pPriv->dcPhysical_inp.exposureTime  ;
        if (pPriv->dcPhysical_inp.ChopFreq != -1.0)  
          pPriv->CurrParam.ChopFreq = pPriv->dcPhysical_inp.ChopFreq  ;
        if (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0)  
          pPriv->CurrParam.SCSDutyCycle = pPriv->dcPhysical_inp.SCSDutyCycle  ;
        if (pPriv->dcPhysical_inp.nodDwelTime != -1.0)  
          pPriv->CurrParam.nodDwelTime = pPriv->dcPhysical_inp.nodDwelTime  ;
        if (pPriv->dcPhysical_inp.nodStlTime != -1.0)  
          pPriv->CurrParam.nodStlTime = pPriv->dcPhysical_inp.nodStlTime  ;
        if (pPriv->dcPhysical_inp.preValidChopTime != -1.0)
          pPriv->CurrParam.preValidChopTime = pPriv->dcPhysical_inp.preValidChopTime ;
        if (pPriv->dcPhysical_inp.postValidChopTime != -1.0)
          pPriv->CurrParam.preValidChopTime = pPriv->dcPhysical_inp.postValidChopTime ;
	/*        if (pPriv->dcPhysical_inp.chopDutyCycle != -1.0)  
		  pPriv->CurrParam.chopDutyCycle = pPriv->dcPhysical_inp.chopDutyCycle ; */

        if(strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0)
          strcpy(pPriv->CurrParam.obs_mode,pPriv->dcPhysical_inp.obs_mode) ;
        if(strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0)
          strcpy(pPriv->CurrParam.readout_mode,pPriv->dcPhysical_inp.readout_mode) ;

        /* update the output links */
        *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
        *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
        *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
        *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
        /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
        *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
        *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ; 
        *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
        *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;

        strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
        strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;

        /* Reset Command state to DONE */
        pPriv->commandState = TRX_GS_DONE;
        some_num = CAR_IDLE ;
        status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                     &some_num,1);
        if (status) {
          trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
      }
      else { /* kkkkkk */
        if (pPriv->dcPhysical_inp.command_mode != -1) { /* We have an init command */
          trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ; 
          /* Check first to see if we have a valid INIT directive */
          if ((pPriv->dcPhysical_inp.command_mode == SIMM_NONE) ||
              (pPriv->dcPhysical_inp.command_mode == SIMM_FAST) ||
              (pPriv->dcPhysical_inp.command_mode == SIMM_FULL) ) {
            /* need to reconnect to the agent ? -- hon */
	    if( pPriv->socfd >= 0 ) {
	      if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	        printf("ufphysicalGproc> socket is writable, no need to reconnect...\n");
	      }
	      else {
                trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
                ufClose(pPriv->socfd) ;
                pPriv->socfd = -1;
                *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
	    }
             pPriv->CurrParam.command_mode = pPriv->dcPhysical_inp.command_mode; /* simulation level */
            *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
            /* Re-Connect if needed */
            printf("My Command Mode is %ld \n",pPriv->dcPhysical_inp.command_mode) ;
            /* Read some initial parameters from a file */
            /*
            pPriv->CurrParam.init_velocity = -1.0 ; 
            pPriv->CurrParam.slew_velocity = -1.0 ; 
            pPriv->CurrParam.acceleration = -1.0 ; 
            pPriv->CurrParam.deceleration = -1.0 ; 
            pPriv->CurrParam.drive_current= -1.0 ; 
            pPriv->CurrParam.datum_speed = -1 ; 
            pPriv->CurrParam.datum_direction = -1 ; 
            */
            /* if (read_dc_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ; */
            read_dc_file(pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel) ;
            pPriv->port_no = dc_port_no ;
 
            if (pPriv->dcPhysical_inp.command_mode != SIMM_FAST) {
	      if( pPriv->socfd < 0 ) {
                trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
                pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
                trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
	        *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	      }
              if (pPriv->socfd < 0) { /* we have a bad socket connection */
                pPriv->commandState = TRX_GS_DONE;
                strcpy (pPriv->errorMessage, "Error Connecting to Agent");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            }
            /* update output links if connection is good or we are in SIMM_FAST*/
            if ( (pPriv->dcPhysical_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
              /* update the output links */
              /* update the output links */
              *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
              *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
              *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
              *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
              /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
              *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
              *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;
              strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;
              *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
              *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;  
              /* Reset Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
              if (status) {
                trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              } 
            }
	  } else { /* we have an error in input of the init command */
            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            strcpy (pPriv->errorMessage, "Bad INIT Directive");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
          strcpy(pgs->j,"") ;
          strcpy(pgs->valu,"") ;
          pgs->novu = 25 ;
          pgs->noj = 25 ; 
        } else { /* We have a configuration command */
          trx_debug("We have a configuration Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
          /* Save the current parameters */


          pPriv->OldParam.FrameTime     = pPriv->CurrParam.FrameTime ;
          pPriv->OldParam.saveFrq       = pPriv->CurrParam.saveFrq ;
          pPriv->OldParam.exposureTime  = pPriv->CurrParam.exposureTime ;
          pPriv->OldParam.ChopFreq      = pPriv->CurrParam.ChopFreq ;
          pPriv->OldParam.SCSDutyCycle   = pPriv->CurrParam.SCSDutyCycle ;
          pPriv->OldParam.nodDwelTime   = pPriv->CurrParam.nodDwelTime ;
          pPriv->OldParam.nodStlTime    = pPriv->CurrParam.nodStlTime ;
          /* pPriv->OldParam.chopDutyCycle = pPriv->CurrParam.chopDutyCycle ; */
          pPriv->OldParam.preValidChopTime = pPriv->CurrParam.preValidChopTime ;
          pPriv->OldParam.postValidChopTime = pPriv->CurrParam.postValidChopTime ;
          strcpy(pPriv->OldParam.obs_mode,pPriv->CurrParam.obs_mode) ;
          strcpy(pPriv->OldParam.readout_mode,pPriv->CurrParam.readout_mode) ;

          /* get the input into the current parameters */
          if (pPriv->dcPhysical_inp.FrameTime != -1.0)  
            pPriv->CurrParam.FrameTime = pPriv->dcPhysical_inp.FrameTime  ;
          if (pPriv->dcPhysical_inp.saveFrq != -1.0)  
            pPriv->CurrParam.saveFrq = pPriv->dcPhysical_inp.saveFrq  ;
          if (pPriv->dcPhysical_inp.exposureTime != -1.0)  
            pPriv->CurrParam.exposureTime = pPriv->dcPhysical_inp.exposureTime  ;
          if (pPriv->dcPhysical_inp.ChopFreq != -1.0)  
            pPriv->CurrParam.ChopFreq = pPriv->dcPhysical_inp.ChopFreq  ;
          if (pPriv->dcPhysical_inp.SCSDutyCycle != -1.0)  
            pPriv->CurrParam.SCSDutyCycle = pPriv->dcPhysical_inp.SCSDutyCycle  ;
          if (pPriv->dcPhysical_inp.nodDwelTime != -1.0)  
            pPriv->CurrParam.nodDwelTime = pPriv->dcPhysical_inp.nodDwelTime  ;
          if (pPriv->dcPhysical_inp.nodStlTime != -1.0)  
            pPriv->CurrParam.nodStlTime = pPriv->dcPhysical_inp.nodStlTime  ;
          if (pPriv->dcPhysical_inp.preValidChopTime != -1.0)
            pPriv->CurrParam.preValidChopTime= pPriv->dcPhysical_inp.preValidChopTime ;
          if (pPriv->dcPhysical_inp.postValidChopTime != -1.0)
            pPriv->CurrParam.postValidChopTime = pPriv->dcPhysical_inp.postValidChopTime ;

	  /*          if (pPriv->dcPhysical_inp.chopDutyCycle != -1.0)  
		      pPriv->CurrParam.chopDutyCycle = pPriv->dcPhysical_inp.chopDutyCycle ; */

          if(strcmp(pPriv->dcPhysical_inp.obs_mode,"") != 0)
            strcpy(pPriv->CurrParam.obs_mode,pPriv->dcPhysical_inp.obs_mode) ;
          if(strcmp(pPriv->dcPhysical_inp.readout_mode,"") != 0)
            strcpy(pPriv->CurrParam.readout_mode,pPriv->dcPhysical_inp.readout_mode) ;

          /* see if this a command right after an init */
          /*
          if (pPriv->send_init_param) {
            pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
            pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
            pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
            pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
            pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current;
            pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
            pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ; 
            pPriv->send_init_param = 0 ; 
          }
          */
          /* formulate the command strings */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;
  
          num_str = UFcheck_dc_Phys_inputs (pPriv->dcPhysical_inp,com, pPriv->paramLevel, rec_name,&pPriv->CurrParam, 
                                             pPriv->CurrParam.command_mode) ;

          /* Clear the input structure */
          pPriv->dcPhysical_inp.command_mode = -1;
          pPriv->dcPhysical_inp.FrameTime = -1.0;
          pPriv->dcPhysical_inp.saveFrq= -1.0 ;
          pPriv->dcPhysical_inp.exposureTime= -1.0 ;
          pPriv->dcPhysical_inp.ChopFreq= -1.0 ;
          pPriv->dcPhysical_inp.SCSDutyCycle= -1.0 ;
          pPriv->dcPhysical_inp.nodDwelTime= -1.0 ;
          pPriv->dcPhysical_inp.nodStlTime = -1.0 ;
          pPriv->dcPhysical_inp.preValidChopTime = -1.0 ;
          pPriv->dcPhysical_inp.postValidChopTime = -1.0 ;
          /* pPriv->dcPhysical_inp.chopDutyCycle = -1.0; */
  
          strcpy(pPriv->dcPhysical_inp.obs_mode,"") ;
          strcpy(pPriv->dcPhysical_inp.readout_mode,"") ;

          pPriv->dcPhysical_inp.pFlag = 1 ;

 
          /* do we have commands to send? */
          if (num_str > 0) {
            /* if no Agent needed then go back to DONE */
            /* we are talking about SIMM_FAST or commands that do not need the agent */
            printf("********** Thn number of strings is : %d\n",num_str) ; 
	     if (bingo) for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;  

            if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* update the output links */


              /* update the output links */
              *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
              *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
              *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
              *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
              /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
              *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
              *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;

              strcpy(pgs->vali, pPriv->CurrParam.obs_mode) ;
              strcpy(pgs->valj, pPriv->CurrParam.readout_mode) ;
              *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
              *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, 
                                   DBR_LONG, &some_num, 1);
              if (status) {
                logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
                return OK;
              }
            } else { /* we are in SIMM_NONE or SIMM_FULL */
              /* See if you can send the command and go to BUSY state */
              /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
              if (pPriv->socfd > 0) {
		if( _verbose )
		  printf("ufphysicalGproc> sending to agent, genSub record: %s\n", pgs->name); 
		status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
              }
	      else {
		printf("ufphysicalGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
		status = 0;
	      } 

              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              if (status == 0) { /* there was an error sending the commands to the agent */
                /* set Command state to DONE */
                pPriv->commandState = TRX_GS_DONE;
                /* set the CAR to ERR with appropriate message like 
                 * "Bad socket"  and go to DONE state*/
                strcpy (pPriv->errorMessage, "Bad socket connection");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else { /* send successful */
                /* establish a CALLBACK */
                requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
                pPriv->startingTicks = tickGet() ;
                /* set Command state to BUSY */
                pPriv->commandState = TRX_GS_BUSY;
              }
            }
          } else {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (num_str < 0) {
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* we have an error in the Input */
              strcpy (pPriv->errorMessage, "Error in input");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* num_str == 0 */
              /* is it possible to get here? */
              /* here we have a change in input.  The inputs though got sent
               * to be checked but no string got formulated.  */
              /* this can only happen if somehow the memory got corrupted */
              /* or the gensub is being processed by an alien. */
              /* Weird. */
              trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
            }
          }
        }
      
        /*Is this a J field processing? */
        strcpy(response,pgs->j) ;
        pgs->noj = 25 ;
        if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 25 ;
      } /* kkkkkkkkkkkkkk */
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /* 
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) { 
        cancelCallback (pPriv->pCallback);  
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ; 
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;  
        strcpy(pgs->i,"") ; 
        strcpy(pgs->k,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;   
        pPriv->commandState = TRX_GS_SENDING;    
        return OK ;   
	} */

      strcpy(response,pgs->j) ;
      pgs->noj = 25 ;
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"FULL",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "DC P Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
        
          /* update the output links */

          *(double *)pgs->valc = pPriv->CurrParam.FrameTime ;
          *(double *)pgs->vald = pPriv->CurrParam.saveFrq ;
          *(double *)pgs->vale = pPriv->CurrParam.exposureTime ;
          *(double *)pgs->valf = pPriv->CurrParam.ChopFreq ;
          /* *(double *)pgs->valg = pPriv->CurrParam.SCSDutyCycle ; */
          *(double *)pgs->valg = pPriv->CurrParam.nodDwelTime ;
          *(double *)pgs->valh = pPriv->CurrParam.nodStlTime ;
          strcpy(pgs->vali,pPriv->CurrParam.obs_mode) ;
          strcpy(pgs->valj,pPriv->CurrParam.readout_mode) ;
          *(double *)pgs->valk = pPriv->CurrParam.preValidChopTime ;
          *(double *)pgs->vall = pPriv->CurrParam. postValidChopTime ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */

            pPriv->CurrParam.FrameTime     = pPriv->OldParam.FrameTime ;
            pPriv->CurrParam.saveFrq       = pPriv->OldParam.saveFrq ;
            pPriv->CurrParam.exposureTime  = pPriv->OldParam.exposureTime ;
            pPriv->CurrParam.ChopFreq      = pPriv->OldParam.ChopFreq ;
            pPriv->CurrParam.SCSDutyCycle   = pPriv->OldParam.SCSDutyCycle ;
            pPriv->CurrParam.nodDwelTime   = pPriv->OldParam.nodDwelTime ;
            pPriv->CurrParam.nodStlTime    = pPriv->OldParam.nodStlTime ;
            /* pPriv->CurrParam.chopDutyCycle = pPriv->OldParam.chopDutyCycle ; */
            pPriv->CurrParam.preValidChopTime = pPriv->OldParam.preValidChopTime;
            pPriv->CurrParam.postValidChopTime = pPriv->OldParam.postValidChopTime;

            strcpy(pPriv->CurrParam.obs_mode,pPriv->OldParam.obs_mode) ;
            strcpy(pPriv->CurrParam.readout_mode,pPriv->OldParam.readout_mode) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            /* unpack_phys_param (pgs, (char *)pgs->j,(char **)hrdware_parm) ; */
            /* for (i = 0; i<12; i++) printf("Element %d is: %s \n",i,hrdware_parm[i]) ; */
            assign_adj_phys_param( pgs, (char *)pgs->j) ;
            pgs->novu = 25 ;
            pgs->valu = adjPhysParm[0] ;
	    pgs->noj = 25 ;
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 25 ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** INAM for physOutGGinit *****************/
long ufphysOutGGinit (genSubRecord *pgs) {
  
  strcpy (pgs->a,"") ;
  pgs->noa = 25 ;
  
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** INAM for physOutGGinit *****************/
long ufphysOutGGproc (genSubRecord *pgs) {
  
  unpack_phys_param (pgs, (char *)pgs->a,(char **)hrdware_parm ) ;
  pgs->novu = 12;
  pgs->valu = hrdware_parm[0] ;
  strcpy (pgs->a,"") ;
  pgs->noa = 25 ;
  
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** INAM for acqControlG *******************/
long ufacqControlGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDCAcqContPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
 
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCAcqContPrivate *) malloc (sizeof(genSubDCAcqContPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  if (strstr(pgs->name,"miri") != NULL) strcpy (buffer, "miri:observe"); 
  else strcpy (buffer, "trecs:observe"); 
  /* buffer[strlen(buffer) - 1] = '\0'; */
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->ObserveCcarinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  if (strstr(pgs->name,"miri") != NULL) strcpy (buffer, "miri:observe"); 
  else strcpy (buffer, "trecs:observe"); 
  /* strcpy (buffer, pgs->name);
     buffer[strlen(buffer) - 1] = '\0'; */
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->ObserveCcarinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }

  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  strcpy(pPriv->CurrParam.command,"") ;
  strcpy(pPriv->CurrParam.obs_id,"") ;
  strcpy(pPriv->CurrParam.dhs_write,"") ;
  strcpy(pPriv->CurrParam.qckLk_id,"") ;
  strcpy(pPriv->CurrParam.local_archive,"") ;
  strcpy(pPriv->CurrParam.nod_handshake,"") ;
  /*  strcpy(pPriv->CurrParam.archive_host,"") ; */
  strcpy(pPriv->CurrParam.archive_path,"") ;
  strcpy(pPriv->CurrParam.archive_file_name,"") ;
  strcpy(pPriv->CurrParam.comment,"") ;
  strcpy(pPriv->CurrParam.remote_archive,"") ;
  strcpy(pPriv->CurrParam.rem_archive_host,"") ;
  strcpy(pPriv->CurrParam.rem_archive_path,"") ;
  strcpy(pPriv->CurrParam.rem_archive_file_name,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  strcpy((char *)pgs->a,"") ; /*  */
  strcpy((char *)pgs->b,"") ; /*  */
  strcpy((char *)pgs->c,"") ; /*  */
  strcpy((char *)pgs->d,"") ; /*  */
  strcpy((char *)pgs->e,"") ; /*  */
  strcpy((char *)pgs->f,"") ; /*  */
  strcpy((char *)pgs->g,"") ; /*  */
  strcpy((char *)pgs->h,"") ; /*  */
  strcpy((char *)pgs->i,"") ; /*  */
  strcpy((char *)pgs->k,"") ; /*  */
  strcpy((char *)pgs->l,"") ; /*  */ 
  strcpy((char *)pgs->m,"") ; /*  */
  strcpy((char *)pgs->n,"") ; /*  */
  strcpy((char *)pgs->o,"") ; /*  */
  strcpy((char *)pgs->p,"") ; /*  */
  strcpy((char *)pgs->q,"") ; /*  */
  strcpy((char *)pgs->r,"") ; /*  */
  strcpy((char *)pgs->s,"") ; /*  */


  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->dcAcqCont_inp.command_mode = -1  ;
  strcpy(pPriv->dcAcqCont_inp.command,"") ;
  strcpy(pPriv->dcAcqCont_inp.obs_id,"") ;
  strcpy(pPriv->dcAcqCont_inp.dhs_write,"") ;
  strcpy(pPriv->dcAcqCont_inp.qckLk_id,"") ;
  strcpy(pPriv->dcAcqCont_inp.local_archive,"") ;
  strcpy(pPriv->dcAcqCont_inp.nod_handshake,"") ;
  /*  strcpy(pPriv->dcAcqCont_inp.archive_host,"") ; */
  strcpy(pPriv->dcAcqCont_inp.archive_path,"") ;
  strcpy(pPriv->dcAcqCont_inp.archive_file_name,"") ;
  strcpy(pPriv->dcAcqCont_inp.comment,"") ;
  strcpy(pPriv->dcAcqCont_inp.remote_archive,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_host,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_path,"") ;
  strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,"") ;

  pPriv->paramLevel = 'A' ;
  pPriv->observing = 0 ;
  pPriv->obs_time_out = 3600 ;
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for motorG *******************/
long ufacqControlGproc (genSubRecord *pgs) {

  genSubDCAcqContPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 
  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;
  char obs_mode[40] ;
  double onSrc_time , ChopDutyCycle, NodDutyCycle, Total_Time, FrameDutyCycle ;
  char temp_str[40];

  /* strcpy(dc_DEBUG_MODE,pgs->m) ; */

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCAcqContPrivate *)pgs->dpvt ;
  /* printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ;
	 printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!dc_initialized)  init_dc_config() ;
      /* read_dc_file(pgs->name, (genSubCommPrivate *)pPriv) ; */ /* ZZZ */

      /* pPriv->port_no = dc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy((char *)pgs->valc,"") ;
      strcpy((char *)pgs->vald,"") ;
      strcpy((char *)pgs->vale,"") ;
      strcpy((char *)pgs->valf,"") ;
      strcpy((char *)pgs->valg,"") ;
      strcpy((char *)pgs->valh,"") ;
      strcpy((char *)pgs->vali,"") ;
      strcpy((char *)pgs->valj,"") ;
      strcpy((char *)pgs->valk,"") ;
      strcpy((char *)pgs->vall,"") ;
      strcpy((char *)pgs->valm,"") ;
      strcpy((char *)pgs->valn,"") ;

      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);

      /* establish the connection and get the socket number */ 
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */
      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
      } */ /* ZZZ */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      
      
      strcpy(pPriv->dcAcqCont_inp.command,pgs->a) ;
      strcpy(pPriv->dcAcqCont_inp.obs_id,pgs->b) ;
      strcpy(pPriv->dcAcqCont_inp.dhs_write,pgs->c) ;
      strcpy(pPriv->dcAcqCont_inp.qckLk_id,pgs->d) ;
      if (strcmp((char *)pgs->e,"" ) == 0)  pPriv->dcAcqCont_inp.command_mode = -1 ;
      else {
        pPriv->dcAcqCont_inp.command_mode = (long)strtod(pgs->e,&endptr) ;
        if (*endptr != '\0'){
          pPriv->dcAcqCont_inp.command_mode = -1 ;
        }
      }
      strcpy(pPriv->dcAcqCont_inp.local_archive,pgs->f) ;
      strcpy(pPriv->dcAcqCont_inp.nod_handshake,pgs->g) ;
      /*      strcpy(pPriv->dcAcqCont_inp.archive_host,pgs->h) ; */
      strcpy(pPriv->dcAcqCont_inp.archive_path,pgs->h) ;
      strcpy(pPriv->dcAcqCont_inp.archive_file_name,pgs->i) ;
      strcpy(pPriv->dcAcqCont_inp.comment,pgs->k) ;
      strcpy(pPriv->dcAcqCont_inp.remote_archive,pgs->l) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_host,pgs->m) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_path,pgs->n) ;
      strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,pgs->o) ;
      /*
      printf("My inputs are ......\n") ;   
      printf("Link A %s\n",(char *)pgs->a ) ;   
      printf("Link B %s\n",(char *)pgs->b ) ;  
      printf("Link C %s\n",(char *)pgs->c ) ;  
      printf("Link D %s\n",(char *)pgs->d ) ;  
      printf("Link E %s\n",(char *)pgs->e ) ;  
      printf("Link F %s\n",(char *)pgs->f ) ;  
      printf("Link G %s\n",(char *)pgs->g ) ;  
      printf("Link H %s\n",(char *)pgs->h ) ;   
      printf("Link I %s\n",(char *)pgs->i ) ;  
      printf("Link K %s\n",(char *)pgs->k ) ;  
      printf("Link L %s\n",(char *)pgs->l ) ;  
      */
      
      if ( (strcmp(pPriv->dcAcqCont_inp.command,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.obs_id,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.dhs_write,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.qckLk_id,"") != 0) ||
           (pPriv->dcAcqCont_inp.command_mode != -1) ||
           (strcmp(pPriv->dcAcqCont_inp.local_archive,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.nod_handshake,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.archive_path,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.archive_file_name,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.comment,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.remote_archive,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_host,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_path,"") != 0) ||
           (strcmp(pPriv->dcAcqCont_inp.rem_archive_file_name,"")) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        strcpy((char *)pgs->a,"") ; /*  */
        strcpy((char *)pgs->b,"") ; /*  */
        strcpy((char *)pgs->c,"") ; /*  */
        strcpy((char *)pgs->d,"") ; /*  */
        strcpy((char *)pgs->e,"") ; /*  */
        strcpy((char *)pgs->f,"") ; /*  */
        strcpy((char *)pgs->g,"") ; /*  */
        strcpy((char *)pgs->h,"") ; /*  */
        strcpy((char *)pgs->i,"") ; /*  */
        strcpy((char *)pgs->k,"") ; /*  */
        strcpy((char *)pgs->l,"") ; /*  */
        strcpy((char *)pgs->m,"") ; /*  */
        strcpy((char *)pgs->n,"") ; /*  */
        strcpy((char *)pgs->o,"") ; /*  */
        /* strcpy((char *)pgs->p,"") ; 
        strcpy((char *)pgs->q,"") ; 
        strcpy((char *)pgs->r,"") ;  
        strcpy((char *)pgs->s,"") ;  */

      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->dcAcqCont_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->dcAcqCont_inp.command_mode == SIMM_NONE) ||
            (pPriv->dcAcqCont_inp.command_mode == SIMM_FAST) ||
            (pPriv->dcAcqCont_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufmotorGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->dcAcqCont_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          printf("My Command Mode is %ld \n",pPriv->dcAcqCont_inp.command_mode) ;
          /* Read some initial parameters from a file */
          /*
          pPriv->CurrParam.init_velocity = -1.0 ; 
          pPriv->CurrParam.slew_velocity = -1.0 ; 
          pPriv->CurrParam.acceleration = -1.0 ; 
          pPriv->CurrParam.deceleration = -1.0 ; 
          pPriv->CurrParam.drive_current= -1.0 ; 
          pPriv->CurrParam.datum_speed = -1 ; 
          pPriv->CurrParam.datum_direction = -1 ; 
          */
          /* if (read_dc_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ; */
          read_dc_file(pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel) ;
          pPriv->port_no = dc_port_no ;

          if (pPriv->dcAcqCont_inp.command_mode != SIMM_FAST) {
	    if(  pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->dcAcqCont_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the output links */
             strcpy(pgs->valc,pPriv->CurrParam.command) ;
            strcpy(pgs->vald,pPriv->CurrParam.obs_id) ;
            strcpy(pgs->vale,pPriv->CurrParam.dhs_write) ;
            strcpy(pgs->valf,pPriv->CurrParam.qckLk_id) ;
            strcpy(pgs->valg,pPriv->CurrParam.local_archive) ;
            strcpy(pgs->valh,pPriv->CurrParam.nod_handshake) ;
            /* strcpy(pgs->vali,pPriv->CurrParam.archive_host) ; */
            strcpy(pgs->valo,pPriv->CurrParam.archive_path) ;
            strcpy(pgs->valp,pPriv->CurrParam.archive_file_name) ;
            strcpy(pgs->valq,pPriv->CurrParam.comment) ;
            strcpy(pgs->valr,pPriv->CurrParam.remote_archive) ;
            strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host) ;
            strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path) ;
            strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name) ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
        } else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      } else { /* We have a configuration command */
        trx_debug("We have a MCE4 Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Save the current parameters */

        strcpy(pPriv->OldParam.command,pPriv->CurrParam.command) ;
        strcpy(pPriv->OldParam.obs_id,pPriv->CurrParam.obs_id) ;
        strcpy(pPriv->OldParam.dhs_write,pPriv->CurrParam.dhs_write) ;
        strcpy(pPriv->OldParam.qckLk_id,pPriv->CurrParam.qckLk_id) ;
        strcpy(pPriv->OldParam.local_archive,pPriv->CurrParam.local_archive) ;
        strcpy(pPriv->OldParam.nod_handshake,pPriv->CurrParam.nod_handshake) ;
        /* strcpy(pPriv->OldParam.archive_host,pPriv->CurrParam.archive_host) ; */
        strcpy(pPriv->OldParam.archive_path,pPriv->CurrParam.archive_path) ;
        strcpy(pPriv->OldParam.archive_file_name,pPriv->CurrParam.archive_file_name) ;
        strcpy(pPriv->OldParam.comment,pPriv->CurrParam.comment) ;
        strcpy(pPriv->OldParam.remote_archive,pPriv->CurrParam.remote_archive) ;
        strcpy(pPriv->OldParam.rem_archive_host,pPriv->CurrParam.rem_archive_host) ;
        strcpy(pPriv->OldParam.rem_archive_path,pPriv->CurrParam.rem_archive_path) ;
        strcpy(pPriv->OldParam.rem_archive_file_name,pPriv->CurrParam.rem_archive_file_name) ;

        /* get the input into the current parameters */
        if (strcmp(pPriv->dcAcqCont_inp.command,"") != 0)  
          strcpy(pPriv->CurrParam.command, pPriv->dcAcqCont_inp.command)  ;
        if (strcmp(pPriv->dcAcqCont_inp.obs_id,"") != 0)  
          strcpy(pPriv->CurrParam.obs_id,pPriv->dcAcqCont_inp.obs_id)  ;
        if (strcmp(pPriv->dcAcqCont_inp.dhs_write,"") != 0)  
          strcpy(pPriv->CurrParam.dhs_write, pPriv->dcAcqCont_inp.dhs_write)  ;
        if (strcmp(pPriv->dcAcqCont_inp.qckLk_id,"") != 0)  
          strcpy(pPriv->CurrParam.qckLk_id ,pPriv->dcAcqCont_inp.qckLk_id)  ;
        if (strcmp(pPriv->dcAcqCont_inp.local_archive,"") != 0)  
          strcpy(pPriv->CurrParam.local_archive,pPriv->dcAcqCont_inp.local_archive)  ;
        if (strcmp(pPriv->dcAcqCont_inp.nod_handshake,"") != 0)  
          strcpy(pPriv->CurrParam.nod_handshake, pPriv->dcAcqCont_inp.nod_handshake)  ;
	/*        if (strcmp(pPriv->dcAcqCont_inp.archive_host,"") != 0)  
          strcpy(pPriv->CurrParam.archive_host,pPriv->dcAcqCont_inp.archive_host); */ 
        if (strcmp(pPriv->dcAcqCont_inp.archive_path,"") != 0)  
          strcpy(pPriv->CurrParam.archive_path, pPriv->dcAcqCont_inp.archive_path)  ;
        if (strcmp(pPriv->dcAcqCont_inp.archive_file_name,"") != 0)  
          strcpy(pPriv->CurrParam.archive_file_name, pPriv->dcAcqCont_inp.archive_file_name)  ;
        if (strcmp(pPriv->dcAcqCont_inp.comment,"") != 0)  
          strcpy(pPriv->CurrParam.comment,pPriv->dcAcqCont_inp.comment)  ;
        if (strcmp(pPriv->dcAcqCont_inp.remote_archive,"") != 0)  
          strcpy(pPriv->CurrParam.remote_archive,pPriv->dcAcqCont_inp.remote_archive)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_host,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_host,pPriv->dcAcqCont_inp.rem_archive_host)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_path,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_path,pPriv->dcAcqCont_inp.rem_archive_path)  ;
        if (strcmp(pPriv->dcAcqCont_inp.rem_archive_file_name,"") != 0)  
          strcpy(pPriv->CurrParam.rem_archive_file_name,pPriv->dcAcqCont_inp.rem_archive_file_name)  ;


        /* see if this a command right after an init */
        /*
        if (pPriv->send_init_param) {
          pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
          pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
          pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
          pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
          pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current;
          pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
          pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ; 
          pPriv->send_init_param = 0 ; 
        }
        */
        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(70*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;

        num_str = UFcheck_dc_Acq_inputs (pPriv->dcAcqCont_inp,com, pPriv->paramLevel, rec_name,&pPriv->CurrParam, 
                                             pPriv->CurrParam.command_mode) ;

        /* Clear the input structure */

        pPriv->dcAcqCont_inp.command_mode = -1  ;
        strcpy(pPriv->dcAcqCont_inp.command,"") ;
        strcpy(pPriv->dcAcqCont_inp.obs_id,"") ;
        strcpy(pPriv->dcAcqCont_inp.dhs_write,"") ;
        strcpy(pPriv->dcAcqCont_inp.qckLk_id,"") ;
        strcpy(pPriv->dcAcqCont_inp.local_archive,"") ;
        strcpy(pPriv->dcAcqCont_inp.nod_handshake,"") ;
        /* strcpy(pPriv->dcAcqCont_inp.archive_host,"") ; */
        strcpy(pPriv->dcAcqCont_inp.archive_path,"") ;
        strcpy(pPriv->dcAcqCont_inp.archive_file_name,"") ;
        strcpy(pPriv->dcAcqCont_inp.comment,"") ;
        strcpy(pPriv->dcAcqCont_inp.remote_archive,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_host,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_path,"") ;
        strcpy(pPriv->dcAcqCont_inp.rem_archive_file_name,"") ;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          printf("********** Thn number of strings is : %d\n",num_str) ;
          for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;
          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) {
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.command) ;
            strcpy(pgs->vald,pPriv->CurrParam.obs_id) ;
            strcpy(pgs->vale,pPriv->CurrParam.dhs_write) ;
            strcpy(pgs->valf,pPriv->CurrParam.qckLk_id) ;
            strcpy(pgs->valg,pPriv->CurrParam.local_archive) ;
            strcpy(pgs->valh,pPriv->CurrParam.nod_handshake) ;
            /* strcpy(pgs->vali,pPriv->CurrParam.archive_host) ; */
            strcpy(pgs->valo,pPriv->CurrParam.archive_path) ;
            strcpy(pgs->valp,pPriv->CurrParam.archive_file_name) ;
            strcpy(pgs->valq,pPriv->CurrParam.comment) ;
            strcpy(pgs->valr,pPriv->CurrParam.remote_archive) ;
            strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host) ;

            strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path) ;

            strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name) ;


            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) {
	      if( _verbose )
	        printf("ufacqControlGproc> sending to agent, genSub record: %s\n", pgs->name); 
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      printf("ufacqControlGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
	      status = 0;
	    } 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
               strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }
      }
     
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;

      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      
      if ( (strcmp(pgs->a,"ABORT") == 0) || (strcmp(pgs->a,"STOP") == 0)) { 
        cancelCallback (pPriv->pCallback);
        strcpy(pPriv->dcAcqCont_inp.command,pgs->a) ;
        strcpy(pgs->a,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;   
        pPriv->commandState = TRX_GS_SENDING;    
        return OK ;
      } 

      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"MID",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        if (!pPriv->observing) {
          /* printf("%s\n",pPriv->errorMessage) ; */
          /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
          ticksNow = tickGet() ;
          if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
               (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* The command timed out */
            strcpy (pPriv->errorMessage, "DC Acq Command Timed out");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }          
          } else {
            /* Quit playing with the buttons. Can't you see I am BUSY? */
            trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
	} else { /* The observation timed out pPriv->obs_time_out */
          ticksNow = tickGet() ;
          if ( (ticksNow >= (pPriv->startingTicks + pPriv->obs_time_out)) &&
               (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;

            strcpy (pPriv->errorMessage, "Observation Timed out");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->ObserveCcarinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

          } else {
            /* Quit playing with the buttons. Can't you see I am BUSY? */
            trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }      
      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          pPriv->observing = 0 ;
          /* update the output links */

          strcpy(pgs->valc,pPriv->CurrParam.command) ;
          strcpy(pgs->vald,pPriv->CurrParam.obs_id) ;
          strcpy(pgs->vale,pPriv->CurrParam.dhs_write) ;
          strcpy(pgs->valf,pPriv->CurrParam.qckLk_id) ;
          strcpy(pgs->valg,pPriv->CurrParam.local_archive) ;
          strcpy(pgs->valh,pPriv->CurrParam.nod_handshake) ;
          /* strcpy(pgs->vali,pPriv->CurrParam.archive_host) ; */
          strcpy(pgs->valo,pPriv->CurrParam.archive_path) ;
          strcpy(pgs->valp,pPriv->CurrParam.archive_file_name) ;
          strcpy(pgs->valq,pPriv->CurrParam.comment) ;
          strcpy(pgs->valr,pPriv->CurrParam.remote_archive) ;
          strcpy(pgs->vals,pPriv->CurrParam.rem_archive_host) ;
          strcpy(pgs->valt,pPriv->CurrParam.rem_archive_path) ;
          strcpy(pgs->valu,pPriv->CurrParam.rem_archive_file_name) ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          if ( (strcmp(pPriv->CurrParam.command,"ABORT") == 0) ||
               (strcmp(pPriv->CurrParam.command,"STOP") == 0))  {
            pPriv->observing = 0 ;
            /* Set observeC to IDLE */
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          }
          if (strcmp(pPriv->CurrParam.command,"START") == 0) {
            /* Set the ObserveC and acqControlC CAR's to BUSY */
            pPriv->commandState = TRX_GS_BUSY;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            some_num = CAR_BUSY ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            /* Get the exposure time and setup a call back*/
            strcpy(obs_mode,pgs->p) ;
            if (strcmp(pgs->q,"") != 0) onSrc_time = strtod(pgs->q, &endptr) ;
            if (strcmp(pgs->r,"") != 0) ChopDutyCycle = strtod(pgs->r, &endptr)/100.0 ;
            if (strcmp(pgs->s,"") != 0) NodDutyCycle = strtod(pgs->s, &endptr)/100.0 ;
            if (strcmp(pgs->t,"") != 0) FrameDutyCycle = strtod(pgs->t, &endptr)/100.0 ;
            if (strcmp(obs_mode,"stare") == 0) Total_Time = onSrc_time ; 
            if (strcmp(obs_mode,"chop-nod") == 0) { 
              if ( (ChopDutyCycle != 0) && (NodDutyCycle != 0) ) 
                Total_Time = onSrc_time / ChopDutyCycle / NodDutyCycle * 2.0 / FrameDutyCycle;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zerooooooooooooooo \n") ;
	    }
            if (strcmp(obs_mode,"chop") == 0) { 
              if ( (ChopDutyCycle != 0) ) Total_Time = onSrc_time / ChopDutyCycle * 2.0 / FrameDutyCycle;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zerooooooooooooooo \n") ;
            }
            if (strcmp(obs_mode,"nod") == 0) { 
              if ( (NodDutyCycle != 0) ) Total_Time = onSrc_time / NodDutyCycle / FrameDutyCycle ;
              else printf ("@@@@@@@@@@@@@ error in input. somthing is zerooooooooooooooo \n") ;
            }
            /* printf ("@@@@@@@@ Total experiment time is %f\n",Total_Time) ;  */
            /* Calculate Time out period */
            /*
            if (Total_Time > 50.0) pPriv->obs_time_out = (long)((Total_Time + 5.0)*3600.0) ; 
            else { 
              if (Total_Time < 8.0) pPriv->obs_time_out = (long)((Total_Time + 1.0)*3600.0) ; 
              else pPriv->obs_time_out = (long) ((Total_Time + 3.0)*3600.0) ; 
            } 
            */
            pPriv->observing = 1 ;            
            /* update output links */
            strcpy(pgs->valj,pgs->p) ;
            sprintf(temp_str,"%7.3f",Total_Time) ;
            strcpy(pgs->valk,temp_str) ;
            strcpy(pgs->vall,pgs->q) ;
            strcpy(pgs->valm,pgs->r) ;
            strcpy(pgs->valn,pgs->s) ;
            strcpy(pgs->valr,pgs->t) ;
            strcpy(pPriv->errorMessage, "CALLBACK") ;
            /* requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ; */
            pPriv->obs_time_out = (long) 3600 ;
            requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ;
            pPriv->startingTicks = tickGet() ;
            /* Stay in a busy state */
            pPriv->commandState = TRX_GS_BUSY;
            
          }
        } else { 
          /* do we have and end of experiment? */
          if (strcmp(response,"COMPLETE") == 0) {
	    /* Cancel the Callback */
            cancelCallback (pPriv->pCallback);
            pPriv->observing = 0 ;
            /* go back to DONE state */
            pPriv->commandState = TRX_GS_DONE; 
            /* Set the CARs to IDLE */
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }

            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else {

            /* else do an update if you can and exit */
            numnum = strtod(response,&endptr) ;
            if (*endptr != '\0'){ /* we do not have a number */
              cancelCallback (pPriv->pCallback);
              pPriv->commandState = TRX_GS_DONE;
              /* restore the old parameters */
              if (pPriv->observing) {
                strcpy (pPriv->errorMessage, response);
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->ObserveCcarinfo.carMessage, DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->ObserveCcarinfo.carState, DBR_LONG, &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              } else {
                strcpy(pPriv->CurrParam.command,pPriv->OldParam.command) ;
                strcpy(pPriv->CurrParam.obs_id,pPriv->OldParam.obs_id) ;
                strcpy(pPriv->CurrParam.dhs_write,pPriv->OldParam.dhs_write) ;
                strcpy(pPriv->CurrParam.qckLk_id,pPriv->OldParam.qckLk_id) ;
                strcpy(pPriv->CurrParam.local_archive,pPriv->OldParam.local_archive) ;
                strcpy(pPriv->CurrParam.nod_handshake,pPriv->OldParam.nod_handshake) ;
                /* strcpy(pPriv->CurrParam.archive_host,pPriv->OldParam.archive_host) ; */
                strcpy(pPriv->CurrParam.archive_path,pPriv->OldParam.archive_path) ;
                strcpy(pPriv->CurrParam.archive_file_name,pPriv->OldParam.archive_file_name) ;
                strcpy(pPriv->CurrParam.comment,pPriv->OldParam.comment) ;
                strcpy(pPriv->CurrParam.remote_archive,pPriv->OldParam.remote_archive) ;
                strcpy(pPriv->CurrParam.rem_archive_host,pPriv->OldParam.rem_archive_host) ;
                strcpy(pPriv->CurrParam.rem_archive_path,pPriv->OldParam.rem_archive_path) ;
                strcpy(pPriv->CurrParam.rem_archive_file_name,pPriv->OldParam.rem_archive_file_name) ;

                /* set the CAR to ERR with whatever the Agent sent you */
                strcpy (pPriv->errorMessage, response);
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                  return OK;
                }
              }
            } else {
              if (strcmp(pgs->vali,response) != 0) {
                cancelCallback (pPriv->pCallback);
                requestCallback(pPriv->pCallback, pPriv->obs_time_out ) ;
                /* printf("Requesting a Callback with %ld ticks for frame count of %s\n",
                   pPriv->obs_time_out,response) ; */
                pPriv->startingTicks = tickGet() ;
                strcpy (pgs->vali,response) ;
              } else {
                strcpy (pgs->vali,response) ;
              }
            }
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}



/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** Unpacks array of strings ***************/
long unpack_meta_param (genSubRecord *pgs, char *zz, char **remainder) {

  char yy[40] ;
  char *original ;
  int i;
  original = zz ; 
              strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->vala,yy) ; /* 1 */
  printf("unpacking in vala: @%s@\n",yy) ;
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valb,yy) ; /* 2 */
  printf("unpacking in valb: @%s@\n",yy) ;
  zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ; strcpy(pgs->valc,yy) ; /* 3 */
  printf("unpacking in valc: @%s@\n",yy) ;

  zz = zz+40; strcpy(meta_phys_parm[0],zz) ;/* 4 */
  zz = zz+40; strcpy(meta_phys_parm[1],zz) ;/* 5 */
  zz = zz+40; strcpy(meta_phys_parm[2],zz) ;/* 6 */
  zz = zz+40; strcpy(meta_phys_parm[3],zz) ;/* 7 */
  zz = zz+40; strcpy(meta_phys_parm[4],zz) ;/* 8 */
  zz = zz+40; strcpy(meta_phys_parm[5],zz) ;/* 9 */
  zz = zz+40; strcpy(meta_phys_parm[6],zz) ;/* 10 */
  zz = zz+40; strcpy(meta_phys_parm[7],zz) ;/* 11 */
  zz = zz+40; strcpy(meta_phys_parm[8],zz) ;/* 12 */
  zz = zz+40; strcpy(meta_phys_parm[9],zz) ;/* 13 */
  zz = zz+40; strcpy(meta_phys_parm[10],zz) ;/* 14 */
  zz = zz+40; strcpy(meta_phys_parm[11],zz) ;/* 15 */
  zz = zz+40; strcpy(meta_phys_parm[12],zz) ;/* 16 */
  zz = zz+40; strcpy(meta_phys_parm[13],zz) ;/* 17 */
  zz = zz+40; strcpy(meta_phys_parm[14],zz) ;/* 18 */
  zz = zz+40; strcpy(meta_phys_parm[15],zz) ;/* 19 */
  zz = zz+40; strcpy(meta_phys_parm[16],zz) ;/* 20 */
  zz = zz+40; strcpy(meta_phys_parm[17],zz) ;/* 21 */
  zz = zz+40; strcpy(meta_phys_parm[18],zz) ;/* 22 */
  zz = zz+40; strcpy(meta_phys_parm[19],zz) ;/* 23 */
  zz = zz+40; strcpy(meta_phys_parm[20],zz) ;/* 24 */
  zz = zz+40; strcpy(meta_phys_parm[21],zz) ;/* 25 */
  zz = zz+40; strcpy(meta_phys_parm[22],zz) ;/* 26 */
  zz = zz+40; strcpy(meta_phys_parm[23],zz) ;/* 27 */
  zz = zz+40; strcpy(meta_phys_parm[24],zz) ;/* 28 */

  if (bingo) for (i=0;i<25;i++) printf("Meta item # %d is: %s\n",i,meta_phys_parm[i]) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** INAM for DCMetaOutG    *****************/
long ufDCMetaOutGinit (genSubRecord *pgs) {
  
  strcpy (pgs->a,"") ;
  pgs->noa = 28 ;
  
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/***************** SNAM for DCMetaOutG    *****************/
long ufDCMetaOutGproc (genSubRecord *pgs) {
  
  if (strcmp(pgs->a,"") != 0) {
    unpack_meta_param (pgs, (char *)pgs->a,(char **)meta_phys_parm ) ;
    pgs->novu = 25;
    pgs->valu = meta_phys_parm[0] ;
    strcpy (pgs->a,"") ;
    pgs->noa = 28 ;
  }
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for ObsControlG *******************/
long ufobsControlGinit (genSubRecord *pgs) {
  /* Gensub Private Structure Declaration */
  genSubDCObsContPrivate *pPriv ;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCObsContPrivate *) malloc (sizeof(genSubDCObsContPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 4; /* DC agent */

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */ 
  strcpy(pPriv->CurrParam.lambda_low,"") ;
  strcpy(pPriv->CurrParam.lambda_hi,"") ;
  strcpy(pPriv->CurrParam.camera_mode,"") ;
  strcpy(pPriv->CurrParam.obs_mode,"") ;
  strcpy(pPriv->CurrParam.on_source_time,"") ;
  strcpy(pPriv->CurrParam.readout_mode,"") ;
  strcpy(pPriv->CurrParam.filter_name,"") ;
  strcpy(pPriv->CurrParam.grating_name,"") ;
  strcpy(pPriv->CurrParam.central_wavelength,"") ;
  strcpy(pPriv->CurrParam.preValid_chop_time,"") ;
  strcpy(pPriv->CurrParam.lyot_name,"") ;
  strcpy(pPriv->CurrParam.sector_name,"") ;
  strcpy(pPriv->CurrParam.slit_name,"") ;
  strcpy(pPriv->CurrParam.window_name,"") ;
  strcpy(pPriv->CurrParam.throughput,"") ;
  strcpy(pPriv->CurrParam.chopThrow,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /*  */
  strcpy((char *)pgs->b,"") ; /*  */
  strcpy((char *)pgs->c,"") ; /*  */
  strcpy((char *)pgs->d,"") ; /*  */
  strcpy((char *)pgs->e,"") ; /*  */
  strcpy((char *)pgs->f,"") ; /*  */
  strcpy((char *)pgs->g,"") ;  /*  */
  strcpy((char *)pgs->h,"") ;  /*  */
  strcpy((char *)pgs->i,"") ; /*  */
  strcpy((char *)pgs->k,"") ; /*  */
  strcpy((char *)pgs->l,"") ; /*  */
  strcpy((char *)pgs->m,"") ; /*  */
  strcpy((char *)pgs->n,"") ; /*  */
  strcpy((char *)pgs->o,"") ; /*  */
  strcpy((char *)pgs->p,"") ; /*  */
  strcpy((char *)pgs->q,"") ; /*  */
  strcpy((char *)pgs->r,"") ; /*  */
  strcpy((char *)pgs->s,"") ; /*  */
  strcpy((char *)pgs->t,"") ; /*  */
  *(double *)pgs->u = -1.0 ; /*  */
  pgs->nou = 8 ;
 
  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->dcObsCont_inp.command_mode = -1  ;
  strcpy(pPriv->dcObsCont_inp.lambda_low,"") ;
  strcpy(pPriv->dcObsCont_inp.lambda_hi,"") ;
  strcpy(pPriv->dcObsCont_inp.camera_mode,"") ;
  strcpy(pPriv->dcObsCont_inp.obs_mode,"") ;
  strcpy(pPriv->dcObsCont_inp.on_source_time,"") ;
  strcpy(pPriv->dcObsCont_inp.readout_mode,"") ;
  strcpy(pPriv->dcObsCont_inp.filter_name,"") ;
  strcpy(pPriv->dcObsCont_inp.grating_name,"") ;
  strcpy(pPriv->dcObsCont_inp.central_wavelength,"") ;
  strcpy(pPriv->dcObsCont_inp.preValid_chop_time,"") ;
  strcpy(pPriv->dcObsCont_inp.lyot_name,"") ;
  strcpy(pPriv->dcObsCont_inp.sector_name,"") ;
  strcpy(pPriv->dcObsCont_inp.slit_name,"") ;
  strcpy(pPriv->dcObsCont_inp.window_name,"") ;
  strcpy(pPriv->dcObsCont_inp.throughput,"") ;
  strcpy(pPriv->dcObsCont_inp.chopThrow,"") ;

  pPriv->paramLevel = 'M' ;
  
  pgs->dpvt = (void *) pPriv;

  pgs->noj = 28 ;
  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ObsControlG *******************/
long ufobsControlGproc (genSubRecord *pgs) {

  genSubDCObsContPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  double numnum ;
  double *temp_double_ptr ;
  /* strcpy(dc_DEBUG_MODE,pgs->m) ; */

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCObsContPrivate *)pgs->dpvt ;
  printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState);
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ;
	 printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!dc_initialized)  init_dc_config() ;
      /* read_dc_file(pgs->name, (genSubCommPrivate *)pPriv) ; */ /* ZZZ */

      /* pPriv->port_no = dc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy((char *)pgs->valc,"") ;
      strcpy((char *)pgs->vald,"") ;
      strcpy((char *)pgs->vale,"") ;
      strcpy((char *)pgs->valf,"") ;
      strcpy((char *)pgs->valg,"") ;
      strcpy((char *)pgs->valh,"") ;
      strcpy((char *)pgs->vali,"") ;
      strcpy((char *)pgs->valj,"") ;
      strcpy((char *)pgs->valk,"") ;
      strcpy((char *)pgs->vall,"") ;
      strcpy((char *)pgs->valm,"") ;
      strcpy((char *)pgs->valn,"") ;
      strcpy((char *)pgs->valo,"") ;
      strcpy((char *)pgs->valp,"") ;
      strcpy((char *)pgs->valq,"") ;
      strcpy((char *)pgs->valr,"") ;
      strcpy((char *)pgs->vals,"") ;
      strcpy((char *)pgs->valt,"") ;
      strcpy((char *)pgs->valu,"") ;

      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */ 
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */ /* ZZZ */
      pPriv->socfd =  -1 ; /* ZZZ */
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
      } 
      */ 
      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */

      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;
      pPriv->dcObsCont_inp.command_mode = * (long *)pgs->a ;

      strcpy(pPriv->dcObsCont_inp.lambda_low,pgs->b) ;
      strcpy(pPriv->dcObsCont_inp.lambda_hi,pgs->c) ;
      strcpy(pPriv->dcObsCont_inp.camera_mode,pgs->d) ;
      strcpy(pPriv->dcObsCont_inp.obs_mode,pgs->e) ;
      strcpy(pPriv->dcObsCont_inp.on_source_time,pgs->f) ;
      strcpy(pPriv->dcObsCont_inp.readout_mode,pgs->g) ;
      strcpy(pPriv->dcObsCont_inp.filter_name,pgs->h) ;
      strcpy(pPriv->dcObsCont_inp.grating_name,pgs->i) ;
      strcpy(pPriv->dcObsCont_inp.central_wavelength,pgs->k) ;
      strcpy(pPriv->dcObsCont_inp.preValid_chop_time,pgs->l) ;

      strcpy(pPriv->dcObsCont_inp.lyot_name,pgs->n) ;
      strcpy(pPriv->dcObsCont_inp.sector_name,pgs->o) ;
      strcpy(pPriv->dcObsCont_inp.slit_name,pgs->p) ;
      strcpy(pPriv->dcObsCont_inp.window_name,pgs->q) ;
      strcpy(pPriv->dcObsCont_inp.throughput,pgs->r) ;
      strcpy(pPriv->dcObsCont_inp.chopThrow,pgs->s) ;
      temp_double_ptr = pgs->u ;
      /* clear the output link */
      /* strcpy(pgs->valu,"") ; */

      for (i = 0; i <= 7; i++)
        pPriv->dcObsCont_inp.meta_env_data[i] = temp_double_ptr[i] ;
      /*
      printf("My inputs are ......\n") ;   
      printf("Link A %ld\n",*(long *)pgs->a ) ;  
      printf("Link B %s\n",(char *)pgs->b ) ; 
      printf("Link C %s\n",(char *)pgs->c ) ;  
      printf("Link D %s\n",(char *)pgs->d ) ; 
      printf("Link E %s\n",(char *)pgs->e ) ;  
      printf("Link F %s\n",(char *)pgs->f ) ; 
      printf("Link G %s\n",(char *)pgs->g ) ;  
      printf("Link H %s\n",(char *)pgs->h ) ; 
      printf("Link I %s\n",(char *)pgs->i ) ;  
      printf("Link K %s\n",(char *)pgs->k ) ;  
      printf("Link L %s\n",(char *)pgs->l ) ; 
      printf("Link M %s\n",(char *)pgs->m ) ;  
      printf("Link N %s\n",(char *)pgs->n ) ; 
      printf("Link O %s\n",(char *)pgs->o ) ;   
      printf("Link P %s\n",(char *)pgs->p ) ; 
      printf("Link Q %s\n",(char *)pgs->q ) ;  
      printf("Link R %s\n",(char *)pgs->r ) ; 
      printf("Link S %s\n",(char *)pgs->s ) ;  
      printf("Link T %s\n",(char *)pgs->t ) ; 
      printf("Link U %s\n",(char *)pgs->u ) ;  
      */

      if ( (strcmp(pPriv->dcObsCont_inp.lambda_low,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.lambda_hi,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.camera_mode,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.obs_mode,"") != 0) ||
           (pPriv->dcObsCont_inp.command_mode != -1) ||
           (strcmp(pPriv->dcObsCont_inp.on_source_time,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.readout_mode,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.filter_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.grating_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.central_wavelength,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.preValid_chop_time,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.lyot_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.sector_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.slit_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.window_name,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.chopThrow,"") != 0) ||
           (strcmp(pPriv->dcObsCont_inp.throughput,"") != 0) ) {

        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;

        *(long *)pgs->a = -1 ; /*  */
        strcpy((char *)pgs->b,"") ; /*  */
        strcpy((char *)pgs->c,"") ; /*  */
        strcpy((char *)pgs->d,"") ; /*  */
        strcpy((char *)pgs->e,"") ; /*  */
        strcpy((char *)pgs->f,"") ; /*  */
        strcpy((char *)pgs->g,"") ;  /*  */
        strcpy((char *)pgs->h,"") ;  /*  */
        strcpy((char *)pgs->i,"") ; /*  */
        strcpy((char *)pgs->k,"") ; /*  */
        strcpy((char *)pgs->l,"") ; /*  */
        strcpy((char *)pgs->m,"") ; /*  */
        strcpy((char *)pgs->n,"") ; /*  */
        strcpy((char *)pgs->o,"") ; /*  */
        strcpy((char *)pgs->p,"") ; /*  */
        strcpy((char *)pgs->q,"") ; /*  */
        strcpy((char *)pgs->r,"") ; /*  */
        strcpy((char *)pgs->s,"") ; /*  */

      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 28 ;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      /* clear the output link */
      strcpy(pgs->valu,"") ;
      if (pPriv->dcObsCont_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->dcObsCont_inp.command_mode == SIMM_NONE) ||
            (pPriv->dcObsCont_inp.command_mode == SIMM_FAST) ||
            (pPriv->dcObsCont_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufmotorGproc> socket is writable, no need to reconnect...\n");
	    } 
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->dcObsCont_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          printf("My Command Mode is %ld \n",pPriv->dcObsCont_inp.command_mode) ;
          /* Read some initial parameters from a file */
          /*
          pPriv->CurrParam.init_velocity = -1.0 ; 
          pPriv->CurrParam.slew_velocity = -1.0 ; 
          pPriv->CurrParam.acceleration = -1.0 ; 
          pPriv->CurrParam.deceleration = -1.0 ; 
          pPriv->CurrParam.drive_current= -1.0 ; 
          pPriv->CurrParam.datum_speed = -1 ; 
          pPriv->CurrParam.datum_direction = -1 ; 
          */
          /* if (read_dc_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ; */
          read_dc_file(pgs->name, (genSubCommPrivate *)pPriv, pPriv->paramLevel) ;
          pPriv->port_no = dc_port_no ;

          if (pPriv->dcObsCont_inp.command_mode != SIMM_FAST) {
  	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->dcObsCont_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.obs_mode) ;
            strcpy(pgs->vald,pPriv->CurrParam.readout_mode) ;
            strcpy(pgs->vale,pPriv->CurrParam.camera_mode) ;
            strcpy(pgs->valf,pPriv->CurrParam.on_source_time) ;
            strcpy(pgs->valg,pPriv->CurrParam.chopThrow) ;
            strcpy(pgs->valh,pPriv->CurrParam.preValid_chop_time) ;
            strcpy(pgs->vali,pPriv->CurrParam.lambda_low) ;
            strcpy(pgs->valj,pPriv->CurrParam.lambda_hi) ;
            strcpy(pgs->valk,pPriv->CurrParam.filter_name) ;
            strcpy(pgs->vall,pPriv->CurrParam.grating_name) ;
            strcpy(pgs->valm,pPriv->CurrParam.central_wavelength) ;
            strcpy(pgs->valn,pPriv->CurrParam.lyot_name) ;
            strcpy(pgs->valo,pPriv->CurrParam.sector_name) ;
            strcpy(pgs->valp,pPriv->CurrParam.slit_name) ;
            strcpy(pgs->valq,pPriv->CurrParam.throughput) ;
            strcpy(pgs->valr,pPriv->CurrParam.window_name) ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
        } else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      } else { /* We have a configuration command */
        trx_debug("We have a ObsControl Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Save the current parameters */
        strcpy(pPriv->OldParam.obs_mode,pPriv->CurrParam.obs_mode) ;
        strcpy(pPriv->OldParam.readout_mode,pPriv->CurrParam.readout_mode) ;
        strcpy(pPriv->OldParam.camera_mode,pPriv->CurrParam.camera_mode) ;
        strcpy(pPriv->OldParam.on_source_time,pPriv->CurrParam.on_source_time) ;
        strcpy(pPriv->OldParam.chopThrow,pPriv->CurrParam.chopThrow) ;
        strcpy(pPriv->OldParam.preValid_chop_time,pPriv->CurrParam.preValid_chop_time) ;
        strcpy(pPriv->OldParam.lambda_low,pPriv->CurrParam.lambda_low) ;
        strcpy(pPriv->OldParam.lambda_hi,pPriv->CurrParam.lambda_hi) ;
        strcpy(pPriv->OldParam.filter_name,pPriv->CurrParam.filter_name) ;
        strcpy(pPriv->OldParam.grating_name,pPriv->CurrParam.grating_name) ;
        strcpy(pPriv->OldParam.central_wavelength,pPriv->CurrParam.central_wavelength) ;
        strcpy(pPriv->OldParam.lyot_name,pPriv->CurrParam.lyot_name) ;
        strcpy(pPriv->OldParam.sector_name,pPriv->CurrParam.sector_name) ;
        strcpy(pPriv->OldParam.slit_name,pPriv->CurrParam.slit_name) ;
        strcpy(pPriv->OldParam.throughput,pPriv->CurrParam.throughput) ;
        strcpy(pPriv->OldParam.window_name,pPriv->CurrParam.window_name) ;

        /* get the input into the current parameters */

        if (strcmp(pPriv->dcObsCont_inp.obs_mode,"") != 0)  
          strcpy(pPriv->CurrParam.obs_mode, pPriv->dcObsCont_inp.obs_mode)  ;
        if (strcmp(pPriv->dcObsCont_inp.readout_mode,"") != 0)  
          strcpy(pPriv->CurrParam.readout_mode, pPriv->dcObsCont_inp.readout_mode)  ;
        if (strcmp(pPriv->dcObsCont_inp.camera_mode,"") != 0)  
          strcpy(pPriv->CurrParam.camera_mode, pPriv->dcObsCont_inp.camera_mode)  ;
        if (strcmp(pPriv->dcObsCont_inp.on_source_time,"") != 0)  
          strcpy(pPriv->CurrParam.on_source_time, pPriv->dcObsCont_inp.on_source_time)  ;
        if (strcmp(pPriv->dcObsCont_inp.chopThrow,"") != 0)  
          strcpy(pPriv->CurrParam.chopThrow, pPriv->dcObsCont_inp.chopThrow)  ;
        if (strcmp(pPriv->dcObsCont_inp.preValid_chop_time,"") != 0)  
          strcpy(pPriv->CurrParam.preValid_chop_time, pPriv->dcObsCont_inp.preValid_chop_time)  ;
        if (strcmp(pPriv->dcObsCont_inp.lambda_low,"") != 0)  
          strcpy(pPriv->CurrParam.lambda_low, pPriv->dcObsCont_inp.lambda_low)  ;
        if (strcmp(pPriv->dcObsCont_inp.lambda_hi,"") != 0)  
          strcpy(pPriv->CurrParam.lambda_hi, pPriv->dcObsCont_inp.lambda_hi)  ;
        if (strcmp(pPriv->dcObsCont_inp.filter_name,"") != 0)  
          strcpy(pPriv->CurrParam.filter_name, pPriv->dcObsCont_inp.filter_name)  ;
        if (strcmp(pPriv->dcObsCont_inp.grating_name,"") != 0)  
          strcpy(pPriv->CurrParam.grating_name, pPriv->dcObsCont_inp.grating_name)  ;
        if (strcmp(pPriv->dcObsCont_inp.central_wavelength,"") != 0)  
          strcpy(pPriv->CurrParam.central_wavelength, pPriv->dcObsCont_inp.central_wavelength)  ;
        if (strcmp(pPriv->dcObsCont_inp.lyot_name,"") != 0)  
          strcpy(pPriv->CurrParam.lyot_name, pPriv->dcObsCont_inp.lyot_name)  ;
        if (strcmp(pPriv->dcObsCont_inp.sector_name,"") != 0)  
          strcpy(pPriv->CurrParam.sector_name, pPriv->dcObsCont_inp.sector_name)  ;
        if (strcmp(pPriv->dcObsCont_inp.slit_name,"") != 0)  
          strcpy(pPriv->CurrParam.slit_name, pPriv->dcObsCont_inp.slit_name)  ;
        if (strcmp(pPriv->dcObsCont_inp.throughput,"") != 0)  
          strcpy(pPriv->CurrParam.throughput, pPriv->dcObsCont_inp.throughput)  ;
        if (strcmp(pPriv->dcObsCont_inp.window_name,"") != 0)  
          strcpy(pPriv->CurrParam.window_name, pPriv->dcObsCont_inp.window_name)  ;


        /* see if this a command right after an init */
        /*
        if (pPriv->send_init_param) {
          pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
          pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
          pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
          pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
          pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current;
          pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
          pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ; 
          pPriv->send_init_param = 0 ; 
        }
        */
        /* formulate the command strings */
        com = malloc(70*sizeof(char *)) ;
        for (i=0;i<70;i++) com[i] = malloc(70*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;
        /* do the lookup table stuff here */
        assign_sky_transparency(&pPriv->dcObsCont_inp) ;



        /* **************************************** */
        /* **************************************** */
        /* **************************************** */
        /* **************************************** */
        /* **************************************** */
        /* **************************************** */
        /* **************************************** */
        num_str = UFcheck_dc_Obs_inputs (pPriv->dcObsCont_inp,com, pPriv->paramLevel, rec_name,&pPriv->CurrParam, 
                                             pPriv->CurrParam.command_mode) ;

        /* Clear the input structure */

        pPriv->dcObsCont_inp.command_mode = -1  ;

        strcpy(pPriv->dcObsCont_inp.lambda_low,"") ;
        strcpy(pPriv->dcObsCont_inp.lambda_hi,"") ;
        strcpy(pPriv->dcObsCont_inp.camera_mode,"") ;
        strcpy(pPriv->dcObsCont_inp.obs_mode,"") ;
        strcpy(pPriv->dcObsCont_inp.on_source_time,"") ;
        strcpy(pPriv->dcObsCont_inp.readout_mode,"") ;
        strcpy(pPriv->dcObsCont_inp.filter_name,"") ;
        strcpy(pPriv->dcObsCont_inp.grating_name,"") ;
        strcpy(pPriv->dcObsCont_inp.central_wavelength,"") ;
        strcpy(pPriv->dcObsCont_inp.preValid_chop_time,"") ;
        strcpy(pPriv->dcObsCont_inp.lyot_name,"") ;
        strcpy(pPriv->dcObsCont_inp.sector_name,"") ;
        strcpy(pPriv->dcObsCont_inp.slit_name,"") ;
        strcpy(pPriv->dcObsCont_inp.window_name,"") ;
        strcpy(pPriv->dcObsCont_inp.throughput,"") ;
        strcpy(pPriv->dcObsCont_inp.chopThrow,"") ;


        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          printf("********** Thn number of strings is : %d\n",num_str) ;
          if (bingo) for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;

          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<70;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.obs_mode) ;
            strcpy(pgs->vald,pPriv->CurrParam.readout_mode) ;
            strcpy(pgs->vale,pPriv->CurrParam.camera_mode) ;
            strcpy(pgs->valf,pPriv->CurrParam.on_source_time) ;
            strcpy(pgs->valg,pPriv->CurrParam.chopThrow) ;
            strcpy(pgs->valh,pPriv->CurrParam.preValid_chop_time) ;
            strcpy(pgs->vali,pPriv->CurrParam.lambda_low) ;
            strcpy(pgs->valj,pPriv->CurrParam.lambda_hi) ;
            strcpy(pgs->valk,pPriv->CurrParam.filter_name) ;
            strcpy(pgs->vall,pPriv->CurrParam.grating_name) ;
            strcpy(pgs->valm,pPriv->CurrParam.central_wavelength) ;
            strcpy(pgs->valn,pPriv->CurrParam.lyot_name) ;
            strcpy(pgs->valo,pPriv->CurrParam.sector_name) ;
            strcpy(pgs->valp,pPriv->CurrParam.slit_name) ;
            strcpy(pgs->valq,pPriv->CurrParam.throughput) ;
            strcpy(pgs->valr,pPriv->CurrParam.window_name) ;

            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) {
	      if( _verbose )
	        printf("ufobsControlGproc> sending to agent, genSub record: %s\n", pgs->name); 
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      printf("ufobsControlGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
	      status = 0;
	    } 
            for (i=0;i<70;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
               strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {
          for (i=0;i<70;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        }
      }
      
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) printf("??????? %s\n",response) ;
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 28 ;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /* 
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) { 
        cancelCallback (pPriv->pCallback);  
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ; 
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;  
        strcpy(pgs->i,"") ; 
        strcpy(pgs->k,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;   
        pPriv->commandState = TRX_GS_SENDING;    
        return OK ;   
	} */

      strcpy(response,pgs->j) ;
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_DC_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          /* The command timed out */
          strcpy (pPriv->errorMessage, "DC Obs Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
         
          /* update the output links */
          strcpy(pgs->valc,pPriv->CurrParam.obs_mode) ;
          strcpy(pgs->vald,pPriv->CurrParam.readout_mode) ;
          strcpy(pgs->vale,pPriv->CurrParam.camera_mode) ;
          strcpy(pgs->valf,pPriv->CurrParam.on_source_time) ;
          strcpy(pgs->valg,pPriv->CurrParam.chopThrow) ;
          strcpy(pgs->valh,pPriv->CurrParam.preValid_chop_time) ;
          strcpy(pgs->vali,pPriv->CurrParam.lambda_low) ;
          strcpy(pgs->valj,pPriv->CurrParam.lambda_hi) ;
          strcpy(pgs->valk,pPriv->CurrParam.filter_name) ;
          strcpy(pgs->vall,pPriv->CurrParam.grating_name) ;
          strcpy(pgs->valm,pPriv->CurrParam.central_wavelength) ;
          strcpy(pgs->valn,pPriv->CurrParam.lyot_name) ;
          strcpy(pgs->valo,pPriv->CurrParam.sector_name) ;
          strcpy(pgs->valp,pPriv->CurrParam.slit_name) ;
          strcpy(pgs->valq,pPriv->CurrParam.throughput) ;
          strcpy(pgs->valr,pPriv->CurrParam.window_name) ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */

            strcpy(pPriv->CurrParam.obs_mode,pPriv->OldParam.obs_mode) ;
            strcpy(pPriv->CurrParam.readout_mode,pPriv->OldParam.readout_mode) ;
            strcpy(pPriv->CurrParam.camera_mode,pPriv->OldParam.camera_mode) ;
            strcpy(pPriv->CurrParam.on_source_time,pPriv->OldParam.on_source_time) ;
            strcpy(pPriv->CurrParam.chopThrow,pPriv->OldParam.chopThrow) ;
            strcpy(pPriv->CurrParam.preValid_chop_time,pPriv->OldParam.preValid_chop_time) ;
            strcpy(pPriv->CurrParam.lambda_low,pPriv->OldParam.lambda_low) ;
            strcpy(pPriv->CurrParam.lambda_hi,pPriv->OldParam.lambda_hi) ;
            strcpy(pPriv->CurrParam.filter_name,pPriv->OldParam.filter_name) ;
            strcpy(pPriv->CurrParam.grating_name,pPriv->OldParam.grating_name) ;
            strcpy(pPriv->CurrParam.central_wavelength,pPriv->OldParam.central_wavelength) ;
            strcpy(pPriv->CurrParam.lyot_name,pPriv->OldParam.lyot_name) ;
            strcpy(pPriv->CurrParam.sector_name,pPriv->OldParam.sector_name) ;
            strcpy(pPriv->CurrParam.slit_name,pPriv->OldParam.slit_name) ;
            strcpy(pPriv->CurrParam.throughput,pPriv->OldParam.throughput) ;
            strcpy(pPriv->CurrParam.window_name,pPriv->OldParam.window_name) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else {
            /* send the output to the output Gensub */
            printf("The number of parameters received is %ld",pgs->noj) ;
            assign_adj_meta_param( pgs, (char *)pgs->j) ;
            pgs->novu = 28 ;
            pgs->valu = adjMetaParm[0] ;
	    pgs->noj = 28 ;
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 28 ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
void parse_bias_values(char * in_str,long *cont_val, long *default_val, long *min_val, long *max_val) {

  char *endptr ;
  *cont_val = 0;
  *default_val = 0 ;
  *min_val = 0 ;
  *max_val = 0 ;
  printf("The string I got is:@%s@\n",in_str) ;
  *cont_val = strtol(in_str,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *default_val = strtol(endptr,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *min_val = strtol(endptr,&endptr,10) ;
  endptr = strchr(endptr,':') ;
  endptr = endptr + 1 ;
  *max_val = strtol(endptr,&endptr,10) ;
  printf("The values I paresed are %ld, %ld, %ld, %ld\n",*cont_val,*default_val,*min_val,*max_val) ;
  return ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the bias file *****************/
long read_Bias_test_file () {
  /* Initialization File */
  FILE *biasTestFile ;
  char in_str[90] ;
  char filename[50] ;
  long  cont_val, default_val, min_val, max_val;
  int i ;
  

  strcpy(filename,"./pv/test_bias.txt");

  if ((biasTestFile = fopen(filename,"r")) == NULL) {
    printf("DC Bias test file could not be opened\n");
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;
    printf("Val  Def  Min  Max \n") ;
    for (i=0;i<24;i++) {
      fgets(in_str,80,biasTestFile);  /* dac name */
      parse_bias_values(in_str, &cont_val, &default_val, &min_val, &max_val) ;
      printf("%3ld  %3ld  %3ld  %3ld\n",cont_val, default_val, min_val, max_val) ;
    }
    /* printf("%s\n","Line 55:") ; */
    fclose(biasTestFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/********************* INAM for DCBiasG *******************/
long ufDCBiasGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDCBiasPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
  int i; 
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCBiasPrivate *) malloc (sizeof(genSubDCBiasPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 3; /* dc agent */

  for (i = 0;i<24;i++) {
    pPriv->CurrParam.M[i] = -1.0 ;
    pPriv->CurrParam.B[i] = -1.0 ;
    pPriv->CurrParam.dac_volt_values[i] = -999999.0 ;
    pPriv->CurrParam.dac_control_values[i] = -1 ;
  }

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */

  strcpy(pPriv->CurrParam.power,"") ;
  strcpy(pPriv->CurrParam.vGate,"") ;
  strcpy(pPriv->CurrParam.vWell,"") ;
  strcpy(pPriv->CurrParam.vBias,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* command mode  */
  *(long *)pgs->b = -1 ;
  strcpy(pgs->c,"") ;
  *(long *)pgs->d = -1 ;
  *(long *)pgs->e = -1 ;
  *(double *)pgs->f = -1.0 ;
  *(long *)pgs->g = -1 ;
  *(long *)pgs->h = -1 ;
   strcpy(pgs->i,"") ;
   *(long *)pgs->k = -1 ;
   strcpy(pgs->l,"") ;
   strcpy(pgs->m,"") ;
   strcpy(pgs->n,"") ;
   strcpy(pgs->o,"") ;

  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->bias_inp.command_mode = -1 ;
  pPriv->bias_inp.datum_directive = -1 ;
  strcpy(pPriv->bias_inp.power,"") ;
  pPriv->bias_inp.park_directive = -1 ;
  pPriv->bias_inp.dac_id = -1 ;
  pPriv->bias_inp.dac_volt = -99999.99 ;
  pPriv->bias_inp.latch_dac_id = -1;
  pPriv->bias_inp.read_all_directive = -1 ;
  strcpy(pPriv->bias_inp.vGate,"") ;
  strcpy(pPriv->bias_inp.vWell,"") ;
  strcpy(pPriv->bias_inp.vBias,"") ;
  strcpy(pPriv->bias_inp.password,"");
  pPriv->bias_inp.DetTemp = -1;
  strcpy(pPriv->bias_inp.PassEnable,"") ;

  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
  pgs->noj = 24 ; 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the bias file *****************/
long read_Bias_file (char *rec_name, genSubDCBiasPrivate *pPriv) {
  /* Initialization File */
  FILE *biasFile ;
  char in_str[90] ;
  char filename[50] ;
  double numnum /*, numnum1, numnum2 */;
  double dac_volt,  dac_M ;
  long  dac_B;
  int i ;
  long bias_num, latch_order ;
  long temp_long ;
  double temp_double ;
  
  strcpy(filename,dc_bias_filename);

  if ((biasFile = fopen(filename,"r")) == NULL) {
    trx_debug("DC Bias file could not be opened", rec_name,"MID",dc_STARTUP_DEBUG_MODE);
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    strcpy (dc_host_ip,in_str) ;
    i = 0;
    while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
    dc_host_ip[i] = '\0' ;
    
    /* Read the port number */
    fgets(in_str,90,biasFile); /* comment line */ 
   
    /* 
    fgets(in_str,40,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(biasFile,"%lf",&numnum) ; /* port number */

    dc_port_no = (long) numnum ;
    fgets(in_str,90,biasFile); /* read till end of line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    /* printf("%s\n","Line 0:") ; */
    for (i=0;i<24;i++) {
      fgets(in_str,15,biasFile);  /* dac name */
      
      fscanf(biasFile, "%ld %ld %lf %lf %ld",&bias_num, &latch_order, &dac_volt, &dac_M, &dac_B) ;
      pPriv->CurrParam.M[i] = dac_M ;
      pPriv->CurrParam.B[i] = dac_B ;
      pPriv->CurrParam.dac_volt_values[i] = dac_volt ;

      temp_double = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                               (double)pPriv->CurrParam.B[i] ;
      temp_long = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                               pPriv->CurrParam.B[i] ;
      numnum = temp_double - (double)temp_long ;
      if (numnum >= 0.5) temp_long = temp_long + 1;
      pPriv->CurrParam.dac_control_values[i] = temp_long ;
      
      /* printf("%s: ",in_str) ;
	 printf("%ld %ld %f %f %ld\n",bias_num, latch_order, dac_volt, dac_M, dac_B) ; */
      
      fgets(in_str,90,biasFile); /* read till end of line */ 
    }
    /* printf("%s\n","Line 55:") ; */
    fclose(biasFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for DCBiasG *******************/
long ufDCBiasGproc (genSubRecord *pgs) {

  genSubDCBiasPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  long timeout ;
  double numnum ;
  char *updated_volts ;
  long default_val, min_val, max_val ;

  if (strcmp(pgs->n,"") != 0) strcpy(dc_DEBUG_MODE,pgs->n) ;

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCBiasPrivate *)pgs->dpvt ;
  /* printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ; */
      /* printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ;  */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!dc_initialized)  init_dc_config() ;
      /* printf("################### Started reading the dc config file %ld\n", tickGet()) ; */
      /* read_Bias_file(pgs->name, pPriv) ; */ /* ZZZ */
      /* printf("################### finished reading the dc config file %ld\n ", tickGet()) ; */

      /* pPriv->port_no = dc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for dc Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy(pgs->valc,"UNKNOWN") ; /* power */
      
      for (i=0;i<24;i++) {
        dc_bias_M[i] = pPriv->CurrParam.M[i] ;
        dc_bias_B[i] = pPriv->CurrParam.B[i] ;
        dc_bias_volt[i] = pPriv->CurrParam.dac_volt_values[i] ;
        dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i];
      }
      
      pgs->novd = 24 ;
      pgs->nove = 24 ;
      pgs->novf = 24 ;
      pgs->novg = 24 ;

      pgs->vald = dc_bias_M ;
      pgs->vale = dc_bias_B;
      pgs->valf = dc_bias_volt ;
      pgs->valg = dc_bias_control ;
      strcpy(pgs->valh,pPriv->CurrParam.vGate) ;
      strcpy(pgs->vali,pPriv->CurrParam.vWell) ;
      strcpy(pgs->valj,pPriv->CurrParam.vBias) ;


      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */ 
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */ /* ZZZ */
      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
      } 
      */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */ /* ZZZ */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;

      pPriv->bias_inp.command_mode = *(long *)pgs->a ;
      pPriv->bias_inp.datum_directive = *(long *)pgs->b ;
      strcpy(pPriv->bias_inp.power,pgs->c) ;
      pPriv->bias_inp.park_directive = *(long *)pgs->d ;
      pPriv->bias_inp.dac_id = *(long *)pgs->e ;
      pPriv->bias_inp.dac_volt = *(double *)pgs->f ;
      pPriv->bias_inp.latch_dac_id = *(long *)pgs->g ;
      pPriv->bias_inp.read_all_directive = *(long *)pgs->h ;
      strcpy(pPriv->bias_inp.password,pgs->i) ;
      pPriv->bias_inp.DetTemp = *(long *)pgs->k ;
      strcpy(pPriv->bias_inp.vWell,pgs->l) ;
      strcpy(pPriv->bias_inp.vBias,pgs->m) ;
      strcpy(pPriv->bias_inp.PassEnable,pgs->o) ;
      /*
      printf("**************My inputs are : \n") ; 
      printf("pPriv->bias_inp.command_mode: %ld  \n",pPriv->bias_inp.command_mode) ; 
      printf("pPriv->bias_inp.datum_directive: %ld  \n",pPriv->bias_inp.datum_directive) ; 
      printf("pPriv->bias_inp.power: %s  \n",pPriv->bias_inp.power) ; 
      printf("pPriv->bias_inp.park_directive: %ld  \n",pPriv->bias_inp.park_directive) ; 
      printf("pPriv->bias_inp.dac_id: %ld  \n",pPriv->bias_inp.dac_id) ; 
      printf("pPriv->bias_inp.dac_volt: %f  \n",pPriv->bias_inp.dac_volt) ; 
      printf("pPriv->bias_inp.latch_dac_id: %ld  \n",pPriv->bias_inp.latch_dac_id) ; 
      printf("pPriv->bias_inp.read_all_directive: %ld  \n",pPriv->bias_inp.read_all_directive) ; 
      printf("pPriv->bias_inp.password: %s \n",pPriv->bias_inp.password) ; 
      printf("pPriv->bias_inp.vWell: %s  \n",pPriv->bias_inp.vWell) ; 
      printf("pPriv->bias_inp.vBias: %s  \n",pPriv->bias_inp.vBias) ; 
      */

      if ( ( pPriv->bias_inp.command_mode != -1 ) ||
           ( pPriv->bias_inp.datum_directive != -1 ) ||
           ( strcmp(pPriv->bias_inp.power,"") != 0 ) ||
           ( pPriv->bias_inp.park_directive != -1 ) ||
           ( pPriv->bias_inp.dac_id != -1 ) ||
           ( (long)pPriv->bias_inp.dac_volt != -1 ) ||
           ( pPriv->bias_inp.latch_dac_id != -1 ) ||
           ( pPriv->bias_inp.read_all_directive != -1 ) ||
           ( strcmp(pPriv->bias_inp.vWell,"") != 0) ||
           ( strcmp(pPriv->bias_inp.password,"") != 0) ||
           ( pPriv->bias_inp.DetTemp != -1 ) ||
           ( strcmp(pPriv->bias_inp.PassEnable,"") != 0) ||
           ( strcmp(pPriv->bias_inp.vBias,"") != 0) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;


        /* Clear the Gensub inputs */
        *(long *)pgs->a = -1 ; /* command mode  */
        *(long *)pgs->b = -1 ;
        strcpy(pgs->c,"") ;
        *(long *)pgs->d = -1 ;
        *(long *)pgs->e = -1 ;
        *(double *)pgs->f = -1.0 ;
        *(long *)pgs->g = -1 ;
        *(long *)pgs->h = -1 ;
        strcpy(pgs->i,"") ;
        *(long *)pgs->k = -1 ;
        strcpy(pgs->l,"") ;
        strcpy(pgs->m,"") ;
        
        strcpy(pgs->o,"") ;

        strcpy(pgs->j,"") ;  /* response from the agent via CA */
                          /* or the task that receives the response via TCP/IP */
        pgs->noj = 24 ; 

      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */
        pgs->noj = 24 ; 
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 24 ; 
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->bias_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->bias_inp.command_mode == SIMM_NONE) ||
            (pPriv->bias_inp.command_mode == SIMM_FAST) ||
            (pPriv->bias_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufDCBiasGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->bias_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          /* printf("My Command Mode is %ld \n",pPriv->mot_inp.command_mode) ; */
          /* Read some initial parameters from a file */

          if (read_Bias_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ;
          pPriv->port_no = dc_port_no ;

          if (pPriv->bias_inp.command_mode != SIMM_FAST) {
  	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
	      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->bias_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
      
            for (i=0;i<24;i++) {
              dc_bias_M[i] = pPriv->CurrParam.M[i] ;
              dc_bias_B[i] = pPriv->CurrParam.B[i] ;
              dc_bias_volt[i] = pPriv->CurrParam.dac_volt_values[i] ;
              dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i];
            }

            pgs->novd = 24 ;
            pgs->nove = 24 ;
            pgs->novf = 24 ;
            pgs->novg = 24 ;
            pgs->vald = dc_bias_M ;
            pgs->vale = dc_bias_B;
            pgs->valf = dc_bias_volt ;
            pgs->valg = dc_bias_control ;
            strcpy(pgs->valh,pPriv->CurrParam.vGate) ;
            strcpy(pgs->vali,pPriv->CurrParam.vWell) ;
            strcpy(pgs->valj,pPriv->CurrParam.vBias) ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      } /* We have another command */else {
        trx_debug("We have a bias Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Save the current parameters */
        if ( strcmp(pPriv->bias_inp.password,"") != 0) { /* construct the strings to send the password */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;

          num_str = UFcheck_bias_inputs (pPriv->bias_inp,com, rec_name,&pPriv->CurrParam, 
                                          pPriv->CurrParam.command_mode) ;

          /* Clear the input structure */
          pPriv->bias_inp.command_mode = -1 ;
          pPriv->bias_inp.datum_directive = -1 ;
          strcpy(pPriv->bias_inp.power,"") ;
          pPriv->bias_inp.park_directive = -1 ;
          pPriv->bias_inp.dac_id = -1 ;
          pPriv->bias_inp.dac_volt = -99999.99 ;
          pPriv->bias_inp.latch_dac_id = -1;
          pPriv->bias_inp.read_all_directive = -1 ;
          strcpy(pPriv->bias_inp.vGate,"") ;
          strcpy(pPriv->bias_inp.vWell,"") ;
          strcpy(pPriv->bias_inp.vBias,"") ;
          strcpy(pPriv->bias_inp.password,"");
          pPriv->bias_inp.DetTemp = -1;
          strcpy(pPriv->bias_inp.PassEnable,"") ;

        } /* not password */ else {
          for (i=0;i<24;i++) {
            pPriv->OldParam.M[i] = pPriv->CurrParam.M[i] ;
            pPriv->OldParam.B[i] = pPriv->CurrParam.B[i] ;
            pPriv->OldParam.dac_volt_values[i] = pPriv->CurrParam.dac_volt_values[i] ;
            pPriv->OldParam.dac_control_values[i] = pPriv->CurrParam.dac_control_values[i] ;
          }

          strcpy(pPriv->OldParam.power ,  pPriv->CurrParam.power) ;
          strcpy(pPriv->OldParam.vGate ,  pPriv->CurrParam.vGate) ;
          strcpy(pPriv->OldParam.vWell ,  pPriv->CurrParam.vWell) ;
          strcpy(pPriv->OldParam.vBias ,  pPriv->CurrParam.vBias) ;

          /* get the input into the current parameters */
	  if ( (pPriv->bias_inp.dac_id >= 0) && (pPriv->bias_inp.dac_id <=23) ) {
            if (pPriv->bias_inp.dac_volt != -99999.99) {
              pPriv->CurrParam.dac_volt_values[pPriv->bias_inp.dac_id] = pPriv->bias_inp.dac_volt ;
              pPriv->CurrParam.dac_control_values[pPriv->bias_inp.dac_id] =  
                pPriv->CurrParam.dac_volt_values[pPriv->bias_inp.dac_id]*pPriv->CurrParam.M[pPriv->bias_inp.dac_id] + 
                pPriv->CurrParam.B[pPriv->bias_inp.dac_id];
	    }
          }
          if (strcmp(pPriv->bias_inp.power,"") != 0)  
            strcpy(pPriv->CurrParam.power, pPriv->bias_inp.power)  ;
          /* if (strcmp(pPriv->bias_inp.vGate,"") != 0)  
	     strcpy(pPriv->CurrParam.vGate, pPriv->bias_inp.vGate)  ; */
          if (strcmp(pPriv->bias_inp.vWell,"") != 0)  
            strcpy(pPriv->CurrParam.vWell, pPriv->bias_inp.vWell)  ;
          if (strcmp(pPriv->bias_inp.vBias,"") != 0)  
            strcpy(pPriv->CurrParam.vBias, pPriv->bias_inp.vBias)  ;

          /* see if this a command right after an init */
          /*
          if (pPriv->send_init_param) { 
            pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
            pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
            pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
            pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
            pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current ; 
            pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
            pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ; 
          } 
          */
          /* formulate the command strings */
          com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
          strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;

          num_str = UFcheck_bias_inputs (pPriv->bias_inp,com, rec_name,&pPriv->CurrParam, 
                                          pPriv->CurrParam.command_mode) ;

          /* Clear the input structure */
          pPriv->bias_inp.command_mode = -1 ;
          pPriv->bias_inp.datum_directive = -1 ;
          strcpy(pPriv->bias_inp.power,"") ;
          pPriv->bias_inp.park_directive = -1 ;
          pPriv->bias_inp.dac_id = -1 ;
          pPriv->bias_inp.dac_volt = -99999.99 ;
          pPriv->bias_inp.latch_dac_id = -1;
          pPriv->bias_inp.read_all_directive = -1 ;
          strcpy(pPriv->bias_inp.vGate,"") ;
          strcpy(pPriv->bias_inp.vWell,"") ;
          strcpy(pPriv->bias_inp.vBias,"") ;
	}
        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          /* printf("********** Thn number of strings is : %d\n",num_str) ; */
          if (bingo) for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;
          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */
  
            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
     
            for (i=0;i<24;i++) {
              dc_bias_M[i] = pPriv->CurrParam.M[i] ;
              dc_bias_B[i] = pPriv->CurrParam.B[i] ;
              dc_bias_volt[i] = pPriv->CurrParam.dac_volt_values[i] ;
              dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i];
            }
 
            pgs->novd = 24 ;
            pgs->nove = 24 ;
            pgs->novf = 24 ;
            pgs->novg = 24 ;
            pgs->vald = dc_bias_M ;
            pgs->vale = dc_bias_B;
            pgs->valf = dc_bias_volt ;
            pgs->valg = dc_bias_control ;
            strcpy(pgs->valh,pPriv->CurrParam.vGate) ;
            strcpy(pgs->vali,pPriv->CurrParam.vWell) ;
            strcpy(pgs->valj,pPriv->CurrParam.vBias) ;

            pPriv->send_init_param = 0 ;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState,
                                 DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) {
	      if( _verbose )
	        printf("ufDCBiasGproc> sending to agent, genSub record: %s\n", pgs->name); 
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      printf("ufDCBiasGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
	      status = 0;
	    } 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
              strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else { /* num_str <= 0 */
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */

            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
        } 
      }
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      pgs->noj = 24 ; 
      /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 24 ; 
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /*
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) { 
        cancelCallback (pPriv->pCallback);  
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ; 
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ; 
        strcpy(pgs->i,"") ; 
        strcpy(pgs->k,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;
        pPriv->commandState = TRX_GS_SENDING;
        return OK ;   
      } 
      */

      strcpy(response,pgs->j) ;
      pgs->noj = 24 ; 
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"MID",dc_DEBUG_MODE) ; 
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* printf("******** Is this a callback from timeout?\n") ;  
	   printf("%s\n",pPriv->errorMessage) ; */
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        timeout = TRX_DC_TIMEOUT ;
        if ( (ticksNow >= (pPriv->startingTicks + timeout)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Bias Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the output links */

          strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
          /*
          for (i=0;i<24;i++) { 
            dc_bias_M[i] = pPriv->CurrParam.M[i] ; 
            dc_bias_B[i] = pPriv->CurrParam.B[i] ; 
            dc_bias_volt[i] = pPriv->CurrParam.dac_volt_values[i] ; 
            dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i]; 
          } 

          pgs->vald = dc_bias_M ; 
          pgs->vale = dc_bias_B; 
          pgs->valf = dc_bias_volt ; 
          pgs->valg = dc_bias_control ;
          */
          strcpy(pgs->valh,pPriv->CurrParam.vGate) ;
          strcpy(pgs->vali,pPriv->CurrParam.vWell) ;
          strcpy(pgs->valj,pPriv->CurrParam.vBias) ;

          pPriv->send_init_param = 0 ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = -1.0 ;
          numnum = strtod(response,&endptr) ;
          /* if (*endptr != '\0'){  we do not have a number */
          if (numnum <= 0) { /*we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            for (i=0;i<24;i++) {
              pPriv->CurrParam.M[i] = pPriv->OldParam.M[i] ;
              pPriv->CurrParam.B[i] = pPriv->OldParam.B[i] ;
              pPriv->CurrParam.dac_volt_values[i] = pPriv->OldParam.dac_volt_values[i] ;
              pPriv->CurrParam.dac_control_values[i] = pPriv->OldParam.dac_control_values[i] ;
            }
  
            strcpy(pPriv->CurrParam.power ,  pPriv->OldParam.power) ;
            strcpy(pPriv->CurrParam.vGate ,  pPriv->OldParam.vGate) ;
            strcpy(pPriv->CurrParam.vWell ,  pPriv->OldParam.vWell) ;
            strcpy(pPriv->CurrParam.vBias ,  pPriv->OldParam.vBias) ;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            /* what am I supposed to update this thing with? Latif is an idiot :P */

            updated_volts = pgs->j ;
            for (i=0;i<24;i++) {
              /* dc_bias_M[i] = pPriv->CurrParam.M[i] ;
		 dc_bias_B[i] = pPriv->CurrParam.B[i] ; */
              /* numnum = strtod(updated_volts,&endptr) ; */
              parse_bias_values(updated_volts, &pPriv->CurrParam.dac_control_values[i], &default_val, &min_val, &max_val) ;
              /* dc_bias_volt[i] = numnum; 
              temp_double = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] + 
                                                      (double)pPriv->CurrParam.B[i] ; 
              temp_long = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] + 
                                                      pPriv->CurrParam.B[i] ; 
              numnum = temp_double - (double)temp_long ; 
              if (numnum >= 0.5) temp_long = temp_long + 1; 
              pPriv->CurrParam.dac_control_values[i] = temp_long ; */

              dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i]; 
              dc_bias_def[i] = default_val ;
              dc_bias_min[i] = min_val ;
              dc_bias_max[i] = max_val ;
              dc_bias_volt[i] = (pPriv->CurrParam.dac_control_values[i] - dc_bias_B[i])/dc_bias_M[i] ;
              updated_volts = updated_volts + 40 ;
            }

            pgs->novd = 24 ;
            pgs->nove = 24 ;
            pgs->novf = 24 ;
            pgs->novg = 24 ;
            pgs->novk = 24 ;
            pgs->novl = 24 ;
            pgs->novm = 24 ;
 
            pgs->vald = dc_bias_M ;
            pgs->vale = dc_bias_B;
            pgs->valf = dc_bias_volt ;
            pgs->valg = dc_bias_control ;
            pgs->valk = dc_bias_def;
            pgs->vall = dc_bias_min;
            pgs->valm = dc_bias_max;

            pgs->noj = 24;

          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 24 ; 
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the preamp file *****************/

long read_preAmp_file (char *rec_name, genSubDCPreAmpPrivate *pPriv) {

  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for preampG *******************/
long ufDCPreAmpGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDCPreAmpPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
  int i; 
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDCPreAmpPrivate *) malloc (sizeof(genSubDCPreAmpPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = dc_port_no ;
  pPriv->agent = 3; /* dc agent */

  for (i = 0;i<24;i++) {
    pPriv->CurrParam.default_values[i] = -1 ;
    pPriv->CurrParam.preAmp_control_values[i] = -1 ;
  }

  pPriv->CurrParam.adjust_value = 0;
  pPriv->CurrParam.global_set = -1;

  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */

  strcpy(pPriv->CurrParam.power,"") ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* command mode  */
  *(long *)pgs->b = -1 ;
  strcpy(pgs->c,"") ;
  *(long *)pgs->d = -1 ;
  *(long *)pgs->e = -1 ;
  *(double *)pgs->f = -99999.99 ;
  *(long *)pgs->g = -1 ;
  *(long *)pgs->h = -1 ;
  *(long *)pgs->k = 0 ;
   strcpy(pgs->i,"NONE") ;

  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_DC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->preAmp_inp.command_mode = -1 ;
  pPriv->preAmp_inp.datum_directive = -1 ;
  strcpy(pPriv->preAmp_inp.power,"") ;
  pPriv->preAmp_inp.park_directive = -1 ;
  pPriv->preAmp_inp.preAmp_id = -1 ;
  pPriv->preAmp_inp.preAmp_volt = -99999.99 ;
  pPriv->preAmp_inp.latch_preAmp_id = -1;
  pPriv->preAmp_inp.read_all_directive = -1 ;
  pPriv->preAmp_inp.global_set = -1;
  pPriv->preAmp_inp.adjust_value = 0;

  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
  pgs->noj = 24 ; 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the preamp file *****************/
long read_PreAmp_file (char *rec_name, genSubDCBiasPrivate *pPriv) {
  /* Initialization File */
  FILE *biasFile ;
  char in_str[90] ;
  char filename[50] ;
  double numnum /*, numnum1, numnum2 */;
  double dac_volt,  dac_M ;
  long  dac_B;
  int i ;
  long bias_num, latch_order ;
  long temp_long ;
  double temp_double ;
 
  strcpy(filename,dc_bias_filename);

  if ((biasFile = fopen(filename,"r")) == NULL) {
    trx_debug("DC Bias file could not be opened", rec_name,"MID",dc_STARTUP_DEBUG_MODE);
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile);  /* comment line */
    fgets(in_str,90,biasFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    strcpy (dc_host_ip,in_str) ;
    i = 0;
    while ( ( (isdigit(dc_host_ip[i])) || (dc_host_ip[i] == '.')) && (i < 15)) i++ ;
    dc_host_ip[i] = '\0' ;

    /* Read the port number */
    fgets(in_str,90,biasFile); /* comment line */ 
   
    /* 
    fgets(in_str,40,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(biasFile,"%lf",&numnum) ; /* port number */

    dc_port_no = (long) numnum ;
    fgets(in_str,90,biasFile); /* read till end of line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    fgets(in_str,90,biasFile); /* comment line */ 
    /* printf("%s\n","Line 0:") ; */
    for (i=0;i<24;i++) {
      fgets(in_str,15,biasFile);  /* dac name */
      
      fscanf(biasFile, "%ld %ld %lf %lf %ld",&bias_num, &latch_order, &dac_volt, &dac_M, &dac_B) ;
      pPriv->CurrParam.M[i] = dac_M ;
      pPriv->CurrParam.B[i] = dac_B ;
      pPriv->CurrParam.dac_volt_values[i] = dac_volt ;

      temp_double = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                               (double)pPriv->CurrParam.B[i] ;
      temp_long = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                               pPriv->CurrParam.B[i] ;
      numnum = temp_double - (double)temp_long ;
      if (numnum >= 0.5) temp_long = temp_long + 1;
      pPriv->CurrParam.dac_control_values[i] = temp_long ;
      
      /* printf("%s: ",in_str) ;
	 printf("%ld %ld %f %f %ld\n",bias_num, latch_order, dac_volt, dac_M, dac_B) ; */
      
      fgets(in_str,90,biasFile); /* read till end of line */ 
    }
    /* printf("%s\n","Line 55:") ; */
    fclose(biasFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for DCBiasG *******************/
long ufDCPreAmpGproc (genSubRecord *pgs) {
  genSubDCPreAmpPrivate  *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  long timeout ;
  double numnum ;
  char *updated_volts ;
  long temp_long ;
  strcpy(dc_DEBUG_MODE,pgs->i) ;

  /* trx_debug("",pgs->name,"FULL",dc_DEBUG_MODE) ; */

  pPriv = (genSubDCPreAmpPrivate *)pgs->dpvt ;
  /* printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ; */
      /* printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!dc_initialized)  init_dc_config() ;
      /* printf("################### Started reading the bias file %ld\n", tickGet()) ; */
      /* read_Bias_file(pgs->name, pPriv) ; */ /* ZZZ */
      /* printf("################### finished reading the bias file %ld\n ", tickGet()) ; */

      /* pPriv->port_no = dc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for bieas Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      /* strcpy(pgs->valc,"UNKNOWN") ; */ /* power */
      strcpy(pgs->valc,"") ;
      for (i=0;i<24;i++) {
        preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
        preAmp_control[i] = pPriv->CurrParam.preAmp_control_values[i] ;
      }

      pgs->vald = preAmp_control ;
      pgs->vale = preAmp_default ;


      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */ 
      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ; */ /* ZZZ */
      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",dc_STARTUP_DEBUG_MODE); 
      } 
      */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",dc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",dc_DEBUG_MODE) ;

      pPriv->preAmp_inp.command_mode = *(long *)pgs->a ;
      pPriv->preAmp_inp.datum_directive = *(long *)pgs->b ;
      /* strcpy(pPriv->preAmp_inp.power,pgs->c) ; */
      strcpy(pPriv->preAmp_inp.power,"") ;
      pPriv->preAmp_inp.park_directive = *(long *)pgs->d ;
      pPriv->preAmp_inp.preAmp_id = *(long *)pgs->e ;
      pPriv->preAmp_inp.preAmp_volt = *(double *)pgs->f ;
      /*pPriv->preAmp_inp.latch_preAmp_id = *(long *)pgs->g ; */
      pPriv->preAmp_inp.global_set = *(long *)pgs->g ;
      pPriv->preAmp_inp.read_all_directive = *(long *)pgs->h ;
      pPriv->preAmp_inp.adjust_value = *(long *)pgs->k ;
      if ( ( pPriv->preAmp_inp.command_mode != -1 ) ||
           ( pPriv->preAmp_inp.datum_directive != -1 ) ||
           ( strcmp(pPriv->preAmp_inp.power,"") != 0 ) ||
           ( pPriv->preAmp_inp.park_directive != -1 ) ||
           ( pPriv->preAmp_inp.preAmp_id != -1 ) ||
           ( pPriv->preAmp_inp.preAmp_volt != -99999.99 ) ||
           ( pPriv->preAmp_inp.global_set != -1 ) ||
           ( pPriv->preAmp_inp.adjust_value != 0 ) ||
           ( pPriv->preAmp_inp.read_all_directive != -1 ) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",dc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",dc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",dc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",dc_DEBUG_MODE) ;


        /* Clear the Gensub inputs */
        *(long *)pgs->a = -1 ; /* command mode  */
        *(long *)pgs->b = -1 ;
        strcpy(pgs->c,"") ;
        *(long *)pgs->d = -1 ;
        *(long *)pgs->e = -1 ;
        *(double *)pgs->f = -99999.99 ;
        *(long *)pgs->g = -1 ;
        *(long *)pgs->h = -1 ;
        *(long *)pgs->k = 0 ;

        strcpy(pgs->j,"") ;  /* response from the agent via CA */
                          /* or the task that receives the response via TCP/IP */
        pgs->noj = 24 ; 

      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */
        pgs->noj = 24 ; 
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",dc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        pgs->noj = 24 ; 
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->preAmp_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->preAmp_inp.command_mode == SIMM_NONE) ||
            (pPriv->preAmp_inp.command_mode == SIMM_FAST) ||
            (pPriv->preAmp_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufDCPreAmpGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", dc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->preAmp_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          /* printf("My Command Mode is %ld \n",pPriv->mot_inp.command_mode) ; */
          /* Read some initial parameters from a file */

          if (read_preAmp_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ;
          pPriv->port_no = dc_port_no ;

          if (pPriv->preAmp_inp.command_mode != SIMM_FAST) {
  	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",dc_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,dc_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",dc_DEBUG_MODE) ;
	      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->preAmp_inp.command_mode == SIMM_FAST) ||
               (pPriv->socfd > 0) ) {
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
      
            for (i=0;i<24;i++) {
              preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
              preAmp_control[i] = pPriv->CurrParam.preAmp_control_values[i];
            }

            pgs->vald = pPriv->CurrParam.preAmp_control_values ;
            pgs->vale = pPriv->CurrParam.default_values;


            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            } 
          }
	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        }
      } else {
        trx_debug("We have a bias Command",pgs->name,"FULL",dc_DEBUG_MODE) ;
        /* Save the current parameters */
        for (i=0;i<24;i++) {
          pPriv->OldParam.default_values[i] = pPriv->CurrParam.default_values[i] ;
          pPriv->OldParam.preAmp_control_values[i] = pPriv->CurrParam.preAmp_control_values[i] ;
        }

        strcpy(pPriv->OldParam.power ,  pPriv->CurrParam.power) ;

        /* get the input into the current parameters */
        
        if (pPriv->preAmp_inp.global_set != -1) {
          for (i=0 ; i<24;i++) pPriv->CurrParam.preAmp_control_values[i] = pPriv->preAmp_inp.global_set ;
        }
        if (pPriv->preAmp_inp.adjust_value != -1) {
          for (i=0 ; i<24;i++) {
            temp_long = pPriv->CurrParam.preAmp_control_values[i] + pPriv->preAmp_inp.adjust_value ;
            if (temp_long < 0) 
              pPriv->CurrParam.preAmp_control_values[i] = 0 ;
            else  
              pPriv->CurrParam.preAmp_control_values[i] = temp_long;
	  }
        }
        strcpy(pPriv->CurrParam.power,pPriv->preAmp_inp.power) ; /* power */
        pPriv->CurrParam.global_set = pPriv->preAmp_inp.global_set ;
        pPriv->CurrParam.adjust_value = pPriv->preAmp_inp.adjust_value ;

	if ( (pPriv->preAmp_inp.preAmp_id >= 0) && (pPriv->preAmp_inp.preAmp_id <=23) ) {
          if (pPriv->preAmp_inp.preAmp_volt != -99999.99) {
            pPriv->CurrParam.default_values[pPriv->preAmp_inp.preAmp_id] = pPriv->preAmp_inp.preAmp_volt ;
	    pPriv->CurrParam.preAmp_control_values[pPriv->preAmp_inp.preAmp_id] =  
	      pPriv->CurrParam.preAmp_control_values[pPriv->preAmp_inp.preAmp_id] ;
	  }
        }
        if (strcmp(pPriv->preAmp_inp.power,"") != 0)  
          strcpy(pPriv->CurrParam.power, pPriv->preAmp_inp.power)  ;

          /* see if this a command right after an init */
        /*
        if (pPriv->send_init_param) { 
          pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ; 
          pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ; 
          pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration; 
          pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ; 
          pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current ; 
          pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ; 
          pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ;  
        } 
        */
        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;

        num_str = UFcheck_preAmp_inputs (pPriv->preAmp_inp, com, rec_name, &pPriv->CurrParam, 
                                        pPriv->CurrParam.command_mode) ;

        /* Clear the input structure */
        pPriv->preAmp_inp.command_mode = -1 ;
        pPriv->preAmp_inp.datum_directive = -1 ;
        strcpy(pPriv->preAmp_inp.power,"") ;
        pPriv->preAmp_inp.park_directive = -1 ;
        pPriv->preAmp_inp.preAmp_id = -1 ;
        pPriv->preAmp_inp.preAmp_volt = -99999.99 ;
        pPriv->preAmp_inp.adjust_value = 0;
        pPriv->preAmp_inp.read_all_directive = -1 ;
        pPriv->preAmp_inp.global_set = -1 ;


        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          /* printf("********** Thn number of strings is : %d\n",num_str) ; */
          if (bingo) for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;
          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* update the output links */

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */

            for (i=0;i<24;i++) {
              preAmp_default[i] = pPriv->CurrParam.default_values[i] ;
              preAmp_control[i] = pPriv->CurrParam.preAmp_control_values[i];
            }

            pgs->vald = pPriv->CurrParam.preAmp_control_values ;
            pgs->vale = pPriv->CurrParam.default_values;

            pPriv->send_init_param = 0 ;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState,
                                 DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) {
	      if( _verbose )
	        printf("ufDCPreAmpGproc> sending to agent, genSub record: %s\n", pgs->name); 
	      status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            }
	    else {
	      printf("ufDCPreAmpGproc> send socfd bad? , genSub record: %s\n", pgs->name); 
	      status = 0;
	    } 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
              strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_DC_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else { /* num_str <= 0 */
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
          }
	}
      }
      
      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      pgs->noj = 24 ; 
      /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",dc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 24 ; 
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",dc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /*
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) { 
        cancelCallback (pPriv->pCallback);  
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ; 
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ; 
        strcpy(pgs->i,"") ; 
        strcpy(pgs->k,"") ; 
        requestCallback(pPriv->pCallback,0 ) ;
        pPriv->commandState = TRX_GS_SENDING;
        return OK ;   
      } 
      */

      strcpy(response,pgs->j) ;
      pgs->noj = 24 ; 
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"MID",dc_DEBUG_MODE) ; 
      }
  
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* printf("******** Is this a callback from timeout?\n") ;  
	   printf("%s\n",pPriv->errorMessage) ; */
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        timeout = TRX_DC_TIMEOUT ;
        if ( (ticksNow >= (pPriv->startingTicks + timeout)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Bias Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",dc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the output links */

          strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power */
          /*
          for (i=0;i<24;i++) { 
            dc_bias_M[i] = pPriv->CurrParam.M[i] ; 
            dc_bias_B[i] = pPriv->CurrParam.B[i] ; 
            dc_bias_volt[i] = pPriv->CurrParam.dac_volt_values[i] ; 
            dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i]; 
          } 

          pgs->vald = dc_bias_M ; 
          pgs->vale = dc_bias_B; 
          pgs->valf = dc_bias_volt ; 
          pgs->valg = dc_bias_control ;
          */


          pPriv->send_init_param = 0 ;

          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",dc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            for (i=0;i<24;i++) {
              pPriv->CurrParam.default_values[i] = pPriv->OldParam.default_values[i] ;
              pPriv->CurrParam.preAmp_control_values[i] = pPriv->OldParam.preAmp_control_values[i] ;
            }
  
            strcpy(pPriv->CurrParam.power ,  pPriv->OldParam.power) ;


            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",dc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            /* what am I supposed to update this thing with? Latif is an idiot :P */

            updated_volts = pgs->j ;
            /*
            for (i=0;i<24;i++) {
	      
              dc_bias_M[i] = pPriv->CurrParam.M[i] ;
              dc_bias_B[i] = pPriv->CurrParam.B[i] ;
              numnum = strtod(updated_volts,&endptr) ;

              dc_bias_volt[i] = numnum;
              temp_double = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                                      (double)pPriv->CurrParam.B[i] ;
              temp_long = pPriv->CurrParam.dac_volt_values[i]*pPriv->CurrParam.M[i] +
                                                      pPriv->CurrParam.B[i] ;
              numnum = temp_double - (double)temp_long ;
              if (numnum >= 0.5) temp_long = temp_long + 1;
              pPriv->CurrParam.dac_control_values[i] = temp_long ;

              dc_bias_control[i] = pPriv->CurrParam.dac_control_values[i];
              updated_volts = updated_volts + 40 ;
              
	      }*/

            pgs->vald = dc_bias_M ;
            pgs->vale = dc_bias_B;
            pgs->valf = dc_bias_volt ;
            pgs->valg = dc_bias_control ;

            pgs->noj = 24;

          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      pgs->noj = 24 ; 
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",dc_DEBUG_MODE) ;
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/****************** SNAM forTCSISnodComplG ****************/
long ufTCSISnodComplGinit (genSubRecord *pgs) {

  strcpy(pgs->j,"") ;
  strcpy(pgs->vala,"") ;
  return OK ;
}



/**********************************************************/
/**********************************************************/
/**********************************************************/
/****************** SNAM forTCSISnodCompl G ***************/
long ufTCSISnodComplGproc (genSubRecord *pgs) {

  strcpy(pgs->vala,pgs->j) ;
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/****************** SNAM for MetaEnvG ****************/
long ufMetaEnvGinit (genSubRecord *pgs) {
  *(double *)pgs->a = -1.0 ;
  *(double *)pgs->b = -1.0 ;
  *(double *)pgs->c = -1.0 ;
  *(double *)pgs->d = -1.0 ;
  *(double *)pgs->e = -1.0 ;
  *(double *)pgs->f = -1.0 ;
  *(double *)pgs->g = -1.0 ;
  *(double *)pgs->h = -1.0 ;

  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/****************** SNAM for MetaEnvG ***************/
long ufMetaEnvGproc (genSubRecord *pgs) {
  char tempstr[40] ;

  dc_env_param[0] = *(double *)pgs->a ;
  dc_env_param[1] = *(double *)pgs->b ;
  dc_env_param[2] = *(double *)pgs->c ;
  dc_env_param[3] = *(double *)pgs->d ;
  dc_env_param[4] = *(double *)pgs->e ;
  dc_env_param[5] = *(double *)pgs->f ;
  dc_env_param[6] = *(double *)pgs->g ;
  dc_env_param[7] = *(double *)pgs->h ;
  pgs->nova = 8 ;
  pgs->vala = dc_env_param ;
  sprintf(tempstr,"%8.2f",dc_env_param[0]) ;
  strcpy(pgs->valb,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[1]) ;
  strcpy(pgs->valc,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[2]) ;
  strcpy(pgs->vald,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[3]) ;
  strcpy(pgs->vale,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[4]) ;
  strcpy(pgs->valf,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[5]) ;
  strcpy(pgs->valg,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[6]) ;
  strcpy(pgs->valh,tempstr) ;
  sprintf(tempstr,"%8.2f",dc_env_param[7]) ;
  strcpy(pgs->vali,tempstr) ;
  return OK ;
}




#endif /* __UFGEMGENSUBCOMMDC_C__  */

