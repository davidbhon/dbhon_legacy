#if !defined(__UFGEMCOMM_C__)
#define __UFGEMCOMM_C__ "RCS: $Name:  $ $Id: ufGemComm.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufUFGEMCOMMC[] = __UFGEMCOMM_C__;

#include <sys/types.h>
#include <sys/socket.h>
#include <stdioLib.h>
#include <string.h>
#include <time.h>
#include <selectLib.h>
#include <sockLib.h>
#include <sys/stat.h>

#include <string.h>
#include <stdio.h>

#include "ufClient.h"
#include "ufLog.h"

#include "trecs.h"
#include "ufGemComm.h"

#define CAR_IDLE_Comm 0
#define CAR_ERROR_Comm 3

static int _verbose = 1;
static int namPosTableInit = 0;	/* false */

/**
 * Establish a connection to a UFAgent host, and sends a timestamp header.
 * Returns -1 on error, otherwise the newly opened socket descriptor.
 */
int
UFGetConnection (const char *name, int port_no, const char *host)
{

  /*Information to send to the agent */
  static ufProtocolHeader sendheader;
  char *temp_str = 0;
  char temp_str2[80];

  int bytesSent = 0 ;
  /* Integer to hold the socket number */
  int socfd = -1;

  /* construct the header */
  memset( &sendheader, 0, sizeof( ufProtocolHeader ) ) ;
  strcpy (sendheader.name, name);
  sendheader.type = 0;
  sendheader.elem = 0;
  sendheader.seqCnt = 1;
  sendheader.seqTot = 1;

  strcpy (temp_str2, ufHostTime (NULL));
  temp_str = strchr (temp_str2, ':');
  temp_str = temp_str + 2;
  strcpy (sendheader.timestamp, temp_str);
  sendheader.timestamp[24] = 0;
  /* printf("The time stamp is: %s\n",sendheader.timestamp) ; */

  sendheader.length = ufHeaderLength (&sendheader);
  /* attempt to establish the connection */
  socfd = ufConnect (host, port_no);

  /* send the header if we are connected */
  if( socfd < 0 )
    {
      return -1 ;
    }

  /* Connect succeeded, attempt to send the header */
  bytesSent = ufHeaderSend (socfd, &sendheader);

  /* Was header send successful? */
  if( bytesSent < 0 )
    {
      /* Error sending the header */
      close( socfd ) ;
     
      /* Return an error */
      return -1 ;
    }

  /* All is well, return the newly connected socket */
  return socfd;
}

long
UFcheck_long (long value, long val1, long val2)
{
  long status = 0;
  if ((value != val1) && (value != val2))
    status = not_valid;
  return status;
}

long
UFcheck_long_r (long value, long loval1, long upval2)
{
  long status = 0;
  if ((value < loval1) || (value > upval2))
    status = not_valid;
  return status;
}

long
UFcheck_double (double value, double loval1, double upval2)
{
  long status = 0;
  if ((value < loval1) || (value > upval2))
    status = not_valid;
  return status;
}

int
UFcheck_motor_inputs (ufMotorInput mot_inp, char **com, char indexer,
		      char *rec_name, ufMotorCurrParam * CurrParam,
		      long com_mode)
{
  long temp_long;
  double acceleration;
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];
  char sign;
  char command[10];

  strcpy (car_name, rec_name);
  /* car_name[strlen(car_name)-1] = 'C' ; */
  /*strcat (car_name,".I") ; */
  /*  strcpy (ai_name, rec_name);
     ai_name[strlen(car_name)-6] = 0 ;
     strcat(ai_name,"currPos.INP") ;
   */
  if (!com_mode)
    sprintf (command, "%craw", indexer);
  else
    sprintf (command, "%csim", indexer);

  /* pPriv->CurrParam.steps_from_home = 0.0 ; */
  CurrParam->steps_to_add = 0.0;
  CurrParam->homing = 0;

  /* printf("********** Initial velocity\n") ; */
  /* initital velocity */
  temp_long = (long) (mot_inp.init_velocity);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.init_velocity, init_velocity_lo,
	   init_velocity_hi) == not_valid)
	{
	  printf ("********** Initial velocity is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /*  printf("********** Initial velocity\n") ; */
	  num_str = num_str + 1;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: initial velocity");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%cI %6.2f", indexer, mot_inp.init_velocity);
	  /*sprintf(temp_str,"%c I %6.0f",indexer,mot_inp.init_velocity); */
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->init_velocity = mot_inp.init_velocity;
	  /*     num_str++;
	     strcpy(temp_str,"Error sending initial velocity") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }
  /* printf("********** Slew velocity\n"); */
  /* slew velocity */

  temp_long = (long) (mot_inp.slew_velocity);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.slew_velocity, slew_velocity_lo,
	   slew_velocity_hi) == not_valid)
	{
	  printf ("********** Slew velocity is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Slew velocity\n"); */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: slew velocity");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;

	  sprintf (temp_str, "%cV %6.2f", indexer, mot_inp.slew_velocity);
	  /*      sprintf(temp_str,"%c V %6.0f",indexer,mot_inp.slew_velocity); */
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->slew_velocity = mot_inp.slew_velocity;
	  /*      num_str++;
	     strcpy(temp_str,"Error sending slew velocity") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }
  /* printf("********** Acceleration\n") ; */
  /* acceleration */

  acceleration = (mot_inp.acceleration);
  if (acceleration != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.acceleration, acceleration_lo,
	   acceleration_hi) == not_valid)
	{
	  printf ("********** Acceleration is not valid\n");
	  status = not_valid;
	  return status;
	}
    }
  /*  printf("********** Deceleration\n") ; */
  /* deceleration */
  temp_long = (long) (mot_inp.deceleration);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.deceleration, deceleration_lo,
	   deceleration_hi) == not_valid)
	{
	  printf ("********** Deceleration is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  if (acceleration != (long) link_clear1)
	    {
	      /* printf("********** Deceleration\n") ; */
	      num_str++;
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrSend: the acceleration");
	      strcpy (com[num_str - 1], temp_str);
	      num_str++;
	      sprintf (temp_str, "%cK %ld %ld", indexer, (long) acceleration,
		       (long) mot_inp.deceleration);
	      /* sprintf(temp_str,"%c K %5ld %6.0f",indexer,acceleration, mot_inp.deceleration); */
	      strcpy (com[num_str - 1], temp_str);
	      CurrParam->acceleration = mot_inp.acceleration;
	      CurrParam->deceleration = mot_inp.deceleration;
	      /*   num_str++;
	         strcpy(temp_str,"Error sending the acceleration") ;
	         strcpy(com[num_str-1],temp_str) ; */
	    }
	}
    }
  /* printf("********** Drive Current\n") ; */
  /* drive current */

  temp_long = (long) (mot_inp.drive_current);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double
	  (mot_inp.drive_current, drive_current_lo,
	   drive_current_hi) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: the current");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "%cY 0 %5.2f", indexer, mot_inp.drive_current);
	  /* sprintf(temp_str,"%c Y  0 %6.0f",indexer,mot_inp.drive_current); */
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->drive_current = mot_inp.drive_current;
	  /*      num_str++;
	     strcpy(temp_str,"Error sending the current") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }

  /* pPriv->mot_inp.setting_time */
  temp_long = (long) (mot_inp.setting_time);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double (mot_inp.setting_time, 0, 255) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: setting time");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "%cE %5.2f", indexer, mot_inp.setting_time);
	  /* sprintf(temp_str,"%c Y  0 %6.0f",indexer,mot_inp.drive_current); */
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->setting_time = mot_inp.setting_time;
	  /*      num_str++;
	     strcpy(temp_str,"Error sending the current") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }
  /*
     pPriv->mot_inp.jog_speed_slow
     pPriv->mot_inp.jog_speed_hi
   */

  temp_long = (long) (mot_inp.jog_speed_slow);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_double (mot_inp.jog_speed_slow, 0, 255) == not_valid)
	{
	  printf ("********** Drive Current is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Drive Current\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: jog_speed_slow");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "%cB %ld %ld", indexer, mot_inp.jog_speed_slow,
		   mot_inp.jog_speed_hi);
	  /* sprintf(temp_str,"%c Y  0 %6.0f",indexer,mot_inp.drive_current); */
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->jog_speed_slow = mot_inp.jog_speed_slow;
	  CurrParam->jog_speed_hi = mot_inp.jog_speed_hi;
	  /*      num_str++;
	     strcpy(temp_str,"Error sending the current") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }

  /* printf("********** Test Directive\n") ; */
  /* Test Directive */
  temp_long = (long) (mot_inp.test_directive);
  /* printf("********** Test Directive0\n") ; */
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_long (temp_long, 0, 1) == not_valid)
	{
	  printf ("********** Test Directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Test Directive1\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error testing the system");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "%cZ 0", indexer);
	  strcpy (com[num_str - 1], temp_str);
	  /*   num_str++;
	     strcpy(temp_str,"Error testing the system") ;
	     strcpy(com[num_str-1],temp_str) ;
	     num_str++;
	     strcpy(temp_str,"AI") ;
	     strcpy(com[num_str-1],temp_str) ;
	     num_str++;
	     strcpy(temp_str,ai_name) ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }

  /* printf("********** Datum Speed\n") ; */
  /* datum speed */
  temp_long = (long) (mot_inp.datum_speed);
  if (temp_long != (long) link_clear1)
    {
      if ((UFcheck_double
	   (mot_inp.datum_speed, datum_speed_lo, datum_speed_hi) == not_valid)
	  && (CurrParam->datum_speed == not_valid))
	{
	  printf ("********** Datum Speed is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  CurrParam->datum_speed = temp_long;
	}
    }

  /* printf("********** Datum Direction\n"); */
  /* datum direction */
  temp_long = (long) (mot_inp.datum_direction);
  if (temp_long != (long) link_clear1)
    {
      if ((UFcheck_long (temp_long, 0, 1) == not_valid)
	  && (CurrParam->datum_direction == not_valid))
	{
	  printf ("********** Datum Direction is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	CurrParam->datum_direction = temp_long;
    }
  /* printf("********** Datum Motor directive\n") ; */
  /* datum motor directive */

  temp_long = (long) (mot_inp.datum_motor_directive);
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_long (temp_long, 0, 1) == not_valid)
	{
	  printf ("********** Datum Motor directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  if ((CurrParam->datum_speed > 0)
	      && (UFcheck_long_r (CurrParam->datum_direction, 0, 1) !=
		  not_valid))
	    {
	      /* printf("********** Datum Motor directive\n") ; */
	      num_str++;
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrDatum");
	      strcpy (com[num_str - 1], temp_str);
	      num_str++;
	      if (use_near_home)
		{
		  if (Hi_Level_Move[(int) indexer - 65])
		    {
		      if (use_backlash)
			near_home_margin =
			  mot_inp.num_steps - CurrParam->backlash;
		      sprintf (temp_str, "%cN %d", indexer, near_home_margin);
		      strcpy (com[num_str - 1], temp_str);
		      temp_long = (long) (mot_inp.num_steps);
		      if (temp_long == (long) link_clear0)
			{
			  num_str++;
			  sprintf (temp_str, "%s %s", command,
				   "!!ErrSend: steps");
			  strcpy (com[num_str - 1], temp_str);
			  num_str++;
			  sign = ' ';
			  temp_long =
			    -(mot_inp.num_steps - CurrParam->backlash);
			  if (temp_long >= 0)
			    sign = '+';
			  sprintf (temp_str, "%c%c%ld", indexer, sign,
				   temp_long);
			  strcpy (com[num_str - 1], temp_str);
			}
		    }		/* high level move */
		  else
		    {		/* low level move */
		      sprintf (temp_str, "%cF %ld %ld", indexer,
			       CurrParam->datum_speed,
			       CurrParam->datum_direction);
		      strcpy (com[num_str - 1], temp_str);
		      if (fabs (CurrParam->backlash) > 0.0)
			{
			  /* long signval = 1; *//* home switch wiring set to high-to-low transition */
			  long signval = -1;	/* home switch wiring set to low-to-high transition */
			  num_str++;
			  sprintf (temp_str, "%s %s", command,
				   "!!ErrSend: steps");
			  strcpy (com[num_str - 1], temp_str);
			  num_str++;
			  /*
			     if home switch is wired low to high the indexor should not perform final jog 
			     the final home direction is exclusively determined by the sign of this backlash
			     and the datum direction: home, then strep (+ or -) 2 * backlash away from home, then
			     home for good.
			   */
			  sign = ' ';
			  temp_long = 2 * (signval) * CurrParam->backlash;
			  if (temp_long >= 0)
			    sign = '+';
			  sprintf (temp_str, "%c%c%ld", indexer, sign,
				   temp_long);
			  strcpy (com[num_str - 1], temp_str);
			  num_str++;
			  strcpy (temp_str, command);
			  strcat (temp_str, " !!ErrDatum");
			  strcpy (com[num_str - 1], temp_str);
			  num_str++;
			  sprintf (temp_str, "%c F %ld %ld", indexer,
				   CurrParam->final_datum_speed,
				   CurrParam->datum_direction);
			  strcpy (com[num_str - 1], temp_str);
			}
		      CurrParam->homing = 1;
		    }		/* low level move */
		}		/* near-home */
	      else
		{		/* real-home */
		  /* long signval = 1; *//* home switch wiring set to high-to-low transition */
		  long signval = -1;	/* home switch wiring set to low-to-high transition */
		  sprintf (temp_str, "%cF %ld %ld", indexer,
			   CurrParam->datum_speed,
			   CurrParam->datum_direction);
		  strcpy (com[num_str - 1], temp_str);
		  num_str++;
		  sprintf (temp_str, "%s %s", command, "!!ErrSend: steps");
		  strcpy (com[num_str - 1], temp_str);
		  num_str++;
		  /* set the backlash/step-away-from home value & direction */
		  sign = ' ';
		  temp_long = 2 * (signval) * CurrParam->backlash;
		  if (temp_long >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%ld", indexer, sign, temp_long);
		  strcpy (com[num_str - 1], temp_str);

		  /* sprintf(temp_str,"%c %ld", indexer, 2 * (signval) * CurrParam->backlash); */
		  strcpy (com[num_str - 1], temp_str);
		  num_str++;
		  strcpy (temp_str, command);
		  strcat (temp_str, " !!ErrDatum");
		  strcpy (com[num_str - 1], temp_str);
		  num_str++;
		  sprintf (temp_str, "%c F %ld %ld", indexer,
			   CurrParam->final_datum_speed,
			   CurrParam->datum_direction);
		  strcpy (com[num_str - 1], temp_str);

		  CurrParam->homing = 1;
		}		/* real-home */
	      /*      num_str++;
	         strcpy(temp_str,"Error in Datum") ;
	         strcpy(com[num_str-1],temp_str);
	       */
	    }			/* good parameter */
	  else
	    {			/* bad parameter */
	      status = -1;
	      return status;
	    }
	}
    }				/* !link_clear1 */
  /* printf("********** Number of Steps\n");  */
  /* num steps */

  temp_long = (long) (mot_inp.num_steps);
  if (temp_long != (long) link_clear0)
    {
      /* printf ("Number of steps is: %f %f %f\n",mot_inp.num_steps,num_steps_lo,num_steps_hi) ; */
      if (UFcheck_double (mot_inp.num_steps, num_steps_lo, num_steps_hi) ==
	  not_valid)
	{
	  printf ("********** Number of Steps is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{

	  if ((use_near_home) && (Hi_Level_Move[(int) indexer - 65]))
	    mot_inp.num_steps = mot_inp.num_steps - (long) near_home_margin;
	  if ((use_near_home) && (use_backlash)
	      && (Hi_Level_Move[(int) indexer - 65]))
	    mot_inp.num_steps =
	      mot_inp.num_steps - (long) CurrParam->backlash;
	  if (!(Hi_Level_Move[(int) indexer - 65]))
	    {
	      num_str++;
	      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
	      strcpy (com[num_str - 1], temp_str);
	      num_str++;
	      sign = ' ';
	      if (temp_long >= 0)
		sign = '+';
	      sprintf (temp_str, "%c%c%8.2f", indexer, sign,
		       mot_inp.num_steps);
	      strcpy (com[num_str - 1], temp_str);
	    }
	  else
	    {
	      num_str++;
	      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
	      strcpy (com[num_str - 1], temp_str);
	      num_str++;
	      sign = ' ';
	      if ((use_backlash))
		{
		  if (!use_near_home)
		    {
		      if (mot_inp.num_steps >= 0)
			sign = '+';
		      mot_inp.num_steps =
			mot_inp.num_steps - (long) CurrParam->backlash;
		      sprintf (temp_str, "%c%c%8.2f", indexer, sign,
			       mot_inp.num_steps);
		      strcpy (com[num_str - 1], temp_str);
		      num_str++;
		      sprintf (temp_str, "%s %s", command, "!!ErrSend steps");
		      strcpy (com[num_str - 1], temp_str);
		      num_str++;
		      sign = ' ';
		    }
		  if (CurrParam->backlash >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%ld", indexer, sign,
			   (long) CurrParam->backlash);
		  strcpy (com[num_str - 1], temp_str);
		}
	      else
		{
		  if (mot_inp.num_steps >= 0)
		    sign = '+';
		  sprintf (temp_str, "%c%c%8.2f", indexer, sign,
			   mot_inp.num_steps);
		  strcpy (com[num_str - 1], temp_str);
		}
	    }
	  /*      num_str++;
	     strcpy(temp_str,"Error in sending number of steps") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }
  /* printf("********** Stop Message\n") ; */
  /* stop message */

  if (strlen (mot_inp.stop_mess) > 0)
    {
      num_str++;
      strcpy (temp_str, command);
      strcat (temp_str, " !!ErrStop");
      strcpy (com[num_str - 1], temp_str);
      num_str++;
      sprintf (temp_str, "%c @", indexer);
      strcpy (com[num_str - 1], temp_str);
      /*   num_str++;
         strcpy(temp_str,"Error in stopping motor") ;
         strcpy(com[num_str-1],temp_str) ; */
      /*
       *num_str++;
       *strcpy(temp_str,"CAR") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++ ;
       *strcpy(temp_str,car_name) ;
       *strcat(temp_str,".IMSS") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,mot_inp.stop_mess) ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++ ;
       *strcpy(temp_str,"CAR") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,car_name) ;
       *strcat(temp_str,".IVAL") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *sprintf(temp_str,"%d",CAR_ERROR) ;
       *strcpy(com[num_str-1],temp_str) ; */
    }
  /* printf("********** Abort Message:   %s\n",mot_inp.abort_mess) ; */
  /* abort message */
  if (strlen (mot_inp.abort_mess) > 0)
    {
      num_str++;
      strcpy (temp_str, command);
      strcat (temp_str, " !!ErrAbort");
      strcpy (com[num_str - 1], temp_str);
      num_str++;
      sprintf (temp_str, "%c @", indexer);
      strcpy (com[num_str - 1], temp_str);
      /*num_str++;
         strcpy(temp_str,"Error in Aborting") ;
         strcpy(com[num_str-1],temp_str) ; */

      /*    num_str++;
       *strcpy(temp_str,"CAR") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,car_name) ;
       *strcat(temp_str,".IMSS") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,mot_inp.abort_mess) ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,"CAR") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *strcpy(temp_str,car_name) ;
       *strcat(temp_str,".IVAL") ;
       *strcpy(com[num_str-1],temp_str) ;
       *num_str++;
       *sprintf(temp_str,"%d",CAR_ERROR) ;
       *strcpy(com[num_str-1],temp_str) ; */
    }

  /* Origin Directive */
  temp_long = (long) (mot_inp.origin_directive);
  /* printf("********** origin Directive0\n") ; */
  if (temp_long != (long) link_clear1)
    {
      if (UFcheck_long (temp_long, 0, 1) == not_valid)
	{
	  printf ("********** origin Directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  /* printf("********** Origin Directive1\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error origining motor");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "%c o", indexer);
	  strcpy (com[num_str - 1], temp_str);
	  /*   num_str++;
	     strcpy(temp_str,"Error testing the system") ;
	     strcpy(com[num_str-1],temp_str) ;
	     num_str++;
	     strcpy(temp_str,"AI") ;
	     strcpy(com[num_str-1],temp_str) ;
	     num_str++;
	     strcpy(temp_str,ai_name) ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }


  if (num_str > 2)
    {
      /* num_str++ ; */
      sprintf (temp_str, "%cCAR", indexer);
      strcpy (com[0], temp_str);
      /* num_str++; */
      strcpy (temp_str, car_name);
      /*    strcat(temp_str,".IVAL") ; */
      strcpy (com[1], temp_str);
      /*    num_str++;
         sprintf(temp_str,"%d",CAR_IDLE_Comm) ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         strcpy(temp_str,car_name) ;
         strcat(temp_str,".IMSS") ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         strcpy(temp_str,car_name) ;
         strcat(temp_str,".IVAL") ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         sprintf(temp_str,"%d",CAR_ERROR_Comm) ;
         strcpy(com[num_str-1],temp_str) ; */

    }
  else
    num_str = 0;
  Hi_Level_Move[(int) indexer - 65] = 0;
  return num_str;
}

int
UFcheck_temp_inputs (ufTempInput temp_inp, char **com, char *rec_name,
		     ufTempCurrParam * CurrParam, long com_mode)
{

  long temp_long;
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  char car_name[30];
  char command[10];
  double P_value = -1.0;
  double I_value = -1.0;
  int heater_on = 0;

  strcpy (car_name, rec_name);
  /* car_name[strlen(car_name)-1] = 'C' ; */
  /* strcat(car_name,".I") ; */
  if (!com_mode)
    strcpy (command, "raw");
  else
    strcpy (command, "sim");

  /* printf("********** P, I, D\n") ; */
  /* P */
  temp_long = (long) (temp_inp.P);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.P, P_value_lo, P_value_hi) == -1)
	{
	  printf ("********** P value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** P value \n") ; */
	  num_str = num_str + 1;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrSend: PID");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "PID 1, %6.2f", temp_inp.P);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->P = temp_inp.P;
	  P_value = temp_inp.P;
	  /*     num_str++;
	     strcpy(temp_str,"Error sending initial velocity") ;
	     strcpy(com[num_str-1],temp_str) ; */
	}
    }
  /* printf("********** I Value\n"); */
  /* I value */
  temp_long = (long) (temp_inp.I);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.I, I_value_lo, I_value_hi) == -1)
	{
	  printf ("********** I value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** I value \n");  */
	  if (P_value < 0.0)
	    {
	      num_str++;
	      strcpy (temp_str, command);
	      strcat (temp_str, " !!ErrSend: PID");
	      strcpy (com[num_str - 1], temp_str);
	      num_str++;
	      sprintf (temp_str, "PID 1, , %6.2f", temp_inp.I);
	    }
	  else
	    {
	      sprintf (temp_str, "%s, %6.2f", com[(int) num_str - 1],
		       temp_inp.I);
	    }
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->I = temp_inp.I;
	  I_value = temp_inp.I;
	}
    }

  /* printf("********** D Value\n"); */
  /* D value */
  temp_long = (long) (temp_inp.D);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.D, D_value_lo, D_value_hi) == -1)
	{
	  printf ("********** D value is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** I value \n"); */
	  if (P_value < 0.0)
	    {
	      if (I_value < 0.0)
		{
		  num_str++;
		  strcpy (temp_str, command);
		  strcat (temp_str, " !!ErrSend: PID");
		  strcpy (com[num_str - 1], temp_str);
		  num_str++;
		  sprintf (temp_str, "PID 1, , , %6.2f", temp_inp.D);
		}
	      else
		{
		  sprintf (temp_str, "%s, %6.2f", com[(int) num_str - 1],
			   temp_inp.D);
		}
	    }
	  else
	    {
	      if (I_value < 0.0)
		{
		  sprintf (temp_str, "%s,, %6.2f", com[(int) num_str - 1],
			   temp_inp.D);
		}
	      else
		{
		  sprintf (temp_str, "%s, %6.2f", com[(int) num_str - 1],
			   temp_inp.D);
		}
	    }
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->D = temp_inp.D;
	}
    }

  /* printf("********** Auto Tune\n") ; */
  if (temp_inp.auto_tune != -1)
    {
      if (UFcheck_long_r (temp_inp.auto_tune, auto_tune_lo, auto_tune_hi) ==
	  -1)
	{
	  printf ("********** Auto Tune is not valid, %ld\n",
		  temp_inp.auto_tune);
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** Auto Tune\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error setting auto tune");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "CMODE 1,%6ld", temp_inp.auto_tune);
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->auto_tune = temp_inp.auto_tune;
	}
    }

  /* Set Point  */

  /* printf("********** Set point\n") ; */
  temp_long = (long) (temp_inp.set_point);
  if (temp_long != -1)
    {
      if (UFcheck_double (temp_inp.set_point, set_point_lo, set_point_hi) ==
	  -1)
	{
	  printf ("********** Set Point is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** Set Point\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !! Error setting the temp");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "SETP 1, %6.3f", temp_inp.set_point);
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->set_point = temp_inp.set_point;
	  heater_on = 1;
	}
    }

  /*  printf("********** Heater Range\n") ; */
  temp_long = (long) (temp_inp.heater_range);
  if (temp_long != -1)
    {
      if (UFcheck_long_r
	  (temp_inp.heater_range, heater_range_lo, heater_range_hi) == -1)
	{
	  printf ("********** Heater Range is not valid\n");
	  status = -1;
	  return status;
	}
      else
	{
	  /* printf("********** Heater Range\n") ; */
	  num_str++;
	  strcpy (temp_str, command);
	  strcat (temp_str, " !!ErrHeaterRange");
	  strcpy (com[num_str - 1], temp_str);
	  num_str++;
	  sprintf (temp_str, "RANGE %ld", temp_inp.heater_range);
	  strcpy (com[num_str - 1], temp_str);
	  CurrParam->heater_range = temp_inp.heater_range;
	}
    }
  /*
     if (heater_on) { 
     num_str++; 
     strcpy(temp_str,command) ;
     strcat(temp_str," !! Error turning the heater on") ; 
     strcpy(com[num_str-1],temp_str) ; 
     num_str++; 
     sprintf(temp_str,"CSET 1,A,1,1") ; 
     strcpy(com[num_str-1],temp_str) ;  
     } 
   */

  if (num_str > 2)
    {
      /* num_str++ ; */
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      /* num_str++; */
      strcpy (temp_str, car_name);
      /*    strcat(temp_str,".IVAL") ; */
      strcpy (com[1], temp_str);
      /*    num_str++;
         sprintf(temp_str,"%d",CAR_IDLE_Comm) ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         strcpy(temp_str,car_name) ;
         strcat(temp_str,".IMSS") ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         strcpy(temp_str,car_name) ;
         strcat(temp_str,".IVAL") ;
         strcpy(com[num_str-1],temp_str) ;
         num_str++;
         sprintf(temp_str,"%d",CAR_ERROR_Comm) ;
         strcpy(com[num_str-1],temp_str) ; */

    }
  else
    num_str = 0;
  return num_str;
}

long
UFAgentSend (char *name, int socFd, int Nstrings, char **strings)
{
  int i;
  int nsent = 0, nb, nbh, nbt = 0;
  static ufProtocolHeader hdr;

  printf ("********** UFMotAgentSend\n");
  printf ("** Record name is: %s\n", name);
  printf ("** The Socket number is %d\n", socFd);
  printf ("** The number of strings is %d\n", Nstrings);

  for (i = 0; i < Nstrings; i++)
    {
      printf ("%s\n", strings[i]);
    }
  strcpy (hdr.name, name);
  hdr.seqCnt = 1;
  hdr.seqTot = 1;
  hdr.type = 1;
  hdr.elem = Nstrings;
  nbh = ufHeaderLength (&hdr);
  hdr.length = nbh + Nstrings * sizeof (int);	/* should add length of strings */

  nb = ufHeaderSend (socFd, &hdr);
  printf ("number of bytes sent = %d\n", nb);
  printf ("number of header bytes = %d\n", nbh);
  /*sprintf(_UFerrmsg,"ufStringsSend> hdr: nbsent=%d, nbhdr=%d", nb, nbh); */
  /*_uflog(_UFerrmsg); */

  if (nb <= 0)
    {
      _uflog ("ufStringsSend> failed to send header!");
      printf ("ufStringsSend> failed to send header!");
      return nb;
    }
  if (nb < nbh)
    _uflog ("ufStringsSend> failed to send complete header?");

  do
    {
      nb = ufSendCstr (socFd, *strings);
      printf ("number of bytes sent (from string) = %d\n", nb);
      if (nb >= 4)
	nbt += (nb - 4);
      nsent++;
      strings++;
    }
  while ((nsent < Nstrings) && (nb > 0));

  printf ("number of bytes total = %d\n", nbt);
  return nbt;

}

int
UFcheck_dc_inputs (ufdcHrdwrInput dcHwrdwr_inp, char **com, char paramLevel,
		   char *rec_name, ufdcHrdwrCurrParam * CurrParam,
		   long com_mode)
{

  int status = 0;
  int num_str = 4;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];

  strcpy (car_name, rec_name);


  /*  if (!com_mode) sprintf(command,"%craw",indexer) ;
     else sprintf(command,"%csim",indexer) ; */

  if (strcmp (dcHwrdwr_inp.obs_mode, "") != 0)
    {
      if ((strcmp (dcHwrdwr_inp.obs_mode, "chop-nod") != 0) &&
	  (strcmp (dcHwrdwr_inp.obs_mode, "chop") != 0) &&
	  (strcmp (dcHwrdwr_inp.obs_mode, "nod") != 0) &&
	  (strcmp (dcHwrdwr_inp.obs_mode, "stare") != 0))
	{
	  printf ("********** Obs mode is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ObsMode");
	  strcat (temp_str, " !!ErrSend: ObsMode");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcHwrdwr_inp.obs_mode);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->obs_mode, dcHwrdwr_inp.obs_mode);
	}
    }

  if (strcmp (dcHwrdwr_inp.readout_mode, "") != 0)
    {
      /*if ( (strcmp(dcHwrdwr_inp.readout_mode,"S1") != 0) &&
         (strcmp(dcHwrdwr_inp.readout_mode,"S1R1") != 0) && 
         (strcmp(dcHwrdwr_inp.readout_mode,"S1R3") != 0) && 
         (strcmp(dcHwrdwr_inp.readout_mode,"s1") != 0) && 
         (strcmp(dcHwrdwr_inp.readout_mode,"s1r1") != 0) && 
         (strcmp(dcHwrdwr_inp.readout_mode,"s1r3") != 0) ) { 
         printf("********** readout mode is not valid\n") ; 
         status = not_valid ; 
         return status ; 
         } else */
      {
	num_str = num_str + 1;
	strcpy (temp_str, "ReadoutMode");
	strcat (temp_str, " !!ErrSend: ReadoutMode");
	strcpy (com[(int) num_str - 1], temp_str);
	num_str = num_str + 1;
	strcpy (temp_str, dcHwrdwr_inp.readout_mode);
	strcpy (com[(int) num_str - 1], temp_str);
	strcpy (CurrParam->readout_mode, dcHwrdwr_inp.readout_mode);
      }
    }



  if (dcHwrdwr_inp.FrmCoadds != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.FrmCoadds, 0, 1000) == not_valid)
	{
	  printf ("********** Frame Coadds is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "FrameCoadds");
	  strcat (temp_str, " !!ErrSend: FrameCoadds");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.FrmCoadds);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->FrmCoadds = dcHwrdwr_inp.FrmCoadds;
	}
    }

  if (dcHwrdwr_inp.ChpSettleReads != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.ChpSettleReads, 0, 1000) == not_valid)
	{
	  printf ("********** chop settle read is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopSettleReads");
	  strcat (temp_str, " !!ErrSend: ChopSettleReads");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.ChpSettleReads);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChpSettleReads = dcHwrdwr_inp.ChpSettleReads;
	}
    }

  if (dcHwrdwr_inp.ChpCoadds != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.ChpCoadds, 0, 1000) == not_valid)
	{
	  printf ("********** chop coadd is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopCoadds");
	  strcat (temp_str, " !!ErrSend: ChopCoadds");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.ChpCoadds);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChpCoadds = dcHwrdwr_inp.ChpCoadds;
	}
    }

  if (dcHwrdwr_inp.Savesets != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.Savesets, 0, 1000000) == not_valid)
	{
	  printf ("********** Save sets is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SaveSets");
	  strcat (temp_str, " !!ErrSend: SaveSets");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.Savesets);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->Savesets = dcHwrdwr_inp.Savesets;
	}
    }

  if (dcHwrdwr_inp.NodSettleReads != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.NodSettleReads, 0, 1000) == not_valid)
	{
	  printf ("********** Nod Settle Reads is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleReads");
	  strcat (temp_str, " !!ErrSend: NodSettleReads");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.NodSettleReads);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSettleReads = dcHwrdwr_inp.NodSettleReads;
	}
    }

  if (dcHwrdwr_inp.NodSets != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.NodSets, 0, 1000) == not_valid)
	{
	  printf ("********** Nod sets is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSets");
	  strcat (temp_str, " !!ErrSend: NodSets");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.NodSets);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSets = dcHwrdwr_inp.NodSets;
	}
    }

  if (dcHwrdwr_inp.NodSettleChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.NodSettleChops, 0, 1000) == not_valid)
	{
	  printf ("********** Nod Settle Chops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleChops");
	  strcat (temp_str, " !!ErrSend: NodSettleChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.NodSettleChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->NodSettleChops = dcHwrdwr_inp.NodSettleChops;
	}
    }

  if (dcHwrdwr_inp.PreValidChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.PreValidChops, 0, 1000) == not_valid)
	{
	  printf ("********** PreValidChops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PreValidChops");
	  strcat (temp_str, " !!ErrSend: PreValidChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.PreValidChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->PreValidChops = dcHwrdwr_inp.PreValidChops;
	}
    }

  if (dcHwrdwr_inp.PostValidChops != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.PostValidChops, 0, 1000) == not_valid)
	{
	  printf ("********** Post Valid Chops is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PostValidChops");
	  strcat (temp_str, " !!ErrSend: PostValidChops");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.PostValidChops);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->PostValidChops = dcHwrdwr_inp.PostValidChops;
	}
    }

  if (dcHwrdwr_inp.pixclock != (long) link_clear1)
    {
      if (UFcheck_long_r (dcHwrdwr_inp.pixclock, 0, 1000) == not_valid)
	{
	  printf ("********** pixclock is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "PixClock");
	  strcat (temp_str, " !!ErrSend: PixClock");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%ld", dcHwrdwr_inp.pixclock);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->pixclock = dcHwrdwr_inp.pixclock;
	}
    }

  /*       
     dcHwrdwr_inp.pixclock = -1  ; 
   */

  if (num_str > 4)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      strcpy (temp_str, "PARM");
      strcpy (com[2], temp_str);
      if (paramLevel == 'H')
	strcpy (temp_str, "HARDWARE");
      else
	{
	  if (paramLevel == 'P')
	    strcpy (temp_str, "PHYSICAL");
	  else
	    strcpy (temp_str, "META");
	}
      strcpy (com[3], temp_str);
    }
  else
    num_str = 0;
  return num_str;
}

int
UFcheck_dc_Phys_inputs (ufdcPhysicalInput dcPhysical_inp, char **com,
			char paramLevel, char *rec_name,
			ufdcPhysicalCurrParam * CurrParam, long com_mode)
{

  int status = 0;
  int num_str = 4;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];

  strcpy (car_name, rec_name);

  /*  if (!com_mode) sprintf(command,"%craw",indexer) ;
     else sprintf(command,"%csim",indexer) ; */

  if (strcmp (dcPhysical_inp.obs_mode, "") != 0)
    {
      if ((strcmp (dcPhysical_inp.obs_mode, "chop-nod") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "chop") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "nod") != 0) &&
	  (strcmp (dcPhysical_inp.obs_mode, "stare") != 0))
	{
	  printf ("********** Obs mode is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ObsMode");
	  strcat (temp_str, " !!ErrSend: ObsMode");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcPhysical_inp.obs_mode);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->obs_mode, dcPhysical_inp.obs_mode);
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ObsMode");
      strcat (temp_str, " !!ErrSend: ObsMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, CurrParam->obs_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      /* strcpy(CurrParam->obs_mode,dcPhysical_inp.obs_mode) ; */
    }

  if (strcmp (dcPhysical_inp.readout_mode, "") != 0)
    {
      /* if ( (strcmp(dcPhysical_inp.readout_mode,"S1") != 0) &&
         (strcmp(dcPhysical_inp.readout_mode,"S1R1") != 0) && 
         (strcmp(dcPhysical_inp.readout_mode,"S1R3") != 0) && 
         (strcmp(dcPhysical_inp.readout_mode,"s1") != 0) && 
         (strcmp(dcPhysical_inp.readout_mode,"s1r1") != 0) &&
         (strcmp(dcPhysical_inp.readout_mode,"s1r3") != 0) ) { 
         printf("********** readout mode is not valid\n") ; 
         status = not_valid ; 
         return status ; 
         } else */
      {
	num_str = num_str + 1;
	strcpy (temp_str, "ReadoutMode");
	strcat (temp_str, " !!ErrSend: ReadoutMode");
	strcpy (com[(int) num_str - 1], temp_str);
	num_str = num_str + 1;
	strcpy (temp_str, dcPhysical_inp.readout_mode);
	strcpy (com[(int) num_str - 1], temp_str);
	strcpy (CurrParam->readout_mode, dcPhysical_inp.readout_mode);
      }
    }
  else
    {

      num_str = num_str + 1;
      strcpy (temp_str, "ReadoutMode");
      strcat (temp_str, " !!ErrSend: ReadoutMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, CurrParam->readout_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      /* strcpy(CurrParam->readout_mode,dcPhysical_inp.readout_mode) ; */

    }

  if (dcPhysical_inp.FrameTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.FrameTime, 0, 1000) == not_valid)
	{
	  printf ("********** FrameTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "FrameTime");
	  strcat (temp_str, " !!ErrSend: FrameTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.FrameTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->FrameTime = dcPhysical_inp.FrameTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "FrameTime");
      strcat (temp_str, " !!ErrSend: FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->FrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      /* CurrParam->FrameTime = dcPhysical_inp.FrameTime ; */

    }


  if (dcPhysical_inp.saveFrq != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.saveFrq, 0, 1000) == not_valid)
	{
	  printf ("********** saveFreq is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SaveFrequency");
	  strcat (temp_str, " !!ErrSend: SaveFrequency");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.saveFrq);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->saveFrq = dcPhysical_inp.saveFrq;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SaveFrequency");
      strcat (temp_str, " !!ErrSend: SaveFrequency");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->saveFrq);
      strcpy (com[(int) num_str - 1], temp_str);
      /* CurrParam->saveFrq = dcPhysical_inp.saveFrq ; */
    }

  if (dcPhysical_inp.exposureTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.exposureTime, 0, 1000) == not_valid)
	{
	  printf ("********** exposureTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "OnSourceTime");
	  strcat (temp_str, " !!ErrSend: OnSourceTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.exposureTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->exposureTime = dcPhysical_inp.exposureTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "OnSourceTime");
      strcat (temp_str, " !!ErrSend: OnSourceTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->exposureTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.ChopFreq != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.ChopFreq, 0, 1000) == not_valid)
	{
	  printf ("********** ChopFrequency is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ChopFrequency");
	  strcat (temp_str, " !!ErrSend: ChopFrequency");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.ChopFreq);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->ChopFreq = dcPhysical_inp.ChopFreq;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ChopFrequency");
      strcat (temp_str, " !!ErrSend: ChopFrequency");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->ChopFreq);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.SCSDutyCycle != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.SCSDutyCycle, 0, 1000) == not_valid)
	{
	  printf ("********** SCSDutyCycle is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "SCSDutyCycle");
	  strcat (temp_str, " !!ErrSend: SCSDC");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.SCSDutyCycle);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->SCSDutyCycle = dcPhysical_inp.SCSDutyCycle;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SCSDutyCycle");
      strcat (temp_str, " !!ErrSend: SCSDC");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->SCSDutyCycle);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.nodDwelTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.nodDwelTime, 0, 1000) == not_valid)
	{
	  printf ("********** nodDwelTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodDwellTime");
	  strcat (temp_str, " !!ErrSend: NodDwellTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.nodDwelTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->nodDwelTime = dcPhysical_inp.nodDwelTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "NodDwellTime");
      strcat (temp_str, " !!ErrSend: NodDwellTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->nodDwelTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.nodStlTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.nodStlTime, 0, 1000) == not_valid)
	{
	  printf ("********** nodStlTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodSettleTime");
	  strcat (temp_str, " !!ErrSend: NodSettleTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.nodStlTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->nodStlTime = dcPhysical_inp.nodStlTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "NodSettleTime");
      strcat (temp_str, " !!ErrSend: NodSettleTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->nodStlTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.preValidChopTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.preValidChopTime, 0, 1000) ==
	  not_valid)
	{
	  printf ("********** preValidChopTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "preValidChopTime");
	  strcat (temp_str, " !!ErrSend: preValidChopTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.preValidChopTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->preValidChopTime = dcPhysical_inp.preValidChopTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "preValidChopTimeTime");
      strcat (temp_str, " !!ErrSend: preValidChopTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->preValidChopTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (dcPhysical_inp.postValidChopTime != -1.0)
    {
      if (UFcheck_double (dcPhysical_inp.postValidChopTime, 0, 1000) ==
	  not_valid)
	{
	  printf ("********** postValidChopTime is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "postValidChopTime");
	  strcat (temp_str, " !!ErrSend: postValidChopTime");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%f", dcPhysical_inp.postValidChopTime);
	  strcpy (com[(int) num_str - 1], temp_str);
	  CurrParam->postValidChopTime = dcPhysical_inp.postValidChopTime;
	}
    }
  else
    {
      num_str = num_str + 1;
      strcpy (temp_str, "postValidChopTimeTime");
      strcat (temp_str, " !!ErrSend: postValidChopTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%f", CurrParam->postValidChopTime);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  /*
     if (dcPhysical_inp.chopDutyCycle != -1.0) { 
     if (UFcheck_double(dcPhysical_inp.chopDutyCycle,0,100) == not_valid) { 
     printf("********** chopDutyCycle is not valid\n") ; 
     status = not_valid ; 
     return status ; 
     } else { 
     num_str = num_str + 1;
     strcpy(temp_str,"chopduty") ; 
     strcat(temp_str," !!ErrSend: chopDutyCycle") ; 
     strcpy(com[(int)num_str-1],temp_str) ; 
     num_str = num_str + 1; 
     sprintf(temp_str,"%f",dcPhysical_inp.chopDutyCycle); 
     strcpy(com[(int)num_str-1],temp_str) ; 
     CurrParam->chopDutyCycle = dcPhysical_inp.chopDutyCycle ; 
     } 
     } 
   */
  if (num_str > 4)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      strcpy (temp_str, "PARM");
      strcpy (com[2], temp_str);
      if (paramLevel == 'H')
	strcpy (temp_str, "HARDWARE");
      else
	{
	  if (paramLevel == 'P')
	    strcpy (temp_str, "PHYSICAL");
	  else
	    strcpy (temp_str, "META");
	}
      strcpy (com[3], temp_str);
    }
  else
    num_str = 0;
  return num_str;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/

int
UFcheck_dc_Acq_inputs (ufdcAcqContInput dcAcqCont_inp, char **com,
		       char paramLevel, char *rec_name,
		       ufdcAcqContCurrParam * CurrParam, long com_mode)
{

  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];

  strcpy (car_name, rec_name);

  /*  if (!com_mode) sprintf(command,"%craw",indexer) ;
     else sprintf(command,"%csim",indexer) ; */

  if (strcmp (dcAcqCont_inp.command, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.command, "CONFIGURE") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "START") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "STOP") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "ABORT") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "INIT") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "PARK") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "DATUM") != 0) &&
	  (strcmp (dcAcqCont_inp.command, "TEST") != 0))
	{
	  printf ("********** command is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ACQ");
	  strcat (temp_str, " !!ErrSend: ACQ");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.command);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->command, dcAcqCont_inp.command);
	}
    }

  if (strcmp (dcAcqCont_inp.obs_id, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ObsID");
      strcat (temp_str, " !!ErrSend: obs_id");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.obs_id);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->obs_id, dcAcqCont_inp.obs_id);
    }
  else
    {
      if (strcmp (CurrParam->obs_id, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ObsID");
	  strcat (temp_str, " !!ErrSend: obs_id");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->obs_id);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.dhs_write, "") != 0)
    {
      /* if ( (strcmp(dcAcqCont_inp.dhs_write,"SAVE") != 0) && 
         (strcmp(dcAcqCont_inp.dhs_write,"DISCARD") != 0) && 
         (strcmp(dcAcqCont_inp.dhs_write,"save") != 0) &&  
         (strcmp(dcAcqCont_inp.dhs_write,"discard") != 0) ) { 
         printf("********** dhs_write is not valid\n") ; 
         status = not_valid ; 
         return status ; 
         } else { */
      num_str = num_str + 1;
      strcpy (temp_str, "DHSWrite");
      strcat (temp_str, " !!ErrSend: dhs_write");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.dhs_write);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->dhs_write, dcAcqCont_inp.dhs_write);
    }
  else
    {
      if (strcmp (CurrParam->dhs_write, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "DHSWrite");
	  strcat (temp_str, " !!ErrSend: dhs_write");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->dhs_write);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  /*
     if (strcmp(dcAcqCont_inp.qckLk_id,"") != 0) { 
     num_str = num_str + 1; 
     strcpy(temp_str,"QuickLookID") ; 
     strcat(temp_str," !!ErrSend: qckLk_id") ; 
     strcpy(com[(int)num_str-1],temp_str) ; 
     num_str = num_str + 1; 
     strcpy(temp_str,dcAcqCont_inp.qckLk_id); 
     strcpy(com[(int)num_str-1],temp_str) ; 
     strcpy(CurrParam->qckLk_id,dcAcqCont_inp.qckLk_id) ; 
     }  
   */

  if (strcmp (dcAcqCont_inp.local_archive, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.local_archive, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.local_archive, "false") != 0))
	{
	  printf ("********** local_archive is not valid &%s&\n",
		  dcAcqCont_inp.local_archive);
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Archive");
	  strcat (temp_str, " !!ErrSend: Archive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.local_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->local_archive, dcAcqCont_inp.local_archive);
	}
    }
  else
    {
      if (strcmp (CurrParam->local_archive, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Archive");
	  strcat (temp_str, " !!ErrSend: Archive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->local_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.nod_handshake, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.nod_handshake, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.nod_handshake, "false") != 0))
	{

	  printf ("********** nod_handshake is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodHandShake");
	  strcat (temp_str, " !!ErrSend: nod_handshake");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.nod_handshake);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->nod_handshake, dcAcqCont_inp.nod_handshake);
	}
    }
  else
    {
      if (strcmp (CurrParam->nod_handshake, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "NodHandShake");
	  strcat (temp_str, " !!ErrSend: nod_handshake");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->nod_handshake);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }
  /*
     if (strcmp(dcAcqCont_inp.archive_host,"") != 0) { 
     num_str = num_str + 1; 
     strcpy(temp_str,"ArchiveHost") ; 
     strcat(temp_str," !!ErrSend: archive_host") ; 
     strcpy(com[(int)num_str-1],temp_str) ; 
     num_str = num_str + 1; 
     strcpy(temp_str,dcAcqCont_inp.archive_host); 
     strcpy(com[(int)num_str-1],temp_str) ; 
     strcpy(CurrParam->archive_host,dcAcqCont_inp.archive_host) ; 
     }  
   */
  if (strcmp (dcAcqCont_inp.archive_path, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ArchivePath");
      strcat (temp_str, " !!ErrSend: archive_path");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.archive_path);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->archive_path, dcAcqCont_inp.archive_path);
    }
  else
    {
      if (strcmp (CurrParam->archive_path, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ArchivePath");
	  strcat (temp_str, " !!ErrSend: archive_path");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->archive_path);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.archive_file_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ArchiveFileName");
      strcat (temp_str, " !!ErrSend: archive_file_name");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.archive_file_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->archive_file_name, dcAcqCont_inp.archive_file_name);
    }
  else
    {
      if (strcmp (CurrParam->archive_file_name, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ArchiveFileName");
	  strcat (temp_str, " !!ErrSend: archive_file_name");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->archive_file_name);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.comment, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Comment");
      strcat (temp_str, " !!ErrSend: comment");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.comment);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->comment, dcAcqCont_inp.comment);
    }
  else
    {
      if (strcmp (CurrParam->comment, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "Comment");
	  strcat (temp_str, " !!ErrSend: comment");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->comment);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.remote_archive, "") != 0)
    {
      if ((strcmp (dcAcqCont_inp.remote_archive, "TRUE") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "FALSE") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "true") != 0) &&
	  (strcmp (dcAcqCont_inp.remote_archive, "false") != 0))
	{
	  printf ("********** remote_archive is not valid &%s&\n",
		  dcAcqCont_inp.remote_archive);
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchive");
	  strcat (temp_str, " !!ErrSend: RemoteArchive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, dcAcqCont_inp.remote_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->remote_archive, dcAcqCont_inp.remote_archive);
	}
    }
  else
    {
      if (strcmp (CurrParam->remote_archive, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchive");
	  strcat (temp_str, " !!ErrSend: RemoteArchive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->remote_archive);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_host, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchiveHost");
      strcat (temp_str, " !!ErrSend: RemArchiveHost");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_host);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_host, dcAcqCont_inp.rem_archive_host);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_host, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchiveHost");
	  strcat (temp_str, " !!ErrSend: RemArchiveHost");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_host);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_path, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchivePath");
      strcat (temp_str, " !!ErrSend: rem_archive_path");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_path);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_path, dcAcqCont_inp.rem_archive_path);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_path, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchivePath");
	  strcat (temp_str, " !!ErrSend: rem_archive_path");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_path);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (strcmp (dcAcqCont_inp.rem_archive_file_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RemoteArchiveFileName");
      strcat (temp_str, " !!ErrSend: rem_archive_file_name");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcAcqCont_inp.rem_archive_file_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rem_archive_file_name,
	      dcAcqCont_inp.rem_archive_file_name);
    }
  else
    {
      if (strcmp (CurrParam->rem_archive_file_name, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "RemoteArchiveFileName");
	  strcat (temp_str, " !!ErrSend: rem_archive_file_name");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, CurrParam->rem_archive_file_name);
	  strcpy (com[(int) num_str - 1], temp_str);
	}
    }

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      /*
         strcpy(temp_str,"PARM") ; 
         strcpy(com[2],temp_str) ; 
         if (paramLevel == 'H') strcpy(temp_str,"HARDWARE") ; 
         else {  
         if (paramLevel == 'P') strcpy(temp_str,"PHYSICAL") ; 
         else if (paramLevel == 'M') strcpy(temp_str,"META") ; 
         else strcpy(temp_str,"ACQ") ; 
         } 
         strcpy(com[3],temp_str) ;
       */
    }
  else
    num_str = 0;
  return num_str;
}

int
UFcheck_dc_Obs_inputs (ufdcObsContInput dcObsCont_inp, char **com,
		       char paramLevel, char *rec_name,
		       ufdcObsContCurrParam * CurrParam, long com_mode)
{

  /* int status = 0; */
  int num_str = 4;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];
  /* double numnum; */
  /* char *endptr; */

  strcpy (car_name, rec_name);
  printf ("???????????????? my lambda low is @%s@\n",
	  dcObsCont_inp.lambda_low);

  if (strcmp (dcObsCont_inp.lambda_low, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "LambdaLow");
      strcat (temp_str, " !!ErrSend: LambdaLow");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.lambda_low);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->lambda_low, dcObsCont_inp.lambda_low);
    }

  if (strcmp (dcObsCont_inp.lambda_hi, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "LambdaHi");
      strcat (temp_str, " !!ErrSend: LambdaHi");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.lambda_hi);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->lambda_hi, dcObsCont_inp.lambda_hi);
    }

  if (strcmp (dcObsCont_inp.camera_mode, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "CameraMode");
      strcat (temp_str, " !!ErrSend: CameraMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.camera_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->camera_mode, dcObsCont_inp.camera_mode);
    }

  if (strcmp (dcObsCont_inp.obs_mode, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ObsMode");
      strcat (temp_str, " !!ErrSend: ObsMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.obs_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->obs_mode, dcObsCont_inp.obs_mode);
    }

  if (strcmp (dcObsCont_inp.on_source_time, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "OnSourceTime");
      strcat (temp_str, " !!ErrSend: OnSourceTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.on_source_time);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->on_source_time, dcObsCont_inp.on_source_time);
    }

  if (strcmp (dcObsCont_inp.readout_mode, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ReadoutMode");
      strcat (temp_str, " !!ErrSend: ReadoutMode");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.readout_mode);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->readout_mode, dcObsCont_inp.readout_mode);
    }

  if (strcmp (dcObsCont_inp.filter_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Filter");
      strcat (temp_str, " !!ErrSend: Filter");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.filter_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->filter_name, dcObsCont_inp.filter_name);
    }

  if (strcmp (dcObsCont_inp.grating_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Grating");
      strcat (temp_str, " !!ErrSend: Grating");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.grating_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->grating_name, dcObsCont_inp.grating_name);
    }

  if (strcmp (dcObsCont_inp.central_wavelength, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "CentralWavelength");
      strcat (temp_str, " !!ErrSend: CentralWavelength");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.central_wavelength);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->central_wavelength,
	      dcObsCont_inp.central_wavelength);
    }

  if (strcmp (dcObsCont_inp.preValid_chop_time, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "preValidChopTime");
      strcat (temp_str, " !!ErrSend: preValidChopTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.preValid_chop_time);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->preValid_chop_time,
	      dcObsCont_inp.preValid_chop_time);
    }

  if (strcmp (dcObsCont_inp.lyot_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "LyotStop");
      strcat (temp_str, " !!ErrSend: LyotStop");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.lyot_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->lyot_name, dcObsCont_inp.lyot_name);
    }

  if (strcmp (dcObsCont_inp.sector_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Sector");
      strcat (temp_str, " !!ErrSend: Sector");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.sector_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->sector_name, dcObsCont_inp.sector_name);
    }

  if (strcmp (dcObsCont_inp.slit_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SlitWidth");
      strcat (temp_str, " !!ErrSend: SlitWidth");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.slit_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->slit_name, dcObsCont_inp.slit_name);
    }

  if (strcmp (dcObsCont_inp.window_name, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "WindowName");
      strcat (temp_str, " !!ErrSend: WindowName");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.window_name);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->window_name, dcObsCont_inp.window_name);
    }

  if (strcmp (dcObsCont_inp.throughput, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Throughput");
      strcat (temp_str, " !!ErrSend: Throughput");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.throughput);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->throughput, dcObsCont_inp.throughput);
    }

  if (strcmp (dcObsCont_inp.chopThrow, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ChopThrow");
      strcat (temp_str, " !!ErrSend: ChopThrow");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.chopThrow);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->chopThrow, dcObsCont_inp.chopThrow);
    }

  if (strcmp (dcObsCont_inp.sky_background, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "WaterVapour");
      strcat (temp_str, " !!ErrSend: WaterVapour");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.sky_background);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->sky_background, dcObsCont_inp.sky_background);
    }

  if (strcmp (dcObsCont_inp.air_mass, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "AirMass");
      strcat (temp_str, " !!ErrSend: AirMass");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.air_mass);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->air_mass, dcObsCont_inp.air_mass);
    }
  /*
     if (strcmp(dcObsCont_inp.sky_noise,"") != 0) {
     num_str = num_str + 1;
     strcpy(temp_str,"SkyNoise") ;
     strcat(temp_str," !!ErrSend: SkyNoise") ;
     strcpy(com[(int)num_str-1],temp_str) ;
     num_str = num_str + 1;
     strcpy(temp_str,dcObsCont_inp.sky_noise);
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->sky_noise,dcObsCont_inp.sky_noise) ;
     }
   */
  if (strcmp (dcObsCont_inp.ambient_temp, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "AmbientTemperature");
      strcat (temp_str, " !!ErrSend: AmbientTemperature");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.ambient_temp);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->ambient_temp, dcObsCont_inp.ambient_temp);
    }

  if (strcmp (dcObsCont_inp.tel_emiss, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "TelescopeEmissivity");
      strcat (temp_str, " !!ErrSend: TelescopeEmissivity");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.tel_emiss);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->tel_emiss, dcObsCont_inp.tel_emiss);
    }

  if (strcmp (dcObsCont_inp.rotat_rate, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "RotatorRate");
      strcat (temp_str, " !!ErrSend: RotatorRate");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.rotat_rate);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->rotat_rate, dcObsCont_inp.rotat_rate);
    }

  if (strcmp (dcObsCont_inp.chop_freq, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "ChopFrequency");
      strcat (temp_str, " !!ErrSend: ChopFrequency");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.chop_freq);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->chop_freq, dcObsCont_inp.chop_freq);
    }

  if (strcmp (dcObsCont_inp.fid_frame_time, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "FiducialFrameTime");
      strcat (temp_str, " !!ErrSend: FiducialFrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.fid_frame_time);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->fid_frame_time, dcObsCont_inp.fid_frame_time);
    }


  if (strcmp (dcObsCont_inp.K_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "K_FrameTime");
      strcat (temp_str, " !!ErrSend: K_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.K_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->K_FilterFrameTime, dcObsCont_inp.K_FilterFrameTime);
    }

  if (strcmp (dcObsCont_inp.L_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "L_FrameTime");
      strcat (temp_str, " !!ErrSend: L_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.L_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->L_FilterFrameTime, dcObsCont_inp.L_FilterFrameTime);
    }

  if (strcmp (dcObsCont_inp.M_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "M_FrameTime");
      strcat (temp_str, " !!ErrSend: M_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.M_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->M_FilterFrameTime, dcObsCont_inp.M_FilterFrameTime);
    }

  if (strcmp (dcObsCont_inp.LoRes10_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "LoRes10_FrameTime");
      strcat (temp_str, " !!ErrSend: LoRes10_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.LoRes10_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->LoRes10_FilterFrameTime,
	      dcObsCont_inp.LoRes10_FilterFrameTime);
    }

  if (strcmp (dcObsCont_inp.HiRes10_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "HiRes10_FrameTime");
      strcat (temp_str, " !!ErrSend: HiRes10_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.HiRes10_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->HiRes10_FilterFrameTime,
	      dcObsCont_inp.HiRes10_FilterFrameTime);
    }

  if (strcmp (dcObsCont_inp.LoRes20_FilterFrameTime, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "LoRes20_FrameTime");
      strcat (temp_str, " !!ErrSend: LoRes20_FrameTime");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, dcObsCont_inp.LoRes20_FilterFrameTime);
      strcpy (com[(int) num_str - 1], temp_str);
      strcpy (CurrParam->LoRes20_FilterFrameTime,
	      dcObsCont_inp.LoRes20_FilterFrameTime);
    }
/*
  if (strcmp(dcObsCont_inp.XXX,"") != 0) {
    num_str = num_str + 1;
    strcpy(temp_str,"XXX") ;
    strcat(temp_str," !!ErrSend: XXX") ;
    strcpy(com[(int)num_str-1],temp_str) ;
    num_str = num_str + 1;
    strcpy(temp_str,dcObsCont_inp.XXX);
    strcpy(com[(int)num_str-1],temp_str) ;
    strcpy(CurrParam->XXX,dcObsCont_inp.XXX) ;
  }
*/
  /*
     if (strcmp(dcObsCont_inp.throughput,"") != 0) {
     numnum  = strtod(dcObsCont_inp.throughput,&endptr) ;
     if ( (*endptr != '\0') || (UFcheck_double(numnum,0.0,1.0) == -1) ) {
     printf("********** throughput is not valid\n") ;
     status = not_valid ;
     return status ;
     } else {
     num_str = num_str + 1;
     strcpy(temp_str,"Throughput") ;
     strcat(temp_str," !!ErrSend: Throughput") ;
     strcpy(com[(int)num_str-1],temp_str) ;
     num_str = num_str + 1;
     strcpy(temp_str,dcObsCont_inp.throughput);
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->throughput,dcObsCont_inp.throughput) ;
     }
     } 

     if (strcmp(dcObsCont_inp.chopThrow,"") != 0) {
     numnum  = strtod(dcObsCont_inp.chopThrow,&endptr) ;
     if ( (*endptr != '\0') || (UFcheck_double(numnum,0.0,1000.0) == -1) ) {
     printf("********** chopThrow is not valid\n") ;
     status = not_valid ;
     return status ;
     } else {
     num_str = num_str + 1;
     strcpy(temp_str,"ChopThrow") ;
     strcat(temp_str," !!ErrSend: ChopThrow") ;
     strcpy(com[(int)num_str-1],temp_str) ;
     num_str = num_str + 1;
     strcpy(temp_str,dcObsCont_inp.chopThrow);
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->throughput,dcObsCont_inp.chopThrow) ;
     }
     }
   */

  if (num_str > 4)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      strcpy (temp_str, "PARM");
      strcpy (com[2], temp_str);
      if (paramLevel == 'H')
	strcpy (temp_str, "HARDWARE");
      else
	{
	  if (paramLevel == 'P')
	    strcpy (temp_str, "PHYSICAL");
	  else if (paramLevel == 'M')
	    strcpy (temp_str, "META");
	  else
	    strcpy (temp_str, "ACQ");
	}
      strcpy (com[3], temp_str);

    }
  else
    num_str = 0;
  return num_str;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/

int
UFcheck_ecVacCh_inputs (ufecVacChInput ecVacCh_inp, char **com,
			char *rec_name, ufecVacChCurrParam * CurrParam,
			long com_mode, int agent)
{

  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];

  strcpy (car_name, rec_name);

  /*  if (!com_mode) sprintf(command,"%craw",indexer) ;
     else sprintf(command,"%csim",indexer) ; */

  if (strcmp (ecVacCh_inp.power, "") != 0)
    {
      if ((strcmp (ecVacCh_inp.power, "ON") != 0) &&
	  (strcmp (ecVacCh_inp.power, "OFF") != 0))
	{
	  printf ("********** command is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  if (agent == 2)
	    {
	      if (strcmp (ecVacCh_inp.power, "ON") == 0)
		{
		  strcpy (temp_str, "raw !!ErrSend: ON Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "IG1");
		}
	      else
		{
		  strcpy (temp_str, "raw !!ErrSend: OFF Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "IG0");
		}
	    }
	  else
	    {
	      if (strcmp (ecVacCh_inp.power, "ON") == 0)
		{
		  strcpy (temp_str, "raw !!ErrSend: ON Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "R 240");
		}
	      else
		{
		  strcpy (temp_str, "raw !!ErrSend: OFF Command");
		  strcpy (com[(int) num_str - 1], temp_str);
		  num_str = num_str + 1;
		  strcpy (temp_str, "S");
		}
	    }
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->power, ecVacCh_inp.power);
	}
    }

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
      /*
         strcpy(temp_str,"PARM") ; 
         strcpy(com[2],temp_str) ; 
         if (paramLevel == 'H') strcpy(temp_str,"HARDWARE") ; 
         else {  
         if (paramLevel == 'P') strcpy(temp_str,"PHYSICAL") ; 
         else if (paramLevel == 'M') strcpy(temp_str,"META") ; 
         else strcpy(temp_str,"ACQ") ; 
         } 
         strcpy(com[3],temp_str) ;
       */
    }
  else
    num_str = 0;
  return num_str;
}

int
UFcheck_bias_inputs (ufDCBiasInput bias_inp, char **com, char *rec_name,
		     ufDCBiasCurrParam * CurrParam, long com_mode)
{
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];
  long temp_num;

  strcpy (car_name, rec_name);

  if (strcmp (bias_inp.password, "") != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "Password");
      strcat (temp_str, " !!ErrSend: Password");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%s %s", "ARRAY_PW_BIAS", bias_inp.password);
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      strcpy (temp_str, "EnableChange");
      strcat (temp_str, " !!ErrSend: EnableChange");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      if (strstr (rec_name, "miri") != NULL)
	strcpy (temp_str, "miri:dc:biPassEnblIn");
      else
	strcpy (temp_str, "trecs:dc:biPassEnblIn");
      strcpy (com[(int) num_str - 1], temp_str);
    }				/* no password here */
  else
    {

      if (bias_inp.datum_directive != -1.0)
	{
	  if (UFcheck_double (bias_inp.datum_directive, 0, 1) == not_valid)
	    {
	      printf ("********** datum_directive is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "BiasDatum");
	      strcat (temp_str, " !!ErrSend: in Datum");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      sprintf (temp_str, "%s", "ARRAY_INIT_STATE_BIAS");
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* CurrParam->datum_directive = bias_inp.datum_directive ; */
	    }
	}

      if (strcmp (bias_inp.power, "") != 0)
	{
	  if ((strcmp (bias_inp.power, "ON") != 0) &&
	      (strcmp (bias_inp.power, "on") != 0) &&
	      (strcmp (bias_inp.power, "On") != 0) &&
	      (strcmp (bias_inp.power, "OFF") != 0) &&
	      (strcmp (bias_inp.power, "Off") != 0) &&
	      (strcmp (bias_inp.power, "off") != 0))
	    {
	      printf ("********** Power is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "BiasPower");
	      strcat (temp_str, " !!ErrSend: setting power");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      if ((strcmp (bias_inp.power, "ON") == 0) ||
		  (strcmp (bias_inp.power, "On") == 0) ||
		  (strcmp (bias_inp.power, "on") == 0))
		{
		  strcpy (temp_str, "ARRAY_POWER_UP_BIAS");
		}
	      else
		{
		  strcpy (temp_str, "ARRAY_POWER_DOWN_BIAS");
		}
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* strcpy(CurrParam->power,bias_inp.power) ; */
	    }
	}

      if (bias_inp.park_directive != -1.0)
	{
	  if (UFcheck_double (bias_inp.park_directive, 0, 1) == not_valid)
	    {
	      printf ("********** park is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "BiasPark");
	      strcat (temp_str, " !!ErrSend: in Park");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      sprintf (temp_str, "%s", "ARRAY_POWER_DOWN_BIAS");
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* CurrParam->park = bias_inp.park ; */
	    }
	}

      if (bias_inp.dac_id != (long) link_clear1)
	{
	  if (UFcheck_long_r (bias_inp.dac_id, 0, 23) == not_valid)
	    {
	      printf ("********** BiasDacId is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "BiasDacId");
	      strcat (temp_str, " !!ErrSend: BiasDacId");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      temp_num =
		bias_inp.dac_volt * CurrParam->M[bias_inp.dac_id] +
		CurrParam->B[bias_inp.dac_id];
	      sprintf (temp_str, "%s %ld %ld", "dac_set_single_bias",
		       bias_inp.dac_id, temp_num);
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* CurrParam->dac_id = bias_inp.dac_id ; */
	    }
	}

      if (bias_inp.latch_dac_id != (long) link_clear1)
	{
	  if (UFcheck_long_r (bias_inp.latch_dac_id, 0, 23) == not_valid)
	    {
	      printf ("********** LatchBiasDacId is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "LatchBiasDacId");
	      strcat (temp_str, " !!ErrSend: LatchBiasDacId");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      sprintf (temp_str, "%s %ld", "dac_lat_single_bias",
		       bias_inp.latch_dac_id);
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* CurrParam->latch_bias_id = bias_inp.latch_bias_id ; */
	    }
	}

      if (bias_inp.read_all_directive != (long) link_clear1)
	{
	  if (UFcheck_long_r (bias_inp.read_all_directive, 0, 1) == not_valid)
	    {
	      printf ("********** read_all_directive is not valid\n");
	      status = not_valid;
	      return status;
	    }
	  else
	    {
	      num_str = num_str + 1;
	      strcpy (temp_str, "ReadAllBias");
	      strcat (temp_str, " !!ErrSend: ReadAllBias");
	      strcpy (com[(int) num_str - 1], temp_str);
	      num_str = num_str + 1;
	      sprintf (temp_str, "%s", "ARRAY_DAC_READBACK_BIAS");	/* ?????????????????? */
	      strcpy (com[(int) num_str - 1], temp_str);
	      /* CurrParam->read_all_directive = dcHwrdwr_inp.read_all_directive ; */
	    }
	}

      if (strcmp (bias_inp.vGate, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "BiasVGate");
	  strcat (temp_str, " !!ErrSend: vGate");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ARRAY_SET_VGATE_BIAS ");
	  strcat (temp_str, bias_inp.vGate);
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* strcpy(CurrParam->vGate,bias_inp.vGate) ; */
	}

      if (strcmp (bias_inp.vWell, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "BiasvWell");
	  strcat (temp_str, " !!ErrSend: vWell");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ARRAY_WELL ");
	  strcat (temp_str, bias_inp.vWell);
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* strcpy(CurrParam->vWell,bias_inp.vWell) ; */
	}

      if (strcmp (bias_inp.vBias, "") != 0)
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "BiasvBias");
	  strcat (temp_str, " !!ErrSend: vBias");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ARRAY_SET_DET_BIAS ");
	  strcat (temp_str, bias_inp.vBias);
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* strcpy(CurrParam->vBias,bias_inp.vBias) ; */
	}
    }

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
    }
  else
    num_str = 0;

  return num_str;
}

long
UFcheck_preAmp_inputs (ufDCPreAmpInput preAmp_inp, char **com, char *rec_name,
		       ufDCPreAmpCurrParam * CurrParam, long command_mode)
{

  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];
  double temp_num;

  strcpy (car_name, rec_name);

  if (preAmp_inp.datum_directive != -1.0)
    {
      if (UFcheck_double (preAmp_inp.datum_directive, 0, 1) == not_valid)
	{
	  printf ("********** datum_directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "preAmpDatum");
	  strcat (temp_str, " !!ErrSend: in Datum");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%s", "ARRAY_OFFSET_DEFAULTS_PREAMP");
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* CurrParam->datum_directive = preAmp_inp.datum_directive ; */
	}
    }

  if (preAmp_inp.global_set != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "SetGlobal");
      strcat (temp_str, " !!ErrSend: SetGlobal");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%s %ld", "ARRAY_OFFSET_GLOBAL_PREAMP",
	       preAmp_inp.global_set);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (preAmp_inp.adjust_value != 0)
    {
      num_str = num_str + 1;
      strcpy (temp_str, "AdjustPreAmp");
      strcat (temp_str, " !!ErrSend: AdjustPreAmp");
      strcpy (com[(int) num_str - 1], temp_str);
      num_str = num_str + 1;
      sprintf (temp_str, "%s %ld", "ARRAY_OFFSET_ADJUST_PREAMP",
	       preAmp_inp.adjust_value);
      strcpy (com[(int) num_str - 1], temp_str);
    }

  if (preAmp_inp.preAmp_id != (long) link_clear1)
    {
      if (UFcheck_long_r (preAmp_inp.preAmp_id, 0, 50) == not_valid)
	{
	  printf ("********** preAmpId is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "preAmpId");
	  strcat (temp_str, " !!ErrSend: preAmpId");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  temp_num = preAmp_inp.preAmp_volt;
	  /* preAmp_inp.dac_volt*CurrParam->M[preAmp_inp.dac_id] + CurrParam->B[preAmp_inp.dac_id]; */
	  sprintf (temp_str, "%s %ld %f", "ARRAY_OFFSET_SINGLE_PREAMP",
		   preAmp_inp.preAmp_id, temp_num);
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* CurrParam->dac_id = preAmp_inp.dac_id ; */
	}
    }

  if (preAmp_inp.read_all_directive != (long) link_clear1)
    {
      if (UFcheck_long_r (preAmp_inp.read_all_directive, 0, 1) == not_valid)
	{
	  printf ("********** read_all_directive is not valid\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, "ReadAllpreAmp");
	  strcat (temp_str, " !!ErrSend: ReadAllpreAmp");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  sprintf (temp_str, "%s", "ARRAY_OFFSET_READBACK_PREAMP");	/* ?????????????????? */
	  strcpy (com[(int) num_str - 1], temp_str);
	  /* CurrParam->read_all_directive = dcHwrdwr_inp.read_all_directive ; */
	}
    }

  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
    }
  else
    num_str = 0;

  return num_str;
}

/*
  char all_devices[40] ;
  char annex[40] ;
  char ls_218[40] ;
  char ls_340[40] ;
  char vacuum[40] ;
  char indexors[40] ;
  char cryocooler[40] ;
  char ppcVME[40] ;
  char mce4[40] ;
  char all_agents[40] ;
  char executive[40] ;
  char ufacqframed[40] ;
  char ufgdhsd[40] ;
  char ufgls218[40] ;
  char ufgls340[40] ;
  char ufg354vacd[40] ;
  char ufgmotord[40] ;
  char ufgmce4d[40] ;
*/

void
str_toUpper (char *target, char *original)
{
  int i;
  for (i = 0; i < strlen (original); i++)
    target[i] = toupper ((int) original[i]);
  target[strlen (original)] = '\0';
  return;
}

long
UFcheck_bt_inputs (ufBTInput boot_inp, char **com, char *rec_name,
		   ufBTCurrParam * CurrParam, long command_mode)
{
  int status = 0;
  int num_str = 2;
  char temp_str[80];
  /* char ai_name[30] ; */
  char car_name[30];
  char in_upper[40];

  strcpy (car_name, rec_name);

  if (strcmp (boot_inp.all_devices, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.all_devices);
      /* printf("XOXOXOXOXOXO I received @%s@ and the upper case of that is @%s@\n",boot_inp.all_devices,in_upper) ; */
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "all_devices");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ALL");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->all_devices, in_upper);
	}
    }

  if (strcmp (boot_inp.annex, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.annex);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "annex");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "annex");
	  /* strcpy(temp_str,"1"); */
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->annex, in_upper);
	}
    }

  if (strcmp (boot_inp.ls_218, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ls_218);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "lakeshore218");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "lakeshore218");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ls_218, in_upper);
	}
    }

  if (strcmp (boot_inp.ls_340, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ls_340);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "lakeshore340");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "lakeshore340");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ls_340, in_upper);
	}
    }

  if (strcmp (boot_inp.vacuum, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.vacuum);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "vacuum");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "vacuum");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->vacuum, in_upper);
	}
    }

  if (strcmp (boot_inp.indexors, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.indexors);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "indexors");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "indexors");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->indexors, in_upper);
	}
    }

  if (strcmp (boot_inp.cryocooler, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.cryocooler);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "cryocooler");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "cryocooler");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->cryocooler, in_upper);
	}
    }

  if (strcmp (boot_inp.ppcVME, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ppcVME);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ppcVME");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ppcVME");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ppcVME, in_upper);
	}
    }

  if (strcmp (boot_inp.mce4, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.mce4);
      if ((strcmp (in_upper, "POWEROFF") != 0) &&
	  (strcmp (in_upper, "POWERON") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "mce4");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "mce4");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->mce4, in_upper);
	}
    }

  if (strcmp (boot_inp.all_agents, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.all_agents);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "All Agents");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "All");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->all_agents, in_upper);
	}
    }

  if (strcmp (boot_inp.executive, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.executive);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "executive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "executive");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->executive, in_upper);
	}
    }

  if (strcmp (boot_inp.ufacqframed, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufacqframed);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufacqframed");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufacqframed");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufacqframed, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgdhsd, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgdhsd);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgdhsd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgdhsd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgdhsd, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgls218, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgls218);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgls218d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgls218d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgls218, in_upper);
	}
    }


  if (strcmp (boot_inp.ufgls340, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgls340);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgls340d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgls340d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgls340, in_upper);
	}
    }

  if (strcmp (boot_inp.ufg354vacd, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufg354vacd);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufg354vacd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufg354vacd");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufg354vacd, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgmotord, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgmotord);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgmotord");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgmotord");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgmotord, in_upper);
	}
    }

  if (strcmp (boot_inp.ufgmce4d, "") != 0)
    {
      str_toUpper (in_upper, boot_inp.ufgmce4d);
      if ((strcmp (in_upper, "SIMAGENTS") != 0) &&
	  (strcmp (in_upper, "STOPAGENTS") != 0) &&
	  (strcmp (in_upper, "STARTAGENTS") != 0))
	{
	  printf ("********** invalid command\n");
	  status = not_valid;
	  return status;
	}
      else
	{
	  num_str = num_str + 1;
	  strcpy (temp_str, in_upper);
	  strcat (temp_str, " !!ErrSend: ");
	  strcat (temp_str, "ufgmce4d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  num_str = num_str + 1;
	  strcpy (temp_str, "ufgmce4d");
	  strcpy (com[(int) num_str - 1], temp_str);
	  strcpy (CurrParam->ufgmce4d, in_upper);
	}
    }




  /*
     if (strcmp(boot_inp.,"") != 0) {
     str_toUpper (in_upper,boot_inp.) ;
     if ( (strcmp(in_upper,"POWEROFF") != 0) &&
     (strcmp(in_upper,"POWERON") != 0) ) {
     printf("********** invalid command\n") ;
     status = not_valid ;
     return status ;
     } else {
     num_str = num_str + 1;
     strcpy(temp_str,in_upper) ;
     strcat(temp_str," !! Error sending") ;
     strcat(temp_str,"");
     strcpy(com[(int)num_str-1],temp_str) ;
     num_str = num_str + 1;
     strcpy(temp_str,"");
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->,in_upper) ;
     }
     }

     if (strcmp(boot_inp.YYY,"") != 0) {
     str_toUpper (in_upper,boot_inp.YYY) ;
     if ( (strcmp(in_upper,"SIMAGENTS") != 0) &&
     (strcmp(in_upper,"STOPAGENTS") != 0) &&
     (strcmp(in_upper,"STARTAGENTS") != 0) ) {
     printf("********** invalid command\n") ;
     status = not_valid ;
     return status ;
     } else {
     num_str = num_str + 1;
     strcpy(temp_str,in_upper) ;
     strcat(temp_str," !! Error sending") ;
     strcat(temp_str,"YYY");
     strcpy(com[(int)num_str-1],temp_str) ;
     num_str = num_str + 1;
     strcpy(temp_str,"YYY");
     strcpy(com[(int)num_str-1],temp_str) ;
     strcpy(CurrParam->YYY,in_upper) ;
     }
     }



   */
  if (num_str > 2)
    {
      strcpy (temp_str, "CAR");
      strcpy (com[0], temp_str);
      strcpy (temp_str, car_name);
      strcpy (com[1], temp_str);
    }
  else
    num_str = 0;
  return num_str;

  return 0;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
UFget_obs_mode_long (char *obs_mode)
{
  long obs_mode_long = 1;

  return obs_mode_long;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
void
UFget_obs_mode (long obs_mode_long, char *obs_mode)
{
  strcpy (obs_mode, "Chop");

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
UFget_readout_mode_long (char *readout_mode)
{
  long readout_mode_long = 1;

  return readout_mode_long;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
void
UFreadout_mode (long readout_mode_long, char *readout_mode)
{
  strcpy (readout_mode, "save");

}



/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFReadPositions (FILE * posFile, int num_mot, int last_mot_num)
{
  long status = 0;
  long i, j;
  char in_str[80];
  double in_dbl, throughput, lambda_lo, lambda_hi;
  int num_pos;
  char *temp_str;
  int done = 0;
  int mot_num, pos_num;
  /* printf("# ok I am in .....\n") ; */

  /* read the remaining motor parameter lines */
  for (i = last_mot_num; i < num_mot; i++)
    fgets (in_str, 80, posFile);

  /* Read the comment lines describing the table of parameters */
  for (i = 0; i < 15; i++)
    fgets (in_str, 80, posFile);

  /* printf("^^^^^^^^^^^^^^^^ number of motors %d\n",num_mot) ; */
  /*namPosTableInit = 1 ; */
  for (i = 0; i < num_mot; i++)
    {
      /*  read the comment line with the full motor name */
      fgets (in_str, 80, posFile);

      /* printf("^^^^^^^^^^^ The first line I read is @%s@\n",in_str) ; */
      /* read the number of positions */
      fscanf (posFile, "%d %d", &mot_num, &num_pos);
      /* printf("^^^^^^^^^^^^^^ number positions %d\n",num_pos) ; */

      namedPositionsTable[i].num_pos = num_pos;
      /* read to the end of the line */
      /* printf("kkkkkkkkk 3 \n") ; */
      fgets (in_str, 80, posFile);

      /* insert the abbreviated name in the table */
      /* printf("kkkkkkkkk 4 \n") ; */
      temp_str = strchr (in_str, '\n');
      if (temp_str != NULL)
	temp_str[0] = '\0';

      /* trim the leading spaces */
      temp_str = strchr (in_str, ' ');
      /* printf("kkkkkkkkk 5 \n") ; */

      while ((temp_str != NULL) && (!done))
	{
	  if (temp_str == in_str)
	    {
	      strcpy (in_str, temp_str + 1);
	    }
	  else
	    {
	      done = 1;
	      if (temp_str != NULL)
		temp_str[0] = '\0';
	    }
	  temp_str = strchr (in_str, ' ');
	}
      /* printf("kkkkkkkkk 6 \n") ; */

      strcpy (namedPositionsTable[i].motorName, in_str);
      /* printf("kkkkkkkkk 7 \n") ; */
      /* printf("in_str is $$%s$$\n",namedPositionsTable[i].motorName) ; */

      /* read the comment line */
      fgets (in_str, 80, posFile);	/* comment line */
      /* printf("kkkkkkkkk 8 \n") ; */

      for (j = 0; j < num_pos; j++)
	{
	  /* read the offset and the name of the positions */
	  fscanf (posFile, "%d %lf %lf %lf %lf", &pos_num, &in_dbl,
		  &throughput, &lambda_lo, &lambda_hi);

	  /* printf("kkkkkkkkk 9 \n") ; */
	  fgets (in_str, 80, posFile);	/* read the position name */
	  temp_str = strchr (in_str, '\n');
	  /* printf("kkkkkkkkk 10 \n") ; */

	  if (temp_str != NULL)
	    temp_str[0] = '\0';
	  /* trim the white spaces */
	  temp_str = strchr (in_str, ' ');
	  /* printf("kkkkkkkkk 11 \n") ; */

	  while ((temp_str != NULL) && (!done))
	    {
	      if (temp_str == in_str)
		{
		  strcpy (in_str, temp_str + 1);
		}
	      else
		{
		  done = 1;
		  if (temp_str != NULL)
		    temp_str[0] = '\0';
		}
	      temp_str = strchr (in_str, ' ');
	    }

	  /* printf("kkkkkkkkk 12 \n") ; */

	  /* insert them in the table */
	  strcpy (namedPositionsTable[i].the_list[j].name, in_str);
	  namedPositionsTable[i].the_list[j].offset = in_dbl;
	  /* printf("kkkkkkkkk 13 \n") ; */
	  namedPositionsTable[i].the_list[j].throughput = throughput;
	  namedPositionsTable[i].the_list[j].lambdaLo = lambda_lo;
	  namedPositionsTable[i].the_list[j].lambdaHi = lambda_hi;

	  /* printf("%ld  %f  %f %f\n",j,namedPositionsTable[i].the_list[j].offset,namedPositionsTable[i].the_list[j].throughput, namedPositionsTable[i].the_list[j].resolution) ; */
	}
    }
  namPosTableInit = 1;
  return status;

}



/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFReadPositionsFile_old (char *dbase)
{
  long status = 0;
  int i, j;
  FILE *posFile;
  char in_str[80];
  double in_dbl;
  int num_pos;
  char *temp_str;
  int done = 0;
  char filename[50];

  if (namPosTableInit != 0)
    {
      printf ("~~~~~~~~~~~~~~~The Table has been initialized already.\n");
      return status;

    }
  else
    {
      /* if (strcmp(dbase,"trecs") == 0) strcpy(filename,"./bin/svgm5/positions.txt") ;
         else  strcpy(filename,"./bin/mv167/positions.txt") ; */
      strcpy (filename, "./data/positions.txt");

      /* if ((posFile = fopen("./bin/svgm5/positions.txt","r")) == NULL) { */
      /* if ((posFile = fopen("./bin/mv167/positions.txt","r")) == NULL) { */
      if ((posFile = fopen (filename, "r")) == NULL)
	{
	  printf
	    ("~~~~~~~~~~~~~~Named Positions file could not be opened.\n");
	  status = -1;
	}
      else
	{
	  NumFiles++;
	  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
	  namPosTableInit = 1;
	  for (i = 0; i < 10; i++)
	    {
	      /*  read the comment line with the full motor name */
	      fgets (in_str, 80, posFile);

	      /*        temp_str = strchr(in_str,'\n') ;
	         if (temp_str != NULL) temp_str[0] = '\0' ; 
	         temp_str = malloc(80) ; 
	         strcpy(temp_str,in_str) ; 
	         printf("%s\n",temp_str) ; 
	         free(temp_str) ; 
	       */

	      /* read the line with the abbreviated motor name */
	      fgets (in_str, 80, posFile);
	      /* insert the abbreviated name in the table */
	      temp_str = strchr (in_str, '\n');
	      if (temp_str != NULL)
		temp_str[0] = '\0';
	      strcpy (namedPositionsTable[i].motorName, in_str);
	      /* read the number of positions */
	      fscanf (posFile, "%d", &num_pos);
	      namedPositionsTable[i].num_pos = num_pos;
	      /* read to the end of the line */
	      fgets (in_str, 80, posFile);
	      /* read the comment line */
	      fgets (in_str, 80, posFile);

	      for (j = 0; j < num_pos; j++)
		{
		  /* read the offset and the name of the positions */
		  fscanf (posFile, "%lf", &in_dbl);
		  fgets (in_str, 80, posFile);
		  temp_str = strchr (in_str, '\n');
		  if (temp_str != NULL)
		    temp_str[0] = '\0';
		  /* trim the white spaces */
		  temp_str = strchr (in_str, ' ');
		  while ((temp_str != NULL) && (!done))
		    {
		      if (temp_str == in_str)
			{
			  strcpy (in_str, temp_str + 1);
			}
		      else
			{
			  done = 1;
			  if (temp_str != NULL)
			    temp_str[0] = '\0';
			}
		      temp_str = strchr (in_str, ' ');
		    }
		  /* insert them in the table */
		  strcpy (namedPositionsTable[i].the_list[j].name, in_str);
		  namedPositionsTable[i].the_list[j].offset = in_dbl;

		}
	    }
	  fclose (posFile);
	  NumFiles--;
	  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
	}
      return status;
    }
}



/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFPrintPositionsTable ()
{
  long status = 0;
  int i, j;

  if (namPosTableInit == 0)
    {
      printf ("The Table has not been initialized already.\n");
      return -1;
    }
  else
    {
      for (i = 0; i < 10; i++)
	{
	  /* print the abbreviated motor name */
	  printf ("%s\n", namedPositionsTable[i].motorName);
	  /* print the number of positions */
	  printf ("%d\n", namedPositionsTable[i].num_pos);
	  for (j = 0; j < namedPositionsTable[i].num_pos; j++)
	    {
	      /* print the offset and the name of the positions  */
	      printf ("%f %f %s\n", namedPositionsTable[i].the_list[j].offset,
		      namedPositionsTable[i].the_list[j].throughput,
		      namedPositionsTable[i].the_list[j].name);

	    }
	}
      return status;
    }
}

long
trecsAlive ()
{

  printf ("Iam awake. I am awake. now quit bugging me.!!!!\n");
  return OK;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFGetSteps (char *motorName, int posNum, double *offset)
{
  long status = 0;
  int i, j;
  i = 0;
  j = 0;
  if (posNum > 0)
    {
      while ((strstr (namedPositionsTable[i].motorName, motorName) == NULL)
	     && (i < 10))
	i++;
      /*printf("Motor %d\n",i) ; */
      if (namedPositionsTable[i].num_pos < posNum)
	status = -1;
      else
	*offset = namedPositionsTable[i].the_list[posNum - 1].offset;
      /* printf("The posNum is %d, the offset is %f \n",posNum,namedPositionsTable[i].the_list[posNum-1].offset) ; */
    }
  else
    status = -1;
  return status;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFGetSteps_old (char *motorName, char *posName, double *offset)
{
  long status = 0;
  int i, j;
  i = 0;
  j = 0;
  while ((strcmp (namedPositionsTable[i].motorName, motorName) != 0)
	 && (i < 10))
    i++;
  if (i < 10)
    {
      while ((strcmp (namedPositionsTable[i].the_list[j].name, posName) != 0)
	     && (j < namedPositionsTable[i].num_pos))
	{
	  j++;
	  printf ("%s %s", namedPositionsTable[i].the_list[j].name, posName);
	}
      if (j < namedPositionsTable[i].num_pos)
	*offset = namedPositionsTable[i].the_list[j].offset;
      else
	status = -1;
    }
  else
    status = -1;
  if (status > -1)
    printf
      ("~~~~~~~~~~~~~~ The number of steps is %f for motr %s and position %s\n",
       *offset, motorName, posName);
  else
    printf ("~~~~~~~~~~~~~~The number of steps is a bust\n");
  return status;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/

long
UFReadFile ()
{
  long status = 0;
  int line_count;
  FILE *posFile;
  char in_str[80];
  char *temp_str;


  line_count = 0;
  if ((posFile = fopen ("./data/positions.txt", "r")) == NULL)
    {
      /* if ((posFile = fopen("./bin/mv167/positions.txt","r")) == NULL) { */
      printf ("Named Positions file could not be opened.\n");
      status = -1;
    }
  else
    {
      NumFiles++;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
      /* read the first line */
      fgets (in_str, 80, posFile);
      line_count++;
      while (!feof (posFile))
	{
	  temp_str = strchr (in_str, '\n');
	  if (temp_str != NULL)
	    temp_str[0] = '\0';
	  temp_str = malloc (80);
	  strcpy (temp_str, in_str);
	  printf ("Line %3d: %s\n", line_count, temp_str);
	  fgets (in_str, 80, posFile);
	  line_count++;
	  free (temp_str);
	}
      fclose (posFile);
      NumFiles--;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    }
  return status;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
int
checkSoc (int socfd, float timeOut)
{
  struct timeval *infinit = 0;
  struct timeval *usetimeout = 0;
  struct timeval timeout;
  /* struct timeval tenthsec = { 0L, 100000L }; 0.1 sec. */
  struct timeval poll = { 0, 0 };	/* do not block */
  int fdcnt = 0, cnt = 32;
			 /*_MaxInterrupts_; // Latif find what that value is */
  struct timeval to, *to_p = 0;
  float _timeOut = 0.0;		/* Latif find what that value is */
  fd_set writefds, readfds, excptfds;
  struct sockaddr _theSockAddr;
  int _theSizeofSockAddrData = sizeof (_theSockAddr);
  int gotpeer = 1, rcnt = 0;
  char *recvBuf = 0;
  struct stat st;

  if (socfd < 0)
    {
      printf ("checkSoc> bad socfd: %d\n", socfd);
      return -1;
    }
  if (fstat (socfd, &st) < 0)
    {
      printf ("checkSoc> failed fstat on socfd: %d\n", socfd);
      return -1;
    }
  /* else if( ! S_ISSOCK(st.st_mode) ) { */
  else if (!(st.st_mode & S_IFSOCK))
    {
      printf ("checkSoc> not a socket, socfd: %d\n", socfd);
      return -1;
    }

  if (ERROR != ioctl (socfd, FIONREAD, (int) &rcnt))
    {
      if (rcnt > 0)
	{
	  recvBuf = malloc (rcnt);
	  /* if( recv( socfd, recvBuf, rcnt, MSG_DONTWAIT | MSG_PEEK ) <= 0 ) { */
	  if (recv (socfd, recvBuf, rcnt, MSG_PEEK) <= 0)
	    {
	      printf
		("checkSoc> peek-recv() returned error on socfd %d: %s\n",
		 socfd, strerror (errno));
	      free (recvBuf);
	      return -3;
	    }
	  free (recvBuf);
	}
    }
  else
    {
      printf ("checkSoc> ioctl returned error on socfd %d: %s\n",
	      socfd, strerror (errno));
      return -2;
    }

  if (getpeername (socfd, &_theSockAddr, &_theSizeofSockAddrData) != OK)
    {
      printf ("checkSoc> unable to getpeername for socket fd= %d\n", socfd);
      gotpeer = 0;
    }
  else if (_verbose)
    {
      printf ("checkSoc> getpeername = %s\n", _theSockAddr.sa_data);
    }

  FD_ZERO (&writefds);
  FD_SET (socfd, &writefds);
  FD_ZERO (&readfds);
  FD_SET (socfd, &readfds);
  FD_ZERO (&excptfds);
  FD_SET (socfd, &excptfds);

  if (timeOut > 0.00000001)
    {
      _timeOut = timeOut;
      timeout.tv_sec = (long) floor (_timeOut);
      timeout.tv_usec =
	(unsigned long) floor (1000000 * (_timeOut - timeout.tv_sec));
      usetimeout = &timeout;
    }
  else if (timeOut > -0.00000001)
    {				/* ~zero timout */
      usetimeout = &poll;
      /* usetimeout = &tenthsec; try a short timeout (0.1 sec) */
    }
  else				/* negative timeout indicates infinite (wait forever, blocking) */
    usetimeout = infinit;

  do
    {
      /* since select may modify this area of memory, always provide fresh input: */
      if (usetimeout)
	{
	  to = *usetimeout;
	  to_p = &to;
	}
      errno = 0;
      fdcnt = select (1 + socfd, &readfds, &writefds, &excptfds, to_p);
      /* fdcnt = select(FD_SETSIZE, &readfds, &writefds, &excptfds, to_p); */
    }
  while (errno == EINTR && --cnt >= 0);

  if (_verbose)
    printf ("checkSoc> select fdcnt = %d\n", fdcnt);

  if (errno == EBADF)
    {
      printf ("checkSoc> invalid (EBADF) socket fd= %d\n", socfd);
      return -2;
    }
  if (fdcnt < 0)
    {
      printf ("checkSoc> error occured on select\n");
      return fdcnt;
    }
  if (fdcnt == 0)
    {
      printf ("checkSoc> timed-out, not writable\n");
      return fdcnt;
    }
  /* 
   * not supported by VxWorks:

   if( FD_ISSET(fd, &excptfds) ) {
   printf("UFSocket::writable> exception present on Fd: %d\n",fd);
   return -1;
   }
   */

  /* if writable, should return > 0 */
  if (gotpeer && FD_ISSET (socfd, &writefds))
    return socfd;
  else
    return -1;
}

int
reassign_bingo ()
{
  if (bingo == 1)
    bingo = 0;
  else
    bingo = 1;
  return OK;
}

int
near_home (int how_close)
{
  if (use_near_home == 1)
    {
      use_near_home = 0;
      printf ("NOT using near home algorithm\n");
    }
  else
    {
      use_near_home = 1;
      near_home_margin = how_close;
      printf ("Using near home algorithm at %d from 0 position\n",
	      near_home_margin);
    }
  return OK;
}


int
backlash (int how_much)
{
  if (use_backlash == 1)
    {
      use_backlash = 0;
      printf ("NOT using backlash\n");
    }
  else
    {
      use_backlash = 1;
      backlash_margin = how_much;
      printf ("Using backlash algorithm\n");
    }
  return OK;
}

int
testing ()
{
  int i;
  for (i = 0; i < 50; i++)
    printf ("***************** The socket %d is : %d \n", i,
	    checkSoc (i, 0.5));
  return 0;
}




/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
int
bingo_test ()
{
  int socfd;			/* Integer to hold the socket number */
  char **message;		/*Holds the string(s) to be sent */
  char **response;		/* Holds the string(s) received */
  int num_bytes_sent;
  int tot_bytes;
  int num_strings_received;
  int status;			/* status of closing the connection */
  static ufProtocolHeader myheader;	/*Information about the receive */
  /* Initialize the message string(s) */
  char mymessage1[] = "Hello World";
  char mymessage2[] = "Goodbye World";
  message[0] = (char *) mymessage1;
  message[1] = (char *) mymessage2;
  printf ("The message is %s\n", message[0]);
  printf ("The message is %s\n", message[1]);
  /* open the connection */
  socfd = ufConnect ("128.227.184.153", 55555);
  printf ("The socket number is: %d\n", socfd);
  if (socfd >= 0)
    {
      /* send the message: Socket number, message string, number of strings being sent */
      num_bytes_sent = ufStringsSend (socfd, message, 2, "hello");
      printf ("The number of bytes sent is %d\n", num_bytes_sent);
      if (num_bytes_sent > 0)
	{
	  /* receive the response from the server */
	  response =
	    ufStringsRecv (socfd, &num_strings_received, &myheader,
			   &tot_bytes);
	  /* print the information from the header */
	  if (response != NULL)
	    {
	      printf ("The number of strings received is %d\n",
		      num_strings_received);
	      printf ("The number of total bytes is %d\n", tot_bytes);
	      printf ("The length of the header is %d\n", myheader.length);
	      printf ("The type of the header is %d\n", myheader.type);
	      printf ("The elem of the header is %d\n", myheader.elem);
	      printf ("The seqCnt of the header is %d\n", myheader.seqCnt);
	      printf ("The seqTot of the header is %d\n", myheader.seqTot);
	      printf ("The duration of the header is %f\n",
		      myheader.duration);
	      printf ("The timestamp of the header is %s\n",
		      myheader.timestamp);
	      printf ("The name of the header is %s\n", myheader.name);
	      /* Print the string(s) received */
	      printf ("The response is %s\n", response[0]);
	      printf ("The response is %s\n", response[1]);

	      /* Close the connection */
	      status = ufClose (socfd);
	      printf ("The status is %d\n", status);
	      if (status != 0)
		{
		  printf ("%s\n", "Error closing the connection");
		}
	    }
	  else
	    {
	      printf ("%s\n", "Unable to recieve response");
	    }
	}
      else
	{
	  printf ("%s\n", "Unable to send message");
	}
    }
  else
    {
      printf ("%s\n", "Unable to create socket");
    }
  return 0;
}

#endif
