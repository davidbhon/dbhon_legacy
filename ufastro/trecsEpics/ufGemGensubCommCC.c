#if !defined(__UFGEMGENSUBCOMMCC_C__)
#define __UFGEMGENSUBCOMMCC_C__ "RCS: $Name:  $ $Id: ufGemGensubCommCC.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufGEMGENSUBCOMMCCC[] = __UFGEMGENSUBCOMMCC_C__ ;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>
#include <sysLib.h>

/* use  select on socfd -- hon */
#include <sockLib.h>

#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
#include <genSub.h>
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <dbStaticLib.h> 
#include <dbBase.h> 

#include "trecs.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"

/* use in getpeername; it crashes system if fed socfd < 0 -- hon
static int _verbose = 1;
static struct sockaddr _theSockAddr;
static int _theSizeofSockAddrData = 0;
*/
/*
static int number_connections = 0 ; 
static int num_req = 0 ; 
*/
/* static char namedPosStrings[200][40] ; */

static char cc_DEBUG_MODE[6] ;
static char cc_STARTUP_DEBUG_MODE[6] ;
static int cc_initialized  = 0 ;

static char cc_host_ip[20] ;
static long cc_port_no ;
static long  number_motors ;
static int counter = 0;
static long updates_count ; 


/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/********************** CC  CODING ************************/



/**********************************************************/
/**********************************************************/
/**********************************************************/
void init_cc_config () {
  FILE *ccFile ;
  char in_str[256] ;
  char *temp_str ;
  char filename[40] ;
 
  strcpy(filename,ccconfig_filename);
  if ((ccFile = fopen(filename,"r")) == NULL) {
    trx_debug("cc Configuration file could not be opened","ccConfig function","NONE","NONE") ;
    strcpy(cc_STARTUP_DEBUG_MODE,"NONE") ;
    strcpy(cc_DEBUG_MODE,"NONE") ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,256,ccFile);
    fgets(in_str,256,ccFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(cc_STARTUP_DEBUG_MODE,in_str) ;
    fclose(ccFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    if ( (strcmp(cc_STARTUP_DEBUG_MODE,"NONE") != 0) ||
         (strcmp(cc_STARTUP_DEBUG_MODE,"MIN") != 0) ||
	 (strcmp(cc_STARTUP_DEBUG_MODE,"FULL") != 0) ) strcpy(cc_STARTUP_DEBUG_MODE,"NONE") ;
    
    strcpy(cc_DEBUG_MODE,"NONE") ;
  }
  logMsg("CC Initialized ... \n",0,0,0,0,0,0) ;
  cc_initialized  = 1 ;
  return ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long ufNamedPosGensub (genSubRecord *gsp) {
  long status = 0;
  /*
  unsigned long line_count ;
  FILE *posFile ;
  char in_str[80] ;
  char *temp_str ;
  
  char rec_name[29] ;
  char filename[50] ;
  */
  /* strcpy(rec_name, gsp->name ) ; */

  /* if (strstr(rec_name, "trecs") != NULL)  strcpy(filename,"./bin/svgm5/positions.txt") ; */
  /*     else  strcpy(filename,"./bin/mv167/positions.txt");  */
   
  
    /* gsp->valb = namedPosStrings ; */
    /*
    double_array[0] = 0.0 ; 
    double_array[1] = 1.0 ; 
    double_array[2] = 2.0 ; 
    double_array[3] = 3.0 ; 
    double_array[4] = 4.0 ; 
    *(long *)gsp->valc = 5; 
    gsp->novd = 5 ; 
    gsp->vald = double_array ;

    long_array[0] = 0 ; 
    long_array[1] = 1 ; 
    long_array[2] = 2 ; 
    long_array[3] = 3 ; 
    long_array[4] = 4 ; 

    *(long *)gsp->vale = 5; 
    gsp->novf = 5;
    gsp->valf = long_array ;
   
    } */
  
  return status ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long ufmrgCarGensubInit (genSubRecord *gsp) {
  long status = 0 ;
  char blank_str[] = "" ;
  int i ;
  int *last_command ;

  strcpy(gsp->a, blank_str) ; /* Sector Wheel */
  strcpy(gsp->b, blank_str) ; /* Window Changer */
  strcpy(gsp->c, blank_str) ; /* Aperture Wheel */
  strcpy(gsp->d, blank_str) ; /* Filter Wheel 1 */
  strcpy(gsp->e, blank_str) ; /* Lyot Wheel */
  strcpy(gsp->f, blank_str) ; /* Filter Wheel 2 */
  strcpy(gsp->g, blank_str) ; /* Pupil Image  */
  strcpy(gsp->h, blank_str) ; /* Slit Wheel  */ 
  strcpy(gsp->i, blank_str) ; /* Grating Wheel */
  strcpy(gsp->k, blank_str) ; /* Cold Clamp */
  strcpy(gsp->l, blank_str) ; /* Temperature Set */
  /*
  if (bingo)  
    printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  mrgCarINIT\n") ; */
  last_command = (int *) malloc (sizeof(int)*10);
  for (i = 0; i<=9;i++) last_command[i] = CAR_IDLE ;
  strcpy(gsp->vald,"IDLE") ;
  strcpy(gsp->valc,"GOOD") ;

  gsp->dpvt = (void *) last_command ;
  return status ;
} 


/**********************************************************/
/**********************************************************/
/**********************************************************/
int status_changed (char *current, int last) {
  int changed = 0 ;
  switch (last) {
    case CAR_IDLE:
      if (strcmp(current,"IDLE") != 0) changed = 1;
      break;
    case CAR_BUSY:
      if (strcmp(current,"BUSY") != 0) changed = 1;
      break;

    case CAR_ERROR:
      if (strcmp(current,"ERR") != 0) changed = 1;
      break;
  }
  return changed ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long ufmrgCarGensubProc (genSubRecord *gsp) {
  long status = 0 ;
  char blank_str[] = "" ;
  static char temp_str[40] ;
  static char out_message[40];
  long action = CAR_IDLE;
  int *last_command ;

  strcpy(out_message, blank_str) ;
  /* if (bingo) 
    printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ mrgCarProc\n") ;
  */
  /* Check the Sector Wheel Car */
  strcpy(temp_str,gsp->a);
  /* if (bingo) printf("Sector Wheel: %s.\n",temp_str) ; */
  /*
  if (strcmp(temp_str,"ERR") == 0) {
    action = CAR_ERROR ;
    strcat(out_message,"01") ;
  } else {
    if (strcmp(temp_str,"BUSY") == 0) {
      action = CAR_BUSY ;
      strcat(out_message,"01") ; 
    } 
  } 
  */
  /* Check the Window Changer Car */
  strcpy(temp_str,gsp->b);
  /* if (bingo) printf("Window Changer: %s.\n",temp_str) ; */

  /* Check the Aperture Wheel Car */
  strcpy(temp_str,gsp->c);
  /* if (bingo) printf("Aperture Wheel: %s.\n",temp_str) ; */

  /* Check the Filter Wheel 1 Car */
  strcpy(temp_str,gsp->d);
  /* if (bingo) printf("Filter Wheel 1: %s.\n",temp_str) ; */

  /* Check the Lyot Wheel Car */
  strcpy(temp_str,gsp->e);
  /* if (bingo) printf("Lyot Wheel: %s.\n",temp_str) ; */

  /* Check the Filter Wheel 2 Car */
  strcpy(temp_str,gsp->f);
  /* if (bingo) printf("Filter Wheel 2: %s.\n",temp_str) ; */

  /* Check the Pupil Image Car */
  strcpy(temp_str,gsp->g);
  /* if (bingo) printf("Pupil Image: %s.\n",temp_str) ; */

  /* Check the Slit Wheel Car */
  strcpy(temp_str,gsp->h);
  /* if (bingo) printf("Slit Wheel: %s.\n",temp_str) ; */

  /* Check the Grating Wheel Car */
  strcpy(temp_str,gsp->i);
  /* if (bingo) printf("Grating Wheel: %s.\n",temp_str) ; */

  /* Check the Cold Clamp Car */
  strcpy(temp_str,gsp->k);
  /* if (bingo) printf("Cold Clamp: %s.\n",temp_str) ; */

  if ( (strcmp(gsp->a,"ERR") == 0) || (strcmp(gsp->b,"ERR") == 0) || (strcmp(gsp->c,"ERR") == 0) || (strcmp(gsp->d,"ERR") == 0) || (strcmp(gsp->e,"ERR") == 0) || (strcmp(gsp->f,"ERR") == 0) || (strcmp(gsp->g,"ERR") == 0) || (strcmp(gsp->h,"ERR") == 0) || (strcmp(gsp->i,"ERR") == 0) || (strcmp(gsp->k,"ERR") == 0) ) action = CAR_ERROR ;

  if (action == CAR_IDLE) {
    if ( (strcmp(gsp->a,"BUSY") == 0) || (strcmp(gsp->b,"BUSY") == 0) || (strcmp(gsp->c,"BUSY") == 0) || (strcmp(gsp->d,"BUSY") == 0) || (strcmp(gsp->e,"BUSY") == 0) || (strcmp(gsp->f,"BUSY") == 0) || (strcmp(gsp->g,"BUSY") == 0) || (strcmp(gsp->h,"BUSY") == 0) || (strcmp(gsp->i,"BUSY") == 0) || (strcmp(gsp->k,"BUSY") == 0) ) action = CAR_BUSY ;
  }
  /* find out which CAR(s) have the ERR */
  /*
  if (action == CAR_ERROR) { 
    if (strcmp(gsp->a,"ERR") == 0) { 
      strcat(out_message,gsp->l) ; 
    } else { 
      if (strcmp(gsp->b,"ERR") == 0) { 
        strcat(out_message,gsp->m) ; 
      } else { 
        if (strcmp(gsp->c,"ERR") == 0) { 
          strcat(out_message,gsp->n) ; 
        } else { 
          if (strcmp(gsp->d,"ERR") == 0) { 
            strcat(out_message,gsp->o) ; 
	  } else { 
            if (strcmp(gsp->e,"ERR") == 0) { 
              strcat(out_message,gsp->p) ; 
            } else { 
              if (strcmp(gsp->f,"ERR") == 0) { 
                strcat(out_message,gsp->q) ; 
              } else { 
                if (strcmp(gsp->g,"ERR") == 0) { 
                  strcat(out_message,gsp->r) ; 
                } else { 
                  if (strcmp(gsp->h,"ERR") == 0) { 
                    strcat(out_message,gsp->s) ; 
                  } else { 
                    if (strcmp(gsp->i,"ERR") == 0) { 
                      strcat(out_message,gsp->t) ; 
		    } else { 
                      if (strcmp(gsp->k,"ERR") == 0) { 
                        strcat(out_message,gsp->u) ; 
                      }  
                    } 
                  } 
                } 
              } 
            } 
          } 
        }  
      } 
    } 
  
    if (bingo) printf("Out Message is : %s\n",out_message) ; 
  } 
  */
  /*Let's figure out the state of the system and its health */
  if (action == CAR_IDLE) {
    strcpy(gsp->vald,"IDLE") ;
    strcpy(gsp->valc,"GOOD") ;
  } else {
    if (action == CAR_BUSY) {
      strcpy(gsp->vald,"BUSY") ;
      strcpy(gsp->valc,"GOOD") ;
    } else {
      strcpy(gsp->vald,"ERROR") ;
      strcpy(gsp->valc,"BAD") ;
    }
  }
  /* Let's see if we can figure out the status of the last command */
  /* first let's find out which one changed */

  last_command = (int *) gsp->dpvt ;
  

  if (status_changed (gsp->a, last_command[0])) {
    if (strcmp(gsp->a,"IDLE") == 0) {
      action = CAR_IDLE ;
      last_command[0] = CAR_IDLE ;
    } else {
      if (strcmp(gsp->a,"BUSY") == 0) {
        action = CAR_BUSY ;
        last_command[0] = CAR_BUSY ;
      } else {
        action = CAR_ERROR ;
        last_command[0] = CAR_ERROR ;
        strcpy(out_message,gsp->l) ;
        /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
        if (*(long *)gsp->valb == CAR_IDLE) {

        }
      }
    }
  } else { /* Well link 'A' did not change, let keep going */
    if (status_changed (gsp->b, last_command[1])) {
      if (strcmp(gsp->b,"IDLE") == 0) {
        action = CAR_IDLE ;
        last_command[1] = CAR_IDLE ;
      } else {
        if (strcmp(gsp->b,"BUSY") == 0) {
          action = CAR_BUSY ;
          last_command[1] = CAR_BUSY ;
        } else {
          action = CAR_ERROR ;
          last_command[1] = CAR_ERROR ;
          strcpy(out_message,gsp->m) ;
          /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
          if (*(long *)gsp->valb == CAR_IDLE) {
          
          }
        }
      }
    } else {/* Well link 'B' did not change, let keep going */
      if (status_changed (gsp->c, last_command[2])) {
        if (strcmp(gsp->c,"IDLE") == 0) {
          action = CAR_IDLE ;
          last_command[2] = CAR_IDLE ;
        } else {
          if (strcmp(gsp->c,"BUSY") == 0) {
            action = CAR_BUSY ;
            last_command[2] = CAR_BUSY ;
          } else {
            action = CAR_ERROR ;
            last_command[2] = CAR_ERROR ;
            strcpy(out_message,gsp->n) ;
            /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
            if (*(long *)gsp->valb == CAR_IDLE) {
            
            }
          }
        }
      } else {/* Well link 'C' did not change, let keep going */
        if (status_changed (gsp->d, last_command[3])) {
          if (strcmp(gsp->d,"IDLE") == 0) {
            action = CAR_IDLE ;
            last_command[3] = CAR_IDLE ;
          } else {
            if (strcmp(gsp->d,"BUSY") == 0) {
              action = CAR_BUSY ;
              last_command[3] = CAR_BUSY ;
            } else {
              action = CAR_ERROR ;
              last_command[3] = CAR_ERROR ;
              strcpy(out_message,gsp->o) ;
              /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
              if (*(long *)gsp->valb == CAR_IDLE) {
          
              }
            }
          }
        } else {/* Well link 'D' did not change, let keep going */
          if (status_changed (gsp->e, last_command[4])) {
            if (strcmp(gsp->e,"IDLE") == 0) {
              action = CAR_IDLE ;
              last_command[4] = CAR_IDLE ;
            } else {
              if (strcmp(gsp->e,"BUSY") == 0) {
                action = CAR_BUSY ;
                last_command[4] = CAR_BUSY ;
              } else {
                action = CAR_ERROR ;
                last_command[4] = CAR_ERROR ;
                strcpy(out_message,gsp->p) ;
                /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                if (*(long *)gsp->valb == CAR_IDLE) {
          
                }
              }
            }
          } else {/* Well link 'E' did not change, let keep going */
            if (status_changed (gsp->f, last_command[5])) {
              if (strcmp(gsp->f,"IDLE") == 0) {
                action = CAR_IDLE ;
                last_command[5] = CAR_IDLE ;
              } else {
                if (strcmp(gsp->f,"BUSY") == 0) {
                  action = CAR_BUSY ;
                  last_command[5] = CAR_BUSY ;
                } else {
                  action = CAR_ERROR ;
                  last_command[5] = CAR_ERROR ;
                  strcpy(out_message,gsp->q) ;
                  /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                  if (*(long *)gsp->valb == CAR_IDLE) {
          
                  }
                }
              }
            } else {/* Well link 'F' did not change, let keep going */
              if (status_changed (gsp->g, last_command[6])) {
                if (strcmp(gsp->g,"IDLE") == 0) {
                  action = CAR_IDLE ;
                  last_command[6] = CAR_IDLE ;
                } else {
                  if (strcmp(gsp->g,"BUSY") == 0) {
                    action = CAR_BUSY ;
                    last_command[6] = CAR_BUSY ;
                  } else {
                    action = CAR_ERROR ;
                    last_command[7] = CAR_ERROR ;
                    strcpy(out_message,gsp->r) ;
                    /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                    if (*(long *)gsp->valb == CAR_IDLE) {
          
                    }
                  }
                }
              }  else {/* Well link 'G' did not change, let keep going */
                if (status_changed (gsp->h, last_command[7])) {
                  if (strcmp(gsp->h,"IDLE") == 0) {
                    action = CAR_IDLE ;
                    last_command[7] = CAR_IDLE ;
                  } else {
                    if (strcmp(gsp->h,"BUSY") == 0) {
                      action = CAR_BUSY ;
                      last_command[7] = CAR_BUSY ;
                    } else {
                      action = CAR_ERROR ;
                      last_command[7] = CAR_ERROR ;
                      strcpy(out_message,gsp->s) ;
                      /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                      if (*(long *)gsp->valb == CAR_IDLE) {
            
                      }
                    }
                  }
                } else {/* Well link 'H' did not change, let keep going */
                  if (status_changed (gsp->i, last_command[8])) {
                    if (strcmp(gsp->i,"IDLE") == 0) {
                      action = CAR_IDLE ;
                      last_command[8] = CAR_IDLE ;
                    } else {
                      if (strcmp(gsp->i,"BUSY") == 0) {
                        action = CAR_BUSY ;
                        last_command[8] = CAR_BUSY ;
                      } else {
                        action = CAR_ERROR ;
                        last_command[8] = CAR_ERROR ;
                        strcpy(out_message,gsp->t) ;
                        /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                        if (*(long *)gsp->valb == CAR_IDLE) {
            
                        }
                      }
                    }
                  } else {/* Well link 'I' did not change, let keep going */
                    if (status_changed (gsp->k, last_command[9])) {
                      if (strcmp(gsp->k,"IDLE") == 0) {
                        action = CAR_IDLE ;
                        last_command[9] = CAR_IDLE ;
                      } else {
                        if (strcmp(gsp->k,"BUSY") == 0) {
                          action = CAR_BUSY ;
                          last_command[9] = CAR_BUSY ;
                        } else {
                          action = CAR_ERROR ;
                          last_command[9] = CAR_ERROR ;
                          strcpy(out_message,gsp->u) ;
                          /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
                          if (*(long *)gsp->valb == CAR_IDLE) {
            
                          }
                        }
                      }
                    }
                  }
                }
              }
	    }
          }
        }
      }
    }
  }
  

  strcpy(gsp->vala,out_message); 
  *(long *)gsp->valb = action; 
 
  return status ;
} 



/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for motorG *******************/
long ufmotorGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubMotPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
 
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubMotPrivate *) malloc (sizeof(genSubMotPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  pPriv->port_no = cc_port_no ;
  pPriv->agent = 3; /* motor agent */

  pPriv->CurrParam.init_velocity = -1.0 ;
  pPriv->CurrParam.slew_velocity = -1.0 ;
  pPriv->CurrParam.acceleration = -1.0 ;
  pPriv->CurrParam.deceleration = -1.0 ;
  pPriv->CurrParam.drive_current= -1.0 ;
  pPriv->CurrParam.datum_speed = -1 ;
  pPriv->CurrParam.datum_direction = -1 ;
  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */
  strcpy(pPriv->mot_inp.home_switch,"enabled") ;
  pPriv->CurrParam.steps_from_home = 0.0 ;
  pPriv->CurrParam.steps_to_add = 0.0;
  pPriv->CurrParam.homing = 0;
  pPriv->CurrParam.backlash = 0.0 ;
  pPriv->CurrParam.setting_time = -1.0;
  pPriv->CurrParam.jog_speed_slow = -1;
  pPriv->CurrParam.jog_speed_hi = -1;
  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* perform test  */
  *(long *)pgs->c = -1 ; /* Simulation Mode  */
  *(long *)pgs->e = -1 ; /* datum directive */
  *(double *)pgs->g = 0.0 ; /* num steps */
  strcpy(pgs->i,"") ; /* stop message */
  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  strcpy(pgs->k,"") ; /* abort message */

  strcpy(pgs->m,"NONE") ; /*debug mode start in NONE */
  *(long *)pgs->u = -1 ; /* origin directive */

  /* *(double *)pgs->n = -1.0 ;*/  /* init velocity */
  /* *(double *)pgs->o = -1.0 ;*/  /* slew velocity */
  /* *(double *)pgs->p = -1.0 ;*/  /* acceleration */
  /* *(double *)pgs->q = -1.0 ;*/  /* deceleration */
  /* *(double *)pgs->r = -1.0 ;*/  /* drive current */
  /* *(double *)pgs->s = -1.0 ;*/  /* datum speed */
  /* *(long *)pgs->t = -1 ;*/  /* datum direction */


  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_CC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->mot_inp.command_mode    = -1 ;
  pPriv->mot_inp.test_directive  = -1 ;
  pPriv->mot_inp.datum_motor_directive = -1;
  pPriv->mot_inp.init_velocity   = -1.0 ;
  pPriv->mot_inp.slew_velocity   = -1.0 ;
  pPriv->mot_inp.acceleration    = -1.0 ;
  pPriv->mot_inp.deceleration    = -1.0 ;
  pPriv->mot_inp.drive_current   = -1.0 ;
  pPriv->mot_inp.datum_speed     = -1.0 ;
  pPriv->mot_inp.datum_direction = -1 ;
  pPriv->mot_inp.num_steps       = -1.0 ;
  pPriv->mot_inp.setting_time = -1.0 ;
  pPriv->mot_inp.jog_speed_slow = -1 ;
  pPriv->mot_inp.jog_speed_hi = -1 ;
  strcpy(pPriv->mot_inp.stop_mess,"");
  strcpy(pPriv->mot_inp.abort_mess,"");
  pPriv->mot_inp.origin_directive = -1;
  strcpy(pPriv->mot_inp.home_switch,"enabled") ;

  /* Assign the indexer */
  /*
  if (strstr(pgs->name, "sectWhl") != NULL)  pPriv->indexer = 'a' ; 
  if (strstr(pgs->name,"winChngr") != NULL)  pPriv->indexer = 'b' ; 
  if (strstr(pgs->name,"aprtrWhl") != NULL)  pPriv->indexer = 'c' ; 
  if (strstr(pgs->name,"fltrWhl1") != NULL)  pPriv->indexer = 'd' ; 
  if (strstr(pgs->name, "lyotWhl") != NULL)  pPriv->indexer = 'e' ; 
  if (strstr(pgs->name,"fltrWhl2") != NULL)  pPriv->indexer = 'f' ; 
  if (strstr(pgs->name,  "pplImg") != NULL)  pPriv->indexer = 'g' ;    
  if (strstr(pgs->name, "slitWhl") != NULL)  pPriv->indexer = 'h' ; 
  if (strstr(pgs->name, "grating") != NULL)  pPriv->indexer = 'i' ; 
  if (strstr(pgs->name,"coldClmp") != NULL)  pPriv->indexer = 'j' ; 
  */
  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  
  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the motor file *****************/
long read_mot_file (char *rec_name, genSubMotPrivate *pPriv) {
  /* Initialization File */
  FILE *motFile ;
  char in_str[256] ;
  char filename[50] ;
  double numnum/* , numnum1, numnum2 */;
  int found ;
  char short_name[40] ;
  char some_str[40] ;
  char *some_str2 ;
  long mot_num ;
  double init_vel, slew_vel, accel, decel, hold_curr, drive_curr, backlash;
  char indexer;
  long home_speed, final_home_speed;
  long home_dir ;
  int  i ;
  
  counter++ ;
  strcpy(some_str,rec_name) ;

  /* printf("******* before deleting the record name @%s@\n", some_str) ; */
  some_str2 = strrchr(some_str,':') ;
  some_str2[0] = '\0' ;
  /* printf("******* After deleting the record name @%s@\n", some_str) ; */
  some_str2 = strrchr(some_str,':') ;
  some_str2 = some_str2 + 1 ;
  strcpy(short_name,some_str2) ;
  /* printf("The record name is still @%s@\n",rec_name) ; */
  /* printf("The short name is: @%s@\n",short_name) ; */
  
  strcpy(filename,mot_filename);

  if ((motFile = fopen(filename,"r")) == NULL) {
    trx_debug("Motor Controller file could not be opened", rec_name,"MID",cc_STARTUP_DEBUG_MODE);
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;

    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,256,motFile);  /* comment line */
    fgets(in_str,256,motFile);  /* comment line */
    fgets(in_str,256,motFile);  /* comment line */
    fgets(in_str,256,motFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    /* strncpy (cc_host_ip,in_str,strlen(in_str)) ; */
    strcpy (cc_host_ip,in_str) ;
    i = 0;

    while ( ( (isdigit(cc_host_ip[i])) || (cc_host_ip[i] == '.')) && (i < 15)) i++ ;
    cc_host_ip[i] = '\0' ;

    /* Read the port number */
    fgets(in_str,256,motFile); /* comment line */ 

    /* 
    fgets(in_str,256,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(motFile,"%lf",&numnum) ;

    cc_port_no = (long) numnum ;
    
    found = FALSE ;

    fgets(in_str,256,motFile);  /* read to end of line */
 
    fgets(in_str,256,motFile);  /* comment line */
    fscanf(motFile,"%lf",&numnum) ;
    number_motors = (long) numnum ;

    fgets(in_str,256,motFile);  /* read to end of line */

    /* printf("********************** number of motors is %ld\n",number_motors) ; */
    fgets(in_str,256,motFile);  /* comment line */
    found = FALSE ;

    while (!found) {
    
      /* read the numbers */
      fscanf(motFile,"%ld %lf %lf %lf %lf %lf %lf %c %ld %ld %ld %lf",
             &mot_num, &init_vel, &slew_vel, &accel, &decel, &hold_curr, &drive_curr,
             &indexer, &home_speed, &final_home_speed, &home_dir, &backlash) ;
      /* printf("%ld %lf %lf %lf %lf %lf %lf %c %lf %ld \n",mot_num, init_vel, slew_vel, accel, decel, hold_curr, drive_curr,
	 indexer, home_speed, home_dir) ; */

      fgets(in_str,256,motFile); /* remainder of the line */
      if (strstr(in_str, short_name) != NULL)  {
        found = TRUE ;
        /* printf("*******   ***** Found %s in line %ld \n",short_name, mot_num) ; */

        pPriv->mot_num                   = mot_num ;
        pPriv->CurrParam.init_velocity   = init_vel ;
        pPriv->CurrParam.slew_velocity   = slew_vel ;
        pPriv->CurrParam.acceleration    = accel ;
        pPriv->CurrParam.deceleration    = decel ;
        pPriv->CurrParam.drive_current   = drive_curr ;
        pPriv->indexer                   = indexer ;
        pPriv->CurrParam.datum_speed     = (long)home_speed ;
        pPriv->CurrParam.datum_direction = home_dir ;
        pPriv->CurrParam.backlash = backlash ;
        pPriv->CurrParam.final_datum_speed = final_home_speed ;
        pPriv->CurrParam.setting_time = 0.0;
        pPriv->CurrParam.jog_speed_slow = 0 ;
        pPriv->CurrParam.jog_speed_hi = 0;
      }
    
    }

    /* printf("\n########### ok ..... I am going ...\n"); */
    UFReadPositions (motFile,number_motors, mot_num) ;

    fclose(motFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for motorG *******************/
long ufmotorGproc (genSubRecord *pgs) {

  genSubMotPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;  
  long ticksNow ; 

  char *endptr ;
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  long timeout ;
  double numnum ;
  
  strcpy(cc_DEBUG_MODE,pgs->m) ;

  /* trx_debug("",pgs->name,"FULL",cc_DEBUG_MODE) ; */

  pPriv = (genSubMotPrivate *)pgs->dpvt ;
  /* printf("**** *** I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the config file %ld\n", tickGet()) ; */
      /* printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (trecs_initialized != 1)  init_trecs_config() ;
      /* printf("################### Ended reading the config file %ld\n", tickGet()) ; */

      if (!cc_initialized)  init_cc_config() ;
      /* printf("################### Started reading the motor file %ld\n", tickGet()) ; */
      /* read_mot_file(pgs->name, pPriv) ; */ /* ZZZ */
      /* printf("################### finished reading the motor file %ld\n ", tickGet()) ; */

      /* pPriv->port_no = cc_port_no ; */ /* ZZZ */

      /* Clear outpus A-J for motor Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy(pgs->valc,"") ; /* raw position */

      *(double *)pgs->vald = pPriv->CurrParam.init_velocity ;
      *(double *)pgs->vale = pPriv->CurrParam.slew_velocity ;
      *(double *)pgs->valf = pPriv->CurrParam.acceleration ; 
      *(double *)pgs->valg = pPriv->CurrParam.deceleration ;
      *(double *)pgs->valh = pPriv->CurrParam.drive_current ;
      *(double *)pgs->vali = pPriv->CurrParam.datum_speed ;
      *(long *)pgs->valj = pPriv->CurrParam.datum_direction ;
      /* printf("????????????? Init velocity = %f \n",pPriv->CurrParam.init_velocity) ; */
      *(double *)pgs->l =  pPriv->CurrParam.backlash ; /* backlash */      
      *(double *)pgs->n =  pPriv->CurrParam.init_velocity ; /* init velocity */
      *(double *)pgs->o =  pPriv->CurrParam.slew_velocity ;/* slew velocity */
      *(double *)pgs->p =  pPriv->CurrParam.acceleration ;/* acceleration */
      *(double *)pgs->q = pPriv->CurrParam.deceleration ; /* deceleration */
      *(double *)pgs->r = pPriv->CurrParam.drive_current ; /* drive current */
      *(double *)pgs->s =  pPriv->CurrParam.datum_speed ; /* datum speed */
      *(long *)pgs->t =    pPriv->CurrParam.datum_direction ; /* datum direction */

      /* trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",cc_STARTUP_DEBUG_MODE); */ /* ZZZ */
  
      /* establish the connection and get the socket number */ 
      pPriv->socfd = -1 ;/* UFGetConnection(pgs->name, pPriv->port_no,cc_host_ip) ; */  /* ZZZ */
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /*
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",cc_STARTUP_DEBUG_MODE); 
      } 
      */  /* ZZZ */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",cc_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */
      }

      /* pPriv->send_init_param = 1; */  /* ZZZ */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:

      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",cc_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",cc_DEBUG_MODE) ;
      
      pPriv->mot_inp.test_directive        = *(long *)pgs->a ;
      strcpy(pPriv->mot_inp.home_switch,pgs->b) ;

      pPriv->mot_inp.command_mode          = *(long *)pgs->c ;
      pPriv->mot_inp.datum_motor_directive = *(long *)pgs->e ;
      pPriv->mot_inp.num_steps             = *(double *)pgs->g ;
      pPriv->mot_inp.origin_directive = *(long *)pgs->u ;
      if (*(double *)pgs->n == pPriv->CurrParam.init_velocity)  pPriv->mot_inp.init_velocity = -1.0 ;
      else  pPriv->mot_inp.init_velocity = *(double *)pgs->n;
      if (*(double *)pgs->o == pPriv->CurrParam.slew_velocity) pPriv->mot_inp.slew_velocity = -1.0 ;
      else  pPriv->mot_inp.slew_velocity = *(double *)pgs->o;
      if (*(double *)pgs->p == pPriv->CurrParam.acceleration) pPriv->mot_inp.acceleration = -1.0 ;
      else  pPriv->mot_inp.acceleration = *(double *)pgs->p;
      if (*(double *)pgs->q == pPriv->CurrParam.deceleration) pPriv->mot_inp.deceleration = -1.0 ;
      else pPriv->mot_inp.deceleration = *(double *)pgs->q;
      if (*(double *)pgs->r == pPriv->CurrParam.drive_current) pPriv->mot_inp.drive_current = -1.0 ;
      else pPriv->mot_inp.drive_current = *(double *)pgs->r;
      if ( *(double *)pgs->s == pPriv->CurrParam.datum_speed) pPriv->mot_inp.datum_speed = -1.0 ;
      else pPriv->mot_inp.datum_speed = *(double *)pgs->s;
      if ( *(long *)pgs->t == pPriv->CurrParam.datum_direction) pPriv->mot_inp.datum_direction = -1 ;
      else pPriv->mot_inp.datum_direction = *(long *)pgs->t;
      if (*(double *)pgs->l == pPriv->CurrParam.backlash) pPriv->mot_inp.backlash = 0.0 ;
      else pPriv->mot_inp.backlash = *(double *)pgs->l;
      if ( (long)pPriv->CurrParam.setting_time != -1) pPriv->mot_inp.setting_time = pPriv->CurrParam.setting_time ;
      if ( (long)pPriv->CurrParam.jog_speed_slow != -1) pPriv->mot_inp.jog_speed_slow = pPriv->CurrParam.jog_speed_slow ;
      if ( (long)pPriv->CurrParam.jog_speed_hi != -1) pPriv->mot_inp.jog_speed_hi = pPriv->CurrParam.jog_speed_hi ;

      strcpy(pPriv->mot_inp.stop_mess,pgs->i) ;
      strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;
      /*
      printf("**************My inputs are : \n") ; 
      printf("test_directive %ld\n",pPriv->mot_inp.test_directive) ; 
      printf("Command Mode: %ld \n",pPriv->mot_inp.command_mode) ; 
      printf("datum_motor_directive  %ld\n",pPriv->mot_inp.datum_motor_directive) ; 
      printf("num_steps %f\n",pPriv->mot_inp.num_steps) ; 
      printf("init_velocity %f\n",pPriv->mot_inp.init_velo city) ; 
      printf("slew_velocity %f\n",pPriv->mot_inp.slew_velocity) ; 
      printf("acceleration %f\n",pPriv->mot_inp.acceleration) ; 
      printf("deceleration %f\n", pPriv->mot_inp.deceleration) ; 
      printf("drive_current %f\n",pPriv->mot_inp.drive_current) ; 
      printf("datum_speed %f\n",pPriv->mot_inp.datum_speed) ; 
      printf("datum_direction %ld\n", pPriv->mot_inp.datum_direction) ; 
      printf("stop message %s\n",pPriv->mot_inp.stop_mess) ; 
      printf("abort message %s\n",pPriv->mot_inp.abort_mess)  ;
      */
      if ( (pPriv->mot_inp.test_directive != -1) ||
           (pPriv->mot_inp.command_mode != -1) ||
           (pPriv->mot_inp.datum_motor_directive != -1) ||
           (pPriv->mot_inp.origin_directive != -1) ||
           ( (long)pPriv->mot_inp.num_steps != 0) ||
           ( (long)pPriv->mot_inp.init_velocity != -1) ||
           ( (long)pPriv->mot_inp.slew_velocity != -1) ||
           ( (long)pPriv->mot_inp.acceleration != -1) ||
           ( (long)pPriv->mot_inp.deceleration != -1) ||
           ( (long)pPriv->mot_inp.drive_current != -1) ||
           ( (long)pPriv->mot_inp.datum_speed != -1) ||
           ( pPriv->mot_inp.datum_direction != -1) ||
           ( strcmp(pPriv->mot_inp.stop_mess,"") != 0) ||
           ( strcmp(pPriv->mot_inp.abort_mess,"") != 0) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",cc_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",cc_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",cc_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-T",pgs->name,"FULL",cc_DEBUG_MODE) ;

        *(long *)pgs->a = -1 ; /* perform test  */
        *(long *)pgs->c = -1 ; /* Simulation Mode  */
        *(long *)pgs->e = -1 ; /* datum directive */
        *(double *)pgs->g = 0.0 ; /* num steps */
        strcpy(pgs->i,"") ; /* stop message */
        strcpy(pgs->j,"") ;  /* response from the agent via CA */
                             /* or the task that receives the response via TCP/IP */
        pgs->noj = 1;
        strcpy(pgs->k,"") ; /* abort message */
        *(long *)pgs->u = -1 ; /* stop directive */
        /* *(double *)pgs->n = -1.0 ; */ /* init velocity */
        /* *(double *)pgs->o = -1.0 ; */ /* slew velocity */
        /* *(double *)pgs->p = -1.0 ; */ /* acceleration */
        /* *(double *)pgs->q = -1.0 ; */ /* deceleration */
        /* *(double *)pgs->r = -1.0 ; */ /* drive current */
        /* *(double *)pgs->s = -1.0 ; */ /* datum speed */
        /* *(long *)pgs->t = -1 ;  */ /* datum direction */

        /*
        pPriv->mot_inp.command_mode    = -1 ; 
        pPriv->mot_inp.test_directive  = -1 ; 
        pPriv->mot_inp.init_velocity   = -1.0 ;
        pPriv->mot_inp.slew_velocity   = -1.0 ; 
        pPriv->mot_inp.acceleration    = -1.0 ; 
        pPriv->mot_inp.deceleration    = -1.0 ; 
        pPriv->mot_inp.drive_current   = -1.0 ; 
        pPriv->mot_inp.datum_speed     = -1.0 ; 
        pPriv->mot_inp.datum_direction = -1 ; 
        pPriv->mot_inp.num_steps       = 0.0 ;
        strcpy(pPriv->mot_inp.stop_mess,""); 
        strcpy(pPriv->mot_inp.abort_mess,""); 
        */       
      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        /*if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
        pgs->noj = 1;
        /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */

        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",cc_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",cc_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
        /* Clear the J Field */
        strcpy(pgs->j,"") ;
        /*if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
        pgs->noj = 1;
        /* Is it possible to get a CALLBACK processing here? */
      }
      break;

    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",cc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->mot_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",cc_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->mot_inp.command_mode == SIMM_NONE) ||
            (pPriv->mot_inp.command_mode == SIMM_FAST) ||
            (pPriv->mot_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufmotorGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", cc_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->mot_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          /* printf("My Command Mode is %ld \n",pPriv->mot_inp.command_mode) ; */
          /* Read some initial parameters from a file */
          pPriv->CurrParam.init_velocity = -1.0 ;
          pPriv->CurrParam.slew_velocity = -1.0 ;
          pPriv->CurrParam.acceleration = -1.0 ;
          pPriv->CurrParam.deceleration = -1.0 ;
          pPriv->CurrParam.drive_current= -1.0 ;
          pPriv->CurrParam.datum_speed = -1 ;
          pPriv->CurrParam.datum_direction = -1 ;
          pPriv->CurrParam.setting_time = -1.0 ;
          pPriv->CurrParam.jog_speed_slow = -1 ;
          pPriv->CurrParam.jog_speed_hi = -1 ;
          pPriv->CurrParam.backlash = 0.0;

          if (read_mot_file(pgs->name, pPriv) == OK) pPriv->send_init_param = 1 ;
          pPriv->port_no = cc_port_no ;
          *(double *)pgs->l = 0.0 ;  /* init velocity */
          *(double *)pgs->n = -1.0 ;  /* init velocity */
          *(double *)pgs->o = -1.0 ;  /* slew velocity */
          *(double *)pgs->p = -1.0 ;  /* acceleration */
	  *(double *)pgs->q = -1.0 ;  /* deceleration */
	  *(double *)pgs->r = -1.0 ;  /* drive current */
	  *(double *)pgs->s = -1.0 ;  /* datum speed  */
	  *(long *)pgs->t = -1 ;  /* datum direction */

          if (pPriv->mot_inp.command_mode != SIMM_FAST) {
            trx_debug("Attempting to reconnect",pgs->name,"FULL",cc_DEBUG_MODE) ;
	    if( pPriv->socfd < 0 ) {
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,cc_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",cc_DEBUG_MODE) ;
	      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->mot_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the output links */
            *(double *)pgs->vald = pPriv->CurrParam.init_velocity ;
            *(double *)pgs->vale = pPriv->CurrParam.slew_velocity ;
            *(double *)pgs->valf = pPriv->CurrParam.acceleration ; 
            *(double *)pgs->valg = pPriv->CurrParam.deceleration ;
            *(double *)pgs->valh = pPriv->CurrParam.drive_current ;
            *(double *)pgs->vali = pPriv->CurrParam.datum_speed ;
            *(long *)pgs->valj = pPriv->CurrParam.datum_direction ;
            *(double *)pgs->vall = pPriv->CurrParam.backlash ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",cc_DEBUG_MODE) ;
              return OK;
            } 
          }
	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
        if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
        }
      } /* We a move command */else {
        trx_debug("We have a motor Command",pgs->name,"FULL",cc_DEBUG_MODE) ;
        /* Save the current parameters */
        /* printf ("What the .....%ld , %s\n",pPriv->mot_inp.datum_motor_directive,pPriv->mot_inp.home_switch) ; */
        if ( (pPriv->mot_inp.datum_motor_directive == 1) && (strcmp(pPriv->mot_inp.home_switch,"disabled") == 0 )) {
	  /* we have a home directive but the home switch is disabled */

          /* Clear the input structure */
          pPriv->mot_inp.command_mode    = -1 ;
          pPriv->mot_inp.test_directive  = -1 ;
          pPriv->mot_inp.datum_motor_directive = -1 ;
          pPriv->mot_inp.init_velocity   = -1.0 ;
          pPriv->mot_inp.slew_velocity   = -1.0 ;
          pPriv->mot_inp.acceleration    = -1.0 ;
          pPriv->mot_inp.deceleration    = -1.0 ;
          pPriv->mot_inp.drive_current   = -1.0 ;
          pPriv->mot_inp.datum_speed     = -1.0 ;
          pPriv->mot_inp.datum_direction = -1 ;
          pPriv->mot_inp.num_steps       = 0.0 ;
          strcpy(pPriv->mot_inp.stop_mess,"");
          strcpy(pPriv->mot_inp.abort_mess,"");
          pPriv->mot_inp.origin_directive = -1;
          pPriv->mot_inp.setting_time = -1.0 ;
          pPriv->mot_inp.jog_speed_slow = -1 ;
          pPriv->mot_inp.jog_speed_hi = -1 ;
          pPriv->mot_inp.backlash = 0.0 ;
          /* set the command state to done */
          pPriv->commandState = TRX_GS_DONE;

          /* send an error message */
          strcpy (pPriv->errorMessage, "Home switch is disabled");
          /* set the CAR to ERR */
         status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
        } /* go head */ else {
         pPriv->OldParam.init_velocity = pPriv->CurrParam.init_velocity ;
          pPriv->OldParam.slew_velocity = pPriv->CurrParam.slew_velocity;
          pPriv->OldParam.acceleration = pPriv->CurrParam.acceleration;
          pPriv->OldParam.deceleration = pPriv->CurrParam.deceleration ;
          pPriv->OldParam.drive_current = pPriv->CurrParam.drive_current;
          pPriv->OldParam.datum_speed = pPriv->CurrParam.datum_speed;
          pPriv->OldParam.datum_direction = pPriv->CurrParam.datum_direction;
          pPriv->OldParam.backlash = pPriv->CurrParam.backlash;

          /* get the input into the current parameters */
          if ((long)pPriv->mot_inp.init_velocity != -1)  
            pPriv->CurrParam.init_velocity = pPriv->mot_inp.init_velocity  ;
          if ((long)pPriv->mot_inp.slew_velocity != -1) 
            pPriv->CurrParam.slew_velocity = pPriv->mot_inp.slew_velocity; 
          if ((long)pPriv->mot_inp.acceleration != -1) 
            pPriv->CurrParam.acceleration = pPriv->mot_inp.acceleration ;
          if ((long)pPriv->mot_inp.deceleration != -1) 
            pPriv->CurrParam.deceleration = pPriv->mot_inp.deceleration ;
          if ((long)pPriv->mot_inp.drive_current != -1) 
            pPriv->CurrParam.drive_current = pPriv->mot_inp.drive_current ;
          if (pPriv->mot_inp.datum_speed != -1) 
            pPriv->CurrParam.datum_speed = pPriv->mot_inp.datum_speed ;
          if (pPriv->mot_inp.datum_direction != -1) 
            pPriv->CurrParam.datum_direction = pPriv->mot_inp.datum_direction ;
          if ((long)pPriv->mot_inp.backlash != 0) 
            pPriv->CurrParam.backlash = pPriv->mot_inp.backlash ;

          /* see if this a command right after an init */
          if (pPriv->send_init_param) {
            pPriv->mot_inp.init_velocity = pPriv->CurrParam.init_velocity ;
            pPriv->mot_inp.slew_velocity = pPriv->CurrParam.slew_velocity ;
            pPriv->mot_inp.acceleration = pPriv->CurrParam.acceleration;
            pPriv->mot_inp.deceleration = pPriv->CurrParam.deceleration ;
            pPriv->mot_inp.drive_current = pPriv->CurrParam.drive_current ;
            pPriv->mot_inp.datum_speed = pPriv->CurrParam.datum_speed ;
            pPriv->mot_inp.datum_direction = pPriv->CurrParam.datum_direction ;
            pPriv->mot_inp.backlash = pPriv->CurrParam.backlash ;
          }
          /* formulate the command strings */
         com = malloc(50*sizeof(char *)) ;
          for (i=0;i<50;i++) com[i] = malloc(80*sizeof(char)) ;
         strcpy(rec_name,pgs->name) ;
          rec_name[strlen(rec_name)] = '\0' ;
          strcat(rec_name,".J") ;
          num_str = UFcheck_motor_inputs (pPriv->mot_inp,com, pPriv->indexer, rec_name,&pPriv->CurrParam, 
                                               pPriv->CurrParam.command_mode) ;
          /* Clear the input structure */
     
          pPriv->mot_inp.command_mode    = -1 ;
          pPriv->mot_inp.test_directive  = -1 ;
          pPriv->mot_inp.datum_motor_directive = -1 ;
          pPriv->mot_inp.init_velocity   = -1.0 ;
          pPriv->mot_inp.slew_velocity   = -1.0 ;
          pPriv->mot_inp.acceleration    = -1.0 ;
          pPriv->mot_inp.deceleration    = -1.0 ;
          pPriv->mot_inp.drive_current   = -1.0 ;
          pPriv->mot_inp.datum_speed     = -1.0 ;
          pPriv->mot_inp.datum_direction = -1 ;
          pPriv->mot_inp.num_steps       = 0.0 ;
          strcpy(pPriv->mot_inp.stop_mess,"");
          strcpy(pPriv->mot_inp.abort_mess,"");
          pPriv->mot_inp.origin_directive = -1;
          pPriv->mot_inp.setting_time = -1.0 ;
          pPriv->mot_inp.jog_speed_slow = -1 ;
          pPriv->mot_inp.jog_speed_hi = -1 ;
          pPriv->mot_inp.backlash = 0.0 ;

          /* do we have commands to send? */
          if (num_str > 0) {
            /* if no Agent needed then go back to DONE */
            /* we are talking about SIMM_FAST or commands that do not need the agent */
            /* printf("********** Thn number of strings is : %d\n",num_str) ; */
            if (bingo) for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;

            if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* update the output links */
              *(double *)pgs->vald = pPriv->CurrParam.init_velocity ;
              *(double *)pgs->vale = pPriv->CurrParam.slew_velocity ;
              *(double *)pgs->valf = pPriv->CurrParam.acceleration ; 
              *(double *)pgs->valg = pPriv->CurrParam.deceleration ;
              *(double *)pgs->valh = pPriv->CurrParam.drive_current ;
              *(double *)pgs->vali = pPriv->CurrParam.datum_speed ;
              *(long *)pgs->valj = pPriv->CurrParam.datum_direction ;
              *(double *)pgs->vall = pPriv->CurrParam.backlash;
              pPriv->CurrParam.setting_time = -1.0 ;
              pPriv->CurrParam.jog_speed_slow = -1 ;
              pPriv->CurrParam.jog_speed_hi = -1 ;
              pPriv->send_init_param = 0 ;
              some_num = CAR_IDLE ;
              status = dbPutField (&pPriv->carinfo.carState, 
                                   DBR_LONG, &some_num, 1);
             if (status) {
                logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
                return OK;
              }
            } else { /* we are in SIMM_NONE or SIMM_FULL */
              /* See if you can send the command and go to BUSY state */
              /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
              if (pPriv->socfd > 0) status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
              else status = 0 ;
              for (i=0;i<50;i++) free(com[i]) ;
              free(com) ;
              if (status == 0) { /* there was an error sending the commands to the agent */
                /* set Command state to DONE */
                pPriv->commandState = TRX_GS_DONE;
                /* set the CAR to ERR with appropriate message like 
                 * "Bad socket"  and go to DONE state*/
                strcpy (pPriv->errorMessage, "Bad socket connection");
                /* set the CAR to ERR */
                status = dbPutField (&pPriv->carinfo.carMessage, 
                                     DBR_STRING, pPriv->errorMessage,1);
                if (status) {
                  trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
                  return OK;
                }
                some_num = CAR_ERROR ;
                status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                               &some_num,1);
                if (status) {
                  trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
                  return OK;
                }
              } else { /* send successful */
                /* establish a CALLBACK */
                /* if (pPriv->CurrParam.homing) requestCallback(pPriv->pCallback,TRX_MOTOR_HOMING_TIMEOUT ) ;
		   else requestCallback(pPriv->pCallback,TRX_MOTOR_TIMEOUT ) ; */
                updates_count = 1 ;
                requestCallback(pPriv->pCallback,TRX_CC_AGENT_TIMEOUT) ;
                pPriv->startingTicks = tickGet() ;
                /* set Command state to BUSY */
                pPriv->commandState = TRX_GS_BUSY;
              }
            }
          } else { /* num_str <= 0 */
          for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (num_str < 0) {
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* we have an error in the Input */
              strcpy (pPriv->errorMessage, "Error in input");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
                return OK;
              }
            } else { /* num_str == 0 */
              /* is it possible to get here? */
              /* here we have a change in input.  The inputs though got sent
               * to be checked but no string got formulated.  */
              /* this can only happen if somehow the memory got corrupted */
              /* or the gensub is being processed by an alien. */
              /* Weird. */
              trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",cc_DEBUG_MODE) ;
            }
          }
        }
      }
      
      /*Is this a J field processing? */
     strcpy(response,pgs->j) ;
      /* if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
      pgs->noj = 1;
      /* if (strcmp(response,"") != 0) printf("??????? %s\n",response) ; */
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",cc_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      /*if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
      pgs->noj = 1;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",cc_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
     
      if ( (strcmp(pgs->i,"") != 0) || (strcmp(pgs->k,"") != 0)) {
        cancelCallback (pPriv->pCallback); 
        strcpy(pPriv->mot_inp.stop_mess,pgs->i) ;
        strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;
        strcpy(pgs->i,"") ;
        strcpy(pgs->k,"") ;
        pPriv->send_init_param = 0 ;
        requestCallback(pPriv->pCallback,0 ) ;  
        pPriv->commandState = TRX_GS_SENDING;   
        return OK ;  
      }

      strcpy(response,pgs->j) ;
      /* if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
      pgs->noj = 1;
      if (strcmp(response,"") != 0) {
        /* printf("??????? %s\n",response) ; */
        trx_debug(response,pgs->name,"MID",cc_DEBUG_MODE) ; 
      }

      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        /* printf("******** Is this a callback from timeout?\n") ;  
	   printf("%s\n",pPriv->errorMessage) ; */
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        /* if (pPriv->CurrParam.homing) timeout = TRX_MOTOR_HOMING_TIMEOUT ;
	   else timeout = TRX_MOTOR_TIMEOUT ; */
        timeout = TRX_CC_AGENT_TIMEOUT ;
        if ( (ticksNow >= (pPriv->startingTicks + timeout)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->CurrParam.homing = 0 ;
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Motor Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",cc_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */

       if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          pPriv->CurrParam.homing = 0 ;        
          /* update the output links */
          *(double *)pgs->vald = pPriv->CurrParam.init_velocity ;
          *(double *)pgs->vale = pPriv->CurrParam.slew_velocity ;
          *(double *)pgs->valf = pPriv->CurrParam.acceleration ; 
          *(double *)pgs->valg = pPriv->CurrParam.deceleration ;
          *(double *)pgs->valh = pPriv->CurrParam.drive_current ;
          *(double *)pgs->vali = pPriv->CurrParam.datum_speed ;
          *(long *)pgs->valj = pPriv->CurrParam.datum_direction ;
          *(double *)pgs->vall = pPriv->CurrParam.backlash ;
          pPriv->CurrParam.setting_time = -1.0 ;
          pPriv->CurrParam.jog_speed_slow = -1 ;
          pPriv->CurrParam.jog_speed_hi = -1 ;
          pPriv->send_init_param = 0 ;
          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",cc_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;

         if (*endptr != '\0'){ /* we do not have a number */

           cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            pPriv->CurrParam.homing = 0 ;
            pPriv->CurrParam.init_velocity = pPriv->OldParam.init_velocity ;
            pPriv->CurrParam.slew_velocity = pPriv->OldParam.slew_velocity;
            pPriv->CurrParam.acceleration = pPriv->OldParam.acceleration;
            pPriv->CurrParam.deceleration = pPriv->OldParam.deceleration ;
            pPriv->CurrParam.drive_current = pPriv->OldParam.drive_current;
            pPriv->CurrParam.datum_speed = pPriv->OldParam.datum_speed;
            pPriv->CurrParam.datum_direction = pPriv->OldParam.datum_direction;
            pPriv->CurrParam.backlash = pPriv->OldParam.backlash;
            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",cc_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",cc_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            cancelCallback (pPriv->pCallback);
            strcpy(pgs->valc,response) ;           
            requestCallback(pPriv->pCallback,TRX_CC_AGENT_TIMEOUT) ;
            pPriv->startingTicks = tickGet() ; 
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      /*if (bingo) printf("J field is: #%s#  Number of elements is %ld\n",response, pgs->noj) ; */
      pgs->noj = 1;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",cc_DEBUG_MODE) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/* Function to distribue one input to more than one output */
void distribute_str_input(genSubRecord *pgs, char link, int num_links) {

  char input_str[39] ;
  
  if (link == 'a') strcpy (input_str,pgs->a) ;
  else 
    if (link == 'b') strcpy (input_str,pgs->b) ;
    else 
      if (link == 'c') strcpy (input_str,pgs->c) ;
      else 
        if (link == 'd') strcpy (input_str,pgs->d) ;
        else 
          if (link == 'e') strcpy (input_str,pgs->e) ;
          else 
            if (link == 'f') strcpy (input_str,pgs->f) ;
            else 
              if (link == 'g') strcpy (input_str,pgs->g) ;
              else 
                if (link == 'h') strcpy (input_str,pgs->h) ;
                else 
                  if (link == 'i') strcpy (input_str,pgs->i) ;
                  else 
                    if (link == 'j') strcpy (input_str,pgs->j) ;
                    else 
                      if (link == 'k') strcpy (input_str,pgs->k) ;
                      else 
                        if (link == 'l') strcpy (input_str,pgs->l) ;
                        else 
                          if (link == 'm') strcpy (input_str,pgs->m) ;
                          else 
                            if (link == 'n') strcpy (input_str,pgs->n) ;
                            else 
                              if (link == 'o') strcpy (input_str,pgs->o) ;
                              else 
                                if (link == 'p') strcpy (input_str,pgs->p) ;
                                else 
                                  if (link == 'q') strcpy (input_str,pgs->q) ;
                                  else 
                                    if (link == 'r') strcpy (input_str,pgs->r) ;
                                    else 
                                      if (link == 's') strcpy (input_str,pgs->s) ;
                                      else 
                                        if (link == 't') strcpy (input_str,pgs->t) ;
                                        else 
                                          if (link == 'u') strcpy (input_str,pgs->u) ;
                                          else strcpy (input_str,"") ;
  
     /* printf("#######  Input link is: %c\n",link) ;
	printf("#######  The input string is: %s\n",input_str) ; 
  printf("#######  The number od links is %d\n",num_links) ;
  */
  strcpy(pgs->vala,input_str) ;
  if (num_links > 1) strcpy(pgs->valb,input_str) ;
  if (num_links > 2) strcpy(pgs->valc,input_str) ;
  if (num_links > 3) strcpy(pgs->vald,input_str) ;
  if (num_links > 4) strcpy(pgs->vale,input_str) ;
  if (num_links > 5) strcpy(pgs->valf,input_str) ;
  if (num_links > 6) strcpy(pgs->valg,input_str) ;
  if (num_links > 7) strcpy(pgs->valh,input_str) ;
  if (num_links > 8) strcpy(pgs->vali,input_str) ;
  if (num_links > 9) strcpy(pgs->valj,input_str) ;
  if (num_links > 10) strcpy(pgs->valk,input_str) ;
  if (num_links > 11) strcpy(pgs->vall,input_str) ;
  if (num_links > 12) strcpy(pgs->valm,input_str) ;
  if (num_links > 13) strcpy(pgs->valn,input_str) ;
  if (num_links > 14) strcpy(pgs->valo,input_str) ;
  if (num_links > 15) strcpy(pgs->valp,input_str) ;
  if (num_links > 16) strcpy(pgs->valq,input_str) ;
  if (num_links > 17) strcpy(pgs->valr,input_str) ;
  if (num_links > 18) strcpy(pgs->vals,input_str) ;
  if (num_links > 19) strcpy(pgs->valt,input_str) ;
  if (num_links > 20) strcpy(pgs->valu,input_str) ;
  return ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccdebug *******************/
long ufccdebugproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'j', 10);
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccInitS *******************/
long ufccinitSproc( genSubRecord *pgs) {
  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccParkS *******************/
long ufccparkSproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccstopS *******************/
long ufccstopSproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccAbortS *******************/
long ufccabortSproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccTestS *******************/
long ufcctestSproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for ccTestS *******************/
long ufccdaatumSproc( genSubRecord *pgs) {

  if (strcmp(pgs->a,"") != 0) distribute_str_input(pgs, 'a', 10);
  return OK ;
}

#endif /* __UFGEMGENSUBCOMMCC_C__  */

