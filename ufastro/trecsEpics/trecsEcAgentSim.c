

/*
 *
 *  Header stuffs for ecAgentSim.c
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <semLib.h>
#include <taskLib.h>
#include <sysLib.h>
#include <tickLib.h>
#include <logLib.h>
#include <msgQLib.h>

#include <recSup.h>
#include <car.h>

#include <trecsEcAgentSim.h>


/*
 *   Local defines
 */

#define EC_AGENT_PRIORITY          80          /* timer task priority     */
#define EC_AGENT_STACK             0x1000      /* task stack size         */
#define EC_AGENT_SCAN_RATE         10          /* task repeat per sec     */
#define EC_AGENT_OPTIONS           0           /* no extra task options   */

#define EC_AGENT_IDLE              0           /* waiting for a command   */
#define EC_AGENT_STARTING          1           /* starting a new command  */
#define EC_AGENT_EXECUTING         2           /* executing a command     */


/*
 *
 *  Data structures
 *
 */

static char buffer [MAX_STRING_SIZE]; /* scraechpad buffer          */
static long ecAgentState;             /* command progress flag      */
static ecAgentCommand command;        /* agent command to process   */

static long scanDelayTicks;           /* ticks between task scans   */
static long elapsedCommandTime;       /* elpased command time       */
static long elapsedHeartbeatTime;     /* elapsed heartbeat time     */
static long heartbeat;                /* heartbeat counter          */

static struct dbAddr carIval;         /* place to write CAR value   */
static struct dbAddr carImss;         /* place to write CAR message */
static struct dbAddr heartbeatVal;    /* place to write heartbeat   */
static struct dbAddr inBandVal;       /* place to write in-band flag*/

long trecsEcDebug = FALSE;

static MSG_Q_ID ecCommandQ = NULL;    /* commands arrive here       */

/*
 *
 *  Private function prototypes
 *
 */

static int ecAgentSimTask (int, int, int, int, int, int, int, int, int, int);



/*
 *
 *  Public functions
 *
 */


/*
 *  Add header for initEcAgentSim
 */

long initEcAgentSim 
(
    MSG_Q_ID messageQ           /* queue to wait for commands on    */
)
{
    int taskId;                 /* timer task ID                    */
    long status;                /* function return status           */


    /*
     *  Make sure that this function is only processed once.
     */

    if (ecCommandQ)
    {
        return OK;
    }


    /*
     *  Initialize the agent
     */
        
    ecCommandQ = messageQ;

    scanDelayTicks = sysClkRateGet() / EC_AGENT_SCAN_RATE;
    elapsedCommandTime = 0;
    elapsedHeartbeatTime = 0;
    heartbeat = 0;
    ecAgentState = EC_AGENT_IDLE;


    /*
     *  Connect to the agent heartbeat status records
     */

    strcpy (buffer, "trecs:ec:heartbeat.VAL");
    status = dbNameToAddr (buffer, &heartbeatVal);
    if (status)
    {
        logMsg ("can't locate PV: %s", (int) &buffer,0,0,0,0,0);
        return status;
    }

    strcpy (buffer, "trecs:ec:arrayG.C");
    status = dbNameToAddr (buffer, &inBandVal);
    if (status)
    {
        logMsg ("can't locate PV: %s", (int) &buffer,0,0,0,0,0);
        return status;
    }


    /*
     *  Create the timekeeping task
     */

    taskId = taskSpawn ("tEcAgentSim",             /* task name            */
                        EC_AGENT_PRIORITY,         /* set priority         */
                        EC_AGENT_OPTIONS,          /* set options          */
                        EC_AGENT_STACK,            /* set stack size       */
                        ecAgentSimTask,            /* function to spawn    */
                        0,0,0,0,0,0,0,0,0,0);      /* invocation args      */
    if (taskId == NULL)
    {
        status = -1;
        logMsg ("can't create EC agent simulator",0,0,0,0,0,0);
        return status;
    }
        
    return OK;
}


/*
 *  Header for ec agent simulator task
 */


int ecAgentSimTask
( 
    int a1, int a2, int a3, int a4, int a5, 
    int a6, int a7, int a8, int a9, int a10
)
{
    long startingTicks;         /* system ticks at start of task    */
    long elapsedTicks;          /* system ticks elapsed during task */
    long action;                /* curent action state              */
    long status;                /* function return status           */


    while (TRUE)
    {
        /*
         *  Get the time at task start
         */

        startingTicks = tickGet();


	/*
         *  Process aecording to the current state of the command
         *  simulation state machine.
         */

        switch (ecAgentState)
        {
            /*
             *  In IDLE state wait for a new command to arrive
             */

            case EC_AGENT_IDLE:
                status = msgQReceive (
                                (MSG_Q_ID) ecCommandQ,
                                (char *) &command,
                                sizeof (command),
                                NO_WAIT);

                if (status == ERROR && errno == S_objLib_OBJ_UNAVAILABLE)
                {
                    break;
                }

                else if (status == sizeof (command))
                {
                    ecAgentState = EC_AGENT_STARTING;
                }
        
                else
                {
                    logMsg ("ecAgentSim:msgQReceive failed\n",0,0,0,0,0,0);
                }

                break;


            /*
             *  In STARTING state we locate the car record and set it to BUSY
             */

            case EC_AGENT_STARTING:

                strcpy (buffer, command.carName);
                strcat (buffer, ".IVAL");

                status = dbNameToAddr (buffer, &carIval);
                if (status)
                {
                    logMsg ("can't locate PV: %s", (int) &buffer,0,0,0,0,0);
                    ecAgentState = EC_AGENT_IDLE;
                    break;
                }      
     

                strcpy (buffer, command.carName);
                strcat (buffer, ".IMSS");

                status = dbNameToAddr (buffer, &carImss);
                if (status)
                {
                    logMsg ("can't locate PV: %s", (int) &buffer,0,0,0,0,0);
                    ecAgentState = EC_AGENT_IDLE;
                    break;
                }

                action = CAR_BUSY;
                status = dbPutField (&carIval, 
                                     DBR_LONG, 
                                     &action, 
                                     1);
                if (status)
                {
                    logMsg ("can't write to CAR.IVAL",0,0,0,0,0,0);
                    ecAgentState = EC_AGENT_IDLE;
                    break;
                }
                 
		if (strcmp (command.commandName, "tempSet") == 0)
                {
                    if (trecsEcDebug) logMsg ("<%ld> Setting temperature to %s, power to %s\n", 
                            (int) tickGet(), 
                            (int) &command.attributeA, (int) &command.attributeB, 0, 0, 0);


                    strcpy (buffer, "FALSE");
                    status = dbPutField (&inBandVal, 
                                         DBR_STRING, 
                                         &buffer, 
                                         1);
                    if (status)
                    {
                        logMsg ("can't access inBandVal",0,0,0,0,0,0);
                    }

                }

		else if (strcmp (command.commandName, "init") == 0)
                {
                    if (trecsEcDebug) logMsg ("<%ld> initializeing with simulation mode %s\n", 
                            (int) tickGet(), 
                            (int) &command.attributeA, 0, 0, 0, 0);
               }
                ecAgentState = EC_AGENT_EXECUTING;
                break;




            /*
             *  In EXECUTING state we fake execution by waiting for the
             *  given execution time then setting the CAR to IDLE
             */

            case EC_AGENT_EXECUTING:

                /*
                 *  Wait for the execution time to expire
                 */

                if (elapsedCommandTime++ >= command.executionTime)
                {
                    elapsedCommandTime = 0;

                    /*
                     *  If another command is ready stay busy and
                     *  go back to execute it immediately...
                     */

                    if (msgQNumMsgs(ecCommandQ) != 0)
                    {
                        ecAgentState = EC_AGENT_IDLE;
                        break;
                    }


                    strcpy( buffer, "TRUE");
                    status = dbPutField (&inBandVal, 
                                         DBR_STRING, 
                                         &buffer, 
                                         1);
                    if (status)
                    {
                        logMsg ("can't acccess inBandVal",0,0,0,0,0,0);
                    }

                    if ((tickGet() % 1000) == 0)
                    {
                        action = CAR_ERROR;
                        strcpy (buffer, "Nasty execution fault....");
                    }
                    else
                    {
                        action = CAR_IDLE;
                        strcpy (buffer, "");
                    }

                    status = dbPutField (&carImss, 
                                         DBR_STRING, 
                                         &buffer, 
                                         1);
                    if (status)
                    {
                        logMsg ("can't access CAR IMSS",0,0,0,0,0,0);
                    }

                    status = dbPutField (&carIval, 
                                         DBR_LONG, 
                                         &action, 
                                         1);
                    if (status)
                    {
                        logMsg ("can't access CAR IVAL",0,0,0,0,0,0);
                    }
                 
                    if (trecsEcDebug) logMsg ("<%ld> %s finished execution\n", 
                            (int) tickGet(), 
                            (int) &command.commandName, 0, 0, 0, 0);

                    ecAgentState = EC_AGENT_IDLE;
                } 
   
                break;

        } /* end switch */


        /*
         *  Update the heartbeat counter once a second and write the
         *  new value to the agent heatbeat record....
         */

        if (elapsedHeartbeatTime++ >= EC_AGENT_SCAN_RATE -1)
        {
            elapsedHeartbeatTime = 0;
            heartbeat++;
            status = dbPutField (&heartbeatVal, 
                                 DBR_LONG, 
                                 &heartbeat, 
                                 1);
            if (status)
            {
                logMsg ("can't access heartbeatVal",0,0,0,0,0,0);
            }
        }
 
        /*
         *  Task finished .... wait for a bit then run it again
         */

        elapsedTicks = tickGet() - startingTicks;
        if (elapsedTicks >= scanDelayTicks)
        {
            logMsg ("ecAgentSim took too long\n",0,0,0,0,0,0);
        }
        else
        {
            taskDelay (scanDelayTicks - elapsedTicks); 
        }

    }  /* end while TRUE */
    
    return OK;              /* for completeness.... */
}
            
       
