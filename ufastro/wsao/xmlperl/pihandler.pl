#!/usr/local/bin/perl

# include package
use XML::Parser;

# initialize parser
$xp = new XML::Parser();

# set PI handler
$xp->setHandlers(Proc => \&pih);

# output some HTML
print "Content-Type: text/html\n\n";
print "<html><head></head><body>And the winning number is: ";
$xp->parsefile("pi.xml");
print "</body></html>";

# this is called whenever a PI is encountered
sub pih()
{
	# extract data
	my ($parser, $target, $data) = @_;

	# if Perl command
	if (lc($target) == "perl")
	{
  # execute it
  eval($data);
	}
}

# end
