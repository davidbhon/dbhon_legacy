#if !defined(__ufglvdtd_cc__)
#define __ufglvdtd_cc__ "$Name:  $ $Id: ufglvdtd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufglvdtd_cc__;

#include "UFGemSchaevitzAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFGemSchaevitzAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufglvdtd> exception in stdlib: "<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufglvdtd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufglvdtd_cc__
