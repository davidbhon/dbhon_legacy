#if !defined(__uflvdtc__)
#define __uflvdtc__ "$Name:  $ $Id: uflvdtc.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

static bool _quiet = false;

class uflvdtc : public UFDaemon, public UFClientSocket {
public:
  ~uflvdtc();
  inline uflvdtc(int argc, char** argv, int port= -1) : UFDaemon(argc, argv), UFClientSocket(port), _fs(0) {
    rename("uflvdtc@" + hostname());
  }

  static int main(int argc, char** argv);

  // return 0 on connection failure:
  FILE* init(const string& host, int port);

  // submit cmd & par (only), return immediately without fetching reply
  int submit(const string& c, const string& p, float flush= -1.0); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  //int submit(const string& raw, UFStrings& reply, float flush= -1.0); // flush output and sleep flush sec.
  //int submit(const string& raw, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& c, const string& p, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.

  virtual string description() const { return __uflvdtc__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
};

int uflvdtc::main(int argc, char** argv) {
  uflvdtc lvdt(argc, argv);
  bool prompt = false;
  string arg, host(hostname()), hist("false"), stat("false"), on("false"), off("false"),
         ra("false"), rl("false"), sa("false"), sl("false"); // default is usage
  int port= 52006;
  float flush= -1.0;

  arg = lvdt.findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = lvdt.findArg("-ra"); // read analogue value
  if( arg != "false" ) {
    _quiet = true;
    ra = arg;
  }

  arg = lvdt.findArg("-rl"); // read logic value
  if( arg != "false" ) {
    _quiet = true;
    rl = arg;
  }

  arg = lvdt.findArg("-sa"); // set analogue value
  if( arg != "false" ) {
    _quiet = true;
    sa = arg;
  }

  arg = lvdt.findArg("-sl"); // set logic value
  if( arg != "false" ) {
    _quiet = true;
    sl = arg;
  }

  arg = lvdt.findArg("-on"); // poweron
  if( arg != "false" ) {
    _quiet = true;
    on = arg;
  }

  arg = lvdt.findArg("-off"); // poweroff
  if( arg != "false" ) {
    _quiet = true;
    off = arg;
  }

  arg = lvdt.findArg("-hist");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      hist = arg;
    else
      hist = "1";
  }

  arg = lvdt.findArg("-stat");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      stat = arg;
    else
      stat = "fits";
  }

  arg = lvdt.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = lvdt.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = lvdt.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"uflvdtc> usage: 'uflvdtc -host host -port port -on -off -ra \"loc numvals\" -rl \"loc numvals\" -sa \"loc val\" -sl \"loc val\"  -hist [numvals]'"<<endl;
    return 0;
  }

  FILE* f = lvdt.init(host, port);
  if( f == 0 ) {
    clog<<"uflvdtc> unable to connect to Schaevitz LVDT agent/daemon..."<<endl;
    return 1;
  }

  UFStrings* reply_p= 0;
  if( ra != "false" ) {
    // raw (explicit) command should be executed once
    int ns = lvdt.submit("RA", ra, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit read analog (ra)= "<<ra<<endl;
  }
  else if( rl != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("RL", rl, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit read logic (rl)= "<<rl<<endl;
  }
  else if( sa != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("SA", sa, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit set analogue (sa)= "<<sa<<endl;
  }
  else if( sl != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("SL", sl, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit set logic (sl)= "<<sl<<endl;
  }
  else if( on != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("ON", on, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit on= "<<on<<endl;
  }
  else if( off != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("OFF", off, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit off= "<<off<<endl;
  }
  else if( hist != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("his", hist, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit hist= "<<hist<<endl;
  }
  else if( stat != "false" ) {
    // command should be executed once
    int ns = lvdt.submit("stat", stat, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit stat= "<<stat<<endl;
  }
  else {
    prompt = true;
  }

  if( !prompt && reply_p == 0 ) {
    clog<<"uflvdtc> no reply..."<<endl;
    return -1;
  }
  else if( !prompt ) {
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  // enter command loop
  /*
  string line;
  while( true ) {
    clog<<"uflvdtc> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;

    raw = line;
    int ns = lvdt.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "uflvdtc> failed to submit raw= "<<raw<<endl;

    if( reply_p == 0 ) {
      clog<<"uflvdtc> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  */
  clog<<"uflvdtc> interactive mode not supported, only command-line..."<<endl;
  return 0;
} // uflvdtc::main

// public func:
// return < 0 on connection failure:
FILE* uflvdtc::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !_quiet )
    clog<<"uflvdtc> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// public
uflvdtc::~uflvdtc() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

// submit string (only), return immediately without fetching reply
int uflvdtc::submit(const string& c, const string& p, float flush) {
  vector<string> cmd;
  cmd.push_back(c);
  cmd.push_back(p);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"uflvdtc::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
/*
int uflvdtc::submit(const string& raw, UFStrings*& r, float flush) {
  int ns = submit("raw", raw, flush);
  if( ns <= 0 )
    return ns;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}
*/

// submit string, recv reply
int uflvdtc::submit(const string& c, const string& p, UFStrings*& r, float flush) {
  int ns = submit(c, p, flush);
  if( ns <= 0 )
    return ns;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

int main(int argc, char** argv) {
  return uflvdtc::main(argc, argv);
}

#endif // __uflvdtc__
