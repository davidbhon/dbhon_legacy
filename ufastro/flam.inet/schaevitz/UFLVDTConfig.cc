#if !defined(__UFLVDTConfig_cc__)
#define __UFLVDTConfig_cc__ "$Name:  $ $Id: UFLVDTConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLVDTConfig_cc__;

#include "UFLVDTConfig.h"
#include "UFGemSchaevitzAgent.h"
#include "UFFITSheader.h"

// soc to baytech power control for lvdt on/off
bool UFLVDTConfig::_powered= false; // turned on/off
string UFLVDTConfig::_baytech; // host name or ip
int UFLVDTConfig::_outlet= 6; // lvdt outlet number 1 <= _outlet <= 8
UFBaytech* UFLVDTConfig::_btechsoc= 0;

UFLVDTConfig::UFLVDTConfig(const string& name) : UFDeviceConfig(name) {}

UFLVDTConfig::UFLVDTConfig(const string& name, const string& tshost, int tsport) : UFDeviceConfig(name, tshost, tsport) {}

string UFLVDTConfig::prefix() { return ";001 "; }
string UFLVDTConfig::terminator() { return "\r\n"; }

// connect calls create which then connects
UFTermServ* UFLVDTConfig::connect(const string& host, int port) {
  if( _btechsoc == 0 ) {
    if( _baytech.empty() )
      _baytech = "flambaytech"; // "irbaytech"; // "192.168.111.5";
    _btechsoc = UFBaytech::create(_baytech);
  }
  if( _btechsoc == 0 ) {
    clog<<"UFLVDTConfig::connect> no connection to F2 Baytech RPC..."<<endl;
  }
  else {
    clog<<"UFLVDTConfig::connect> connected to F2 Baytech RPC..."<<endl;
    string bstat = _btechsoc->powerOn(_outlet);
    clog<<"UFLVDTConfig::connect> powered On stat: "<<bstat<<endl;
  }

  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFLVDTConfig::connect> no connection to terminal server..."<<endl;
  }
  else if( !_devIO->validConnection() || _devIO->writable() <= 0 ) {
    clog<<"UFLVDTConfig::connect> (invalid soc) not connected to terminal server, assume sim..."<<endl;
    _devIO->close(); delete _devIO; _devIO = 0;
    return _devIO;
  }
  else {
    clog<<"UFLVDTConfig::connect> connected to terminal server..."<<endl;
  }

  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);

  string sot = prefix();
  _devIO->resetSOTS(sot);


  return _devIO;
}

vector< string >& UFLVDTConfig::queryCmds() {
  if( _queries.size() > 0 ) 
    return _queries;
  // "RA 1 1" -- read from location 1 one analogue value  -- measured unfiltered value (volts?)
  _queries.push_back("RA"); // analogue location read
  //_queries.push_back("Ra"); // analogue location read
  // _queries.push_back("ra"); // analogue location read

  _queries.push_back("RL"); // logic location read
  //_queries.push_back("Rl"); // logic location read
  //_queries.push_back("rl"); // logic location read


  return _queries;
}

vector< string >& UFLVDTConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  // "SA 62 0" -- set analogue location 62 to 0 -- selects input setup for secondary coil reading 
  // "SA 62 1" -- set analogue location 62 to 0 -- selects input setup for secondary/primary ratio (s/p)
  // "SA 62 2" -- set analogue location 62 to 0 -- selects input setup for (s-p)/(s+p)
  _actions.push_back("SA"); // analogue location write

  // "SL 55 ON" -- set logic location 55 ON -- excitation freq==5KHz
  // "SL 55 OFF" -- set logic location 55 ON -- excitation freq==2.5KHz
  // "SL 56 ON" -- set logic location 56 ON -- excitation voltage==5
  // "SL 56 OFF" -- set logic location 55 ON -- excitation voltage==3
  _actions.push_back("SL"); // logic location write 

  _actions.push_back("ON"); // power on the lvdt via the baytech
  _actions.push_back("OFF"); // power off the lvdt via the baytech

  return _actions;
}

int UFLVDTConfig::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      return 1;
    }
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }

  clog<<"UFLVDTConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFLVDTConfig::status(UFDeviceAgent* da) {
  //UFGemSchaevitzAgent* dasbc = dynamic_cast< UFGemSchaevitzAgent* > (da);
  if( da->_verbose )
    clog<<"UFLSConfig::status> "<<da->name()<<endl;

  vector< string > vec;

  string lvdt= "LVDTPWR == ";
  if( isOn() ) 
    lvdt += "ON";
  else
    lvdt += "OFF";

  vec.push_back(lvdt);

  string v, d;
  string g = UFGemSchaevitzAgent::getCurrent(v, d); 
  lvdt= "LVDTVLTS == ";
  lvdt += v;
  lvdt += " mV";
  vec.push_back(lvdt);

  lvdt = "LVDTDISP == ";
  lvdt += d;
  lvdt += " Displacement";
  vec.push_back(lvdt);

  return new UFStrings(da->name(), vec);
}

UFStrings* UFLVDTConfig::statusFITS(UFDeviceAgent* da) {
  // UFGemSchaevitzAgent* dasbc = dynamic_cast< UFGemSchaevitzAgent* > (da);
  if( da->_verbose )
    clog<<"UFLVDTConfig::statusFITS> "<<da->name()<<endl;

  string v, d;
  string g = UFGemSchaevitzAgent::getCurrent(v, d); 

  map< string, string > valhash, comments;
  valhash["LVDTDISP"] = d;
  comments["LVDTDISP"] = "LVDT Position (displacement in mm)";
  valhash["LVDTVLTS"] = v;
  comments["LVDTVLTS"] = "LVDT Primary or Secondary Coil voltage";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

string UFLVDTConfig::pwrOn() {
  if( UFDeviceAgent::_sim ) {
    _powered = true; 
    return "powered";
  }
  if( _btechsoc == 0 ) {
    _btechsoc = UFBaytech::create(_baytech);
  }
  else if( !_btechsoc->validConnection() ) {
    clog<<"UFLVDTConfig::pwrOn> bad connection, re-connecting..."<<endl;
    _btechsoc->close(); delete _btechsoc;
    _btechsoc = UFBaytech::create(_baytech);
  }
  string s = _btechsoc->powerOn(_outlet);
  if( s != "" ) _powered = true;
  return s;
}

string UFLVDTConfig::pwrOff() {
  if( UFDeviceAgent::_sim ) {
    _powered = false; 
    return "not powered";
  }
  if( _btechsoc == 0 ) {
    _btechsoc = UFBaytech::create(_baytech);
  }
  else if( !_btechsoc->validConnection() ) {
    clog<<"UFLVDTConfig::pwrOff> bad connection, re-connecting..."<<endl;
    _btechsoc->close(); delete _btechsoc;
    _btechsoc = UFBaytech::create(_baytech);
  }
  string s = _btechsoc->powerOff(_outlet);
  if( s != "" ) _powered = false;
  return s;
}

// refresh baytech menu (and keep connected)
string UFLVDTConfig:: refresh() {
  if( UFDeviceAgent::_sim ) return "SimMode"; 
  if( _btechsoc == 0 ) {
    _btechsoc = UFBaytech::create(_baytech);
  }
  else if( !_btechsoc->validConnection() ) {
    clog<<"UFLVDTConfig::refresh> bad connection, re-connecting..."<<endl;
    _btechsoc->close(); delete _btechsoc;
    _btechsoc = UFBaytech::create(_baytech);
  }
  //return _btechsoc->status();
  string bt = _btechsoc->celsius();
  UFRuntime::rmJunk(bt);
  size_t nlpos = bt.rfind("\n");
  while( nlpos != string::npos ) { bt.erase(nlpos, 1); nlpos = bt.rfind("\n"); }

  return "Baytech " + bt;
} 

#endif // __UFLVDTConfig_cc__
