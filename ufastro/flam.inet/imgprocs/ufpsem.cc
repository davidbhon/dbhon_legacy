#if !defined(__ufpsem_cc__)
#define __ufpsem_cc__ "$Name:  $ $Id: ufpsem.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufpsem_cc__;

#include "UFPSem.h"

int main(int argc, char** argv, char** envp) {
  // just call UFPSem main:
  return UFPSem::main(argc, argv, envp);
}

#endif // __ufpsem_cc__
