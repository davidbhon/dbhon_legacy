#if !defined(__UFGD_h__)
#define __UFGD_h__ "$Name:  $ $Id: .UFgd.h,v 0.0 2002/06/03 17:42:21 hon beta $"
#define __UFGD_H__(arg) const char arg##GD_h__rcsId[] = __UFGD_h__;

#include "gd.h" // the GD library header
#include "vector" // STL vector template class

class UFgd {
public:
  struct Lut8bit { int r; int g; int b; }; 
  typedef vector< UFgd::Lut8bit > Lut; 
  UFgd(int w= 320, int h= 240);
  virtual ~UFgd();

  // simple unit test main
  static int main(int argc, char** argv);

  int setDim(int w, int h);
  int getDim(int& w, int& h);

  // simple grayscale lut is default:
  int grayLut();

  // for use by "endian-conversions"
  int reorder(int val);
  // Little-endian data:
  int applyLutL(int *data, int w, int h);
  // Big-endian data:
  int applyLutB(int *data, int w, int h);

  int writePng(const string& filenm);
  int writeJpeg(const string& filenm);

  // the following are not yet implemented:
  int readLut(const string& lutfilenm);
  int writeLut(const string& lutfilenm);
  int getLut(Lut& lut);
  int resetLut(const Lut& lut);

protected:
  static int _w, _h;
  static gdImagePtr _im;
  static UFgd::Lut _lut;
};

#endif // __UFGD_h__
