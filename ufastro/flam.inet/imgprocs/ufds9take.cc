#if !defined(__ufds9take_cc__)
#define __ufds9take_cc__ "$Name:  $ $Id: ufds9take.cc,v 0.1 2000/02/18 20:50:29 hon Rel
ease $"
const char rcsId[] = __ufds9take_cc__;

#include "UFRuntime.h"
#include "UFEdtDMA.h"
#include "UFDs9.h"
#include "sys/stat.h"

// for use by UFEdtDMA::takeAndStore():
static UFDs9 _ds9;

void display(char* fits, int sz) {
  int stat = _ds9.display(fits, sz);
  if( stat < sz )
    clog<<"display> Ds9 not up?"<<endl;
}

int main(int argc, char** argv) {
  int cnt= 1, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0;
  UFEdtDMA::init(argc, argv, cnt, lut, index, filehint, fits, fits_sz);

  // open connetion to Ds9 and enter frame take loop:
  cnt = UFEdtDMA::takeAndStore(cnt, index, filehint, lut, fits, fits_sz, display);
  clog<<"ufds9take> took/grabbed frames: "<<cnt<<endl;

  return 0;
}

#endif // __ufds9take_cc__
