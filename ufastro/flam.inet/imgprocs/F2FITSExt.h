const char fitsF2Ext[] = "XTENSION= 'IMAGE   '           / IMAGE extension
BITPIX  =                   32 / number of bits per data pixel
NAXIS   =                    2 / number of data axes
NAXIS1  =                 2048 / Axis length
NAXIS2  =                 2048 / Axis length
PCOUNT  =                    0 / required keyword; must = 0
GCOUNT  =                    1 / required keyword; must = 1
AXISLAB1= 'Xaxis   '           / Axis label
AXISLAB2= 'Yaxis   '           / Axis label
EXTVER  =                    1 / Number assigned to a FITS extension.
END
";
