#if !defined(__UFCdl_cc__)
#define __UFCdl_cc__ "$Name:  $ $Id: UFCdl.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCdl_cc__;

#include "UFCdl.h"
__UFCdl_H__(UFCdl_cc);

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"
#include "iostream"

// IRAF Client Display Library (Cdl & ImTool)
#include "cdl.h"

CDLPtr UFCdl::_cdl = 0;

int UFCdl::open() {
  _cdl = ::cdl_open( (char *)getenv("IMTDEV") );
  if( _cdl == 0 ) {
    clog<<"UFCdl::open> cdl open failed"<<endl;
    return -1;
  }
  return (int)_cdl;
}

int UFCdl::close() {
  if( _cdl != 0 ) {
    ::cdl_close( _cdl ); 
    _cdl = 0;
  }
  return (int)_cdl;
}

int UFCdl::display(unsigned int* img, int w, int h) {
  if( _cdl == 0 )
    return -1;

  int fb_w, fb_h, nf;
  int stat= 0, fbconfig= 0;
  float max = img[0];

  for(int i = 0; i < w*h; ++i )
    if( max < img[i] ) max = img[i];

  ::cdl_selectFB( _cdl, w, h, &fbconfig, &fb_w, &fb_h, &nf, 1 );
  stat = ::cdl_displayPix( _cdl, img, w, h, 8*sizeof(int), 1, fbconfig, 1 ); 
  return stat;
}

// unit test main
/*
int UFCdl::main(int argc, char** argv) {
  int nx= 2048, ny= 2048;
  unsigned int *img = new  unsigned int[nx*ny];
  for( int i = 0; i < nx*ny; ++i )
    img[i] = i;

  UFCdl d;
  d.open();
  d.display(img, nx, ny);
  d.close();
  delete [] img;
}
*/

#endif // __UFCdl_cc__
