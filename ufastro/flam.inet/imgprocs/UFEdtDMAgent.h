#if !defined(__UFEdtDMAgent_h__)
#define __UFEdtDMAgent_h__ "$Name:  $ $Id: UFEdtDMAgent.h,v 0.9 2005/03/04 20:25:13 hon Exp $"
#define __UFEdtDMAgent_H__(arg) const char arg##UFEdtDMAgent_h__rcsId[] = __UFEdtDMAgent_h__;

#include "iostream.h"
#include "deque"
#include "map"
#include "string"
#include "vector"

#include "UFGemDeviceAgent.h"
#include "UFTermServ.h"
#include "UFEdtDMA.h"
#include "UFFrameConfig.h"
#include "UFObsConfig.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFDs9.h"

class UFEdtDMAgent: public UFGemDeviceAgent, public UFEdtDMA {
public:
  // instrument name should indicate nature of frame buffs.
  // trecs has 14, flamingos just 2.
  UFEdtDMAgent(const string& name, int argc, char** argv, char** envp);
  inline virtual ~UFEdtDMAgent() { delete _edtcfg; }

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual void startup();
  virtual UFTermServ* init();
  virtual int options(string& servlist);
  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  // edt dma event loop:
  static void dmaThread(UFEdtDMAgent& dma);
  // start dma event loop in new pthread
  static void* _dmaThread(void* p= 0);

  // create & start new dmaThread for each observation
  // abort/stop should kill current dmeTread 
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& replies);

  // main should start signal handler then enter DeviceAgent::exec...
  // DeviceAgent::action 
  static int main(const string& name, int argc, char** argv, char** envp);

  inline static int width() { return UFEdtDMA::width(); }
  inline static int height() { return UFEdtDMA::height(); }

protected:
  // handlers
  static void _clearAll();
  static void _sighandler(int sig);
  static void _cancelhandler(void* p= 0);
  static void _postacq(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fitsfilenm);
  // simulation mode
  static bool _SimAcq, _obsdone;
  static void _simacq(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fitsfilenm);
  // override UFDevAgent virtual:
  virtual void _setDefaults(const string& instrum= "flam");

  static pthread_t _dmaThrdId;
  static pthread_mutex_t _theFrmMutex, _theEdtMutex;

  static UFFrameConfig* _theFrmConf;
  static UFObsConfig* _theObsConf;

  // abort discards, stop saves data.
  // take options
  //static string _Options, _LutFile, _FitsFile;
  static UFRuntime::Argv _acqArgs;
  // postacq options:
  static bool _jpeg, _png;
  static double _exptime;
  static int _totalcnt, _edttotal, _postcnt, _scale, _ds9Disp;
  static string _car, _edtfrm, _edttot, _edtacq;
  static UFDs9 _ds9;

  // simulation mode 
  static string _simFITS; // fits filename (MEF)
  static map< UFStrings*, UFInts* > _simFrms; // contents of MEF: {header, frame(s)}
  static UFObsConfig* _readMEF(const string& filenm, map< UFStrings*, UFInts* >& data);

  static int _maxBuff;
  static map< string, int > *_nameIdxFrmBuff; // index # of named frame buff.
  // use theFrmMutex when accessing this:
  static deque < UFInts* > *_dataFrmBuff; // each named/idexed buff is a UFInt object

  // helpers
  static int _setAcqArgs(UFDeviceAgent::CmdInfo* act);
  int _allocNamedBuffs(const string& name);
  int _buffNames(const string& instrum, vector< string >& names);

  // jpeg, png output
  static void _writeImg(char* fits, int sz, const char* filenm, bool png= true, bool jpeg= false);

  // raw frame replication
  static int _replFrm(int* acqbuf);

  // raw frame history buff
  static int _histFrm(int* acqbuf);

  // trecs-canaricam diffs & accums:
  static int _evalAccumDiffs(int frmIdx, int buffIdx); 

};

#endif // __UFEdtDMAgent_h__
      
