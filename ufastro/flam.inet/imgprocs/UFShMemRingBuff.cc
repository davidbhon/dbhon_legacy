#if !defined(__UFShMemRingBuff_cc__)
#define __UFShMemRingBuff_cc__ "$Name:  $ $Id: UFShMemRingBuff.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFShMemRingBuff_cc__;
#include "UFShMemRingBuff.h"
__UFShMemRingBuff_H__(__UFShMemRingBuff_cc);

#include "UFRuntime.h"

#include "UFBytes.h"
#include "UFPosixRuntime.h"

///////////////////////// protected /////////////////////
bool UFShMemRingBuff::_locked(const string& name) {
  int val = UFPosixRuntime::semValue(name);
  if( val == 0 ) 
    return true;
  else
    return false;
}

int UFShMemRingBuff::_getActiveIndex(bool& isLocked) {
  UFPosixRuntime::semTake(_activeIndexSemName);
  //if( !_locked(_activeIndexSemName) ) UFPosixRuntime::semTake(_activeIndexSemName);

  _activeIdx = *_theActiveIndex;
  isLocked = (*_theFlag != (pid_t) -1) ? true : false;

  UFPosixRuntime::semRelease(_activeIndexSemName);
  return _activeIdx;
}  

int UFShMemRingBuff::_setActiveIndex(int idx) {
  //if( !_locked(_activeIndexSemName) ) UFPosixRuntime::semTake(_activeIndexSemName);
  UFPosixRuntime::semTake(_activeIndexSemName);

  if( idx >= size() ) idx = 0;

  _activeIdx = *_theActiveIndex = idx;

  UFPosixRuntime::semRelease(_activeIndexSemName);
  return _activeIdx;
} 
   
/////////////////////// public /////////////////////////////////////////////
// override these virtuals:

UFProtocol* UFShMemRingBuff::lockActiveBuf(int& index) {
  UFPosixRuntime::semTake(_activeIndexSemName);
  *_theFlag = getpid();
  int sz = size();
  if( index >= 0 ) {
    if( index >= sz )
      index = 0;
    _activeIdx = *_theActiveIndex = index;
  }
  else
    _activeIdx = *_theActiveIndex;

  UFPosixRuntime::semRelease(_activeIndexSemName);
  return activeBuf(index);
}

int UFShMemRingBuff::unlockActiveBuf() {
  int index= -1;
  UFPosixRuntime::semTake(_activeIndexSemName);
  *_theFlag = (pid_t) -1;
  index = _activeIdx;
  UFPosixRuntime::semRelease(_activeIndexSemName);
  return index;
}

int UFShMemRingBuff::activeIndex(bool& isLocked) { 
  int index = -1;
  if( _theActiveIndex && _theFlag )
    index = _getActiveIndex(isLocked); 

  return index;
}

int UFShMemRingBuff::nextActiveBuf() {
  _setActiveIndex(1 + _activeIdx);
  return unlockActiveBuf();
}  

UFProtocol* UFShMemRingBuff::waitForNewData(int& index) {
  bool locked = true;
  UFProtocol* ufp = 0;
  // wait for the framegrabber to finish and unlock the buffer
  int  _index = activeIndex(locked);
  do {
    index = activeIndex(locked);
    //clog<<"UFShMemRingBuff::waitForNewData> activeIndex= "<<index<<", locked= "<<locked<<endl;
    if( locked ) UFRuntime::sleep(0.01); //UFPosixRuntime::yield();
  } while( locked && index == _index );
  if( --index < 0 ) index = 0;
  return ufp = bufAt(index);
}


UFProtocol* UFShMemRingBuff::createShared(string& name, int bufsize, int type) {
  // this switch determines the size of the shared memory segment according to 
  // UFProtocol type:
  string semName;
  switch(type) {
  case UFProtocol::_Bytes_: {
    const int elem = bufsize / sizeof(short);
    // tmp protocol object used for proper evaluation of shared-memory size:
    UFBytes tmp("ufbytes", (unsigned char*)0, elem); // shallow copy ctor
    int shmsz = tmp.size(); // this better match the size of the shared memory protocol attributes!
    UFPosixRuntime::ShmSeg* sm = UFPosixRuntime::shmCreate(name, shmsz, semName);
    if( sm == 0 ) {
      clog<<"UFShMemRingBuff::init> failed to create or attach to"<<name<<endl;
      clear();
      return 0;
    }
    clog<<"UFShMemRingBuff::init> "<<name<<", sz= "<<shmsz<<", base="<<(int*)sm->base<<endl;
    UFProtocolAttributes* pa = (UFProtocolAttributes*) sm->base;
    // shared mem. ctor takes UFProtocolAttributes*, must be fixed length and correctly sized!
    return new UFBytes(name, pa, elem, true);
  }
  break;
  case UFProtocol::_Shorts_: {
    const int elem = bufsize / sizeof(short);
    // tmp protocol object used for proper evaluation of shared-memory size:
    UFShorts tmp("ufshorts", (unsigned short*)0, elem);
    int shmsz = tmp.size(); // this better match the size of the shared memory protocol attributes!
    UFPosixRuntime::ShmSeg* sm = UFPosixRuntime::shmCreate(name, shmsz, semName);
    if( sm == 0 ) {
      clog<<"UFShMemRingBuff::init> failed to create or attach to"<<name<<endl;
      clear();
      return 0;
    } 
    clog<<"UFShMemRingBuff::init> "<<name<<", sz= "<<shmsz<<", base="<<(int*)sm->base<<endl;
    UFProtocolAttributes* pa = (UFProtocolAttributes*) sm->base;
    // shared mem. ctor takes UFProtocolAttributes*, must be fixed length and correctly sized!
    return new UFShorts(name, pa, elem, true);
  }
  break;
  case UFProtocol::_Floats_: {
    const int elem = bufsize / sizeof(float);
    // tmp protocol object used for proper evaluation of shared-memory size:
    UFFloats ufp("uffloats", (float*)0, elem); 
    int shmsz = ufp.size(); // this better match the size of the shared memory protocol attributes!
    UFPosixRuntime::ShmSeg* sm = UFPosixRuntime::shmCreate(name, shmsz, semName);
    if( sm == 0 ) {
      clog<<"UFShMemRingBuff::init> failed to create or attach to"<<name<<endl;
      clear();
      return 0;
    }
    clog<<"UFShMemRingBuff::init> "<<name<<", sz= "<<shmsz<<", base="<<(int*)sm->base<<endl;
    UFProtocolAttributes* pa = (UFProtocolAttributes*) sm->base;
    // shared mem. ctor takes UFProtocolAttributes*, must be fixed length and correctly sized!
    return new UFFloats(name, pa, elem, true);
  }
  break;
  case UFProtocol::_Ints_:
  default: {
    const int elem = bufsize / sizeof(int);
    // tmp protocol object used for proper evaluation of shared-memory size:
    UFInts ufp("ufints", (int*)0, elem);
    int shmsz = ufp.size(); // this better match the size of the shared memory protocol attributes!
    UFPosixRuntime::ShmSeg* sm = UFPosixRuntime::shmCreate(name, shmsz, semName);
    if( sm == 0 ) {
      clog<<"UFShMemRingBuff::init> failed to create or attach to"<<name<<endl;
      clear();
      return 0;
    }
    clog<<"UFShMemRingBuff::init> "<<name<<", sz= "<<shmsz<<", base="<<(int*)sm->base<<endl;
    UFProtocolAttributes* pa = (UFProtocolAttributes*) sm->base;
    // shared mem. ctor takes UFProtocolAttributes*, must be fixed length and correctly sized!
    return new UFInts(name, pa, elem, true);
  }
  break;
  } // switch(type)
}

// this init signatures allows creation of shmem segments 
// for UFBytes, UFShorts, UFInts, & UFFloats; rather clumsily
// for now via a pair of switch/case statements:
int UFShMemRingBuff::init(vector< string >& names, int bufsize, int type) {
  if( names.size() <= 0 ) { // default to 2
    names.push_back("/.UFShMemRingBuff#1"); names.push_back("/.UFShMemRingBuff#2"); 
  }
  clear();
  _bufsize = bufsize; // byte count
  _externalData = true;
  _activeIdx = -1, 
  _activeBuf = 0;

  // create numbuf shared memory segments
  clog<<"UFShMemRingBuff::init> "<<names.size()<<" shared memory buffs."<<endl;

  // assume homogenius ring (each buffer is same fixed size == shmsz) 
  for( vector< string >::size_type i = 0; i< names.size(); ++i ) {
    UFProtocol* ufp = createShared(names[i], bufsize, type);
    clog<<"UFShMemRingBuff::init> create "<<names[i]<<endl;
    //sanityCheck(ufp);
    push_back(ufp);
    clog<<"UFShMemRingBuff::init> inserted new UFProtocol* "<<ufp->name()<<", "<<(*this)[i]->description()<<", "<<(*this)[i]->name()<<endl;
    // and also store the semaphore names
    _bufShmNames.push_back(names[i]);
  }

  // also create a shared memory segment for active buf info: index and write process pid (lock)
  string shmname = "/.UFShMemRingBuffIndex.";
  UFPosixRuntime::ShmSeg* smi = UFPosixRuntime::shmCreate(shmname,
							  sizeof(int)+sizeof(pid_t),
							  _activeIndexSemName);
  if( smi == 0 ) { 
    clog<<"UFShMemRingBuff::init> unable to create "<<shmname<<endl;
    return 0;
  }
  clog<<"UFShMemRingBuff::init> created "<<shmname<<", base="<<(int*)smi->base<<endl;
  _theActiveIndex = (int*) (smi->base);
  _theFlag = (pid_t*) (_theActiveIndex+1);
  _activeIdx = *_theActiveIndex = 0;
  _activeBuf = (*this)[_activeIdx];
  *_theFlag = (pid_t) -1;
  clog<<"UFShMemRingBuff::init> _activeIdx= "<<_activeIdx<<" buffer type & name: "
      << _activeBuf->typeId()<<" "<<_activeBuf->name()<<", Activity Flag= "<<*_theFlag<<endl;

  return size();
}

int UFShMemRingBuff::attach(vector< string >& names, int bufsize, bool reset) {
  if( names.size() <= 0 ) { // default to 4
    names.push_back(" "); 
    names.push_back(" "); 
    names.push_back(" "); 
    names.push_back(" ");
  }
  clear();
  _bufsize = bufsize;
  _externalData = true;
  _activeIdx = -1, 
  _activeBuf = 0;

  // this is a kludgey way to calc. the supposed size of the presumed existing shared mem. seg.
  UFBytes ufb(string("foo"), (const char*)0, bufsize, true); // shallow copy ctor
  int sz = ufb.size(); // this better match the size of the shared memory protocol attributes!
  // attach numbuf shared memory segments
  clog<<"UFShMemRingBuff::attach> "<<size()<<" shared memory buffs."<<endl;

  const int elem = _bufsize;
  for( vector< string >::size_type i = 0; i< names.size(); ++i ) {
    // provide a name if one is not supplied
    strstream s;
    s<<"/.UFShMemRingBuff#"<<i<<"."<<ends; 
    string name = s.str();
    delete s.str();

    if( names[i] == "" || names[i] == " " ) 
      names[i] = name;

    string bufSemName; // a sem. was created, but we don't need it now
    UFPosixRuntime::ShmSeg* sm = UFPosixRuntime::shmAttach(names[i], sz, bufSemName);

    if( sm ) {
      clog<<"UFShMemRingBuff::attached> attached "<<names[i]<<", base="<<(int*)sm->base<<endl;
      UFProtocolAttributes* pa = (UFProtocolAttributes*) sm->base;
      // shared mem. ctor takes UFProtocolAttributes*, everything must be fixed length
      // and correctly sized!
      UFBytes* ufbp = new UFBytes(names[i], pa, elem, true);
      //sanityCheck(ufbp);
      push_back(ufbp);
      clog<<"UFShMemRingBuff::attach> inserted new UFBytes* "<<ufbp<<", "<<(*this)[i]->description()<<", "<<(*this)[i]->name()<<endl;
      // and also store the semaphore names
      _bufShmNames.push_back(names[i]);
    }
    else {
      clog<<"UFShMemRingBuff::init> failed to create or attach to"<<names[i]<<endl;
      clear();
      // NOTE: guessing here on the return value
      return -1 ;
    }      
  }

  // attach toshared memory segment for active buf info: index and write process pid (lock)
  string shmname = "/.UFShMemRingBuffIndex.";
  UFPosixRuntime::ShmSeg* smi = UFPosixRuntime::shmAttach(shmname,
							  sizeof(int)+sizeof(pid_t),
							  _activeIndexSemName);
  if( smi == 0 ) { 
    clog<<"UFShMemRingBuff::attach> unable to attach "<<shmname<<endl;
    // NOTE: guessing on the return value
    return -1;
  }
  clog<<"UFShMemRingBuff::attach> attached "<<shmname<<", base="<<(int*)smi->base<<endl;
  
  _theActiveIndex = (int*) (smi->base);
  _theFlag = (pid_t*) (_theActiveIndex+1);
  // is another process is attaching ?
  UFPosixRuntime::semTake(_activeIndexSemName);
  if( reset ) {
    *_theFlag = (pid_t) -1;
    *_theActiveIndex = 0;
  }
  _activeIdx = *_theActiveIndex;
  _activeBuf = (*this)[_activeIdx];
  clog<<"UFShMemRingBuff::attach> _activeIdx= "<<_activeIdx<<" buffer type & name: "
      << _activeBuf->typeId()<<" "<<_activeBuf->name()<<", Activity Flag= "<<*_theFlag<<endl;

  return size();
}


// these virtual functions have default behavior that must be replaced
// for shared memory objects. all r/w funcs. must take/give the
// buffer's semaphore
// the write is the only function that set the activeIdx
// returns the index of the buffer written
int UFShMemRingBuff::write(const UFProtocol* data, int index) {
  // this assumes that only one process can write
  // lockActiveBuff also sets _active idx & Buf
  UFProtocol* p = lockActiveBuf(index);
  //p->deepCopy(*data);
  p->shallowCopy(*data);
  // increment the buffer index to make the next buffer active
  // and unlocks
  return nextActiveBuf();
}

int UFShMemRingBuff::read(UFProtocol* data, int index) {
  bool locked;
  if( index < 0 ) // this blocks until the active index semaphore is released
    index = _getActiveIndex(locked);

  UFProtocol* readBuf = bufAt(index);
  if( index != _activeIdx || !locked ) {
    //data->deepCopy(*readBuf);
    data->shallowCopy(*readBuf);
  }

  return index;
}

void UFShMemRingBuff::sanityCheck(UFProtocol* p) {
  const int elem = p->elements();
  const char* vals = p->valData();

  for( int i=0; i<elem; ++i ) 
  {
    clog<<"UFShMemRingBuff::sanityCheck> i, vals[i] = "<<i<<", "<<vals[i]<<endl;
  }
}

// static attributes
vector< string > UFShMemRingBuff::_bufShmNames;
//vector< string > UFShMemRingBuff::_bufSemNames;
string UFShMemRingBuff::_activeIndexSemName;

#endif // __UFShMemRingBuff_cc__




