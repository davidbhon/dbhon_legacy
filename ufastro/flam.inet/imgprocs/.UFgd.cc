#if !defined(__UFGD_cc__)
#define __UFGD_cc__ "$Name:  $ $Id: .UFgd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGD_cc__;

#include "UFgd.h"
__UFGD_H__(__UFGD_cc);

#include "stdio.h"
#include "iostream.h"

#include "UFRuntime.h"

// globals:
int UFgd::_w;
int UFgd::_h;
gdImagePtr UFgd::_im;
UFgd::Lut UFgd::_lut;

UFgd::UFgd(int w, int h) {
  // default to gray lut
  grayLut();
  // allocate the image
  UFgd::_im = ::gdImageCreate(w, h);
}

UFgd::~UFgd() {
  _lut.clear();
  for( int i = 0; i <= 255; ++i )
    ::gdImageColorDeallocate(UFgd::_im, i);
  
  // destroy the image in memory.
  ::gdImageDestroy(UFgd::_im); UFgd::_im = 0;
}

int UFgd::setDim(int w, int h) {
  _w = w;
  _h = h;
  // destroy & rreallocate image in memory.
  ::gdImageDestroy(UFgd::_im);
  UFgd::_im = ::gdImageCreate(w, h);
  return _w*_h;
}

int UFgd::getDim(int& w, int& h) {
  w = _w;
  h = _h;
  return _w*_h;
}

int UFgd::grayLut() {
  UFgd::Lut8bit gray;
  int color= 0;
  for( int i = 0; i <= 255; ++i ) {
    // assuming that gdImageColorAllocate returns indices 0-255 sequentially:
    gray.r = gray.g = gray.b = i;
    color = ::gdImageColorAllocate(UFgd::_im, gray.r, gray.g, gray.b);
    UFgd::_lut.push_back(gray);
    clog<<"UFgd::grayLut> color: "<< color << ", lutrgb: "
    	<< UFgd::_lut[color].r << " " << UFgd::_lut[color].g << " " << UFgd::_lut[color].b << endl;
  }
  //clog<<"UFgd::grayLut> color: "<< color << ", lutrgb: "
  //	<< _lut[color].r << " " << _lut[color].g << " " << _lut[color].b << endl;
  return (int)_lut.size();
}

int UFgd::reorder(int val) {
  char* tmp = (char*) &val;
  char b = tmp[0];
  tmp[0] = tmp[3]; tmp[3] = b;  b = tmp[1]; tmp[1] = tmp[2]; tmp[2] = b;
  int* r = (int*)tmp;

  return *r;
}
  
// little-endian data
int UFgd::applyLutL(int *data, int w, int h) {
  // truncate to fit, if necessary:
  if( w > _w ) w = _w;
  if( h > _h ) h = _h;
  int elem = w*h;
  int min;
  int max;
  if( !UFRuntime::littleEndian() ) { // big-endian machine  
    min = max = reorder(data[0]);
    for(int i= 1; i < elem; ++i ) {
      int tmp = reorder(data[i]);
      if( tmp > max ) max = tmp;
      if( tmp < min ) min = tmp;
    }
    int spread = max - min;
    int i= 0;
    for( int y= 0; y < h; ++y ) {
      for( int x= 0; x < w; ++x ) {
        int tmp = reorder(data[i++]);
        int color = 255 * (tmp - min) / spread; 
        UFgd::_im->pixels[y][x] = color;
      }
    }
    return elem;
  }
  min = max = data[0];
  for( int i= 1; i < elem; ++i ) {
    if( data[i] > max ) max = data[i];
    if( data[i] < min ) min = data[i];
  }
  int spread = max - min;
  int i= 0;
  for( int y= 0; y < h; ++y ) {
    for( int x= 0; x < w; ++x ) {
      int color = 255 * (data[i++] - min) / spread; 
      UFgd::_im->pixels[y][x] = color;
    }
  }
  return elem;
}

// data is big-endian
int UFgd::applyLutB(int *data, int w, int h) {
  // big-endian data
  // truncate to fit, if necessary:
  if( w > _w ) w = _w;
  if( h > _h ) h = _h;
  int elem = w*h;
  int min;
  int max;
  if( UFRuntime::littleEndian() ) { // little-endian machine  
    min = reorder(data[0]);
    max = reorder(data[0]);
    for(int i= 0; i < elem; ++i ) {
      int tmp = reorder(data[i]);
      if( tmp > max ) max = tmp;
      if( tmp < min ) min = tmp;
    }
    int spread = max - min;
    int i= 0;
    for(int y= 0; y < h; ++y ) {
      for(int x= 0; x < w; ++x, ++i ) {
        int tmp = reorder(data[i]);
        int color = 255 * (tmp - min) / spread; 
        UFgd::_im->pixels[y][x] = color;
      }
    }
    return elem;
  }
  min = data[0];
  max= data[0];
  for(int i= 1; i < elem; ++i ) {
    if( data[i] > max ) max = data[i];
    if( data[i] < min ) min = data[i];
  }
  int spread = max - min;
  int i= 0;
  for(int y= 0; y < h; ++y ) {
    for(int x= 0; x < w; ++x, ++i ) {
      int color = 255 * (data[i] - min) / spread; 
      UFgd::_im->pixels[y][x] = color;
    }
  }
  return elem;
}

int UFgd::writePng(const string& filenm) {
  FILE *pngout = ::fopen(filenm.c_str(), "w");
  ::gdImagePng(UFgd::_im, pngout);
  return ::fclose(pngout);
}

int UFgd::writeJpeg(const string& filenm) {
  FILE *jpegout = ::fopen(filenm.c_str(), "w");
  ::gdImageJpeg(UFgd::_im, jpegout, -1);
  return ::fclose(jpegout);
}

int UFgd::main(int argc, char** argv) {
  // test  trecs
  int w= 320, h= 240;
  const int ttot= w*h;
  int trecs[ttot];
  for( int i = 0; i < ttot; ++i )
    trecs[i] = i;
  
  UFgd tgd(w, h);
  if( UFRuntime::littleEndian() )
    tgd.applyLutL(trecs, w, h);
  else
    tgd.applyLutB(trecs, w, h);

  tgd.writeJpeg("trecs.jpg");
  tgd.writePng("trecs.png");

  // test flamingos
  w = h = 2048;
  const int ftot= w*h;
  int flmn[ftot];
  for( int i = 0; i < ftot; ++i )
    flmn[i] = i;
  
  UFgd fgd(w, h);
  if( UFRuntime::littleEndian() )
    fgd.applyLutL(flmn, w, h);
  else
    fgd.applyLutB(flmn, w, h);

  fgd.writeJpeg("flmn.jpg");
  fgd.writePng("flmn.png");

  return 0;
}

#endif // __UFGD_cc__
