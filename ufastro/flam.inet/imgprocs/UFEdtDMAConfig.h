#if !defined(__UFEdtDMAConfig_h__)
#define __UFEdtDMAConfig_h__ "$Name:  $ $Id: UFEdtDMAConfig.h,v 0.6 2006/02/10 22:07:38 hon Exp $"
#define __UFEdtDMAConfig_H__(arg) const char arg##UFEdtDMAConfig_h__rcsId[] = __UFEdtDMAConfig_h__;

#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFTermServ.h"
#include "UFFlamObsConf.h"

//using namespace std;
#include "string"

// forward declarations for friendships
class UFGemEdtDMAAgent;

class UFEdtDMAConfig : public UFDeviceConfig {
public:
  UFEdtDMAConfig(const string& name= "UnknownEdtDMA@DefaultConfig");
  inline virtual ~UFEdtDMAConfig() {}

  virtual UFStrings* status(UFDeviceAgent*);
  virtual UFStrings* statusFITS(UFDeviceAgent*);

  // allowed cmds
  virtual vector< string >& UFEdtDMAConfig::queryCmds();
  virtual vector< string >& UFEdtDMAConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& name);
  virtual int validCmd(const string& name, const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  // if start of new observation requested, and/or obsconf parameters
  // have been provided, create new obsconf and return, otherwise
  // return 0:
  static UFFlamObsConf* obsconf(UFDeviceAgent::CmdInfo* info);

  static string infocam(); 
  static string initcam(const string& configfile); 
  inline static string initF2test() {
    char* uf = getenv("UFINSTALL"); if( uf == 0 ) uf = "/share/local/uf";
    string confile = uf; confile += "/etc/test_2048.cfg";
    string info = initcam(confile);
    info += infocam();
    return info;
  }
  inline static string initF2mce() {
    char* uf = getenv("UFINSTALL"); if( uf == 0 ) uf = "/share/local/uf";
    string confile = uf; confile += "/etc/boeing_2048.cfg";
    string info = initcam(confile);
    info += infocam();
    return info;
  }

protected:
  // allow Agent access
  friend class UFGemEdtDMAgent;
};

#endif // __UFEdtDMAConfig_h__
