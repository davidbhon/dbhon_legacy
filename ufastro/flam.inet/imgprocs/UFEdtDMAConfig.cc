#if !defined(__UFEdtDMAConfig_cc__)
#define __UFEdtDMAConfig_cc__ "$Name:  $ $Id: UFEdtDMAConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFEdtDMAConfig_cc__;

#include "UFEdtDMAConfig.h"
#include "UFGemEdtDMAgent.h"
#include "UFFITSheader.h"
#include "UFRuntime.h"

// statics

string UFEdtDMAConfig::initcam(const string& configfile) {
  string info;
  string cmdinit = "/opt/EDTpdv/initcam -f " + configfile;
  FILE* fp = UFRuntime::pipeReadChild(cmdinit.c_str());
  if( fp == 0 )
    return info;

  info = UFRuntime::pipeRead(fp);
  return info;
}

string UFEdtDMAConfig::infocam() {
  string info;
  string cmdinfo = "/opt/EDTpdv/edtinfo ";
  FILE* fp = UFRuntime::pipeReadChild(cmdinfo.c_str());
  if( fp == 0 )
    return info;

  info = UFRuntime::pipeRead(fp);
  return info;
}

// construct an obs. config from action cmd list (of strings) if
// transacted from epics osberve cad -- i.e. if at least exposure time and
// and dhs datalabel have been provided, otherwise return a default obs. conf.
UFFlamObsConf* UFEdtDMAConfig::obsconf(UFDeviceAgent::CmdInfo* info) { 
  UFFlamObsConf* ufo = 0;
  deque< string > cmds;
  int nc = info->cmdDeque(cmds);
  // check that cmdinfo contains this; if so use special ctor..
  if( nc <= 0 ) {
    clog<<"UFEdtDMAConfig::obsconf> empty UFDeviceAgent::CmdInfo?"<<endl;
    return ufo;
  }
  clog<<"UFEdtDMAConfig::obsconf> UFDeviceAgent::CmdInfo elem. cnt: "<<nc<<endl;
  int nf = nc;
  for( int i = 0; i < nc; ++i ) {
    string cmd = cmds[i];
    UFRuntime::upperCase(cmd);
    clog<<"UFEdtDMAConfig::obsconf> UFDeviceAgent::CmdInfo elem: "<<cmd<<endl;
    if( cmd.find("OBSCONF") != string::npos || cmd.find("OBSSET") != string::npos ) {
      nf = i;
      clog<<"UFEdtDMAConfig::obsconf> found: "<<cmd<<endl;
      break;
    }
  }
  if( nf >= nc ) {
    clog<<"UFEdtDMAConfig::obsconf> unable to find OBSCONF or OBSSETUP directive"<<endl;
    return ufo;
  }

  ufo = new UFFlamObsConf(cmds);
  clog<<"UFEdtDMAConfig::obsconf> exptime: "<<ufo->expTime()<<", frmmode: "<<ufo->imageCnt()
      <<", dhslabel: "<<ufo->datalabel()<<endl;

  return ufo;
}

// ctors
UFEdtDMAConfig::UFEdtDMAConfig(const string& name) : UFDeviceConfig(name) {}
/*
UFEdtDMAConfig::UFEdtDMAConfig(const string& name,
			       const string& tshost,
			       int tsport) : UFDeviceConfig(name, tshost, tsport) {}
*/
// generate a status report
UFStrings* UFEdtDMAConfig::status(UFDeviceAgent* da) {
  UFGemEdtDMAgent* dma = dynamic_cast< UFGemEdtDMAgent* > (da);
  vector < string > list;
  strstream s;
  string idle = "false";
  string applyingLut = "false";

  int w, h, id;
  if( dma->takeIdle(w, h) )
    idle = "true";
  if( dma->applyingLut(id) )
    applyingLut = "true";

  int tkcnt = dma->takeCnt();
  int totcnt = dma->takeTotCnt();
  int acqcnt = dma->acqCnt();
  int acqto = dma->acqTimeOut();

  s<<da->name()<<", config: "<<w<<"x"<<h<<", idle: "<<idle
   <<", applyingLUT: "<<applyingLut<<", LUT Id: "<<id
   <<", frames/total: "<< tkcnt <<" / "<< totcnt
   <<", totalAcqs: "<< acqcnt <<", timeOut: "<< acqto <<ends;

  list.push_back(s.str()); delete s.str();
  return new UFStrings(da->name(), list);
}

UFStrings* UFEdtDMAConfig::statusFITS(UFDeviceAgent* da) {
  UFGemEdtDMAgent* dma = dynamic_cast< UFGemEdtDMAgent* > (da);
  map < string, string > valhash, comments;
  string idle = "false";
  string applyingLut = "false";

  int w, h, id;
  if( dma->takeIdle(w, h) )
    idle = "true";
  if( dma->applyingLut(id) )
    applyingLut = "true";

  valhash["idle"] = idle; comments["idle"] = "acquisition idle == true/false";
  valhash["appLut"] = applyingLut; comments["appLut"] = "applying LUT == true/false";
  valhash["LutId"] = id; comments["LutId"] = "LUT >= 0";
  valhash["tkcnt"] = dma->takeCnt(); comments["tkcnt"] = "observation acquisition (edt take) count";
  valhash["totcnt"] = dma->takeTotCnt(); comments["totcnt"] = "observation total acquisition (edt take) count";
  valhash["acqcnt"] = dma->acqCnt(); comments["acqcnt"] = "total acquisition (all edt take) count";
  valhash["acqto"] = dma->acqTimeOut(); comments["acqto"] = "acquisition timeout";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

vector< string >& UFEdtDMAConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("acq");  _actions.push_back("Acq"); _actions.push_back("ACQ");
  _actions.push_back("abort"); _actions.push_back("Abort"); _actions.push_back("ABORT");
  _actions.push_back("data"); _actions.push_back("Data"); _actions.push_back("DATA");
  _actions.push_back("cnt"); _actions.push_back("Cnt"); _actions.push_back("CNT");
  _actions.push_back("expcnt"); _actions.push_back("ExpCnt"); _actions.push_back("EXPCNT");
  _actions.push_back("comment"); _actions.push_back("Comment"); _actions.push_back("COMMENT");
  _actions.push_back("conf"); _actions.push_back("Conf"); _actions.push_back("CONF");
  _actions.push_back("dhs"); _actions.push_back("DHS"); _actions.push_back("DHS");
  _actions.push_back("ds9"); _actions.push_back("Ds9"); _actions.push_back("DS9");
  _actions.push_back("exptime"); _actions.push_back("ExpTime"); _actions.push_back("EXPTIME");
  //  _actions.push_back("false"); _actions.push_back("False"); _actions.push_back("FALSE");
  _actions.push_back("fits"); _actions.push_back("Fits"); _actions.push_back("FITS");
  _actions.push_back("file"); _actions.push_back("File"); _actions.push_back("FILE");
  _actions.push_back("frame"); _actions.push_back("Frame"); _actions.push_back("FRAME");
  _actions.push_back("host"); _actions.push_back("Host"); _actions.push_back("HOST");
  _actions.push_back("info"); _actions.push_back("Info"); _actions.push_back("INFO");
  _actions.push_back("index"); _actions.push_back("Index"); _actions.push_back("INDEX");
  _actions.push_back("jpeg"); _actions.push_back("Jpeg"); _actions.push_back("JPEG");
  _actions.push_back("lut"); _actions.push_back("Lut"); _actions.push_back("LUT");
  _actions.push_back("mode"); _actions.push_back("Mode"); _actions.push_back("MODE");
  _actions.push_back("nfs"); _actions.push_back("Nfs"); _actions.push_back("NFS");
  _actions.push_back("obs"); _actions.push_back("Obs"); _actions.push_back("OBS");
  _actions.push_back("png"); _actions.push_back("Png"); _actions.push_back("PNG");
  _actions.push_back("scale"); _actions.push_back("Scale"); _actions.push_back("SCALE");
  _actions.push_back("shutdown"); _actions.push_back("Shutdown"); _actions.push_back("SHUTDOWN");
  _actions.push_back("sim"); _actions.push_back("Sim"); _actions.push_back("SIM");
  _actions.push_back("stop"); _actions.push_back("Stop"); _actions.push_back("STOP");
  _actions.push_back("start"); _actions.push_back("Start"); _actions.push_back("START");
  _actions.push_back("timeout"); _actions.push_back("Timeout"); _actions.push_back("TIMEOUT");
  _actions.push_back("true"); _actions.push_back("True"); _actions.push_back("TRUE");
  _actions.push_back("user"); _actions.push_back("User"); _actions.push_back("USER");
  _actions.push_back("usr"); _actions.push_back("Usr"); _actions.push_back("USR");

  // support varosi-like requests?
  _actions.push_back("OC"); // obsconf
  _actions.push_back("BN"); // buffernames
  //_actions.push_back("FC"); // frameconf

  // trecs obs. config parameters
  _actions.push_back("chop"); _actions.push_back("Chop"); _actions.push_back("CHOP"); // 1 or 2
  _actions.push_back("nod"); _actions.push_back("Nod"); _actions.push_back("NOD");  // 1 or 2
  // number of chop images
  _actions.push_back("saveset"); _actions.push_back("Saveset"); _actions.push_back("SAVESET");
  //  number of nod images
  _actions.push_back("nodset"); _actions.push_back("Nodset"); _actions.push_back("NODSET");

  return _actions;
}

vector< string >& UFEdtDMAConfig::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("stat"); _queries.push_back("Stat"); _queries.push_back("STAT");
  return _queries;
}

int UFEdtDMAConfig::validCmd(const string& name) {
  if( name.length() <= 1 )
   return -1; // cmd name & impl. must be non empty & unique

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  int cnt = (int)av.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFEdtDMAConfig::validCmd> av: "<<av[i]<<", name: "<<name<<endl;
    if( name.find(av[i]) != string::npos ) {
      return 0; 
    }
  }

  vector< string >& qv = queryCmds();
  cnt = (int)qv.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFEdtDMAConfig::validCmd> qv: "<<qv[i]<<", name: "<<name<<endl;
    if( name.find(qv[i]) != string::npos )
      return 0;
  }
  
  clog<<"UFEdtDMAConfig::validCmd> cmd name unknown: "<<name<<endl; 
  return -1;
}

int UFEdtDMAConfig::validCmd(const string& name, const string& c) {
  if( name.length() <= 1 && c.length() <= 0 )
   return -1; // cmd name & impl. must be non empty & unique

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  int cnt = (int)av.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFEdtDMAConfig::validCmd> av: "<<av[i]<<", name: "<<name<<", c: "<<c<<endl;
    if( name.find(av[i]) != string::npos ) {
      return 0; 
    }
    if( c.find(av[i]) != string::npos ) {
      return 0; 
    }
  }

  vector< string >& qv = queryCmds();
  cnt = (int)qv.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFEdtDMAConfig::validCmd> qv: "<<qv[i]<<", name: "<<name<<", c: "<<c<<endl;
    if( name.find(qv[i]) != string::npos )
      return 0;
    if( c.find(qv[i]) != string::npos )
      return 0;
  }
  
  clog<<"UFEdtDMAConfig::validCmd> cmd unknown: "<<name<<", "<<c<<endl; 
  return -1;
}


#endif // __UFEdtDMAConfig_cc__
