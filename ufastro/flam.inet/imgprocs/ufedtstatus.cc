#if !defined(__ufedtstatus_cc__)
#define __ufedtstatus_cc__ "$Name:  $ $Id: ufedtstatus.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufedtstatus_cc__;

#include "UFEdtDMA.h"
__UFEdtDMA_H__(ufedtstatus_cc);

#include "UFPosixRuntime.h"
__UFPosixRuntime_H__(ufedtstatus_cc);

#include "math.h"

int main(int argc, char** argv) {
  // call some statics in UFEdtDMA::
  UFEdtDMA::Config cg, *cgp = UFEdtDMA::getConfig(&cg);
  if( cgp == 0  ) {
    clog<<"ufedtstatus> failed to find device."<<endl;
    return 0;
  }
  int cnt = 1;
  if( argc > 1 ) {
    int ac = abs(atoi(argv[1]));
    cnt = (ac > cnt) ? ac : cnt;
  }
  cout<<"ufedtstatus> config: "<<cg.w<<"x"<<cg.h<<"x"<<cg.d<<", frames/total: "<< UFEdtDMA::takeCnt()<<" / "
      <<UFEdtDMA::takeTotCnt()<<endl;
  while( --cnt > 0 ) { 
    cout<<"ufedtstatus> frame/total: "<< UFEdtDMA::takeCnt()<<" / "
        <<UFEdtDMA::takeTotCnt()<<endl;
    UFPosixRuntime::sleep(0.5);
  }
  return 0;
}

#endif // __ufedtstatus_cc__
