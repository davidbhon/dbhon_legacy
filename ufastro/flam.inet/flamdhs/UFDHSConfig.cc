#if !defined(__UFDHSConfig_cc__)
#define __UFDHSConfig_cc__ "$Name:  $ $Id: UFDHSConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDHSConfig_cc__;

#include "UFDHSConfig.h"
#include "UFFlamDHSAgent.h"
#include "UFFITSheader.h"

// ctors
UFDHSConfig::UFDHSConfig(const string& name) : UFDeviceConfig(name) {}

/*
UFDHSConfig::UFDHSConfig(const string& name,
			       const string& tshost,
			       int tsport) : UFDeviceConfig(name, tshost, tsport) {}
*/
// generate a status report
UFStrings* UFDHSConfig::status(UFDeviceAgent* da) {
  UFFlamDHSAgent* dhsa = dynamic_cast< UFFlamDHSAgent* > (da);
  vector < string > list;
  strstream s;
  string label = dhsa->_obscfg->datalabel();

  s<<da->name()<<", datalabel: "<<label;

  list.push_back(s.str()); delete s.str();
  return new UFStrings(da->name(), list);
}

UFStrings* UFDHSConfig::statusFITS(UFDeviceAgent* da) {
  UFFlamDHSAgent* dhsa = dynamic_cast< UFFlamDHSAgent* > (da);
  map < string, string > valhash, comments;
  string idle = "false";
  string applyingLut = "false";

  valhash["datalabl"] = dhsa->_obscfg->datalabel(); comments["datalabl"] = "DHS DataLabel is Observation ID";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

vector< string >& UFDHSConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  // DHS Bulk data cmd (for new data label impl == label)
  // use FIT status from all agents to set dataset primary header attributes (impl == ocs):
  _actions.push_back("dhsbd"); _actions.push_back("DhsBd"); _actions.push_back("DHSBD");
  // DHS control cmd?
  _actions.push_back("dhsctl"); _actions.push_back("DhsCtl"); _actions.push_back("DHSCTL");
  // FITS header
  _actions.push_back("fits"); _actions.push_back("Fits"); _actions.push_back("FITS");
  _actions.push_back("shutdown"); _actions.push_back("Shutdown"); _actions.push_back("SHUTDOWN");
  // status should indicate such things as datalabel(s), fram counts, etc.
  _actions.push_back("status"); _actions.push_back("Status"); _actions.push_back("STATUS  ");
  _actions.push_back("true"); _actions.push_back("True"); _actions.push_back("TRUE");

  return _actions;
}

vector< string >& UFDHSConfig::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("fits"); _queries.push_back("Fits"); _queries.push_back("FITS");
  _queries.push_back("dataset"); _queries.push_back("Dataset"); _queries.push_back("DATASET");
  _queries.push_back("stat"); _queries.push_back("Stat"); _queries.push_back("STAT");

  return _queries;
}

int UFDHSConfig::validCmd(const string& name) {
  if( name.length() <= 1 )
   return -1; // cmd name & impl. must be non empty & unique

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  int cnt = (int)av.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFDHSConfig::validCmd> av: "<<av[i]<<", name: "<<name<<endl;
    if( name.find(av[i]) != string::npos ) {
      return 0; 
    }
  }

  vector< string >& qv = queryCmds();
  cnt = (int)qv.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFDHSConfig::validCmd> qv: "<<qv[i]<<", name: "<<name<<endl;
    if( name.find(qv[i]) != string::npos )
      return 0;
  }
  
  clog<<"UFDHSConfig::validCmd> cmd name unknown: "<<name<<endl; 
  return -1;
}

int UFDHSConfig::validCmd(const string& name, const string& c) {
  if( name.length() <= 1 && c.length() <= 0 )
   return -1; // cmd name & impl. must be non empty & unique

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  int cnt = (int)av.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFDHSConfig::validCmd> av: "<<av[i]<<", name: "<<name<<", c: "<<c<<endl;
    if( name.find(av[i]) != string::npos ) {
      return 0; 
    }
    if( c.find(av[i]) != string::npos ) {
      return 0; 
    }
  }

  vector< string >& qv = queryCmds();
  cnt = (int)qv.size();
  for( int i=0; i < cnt; ++i ) {
    //if( UFDeviceAgent::_verbose )
    // clog<<"UFDHSConfig::validCmd> qv: "<<qv[i]<<", name: "<<name<<", c: "<<c<<endl;
    if( name.find(qv[i]) != string::npos )
      return 0;
    if( c.find(qv[i]) != string::npos )
      return 0;
  }
  
  clog<<"UFDHSConfig::validCmd> cmd unknown: "<<name<<", "<<c<<endl; 
  return -1;
}


#endif // __UFDHSConfig_cc__
