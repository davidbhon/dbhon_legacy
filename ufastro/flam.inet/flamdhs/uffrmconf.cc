#include "UFFrameConfig.h"

int main(int argc, char** argv) {
  UFFrameConfig ufc(320, 240);
  vector< string > names;
  names.push_back("Abuf"); names.push_back("Bbuf"); names.push_back("Cbuf"); names.push_back("Dbuf");
  int n = ufc.setUpdatedBuffNames(names);
  clog<<ufc.name()<<endl;
  n = ufc.getUpdatedBuffNames(names);
  for( int i = 0; i < n; ++i )
    clog<<names[i]<<endl;
  n = ufc.getUpdatedBuffNames("trecs", names);
  for( int i = 0; i < n; ++i )
    clog<<names[i]<<endl;

  return 0;
}
