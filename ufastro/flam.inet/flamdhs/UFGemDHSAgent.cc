#if !defined(__UFGemDHSAgent_cc__)
#define __UFGemDHSAgent_cc__ "$Name:  $ $Id: UFGemDHSAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemDHSAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "strings.h"

#include "UFGemDHSAgent.h"
#include "UFGemDHSConfig.h"
#include "UFImgHandler.h"
#include "UFgd.h"
#include "uffits.h"

// for use with external test card (since it transmits continously)
static float _exptime= 1.0; // test exposure time

// global statics
// make instrument name available to static funcs:
string UFGemDHSAgent::_Instrum;
pthread_mutex_t UFGemDHSAgent::_theFrmMutex;
pthread_t UFGemDHSAgent::_qlookThrdId= 0;
pthread_t UFGemDHSAgent::_archvThrdId= 0;
UFFrameConfig* UFGemDHSAgent::_theFrmConf= 0;
UFObsConfig* UFGemDHSAgent::_theObsConf= 0;

// abort discards, stop saves data.?
// take options stored as Argv:
UFRuntime::Argv UFGemDHSAgent::_acqArgs;

// pre/post dhs send activities
int UFGemDHSAgent::_qlcnt= 0;
int UFGemDHSAgent::_arcnt= 0;
bool UFGemDHSAgent::_ds9Disp= false;
bool UFGemDHSAgent::_jpeg= false;
bool UFGemDHSAgent:: _png= false;
int UFGemDHSAgent::_scale= 1;
UFDs9 UFGemDHSAgent::_ds9;

// buffer info. (one-time init., no mutex required)
int UFGemDHSAgent::_maxBuff= 3;
map < string, int > *UFGemDHSAgent::_nameIdxFrmBuff= 0; // index # of named frame buff.
// use theFrmMutex when accessing this:
//map < int, UFInts* > *UFGemDHSAgent::_dmaFrmBuff= 0; // each named buff is a UFInt object
deque < UFInts* > *UFGemDHSAgent::_dmaFrmBuff= 0; // each named buff is a UFInt object

// ctors:
UFGemDHSAgent::UFGemDHSAgent(const string& name, int argc,
			   char** argv, char** envp) : UFDeviceAgent(name, argc, argv, envp) {
  _IsThreaded = true;
  _Instrum = name;

  ::pthread_mutex_init(&_theFrmMutex, 0);
  ::pthread_mutex_init(&_theEdtMutex, 0);
  if( name.find("tr") != string::npos  || name.find("TR") != string::npos )
    _allocNamedBuffs("trecs");
  else if( name.find("ca") != string::npos || name.find("Ca") != string::npos )
    _allocNamedBuffs("canaricam");
  else
    _allocNamedBuffs("flam");
}

void UFGemDHSAgent::_sighandler(int sig) {
  switch(sig) {
  case SIGTERM:
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemDHSAgent::_sighandler> (SIGTERM) sig: "<<sig<<endl;
  case SIGINT: {
    if( UFDeviceAgent::_verbose )
       clog<<"UFGemDHSAgent::_sighandler> (SIGINT) sig: "<<sig<<endl;
    if( _dmaThrdId ) {
      ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
      _dmaThrdId = 0;
    }
    UFRndRobinServ::_shutdown = true; //UFRndRobinServ::shutdown();
    UFRndRobinServ::shutdown();
  }
  default:
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemDHSAgent::_sighandler> (default) sig: "<<sig<<endl;
    UFRndRobinServ::sighandlerDefault(sig); // handle SIGPIPE, SIGCHILD
    break;
  }
}

void UFGemDHSAgent::_cancelhandler(void* p) {
  UFGemDHSAgent* dma = static_cast< UFGemDHSAgent* > (p);
  // always unlock mutexes on cancellation of thread
  ::pthread_mutex_unlock(&dma->_theFrmMutex);
  ::pthread_mutex_unlock(&dma->_theEdtMutex);
}

void UFGemDHSAgent::_postacq(unsigned char* acqbuf, char* fitsbuf, int sz, const char* filenm) {


// static funcs:
int UFGemDHSAgent::main(const string& name, int argc, char** argv, char** envp) {
  UFGemDHSAgent ufdhsa(name, argc, argv, envp); // ctor binds to listen socket
  // enter event loop:
  ufdhsa.exec((void*)&ufdhsa);
  return 0;
}

// public virtual funcs:
void UFGemDHSAgent::startup() {
  // start signal handler
  //setSignalHandler(UFGemDHSAgent::_sighandler, _IsThreaded);
  sigWaitThread(UFGemDHSAgent::_sighandler);
  clog << "UFGemDHSAgent::startup> established signal handler (sigwait) thread."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  init();
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemDHSAgent::startup> listening on port= " <<listenport
       <<", with server soc: "<<_theServer.description()<<endl;
  return;  
}

UFTermServ* UFGemDHSAgent::init() {
  // save edtconfig to a file
  pid_t p = getpid();
  strstream s;
  s<<"/usr/tmp/.edtconf."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemDHSAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return 0;
  }
  delete s.str();

  int w= 0, h= 0;
  UFGemDHS::dimensions(w, h);
  strstream ss;
  ss<<name()<<"> Initial/current Edt dev. conf is: "<<w<<"x"<<h<<endl;
  char *edtstr = ss.str();
  int nc = ::write(fd, edtstr, strlen(edtstr));
  if( nc <= 0 ) {
    clog<<"UFGemDHSAgent> edt config log failed..."<<endl;
  }
  ::close(fd);
  return 0;
}

// this should always return the service/agent listen port #
int UFGemDHSAgent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  _config = new UFGemDHSConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = -1;
  _config->_tshost = "";

  string arg = findArg("-v");
  if( arg == "true" ) {
    UFDeviceAgent::_verbose = true;
  }
  arg = findArg("-vv");
  if( arg == "true" ) {
    UFGemDHS::_verbose = UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  }

  if( _config->_tsport >= 0 ) {
    port = 52001 + _config->_tsport - 7000;
  }
  else {
    port = 52001;
  }

  if( UFDaemon::threaded() )
    clog<<"UFGemDHSAgent::options> this service is threaded..."<<endl;
  clog<<"UFGemDHSAgent::options> set port to GemDHSAgent port "<<port<<endl;
  return port;
}

int UFGemDHSAgent::_setAcqArgs(UFDeviceAgent::CmdInfo* act) {
  _acqArgs.clear();
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    _acqArgs.push_back(act->cmd_name[i]); _acqArgs.push_back(act->cmd_impl[i]);
  }

  return (int)_acqArgs.size();
}

// the key virtual function(s) to override,
// presumably any allocated (UFStrings reply) memory is freed by the calling layer....
int UFGemDHSAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat = -1;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;
  string reply, simInstrum = name();
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<endl;

    int nr = _config->validCmd(cmdname, cmdimpl);
    if( nr < 0 ) {
      clog<<"UFGemDHSAgent::action> ?Bad cmd: "<<cmdname<<" :: "<<cmdimpl<<endl;
      continue;
    }
    act->time_submitted = currentTime();
    _active = act;
      // check if sim, don't create dma thread
    if( cmdname.find("sim") != string::npos ||
	cmdname.find("Sim") != string::npos ||
	cmdname.find("SIM") != string::npos) {
	_sim = true;
        if( cmdimpl.find("flam") != string::npos || cmdimpl.find("Flam") != string::npos || cmdimpl.find("FLAM") != string::npos ) 
	  simInstrum = "flam";
    }

    if( cmdname.find("shutdown") != string::npos || cmdname.find("Shutdown") != string::npos || 
	cmdname.find("SHUTDOWN") != string::npos ) {
      if( _dmaThrdId ) {
        ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dmaThrdId = 0;
      }
      UFRndRobinServ::_shutdown = true; //UFRndRobinServ::shutdown();
      reply = "Ok, shutting down: " + cmdname + "::" + cmdimpl;
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFGemDHSAgent::action> shutdown", &reply));
      break;
    }

    if( cmdname.find("acq") != string::npos || cmdname.find("Acq") != string::npos || 
	cmdname.find("ACQ") != string::npos ) {
      if( cmdimpl.find("abort") != string::npos || cmdimpl.find("Abort") != string::npos ||
	  cmdimpl.find("ABORT") != string::npos ) {
	// abort acquisition
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId )
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        stat = (int) _dmaThrdId = 0;
	reply = "Ok, aborted DMA: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::abort", &reply));
	break;
      } // acq::abort
      if( cmdimpl.find("stop") != string::npos || cmdimpl.find("Stop") != string::npos || 
	  cmdimpl.find("STOP") != string::npos ) {
	// get dma status cmdimpl == fits/other
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId ) 
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        stat = (int) _dmaThrdId = 0;
	reply = "Ok, stopped DMA: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::stop", &reply));
	break;
      } // acq::stop
      if( cmdimpl.find("start") != string::npos || cmdimpl.find("Start") != string::npos ||
	  cmdimpl.find("START") != string::npos ) {
        // start acquisition
        stat = _setAcqArgs(act);
        int w, h;
        bool idle = UFGemDHS::takeIdle(w, h);
        if( UFDeviceAgent::_verbose )
	  clog<<"UFGemDHSAgent::action> takeIdle: "<<idle<<"w, h = "<<w<<", "<<h<<endl;
        if( !idle ) { // 
          clog<<"UFGemDHSAgent::action> "<<cmdimpl<<" :: "<<cmdimpl<<" rejected, Waiting on Image"<<endl;
          strstream s;
          s<<cmdimpl<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
          reply = s.str(); delete s.str();
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::start", &reply));
	  break;
        }
        else if( _dmaThrdId > 0 ) { 
	  int m = ::pthread_mutex_trylock(&_theEdtMutex);
	  if( m != 0 && errno == EBUSY ) // edt dma in process, reject request
          clog<<"UFGemDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" rejected, dma (thread) still in progress"<<endl;
          strstream s;
          s<<cmdname<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
          reply = s.str(); delete s.str();
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::start", &reply));
	  break;
        }
        else if( _sim ) { // simulate edt dma acqs. 
	  stat = (int)_dmaThrdId = _simAcq(simInstrum, *this); 
	  reply = "Ok, started SIM thread for: " + cmdname + "::" + cmdimpl;
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::sim", &reply));
	  break;
        }
        else { // create & run dma thread
	  // dmaThread should lock and unlock the EdtMutex before exiting..
	  stat = (int)_dmaThrdId = newThread(_dmaThread, this);
	  reply = "Ok, started DMA thread for: " + cmdname + "::" + cmdimpl;
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemDHSAgent::action> acq::start", &reply));
	  break;
        }
      } // acq start
    } // end acq
    else if( cmdname.find("fram") != string::npos || cmdname.find("Fram") != string::npos ||
	     cmdname.find("FRAM") != string::npos ) { // fetch frame from cmdimpl == buffer name or index
      const char* c = cmdimpl.c_str();
      int frmidx = 0;
      if( isdigit(c[0]) ) // specify frame by index (index 0 should be most recent full image) or by name 
	frmidx = atoi(c);
      else if( cmdimpl == "flam:FullImage" || cmdimpl == "flam:1024Image" || cmdimpl == "flam:512Image")
	frmidx = (*_nameIdxFrmBuff)[cmdimpl];
      else if( cmdimpl == "trecs:src1" || cmdimpl == "trecs:ref1" || cmdimpl == "trecs:src2" || cmdimpl == "trecs:ref2" ||
	       cmdimpl == "trecs:dif1" || cmdimpl == "trecs:dif2" ||
	       cmdimpl == "trecs:accum(src1)" || cmdimpl == "trecs:accum(ref1)" || cmdimpl == "trecs:accum(dif1)" ||
	       cmdimpl == "trecs:accum(src2)" || cmdimpl == "trecs:accum(ref2)" || cmdimpl == "trecs:accum(dif2)" ||
	       cmdimpl == "trecs:sig"  || cmdimpl == "trecs:accum(sig)" )
	frmidx = (*_nameIdxFrmBuff)[cmdimpl];
      else
	frmidx = _maxBuff-1;
      if( frmidx >= _maxBuff ) frmidx = _maxBuff-1; 
      if( frmidx < 0 ) frmidx = 0;
      UFInts* frm = (*_dmaFrmBuff)[frmidx];
      int m = ::pthread_mutex_trylock(&_theFrmMutex); // fails if not owned by this thread
      if( m != 0 ) { // failed to get the lock
        reply = "UFGemDHSAgent::action> unable to access frame buffer, mutex still locked...";
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemDHSAgent::action> frame", &reply));
        break;
      }
      reply = "UFGemDHSAgent::action> sending " + frm->name();
      replies.push_back(new UFStrings("UFGemDHSAgent::action> frame", &reply));
      // make a deep copy to be sent back to client (and deleted by UFDeviceAgent::exec)
      UFInts* img = new UFInts(frm->name(), (int*)frm->valData(), frm->numVals());
      ::pthread_mutex_unlock(&_theFrmMutex);
      // send frame config, followed by image?
      int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
      UFFrameConfig* fc = new UFFrameConfig(width(), height());
      fc->setDMACnt(edtdone);
      fc->setFrameObsSeqTot(tot);
      fc->setImageCnt(cnt);
      fc->setFrameWriteCnt(cnt);
      replies.push_back(fc);
      img->setSeq(cnt, tot); // (ab)use the seq. cnt in header for frame info.
      replies.push_back(img);
      return (int) replies.size(); // reply with conf & image objects
    }
    else if( cmdname.find("conf") != string::npos ||
	     cmdname.find("Conf") != string::npos ||
	     cmdname.find("CONF") != string::npos ) { // fetch obs or frame config, cmdimpl == frm/obs
      bool all = (cmdimpl.find("all") != string::npos ||
	          cmdimpl.find("All") != string::npos || 
	          cmdimpl.find("ALL") != string::npos);
      if( all ) {
        int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
        UFFrameConfig* fc = new UFFrameConfig(width(), height());
        fc->setDMACnt(edtdone);
        fc->setFrameObsSeqTot(tot);
        fc->setImageCnt(cnt);
        fc->setFrameWriteCnt(cnt);
        UFObsConfig* oc = new UFObsConfig();
	reply = "UFGemDHSAgent::action> sending " + fc->name() + ", and " + oc->name();
	replies.push_back(new UFStrings("UFGemDHSAgent::action> frame", &reply));
        replies.push_back(fc);
        replies.push_back(oc);
      } // all configs
      else if( cmdimpl.find("frame") != string::npos ||
	       cmdimpl.find("Frame") != string::npos || 
	       cmdimpl.find("FRAME") != string::npos ) {
        int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
        UFFrameConfig* fc = new UFFrameConfig(width(), height());
        fc->setDMACnt(edtdone);
        fc->setFrameObsSeqTot(tot);
        fc->setImageCnt(cnt);
        fc->setFrameWriteCnt(cnt);
	reply = "UFGemDHSAgent::action> sending " + fc->name();
	replies.push_back(new UFStrings("UFGemDHSAgent::action> frame", &reply));
        replies.push_back(fc);
      } // frame config
      else if( cmdimpl.find("obs") != string::npos ||
	       cmdimpl.find("Obs") != string::npos || 
	       cmdimpl.find("OBS") != string::npos ) {
        UFObsConfig* oc = new UFObsConfig();
	reply = "UFGemDHSAgent::action> sending " + oc->name();
	replies.push_back(new UFStrings("UFGemDHSAgent::action> frame", &reply));
        replies.push_back(oc);
      } // obs config
      return (int) replies.size(); // reply with image object
    }
    else if( cmdname.find("stat") != string::npos || cmdname.find("Stat") != string::npos ||
	     cmdname.find("STAT") != string::npos ) { // get dma status cmdimpl == edt, fits/other
      if( cmdimpl.find("edt") != string::npos || cmdimpl.find("Edt") != string::npos ||
	  cmdimpl.find("EDT") != string::npos ||
	  cmdimpl.find("frm") != string::npos || cmdimpl.find("Frm") != string::npos ||
	  cmdimpl.find("FRM") != string::npos ) { // edt config (initcam) & take sem. counter vals
        reply = UFGemDHS::status();
	replies.push_back(new UFStrings("UFGemDHSAgent::action> stat::edt", &reply));
        stat = reply.length();
      }
      else if( cmdimpl.find("fits") != string::npos || cmdimpl.find("Fits") != string::npos ||
	       cmdimpl.find("FITS") != string::npos ) {
        act->cmd_reply.push_back(reply);
	replies.push_back(_config->statusFITS(this));
      }
      else {
        act->cmd_reply.push_back(reply);
        replies.push_back(_config->status(this));
      }
    }
    else if( cmdname.find("archv") != string::npos ||
	     cmdname.find("Archv") != string::npos ||
	     cmdname.find("ARCHV") != string::npos ) { // query (cmdimpl == '?') or set fits file pathname
      clog<<"UFGemDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" not supported."<<endl;
      strstream s;
      s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
      reply = s.str(); delete s.str();
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFGemDHSAgent::action> archv", &reply));
    }
    else if( cmdname.find("obs") != string::npos ||
	     cmdname.find("Obs") != string::npos ||
	     cmdname.find("OBS") != string::npos ) { // eh?
      // obsconf (transnmission from client) follows this obs start request
      clog<<"UFGemDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" not supported."<<endl;
      strstream s;
      s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
      reply = s.str(); delete s.str();
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFGemDHSAgent::action> obs", &reply));
    }
    else {
      clog<<"UFGemDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" not supported."<<endl;
      strstream s;
      s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
      reply = s.str(); delete s.str();
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFGemDHSAgent::action> unknown", &reply));
    }
  } // end for loop of cmd bundle

  if( stat < 0 )
    act->status_cmd = "failed";
  else if( act->cmd_reply.empty() )
    act->status_cmd = "failed/rejected";
  else
    act->status_cmd = "succeeded";
  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int) replies.size();
} 


int UFGemDHSAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemDHSAgent::dmaThread(UFGemDHSAgent& dma) {
  string usage = "[-v] [-nosem] [-stdout] [-index start index] [-cnt frmcnt] [-timeout sec.] [-lut [lutfile]] [-fits fitsfile] [-file hint] [-rename] [-lock lockfile] [-ds9] [-jpeg [scale]] [-png [scale]] [-frmconf]";

  // is agent in very verbose mode?
  string arg = dma.findArg("-vv");
  if( arg == "true" ) {
    UFGemDHS::_verbose = UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  }

  if( UFDeviceAgent::_verbose )
    for( int i = 0; i < (int)dma._acqArgs.size(); ++i )
      clog<<"UFGemDHSAgent::dmaThread> arg# "<<i<<" : "<<dma._acqArgs[i]<<endl;
 
  _exptime = 1.0;
  arg = UFRuntime::argVal("-exp", dma._acqArgs);
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemDHSAgent::dmaThread> exposure time:"<<_exptime<<endl;

  _ds9Disp = false;
  arg = UFRuntime::argVal("-ds9", dma._acqArgs);
  if( arg != "false" ) {
    if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> display to ds9..."<<endl;
    _ds9Disp = true;
  }

  _png = false;
  arg = UFRuntime::argVal("-png", dma._acqArgs);
  if( arg != "false" ) {
    _png = true;
    if( arg != "true" ) // scale png
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> write PNG (portable network graphics) file(s), _scale: "<<_scale<<endl;
  }

  _jpeg = false;
  arg = UFRuntime::argVal("-jpg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }
  arg = dma.UFRuntime::argVal("-jpeg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg requested
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }

  int cnt= 1, timeOut= 0, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0; 
  // this also examines acq args:
  UFGemDHS::init(dma._acqArgs, cnt, timeOut, lut, index, filehint, fits, fits_sz);
  if( fits == 0 ) {
    if( width() == 2048 ) {
      if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> set fits header to default for current EDT config.(FLAMINGOS)"<<endl;
      fits = (char*)fits2048;
      fits_sz = strlen(fits);
    }
    else {
      if( UFDeviceAgent::_verbose ) clog<<"UFGemDHSAgent::dmaThread> set fits header to default for current EDT config.(FLAM)"<<endl;
      fits = (char*)fits320x240;
      fits_sz = strlen(fits);
    }
  }
  UFGemDHS::setDpySeq(1); // call postacquisition function modula 1
  // enter frame acquisition/take loop:
  // postacq func. should lock the FrmMutex, copy the frame into the _dmeFrameBuff UFInt*
  // and setSeq(takeCnt(), takeTotal())... 
  dma._postcnt = 0; // clear the global counter
 
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemDHSAgent::dmaThread> Start DMA acq. of frames: "<<cnt<<", time: "<<UFRuntime::currentTime()<<endl;

  cnt = UFGemDHS::takeAndStore(cnt, index, filehint, lut, fits, fits_sz, _postacq);

  if( UFDeviceAgent::_verbose )
    clog<<"UFGemDHSAgent::dmaThread> Completed DMA acq. of frames: "<<cnt<<", time: "<<UFRuntime::currentTime()<<endl;

  return;
}

// allow qlook event loop to be run in its own (cancellable) thread:
void* UFGemDHSAgent::_qlookThread(void* p) {
  if( p == 0 )
    return p;

  UFGemDHSAgent* dhsa = static_cast< UFGemDHASgent* > (p);
   
  int oldstate, oldtype;
  ::pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
  ::pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
// since linux & solaris "pthread.h" declare pthread_cleanup_push as a macro
// that g++ seems to have problems compiling on each, use the actual
// extern function
#if defined(LINUX)
  struct _pthread_cleanup_buffer _buffer;
  ::_pthread_cleanup_push(&_buffer, UFGemDHSAgent::_cancelhandler, p);
#else
  _cleanup_t _cleanup_info;
  ::__pthread_cleanup_push(UFGemDHSAgent::_cancelhandler, p,
			   (caddr_t)_getfp(), &_cleanup_info);
#endif  
  // enter qlook event loop:
  dhsa->qlookThread(*dma);

  // clear the ThrdId
  dma->_qlookThrdId = 0;

  int exitstat;
  ::pthread_exit(&exitstat);
  return p;
}

// helpers
int UFGemDHSAgent::_allocNamedBuffs(const string& instrum) {
  if( _nameIdxFrmBuff == 0 )
    _nameIdxFrmBuff = new map < string, int >;
  if( _dmaFrmBuff == 0 )
    _dmaFrmBuff = new deque < UFInts* >;

  vector< string > names;
  _maxBuff = _buffNames(instrum, names);
  string bufname;
  int elem = width() * height(); // if failed, use defaults
  for( int i = 0; i<_maxBuff; ++i ) {
    const int* imgdata = (const int*) new int[elem];
    bufname = names[i];
    (*_nameIdxFrmBuff)[bufname] = i;
    (*_dmaFrmBuff)[i] = new UFInts(bufname, imgdata, elem); // shallow ctor
    clog<<"UFGemDHSAgent::_allocNamedBuffs> i, elem, name: "<<(*_nameIdxFrmBuff)[bufname]<<", "<<elem<<", "<<bufname<<endl;
  }
  return _maxBuff;  
}

int UFGemDHSAgent::_buffNames(const string& instrum, vector< string >& names) {
  names.clear();
  if( instrum.find("flam") != string::npos || instrum.find("Flam") || instrum.find("FLAM") ) {
    names.push_back(instrum+":FullImage");
    names.push_back(instrum+":1024Image");
    names.push_back(instrum+":512Image");
    return (int) names.size();
  }

  names.push_back(instrum+":src1");        // 0
  names.push_back(instrum+":ref1");        // 1
  names.push_back(instrum+":dif1");        // 2
  names.push_back(instrum+":src2");        // 3
  names.push_back(instrum+":ref2");        // 4
  names.push_back(instrum+":dif2");        // 5
  names.push_back(instrum+":sig");         // 6
  names.push_back(instrum+":accum(src1)"); // 7
  names.push_back(instrum+":accum(ref1)"); // 8
  names.push_back(instrum+":accum(src2)"); // 9
  names.push_back(instrum+":accum(ref2)"); // 10
  names.push_back(instrum+":accum(dif1)"); // 11
  names.push_back(instrum+":accum(dif2)"); // 12
  names.push_back(instrum+":accum(sig)");  // 13

  return (int) names.size();
}

int UFGemDHSAgent::_insertFrm(UFFrameConfig* fc, UFInts* frm) {
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemDHSAgent::_inserFrm..."<<endl;

  ++_qlcnt;
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFEdtDHSAgent::_postacq> acqbuf, fitsbuf: "<< (int)acqbuf <<", "<< (int)fitsbuf<<", sz: "<<sz<<endl;
  if( _ds9Disp && (filenm != 0 || fitsbuf != 0) ) {
    if( UFDeviceAgent::_verbose )
      if( filenm != 0 )
        clog<<"UFEdtDMAgent::_postacq> DS9 fits:"<<filenm<<endl;
      else 
        clog<<"UFEdtDMAgent::_postacq> DS9 fits, no filename specified"<<endl;
    //int stat = _ds9.display(fitsbuf, sz);
    int stat = -1;
    if( filenm != 0 )
      stat  = _ds9.display(filenm);
    else if( fitsbuf != 0 && sz > 0 )
      stat  = _ds9.display(fitsbuf, sz);
    else
      clog<<"UFEdtDMAgent::_postacq> DS9 no fits data?"<<endl;

    if( stat < 0 )
      clog<<"_postacq> Ds9 display failed?"<<endl;
  }

  // insert acquired frame into frame buffer history
  _insertFrm((int*) acqbuf);

  // and while ds9 displays it, and the user gazes fondly at it,
  // write jpeg and/or png to disk:
  if( _jpeg || _png ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_postacq> _jpeg, _png: "<<_jpeg<<", "<<_png<<endl;
    _writeImg(fitsbuf, sz, filenm, _png, _jpeg);
  }
  if( UFDaemon::threaded() ) {
    clog<<"UFEdtDMAgent::_postacq> dma thread will sleep for "<<_exptime<<" sec..."<<endl;
    UFPosixRuntime::sleep(_exptime); // allow other threads some cpu
  }

  int w = width(), h = height(); 

  // flamingos frames go into 3 buffers:
  if( (w > 1024 && h > 1024 ) ||
      (_Instrum.find("fl") != string::npos || _Instrum.find("Fl") != string::npos || _Instrum.find("FL") != string::npos) ) {

    UFInts* ufi = (*_dmaFrmBuff)[0]; // full image
    int elem = ufi->elements();
    int* img0 = (int*) ufi->valData();

    ufi = (*_dmaFrmBuff)[1]; // 1/4 image
    int* img1024 = (int*) ufi->valData();

    ufi = (*_dmaFrmBuff)[1]; // 1/16 image
    int* img512 = (int*) ufi->valData();

    // lock the mutex and copy image frame:
    int m = ::pthread_mutex_trylock(&_theFrmMutex); // fails if not owned by this thread
    if( m != 0 ) { // failed to get the lock
      clog<<"UFGemDHSAgent::_insertFrm> unable to access frame buffer, mutex still locked..."<<endl;
      return 0;
    }
    clog<<"UFGemDHSAgent::_insertFrm> write frame buffer, mutex locked, copy new frame elem: "<<elem<<" ..."<<endl;

    // replace old (full) image with new:
    for( int i = 0; i < elem; ++i )
      img0[i] = acqbuf[i];

    m = ::pthread_mutex_unlock(&_theFrmMutex);
    clog<<"UFGemDHSAgent::_insertFrm> mutex unlocked."<<endl;

    // 2x2 avg:
    int w2 = w/2, h2 = h/2;
    for( int i = 0; i < w2; ++i ) {
      int k = i*h2;
      for( int j = 0; j < h2; ++j ) {
	k += j;
        int kk0 = i*w + 2*j, kk1 = (1+1)*w + 2*j;
	img1024[k] = img0[kk0] + img0[kk0 + 1] + img0[kk1] + img0[kk1 + 1]; 
      }
    }

    // 4x4 avg:        
    int w4 = w2/2, h4 = h2/2;
    for( int i = 0; i < w4; ++i ) {
      int k = i*h4;
      for( int j = 0; j < h4; ++j ) {
	k += j;
        int kk0 = i*w2 + 2*j, kk1 = (1+1)*w2 + 2*j;
	img512[k] = img1024[kk0] + img1024[kk0 + 1] + img1024[kk1] + img1024[kk1 + 1]; 
      }
    }
    return elem;
  }

  // trecs frames go into 14 buffers:
  int bufIdx = bufIdFrmObsSeq(_postcnt-1);
  UFInts* ufi = (*_dmaFrmBuff)[bufIdx]; // full image
  int elem = ufi->elements();
  int* img = (int*) ufi->valData();

  for( int i = 0; i < elem; ++i )
    img[i] = acqbuf[i];

  return 0;
}

int UFGemDHSAgent::bufIdFrmObsSeq(int frmIdx) { // stub
  return frmIdx % 14;
}

// simulation mode
pthread_t UFGemDHSAgent::_simAcq(const string& instrum, UFGemDHSAgent& dma) {
  ++_postcnt;
  if( instrum.find("fl") != string::npos || instrum.find("Fl") != string::npos || instrum.find("FL") != string::npos ) {
    // assume flamingos
    UFInts* ufi = (*dma._dmaFrmBuff)[0];
    int elem = ufi->elements();
    int* simimg0 = (int*) ufi->valData();
    ufi = (*dma._dmaFrmBuff)[1];
    int* simimg1 = (int*) ufi->valData();
    for( int i = 0; i < elem; ++i ) {
      simimg1[i] = simimg0[i];
      simimg0[i] = _postcnt;
    }
    return (pthread_t) 1;
  }

  // FLAM
  return (pthread_t) 1;
}

void UFGemDHSAgent::_writeImg(char* fits, int sz, const char* filenm, bool png, bool jpeg) {
  if( _scale == 0 ) _scale = 1; // compiler/thread bug?
  UFImgHandler::_scale = _scale;
  UFImgHandler::writePngJpeg(fits, sz, filenm, png, jpeg);
}

#endif // UFGemDHSAgent
