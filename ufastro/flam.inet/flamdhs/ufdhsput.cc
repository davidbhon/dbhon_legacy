#if !defined(__ufdhsput_cc__)
#define __ufdhsput_cc__ "$Name:  $ $Id: ufdhsput.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFRuntime.h"
#include "UFSocket.h"
#include "UFFITSClient.h"
#include "UFFITSheader.h"
#include "UFFrameClient.h"
/*
#include "UFObsConfig.h"
#include "UFFrameConfig.h"
*/
#include "UFFlamObsConf.h"
#include "UFSADFITS.h" // new feature -- get fits from epics sad
#include "UFDHSFlam.h"

#define UFMAXTRY 3

// DHS data server name at gemini south summit:
//static string _dataserver = "dataServerSS"; // gemini south
static string _dataserver = "dataServerNS"; // gemini north
static bool _noext= false;
static bool _noprm= false;
static bool _verbose= false;
static float _sleep = 0.5;
static int _loop= 0; // _loop <= 0 means forever
static int _w=0, _h=0, _nods=0;
static string _agenthost, _frmhost;
static string _quicklook, _archv;
static string _epics;
static UFStrings *_fits0Hdr= 0, *_fitsHdr = 0;
static UFFlamObsConf* _obscfg= 0;
//static UFFrameConfig* _frmcfg= 0;
static UFSocket::ConnectTable _connections;
static bool _frmstart= false;
// or to frame replication server (ufedtd):
static int _frmport = 52001;
static bool _reconnect= false;
static int _agentcons = 0;

// estimated adjustment to start time of frame:
float _estadjtime= 0.0;

//static bool _newlabelreq= false;
static int _fifo= -1;
static string _fifoname= "/tmp/.ufdhslabel";
static int _totobs= 0;
static int _totimg= 0;
static bool _shutdown= false;
static UFDHSFlam* _dhs= 0;

static void shutdown() {
  clog<<"Ufdhsput::shutdown> UFDHSFlam::_archvThrdCnt= "<<UFDHSFlam::_archvThrdCnt<<endl;
  while( UFDHSFlam::_archvThrdCnt > 0 ) {
    UFPosixRuntime::sleep(0.5); // allow dhs threads cpu time
  }
  clog<<"ufdhsput::shutdown> All observating completed or aborted, total images processed: "<<_totimg
      <<", total number of observations: "<<_totobs<<endl;
  // should there be any final dhswait here?
  if( _dhs ) {
    clog<<"ufdhsput::shutdown> closing DHS connection."<<endl;
    _dhs->close();
  }
  ::remove(_fifoname.c_str()); 

  ::exit(0);
}

static void newdhslabel() {
  // get a new datalabel and write it to fifo (named pipe), which has presumably been opened for read:
  string label = (*_dhs).newlabel();
  _fifo = ::open(_fifoname.c_str(), O_WRONLY); // this will block if no other process or thread has opened fifo for read?
  if( _fifo < 0 ) {
    clog<<"ufdhsput> failed to open fifo: "<<_fifoname<<endl;
  }
  else {
    int nb = ::write(_fifo, label.c_str(), label.length());
    if( nb < (int)label.length() ) 
      clog<<"ufdhsput> failed to write label: "<<label<<" to fifo: "<<_fifoname<<endl;
    else 
      //clog<<"ufdhsput> wrote label: "<<label<<" to fifo: "<<_fifoname<<endl;
    ::close(_fifo);
  }
}

static void _sigHandler(int signum) {
  //clog<<"ufdhsput::_sigHandler> caught signal "<<signum<<endl;
  switch(signum) {
    case SIGABRT: clog<<"ufdhsput> caught signal "<<signum<<" SIGABRT"<<endl;
                 shutdown(); break;
    case SIGINT: clog<<"ufdhsput> caught signal "<<signum<<" SIGINT"<<endl;
                 shutdown(); break;
    case SIGTERM: clog<<"ufdhsput> caught signal "<<signum<<" SIGTERM"<<endl;
                  shutdown(); break;
    case SIGPIPE: clog<<"ufdhsput> caught signal "<<signum<<" SIGPIPE"<<endl;
                 _reconnect = true; break;
    case SIGUSR1: clog<<"ufdhsput> caught signal "<<signum<<" SIGUSR1 -- new dhslabel req..."<<endl;
                  newdhslabel(); break;
    case SIGUSR2: clog<<"ufdhsput> caught signal "<<signum<<" SIGUSR2-- new dhslabel req..."<<endl;
                  newdhslabel(); break;
    case SIGALRM: clog<<"ufdhsput> caught signal "<<signum<<" SIGALRM"<<endl; break;
    case SIGCHLD: clog<<"ufdhsput> caught signal "<<signum<<" SIGCHLD"<<endl; break;
    case SIGCONT: clog<<"ufdhsput> caught signal "<<signum<<" SIGCONT"<<endl; break;
    case SIGFPE: clog<<"ufdhsput> caught signal "<<signum<<" SIGFPE"<<endl; break;
    case SIGPWR: clog<<"ufdhsput> caught signal "<<signum<<" SIGPWR"<<endl; break;
    case SIGURG: clog<<"ufdhsput> caught signal "<<signum<<" SIGURG"<<endl; break;
    case SIGVTALRM: clog<<"ufdhsput> caught signal "<<signum<<" SIGVTALRM"<<endl; break;
    case SIGXFSZ: clog<<"ufdhsput> caught signal "<<signum<<" SIGXFSZ"<<endl; break;
    default: clog<<"ufdhsput::_sigHandler> ignoring signal "<<signum<<endl; break;
  }
}

static UFStrings* fetchOHdr(UFDHSFlam& dhs, UFFITSClient& ufits) {
  // get current header, for start of observation
  int trycnt = UFMAXTRY;
  do {
    _fits0Hdr = ufits.fetchAllFITS(dhs._connections, UFDHSFlam::_fitstimeout, dhs._observatory);
    if( _fits0Hdr == 0 ) UFPosixRuntime::sleep(0.5);
  } while( _fits0Hdr == 0 && --trycnt > 0 );
  if( _fits0Hdr == 0 ) {
    clog<<"ufdhsput> failed to get any FITS info from agents."<<endl;
    return 0;
  }
  return _fits0Hdr;
}
  
static int connectAgents(UFDHSFlam& dhs, UFFITSClient& ufits) {
  // connect to agents
  UFFITSClient::AgentLoc loc;
  int aidx = ufits.locateAgents(_agenthost, loc);
  if( aidx <= 0 ) {
    clog<<"ufdhsput> no agents located"<<endl;
    return -1;
  }
  int ncon= 0, maxtry= 2;
  while( ncon <= 0 && maxtry-- > 0 ) {
    ncon = ufits.connectAgents(loc, dhs._connections, -1, false);
    if( ncon <= 0 ) {
      if( _verbose )
        clog<<"ufdhsput> no connections yet, sleep & retry..."<<endl;
      ufits.sleep(1.0);
    }
  }

  _agentcons = ncon; 
  clog<<"ufdhsput> _agentcons: "<<_agentcons<<endl;
  delete _fits0Hdr; _fits0Hdr = 0;

  /*
  UFDHSFlam::_ufits = &ufits;
  // get current header, for start of observation
  _fits0Hdr = fetchOHdr(dhs, ufits);
  if( _fits0Hdr == 0 ) // failed to get fits
    return -1;
  */

  return aidx;
}

int main(int argc, char** argv, char** envp) {
  // unit test of single dhs connection client
  // use this UFDaemon class for argv stuff

  UFFITSClient ufits("ufdhsput", argc, argv, envp);

  string arg = ufits.findArg("-v");
  if( arg == "true" ) { 
    _verbose = true;
    UFDHSwrap::_verbose = true;
  }

  arg = ufits.findArg("-vv");
  if( arg == "true" ) { 
    _verbose = true;
    UFDHSwrap::_verbose = true;
    UFFITSheader::_verbose = true;
  }

  _epics = "flam";
  arg = ufits.findArg("-epics");
  if( arg != "false" && arg != "true" ) { // override default epics db name:
    _epics = arg;
  }

  arg = ufits.findArg("-noepics"); // no epics (i.e. do not fetch FITS header from epics SAD
  if( arg != "false" ) { 
    _epics = "";
  }

  arg = ufits.findArg("-noqlook");
  if( arg != "false" )
    UFDHSFlam::_qlook = false;

  arg = ufits.findArg("-fitstimeout");
  if( arg != "true" && arg != "false" )
    UFDHSFlam::_fitstimeout = atof(arg.c_str());

  arg = ufits.findArg("-nodtimeout");
  if( arg != "true" && arg != "false" )
    UFDHSFlam::_timeout = UFFrameClient::_timeout = atof(arg.c_str());

  arg = ufits.findArg("-nowait");
  if( arg == "true" )
    UFDHSwrap::_wait = false;

  arg = ufits.findArg("-noext");
  if( arg == "true" ) { // no extension headers
    _noext = true;
  }
  arg = ufits.findArg("-noprm");
  if( arg == "true" ) { // no primary header
    _noprm = true;
  }

  arg = ufits.findArg("-dd");
  if( arg == "true" ) { // write data dictionay to stdout in libdd.config format
    UFDHSwrap::_writeDD = true;
  }

  arg = ufits.findArg("-nods");
  if( arg != "true" && arg != "false" ) {
    _frmstart = true;
    _nods = atoi(arg.c_str());
  }

  arg = ufits.findArg("-loop");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufits.findArg("-l");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufits.findArg("-cnt");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufits.findArg("-doobs");
  if( arg == "true" ) // bother with observatory stuff?
    UFDHSFlam::_observatory = true;

  arg = ufits.findArg("-noobs");
  if( arg == "true" ) // bother with observatory stuff?
    UFDHSFlam::_observatory = false;

  arg = ufits.findArg("-noskip");
  if( arg == "true" ) 
    UFDHSwrap::_skip = false;

  _agenthost = UFRuntime::hostname();
  arg = ufits.findArg("-host");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = ufits.findArg("-agents");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = ufits.findArg("-agent");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  _frmhost = UFRuntime::hostname();
  arg = ufits.findArg("-frmhost");
  if( arg != "true" && arg != "false" )
    _frmhost = arg;

  arg = ufits.findArg("-q");
  if( arg != "false" ) {
    _quicklook = "all";
    if( arg != "true" )
    _quicklook = "trecs:" + arg;
  }

  arg = ufits.findArg("-qlook");
  if( arg != "false" ) {
    UFDHSFlam::_qlook = true;
    _quicklook = "all";
    if( arg != "true" )
    _quicklook = "trecs:" + arg;
  }

  arg = ufits.findArg("-sleep");
  if( arg != "false" ) {
    if( arg != "true" )
      _sleep = atof(arg.c_str());
  }
  arg = ufits.findArg("-s");
  if( arg != "false" ) {
    if( arg != "true" )
      _sleep = atof(arg.c_str());
  }

  arg = ufits.findArg("-fifo");
  if( arg != "false" ) {
    if( arg != "true" )
      _fifoname = arg;
    else
      _fifoname = "/tmp/.ufdhslabel";
  }
  _fifo = UFPosixRuntime::fifoCreate(_fifoname);
  if( _fifo < 0 ) {
    clog<<"ufdhsput> failed to create fifo: "<<_fifoname<<endl;
    return _fifo;
  }

  // start sighandler
  UFPosixRuntime::sigWaitThread(_sigHandler);

  // total number of (multi-dim.) frames/extentions in header == (_nods+1)*SciMode or EngMode
  int _total = _loop;
  // get a fits header from a file (rather than from agents?)
  string _file = "";
  FILE* _fsfits= 0;;
  arg = ufits.findArg("-file");
  if( arg != "false" ) {
    if( arg == "true" ) { // use stdin
      _fsfits = stdin;
      _file = "stdin";
    }
    else {
      _file = arg;
      _fsfits = ::fopen(_file.c_str(), "r");
      if( _fsfits == 0 ) {
	clog<<"ufdhsput> failed to open file: "<<_file<<endl;
	return -1;
      }
      _fitsHdr = _fits0Hdr = UFFITSheader::readPrmHdr(_fsfits, _total, _nods);
    }
  }
  if( _loop < _total) _loop = _total; // file header overrides any cmd-line opts
 
  _w= 2048; _h= 2048;
  arg = ufits.findArg("-w");
  if( arg != "false" && arg != "true" )
    _w = atoi(arg.c_str());

  arg = ufits.findArg("-h");
  if( arg != "false" && arg != "true" )
    _h = atoi(arg.c_str());

  // connect to the dhs:
  arg = ufits.findArg("-ns");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNS";

  arg = ufits.findArg("-nb");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNB";
 
  arg = ufits.findArg("-gs");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerSS";
 
  arg = ufits.findArg("-gb");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerSB";
 
  string clhost = UFRuntime::hostname();
  string clhostIP = UFSocket::ipAddrOf(clhost);
  string _dhshost = clhostIP;
  arg = ufits.findArg("-dhs");
  if( arg != "false" && arg != "true" ) {
    //_dhshost = arg;
    _dhshost = UFSocket::ipAddrOf(arg);
    if( arg.find("reggie") != string::npos ) // force dataserver name:
      _dataserver = "dataServerSS";
    if( arg.find("veronica") != string::npos ) // force dataserver name:
      _dataserver = "dataServerSB";
    if( arg.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( arg.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
    if( arg.find("flam") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  if( _dhshost == clhostIP ) {
    if( clhost.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( clhost.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
    if( clhost.find("flam") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  int fc= -1;
  clog<<"ufdhsput> (blocking) connect to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;
  UFFrameClient ufrm(_frmhost, _frmport, _w, _h);
  if( _frmstart ) { // unit test:
    fc = ufrm.replConnectAndStartObs(*_obscfg, _frmhost, _frmport);
  }
  else {
    fc = ufrm.replConnect(_frmhost, _frmport);
  }

  if( fc <= 0 )
    clog<<"ufdhsput> unable to connect to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;
  else 
    clog<<"ufdhsput> connected to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;

  UFDHSFlam dhs(_w, _h); // provides connection table too
  //clog<<"ufdhsput> connect to DHS: "<<_dhshost<<endl;
  DHS_STATUS dstat = dhs.open(_dhshost, _dataserver);
  if( dstat != DHS_S_SUCCESS ) {
    clog<<"ufdhsput> failed to connect to DHS: "<<_dhshost<<endl;
    return -1;
  }
  //if( _verbose )
    clog<<"ufdhsput> connected to DHS: "<<_dhshost<<endl;

  // set the global dhs object ptr
  _dhs = &dhs; 

  // either the detector agent or the frame service should send down
  // a new observation configuration upon the start of a new obs.
  // for testing, we can send the ufqcqframed an obsconfig ... 
  // for testing multiple observations, use ufnewdhslabel -frmstart ...
  /*
  if( !_obscfg ) { // once we have all the parms for the ctor:
    string label = dhs.newlabel();
    _obscfg = new UFFlamObsConf(1.0, label); // defaults to startObs
    //_obscfg->startObs(); // set 'start' directive in name field
    dhs.setObs(_obscfg);
  }
  */

  vector< string > qlnames;
  int nstrms= 0; 
  if( _quicklook == "all" ) {
    nstrms = _obscfg->getBufNames(qlnames);
  }
  else if( _quicklook != "" || _quicklook != "none" || _quicklook != "false" ) {
    qlnames.push_back(_quicklook);
  }
  UFDHSwrap::setQlNames("Flam", qlnames); 
  // infinit loop should create obsdataque and obsthread for each new
  // UFObdConfig that is sent either from frame service or from dc agent...
  // test one observation:
  // if no observation datalabel is provided, set to ""
  // and generate label via dhs runtime...
  int idx= 0, total=0, qlength = 0;
  UFInts* data = 0;
  UFProtocol* ufp = 0;
  UFDHSFlam::ObsDataQue* dataque = 0;
  int obsimgcnt= 0, finalcnt= -1;
  UFSADFITS* _sadfits= 0;
  if( !_epics.empty() ) {
    clog<<"ufdhsput> EPICS SAD FITS search/connect "<<_epics<<", time: "<<UFRuntime::currentTime()<<endl;
    _sadfits = new UFSADFITS(_epics);
    std::map< string, string > sad;
    _fits0Hdr = _sadfits->fitsHeader(sad, _epics);
    _fits0Hdr->relabel("DHSInitFITS");
    clog<<"ufdhsput> EPICS SAD FITS "<<_epics<<" connections completed: "<<_fits0Hdr->elements()
        <<", time: "<<UFRuntime::currentTime()<<endl;
  }
  else { // use agent fits  connection if ecpis sad not available
    int fitsc = connectAgents(dhs, ufits);
    if( fitsc < 0 ) clog<<"ufdhsput> failed FITS connection to agents..."<<endl;
  }

  string dhslabel("N/A"), datalabel("N/A");
  clog<<"ufdhsput> entering observation event loop..."<<endl;
  while( true ) {
    if( _shutdown ) {
      clog<<"ufdhsput> shutdown..."<<endl;
      break;
    }
    if( fc <= 0 || _reconnect && !ufrm.validConnection() ) {
       clog<<"ufdhsput> no frame service connection (connection closed by server?), (re)connect to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;
      if( _frmstart ) // connect and start obs (unit test)
        fc = ufrm.replConnectAndStartObs(*_obscfg, _frmhost, _frmport);
      else // connect to the frame replication service, don't start obs:
        fc = ufrm.replConnect(_frmhost, _frmport);

      if( fc <= 0 ) {
        clog<<"ufdhsput> unable to (re)connect to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;
        sleep(2);
        continue;
      }
      else {
        _reconnect = false; // reset from sigpipe (server side closed down, or invalid socket)
        clog<<"ufdhsput> (re)connected to Frame Server: "<<_frmhost<<", port: "<<_frmport<<endl;
      }
    }
    /*
    if( ufrm.available() <= 0 ) {
      UFRuntime::mlsleep(0.1); // allow dhs threads cpu time
      continue;
    }
     */
    clog<<"ufdhsput> current/most recent datalabel: "<<dhslabel<<", obsimgcnt: "<<obsimgcnt<<" out of: "<<finalcnt<<endl;
    clog<<"ufdhsput> waiting for observation config/notice/frame data from EDT daemon..."<<endl;
    ufp = ufrm.replFrame(idx, total); // recv replicated frame, incr. idx
    if( ufp == 0 ) {
      clog<<"ufdhsput> got null data object, closing connection to frame server..."<<endl;
      ufrm.close();
      _reconnect = true;
      continue;
    }
    else {
      clog<<"ufdhsput> ufprotocol obj. type: "<<ufp->typeId()<<", name: "<<ufp->name()<<endl;
      //UFProtocol::printHeader(ufp->attributesPtr());
    }
    if( ufp->isFITS() ) {
      clog<<"ufdhsput> recvd UFStrings FITS header: "<<ufp->name()<<", for datalabel: "<<ufp->datalabel()<<endl;
      if( dataque )
        qlength = dataque->push(ufp);
      else
	clog<<"ufdhsput> dataque is NULL !!!"<<endl;
      continue;
    }
    if( ufp->isNotice()  ) { // notification (only stop/abort is currently anticipated)
      string notice = ufp->name(); 
      clog<<"ufdhsput> notice: "<<notice<<", for datalabel: "<<ufp->datalabel()<<endl;
      UFStrings::lowerCase(notice);
      if( notice.find("shutdown") != string::npos ) {
	_shutdown = true;
	notice = "stop";
        clog<<"ufdhsput> shutdown indicated, treate it as stop? ..."<<endl;
      }
      UFInts* finalframe = 0;
      if( notice.find("stop") != string::npos && dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> Stopping datastreams for datalabel: "<<ufp->datalabel()<<endl;
        finalframe = new UFInts(notice, false);
      }
      if( notice.find("abort") != string::npos && dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> Aborting datastreams for datalabel: "<<ufp->datalabel()<<endl;
        finalframe = new UFInts(notice, false);
      }
      if( finalframe ) {
        qlength = dataque->push(finalframe); // thread should clean up after itself (free dataque, etc.)
        dataque = 0; // dataque thread will free que and exit on null or empty frame...
      }
      continue;
    }
    // additional behavior -- recv'd obsconfig name may include stop/abort notice
    if( ufp->isObsConf() ) { // new observation, should be first protocol object received...
      string notice = ufp->name(); UFRuntime::upperCase(notice);
      UFInts* finalframe = 0;
      if( notice.find("ABORT") != string::npos && dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> Aborting datastreams for datalabel: "<<ufp->datalabel()<<endl;
        finalframe = new UFInts(notice, false);
      }
      if( notice.find("STOP") != string::npos && dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> Stopping datastreams for datalabel: "<<ufp->datalabel()<<endl;
        finalframe = new UFInts(notice, false);
      }
      if( finalframe ) {
        qlength = dataque->push(finalframe); // thread should clean up after itself (free dataque, etc.)
        dataque = 0; // dataque thread will free queue and exit on null or empty frame...
        continue;
      }
      _obscfg = dynamic_cast< UFFlamObsConf* > (ufp);
      _estadjtime = 0.0; _estadjtime -= _obscfg->estXferTime();
      dhslabel = datalabel = _obscfg->datalabel();
      clog<<"ufdhsput> new observation datalabel: "<<datalabel<<endl;
      clog<<"ufdhsput> savePeriod + estXferTime: "<<_estadjtime<<endl;
      if( datalabel.find("discard" ) != string::npos || datalabel.find("Discard" ) != string::npos ||
	  datalabel.find("DISCARD" ) != string::npos ) { // ignore this observation...
	clog<<"ufdhsput> new UFFlamObsConf indicates DISCARD, all data for this observation will be ignored..."<<endl;
      }
      else if( datalabel.find("N2") != 0 && datalabel.find("S2") != 0 ) { 
        // force creation of new datalabel in dhs
	clog<<"ufdhsput> invalid datalabel provided: "<<datalabel<<endl;
	dhslabel = UFDHSwrap::newlabel();
	clog<<"ufdhsput> set it via DHS newlabel request:"<<dhslabel<<endl;
        _obscfg->relabel(dhslabel);
      }
      int imgcnt = _obscfg->framesPerNod(); // per dhs frame 'extension'
      //  unlike trecs, f2 mefs contain an extension for each image/frame:
      int extcnt = imgcnt * ( 1 + _obscfg->nodsPerObs() );
      finalcnt = extcnt;
      datalabel = _obscfg->datalabel();
      clog<<"ufdhsput> starting new obs. with datalabel: \""<<datalabel<<", imgcnt= "<<imgcnt
      <<", extcnt= "<<extcnt<<", expect finalcnt= "<<finalcnt<<" images for this obs."<<endl;

      if( _sadfits != 0 ) {
        std::map< string, string > sad;
        clog<<"ufdhsput> fetch FITS from EPICS SAD, time: "<<UFRuntime::currentTime()<<endl;
	_fits0Hdr = _sadfits->fitsHeader(sad, _epics);
	_fits0Hdr->relabel(datalabel);
        clog<<"ufdhsput> fetched FITS, time: "<<UFRuntime::currentTime()<<endl;
      }
      /*
      else if( _fits0Hdr == 0 && _agentcons == 0 ) { // defered FITS connection
        // use this connection if all else fails...
        int fitsc = connectAgents(dhs, ufits);
        if( fitsc < 0 ) clog<<"ufdhsput> failed FITS connection to agents..."<<endl;
      }
      */
      if( _fits0Hdr == 0 ) {
        clog<<"ufdhsput> fetch FITS from Device Agents, time: "<<UFRuntime::currentTime()<<endl;
	_fits0Hdr = fetchOHdr(dhs, ufits); // via agents
        clog<<"ufdhsput> fetched FITS, time: "<<UFRuntime::currentTime()<<endl;
      }
      if( dataque ) {
        clog<<"ufdhsput> aborted previous observation?"<<endl;
        // null data indicates termination of (prior) observation..
        qlength = dataque->push(0); // thread should clean up after itself (free dataque, etc.)
      }
      obsimgcnt = 0; ++_totobs;
      dataque = new (nothrow) UFDHSFlam::ObsDataQue(_dhs, _obscfg); // create new dataque for obs...
      if( dataque ==  0 ) {
        clog<<"ufdhsput> unable to allocate new dataque for putAllStreams ..."<<endl;
        return -1;
      }
      pthread_t obsthrd = UFPosixRuntime::newThread(UFDHSFlam::_putAllStreams, (void*) dataque);
      if( obsthrd ==  0 ) {
        clog<<"ufdhsput> unable to start new thread for putAllStreams ..."<<endl;
        delete dataque;
        return -1;
      }
      clog<<"ufdhsput> started new thread with dataque for datalabel: "<<_obscfg->name()<<endl;
      continue;
    }
    // FlamObsConf includes frameconf, so this is no longer needed:
    /*
    if( ufp->isFrmConf() ) { // should follow or precede obs conf.
      clog<<"ufdhsput> (notification) new data available: "<<ufp->timeStamp()<<" -- "<<ufp->name()<<endl;
      delete _frmcfg;
      _frmcfg = dynamic_cast< UFFrameConfig* > (ufp);
      _estadjtime -= _frmcfg->savePeriod();
      //clog<<"ufdhsput> _estadjtime "<<_estadjtime<<endl;
      }
      continue;
    }
    */
    if( ufp->isData() && dataque ) {
      data = dynamic_cast< UFInts* > (ufp);
      //if( _verbose )
        clog<<"ufdhsput> new data time: "<<data->timeStamp()<<endl;
      data->adjTime(_estadjtime);
      ++obsimgcnt; ++_totimg;
      //if( _verbose )
	clog<<"ufdhsput> "<<idx<<", adjusted time: "<<data->timeStamp()<<", _estadjtime: "<<_estadjtime<<endl;
	clog<<"ufdhsput> obsimgcnt: "<<obsimgcnt<<", finalcnt: "<<finalcnt<<endl;
      if( obsimgcnt == 1 ) { // put the primary header in the queue immediately
	dataque->push(_fits0Hdr);
      //if( _verbose )
        clog<<"ufdhsput> inserted primary fits header: "<<_fits0Hdr->name()<<" into dataque idx: "<<idx<<", qlength: "<<qlength<<endl;
      }
      if( obsimgcnt == finalcnt ) { // push final fits (primary) header onto dataque before data...
        clog<<"ufdhsput> final frame/image: "<<data->name()<<", qlength: "<<qlength<<endl;
        if( _sadfits != 0 ) {
          std::map< string, string > sad;
          clog<<"ufdhsput> fetch FITS from EPICS SAD, time: "<<UFRuntime::currentTime()<<endl;
	  _fits0Hdr = _sadfits->fitsHeader(sad, _epics);
	  _fits0Hdr->relabel(datalabel);
          clog<<"ufdhsput> pushed final FITS info onto dataque, time: "<<UFRuntime::currentTime()<<endl;
	  dataque->push(_fits0Hdr);
	}
        qlength = dataque->push(data);
        //if( _verbose )
          clog<<"ufdhsput> inserted final image: "<<data->name()<<" into dataque idx: "<<idx<<", qlength: "<<qlength<<endl;
        dataque->push(0); // indicate final frame
        dataque = 0; // dataque thread will free que and exit on null frame...
      }
      else {
        qlength = dataque->push(data);
        //if( _verbose )
          clog<<"ufdhsput> inserted new image: "<<data->name()<<" into dataque idx: "<<idx<<", qlength: "<<qlength<<endl;
      }
      continue;
    }
    clog<<"ufdhsput> no interest in this uf protocol object type: "<<ufp->typeId()
        <<", qlength: "<<qlength<<endl;
    delete ufp; ufp = 0;
  } // all observations

  shutdown();
  return 0;
}

#endif // __ufdhsput_cc__
