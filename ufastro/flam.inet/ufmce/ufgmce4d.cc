#if !defined(__ufgmce4d_cc__)
#define __ufgmce4d_cc__ "$Name:  $ $Id: ufgmce4d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgmce4d_cc__;

#include "UFGemMCE4Agent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFGemMCE4Agent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgmce4d> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgmce4d> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgmce4d_cc__
