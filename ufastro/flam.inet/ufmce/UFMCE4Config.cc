#if !defined(__UFMCE4Config_cc__)
#define __UFMCE4Config_cc__ "$Name:  $ $Id: UFMCE4Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFMCE4Config_cc__;

#include "UFMCE4Config.h"
#include "UFGemMCE4Agent.h"
#include "UFDaemon.h"
//#include "UFROpticsCoef.h"

#include "sys/types.h"
#include "sys/stat.h"
#include "unistd.h"
#include "sys/wait.h"
#include "fstream.h"
#include "math.h"

#define _MAX2_(a,b) ((a)>(b)) ? (a) : (b)
#define _MAX3_(a,b,c) (_MAX2_(_MAX2_(a,b),_MAX2_(b,c)))
#define _MIN2_(a,b) ((a)<(b)) ? (a) : (b)
#define _MIN3_(a,b,c) (_MIN2_(_MIN2_(a,b),_MIN2_(b,c)))

// static
UFAcqCtrl* UFMCE4Config::_AcqCtrl = 0; // should be a duplicate of UFGemMCE4Agent's

float UFMCE4Config::_flush= 0.05; 
int UFMCE4Config::_minPBC=2;           //minimum allowed Pixel Base Clock
bool UFMCE4Config::_verbose= false;
bool UFMCE4Config::_edtd= true; // if true, connect to edtd and send acq cmd
bool UFMCE4Config::_take= false; // if not using edtd, optionally spawn ufgtake on acq
bool UFMCE4Config::_fits= false; // send fits to edtd?
UFEdtDMA::Conf* UFMCE4Config::_edtcfg;
double UFMCE4Config::_FrmDutyCycle = 1.0;
int UFMCE4Config::_wellDepth = 0;
int UFMCE4Config::_biasLevel = 0;
int UFMCE4Config::_VdetgrvDACval = 0; //must perform DAC bias read all voltages to get value.
bool UFMCE4Config::_idle = true;
// for unit tests:
// frame count anticipated from current observation (0 == infinite, ala edt/pdv start_dma):
int UFMCE4Config::_frmCnt= 0; 
// option to run ufgtake child with mce4 start/run directive
pid_t UFMCE4Config::_takepid = (pid_t)-1;
UFMCE4Config::DACParms UFMCE4Config::_biasP; // DAC bias params (as provided by mce4 query)
UFMCE4Config::DACParms UFMCE4Config::_preamP; // DAC preamp params (as provided by mce4 query)
string UFMCE4Config::_des_obsmode, UFMCE4Config::_des_readoutmode;
string UFMCE4Config::_actv_obsmode, UFMCE4Config::_actv_readoutmode;

// hardware settings currently active
UFMCE4Config::HdwrParms UFMCE4Config::_activeH; 
// desired hadrware settings
UFMCE4Config::HdwrParms UFMCE4Config::_desH; 
// meta params associated with physical params
UFMCE4Config::TextParms UFMCE4Config::_metaP;
// basic (PBC=1) MCE frame times for each readout mode from method queryMCEtiming().
UFMCE4Config::HdwrParms UFMCE4Config::_frameTimes;
// constant meta config params from method readMetaConfigFile().
UFMCE4Config::HdwrParms UFMCE4Config::_metaConParms;

// 4 possible cycle types
UFMCE4Config::HdwrParms UFMCE4Config::_cycletypes;
UFMCE4Config::TextParms UFMCE4Config::_mceparms;

// FITS header keywords and comments corresponding to HdwrParms:
UFMCE4Config::TextParms UFMCE4Config::FITS_KeyWords;
UFMCE4Config::TextParms UFMCE4Config::FITS_Comments;
  
// helper for ctors:
int UFMCE4Config::clearAll() {
  clear(_activeH); clear(_desH);
  _des_obsmode = "Stare"; _des_readoutmode = "S1";
  return 1;
}

// helper for ctors:
void UFMCE4Config::_create() {
  clearAll();
  _cycletypes["stare"] = 20; _cycletypes["Stare"] = 20; _cycletypes["STARE"] = 20;
  _cycletypes["nod"] = 17; _cycletypes["Nod"] = 17; _cycletypes["NOD"] = 17;
  if( _verbose )    
    clog<<"UFMCE4Config::_create> _cycletypes: stare= "<<_cycletypes["stare"]
        <<", nod= "<<_cycletypes["nod"]<<endl;
      
  _mceparms["FrameCoadds"] = "LDVAR 0 "; 
  _mceparms["NodSettleReads"] = "LDVAR 6 ";
  _mceparms["Nods"] = "LDVAR 7 ";
  _mceparms["PixClock"] = "PBC ";
  _mceparms["ObsMode"] =  "CT ";
  _mceparms["ReadoutMode"] = "ARRAY_SAMPLING_TYPE ";

  // Setup FITS keywords and comments for method makeFITSheader():
  // These values are obtained from corresponding _activeH["*"] map arrays:
  FITS_KeyWords["FrameCoadds"]     = "FRMCOADD"; 
  FITS_Comments["FrameCoadds"]     = "# frames coadded per chop phase";

  FITS_KeyWords["NodSettleReads"]  = "NODSETTR"; 
  FITS_Comments["NodSettleReads"]  = "# of frames discarded during nod settle";

  FITS_KeyWords["Nods"]         = "NODSETS "; 
  FITS_Comments["Nods"]         = "# of nod cycles per total integration"; 

  FITS_KeyWords["PixClock"]        = "PIXCLOCK"; 
  FITS_Comments["PixClock"]        = "MCE4 Pixel Base Clock (PBC)"; 
                          
  FITS_KeyWords["PBC"] = "10";
  FITS_Comments["PBC"] = "Pixel base clock period";

  FITS_KeyWords["TotalFrames"]     = "TOTFRMS "; 
  FITS_Comments["TotalFrames"]     = "total # of frames from MCE saved"; 

  // These values are obtained from corresponding _activeP["*"] map arrays:
  FITS_KeyWords["FrameTime"]       = "FRMTIME "; 
  FITS_Comments["FrameTime"]       = "Frame time - ms"; 

  FITS_KeyWords["OnSourceTime"]    = "OBJTIME "; 
  FITS_Comments["OnSourceTime"]    = "On-source time - min"; 

  FITS_KeyWords["ElapsedTime"]     = "EXPTIME "; 
  FITS_Comments["ElapsedTime"]     = "Elapsed time - min"; 

  FITS_KeyWords["NodDwellTime"]    = "NODTIME "; 
  FITS_Comments["NodDwellTime"]    = "Nod time - sec"; 

  FITS_KeyWords["NodSettleTime"]   = "NODDELAY"; 
  FITS_Comments["NodSettleTime"]   = "Nod settle time - sec"; 

  FITS_KeyWords["NodDutyCycle"]    = "EFF_NOD "; 
  FITS_Comments["NodDutyCycle"]    = "Nod duty cycle (%)"; 

  FITS_KeyWords["FrameDutyCycle"]  = "EFF_FRM "; 
  FITS_Comments["FrameDutyCycle"]  = "Frame duty cycle (%)";


  FITS_KeyWords["PRERSETS"] = "10";
  FITS_Comments["PRERSETS"] = "Number of pre-resets";                           
  FITS_KeyWords["PSTRSETS"] = "1000";
  FITS_Comments["PSTRSETS"] = "Number of post-resets";
                       
  FITS_KeyWords["EDTIMOUT"] = "30";
  FITS_Comments["EDTIMOUT"] = "Number of seconds before EDT card timesout";

  FITS_KeyWords["LUTIMOUT"] = "30";
  FITS_Comments["LUTIMOUT"] = "Number of seconds before LUT apply timesout";

  FITS_KeyWords["CYCLETYP"] = "20";
  FITS_Comments["CYCLETYP"] = "Clocking Pattern Reference Number";

  FITS_KeyWords["TIMER"] = "S";
  FITS_Comments["TIMER"] = "Clocking Timer Units < mS | S >";
               
  FITS_KeyWords["DACVAL01"] = "192";
  FITS_Comments["DACVAL01"] = "Pream DAC value for Channel 1";

  FITS_KeyWords["DACVAL02"] = "192";
  FITS_Comments["DACVAL02"] = "Pream DAC value for Channel 2";

  FITS_KeyWords["DACVAL03"] = "192";
  FITS_Comments["DACVAL03"] = "Pream DAC value for Channel 3";

  FITS_KeyWords["DACVAL04"] = "192";
  FITS_Comments["DACVAL04"] = "Pream DAC value for Channel 4";

  FITS_KeyWords["DACVAL05"] = "192";
  FITS_Comments["DACVAL05"] = "Pream DAC value for Channel 5";                  

  FITS_KeyWords["DACVAL06"] = "192";
  FITS_Comments["DACVAL06"] = "Pream DAC value for Channel 6";

  FITS_KeyWords["DACVAL07"] = "192";
  FITS_Comments["DACVAL07"] = "Pream DAC value for Channel 7";

  FITS_KeyWords["DACVAL08"] = "192";
  FITS_Comments["DACVAL08"] = "Pream DAC value for Channel 8";

  FITS_KeyWords["DACVAL09"] = "192";
  FITS_Comments["DACVAL09"] = "Pream DAC value for Channel 9";

  FITS_KeyWords["DACVAL10"] = "192";
  FITS_Comments["DACVAL10"] = "Pream DAC value for Channel 10";

  FITS_KeyWords["DACVAL11"] = "192";
  FITS_Comments["DACVAL11"] = "Pream DAC value for Channel 11";

  FITS_KeyWords["DACVAL12"] = "192";
  FITS_Comments["DACVAL12"] = "Pream DAC value for Channel 12";

  FITS_KeyWords["DACVAL13"] = "192";
  FITS_Comments["DACVAL13"] = "Pream DAC value for Channel 13";

  FITS_KeyWords["DACVAL14"] = "192";
  FITS_Comments["DACVAL14"] = "Pream DAC value for Channel 14";
              
  FITS_KeyWords["DACVAL15"] = "192";
  FITS_Comments["DACVAL15"] = "Pream DAC value for Channel 15";

  FITS_KeyWords["DACVAL16"] = "192";
  FITS_Comments["DACVAL16"] = "Pream DAC value for Channel 16";

  FITS_KeyWords["DACVAL17"] = "192";
  FITS_Comments["DACVAL17"] = "Pream DAC value for Channel 17";

  FITS_KeyWords["DACVAL18"] = "192";
  FITS_Comments["DACVAL18"] = "Pream DAC value for Channel 18";

  FITS_KeyWords["DACVAL19"] = "192";
  FITS_Comments["DACVAL19"] = "Pream DAC value for Channel 19";

  FITS_KeyWords["DACVAL20"] = "192";
  FITS_Comments["DACVAL20"] = "Pream DAC value for Channel 20";

  FITS_KeyWords["DACVAL21"] = "192";
  FITS_Comments["DACVAL21"] = "Pream DAC value for Channel 21";

  FITS_KeyWords["DACVAL22"] = "192";
  FITS_Comments["DACVAL22"] = "Pream DAC value for Channel 22";

  FITS_KeyWords["DACVAL23"] = "192";
  FITS_Comments["DACVAL23"] = "Pream DAC value for Channel 23";

  FITS_KeyWords["DACVAL24"] = "192";
  FITS_Comments["DACVAL24"] = "Pream DAC value for Channel 24";

  FITS_KeyWords["DACVAL25"] = "192";
  FITS_Comments["DACVAL25"] = "Pream DAC value for Channel 25";

  FITS_KeyWords["DACVAL26"] = "192";
  FITS_Comments["DACVAL26"] = "Pream DAC value for Channel 26"; 

  FITS_KeyWords["DACVAL27"] = "192";
  FITS_Comments["DACVAL27"] = "Pream DAC value for Channel 27";

  FITS_KeyWords["DACVAL28"] = "192";
  FITS_Comments["DACVAL28"] = "Pream DAC value for Channel 28";

  FITS_KeyWords["DACVAL29"] = "192";
  FITS_Comments["DACVAL29"] = "Pream DAC value for Channel 29";

  FITS_KeyWords["DACVAL30"] = "192";
  FITS_Comments["DACVAL30"] = "Pream DAC value for Channel 30";                 

  FITS_KeyWords["DACVAL31"] = "192";
  FITS_Comments["DACVAL31"] = "Pream DAC value for Channel 31";                 

  FITS_KeyWords["DACVAL32"] = "192";
  FITS_Comments["DACVAL32"] = "Pream DAC value for Channel 32]";

  FITS_KeyWords["BUNIT"] = "ADU";
  FITS_Comments["BUNIT"] = "Raw data units";

  FITS_KeyWords["BIAS"] = "1.0";
  FITS_Comments["BIAS"] = "Applied bias in Volts";

  FITS_KeyWords["DETTYPE"] = "Hawaii-II";
  FITS_Comments["DETTYPE"] = "Detector Type (Rockwell HgCdTe)";               

  FITS_KeyWords["DETECTOR"] = "Engineering";
  FITS_Comments["DETECTOR"] = "Detector Name < Engineering | Science >";        

  FITS_KeyWords["DETSER"] = "Serial Number 28";
  FITS_Comments["DETSER"] = "Rockwell Serial Number";
                         
  FITS_KeyWords["DETID"] = "9-307R";
  FITS_Comments["DETID"] = "Rockwell Detector ID Number";                   
  
  FITS_KeyWords["MUXID"] = "L3W16C2";
  FITS_Comments["MUXID"] = "Rockwell Multiplexer ID Number";                 

  FITS_KeyWords["DETINSTA"] = "2004-01-01";
  FITS_Comments["DETINSTA"] = "Date of first cooldown";                        

  FITS_KeyWords["DCHEALTH"] = "GOOD";
  FITS_Comments["DCHEALTH"] = "MCE4 health";                                   

  FITS_KeyWords["DCNAME"] = "MCE4 version";
  FITS_Comments["DCNAME"] = "Put MCE4 code version number here";              

  FITS_KeyWords["DCSTATE"] = "IDLE";
  FITS_Comments["DCSTATE"] = "MCE4 state";                                     
} // _create

// ctors
UFMCE4Config::UFMCE4Config(const string& name) : UFDeviceConfig(name) {
  _create();
}

UFMCE4Config::UFMCE4Config(const string& name,
			   const string& tshost,
			   int tsport) : UFDeviceConfig(name, tshost, tsport) {
  _create();
}

/*
int UFMCE4Config::_acqStartEdt(UFAcqCtrl* acq) {
  UFStrings *fitsprm= 0, *fitsext= 0;
  UFFlamObsConf* ocfg= 0; // assume obssetup has been performed?
  string file= acq->startObs(this, ocfg, fitsprm, fitsext);
  if( ocfg == 0 ) { // no obssetup?
     clog<<"UFMCE4Config::_acqStartEdt> No acq obssetup info!"<<endl;
     return -1;
  }
  string edtd= acq->startObs(this);
  clog<<"UFMCE4Config::_acqStartEdt> edtd: "<<edtd<<endl;
  return edtd.length();
}

int UFMCE4Config::_acqAbortEdt(UFAcqCtrl* acq) {
  UFStrings *fitsprm= 0, *fitsext= 0;
  UFFlamObsConf* ocfg= 0; // assume obssetup has been performed?
  string edtd= acq->abortObs(this, ocfg); //, fitsprm, fitsext);
  if( ocfg == 0 ) { // no obssetup?
     clog<<"UFMCE4Config::_acqAbortEdt> No acq obssetup info!"<<endl;
     return -1;
  }
  string edtd= acq->abortObs(this); //, fitsprm, fitsext);
  clog<<"UFMCE4Config::_acqAbortEdt> edtd: "<<edtd<<endl;
  return edtd.length();
}

int UFMCE4Config::_acqStopEdt(UFAcqCtrl* acq) {
  UFStrings *fitsprm= 0, *fitsext= 0;
  UFFlamObsConf* ocfg= 0; // assume obssetup has been performed?
  string edtd= acq->stopObs(this, ocfg, fitsprm, fitsext);
  if( ocfg == 0 ) { // no obssetup?
     clog<<"UFMCE4Config::_acqStopEdt> No acq obssetup info!"<<endl;
     return -1;
  }
  string edtd= acq->stopObs(this);
  clog<<"UFMCE4Config::_acqStopEdt> edtd: "<<edtd<<endl;
  return edtd.length();
}
*/

// support old flamingos-1 ufgtake spawn technique:
int UFMCE4Config::_flamtake(const string& mcd) {
  size_t opos = mcd.find(" -");
  char* uf= getenv("UFINSTALL");
  string takeopt;
  if( uf == 0 ) {
    takeopt = "env LD_LIBRARY_PATH=/usr/local/uf/lib/:/usr/local/lib:/opt/EDTpdv ";
    takeopt += "/usr/local/uf/bin/ufgtake " + mcd.substr(opos);
  }
  else {
    strstream s;
    s<<"env LD_LIBRARY_PATH="<<uf<<"/lib/:/usr/local/lib:/opt/EDTpdv "<<uf<<"/bin/ufgtake "<<ends;
    takeopt = s.str(); delete s.str();
    takeopt += mcd.substr(opos);
  }
  if( _takepid > 0 ) {
    clog<<"UFMCE4Config::_flamtake> start ? frame grad/take in progress, mce4 not idle"<<endl;
    return -1;
  }

  if( _verbose )
    clog<<"UFMCE4Config::_flamtake> exec ufgtake child before starting mce4."<<endl;

  _takepid = UFRuntime::execApp(takeopt, UFDaemon::envp());
  if( _takepid < 0 ) {
    clog<<"UFMCE4Config::_flamtake> ? exec ufgtake child failed, pid: "<<_takepid<<endl;
    return _takepid;
  }	
    
  UFPosixRuntime::sleep(0.5); // wait for edt semaphore(s)/lockfiles to be created and set
  int w, h, cnt= 10;
  while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) ) 
    UFPosixRuntime::sleep(0.5); // semaphores and/or lockfiles not ready?
  if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
    clog<<"UFMCE4Config::_flamtake> exec ufgtake child failed? aborting start directive"<<endl;
    UFEdtDMA::takeAbort(_takepid);
    _takepid = (pid_t) -1;
    return -1;
  }

  return UFEdtDMA::takeTotCnt(); // if zero, expected cnt is infinte
} // _flamtake (old flamingos-1 take)

// mce4 agent entry points...
// acquisition directives: start, stop/abort, etc.
// non static func. can optionally use acqctrl:
int UFMCE4Config::acquisition(const string& directive, string& reply, UFAcqCtrl* acq, float flush) { 
  // handle any paramaters that follow a "start" or "sim" directive to mce4 which 
  // are meant to be passed down to ufedtd via the UFAcqCtrl class:
  // or run ufgtake as child proc:
  clog<<"UFMCE4Config::acquisition> directive: "<<directive<<", UFAcqCtrl* == "<<(size_t)acq<<endl;
  if( acq != 0 ) {
    if( acq != _AcqCtrl ) {
      clog<<"UFMCE4Config::acquisition> acq != UFGemMCE4Agent::_AcqCtrl, reset it?"<<endl;
      acq = _AcqCtrl;
    }
    if( acq->_obssetup == 0 ) {
      clog<<"UFMCE4Config::acquisition> acq->_obssetup uninitialized, perform at least one OBSSETUP/CONF before acquistion!"<<endl;
      return -1;
    }
    clog<<"UFMCE4Config::acquisition> acq->_obssetup: "<<(size_t)acq->_obssetup
	<<" ,acq->_obssetup->obscfg: "<<(size_t)acq->_obssetup->_obscfg<<endl;
  }

  char* cd = (char*)directive.c_str(); while( *cd == ' ' || *cd == '\t' ) ++cd;
  string mcd = cd; // eliminated any leading white spaces in directive
  UFStrings::upperCase( mcd );

  // commit a configuration:
  if( mcd.find("CONF") != string::npos )
    return parmConfig(reply, flush); // issue a parmConfig using current _desH values.
   
  // clear/datum a configuration:
  if( mcd.find("CLEAR") != string::npos )
    return clearConfig(reply, flush); // clear parms, and set mce4 to cycle type 0.
   
  // test MCE
  if( mcd.find("TEST") != string::npos ) {
    if( _devIO ) {
      if( _devIO->submit("STATUS", reply, flush) > 0 ) {
	size_t ct0 = reply.find("Cycle Type-------------------> 0");
	if( ct0 == string::npos )
	  _idle = false;
	else
	  _idle = true; // cycle type 0 is always idle, even after start/run directive?
	return reply.size();
      }
      else return(-1);  // indicate that test communication with MCE failed.
    }
    else { // assume sim.
      _idle = true;
      return 0;
    }
  }

  // start frame acquisition(s)
  size_t opos = mcd.find(" -");
  if( opos == string::npos ) opos = mcd.length();
  bool edtAcqOrTake= false;
   // check for acq/edtd or ufgtake options, start grab/take child, issue mce start
  if( acq == 0 && _take && (mcd.find("START") != string::npos || mcd.find("RUN") != string::npos) ) {
    // use old F1 ufgtake child technique if acq == 0:
    clog<<"UFMCE4Config::acquisition> ufgtake spawn... "<<endl;
    int ft = _flamtake(mcd);
    if( ft <= 0 ) {
      clog<<"UFMCE4Config::acquisition> failed ufgtake spawn?"<<endl;
      return ft;
    }
    edtAcqOrTake = true;
  }
  else if( acq != 0  && (mcd.find("START") != string::npos || mcd.find("RUN") != string::npos) ) {
    if( _edtd ) {
      clog<<"UFMCE4Config::acquisition> send obsconf to edtd: "<<acq->_obssetup->_obscfg->name()<<endl;
      string edtreply = acq->startObs(this); // _acqStartEdt(acq);
      if( edtreply.empty() ) { // no obssetup?
        clog<<"UFMCE4Config::acquisition> no edtd reply..."<<endl;
        return -1;
      }
      clog<<"UFMCE4Config::acquisition> edtd reply: "<<edtreply<<endl;
      edtAcqOrTake = true;
    }
  }

  if( _devIO == 0 || UFAcqCtrl::_sim ) { // can't/don't send command to mce4
    clog<<"UFMCE4Config::acquisition> no mce transaction (sim)."<<endl;
    return 0;
  }

  if( edtAcqOrTake ) {
    // issue start/run acq directive to mce:
    string mcecmd = mcd.substr(0, opos);
    /*
    if( mcecmd.find("START") != string::npos ) // replace with RUN
      mcecmd = "RUN";
	
    //if( _verbose )
      clog<<"UFMCE4Config::acquisition> resetting MCE4 and submitting directive: "<<mcecmd<<endl;

    _devIO->submit("CT 0", reply, flush);
    _devIO->submit("START", reply, flush);
    string ctcmd = _mceparms["ObsMode"] + UFRuntime::numToStr(_cycletypes[_des_obsmode]);
    clog<<"UFMCE4Config::acquisition> re-submitting cycle type: "<<ctcmd<<endl;
    _devIO->submit(ctcmd, reply, flush);
    string reply2;
    clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
    _devIO->submit("CHOP_0", reply2, flush);
    clog<<reply2<<endl;
     reply += reply2;
    _devIO->submit("RUN", reply2, flush);
     reply += reply2;
    */

     //if( _verbose )
      clog<<"UFMCE4Config::acquisition> submit MCE4: "<<mcecmd<<endl;

    _devIO->submit(mcecmd, reply, flush);

     //if( _verbose )
       clog<<"UFMCE4Config::acquisition> MCE acq start/run cycle started, check status of MCE4..."<<endl;

    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?
  
    return reply.length();
  } // end of proceesing ACQEdt or AQCTake START/RUN

  // check for (mce4) sim directive
  if( _take && mcd.find("SIM") != string::npos ) {
    // check for ufgtake options, start grab/take child, issue mce start
    size_t opos = mcd.find(" -");
    if( opos != string::npos && mcd.length() > opos ) {
      string takeopt = "env LD_LIBRARY_PATH=/usr/local/lib:/usr/local/uf/lib/:/opt/EDTpdv ";
      takeopt += "/usr/local/uf/bin/ufgtake " + mcd.substr(opos);
      if( _takepid > 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take in progress, mce4 not idle"<<endl;
	return -1;
      }
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> exec ufgtake child before starting mce4."<<endl;
      _takepid = UFRuntime::execApp(takeopt, UFDaemon::envp());
      if( _takepid < 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take failed to initialize."<<endl;
	return -1;
      }
      UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false
      int w, h, cnt= 10;
      while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) )
	UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false

      if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
        clog<<"UFMCE4Config::acquisition> exec ufgtake child failed? aborting start directive"<<endl;
	UFEdtDMA::takeAbort(_takepid);
	int status; ::waitpid(_takepid, &status, WNOHANG);
	_takepid = (pid_t) -1;
	return -1;
      }
    }
    if( _devIO == 0 )
      return 0;

    string mcecmd = mcd.substr(0, opos);
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> submitting directive to MCE4: "<<mcecmd<<endl;
    _devIO->submit(mcecmd, reply, flush);

    if( _verbose )
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?

    return reply.length();
  } // _take && SIM

  // stop or abort directive should be implemented by submitting CT 0 & START (start idle cycle-type) 
  if( mcd.find("STOP") != string::npos ||
      mcd.find("DATUM") != string::npos ||
      mcd.find("PARK")  != string::npos ||
      mcd.find("ABORT") != string::npos ) {

    bool abort= false, stop= false;
    if( mcd.find("ABORT") != string::npos ) 
      abort = true;
    if( mcd.find("STOP") != string::npos ) 
      stop = true;

    if( _edtd ) {
      if( abort && acq != 0 )
        acq->abortObs(this); //_acqAbortEdt(acq);
      if( stop && acq != 0 )
        acq->stopObs(this); //_acqStopEdt(acq);
    }
    else if( _take && _takepid > 0 ) {
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> aborting ufgtake"<<endl;
      UFEdtDMA::takeAbort(_takepid);
      _takepid = (pid_t) -1;
    }

    if( _devIO ) {
      //if( _verbose )
        clog<<"UFMCE4Config::acquisition> (Abort/Datum/Park/Stop) submit CT 0 & START directive to mce4 ("<<mcd<<")"<<endl;
      string rep;
      _devIO->submit("CT 0", rep, flush);
      reply += rep;
      _devIO->submit("START", rep, flush);
      reply += rep;
      /*
      clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
      _devIO->submit("CHOP_0", rep, flush);
      clog<<rep<<endl;
      reply += rep;
      */
      _idle = true;
      return reply.size();
    }
    else { // assume sim.
      //if( _verbose )
        clog<<"UFMCE4Config::acquisition> submitting CT 0 & START directive to mce4 ("<<mcd<<")"<<endl;
      _idle = true;
      return 0;
    }
  } // abort or stop or datum or park

  clog<<"UFMCE4Config::acquisition> ? not supported (start/run/sim/stop/abort) directive: "<<mcd<<endl;
  return -1;
} // acquisition with acqctrl

// static func. can optionally use ufgtake:
int UFMCE4Config::acquisition(const string& directive, string& reply, float flush) { 
  // handle any paramaters that follow a "start" or "sim" directive to mce4 which 
  // are meant to be passed down to ufedtd via the UFAcqCtrl class:
  // or run ufgtake as child proc:

  char* cd = (char*)directive.c_str(); while( *cd == ' ' || *cd == '\t' ) ++cd;
  string mcd = cd; // eliminated any leading white spaces in directive
  UFStrings::upperCase( mcd );

  // commit a configureation:
  if( mcd.find("CONF") != string::npos )
    return parmConfig(reply, flush); // issue a parmConfig using current _desH values.
   
  // clear/datum a configureation:
  if( mcd.find("CLEAR") != string::npos )
    return clearConfig(reply, flush); // clear parms, and set mce4 to cycle type 0.
   
  // test MCE
  if( mcd.find("TEST") != string::npos ) {
    if( _devIO ) {
      if( _devIO->submit("STATUS", reply, flush) > 0 ) {
	size_t ct0 = reply.find("Cycle Type-------------------> 0");
	if( ct0 == string::npos )
	  _idle = false;
	else
	  _idle = true; // cycle type 0 is always idle, even after start/run directive?
	return reply.size();
      }
      else return(-1);  // indicate that test communication with MCE failed.
    }
    else { // assume sim.
      _idle = true;
      return 0;
    }
  }

  // start frame acquisition(s)
  size_t opos = mcd.find(" -");
  if( _take && (mcd.find("START") != string::npos || mcd.find("RUN")  != string::npos) ) {
    // use old F1 ufgtake child technique if acq == 0:
    int ft = _flamtake(mcd);
    if( ft <= 0 ) {
      clog<<"UFMCE4Config::acquisition> failed ufgtake spawn?"<<endl;
      return ft;
    } // _takepid set

    if( _takepid > 0 ) {
      UFPosixRuntime::sleep(0.5); // wait for edt semaphore(s) to be created and set
      int w, h, cnt= 10;
      while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) )
        UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false
      if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
        clog<<"UFMCE4Config::acquisition> exec ufgtake child failed? aborting start directive"<<endl;
        UFEdtDMA::takeAbort(_takepid);
        _takepid = (pid_t) -1;
        return -1;
      }
      _frmCnt = UFEdtDMA::takeTotCnt(); // if zero, expected cnt is infinte
    }

    if( _devIO == 0 || UFAcqCtrl::_sim ) // don't/can't send command to mce4
      return 0;

    // issue start/run acq firective to mce:
    string mcecmd = mcd.substr(0, opos);
    if( mcecmd.find("START") != string::npos ) // replace with RUN
      mcecmd = "RUN";
	
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> resetting MCE4 and submitting directive: "<<mcecmd<<endl;

    _devIO->submit("CT 0", reply, flush);
    _devIO->submit("START", reply, flush);
    string ctcmd = _mceparms["ObsMode"] + UFRuntime::numToStr(_cycletypes[_des_obsmode]);
    clog<<"UFMCE4Config::acquisition> re-submitting cycle type: "<<ctcmd<<endl;
    _devIO->submit(ctcmd, reply, flush);
    string reply2;
    clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
    _devIO->submit("CHOP_0", reply2, flush);
    clog<<reply2<<endl;
    reply += reply2;
    _devIO->submit("RUN", reply2, flush);
    reply += reply2;

    if( _verbose )
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?

    return reply.length();
  } //end of proceesing START/RUN

  // check for (mce4) sim directive
  if( mcd.find("SIM") != string::npos ) {
    // check for ufgtake options, start grab/take child, issue mce start
    size_t opos = mcd.find(" -");
    if( opos != string::npos && mcd.length() > opos ) {
      string takeopt = "env LD_LIBRARY_PATH=/usr/local/lib:/usr/local/uf/lib/:/opt/EDTpdv ";
      takeopt += "/usr/local/uf/bin/ufgtake " + mcd.substr(opos);
      if( _takepid > 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take in progress, mce4 not idle"<<endl;
	return -1;
      }
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> exec ufgtake child before starting mce4."<<endl;
      _takepid = UFRuntime::execApp(takeopt, UFDaemon::envp());
      if( _takepid < 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take failed to initialize."<<endl;
	return -1;
      }
      UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false
      int w, h, cnt= 10;
      while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) )
	UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false

      if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
        clog<<"UFMCE4Config::acquisition> exec ufgtake child failed? aborting start directive"<<endl;
	UFEdtDMA::takeAbort(_takepid);
	int status; ::waitpid(_takepid, &status, WNOHANG);
	_takepid = (pid_t) -1;
	return -1;
      }
    }
    if( _devIO == 0 )
      return 0;

    string mcecmd = mcd.substr(0, opos);
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> submitting directive to MCE4: "<<mcecmd<<endl;
    _devIO->submit(mcecmd, reply, flush);

    if( _verbose )
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?

    return reply.length();
  }

  // stop or abort directive should be implemented by submitting CT 0 & START (start idle cycle-type) 
  if( mcd.find("STOP") != string::npos ||
      mcd.find("DATUM") != string::npos ||
      mcd.find("PARK")  != string::npos ||
      mcd.find("ABORT") != string::npos ) {

    if( _takepid > 0 ) {
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> aborting ufgtake"<<endl;
      UFEdtDMA::takeAbort(_takepid);
      _takepid = (pid_t) -1;
    }
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> submitting CT 0 & START directive to mce4 ("<<mcd<<")"<<endl;

    if( _devIO ) {
      string rep;
      _devIO->submit("CT 0", rep, flush);
      reply += rep;
      _devIO->submit("START", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
      _devIO->submit("CHOP_0", rep, flush);
      clog<<rep<<endl;
      reply += rep;
      _idle = true;
      return reply.size();
    }
    else { // assume sim.
      _idle = true;
      return 0;
    }
  }

  clog<<"UFMCE4Config::acquisition> ? not supported (start/run/sim/stop/abort) directive: "<<mcd<<endl;
  return -1;
} // static acquisition

int UFMCE4Config::validAcq(const string& Directive) {
  string directive = Directive;
  UFStrings::upperCase( directive );
  if( directive.find("DAT") != string::npos || directive.find("INF") != string::npos ||
      directive.find("IND") != string::npos || directive.find("IDX") != string::npos ||
      directive.find("LUT") != string::npos) {
    clog<<"UFMCE4Config::validAcq> Ok: "<<directive<<endl;
    return 2;
  }
  if( directive.find("START") != string::npos || directive.find("RUN") != string::npos) {
    clog<<"UFMCE4Config::validAcq> Ok: "<<directive<<endl;
    return 1; // mce4 will no longer be idle, it should be generating frames
  }
  if( directive.find("ABORT") != string::npos || directive.find("CLEAR") != string::npos ||
      directive.find("CLOSE") != string::npos || directive.find("TEST") != string::npos ||
      directive.find("DATUM") != string::npos || directive.find("OPEN") != string::npos ||
      directive.find("PARK") != string::npos || directive.find("STOP") != string::npos ) {
    clog<<"UFMCE4Config::validAcq> Ok: "<<directive<<endl;
    return 0; // idle
  }
  clog<<"UFMCE4Config::validAcq> Reject: "<<directive<<endl;
  return -1;
}

// commit hardware params to mce4
// if successfull, this resets actives to 
// be congruent with desired:
// parameter config directive:
int UFMCE4Config::parmConfig(string& reply, float flush) {
  if( _devIO == 0 ) 
    reply = "sim: ";
  else
    reply = "";
  string rep;

  // Submit the det.array readout mode ONLY if it has changed.
  // First determine the basic readoutmode, without the "RAW" option:
  UFStrings::upperCase( _des_readoutmode );
  string basic_des_rdout = _des_readoutmode;
  if( _des_readoutmode.find("_RAW") != string::npos )
    basic_des_rdout = _des_readoutmode.substr( 0, _des_readoutmode.find("_RAW") );
  else if( _des_readoutmode.find("RAW") != string::npos )
    basic_des_rdout = _des_readoutmode.substr( 0, _des_readoutmode.find("RAW") );

  string basic_actv_rdout = _actv_readoutmode;
  if( _actv_readoutmode.find("_RAW") != string::npos )
    basic_actv_rdout = _actv_readoutmode.substr( 0, _actv_readoutmode.find("_RAW") );
  else if( _actv_readoutmode.find("RAW") != string::npos )
    basic_actv_rdout = _actv_readoutmode.substr( 0, _actv_readoutmode.find("RAW") );

  if( basic_actv_rdout != basic_des_rdout ) {
    string rdocmd;

    if( _des_readoutmode.find("S1R3") != string::npos ||
	_des_readoutmode.find("1S3R") != string::npos ) {
      rdocmd = " 4"; //quadruple sampling (with both output and column clamp refs)
    }
    else if( _des_readoutmode.find("S1R1") != string::npos ||
	     _des_readoutmode.find("1S1R") != string::npos )
      {
	if( _des_readoutmode.find("CR") != string::npos ) //two types of double sampling are possible
	  rdocmd = " 2 2"; // Column clamp Ref.
	else
	  rdocmd = " 2 1"; // Output clamp Ref.
      }
    else rdocmd = " 1"; //single sampling

    rdocmd = _mceparms["ReadoutMode"] + rdocmd;
    clog<<"UFMCE4Config::parmConfig> readoutmode="<<_des_readoutmode<<", mcecmd="<<rdocmd<<endl;

    if( _devIO && rdocmd.length() > 1 ) {
      _devIO->submit(rdocmd, rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> "<<rdocmd<<", reply: "<<rep<<endl;
      _devIO->submit("STOP", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> STOP, reply: "<<rep<<endl;
      _devIO->submit("LOAD_PAT_RAM", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> LOAD_PAT_RAM, reply: "<<rep<<endl;
    }
    else {  // assume simulation
      reply += "\n\t" + rdocmd;
      reply += "\n\t STOP";
      reply += "\n\t LOAD_PAT_RAM";
    }
  }

  //restart the clocking of array in idle cycle type (no data transfers):
  if( _devIO ) {
    _devIO->submit("CT 0", rep, flush);
    reply += rep;
    _devIO->submit("START", rep, flush);
    reply += rep;
  }

  // now configure the other MCE registers: LDVARs and CycleType:
  HdwrParms::iterator itr = _desH.begin();
  string mcep;

  do {
    string key = itr->first;
    double val = itr->second;    bool cycle = false;
    if( mcep.find("CT") != string::npos )
      cycle = true;
    bool ldvar = false;
    if( mcep.find("LDVAR") != string::npos )
      ldvar = true;

    clog<<"UFMCE4Config::parmConfig> key: "<<key<<", val: "<<val<<endl;
    // is this a PBC or CT or LDVAR or some other MCE cmd?
    strstream s;
    mcep = _mceparms[key];
    if( ldvar && (mcep.find("LDVAR") != string::npos) &&  // no-op.
	     (key.find("FrameCoadds") != string::npos) )
      s<<"0"<<ends;
    else if( cycle ) // deal with cycle type last
      s<<"0"<<ends;
    else if( mcep.length() < 1 ) // no-op
      s<<"0"<<ends;
    else // ok
      s<<mcep<<val<<ends;
       
    string cmd= s.str(); delete s.str();

    if( _devIO && cmd.length() > 1 ) {
      _devIO->submit(cmd, rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> cmd:"<<cmd<<", reply: "<<rep<<endl;
    }
    else {// assume simulation
      strstream sim;
      sim<<"\n\t"<<key<< " == " << val<<ends;
      reply += sim.str(); delete sim.str();
      if( cmd.length() > 1 )
	clog<<"UFMCE4Config::parmConfig> cmd:"<<cmd<<endl;
    }
  } while( ++itr != _desH.end() );

  // now submit the cycle type, followed by "SIM 0":
  mcep = _mceparms["ObsMode"];
  double ct = _cycletypes[_des_obsmode];
  clog<<"UFMCE4Config::parmConfig> obsmode= "<<_des_obsmode<<", mcep= "<<mcep<<", ct= "<<ct<<endl;
  strstream sc;
  sc<<mcep<<ct<<ends;
  string ctcmd= sc.str(); delete sc.str();

  if( _devIO && ctcmd.length() > 1 ) {
    _devIO->submit(ctcmd, rep, flush);
    reply += rep;
    clog<<"UFMCE4Config::parmConfig> "<<ctcmd<<", reply: "<<rep<<endl;
    _devIO->submit("SIM 0", rep, flush);
    reply += rep;    
    clog<<"UFMCE4Config::parmConfig> SIM 0, reply: "<<rep<<endl;
  }
  else {// assume simulation
    strstream sim;
    sim<<"\n\t"<<mcep<< " == " <<_cycletypes[_des_obsmode]<<ends;
    reply += sim.str(); delete sim.str();
  }

  //do a quick RUN of new CycleType and then back to idle to flush possible 1st frame:
  if( _devIO ) {
    _devIO->submit("RUN", rep, flush);
    reply += rep;
    _devIO->submit("CT 0", rep, flush);
    reply += rep;
    _devIO->submit("START", rep, flush);
    reply += rep;
  }

  if( _devIO == 0 ) 
    clog<<"UFMCE4Config::parmConfig> reply: "<<reply<<endl;

  if( reply != "" ) {
    _actv_obsmode = _des_obsmode;
    _actv_readoutmode = _des_readoutmode;
    _activeH["FrameCoadds"] = _desH["FrameCoadds"];
    _activeH["Nods"] = _desH["Nods"];
    _activeH["TotalFrames"] = _desH["TotalFrames"];
    _activeH["PixClock"] = _desH["PixClock"];
    _activeH["FrameTime"] = _desH["FrameTime"];
    _activeH["OnSourceTime"] = _desH["OnSourceTime"];
    _activeH["ElapsedTime"] = _desH["ElapsedTime"];
    _activeH["NodDwellTime"] = _desH["NodDwellTime"];
    _activeH["NodSettleTime"] = _desH["NodSettleTime"];
    _activeH["NodDutyCycle"] = _desH["NodDutyCycle"];
    _activeH["FrameDutyCycle"] = _desH["FrameDutyCycle"];
  }

  isConfigured();
  return reply.length();
}

// clear & commit hardware params to mce4
// if successfull, this resets actives to 
// be congruent with desired:
// parameter config directive:
int UFMCE4Config::clearConfig(string& reply, float flush) {
  if( _devIO == 0 ) 
    reply = "sim: ";
  else
    reply = "";
       
  string cmd= "CT 0";

  if( _devIO ) {
    string rep;
    _devIO->submit(cmd, rep, flush);
    reply += rep;
  }
  else {// assume simulation
    reply += cmd;
  }
  if( _verbose )
    clog<<"UFMCE4Config::parmConfig> "<<cmd<<", reply: "<<reply<<endl;

  if( reply != "" ) {
    clearAll();
  }

  isConfigured();
  return reply.length();
}

bool UFMCE4Config::isConfigured() {
  bool r = !isClear(_activeH);
  if( _verbose ) {
    if( r )
      clog<<"UFMCE4Config::isConfigured::> true"<<endl;
    else
      clog<<"UFMCE4Config::isConfigured::> false"<<endl;
  }
  return r;
}

bool UFMCE4Config::isClear(HdwrParms& hp) {
  if( hp.size() <= 0 )
    return true;

  HdwrParms::iterator itr = hp.begin();
  do {
    //string key = itr->first;
    double val = itr->second;
    if( val != 0 )
      return false;
  } while( ++itr != hp.end() );

  return true;
}

// clear Hardware Parameters:
int UFMCE4Config::clear(HdwrParms& hp) {
  hp.clear();
  hp.insert(HdwrParms::value_type("FrameCoadds", 0));
  hp.insert(HdwrParms::value_type("NodSettleReads", 0));
  hp.insert(HdwrParms::value_type("Nods", 0));
  hp.insert(HdwrParms::value_type("TotalFrames", 1));
  hp.insert(HdwrParms::value_type("PixClock", 2));
  hp.insert(HdwrParms::value_type("FrameTime", 0.0));
  hp.insert(HdwrParms::value_type("OnSourceTime", 0.0));
  hp.insert(HdwrParms::value_type("NodDwellTime", 0.0));
  hp.insert(HdwrParms::value_type("NodSettleTime", 0.0));
  hp.insert(HdwrParms::value_type("FrameDutyCycle", 0.0));
  hp.insert(HdwrParms::value_type("OnSourceTime", 0.0));
  hp.insert(HdwrParms::value_type("ElapsedTime", 0.0));
  hp.insert(HdwrParms::value_type("NodDutyCycle", 0.0));
  /*
  HdwrParms::iterator itr = hp.begin();
  do {
    string key = itr->first;
    double val = itr->second;
    clog<<"UFMCE4Config::clearHdwr> key: "<<key<<", val: "<<val<<endl;
  } while( ++itr != pp.end() );
  */
  return hp.size();
}

// clear Meta Parameters:
int UFMCE4Config::clear(TextParms& mp) {
  mp.clear();
  mp.insert(TextParms::value_type("FluxBackground", "nominal"));
  mp.insert(TextParms::value_type("FluxNoise", "nominal"));
  mp.insert(TextParms::value_type("CameraMode", "imaging"));
  mp.insert(TextParms::value_type("ObsMode", "stare"));
  mp.insert(TextParms::value_type("OnSourceTime", "seconds"));
  mp.insert(TextParms::value_type("DeckerName", "Mask"));
  mp.insert(TextParms::value_type("FilterName", "Si7.8um"));
  mp.insert(TextParms::value_type("GrismName", "LowRes10"));
  mp.insert(TextParms::value_type("CentralWavelength", "microns"));
  mp.insert(TextParms::value_type("ImagingMode", "Field"));
  mp.insert(TextParms::value_type("LensName", "Pupil-Imager"));
  mp.insert(TextParms::value_type("LyotName", "CrossHair-100"));
  mp.insert(TextParms::value_type("FocusName", "focus"));
  mp.insert(TextParms::value_type("SlitName", "0.21"));
  mp.insert(TextParms::value_type("MOSName", "ZnSe"));
  mp.insert(TextParms::value_type("Throughput", "0.8"));
  mp.insert(TextParms::value_type("SetPoint", "7.0"));
  mp.insert(TextParms::value_type("HeaterPower", "watts"));
  /*
  TextParms::iterator itr = mp.begin();
  do {
    string key = itr->first;
    string val = itr->second;
    clog<<"UFMCE4Config::clearMeta> key: "<<key<<", val: "<<val<<endl;
  } while( ++itr != mp.end() );
  */
  return mp.size();
}

// encode hardware parameters for transaction:
// this allocation must be freed by caller.
// this should emulate expected transaction from epics
UFStrings* UFMCE4Config::encode(const HdwrParms& hard, 
				const string& obsmode,
				const string& readoutmode,
				const string& car) {
  string k, v;
  vector< string > sv; 
  k = "Car"; v = car;
  sv.push_back(k); sv.push_back(v);
  k = "Parm"; v = "Hardware";
  sv.push_back(k); sv.push_back(v);

  sv.push_back("ObsMode"); sv.push_back(obsmode);
  sv.push_back("ReadoutMode"); sv.push_back(readoutmode);

  HdwrParms::const_iterator itr = hard.begin();
  do {
    k = itr->first;
    strstream s;
    s<<itr->second<<ends; // int to str
    v = s.str(); delete s.str();
    sv.push_back(k); sv.push_back(v);
    //clog<<"UFMCE4Config::encode> key: "<<k<<", val: "<<v<<endl;
  }   while( ++itr != hard.end() );

  return new UFStrings("UFMCE4Config::encodeHdwr", sv);
}

// encode meta parameters for transaction:
// this allocation must be freed by caller.
UFStrings* UFMCE4Config::encode(const TextParms& meta,
				const string& car) {
  string k, v;
  vector< string > sv; 
  k = "Car"; v = car;
  sv.push_back(k); sv.push_back(v);
  k = "Parm"; v = "Meta";
  sv.push_back(k); sv.push_back(v);

  TextParms::const_iterator itr = meta.begin();
  do {
    k = itr->first;
    v = itr->second;
    sv.push_back(k); sv.push_back(v);
    //clog<<"UFMCE4Config::encode> key: "<<k<<", val: "<<v<<endl;
  } while( ++itr != meta.end() );

  return new UFStrings("UFMCE4Config::encodeMeta", sv);
}

string UFMCE4Config::stripErrmsg( string& cmdname ) {
  string errmsg;
  int errIdx = cmdname.find("!!");
  if( errIdx != (int)string::npos ) {
    errmsg = cmdname.substr(2+errIdx);
    // strip error any white spaces out of the cmdname...
    cmdname = cmdname.substr(0, errIdx);
    char* cs = (char*)cmdname.c_str();
    // leading whites:
    while( *cs == ' ' || *cs == '\t' || *cs == '\n'|| *cs == '\r' ) ++cs;
    cmdname = cs;
    // trailing whites:
    int trw = cmdname.find(" ");
    if( trw != (int) string::npos )
      cmdname = cmdname.substr(0, trw);
  }
  return errmsg;
}

// decode & insert hardware parameters
int UFMCE4Config::parse(const UFDeviceAgent::CmdInfo* info, HdwrParms& hp,
		        string& obsmode, string& readoutmode) {
  // assume that a car must be followed by a hard
  bool valid= false;
  string cmdname, cmdimpl;
  obsmode = "stare"; readoutmode = "S1";//defaults
  // and the list of parameter key/values follows the hard... 
  hp.clear();

  for( int i = 0; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    //  should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    string CMDname = cmdname;  UFStrings::upperCase( CMDname );
    string CMDimpl = cmdimpl;  UFStrings::upperCase( CMDimpl );
    if( CMDname.find("CAR") != string::npos ) // not interested, skip epics CAR
      continue;
    else if( CMDimpl.find("HARD") != string::npos )
      valid = true; // valid directive
    else if( CMDname.find("MODE") != string::npos ) { // obs. or readout mode
      if( CMDname.find("OBS") != string::npos ) {
	obsmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Hdwr: obsmode => "<<cmdimpl<<endl;
      }
      else { // assume it is the readout
	readoutmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Hdwr: readoutmode => "<<cmdimpl<<endl;
      }
    } else { // not a mode parameter, put into hash:
      stripErrmsg( cmdname );
      hp.insert(HdwrParms::value_type(cmdname, atoi(cmdimpl.c_str())));
      clog<<"UFMCE4Config::parse> inserted Hdwr: "<<cmdname<<" => "<<cmdimpl<<endl;
    }
  }

  if( !valid ) {
    hp.clear();
    clog<<"UFMCE4Config::parse> Par directive lacks Hard keyword..."<<endl;
    return 0;
  }

  if( readoutmode.length() < 2 ) {
    readoutmode = "S1";
    clog<<"UFMCE4Config::parse> set Hdwr: readoutmode => "<<readoutmode<<endl;
  }

  if( _verbose ) {
    HdwrParms::iterator itr = hp.begin();
    do {
      string key = itr->first;
      double val = itr->second;
      clog<<"UFMCE4Config::parseHdwr> parsed key: "<<key<<", val: "<<val<<endl;
    } while( ++itr != hp.end() );
  }

  return hp.size();
}

// decode & insert meta parameters:
int UFMCE4Config::parse(const UFDeviceAgent::CmdInfo* info, TextParms& mp) {
  // assume that a CAR must be followed by a PARM & META.
  string cmdname;
  string cmdimpl;
  int istart = 0;
  
  cmdname = info->cmd_name[istart];
  UFStrings::upperCase( cmdname );

  if( cmdname.find("CAR") != string::npos ) // skip epics CAR here
    ++istart;

  cmdname = info->cmd_name[istart];
  cmdimpl = info->cmd_impl[istart];
  UFStrings::upperCase( cmdimpl );

  if( cmdname.find("PARM") != string::npos && cmdimpl.find("META") == string::npos ) {
    clog<<"UFMCE4Config::parse> PARM directive lacks META keyword..."<<endl;
    return 0;
  }

  ++istart;
  mp.clear();

  for( int i = istart; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    // should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    stripErrmsg( cmdname );
    mp.insert(TextParms::value_type(cmdname, cmdimpl));
    clog<<"UFMCE4Config::parse> inserted Meta: "<<cmdname<<" => "<<cmdimpl<<endl;
  }

  if( _verbose ) {
    TextParms::iterator itr = mp.begin();
    do {
      string key = itr->first;
      string val = itr->second;
      clog<<"UFMCE4Config::parseMeta> parsed key: "<<key<<", val: "<<val<<endl;
    } while( ++itr != mp.end() );
  }

  return (int)mp.size();
}

// get hdwr parms via ordered vector of strings:
// as cmd action reply of phys parm directive to flam epics:
int UFMCE4Config::flamHdwrCAR(vector<string>& tc, bool append) {
  if( !append ) 
    tc.clear();

  tc.push_back( UFRuntime::numToStr(_desH["FrameTime"]) );
  tc.push_back( UFRuntime::numToStr(_desH["OnSourceTime"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodDwellTime"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodSettleTime"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodDutyCycle"]) );
  tc.push_back( UFRuntime::numToStr(_desH["FrameDutyCycle"]) );
  tc.push_back( UFRuntime::numToStr(_desH["FrameCoadds"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodSettleReads"]) );
  tc.push_back( UFRuntime::numToStr(_desH["Nods"]) );
  tc.push_back( UFRuntime::numToStr(_desH["PixClock"]) );
  tc.push_back(_des_obsmode);
  tc.push_back(_des_readoutmode);

  return tc.size();
}

// get phys & hdwr parms via ordered vector of strings:
// as cmd action reply of meta parm directive to flam epics:
int UFMCE4Config::flamMetaCAR(vector<string>& mc) {
  mc.clear();
  mc.push_back( _metaP["SetPoint"] );
  mc.push_back( _metaP["HeaterPower"] );
  //mc.push_back( _metaP["MetaFrameTime"] );
  return flamHdwrCAR(mc, true);   
}

int UFMCE4Config::flamBiasCAR(vector<string>& tc) { 
  tc.clear();
  int cnt = (int)_biasP.size();
  for( int i = 0; i < cnt; ++i )
    tc.push_back(_biasP[i]);
  return (int)tc.size();
}

int UFMCE4Config::flamPreampCAR(vector<string>& tc) { 
  tc.clear();
  int cnt = (int)_preamP.size();
  for( int i = 0; i < cnt; ++i )
    tc.push_back(_preamP[i]);
  return (int)tc.size();
}

int UFMCE4Config::flamBiasCmds(map< string, string >& biascmds) {
  biascmds.clear();
  biascmds["BiasDatum"] = "ARRAY_INIT_STATE_BIAS";
  biascmds["BiasPower"] = "ARRAY_POWER_UP_BIAS";
  biascmds["BiasPark"] = "ARRAY_POWER_DOWN_BIAS";
  biascmds["ReadAllBias"] = "ARRAY_DAC_READBACK_BIAS";
  biascmds["BiasvWell"] = "ARRAY_WELL 0/1";
  biascmds["BiasvBias"] = "ARRAY_SET_DET_BIAS 0/1/2/3";
  return (int) biascmds.size();
}

int UFMCE4Config::flamPreampCmds(map< string, string >& preampcmds) {
  preampcmds.clear();
  preampcmds["PreampDatum"] = "ARRAY_OFFSET_DEFAULTS_PREAMP";
  preampcmds["PreampPower"] = "ARRAY_POWER_UP_PREAMP";
  preampcmds["PreampPark"] = "ARRAY_POWER_DOWN_PREAMP";
  preampcmds["ReadAllPreamp"] = "ARRAY_OFFSET_READBACK_PREAMP";
  return (int)preampcmds.size();
}

// format active parms into string vec.
int UFMCE4Config::getActive(vector< string >& act) {
  act.clear();
  string md = "Active: Observation Mode == ";
  md += _actv_obsmode + ", Readout Mode == " + _actv_readoutmode;
  act.push_back(md);

  act.push_back("Active Hardware Parameters:");
  int hcnt = _activeH.size();
  HdwrParms::iterator itrh = _activeH.begin();
  while( itrh != _activeH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    double val = itrh->second;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getActive> active Hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    act.push_back(s.str()); delete s.str();
    ++itrh;
  }
  return act.size();   
}

// create the full observation fits header (detector conf & observation info):
// add all active Hardware Parms into UFFITSheader object.
// note: do not clear() or end() the FITS header in this method,
// since it will have existing records and more records may be added later.
int UFMCE4Config::obsFITSheader( UFFITSheader* FITSheader ) {
  FITSheader->add("OBSMODE", _actv_obsmode,    "Observing mode (stare, nod)");
  FITSheader->add("READMODE",_actv_readoutmode,"Detector Readout mode");

  HdwrParms::iterator itrh = _activeH.begin();
  while( itrh != _activeH.end() ) {
    string key = itrh->first;
    double val = itrh->second;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::obsFITSheader> active Hard. hash has blank key= "<< key <<endl;
      continue;
    }
    string FITSkey = FITS_KeyWords[key];
    if( FITSkey.length() > 0 )
      FITSheader->add( FITSkey, val, FITS_Comments[key] );
    ++itrh;
  }

  return FITSheader->size();
}

// Usually this next method is invoked by the Detector Control DeviceAgent,
// to create the basic FITS header of the D.C. DeviceAgent (from FrameConfig and ObsConfig),
// and then adding the MCE4Config physical & hardware parms to FITS header.
UFStrings* UFMCE4Config::statusFITS(UFDeviceAgent* da) {
  // this fails at link time for the original (non Gemini) MCE4Agent, for obvious reasons!
  // UFGemMCE4Agent* gda = dynamic_cast<UFGemMCE4Agent*>(da);
  // this works for both agents (virtuously?)
  UFGemDeviceAgent* gda = dynamic_cast<UFGemDeviceAgent*>(da);
  UFFITSheader* fh = gda->basicFITSheader(); // observation fits values from agent buffs
  int fsz = obsFITSheader(fh);
  if( fsz <= 0 ) { 
    clog<<"UFMCE4Config::statusFITS> error setting FITS header."<<endl;
    return 0;
  }

  return fh->Strings(da->name());
}

// get hdwr parms via one vector of strings:
int UFMCE4Config::getDes(vector<string>& des) {
  des.clear();
  string md = "Desired: Observation Mode == ";
  md += _des_obsmode + ", Readout Mode == " + _des_readoutmode;
  des.push_back(md);

  // the hardware parms:
  des.push_back("Desired Hardware Parameters:");
  int hcnt = _desH.size();
  HdwrParms::iterator itrh = _desH.begin();
  while( itrh != _desH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    double val = itrh->second;
    ++itrh;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    des.push_back(s.str()); delete s.str();
  }
  return des.size();   
}

// get hdwr parms via one map of strings:strings
int UFMCE4Config::getDes(map<string, string>& des) {
  des.clear();
  des["ObsMode"] = _des_obsmode;
  des["ReadoutMode"] = _des_readoutmode;

  int hcnt = _desH.size();
  HdwrParms::iterator itrh = _desH.begin();
  while( itrh != _desH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    double val = itrh->second;
    ++itrh;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<val<<ends;
    des[key] = s.str(); delete s.str();
  } 

  return des.size();   
}

// set desired params
// sets desired hardware parms from hardware input
int UFMCE4Config::setDes(HdwrParms& hp, const string& obsmode, const string& readoutmode) {
  _des_obsmode = obsmode;
  _des_readoutmode = readoutmode;
  hp["TotalFrames"] =  1 + hp["Nods"];

  _desH.clear();
  _desH["FrameCoadds"] = hp["FrameCoadds"];
  _desH["NodSettleReads"] = hp["NodSettleReads"];
  _desH["Nods"] = hp["Nods"];
  _desH["PixClock"] = hp["PixClock"];
  _desH["TotalFrames"] = hp["TotalFrames"];

  _desH["FrameTime"] = hp["FrameTime"];
  _desH["MetaFrameTime"] = hp["MetaFrameTime"];  //not used, just for the record.
  _desH["OnSourceTime"] = hp["OnSourceTime"];
  _desH["ElapsedTime"] = hp["ElapsedTime"];
  _desH["NodDwellTime"] = hp["NodDwellTime"];
  _desH["NodSettleTime"] = hp["NodSettleTime"];
  _desH["NodDutyCycle"] = hp["NodDutyCycle"];
  _desH["FrameDutyCycle"] = hp["FrameDutyCycle"];

  return _desH.size();
}
  

// set desired physical & hardware parms from meta-config input:
//(negative return value means error occurred and should check string errmsg)
int UFMCE4Config::setDes(TextParms& mp, string& errmsg) {
  HdwrParms hp;
  string obsmode, readoutmode;

  // evaluate physical parameters from meta-config params,
  // and get temperature control settings back in meta params.
  if( eval(mp, hp, obsmode, readoutmode, errmsg) < 0 ) return(-1);

  // set all desired parameters (and put T cntrl set into _metaP):
  _metaP = mp;
  return setDes(hp, obsmode, readoutmode);
}

// generate harware params from meta-config params:
//(negative return value means error occurred and caller should check string errmsg)
int UFMCE4Config::eval(TextParms& mp, HdwrParms& hp, string& obsmode, string& readoutmode, string& errmsg) {
  //read file of meta-config parameters if first time:
  //if( _metaConParms.size() <= 0 ) readMetaConfigFile();

  hp.clear();
  obsmode = mp["ObsMode"];
  if( obsmode.length() < 3 ) obsmode = "chop-nod"; // use default if no override.
  readoutmode = mp["ReadoutMode"];
  //if readoutmode mode input does not have S1 in string it means use automatic decision/defaults.
  if( readoutmode.find("S1") == string::npos ) readoutmode = "S1R3";

  mp["MetaFrameTime"] = "?"; //must always have some default non-blank value for reply to epics.

  hp["OnSourceTime"] = ::atof( mp["OnSourceTime"].c_str() );

  //check if nod dwell/settle times are specified, otherwise use current or default values:

  string nodtime = mp["NodDwellTime"];
  if( nodtime.length() > 0 )
    hp["NodDwellTime"] = ::atof( nodtime.c_str() );
  else
    hp["NodDwellTime"] = _activeH["NodDwellTime"];

  if( hp["NodDwellTime"] < 7 ) hp["NodDwellTime"] = 30;

  nodtime = mp["NodSettleTime"];
  if( nodtime.length() > 0 )
    hp["NodSettleTime"] = ::atof( nodtime.c_str() );
  else
    hp["NodSettleTime"] = _activeH["NodSettleTime"];

  if( hp["NodSettleTime"] < 1 ) hp["NodSettleTime"] = 4;

  // This is curvefit to old estimated duty cycle provided by Gemini:
  //  double SCSDutyCycle = 100*( 1 - (pp["ChopFrequency"] * sqrt( pp["ChopThrow"] ))/234 );


  string camMode = mp["CameraMode"];
  UFStrings::lowerCase( camMode );

  // set default temperature control params:
  mp["SetPoint"] = UFRuntime::numToStr(_metaConParms["SetPoint::"+camMode]);
  mp["HeaterPower"] = "Medium";

  return 0;
}

//compute MCE PBC by finding nearest valid frame time:
int UFMCE4Config::pixClockAdj( double& frmTimeSec, const string& rdOutMd ) {
  double frameTime1; //frame time in secs for PBC=1.

  if( rdOutMd.find("S1R3") != string::npos ||
      rdOutMd.find("1S3R") != string::npos ) {
    //quadruple sampling (with 2 output clamp refs and 1 column clamp ref):
    frameTime1 = _frameTimes["S1R3"];
  }
  else if( rdOutMd.find("S1R1") != string::npos ||
	   rdOutMd.find("1S1R") != string::npos )
    {
      //two types of double sampling are possible:
      if( rdOutMd.find("CR") != string::npos )
	frameTime1 = _frameTimes["S1R1_CR"]; // Column clamp Ref:
      else
	frameTime1 = _frameTimes["S1R1"]; // Output clamp Ref:
    }
  else frameTime1 = _frameTimes["S1"]; //single sampling (no ref):

  //compute PBC and round off:
  double PBC = _MAX2_( _minPBC, rint( frmTimeSec / frameTime1 ) );
  frmTimeSec = PBC * frameTime1;
  return _MAX2_( 1, (int)PBC );
}

// Commanding and reading of detector Bias or PreAmp DACs:
int UFMCE4Config::cmdDAC(UFDeviceAgent::CmdInfo* info, bool& readings, bool& didInitBias, float flush) {
  // assume that a car must be followed by a Bias or Preamp directive
  string cmdname;
  string cmdimpl;
  int offset= 0;
   
  cmdname = info->cmd_name[0];
  UFStrings::upperCase( cmdname );

  if( cmdname.find("CAR") != string::npos ) // not interested, skip it.
    offset = 1;
 
  readings = false;
  bool bias= false, preamp= false;
  string reply, errmsg;
  int nret= 0;
  clog<<"UFMCE4Config::cmdDAC> processing " << info->cmd_name.size() <<"-"<< offset
      <<" cmd -> directive pairs..."<<endl;

  for( int i = offset; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    //  should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    UFStrings::upperCase( cmdimpl );
    if( cmdimpl.find("DC:BIPASSENBLIN") != string::npos )
      break; //this is not a DAC cmd (just epics stuff) so skip it.

    errmsg = stripErrmsg( cmdname );
    clog<<"UFMCE4Config::cmdDAC> "<<cmdname<<" : "<<cmdimpl<<endl;
    int nc;

    if( cmdimpl.find("READ") != string::npos ||
	cmdimpl.find("ADRB") != string::npos ||
	cmdimpl.find("AORP") != string::npos ) {
      readings = true;
      if( cmdimpl.find("BIAS") != string::npos ||
	  cmdimpl.find("ADRB") != string::npos ) bias = true;
      if( cmdimpl.find("PREAMP") != string::npos ||
	  cmdimpl.find("AORP") != string::npos ) preamp = true;
    }

    if( cmdimpl.find("INIT") != string::npos && cmdimpl.find("BIAS") != string::npos )
      didInitBias = true;

    if( _devIO ) {
      if( cmdimpl.find("POWER_UP") != string::npos ) { //start idle clocking before power up.
	string repli;
	_devIO->submit("CT 0", repli, flush);
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<repli<<endl;
	_devIO->submit("START", repli, flush);
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<repli<<endl;
      }
      nc = _devIO->submit( cmdimpl, reply, flush );
    }
    else {
      if( readings ) {
	if( bias )
	  _simDACBias(reply);
	else if( preamp )
	  _simDACPreamp(reply);
	else
	  reply = "simulation reading ?";
      }
      else reply = "simulation";
      nc = 1;
      clog<<"UFMCE4Config::cmdDAC> no device connection... assuming simulation..."<<endl;
      if( cmdimpl.find("POWER_UP") != string::npos )
	clog<<"UFMCE4Config::cmdDAC> normally would set MCE to idle CT..."<<endl;
      if( cmdimpl.find("WELL") != string::npos )
	_wellDepth = ::atoi( (cmdimpl.substr( cmdimpl.find("WELL")+4 )).c_str() );
      if( cmdimpl.find("DET_BIAS") != string::npos )
	_biasLevel = ::atoi( (cmdimpl.substr( cmdimpl.find("DET_BIAS")+8 )).c_str() );
    }

    if( nc <= 0 ) {
      reply = errmsg + reply;
      nret = nc;
    }
    else if( reply.find("Invalid") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("Invalid") );
      nret = 0;
    }
    else if( reply.find("Error") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("Error") );
      nret = 0;
    }
    else if( reply.find("WARNING") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("WARNING") );
      nret = 0;
    }
    else {
      if( readings ) {
	if( bias )
	  nret = _DACBiasReadings(reply);
	else if( preamp )
	  nret = _DACPreampReadings(reply);
      }
      else {
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
	nret = nc;
      }
    }

    info->cmd_reply.push_back(reply);
    if( nret <= 0 ) return nret;
  }

  return nret;
}

int UFMCE4Config::_DACBiasReadings(const string& mcebias) {
  // parse the mce bias readings (reply) into the map
  size_t eqpos = mcebias.find("=");
  size_t eolpos = mcebias.find("\r", eqpos);
  size_t len = eolpos - eqpos;
  string line= "";
  int i= 0;
  while( eqpos != string::npos && i < 24 ) {
    line = mcebias.substr(++eqpos, len);
    UFFITSheader::rmJunk(line); // eliminate \n \r \t and extraneous spaces
    _biasP[i++] = line;
    eqpos = mcebias.find("=", eolpos);
    eolpos = mcebias.find("\r", eqpos);
    len = eolpos - eqpos;
  }
  size_t Vpos = mcebias.find("V_DETGRV");
  if( Vpos != string::npos ) {
    eqpos = mcebias.find("=",Vpos);
    eolpos = mcebias.find("(", eqpos);
    line = mcebias.substr( ++eqpos, eolpos - eqpos );
    _VdetgrvDACval = ::atoi( line.c_str() );
  }
  return (int)_biasP.size(); 
}

int UFMCE4Config::_DACPreampReadings(const string& mcepreamp) {
  // parse the mce preamp readings (reply) into the map
  size_t eqpos = mcepreamp.find("=");
  //size_t eolpos = mcepreamp.find("\r", eqpos);
  size_t eolpos = mcepreamp.find("?", eqpos);
  size_t len = eolpos - eqpos;
  string line= "";
  string temp;
  string epicsRec;
  int i= 0;
  while( eqpos != string::npos && i < 32 ) { //F2 uses first 32 preamp channels out of 32.
    line = mcepreamp.substr(++eqpos, len);
    UFFITSheader::rmJunk(line); // eliminated \n \r \t and extraneous spaces
    //_preamP[i++] = line;
    temp = line.substr(0, line.find("("));
    while (temp.find(" ") != string::npos) {
      temp.replace(temp.find(" "), 1, "");
    }
    _preamP[i++] = temp;
    eqpos = mcepreamp.find("=", eolpos);
    //eolpos = mcepreamp.find("\r", eqpos);
    eolpos = mcepreamp.find("?", eqpos);
    //clog << "TEST " << i << " " << eqpos << " " << eolpos << endl;
    //clog << temp << endl;
    len = eolpos - eqpos;
  }
  return (int)_preamP.size(); 
}

void UFMCE4Config::_simDACBias( string& reply ) {
      reply  = "ARRAY_DAC_READBACK_BIAS\r";
      reply += " Bias Board DAC Table Listing....\r";
      reply += "Channel #  0 : V_CASUC      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";
      reply += "Channel #  1 : V_RSTUC      =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";
      reply += "Channel #  2 : V_DETGRDRNG  =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";
      reply += "Channel #  3 : V_DETGRV     =    42 (0x2a)   ;  Def:042, Min:019, Max:046\r";
      reply += "Channel #  4 : V_CLCOL      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";
      reply += "Channel #  5 : V_SHPCRG     =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";
      reply += "Channel #  6 : V_SH         =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";
      reply += "Channel #  7 : V_OFFSET     =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";     
      reply += "Channel #  8 : V_CLOUT      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";    
      reply += "Channel #  9 : V_PW         =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 10 : V_VSS_1      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 11 : V_VSS_2      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 12 : V_VSS_D      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";     
      reply += "Channel # 13 : V_VGG_1      =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";    
      reply += "Channel # 14 : V_VGG_2      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";     
      reply += "Channel # 15 : V_VGG_3      =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";     
      reply += "Channel # 16 : V_VGG_4      =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";     
      reply += "Channel # 17 : V_IMUX       =    28 (0x1c)   ;  Def:028, Min:025, Max:031\r";     
      reply += "Channel # 18 : V_CAP        =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";     
      reply += "Channel # 19 : V_GATE_HI    =   102 (0x66)   ;  Def:102, Min:091, Max:112\r";     
      reply += "Channel # 20 : V_GATE_LO    =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 21 : V_GATE       =   102 (0x66)   ;  Def:102, Min:000, Max:255\r";     
      reply += "Channel # 22 : V_NEG        =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 23 : V_SUB        =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";     
}

void UFMCE4Config::_simDACPreamp( string& reply ) {
      reply  = "ARRAY_OFFSET_READBACK_PREAMP\r";
      reply += " Preamp DAC Offset Table Listing....\r";
      reply += "Channel #  0 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  1 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  2 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  3 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  4 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  5 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  6 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  7 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  8 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  9 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 10 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 11 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 12 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 13 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 14 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 15 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 16 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 17 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 18 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 19 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 20 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 21 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 22 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 23 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 24 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 25 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 26 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 27 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 28 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 29 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 30 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 31 =      255 (0xff)   ;   DEF:255\r";
}

// MCE4 terminator is carriage-return:
string UFMCE4Config::terminator() { return "\r"; }

// MCE4 prefix is none:
//string UFMCE4Config::prefix() { return ""; }

vector< string >& UFMCE4Config::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("STATUS"); // text or FITS or OBSFITS
  _queries.push_back("CONFIG");

  return _queries;
}

vector< string >& UFMCE4Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  // agent Par directives: (trecs only?)
  _actions.push_back("HAR");
  _actions.push_back("PHY");
  _actions.push_back("meta"); _actions.push_back("Meta"); _actions.push_back("META");

   // agent ObsSetup/ Acq/Seq directives:
  _actions.push_back("COMM"); //comment
  _actions.push_back("DATA"); 
  _actions.push_back("DIR"); 
  _actions.push_back("DHS"); 
  _actions.push_back("DS9"); 
  _actions.push_back("EXPTIME"); 
  _actions.push_back("EXPCNT"); 
  _actions.push_back("DHS"); 
  _actions.push_back("FILE"); 
  _actions.push_back("FITS"); 
  _actions.push_back("HOST"); 
  _actions.push_back("INDEX"); 
  _actions.push_back("INFO"); 
  _actions.push_back("LABEL");
  _actions.push_back("LUT");
  _actions.push_back("NFS"); 
  _actions.push_back("OBSSETUP"); // (re)set current obsconf and (optionally) set primary FITS header and send it to edtd
  _actions.push_back("OBSCONF"); // (re)set current obsconf and (optionally) set primary FITS header and send it to edtd
  _actions.push_back("USER"); // 
  _actions.push_back("USR"); // 
  _actions.push_back("PNG"); // 
  _actions.push_back("JPEG"); // 
  _actions.push_back("JPG"); // 
  _actions.push_back("QLOOK"); // 

  // gemini sequencer directives
  _actions.push_back("DATUM"); // CT 0
  _actions.push_back("ABORT"); // stop mce4, abort edtd, no FITS
  _actions.push_back("CLOSE"); //?
  _actions.push_back("CONF"); // request current obsconf?
  _actions.push_back("CONT"); // continue paused obs? (relay to edtd)
  _actions.push_back("OPEN"); //?
  _actions.push_back("PARK"); // CT 0
  _actions.push_back("PAUSE"); // pause current obs (relay to edtd)
  _actions.push_back("START"); // send current obsconf to edtd get reply back that edt is waiting for images, send start/run to MCE4
  _actions.push_back("STOP"); // get final FITS and send to UFEdtd then stop
  _actions.push_back("QLOOK"); // ?
  _actions.push_back("TEST"); // ?
  _actions.push_back("WCP"); // ?


 // these are known mce4 dierctives: 
  _actions.push_back("?");
  _actions.push_back("DAC");
  _actions.push_back("DLEN");
  _actions.push_back("HELP");
  _actions.push_back("CHOP");
  // setting cycle types and loading acq. variables does not affect
  // the current/active acquisition (non-idle mce4). 
  // the new parameters will affect the next acq. (run/start):
  _actions.push_back("CT");
  _actions.push_back("CYCLE");
  _actions.push_back("DLAB");
  _actions.push_back("DLAP");
  _actions.push_back("DLSB");
  _actions.push_back("DLSP");
  _actions.push_back("DSSB");
  _actions.push_back("DSSP");
  _actions.push_back("DVAR");
  _actions.push_back("DUMP_PERIOD");
  _actions.push_back("LDVAR");
  _actions.push_back("NOD");
  _actions.push_back("PBC");
  _actions.push_back("REP");
  _actions.push_back("RESET");
  // note that "run" waits for current processing (in mce4) to complete
  _actions.push_back("RUN");
  // to switch out of sim mode issue "sim 0"
  _actions.push_back("SIM");
  // "start" will abort whatever is currently running on mce4 and start new acq.
  _actions.push_back("TIME");

  // long versions of dac preamp * bias cmds:
  _actions.push_back("PREAMP_DAC");
  _actions.push_back("DAC_SET_SINGLE_PREAMP");
  _actions.push_back("DAC_LAT_SINGLE_PREAMP");
  _actions.push_back("DAC_LAT_ALL_PREAMP");
  _actions.push_back("BIAS_DAC");
  _actions.push_back("DAC_SET_SINGLE_BIAS");
  _actions.push_back("DAC_LAT_SINGLE_BIAS");
  _actions.push_back("DAC_LAT_ALL_BIAS");
  _actions.push_back("DAC_MODE_BIAS");
  _actions.push_back("DAC_REF_ENABLE_BIAS");
  _actions.push_back("DUMP_PERIOD");

  // new dac commands:
  _actions.push_back("ARRAY_TEMP"); // in kelvin
  _actions.push_back("ARRAY_WELL"); _actions.push_back("AWELL");
  _actions.push_back("ARRAY_VGATE"); _actions.push_back("AVGATE");
  _actions.push_back("ARRAY_SET_DET_BIAS"); _actions.push_back("ASDB");     
  _actions.push_back("ARRAY_PW_BIAS"); _actions.push_back("APWB"); // password "ENGINEERING"
  _actions.push_back("ARRAY_INIT_STATE_BIAS"); _actions.push_back("AISB");
  _actions.push_back("ARRAY_POWER_UP_BIAS"); _actions.push_back("APUB");
  _actions.push_back("ARRAY_POWER_DOWN_BIAS"); _actions.push_back("APDB");
  _actions.push_back("ARRAY_READBACK_BIAS");
  _actions.push_back("ARRAY_DAC_READBACK_BIAS"); _actions.push_back("ADRB");
  _actions.push_back("ARRAY_OFFSET_DEFAULTS_PREAMP"); _actions.push_back("AODP");
  _actions.push_back("ARRAY_OFFSET_GLOBAL_PREAMP"); _actions.push_back("AOGP");
  _actions.push_back("ARRAY_OFFSET_SINGLE_PREAMP"); _actions.push_back("AOSP");
  _actions.push_back("ARRAY_OFFSET_ADJUST_PREAMP"); _actions.push_back("AOAP");
  _actions.push_back("ARRAY_OFFSET_READBACK_PREAMP"); _actions.push_back("AORP");
  
  _actions.push_back("ARRAY_PATTERN_CLOCK_LEN"); _actions.push_back("APCL");
  _actions.push_back("ARRAY_SAMPLING_TYPE"); _actions.push_back("AST");
  _actions.push_back("LOAD_PAT_RAM"); _actions.push_back("LPR");
  return _actions;
}

int UFMCE4Config::validCmd(const string& c) {
  char* cs = (char*)c.c_str();
  while( *cs == ' ' || *cs == '\t' ) ++cs; //eliminate leading whites
  string cc = cs;
  UFStrings::upperCase(cc); // compare as uppercase
  
  // mce4 always echos cmd back along with optional reply info.
  vector< string >& av = actionCmds();
  int i= 0, cnt = (int)av.size();
  while( i < cnt ) {
    if( cc.find(av[i++]) != string::npos ) {
      return 1; 
    }
  }
  
  vector <string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( cc.find(qv[i++]) != string::npos )
      return 1;
  }

  int vc = validAcq(cc);
  if( vc >= 0 )
    return 1;
 
  clog<<"UFMCE4Config::validCmd> unknown cmd: "<<cc<<endl; 
  return vc;
}

#endif // __UFMCE4Config_cc__
