#if !defined(__UFAcqCtrl_h__)
#define __UFAcqCtrl_h__ "$Name:  $ $Id: .UFAcqCtrl.h,v 0.11 2005/09/16 22:54:02 hon Exp $";
#define __UFAcqCtrl_H__(arg) const char arg##UFAcqCtrl_h__rcsId[] = __UFAcqCtrl_h__;

// c++
using namespace std;
#include "string"
#include "deque"
#include "map"
#include "multimap.h"

// uflib
#include "UFDeviceAgent.h"
#include "UFEdtDMA.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFFlamObsConf.h"
#include "UFFITSClient.h"
#include "UFFITSheader.h"
#include "UFMCE4Config.h"

// this is a 'control' client to ufedtd, and a FITS client
// to all ufagents
class UFAcqCtrl : public UFClientSocket {
  // for use by any daemon that wishes to command the ufedtd
  // used by the F2 detector agent to coordinate frame output
  // by mce4 with frame capture/acquitision by ufedtd
  // additional support for FITS headers (primary and extesion)
  // via static funcs. that use either epics sad or agent fits req.
  // it would be most efficient to use the fits sutff in the edtdma post acq.
  // function or the dhs client observation thread
public:
  struct Observe {
    // support obsetup command
    string _exptime, _dhswrite, _dhsqlook, _comment, _nfshost, _rootdir, _file, _fileIdx, _jpeg, _ds9; // archive
    UFFlamObsConf *_obscfg;
    Observe(double exptime= 1.0,
	    const string& dhslabel= "", const string& dhsqlook= "", const string& comment= "",
	    const string& file="", const string& fileIdx="0", const string& jpeg= "", const string& ds9= "",
	    const string& rootdir="", const string& nfshost="");
    inline ~Observe() { delete _obscfg; }
    string asStrings(deque< string >& text); // helper to formulate cmd bundle to edtd or status from it?
  };

  static float _timeout;
  static bool _sim, _verbose, _observatory; // simulation may include observatory info?
  static UFAcqCtrl::Observe* _obssetup; // current observation setup, if exposing (not idle)
  static map< string, UFAcqCtrl::Observe* > _Observations; // history of observations

  UFAcqCtrl(const string& agenthost, const string& framehost);
  UFAcqCtrl(const string& agenthost, const string& framehost, map< string, int>& portmap);
  inline virtual ~UFAcqCtrl() {}

  UFAcqCtrl::Observe* obsSetup(UFDeviceAgent::CmdInfo* act);
  // additional (optional?) attributes of the aquisition (ACQ) directive
  UFAcqCtrl::Observe* UFAcqCtrl::attributes(UFDeviceAgent::CmdInfo* act);

  int defaultPorts(map< string, int>& portmap);
  string submitEdtd(); // submit acq directive(s) vi UFStrings to edtd
  string submitDhsc(UFSocket* replicant); // send flam2obsconf to dhsclient (replicant client)
  int connectFITS(string& agenthost, const string& exclude= "");
  int connectEdtd(const string& clientname, string& frmhost, int frmport= 52001, bool block= false);
  UFInts* simAcq(const string& name, int index= 0);

  // these should return the datalabel and local archive file name:
  // start observation (connect to ufedtd if necessary), and set FITS
  // prmary and extension headers
  string startObs(UFMCE4Config* mce); // mce4d sends acq action to edt then starts mce4
  string startObs(UFSocket* replicant); // edtd sends obssetup to dhs (replican) client
  // start a (test) discard observation 
  string discardObs(UFMCE4Config* mce4); // mce4d sends acq action to edt then starts mce4
  string discardObs(UFSocket* replicant); // edtd sends obssetup to dhs (replican) client
  // stop observation (connect to ufedtd if necessary)
  // data should be saved
  string stopObs(UFMCE4Config* mce); // mce4d sends acq stop action to edt then stops mce4
  string stopObs(UFSocket* replicant); // mce4edtd sends acq stop action to dhsclient
  // abort observation (connect to ufedtd if necessary)
  // data should be discarded
  string abortObs(UFMCE4Config* mce); // mce4d sends acq stop action to edt then stops mce4
  string abortObs(UFSocket* replicant); // mce4edtd sends acq stop action to dhsclient

  // return file decr. &  FITS header from opened file, seeking to start of first data frame
  int openFITS(const string& filename, UFStrings*& fitsHdr, int& w, int& h, int& frmtotal);
  // return FITS data from open file, seeking to next data frame
  UFInts* seekFITSData(const int fd, const int w= 2048, const int h= 2048);

  // allocate & fetch/set FITS header for obs. datalabel, using agents (default) or epics sad:
  // primary header (initial or final vals)
  UFStrings* fitsHdrPrm(const string& datalabel, UFFITSheader& fh, const string& epics= "");
  UFStrings* fitsHdrExt(const string& datalabel= "", const string& epics= ""); // extension header

  // extract all headers for the given datalabel from the _FITSheaders history
  // (for real DHS datalabels will likely be one primay with one or more extensions,
  // but for Test datalabels, who knows):
  //int fitsHdrs(const string& datalabel, deque< UFStrings* >& fits);

  // integer key value
  static int fitsInt(const string& key, char* fitsHdr);
  // float/double key value
  static double fitsFlt(const string& key, char* fitsHdr);
  // string key value
  static string fitsStr(const string& key, char* fitsHdr);

  // just a reminder that this is the actuall ufedtd frame socket client connection:
  inline UFClientSocket* getFrmSoc() { return this; }

protected:
  bool _connected;
  string _agthost, _frmhost;
  UFFITSClient* _clientFITS;
  UFSocket::ConnectTable _fitsoc;
  map< string, int > _portmap; // 'agent@host', portNo
  //multimap< string, UFStrings* > _FITSheaders; // datalabel, FITS header (primary & extensions)
};

#endif // __UFAcqCtrl_h__
