#if !defined(__UFGemMCE4Agent_cc__)
#define __UFGemMCE4Agent_cc__ "$Name:  $ $Id: UFGemMCE4Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemMCE4Agent_cc__;

#include "UFGemMCE4Agent.h"
__UFGemMCE4Agent_H__(GemMCE4Agent_cc);

#include "UFMCE4Config.h"
__UFMCE4Config_H__(GemMCE4Agent_cc);

#include "UFEdtDMA.h"
__UFEdtDMA_H__(GemMCE4Agent_cc);

#include "UFFlamObsConf.h"
__UFFlamObsConf_H__(GemMCE4Agent_cc);

#include "UFSADFITS.h"

#include "sys/types.h"
#include "sys/wait.h"

// these statics might be of use to friend classes:
string UFGemMCE4Agent::_archvRootDir = "/data/flam";
UFAcqCtrl* UFGemMCE4Agent::_AcqCtrl = 0; // should be a duplicate of UFMCE4Config's

string UFGemMCE4Agent::_frmhost; // = "localhost";
string UFGemMCE4Agent::_FrmAcqVersion = "ufgedtd";
int UFGemMCE4Agent::_frmport = 52001;
bool UFGemMCE4Agent::_idle = true;

// from trecs (comment out for now):
//size of Raytheon det.arr. with CRC-774 MUX:

float UFGemMCE4Agent::_exptime= 1.0;
int UFGemMCE4Agent::_arrayColumns = 2048;
int UFGemMCE4Agent::_arrayRows = 2048;
int UFGemMCE4Agent::_arrayChannels = 16;
UFFlamObsConf* UFGemMCE4Agent::_ObsConf = 0;
UFFlamObsConf* UFGemMCE4Agent::_nextObsConf = 0;
UFFlamObsConf* UFGemMCE4Agent::_abortedObsConf = 0;

// these static objects keep the status/config of FrameAcqServer:
int UFGemMCE4Agent::_bgADUStart = 0; //0 indicates to set with first value from _FrmConf
int UFGemMCE4Agent::_rdADUStart = 0;
float UFGemMCE4Agent::_bgWellStart = 0.0;
float UFGemMCE4Agent::_bgWellMax = 0.0;
float UFGemMCE4Agent::_bgWellMin = 100.0;
string UFGemMCE4Agent::_ObsCompStat = "unknown";
string UFGemMCE4Agent::_warmmux = "false";

//_devhost is host on which other Device Agents are running (but default is the same host).
string UFGemMCE4Agent::_devhost = "";

// (internal) simulation mode:
//map<string, double> UFGemMCE4Agent::_Sim;

// private virtual:
// helper for ctors:
void UFGemMCE4Agent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //_confGenSub = _epics + ":dc:configG";
    _confGenSub = "";
    //_heart = _epics + ":dc:heartbeat.INP";
    _heart = _epics + ":dc:heartbeat.VAL";
    //    _confGenSub = _epics + ":dc:configG";
    // presumably these are == 0 when lvdt is not powered on, < 0 when com. error
     for( int i = 0; i < (int) UFSADFITS::_dcsad->size(); ++i )
      //_StatRecs.push_back((*UFSADFITS::_dcsad)[i] + ".INP");
      _StatRecs.push_back((*UFSADFITS::_dcsad)[i] + ".VAL");
    _statRec = _StatRecs[0];
  }
  else {
    _epics = "false";
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
}

//public:
// ctors:
UFGemMCE4Agent::UFGemMCE4Agent(int argc, char** argv,
			       char** envp) : UFGemDeviceAgent(argc, argv, envp) {
  UFRndRobinServ::threadedServ();
  //_Update = 2.0;
  _flush = 0.05; //  0.1; //0.35;
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _heart = _epics + ":dc:heartbeat";
  _statRec = ""; // CAR should be set at start of each acquisition request packet
  _warmmux = "false";
  _dhsLabel= "Test";
  _comment= "";
  _dhsWrite = false;
  _archive = false;
  //  _confGenSub = _epics + ":dc:configG";
  //_queryLastTime = ::time(0);
  clog<<"UFGemMCE4Agent> archive Root Directory: "<<_archvRootDir<<endl;
}

UFGemMCE4Agent::UFGemMCE4Agent(const string& name, int argc,
			       char** argv, char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  UFRndRobinServ::threadedServ();
  //_Update = 2.0;
  _flush = 0.05; //  0.1; //0.35;
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _heart = _epics + ":dc:heartbeat";
  _statRec = ""; // CAR should be set at start of each acquisition request packet
  _warmmux = "false";
  _dhsLabel= "Test";
  _comment= "";
  _dhsWrite = false;
  _archive = false;
  //  _queryLastTime = ::time(0);
  //_confGenSub = _epics + ":dc:configG";
  clog<<"UFGemMCE4Agent> archive Root Directory: "<<_archvRootDir<<endl;
}

// static funcs:
int UFGemMCE4Agent::main(int argc, char** argv, char** envp) {
  UFGemMCE4Agent ufs("UFGemMCE4Agent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemMCE4Agent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  if( clname.find("trecs:") != string::npos )
    _epics = "trecs";
  else if( clname.find("miri:") != string::npos )  
    _epics = "miri";
  else if( clname.find("flam:") != string::npos )  
    _epics = "flam";

  if( !( _epics == "trecs" || _epics == "miri" || _epics == "flam") )
    return clname;

  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string linecnt =  _epics + genSub + ".VALA";
  string positions =  _epics + genSub  + ".VALB";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemMCE4Agent::newClient> Epics client." <<clname<<endl;

  /* don't care right now
  int nr = getEpicsOnBoot(_epics, linecnt, _StatList);
  if( nr > 0 && _StatList.size() > 0 ) {
    linecnt = atof(_StatList[0].c_str());
  }

  nr = getEpicsOnBoot(_epics, positions, _StatList);
  */

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemMCE4Agent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  _config = new UFMCE4Config(); // needs to be proper device subclass
  // defaults for trecs:
  _config->_tsport = 7008;
  _config->_tshost = "flamperle"; //"192.168.111.125";

  string arg;
  arg = findArg("-help"); 
  if( arg != "false" ) {
    clog<<"UFGemMCE4Agent::options> [-v] [-sim] [-nofits] [-noedtd] [-take] [-[no]epics name] [-warm [kelvin]] [-conf db] [-heart db][-flush sec.] [-tsport annexport] [-tshost annexIP] [-frmport 52000] [-frmhost localhost] [-devagshost localhost]"<<endl;
    exit(0);
  } 

  arg = findArg("-warm");
  if( arg != "false" && arg != "true" )
    _warmmux = arg;
  else if( arg == "true" )
    _warmmux = "77";

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());
  
  arg = findArg("-frmport"); 
  if( arg != "false" && arg != "true" )
    _frmport = atoi(arg.c_str());

  arg = findArg("-frmhost");
  if( arg != "false" && arg != "true" )
    _frmhost = arg;
  
  arg = findArg("-devagshost");
  if( arg != "false" && arg != "true" )
    _devhost = arg;
  else
    _devhost = UFRuntime::hostname();

  //_ls340host = _devhost; // do not worry about getting array temp. values to send to mcde4!
  
  arg = findArg("-sim"); // epics host/db name
  if( arg != "false" )
    _sim = UFAcqCtrl::_sim = true;

  arg = findArg("-take"); // epics host/db name
  if( arg != "false" ) {
    UFMCE4Config::_take = true; // and use dummy simple fits header?
    UFMCE4Config::_edtd = false;
    UFMCE4Config::_fits = false;
  }
  clog<<"UFGemMCE4Agent::options> _take: "<<UFMCE4Config::_take<<ends;
  if( UFMCE4Config::_take )
    clog<<", ACQ directive will spawn ufgtake child proc. NO other EDT app. should run (no ufgedtd)!"<<endl;
  else
    clog<<", ACQ directive will NOT spawn ufgtake child proc. (ufgedtd agent should recv. ACQ directive)."<<endl;

  arg = findArg("-nofits"); // epics host/db name
  if( arg != "false" ) UFMCE4Config::_fits = false;
  clog<<"UFGemMCE4Agent::options> _fits: "<<UFMCE4Config::_fits<<ends;
  if( UFMCE4Config::_fits )
    clog<<", ACQ directive will also perform full system FITS update"<<endl;
  else
    clog<<", ACQ directive will NOT perform full system FITS update..."<<endl;


  arg = findArg("-noedtd"); // epics host/db name
  if( arg != "false" ) UFMCE4Config::_edtd = false;
  clog<<"UFGemMCE4Agent::options> _edtd: "<<UFMCE4Config::_edtd<<ends;
  if( UFMCE4Config::_edtd )
    clog<<", ACQ directive will be forwarded to ufgedtd."<<endl;
  else
    clog<<", ACQ directive will NOT be forarded to ufgedtd."<<endl;


  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = UFMCE4Config::_flush = atof(arg.c_str());

  //UFTermServ::_verbose = true;
  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
  }
  if( _vverbose )
    _verbose = true;
  UFMCE4Config::_verbose = _verbose;
  UFTermServ::_verbose = _verbose;
  /*
  arg = findArg("-scs");
  if( arg != "false" ) {
    UFMCE4Config::_SCSDelta = atof(arg.c_str());
  }
  */
  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport > 7000 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52008;
  }
  //if( _verbose )
    clog<<"UFGemMCE4Agent::options> _heart: "<<_heart<<endl;

  return port;
}

void UFGemMCE4Agent::startup() {
  sigWaitThread(UFGemDeviceAgent::sighandler); // create a thread devoted to all signals of interest 
  //setSignalHandler(UFGemDeviceAgent::sighandler); // single-threaded handler seems disfunctional
  //clog << "UFGemMCE4Agent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  UFTermServ* ts = init(_config->_tshost, _config->_tsport);
  if( ts == 0 && !_sim ) {
    clog<<"UFGemMCE4Agent::startup> initial connect to UFMCE4 Perle port failed or returned bad boot prompt, aborting..."<<endl;
    exit(1);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog<<"UFGemMCE4Agent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;
}

UFTermServ* UFGemMCE4Agent::init(const string& tshost, int tsport) {
  if( _capipe == 0 && _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics); // this popens ufcaputarr
    if( _capipe )
      clog<<"UFGemMCE4Agent::init> Epics CA PUT started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemMCE4Agent::init> Ecpics CA PUT child proc. failed to start"<<endl;
    /*
    if( _capipeWr < 0 && _capipeRd < 0 ) {
      int stat = pipeToFromEpicsCAchild(_epics);
      if( stat < 0 )
        clog<<"UFGemMCE4Agent::init> Ecpics CA GET child proc. failed to start"<<endl;
      else
        clog<<"UFGemMCE4Agent::init> Epics CA GET started"<<endl;
    }
    */
  }
  // make sure _config points to MCE4Config
  UFMCE4Config* mce = dynamic_cast< UFMCE4Config* > (_config);  
  _config = mce;

  // connect to ufedtd:
  string host = hostname();
  _AcqCtrl = UFMCE4Config::_AcqCtrl = new UFAcqCtrl(host, host); // one-time alloc

  if( UFMCE4Config::_edtd ) { // forward acq cmds to edtd
    bool block = true;
    clog<<"UFGemMCE4Agent::init> connect to ufgedtd (support acq commands)..."<<endl;
    int edtdsoc = _AcqCtrl->connectEdtd(name(), host, 52001, block);
    if( edtdsoc <= 0 )
      clog<<"UFGemMCE4Agent> failed to connect to ufedtd..."<<endl;
  }
  if( UFMCE4Config::_fits ) {
    clog<<"UFGemMCE4Agent::init> perform FITS connect to agents..."<<endl;
    int fitsoc = _AcqCtrl->connectFITS(host, "mce and edt"); // exclude self (mce) and already connected to edt
    if( fitsoc <= 0 )
    clog<<"UFGemMCE4Agent::init> failed to connect to device agents for FITS info, try later ..."<<endl;
  }

  UFTermServ* ts= 0;
  if( ! _sim ) {
    // connect to the device:
    ts =_config->connect();
    if( _config->_devIO == 0 ) {
      clog<<"UFGemMCE4Agent> failed to connect to device port..."<<endl;
    }
    else {
      ts->resetEOTR(">>"); // mce4 returns prompt
      string booted = UFTermServ::bootPrompt();
      // recv any buffered boot prompt..
      clog<<"UFGemMCE4Agent::init> boot prompt recv'd: "<<booted<<endl;
    }
  }

  return ts;
} // UFGemMCE4Agent::init

// Send heartbeat to EPICS,
// monitor status of MCE4, the FrameAcqServer,
// the detector array temperature by querying the Lakeshore340,
// and check for the telescope beam switch (nodding) completion:

void* UFGemMCE4Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  if( !_idle ) { // check acq status via semaphores:
    string edtstat = UFEdtDMA::status();
    clog<<edtstat<<endl;
  }

  if( _capipe == 0 && _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemMCE4Agent::ancillary> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemMCE4Agent::ancillary> Ecpics CA child proc. failed to start"<<endl;
  }

  // update the heartbeat:
  time_t clck = ::time(0);
  if( _capipe && _heart != "" )
    updateEpics(_heart, UFRuntime::numToStr(clck));

  // check MCE status once a minute
  // (or always on startup when init _nDTqueryFail = -1):
  /*
  if( ((int)(clck - _queryLastTime)) >= _queryDTperiod || _nDTqueryFail < 0) {
    _queryLastTime = clck;
    string detKelvin;

    if( _queryLS340DT( detKelvin ) > 0 ) {
      if( detKelvin.length() > 0 )
	_nDTqueryFail = 0;
      else
	++_nDTqueryFail;
    }
    else ++_nDTqueryFail;

    if( _nDTqueryFail >= _maxDTqueryFail ) {
      detKelvin = "299";
      clog<<"UFGemMCE4Agent::ancillary> Detector temperature is unknown, assuming worst case: "
	  <<detKelvin<<"K"<<endl;
      if( _epics != "false" ) {
	string message = "WARN: Det. Temperature is unknown!";
	sendEpics( _epics + ":sad:observationStatus", message );
	sendEpics( _epics + ":sad:dcState", message );
      }
    }

    if( _frmAcqNow ) clog<<"\n";
    clog<<"UFGemMCE4Agent> time="<<currentTime()<<" \n";
    int mceStat = 0; //this should become > 0 if det.temp got sent to MCE.

    if( _config->_devIO && detKelvin.length() > 0 ) {
	string mceKelvin = "ARRAY_TEMP " + detKelvin;
	clog<<"UFGemMCE4Agent::ancillary> Sending MCE4 the Det. Temp.: "<<mceKelvin<<" (K)"<<endl;
	string mceReply;
	mceStat = _config->_devIO->submit(mceKelvin, mceReply, _flush);

	if( mceStat <= 0 || mceReply.length() == 0 ) {
	  string message = "ERR: failed to send Det.Temp. to MCE !";
	  clog<<"UFGemMCE4Agent::ancillary> "<<message<<endl;
	  if( _epics != "false" ) {
	    sendEpics( _epics + ":sad:observationStatus", message );
	    sendEpics( _epics + ":sad:dcState", "failed MCE-I/O: check Terminal Server ?" );
	  }
	}
      }
    else {
      clog<<"UFGemMCE4Agent::ancillary> Detector temperature is: "<<detKelvin<<"K ";
      if( _sim ) {
	clog <<"(sim mode).";
	mceStat = 1;
      }
      clog<<endl;
    }

    // get MCE status and parse it for info about state of bias voltages:
    checkMCEstatus();
    float detTemp = ::atof( detKelvin.c_str() );

    if( (detTemp < _maxDTemp) && (detTemp > _minDTemp) && (mceStat>0) && !_powerUpDone ) {
      clog<<"UFGemMCE4Agent::ancillary> commanding MCE to power up detector..."<<endl;
      vector<string> biasCmds;
      biasCmds.push_back("BiasPower");
      biasCmds.push_back("ARRAY_POWER_UP_BIAS"); //power on bias voltages to detector.
      biasCmds.push_back("BiasVgate");
      biasCmds.push_back("ARRAY_VGATE 1"); //turn Vgate on.
      UFStrings BiasCmds( "BiasCmds", biasCmds );
      UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
      bool readflag, initflag;
      int statDAC = UFMCE4Config::cmdDAC( BiasCmdinfo, readflag, initflag );
      clog<<"UFGemMCE4Agent::ancillary> cmd DAC status = "<<statDAC<<endl;
      checkMCEstatus();
      if( (_biasPower == "1" && _Vgate == "1") || _sim )
	_powerUpDone = true;
    }

    //send status of detector power to epics sad db:
    if( _epics != "false" ) {
      string message;
      if( _biasPower == "1" && _Vgate == "1" )
	message = "Detector is powered ON";
      else if( _sim && _powerUpDone )
	message = "Detector is powered ON (sim mode).";
      else
	message = "WARN: Detector is NOT powered on.";
      sendEpics( _epics + ":sad:dcState", message );
    }
  }
  */

  // done every heartbeat
  //need function to return result of DDP, call parser function to set SADs.

  if( !_sim && _config->_devIO ) {
    int mceStat;
    string mceReply;
    mceStat = _config->_devIO->submit("DDP", mceReply, _flush);
    int dac = UFMCE4Config::_DACPreampReadings(mceReply);
    string epicsRec, sval;
    float volts;
    for (int j = 0; j < dac; j++) {
      strstream s;
      epicsRec = _epics+":sad:DACPRE";
      if (j < 10) epicsRec+="0";
      s << epicsRec << j << ends;
      epicsRec = s.str(); delete s.str();
      volts = (float)(atoi((UFMCE4Config::_preamP[j]).c_str())*9.4/255);
      if ((int)((int)(volts*1000)/1000.*255/9.4) != (int)(volts*255/9.4+0.0005)) {
        volts += 0.001;
      } 
      strstream s2;
      s2 << volts << ends;
      sval = s2.str(); delete s2.str();
      if (sval.find(".") != string::npos) {
	sval = sval.substr(0, sval.find(".")+4);
      }
      //if( _epics != "false" ) sendEpics(epicsRec,UFMCE4Config::_preamP[j]);
      if( _epics != "false" ) sendEpics(epicsRec,sval);
    }
  }
  return p;
} // ancillary

// check status of MCE, in particular the bias board voltage states,
// setting the protected attribs: string _biasPower, _Vgate, _wellDepth, _biasLevel
//  and then send status to Epics if connected to a db:

void UFGemMCE4Agent::checkMCEstatus() {
  _biasPower="?", _Vgate="?", _wellDepth="?", _biasLevel="?";

  if( _config->_devIO ) {
    string mceReply;
    if( _config->_devIO->submit("status", mceReply, _flush) > 0 ) {
      string Line;
      if( mceReply.find("Bias Board") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Bias Board") );
	_biasPower = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Vgate") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Vgate") );
	_Vgate = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Well") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Well") );
	_wellDepth = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Detector Bias") != string::npos ) { //this is Bias Level setting
	Line = mceReply.substr( mceReply.find("Detector Bias") );
	_biasLevel = Line.substr( Line.find(">")+2, 1 );
      }
    }
    else {
      string message = "TermServ I/O failed to check MCE status!";
      clog<<"UFGemMCE4Agent::checkMCEstatus> "<<message<<endl;
      if( _epics != "false" ) {
	sendEpics( _epics + ":sad:observationStatus", message );
	sendEpics( _epics + ":sad:dcState", message );
      }
    }
  }
  else if( _sim ) {
    _wellDepth = UFRuntime::numToStr( UFMCE4Config::getWellDepth() );
    _biasLevel = UFRuntime::numToStr( UFMCE4Config::getBiasLevel() );
  }

  if( _epics != "false" ) {
    if( _biasPower == "0" || _Vgate == "0" )
      sendEpics( _epics + ":dc:DCBiasG.VALC", "off" );
    else
      sendEpics( _epics + ":dc:DCBiasG.VALC", "on" );
    sendEpics( _epics + ":dc:DCBiasG.VALI", _wellDepth );
    sendEpics( _epics + ":dc:DCBiasG.VALJ", _biasLevel );
  }

  clog<<"UFGemMCE4Agent::checkMCEstatus> biasPower="<<_biasPower<<", Vgate="<<_Vgate
      <<", wellDepth="<<_wellDepth<<", biasLevel="<<_biasLevel<<endl;
}

void* UFGemMCE4Agent::_simMCE4(void* p) {
  return p;
}

void UFGemMCE4Agent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0 ) {
    mlsleep(_Update); // no connections, no queued reqs., go to sleep
  }
  else {
    //UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
    UFRndRobinServ::hibernate();
  }
}

// override this base class func to selectively debug this logic
/*
int UFGemMCE4Agent::pendingReqs(vector <UFSocket*>& clients) {
  clients.clear();
  UFSocket::AvailTable bytcnts;
  if( _theConnections == 0 ) {
    clog<<"UFGemMCE4Agent::pendingReqs> no connection table?"<<endl;
    return -1;
  }
  if( _theConnections->size() == 0 ) {
    clog<<"UFGemMCE4Agent::pendingReqs> empty connection table?"<<endl;
    return 0;
  }
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemMCE4Agent::pendingReqs> # of clients in connection table to check: "<<_theConnections->size()<<endl;
  int acnt = UFSocket::availableList(*_theConnections, bytcnts, _theConMutex);
  //if( _verbose )
  clog<<"UFGemMCE4Agent::pendingReqs> # of clients in connection table to (double) check: "<<acnt<<" out of: "<<_theConnections->size()<<endl;
  if( acnt <= 0 ) {
    //if( _verbose )
      clog<<"UFGemMCE4Agent::pendingReqs> no clients have pending/available i/o, acnt: "<<acnt<<", of "<<_theConnections->size()<<endl;
    return acnt;
  }
  UFSocket::AvailTable::iterator i = bytcnts.begin();
  for( ; i != bytcnts.end(); ++i ) {
    UFSocket* socp = i->first; 
    int nb = i->second;
    if( nb > 0 && socp != 0 ) {
      clients.push_back(socp);
    }
    else if( nb < 0 && socp != 0 ) { // stale connection? close it...
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemMCE4Agent::pendingReqs> connection stale?, closing..."<<acnt<<endl;
      int concnt = UFSocket::closeAndClear(*_theConnections, socp, _theConMutex);
      if( concnt != (int) _theConnections->size() ) clog<<"UFGemMCE4Agent::pendingReqs> connectio count error?"<<endl;
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemMCE4Agent::pendingReqs> current connection cnt= "
	    <<_theConnections->size()<<", avail. cnt= "<<concnt<<endl;
    }
    else if( socp != 0 ) { // available timed-out?
      if( UFDeviceAgent::_verbose )
        clog<<"UFGemMCE4Agent::pendingReqs> nb: "<<nb<<", nothing yet on "<<socp->description()<<endl;
    }
  }

  if( _verbose )
    clog<<"UFGemMCE4Agent::pendingReqs> # of clients with valid pendingReqs: "
        <<clients.size()<<", out of: "<<_theConnections->size()<<endl;

  return (int) clients.size();   
} 
*/

// These are the key virtual functions to override.
// Send command string to TermServ, and return response.
// Presumably any allocated memory is freed by the calling layer....
// This version must always return null to (epics) clients, and
// instead send the command completion status to designated CARs (actually J input of Gensub ? ).
// If no CAR is desginated, an error/alarm condition should be indicated.
// new signature for action:

int UFGemMCE4Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFGemMCE4Agent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_client= true;

  if( act->clientinfo.find("flam:") != string::npos ||
      act->clientinfo.find("foo:") != string::npos ||
      act->clientinfo.find("fu:") != string::npos ||
      act->clientinfo.find("f2:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    if( _verbose )
      clog<<"UFGemMCE4Agent::action> No UFProtocol replies will be sent to Gemini/Epics client: "
	  <<act->clientinfo<<endl;
  }
  int elemcnt = (int)act->cmd_name.size();
  if( _verbose )
    clog<<"UFGemMCE4Agent::action> client: "<<act->clientinfo<<", req. elem.: "<<elemcnt<<endl;

  string sad, car, errmsg, datalabel, nfshost, rootdir, file;
  if( _epics.empty() )
    if( _epics != "false" )
      //_statRec = sad = _epics + ":sad:MCEReply.INP";
      _statRec = sad = _epics + ":sad:MCEReply.VAL";

  // raw commands are exepected for instrum. setup
  // support macros for: datum and obs. setup
  bool sim, raw, acq, status, datum, obssetup, park, test, readdac= false;
  int dac= -1, obsconfcnt= 0;
  UFAcqCtrl::Observe* obsconf= 0;
  UFProtocol* obsfits= 0; // obssetup should init observation fits primary header
  
  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    clog<<"UFGemMCE4Agent::action> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
  }
 
  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    errmsg = UFMCE4Config::stripErrmsg( cmdname );

    //if( _verbose ) {
      clog<<"UFGemMCE4Agent::action> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
      clog<<"UFGemMCE4Agent::action> errmsg: "<<errmsg<<endl;
      //}

    //convert to uppercase for convenience:
    UFStrings::upperCase( cmdname );

    if( cmdname.find("CAR") != string::npos ) {
      reply_client= false;
      car = cmdimpl;
      clog<<"UFGemMCE4Agent::action> CAR: "<<car<<endl;
      continue;
    }

    if( cmdname.find("SAD") != string::npos ) {
      reply_client= false;
      sad = cmdimpl;
      //if( sad.find(".INP") == string::npos ) sad += ".INP";
      _statRec = sad;
      clog<<"UFGemMCE4Agent::action> SAD: "<<sad<<endl;
      continue;
    }

    //now convert to uppercase for convenience (after CAR has been saved):
    //UFStrings::upperCase( cmdimpl ); NOT!

    // now check for raw/real or sim commands
    // command-line option can force all actions to be simulated
    sim = _sim || cmdname.find("SIM") != string::npos;
 
    // if not simulation, expect either a raw/direct mce cmd or status/info req. or
    // a parameter configuration or an observation start/stop/abort:
    // status directive can be "FITS", "full/nonFITS" or "partial/nonFITS", the latter
    // is simply anything other than full. full status directive
    // returns not only mce4 state, but also the full list _activeH * _activeP
    // (and meta values?)
    raw      = cmdname.find("RAW") != string::npos; // non hazardous cmds
    status   = cmdname.find("STAT") != string::npos; // mce status FITS or basic text
    // start (observe), stop, abort, pause, continue data acq. (via UFAcqCtrl):
    acq      = cmdname.find("ACQ") != string::npos; 
    datum    = cmdname.find("DATUM") != string::npos; // ct 0
    park     = cmdname.find("PARK") != string::npos; // ct 0
    test     = cmdname.find("TEST") != string::npos; // mce status & set obs FITS
    obssetup = cmdname.find("OBSSETUP") != string::npos; // (re)set obsconfig and DHS DataLabel (via UFAcqCtrl)
    // also support TReCS cmd bundles?
    if( !obssetup )
      obssetup = cmdname.find("OBSCONF") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("DHS") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("LABEL") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("MODE") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("INDEX") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("IDX") != string::npos; // alt. cmd name for obsconf (re)set
    if( !obssetup )
      obssetup = cmdname.find("LUT") != string::npos; // alt. cmd name for obsconf (re)set

    string reply= "";
    string mcestatus= "";
    UFProtocol* ufp= 0;

    // allow bundles to contain mult. obsconf directive and also an acq, but only 
    // create one definitive obsconf from a cmd bundle, so keep count...
    if( obssetup && ++obsconfcnt <= 1 ) {
      // handle any/all elements of bundle devoted to obssetup/obsconf
      obsconf = _AcqCtrl->obsSetup(act);
      clog<<"UFGemMCE4Agent::action> ObsSetup: "<<cmdname<<", "<<cmdimpl<<", obsconf: "
	  <<(size_t)obsconf<<", obsname: "<<obsconf->name()<<endl;
      clog<<"UFGemMCE4Agent::action> _AcqCtrl: "<<(size_t) _AcqCtrl<<", _obssetup: "<<(size_t)_AcqCtrl->_obssetup<<endl;
      if( reply_client ) {
	deque< string > text;
	obsconf->asStrings(text);
	ufp = new UFStrings(obsconf->name(), text);
        act->cmd_reply.push_back(reply);
      }
      _nextObsConf = obsconf->_obscfg;
      // break; rather than break... allow inclusion of acq (final?) directive in cmd bundle 
      continue;
    } // obssetup 

    if( acq ) { 
      // this allows acquisition status to be sent to EPICS acqControl gensub/CAR
      _statRec = sad; // car; // change this?
      clog<<"UFGemMCE4Agent::action> Acq: "<<cmdname<<", "<<cmdimpl<<endl;
      if( _nextObsConf == 0 ) {
	clog<<"UFGemMCE4Agent::action> no prior ObsSetup?"<<endl;
        if( reply_client ) {
	  string nosetup = "No Prior ObsSetup/ObsCOnfig, Reject ACQ!";
	  ufp = new UFStrings("AcqError", &nosetup);
          act->cmd_reply.push_back(reply);
	}
	break;
      }
      else { // note the ptr assignment below
	// if _nextObsConf and _ObsConf should never be deleted since they are assigned to the 
	// static UFAcqCtrl::_obssetup which (may) reuses/reset its obsconf object
	_ObsConf = _nextObsConf;
	clog<<"UFGemMCE4Agent::action> ACQ using ObsConf: "<<(size_t)_ObsConf<<", name: " <<_ObsConf->name()<<endl;
	clog<<"UFGemMCE4Agent::action> _AcqCtrl: "<<(size_t) _AcqCtrl<<", _obssetup: "<<(size_t)_AcqCtrl->_obssetup<<endl;
        if( obsconf != _AcqCtrl->_obssetup ) {
	  clog<<"UFGemMCE4Agent::action> mismatch in pointer AcqCtrl::Observe* ! "<<(size_t)obsconf<<endl;
	  break;
	}
	UFMCE4Config* mce = dynamic_cast< UFMCE4Config* > (_config);  
        stat = mce->acquisition(cmdimpl, reply, _AcqCtrl, _flush); // acq start, stop, abort, conf, ?
        act->cmd_reply.push_back(reply);
      }
      break; // this should have processed the entire bundle of acquisition directives.
    } // acq

    // two types of dac params: bias or preamp
    if( cmdname.find("BIAS") != string::npos ||
	cmdimpl.find("BIAS") != string::npos )
      dac = UFMCE4Config::DACBias;

    if( cmdname.find("PREAMP") != string::npos ||
	cmdimpl.find("PREAMP") != string::npos )
      dac = UFMCE4Config::DACPreamp;

    if( !(sim || status || raw || datum || park || test || datum || obssetup || acq) && (dac < 0) ) {
      // directive not found/supported, quit
      errmsg += "?Bad directive " + cmdname + " " + cmdimpl;
      clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
      if( car != "" ) sendEpics(car, errmsg);
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    int nr = 1;
    if( !(status || raw) ) // any form of status or raw OK
      //nr = _config->validCmd(cmdimpl); // check non-status directives
      nr = _config->validCmd(cmdname); // check non-status directives

    if( nr < 0 ) {   // invalid cmd ?
      errmsg += "?Invalid MCE4 req: " + cmdname + " " + cmdimpl;
      clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
      act->cmd_reply.push_back(errmsg);
      if( car != "" ) sendEpics(car, errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      // this should be freed by the calling func...
      return new UFStrings(act->clientinfo, act->cmd_reply); 
    }

    act->time_submitted = currentTime();
    _active = act;

    if( cmdimpl.find("STATUS") != string::npos)
      mcestatus = "status";

    if( sim ) {
      // simulate mce4 queries:
      strstream s;
      if( UFMCE4Config::_idle ) {
	if( UFMCE4Config::isConfigured() ) 
	  s << "(Sim. mode:, " << cmdname << "=>" << cmdimpl << ") MCE4 configured & idle" <<ends;
	else
	  s << "(Sim. mode: " << cmdname << "=>" << cmdimpl << ") MCE4 not configured & idle" <<ends;
      }
      else {
        s << "(Sim. mode: " << cmdname << "=>" << cmdimpl << ") MCE4 is cycling" <<ends;
      }
      reply = s.str(); delete s.str();
      //if( _verbose )
	clog<<"UFGemMCE4Agent::action> (sim) "<<reply<<endl;
      if( raw ) { // reset reply to sim. mce4 echo...
	reply = cmdimpl + " AgentSimNoOp";
	clog<<"UFGemMCE4Agent::action> (sim) "<<reply<<endl;
        if( !car.empty() ) sendEpics(car, reply);
        if( !sad.empty() ) sendEpics(sad, reply);
	stat = reply.length();
        act->cmd_reply.push_back(reply);
      }
    }
    else if( status && reply_client ) {
      if( cmdimpl.find("FIT") == string::npos ) {
        // client wants full status, include active parameter settings, non-FITS format:
        if( !UFMCE4Config::isConfigured() )
	  act->cmd_reply.push_back("MCE4 is uninitialized/unconfigured");
        else
  	  act->cmd_reply.push_back("MCE4 has been configured at least once");

        vector< string > des;
        int n = UFMCE4Config::getDes(des);
        for( int i = 0; i < n; ++i )
          act->cmd_reply.push_back(des[i]);

        vector< string > actv;
        n = UFMCE4Config::getActive(actv);
        for( int i = 0; i < n; ++i )
          act->cmd_reply.push_back(actv[i]);
      } // non-fits
      else { // FITS status request should be last in bundle
	return _config->statusFITS(this);
      } // fits
    } // status reply to client (if non-epics client)
    else if( park || datum ) {
      vector<string> biasCmds;
      biasCmds.push_back("BiasPark");
      biasCmds.push_back("ARRAY_POWER_DOWN_BIAS"); //power off bias voltages to detector.
      UFStrings BiasCmds( "BiasCmds", biasCmds );
      UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
      bool readflag, initflag;
      int statDAC = stat = UFMCE4Config::cmdDAC( BiasCmdinfo, readflag, initflag );
      clog<<"UFGemMCE4Agent::action> PARK cmd DAC status = "<<statDAC<<endl;
    } // park or datum
    else if( obssetup ) { //re-query MCE for basic frame times (at PBC=1).
      clog<<"UFGemMCE4Agent::action> (reset) obssetup element: "<<cmdimpl<<endl;
    } // test
    else if( test ) { //re-query MCE for basic frame times (at PBC=1).
      //initMCEtiming();
      checkMCEstatus();
    } // test
    else if( !_sim && (raw || status || mcestatus != "") ) {
      // if raw, use the Perle port to submit the action command and get reply:
      // (really should be calling DeviceConfig functions here)
      //if( _verbose )
        clog<<"UFGemMCE4Agent::action> submit raw: "<<cmdimpl<<endl;
      // a raw stop directive should only be applied to mce4, not to any lingering frame acq.
      if( status && mcestatus != "" && _config->_devIO ) { // submit mce status command directly
        stat = _config->_devIO->submit(mcestatus, reply, _flush); // expect single line reply
      }
      else if( _config->_devIO ) { // submit raw non-acq. command directly
        if( raw && cmdimpl.find("?") != string::npos) // multiline help reply
          stat = _config->_devIO->submitMultiReply(cmdimpl, reply, _flush); 
        else  // expect single line reply
          stat = _config->_devIO->submit(cmdimpl, reply, _flush);
      }
    //if( _verbose )
        clog<<"UFGemMCE4Agent::action> reply: "<<reply<<endl;
      if( raw && (!_epics.empty() && _epics != "false") && 
	  (cmdimpl.find("start") != string::npos || cmdimpl.find("Start") != string::npos || cmdimpl.find("START") != string::npos) ) {
        string sadrec = _epics + ":sad:" + "EDTFRAME";
	sendEpicsSAD(sadrec, (int)0, _capipe);
        //if( _verbose )
	clog<<"UFGemMCE4Agent::action> cleared frame count SAD: "<<sadrec<<endl;
      }
      if( _DevHistory.size() >= _DevHistSz )
        _DevHistory.pop_front();
      _DevHistory.push_back(reply);
      if( _config->_devIO && stat < 0 ) { // failed!
        errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        if( car != "" ) sendEpics(car, errmsg);
        if( _verbose )
          clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by the calling func...
      } // end failed dev. comm.
      else { // send mce4 (successful i/o) reply to epics..
        //if( !car.empty() ) sendEpics(car, reply);
        if( !sad.empty() ) {
          reply = reply.substr(0,39);
          char *cs = (char*)reply.c_str(), *cs0= cs;
          while( *cs != 0 ) { if ( *cs > 0 && *cs < 32 ) *cs = '?'; ++cs; }
          reply = cs0;
          clog<<"UFGemMCE4Agent::action> sad: "<<sad<<" (truncated) reply: "<<reply<<endl;
          sendEpics(sad, reply);
        }
      }
      act->cmd_reply.push_back(reply);
      // don't break here, allow multiple raw/status directives in a bundle...
    } // raw or status
    else if( dac > 0 ) { // submit DAC cmd(s) to MCE4
      // cmdDAC() gets dac (bias/preamp) parms from remaining elements in cmdname[], cmdimpl[] vecs.
      bool didInitBias = false;
      stat = UFMCE4Config::cmdDAC(act, readdac, didInitBias, _flush);
      if( stat <= 0 ) {
        string mcemsg = act->cmd_reply.back();
        clog<<"UFGemMCE4Agent::action> "<<mcemsg<<endl;
	int colon = 0;
	if( mcemsg.find(":") != string::npos )
	  colon = (int)mcemsg.find(":") + 1;
        if( car != "" ) sendEpics(car, mcemsg.substr(colon) );
	errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
	// after error is indicated to Epics,
	//  get MCE status, parse it for info about state of bias voltages,
	//   and send status to epics bias records (if epics db is available):
	checkMCEstatus();
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by  calling func...
      }
      // we have processed the entire DAC Bias/Preamp Config bundle of directives.
      // assume for simplicity that the cmd-bundle is all dac related.
    } // dac
    else if( !raw ) {
      clog<<"UFGemMCE4Agent::action> Not a raw command:"<<cmdname<<" => "<<cmdimpl<<ends;
      if( _sim ) { // reset reply to sim. mce4 echo...
	reply = cmdimpl + " AgentSimNoOp";
	clog<<"UFGemMCE4Agent::action> (sim) "<<reply<<endl;
        if( !car.empty() ) sendEpics(car, reply);
        if( !sad.empty() ) sendEpics(sad, reply);
	stat = reply.length();
        act->cmd_reply.push_back(reply);
      }
      else {
	clog<<"."<<endl;
      }
    }
    // digest error messages:
    if( stat < 0 ) { // consider any error as fatal and dicsontinue processing of command(s) 
      //If error message is from frm.acq.server (stat == -2) and sending to Epics,
      // then extract essential trailing content of message because of Epics 40 char limit.
      if( stat < -1 && car != "" && errmsg.find(">") != string::npos )
        errmsg = errmsg.substr( errmsg.find(">")+1 );
      errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
      if( car != "" ) sendEpics(car, errmsg);
      if( _epics != "false" ) sendEpics( _epics + ":sad:observationStatus", errmsg );
      if( _verbose )
        clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by calling func.
    }
  } // end for loop over cmd action bundle

  // if the client is an epics db (car != "" ) send ok or error
  // completion of all req. in this bundle.
  // if it was a Parm==Hardware, only send back OK or Error
  // if it was a Parm==Meta, also send back hardware parms 
  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    //if( _verbose )
      clog<<"UFGemMCE4Agent::action> one or more cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    if( car != "" ) setCARError(car, &errmsg);
  }
  else {
    act->status_cmd = "succeeded";
    //if( _verbose )
    clog<<"UFGemMCE4Agent::action> cmds. complete..."<<endl;
    /*
    if( car != "" && readdac ) {
      if( dac == UFMCE4Config::DACBias ) { // dac bias readings requested
	vector< string > dacvec;
	int n = UFMCE4Config::flamBiasCAR(dacvec);
	n = sendEpics(car, dacvec);
      }
      else if( dac == UFMCE4Config::DACPreamp ) { // dac preamp readings requested
	vector< string > dacvec;
	int n = UFMCE4Config::flamPreampCAR(dacvec);
	n = sendEpics(car, dacvec);
      }
    }
    */
    if( !acq && car != "" ) {
      clog<<"UFGemMCE4Agent::action> set (non acq) CAR to idle... "<<endl;
      setCARIdle(car);
    }
  } // success -- the car be set idle only if wei did not started an acq
 
  // if action was a DAC bias cmd, after success is indicated to Epics,
  // get MCE status, parse it for info about state of bias voltages,
  //  and send status to epics bias records (if epics db is available):
  if( dac == UFMCE4Config::DACBias )
    checkMCEstatus();

  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client )
    return (UFStrings*) 0;

  if( obsfits != 0 )
    return obsfits;

  return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
} // end of action() method. 
/*
int UFGemMCE4Agent::query(const string& q, vector<string>& qreply) {
  int stat= 0;
  bool reply_client= true;

  if( act->clientinfo.find("flam:") != string::npos ||
      act->clientinfo.find("foo:") != string::npos ||
      act->clientinfo.find("fu:") != string::npos ||
      act->clientinfo.find("f2:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    if( _verbose )
      clog<<"UFGemMCE4Agent::action> No UFProtocol replies will be sent to Gemini/Epics client: "
	  <<act->clientinfo<<endl;
  }
  int elemcnt = (int)act->cmd_name.size();
  if( _verbose )
    clog<<"UFGemMCE4Agent::action> client: "<<act->clientinfo<<", req. elem.: "<<elemcnt<<endl;

  string car= "", sad= "", errmsg= "";
  string datalabel= "", nfshost= "", rootdir= "", file= "";
  // raw commands are exepected for instrum. setup
  // support macros for: datum and obs. setup
  bool sim, raw, acq, status, datum, obssetup, park, test, readdac= false;
  int dac= -1;
  UFProtocol* obsfits= 0; // obssetup should init observation fits primary header

  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    errmsg = UFMCE4Config::stripErrmsg( cmdname );

    //if( _verbose ) {
      clog<<"UFGemMCE4Agent::query> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
      clog<<"UFGemMCE4Agent::query> errmsg: "<<errmsg<<endl;
    //}

    //convert to uppercase for convenience:
    UFStrings::upperCase( cmdname );

    if( cmdname.find("CAR") != string::npos ) {
      reply_client= false;
      car = cmdimpl;
      clog<<"UFGemMCE4Agent::query> CAR: "<<car<<endl;
      continue;
    }

    if( cmdname.find("SAD") != string::npos ) {
      reply_client= false;
      sad = cmdimpl;
      clog<<"UFGemMCE4Agent::query> SAD: "<<sad<<endl;
      continue;
    }

    //now convert to uppercase for convenience (after CAR has been saved):
    UFStrings::upperCase( cmdimpl );

    // now check for raw/real or sim commands
    // command-line option can force all actions to be simulated
    sim = _sim || cmdname.find("SIM") != string::npos;
 
    // if not simulation, expect either a raw/direct mce cmd or status/info req. or
    // a parameter configuration or an observation start/stop/abort:
    // status directive can be "FITS", "full/nonFITS" or "partial/nonFITS", the latter
    // is simply anything other than full. full status directive
    // returns not only mce4 state, but also the full list _activeH * _activeP
    // (and meta values?)
    raw      = cmdname.find("RAW") != string::npos; // non hazardous cmds
    status   = cmdname.find("STAT") != string::npos; // mce status FITS or basic text
    if( !raw ) // && !status )
      continue; // only support raw queries for now

    if( _config->_devIO ) // submit raw non-acq. command directly
        stat = _config->_devIO->submit(cmdimpl, reply, _flush); // expect single line reply?
      if( _verbose )
        clog<<"UFGemMCE4Agent::query> reply: "<<reply<<endl;
      if( _DevHistory.size() >= _DevHistSz )
        _DevHistory.pop_front();
      _DevHistory.push_back(reply);
      if( _config->_devIO && stat < 0 ) {
        errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        if( car != "" ) sendEpics(car, errmsg);
        if( _verbose )
          clog<<"UFGemMCE4Agent::query> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by the calling func...
      } // end failed dev. comm.
      act->cmd_reply.push_back(reply);
}
*/
// Method to create the basic FITS header which contains NAXIS values,
// defined using the current _FrmConf, _ObsConf, and _ObsCompStat, etc..
// Optionally will use a different abortObsConf if given, describing an aborted observation.
UFFITSheader* UFGemMCE4Agent::basicFITSheader() {
  if( _ObsConf == 0 )
    _ObsConf = new (nothrow) UFFlamObsConf(0.0, "Idle");
  UFFlamObsConf* obscfg= 0;
  //use ObsConfig for aborted observation if present:
  if( _abortedObsConf ) 
    obscfg = _abortedObsConf;
  else
    obscfg = _ObsConf;

  UFFITSheader* FITSheader = new UFFITSheader(*obscfg); //, "Written by "+_FrmAcqVersion );

  FITSheader->add("INSTRUMENT", "Flamingos",             "name of instrument");
  FITSheader->add("ARRAY",     "Hawaii-2",               "name of detector array & MUX");
  FITSheader->add("DHSLABEL", _dhsLabel,                 "DHS Label (= DHS data file name)");
  FITSheader->add("USRNOTE",  _comment,                  "Optional note made by user about obs.");
  FITSheader->add("COMPSTAT", _ObsCompStat,              "Observation completion status");
  FITSheader->add("SIGFRM",   obscfg->sigmaFrmNoise(), "st.dev. of bg+read noise in frame (ADU)");
  FITSheader->add("SIGFMAX",  obscfg->sigmaFrmMax(),   "maximum st.dev. of bg+read noise per frame");
  FITSheader->add("SIGFMIN",  obscfg->sigmaFrmMin(),   "minimum st.dev. of bg+read noise per frame");
  FITSheader->add("BKASTART", _bgADUStart,               "Background ADUs at start of obs.");
  FITSheader->add("BKAEND",   obscfg->bgADUs(),        "Background ADUs at end of obs.");
  FITSheader->add("BKAMAX",   obscfg->bgADUmax(),      "maximum ADUs det. readout during obs.");
  FITSheader->add("BKAMIN",   obscfg->bgADUmin(),      "minimum ADUs det. readout during obs.");
  FITSheader->add("BKWSTART", _bgWellStart,              "% filling of det. wells at start of obs.");
  FITSheader->add("BKWEND",   obscfg->bgWellpc(),      "% filling of det. wells at end of obs.");
  FITSheader->add("BKWMAX",   _bgWellMax,                "maximum % filling of det. wells during obs.");
  FITSheader->add("BKWMIN",   _bgWellMin,                "minimum % filling of det. wells during obs.");
  FITSheader->add("SIGREAD",  obscfg->sigmaReadNoise(),"st.dev. of read noise in last frame (ADU)");
  FITSheader->add("SIGRMAX",  obscfg->sigmaReadMax(),  "maximum st.dev. of read noise per frame");
  FITSheader->add("SIGRMIN",  obscfg->sigmaReadMin(),  "minimum st.dev. of read noise per frame");
  FITSheader->add("RDASTART", _rdADUStart,               "Det. clamped off readout ADUs at start of obs.");
  FITSheader->add("RDAEND",   obscfg->rdADUs(),        "Det. clamped off readout ADUs at end of obs.");
  FITSheader->add("RDAMAX",   obscfg->rdADUmax(),      "maximum ADUs det-off readout during obs.");
  FITSheader->add("RDAMIN",   obscfg->rdADUmin(),      "minimum ADUs det-off readout during obs.");
  FITSheader->add("WELLDPTH", _wellDepth,                "detector well depth: 0=shallow, 1=deep");
  FITSheader->add("BIASLEVL", _biasLevel,                "bias V_detgrv: 0=off, 1=Low, 2=Med, 3=Hi");
  // FITSheader->add("BIASVDAC", UFMCE4Config::VdetgrvDAC(),"DAC control value for bias V_detgrv");
  FITSheader->add("MJD-OBS",  getEpics( _epics, ":wcs:mjdobs" ), "MJD at observation start");
  FITSheader->add("RADECSYS", getEpics( _epics, ":wcs:radecsys" ), "R.A.-DEC. coordinate system ref.");
  FITSheader->add("EQUINOX",  getEpics( _epics, ":wcs:equinox" ), "Equinox of coordinate system");
  FITSheader->add("CTYPE1",   getEpics( _epics, ":wcs:ctype1" ), "R.A. in tangent plane projection");
  FITSheader->add("CRPIX1",   getEpics( _epics, ":wcs:crpix1" ), "Ref pix of axis 1");
  FITSheader->add("CRVAL1",   getEpics( _epics, ":wcs:crval1" ), "RA at Ref pix in decimal degrees");
  FITSheader->add("CTYPE2",   getEpics( _epics, ":wcs:ctype2" ), "DEC. in tangent plane projection");
  FITSheader->add("CRPIX2",   getEpics( _epics, ":wcs:crpix2" ), "Ref pix of axis 2");
  FITSheader->add("CRVAL2",   getEpics( _epics, ":wcs:crval2" ), "DEC at Ref pix in decimal degrees");
  FITSheader->add("CD1_1",    getEpics( _epics, ":wcs:cd11" ), "WCS matrix element 1,1");
  FITSheader->add("CD1_2",    getEpics( _epics, ":wcs:cd12" ), "WCS matrix element 1,2");
  FITSheader->add("CD2_1",    getEpics( _epics, ":wcs:cd21" ), "WCS matrix element 2,1");
  FITSheader->add("CD2_2",    getEpics( _epics, ":wcs:cd22" ), "WCS matrix element 2,2");
  float airMassEnd = 0.0;
  if( _epics != "false" ) {
    string airmass = getEpics( "tcs", ":sad:airMassNow" );
    airMassEnd = ::atof( airmass.c_str() );
  }
  FITSheader->add("AIRMASS1",  airMassBeg, "Air Mass at start of obs.");
  FITSheader->add("AIRMASS2",  airMassEnd, "Air Mass at end of obs.");
  return FITSheader;
}

void UFGemMCE4Agent::setEpicsAcqFlags(bool acq, bool rdout) {
  if( _epics == "false" || _epics == "" || _epics == " " )
    return;

  string acqdbrec = _epics + ":acq";
  string rdoutdbrec = _epics + ":rdout";

  if( acq )
    sendEpics(acqdbrec, "1");
  else
    sendEpics(acqdbrec, "0");

  if( rdout )
    sendEpics(rdoutdbrec, "1");
  else
    sendEpics(rdoutdbrec, "0");

  string prepdbrec = _epics + ":prep";
  sendEpics(prepdbrec, "0");  // prep should always be false while observing.
}

#endif // UFGemMCE4Agent
