#if !defined(__UFFLAM1MECH_H__)
#define __UFFLAM1MECH_H__
/* for use by C & C++ to describe mechanism names & positions */
/* assume Epics-like string length limits */

#include "stdlib.h"
#include "string.h"
#if defined(CYGWIN)
#include "libguile/values.h"
#else
#include "values.h"
#endif
#include "limits.h"
#include "math.h"

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

static const int __UFEPSLENF1_ = 40; 
static const int __UFMAXPOSFLAM1_ = 40; 
static const int __UFMAXINDXFLAM1_ = 5; 

typedef struct {
  int posCnt; /* number of (<= max) positions (including home) in use */
  char *name; /* name[__UFEPSLENF1_]; */
  char **positions; /* positions[__UFMAXPOSFLAM1_][__UFEPSLENF1_]; */
  float *steps; /* steps[__UFMAXPOSFLAM1_]; + steps from home */
} UFFlam1MechPos;

typedef struct __UFFlam1Mech_ {
  int mechCnt;
  char **indexor; /* indexor[__UFMAXINDXFLAM1_][__UFEPSLENF1_]; */
  UFFlam1MechPos *mech; /* mech[__UFMAXINDXFLAM1_]; */
} UFFlam1Mech;

/* non-reentrant non-thread-safe: */
static UFFlam1Mech* getTheUFFlam1Mech() {
  static char* _indexors[]= { "a", "b", "c", "d", "e" };

  static UFFlam1MechPos dw;
  static char* _nameDW = "DeckerWheel"; 
  static char* _positionsDW[] = { "Home/Dark", "Open/Image", "MOS", "LongSlit" }; 
  static float   _stepDW[] = { 0, 2250, 4500, 6750 };

  static UFFlam1MechPos mw;
  static char* _nameMW = "MOSWheel"; 
  static char* _positionsMW[] = { "Home/Open", "6PIX/A", "MOS/B", "2PIX/C", "MOS/D", "12PIX/E",
                                  "MOS/F", "20PIX/G", "MOS/H", "9PIX/J", "MOS/K", "MOS/L",
				  "MOS/M", "MOS/N", "MOS/O", "MOS/P", "MOS/Q", "3PIX/R" };
  static float   _stepMW[] = { 0, 87336, 83600, 76480, 69360, 62240,
                               55120, 48000, 40880, 37312, 33752, 30192,
			       26632, 23064, 19504, 15944, 12384, 8356 };
  static UFFlam1MechPos fw;
  static char* _nameFW = "FilterWheel"; 
  static char* _positionsFW[] = { "Home/J-BAND", "H-BAND", "K-BAND", "HK-BAND", "JH-BAND", "Ks-BAND" };
  static float   _stepFW[] = { 0, 208, 417, 625, 833, 1042 };

  static UFFlam1MechPos lw;
  static char* _nameLW = "LyotWheel"; 
  static char* _positionsLW[] = { "Home/DARK1", "DARK2", "OPEN", "GEMINI", "MMT", "4m-KPNO", "2.1m-KPNO" };
  static float   _stepLW[] = { 0, 178, 357, 536, 714, 893, 1071 };

  static UFFlam1MechPos gw;
  static char* _nameGW = "GrismWheel"; 
  static char* _positionsGW[] = { "Home/Open1", "DARK1", "DARK2", "OPEN2", "HK" };
  static float   _stepGW[] =  { 0, 250, 500, 750, 1000 };

  static UFFlam1Mech _TheFlam1Mech;
  static UFFlam1MechPos* _mech= 0;
  if( _mech != 0 )
    return &_TheFlam1Mech;

  _mech = (UFFlam1MechPos*) calloc(__UFMAXINDXFLAM1_, sizeof(UFFlam1MechPos));

  dw.name = _nameDW;
  dw.positions = _positionsDW;
  dw.steps = _stepDW;
  dw.posCnt = _ArrayLen__(_stepDW);

  mw.name = _nameMW;
  mw.positions = _positionsMW;
  mw.steps = _stepMW;
  mw.posCnt = _ArrayLen__(_stepMW);

  fw.name = _nameFW;
  fw.positions = _positionsFW;
  fw.steps = _stepFW;
  fw.posCnt = _ArrayLen__(_stepFW);

  lw.name = _nameLW;
  lw.positions = _positionsLW;
  lw.steps = _stepLW;
  lw.posCnt = _ArrayLen__(_stepLW);

  gw.name = _nameGW;
  gw.positions = _positionsGW;
  gw.steps = _stepGW;
  gw.posCnt = _ArrayLen__(_stepGW);

  _TheFlam1Mech.mechCnt = __UFMAXINDXFLAM1_;
  _TheFlam1Mech.indexor = _indexors; 
  _TheFlam1Mech.mech = _mech;

  _TheFlam1Mech.mech[0] = dw;
  _TheFlam1Mech.mech[1] = mw;
  _TheFlam1Mech.mech[2] = fw;
  _TheFlam1Mech.mech[3] = lw;
  _TheFlam1Mech.mech[4] = gw;

  return &_TheFlam1Mech;
}

// this assumes that char** names never been initialized
// and is passed via &names:
static int mechNamesFlam1(char*** names) {
  static char** _names= 0;
  UFFlam1Mech* _TheFlam1Mech= getTheUFFlam1Mech();
  int i;
  if( _names == 0 ) {
    _names = (char**) calloc(__UFMAXINDXFLAM1_, sizeof(char*));
    for( i = 0; i < _TheFlam1Mech->mechCnt; ++i ) 
      _names[i] = _TheFlam1Mech->mech[i].name;
  }

  *names = _names;
  return _TheFlam1Mech->mechCnt;
}
  
static double stepsFromHomeFlam1(const char* mechName, char* posName) {
  int m, p;
  char* s= 0;
  UFFlam1MechPos mech;
  UFFlam1Mech *_TheFlam1Mech = getTheUFFlam1Mech();

  for( m = 0; m < _TheFlam1Mech->mechCnt; ++m ) {
    mech = _TheFlam1Mech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  for( p = 0; p < mech.posCnt; ++p ) {
    if( (s = strstr(mech.positions[p], posName)) != 0 ) 
      break;
  }

  return mech.steps[p];
};

static char* posNameNearFlam1(const char* mechName, double stepsFromHome) {
  static char* s= 0;
  int m, p;
  double diffmin= INT_MAX, diffs[__UFMAXPOSFLAM1_];
  UFFlam1MechPos mech;
  UFFlam1Mech *_TheFlam1Mech = getTheUFFlam1Mech();

  for( m = 0; m < _TheFlam1Mech->mechCnt; ++m ) {
    mech = _TheFlam1Mech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  for( p = 0; p < mech.posCnt; ++p ) {
    diffs[p] = fabs(stepsFromHome - mech.steps[p]);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
  }
  return s;
}

#endif /* __UFFLAM1MECH_H__ */
