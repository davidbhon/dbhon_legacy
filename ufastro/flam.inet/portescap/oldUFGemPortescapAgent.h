#if !defined(__UFGemPortescapAgent_h__)
#define __UFGemPortescapAgent_h__ "$Name:  $ $Id: oldUFGemPortescapAgent.h,v 0.0 2006/09/14 18:35:34 hon Exp $"
#define __UFGemPortescapAgent_H__(arg) const char arg##UFGemPortescapAgent_h__rcsId[] = __UFGemPortescapAgent_h__;

#include "UFGemDeviceAgent.h"
#include "UFPortescapConfig.h"

#include "string"
#include "list"
#include "map"
#include "iostream.h"

class UFGemPortescapAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemPortescapAgent(int argc, char** argv);
  UFGemPortescapAgent(const string& name, int argc, char** argv);
  inline virtual ~UFGemPortescapAgent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  virtual void startup();

  virtual string newClient(UFSocket* client);

  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in addition to establishing the iocomm/annex connection tp lakeshore,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline virtual int getValue(const string& indexor, double& steps) { 
    steps = UFGemPortescapAgent::_CurPosition[indexor]; return 0;
  }

  inline virtual int instrumentId() { return _instrumId; }

  // reset hold&run current after each home?
  static bool _resetHRC;
  static string _SADDatumCnt;
  void datumCompleted(int errcnt, string& errmsg);

protected:
  // these are used to coordinate the action() and ancillary() functions:
  static int _instrumId;
  static string _currentval;
  static string _reqval;
  static time_t _timeOut; // in seconds 
  static time_t _clock;
  static double _NearHome, _simSpeed;
  static map<string, string> _Status; // use this instead of _StatList for EDB/CA names
  static map<string, string> _ErrPosition; // error message

  static map<string, string> _HomeThenStep; // key = names of indexors to Home then Move by val = +/- steps 
  static map<string, string> _HomeStepHome; // key = names of indexors to Home then Move by val = +/- steps then Home 
  static map<string, string> _HomeHome; // key = names of indexors to Home at slow speed (2nd home) 
  static map<string, string> _InitVelCmd; // last/latest initial velocity command setting

  static map<string, double> _DesPosition; // desired/requested
  static map<string, double> _CurPosition; // current position (steps from home)
  static map<string, double> _Motion; // active/in-motion desired position (steps from home)
  static map<string, double> _NearHoming; // desired position while (virtual/near) homing should be = _NearH
  static map<string, double> _Homing; // desired position while homing should be = 0
  static map<string, double> _HomingFinal; // home-step-home
  static map<string, double> _LimitSeeking; // desired position while seeking limit should be = +-Limit

  static map<string, vector <string> > _ParamSads;

  // just use the PortescapConfig object attibutes...
  //static vector <string>& _Indexors; // names of All indexors from Config object 
  //static string& _Motors; // default list of indexor names (ala command line)

  // support compound motion sequences: home-step, near/not home-step, home-step-home, etc.
  int _reqHomeThenMore(UFDeviceAgent::CmdInfo* act);
  bool _nearHome(const string& motor, string& cmdimpl);
  void _execMovement(map<string, double>& moving);
  void _simMovement(map<string, double>& moving);
  string _guessSADchan(const string& indexor);
  int _parseXCmdReply(const string& indexor, const string& xReply);
};

#endif // __UFGemPortescapAgent_h__
      
