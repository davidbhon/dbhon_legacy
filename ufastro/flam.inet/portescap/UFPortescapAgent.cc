#if !defined(__UFPortescapAgent_cc__)
#define __UFPortescapAgent_cc__ "$Name:  $ $Id: UFPortescapAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPortescapAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFPortescapAgent.h"
#include "UFPortescapConfig.h"

// statics
// assuming space separated list of 1 character motor names:
vector <string>& UFPortescapAgent::_Indexors = UFPortescapConfig::_Indexors;
string& UFPortescapAgent::_Motors = UFPortescapConfig::_Motors;
map<string, double> UFPortescapAgent::_CurPosition; // current position (steps from home)

// ctors:
UFPortescapAgent::UFPortescapAgent(int argc, char** argv) : UFDeviceAgent(argc, argv) { _flush = 0.35; }

UFPortescapAgent::UFPortescapAgent(const string& name,
				   int argc, char** argv) : UFDeviceAgent(name, argc, argv) { _flush = 0.35; }

// static funcs:
int UFPortescapAgent::main(int argc, char** argv) {
  UFPortescapAgent ufs("UFPortescapAgent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
UFTermServ* UFPortescapAgent::init(const string& host, int port) {
  UFTermServ* ts= 0;
  if( ! _sim ) {
    // connect to the device:
    ts =_config->connect();
    if( _config->_devIO == 0 ) {
      clog<<"UFGemPortescapAgent> failed to connect to device port..."<<endl;
      return 0;
    }
  }
  // test connectivity to indexors, save transaction to a file
  string querystat, portescap; // portescap cmd & reply strings 
  string fullstatus = " [ 1990 23 "; // should be function in UFDeviceConfig
  // save transaction to a file
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.portescap."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFPortescapAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return ts;
  }
  delete s.str();
  clog<<"UFPortescapAgent> indexor names: "<<_Motors<<endl;
  for( int i = 0; i < (int)_Indexors.size(); ++i ) {
    querystat = _Indexors[i];
    querystat += fullstatus;
    int nc = _config->_devIO->submit(querystat, portescap, _flush); // test for a prompt
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status failed..."<<endl;
      break;
    }
    nc = ::write(fd, querystat.c_str(), querystat.length());
    nc = ::write(fd, portescap.c_str(), portescap.length());
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status log failed..."<<endl;
      break;
    }
  }
  ::close(fd);
  return ts;
}

// this should always return the service/agent listen port #
int UFPortescapAgent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  string arg;

  _config = new UFPortescapConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = 7005;
  _config->_tshost = "192.168.111.100";

  arg = findArg("-motor"); // motor names
  if( arg != "false" && arg != "true" )
    _Motors = arg;

  arg = findArg("-motors"); // motor names
  if( arg != "false" && arg != "true" )
    _Motors = arg;

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52005;
  }

  // should put these specifics into UFDeviceConfig subclass UFPortscapConfig...
  // assuming space separated list of 1 character motor names:
  int indxcnt = (1 + _Motors.length()) / 2;
  char* cm = (char*) _Motors.c_str(); // null-terminated c-string
  for( int i = 0; i < indxcnt; ++i, cm += 2 ) {
    *(1+cm) = '\0'; // replace space with null
    _Indexors.push_back(cm);
    *(1+cm) = ' '; // restor space
  }    

  clog<<"UFPortescapAgent::options> set port to Portescap port "<<port<<endl;
  return port;
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// to portescap indexor
// new signature for action:
int  UFPortescapAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  delete repv; repv = 0;
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFPortescapAgent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_expected = true;
  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_expected = false;
    clog<<"UFPortescapAgent::action> No reply will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }

  vector< string > reply;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool raw= false;
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    if( cmdname.find("raw") == 0 || cmdname.find("Raw") == 0 || cmdname.find("RAW") == 0 ) {
      raw = true;
      clog<<"UFPortescapAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFPortescapAgent::action> cmdimpl: "<<cmdimpl<<endl;
    }
    else {
      // deal with (agent) queries?
      //int qcnt = query(cmdname, act->cmd_reply);
      //if( qcnt < 0 )
      //  clog<<"UFDeviceAgent::> error checking for query?"<<endl;
      clog<<"UFPortescapAgent::action> sorry, Not (yet) implemented: "<<cmdname<<endl;
      continue;
    }
    // check if requires device iterraction, or is a query that can be handled 
    // without actual device interaction
    // if( _config && _config->interaction(cmdname, cndimpl) ) ...
    // this logic also belongs in the DeviceConfig class:
    const char *impl = cmdimpl.c_str();
    char m[2]; m[0] = impl[0]; m[1] = '\0';
    if( raw && _Motors.find(m) == string::npos ) { // raw commands MUST begin with indexor name
      act->status_cmd = "rejected";
      clog<<"UFPortescapAgent::action> illegal command, must idicate indexor name: "+_Motors<<endl;
      act->cmd_reply.push_back("illegal command, must idicate indexor name: "+_Motors);
      if( reply_expected )
        return new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply);
      else
	return (UFStrings*) 0;
    }
    if( cmdimpl.find("&") != string::npos ) { // stop processing list
      clog<<"UFPortescap::action> rejected: "<<cmdimpl<<endl;
      return (UFStrings*) 0;
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
     string reply;
    if( _config != 0 && _config->_devIO )
      stat = _config->_devIO->submit(cmdimpl, reply, _flush); // expect single line reply?

    act->cmd_reply.push_back(reply);
  } // end for loop of cmd bundle

  if( stat < 0 )
    act->status_cmd = "failed";
  else if( act->cmd_reply.empty() )
    act->status_cmd = "failed/rejected";
  else
    act->status_cmd = "succeeded";
  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) 
    return new UFStrings(act->clientinfo, act->cmd_reply);
  else
    return (UFStrings*) 0;
} 

int UFPortescapAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFPortescapAgent
