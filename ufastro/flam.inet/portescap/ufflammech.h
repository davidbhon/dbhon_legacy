#if !defined(__UFFLAMMECH_H__)
#define __UFFLAMMECH_H__
/* for use by C & C++ to describe mechanism names & positions */
/* assume Epics-like string length limits */

#include "stdlib.h"
#include "string.h"
#include "values.h"
#if defined(CYGWIN)
#include "libguile/values.h"
#else
#include "limits.h"
#endif
#include "math.h"

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

static const int __UFEPSLEN_ = 40; 
static const int __UFMAXPOSFLAM_ = 40; 
static const int __UFMAXINDXFLAM_ = 5; 

typedef struct {
  int posCnt; /* number of (<= max) positions (including home) in use */
  char *name; /* name[__UFEPSLEN_]; */
  char **positions; /* positions[__UFMAXPOSFLAM_][__UFEPSLEN_]; */
  float *steps; /* steps[__UFMAXPOSFLAM_]; + steps from home */
} UFFlamMechPos;

typedef struct __UFFlamMech_ {
  int mechCnt;
  char **indexor; /* indexor[__UFMAXINDXFLAM_][__UFEPSLEN_]; */
  UFFlamMechPos *mech; /* mech[__UFMAXINDXFLAM_]; */
} UFFlamMech;

/* non-reentrant non-thread-safe: */
static UFFlamMech* getTheUFFlamMech() {
  static char* _indexors[]= { "a", "b", "c", "d", "e" };

  static UFFlamMechPos dw;
  static char* _nameDW = "DeckerWheel"; 
  static char* _positionsDW[] = { "Home/Dark", "Open/Image", "MOS", "LongSlit" }; 
  static float   _stepDW[] = { 0, 2250, 4500, 6750 };

  static UFFlamMechPos mw;
  static char* _nameMW = "MOSWheel"; 
  static char* _positionsMW[] = { "Home/Open", "6PIX/A", "MOS/B", "2PIX/C", "MOS/D", "12PIX/E",
                                  "MOS/F", "20PIX/G", "MOS/H", "9PIX/J", "MOS/K", "MOS/L",
				  "MOS/M", "MOS/N", "MOS/O", "MOS/P", "MOS/Q", "3PIX/R" };
  static float   _stepMW[] = { 0, 87336, 83600, 76480, 69360, 62240,
                               55120, 48000, 40880, 37312, 33752, 30192,
			       26632, 23064, 19504, 15944, 12384, 8356 };
  static UFFlamMechPos fw;
  static char* _nameFW = "FilterWheel"; 
  static char* _positionsFW[] = { "Home/J-BAND", "H-BAND", "K-BAND", "HK-BAND", "JH-BAND", "Ks-BAND" };
  static float   _stepFW[] = { 0, 208, 417, 625, 833, 1042 };

  static UFFlamMechPos lw;
  static char* _nameLW = "LyotWheel"; 
  static char* _positionsLW[] = { "Home/DARK1", "DARK2", "OPEN", "GEMINI", "MMT", "4m-KPNO", "2.1m-KPNO" };
  static float   _stepLW[] = { 0, 178, 357, 536, 714, 893, 1071 };

  static UFFlamMechPos gw;
  static char* _nameGW = "GrismWheel"; 
  static char* _positionsGW[] = { "Home/Open1", "DARK1", "DARK2", "OPEN2", "HK" };
  static float   _stepGW[] =  { 0, 250, 500, 750, 1000 };

  static UFFlamMechPos* _mech= 0;
  static UFFlamMech _TheFlamMech;

  if( _mech != 0 ) 
    return &_TheFlamMech;

  _mech = (UFFlamMechPos*) calloc(__UFMAXINDXFLAM_, sizeof(UFFlamMechPos));

  dw.name = _nameDW;
  dw.positions = _positionsDW;
  dw.steps = _stepDW;
  dw.posCnt = _ArrayLen__(_stepDW);

  mw.name = _nameMW;
  mw.positions = _positionsMW;
  mw.steps = _stepMW;
  mw.posCnt = _ArrayLen__(_stepMW);

  fw.name = _nameFW;
  fw.positions = _positionsFW;
  fw.steps = _stepFW;
  fw.posCnt = _ArrayLen__(_stepFW);

  lw.name = _nameLW;
  lw.positions = _positionsLW;
  lw.steps = _stepLW;
  lw.posCnt = _ArrayLen__(_stepLW);

  gw.name = _nameGW;
  gw.positions = _positionsGW;
  gw.steps = _stepGW;
  gw.posCnt = _ArrayLen__(_stepGW);

  _TheFlamMech.mechCnt = __UFMAXINDXFLAM_;
  _TheFlamMech.indexor = _indexors; 
  _TheFlamMech.mech = _mech;

  _TheFlamMech.mech[0] = dw;
  _TheFlamMech.mech[1] = mw;
  _TheFlamMech.mech[2] = fw;
  _TheFlamMech.mech[3] = lw;
  _TheFlamMech.mech[4] = gw;

  return &_TheFlamMech;
}

// this assumes that char** names never been initialized
// and is passed via &names:
static int mechNamesFlam(char*** names) {
  static char** _names= 0;
  UFFlamMech* _TheFlamMech= getTheUFFlamMech();
  int i;
  if( _names == 0 ) {
    _names = (char**) calloc(__UFMAXINDXFLAM_, sizeof(char*));
    for( i = 0; i < _TheFlamMech->mechCnt; ++i ) 
      _names[i] = _TheFlamMech->mech[i].name;
  }

  *names = _names;
  return _TheFlamMech->mechCnt;
}
  
static double stepsFromHomeFlam(const char* mechName, char* posName) {
  int m, p;
  char* s= 0;
  UFFlamMechPos mech;
  UFFlamMech *_TheFlamMech = getTheUFFlamMech();

  for( m = 0; m < _TheFlamMech->mechCnt; ++m ) {
    mech = _TheFlamMech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  for( p = 0; p < mech.posCnt; ++p ) {
    if( (s = strstr(mech.positions[p], posName)) != 0 ) 
      break;
  }

  return mech.steps[p];
};

static char* posNameNearFlam(const char* mechName, double stepsFromHome) {
  static char* s= 0;
  int m, p;
  double diffmin= INT_MAX, diffs[__UFMAXPOSFLAM_];
  UFFlamMechPos mech;
  UFFlamMech *_TheFlamMech = getTheUFFlamMech();

  for( m = 0; m < _TheFlamMech->mechCnt; ++m ) {
    mech = _TheFlamMech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  for( p = 0; p < mech.posCnt; ++p ) {
    diffs[p] = fabs(stepsFromHome - mech.steps[p]);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
  }
  return s;
}

#endif /* __UFFLAMMECH_H__ */
