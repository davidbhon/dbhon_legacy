static const char rcsId[] = "$Name:  $ $Id: ufF2mechMotion.c,v 1.18 2007/01/25 19:49:16 hon Exp $";
#include "ufF2mechMotion.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "ctype.h"
#include "ufflam2mech.h"

/* make (some) of the static funcs. from ufflam2mech.h available externally (for SWIG) */
char* ufF2indexorOfMechName(const char* name) { return indexorOfMechName(name); }
char* ufF2mechNameOfIndexor(const char* idxor) { return mechNameOfIndexor(idxor); }
int ufF2setIndexorMechNameList(char* list) { return setIndexorMechNameList(list); }
double ufF2stepsFromHome(const char* mechName, char* posName) { return stepsFromHomeFLAM2(mechName,posName); }
char* ufF2posNameNear(const char* mechName, double steps) { return posNameNearFLAM2(mechName,steps); }
int ufF2setNamedPositionList(const char* mechName, char* posNames) { return setNamedPositionList(mechName,posNames); }
int ufF2printNamedPositions(const char* mechName) { return printNamedPositions(mechName); }
int ufF2printAllNamedPositions() { return printAllNamedPositions(); }
double ufF2park(const char* mechName) { return parkFLAM2(mechName); }

/*
The ANSI C "ufF2mechMotion()" should be the single entry point (function) used by (visible to)
the Epics DB logic. It is of course free to call any set of functions it
needs to do the job. It must be ANSI C to be of use to Ropberto's VME DB code.
And it can be used by the C++ portable DB as C code.

Return int number of string commands placed into pre-allocated char** buf.
Return <= 0 if there is an error of any kind.
 
If desiredPosition is == 0.0 assume Datum motion, otherwise step/move.

Example usage of "ufF2mechMotion()" is provided in the  main at the bottom of this file,
it compiles Ok via "gcc -ansi ufF2mechMotion.c"

The backlash is a (signed) value that may be used to indicate the +/- magnitude
of an intermediate or final motion. The difference between the newpos and the
current position provides the principal (signed value) motion magnitude which
should include any backlash over-shoot. That is, if a non-zero backlash is
provided, it should be added or subtracted (perhaps rescaled) from the difference 
of current and desired to provide the step count for the first motion, and then used
directly (again perhaps rescaled) for the second motion, etc. If more paramters
are needed beyond these, we'll do what we must.

I suggest writing a unique function for each mechanism:

ufF2moveWindow(); ufF2moveMOS(); ufF2moveDecker();
ufF2moveFilter1(); ufF2moveLyot(); ufF2moveFilter2();
ufF2moveGrism(); ufF2moveFocus();

I believe the motor agent currently only allows the following types of commands.
The Indexor char must be the first char (and spaces are Ok), followed by the indexor
single character command macro, followed by command params which are usually ints but
can be floats.

The following would apply to the Window:

Atomic in either direction step: "A+Steps" or "A-Steps". (in shorthand notation: "A +- Steps")
Atomic datum in either direction: "AF Speed Direction" (where speed is >=20, and Direction is 0 or 1)
Compound motion with backlash: "AN +- PrincipalSteps" ; "A +- BacklashSteps"    
Compound datum with backlash: "AF HighSpeed Direction" ; "A +- BacklashSteps" ; "AF SlowSpeed Direction"

If we need to support other combinations of motions in the agent, it will be a bit more work and rather
painful, considering the tangle of evolved logic present int the motor agent. The motor agent
must insure that the (allowed) compound sequences are spoon fed to the indexor firmware without
confusing it and causing it to hang, etc. It will scan the command 'bundle' sequence and either acccept
or reject it. If the bundle looks safe to perform (given whatever may be the perceived current state
of the indexor), it will queue them up and trickle them out through the Perle connection to the indexor, etc. 
*/

/* 'stub' implementation: */
static int ufF2moveWindow(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) {
  /*  double offset = desiredPosition - currentPosition; */
  char datum[] = "AM+200::0";
  char park[]  = "AM-200::-15000";

  if( fabs(desiredPosition) <= 0.001 ) { /* assume datum */
    strcpy(indexorCmdBuf[0], datum);
    return 1;
  }

  strcpy(indexorCmdBuf[0], park);
  return 1; 
}

static int ufF2moveWindow4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
  char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
  return ufF2moveWindow(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveWindowP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveWindow(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}

/* stub MOS */
static int ufF2moveMOS(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) {
  double offset = desiredPosition - currentPosition;
  char datum[] = "BF 200 0::0"; /* datum[7] is direction */
  char step[BUFSIZ]; memset(step, 0, BUFSIZ);

  if( offset > 0.001 )
    datum[7] = '1';

  if( fabs(desiredPosition) <= 0.001 ) { /* assume datum */
    strcpy(indexorCmdBuf[0], datum);
    return 1;
  }

  sprintf(step, "B %+d::%d", (int)offset, (int)(currentPosition+offset));
  strcpy(indexorCmdBuf[0], step);
  
  return 1;
}

static int ufF2moveMOS4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
  char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
  return ufF2moveMOS(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveMOSP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveMOS(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


/* stub Decker */
static int ufF2moveDecker(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) {
  double offset = desiredPosition - currentPosition;
  char datum[] = "CF 200 0::0"; /* datum[7] is direction */
  char step[BUFSIZ]; memset(step, 0, BUFSIZ);

  if( offset > 0.001 )
    datum[7] = '1';

  if( fabs(desiredPosition) <= 0.001 ) { /* assume datum */
    strcpy(indexorCmdBuf[0], datum);
    return 1;
  }

  sprintf(step, "C %+d::%d", (int)offset, (int)(currentPosition+offset));
  strcpy(indexorCmdBuf[0], step);
  
  return 1;
} 

static int ufF2moveDecker4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
  char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
  return ufF2moveDecker(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveDeckerP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveDecker(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


static int ufF2moveFilter1(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) { 
    double steps = 69000;
    double datumOffset = -1500;
    if (desiredPosition == 0) {
	/* datum */
        sprintf(indexorCmdBuf[0], "D F %d 0::0", fastVel);
        sprintf(indexorCmdBuf[1], "D %+d::%d", (int)datumOffset, (int)datumOffset);
        sprintf(indexorCmdBuf[2], "D F %d 0::0", slowVel);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
        return 3;
    } else {
	/* relative motion */
	double offset = desiredPosition-currentPosition;
	if (offset < 0) {
	    while (offset <= -0.5*steps) {
		offset+=steps;
	    }
	} else {
	    while (offset >= 0.5*steps) {
		offset-=steps;
	    }
	}
        if (offset == 0) return 0;
	if (offset+currentPosition >= steps || offset+currentPosition <= -1*steps) {
	    /* datum if +/- 1 rev */
	    int revs = (int)(offset+currentPosition) / (int)steps;
	    double move1 = revs*steps-currentPosition+datumOffset;
	    double move2 = offset-move1+datumOffset;
	    char cmda[32];
	    sprintf(cmda, "D %+d::%d",(int)move1, (int)(move1+currentPosition));
	    strcpy(indexorCmdBuf[0], cmda);
            sprintf(indexorCmdBuf[1], "D F %d 0::0", slowVel);
	    char cmdc[32];
	    if (offset < 0) {
	        sprintf(cmdc, "D %+d::%d",(int)move2, (int)move2);
		strcpy(indexorCmdBuf[2], cmdc);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
		return 3;
	    } else {
		/* use backlash if moving positive direction */
	        sprintf(cmdc, "D U %+d::%d",(int)(move2+backlash), (int)(move2+backlash));
                strcpy(indexorCmdBuf[2], cmdc);
		char cmdd[32];
		sprintf(cmdd, "D %+d::%d",(int)(-1*backlash), (int)move2);
                strcpy(indexorCmdBuf[3], cmdd);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
printf(indexorCmdBuf[3]);
printf("\n");
		return 4;
	    }
	} else if (offset < 0) {
	    /* single move */
	    char cmda[32];
	    sprintf(cmda, "D %+d::%d", (int)offset, (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[0], cmda);
printf(indexorCmdBuf[0]);
printf("\n");
	    return 1;
	} else {
            /* use backlash if moving positive direction */
	    char cmda[32];
	    char cmdb[32];
	    sprintf(cmda, "D U %+d::%d", (int)(offset+backlash), (int)(currentPosition+offset+backlash));
            strcpy(indexorCmdBuf[0], cmda);
	    sprintf(cmdb, "D %+d::%d", (int)(-1*backlash), (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[1], cmdb);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
	    return 2;
	}
    }
    return -1;
}

static int ufF2moveFilter14(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {

    char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
    return ufF2moveFilter1(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveFilter1P(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveFilter1(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


static int ufF2moveFilter2(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) { 
    double steps = 69000;
    double datumOffset = -1500;
    if (desiredPosition == 0) {
        /* datum */
	sprintf(indexorCmdBuf[0], "F F %d 0::0", fastVel);
	sprintf(indexorCmdBuf[1], "F %+d::%d", (int)datumOffset, (int)datumOffset);
	sprintf(indexorCmdBuf[2], "F F %d 0::0", slowVel);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
        return 3;
    } else {
        /* relative motion */
        double offset = desiredPosition-currentPosition;
        if (offset < 0) {
            while (offset <= -0.5*steps) {
                offset+=steps;
            }
        } else {
            while (offset >= 0.5*steps) {
                offset-=steps;
            }
        }
        if (offset == 0) return 0;
        if (offset+currentPosition >= steps || offset+currentPosition <= -1*steps) {
            /* datum if +/- 1 rev */
            int revs = (int)(offset+currentPosition) / (int)steps;
            double move1 = revs*steps-currentPosition+datumOffset;
            double move2 = offset-move1+datumOffset;
            char cmda[32];
            sprintf(cmda, "F %+d::%d",(int)move1, (int)(move1+currentPosition));
            strcpy(indexorCmdBuf[0], cmda);
            sprintf(indexorCmdBuf[1], "F F %d 0::0", slowVel);
            char cmdc[32];
            if (offset < 0) {
                sprintf(cmdc, "F %+d::%d",(int)move2, (int)move2);
                strcpy(indexorCmdBuf[2], cmdc);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
                return 3;
            } else {
                /* use backlash if moving positive direction */
                sprintf(cmdc, "F U %+d::%d",(int)(move2+backlash), (int)(move2+backlash));
                strcpy(indexorCmdBuf[2], cmdc);
                char cmdd[32];
                sprintf(cmdd, "F %+d::%d",(int)(-1*backlash), (int)move2);
                strcpy(indexorCmdBuf[3], cmdd);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
printf(indexorCmdBuf[3]);
printf("\n");
                return 4;
            }
        } else if (offset < 0) {
            /* single move */
            char cmda[32];
            sprintf(cmda, "F %+d::%d", (int)offset, (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[0], cmda);
printf(indexorCmdBuf[0]);
printf("\n");
            return 1;
        } else {
            /* use backlash if moving positive direction */
            char cmda[32];
            char cmdb[32];
            sprintf(cmda, "F U %+d::%d", (int)(offset+backlash), (int)(currentPosition+offset+backlash));
            strcpy(indexorCmdBuf[0], cmda);
            sprintf(cmdb, "F %+d::%d", (int)(-1*backlash), (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[1], cmdb);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
            return 2;
        }
    }
    return -1;
}

static int ufF2moveFilter24(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {

    char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
    return ufF2moveFilter2(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveFilter2P(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveFilter2(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


static int ufF2moveLyot(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) { 
    double steps = 2875;
    double datumOffset = -150;
    if (desiredPosition == 0) {
        /* datum */
        sprintf(indexorCmdBuf[0], "E F %d 0::0", fastVel);
        sprintf(indexorCmdBuf[1], "E %+d::%d", (int)datumOffset, (int)datumOffset);
        sprintf(indexorCmdBuf[2], "E F %d 0::0", slowVel);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
        return 3;
    } else {
        /* relative motion */
        double offset = desiredPosition-currentPosition;
        if (offset < 0) {
            while (offset <= -1*steps) {
                offset+=steps;
            }
        } else {
            while (offset >= 0) {
                offset-=steps;
            }
        }
        if (offset == 0 || offset == steps || offset == -1*steps) return 0;
        if (offset+currentPosition <= -1*steps) {
	    /* wheel only moves to negative steps -- datum if more than 1 rev */
            int revs = (int)(offset+currentPosition) / (int)steps;
            double move1 = revs*steps-currentPosition+datumOffset;
            double move2 = offset-move1+datumOffset;
            char cmda[32];
            sprintf(cmda, "E %+d::%d",(int)move1, (int)(move1+currentPosition));
            strcpy(indexorCmdBuf[0], cmda);
	    sprintf(indexorCmdBuf[1], "E F %d 0::0", slowVel);
            char cmdc[32];
            sprintf(cmdc, "E %+d::%d",(int)move2, (int)move2);
            strcpy(indexorCmdBuf[2], cmdc);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
            return 3;
        } else {
	    /* single move negative */
            char cmda[32];
            sprintf(cmda, "E %+d::%d", (int)offset, (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[0], cmda);
printf(indexorCmdBuf[0]);
printf("\n");
            return 1;
        }
    }
    return -1;
}

static int ufF2moveLyot4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {

    char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
    return ufF2moveLyot(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveLyotP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveLyot(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


static int ufF2moveGrism(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) { 
    double steps = 69000;
    double datumOffset = -1500;
    if (desiredPosition == 0) {
        /* datum */
        sprintf(indexorCmdBuf[0], "G F %d 0::0", fastVel);
        sprintf(indexorCmdBuf[1], "G %+d::%d", (int)datumOffset, (int)datumOffset);
        sprintf(indexorCmdBuf[2], "G F %d 0::0", slowVel);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
printf(indexorCmdBuf[2]);
printf("\n");
        return 3;
    } else {
        /* relative motion */
        double offset = desiredPosition-currentPosition;
        if (offset < 0) {
            while (offset <= -0.5*steps) {
                offset+=steps;
            }
        } else {
            while (offset >= 0.5*steps) {
                offset-=steps;
            }
        }
        if (offset == 0) return 0;
        if (offset+currentPosition >= steps) offset-=steps;
	if (offset+currentPosition <= -1*steps) offset+=steps; 
	/* never go +/- 1 rev for relative motions */
        if (offset < 0) {
	    /* single move */
            char cmda[32];
            sprintf(cmda, "G %+d::%d", (int)offset, (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[0], cmda);
printf(indexorCmdBuf[0]);
printf("\n");
            return 1;
        } else {
            /* use backlash if moving positive direction */
            char cmda[32];
            char cmdb[32];
            sprintf(cmda, "G U %+d::%d", (int)(offset+backlash), (int)(currentPosition+offset+backlash));
            strcpy(indexorCmdBuf[0], cmda);
            sprintf(cmdb, "G %+d::%d", (int)(-1*backlash), (int)(currentPosition+offset));
            strcpy(indexorCmdBuf[1], cmdb);
printf(indexorCmdBuf[0]);
printf("\n");
printf(indexorCmdBuf[1]);
printf("\n");
            return 2;
        }
    }
    return -1;
}

static int ufF2moveGrism4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {

    char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
    return ufF2moveGrism(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveGrismP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveGrism(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


/* stub focus */
static int ufF2moveFocus(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char** indexorCmdBuf) {
  double offset = desiredPosition - currentPosition;
  char datum[] = "HM-200::0";
  char park[]  = "HM+200::10000";
  char f16[]  = "H+5000::5000";
  char mcao[]  = "H+6000::6000";
  const int _park = 10000;
  char step[BUFSIZ]; memset(step, 0, BUFSIZ);

  if( fabs(desiredPosition) <= 0.001 ) { /* assume datum */
    strcpy(indexorCmdBuf[0], datum);
    return 1;
  }

  if( fabs(desiredPosition) > _park-1 ) { /* park */ 
    strcpy(indexorCmdBuf[0], park);
    return 1;
  }

  if( fabs(currentPosition) <= 0.001 ) {/* f/16 or MCAO */
    if (offset <= 5000 )
      strcpy(indexorCmdBuf[0], f16);
    else
      strcpy(indexorCmdBuf[0], mcao);
    return 1; 
  }

  sprintf(step, "H %+d::%d", (int)offset, (int)(currentPosition+offset));
  strcpy(indexorCmdBuf[0], step);
  return 1;
}

static int ufF2moveFocus4(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
  char* cmds[] = { indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3 };
  return ufF2moveFocus(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
}

static int ufF2moveFocusP(double currentPosition, double desiredPosition, double backlash, int fastVel, int slowVel, char* indexorCmd) {
    char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
    char* cmds[] = { c0, c1, c2, c3 };
    memset(cmds[0], 0, BUFSIZ); memset(cmds[1], 0, BUFSIZ);  memset(cmds[2], 0, BUFSIZ); memset(cmds[3], 0, BUFSIZ);
    memset(indexorCmd, 0, 1+strlen(indexorCmd));
    int nc = ufF2moveFocus(currentPosition, desiredPosition, backlash, fastVel, slowVel, cmds);
    switch (nc) {
        case 1:
            sprintf(indexorCmd, "%s", cmds[0]);
            break;
        case 2:
            sprintf(indexorCmd, "%s,%s", cmds[0], cmds[1]);
            break;
        case 3:
            sprintf(indexorCmd, "%s,%s,%s", cmds[0], cmds[1], cmds[2]);
            break;
        case 4:
            sprintf(indexorCmd, "%s,%s,%s,%s", cmds[0], cmds[1], cmds[2], cmds[3]);
            break;
        default:
            return -1;
    }
    return nc;
}


int ufF2mechMotion(char* mechname,
		   double currentPosition, double desiredPosition, double backlash, 
		   int fastVel, int slowVel, char** indexorCmdBuf) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

/*check that indexorCmdBuf is able to hold at least 4 commands*/
if (sizeof(indexorCmdBuf) < 4) return -3;

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': return ufF2moveWindow(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'B': return ufF2moveMOS(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'C': return ufF2moveDecker(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'D': return ufF2moveFilter1(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'E': return ufF2moveLyot(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'F': return ufF2moveFilter2(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'G': return ufF2moveGrism(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  case 'H': return ufF2moveFocus(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
    break;
  default: return -1;
    break;
  }
}

/* otherwise look at mechname (sub)strings and decide which func. to invoke: */

if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
  return ufF2moveWindow(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
  return ufF2moveMOS(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
  return ufF2moveDecker(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
  return ufF2moveFilter1(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
  return ufF2moveLyot(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
  return ufF2moveFilter2(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
  return ufF2moveGrism(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
  return ufF2moveFocus(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);

  return -2;
}


int ufF2mechMotion4(char* mechname,
		   double currentPosition, double desiredPosition, double backlash, 
		   int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': return ufF2moveWindow4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'B': return ufF2moveMOS4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'C': return ufF2moveDecker4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'D': return ufF2moveFilter14(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'E': return ufF2moveLyot4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'F': return ufF2moveFilter24(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'G': return ufF2moveGrism4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  case 'H': return ufF2moveFocus4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
    break;
  default: return -1;
    break;
  }
}

/* otherwise look at mechname (sub)strings and decide which func. to invoke: */

if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
  return ufF2moveWindow4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
  return ufF2moveMOS4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
  return ufF2moveDecker4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
  return ufF2moveFilter14(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
  return ufF2moveLyot4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
  return ufF2moveFilter24(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
  return ufF2moveGrism4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
  return ufF2moveFocus4(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);

  return -2;
}

int ufF2mechMotionP(char* mechname,
		   double currentPosition, double desiredPosition, double backlash, 
		   int fastVel, int slowVel, char* indexorCmd) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': return ufF2moveWindowP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'B': return ufF2moveMOSP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'C': return ufF2moveDeckerP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'D': return ufF2moveFilter1P(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'E': return ufF2moveLyotP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'F': return ufF2moveFilter2P(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'G': return ufF2moveGrismP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  case 'H': return ufF2moveFocusP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
    break;
  default: return -1;
    break;
  }
}

/* otherwise look at mechname (sub)strings and decide which func. to invoke: */

if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
  return ufF2moveWindowP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
  return ufF2moveMOSP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
  return ufF2moveDeckerP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
  return ufF2moveFilter1P(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
  return ufF2moveLyotP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
  return ufF2moveFilter2P(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
  return ufF2moveGrismP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
  return ufF2moveFocusP(currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);

  return -2;
}

int ufF2mechMotionNamed(char* mechname,
		   double currentPosition, char* desiredPositionName, double backlash, 
		   int fastVel, int slowVel, char** indexorCmdBuf) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/



if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': mechname = "Window";
    break;
  case 'B': mechname = "MOS";
    break;
  case 'C': mechname = "Decker";
    break;
  case 'D': mechname = "Filter1";
    break;
  case 'E': mechname = "Lyot";
    break;
  case 'F': mechname = "Filter2";
    break;
  case 'G': mechname = "Grism"; 
    break;
  case 'H': mechname = "Focus"; 
    break;
  default: return -1;
    break;
  }
}

else {
  /* otherwise look at mechname (sub)strings and decide which func. to invoke: */
  
  if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
    mechname = "Window";

  else if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
    mechname = "MOS";

  else if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
    mechname = "Decker";

  else if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
    mechname = "Filter1";

  else if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
    mechname = "Lyot";

  else if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
    mechname = "Filter2";

  else if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
    mechname = "Grism";

  else if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
    mechname = "Focus";

  else return -2;
}
double desiredPosition = stepsFromHomeFLAM2(mechname, desiredPositionName);

if (desiredPosition == INT_MIN) return -3;

return ufF2mechMotion(mechname, currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmdBuf);
}

int ufF2mechMotion4Named(char* mechname,
		   double currentPosition, char* desiredPositionName, double backlash, 
		   int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': mechname = "Window";
    break;
  case 'B': mechname = "MOS";
    break;
  case 'C': mechname = "Decker";
    break;
  case 'D': mechname = "Filter1";
    break;
  case 'E': mechname = "Lyot";
    break;
  case 'F': mechname = "Filter2";
    break;
  case 'G': mechname = "Grism"; 
    break;
  case 'H': mechname = "Focus"; 
    break;
  default: return -1;
    break;
  }
}

else {
  /* otherwise look at mechname (sub)strings and decide which func. to invoke: */
  
  if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
    mechname = "Window";

  else if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
    mechname = "MOS";

  else if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
    mechname = "Decker";

  else if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
    mechname = "Filter1";

  else if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
    mechname = "Lyot";

  else if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
    mechname = "Filter2";

  else if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
    mechname = "Grism";

  else if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
    mechname = "Focus";

  else return -2;
}
double desiredPosition = stepsFromHomeFLAM2(mechname, desiredPositionName);
printf("DES POS: %f\n",desiredPosition);

if (desiredPosition == INT_MIN) return -3;

return ufF2mechMotion4(mechname, currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd0, indexorCmd1, indexorCmd2, indexorCmd3);
}


int ufF2mechMotionPNamed(char* mechname,
                   double currentPosition, char* desiredPositionName, double backlash,
                   int fastVel, int slowVel, char* indexorCmd) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return 0;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': mechname = "Window";
    break;
  case 'B': mechname = "MOS";
    break;
  case 'C': mechname = "Decker";
    break;
  case 'D': mechname = "Filter1";
    break;
  case 'E': mechname = "Lyot";
    break;
  case 'F': mechname = "Filter2";
    break;
  case 'G': mechname = "Grism";
    break;
  case 'H': mechname = "Focus";
    break;
  default: return -1;
    break;
  }
}

else {
  /* otherwise look at mechname (sub)strings and decide which func. to invoke: */

  if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
    mechname = "Window";

  else if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
    mechname = "MOS";

  else if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
    mechname = "Decker";

  else if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
    mechname = "Filter1";

  else if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
    mechname = "Lyot";

  else if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
    mechname = "Filter2";

  else if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
    mechname = "Grism";

  else if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
    mechname = "Focus";

  else return -2;
}
double desiredPosition = stepsFromHomeFLAM2(mechname, desiredPositionName);
printf("DES POS: %f\n",desiredPosition);

if (desiredPosition == INT_MIN) return -3;

return ufF2mechMotionP(mechname, currentPosition, desiredPosition, backlash, fastVel, slowVel, indexorCmd);
}


char* getPositionName(char* mechname,
                   double currentPosition) {
/* mechname might be one char indexor name (A-H), or mechanism names provided by ufflam2mech.h */
if( mechname == 0 ) /* defend against coredumps */
  return NULL;

/*
note that the following may fail if the machname has any leading or trailing white spaces,
so that should be dealt with (more defensive programming)
*/

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': mechname = "Window";
    break;
  case 'B': mechname = "MOS";
    break;
  case 'C': mechname = "Decker";
    break;
  case 'D': mechname = "Filter1";
    break;
  case 'E': mechname = "Lyot";
    break;
  case 'F': mechname = "Filter2";
    break;
  case 'G': mechname = "Grism";
    break;
  case 'H': mechname = "Focus";
    break;
  default: return NULL;
    break;
  }
}

else {
  /* otherwise look at mechname (sub)strings and decide which func. to invoke: */

  if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
    mechname = "Window";

  else if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
    mechname = "MOS";

  else if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
    mechname = "Decker";

  else if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
    mechname = "Filter1";

  else if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
    mechname = "Lyot";

  else if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
    mechname = "Filter2";

  else if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
    mechname = "Grism";

  else if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
    mechname = "Focus";

  else return NULL;
}

return posNameNearFLAM2(mechname, currentPosition);
}

void getNamedPositions(char* mechname) {
if( mechname == 0 ) /* defend against coredumps */
  return;

if( strlen(mechname) == 1 ) {
  /* assume single char indexor name */
  switch(mechname[0]) {
  case 'A': mechname = "Window";
    break;
  case 'B': mechname = "MOS";
    break;
  case 'C': mechname = "Decker";
    break;
  case 'D': mechname = "Filter1";
    break;
  case 'E': mechname = "Lyot";
    break;
  case 'F': mechname = "Filter2";
    break;
  case 'G': mechname = "Grism";
    break;
  case 'H': mechname = "Focus";
    break;
  default: return;
    break;
  }
}

else {
  /* otherwise look at mechname (sub)strings and decide which func. to invoke: */

  if( strstr("Window", mechname) || strstr(mechname, "DOW") || strstr(mechname, "dow") )
    mechname = "Window";

  else if( strstr("MOS", mechname) || strstr(mechname, "OS" )|| strstr(mechname, "os") )
    mechname = "MOS";

  else if( strstr("Decker", mechname) || strstr(mechname, "KER") || strstr(mechname, "ker") )
    mechname = "Decker";

  else if( strstr("Filter1", mechname) || strstr(mechname, "ER1" ) || strstr(mechname, "er1") )
    mechname = "Filter1";

  else if( strstr("Lyot", mechname) || strstr(mechname, "YOT") || strstr(mechname, "yot") )
    mechname = "Lyot";

  else if( strstr("Filter2", mechname) || strstr(mechname, "ER2" ) || strstr(mechname, "er2") )
    mechname = "Filter2";

  else if( strstr("Grism", mechname) || strstr(mechname, "ISM") || strstr(mechname, "ism") )
    mechname = "Grism";

  else if( strstr("Focus", mechname) || strstr(mechname, "CUS" ) || strstr(mechname, "cus") )
    mechname = "Focus";

  else return;
}

printNamedPositions(mechname);
}

int parseMotorString(char* motorString, int n, char* command) {
  int j, pos= INT_MIN, step= INT_MIN;
  char motion[BUFSIZ], temp[BUFSIZ], *tmp = &temp[0];
  char *s = 0;

  /* Empty buffers */
  memset(motion, 0, BUFSIZ);
  memset(temp, 0, BUFSIZ);
  memset(command, 0, strlen(command));

  strcpy(tmp, motorString);
  s = strstr(tmp, ",");
  if( s == 0) {
    strcpy(motion,tmp);
/*
    return INT_MIN;
*/
  } else {

     pos = (int)s - (int)tmp;
     strncpy(motion, tmp, pos); 
  }

  /*if n > 0, reset pos to find nth command*/
  for (j = 0; j < n; j++) {
    s = strstr(tmp, ",");
    if( s == 0 ) return INT_MIN;
/*
    pos = (int)s - (int)tmp;
    strncpy(motion, ++s, pos);
*/
    strcpy(motion, ++s);
    tmp = s;
  }   
  s = strstr(motion, "::");
  if( s == 0 )
    return INT_MIN;

  /*find step count*/

  pos = (int)s - (int)motion;
  strncpy(command, motion, pos);

  step = atoi(strstr(motion,"::")+2);
  return step; 
}

int cmdTransaction(char* indexorCmds, char* cmdName) {
  /* assume comma and double colon syntax for indexorCmds and construct the cmdName
     string: "[A-H]F2-N::FinalSteps" where N is 1-9*/
  int nc = 1, nc_ascii = 49; /* 49 == ascii decimal '1' */
  char *comma = strchr(indexorCmds, ',');
  char *fsteps = strrchr(indexorCmds, ':');
  if( fsteps != 0 )
    ++fsteps;
  else
    return -1;

  if( cmdName == 0 )
    return -2;
  
  /* check for white spaces, but otherwise assume first char is indexeor */
  while( *indexorCmds == ' ' || *indexorCmds == '\t' || *indexorCmds == '\r' || *indexorCmds == '\n' )
    ++indexorCmds;

  /* count the number of commas */
  while( comma != 0 ) {
    ++nc; ++nc_ascii;
    comma = strchr(1+comma, ',');
  }
  cmdName[0] = indexorCmds[0];
  cmdName[1] = 'F'; cmdName[2] = '2' ; cmdName[3] = '-'; cmdName[4] = (char)nc_ascii; cmdName[5] = ':'; cmdName[6] = ':';
  strcpy(&cmdName[7], fsteps);

  return strlen(cmdName);
}

