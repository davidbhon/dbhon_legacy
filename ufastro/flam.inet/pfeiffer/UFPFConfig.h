#if !defined(__UFPFConfig_h__)
#define __UFPFConfig_h__ "$Name:  $ $Id: UFPFConfig.h,v 0.1 2004/02/26 17:45:47 hon beta $"
#define __UFPFConfig_H__(arg) const char arg##UFPFConfig_h__rcsId[] = __UFPFConfig_h__;

#include "UFDeviceConfig.h"

class UFPFConfig : public UFDeviceConfig {
public:
  UFPFConfig(const string& name= "UnknownPF@DefaultConfig");
  UFPFConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFPFConfig() {}

  // override these virtuals:
  virtual vector< string >& UFPFConfig::queryCmds();
  virtual vector< string >& UFPFConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // override these virtuals:
  // device i/o behavior
  //virtual string prefix();
  virtual string terminator();
  virtual UFTermServ* connect(const string& host= "", int port= 0);
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);
};

#endif // __UFPFConfig_h__
