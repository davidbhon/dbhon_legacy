#if !defined(__UFGPConfig_h__)
#define __UFGPConfig_h__ "$Name:  $ $Id: UFGPConfig.h,v 0.0 2003/11/04 19:17:22 hon beta $"
#define __UFGPConfig_H__(arg) const char arg##UFGPConfig_h__rcsId[] = __UFGPConfig_h__;

#include "UFDeviceConfig.h"

class UFGPConfig : public UFDeviceConfig {
public:
  UFGPConfig(const string& name= "UnknownGP@DefaultConfig");
  UFGPConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFGPConfig() {}

  // override these virtuals:
  virtual vector< string >& UFGPConfig::queryCmds();
  virtual vector< string >& UFGPConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  //defaults are ok
  //virtual string terminator();
  //virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);
};

#endif // __UFGPConfig_h__
