#!/bin/tcsh -f
# rcsId = $Name:  $ $Id: ufstartfu,v 0.1 2006/02/23 21:15:19 drashkin Exp $
#
set instrum = flam
set dhssys = `hostname`
#set dhssys = irflam2a
mkdir -p /data/${instrum} >& /dev/null
#
# note that this script should be used by the FLAM Executive daemon (ufg${instrum}) for starting/stopping, i.e.
# performing "soft reboots" of the system
# start a specific agent via first cmd line parameter: "ufsimagents ufagentname ...usual params..."
# start all agents, including ${instrum} executive via d cmd line parameter: "ufsimagents -all ...usual params..."
# start all agents, excluding ${instrum} executive via first cmd line parameter: "ufsimagents -ex ..usual params...."
if( ! $?UFINSTALL ) setenv UFINSTALL /share/local/uf
#setenv LD_LIBRARY_PATH ${UFINSTALL}/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv
#set path = (./ ~/bin $UFINSTALL/bin $UFINSTALL/sbin /usr/local/bin /opt/EDTpdv /usr/X11R6/bin /usr/java/bin /bin /etc /usr/bin /sbin /usr/sbin)
#
source $UFINSTALL/.ufcshrc
# linux and cygwin static binaries work, but dynamic/shared do not:
setenv UFBIN ${UFINSTALL}/sbin
set procls = '/bin/ps -ef'
set osname = `/bin/uname | cut -d'_' -f1`
if( "$osname" == "SunOS" ) then
  setenv UFBIN ${UFINSTALL}/bin
endif
if( "$osname" == "Linux" ) then
  set procls = '/bin/ps -efw'
endif
#
set slp = 1
# just list agents this can start:
if( "$1" == "-h" || "$1" == "-help" ) then
  echo '"ufsimagents [-a]" -- starts all agents with default runtime options'
  echo '"ufsimagents -l/ls/list" -- list installed agents'
#  echo '"ufsimagents -ex" -- start all agents except the executive'
  echo '"ufsimagents ufagentname" -- start only specified agent'
  exit
endif
if( "$1" == "-l" || "$1" == "-ls" || "$1" == "-list" ) then
  \ls -lqF $UFBIN/uffuepicsd
  \ls -lqF $UFBIN/ufgedtd
  \ls -lqF $UFBIN/ufdhsput
  \ls -lqF $UFBIN/ufgls218d
  \ls -lqF $UFBIN/ufgls33xd
  \ls -lqF $UFBIN/ufgpf26xvacd
  \ls -lqF $UFBIN/ufgmotord
  \ls -lqF $UFBIN/ufgsymbarcd
  \ls -lqF $UFBIN/ufglvdtd
  \ls -lqF $UFBIN/ufgisplcd
  \ls -lqF $UFBIN/ufgmce4d
  \ls -lqF $UFBIN/ufg${instrum}2d
  exit
endif
set epics = "-epics fu"
#if( "$1" == "" || "$1" == "-all" ) then
#  set epics = "-sim -epics ${instrum} "
#endif
if( "$1" == "-noepics" ) then
  set epics = "-noepics"
endif
set jd = `date "+%Y:%j:%H:%M"`
set agent = "unknown"
# presumably all agent/daeomn names start with uf and end with d:
if( "$1" == "uffuepicsd" ) then 
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "$argv[2-]"
endif
if( "$1" == "ufgedtd" ) then 
  mkdir -p /data/${instrum}
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "$argv[2-]"
endif
if( "$1" == "ufdhsput" ) then 
#  $UFBIN/dhsClientBoot start
  set agent = ${UFINSTALL}/bin/"$1"
  set params = -dhs ${dhssys} "$argv[2-]"
endif
if( "$1" == "ufgls218d" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7002 $argv[2-]"
endif
if( "$1" == "ufgls33xd" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7003 $argv[2-]"
endif
if( "$1" == "ufgpf26xvacd" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7004 $argv[2-]"
endif
if( "$1" == "ufgmotord" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7024 $argv[2-]"
#  set params = " $argv[2-]"
endif
if( "$1" == "ufgsymbarcd") then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7005 $argv[2-]"
endif
if( "$1" == "ufglvdtd" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7006 $argv[2-]"
endif
if( "$1" == "ufgisplcd" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost ${instrum}perle -tsport 7007 $argv[2-]"
endif
if( "$1" == "ufgmce4d" ) then
  set agent = ${UFINSTALL}/bin/"$1"
  set params = "-tshost 192.168.111.103 -tsport 7008 $argv[2-]"
endif
pgrep -l $agent:t
if( $status == 0 ) then
  echo Sorry, "$agent" is already up, please use ufstop first...
  exit
endif
if( -x $agent && "$agent" != "unknown" ) then
  $agent $epics $params >& /usr/tmp/$1.${jd} &
  echo UF agents currently running:
  $procls | grep $UFINSTALL | grep uf | grep d | grep -v jei
  exit
endif
# is there is no leading dash, assume this is not an option, rather
# just a typo or mispelled arg
set arg = `echo $1 | egrep -v '\-'`
if( "$arg" != "" ) then
  echo Sorry, "$1" '(' "$arg" ')' is ambiguous...
  exit
endif
if( -e /tmp/.ufSimAgents ) then
  echo Sorry, lockfile /tmp/.ufSimAgents already exists, please use ufstop first...
  exit
endif
# if we get here start all or all but executive
touch /tmp/.ufSimAgents
# rm all existing log files (for now...) --hon
#ufpurge /var/tmp 0
# presumably all agent/daeomn names start with uf and end with d:
# bring up the epics portable db (instrum. seq. and sad):
if( $status != 0 ) then
  echo start caRepeater
  set log = /usr/tmp/caRepeater.${jd}
  caRepeater >& $log &
endif
echo start uffuepicsd
set log = /usr/tmp/uffuepicsd.${jd}
$UFBIN/uffuepicsd $argv[1-] >& $log &
# give epics runtime a chance to init itself before starting all other agents
set status = 1
while ( $status != 0 ) 
  grep started $log
  sleep $slp
end
# frame acquisition daemon (in sim mode, ignore the EDT card):
# not sim!
set log = /usr/tmp/ufgedtd.${jd}
echo start ufgedtd -v $epics $argv[1-] 
$UFBIN/ufgedtd -v $epics $argv[1-] >& $log &
set status = 1
while ( $status != 0 ) 
  grep listen $log
  sleep $slp
end
# lakeshore 332 (array temp. controller) daemon:
echo start ufgls33xd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7003
set log = /usr/tmp/ufgls33xd.${jd}
$UFBIN/ufgls33xd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7003 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# lakeshore 218 daemon:
set log = /usr/tmp/ufgls218d.${jd}
echo start ufgls218d -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7002
$UFBIN/ufgls218d $epics $argv[1-] -tshost ${instrum}perle -tsport 7002 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# pfeiffer vacuum monitor daemon:
set log = /usr/tmp/ufgpf26xvacd.${jd}
echo start ufgpf26xvacd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7004
$UFBIN/ufgpf26xvacd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7004 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# potescap motor indexor daemon:
set log = /usr/tmp/ufgmotord.${jd}
#echo start ufgmotord $epics $argv[1-] -tshost ${instrum}perle -tsport 7024
#$UFBIN/ufgmotord $epics $argv[1-] -tshost ${instrum}perle -tsport 7024 -motors 'A:7024 B:7023 C:7022 D:7021 E:7020 F:7019 G:7018' >& $log &
#$UFBIN/ufgmotord $epics $argv[1-] -tshost ${instrum}perle -tsport 7024 >& $log &
echo start ufgmotord -sim $epics $argv[1-]
$UFBIN/ufgmotord -sim $epics $argv[1-] >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# symbol barcode daemon:
set log = /usr/tmp/ufgsymbarcd.${jd}
#echo start ufgsymbarcd $epics $argv[1-] -tshost ${instrum}perle -tsport 7005
echo start ufgsymbarcd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7005
$UFBIN/ufgsymbarcd $epics -sim $argv[1-] -tshost ${instrum}perle -tsport 7005 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# schaevitz lvdt daemon:
set log = /usr/tmp/ufglvdtd.${jd}
#echo start ufglvdtd $epics $argv[1-] -tshost ${instrum}perle -tsport 7006
echo start ufglvdtd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7006
$UFBIN/ufglvdtd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7006 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# automation plc (gis) daemon:
set log = /usr/tmp/ufgisplcd.${jd}
#echo start ufgisplcd $epics $argv[1-] -tshost ${instrum}perle -tsport 7007
echo start ufgisplcd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7007
$UFBIN/ufgisplcd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7007 >& $log &
#
# detector controller (mce4) daemon:
# this should come up after all other device agents because it does a FITS connect (to all other device agents) on startup:
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# not sim!
echo start ufgmce4d $epics $argv[1-] -tshost 192.168.111.103 -tsport 7008
set log = /usr/tmp/ufgmce4d.${jd}
$UFBIN/ufgmce4d $epics $argv[1-] -tshost 192.168.111.103 -tsport 7008 >& $log &
set status = 1
while ( $status != 0 )
  grep listen $log
  sleep $slp
end
# optionally start the ${instrum} executive:
# note that the ${instrum} executive communicates with the baytech rpc-3
# and handles requests for powerOn-Off of each/all instrument hardware component(s)
# so it is responsible for turning the cryo-cooler "coldhead" on or off:
set log = /usr/tmp/ufg${instrum}2d.${jd}
#pgrep -lf ufg${instrum}2d
#if( $status != 0 ) then
#  $UFBIN/ufg${instrum}2d $epics $argv[1-] -baytech irbaytech -tsport 7001 >& $log &
#endif
#set status = 1
#while ( $status != 0 )
#  grep listen $log
#  sleep $slp
#end
# not sim!
# gemini dhs client daemon is last -- it should connect to ufgedtd as replication client
# and it also connects to all device agents for fits (nod-sad route)
# assume dhs service ihas been booted (either on this host or elsewhere)
#$UFBIN/dhsClientBoot start
#sleep $slp
set log = /usr/tmp/ufdhsput.${jd}
echo start ufdhsput $epics -dhs ${dhssys} $argv[2-] \(ufgdhsd\)
$UFBIN/ufdhsput $epics -dhs ${dhssys} $argv[2-] >& $log &
#$UFBIN/ufdhsput -dhs ${dhssys} $argv[2-] >& $log &
#$UFBIN/ufdhsput >& $log &
#
echo UF agents started:
$procls | grep $UFINSTALL | grep uf | grep d | grep -v jei
# purge older files from local archive, since this can take a while, do it last:
#ufpurge /data/${instrum}
exit
