#if !defined(__UFFLAMExecutiveAgent_cc__)
#define __UFFLAMExecutiveAgent_cc__ "$Name:  $ $Id: UFFLAMExecutiveAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFLAMExecutiveAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFFLAMExecutiveAgent.h"
#include "UFExecutiveConfig.h"

UFBaytech* UFFLAMExecutiveAgent::_baytech= 0;
string* UFFLAMExecutiveAgent::_boot= 0; // bring up all the device agents?
bool UFFLAMExecutiveAgent::_powerOnAll= false;
bool UFFLAMExecutiveAgent::_powerOffAll= false;
int UFFLAMExecutiveAgent::_pulsePeriod= 60;

// static funcs:
void UFFLAMExecutiveAgent::sighandler(int sig) {
  if( sig == SIGCHLD ) { // presumably this is the _capipe child:
    clog<<"UFFLAMExecutiveAgent::sighandler> child-death, sig: "<<sig<<endl;
    //UFRndRobinServ::_childdeath = true;
    //::pclose(_capipe);
    //_capipe = 0;
    return;
  }
  if( sig == SIGPIPE ) {
    clog<<"UFFLAMExecutiveAgent::sighandler> socket or pipe closed, sig: "<<sig<<endl;
    UFRndRobinServ::_lost_connection = true;
    return;
  }
  strstream s;
  s<<"/usr/bin/pkill -"<<sig<<ends;
  string sigcmd = s.str(); delete s.str();
  string scmd;
  if( sig != SIGINT && sig != SIGTERM ) {
    clog<<"UFFLAMExecutiveAgent::sighandler> forward sig: "<<sig<<" to all agents."<<endl;
    // blindly forward all other signals to all device agents.
    scmd = sigcmd + " ufgmce4d"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgedtd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgmotord"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgpf26xvacd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgls33xd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgls218d"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufglvdtd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgsymbarcd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufgisplcd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufcaputarr"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufcaget"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufdhsput"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> "<<scmd<<endl;
    scmd = sigcmd + " ufflam2epicsd"; system(scmd.c_str());
    clog<<"UFFLAMExecutiveAgent::sighandler> (NOT!) "<<scmd<<endl;
  }
  if( sig == SIGTERM ) {
    clog<<"UFFLAMExecutiveAgent::sighandler> shutdown/terminate, sig: "<<sig<<endl;
    sigcmd = "/usr/bin/pkill -TERM";
    /*
    scmd = sigcmd + " ufgmce4d"; system(scmd.c_str());
    scmd = sigcmd + " ufgedtd"; system(scmd.c_str());
    scmd = sigcmd + " ufgmotord"; system(scmd.c_str());
    scmd = sigcmd + " ufgpf26xvacd"; system(scmd.c_str());
    scmd = sigcmd + " ufgls33xd"; system(scmd.c_str());
    scmd = sigcmd + " ufgls218d"; system(scmd.c_str());
    scmd = sigcmd + " ufglvdtd"; system(scmd.c_str());
    scmd = sigcmd + " ufgsymbarcd"; system(scmd.c_str());
    scmd = sigcmd + " ufgisplcd"; system(scmd.c_str());
    scmd = sigcmd + " ufcaputarr"; system(scmd.c_str());
    scmd = sigcmd + " ufcaget"; system(scmd.c_str());
    scmd = sigcmd + " ufdhsput"; system(scmd.c_str());
    scmd = sigcmd + " ufflam2epicsd"; system(scmd.c_str()); 
    scmd = sigcmd + " caRepeater"; system(scmd.c_str()); // also kill caRepeater (desirable?)
    */
    // use standard shutdown script (also purges /var/tmp)
    // which terminates all agents except this one (the executive)
    char* uf = getenv("UFINSTALL"); 
    if( uf == 0 )
      scmd = "/share/local/uf";
    else
      scmd = uf;
    scmd += "/bin/ufstop -a";
    clog<<"UFFLAMExecutiveAgent::sighandler> shutdown/terminate, cmd: "<<scmd<<endl;
    system(scmd.c_str());

    clog<<"UFFLAMExecutiveAgent::sighandler> all done... "<<endl;
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  if( sig == SIGINT ) {
    clog<<"UFFLAMExecutiveAgent::sighandler> interrupt/terminate, sig: "<<sig<<endl;
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  return UFRndRobinServ::sighandlerDefault(sig);
}

// this virtual should be called from options()...
void UFFLAMExecutiveAgent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;

  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    _confGenSub = "";
    _heart = _epics + ":heartbeat.VAL";
  }
}

// ctors:
UFFLAMExecutiveAgent::UFFLAMExecutiveAgent(int argc,
					   char** argv,
					   char** envp) : UFGemDeviceAgent(argc, argv, envp) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  _Update = 1.0; // rather than 1.0 sec.
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //_confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:heartbeat.executive";
  //_statRec = _epics + ":ec:sysexec.INP"; 
  _statRec = _epics + ":ec:sysexec.VAL"; 
}

UFFLAMExecutiveAgent::UFFLAMExecutiveAgent(const string& name,
					   int argc,
					   char** argv,
 				           char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  _Update = 1.0; // rather than 1.0 sec.
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //_confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:heartbeat.executve";
  //_statRec = _epics + ":ec:sysexec.INP"; // 
  _statRec = _epics + ":ec:sysexec.VAL"; // 
}

// static funcs:
int UFFLAMExecutiveAgent::main(int argc, char** argv, char** envp) {
  UFFLAMExecutiveAgent ufs("UFFLAMExecutiveAgent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFFLAMExecutiveAgent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  // _epics may have been reset:
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:heartbeat.executive";
  //_statRec = _epics + ":ec:sysexec.INP"; // 
  _statRec = _epics + ":ec:sysexec.VAL"; // 

  _config = new UFExecutiveConfig(); // needs to be proper device subclass 
  // defaults for flam2 exec:
  // the baytech is actually telnet port 23, but this agent listens on 52000
  _config->_tsport = 7000;
  _config->_tshost = "false";

  string arg;
  arg = findArg("-boot");  // start all agents
  if( arg != "false" ) {
    if( arg != "true" ) // assume argval provides name
      _boot = new string(arg);
    else
      _boot = new string("ufstartagents"); // default start script
  }

  arg = findArg("-sim");
  if( arg != "false" ) {
    _sim = true;
    if( arg != "true" ) // assume argval provides name
      _boot = new string(arg);
    else
      _boot = new string("ufsimagents"); // default start script
  }

  arg = findArg("-noepics"); // don't connect to epics db
  if( arg == "true" ) {
    _epics = "false";
    _heart = "";
    _statRec = "";
  }

  arg = findArg("-epics"); // epics db name
  if( arg != "false" && arg != "true" ) {
    _epics = arg;
    //_confGenSub = _epics + ":ec:configG";
    _heart = _epics + ":ec:heartbeat.executive";
    //_statRec = _epics + ":ec:sysexec.INP"; // 
    _statRec = _epics + ":ec:sysexec.VAL"; // 
  }

  arg = findArg("-pulse");  // heartbeat period
  if( arg != "true" && arg != "false" ) {
    _pulsePeriod = atoi(arg.c_str());
  }

  arg = findArg("-on");  // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-pwron"); // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-powerOn"); // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-off");  // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-pwroff"); // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-powerOff"); // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-heart"); // heartbeat epics record
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost"); // interpret as baytech host, not terminal console!
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-baytech"); // connect to baytech, not terminole console
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-nobaytech"); // do not connect to baytech
  if( arg == "true" )
    _config->_tshost = "false";
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    //UFDeviceConfig::_verbose = true;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52000;
  }
  //if( _verbose ) 
  clog<<"UFFLAMExecutiveAgent::options> port "<<port<<", _boot: "<<_boot<<", _sim: "<<_sim<<endl;

  return port;
}

// override base class startup here to avoid connect to term. serv
// if no motors are specified, assume simulation is desired,
// then do server listen stuff
void UFFLAMExecutiveAgent::startup() {
  setSignalHandler(UFFLAMExecutiveAgent::sighandler);
  clog << "UFFLAMExecutiveAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  init();
  if( _sim ) {
    clog<<"UFFLAMExecutiveAgent::startup> simulation."<<endl;
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFFLAMExecutiveAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFFLAMExecutiveAgent::init(const string& host, int port) {
  _capipe = 0;
  /*
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild("flam");
    if( _capipe )
      clog<<"UFFLAMExecutiveAgent> Epics CA child proc. started"<<endl;
    else
      clog<<"UFFLAMExecutiveAgent> Ecpics CA child proc. failed to start"<<endl;
  }
  */
  //if( _config->_tshost == "" && host != "" )
  //  _config->_tshost = host;
  
  // connect to baytech:
  if( _sim || _config->_tshost == "false" ) {
    _baytech = 0;
  }
  else {
    _baytech = UFBaytech::create(_config->_tshost);
    if( _baytech == 0 ) {
      clog<<"UFFLAMExecutiveAgent::init> unable to connect to "<<_config->_tshost<<endl;
      UFRndRobinServ::shutdown();
    }
  }
  // test connectivity to baytech:
  string btstat= "";
  if( _baytech )
    btstat = _baytech->status();
  else
    btstat = UFBaytech::statusSim();

  /* 
  strstream s;
  s<<"/tmp/.ufbaytech."<<getpid()<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFFLAMExecutiveAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  //clog<<"UFFLAMExecutiveAgent> status (sim: "<<_sim<<" Baytech channels 1 - 8)"<<endl;
  //clog<<"UFFLAMExecutiveAgent> reply: "<<btstat<<ends;
  int nc = ::write(fd, btstat.c_str(), btstat.length());
  if( nc <= 0 ) {
    clog<<"UFFLAMExecutiveAgent::init> status log failed..."<<endl;
  }
  ::close(fd);
  */
  if( _verbose && _baytech )
    clog<<"UFFLAMExecutiveAgent::init> baytech status: "<<btstat<<endl;

  if( _powerOffAll && _baytech )
    _baytech->powerOff();

  //if( (_boot || _powerOnAll) && _baytech )
  if( _powerOnAll && _baytech != 0 )
    _baytech->powerOn();

  //if( _baytech ) { _baytech->close(); _baytech = 0; }

  if( _boot ) {
    string opt;
    if( _verbose )
      opt += " -v";
    if( _epics.empty() || _epics == "false" ) {
      opt += " -noepics";
    }
    else if( _epics == "true" && _sim ) {
      opt += " -epics flam"; // or alternate sim. epics db name
    }
    else {
      opt += " -epics ";
      opt += _epics;
    }
    opt += UFExecutiveConfig::bootAgents(opt, *_boot);
    //if( _verbose )
      clog<<"UFFLAMExecutiveAgent::init> boot/startagents: "<<opt<<endl;
  }

  return _config->_devIO;
} // init
  
void UFFLAMExecutiveAgent::shutdown(const string option) {
  if( option.find("full") != string::npos || 
      option.find("Full") != string::npos ||
      option.find("FULL") != string::npos ) {
    // full system shutdown
    string all = "All";
    clog<<"UFFLAMExecutiveAgent::shutdown> stopping all agents..."<<endl;
    string reply = UFExecutiveConfig::stopAgents(all);
    clog<<"UFFLAMExecutiveAgent::shutdown> power off all baytech channels..."<<endl;
    reply = powerOff(all);
  }

  // disconnect from baytech
  if( _baytech )
    _baytech->close();

  delete _baytech;

  clog<<"UFFLAMExecutiveAgent::shutdown> closing all client connections and terminating..."<<endl;
  // disconnect from all clients and exit
  UFRndRobinServ::shutdown();
}

void* UFFLAMExecutiveAgent::ancillary(void* p) {
  static time_t _prev_clck = 0;
  bool block = false;
  if( p )
    block = *((bool*)p);

  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe != 0 && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
  }
    
  // update the status values...
  //if( _capipe != 0 && _statRec != "" )
  //  updateEpics(_statRec, v);

  if( clck - _prev_clck < _pulsePeriod ) // heartbeat period?
    return p;

  _prev_clck = clck;

  // once a minute or so, check the baytech connection
  // (baytech will close it's connection if client is inactive
  // for too long...
  // there may be other issues as well, the baytech may have 
  // it's own firmware bug which will cause us to lose the connection
  // over time, and/or it may drop out of the outlet control menu
  // and the outlet commands will then be rejected, etc.
  // so it may be best to not maintain the connection, rather
  // re-establish it upon an action request, and close.
  // this is slow, but presumably such actions are infrequent.

  // heartbeat the baytech
  string btstat= UFBaytech::statusSim();
  // always reconnect and close (keeping connection alive causes prblems with baytech?)
  if( !_sim && _config->_tshost != "false" ) {
    // use existing connection from init? 
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFFLAMExecutiveAgent::ancillary> baytech (re)connect failed..."<<endl;
    }
    else {
      // check connectivity to baytech:
      btstat = _baytech->status();
      // close connection 
      _baytech->close(); 
      //clog<<"UFFLAMExecutive::ancillary> Pinged Baytech ("<<currentTime()<<")"<<endl;
    }
  }

  if( _verbose && _baytech) {
    clog<<"UFFLAMExecutive::ancillary> Baytech status ("<<currentTime()<<"):"<<endl;
    clog<<btstat<<endl;
  }

  return p;
}

// new action signature:
int UFFLAMExecutiveAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  delete repv; repv = 0;
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFFLAMExecutiveAgent::action(UFDeviceAgent::CmdInfo* act) {
  int stat= 0;
  bool reply_expected= true;
  if( act->clientinfo.find("flam2:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_expected = false;
    clog<<"UFFLAMExecutiveAgent::action> No replies will be sent to GeminiEpics client: "
        <<act->clientinfo<<endl;
  }
  /*
  if( _verbose ) {
    clog<<"UFFLAMExecutiveAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFFLAMExecutiveAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
  */

  string car, errmsg, agent= "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];

    if( _verbose ) {
      clog<<"UFFLAMExecutiveAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFFLAMExecutiveAgent::action> cmdimpl: "<<cmdimpl<<endl;
    }

    if( cmdname.find("car") == 0 || cmdname.find("Car") == 0 || cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFFLAMExecutiveAgent::action> CAR: "<<car<<endl;
      continue;
    }

    act->time_submitted = currentTime();
    _active = act;
    string reply = "";

    if( cmdname.find("shut") == 0 || cmdname.find("Shut") == 0 || cmdname.find("SHUT") == 0 ) {
      // NO REPLY! SHOULD CAUSE A GRACEFULL PROCESS TERMINATION
      shutdown(cmdimpl);
    }
    if( cmdname.find("sim") == 0 || cmdname.find("Sim") == 0 || cmdname.find("SIM") == 0 ) {
      delete _boot; _boot = new string("ufsimagents");
      reply = "Simulation Request: ";
      reply += cmdimpl + " -- ";
      reply += UFExecutiveConfig::bootAgents(cmdimpl, *_boot);
      if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply);
      int last = (int)_DevHistory.size() - 1;
      act->cmd_reply.push_back(_DevHistory[last]);
    }
    else if( cmdname.find("start") == 0 || cmdname.find("Start") == 0 || cmdname.find("START") == 0 ) {
      delete _boot; _boot = new string("ufstyartagents");
      reply = "StartAgent Request: ";
      reply += cmdimpl + " -- ";
      reply += UFExecutiveConfig::bootAgents(cmdimpl, *_boot);
      if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply);
      int last = (int)_DevHistory.size() - 1;
      act->cmd_reply.push_back(_DevHistory[last]);
    }
    else if( _config->_tshost != "false" ) {
      if( cmdname.find("stop") == 0 || cmdname.find("Stop") == 0 || cmdname.find("STOP") == 0 ) {
        reply = "StopAgent Request: ";
        reply += cmdimpl + " -- ";
        reply += UFExecutiveConfig::stopAgents(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("poweron") == 0 || cmdname.find("PowerOn") == 0 || cmdname.find("POWERON") == 0 ) {
        reply = "PowerOn Request: ";
        reply += cmdimpl + " -- ";
        reply += powerOn(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("poweroff") == 0 || cmdname.find("PowerOff") == 0 || cmdname.find("POWEROFF") == 0 ) {
        reply = "PowerOff Request: ";
        reply += cmdimpl + " -- ";
        reply += powerOff(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("powerstat") == 0 || cmdname.find("PowerStat") == 0 || cmdname.find("POWERSTAT") == 0 ) {
        reply = "PowerStat Request: ";
        reply += cmdimpl + " -- ";
        reply += powerStat(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
    } // if baytech is available
    else { // unsupported request
      clog<<"UFFLAMExecutiveAgent::action> unsupported rquest: "<<cmdname<<", "<<cmdimpl<<endl;
      act->cmd_reply.push_back(cmdname + ", " + cmdimpl + " is not supported");
      break;
    }
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( _capipe != 0 &&  car != "" ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( _capipe != 0 && car != "" ) sendEpics(car, "OK");
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected )
    return new UFStrings(act->clientinfo, act->cmd_reply);
  else
    return (UFStrings*) 0;
} // action

int UFFLAMExecutiveAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFFLAMExecutiveAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update);
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

string UFFLAMExecutiveAgent::status() {
  // for now just baytech status;
  // but this could also status all the agents...
  if( _sim || _baytech == 0 )
    return UFBaytech::statusSim();

  string btstat;
  if( _baytech->reconnect() < 0 ) {
    btstat = "No connection to Baytech!";
    clog<<"UFFLAMExecutiveAgent::status> baytech reconnect failed..."<<endl;
    return btstat;
  }
  btstat = _baytech->status();
  _baytech->close();

  return btstat;
}

string UFFLAMExecutiveAgent::powerOn(const string& pwr) {
  string btstat, nil = "";
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFFLAMExecutiveAgent::powerOn> baytech reconnect failed..."<<endl;
      return btstat;
    }
  }
  
  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerOnSim();
    else
      return _baytech->powerOn();
  }
  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFFLAMExecutiveAgent::powerOn> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFFLAMExecutiveAgent::powerOn> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFFLAMExecutiveAgent::powerOn> Bad implementation: "<<pwr<<endl;
    return nil;
  }
  if( _sim )
    return _baytech->powerOnSim(chan);
  else {
    btstat = _baytech->powerOn(chan);
    _baytech->close();
  }
  return btstat;
}

string UFFLAMExecutiveAgent::powerOff(const string& pwr) {
  string btstat, nil = "";
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFFLAMExecutiveAgent::powerOff> baytech reconnect failed..."<<endl;
      return btstat;
    }
  }
  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerOffSim();
    else
      return _baytech->powerOff();
  }

  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFFLAMExecutiveAgent::powerOff> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFFLAMExecutiveAgent::powerOff> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFFLAMExecutiveAgent::powerOff> Bad implementation: "<<pwr<<endl;
    return nil;
  }

  if( _sim )
    return _baytech->powerOffSim(chan);
  else {
    btstat = _baytech->powerOff(chan);
    _baytech->close();
  }
  return btstat;
}

string UFFLAMExecutiveAgent::powerStat(const string& pwr) {
  string btstat, nil = "";
  bool pwrOnOff = true;
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFFLAMExecutiveAgent::powerStat> baytech reconnect failed..."<<endl;
    }
  }

  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerStatSim();
    else {
      pwrOnOff = _baytech->powerStat(-1, btstat);
      _baytech->close();
      return btstat;
    }
  }

  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFFLAMExecutiveAgent::powerStat> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFFLAMExecutiveAgent::powerStat> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFFLAMExecutiveAgent::powerStat> Bad implementation: "<<pwr<<endl;
    return nil;
  }

  if( _sim )
    return _baytech->powerStatSim(chan);
  else {
    pwrOnOff = _baytech->powerStat(chan, btstat);
    _baytech->close();
  }
  return btstat;
}

#endif // UFFLAMExecutiveAgent
