#if !defined(__UFFLAMExecutiveAgent_h__)
#define __UFFLAMExecutiveAgent_h__ "$Name:  $ $Id: UFFLAMExecutiveAgent.h,v 0.3 2006/04/06 17:07:54 hon Exp $"
#define __UFFLAMExecutiveAgent_H__(arg) const char arg##UFFLAMExecutiveAgent_h__rcsId[] = __UFFLAMExecutiveAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFBaytech.h"

class UFFLAMExecutiveAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char **envp);
  UFFLAMExecutiveAgent(int argc, char** argv, char **envp);
  UFFLAMExecutiveAgent(const string& name, int argc, char** argv, char **envp);
  inline virtual ~UFFLAMExecutiveAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();
  virtual void setDefaults(const string& instrum= "flam", bool initsad= false);

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in adition to establishing the baytech connection,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  virtual int action(UFDeviceAgent::CmdInfo* a, vector< UFProtocol* >*& replies);

  virtual void* ancillary(void* p);
  virtual void hibernate();
  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  string status();

  // full system shutdown (stop all agents, powerOff all, exit) or just self shutdown
  void shutdown(const string option);

  string powerOn(const string& pwr);
  string powerOff(const string& pwr);
  string powerStat(const string& pwr);

protected:
  // note that bool _shutdown is inherited from UFRndRobinServ
  static bool _powerOnAll, _powerOffAll;
  static string *_boot; // indicate optional nondefault boot script
  static int _pulsePeriod;
  static UFBaytech* _baytech;
};

#endif // __UFFLAMExecutiveAgent_h__
      
