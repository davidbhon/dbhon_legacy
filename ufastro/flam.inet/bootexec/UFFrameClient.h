#if !defined(__UFFrameClient_h__)
#define __UFFrameClient_h__ "$Name:  $ $Id: UFFrameClient.h,v 0.3 2005/07/26 17:53:56 hon Exp $";
#define __UFFrameClient_H__(arg) const char arg##UFFrameClient_h__rcsId[] = __UFFrameClient_h__;

// c++
using namespace std;
#include "string"
#include "vector"

// uflib
#include "UFRuntime.h"
#include "UFClientSocket.h"
#include "UFObsConfig.h"
#include "UFInts.h"
#include "UFStrings.h"

class UFFrameClient : public UFClientSocket {
public:
  inline UFFrameClient(const string& host, int port= 52001,
		       int w= 2048, int h= 2048) : _frmhost(host), _frmport(port), _w(w), _h(h) {
    if( _frmhost == "" ) _frmhost = UFRuntime::hostname();
  }
  inline virtual ~UFFrameClient() {}

  // start observation (connect to ufacqframed if necessary) with ufacqframed
  int UFFrameClient::startObs(UFObsConfig& obscfg, const string& host="", int port= 52001);
  // connect to ufacqframed and start observation
  int UFFrameClient::replConnectAndStartObs(UFObsConfig& obscfg, const string& host, int port);

  // connect (to ufedtd) as replication client, return 0 on connection failure:
  int replConnect(const string& host, int port);

  // recv replicated frame:
  // the int* memory must be freed via delete of the returned UFInts* 
  UFProtocol* replFrame(int& frmcnt, int& frmtotal);

  // request frame from named buffer:
  UFInts* fetchFrame(const string& name, int& frmcnt, int& frmtotal);

  // get the quick look stream names that need to be processed
  // also get frmcnt info. to check for observation completion
  int getUpdatedQlNames(const string& instrum, vector< string >& bufnames,
			int& frmcnt, int& frmtotal);  

  // get the permanent store (local) file name that needs to be processed
  // also get writecnt info. to check for observation completion
  // return true if file write has completed
  bool getFileStoreName(string& readyfile, string& filename, string& prevfile,
			int& frmcnt, int& frmtotal);  

  // return file decr. &  FITS header from opened file, seeking to start of first data frame
  virtual int openFITS(const string& filename, UFStrings*& fitsHdr,
		       int& width, int& height, int& frmtotal);

  // return FITS data from open file, seeking to next data frame
  virtual UFInts* seekFITSData(const int fd, const int width= 320, const int height= 240);

  // integer key value
  static int fitsInt(const string& key, char* fitsHdr);

  static bool _verbose;
  static bool _observatory;
  static UFStrings* _bufnames;
  static float _timeout;

protected:
  string _frmhost;
  int _frmport, _w, _h;
  static UFClientSocket _frmcmdsoc; // separate connection to ufacqframed is required for commands

  UFInts* _simFrame(const string& name, int index= 0);
};

#endif // __UFFrameClient_h__
