#if !defined(__UFExecutiveConfig_h__)
#define __UFExecutiveConfig_h__ "$Name:  $ $Id: UFExecutiveConfig.h,v 0.3 2006/04/06 17:07:53 hon Exp $"
#define __UFExecutiveConfig_H__(arg) const char arg##UFExecutiveConfig_h__rcsId[] = __UFExecutiveConfig_h__;

#include "UFDeviceConfig.h"
#include "UFBaytech.h"
#include "UFSADFITS.h"

class UFDeviceAgent;

class UFExecutiveConfig : public UFDeviceConfig {
public:
  UFExecutiveConfig(const string& name= "UnknownExecutive@DefaultConfig");
  UFExecutiveConfig(const string& name, const string& host, int port= 23);
  inline virtual ~UFExecutiveConfig() {}

  // override these virtuals:
  virtual UFTermServ* UFExecutiveConfig::connect(const string& host, int port= 23);
  virtual vector< string >& UFExecutiveConfig::queryCmds();
  virtual vector< string >& UFExecutiveConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& name);
  virtual int validCmd(const string& name, const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  // ufstartagents is default boot script
  // alternatives are ufsimagents, ufsimfu, ufsimf2, and ufsimfoo
  static string bootAgents(const string& agntargs= "", const string& scriptname= "ufstartagents");
  static string stopAgents(const string& agnt= "");
  static int chanOf(const string& name);
};

#endif // __UFExecutiveConfig_h__
