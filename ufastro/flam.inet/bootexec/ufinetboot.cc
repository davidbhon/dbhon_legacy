#if !defined(__ufinetboot_cc__)
#define __ufinetboot_cc__ "$Name:  $ $Id: ufinetboot.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufinetboot_cc__;

#include "unistd.h"
#include "errno.h"
#include "netdb.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "sys/stat.h"
#include "sys/fcntl.h"
#include "ctype.h"
#include "cstdio"
#include "cstring"
#include "string"

#if defined(LINUX)
#include "sys/ioctl.h" // ioctl
#include "asm/ioctls.h" // FIONREAD
#include "sys/sysinfo.h"
#define __restrict
#include "time.h"
#undef __restrict
#else // SOLARIS
#include "inttypes.h"
#include "sys/filio.h" // FIONREAD
#include "sys/systeminfo.h"
#include "sys/time.h"
#include "sys/uio.h"
#endif

using namespace std;

const bool _verbose = true;
FILE* _log= 0;

string currentTime(const string& zone) {
  struct tm *t, tbuf;
  struct timespec tspec;
#if defined(SOLARIS)
  ::clock_gettime(CLOCK_REALTIME, &tspec); // posix func.
  const time_t sec = tspec.tv_sec;
#else
 time_t sec = ::time(0); // Linux still lacks clock_gettime ?
 tspec.tv_nsec = 0;
#endif
 if( zone.find("utc") == std::string::npos &&
      zone.find("UTC") == string::npos &&
      zone.find("gmt") == string::npos &&
      zone.find("GMT") == string::npos ) { // use localtime
    t = ::localtime_r(&sec, &tbuf); // mt-safe
  }
  else {
    t = ::gmtime_r(&sec, &tbuf); // mt-safe
  }

  char s[] = "yyyy:ddd:hh:mm:ss.uuuuuu";
  if( t == 0 )
    return s;

  sprintf(s, "%04d:%03d:%02d:%02d:%02d.%06d",
          1900+t->tm_year, t->tm_yday, t->tm_hour, t->tm_min, t->tm_sec,
          (int) (tspec.tv_nsec/1000));
  string retval(s);
  return retval;
}

string peerIP(int fd) {
  string pn;
  struct sockaddr sa;
  socklen_t sl = sizeof(sa);
  int peer = ::getpeername(fd, &sa, &sl);
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  peer = ::getnameinfo(&sa, sl, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf),
                       NI_NUMERICHOST | NI_NUMERICSERV);
  if( peer != 0 ) {
    ::fprintf(_log,"peerIP> unable to get peername, error: %s\n", strerror(errno));
    return pn;
  }
  pn = hbuf;
  return pn;
}

bool isSock(int fd) {
  struct stat st;
  ::fstat(fd, &st);
  return S_ISSOCK(st.st_mode);
}

int available(int fd) {
  int retval= 0;
  int stat = ::ioctl(fd, FIONREAD, &retval);
  if( stat != 0 ) {
    ::fprintf(_log, "available> %s\n", strerror(errno));
    ::fflush(_log);
    retval = -1;
  }
  return retval;
}

int recvAuth(string& s, int fd) {
  /*
  int na = available(fd);
  ::fprintf(_log, "ufinetboot::recv> available: %d\n", na);
  ::fflush(_log);
  if( na < 0 )
    return na;

  int cnt = 10; // sec.
  // expect slen (int)
  while( na < (int)sizeof(int) && --cnt > 0 ) {
    ::sleep(1); // try again
    na = available(fd);
    ::fprintf(_log, "ufinetboot::recvAuth> available: %d\n", na);
    ::fflush(_log);
  }
  if( na < (int)sizeof(int) ) // give up
    return na;
  */
  int slen = 0;
  int nr = ::recv(fd, &slen, sizeof(int), MSG_WAITALL);
  slen = ntohl(slen);
  ::fprintf(_log, "ufinetboot::recvAuth> nr: %d, slen: %d, errmsg: %s\n", nr, slen, strerror(errno));
  ::fflush(_log);
  const size_t blen = 1+slen;
  char buf[blen]; memset(buf, 0, sizeof(buf));
  nr = ::recv(fd, buf, slen, MSG_WAITALL);
  ::fprintf(_log, "ufinetboot::recvAuth> nr: %d, slen: %d, errmsg: %s, text: %s\n", nr, slen, strerror(errno), buf);
  ::fflush(_log);

  s = buf;
  return s.length();
}

bool authenticate(int fd, bool& shutdown, bool& boot, bool& sim, string& script) {
  if( !isSock(fd) ) {
    ::fprintf(_log, "ufinetboot::authenticate> %d not a socket?\n", fd);
    ::fclose(_log);
    return false;
  }
  else {
    string clhost = peerIP(fd);
    ::fprintf(_log, "ufinetboot::authenticate> %d peerIP: %s\n", fd, clhost.c_str());
    ::fflush(_log);
  }
  string s;
  int nr = recvAuth(s, fd);
  if( nr <(int) sizeof(int) || s.length() < strlen("boot") ) {
    ::fprintf(_log, "ufinetboot::authenticate> recv. failed.\n");
    ::fflush(_log);
    return false;
  }
  ::fprintf(_log, "ufinetboot::authenticate> %s\n", s.c_str());
  ::fflush(_log);

  if( s.find("uf") == string::npos && s.find("UF") == string::npos )
    return false;

  bool trecs = (s.find("trec") == string::npos && s.find("Trec") == string::npos && s.find("TREC") == string::npos);
  bool flam = (s.find("flam") == string::npos && s.find("Flam") == string::npos && s.find("FLAM") == string::npos);
  bool foo = (s.find("foo") == string::npos && s.find("Foo") == string::npos && s.find("FOO") == string::npos);
  bool fu = (s.find("fu") == string::npos && s.find("Fu") == string::npos && s.find("FU") == string::npos);
  bool f2 = (s.find("f2") == string::npos && s.find("F2") == string::npos );
  if( !trecs && !flam && !foo && !fu && !f2 )
    return false;

  if( s.find("reboot") != string::npos || s.find("Reboot") != string::npos ||
      s.find("ReBoot") != string::npos || s.find("REBOOT") != string::npos ) {
    boot = true; shutdown = true;
    ::fprintf(_log, "ufinetboot::authenticate> ReBoot.\n", s.c_str());
    ::fflush(_log);
  }
  else if( s.find("boot") != string::npos || s.find("Boot") != string::npos ||
	   s.find("BOOT") != string::npos ) {
    boot = true;
    ::fprintf(_log, "ufinetboot::authenticate> Boot.\n", s.c_str());
    ::fflush(_log);
  }

  if( s.find("sim") != string::npos || s.find("Sim") != string::npos || s.find("SIM") != string::npos ) {
    sim = true;
    ::fprintf(_log, "ufinetboot::authenticate> Simulation mode.\n", s.c_str());
    ::fflush(_log);
  }


  if( s.find("shutdown") != string::npos || s.find("ShutDown") != string::npos || s.find("SHUTDOWN") != string::npos ) {
    shutdown = true;
    ::fprintf(_log, "ufinetboot::authenticate> Shutdown only.\n", s.c_str());
    ::fflush(_log);
  }

  return (boot || shutdown);
}

int main(int argc, char** argv, char** envp) {
  //char buf[BUFSIZ]; ::memset(buf, 0 , BUFSIZ);
  //string path = ::getcwd(buf, BUFSIZ); path += "/";
  string path = argv[0];
  size_t tail = path.rfind("/");
  if( tail != string::npos )
    ++tail;
  else
    tail = 1;

  // works for solaris inetd but not linux xinetd 
  // /usr/local/uf/sbin/{ufinetboot,ufstartup}
  path = path.substr(0, tail);
  if( path.find("/") != 0 ) { // no leading slash (probably linux) try env:
    char* ufinst = getenv("UFINSTALL");
    if( ufinst != 0 ) {
      path = ufinst;
      path += "/sbin/"; // ensure trailing slash
    }
  }

  // presumably this is exec'd by the (x)inetd listening on port 3720
  // (the official ufastro ir services port is now in the iana db)
  // (x)inetd is expected to associate all 3 file desc. 0, 1, and 2 with
  // the new client connection (soc):
  _log = ::fopen("/usr/tmp/ufinetboot.log", "a+");
  ::fprintf(_log, "%s\n", argv[0]);
  string header = path + "ufinetboot>::::::::: started by (x)inetd @ ";
  string t = currentTime("localtime");
  header += t; header += " :::::::::";
  ::fprintf(_log, "%s\n", header.c_str());
  ::fflush(_log);

  int soc = 0; // presumably (x)inet has duped 0, 1, and 2 to the accepted client socket`
  bool shutdown= false, boot= false, sim= false;
  string script;
  if( !authenticate(soc, shutdown, boot, sim, script) ) {
    //if( _verbose ) {
      ::fprintf(_log, "ufinetboot> failed client authentication ...\n");
      ::fclose(_log);
    //}
    string fail = "ufinetboot> failed client authentication";
    int slen = htonl(fail.length());
    write(soc, &slen, sizeof(slen));
    write(soc, fail.c_str(), fail.length());
    ::close(0); ::close(1); ::close(2);
    return 2;
  }
  string ack = "ufinetboot> client authenticated";
  int slen = htonl(ack.length());
  write(soc, &slen, sizeof(slen));
  write(soc, ack.c_str(), ack.length());
    
  pid_t childpid = ::fork();
  if( (int) childpid < 0 ) { // failed to create child
    ::fprintf(_log,"ufinetboot::greetClient> failed to fork... \n");
    ::fclose(_log);
    return 3;
  }

  if( childpid > 0 ) { // parent process can exit now
    ::fprintf(_log,"ufinetboot::greetClient> succeeded to fork... \n");
    ::fclose(_log);
    return 0;
  }

  // proceed with ufboot or ufreboot, this should not return unless it fails
  // script shuts down any existing uf executive and (re)starts it
  // the script ensures a proper runtime environment for the executive:
  int exstat= 0;
  char** args = argv;
  if( shutdown && boot ) {
    path += "ufreboot";
    if( sim && !script.empty() ) {
      args = new char*[argc+3]; memset(args, 0, sizeof(args));
      args[0] = argv[0]; args[1] = "-sim"; args[2] = (char*)script.c_str();
      for(int i = 3; i <= argc; ++i )
        args[i] = argv[i-1];
    }
    else if( sim ) {
      args = new char*[argc+2]; memset(args, 0, sizeof(args));
      args[0] = argv[0]; args[1] = "-sim";
      for(int i = 2; i <= argc; ++i )
        args[i] = argv[i-1];
    }
    if( args[1] != 0 && args[2] != 0 )
      ::fprintf(_log,"ufinetboot> execv of %s %s %s\n", path.c_str(), args[0], args[1], args[2]);
    else if( args[1] != 0 )
      ::fprintf(_log,"ufinetboot> execv of %s %s %s\n", path.c_str(), args[0], args[1]);
    else
      ::fprintf(_log,"ufinetboot> execv of %s %s\n", path.c_str(), args[0]);

    ::fflush(_log);
    exstat = ::execve(path.c_str(), args, envp);
  }
  else if( boot ) {
    path += "ufboot";
    if( sim && !script.empty() ) {
      args = new char*[argc+3]; memset(args, 0, sizeof(args));
      args[0] = argv[0]; args[1] = "-sim"; args[2] = (char*) script.c_str();
      for(int i = 3; i <= argc; ++i )
        args[i] = argv[i-1];
    }
    else if( sim ) {
      args = new char*[argc+2]; memset(args, 0, sizeof(args));
      args[0] = argv[0]; args[1] = "-sim";
      for(int i = 2; i <= argc; ++i )
        args[i] = argv[i-1];
    }
    else if( !script.empty() ) {
      args = new char*[argc+2]; memset(args, 0, sizeof(args));
      args[0] = argv[0]; args[1] = (char*) script.c_str();
      for(int i = 2; i <= argc; ++i )
        args[i] = argv[i-1];
    }
    if( args[1] != 0 && args[2] != 0 )
      ::fprintf(_log,"ufinetboot> execv of %s %s %s\n", path.c_str(), args[0], args[1], args[2]);
    else    if( args[1] != 0 )
      ::fprintf(_log,"ufinetboot> execv of %s %s %s\n", path.c_str(), args[0], args[1]);
    else
      ::fprintf(_log,"ufinetboot> execv of %s %s\n", path.c_str(), args[0]);

    ::fflush(_log);
    exstat = ::execve(path.c_str(), args, envp);
  }
  else if( shutdown ) {
    path += "ufshutdown";
    ::fprintf(_log,"ufinetboot> execve of %s\n", path.c_str());
    ::fflush(_log);
    exstat = ::execve(path.c_str(), args, envp);
  }
  else {
    ::fprintf(_log,"ufinetboot> neither shutdown nor boot requested...\n");
    ::fflush(_log);
  }
  ::fclose(_log);
  return exstat;
}

#endif // __ufinetboot_cc__
