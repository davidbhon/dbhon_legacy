#if !defined(__UFSBConfig_cc__)
#define __UFSBConfig_cc__ "$Name:  $ $Id: UFSBConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSBConfig_cc__;

#include "UFSBConfig.h"
#include "UFGemSymbolAgent.h"
#include "UFFITSheader.h"
#include "ufflam2mech.h"

vector< string > UFSBConfig::_circMOSBarCode; // 2
vector< string > UFSBConfig::_plateMOSBarCode; // 12 ?

UFSBConfig::UFSBConfig(const string& name) : UFDeviceConfig(name) {
  _circMOSBarCode.push_back("00001234");
  _circMOSBarCode.push_back("00009876");
  _plateMOSBarCode.push_back("11119875");
  _plateMOSBarCode.push_back("22229875");
  _plateMOSBarCode.push_back("33339875");
  _plateMOSBarCode.push_back("44449875");
  _plateMOSBarCode.push_back("55559875");
  _plateMOSBarCode.push_back("66669875");
  _plateMOSBarCode.push_back("77779875");
  _plateMOSBarCode.push_back("88889875");
  _plateMOSBarCode.push_back("99998754");
  _plateMOSBarCode.push_back("10100123");
  _plateMOSBarCode.push_back("11110123");
  _plateMOSBarCode.push_back("12120123");
}

UFSBConfig::UFSBConfig(const string& name, const string& tshost, int tsport) : UFDeviceConfig(name, tshost, tsport) {
  _circMOSBarCode.push_back("00001234");
  _circMOSBarCode.push_back("00009876");
  _plateMOSBarCode.push_back("11119875");
  _plateMOSBarCode.push_back("22229875");
  _plateMOSBarCode.push_back("33339875");
  _plateMOSBarCode.push_back("44449875");
  _plateMOSBarCode.push_back("55559875");
  _plateMOSBarCode.push_back("66669875");
  _plateMOSBarCode.push_back("77779875");
  _plateMOSBarCode.push_back("88889875");
  _plateMOSBarCode.push_back("99998754");
  _plateMOSBarCode.push_back("10100123");
  _plateMOSBarCode.push_back("11110123");
  _plateMOSBarCode.push_back("12120123");
}

string UFSBConfig::terminator() { return "\r\n"; }

// connect calls create which then connects
UFTermServ* UFSBConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }
  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);
  return _devIO;
}

vector< string >& UFSBConfig::queryCmds() {
  if( _queries.size() > 0 ) 
    return _queries;

  // fetch parameter setting
  _queries.push_back("PARAM");
  // fetch revision id
  _queries.push_back("REVISION");

  return _queries;
}

vector< string >& UFSBConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("Abort"); _actions.push_back("ABORT"); _actions.push_back("abort");
  _actions.push_back("Beep"); _actions.push_back("BEEP"); _actions.push_back("beep");
  // post current barcode reading to designated epics sad channel:
  // e.g.: cmd==post, cmdimpl==instrum:sad:MOSPos1BarCode.inp
  _actions.push_back("Post"); _actions.push_back("POST"); _actions.push_back("post");
  _actions.push_back("Disable"); _actions.push_back("DISABLE"); _actions.push_back("disable");
  _actions.push_back("Enable"); _actions.push_back("ENABLE"); _actions.push_back("enable");
  // pull trigger
  _actions.push_back("Scan"); _actions.push_back("SCAN"); _actions.push_back("scan");

  return _actions;
}

int UFSBConfig::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      return 1;
    }
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }

  clog<<"UFSBConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFSBConfig::status(UFDeviceAgent* da) {
  UFGemSymbolAgent* dasbc = dynamic_cast< UFGemSymbolAgent* > (da);
  if( dasbc->_verbose )
    clog<<"UFLSConfig::status> "<<dasbc->name()<<endl;
  UFFLAM2MechPos* mech;
  char* indexor = infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor == 0 )
    return 0;

  int pcnt = mech->posCnt;
  vector< string > vs;
  // assuming first 2 elements are the 2 circulars:
  for( int i = 0; i < pcnt; ++i ) {
    string barc = mech->positions[i];
    if( i < 2 ) {  
      barc += " == "; barc += _circMOSBarCode[i];
    }
    else {
      barc += " == "; barc += _plateMOSBarCode[i-2];
    }
    vs.push_back(barc);
  }

  return new UFStrings(dasbc->name(), vs);
}

UFStrings* UFSBConfig::statusFITS(UFDeviceAgent* da) {
  UFGemSymbolAgent* dasbc = dynamic_cast< UFGemSymbolAgent* > (da);
  if( dasbc->_verbose )
    clog<<"UFLSConfig::status> "<<dasbc->name()<<endl;

  UFFLAM2MechPos* mech;
  char* indexor = infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor == 0 )
    return 0;

  int pcnt = mech->posCnt;
  map< string, string > valhash, comments;
  // assuming first 2 elements are the 2 circulars:
  for( int i = 0; i < pcnt; ++i ) {
    string posname = mech->positions[i];
    comments[mech->positions[i]] = posname + " MOS WHEEL BARCODE";
    if( i < 2 )
      valhash[mech->positions[i]] = _circMOSBarCode[i];
    else
      valhash[mech->positions[i]] = _plateMOSBarCode[i-2];
  }

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(dasbc->name());

  return ufs;
}

int UFSBConfig::setBarc(const string& postsad, const string& current) {
  if( postsad.find("CIRC") != string::npos ||
      postsad.find("Circ") != string::npos ||
      postsad.find("circ") != string::npos ) {
    if( postsad.find("2") != string::npos )
      _circMOSBarCode[1] = current;
    else 
      _circMOSBarCode[0] = current;
  }
  else if( postsad.find("2") != string::npos )
    _circMOSBarCode[1] = current;
  else if( postsad.find("1")!= string::npos  )
    _plateMOSBarCode[0] = current;
  else if( postsad.find("2") != string::npos )
    _plateMOSBarCode[1] = current;
  else if( postsad.find("3") != string::npos )
    _plateMOSBarCode[2] = current;
  else if( postsad.find("4") != string::npos )
    _plateMOSBarCode[3] = current;
  else if( postsad.find("5") != string::npos )
    _plateMOSBarCode[4] = current;
  else if( postsad.find("6") != string::npos )
    _plateMOSBarCode[5] = current;
  else if( postsad.find("7") != string::npos  )
    _plateMOSBarCode[6] = current;
  else if( postsad.find("8") != string::npos )
    _plateMOSBarCode[7] = current;
  else if( postsad.find("9") != string::npos )
    _plateMOSBarCode[8] = current;
  else if( postsad.find("10") != string::npos )
    _plateMOSBarCode[9] = current;
  else
    return 0;

  return 1;
}

// Symbol Barcode command packet funcs:
// dynamically allocate and init a packet, must be freed/deleted!
int UFSBConfig::hostPacket(unsigned char opcode, unsigned char* p, const string& cmddata) {
  unsigned short* csum= 0;
  size_t sz= 6;
  char len = sz-2;
  if( cmddata.empty() ) {
    p[0] = len;
    csum = (unsigned short* ) &p[sz-2];
  }
  else {
    sz = 6 + cmddata.length();
    len = sz-2;
    p[0] = len;
    csum = (unsigned short* ) &p[sz-2];
    memcpy(p+4, cmddata.c_str(), cmddata.length());
  }
  p[1] = opcode;
  p[2] = 0x04; // message source is host
  p[3] = 0;
  unsigned short sum= 0;
  for( size_t i = 0; i < sz-2; ++i ) {
    unsigned short s = p[i];
    sum += s;
  }
  sum = htons(sum);
  memcpy(csum, &sum, sizeof(short));

  return (int)sz;
}

int UFSBConfig::sendPacket(unsigned char opcode, const string& cmddata) {
  unsigned char p[257]; memset(p, 0, sizeof(p)); // max. packet length
  int sz = hostPacket(opcode, p, cmddata);
  // assume usage of Perle:
  int ns= -1;
  if( _devIO != 0 ) {
    UFClientSocket* soc = static_cast< UFClientSocket* > ( _devIO );
    ns = soc->send(p, sz);
  }
  return ns;
}

string UFSBConfig::recvPacket(float timeOut, int tryCnt) {
  // testing of the barcode scanner in its default mode
  // seems to indicate the triggered scan is sending out
  // a simple 9 or 11 character string (depending on the 
  // type of barcode being scanned; with the first character
  // indicating the something about type...
  // (not always F for 8? and B for 10?):
  string s;
  if( _devIO == 0 )
    return s;

  UFClientSocket* soc = static_cast< UFClientSocket* > ( _devIO );
  unsigned char p[257]; memset(p, 0, sizeof(p)); // max. packet length
  int ne= 9, na= soc->available(timeOut, tryCnt);
  if( na <= 0 )
    return s; // bad socket?

  if( na < ne ) // try again?
    na= soc->available(timeOut, tryCnt);

  // somehting became available within the timeout...
  int nr = soc->recv(p, na);
  if( nr < na ) 
    clog<<"UFSBConfig::recvPacket> na: "<<na<<", nr: "<<nr<<endl;

  s = (char*)p;
  return s;
}

#endif // __UFSBConfig_cc__
