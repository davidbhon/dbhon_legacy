#if !defined(__ufplctalk__)
#define __ufplctalk__ "$Name:  $ $Id: ufplctalk.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"
#include "UFDaemon.h"
#include "UFPLCConfig.h"
#include "UFTermServ.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

static bool _quiet = true;

class ufplctalk : public UFDaemon, public UFPLCConfig {
public:
  ~ufplctalk();
  ufplctalk(int argc, char** argv, int port= -1);

  // return 0 on connection failure:
  UFClientSocket* init(const string& host, int port);

  // submit req and wait for reply, return 
  string submit(string& data, const string& target= "0x01", float flush= -1.0);

  virtual string description() const { return __ufplctalk__; }

  UFClientSocket* _soc; 
};

ufplctalk::ufplctalk(int argc, char** argv, int port) : UFDaemon(argc, argv), UFPLCConfig(), _soc(0) {
  rename("ufplctalk@" + hostname());
}


// public func:
// return < 0 on connection failure:
UFClientSocket* ufplctalk::init(const string& host, int port) {
  UFTermServ* ts = connect(host, port);
  if( ts == 0 ) {
    return 0;
  }
  // use this for plc i/o
  _soc = static_cast< UFClientSocket* > (ts );

  return _soc;
}

// public
ufplctalk::~ufplctalk() {
  if( _soc ) {
    _soc->close();
  }
}


// submit data read req. & recv. reply
string ufplctalk::submit(string& data, const string& target, float flush) {
  // use funcs. inherited from UFPLCConfig:
  string r;
  float to= 10.0;
  // init a host (master) read request:
  hex hxtarg = strToHex(target); //(hex) atoi(target.c_str()); //0x21
  if( !_quiet ) clog<<"ufplctalk::submit> read init for target: "<<target<<endl;
  int ns = initReadReq(hxtarg);
  clog << "ufplctalk::submit> Number of bytes sent: "<<ns << endl;
  if( ns <= 0 ) {
    clog<<"ufplctalk::submit> init submission failed for read, target: "<<target<<endl;
    return r;
  }

  // if ack == true; if naq == false (binary hex only)
  bool ak = recvInitAck(to);
  if( !ak ) {
    clog<<"ufplctalk::submit> failed read init for target: "<<target<<endl;
    return r;
  }
  if( !_quiet ) clog<<"ufplctalk::submit> ACK return by read init for target: "<<target<<endl;
  // hex binary:
  hex DataHexAddr[2]= {0x00, 0x01};
  if( !data.empty() ) {
    UFRuntime::rmJunk(data);
    // assume data is formated: "0xMMLL"
    size_t xpos = data.find("0x");
    xpos += 2;
    string msb = "0x";
    msb += data.substr(xpos, 2);
    DataHexAddr[0] = strToHex(msb);
    xpos += 2;
    string lsb = "0x";
    lsb += data.substr(xpos, 2);
    DataHexAddr[1] = strToHex(lsb);
  }
  int nb = sendReadReqHA(DataHexAddr, hxtarg);
  if( nb <= 0 ) {
    clog<<"ufplctalk::submit> req. submission failed for read for data address: "<<data<<endl;
    return r;
  }
  if( !_quiet ) clog<<"ufplctalk::submit> submitted req. read for nbytes: "<<nb<<", data address: "<<data<<endl;

  ak = recvAck(to);
  if( !ak ) {
    clog<<"ufplctalk::submit> failed read block req. for data address: "<<data<<endl;
    return r;
  }
  if( !_quiet ) clog<<"ufplctalk::submit> ACK returned for read block of data address: "<<data<<endl;

  hex block[256];
  int nr = recvBlock(block, nb, to);
  if( nr <= 0 ) {
    clog<<"ufplctalk::submit> recv failed for read for data address: "<<data<<endl;
    return r;
  }

  ns = sendEot();
  if( ns <= 0 ) {
    clog<<"ufplctalk::submit> EOT submission failed for read for data address: "<<data<<endl;
    return r;
  }

  r = (char*)&block[0];

  rmJunk(r);

  return r;
}

int main(int argc, char** argv, char** env) {
  ufplctalk plc(argc, argv);
  bool prompt = false;
  string arg, host = "192.168.111.125"; // UFRuntime::hostname();
  int port= 7012;
  float flush= -1.0;
  string target= "0x01", data=""; // "0x0001";

  arg = plc.findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = plc.findArg("-v");
  if( arg != "false" ) 
    UFDeviceConfig::_verbose = UFTermServ::_verbose = true;

  arg = plc.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = plc.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = plc.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  arg = plc.findArg("-target");
  if( arg != "false" && arg != "true" )
    target = arg;

  arg = plc.findArg("-data");
  if( arg != "false" && arg != "true" ) {
    data = arg;
    UFPLCConfig::_PLCBase =  UFPLCConfig::strToHex(data);
  }

  arg = plc.findArg("-bytes");
  if( arg != "false" && arg != "true" )
    UFPLCConfig::_numBytes = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufplctalk> usage: 'ufplctalk -host host -port port -target addres -data address"<<endl;
    return 0;
  }
 
  clog<<"ufplctalk> connect to PLC via Perle Console Server: "<<host<<" : "<<port<<endl;
  UFClientSocket* soc = plc.init(host, port);
  if( soc == 0 ) {
    clog<<"ufplctalk> unable to connect to PLC via Perle: "<<host<<" : "<<port<<endl;
    return 1;
  }

  string r;
  if( !data.empty() ) {
    // data read command/req should be executed once
    r = plc.submit(data, target, flush);
    if( r.empty() )
      clog << "ufplctalk> failed to submit read req for data address: "<<data<<", target: "<<target<<endl;
  }
  else {
    prompt = true;
  }

  if( !prompt && r.empty() ) {
    clog<<"ufplctalk> no data returned  for address: "<<data<<", target: "<<target<<endl;
    return -1;
  }
  else if( !prompt ) {
    cout<<r<<endl;
    return 0;
  }
  // enter command loop
  string line;
  while( true ) {
    cout<<"ufplctalk> enter hex address (\"0x0001\" - \"0xffff\") or quit> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      break;

    if( line.find("0x") == string::npos ) {
      clog<<"ufplctalk> data address should specified in hex (0x21)"<<endl;
      continue;
    }
    data = line;
    string reply = plc.submit(data, target, flush);
    if( reply.empty() ) {
      clog << "ufplctalk> failed to read req for data addr: "<<data<<endl;
      continue;
    }
    cout<<reply<<endl;
  }

  return 0;
} // ufplctalk::main

#endif // __ufplctalk__
