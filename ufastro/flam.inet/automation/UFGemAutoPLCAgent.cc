#if !defined(__UFGemAutoPLCAgent_cc__)
#define __UFGemAutoPLCAgent_cc__ "$Name:  $ $Id: UFGemAutoPLCAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemAutoPLCAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemAutoPLCAgent.h"
#include "UFPLCConfig.h"
#include "UFSADFITS.h"

// the byte packet returned from the PLC, interpreted as a string
string UFGemAutoPLCAgent::_current;

// submit data read req. & recv. reply
string UFGemAutoPLCAgent::getCurrent(int& inoutlines, const string& data) {
  UFPLCConfig* plc = static_cast< UFPLCConfig* >(_config);
  string target = "0x21";
  UFPLCConfig::_PLCBase = UFPLCConfig::strToHex(data);

  // use funcs. from UFPLCConfig:
  string rs;
  float to= 30.0;
  // init a host (master) read request:
  UFPLCConfig::hex hxtarg = plc->strToHex(target); //(hex) atoi(target.c_str()); //0x21
  if( _verbose ) clog<<"ufplctalk::submit> read init for target: "<<target<<endl;
  int ns = plc->initReadReq(hxtarg); 
  if( ns <= 0 ) {
    clog<<"ufplctalk::submit> init submission failed for read, target: "<<target<<endl;
    return rs;
  }

  // if ack == true; if naq == false (binary hex only)
  bool ak = plc->recvInitAck(to);
  if( !ak ) {
    clog<<"ufplctalk::submit> failed read init for target: "<<target<<endl;
    return rs;
  }
  if( _verbose ) clog<<"ufplctalk::submit> ACK return by read init for target: "<<target<<endl;
  // hex binary:

  UFPLCConfig::hex DataHexAddr[2]= {0x00, 0x01};
  // assume data is formated: "0xMMLL"
  size_t xpos = data.find("0x");
  xpos += 2;
  string msb = "0x";
  msb += data.substr(xpos, 2);
  DataHexAddr[0] = plc->strToHex(msb);
  xpos += 2;
  string lsb = "0x";
  lsb += data.substr(xpos, 2);
  DataHexAddr[1] = plc->strToHex(lsb);

  int nb = plc->sendReadReqHA(DataHexAddr, hxtarg);
  if( nb <= 0 ) {
    clog<<"ufplctalk::submit> req. submission failed for read for data address: "<<data<<endl;
    return rs;
  }
  if( _verbose ) clog<<"ufplctalk::submit> submitted req. read for nbytes: "<<nb<<", data address: "<<data<<endl;

  ak = plc->recvAck(to);
  if( !ak ) {
    clog<<"ufplctalk::submit> failed read block req. for data address: "<<data<<endl;
    return rs;
  }
  if( _verbose ) clog<<"ufplctalk::submit> ACK returned for read block of data address: "<<data<<endl;

  UFPLCConfig::hex block[256]; memset(block, 0, sizeof(block));
  int nr = plc->recvBlock(block, nb, to);
  if( nr <= 0 ) {
    clog<<"ufplctalk::submit> recv failed for read for data address: "<<data<<endl;
    return rs;
  }

  ns = plc->sendEot();
  if( ns <= 0 ) {
    clog<<"ufplctalk::submit> EOT submission failed for read for data address: "<<data<<endl;
    return rs;
  }
  rs = (char*)&block[0];
  rmJunk(rs);

  int inlines, outlines;
  //inoutlines = UFPLCConfig::parseBlock(inlines, outlines, block, nr);
  inoutlines = UFPLCConfig::parseBlock(inlines, outlines, rs);

  return rs;
}

// helper for ctors:
void UFGemAutoPLCAgent::setDefaults(const string& instrum, bool initsad) {
  _current = "";
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //_confGenSub = _epics + ":ec:configG";
    _confGenSub = "";
    _heart = _epics + ":ec:PLCheartbeat.VAL";
    // just one GIS -- gemini interlock sys. sad:
    if( UFSADFITS::_ecplcsad != 0 )
      _StatRecs.push_back(*UFSADFITS::_ecplcsad);
    else
      _StatRecs.push_back(_epics + ":sad:INTERLCK");
    /*
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");
    _StatRecs.push_back(_epics + ":sad:gis.inp01line");

    _StatRecs.push_back(_epics + ":sad:gis.out01line");
    _StatRecs.push_back(_epics + ":sad:gis.out02line");
    _StatRecs.push_back(_epics + ":sad:gis.out03line");
    _StatRecs.push_back(_epics + ":sad:gis.out04line");
   */
    _statRec = _StatRecs[0];
  }
  else {
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
}

// ctors:
UFGemAutoPLCAgent::UFGemAutoPLCAgent(int argc, char** argv, char** env) : UFGemDeviceAgent(argc, argv, env) {
  // use ancillary function to retrieve barcode readings
  _flush = 0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFGemAutoPLCAgent::UFGemAutoPLCAgent(const string& name,
					     int argc, char** argv, char** env) : UFGemDeviceAgent(name, argc, argv, env) {
  // use ancillary function to retrieve barcode readings
  _flush =  0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

// static funcs:
int UFGemAutoPLCAgent::main(int argc, char** argv, char** env) {
  UFGemAutoPLCAgent ufs("UFGemAutoPLCAgent", argc, argv, env); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemAutoPLCAgent::options(string& servlist) {
  // was this a compiler bug?
  UFDeviceAgent::_config = _config = new UFPLCConfig(); // automation PLC
  // g-p defaults for ufinstrum
  _config->_tsport = _config->_tsport = 7007;
  _config->_tshost = _config->_tshost = "flamperle"; // "192.168.111.125";

  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  string arg = findArg("-heart"); // override heartbeat channel name
  if( arg != "false" && arg != "true" )
    _heart = arg;
  /*
  arg = findArg("-conf"); // override config channel name
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;
  */
  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    UFTermServ::_verbose = _verbose;
    //UFSocket::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52007;
  }
  if( _verbose ) {
    clog<<"UFGemAutoPLCAgent::options> _epics: "<< _epics<<endl;
    clog<<"UFGemAutoPLCAgent::options> set port to GemAutoPLC port "<<port<<endl;
  }
  return port;
}

// override base class startup here to avoid connect to term. serv
// then do server listen stuff
void UFGemAutoPLCAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemAutoPLCAgent::startup> established signal handler."<<endl;

  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the perle should initialize a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemAutoPLCAgent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemAutoPLCAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemAutoPLCAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemAutoPLCAgent> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemAutoPLCAgent> Ecpics CA child proc. failed to start"<<endl;
  }
  if( !_sim ) {
    if( _verbose )
	clog << "UFGemAutoPLCAgent::init> connect to PerleConsole: "<<tshost<<" on port= " <<tsport<<endl;
    // connect to terminal server:
    ts = _config->connect(tshost, tsport);
    if( ts == 0 || _config->_devIO == 0 ) {
      clog<<"UFGemAutoPLCAgent> failed connection to annex/iocom."<<endl;
      exit(-1);
    }
  }
  // test connectivity to barcode (beep the bell):
  string plc; // cmd & reply strings 
  time_t clck = time(0);
  if( clck % 2 == 0 )
    _current = plc = "01234BogusAutomationPLCPacket5789";
  else
    _current = plc = "98765BogusAutomationPLCPacket43210";

  if( _sim ) {
    clog<<"UFGemAutoPLCAgent> simulating connection to Automation PLC."<<endl;
  }
  else {
    clog<<"UFGemAutoPLCAgent> checking availiblity of Automation PLC."<<endl;
    UFClientSocket* soc = static_cast< UFClientSocket* > (_config->_devIO);
    int na = soc->available();
    unsigned char con[2];
    if( na < 0 ) {
      clog<<"UFGemAutoPLCAgent> AutoPLC connect failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
    else if( na == 0 ) {
      soc->recv(con, 1); // block on 1 byte
    }
    na = soc->available();
    if( na > 0 ) {
      soc->recv(con, na); // empty out any 'greeting' from device
    }
    /*
    UFSBConfig* barcd = static_cast< UFSBConfig* > (_config);
    int nc = barcd->beep();
    if( nc <= 0 ) {
      clog<<"UFGemAutoPLCAgent> AutoPLC BarCode beep failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
    */
  }

  return ts;
}

void* UFGemAutoPLCAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  int bits= 0;
  time_t clck = time(0);
  // assume blocking i/o for now...
  // transactions with the plc may take more than 1 sec, so
  // let's not over stress it 
  if( !_sim && _config && _config->_devIO && clck % 3 == 0 ) {
    //UFPLCConfig* plc = static_cast< UFPLCConfig* > (_config);
    UFClientSocket* soc = static_cast< UFClientSocket* > (_config->_devIO);
    int na = soc->available();
    if( na > 0 ) {
      _current = getCurrent(bits); // recv any buffered scan
      if( _verbose )
	clog<<"UFGemAutoPLCAgent::ancillary> new plc reading length: "<<_current.length()
	    <<", reading: "<<_current<<endl;
    }
    else if( _verbose ) {
      clog<<"UFGemAutoPLCAgent::ancillary> no new barcode reading yet..."<<endl;
      //_current = ""; // clear current reading only after postsad prerformed...
    }
  }
  else if( _sim ) { 
    if( clck % 2 == 0 )
      _current ="01234BogusAutomationPLCPacket5789";
    else
     _current = "98765BogusAutomationPLCPacket43210";
  }

  // update the status value:
  if( !_current.empty() ) {
    if( _capipe && _epics != "false" && !_statRec.empty() ) {
      //clog<<"UFGemAutoPLCAgent::ancillary> sendEpics: "<<_statRec<<", current: "<<_current<<endl;
      sendEpics(_statRec, _current);
    }
  }
   
  if( !_current.empty() )
    _DevHistory.push_back(_current);
  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();

  if( _verbose )
    clog<<"UFGemAutoPLCAgent::ancillary> current reading: "<<_current<<endl;

  // update the heartbeat:
  if( _capipe && _epics != "false" && _heart != "" ) {
    strstream s;
    s<<clck<<ends;
    updateEpics(_heart, s.str());
    delete s.str();
  }
 
  return p;
} //ancillary

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// the Gemini/Epics version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error/alarm condition should be indicated
int UFGemAutoPLCAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  //int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;

  if( act->clientinfo.find(":") != string::npos ) { // must be epics client
    reply_expected = false;
    clog<<"UFGemAutoPLCAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemAutoPLCAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemAutoPLCAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car, sad, raw, errmsg, hist;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= false;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];
    //if( _verbose ) {
      clog<<"UFGemAutoPLCAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemAutoPLCAgent::action> cmdimpl: "<<cmdimpl<<endl;
    //}
    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int)replies.size();
      }
      else {
        replies.push_back(_config->status(this)); 
	return (int)replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	     cmdname.find("Car") == 0 ||
             cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemAutoPLCAgent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("sad") == 0 ||
	     cmdname.find("Sad") == 0 ||
             cmdname.find("SAD") == 0 ) {
      _statRec = sad = cmdimpl;
      //if( _verbose )
        clog<<"UFGemAutoPLCAgent::action> update SAD: "<<sad<<endl;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      raw = cmdimpl; // presumably a plc address read
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemAutoPLCAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") != 0 ||
	     cmdname.find("Sim") != 0 || 
	     cmdname.find("SIM") != 0 ) {
      sim = true;
      size_t errIdx = cmdname.find("!!");
      if( errIdx != string::npos && errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemAutoPLCAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemAutoPLCAgent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
        return (int)replies.size();
      }
      else {
	delete rep; rep = 0;
        return 0;      
      }
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    if( !_sim && _config != 0 && _config->_devIO ) {
      int nr = _config->validCmd(cmdname);
      if( nr > 0 ) { // valid  cmd
        if( _verbose ) clog<<"UFGemAutoPLCAgent::action>: "<<cmdname<<" : "<<cmdimpl<<endl;
      }
      else { // default to history
	hist = "1";
      }
    }
    if( hist != "" ) {
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(cmdimpl.c_str());
      if( cnt <= 0 ) cnt = 1;
      if( cnt > last+1 ) cnt = last+1;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    // simulate an error condition?
    if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      errmsg += cmdimpl;
      act->cmd_reply.push_back("Sim OK, sleeping 1/2 sec...");
      //if( _verbose ) 
	clog<<"UFGemAutoPLCAgent::action> simulating error condition..."<<endl;
      if( car != "" && _epics != "false" && _capipe ) setCARError(car, &errmsg);
    }
    else if( sim ) {
      time_t clck = time(0);
      if( clck % 2 == 0 )
        _current = "F11112345";
      else
        _current = "F99998765";
      act->cmd_reply.push_back(_current);
    }
  } // end for loop of cmd bundle

  if(  act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    //if( _verbose ) 
      clog<<"UFGemAutoPLCAgent::action> error condition?"<<endl;
    if( car != "" && _epics != "false" && _capipe )
      setCARError(car, &errmsg);
      //sendEpics(car, errmsg);
  }
  else {
    // success (CAR idle val = 0) to _capipe performed by acnillary
    act->status_cmd = "succeeded";
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size(); // rep gets deleted by UFRndRobin::exec
  }

  delete rep; rep = 0; // rep not used, delete it here
  return 0;
} 

int UFGemAutoPLCAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemAutoPLCAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update);
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

#endif // UFGemAutoPLCAgent
