#if !defined(__UFGemAutoPLCAgent_h__)
#define __UFGemAutoPLCAgent_h__ "$Name:  $ $Id: UFGemAutoPLCAgent.h,v 0.4 2005/06/07 15:48:37 hon Exp $"
#define __UFGemAutoPLCAgent_H__(arg) const char arg##UFGemAutoPLCAgent_h__rcsId[] = __UFGemAutoPLCAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFPLCConfig.h"

class UFGemAutoPLCAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemAutoPLCAgent(int argc, char** argv, char** envv);
  UFGemAutoPLCAgent(const string& name, int argc, char** argv, char** envv);
  inline virtual ~UFGemAutoPLCAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);
  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // in adition to establishing the iocomm/annex connection to
  // the motors, open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  // get the plc's current address state and return byte packet as string
  // set the low order 0-20 (up to 24) bits for inputs, and the high order 8 bits for outlines 
  string getCurrent(int& inoutlines, const string& data= "0x01"); // read data address default is 0x01

protected:
 // current plc reading, and to which sad it should be posted
  static string _current, _postsad, _postcar, _simtrigger;
};

#endif // __UFGemAutoPLCAgent_h__
      
