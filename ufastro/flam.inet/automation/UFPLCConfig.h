#if !defined(__UFPLCConfig_h__)
#define __UFPLCConfig_h__ "$Name:  $ $Id: UFPLCConfig.h,v 0.9 2004/10/08 12:51:38 hon Exp $"
#define __UFPLCConfig_H__(arg) const char arg##UFPLCConfig_h__rcsId[] = __UFPLCConfig_h__;

#include "UFDeviceConfig.h"
#include "vector"

#include "stdlib.h"

class UFPLCConfig : public UFDeviceConfig {
// automation direct plc serial line packet funcs:
public:
  typedef unsigned char hex;
  typedef unsigned short PLCAddress;

  UFPLCConfig(const string& name= "UnknownPLC@DefaultConfig");
  UFPLCConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFPLCConfig() {}

  // override these virtuals:
  virtual vector< string >& UFPLCConfig::queryCmds();
  virtual vector< string >& UFPLCConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // override these virtuals:
  // device i/o behavior
  //virtual string prefix();
  virtual UFTermServ* connect(const string& host= "", int port= 0);
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  inline static int setPLC(const string& postsad, const string& current) { return 0; } //stub

  inline static int strToHex(const string& s) {
    if( s.find("0x") != string::npos ) 
      return (int) strtol(s.c_str(), 0, 0);
    char* cs = (char*)s.c_str();
    while( !isdigit(*cs) && *cs != '-' ) ++cs;
    string hs = "0x"; hs += cs;
    return (int) strtol(hs.c_str(), 0, 0);
  }

  // convenience funcs:
  inline static hex soh() { return 0x01; }
  inline static hex stx() { return 0x02; }
  inline static hex etx() { return 0x03; }
  inline static hex eot() { return 0x04; }
  inline static hex enq() { return 0x05; }
  inline static hex ack() { return 0x06; }
  inline static hex nak() { return 0x15; }
  inline static hex etb() { return 0x17; }
  inline static hex N()   { return 0x4e; }

  int sendAck();
  bool recvAck(float timeOut= -1.0);

  int sendEot();
  bool recvEot(float timeOut= -1.0);

  // evaluate lrc of hex code (binary)
  static hex lrc(const hex* hdr, int sz= 8);

  // this is not needed...
  // evaluate lrc of hex code (performs ascii to hex)
  static hex lrcHA(const hex* ahdr, int sz= 14);

  // hepler to transate binar hex code to 1 or 2 character ascii text representation
  static string hexToHA(hex in, hex* out, int hlen= 1);

  // since ioctrl used by 'available()' func is fudged for cygwin, just use 'readable()'
  int waitForReply(float timeOut= -1.0); 

  // defaults follow directnet manual host comm. example, modified for target address 21 rather 22,
  // but with only one complete (256 byte) block, and 0 incomplete/fractional block:
  // init a host (master) read request
  int initReadReq(const hex target= 0x01);

  // if ack == true; if naq == false (binary hex only)
  bool recvInitAck(float timeOut= -1.0); 

  // hex binary:
  //int sendReadReq(const hex DataHexAddr[2]= {0x41, 0x01}, const hex target= 0x01);
  int sendReadReq(const hex DataHexAddr[2], const hex target= 0x01);
  // hex ascii: 
  // returns fractional bytecount
  int sendReadReqHA(const hex DataHexAddr[2], const hex target= 0x01);

  int recvBlock(hex block[256], int fracbytcnt= 0, float timeOut= -1.0);

  // parse hex block and set bits of in and out lines
  // return (outlines << 24 | inlines)
  static int parseBlock(int& inlines, int& outlines, hex* block, int sz= 256);

  // parse block as string and set bits of in and out lines
  // return (outlines << 24 | inlines)
  static int parseBlock(int& inlines, int& outlines, const string& block);

  // base address
  static PLCAddress _PLCBase;
  static hex _numBytes;

private:
  // convenience attribute is the post connection one-time cast of _devIO socket
  // (i.e. talk to PLC serial port via Perle Console Server TCP/IP socket)
  static UFClientSocket* _soc; 

  // offsets from base
  static std::map< string, short > _PLCInputLine; 
  static std::map< string, short > _PLCOUtputLine;
  // status
  static std::map< string, string > _PLCInputStatus;
  static std::map< string, string > _PLCOutputStatus;
};

#endif // __UFPLCConfig_h__
