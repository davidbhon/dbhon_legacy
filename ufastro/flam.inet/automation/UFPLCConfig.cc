#if !defined(__UFPLCConfig_cc__)
#define __UFPLCConfig_cc__ "$Name:  $ $Id: UFPLCConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPLCConfig_cc__;

#include "UFPLCConfig.h"
#include "UFGemAutoPLCAgent.h"
#include "UFFITSheader.h"

UFClientSocket* UFPLCConfig::_soc= 0;

UFPLCConfig::hex UFPLCConfig::_numBytes= 0x00; // 28;
UFPLCConfig::PLCAddress UFPLCConfig::_PLCBase = 0x0001;

// offsets from base
std::map< string, short > UFPLCConfig::_PLCInputLine; 
std::map< string, short > UFPLCConfig::_PLCOUtputLine;
// status
std::map< string, string > UFPLCConfig::_PLCInputStatus;
std::map< string, string > UFPLCConfig::_PLCOutputStatus;

UFPLCConfig::UFPLCConfig(const string& name) : UFDeviceConfig(name) {}

UFPLCConfig::UFPLCConfig(const string& name, const string& tshost, int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// connect calls create which then connects
UFTermServ* UFPLCConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }

  // use this for plc i/o
  _soc = static_cast< UFClientSocket* > ( _devIO );

  return _devIO;
}

vector< string >& UFPLCConfig::queryCmds() {
  if( _queries.size() > 0 ) 
    return _queries;

  // fetch parameter setting
  _queries.push_back("READ");
  _queries.push_back("STATUS");

  return _queries;
}

vector< string >& UFPLCConfig::actionCmds() {
  if( _actions.size() > 0 ) // should be none
    _actions.clear();

  return _actions;
}

int UFPLCConfig::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      return 1;
    }
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }

  clog<<"UFPLCConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFPLCConfig::status(UFDeviceAgent* da) {
  // UFGemAutoPLCAgent* plcda = dynamic_cast< UFGemAutoPLCAgent* > (da);
  if( da->_verbose )
    clog<<"UFPLCConfig::status> "<<da->name()<<endl;
  

  vector< string > vs;

  string plc = "PLC ADDR 0 == ";
  plc += "0";
  plc += "DC Enclosure Interlock";

  plc = "PLC ADDR 1 == ";
  plc += "0";
  plc += "CC Enclosure Interlock";
  vs.push_back(plc);

  return new UFStrings(da->name(), vs);
}

UFStrings* UFPLCConfig::statusFITS(UFDeviceAgent* da) {
  //  UFGemAutoPLCAgent* plcda = dynamic_cast< UFGemAutoPLCAgent* > (da);
  if( da->_verbose )
    clog<<"UFPLCConfig::statusFITS> "<<da->name()<<endl;
  
  map< string, string > valhash, comments;
  valhash["DCENCLOS"] = "0";
  comments["DCENCLOS"] = "DC Enclosure Interlock";
  valhash["CCENCLOS"] = "0";
  comments["CCENCLOS"] = "DC Enclosure Interlock";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

// AutoPLC command packet funcs:
int UFPLCConfig::sendAck() {
  if( _soc == 0 )
    return 0;

  hex h = ack();
  int ns = _soc->send(&h, 1);
  if( ns <= 0 )
    clog<<"UFPLCConfig::sendAck> failed to send ACK."<<endl;
  else if( _verbose )
    clog<<"UFPLCConfig::sendAck> sent ACK."<<endl;

  return ns;
}

int UFPLCConfig::waitForReply(float timeOut) {
  float to= 0;
  int nr= _soc->available();
  while ( nr <= 0 && ((timeOut < 0) || (timeOut > 0 && to < timeOut)) ) {
    UFPosixRuntime::sleep(0.5); to += 0.5;
    nr = _soc->available();
    if( nr < 0 )
      return nr;
  }
  if( nr <= 0 )
    return nr;

  return nr;
}

// is ack/nak char followed by '\r'?
bool UFPLCConfig::recvAck(float timeOut) {
  if( _soc == 0 )
    return false;

  int nr = waitForReply(timeOut);
  if( nr < 0 )
    return false;

  if( _verbose ) clog<<"UFPLCConfig::recvAck> plc msg length: "<<nr<<endl;
  hex h[2] = { 0x00, 0x00 };
  hex ak = ack(), nk = nak();
  int r= 0;
  if( nr >= 2 )
     r = _soc->recv(h, sizeof(h));
  else 
     r = _soc->recv(h, nr);

  if( r <= 0 ) {
    clog<<"UFPLCConfig::recvAck> recv failed!"<<endl;
    return false;
  }

  if( h[1] == '\r' && _verbose )
    clog<<"UFPLCConfig::recvAck> read CR terminator."<<endl;

  if( h[0] == nk ) {
    clog<<"UFPLCConfig::recvAck> NAK!"<<endl;
    return false;
  }

  if( h[0] != ak ) {
    clog<<"UFPLCConfig::recvAck> expected ACK, got: "<<(int)h[0]<<endl;
    return false;
  }

  return true;
}

int UFPLCConfig::sendEot() {
  if( _soc == 0 )
    return false;

  hex h = eot();
  int ns =  _soc->send(&h, 1);
  if( ns <= 0 )
    clog<<"UFPLCConfig::sendAck> failed to send EOT."<<endl;
  else if( _verbose )
    clog<<"UFPLCConfig::sendAck> sent EOT."<<endl;

  return ns;
}

// is eot char followed by '\r'?
bool UFPLCConfig::recvEot(float timeOut) {
  if( _soc == 0 )
    return false;

  int nr = waitForReply(timeOut);
  if( nr < 0 )
    return false;

  if( _verbose ) clog<<"UFPLCConfig::recvEOT> plc msg length: "<<nr<<endl;
  hex h[2] = { 0x00, 0x00 };
  int r= 0;
  if( nr >= 2 )
     r = _soc->recv(h, sizeof(h));
  else 
     r = _soc->recv(h, nr);

  if( r <= 0 )
    return false;

  if( h[1] == '\r' &&  _verbose )
    clog<<"UFPLCConfig::recvEOT> read CR terminator."<<endl;

  if( h[0] != eot() ) {
    clog<<"UFPLCConfig::recvEOT> expected EOT, got: "<<(int)h[0]<<endl;
    return false;
  }

  return true;
}

// evaluate lrc == Exclusive OR of the bytes  
UFPLCConfig::hex UFPLCConfig::lrc(const hex* hdr, int sz) {
  hex h = hdr[0] ^ hdr[1];
  if( _verbose ) clog<<"UFPLCConfig::lrc> val0: "<<hdr[0]<<", val1: "<<hdr[1]<<", xor(hex): "<<(int)h<<endl;
  for( int i = 2; i < sz; ++i ) {
    h = h ^ hdr[i];
    if( _verbose ) clog<<"UFPLCConfig::lrc> val: "<<hdr[i]<<", xor(hex): "<<(int)h<<endl;
  }
  return h;
}

// this is not needed...
// evaluate lrc of hex ascci  == Exclusive OR of the bytes  
UFPLCConfig::hex UFPLCConfig::lrcHA(const hex* hdr, int sz) {
  char ha[2] = { 0x00, 0x00 };
  ha[0] = (char) hdr[0];
  string s = (char*) ha;
  hex h = strToHex(s);
  ha[0] = (char) hdr[1];
  s = (char*) ha;
  hex val = strToHex(s);
  h = h ^ val;
  if( _verbose ) clog<<"UFPLCConfig::lrcHA> val: "<<s<<", val(hex): "<<(int)val<<", xor(hex): "<<(int)h<<endl;
  for( int i = 3; i < sz; ++i ) {
    ha[0] = (char) hdr[i];
    s = (char*) ha;
    val = strToHex(s);
    h = h ^ val;
    if( _verbose ) clog<<"UFPLCConfig::lrcHA> val: "<<s<<", val(hex): "<<(int)val<<", xor(hex): "<<(int)h<<endl;
  }

  return h;
}

unsigned char* hexToAscii(unsigned char hx) {
  static unsigned char retval[2];
  unsigned char tmp = hx & 0xF;
  if (tmp < 10) tmp += 0x30;
  else tmp += 55;
  retval[1] = tmp;
  tmp = (hx & 0xF0)>>4;
  if (tmp < 10) tmp += 0x30;
  else tmp += 55;
  retval[0] = tmp;
  return retval;
}

// defaults follow directnet manual host comm. example, modified for target address 21 rather 22,
// but with only one complete (256 byte) block, and 0 incomplete/fractional block:
// init a host (master) read request
int UFPLCConfig::initReadReq(const hex target) {
  if( _soc == 0 )
    return 0;

  hex req[3];
  req[0] = N();
  req[1] = 0x20 + target;
  req[2] = enq();
  /*
  hex *junk;
  hex newreq [6];
  for (int i=0; i<3; i++) {
    junk = hexToAscii(req[i]);
    newreq[i*2] = junk[0]; newreq[i*2+1]=junk[1];
    clog << newreq[i*2]<<newreq[i*2+1]<<endl;
  }
  for (int i=0; i<sizeof(newreq); i++) clog <<newreq[i];
  clog << endl;
  */
  return _soc->send(req, sizeof(req));
}

  // if ack == true; if naq == false
bool UFPLCConfig::recvInitAck(float timeOut) {
  if( _soc == 0 )
    return false;

  int nr = waitForReply(timeOut);
  if( nr < 0 ) {
    clog<<"UFPLCConfig::recvInitAck> PLC timedout: "<<timeOut<<endl;
    return false;
  }

  if( _verbose ) clog<<"UFPLCConfig::recvInitAck> plc msg length: "<<nr<<endl;
  hex reqack[nr], ak = ack(), nk = nak();
  //clog << "UFPLCConfig::recvInitAck> "<<nr<<" "<<reqack<<endl;
  int r = _soc->recv(reqack, nr);
  for (int i=0; i<r; i++)
    clog << hexToAscii(reqack[i])[1] << hexToAscii(reqack[i])[0];
  clog << endl;
  if( r <= 0 ) {
    clog<<"UFPLCConfig::recvInitAck> soc recv failed... no data/timeout."<<endl;
    return false;
  }
  if( reqack[nr-1] == '\r' && _verbose )
    clog<<"UFPLCConfig::recvInitAck> read CR terminator."<<endl;
  if( reqack[2] == nk ) {
    clog<<"UFPLCConfig::recvInitAck> NAK -- request init failed."<<endl;
    return false;
  }
  else if( reqack[2] != ak ) {
    clog<<"UFPLCConfig::recvInitAck> expected ACK, got: "<<reqack[2]<<endl;
    return false;
  }

  return true;
}

int UFPLCConfig::sendReadReq(const hex DataHexAddr[2], const hex target) {
  if( _soc == 0 )
    return 0;

  hex HexAddr[2]= {0x41, 0x01};
  if( !(DataHexAddr[0] == DataHexAddr[1] && DataHexAddr[0] == 0x00) ) {
    HexAddr[0] = DataHexAddr[0]; 
    HexAddr[1] = DataHexAddr[1]; 
  }

  hex packet[11];
  packet[0] = soh();
  packet[1] = target;
  packet[2] = 0x00; // read input or output lines
  packet[3] = 0x01; // data type (0x01 == output lines; 0x02 == input lines)
  packet[4] = DataHexAddr[0]; //data adddr msb
  packet[5] = DataHexAddr[1]; //data adddr lsb
  packet[6] = 0x01; // only 1 (complete) block cnt
  packet[7] = 0x00; // byte cnt of 0 fractional (final) block
  packet[8] = 0x00; // master station ID is always either 0 or 1
  packet[9] = etb();
  packet[10] = lrc(&packet[1]);

  if( _verbose ) clog<<"UFPLCConfig::sendReadReq> LRC: "<<(int)packet[10]<<endl;
  int ns =  _soc->send(packet, sizeof(packet));
  if( ns < 0 ) 
    clog<<"UFPLCConfig::sendReadReq> failed send read req."<<endl;
  else if( _verbose ) 
    clog<<"UFPLCConfig::sendReadReq> sent read req."<<endl;
  
  return ns;
}

string UFPLCConfig::hexToHA(hex in, hex* out, int hlen) {
  string s;
  if( hlen < 1 )
    return s;
  hex tmp= 0, ascii_digit = 0x30; // '0'
  hex ascii_text = 0x41; // 'A'
  char cs[3] = { 0x00, 0x00, 0x00 };
  if( hlen < 2 ) {
    tmp = in & 0x0f;
    if( tmp < 10 ) {
      tmp += ascii_digit;
    }
    else {
      tmp -= 10;
      tmp += ascii_text;
    }
    cs[0] = (char) tmp;
    s = cs;
    *out = (hex) cs[0];
    return s;
  }

  tmp = in >> 4;
  if( tmp < 10 ) {
    tmp += ascii_digit;
  }
  else {
    tmp -= 10;
    tmp += ascii_text;
  }
  cs[0] = (char) tmp;

  tmp = in & 0x0f;
  if( tmp < 10 ) {
    tmp += ascii_digit;
  }
  else {
    tmp -= 10;
    tmp += ascii_text;
  }
  cs[1] =  (char) tmp;
  s = cs;
  out[0] = (hex) cs[0];
  out[1] = (hex) cs[1];
  return s;
}
 
int UFPLCConfig::sendReadReqHA(const hex DataHexAddr[2], const hex target) {
  if( _soc == 0 )
    return 0;

  hex HexAddr[2]= { 0x00, 0x01 };
  HexAddr[0] = DataHexAddr[0]; 
  HexAddr[1] = DataHexAddr[1]; 

  // according to example pg 6-12, this should request hex data for 1 full block...
  //hex packet[17];
  hex packet[18];
  packet[0] = soh();

  hex ha[2]= { 0x00, 0x00 };
  string s = hexToHA(target, ha, sizeof(ha));
  if( _verbose ) clog<<"UFPLCConfig::sendReadReqHA> target: "<<s<<endl;
  packet[1] = ha[0];
  packet[2] = ha[1];

  hex read = 0x00; // read input or output lines
  s += hexToHA(read, &packet[3]);

  hex dtyp = 0x01;// data type (0x01 == output lines; 0x02 == input lines)
  s += hexToHA(dtyp, &packet[4]);
  
  s += hexToHA(DataHexAddr[0], ha, sizeof(ha)); 
  packet[5] = ha[0]; //data addr msb
  packet[6] = ha[1]; //data addr msb

  s += hexToHA(DataHexAddr[1], ha, sizeof(ha)); 
  packet[7] = ha[0]; //data addr lsb
  packet[8] = ha[1]; //data addr lsb

  hex blocks = 0x01; // only 1 (complete) block cnt
  //hex blocks = 0x00; // only 1 (complete) block cnt
  s += hexToHA(blocks, ha, sizeof(ha));
  packet[9] = ha[0];
  packet[10] = ha[1];

  //hex frac = 0x00; // byte cnt of 0 fractional (final) block
  hex frac = _numBytes; // byte cnt of fractional (final) block
  s += hexToHA(frac, ha, sizeof(ha));
  packet[11] = ha[0];
  packet[12] = ha[1];

  hex master = 0x00; // master station ID is always either 0 or 1
  s += hexToHA(master, ha, sizeof(ha));
  packet[13] = ha[0];
  packet[14] = ha[1];

  //if( _verbose ) clog<<"UFPLCConfig::sendReadReqHA> "<<s<<endl;
  packet[15] = etb();
  hex hlrc = lrc(&packet[1], 14);
  s += hexToHA(hlrc, ha, sizeof(ha));
  packet[16] = ha[0]; 
  packet[17] = ha[1]; 
  
  if( _verbose ) clog<<"UFPLCConfig::sendReadReqHA> LRC: "<<(int)hlrc<<" for: "<<s<<endl;
  int ns =  _soc->send(packet, sizeof(packet));
  if( ns < 0 ) 
    clog<<"UFPLCConfig::sendReadReqHA> failed send read req."<<endl;
  else if( _verbose ) 
    clog<<"UFPLCConfig::sendReadReqHA> sent read req."<<endl;

  return (int)66;
}


int UFPLCConfig::recvBlock(hex block[256], int fracbytcnt, float timeOut) {
  memset(block, 0, sizeof(block));
  if( _soc == 0 )
    return 0;

  int nr = waitForReply(timeOut);
  if( nr <= 0 )
    return nr;

  int sz = (int)sizeof(block);
  if( fracbytcnt > 0 )
    sz = fracbytcnt;

  hex h= 0x00;
  hex s= stx(), ex= etx(), eb= etb(), lrc= 0;
  int r = _soc->recv(&h, 1);
  if( r <= 0 )
    return r;

  if( h != s )
    clog<<"UFPLCConfig::recvBlock> expected STX, but got: "<<(int)h<<endl;
  else if( _verbose ) 
    clog<<"UFPLCConfig::recvBlock> got STX"<<endl;

   hex buf[BUFSIZ]; memset(buf, 0, sizeof(buf));
   nr = waitForReply(timeOut);
   if( nr > sz ) 
     r = _soc->recv(buf, nr);
   else
     r = _soc->recv(buf, sz);

  if( r !=  sz )
    clog<<"UFPLCConfig::recvBlock> expected: "<<sz<<", but got: "<<r<<endl;
  
  if (_verbose) {
    //for (int i=0; i<r; i++) clog << hexToAscii(buf[i])[0]<<hexToAscii(buf[i])[1];
    clog << "UFPLCConfig::recvBlock> got: ";
    for (int i=0; i<r; i++) clog << buf[i];
    clog << endl;
  }

  if( r < sz )
    memcpy(block, buf, r);
  else
    memcpy(block, buf, sz);

  if( r <= sz ) { 
    r = _soc->recv(&h, 1); // recv etx/etb
    if( r <= 0 )
      return r;
    if( h != eb && h != ex )
      clog<<"UFPLCConfig::recvBlock> expected ETB or  ETX, but got: "<<h<<endl;
    else if( _verbose ) 
      clog<<"UFPLCConfig::recvBlock> got ETB/ETX"<<endl;
    r = _soc->recv(&lrc, 1); // recv lrc
    if( r <= 0 )
      return r;
    if( _verbose ) clog<<"UFPLCConfig::recvBlock> got LRC: "<<lrc<<endl;
  }
  else {
    h = 0;
    int i;
    for( i = sz; i < r && (h != eb && h != ex); ++i )
      h = buf[i]; // etx/etb followed by lrc
    // got etx/etb
    if( i < r ) {
      lrc = buf[i];
    }
    else {
      r = _soc->recv(&lrc, 1); // recv lrc
      if( r <= 0 )
        return r;
    }
    if( _verbose ) clog<<"UFPLCConfig::recvBlock> got LRC: "<<lrc<<endl;
  }

  return r;
}

// static funcs.
// parse hex block and set bits of in and out lines
// return (outlines << 24 | inlines)
int UFPLCConfig::parseBlock(int& inlines, int& outlines, hex* block, int sz) {
  int inoutlines = inlines = outlines = 0;
  return inoutlines;
}

// parse block as string and set bits of in and out lines
// return (outlines << 24 | inlines)
int UFPLCConfig::parseBlock(int& inlines, int& outlines, const string& block) {
  int inoutlines = inlines = outlines = 0;
  return inoutlines;
}

#endif // __UFPLCConfig_cc__
