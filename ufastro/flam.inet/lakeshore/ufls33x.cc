#if !defined(__ufls33x_cc__)
#define __ufls33x_cc__ "$Name:  $ $Id: ufls33x.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

bool quiet = false;

class ufls33x : public UFDaemon, public UFClientSocket {
public:
  ~ufls33x();
  inline ufls33x(int argc, char** argv, int port= -1) : UFDaemon(argc, argv),
						        UFClientSocket(port), _fs(0) {
    rename("ufls33x@" + hostname());
  }

  static int main(int argc, char** argv);
  // return 0 on connection failure:
  FILE* init(const string& host, int port);

  // submit string (only), return immediately without fetching reply
  int submit(const string& raw, float flush= -1.0); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  //int submit(const string& raw, UFStrings& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& raw, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.

  virtual string description() const { return __ufls33x_cc__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
};

int ufls33x::main(int argc, char** argv) {
  ufls33x ls33x(argc, argv);
  string arg, host(hostname()), raw("true"); // default is raw command mode
  int port= 52003;
  float flush= -1.0;

  arg = ls33x.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = ls33x.findArg("-raw");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = ls33x.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = ls33x.findArg("-port");
  if( arg != "false" && arg != "false" )
    port = atoi(arg.c_str());

  arg = ls33x.findArg("-q");
  if( arg != "false" )
    quiet = true;

  if( port <= 0 || host.empty() ) {
    clog<<"ufls33x> usage: 'ufls33x -flush sec. -host host -port port -raw raw-command'"<<endl;
    return 0;
  }

  FILE* f  = ls33x.init(host, port);
  if( f == 0 ) {
    clog<<"ufls33x> unable to connect to LakeShore208 command server/agent..."<<endl;
    return 1;
  }

  UFStrings* reply_p;;
  if( raw != "true" && raw != "false" ) {
    // raw (explicit) command should be executed once
    int ns = ls33x.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufls33x> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  // enter command loop
  string line;
  while( true ) {
    clog<<"ufls33x(raw)> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;

    raw = line;
    int ns = ls33x.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufls33x> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  return 0; 
}

// public static func:
// return < 0 on connection failure:
FILE* ufls33x::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !quiet )
    clog<<"ufls33x> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// public
ufls33x::~ufls33x() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

// submit string (only), return immediately without fetching reply
int ufls33x::submit(const string& s, float flush) {
  vector<string> cmd;
  cmd.push_back("raw");
  cmd.push_back(s);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufls33x::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
//int ufls33x::submit(const string& s, UFStrings& r, float flush) {
int ufls33x::submit(const string& s, UFStrings*& r, float flush) {
  int ns = submit(s, flush);
  if( ns <= 0 )
    return ns;

  /*
  ns = recv(r);
  if( ns <= 0 )
    return ns;

  return r.elements();
  */

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

int main(int argc, char** argv) {
  return ufls33x::main(argc, argv);
}

#endif // __ufls33x_cc__
