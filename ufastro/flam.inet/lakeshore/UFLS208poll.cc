#if !defined(__UFLS208poll_cc__)
#define __UFLS208poll_cc__ "$Name:  $ $Id: UFLS208poll.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFLS208poll.h"
#include "UFDeviceAgent.h"
#include "fcntl.h"
#include "errno.h"

string UFLS208poll::description() const { return __UFLS208poll_cc__; }

UFLS208poll::UFLS208poll(const string& name,
			 UFRuntime::Argv* args) : UFDaemon(name, args),
					          _config(0), _flush(5.0), 
                                                  _fifoname(), _chan() {
  strstream s;
  s<<"/tmp/.lakeshore208."<<getpid()<<ends;
  _fifoname = s.str();
  delete s.str();
  for( int i = 1; i <= 8 ; ++i )
    _chan.push_back(i);
}

UFLS208poll::UFLS208poll(const string& name, UFDeviceConfig* conf,
			 UFRuntime::Argv* args) : UFDaemon(name, args),
					          _config(conf), _flush(5.0), 
                                                  _fifoname(), _chan() {
  strstream s;
  s<<"/tmp/.lakeshore208."<<getpid()<<ends;
  _fifoname = s.str();
  delete s.str();
  for( int i = 1; i <= 8 ; ++i )
    _chan.push_back(i);
}

int UFLS208poll::fetchKelvin(int outFd) {
  // event loop gets lakeshore temperature readings and write to fifo
  string eots = "\n";
  string eotr = "K,I\r\n";
  _config->_devIO->resetEOTR(eotr);
  _config->_devIO->resetEOTS(eots);

  //string line = "1,KKK.K 2,KKK.K 3,KKK.K 4,KKK.K 5,KKK.K 6,KKK.K 7,KKK.K 8,KKK.K\n";
  int nb= 0, tchan= (int)_chan.size();
  string reply, line;
  strstream s1;
  s1<<"YC"<<_chan[0]<<ends; // set first channel
  string cmd = s1.str(); delete s1.str();
  //clog<<"UFLS208poll> submit channel change cmd: "<<cmd<<", flush= "<<_flush<<endl;
  nb = _config->_devIO->submit(cmd, _flush); // must we wait so long?
  if( nb <= 0 ) {
    clog<<"UFLS208poll> unable to submit channel change cmd: "<<cmd<<endl;
     return nb;
  }
  while( true ) {
    line = "(";
    line += UFRuntime::currentTime();
    line += ") ";
    for( int i = 0; i < tchan; ++i ) {
      strstream s;
      s<<"YC"<<_chan[i]<<ends; // change channel
      cmd = s.str(); delete s.str();
      //clog<<"UFLS208poll> submit channel change cmd: "<<cmd<<", flush= "<<_flush<<endl;
      if( tchan > 1 ) { // change channels
        nb = _config->_devIO->submit(cmd, _flush); // must we wait so long?
        if( nb <= 0 ) {
      	  clog<<"UFLS208poll> unable to submit channel change cmd: "<<cmd<<endl;
      	  return nb;
        }
      }
      cmd = "WS"; // get reading
      //clog<<"UFLS208poll> submit channel read cmd: "<<cmd<<", flush= "<<_flush<<endl;
      nb = _config->_devIO->submit(cmd, reply, _flush); // returns channel temp. reading
      if( nb <= 0 ) {
	clog<<"UFLS208poll> unable to submit temperature reading cmd: "<<cmd<<endl;
	return nb;
      }
      else {
	int eotpos = reply.rfind(eotr);
	reply.erase(eotpos, reply.length()-1); // erase to end-of-text?
	reply += " "; // append a space
	line += reply; // append to full-line output
      }
      //clog<<"UFLS208poll> reading(s): "<<line<<endl;
    }
    // line should now have the 8 values with EOTRs eliminated:
    line += "\n"; // append default terminator
    //clog<<"UFLS200poll> write fifo( "<<_fifoname<<"): "<<line<<flush;
    // write it out the fifo
    nb = ::write(outFd, line.c_str(), line.length());
    if( nb <= 0 ) {
      clog<<"UFLS200poll> unable to write fifo. "<<_fifoname<<endl;
      return nb;
    }
  } // forever while
  return nb;
}

UFDeviceConfig* UFLS208poll::init(const string& name,
				  const UFRuntime::Argv* args ) {
  _name = name; _args = (UFRuntime::Argv*) args;

  string arg, host(hostname());
  int port= 7003;
  int argc = (int) _args->size();

  clog<<"UFLS208poll::init> argc= "<<argc<<endl;

  // any more cmd line opts?
  arg = findArg("-tshost");
  if( arg != "false" )
    host = arg;

  arg = findArg("-tsport");
  if( arg != "false" && arg != "false" )
    port = atoi(arg.c_str());

  arg = findArg("-flush");
  if( arg != "false" )
    _flush = atof(arg.c_str());

  arg = findArg("-fifo");
  if( arg != "false" && arg != "true" )
    _fifoname = atoi(arg.c_str());

  arg = findArg("-chan"); // -chan "1 2 3 4 5 6 7 8"
  if( arg != "false" && arg != "true" ) {
    _chan.clear();
    //clog<<"UFLS208poll> -chan= "<<arg<<endl;
    size_t len= 0, pos= 0, posSp= 0;
    int cnt= 0;
    while( posSp != string::npos  && ++cnt <= 8 ) {
      pos = posSp;
      posSp = arg.find(" ", 1+pos);
      if( posSp != string::npos )
        len = posSp - pos;
      else
        len = arg.size() - pos;
      string s = arg.substr(pos, len);
      int c = atoi(s.c_str());
      _chan.push_back(c);
      //clog<<"UFLS208poll> "<<pos<<" "<<posSp<<" "<<len<<" "<<_chan.size()<<" "<<s<<" "<<c<<endl;
    }
  }

  clog<<"UFLS208poll> flush= "<<_flush<<",  tshost= "<<host<<", tsport= "
      <<port<<", fifo= "<<_fifoname<<", chan= "<<_chan.size()<<endl;
  for( int i = 0; i < (int)_chan.size(); ++i )
    clog<<"UFLS208poll> chan= "<<_chan[i]<<endl;

  if( port <= 0 || host.empty() ) {
    clog<<"UFLS208poll> usage: 'UFLS208poll -flush sec. -tshost host -tsport port -fifo fifofile -chan \"1 2 3 4 5 6 7 8\"'"
	<<endl;
  }

  if( _config == 0 ) {
    _config = new UFDeviceConfig(name, host, port); // given host & port this should connect
  }
  if( _config == 0 ) {
    clog<<"UFLakeShore208Agent> failed to init device config..."<<endl;
  }
  _config->connect();
  if( _config->_devIO == 0 ) {
    clog<<"UFLakeShore208Agent> failed to connect to device port..."<<endl;
    delete _config; _config = 0;
  }
  return _config;
}

void* UFLS208poll::exec(void* p) {
  UFLS208poll* ls208p = (UFLS208poll*)p;
  string devname = ls208p->name();
  const UFRuntime::Argv* args = ls208p->getArgs();
  if( ls208p->init(devname, args) == 0 ) {
    clog<<"UFLS208poll::exec> abort..."<<endl;
    return 0;
  }
  // this open blocks until something opens other end for read...
  clog<<"UFLS200poll> blocking on open of output fifo. "<<_fifoname<<endl;
  //getchar();
  //  int outFd = ::open(_fifoname.c_str(), O_WRONLY | O_NONBLOCK);
  int outFd = ::open(_fifoname.c_str(), O_WRONLY);
  if( outFd <= 0 ) {
    clog<<"UFLS200poll> unable to open output fifo. "<<_fifoname<<", fd= "
	<<outFd<<", errno: "<<strerror(errno)<<endl;
    return 0;
  }
  // send out formatted example:
  string t0 = UFRuntime::currentTime();
  string line = "("+t0+") "+"1,KKK.K 2,KKK.K 3,KKK.K 4,KKK.K 5,KKK.K 6,KKK.K 7,KKK.K 8,KKK.K\n";
  int nb = ::write(outFd, line.c_str(), line.length());
  if( nb <= 0 ) {
    clog<<"UFLS200poll> unable to write fifo. "<<_fifoname<<endl;
    return 0;
  }

  // enter poll loop:
  nb = fetchKelvin(outFd);
  if( nb <= 0 )
    return 0;
  else
    return p;
}

// public dtor relies on UFDaemon & UFDeviceConfig dtors
UFLS208poll::~UFLS208poll() {}

// static main for unit test
int UFLS208poll::main(int argc, char** argv) {
  UFRuntime::Argv args;
  int cnt = argVec(argc, argv, args); // init args
  if( cnt <= 0 )
    clog<<"UFLS208poll> usage: 'UFLS208poll -flush sec. -tshost host -tsport port -fifo fifofile'"<<endl;

  string name = "test208poll";
  UFLS208poll LS208(name, &args);
  void* p = LS208.exec((void*) &LS208);
  if( p == 0 )
    return -1;
  else
    return 0;
}

// test main
#if defined(_UNIT_TEST_)
int main(int argc, char** argv) { return UFLS208poll::main(argc, argv); }
#endif

#endif // __UFLS208poll_cc__
