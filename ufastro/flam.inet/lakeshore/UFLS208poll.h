#if !defined(__UFLS208poll_h__)
#define __UFLS208poll_k__ "$Name:  $ $Id: UFLS208poll.h,v 0.1 2002/06/26 20:34:06 trecs beta $"
#define __UFLs208poll_H__(arg) const char arg##UFLS208poll_h__rcsId[] = __UFLakeShore208Agent_h__;

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFLS208Config.h"

class UFLS208poll : public UFDaemon {
public:
  ~UFLS208poll();
  UFLS208poll(const string& name, UFRuntime::Argv* args);
  UFLS208poll(const string& name, UFDeviceConfig* conf, UFRuntime::Argv* args);

  static int main(int argc, char** argv);
  inline string fifoName() { return _fifoname; }

  // writes stdout by default, otherwise fifo
  int UFLS208poll::fetchKelvin(int outFd= 1);
  UFDeviceConfig* init(const string& name, const UFRuntime::Argv* args);

  // override UFDaemon virtuals:  
  virtual string description() const;
  virtual void* exec(void* p= 0);

protected:
  UFDeviceConfig* _config;
  float _flush;
  string _fifoname;
  vector < int > _chan;
};

#endif // __UFLS208poll_h__
