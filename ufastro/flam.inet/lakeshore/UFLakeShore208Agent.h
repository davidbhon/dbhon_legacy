#if !defined(__UFLakeShore208Agent_h__)
#define __UFLakeShore208Agent_h__ "$Name:  $ $Id: UFLakeShore208Agent.h,v 0.1 2002/06/26 20:34:04 trecs beta $"
#define __UFLakeShore208Agent_H__(arg) const char arg##UFLakeShore208Agent_h__rcsId[] = __UFLakeShore208Agent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFDeviceAgent.h"

class UFLakeShore208Agent: public UFDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFLakeShore208Agent(int argc, char** argv);
  UFLakeShore208Agent(const string& name, int argc, char** argv);
  inline virtual ~UFLakeShore208Agent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // initialize connection to device & return device info:
  // since the baud rate to the 208 is so slow (300) and
  // its command processor takes ~2-4 sec, this should
  // fork a child process that will poll the values and
  // write to a named-pipe. child should open the pipe
  // for writing, parent for reading:
  virtual UFTermServ* init(const string& host= "", int port= 0);

  // this should check for avaiablity of new values
  // from child poll process via the named-pipe:
  virtual void* ancillary(void* p= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  // some (raw) LakeShore208 actions return replies, some do not:
  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline string getCurrent() { return _currentval; }

protected:
  int _fifoInput; // fd for LS208 fifo
  static string _currentval;
  // helper for query & action cmds, just access history cache
  UFStrings* _allKelvin(UFDeviceAgent::CmdInfo* req); 
};

#endif // __UFLakeShore208Agent_h__
      
