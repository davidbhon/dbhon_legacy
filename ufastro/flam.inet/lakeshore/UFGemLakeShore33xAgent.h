#if !defined(__UFGemLakeShore33xAgent_h__)
#define __UFGemLakeShore33xAgent_h__ "$Name:  $ $Id: UFGemLakeShore33xAgent.h,v 0.3 2006/08/28 21:26:06 hon Exp $"
#define __UFGemLakeShore33xAgent_H__(arg) const char arg##UFGemLakeShore33xAgent_h__rcsId[] = __UFGemLakeShore33xAgent_h__;

#include "UFGemDeviceAgent.h"
#include "UFLS33xConfig.h"

#include "string"
#include "vector"
#include "deque"
#include "iostream.h"

class UFGemLakeShore33xAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemLakeShore33xAgent(int argc, char** argv);
  UFGemLakeShore33xAgent(const string& name, int argc, char** argv);
  inline virtual ~UFGemLakeShore33xAgent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  virtual void startup();

  virtual string newClient(UFSocket* client);

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in addition to establishing the iocomm/annex connection tp lakeshore,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);
  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);
  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  inline void getCurrent(string& chanA, string& chanB) {
    chanA = _currentvalA; chanB = _currentvalB;
    return;
  }

  inline float getSetPntA() { return _setpvalA; }
  inline float getSetPntB() { return _setpvalB; }
  inline float getSetPntA(string& sval) { strstream s; s<<_setpvalA<<ends; sval = s.str(); delete s.str(); return _setpvalA; }
  inline float getSetPntB(string& sval) { strstream s; s<<_setpvalB<<ends; sval = s.str(); delete s.str(); return _setpvalB; }
  inline void getSetPnts(string& svalA, string& svalB) { getSetPntA(svalA); getSetPntB(svalB); }

protected:
  static string _currentvalA, _currentvalB;
  static float  _setpvalA, _setpvalB;
  static string _setpvalACmd, _setpvalBCmd;
  static string _setpidACmd, _setpidBCmd;
  static string _manualACmd, _manualBCmd;
  static string _rangeA, _rangeB; // evidently there is range for the 332 B (assuming control loop 2) is 0/off or 1/on?
  static string _ctrlA, _ctrlB;
  static string _setPointRec;
  static string _simkelvin1, _simkelvin2;
  static time_t _timeOut; // in seconds 
  static time_t _clock;
  static bool _setcrv, _setintyp;

  static string _simsetp();
};

#endif // __UFGemLakeShore33xAgent_h__
      
