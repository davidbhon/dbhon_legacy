#if !defined(__UFLS218Config_cc__)
#define __UFLS218Config_cc__ "$Name:  $ $Id: UFLS218Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLS218Config_cc__;

#include "UFLS218Config.h"
#include "UFGemLakeShore218Agent.h"
#include "UFFITSheader.h"

#include "ctype.h"

UFLS218Config::UFLS218Config(const string& name) : UFDeviceConfig(name) {}

UFLS218Config::UFLS218Config(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// LS218 terminator is carriage-retuen + new-line:
string UFLS218Config::terminator() { return "\r\n"; }

// LS218 prefix is none:
//string UFLS218Config::prefix() { return ""; }

vector< string >& UFLS218Config::queryCmds() {
  _queries.push_back("history"); _queries.push_back("History"); _queries.push_back("HISTORY");
  return _queries;
}

vector< string >& UFLS218Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("status"); _actions.push_back("Status"); _actions.push_back("STATUS");
  _actions.push_back("krdg?0"); _actions.push_back("Krdg?0"); _actions.push_back("KRDG?0"); 

  return _actions;
}

int UFLS218Config::validCmd(const string& c) {
  // allow psuedo querry in the form of a numeric (history cnt)
  const char* cc =  c.c_str();
  if( isdigit(*cc) != 0 )
    return 0;    

  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      if( c.find("?") == string::npos )
        return 0;
      else
	return 1;
    }
  }
  
  vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( c.find(qv[i++]) == 0 )
      return 1;
  }

  clog<<"UFLS218Config::validCmd> ?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFLS218Config::status(UFDeviceAgent* da) {
  UFGemLakeShore218Agent* da218 = dynamic_cast< UFGemLakeShore218Agent* > (da);
  if( da218->_verbose )
    clog<<"UFLS218Config::status> "<<da218->name()<<endl;
  string ls218 = da218->getCurrent(); 
  // parse this comma separated list and make a UFStrings...
  vector< string > vals;
  string s;
  int pos = 0;
  int comma = ls218.find(",", pos);
  //s = "Cryo. Cooler";
  s = "LS218Kelvin1";
  s += " == " + ls218.substr(pos, comma) + " Kelvin";
  vals.push_back(s);
 
  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin2";
  s += " == " + ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin3"; s += " == " + ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin4"; s += " == " +  ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin5"; s += " == " + ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin6"; s += " == " + ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  s = "LS218Kelvin7"; s += ls218.substr(pos, comma-pos) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  //comma = ls218.find(",", pos);
  // last value is not followed by comma
  s = "LS218Kelvin8"; s += " == " + ls218.substr(pos) + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  return new UFStrings(da218->name(), vals);
}

UFStrings* UFLS218Config::statusFITS(UFDeviceAgent* da) {
  UFGemLakeShore218Agent* da218 = dynamic_cast< UFGemLakeShore218Agent* > (da);
  string ls218 = da218->getCurrent(); 
  // parse this comma separated list and make a UFStrings...
  map< string, string > valhash, comments;
  int pos = 0;
  int comma = ls218.find(",", pos);
  //valhash["CryoCool"] = ls218.substr(pos, comma-pos);
  //comments["CryoCool"] = "Kelvin";
  valhash["LS218K1"] = ls218.substr(pos, comma-pos);
  comments["LS218K1"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K2"] = ls218.substr(pos, comma-pos);
  comments["LS218K2"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K3"] = ls218.substr(pos, comma-pos);
  comments["LS218K3"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K4"] = ls218.substr(pos, comma-pos);
  comments["LS218K4"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K5"] = ls218.substr(pos, comma-pos);
  comments["LS218K5"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K6"] = ls218.substr(pos, comma-pos);
  comments["LS218K6"] = "Kelvin";

  pos = 1 + comma;
  comma = ls218.find(",", pos);
  valhash["LS218K7"] = ls218.substr(pos, comma-pos);
  comments["LS218K7"] = "Kelvin";

  pos = 1 + comma;
  //comma = ls218.find(",", pos);
  valhash["LS218K8"] = ls218.substr(pos); // last value is not followed by comma
  comments["LS218K8"] = "Kelvin";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da218->name());

  return ufs;
}

#endif // __UFLS218Config_cc__
