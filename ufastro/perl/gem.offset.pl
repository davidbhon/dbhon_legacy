#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
  #Get present values
  my $soc = ufgem::viiconnect();
  ufgem::getOffset_radec($soc);

}elsif( $cnt < 2 ) {
  #Print help message
  die "\n\tUsage:".
      "\n\tEnter absolute offsets, in units of arcseconds, ".
      "for both ra and dec:".
      "\n\tgem.offset.pl ra dec\n\n";

}else{
  my $ra = shift;
  my $dec = shift;

  if( $ra =~ m/[a-zA-Z]/ or $dec =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical ra or dec by mistake?".
        "\n\nra = $ra; dec = $dec\n\n";

  }else{
    my $soc = ufgem::viiconnect();
    ufgem::setOffset_radec($soc, $ra, $dec);
    close( $soc );

  }
}#End
