#!/usr/bin/perl
open PAD, ">TReCS.pad";
$sz = 320*240*4;
$pad = $sz / (36*80); # i.e. 2880 byte increments
print "pad = $pad\n";
$pad = $pad - int($pad); # subtract integer part
print "pad = $pad\n";
$pad = (1 - $pad) * (36*80);
print "pad = $pad\n";
$roundup = 0.000001;
$pad = int($pad + $roundup);
print "pad = $pad\n";
for( $i = 0; $i < $pad; $i++ ) { syswrite PAD, 0, 1; }
close PAD;
############################
open PAD, ">Flamingos.pad";
$sz = 2048*2048*4;
$pad = $sz / (36*80); # i.e. 2880 byte increments
print "pad = $pad\n";
$pad = $pad - int($pad); # subtract integer part
print "pad = $pad\n";
$pad = (1 - $pad) * (36*80);
print "pad = $pad\n";
$roundup = 0.000001;
$pad = int($pad + $roundup);
print "pad = $pad\n";
for( $i = 0; $i < $pad; $i++ ) { syswrite PAD, 0, 1; }
close PAD;
