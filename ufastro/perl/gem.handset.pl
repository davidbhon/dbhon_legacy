#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
  #Get present values
  my $soc = ufgem::viiconnect();
  ufgem::getOffset_radec($soc);
  close( $soc );

}elsif( $cnt < 2 ) {
  #Print help message
   die "\n\tUsage:".
       "Enter relative offsets, in units of arcseconds, ".
       "for both ra and dec:\n".
       "gem.handset.pl ra dec\n\n";

}else{
  my $ra = shift;
  my $dec = shift;

  if( $ra =~ m/[a-zA-Z]/ or $dec =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical ra or dec by mistake?".
        "\n\nra = $ra; dec = $dec\n\n";

  }else{
    #print "ra = $ra; dec = $dec\n"; die;
    my $soc = ufgem::viiconnect();
    ufgem::setHandset_radec($soc, $ra, $dec);
    close( $soc );
  }
}#End


