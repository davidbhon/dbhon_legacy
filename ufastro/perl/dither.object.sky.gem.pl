#!/usr/local/bin/perl -w

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = param_value( 'EXP_TIME' );
my $nreads          = param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

#>>>Get dither info from header
#>>>Also insist that it starts at position 0
my $startpos = 0;
my $dpattern = param_value( 'DPATTERN' );#general pattern
my $d_scale  = param_value( 'D_SCALE'  );
my $d_rptpos = param_value( 'D_RPTPOS' );
my $d_rptpat = param_value( 'D_RPTPAT' );

my $usenudge = param_value( 'USENUDGE' );
my $nudgesiz = param_value( 'NUDGESIZ' );

#Load dither patterns
my $pat_name_len = length $dpattern;
print "pattern is $dpattern; length $pat_name_len\n";
my $ar_pat_x = load_pattern_x( $dpattern );
my $ar_pat_y = load_pattern_y( $dpattern );
my $ar_pat_wrap = load_pattern_wrap( $dpattern );
scale_pattern( $ar_pat_x, $ar_pat_y );
scale_pattern_wrap( $ar_pat_wrap );
my @pat_x = @$ar_pat_x;
my @pat_y = @$ar_pat_y;
my @wrap_x_y = @$ar_pat_wrap;
my $num_moves = @pat_x - 1;
my $last_pos_num  = $num_moves - 1;
print "num moves = $num_moves, lastpos= $last_pos_num\n";

my $ar_offset_x = load_pat_rel_Xorigin( $dpattern );
my $ar_offset_y = load_pat_rel_Yorigin( $dpattern );
scale_pattern( $ar_offset_x, $ar_offset_y );
my @rel_offset_x = @$ar_offset_x;
my @rel_offset_y = @$ar_offset_y;

my $X; my $Y;; my $X_origin; my $Y_origin;

print "pos:      Xrel       Yrel     X_wrt_Im1    Y_wrt_Im1\n";
for( my $i = 0; $i < (@pat_x - 1); $i++){
  print  "$i:  ";
  printf '% 10.3f', $pat_x[$i];print ", ";
  printf '% 10.3f', $pat_y[$i];print ", ";
  printf '% 10.3f', $rel_offset_x[$i];print ", ";
  printf '% 10.3f', $rel_offset_y[$i];
  printf "\n";
}
print "\n\n";

print "Number of images per position:    $d_rptpos\n";
print "Number of times through pattern:  $d_rptpat\n\n";

#>>>Get Filename info from header
my $orig_dir  = param_value( 'ORIG_DIR' );
my $filebase  = param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = does_dir_exist( $orig_dir );
my $last_index = whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#Setup mce4 only once
print "Configuring the exposure time and cycle type on MCE4\n";
setup_mce4( $expt, $nreads );

my $soc;
if( $VIIDO ){
  ##See if we can connect to the vii
  print "Connecting to the VII\n";
  $soc = ufgem::viiconnect();
  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    #die "?Bad socket connection to Gemini VII server\n";
  }
}else{
  print "Viido = $VIIDO; should be doing vii test connection\n\n";
}

prompt_setup();
my $probe_name = query_probe_name();

if( $VIIDO ){
#print "Viido = $VIIDO; should be setting up nod\n\n";
#print "Temporarily commented out\n\n";
setup_nod( $soc );
}else{
  print "Viido = $VIIDO; should be setting up nod\n\n";
}

#See if should get tcs info
my $use_tcs = param_value( 'USE_TCS' );
my $telescop = param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n";

my $x_nudge = 0; my $y_nudge = 0;
my $next_index = $this_index;
set_filename_header_param( $filebase, $next_index, $header);
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ( $x_nudge, $y_nudge ) = nudge_pointing( $d_rptpat, $rpt_pat );
  $X_origin = $rel_offset_x[$startpos] + $x_nudge; 
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;
  write_rel_offset_to_header( $header, $X_origin, $Y_origin );
  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position
    
    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){ 
      print "Probe $probe_name follow on\n";
      probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      guideOff( $soc );
      print "offset to position $startpos: $X, $Y, in beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      setOffset_radec( $soc, $X, $Y );
      offset_wait();
      print "guide on\n";
      guideOn( $soc );
      guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "***offset to position $startpos: $X, $Y, in beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; offset_wait();
	   print "***guide on\n";
	   print "***"; guide_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    #Should already be in beam A this time, so don't nod
    $X = $wrap_x_y[0] + $x_nudge; 
    $Y = $wrap_x_y[1] + $y_nudge;
    if( $VIIDO ){ 
      print "Probe $probe_name follow on\n";
      probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      guideOff( $soc );
      print "handset by $X, $Y, back to position $startpos in beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      setHandset_radec( $soc, $X, $Y );
      handset_wait();
      print "guide on\n";
      guideOn( $soc );
      guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "***handset by $X, $Y, back to position $startpos in beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; handset_wait();
	   print "***guide on\n";
	   print "***"; guide_wait();
	 }

  }

  
  print "Should image $d_rptpos times here\n\n";
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  set_filename_header_param( $filebase, $next_index, $header);
  my $this_pos; my $nod_alpha; my $nod_delta;
  for( my $i = $startpos; $i <= $last_pos_num; $i+=2 ){
    #------------------Nod to sky beam---------------
    #-------------first position in sky beam---------
    ( $nod_alpha, $nod_delta ) = 
      compute_nod_pos_wrt_origin( $rel_offset_x[$i], $rel_offset_y[$i]);

    $X_origin = $nod_alpha + $x_nudge; 
    $Y_origin = $nod_delta + $y_nudge;
    write_rel_offset_to_header( $header, $X_origin, $Y_origin );
    if( $VIIDO ){ 
      print "m1Corrections off\n";
      m1CorrectionsOff( $soc );
      print "mountguide off\n";
      mountGuideOff( $soc );
      print "guide off\n";
      guideOff( $soc );
      print "Probe $probe_name follow off\n";
      probe_follow_off( $soc, $probe_name );
      print "probeGuideConfig off\n";
      probeGuideConfig( $soc, $probe_name, "off" );
      print "Nodding to position $i, beam=B\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      nodbeam( $soc, "B" );
      nod_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***m1CorrectionsOff\n";
	   print "***mountguide off\n";
	   print "***guide off\n";
	   print "***Probe $probe_name follow off\n";
	   print "***probeGuideConfig off\n";
	   print "***Nodding to position $i, beam=B\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; nod_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    set_filename_header_param( $filebase, $next_index, $header);
    #-------------second position in sky beam---------    
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];

    ( $nod_alpha, $nod_delta ) = 
      compute_nod_pos_wrt_origin( $rel_offset_x[$this_pos], $rel_offset_y[$this_pos]);

    $X_origin = $nod_alpha + $x_nudge; 
    $Y_origin = $nod_delta + $y_nudge;
    write_rel_offset_to_header( $header, $X_origin, $Y_origin );
    if( $VIIDO ){ 
      print "handset to position $this_pos: $X, $Y, beam=B\n";
      print "position relative to origin:  $X_origin, $Y_origin\n";
      setHandset_radec( $soc, $X, $Y );
      handset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***handset to position $this_pos: $X, $Y, beam=B\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; handset_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    set_filename_header_param( $filebase, $next_index, $header);
    #------------------Nod to object beam---------------
    #-------------first position in object beam---------
    $X_origin = $rel_offset_x[$this_pos] + $x_nudge; 
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;
    write_rel_offset_to_header( $header, $X_origin, $Y_origin );
    if( $VIIDO ){
      print "Nodding to position $this_pos, beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      nodbeam( $soc, "A" );
      print "probeGuideConfig on\n";
      probeGuideConfig( $soc, $probe_name, "on" );
      print "Probe $probe_name follow on\n";
      probe_follow_on( $soc, $probe_name );
      probe_wait();
      print "guide on\n";
      guideOn( $soc );
      guide_wait();
      print "mountguide on\n";
      mountGuideOn( $soc );
      print "m1Corrections on\n";
      m1CorrectionsOn( $soc );
    }else{ print "***Should be doing the following:\n";
	   print "***Nodding to position $this_pos, beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***probeGuideConfig on\n";
	   print "***Probe $probe_name follow on\n";
	   print "***"; probe_wait();
	   print "***guide on\n";
	   print "***"; guide_wait();
	   print "***mountguide on\n";
	   print "***m1Correctoins\n";
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    set_filename_header_param( $filebase, $next_index, $header);
    #-------------second position in object beam---------    
    $this_pos = $i+2;
    if( $this_pos <= $last_pos_num ){
      $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];
      $X_origin = $rel_offset_x[$this_pos] + $x_nudge; 
      $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;
      write_rel_offset_to_header( $header, $X_origin, $Y_origin );
      if( $VIIDO ){ 
	print "handset to position $this_pos: $X, $Y, beam=A\n";
	print "position relative to first position:  $X_origin, $Y_origin\n";
	print "guide off\n";
	guideOff( $soc );
	setHandset_radec( $soc, $X, $Y );
	handset_wait();
	print "guide on\n";
	guideOn( $soc );
	guide_wait();
      }else{ print "***Should be doing the following:\n";
	     print "***handset to position $this_pos: $X, $Y, beam=A\n";
	     print "***position relative to first position:  $X_origin, $Y_origin\n";
	     print "***"; handset_wait();
	   }
      print "Should image $d_rptpos times here\n\n";
      $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		     $file_hint, $next_index, $net_edt_timeout, $use_tcs);

      set_filename_header_param( $filebase, $next_index, $header);
    }
  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){ 
    print "offset to origin in beam=A\n";
    print "guide off\n";
    guideOff( $soc );
    setOffset_radec( $soc, 0, 0 );
    offset_wait();
    print "guide on\n";
    guideOn( $soc );
    guide_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin in beam A\n";
	 print "***guide off\n";
	 print "***"; offset_wait();
	 print "***guide on\n";
	 print "***"; guide_wait();
       }


####SUBS
sub load_pattern_x{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_X;
    
  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    $ret_ar_ref = \@pat_3x3_rel_X;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_X;
    
  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_X;
    
  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_X;
    
  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_X;
  }
  
  return $ret_ar_ref;
}#Endsub load_pattern_x


sub load_pattern_y{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_rel_Y;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_Y;
  }

  return $ret_ar_ref;

}#Endsub load_pattern_y


sub load_pat_rel_Xorigin{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_Xorigin;
    
  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    $ret_ar_ref = \@pat_3x3_rel_Xorigin;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_Xorigin;
    
  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_Xorigin;
    
  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_Xorigin;
    
  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_Xorigin;
  }
  
  return $ret_ar_ref;
}#Endsub load_pat_rel_Xorigin


sub load_pat_rel_Yorigin{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_Yorigin;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_rel_Yorigin;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_Yorigin;
  }

  return $ret_ar_ref;

}#Endsub load_pat_rel_Yorigin


sub load_pattern_wrap{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_wrap_X_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_wrap_X_Y;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_wrap_X_Y;
  }

  return $ret_ar_ref;

}#Endsub load_pattern_wrap


sub scale_pattern{
  my ( $ar_x, $ar_y ) = @_;

  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $extra_dither_scale   = param_value( 'D_SCALE' );
  my $net_factor = $default_dither_scale * $extra_dither_scale;

  print "\n\nApplying extra scale factors for $ENV{THIS_TELESCOP}:\n";
  print "-----------------------------------------------\n";
  print "Default dither scale    = $default_dither_scale\n";
  print "Extra dither scale      = $extra_dither_scale\n";
  print "Net picture frame width = ".($net_factor*90)." arcseconds\n\n";

  my $pat_len = @$ar_x;
  #print "$pat_len\n";
  for( my $i = 0; $i < $pat_len; $i++ ){
    $$ar_x[$i] = $$ar_x[$i] * $net_factor;
    $$ar_y[$i] = $$ar_y[$i] * $net_factor;
    #print "$i:\t$$ar_x[$i]\t$$ar_y[$i]\t$net_factor\n";
  }

}#Endsub scale_pattern


sub compute_rel_offsets{
  my $ar_vec = $_[0];
  my @vector = @$ar_vec;

  my $first_pos = $vector[0];
  my @output_vector; $output_vector[0] = 0;
  my $vec_len = @vector;
  for( my $i = 1; $i < $vec_len; $i++){
    $output_vector[$i] = $first_pos - $vector[$i];
    #print "$i: $output_vector[$i]\n";
  }
  
  return \@output_vector;
}#Endsub compute_rel_offsets

sub scale_pattern_wrap{
  my  $ar_wrap = $_[0];

  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $extra_dither_scale   = param_value( 'D_SCALE' );
  my $net_factor = $default_dither_scale * $extra_dither_scale;

  $$ar_wrap[0] = $$ar_wrap[0] * $net_factor;
  $$ar_wrap[1] = $$ar_wrap[1] * $net_factor;

}#Endsub scale_pattern_wrap


sub prompt_setup{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "Please have the SSA start guiding on the source pointing center\n";
  print "(no offsets) and set up in NOD BEAM=A\n\n";

  print "Are you ready?  Enter y to continue; n to exit.  ";
  my $ready = get_yn();
  if( !$ready ){
    die "\n\nExiting\n\n";
  }

}#Endsub prompt_setup


sub query_probe_name{
  print "Please tell me what guide probe you are using.\n";
  print "This code knows of the following probes:\n";
  print "Probe #\tProbe Name\n";
  print "  1    \t   p1\n";
  print "  2    \t   p2\n\n";
  print "Enter 1 or 2: ";

  my $probe_num;
  my $probe_name;
  my $is_valid = 0;
  until( $is_valid ){
    my $input = <STDIN>; chomp $input;
    $probe_num = $input;
    
    if( $probe_num == 1 or $probe_num == 2 ){
      $is_valid = 1;
      if( $probe_num == 1 ){
	$probe_name = "p1";
      }else{
	$probe_name = "p2";
      }
    }else{
      print "Please enter 1 or 2 ";
    }
  }
  print "Probe name is $probe_name\n\n";

  return $probe_name;

}#Endsub query_probe_name


sub setup_nod{
  my $soc = $_[0];
  my $throw    = param_value( 'THROW'    );
  my $throw_pa = param_value( 'THROW_PA' );

  ufgem::nodConfig( $soc, $throw, $throw_pa );

}#Endsub setup_nod


sub compute_nod_pos_wrt_origin{
  my ($alpha_offset, $dec_offset) = @_;
  my $throw    = param_value( 'THROW'    );
  my $throw_pa = param_value( 'THROW_PA' );
  use Math::Trig;

  my $pa_rad = deg2rad( $throw_pa );
  my $d_alpha = $throw * sin( $pa_rad );
  my $d_delta = $throw * cos( $pa_rad );

  my $net_alpha_offset = $alpha_offset + $d_alpha;
  my $net_dec_offset   = $dec_offset   + $d_delta;

  return( $net_alpha_offset, $net_dec_offset );
}#Endsub compute_nod_pos_wrt_origin

sub use_tcs{
  my ( $use_tcs, $header ) = @_;

  if( $use_tcs ){
    ####call to tcs subroutines goes here
    ####Must match all 18 characters in header
    if( $telescop eq 'Gemini-South      ' ){
      getGemTCSinfo( $header );
    }
  }else{
    print "___NOT___ getting tcs info\n\n";
  }

}#Endsub use_tcs


sub probe_follow_on{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOn( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOn( $soc );
  }

}#Endsub probe_follow_on


sub probe_follow_off{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOff( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOff( $soc );
  }

}#Endsub probe_follow_off


sub nudge_pointing{
  my ( $d_rptpat, $rpt_pat ) = @_;
  my $rndm_num = rand;
  my $rdx = 0; my $rdy = 0; 

  if( param_value( 'USENUDGE' ) == 1 ){
    if( $rpt_pat >= 1 ){
      if( 0 <= $rndm_num and $rndm_num < 0.25 ){
	$rdx = 0; $rdy = 1;
      }elsif( 0.25 <= $rndm_num and $rndm_num < 0.5 ){
	$rdx = 1; $rdy = 0;
      }elsif( 0.50 <= $rndm_num and $rndm_num < 0.75 ){
	$rdx = 0; $rdy = -1;
      }elsif( 0.75 <= $rndm_num and $rndm_num <= 1.0 ){
	$rdx = -1; $rdy = 0;
      }
    }
    my $nudge = param_value( 'NUDGESIZ' );
    $rdx = $rdx * $nudge;
    $rdy = $rdy * $nudge;
  }
  print "\nNudge offsets by $rdx, $rdy\n\n";
  return( $rdx, $rdy );
}#Endsub nudge_pointing


sub write_rel_offset_to_header{
  my ( $header, $xoffset, $yoffset ) = @_;

  setNumericParam( 'XOFFSET', $xoffset );
  setNumericParam( 'YOFFSET', $yoffset );
  
  print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  writeFitsHeader( $header );
  print "\n\n";

}#Endsub write_rel_offset_to_header


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    use_tcs( $use_tcs, $header );
    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);
    
    print "acq cmd is \n$acq_cmd\n\n";
    acquire_image( $acq_cmd );

    idle_lut_status( $expt, $nreads );
    #move_file_for_index_sep( $file_hint, $next_index );#Not used

    my $unlock_param = param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	  "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n\n";
	print "IF it looks like it has stalled while applying the LUT,\n";
	print "run ufstop.pl -clean, and start over\n\n";
	print "Else if it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";
	exit;
      }#end time expired      
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub set_filename_header_param{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};
  
  my $index_str = parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  print "setting FILENAME to $filename\n";
  setFilenameParam( $filename );

}#Endsub set_filename_header_param
