#!/usr/bin/perl -w
use Config;
my $rcsIq = '$Name:  $ $Id: sighandler.pl 14 2008-06-11 01:49:45Z hon $';

defined $Config{sig_name} || die "No sigs?";

sub sighandler {
  my $sig = shift;
  print "sighandler> recv'd sig: $sig\n";
  if( $sig eq "INT" ) { 
    my $u = "n";
    print "exit? [n]: ";
    # note that getc returns "\n" if one simply hits enter key
    # but if one hits any other key followed by enter, getc return the single key char
    $u = getc; #print "you entered: \"$u\"\n";
    if( $u eq "y") { exit; }
  }
}

my $name = "ZERO";    
foreach $name (split(' ', $Config{sig_name})) {
  print "set sighandler for sig: $name\n";
  if( $name ne "ZERO" ) { $SIG{$name} = \&sighandler; }
}

my $true = 1;
while( $true ) {
  print "waiting for signal...\n";
  sleep 1;
}

