#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $val;
my $cnt = @ARGV;
if( $cnt < 1 ) {
  $val = ufgem::getStatus($soc); # get All

}else {
  my $stat = shift;
  $val = ufgem::getStatus($soc, $stat);
}

print "$val";

close($soc);
