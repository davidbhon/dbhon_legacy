#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage:  gem.nodbeam.pl A|B\n\n";

}else{
  my $beam = shift;

  if( $beam =~ m/(a|b)/i ){
    my $soc = ufgem::viiconnect();
    print "Going to beam = $beam\n";
    ufgem::nodbeam($soc, $beam);
    close( $soc );

  }else{
    die "\n\n\tPlease enter gem.nodbeam.pl a or ".
        "gem.nodbeam.pl b\n\n";
  }

}#End


