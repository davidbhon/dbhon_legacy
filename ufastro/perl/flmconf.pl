#!/usr/local/bin/perl
#my $rcsId = '$Name:  $$Id: flmconf.pl 14 2008-06-11 01:49:45Z hon $';

use strict;
#use CGI;
#use CGI::Carp qw( fatalsToBrowser );
#my $q = new CGI;

my $ufclient = "env LD_LIBRARY_PATH=/usr/local/lib:/opt/EDTpdv /usr/local/uf/sbin/ufmotor -q ";
my $ufhost = "-host trifid " ; # assume agent is running on local host
#my $ufhost = "-host flamingos1b ";
my $ufport = "-port 52007 ";
#my $ufport = "-port 52004 ";
my $ufmotor = $ufclient . $ufhost . $ufport . "-raw "; # full client invocation
my $ufsleep = "env LD_LIBRARY_PATH=/usr/local/lib:/opt/EDTpdv /usr/local/uf/sbin/ufsleep 0.05";
my $reply = "";
my $flam = "";
my $slp = "";

# options:
my $abort = "";
my $command = "";
my $home = "";
my $status = "";
my $imgOrdark = "";
my $mos = "";
my $slit = "";
my $filter = "";
my $lyot = "";
my $grism = "";
my $monitor = "";

my %args = @ARGV; # assuming all options are provided in pairs, create hash args
$abort = $args{'-abort'};
$home = $args{'-home'};
$command = $args{'-cmd'};
$status = $args{'-stat'};
$imgOrdark = $args{'-conf'};
$mos = $args{'-mos'};
$slit = $args{'-slit'};
$filter = $args{'-filt'};
$lyot = $args{'-lyot'};
$grism = $args{'-grism'};
$monitor = $args{'-mon'};

if( !$command && !$status && !$imgOrdark && !$mos && !$slit && 
    !$filter && !$lyot && !$grism && !$abort & !$monitor & !$home ) {
  &usage;
  exit 1;
}

my $output = "Sorry, ambiguous request, please reset & try again.\n";

if( $command ) {
  $output = raw_cmd( $command ); # execute raw/uninterpretted command
}

if( $abort ) {
  $output = abort_motion( $abort ); # abort motion on specified motor/indexor
}

if( $status ) {
  $output = query_status( $status ); # all or specific indexor a,b,c,d,e
}

if( $monitor ) {
  $output = "Monitoring/Polling motion of: $monitor";
}

if( $home ) {
  $monitor = $home;
  $output = $output . home_motors($home);
}

if( $imgOrdark ) {
  $output = config( $imgOrdark ); # image or dark
  if( $imgOrdark eq "image" ) {
    $monitor = "a b e";
  }
  elsif( $imgOrdark eq "dark" ) {
    $monitor = "a c d e";
  }
}

if( $mos ) {
  my %MOS = ("1"=>"1540", "2"=>"2440", "3"=>"3320", "4"=>"4220", "5"=>"5110", "6"=>"6000", 
             "7"=>"6890", "8"=>"7781", "9"=>"8671", "10"=>"9562", "11"=>"10452" );
  $output = mos_select( $MOS{$mos} );
  $monitor = "a b";
}

if( $slit ) {
  my %SLIT = ("1p"=>"10007", "2p"=>"1083", "3p"=>"10933", "4p"=>"9117", "6p"=>"8226",
	      "12p"=>"7336");
  $output = slit_select( $SLIT{$slit} );
  $monitor = "a b";
}

if( $filter ) {
  my %FILT = ("j"=>"0", "h"=>"208", "k"=>"417", "hk"=>"625", "jh"=>"833", "dark"=>"1042");
  $output = filter_select( $FILT{$filter} );
  $monitor = "c";
}

if( $lyot ) {
  my %LYOT = ("open"=>"357", "dark1"=>"0",  "dark2"=>"178", "dark3"=>"1074", 
	      "2.1m"=>"1071", "4m"=>"893", "mmt"=>"714", "gem"=>"536", 
	     );
  $output = lyot_select( $LYOT{$lyot} );
  $monitor = "d";
}

if( $grism ) {
  my %GRISM = ("hk"=>"0", "open"=>"250", "dark1"=>"500", "dark2"=>"750", "dark3"=>"1000");
  $output = grism_select( $GRISM{$grism} );
  $monitor = "e";
}

#$output =~ s/\r//g;
#$output =~ s/\n/<br>/g;

#print <<EOF;
#$output
#EOF


if( $monitor eq "" ) {
  print "$output\n";
  exit;
}
else {
  monitor_motion($monitor);
}

###################### flamingos functions ########################

sub usage {
  print "usage:\
flmconf.pl [-abort \"a/b/c/d/e/all\"]\
           [-mon \"a/b/c/d/e/all\"]\
           [-home \"a/b/c/d/e/all\"]\
           [-cmd \"raw command\"]\
           [-stat a/b/c/d/e/all]\
           [-conf image/dark]\
           [-mos 1/2/3/.../11]\
           [-slit 1p/2p/3p/4p/6p/12p]\
           [-filt j/h/k/hk/jh/dark]\
           [-lyot 2.1m/4m/mmt/gem/dark1/dark2/dark3]\
           [-grism hk/open/dark1/dark2/dark3]\n";
}

sub submit {
# this is useful for insuring a sleep after each request...
  my ( $req ) = @_;
  #print "submit: $req\n";
  my $req_reply = `$req`;
  my $slp = `$ufsleep`;
  return $req_reply;
}

sub raw_cmd {
  my ( $cmd ) = @_;
  print "$cmd\n";
  my $cmd_reply = ""; # "$cmd\n";
  $flam = submit("$ufmotor \"$cmd\"");
  $cmd_reply = $cmd_reply.$flam;

  return $cmd_reply;
}

sub query_status { 
# "all" or "a b c d e" or "a c e", etc.
  my ( $query ) = @_;
  my $q_reply = ""; # "$query\n";

  my $m = "";
  my $pos = "";
  my $motion = "";
#  my @motors = ("a", "b", "c", "d", "e");
  my @motors = split / /,$query;
  foreach $m(@motors) {
    $q_reply = $q_reply . "\nMotor/Indexor $m: ";
    $pos = submit("$ufmotor \"$m Z\""); # steps from home
    $motion = submit("$ufmotor \"$m ^\""); # moving?
    $flam = submit("$ufmotor \"$m [ 1990 23\""); # configuration status
    $q_reply = $q_reply.parse_motion( $motion ).parse_pos( $pos )." ...\n".parse_status( $flam )."\n";
  }
  return $q_reply;    
}

sub parse_status {
  my ( $stat ) = @_;
  #print "$stat\n";
  my $stat_reply = ""; # "$stat\n";
  my @list = split / /,$stat;
  my $elem = "";
  foreach $elem(@list) {
    print "$elem, ";
  }
  # get the values for the lo and hi byte of the Initial Velocity
  my $ivlo = $list[20];
  my $ivhi = $list[21];
  # get the valuse for the lo and hi byte of the Slew Velocity
  my $svlo = $list[25];
  my $svhi = $list[26];
  print "ivlo= $ivlo, ivhi= $ivhi, svlo= $svlo, svhi= $svhi\n";
  # calculate Initial Velocity
  my $ivel = 14749000.0/(12.0*(65535.0-($ivhi*256.0+$ivlo)));
  # calculate the Slew Velocity
  my $svel = 14749000.0/(12.0*(65535.0-($svhi*256.0+$svlo)));
  $stat_reply = "Acc. $list[4], ";
  $stat_reply = $stat_reply . "Dec. $list[5], ";
  $stat_reply = $stat_reply . "Hold $list[10], ";
  $stat_reply = $stat_reply . "Run $list[11], ";
  $stat_reply = $stat_reply . "Init. Vel. $ivel, ";
  $stat_reply = $stat_reply . "Slew Vel. $svel.";
  
  return $stat_reply;
}

sub parse_pos {
  my ( $pos ) = @_; 
  #print "$pos\n";
  my $pos_reply = ""; # "$pos\n";
  my @list = split / /,$pos; # split space separated single-string list into array/list of names
  my $pos_reply = $pos_reply . "Steps from Home: $list[2]";
  $pos_reply =~ s/\n//g;
  return $pos_reply;
}

sub parse_motion {
  my ( $motion ) = @_;  
  my $mot_reply = ""; # "$motion\n";

  my @list = split / /,$motion;
  if( $list[2] == 0 ) {
    $mot_reply = $mot_reply . "Stationary, ";
  }
  else {
    $mot_reply = $mot_reply . "Moving, ";
  }
  return $mot_reply;
}

sub config {
# "image" or "dark"
  my ( $imgOrdark ) = @_;
  my $conf_reply = "Unknown Configuration selection: $imgOrdark\n";
# if limit switch fails, it will be a problem to send origin cmd while motor is still moving
#  my $imgcmd = "$ufmotor \"a F 100& a O& b F 100& b O& e F 10 & e O & a +2250& e +250\"";
#  my $drkcmd = "$ufmotor \"a F 100& a O& c F 100& c O& d F 100& d O& e F 100& e O& c +1040 &e +500\"";
  my $imgcmd = "$ufmotor \"a +2250& e +250\"";
  my $drkcmd = "$ufmotor \"c +1040 &e +500\"";
  if( $imgOrdark eq "image" ) {
    home_motors("a b e");
    $conf_reply = submit($imgcmd);
  }
  elsif( $imgOrdark eq "dark" ) {
    home_motors("a c d e");
    $conf_reply= submit($drkcmd);
  }
  return $conf_reply;
}

sub mos_select {
  my ( $mos ) = @_;
  my $mos_reply = "Unknown MOS selection: $mos\n";
  home_motors("a b");
  my $moscmd = "$ufmotor \"";
  if( $mos eq "1540" || $mos eq "2440" || $mos eq "3320" || $mos eq "4220" ||
      $mos eq "5110" || $mos eq "6000" || $mos eq "6890" || $mos eq "7781" ||
      $mos eq "8671" || $mos eq "9562" || $mos eq "10452" ) {
    $moscmd = $moscmd . "b +$mos\"";
    $mos_reply = submit($moscmd);
  }

  return $mos_reply;
}

sub slit_select {
# slit 2Pix == "1083", 3Pix == "10933", 1Pix == "10007", 4Pix == "9117",
# slit 6Pix == "8226", 12Pix == "7336"
  my ( $slit ) = @_;
  my $slit_reply = "Unknown Slit selection: $slit\n";
  my $slitcmd = "$ufmotor \"a F 100& a O& & b F 100 0& b O & a +6750 & ";
  if( $slit eq "1083" || $slit eq "10933" || $slit eq "10007" ||
      $slit eq "9117" || $slit eq "8226" || $slit eq "7336" ) {
    $slitcmd = $slitcmd . "b +$slit\"";
    $slit_reply=  submit($slitcmd);
  }
  return $slit_reply;
}

sub filter_select {
# filt J == "0", H == "208", K == "416", HK == "625", JH == "833", Dark == "1042"
  my ( $filt ) = @_;
  my $filt_reply = "Unknown Fliter selection: $filt\n";
  home_motors("c");
  my $filtcmd = "$ufmotor \"";
  if( $filt eq "0" || $filt eq "208" || $filt eq "416" ||
      $filt eq "625" || $filt eq "833" || $filt eq "1042" ) {
    $filtcmd = $filtcmd . "c +$filt\"";
    $filt_reply=  submit($filtcmd);
  }

  return $filt_reply;
}

sub lyot_select {
# lyot Dark == "0", 2.1m == "178", 4m == "357", MMT == "536",
# lyot Gemini == "714", Dark2 == "893", Dark3 == "1071"
  my ( $lyot ) = @_;
  my $lyot_reply = "Unknown Lyot selection: $lyot";
  home_motors("d");
  my $lyotcmd = "$ufmotor \""; 
  if( $lyot eq "0" || $lyot eq "178" || $lyot eq "357" ||
      $lyot eq "536" || $lyot eq "714"  || $lyot eq "893" || $lyot eq "1071" ) {
    $lyotcmd = $lyotcmd . "d +$lyot\"";
    $lyot_reply = submit($lyotcmd);
  }

  return $lyot_reply;
}

sub grism_select {
# grism HK == "0", Open == "250", Dark == "500", Dark1 == "750", Dark2 == "1000"
  my ( $grism ) = @_;
  my $grism_reply = "Unknown Grism selection: $grism\n";
  home_motors("e");
  my $grismcmd = "$ufmotor \"";
  if( $grism eq "0" || $grism eq "250" || $grism eq "500" ||
      $grism eq "750" || $grism eq "1000" ) {
    $grismcmd = $grismcmd . "e +$grism\"";
    $grism_reply = submit($grismcmd);
  }

  return $grism_reply;
}

sub abort_motion {
# actually sends "stop" directive
  my ( $arg ) = @_;
  my $cmd = "";
  my $abort_reply = "";
  my @motors = split / /,$arg;
  my $m = "";
  my $abrtcmd = "$ufmotor \"";
  foreach $m(@motors) {
    $abrtcmd = $abrtcmd . "$m @ & ";
  }
  # should eliminate the last '&' and replaced it with end quote:
  $abrtcmd =~ s/ \& $/\"/;
  #$abrtcmd = substr($abrtcmd, 0, rindex($abrtcmd, "& ")-1);
  #$abrtcmd = $abrtcmd . "\"";
  $abort_reply = submit($abrtcmd);
  return $abort_reply;
}

sub home_motors {
# send home cmd, monitor motion, send origin cmd after motion ceases
  my ( $arg ) = @_;
  my $cmd = "";
  my $home_reply = "";
  my @motors = split / /,$arg;
  print "Home/Datum: $arg\n";
  my @stationary;
  my @moving;
  my $m = "";
  my $homecmd = "$ufmotor \"";
  foreach $m(@motors) {
    $homecmd = $homecmd . "$m F 100& ";
  }
  # should eliminate the last '&' and replaced it with end quote:
  $homecmd =~ s/\& $/\"/;
  $home_reply = submit($homecmd);
  # origin motors after motion ends:
  my @moving = query_motion($arg);
  my $mvcnt = @moving;
  my $qcnt = 3; # some reasonable number of max. motion queries
  while( $qcnt-- > 0 && $mvcnt > 0 ) {
    @moving = query_motion($arg);
    $mvcnt = @moving;
  }
  if( $qcnt <= 0 && $mvcnt > 0 ) {
    print "?error homing motors: @moving, abort/stop motion...\n";
    my $stopcmd = "$ufmotor \"";
    foreach $m(@moving) {
      $stopcmd = $stopcmd . "$m @& ";
    }
    $stopcmd =~ s/\& $/\"/;
    $home_reply = $home_reply .  submit($stopcmd);
  }
  my @stationary = query_stationary($arg);
  my $origcmd = "$ufmotor \"";
  foreach $m(@stationary) {
    $origcmd = $origcmd . "$m O& ";
  }
  $origcmd =~ s/\& $/\"/;
  $home_reply = $home_reply . submit($origcmd);
  return $home_reply;
}

sub monitor_motion {
  my ( $arg ) = @_;
  my $m = "";
  my @motors = split / /, $arg;
  my $motcnt = @motors;
  my $moncmd = "$ufmotor \"";
  foreach $m(@motors) {
    $moncmd = $moncmd . "$m ^ & $m Z&";
  }
  # should eliminate the last '&' and replaced it with end quote:
  $moncmd =~ s/Z\&$/Z\"/;
  my $inmotion = "true";
  my $moncnt = 0;
  my $motion = "";
  my @mpairlist;
  my $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
  print "Monitor motion: $arg $date \n";
  my $cnt = 0;
  my @stationarylist;
  my @movinglist;
  while( $inmotion eq "true" ) {
    $motion = submit($moncmd);
    $motion =~ s/\n\n/, /g; $motion =~ s/ \^//g; $motion =~ s/Z//g;
    @mpairlist = split /\,/, $motion; # should have 2*motcnt entrees
    while( $cnt < $motcnt ) {
      $motion = @mpairlist[2*$cnt];
      if( rindex($motion,"10") >= 0 ) { # this motor is still moving
        push @movinglist, @mpairlist[1+2*$cnt];
      }
      else {
        push @stationarylist, @mpairlist[1+2*$cnt];
      }
      $cnt++;
    }
    $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
    print "$date -- Stationary: @stationarylist\n";
    print "$date -- Moving: @movinglist\n";
    $moncnt = @movinglist;
    if( $moncnt == 0 ) {
      $inmotion = "false";
    }
  }
}

sub query_motion {
  my ( $arg ) = @_;
  my $m = "";
  my @motors = split / /, $arg;
  my $motcnt = @motors;
  my $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
  print "Query motion: $arg $date \n";
  my $cmd = "$ufmotor \"";
  foreach $m(@motors) {
    $cmd = $cmd . "$m ^ &";
  }
  # should eliminate the last '&' and replaced it with end quote:
  $cmd =~ s/\&$/\"/;
  my $inmotion = "true";
  my $moncnt = 0;
  my $motion = "";
  my @mpairlist;
  my $cnt = 0;
  my @stationarylist;
  my @movinglist;
  $motion = submit($cmd);
  $motion =~ s/\n\n/, /g; $motion =~ s/ \^//g;
  @mpairlist = split /\,/, $motion; # should have motcnt entrees
  while( $cnt < $motcnt ) {
    $motion = @mpairlist[$cnt];
    if( rindex($motion,"10") >= 0 ) { # this motor is still moving
      push @movinglist, @mpairlist[$cnt];
    }
    else {
      push @stationarylist, @mpairlist[$cnt];
    }
    $cnt++;
  }
  $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
  print "$date -- Stationary: @stationarylist\n";
  print "$date -- Moving: @movinglist\n";
  return @movinglist;
}

sub query_stationary {
  my ( $arg ) = @_;
  my $m = "";
  my @motors = split / /, $arg;
  my $motcnt = @motors;
  my $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
  print "Query stationary: $arg $date \n";
  my $cmd = "$ufmotor \"";
  foreach $m(@motors) {
    $cmd = $cmd . "$m ^ &";
  }
  # should eliminate the last '&' and replaced it with end quote:
  $cmd =~ s/\&$/\"/;
  my $inmotion = "true";
  my $moncnt = 0;
  my $motion = "";
  my @mpairlist;
  my $cnt = 0;
  my @stationarylist;
  my @movinglist;
  $motion = submit($cmd);
  $motion =~ s/\n\n/, /g; $motion =~ s/ \^//g;
  @mpairlist = split /\,/, $motion; # should have motcnt entrees
  while( $cnt < $motcnt ) {
    $motion = @mpairlist[$cnt];
    if( rindex($motion,"10") >= 0 ) { # this motor is still moving
      push @movinglist, @mpairlist[$cnt];
    }
    else {
      push @stationarylist, @mpairlist[$cnt];
    }
    $cnt++;
  }
  $date = `date "+%Y:%j:%H:%M:%S"`; $date =~ s/\n//;
  print "$date -- Stationary: @stationarylist\n";
  return @stationarylist;
}
