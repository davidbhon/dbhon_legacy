#!/usr/local/bin/perl -w

my $rcsId = q($Id: config.dither.pl 14 2008-06-11 01:49:45Z hon $);

###############################################################################
#
#Configure fits header parameters for next dither sequence
#
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\n>>> Present exposure parameters are: <<<\n";

#printFitsHeader( "DPATTERN", "D_SCALE", 
#		 "D_RPTPOS",  "D_RPTPAT", "STARTPOS" );#Start pos to be implemented later

printFitsHeader( "DPATTERN", "D_SCALE", 
		 "D_RPTPOS",  "D_RPTPAT" );

print "\n";
printFitsHeader( "USENUDGE", "NUDGESIZ" );
print "\n";
#printFitsHeader( "NOD_SKY", "THROW", "THROW_PA" );#Nod_sky not used
printFitsHeader( "THROW", "THROW_PA" );
print "___________________________________________________\n\n";

my $didmod = 0;
my $redo = 0;

print "Change anything? ";
my $change = get_yn();
if( $change ){
  until( $redo ){
    my $pattern = select_from_known_patterns();
    if( $pattern eq 0 ){
      #Didn't pick a new pattern
      $didmod = 0;
    }else{
      setStringParam( "DPATTERN", $pattern );
      $didmod += 1;
      print "pattern = $pattern; didmod = $didmod\n";
    }

    my $changed_dscale = query_dscale_value();
    print "changed_dscale = $changed_dscale\n";
    if( $changed_dscale > 0 ){
      setNumericParam( "D_SCALE", $changed_dscale );
      $didmod += 1;
    }

    #my $mod_num_params = 
    #  modNumericParams( "D_RPTPOS", "D_RPTPAT", "STARTPOS",
    #    		"USENUDGE", "NUDGESIZ", "NOD_SKY", "THROW",
    #   	        "THROW_PA");
    my $mod_num_params = 0;

    my $telescop = param_value( 'TELESCOP' );    
    if( $telescop eq 'Gemini-South      ' ){
      $mod_num_params = modNumericParams( "D_RPTPOS", "D_RPTPAT",
					  "USENUDGE", "NUDGESIZ", "THROW"); 
      
      my $tmp =  mod_THROW_PA_for_gem();
      $mod_num_params = $mod_num_params + $tmp;
    }else{
      my $mod_num_params = 
	modNumericParams( "D_RPTPOS", "D_RPTPAT",
			  "USENUDGE", "NUDGESIZ", "THROW",
			  "THROW_PA");
    }
    
    if( $mod_num_params > 0 ){
      $didmod += 1;
    }
    
    if( $didmod > 0 ){
      print "\n\nThe new set of dither paramaters are:\n";
      #printFitsHeader( "DPATTERN", "D_SCALE", 
      #		       "D_RPTPOS",  "D_RPTPAT", "STARTPOS" );
      printFitsHeader( "DPATTERN", "D_SCALE", 
		       "D_RPTPOS",  "D_RPTPAT" );


      print "\n";
      printFitsHeader( "USENUDGE", "NUDGESIZ" );
      print "\n";
      #printFitsHeader( "NOD_SKY", "THROW", "THROW_PA" );
      printFitsHeader( "THROW", "THROW_PA" );
      print "___________________________________________________\n\n";
      print "\nAccept changes?";
      $redo = get_yn();
    }else{
      $redo = 1;
    }
  }
}elsif( !$change ){
  #do nothing
}


if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub query_dscale_value{
  my $response = 0;
  my $is_valid = 0;

  my $iparm = param_index( 'D_SCALE' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print("Enter a real value > 0 for the additional scale factor to apply\n ");

  until($is_valid){
    chomp ($response = <STDIN>);
    print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[0-9]{0,}[a-z,A-Z][0-9]{0,}/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a real value ";
      }elsif( $response =~ m/^\.$/ ){
	#print "$response is not valid\n";
	print "Please enter a real value ";
      }elsif( $response == 0 ){
	print "0 is not a valid scale factor\n";
      }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}
      }

    }else{
      print "Leaving D_SCALE unchanged\n";
      $response = 0;
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub query_dscale_value
