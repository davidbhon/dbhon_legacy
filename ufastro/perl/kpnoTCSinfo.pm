package kpnoTCSinfo;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use kpnoTCSinfo ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = 
  ( 'all' => [ qw(
		  &getKPNOtcsinfo
		 ) ]
  );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);
our $VERSION = '0.01';

# Preloaded methods go here.

sub getKPNOtcsinfo{
  my $header = $_[0];

  my $tcstel = $ENV{UF_KPNO_TCS_PROG};
  my $quote  = "\"";
  my $space  = " ";
  my $teleinfo = $tcstel.$space.$quote."tele info".$quote;

  my ($ibeg);
  my ($iend);
  my ($tinfo);

  if( ($ibeg = index( $teleinfo, "mean position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA",$RA);
    setStringParam("DEC",$DEC);
    if( ($ibeg = index( $tinfo, "(" )) >= 0 ){
      $iend = rindex( $tinfo, ")" );
      my $epoch = substr( $tinfo, $ibeg+1, $iend-$ibeg-1 );
      setNumericParam("EPOCH",$epoch);
    }
  }else{
    setStringParam( "RA","unkown");
    setStringParam("DEC","unkown");
  }

  if( ($ibeg = index( $teleinfo, "appar position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA_APR",$RA);
    setStringParam("DEC_APR",$DEC);
  }else{
    setStringParam( "RA_APR","unkown");
    setStringParam("DEC_APR","unkown");
  }

  if( ($ibeg = index( $teleinfo, "ha" )) >= 0 ){
    $iend = index( $teleinfo, ",", $ibeg );
    my $ha = substr( $teleinfo, $ibeg, $iend-$ibeg );
    $ha = substr( $ha, index($ha,':')+1, length($ha) );
    setStringParam("HA",$ha);
  }else{ 
    setStringParam("HA","unkown");
  }

  if( ($ibeg = index( $teleinfo, "utime/stime" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $UT, $ST ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam("UTC",$UT);
    setStringParam("LST",' '.$ST);
  }else{
    setStringParam("UTC","unkown");
    setStringParam("LST","unkown");
  }

  if( ($ibeg = index( $teleinfo, "airmass/zen.dist" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $AM, $ZD ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setNumericParam( "AIRMASS",$AM);
    setNumericParam("ZD",$ZD);
  }else{
    setNumericParam( "AIRMASS",0);
    setNumericParam("ZD",0);
  }
  my $date = `date -u '+%Y-%m-%d'`;
  chomp( $date );
  setStringParam("DATE",$date);

  my $time = `date -u '+%H:%M:%S'`;
  chomp( $time );
  setStringParam("TIME",$time);

  print "Writing KPNO $ENV{THIS_TELESCOP} TCS info into header\n";
  print "$header\n\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub getKPNOtcsinfo



# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

kpnoTCSinfo - Perl extension for Flamingos at KPNO

=head1 SYNOPSIS

  use kpnoTCSinfo qw/:all/;


=head1 DESCRIPTION


=head2 EXPORT

  &getKPNOtcsinfo()

=head1 AUTHOR

SNR 26 Oct 2001

=head1 SEE ALSO

perl(1).

=cut
