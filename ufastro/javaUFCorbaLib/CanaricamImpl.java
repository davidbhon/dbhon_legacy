import CIS.*;
import CIS.CanaricamPackage.*;
import javaUFProtocol.*;
import java.net.*;
import java.io.*;

public class CanaricamImpl extends CanaricamPOA {

    private String DCAgentHost = "kepler";
    private int DCAgentPort = 52008;
    private String DASHost = "kepler"; 
    private int DASPort = 52000;
    private Socket dcSocket;
    private Socket dasSocket;

    public void setDCAgentHost(String dcagenthost) {
	DCAgentHost = dcagenthost;
    }

    public void setDCAgentPort(int dcagentport) {
	DCAgentPort = dcagentport;
    }

    public void setDASHost(String dashost) {
	DASHost = dashost;
    }

    public void setDASPort(int dasport) {
	DASPort = dasport;
    }

    public void ping() {

    }

    public void abort() throws DAF.GCSException {

    }

    public void disable() throws DAF.GCSException {

    }
    
    public void enable() throws DAF.GCSException {

    }

    public void start() throws DAF.GCSException {

    }

    public String report (int detailLevel) throws DAF.GCSException {
	return "report";
    }

    public String operationalState() throws DAF.GCSException {
	return "operational";
    }

    public String name () throws DAF.GCSException {
	return "CANARICAM";
    }

    public String instrumentFITSHeader() throws DAF.GCSException {
	return "FITSHeader";
    }

    public String getProcessedDataFileName() throws DAF.GCSException {
	return "Filename";
    }

    public String getRawDataFileName() throws DAF.GCSException {
	return "Filename";
    }

    public boolean isObservationInProgress() throws DAF.GCSException {
	return false;
    }

    public void beamSwitchDone() throws DAF.GCSException {
	String [] strs = {"BEAMSWITCH","DONE"};
	UFStrings ufs = new UFStrings("CIS",strs);
	try {
	    if (_connectToDC()) {
		if (ufs.sendTo(dcSocket)<=0) {
		    if (dcSocket != null) dcSocket.close();
		    dcSocket = null;
		    _connectToDC();
		    if (ufs.sendTo(dcSocket)<=0) {
			if (dcSocket != null) dcSocket.close();
			dcSocket = null;
			throw new DAF.GCSException();
		    }
		}
		// receive reply
		UFStrings ufsr = (UFStrings) UFProtocol.createFrom(dcSocket);
		System.out.println(ufsr.toString());
	    } else {
		// try one more time
		if (dcSocket != null) dcSocket.close();
		dcSocket = null;
		if (_connectToDC()) {
		    if (ufs.sendTo(dcSocket)<=0) {
			if (dcSocket != null) dcSocket.close();
			dcSocket = null;
			_connectToDC();
			if (ufs.sendTo(dcSocket)<=0) {
			    if (dcSocket != null) dcSocket.close();
			    dcSocket = null;
			    throw new DAF.GCSException();
			}
		    }
		    // receive reply
		    UFStrings ufsr = (UFStrings) UFProtocol.createFrom(dcSocket);
		    System.out.println(ufsr.toString());		    
		} else {
		    System.err.println("CanaricamImpl::beamSwitchDone> Connection to DCAgent failed");
		    throw new DAF.GCSException();
		}
	    }
	} catch (Exception e) {
	    System.err.println(e.toString());
	    throw new DAF.GCSException();
	} 
    }

    private  boolean _connectToDC() {
	if (dcSocket == null) {
	    try {
		dcSocket = new Socket(DCAgentHost,DCAgentPort);
		UFTimeStamp uft = new UFTimeStamp("CIS");
		uft.sendTo(dcSocket);
		UFProtocol.createFrom(dcSocket);
	    } catch (Exception e) { dcSocket = null; return false; }
	} 
	return true;
    }

    public FrameConfig getFrameConfig() throws DAF.GCSException {
	try {
	    Socket s = new Socket(DASHost,DASPort);
	    UFTimeStamp uft = new UFTimeStamp("FC");
	    uft.sendTo(s);
	    //UFProtocol.createFrom(s);
	    // receive reply (??)
	    UFFrameConfig uffc = (UFFrameConfig)UFProtocol.createFrom(s);
	    System.out.println(uffc.toString());
	    s.close();
	    FrameConfig fc = new FrameConfig();
	    fc.w = uffc.width;
	    fc.h = uffc.height;
	    fc.d = uffc.depth;
	    fc.DMAcnt = uffc.DMAcnt;
	    fc.frameGrabCnt = uffc.frameGrabCnt;
	    fc.frameObsSeqNum = uffc.frameProcCnt;//??
	    fc.frameObsSeqTot = uffc.frameObsTotal;//??
	    fc.chopBeam = uffc.ChopBeam;
	    fc.saveSetCnt = uffc.SaveSet;//??
	    fc.nodBeam = uffc.NodBeam;
	    fc.nodSetCnt = uffc.NodSet; //??
	    fc.coadds = uffc.coAdds;
	    fc.frameWriteCnt = uffc.frameWriteCnt;
	    fc.frameSendCnt = uffc.frameSendCnt;
	    fc.bg_ADUs = uffc.bgADUs;
	    fc.bg_Well = uffc.bgWellpc;//??
	    fc.sigmaFrameNoise = uffc.sigmaFrmNoise;
	    fc.off_ADUs = uffc.rdADUs;//??
	    fc.off_Well = uffc.rdWellpc;//??
	    fc.sigmaReadNoise = uffc.sigmaReadNoise;
	    return fc;
	} catch (Exception e) {
	    System.out.println(e.toString());
	    throw new DAF.GCSException();
	} 	
    }

    public ObsConfig getObsConfig() throws DAF.GCSException {
	try {
	    Socket s = new Socket(DASHost,DASPort);
	    UFTimeStamp uft = new UFTimeStamp("OC");
	    uft.sendTo(s);
	    //UFProtocol.createFrom(s);
	    // receive reply (??)
	    UFObsConfig ufoc = (UFObsConfig)UFProtocol.createFrom(s);
	    System.out.println(ufoc.toString());
	    s.close();
	    ObsConfig oc = new ObsConfig();
	    oc.chopBeams = ufoc.chopBeams();
	    oc.saveSets = ufoc.saveSets();
	    oc.nodBeams = ufoc.nodBeams();
	    oc.nodSets = ufoc.nodSets();
	    oc.coaddsPerFrm = ufoc.coaddsPerFrm();
	    oc.readoutMode = new String(ufoc.readoutMode());
	    oc.dataLabel = new String(ufoc.dataLabel());
	    return oc;
	} catch (Exception e) {
	    System.out.println(e.toString());
	    throw new DAF.GCSException();
	} 	
    }

}
