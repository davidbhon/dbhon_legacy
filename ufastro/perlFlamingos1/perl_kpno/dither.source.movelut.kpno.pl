#!/usr/local/bin/perl -w

## Execute dither patterns at KPNO 4m and 2.1m telescopes.
## This script uses absolute offsets wrt the pointing center
## and attempt to move to the next dither position while the
## lut is being applied.

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Dither_patterns qw/:all/;
use Fitsheader qw/:all/;
use Getopt::Long;
use GetYN;
use ImgTests;
use KPNO_offsets qw/:all/;
use kpnoTCSinfo qw/:all/;
use MCE4_acquisition qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($dpattern, $startpos, $net_scale_factor, $d_rptpos,
    $d_rptpat, $usenudge, $nudgesiz) = RW_Header_Params::get_dither_params();


#Load dither patterns
my @pat_x        = Dither_patterns::load_pattern_x( $dpattern );
my @pat_y        = Dither_patterns::load_pattern_y( $dpattern );
my @rel_offset_x = Dither_patterns::load_pat_rel_x_startpos( $startpos, $dpattern );
my @rel_offset_y = Dither_patterns::load_pat_rel_y_startpos( $startpos, $dpattern );

@pat_x           = Dither_patterns::scale_pattern_x_or_y( \@pat_x, $net_scale_factor );
@pat_y           = Dither_patterns::scale_pattern_x_or_y( \@pat_y, $net_scale_factor );

@rel_offset_x    = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_x, $net_scale_factor );
@rel_offset_y    = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_y, $net_scale_factor );

#>>>Get Filename info from header
#>>>Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

main::prompt_setup();
main::print_setup();
GetYN::query_ready();

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $x_nudge = 0;
my $y_nudge = 0;
my $next_index = $this_index;

my $X;
my $Y;
my $X_origin;
my $Y_origin;
my $last_pos_num  = @pat_x - 2;#num moves is @pat_x-1, then -1 to start count at 0
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ($x_nudge, $y_nudge) = Dither_patterns::nudge_pointing($d_rptpat, $rpt_pat, $usenudge, $nudgesiz);
  $X_origin = $rel_offset_x[$startpos] + $x_nudge;
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position

    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
      KPNO_offsets::do_offset( $X, $Y );
      #KPNO_offsets::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
	  print "***"; #KPNO_offsets::offset_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    $X = $pat_x[0] + $x_nudge;
    $Y = $pat_y[0] + $y_nudge;
    if( $VIIDO ){ 
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
      KPNO_offsets::do_offset( $X, $Y );
      #KPNO_offsets::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
	  print "offset by $X, $Y, back to position $startpos\n";
	  print "***"; #KPNO_offsets::offset_wait();
	}
  }

  RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

  #print "SHOULD BE: Should image $d_rptpos times here\n\n";
  print "IMAGE $d_rptpos times here\n\n";

  my $frame_status_reply;
  my $idle_notLutting;
  until($idle_notLutting ){
    $frame_status_reply = get_frm_status();
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print "FrameStatus: Not yet ready to take image at first position.....\n";
    }elsif( $idle_notLutting ==1 ){
      print "FrameStatus: Ready to take image at first position\n";
    }
  }
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  my $this_pos;
  for( my $i = $startpos; $i < $last_pos_num; $i+=1){
    #-------------first position in object beam---------
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]+$x_nudge; $Y = $pat_y[$this_pos]+$y_nudge;

    $X_origin = $rel_offset_x[$this_pos] + $x_nudge;
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;

    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
      KPNO_offsets::do_offset( $X, $Y );
      #KPNO_offsets::offset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "++++++++++++++++++++++++++++++++++++++++++++\n";
	   print "***offset by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
	   print "***"; #KPNO_offsets::offset_wait();
	 }

    RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    #print "SHOULD BE: Should image $d_rptpos times here\n\n";
    print "IMAGE $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
    		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);
  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){
    print "++++++++++++++++++++++++++++++++++++++++++++\n";
    print "offset to origin\n";
    KPNO_offsets::do_offset( 0, 0 );
    KPNO_offsets::offset_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin\n";
	 print "***offset to position $startpos: 0, 0\n";
	 print "***"; #KPNO_offsets::offset_wait();
       }

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
####SUBS
sub prompt_setup{
  print "\n\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern: \n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type CLEAR to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $clear_home = <>;chomp $clear_home;
  my $valid = 0;
  until( $valid ){
    if( $clear_home =~ m/clear/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/home/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either CLEAR or HOME ";
      $clear_home = <>; chomp $clear_home;
    }
  }

  if( $clear_home =~ m/clear/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    KPNO_offsets::clear_offset();
  }elsif( $clear_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;

  my $frame_status_reply;
  my $idle_notLutting;
  my $lut_sleep_count = 0;
  my $waited_enough_for_move = 0;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  my $offset_wait_time = $ENV{OFFSET_WAIT};
  until( $idle_notLutting ){
    sleep 1;
    $lut_sleep_count += 1;
    $waited_enough_for_move += 1;

    $frame_status_reply = get_frm_status();
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print "Have moved to next images' pointing center.\n";
      print "Now waiting for most recent image to read out (UFEdtDMA::applyLut> in MCE4 window).\n";
      print "This may take an additional $lut_timeout seconds.\n\n";
      print "Have waited an extra $lut_sleep_count seconds past the exposure time\n\n";
    }elsif( $idle_notLutting ==1 ){
      print "FrameStatus: Now ready to take next image\n\n";
    }
  }
  until( $waited_enough_for_move > $offset_wait_time ){
    print "Have moved to next images' pointing center.\n";
    print "Have waited $waited_enough_for_move seconds for telesecope.\n";
    sleep 1;
    $waited_enough_for_move += 1;
  }

  #Original version of loop
  #until( $idle_notLutting ){
  #  $frame_status_reply = get_frm_status();
  #  $idle_notLutting = 
  #    grep( /idle: true, applyingLUT: false/, $frame_status_reply);
  #
  #  if( $idle_notLutting == 0 ){
  #    print "FrameStatus: Must be applyingLut\n";
  #    print "           : Not yet ready to take next image\n";
  #    print "           : Have slept an extra $lut_sleep_count seconds past the exposure time\n\n";
  #  }elsif( $idle_notLutting ==1 ){
  #    print "FrameStatus: Now ready to take next image\n\n";
  #  }
  #  sleep 1;
  #  $lut_sleep_count += 1;
  #}

  for( my $i = 0; $i < $d_rptpos-1; $i++ ){
    print "take image with index= $this_index\n";
    print "SHOULD BE GETTING TCSINFO\n";
    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    #print "acq cmd is \n$acq_cmd\n\n";
    acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    idle_lut_status( $expt, $nreads );

    #move_file_for_index_sep( $file_hint, $next_index );#NOT used
    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "take image with index= $this_index\n";
  print "SHOULD BE GETTING TCSINFO\n";
  UseTCS::use_tcs( $use_tcs, $header, $telescop );

  print "SHOULD BE calling write filename header param with $filebase $this_index\n";
  RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

  my $acq_cmd = acquisition_cmd( $header, $lut_name, 
				 $file_hint, $this_index, $net_edt_timeout);

  #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
  #print "acq cmd is \n$acq_cmd\n\n";
  acquire_image( $acq_cmd );

  #print "SHOULD BE DOING idle_lut_status loop\n\n";
  print "ENTERING idle_lut_status_before_move loop\n";
  idle_lut_status_before_move( $expt, $nreads );

  #move_file_for_index_sep( $file_hint, $next_index );#NOT used
  my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
  if( $unlock_param ){
    ImgTests::make_unlock_file( $file_hint, $this_index );
  }

  $this_index += 1;

  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub idle_lut_status_before_move{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    #print "UFFRMSTATUS: $frame_status_reply\n";
    $idle_notLutting = 
      grep( /idle: false, applyingLUT: true/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print ">>>>>>>>>>..........SHOULD BE APPLYING LUT..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status_before_move


sub get_frm_status{
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  $frame_status_reply = `$uffrmstatus`;
  #print "UFFRMSTATUS: $frame_status_reply\n";
  return $frame_status_reply;
}#Endsub get_frm_status


sub print_setup{
  my $num_moves = @pat_x - 1;
  my $last_pos_num = $num_moves - 1;

  Dither_patterns::scale_pattern_msg();

  print "The pattern center has the following initial offsets wrt the catalog center:\n";
  print "The absolute offsets that will be applied,\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xabs       Yabs     X_wrt_Im1    Y_wrt_Im1\n";

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $pat_x[$i];print ", ";
    printf '% 10.3f', $pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "Number of images per position--------------  $d_rptpos\n";
  print "Number of times through pattern------------  $d_rptpat\n";
  print "Start position, first time through pattern-  $startpos\n";
  print "num moves = $num_moves, lastpos= $last_pos_num\n";
  print "----------------------------------------------\n\n";
}#Endsub print_setup


__END__

=head1 NAME

dither.source.movelut.kpno.pl

=head1 Description

An alternative script for image dithering.

It attempts to move the telescope while the 
image is being written to disk.  I don't really
recommend using it, but if you are really unhappy
about the overheads with the normal dithering script,
try using this one.

=head1 REVISION & LOCKER

$Name:  $

$Id: dither.source.movelut.kpno.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
