#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: z.test.tcsinfo.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;

use Fitsheader qw/:all/;
my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

getKPNOtcsinfo( $header );

sub getKPNOtcsinfo{
  my $header = $_[0];

  my $tcstel = $ENV{UF_KPNO_TCS_PROG};
  my $quote  = "\"";
  my $space  = " ";
  my $teleinfo_call = $tcstel.$space.$quote."tele info".$quote;

  my $teleinfo = `$teleinfo_call`;
  print "tcsinfo is:\n\n";
  print $teleinfo."\n\n";

  my ($ibeg);
  my ($iend);
  my ($tinfo);

  if( ($ibeg = index( $teleinfo, "mean position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA",$RA);
    setStringParam("DEC",$DEC);
    if( ($ibeg = index( $tinfo, "(" )) >= 0 ){
      $iend = rindex( $tinfo, ")" );
      my $epoch = substr( $tinfo, $ibeg+1, $iend-$ibeg-1 );
      setNumericParam("EPOCH",$epoch);
    }
  }else{
    setStringParam( "RA","unkown");
    setStringParam("DEC","unkown");
  }

  if( ($ibeg = index( $teleinfo, "appar position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA_APR",$RA);
    setStringParam("DEC_APR",$DEC);
  }else{
    setStringParam( "RA_APR","unkown");
    setStringParam("DEC_APR","unkown");
  }

  if( ($ibeg = index( $teleinfo, "ha" )) >= 0 ){
    $iend = index( $teleinfo, ",", $ibeg );
    my $ha = substr( $teleinfo, $ibeg, $iend-$ibeg );
    $ha = substr( $ha, index($ha,':')+1, length($ha) );
    setStringParam("HA",$ha);
  }else{ 
    setStringParam("HA","unkown");
  }

  if( ($ibeg = index( $teleinfo, "utime/stime" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $UT, $ST ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam("UTC",$UT);
    setStringParam("LST",' '.$ST);
  }else{
    setStringParam("UTC","unkown");
    setStringParam("LST","unkown");
  }

  if( ($ibeg = index( $teleinfo, "airmass/zen.dist" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $AM, $ZD ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setNumericParam( "AIRMASS",$AM);
    setNumericParam("ZD",$ZD);
  }else{
    setNumericParam( "AIRMASS",0);
    setNumericParam("ZD",0);
  }
  my $date = `date -u '+%Y-%m-%d'`;
  chomp( $date );
  setStringParam("DATE",$date);

  my $time = `date -u '+%H:%M:%S'`;
  chomp( $time );
  setStringParam("TIME",$time);

  print "Writing KPNO $ENV{THIS_TELESCOP} TCS info into header\n";
  print "$header\n\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub getKPNOtcsinfo
