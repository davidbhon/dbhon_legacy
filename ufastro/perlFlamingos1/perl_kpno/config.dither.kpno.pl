#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next dither sequence
#
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);


use Fitsheader qw/:all/;
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\n\n".
      ">>>---------------------Present exposure parameters are".
      "---------------------<<<\n";

printFitsHeader( "DPATTERN", "D_SCALE", 
		 "D_RPTPOS",  "D_RPTPAT", "STARTPOS" );

#printFitsHeader( "DPATTERN", "D_SCALE", 
#		 "D_RPTPOS",  "D_RPTPAT" );

print "\n";
printFitsHeader( "USENUDGE", "NUDGESIZ" );
print "\n";
#printFitsHeader( "NOD_SKY", "THROW", "THROW_PA" );#Nod_sky not used
printFitsHeader( "THROW", "THROW_PA" );
print "_____________________________________________________________".
      "__________________\n\n";

my $didmod = 0;
my $redo = 0;

print "Change anything? ";
my $change = get_yn();
if( $change ){
  until( $redo ){
    my $pattern = select_from_known_patterns();
    if( $pattern eq 0 ){
      #Didn't pick a new pattern
    }else{
      setStringParam( "DPATTERN", $pattern );
      $didmod += 1;
      #print "pattern = $pattern; didmod = $didmod\n";
    }

    my $changed_dscale = query_dscale_value();
    #print "changed_dscale = $changed_dscale\n";
    if( $changed_dscale > 0 ){
      setNumericParam( "D_SCALE", $changed_dscale );
      $didmod += 1;
    }

    my $chg_bigger_than_zero = chg_bigger_than_zero( "D_RPTPOS" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "D_RPTPOS", $chg_bigger_than_zero );
      $didmod += 1;
    }

    $chg_bigger_than_zero = chg_bigger_than_zero( "D_RPTPAT" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "D_RPTPAT", $chg_bigger_than_zero );
      $didmod += 1;
    }

    my $this_pattern = Fitsheader::param_value( "DPATTERN" );
    my $chg_startpos = change_start_pos( $this_pattern );
    if( $chg_startpos > -1 ){
      setNumericParam( "STARTPOS", $chg_startpos );
      $didmod += 1;
      #print "new startpos = $chg_startpos; didmod = $didmod\n";
    }

    my $chg_zero_or_one = chg_zero_or_one( "USENUDGE" );
    if( $chg_zero_or_one > -1 ){
      Fitsheader::setNumericParam( "USENUDGE", $chg_zero_or_one );
      $didmod += 1;
    }

    my $mod_num_params = 
      modNumericParams( "NUDGESIZ", "THROW","THROW_PA");

    if( $mod_num_params > 0 ){
      $didmod += 1;
    }

    if( $didmod > 0 ){
      print "\n\nThe new set of dither parameters are:\n";
      printFitsHeader( "DPATTERN", "D_SCALE",
      		       "D_RPTPOS",  "D_RPTPAT", "STARTPOS" );

      print "\n";
      printFitsHeader( "USENUDGE", "NUDGESIZ" );
      print "\n";
      #printFitsHeader( "NOD_SKY", "THROW", "THROW_PA" );
      printFitsHeader( "THROW", "THROW_PA" );
      print "___________________________________________________\n\n";
      print "\nAccept changes?";
      $redo = get_yn();
    }else{
      $redo = 1;
    }
  }
}elsif( !$change ){
  #do nothing
}


if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub query_dscale_value{
  my $response = 0;
  my $is_valid = 0;

  my $iparm = param_index( 'D_SCALE' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print "Enter a real value > 0 for the additional scale factor to apply: ";

  until($is_valid){
    chomp ($response = <STDIN>);

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[0-9]{0,}[a-z,A-Z][0-9]{0,}/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a real value:  ";
      }elsif( $response =~ m/^\.$/ ){
	#print "$response is not valid\n";
	print "Please enter a real value:  ";
      }elsif( $response == 0 ){
	print "0 is not a valid scale factor:  ";
      }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}
      }

    }else{
      print "Leaving D_SCALE unchanged\n";
      $response = 0;
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub query_dscale_value


sub change_start_pos{
  my $pattern = $_[0];

  my $largest_startpos;
  for( my $i = 0; $i < $Dither_patterns::num_pats; $i++ ){
    my $compat =  $Dither_patterns::pattern_names[$i];
    my $len = length $compat;
    my $extended_compat = $compat . ( ' ' x (18-$len));

    if( $pattern eq $extended_compat ){
      #print "Pattern $pattern matches $extended_compat\n";
      $largest_startpos = $Dither_patterns::largest_start_positions[$i];
      #print "largest start position index = $largest_startpos\n";
    }
  }

  #print $Dither_patterns::largest_start_positions[0]."\n";

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( "STARTPOS" );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter a start position that is >=0 and <= $largest_startpos:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter a number >= 0 and <= $largest_startpos:  ";
      }else{
	if( $response >=0 and $response <= $largest_startpos ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter a number >= 0 and <= $largest_startpos:  ";
	}
      }

    }else{
      print "Leaving STARTPOS unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub change_start_pos


sub chg_bigger_than_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value > 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chng_bigger_than_zero


sub chg_zero_or_one{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter either 0 or 1:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please either 0 or 1:  ";
      }else{
	if( $response == 0 or $response == 1 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter either 0 or 1:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_zero_or_one


__END__

=head1 NAME

config.dither.kpno.pl

=head1 Description

Use to setup for image dithering.


=head1 REVISION & LOCKER

$Name:  $

$Id: config.dither.kpno.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
