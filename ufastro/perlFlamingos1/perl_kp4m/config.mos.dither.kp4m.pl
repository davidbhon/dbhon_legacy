#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next mos dither sequence
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = Fitsheader::select_header($DEBUG);

use Fitsheader qw/:all/;
Fitsheader::Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
Fitsheader::readFitsHeader($header);

print "\n\n".
      ">>>---------------------Present exposure parameters are".
      "---------------------<<<\n";

Fitsheader::printFitsHeader( "M_RPTPAT", "M_THROW" );
print "\n";

Fitsheader::printFitsHeader( "M_NDGSZ", "USEMNUDG" );
print "\n";

Fitsheader::printFitsHeader( "CHIP-PA", "ROT_PA", "SLIT-CPA" );
print "\n";

my $pa_chip_on_sky_for_rot0  = Fitsheader::param_value( "CHIP-PA" );
my $pa_rotator               = Fitsheader::param_value( "ROT_PA" );
my $chip_pa_on_sky_this_rotator_pa = $pa_rotator + $pa_chip_on_sky_for_rot0;

my $pa_slit_on_chip = Fitsheader::param_value( "SLIT-CPA" );

my $pa_slit_on_sky  = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
                                                    $pa_slit_on_chip );
print "\n";
print "PA of slit on sky --------------------- = $pa_slit_on_sky (deg)\n";

print "_____________________________________________________________".
      "__________________\n\n";

my $didmod = 0;
my $redo = 0;

print "CHIP-PA  and SLIT_CPA are changed with a different config scripts.\n";
print "\nChange any of the other paramters?  ";
my $change = get_yn();
if( $change ){
  until( $redo ){
    my $chg_bigger_than_zero = chg_bigger_than_zero( "M_RPTPAT" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "M_RPTPAT", $chg_bigger_than_zero );
      $didmod += 1;
    }

    $chg_bigger_than_zero = chg_bigger_than_zero( "M_THROW" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "M_THROW", $chg_bigger_than_zero );
      $didmod += 1;
    }

    my $chg_zero_or_one = chg_zero_or_one( "USEMNUDG" );
    if( $chg_zero_or_one > -1 ){
      Fitsheader::setNumericParam( "USEMNUDG", $chg_zero_or_one );
      $didmod += 1;
    }

    #$chg_bigger_than_zero = chg_bigger_than_zero( "M_NDGSZ" );
    my $new_nudge = nudge_size( "M_NDGSZ" );
    if( $new_nudge != 900 ){
      Fitsheader::setNumericParam( "M_NDGSZ", $new_nudge );
      $didmod += 1;
    }

    my $rot_pa = chg_rotator_pa( "ROT_PA" );
    if( $rot_pa != 900 ){
      Fitsheader::setNumericParam( "ROT_PA", $rot_pa );

      my $crota1 = 180 - ($rot_pa + $pa_chip_on_sky_for_rot0);
      $crota1 = sprintf "%.5f", $crota1;
      setNumericParam( "CROTA1", $crota1 );

      $didmod += 1;
    }

    if( $didmod > 0 ){
      print "\n\nThe new set of dither parameters are:\n";
      Fitsheader::printFitsHeader( "M_RPTPAT", "M_THROW");
      print "\n";

      Fitsheader::printFitsHeader( "M_NDGSZ", "USEMNUDG" );
      print "\n";

      Fitsheader::printFitsHeader( "CHIP-PA", "ROT_PA", "SLIT-CPA" );

      my $new_rotator_pa = Fitsheader::param_value( "ROT_PA" );
      my $chip_pa_on_sky_w_new_rotator_pa = $new_rotator_pa + $pa_chip_on_sky_for_rot0;
      my $pa_slit_on_sky  = main::compute_pa_slit_on_sky( $chip_pa_on_sky_w_new_rotator_pa,
                                                          $pa_slit_on_chip );
      print "\n";
      print "PA of slit on sky --------------------- = $pa_slit_on_sky (deg)\n";
      print "_____________________________________________________________".
	"__________________\n";
      print "\nAccept changes?";
      $redo = get_yn();
    }else{
      $redo = 1;
    }
  }
}elsif( !$change ){
  #do nothing
}


if( $didmod > 0 ){
  if( !$DEBUG ){
    Fitsheader::writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    Fitsheader::writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub chg_bigger_than_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value > 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_bigger_than_zero


sub chg_rotator_pa{
  my $response = 900;
  my $is_valid = 0;

  my $iparm = param_index( 'ROT_PA' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print "Enter the rotator PA as listed on the TCS\n";
  print "Enter a real value between -3.0 and +185(degrees):  ";

  until($is_valid){
    chomp ($response = <STDIN>);

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[0-9]{0,}[a-z,A-Z][0-9]{0,}/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a real value:  ";
      }elsif( $response =~ m/^\.$/ ){
	print "$response is not valid\n";
	print "Please enter a real value:  ";
      }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response <= 185 and $response >= -3.0 ){
	    print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a value between -3.0 and +185 degrees\n";}
	}
      }

    }else{
      print "Leaving ROT_PAA unchanged\n";
      $response = 900;
      $is_valid = 1;
    }
  }
  print"\n";
  return $response;
}#Endsub chg_rotator_pa


sub chg_zero_or_one{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter either 0 (no) or 1 (yes):  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please either 0 or 1:  ";
      }else{
	if( $response == 0 or $response == 1 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter either 0 or 1:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_zero_or_one


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub nudge_size{
  my $response = 900;
  my $is_valid = 0;

  my $iparm = param_index( "M_NDGSZ" );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print "Enter a real value for the nudge between ABBA patterns:  ";

  until($is_valid){
    chomp ($response = <STDIN>);

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[0-9]{0,}[a-z,A-Z][0-9]{0,}/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a real value:  ";
      }elsif( $response =~ m/^\.$/ ){
	print "$response is not valid\n";
	print "Please enter a real value:  ";
      }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response > 0 ){
	    print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a real value > 0:  ";}
	}
      }

    }else{
      print "Leaving M_NDGSZ unchanged\n";
      $response = 900;
      $is_valid = 1;
    }
  }
  print"\n";
  return $response;
}#Endsub nudge_size


__END__

=head1 NAME

config.mos.dither.kp4m.pl

=head1 Description

Setup the parameters for the next mos ABBA exposure
sequence.


=head1 REVISION & LOCKER

$Name:  $

$Id: config.mos.dither.kp4m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
