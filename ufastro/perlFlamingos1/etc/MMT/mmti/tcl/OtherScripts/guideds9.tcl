set type        1
set nboxes 	2
set nbox 	2

proc acquirefull { } {
		global	split guideexp guidebin image1x image1y  ds9

    set timeout [expr 1000 * (20 + (60 / ($split+1) / ($guidebin*$guidebin)) + $guideexp) + 15000]

    set ds9(next) Frame3
    GotoFrame
    SetWatchCursor
    msg_cmd GUIDE stop
    msg_cmd GUIDE "expose full $guideexp" $timeout
}

proc acquirebox { } {
		global exptime 
		global imagesize guidebin
		global image1x image1y
		global image2x image2y
	
        global ds9
        set ds9(next) Frame1
        GotoFrame
	msg_cmd GUIDE "expose box $exptime" 30000

	#exec echo "file $file\[$cam]" | xpaset Guide
}

trace variable exptime  w setexptime 
proc setexptime { args } {
	global NeedRTConfigFrame1; set NeedRTConfigFrame1 1
	global NeedRTConfigFrame2; set NeedRTConfigFrame2 1
}

trace variable guideexp w setguideexp 
proc setguideexp { args } {
	global NeedRTConfigFrame3; set NeedRTConfigFrame3 1
}

proc tweakboxes { } {
	set tweak [msg_cmd GUIDE "tweakboxes data" 60000]
	puts $tweak
}

proc tick { } {
	msg_cmd GUIDE tick 12000
}

proc startstop { } {
	global state

	global NeedRTConfigFrame1
	global NeedRTConfigFrame2

    set NeedRTConfigFrame1 2
    set NeedRTConfigFrame2 2

    if { $state == 0 } {
	guidestart
    } else {
	msg_cmd GUIDE stop 10000
    }
}

proc startstoplabel { name indx op } {
	upvar $name value
	global buttons

    if { $value == 0 } {
	$buttons(name).guide.start configure -text "Start"
    } else {
	$buttons(name).guide.start configure -text "Stop "
    }
}

proc guidebox { w n } {
    frame $w -borderwidth 8

    label $w.gui_l -text "Guide Box $n"
    Grid  $w.gui_l - 

    set box [lindex [split $w .] end]

    Grid [checkbutton $w.${box}px -text "X Parity"  -variable ${box}px \
		-onvalue 1 -offvalue -1] - 
    Grid [checkbutton $w.${box}py -text "Y Parity"  -variable ${box}py \
		-onvalue 1 -offvalue -1] - 

    return $w
}
    
proc guideimage { w n } {
    frame $w

    label $w.gui_l -text "Guide Image $n"
    Grid  $w.gui_l - 

    set box [lindex [split $w .] end]

    eval Grid  [msgentry_async GUIDE "X pos"   $w.${box}x   {} Up "movemarker $n" {} nowait -width 6] 
    eval Grid  [msgentry_async GUIDE "Y pos"   $w.${box}y   {} Up "movemarker $n" {} nowait -width 6] 

    eval Grid [msgentry_async GUIDE "Snr$n"   $w.snr$n      {} {} {} {} {} -width 6] 
    eval Grid [msgentry_async GUIDE "fwhm$n"  $w.fwhm$n     {} {} {} {} {} -width 6] 
    eval Grid [msgentry_async GUIDE "cenx$n"  $w.cenx$n     {} {} {} {} {} -width 6] 
    eval Grid [msgentry_async GUIDE "ceny$n"  $w.ceny$n     {} {} {} {} {} -width 6] 

    return $w
}

proc basis_row {w err text} {
	global	${err}bamount 	\
		${err}bscale 	\
		${err}scale 

 msg_variable GUIDE ${err}bamount ${err}bamount rw {} Up 
 msg_variable GUIDE ${err}bscale  ${err}bscale  rw {} Up
 msg_variable GUIDE ${err}scale   ${err}scale   rw {} Up

 return "\
    [label $w.${err}label -text "$text" -justify left]		\
    [retentry $w.${err}bamount -width 8 -justify right]	\
    [retentry $w.${err}bscale  -width 8 -justify right]	\
    [retentry $w.${err}scale   -width 8 -justify right]"
}


proc guide_row {w err text} {
	global	${err}enable	\
		${err}rawerror	\
		${err}rawtotal	\
		${err}error	\
		${err}total	\
		${err}parity 	\
		${err}gain 	\
		${err}step 	\
		${err}limit 	\
		${err}sample 	\
		${err}lookb 	\
		${err}snrmin 

    global ${err}rawtotal
    set ${err}rawtotal 0.0

    # These extra traces drive the graphs
    #
    trace variable ${err}error     w "settotal ${err}"
    trace variable ${err}rawerror  w "settotal ${err}raw"

     msg_variable GUIDE ${err}enable ${err}enable rw {} Up 
     msg_variable GUIDE ${err}parity ${err}parity rw {} Up 

     msg_variable GUIDE ${err}gain   ${err}gain   rw {} Up
     msg_variable GUIDE ${err}step   ${err}step   rw {} Up
     msg_variable GUIDE ${err}limit  ${err}limit  rw {} Up
     msg_variable GUIDE ${err}sample ${err}sample rw {} Up
     msg_variable GUIDE ${err}offset ${err}offset rw {} Up
     msg_variable GUIDE ${err}snrmin ${err}snrmin rw {} Up

    return "							\
     [checkbutton $w.${err}button -text "$text" 	\
		-justify left 				\
		-variable ${err}enable 			\
		-onvalue 1 -offvalue 0                  \
                -command "msg_set GUIDE ${err}enable \[set ${err}enable]" ] \
     [entry $w.${err}rawerrol -width 10 -textvariable ${err}rawerrol]	\
     [entry $w.${err}errol    -width 10 -textvariable ${err}errol]	\
     [entry $w.${err}total    -width 10 -textvariable ${err}total]	\
     [checkbutton $w.${err}parity -text "" 		\
		    -variable ${err}parity 		\
		    -onvalue 1 -offvalue -1]		\
     [retentry $w.${err}gain   -width 8 -justify right]	\
     [retentry $w.${err}step   -width 8 -justify right]	\
     [retentry $w.${err}limit  -width 8 -justify right]	\
     [retentry $w.${err}sample -width 8 -justify right]	\
     [retentry $w.${err}offset -width 8 -justify right]	\
     [retentry $w.${err}snrmin -width 8 -justify right]"
}

proc basiscontrol { w } {
    eval Grid  [msgentry_async GUIDE "NBasis"  $w.nbasis  {} Up {} {} {} -width 8 -justify right]
    eval Grid  [msgentry_async GUIDE "Defocus" $w.defocus {} Up {} {} {} -width 8 -justify right]
    eval Grid  [msgentry_async GUIDE "Seeing"  $w.seeing  {} Up {} {} {} -width 8 -justify right]
    Grid  [label $w.error   -text Error] 	\
	  [label $w.bamount -text "B Amount"]	\
	  [label $w.bscale  -text "B Scale"]	\
	  [label $w.scale   -text "E Scale"]


    eval Grid [basis_row $w xt  "XTilt" ] 
    eval Grid [basis_row $w yt  "YTilt" ] 
    eval Grid [basis_row $w de  "Defoc" ]
    eval Grid [basis_row $w ax "AstigX" ]
    eval Grid [basis_row $w ay "AstigY" ] 
    eval Grid [basis_row $w cx  "ComaX" ]
    eval Grid [basis_row $w cy  "ComaX" ] 
}

proc imagecontrol { w } {
    eval Grid [label $w.gdrlab -text "Gdr Image"] 		\
	      [msgentry_async GUIDE "binning" $w.guidebin  {} Up {} {} {} -width 5]  	\
	      [labentry       "expose"  $w.guideexp          -width 5]
    eval Grid [label $w.ccdlab -text "CCD Image"] 		\
	      [msgentry_async GUIDE "binning" $w.imagebin  {} Up {} {} {} -width 5] 	\
	      [msgentry_async GUIDE "size"    $w.imagesize {} Up {} {} {} -width 5]
    eval Grid [label $w.guilab -text "Guide Box"] 		\
	      [msgentry_async GUIDE "binning"  $w.boxbin  {} Up { boxmarkers2 } {} {} -width 5] 	\
	      [msgentry_async GUIDE "bk width" $w.boxbkw  {} Up { boxmarkers2 } {} {}  -width 5]

    eval Grid x [msgentry_async GUIDE "expose" $w.exptime {} Up {} {} {} -width 5]

    global split
    Grid x x x \
           [checkbutton $w.split   -text "split  "] 				-

    msg_variable GUIDE split split rw {} {} {} {} Up

    Grid [guideimage $w.image1 1] -  	\
	 [guideimage $w.image2 2] - 

    Grid [guidebox   $w.guide1 1] -  	\
	 [guidebox   $w.guide2 2] - 


}

proc errorcontrol { w } {
    Grid  [label $w.label    -text Error]	\
	  [label $w.rawerror -text Raw]		\
	  [label $w.error    -text Current]	\
	  [label $w.total    -text Total]	\
	  [label $w.parity   -text Par]		\
	  [label $w.gain     -text Gain] 	\
	  [label $w.step     -text Step]	\
	  [label $w.limit    -text Limit]	\
	  [label $w.sample   -text Sample]	\
	  [label $w.offset   -text Offset]	\
	  [label $w.snrmin   -text SNR]

    eval Grid [guide_row $w az "Az     "]
    eval Grid [guide_row $w el "El    "] 
    eval Grid [guide_row $w rt "Rot    "] 
    eval Grid [guide_row $w fc "Focus  "]
    eval Grid [guide_row $w ax "Astig X"] 
    eval Grid [guide_row $w ay "Astig Y"] 
    eval Grid [guide_row $w cx "Coma X "]
    eval Grid [guide_row $w cy "Coma Y "]
}


proc guidestart { } {
		global	aztotal	\
			eltotal	\
			rttotal	\
			fctotal	\
			axtotal	\
			aytotal	\
			cxtotal	\
			cytotal

	set aztotal "  0.000"
	set eltotal "  0.000"
	set rttotal "  0.000"
	set fctotal "  0.000"
	set axtotal "  0.000"
	set aytotal "  0.000"
	set cxtotal "  0.000"
	set cytotal "  0.000"

        global ds9
        set ds9(next) Frame1
        GotoFrame

	msg_cmd GUIDE start 10000
}

trace variable type w updatemodemenu
trace variable nboxes w updatemodemenu

proc updatemodemenu { args } {
	global type nboxes nbox guidemode

    set nbox $nboxes
    set guidemode [expr $type*2+$nboxes]
}
	
proc setmode { t n } {
	global type nboxes

	if { $type != $t }   { set type   $t }
	if { $nboxes != $n } { set nboxes $n }

	Frame3 marker delete all

	makemarker 1
	makemarker 2
}

proc menubar { } {
    #.menuBar delete "Analysis"

    menu .menuBar.optsmenu      -tearoff 0
    menu .menuBar.optsmenu.mode -tearoff 0

    .menuBar add cascade -menu .menuBar.optsmenu -label "Guide"

    .menuBar.optsmenu add cascade -label "Mode" -menu .menuBar.optsmenu.mode
    .menuBar.optsmenu.mode add radio -label "Centroid 1 Box" -command { setmode 1 1 } -value 3 -variable guidemode
    .menuBar.optsmenu.mode add radio -label "Centroid 2 Box" -command { setmode 1 2 } -value 4 -variable guidemode
    .menuBar.optsmenu.mode add radio -label "Defocus 1 Box"  -command { setmode 0 1 } -value 1 -variable guidemode
    .menuBar.optsmenu.mode add radio -label "Defocus 2 Box"  -command { setmode 0 2 } -value 2 -variable guidemode

    .menuBar.optsmenu add command -label "Error Control"		\
	-command { ToplevelRaise .errorcontrol }
    .menuBar.optsmenu add command -label "Image Control"		\
	-command { ToplevelRaise .imagecontrol }
    .menuBar.optsmenu add command -label "Basis Control"		\
	-command { ToplevelRaise .basiscontrol }
    .menuBar.optsmenu add command -label "Raw Error Graphs"			\
	-command { ToplevelRaise .rgraphs }
    .menuBar.optsmenu add command -label "Raw Total Graphs"			\
	-command { ToplevelRaise .xgraphs }
    .menuBar.optsmenu add command -label "Cur Error Graphs"			\
	-command { ToplevelRaise .egraphs }
    .menuBar.optsmenu add command -label "Cur Total Graphs"			\
	-command { ToplevelRaise .tgraphs }
}
 
proc UpdateAnalysisMenu { } { }


#my $rcsId = q($Name:  $ $Id: guideds9.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);
