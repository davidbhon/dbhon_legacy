# Table Editor Tcl Interface
#
#my $rcsId = q($Name:  $ $Id: ted.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

proc Ted { } {
	return "Table Editor Version 1.1"
}

proc docol_add { cd } {
    foreach col [$cd.columns.list curselection] {
    	$cd.project.list insert end [$cd.columns.list get $col]
    }
}
proc docol_ins { cd } {
    set here [$cd.project.list curselection]
    if { [string compare $here {}] } {
	foreach col [$cd.columns.list curselection] {
	    $cd.project.list insert $here [$cd.columns.list get $col]
	}
    }
}
proc docol_rem { cd } {
    set this [$cd.project.list curselection]
    if { [string compare $this {}] } {
        $cd.project.list delete $this
    }
}
proc docol_can { cd } {
    upvar #0 $cd d
    ted_project [winfo parent $cd] $d

    destroy $cd
    set d 1
}
proc docol_app { cd } {
    ted_project [winfo parent $cd] [$cd.project.list get 0 end]
}
proc docol_ok { cd } {
    upvar #0 $cd d
    destroy $cd
    set d 1
}

proc ted_docolumn { E } {
     	set d [toplevel $E.cd]

    upvar #0 $d cd

    wm title $d "Pick Columns to Display"

    Grid   [label $d.display -text "Displayed"]	\
	 x [label $d.availab -text "Available"]
    Grid [scrolllist $d.project -exportselection 0]	\
	 [InFrame $d.frame {
		upvar E E

		grid [button $w.add -text "Add"    -command "docol_add $E.cd"] -sticky new
		grid [button $w.ins -text "Insert" -command "docol_ins $E.cd"] -sticky new
		grid [button $w.del -text "Remove" -command "docol_rem $E.cd"] -sticky new
	 }]			\
	 [scrolllist $d.columns -exportselection 0]

    grid [button $d.cancel -width 6 -text Cancel   -command "docol_can $E.cd"]	\
	 [button $d.apply  -width 6 -text Apply    -command "docol_app $E.cd"]	\
	 [button $d.ok     -width 6 -text OK       -command "docol_ok  $E.cd"]

    eval $d.project.list insert 0 [ted_projection $E]
    eval $d.columns.list insert 0 [ted_columns    $E]

    grid rowconfigure    $d 1 -weight 1

    set cd [ted_projection $E]
    vwait $d
}

proc tab_save { E file } {
	    global $E.data

	starbase_write $file $E.data
}

proc tab_new { E file args } {
	    global $E.data

	starbase_new    $E.data
	starbase_driver $E.data

	eval ted_create $E $E.data $args
	tab_open $E.edit $file
	wm title [winfo toplevel $E] $file
}

proc tab_http { E url } {
	    global $E.data

    starbase_http $url $E.data
    starbase_driver $E.data

    ted_data $E $E.data
}

proc tab_read { E fp } {
	    global $E.data

    starbase_readfp $fp $E.data
    starbase_driver $E.data

    ted_data $E $E.data
}

proc tab_open { E file } {
	    global $E.data

	set fp [open $file]
	tab_read $E $fp
	close $fp

	filepath $E $file

	wm title [winfo toplevel $E] $file
}

set TableFileClass(file) 	{}
set TableFileClass(directory) 	[exec pwd]
set TableFileClass(dirslist)	{}
set TableFileClass(loadtype)  	{}
set TableFileClass(pattern)	 0
set TableFileClass(types) {
        { "Starbase Files"  	.tab    }
        { "Starbase Files"  	.cat    }
        { "Starbase Files"  	.gal    }
        { "Starbase Files"  	.fld    }
        { "Table Files"  	.tab    }
        { "Catalog Files"  	.cat    }
        { "Field Files"  	.fld    }
        { "Galaxy Files"	.gal    }
        { "All files"           *     	}
}

proc ted_create { E D args } {

    set menubar [Menubar  $E]

    set filemenu [filemenu $menubar TableFileClass]
    setvalue $filemenu new  "tab_new  $E.edit"
    setvalue $filemenu open "tab_open $E.edit"
    setvalue $filemenu save "tab_save $E.edit"
    setvalue $filemenu path ""

    set tablmenu [tablmenu $menubar]
    setvalue $tablmenu rowins "ted_dorowins $E.edit"
    setvalue $tablmenu rowdel "ted_dorowdel $E.edit"
    setvalue $tablmenu colins "ted_docolins $E.edit"
    setvalue $tablmenu coldel "ted_docoldel $E.edit"
    setvalue $tablmenu column "ted_docolumn $E.edit"

    eval ted_editor $E.edit None $args
    grid $E.edit -row 1 -column 0 -sticky news
    grid rowconfigure $E    0 -weight 0
    grid rowconfigure $E    1 -weight 1
    grid columnconfigure $E 0 -weight 1
}

# Set the data table for the editor
#
proc ted_data { E data } { 
	upvar #0 $E editor

    setvalue $E data $data

    if { ![string compare [getvalue $E project] {}] } {
	upvar #0 $editor(data) D
	ted_project $E [$D(columns) D]
    }

    $E.table configure -rows [expr [ted_nrows $E] + 1]	\
		       -cols [expr [ted_projected $E] + 1]

}

# File reading drivers
#
proc ted_readers { E list } { setvalue E readers $list }

# Top level IO procs
#
proc ted_readfile  { E filename } {
    foreach { pattern reader } { [getvalue $E readers] } {
	if [regex $pattern $filename] {
	    [getvalue $E write] [getvalue $E data] $filename
	}
    }
}
proc ted_writfile { E filename } 	{ [getvalue $E write] 	[getvalue $E data] $fp 	 	  	}

proc tablmenu_dotablcommand { w cmd } {
	global errorInfo
	upvar #0 $w tabl

	eval $tabl($cmd)
}

proc tablmenu { w } {
    menu $w.tablmenu -tearoff 0
    $w add cascade -menu $w.tablmenu -label "Table"
    $w.tablmenu add command -label "Columns ..."		\
	-command "tablmenu_dotablcommand $w.tablmenu column"
    $w.tablmenu add command -label "Insert Row"			\
	-command "tablmenu_dotablcommand $w.tablmenu rowins"
    $w.tablmenu add command -label "Delete Row"			\
	-command "tablmenu_dotablcommand $w.tablmenu rowdel"
    $w.tablmenu add command -label "Insert Col"			\
	-command "tablmenu_dotablcommand $w.tablmenu colins"
    $w.tablmenu add command -label "Delete Col"			\
	-command "tablmenu_dotablcommand $w.tablmenu coldel"

    return $w.tablmenu
}
proc ted_dorowdel { w } {
                upvar #0 $w E
 
    set row [lindex [$w.table curselection] 0]
    if { $row == "" } { return }

    set r [lindex [split $row ","] 0]
    set c [lindex [split $row ","] 1]

    ted_rowdel $w $r
    #ted_setcell E $r $c $T($r,$c)
}
 
proc ted_dorowins { w } {
                upvar #0 $w.edit E
 
    set row [lindex [$w.table curselection] 0]
    if { $row == "" } {
	set r 1
    } else {
        set r [lindex [split $row ","] 0]
        set c [lindex [split $row ","] 1]
    }
 
    ted_rowins $w $r
    #ted_setcell E $r $c $T($r,$c)
}


# Get a new table editor
#
proc ted_editor { E D args } {
	    frame $E
	    upvar #0 $E editor


	if { $D != "None" } {
	    ted_data $E $D
	    set Nrows [expr [ted_nrows     $E] + 1]
	    set Ncols [expr [ted_projected $E] + 1]
	} else {
	    set editor(date) None
	    set Ncols 10
	    set Nrows 10
	}

	set command "ted_tablecommand $E %i %r %c %s"
	eval table $E.table				\
	    -command {$command}			 	\
	    -cache 0 					\
	    -titlerows 1				\
	    -titlecols 1				\
	    -yscrollcommand {[list $E.sy set]}		\
	    -xscrollcommand {[list $E.sx set]}		\
	    -rows $Nrows				\
	    -cols $Ncols				\
	    -anchor e					\
	    -colorigin 0 -roworigin 0			\
	    -colwidth	15				\
	    -height 24					\
	    -rowstretch unset 				\
	    -colstretch last 				\
	    -selectmode extended			\
	    $args
	scrollbar $E.sy -command [list $E.table yview] -orient v
	scrollbar $E.sx -command [list $E.table xview] -orient h
	 
	$E.table width 0 6

	grid $E.table $E.sy -sticky nsew
	grid $E.sx          -sticky nsew
	grid rowconfigure    $E 0 -weight 1
	grid columnconfigure $E 0 -weight 1

	return $E
}


proc ted_nrows	  { E }	      		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(nrows)   D   	}
proc ted_ncols	  { E }			{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(ncols)   D 		}
proc ted_set      { E row col value } 	{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(set)     D $row $col $value }
proc ted_get      { E row col } 	{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(get)     D $row $col }
proc ted_colnum   { E name } 		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(colnum)  D $name 	}
proc ted_colname  { E col }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(colname) D $col 	}
proc ted_columns  { E }			{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(columns) D 		}
proc ted_coldel   { E col }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(coldel)  D $col
					  $E.table configure -cols [expr [ted_projected E]+1] 		}
proc ted_colins   { E name here }	{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(colins)  D $name $here
					  $E.table configure -cols [expr [ted_projected E]+1] 		}
proc ted_colapp   { E name }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(colapp)  D $name 
					  $E.table configure -cols [expr [ted_projected E]+1] 		}
proc ted_rowdel   { E row }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(rowdel)  D $row
					  $E.table configure -rows [expr [$D(nrows)  D]+1];   		}
proc ted_rowins   { E here }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(rowins)  D $here	
					  $E.table configure -rows [expr [$D(nrows)  D]+1] 		}
proc ted_rowapp   { E row }		{ upvar #0 $E editor; upvar #0 $editor(data) D; $D(rowapp)  D $row 
					  $E.table configure -rows [expr [$D(nrows)  D]+1] 		}

# A user level proc to set a value in the data array and update the screen
#
proc ted_setcell { E row col value } {
	upvar #0 $E editor

    ted_set $E $row $col $value

    filedirty $E yes
    if { $col <= [ted_projected $E] } {
	$E.table set $row,$editor(R-$col) $value
    }
}

proc ted_project { E columns } {
	upvar #0 $E editor
	upvar #0 $editor(data) D

    set editor(project) $columns

    set i 1
    foreach c $columns { 
	set c [string trim $c]

	set editor(0,$i) $c
	set editor($c)   $i
	set editor($i)   [$D(colnum) D $c]
	set editor(R-[$D(colnum) D $c]) $i
	incr i
    }

    $E.table configure -cols [expr [ted_projected $E]+1]
}

proc ted_projection { E } { upvar #0 $E editor;  return $editor(project)	   }
proc ted_projected  { E } { upvar #0 $E editor;  return [llength $editor(project)] }

# This command is called from the widget to fill in the matrix
#
proc ted_tablecommand { E set row col value } {
	upvar #0 $E editor
	upvar #0 $editor(data) D

    if { $col == 0 } {
	if { $row == 0 } { 
	    return ""
	} else {
	    return $row
	}
    }

    if { $editor(data) == "None" } {
	return {}
    }

    if { $row == 0 } {
	return $editor(0,$col)
    }

    if { $set } {
	filedirty $E yes
	return [$D(set) D $row $editor($col) $value]
    } else {
	return [$D(get) D $row $editor($col)]
    }
}

# Table driver wrappers
#
proc tab_datanrows  { D }		{ upvar #0 $D data; $data(nrows)   data 		}
proc tab_datancols  { D }		{ upvar #0 $D data; $data(ncols)   data 		}
proc tab_dataset    { D row col value }	{ upvar #0 $D data; $data(set)     data $row $col $value}
proc tab_dataget    { D row col } 	{ upvar #0 $D data; $data(get)     data $row $col 	}
proc tab_datacolnum { D name } 		{ upvar #0 $D data; $data(colnum)  data $name 		}
proc tab_datacolname { D col }		{ upvar #0 $D data; $data(colname) data $col 		}
proc tab_datacolumns { D     }		{ upvar #0 $D data; $data(columns) data  		}
proc tab_datacoldel { D col }		{ upvar #0 $D data; $data(coldel)  data $col		}
proc tab_datacolins { D name here }	{ upvar #0 $D data; $data(colins)  data $name $here	}
proc tab_datacolapp { D name }		{ upvar #0 $D data; $data(colapp)  data $name		}
proc tab_datarowdel { D row }		{ upvar #0 $D data; $data(rowdel)  data $row		}
proc tab_datarowins { D here }		{ upvar #0 $D data; $data(rowins)  data $here		}
proc tab_datarowapp { D row }		{ upvar #0 $D data; $data(rowapp)  data $row		}

proc tab_driver { D Dr } {
	upvar $D  data
	upvar $Dr driver

	set data(nrows)		$driver(nrows)
	set data(ncols)		$driver(ncols)
	set data(get)		$driver(get)
	set data(set)		$driver(set)
	set data(colname)	$driver(colname)
	set data(colnum)	$driver(colnum)
	set data(colins)	$driver(colins)
	set data(coldel)	$driver(coldel)
	set data(colapp)	$driver(colapp)
	set data(rowins)	$driver(rowins)
	set data(rowdel)	$driver(rowdel)
	set data(rowapp)	$driver(rowapp)
}
