
catch {
    menu $ds9(mb).observe -tearoff 0
    $ds9(mb) add cascade -menu $ds9(mb).observe -label Observe
    $ds9(mb).observe add command -label "Fix Pointing" -command miniFixPoint
}

set instname flamingos

set minicam_f9(x0) 4518
set minicam_f9(y0) 4703
set minicam_f9(angle)  -90
set minicam_f9(parity) 1
set minicam_f9(scale)  0.0455
set minicam_f9(coordsys) detector
set minicam_f9(fitsextn) 1

set flamingos(x0)    1024
set flamingos(y0)    1000
set flamingos(angle) 180
set flamingos(parity) -1
set flamingos(scale) 0.169
set flamingos(coordsys) image
set flamingos(fitsextn) 0

proc pingcheck { s } {
    global env
    set host [lindex [split $env($s) :] 0]
    return [expr ![catch {exec ping $host 1}]]
}

source $env(MMTITCL)/msg.tcl

proc msg_connect { s } {
    global env
    if {[info exists env($s)]} {
	if {![info exists ${s}(up)]} {
	    if [pingcheck  $s] {
		msg_client $s
	    } else {
		error "$s server machine is down."
	    }
	}
    } else {
	error "$s not defined"
    }
}

proc miniKWLookup { keyword } {
    global current instname
    upvar #0 $instname Instname
    foreach line [split [$current(frame) get fits header $Instname(fitsextn) ] \n] {
	set ll [split $line =]
	if {[llength $ll] == 2} {
	    scan [lindex $ll 0] %s kw
	    set val [lindex [split [lindex $ll 1] /] 0]
	    regsub -all {\'} $val {} val
	    set key($kw) $val
	}
    }
    return $key($keyword)
}

proc PixToArcsec { instname x y { angle 0 } } {
    upvar #0 $instname caminfo
    set angle [expr ($angle + $caminfo(angle)) / 180. * 3.14159]
    set x [expr $x * $caminfo(parity)]
    set xx [expr ($x *  cos($angle) + $y * sin($angle)) * $caminfo(scale)]
    set yy [expr ($x * -sin($angle) + $y * cos($angle)) * $caminfo(scale)]
    return "$xx $yy"
}

proc ArcsecToPix { instname x y { angle 0 } } {
    upvar #0 $instname caminfo
    set angle [expr -($angle + $caminfo(angle)) / 180. * 3.1415926535]
    set xx [expr ($x *  cos($angle) + $y * sin($angle)) / $caminfo(scale)]
    set yy [expr ($x * -sin($angle) + $y * cos($angle)) / $caminfo(scale)]
    set xx [expr $xx * $caminfo(parity)]
    return "$xx $yy"
}

proc miniFixPoint {} {
    global instaz instel rot instscale instpixx0 instpixy0 instname
    global initpos 
    global targid currid current

    upvar #0 $instname Instname

    msg_connect TELESCOPE

    if {[winfo exists .fixpointing]} {
	wm deiconify .fixpointing
	raise .fixpointing
    } else {

	if { [ catch {
	    set instaz [miniKWLookup INSTAZ]
	} ] } { 
	    set instaz 0
	}

	if { [ catch {
	    set instel [miniKWLookup INSTEL]
	} ] } {
	    set instel 0
	}

	if { [ catch {
	    set rot    [miniKWLookup ROTANGLE]
#	    set rot    [msg_get TELESCOPE rot]
	} ] } {
	    puts "Setting rot to 0"
	    set rot 0
	}

	puts "instaz $instaz instel $instel"
	puts "zz $zz [lindex $zz 0] [lindex $zz 1]"
	set zz [ArcsecToPix $instname $instaz $instel 0]
	set instpixx [expr $Instname(x0) - [lindex $zz 0]]
	set instpixy [expr $Instname(y0) - [lindex $zz 1]]
	puts "Initnew $instpixx $instpixy"
	
	set currid [$current(frame) marker create circle $instpixx $instpixy 1 color = red text = Actual]
	$current(frame) marker $currid move to $Instname(coordsys) $instpixx $instpixy

	set targid [$current(frame) marker create circle $instpixx $instpixy 1 color = green text = Desired ]
	$current(frame) marker $targid move to $Instname(coordsys) $instpixx $instpixy

	$current(frame) marker $targid circle radius 50 pixels
	$current(frame) marker $currid circle radius 25 pixels
	set initpos [$current(frame) get marker $currid center $Instname(coordsys) pixels]


	puts "Initpos read: $initpos"
	set w [toplevel .fixpointing]
	message $w.msg -text {
	    Move the red marker to the actual target position.
	    
	    Move the green marker to the desired target position.
	}
	
	frame $w.b
	pack $w.msg $w.b -side top
	button $w.b.ok -text OK -command fixpointingok
	button $w.b.cancel -text Cancel -command fixpointingcancel
	pack $w.b.ok -side left
	pack $w.b.cancel -side right
    }
}

proc fixpointingok {} {
    global targid currid current initpos rot instscale instname
    upvar #0 $instname Instname
    puts "Fixing pointing"
    set targ [$current(frame) get marker $targid center $Instname(coordsys) pixels]
    set curr [$current(frame) get marker $currid center $Instname(coordsys) pixels]
    puts [$current(frame) get marker $currid center canvas pixels]
    puts "targ: $targ"
    puts "curr: $curr"
    
    set x0      [lindex $initpos 0]
    set y0      [lindex $initpos 1]

    set dx      [expr [lindex $curr 0] - $x0]
    set dy      [expr [lindex $curr 1] - $y0]
    set dazel     [PixToArcsec $instname $dx $dy $rot]

    set dx      [expr -([lindex $targ 0] - $x0)]
    set dy      [expr -([lindex $targ 1] - $y0)]
    set dinstazel [PixToArcsec $instname $dx $dy 0]

    catch {
	msg_cmd TELESCOPE "azelrel   $dazel"        5000
	msg_cmd TELESCOPE "instrel   $dinstazel"    5000
    }

    $current(frame) marker $targid delete
    $current(frame) marker $currid delete
    destroy .fixpointing
}

proc fixpointingcancel {} {
    global targid currid current
    $current(frame) marker $targid delete
    $current(frame) marker $currid delete
    destroy .fixpointing
}


#my $rcsId = q($Name:  $ $Id: observemenu.tcl,v 0.1 2003/05/22 14:57:11 raines Exp $);

