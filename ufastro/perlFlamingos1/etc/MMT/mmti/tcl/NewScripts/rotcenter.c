/* Compute the rotation center given two positions
   and the difference in rotator angle 
   There are two solutions */

#include <math.h>
#define DEGSPERRAD (180./M_PI)

/*
#my $rcsId = q($Name:  $ $Id: rotcenter.c,v 0.1 2003/05/22 14:57:11 raines Exp $);
*/

main(int argc, char *argv[]) 
{
    double x1,y1,x2,y2,angle;
    double c,a,phi,x0,y0;

    if (argc != 6 ) {
	fprintf(stderr, "Usage:  rotcenter x1 y1 x2 y2 angle(deg)\n");
	exit(1);
    }

    x1 = atof(argv[1]);
    y1 = atof(argv[2]);
    x2 = atof(argv[3]);
    y2 = atof(argv[4]);
    angle = atof(argv[5]) / DEGSPERRAD;

    phi = atan2(y2-y1,x2-x1);
    
    printf("phi = %g\n",phi*DEGSPERRAD);

    c = sqrt((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1));
    printf("c = %g\n", c);
    
    a = c/2/atan(angle/2);
    printf("a = %g\n", a);
    
    x0 = (x1+x2)/2 + sin(phi) * a;
    y0 = (y1+y2)/2 - cos(phi) * a;

    printf("%f %f\n", x0, y0);


    x0 = (x1+x2)/2 - sin(phi) * a;
    y0 = (y1+y2)/2 + cos(phi) * a;

    printf("%f %f\n", x0, y0);
}
