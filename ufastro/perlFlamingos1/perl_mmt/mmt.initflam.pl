#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use GetYN;

my $DEBUG   = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug'   => \$DEBUG );

#--------------
my $uf_perl_path      = $ENV{UF_PERL_ALL_TEL_PATH};
my $gui_display       = "packrat:0.0 ";
my $daemon_display    = "cfaguider:0 ";

#Check paths exist
die("Cannot find $uf_perl_path: ". $! . "\n") unless (-e $uf_perl_path);

look_for_demons();

my $mce4_agent_up = start_agents();

if($mce4_agent_up == 1){
  print("The mce4 agent is up, and may be listening\n" .
        "Initialize mce4?");
  my $reply = get_yn();
  if($reply){
    mce_agent_sleep(2);
    initialize_mce4();
  }
}

Start_ufstatus();
Start_array_temp_mon();

print "\nDone with Init_Flam.pl\n\n";

###########################
###                     ###
###Subs embedded in main###
###                     ###
###########################
sub look_for_demons{
  #print ">>>Entering sub look_for_demons\n";
  my @demons = ($ENV{UFLS208D},  $ENV{UFGMOTORD},
		$ENV{UFVACUUMD}, $ENV{UFGMCE4D}
	       );

  foreach my $item (@demons) {
    if(-e ($item)){
      #print("found " . $item . "\n");
    }else {
      print("Cannot find $item:\n $!" . "\nContinue?\n");
      my $response = get_yn();
      if($response == 0){
	die "Exiting\n";
      }
    }
  }
  print "Found daemons/agents\n";
  #print ">>>Exiting sub look_for_demons\n";
}#endsub look_for_demons


sub Start_ufstatus{
  my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . "mmt.ufstatus_tkgui.pl";
  #my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . "ufstatus.pl";
  if( -e $executable ){
    print "Found $executable\n";
  }else{
    print "Cannot find $executable\n\nContinue?\n";
    my $response = get_yn();
    if($response == 0){
      die "Exiting\n";
    }
  }#end existence if

  my $pslist = `ps -ef`;
  my $look_for_ps = index($pslist, "mmt.ufstatus_tkgui.pl");
  if($look_for_ps >= 0){
    print "ufstatus_tkgui.pl already running\n";
  }elsif ($look_for_ps < 0){
    my $xtp       = "xterm -fn 6x12 -geometry 75x10+1+330 -sb -sl 2000 -display $gui_display -iconic ";
    my $start_str = ($xtp . "-n Ufstatus_Gui_Messages -T Ufstatus_Gui_Messages -e $executable &" );


    print "Executing $start_str\n";
    system($start_str);
    #print "Sleep 5\n";
    #sleep 5;
    #print "\n";
  }

}#Endsub Start_ufstatus


sub Start_array_temp_mon{
  my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . "array.temperature.quick.look.pl";
  if( -e $executable ){
    print "Found $executable\n";
  }else{
    print "Cannot find $executable\n\nContinue?\n";
    my $response = get_yn();
    if($response == 0){
      die "Exiting\n";
    }
  }#end existence if

  my $pslist = `ps -ef`;
  my $look_for_ps = index($pslist, "array.temperature.quick.look.pl");
  if($look_for_ps >= 0){
    print "Array.temperature.quick.look.pl already running\n";
  }elsif ($look_for_ps < 0){
    my $xtp       = "xterm -fn 6x12 -geometry 75x10-1-0 -sb -sl 2000 -display $daemon_display ";
    my $start_str = ($xtp . "-n Array_temperature_Messages -T Array_temperature_Messages -e $executable &" );

    print "Executing $start_str\n";
    system($start_str);
    #print "Sleep 5\n";
    #sleep 5;
    #print "\n";
  }

}#Endsub Start_array_temp_mon


sub Start_setbias{
  #my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . "setbias.pl";
  my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . $ENV{SET_BIAS_SCRIPT};
  if( -e $executable ){
    print "Found $executable\n";
  }else{
    print "Cannot find $executable\n\nContinue?\n";
    my $response = get_yn();
    if($response == 0){
      die "Exiting\n";
    }
  }#end existence if
  system($executable);

}#Endsub Start_setbias


sub start_agents{
  #print ">>>Entering sub start_agents\n";
  #start agents and tee agent std err to output file
  #returns array (with leading 0) of output files to look for,
  #if sub starts agent; strip off leading 0

  my $tee_agent_array_ref = tee_start_agents();

  my @sh_array = @$tee_agent_array_ref;
  shift(@sh_array);
  my $num_ele = @sh_array;

  #Now look at text files to verify agents started
  #Note does not address question can we actually
  #talk to the devices using the agents
  my $mce4_agent_up = 1;
  if($num_ele > 0){
    my $i;
    for( $i=0; $i <= ($num_ele - 1); $i++ ){
      #response = (0|1) = (!listening | listening)
      my $response = did_agents_start($sh_array[$i]);

      if (!$response){
	my $str = $sh_array[$i];
	
	$str =~ s/^xt_//;
	$str =~ s/\.txt$//;

	if($str eq "mce4"){
	  $mce4_agent_up = 0;
	}
	
	print("$str agent did not initialize yet\n".
	      "(Does not say listening on port)\n".
	      "You will not be able to use $str to take data\n");
	print("\nYou must close stalled agent by hand ".
              "before attempting to re init $str\n");

	print "Continue?\n";
	my $response = get_yn();
	if ($response == 0){ die "Exiting\n";}
      }
    }
  }
  #print ">>>Exiting sub start_agents\n";
  return $mce4_agent_up;
}#endsub start_agents


sub tee_start_agents{
  #print ">>>Entering sub tee_start_agents\n";
  my $xtp       = "xterm -fn 6x12 -sb -sl 2000 -display $daemon_display";
  my $xtp1      = "xterm -fn 6x12 -geometry 112x6+1+0 -sb -sl 2000 -display $daemon_display";
  my $xtp2      = "xterm -fn 6x12 -geometry 100x6+1+110 -sb -sl 2000 -display $daemon_display";
  my $xtp3      = "xterm -fn 6x12 -geometry 100x6+1+220 -sb -sl 2000 -display $daemon_display";
  my $xtp4      = "xterm -fn 6x12 -geometry 87x15+1+325 -sb -sl 2000 -display $daemon_display";

  my $ls208_str = ($xtp1 . "-n TEMP -T TEMP -e " .
		   "$ENV{UFLS208D} -chan \"1, 2, 4, 5, 6\" " .
		   "$ENV{DAEMON_TSHOST} $ENV{IOCOMM} " .
		   "$ENV{DAEMON_TSPORT} $ENV{UFLS208D_PORT} &"
		  );

  my $vac_str = ($xtp . "-iconic -n VAC -T VAC -e " .
		 "$ENV{UFVACUUMD} " . 
		 "$ENV{DAEMON_TSHOST} $ENV{IOCOMM} " .
		 "$ENV{DAEMON_TSPORT} $ENV{UFVACUUMD_PORT} &"
		);

  my $motor_str = ($xtp2 . "-n MOTOR -T MOTOR -e " .
		   "$ENV{SH_MOTORD} /tmp/xt_motor.txt &"
		  );

  my $mce4_str = ($xtp3 . "-n MCE4 -T MCE4 -e " .
		  "$ENV{SH_MCE4D} /tmp/xt_mce4.txt &"
		 );

  my $rec_temp_str = ($xtp4 . "-n RECORD_TEMPS -T RECORD_TEMPS -e ".
		      "/usr/local/flamingos/perl_all_tel/Sh_rec_temp &" );

  #print "$ls208_str\n$vac_str\n$motor_str\n$mce4_str\n";

  print("\nAbout to initialize agents.\n\n".
	"All of the following items should be on:\n".

	"the detector controller (MCE4),\n".
	"the temperature sensor (LakeShore 208),\n".
	"the motor contoller.\n\n");

  print "Continue? ";
  my $response = get_yn();
  if ($response == 0){ die "Exiting\n";}

  #run them next; need tee off to capture listening msg
  my $pslist = `ps -ef`;

  my $look_for_ps = index($pslist,"ufls208d");
  if($look_for_ps >= 0){
    print "LakeShore Temperature agent already running\n\n";
  }elsif ($look_for_ps < 0){
    print "Executing LakeShore Temperature agent\n";

    system($ls208_str);
    print "sleep 5 to start agent\n";
    sleep 5;
    print "\n";
  }

  $look_for_ps = index($pslist, "record");
  if( $look_for_ps >= 0){
    print "record.temperatures.pl already running\n";
  }elsif( $look_for_ps < 0){
    print "starting temperature logging; please leave running\n";
    system( $rec_temp_str );
    print "sleep 5\n";
    sleep 5;
    print "\n";
  }

  my $xt_motor_out = "/tmp/xt_motor.txt";
  my @agent_arr = qw(0);
  $look_for_ps = index($pslist,"ufmotord");
  if($look_for_ps >= 0){
    print "Motor agent already running\n\n";
  }elsif ($look_for_ps < 0){
    push(@agent_arr, $xt_motor_out);

    my $sh_file = $xt_motor_out;
#dbg
#    if(-e $sh_file){
#      print "Removing previous log file $sh_file\n\n";
#      my $rm_reply   = `rm $sh_file`;
#    }

    print "Executing motor agent:\n";
    #print "Executing motor agent:\n$motor_str\n";
    system($motor_str);
    print "sleep 5 to start agent\n";
    sleep 5;
    print "\n";

    #make sure other users of script don't have trouble
    my $chmodstr = "chmod a+wx $sh_file";
    system($chmodstr);
  }

  #$look_for_ps = index($pslist,"ufvacuumd");
  #if($look_for_ps >= 0){
  #	print "Vacuum agent already running\n\n";
  #}elsif ($look_for_ps < 0){
  #	print "Executing vacuum agent\n";
  #	system($vac_str);
  #   print "sleep 5 to start agent\n";
  #   sleep 5;
  #   print "\n";
  #}

  my $xt_mce4_out = "/tmp/xt_mce4.txt";
  $look_for_ps = index($pslist,"ufmce4d");
  if($look_for_ps >= 0){
    print "MCE4 agent already running\n\n";
  }elsif ($look_for_ps < 0){
    push(@agent_arr, $xt_mce4_out);

    my $sh_file = $xt_mce4_out;
#    if(-e $sh_file){
#      print "Removing previous log file $sh_file\n\n";
#      my $rm_reply   = `rm $sh_file`;
#    }

    print "Executing MCE4 agent:\n";
    #print "Executing MCE4 agent:\n$mce4_str\n";
    system($mce4_str);
    print "sleep 5 to start agent\n";
    sleep 5;
    print "\n";

    #make sure other users of script don't have trouble
    my $chmodstr = "chmod a+wx $sh_file";
    system($chmodstr);
  }

  #run a dummy process
  #$look_for_ps = index($pslist,"XT1");
  #if($look_for_ps >= 0){
  #  print "xt1 agent already running\n\n";
  #}elsif ($look_for_ps < 0){
  #  print "Executing xt1\n";
  #  system('xterm -fn 6x12 -n XT1 -iconic &');	
  #  print "sleep 5 to start xt1\n";
  #  sleep 5;
  #  print "\n";
  #}
  #print ">>>Exiting sub tee_start_agents\n";
  return \@agent_arr;
}#endsub tee_start_agents


sub did_agents_start{
  #print ">>>Entering sub did_agents_start\n";
  my $file_name = $_[0];

  my $infile =  $file_name;
  #print "did agents start infile is $infile\n";
  my $listenumber = -1;
  my $reply       =  0;

  open(AGENT_FILE, $infile) || die "Can't open file $infile:\n $!\n";
  while( <AGENT_FILE> ){
    $listenumber = index($_, 'listening on port');
  }
  close AGENT_FILE;

  if($listenumber != -1){
    $reply = 1; #saw listening on port statement
  }else{
    $reply = 0;
  }
  #print ">>>Exiting sub did+agents_start\n";
  return $reply;
}#endsub did_agents_start


sub initialize_mce4{
  my @mce_vars = ($ENV{DEFAULT_NREADS},
		  $ENV{DEFAULT_NFRAMES},
		  $ENV{DEFAULT_EXPT});

  my @mce_fixed_pars = ($ENV{SIM_MODE},
			$ENV{PBC},'dac_set_all_preamp 190',
			$ENV{DLAP},
			$ENV{LDVAR_0_RESETS},
			$ENV{LDVAR_1_RESETS},
			$ENV{DEFAULT_CT}
		       );

  my $space = " ";

  ###Set mce_fixed_pars
  my $str_quote   = "\"";
  my $submit_list = $str_quote . join( " & ", @mce_fixed_pars) . $str_quote;
  my $mce_head = $ENV{UFDC_DO_BASE}.
                 $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                 $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                 $ENV{CLIENT_RAW}.$space;

  my $mce_cmd = $mce_head . $submit_list;

  print("Set the following fixed mce parameters\n $mce_cmd\n" .
	"(mode, pixel timing, latch preamp dacs,\n" .
	"resets before first cds read (10) and after second cds read (1000)," .
	"and cycle type 40)\n\n");

  my $reply = `$mce_cmd`;
  #print "sleep 2 before setting next parameters\n";
  mce_agent_sleep(2);


  ###Set mce_var_pars
  $submit_list = $str_quote . join( " & ", @mce_vars) . $str_quote;
  $mce_cmd = $mce_head . $submit_list;

  print "Set the following frequently changed mce parameters\n $mce_cmd\n";
  print "(2 reads per cds read, 1 frame, 5 second integration time)\n\n";

  $reply = `$mce_cmd`;
  mce_agent_sleep(2);

  ###Set bias?
  Start_setbias();
  mce_agent_sleep(2);

  ###CT 0 Idle mode
  print "Start mce idling in constant reset mode (CT 0)\n";

  $submit_list = $str_quote . "CT 0 & START" . $str_quote;
  $mce_cmd = $mce_head . $submit_list;

  print "$mce_cmd\n";
  $reply = `$mce_cmd`;
  print "sleep 2 before setting next parameters\n";
  mce_agent_sleep(2);


  ###Sync the two edt frame grabber cards
  my $init_edt_str = $ENV{INIT_EDT};
  print "Initializing EDT frame grabber\n";
  print "Executing $init_edt_str\n";

  mce_agent_sleep(2);
  $reply = `$init_edt_str`;


  print("\nIf initialization of EDT card was successful, ".
	"you should see 10 lines of text above,\n".
	"beginning with \"opening pdv unit 0....\",".
	"and ending with \"done\"\n");

  print "Continue?";
  my $response = get_yn();
  if ($response == 0){ die "Exiting\n";}

}#endsub initialize_mce4



sub mce_agent_sleep{
  my $wait_time = 5;

  if (defined $_[0]){
    $wait_time = $_[0];
  }

  my $waitcnt = 0;
  while( $waitcnt++ < $wait_time ) { 
    print"Sleep $waitcnt of $wait_time\n";
    sleep 1; 
  }
  print "\n";

}#endsub mce_agent_sleep


__END__

=head1 NAME

mmt.initflam.pl

=head1 Description

Sets up the daemons.  Specfic to the mmt for
the particular configuation of monitors.


=head1 REVISION & LOCKER

$Name:  $

$Id: mmt.initflam.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

