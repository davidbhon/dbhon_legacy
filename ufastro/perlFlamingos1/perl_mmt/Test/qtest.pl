#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: qtest.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use mmtTCSinfo qw/:all/;

my $d_x =  -5;
my $d_y = -10;
my $sock = mmtTCSinfo::tcsops_connect();


my $result = do_sub( $sock, $d_x, $d_y );


sub do_sub{
  my ( $sock, $d_x, $d_y )= @_;

  my $space = " ";
  my $quote = "'";
  my $offset_cmd = $quote."1 instrel".$space.$d_x.$space.$d_y.$quote;

  print "offset cmd = :$offset_cmd:\n";
}#Endsub do_sub
