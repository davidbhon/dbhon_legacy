#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: connection.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use IO::Socket;

my $sock = make_connection();
print "main sock is $sock\n";


sub make_connection{
  my $tcs_host_name = $ENV{TCS_SERVER_NAME};
  my $tcs_host_ip   = $ENV{TCS_SERVER_IP};
  my $tcs_host_port = $ENV{TCS_SERVER_PORT};

  my $sock = IO::Socket::INET->new (
				    PeerAddr => $tcs_host_ip,
				    PeerPort => $tcs_host_port,
				    Proto => 'tcp');
  if( !defined $sock ){
    $sock = "socket_not_defined";
  }

  print "sock is $sock\n";
  return $sock;
}#Endsub make_connection


sub foo{
  my $sock = $_[0];
  if( defined($sock) ){
    print "\n\n";
    print "tcp socket address is $sock\n";
  }else{
    print "\n\n";
    print "Unable to connect to MMT TCS host; $!\n\n".
      "Either fix the problem, or use config.output.pl\n".
	"to set this script to not ask for tcs info.\n\n";
    
    print "Continue?\n";
    print "(If dithering continuing will leave tcs info values in header at last known values) ";
    use GetYN;
    my $reply = GetYN::get_yn();
    if( $reply == 0 ){
      die "\n\nExiting\n\n";
    }else{
      $sock = q(do_not_update_tcs_info);
    }
  }
}#Endsub foo

