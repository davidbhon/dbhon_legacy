#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: alpha.dec.relative.offset.mmt.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use mmtTCSinfo qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
  print_usage();

}elsif( $cnt == 1){
  my $arg = shift;
  unless( $arg =~ m/now/i ){print_usage()};

  #Get present values
  my $get_instazoff = "1 get instazoff";
  my $get_insteloff = "1 get insteloff";

  my $sock = mmtTCSinfo::tcsops_connect();
  my $instazoff = mmtTCSinfo::tcsops( $get_instazoff, $sock, "not_quiet" );
  my $insteloff = mmtTCSinfo::tcsops( $get_insteloff, $sock, "not_quiet" );
  close( $sock );

  $instazoff = mmtTCSinfo::parse_tcs_return( $instazoff );
  $insteloff = mmtTCSinfo::parse_tcs_return( $insteloff );

  print "\n";
  print "The present offsets:  (d_x, d_y) = ($instazoff, $insteloff)\n";
  print "\n";

}elsif( $cnt == 2 ){
  my $d_x = shift;
  my $d_y = shift;

  if( $d_x =~ m/[a-zA-Z]/ or $d_y =~ m/[a-zA-Z]/ ){
    print "\n\nDid you enter a non-numerical offset by mistake?".
          "\n\nd_x = $d_x; d_y = $d_y\n\n";
    print_usage();

  }else{
    my $sock      = mmtTCSinfo::tcsops_connect();
    my $ops_reply = mmtTCSinfo::instrel_offset( $sock, $d_x, $d_y );
    close( $sock );

    mmtTCSinfo::check_tcs_returned_ack( $ops_reply );
  }
}else{
  print_usage();
}
###################
sub print_usage{
  die "\n\tUSAGE:".
      "\n\tOffset the telescope relative to its present position.".
      "\n\tThe motion will be in RA/DEC, rotated by the present rotator position,".
      "\n\twhich effectively moves the telescope in detector coordinates, x and y.".
      "\n".
      "\n\tTo get present offsets type: relative.offset.mmt.pl now".
      "\n\tTo   apply new offsets type: relative.offset.mmt.pl d_x_arcsec d_y_arcsec".
      "\n".
      "\n\te.g.".
      "\n\trelative.offset.mmt.pl 5 10 will move +5\" in X, -10\" in Y".
      "\n".
      "\n\tIf Position angle =    0, then the motion will be 5\" W, 10\" N".
      "\n\tIf Position angle = -180, then the motion will be 5\" E, 10\" S".
      "\n\n";
}#Endsub print_usage
