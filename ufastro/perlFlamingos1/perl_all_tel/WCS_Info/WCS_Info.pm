package WCS_Info;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use WCS_Info ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS =
  ( 'all' => [ qw(
		  &set_wcs_info &hms2deg &dms2deg
		  &gem_set_wcs_info
		 )
	     ]
  );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);


use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
sub set_wcs_info{
  my ( $RA, $DEC, $header ) = @_;

  my $ra_deg  = hms2deg( $RA );
  my $deg_deg = dms2deg( $DEC );


  setNumericParam( "CRVAL1", $ra_deg  );
  setNumericParam( "CRVAL2", $deg_deg );

  my $chip_pa = param_value( 'CHIP-PA' );
  my $rot_pa  = param_value( 'ROT_PA'  );

  my $crota1  = 180 - ($rot_pa + $chip_pa);
  $crota1 = sprintf "%.5f", $crota1;

  setNumericParam( "CROTA1", $crota1 );
}#Endsub set_wcs_info


sub gem_set_wcs_info{
  my ( $ra_deg, $dec_deg, $instrpa, $header ) = @_;

  setNumericParam( "CRVAL1", $ra_deg  );
  setNumericParam( "CRVAL2", $dec_deg );

  my $crota1  = -1*$instrpa;
  $crota1 = sprintf "%.5f", $crota1;

  setNumericParam( "CROTA1", $crota1 );
}#Endsub gem_set_wcs_info


sub hms2deg{
  my $input = $_[0];

  my $colon = index $input, ':' ;
  my $hour  = substr $input, 0, $colon;

  my $min_sec = substr $input, $colon+1;
     $colon = index $min_sec, ':' ;
  my $min   = substr $min_sec, 0, $colon;

  my $sec   = substr $min_sec, $colon+1;
     $colon = index $sec, ':' ;
     $sec   = substr $sec, 0, $colon;

  my $deg = ( $hour + ($min + $sec/60)/60 ) * 360 / 24;
  $deg = sprintf "%.5f", $deg;
  return $deg;
}#Endsub hms2deg


sub dms2deg{
  my $input = $_[0];

  my $colon = index $input, ':' ;
  my $deg  = substr $input, 0, $colon;

  my $min_sec = substr $input, $colon+1;
     $colon = index $min_sec, ':' ;
  my $min   = substr $min_sec, 0, $colon;

  my $sec   = substr $min_sec, $colon+1;
     $colon = index $sec, ':' ;
     $sec   = substr $sec, 0, $colon;

  my $sign = -1 if( $deg < 0 );
     $sign = +1 if( $deg >= 0);

  $deg = $sign * (abs($deg) + ( $min + $sec/60 )/ 60);
  $deg = sprintf "%.5f", $deg;
  return $deg;
}#Endsub dms2deg

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

WCS_Info - Perl extension for Flamingos

=head1 SYNOPSIS

  use WCS_Info qw/:all/;

=head1 DESCRIPTION

  Given the Ra and Dec in hh:mm:ss format, and the name of the header file, convert Ra and Dec decimal degrees, and stick them into the CRVAL1 and 2 header keywords

=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: WCS_Info.pm,v 0.1 2003/05/22 15:37:20 raines Exp $

$Locker:  $

=head1 AUTHOR

S. N. Raines, Nov 2001 & Jan 2002

=head1 SEE ALSO

perl(1).

=cut
