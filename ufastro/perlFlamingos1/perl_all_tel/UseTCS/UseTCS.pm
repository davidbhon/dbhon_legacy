package UseTCS;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use UseTCS ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
				   &make_socket_connection
				   &use_tcs
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
sub use_tcs{
  my ( $use_tcs, $header, $telescop ) = @_;

  if( $use_tcs ){
    ####call to tcs subroutines goes here
    ####Must match all 18 characters in header
    if( $telescop eq '4m                ' ){
      kpnoTCSinfo::getKPNOtcsinfo( $header );

    }elsif( $telescop eq '2m                ' ){
      kpnoTCSinfo::getKPNOtcsinfo( $header );

    }elsif( $telescop eq 'MMT               ' ){
      mmtTCSinfo::getMMTtcsinfo( $header );

    }elsif( $telescop eq 'Gemini-South      ' ){
      GemTCSinfo::getGemTCSinfo( $header );

    }else{
      print "\a";
      print "WARNING WARNING WARNING WARNING WARNING\n";
      print "Not at either 4m or 2m at KPNO, nor at \n";
      print "MMT or Gemini-South\n\n";
      print "Not able to get any tcs info\n\n";
    }
  }else{
    print "___NOT___ getting tcs info\n\n";
  }

}#Endsub use_tcs


sub make_socket_connection{
  my ($telescop, $VIIDO) = @_;
  my $soc;

  if( $telescop eq 'Gemini-South      ' ){
    if( $VIIDO ){
      print "Connecting to the VII\n";
      $soc = ufgem::viiconnect();
      if( !defined $soc || $soc eq "" ) {
	print "?Bad socket connection to Gemini VII server\n";
      }
    }else{
      print "Viido = $VIIDO; should be doing vii test connection\n\n";
    }

  }elsif( $telescop eq 'MMT               ' ){
    if( $VIIDO ){
      print "\n";
      print "Connecting to the tcs\n";
      $soc = mmtTCSinfo::tcsops_connect();
      if( $soc eq $mmtTCSinfo::socket_undefined ) {
	die "\n\n\tWARNING WARNING WARNING\n".
	  "\tCannot connect to $ENV{THIS_TELESCOP} TCSserver\n".
	    "\tsocket = $soc; $!\n\n";
      }
    }else{
      print "variable viido = $VIIDO. Should be making tcs connection\n\n";
    }
  }

  return $soc;
}#Endsub make_socket_connection

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

UseTCS - Perl extension for Flamingos data acquisition

=head1 SYNOPSIS

  use UseTCS qw/:all/;

=head1 DESCRIPTION

  use_tcs( $use_tcs, $header, $telescop);

  Where 
  $use_tcs   = <0|1>,
  $header    = filename of the the default fitsheader, including complete path
  $telescope = the value of the previously read TELESCOP fits header keyword, 
               see if it matches any of the following:
               '4m                '
               '2m                '
               'mmt               '
               'Gemini-South      '

  If it matches, then call the appropriate perl module to connect to the TCS
  and request the tcs info, then write it into the header.

=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: UseTCS.pm,v 0.1 2003/05/22 15:36:33 raines Exp $

$Locker:  $


=head1 AUTHOR

S. N. Raines 18 Jan 2002

=head1 SEE ALSO

perl(1).

=cut
