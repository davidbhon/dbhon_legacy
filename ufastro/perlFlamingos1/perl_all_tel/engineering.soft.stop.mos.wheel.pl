#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use MosWheel qw/:all/;


my $stop_cmd = $ENV{MOTOR_B}."@";

my $cmd = $MosWheel::cmd_head.$MosWheel::str_quote.
          $stop_cmd.$MosWheel::str_quote;

print "\n\nEXECUTING a soft stop of the mos motor:\n$cmd\n\n\n";

print "If this fails try ".
      "$ENV{UFMOTOR} $ENV{CLIENT_HOST} $ENV{HOST} ".
      "$ENV{CLIENT_PORT} $ENV{CLIENT_UFMOTOR_PORT}\n";
print "Type $ENV{MOTOR_B}(ESC) at the ufmotor prompt.\n\n";
print "Type quit to exit from ufmotor.\n\n";
print "If that fails cycle the power on the baytech.\n\n";


system( $offset_cmd );

__END__

=head1 NAME

engineering.soft.stop.mos.wheel.pl

=head1 DESCRIPTION

Sends a soft stop signal to the FLAMINGOS-1 motor controller/
indexer on the Mos Wheel mechanism.

=head1 REVISION & LOCKER

$Name:  $

$Id: engineering.soft.stop.mos.wheel.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
