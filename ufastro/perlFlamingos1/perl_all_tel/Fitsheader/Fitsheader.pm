package Fitsheader;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Fitsheader ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 
    'all' => [ qw(&select_header &select_lut &Find_Fitsheader
		  @fh_params @fh_pvalues @fh_comments
		  @FH_PARAMS %FH_VALUES %FH_COMMENTS
		  &printFitsHeader &readFitsHeader   &writeFitsHeader
		  &setStringParam  &setNumericParam
		  &modStringParams &modNumericParams &mod_ORIG_DIR
		  &param_index   &param_value 
		  &mod_THROW_PA_for_gem
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
###GLOBAL VARIABLES
our $Fitsheader_file;
our (@fh_params, @fh_pvalues, @fh_comments, @FH_PARAMS) = ();
our (%FH_VALUES, %FH_COMMENTS) = ();

###SUBROUTINES
sub select_header{
  my $DEBUG = $_[0];
  my $fitsheader_path;
  my $fitsheader_file = $ENV{FITS_HEADER_NAME};
  my $header;
  if( !$DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT};
    $header          = $fitsheader_path . $fitsheader_file;
  }elsif( $DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT_DEBUG};
    $header          = $fitsheader_path . $fitsheader_file;
    print "In debug mode\n";
    print "Header is $header\n";
  }
  return $header;
}#Endsub select_header

sub select_lut{
  my $DEBUG = $_[0];
  my $fitsheader_path;
  my $lut_file = $ENV{LUT_NAME};
  my $lut;
  if( !$DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT};
    $lut             = $fitsheader_path . $lut_file;
  }elsif( $DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT_DEBUG};
    $lut             = $fitsheader_path . $lut_file;
    print "In debug mode\n";
    print "Lut is $lut\n";
  }
  return $lut;
}#Endsub select_lut

sub Find_Fitsheader{
  $Fitsheader_file = $_[0];
  
  if( -e $Fitsheader_file and -r $Fitsheader_file ) {
    print "\nFound fits header template file:\n";
    print "$Fitsheader_file\n";
  }else{
    print "\n***\n***\n*** ERROR! \n";
    print "***file: $Fitsheader_file , \n";
    print "***does not exist or is not readable\n\n";
  }
}#Endsub define_FH_files


sub setStringParam{
  # 1st arg is name of string param, 2nd arg is new value of param.

  my ($new_pvalue);
  my ($vlen);
  my $iparm = param_index( $_[0] );

  if( $iparm < @fh_pvalues ){
    $new_pvalue = $_[1];

    if( defined $new_pvalue ){
      $vlen = length( $new_pvalue );
      if( $vlen < 18 ) {
	$fh_pvalues[$iparm] = '\'' . $new_pvalue. (' ' x (18-$vlen)) . '\'' ;
      }else{
	$fh_pvalues[$iparm] = '\'' . substr( $new_pvalue, 0, 18 ) . '\'';
      }
    }
  }else{
    print "$_[0] not in FITS header\n";
  }
}#Endsub setStringParams

sub setNumericParam{
  # 1st arg is name of numeric param, 2nd arg is new value.
  
  my ($new_pvalue);
  my ($vlen);
  my $iparm = param_index( $_[0] );
  
  if( $iparm < @fh_pvalues ){
    #make sure it is converted to string type.
    #$new_pvalue = ' ' . $_[1];   
    #The above two lines by Frank aren't necessary.  Perl knows how to
    #convert numbers and strings back and forth as necessary.
    #Most importantly, his code assumes this routine will never be handed
    #a number that is exactly 20 characters long--allowd in FITS format.

    $new_pvalue = $_[1];
    if( ($vlen = length( $new_pvalue )) > 0 ){
      if( $vlen < 20 ) {
	$fh_pvalues[$iparm] = (' ' x (20-$vlen)). $new_pvalue;
      }else{
	$fh_pvalues[$iparm] = substr( $new_pvalue, 20-$vlen, 20 );
      }
    }
  }else{ 
    print "$_[0] not in FITS header\n";
  }
}#Endsub setNumericParam


sub modStringParams{
# interactively modify string parameter values of FITS header.

  #print "Optionally modify FITS header STRING parameters:\n\n";
  print 
"__________________________________________________________________________\n";
  print "Enter new strings param values,\n";
  print "<= 18 characters, with NO quotes,\n";
  print "  or enter nothing (RETURN) to skip,\n";
  print "  or press ESCAPE and RETURN to quit,\n";
  print "  or press the UP ARROW key and RETURN at any time " . 
    "to go back in list.\n";
  print 
"__________________________________________________________________________\n";

  my $nparams = @_;
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  for( $i=0; $i<$nparams; $i++ ){
    $iparm = param_index( $_[$i] );
    print "\n";
    
    if( $iparm < $nvals ){
      print $fh_comments[$iparm];
      print "\n";
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      
      if( index( $new_pvalue, "\e[A" ) >= 0 ){
	$i -= 1;
	if( $i < 0 ) { $i=0 }
	redo;
      }

      if( $new_pvalue eq "\e" ){
	print " Leaving rest of FITS header unchanged\n";
	last;
      }
      
      if( ($vlen = length( $new_pvalue )) > 0 ){
	if( $vlen < 18 ) {
	  $fh_pvalues[$iparm] = '\'' . $new_pvalue . '\'' . (' ' x (18-$vlen));
	}else{
	  $fh_pvalues[$iparm] = '\'' . substr( $new_pvalue, 0, 18 ) . '\'';
	}
	print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	print "\n";
	$didmod = 1;
      }
    }else{ 
      print "$_[$i] not in FITS header\n";
    }
  }
  print "\n";
  return $didmod;
}#Endsub modStringParams


sub mod_ORIG_DIR{
  #interactively modify string parameter values of FITS header.
  
  print "\n";
  print 
"__________________________________________________________________________\n";
  print "Set ABSOLUTE path to which data will be written\n";
  print "The length must be less than 40 characters, including\n";
  print "LEADING AND TRAILING SLASHES\n\n";
    
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  $iparm = param_index( 'ORIG_DIR' );
  
  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      
      $vlen = length( $new_pvalue );
      if( $vlen > 0 and $vlen <= 40 ){
	my $lslash = index  $new_pvalue, "/";
	my $rslash = rindex $new_pvalue, "/";
	if( $lslash != 0 ){
	  print "The directory entered does not start with a slash\n";
	  print "Please enter an absolute path\n";
	}elsif( $rslash != $vlen -1 ){
	  print "The directory entered does not end with a slash\n";
	  print "Please end with a slash\n";
	}else{
	  $fh_pvalues[$iparm] = '\'' . $new_pvalue . '\'' . (' ' x (40-$vlen));
	  print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	  print "\n";
	  $is_valid = 1;
	  $didmod = 1;
	}
      }elsif( $vlen > 40 ){
	print "The path entered is > 40 characters long\n";
	print "Sorry, but you have to use a different directory\n";
      }elsif( $vlen <= 0 ){
	print "Not changing the current path\n";
	$didmod = 0;
	$is_valid = 1;
      }
    }else{
      print ">>>      WARNGING      WARNGING      <<<\n";
      print "Cannot find ORIG_DIR keyword in FITS header\n";
      print "NONE of the data taking scripts will work without this field\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $didmod;
}#Endsub mod_ORIG_DIR


sub mod_THROW_PA_for_gem{
  #interactively modify string parameter values of FITS header.
  
  print "\n";
  print 
"__________________________________________________________________________\n";
  print "Set THROW PA, where -180 <= THROW_PA <= +180\n";
  print "You must include either a - or a + sign\n";
    
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  $iparm = param_index( 'THROW_PA' );
  
  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      $vlen = length( $new_pvalue );
      
      if( $vlen > 0 ){
	#if( $new_pvalue =~ m/^(\-|\+)[0-9]*$/ ){
	if( $new_pvalue =~ m/^(\-|\+){0,1}[0-9]+\.{0,1}[0-9]*$/ ){
	  if( -180 <= $new_pvalue and $new_pvalue <= 180 ){
	    print "new_pvalue = $new_pvalue\n";
	    setNumericParam( "THROW_PA", $new_pvalue );
	    $is_valid = 1;
	    $didmod = 1;
	  }else{
	    print "\nPlease enter a value between -180 and +180, ".
	      "NOTE: Include the sign; for zero type +0\n";
	  }
	}else{
	  print "\nPlease enter a value between -180 and +180, ".
	    "NOTE: Include the sign; for zero type +0\n";
	}
      }elsif( $vlen <=0 ){
	print "Not changing the nod pa\n";
	$didmod = 0;
	$is_valid = 1;
      }
    }else{
      print ">>>      WARNGING      WARNGING      <<<\n";
      print "Cannot find ORIG_DIR keyword in FITS header\n";
      print "NONE of the data taking scripts will work without this field\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $didmod;
}#Endsub mod_THROW_PA_for_gem


sub modNumericParams{
# interactively modify numeric parameter values of FITS header.

  print 
"__________________________________________________________________________\n";
  print "Enter new integer values of numeric params,\n\n";
  #print " or enter nothing (RETURN) to skip,\n";
  #print " or press ESCAPE and RETURN to quit,\n";
  #print " or press the UP ARROW key and RETURN at any time " . 
  #  "to go back in list:\n\n";
  print 
"__________________________________________________________________________\n";

  
  my $nparams = @_;
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);

  for( $i=0; $i<$nparams; $i++ ){
    $iparm = param_index( $_[$i] );
    print "\n";
    
    if( $iparm < $nvals ){
      my $input_valid = 0;
      my $test;
      while( $input_valid == 0 ){
	print $fh_comments[$iparm];
	print "\n";
	print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . ' ? ';
	$new_pvalue = <STDIN>;
	chomp( $new_pvalue );

	my $input_len = length $new_pvalue;
	print "input len = $input_len\n";
	if( $input_len > 0 ){

	  $test = $new_pvalue;
	  $test =~ m/^(\d+)$/;
	  if( defined($1) ){
	    $input_valid = 1;
	  }else{
	    print "Please enter a number.\n";
	  }

	}else{
	  print "Leaving $fh_params[$iparm] unchanged\n";
	  $input_valid = 1;
	}

      }

      if( index( $new_pvalue, "\e[A" ) >= 0 ){
	$i -= 1;
	if( $i < 0 ) { $i=0 }
	redo;
      }
      
      if( $new_pvalue eq "\e" ){
	print " Leaving rest of FITS header unchanged\n";
	last;
      }
      
      if( ($vlen = length( $new_pvalue )) > 0 ){
	
	if( $vlen < 20 ) {
	  $fh_pvalues[$iparm] = (' ' x (20-$vlen)). $new_pvalue;
	}else{
	  $fh_pvalues[$iparm] = substr( $new_pvalue, 20-$vlen, 20 );
	}
	print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm];
	print "\n";
	$didmod = 1;
      }
    }else{ 
      print "$_[$i] not in FITS header\n";
    }
  }
  print "\n";
  return $didmod;
}#Endsub modNumericParams


sub param_index{
#takes one arg: single param being seached for, and returns index.

  my ( $param ) = $_[0];
  my $tlen = length( $param );
  my $nparms = @fh_params;
  my ($i);
  $param =~ tr/a-z/A-Z/;     #convert any lowercase to uppercase.

  if( $tlen < 8 ) {
    $param = $param . (' ' x (8-$tlen));

  }elsif( $tlen > 8 ){
    $param = substr( $param, 8 );
  }

  for( $i=0; $i<$nparms; $i++ ){
    if( $fh_params[$i] eq $param ) { last }
  }
  return $i;
}#Endsub pram_index


sub param_indices{
#input arg is list of params being seached for, and returns indices.

  my ( @params ) = @_;
  my (@indices);
  
  foreach my $param (@params){
    push( @indices, param_index($param) );
  }
  return @indices;
}#Endsub param_indices


sub param_value{
#input is single param being seached for, and returns value.
#all values stored as strings.

  my $value = $fh_pvalues[param_index($_[0])];   
  
  if( !$value ) {
    print "% param_value: input param=($_[0]) may be invalid, no value\n";
    return "";
  }
  if( (my $fq = index( $value, "'" )) >= 0 ){ 
    #get rid of quotes around string:
    $value = substr( $value, $fq+1, rindex( $value, "'" )-1 );
  }else{ 
    #convert to numeric:
    $value += 0;
  }   

  # so return type is same type as fits value: string or numeric.
  return $value;    
}#Endsub param_value


sub params_values{
  #input is list of params being seached for, and returns values.
  # return type is same type as fits value: string or numeric.
  my (@params) = @_;
  my (@values);
  
  foreach my $param (@params){
    push( @values, param_value($param) );
  }
  return @values;
}#Endsub params_values


sub printFitsHeader{
  #print FITS header arrays: @fh_params, @fh_pvalues, @fh_comments
  #if an array of param keys is passed, print corresponding records,
  my ($i);         # otherwise it prints all records.
  my $nrec = @fh_params;
  
  if( @_ ){
    my @indices = param_indices(@_);
    
    foreach $i (@indices){
      print $fh_params[$i] . '= ' . $fh_pvalues[$i] . $fh_comments[$i];
      print "\n";
    }
  }else{
    for( $i=0; $i<$nrec; $i++ ){
      if( $fh_params[$i] ne "COMMENT "){
      #if( $fh_params[$i] ne "SPARE   " ){
	print($fh_params[$i] . '= ' . 
	      $fh_pvalues[$i] . 
	      $fh_comments[$i] . "\n"
	      );
      }
    }
    print "\n";
  }
}#Endsub printFitsHeader


sub readFitsHeader{
  #one arg. is name of file containing FITS header, then
  # read header into arrays: @fh_params, @fh_pvalues, @fh_comments

  my ($fitsheader);
  open( IN, $_[0] ) || die "cannot open $_[0]\n";
  while( <IN> ) { $fitsheader = $_; }
  close( IN );

  my $slen = length( $fitsheader );
  my $nrec = $slen/80;

  my @fh_records;
  for( my $i=0; $i<$nrec; $i++ ){
    push( @fh_records, substr( $fitsheader, $i*80, 80 ) );
  }

  my $nrecs = @fh_records;

  if( $nrec ne $nrecs ){
    print "number of records is not what was expected\n";
    print "expected=$nrec , obtained=$nrecs\n";
  }

  my $loceq = index($fh_records[0],'=');
  my $loccom;
  #my $loccom = index($fh_records[0],' / ');
  my ($record);
  my ($fh_param);

  my $iparm;
  foreach $record (@fh_records){
    $iparm = param_index( $record );
    $loccom = index($fh_records[$iparm],' / ');
    push( @fh_params, substr( $record, 0, $loceq ) );
    $fh_param = substr( $record, 0, $loceq );
    push( @FH_PARAMS, $fh_param );
    push( @fh_pvalues, substr( $record, $loceq+2, $loccom-$loceq-2 ) );

    %FH_VALUES = ( %FH_VALUES, 
		   $fh_param=>substr( $record, $loceq+2, $loccom-$loceq-2 ) );

    push( @fh_comments, substr( $record, $loccom ) );

    %FH_COMMENTS = ( %FH_COMMENTS, $fh_param=>substr( $record, $loccom ) );
  }

}#Endsub readFitsHeader



sub writeFitsHeader{
  #write FITS header arrays: @fh_params, @fh_pvalues, @fh_comments,
  # to FITS header file, of name given by first and only arg.
  
  open( OUT, ">$_[0]" ) || die "cannot create $_[0]\n";
  print "File open successful\n";
  print "Updating FITS header\n";
  print "$_[0]\n";
  my ($i);
  my $nrec = @fh_params;
  
  for( $i=0; $i<$nrec-1; $i++ ){
    print OUT $fh_params[$i] . '= ' . $fh_pvalues[$i] . $fh_comments[$i];
  }
  
  #make sure FITS header is multiple of 36 lines (80 chars each).
  my $nb = $nrec/36;   
  
  if( ($i=index($nb,'.')) > 0 ){
    $nb = 1 + substr($nb,0,$i);
    for( $i=0; $i<(36*$nb-$nrec); $i++ ){ 
      print OUT "SPARE" . (' ' x 75);
    }
  }
  
  print OUT "END" . (' ' x 77);
  close( OUT );
}#Endsub writeFitsHeader



# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Fitsheader - Perl extension for dealing with fitsheaders for FLAMINGOS

=head1 SYNOPSIS

  use Fitsheader;
  
=head1 DESCRIPTION

Frank Varosis fitsheader routines rewritten as a module.
Calling scripts must supply the correct path to the fits
header template.

Other scripts should copy this file to the /data1/flamingos
and /data2/flamingos directories on flamingos1b, and maybe 
to the corresponding directories on flamingos1a, if the data
drives on flamingos1b get too full to use.

=head2 EXPORT

None by default.

=head2 EXPORT_OK

The tag :all will get the following:
		  
_Arrays of Fits header entries_
     @fh_params @fh_pvalues @fh_comments
     @FH_PARAMS %FH_VALUES %FH_COMMENTS

_Subroutines__
     &Find_Fitsheader($input) to see if $input exists
     &readFitsHeader  
     *****Execute readFitsHeader before any of the following
     *****This populates the above array variables

     &printFitsHeader
     &writeFitsHeader

  To modify single entries in the fits header:
     &setStringParam  
     &setNumericParam

  To modify groups of entries in the fits header:
     &modStringParams  returns 0 for no changes, 1 for changes
     &modNumericParams returns 0 for no changes, 1 for changes


=head1 REVISION & LOCKER

$Name:  $

$Id: Fitsheader.pm,v 0.1 2003/05/22 15:17:54 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR

=head1 SEE ALSO

perl(1).

=cut
