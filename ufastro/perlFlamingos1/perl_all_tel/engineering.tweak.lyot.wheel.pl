#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Inputs qw/:all/;
use Math::Round qw/ round /;

our $str_quote = "\"";
our $space     = " ";
our $ampersand = " & ";
our $caret     = "^";

our $cmd_head = $ENV{UFMOTOR}.$space.
                $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
                $ENV{CLIENT_QUIET}.$space;

my $cnt = @ARGV;
if( $cnt < 1 ){ 
  #assume -help
  die "\n\tUSAGE:\n".
      "\n\ttweak.lyot.pl (+|-)integer_offset".
      "\n\teg, tweak.lyot.pl +5, or tweak.lyot.pl -5".
      "\n\n\t(Will apply hold current to filter and grism wheels.)\n".
      "\t(If the script has to be interupted, please run the\n".
      "\t(script turn.off.motor.hold.current.pl.)\n\n";
}
else{
  my $offset = shift;

  my $reply = Inputs::check_alpha( $offset );
  if( !$reply ){ exit; }

  $reply = check_integer( $offset );
  if( !$reply ){ exit; }

  my ($sign, $value, $this_reply) = check_number_signs( $offset );
  if( !$this_reply ){ exit; }

  my $offset_cmd       = main::build_offset_cmd( $sign, $value );

  #print "\n\nSHOULD BE EXECUTING\n$offset_cmd.\n";
  print "\n\nEXECUTING\n$offset_cmd\n\n";
  print "If this script fails, please execute the script 'turn.off.motor.hold.current.pl'\n\n";
  system( $offset_cmd );

  my $motion = "";
  until( $motion eq "Stationary"){
    print "Sleeping 5 to allow motion\n";
    my $cnt = 1; 
    until( $cnt > 5 ){
      print "Sleep $cnt of 5\n";
      sleep(1); 
      $cnt++;
    }

    $motion = Get_Single_Motor_Motion( "d" );
    print "Wheel is $motion\n";
  }
  print "\n";
  Turn_off_hold_current();
}

#####SUBS
sub build_offset_cmd{ 
  my ( $sign, $integer_offset ) = @_;

  my $hold_cmd = $ENV{"MOVING_CURRENT_C"};
  $hold_cmd    = $hold_cmd.$ampersand.
                 $ENV{"MOVING_CURRENT_E"};

  my $i_v_cmd = $ENV{"I_3"}.$ampersand.$ENV{"V_3"};
  my $motion = $ENV{MOTOR_D}.$sign.$integer_offset;

  my $cmd = $cmd_head.$str_quote.$hold_cmd.$ampersand.
            $i_v_cmd.$ampersand.$motion.$str_quote;

  return $cmd;
}#Ensub build_offset_cmd


sub Get_Single_Motor_Motion{
  my $motor = $_[0];

  my $get_motion = $cmd_head.$str_quote.$motor.$caret.$str_quote;
  print "\n";
  print "Asking for motion.  Executing:\n";
  print "$get_motion\n\n";

  my $reply = `$get_motion`;
  chomp $reply;
  { local $/ = "\r"; chomp $reply }
  chomp $reply;
  sleep 1.0;

  my $caret_index  = index $reply, $caret;
  my $l_index    = length $reply;
  my $mot_val = substr $reply, $caret_index+1, $l_index - $caret_index ;

  if( $mot_val == 0  ){ return "Stationary" }
  if( $mot_val == 1  ){ return "Moving"     }
  if( $mot_val == 10 ){ return "Homing, Constant Vel" }
  if( $mot_val == 42 ){ return "Homing, Constant Vel, Ramping" }
}#Endsub Get_Single_Motion


sub Turn_off_hold_current{
  my $c_y_cmd = $ENV{NOT_MOVING_CURRENT_C};
  my $d_y_cmd = $ENV{NOT_MOVING_CURRENT_D};
  my $e_y_cmd = $ENV{NOT_MOVING_CURRENT_E};
  
  my $all_y_cmd = $c_y_cmd.$ampersand.$d_y_cmd.$ampersand.$e_y_cmd;
  my $net_cmd = $cmd_head.$str_quote.$all_y_cmd.$str_quote;

  print "\n\n";
  #print "SHOULD BE: \n";
  print "Setting the hold current to the camera motors to zero:\n";
  print "$net_cmd\n\n";

  my $reply = `$net_cmd`;
}#Endsub Turn_off_hold_current

__END__

=head1 NAME

engineering.tweak.lyot.pl

=head1 DESCRIPTION

Use to adjust the position of the Lyot wheel in
FLAMINGOS-1.  Used during instrument checkout at each
telescope.

=head1 REVISION & LOCKER

$Name:  $

$Id: engineering.tweak.lyot.wheel.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
