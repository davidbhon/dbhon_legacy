package MosWheel;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use MosWheel ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 
    'all' => [ qw( 
		   $str_quote
		   $space
		   $ampersand
		  
		  $MOS_SLIT_PN_REV
		  $KL_MDPT_POS              $KL_MDPT_NEG
		  
		  @MOS_SLIT_PN_NAMES
		  @MOS_SLIT_PN_BASE
		  @MOS_SLIT_PN_MDPTS
		  @MOS_SLIT_PN_OFFSET
		  @PN_MOS_SLIT

		  @MOS_SLIT_OPOS_NAMES      @MOS_SLIT_ONEG_NAMES 
		  @MOS_SLIT_OPOS_BASE       @MOS_SLIT_ONEG_BASE 
		  @MOS_SLIT_OPOS_MDPTS      @MOS_SLIT_ONEG_MDPTS
		  @MOS_SLIT_OPOS_OFFSET     @MOS_SLIG_ONEG_OFFSET
		  @OPOS_MOS_SLIT            @ONEG_MOS_SLIT 
		  
		  &get_mos_status
		  &get_mos_position_and_motion 
		  
		  &print_mos_info
		  &select_new_position      &print_mos_selection
		   
		 ) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
####MOS_SLIT
#Motion both in Plus/Neg from Image hole
#Numbers scaled for full rev value returned by controller
our $MOS_SLIT_PN_REV   = 97558;
our $KL_MDPT_POS =  52397;
our $KL_MDPT_NEG = -45161;

our @MOS_SLIT_PN_NAMES = qw(        K    9pix       H    20pix      F   12pix  
                                    D    2pix       B     6pix imaging   3pix
                                    Q       P       O        N      M       L );

our @MOS_SLIT_PN_BASE  =    (  -41543, -37918, -34300, -30682, -26251, -23438,
			       -19821, -16203, -12585,  -8675,      0,   8805,
			        12601,  19837,  27072,  34308,  41543,  48779 );

our @MOS_SLIT_PN_MDPTS =   (   -45161, -39731, -36109, -32491, -28467, -24845,
			       -21630, -18012, -14394, -10630,  -4338,   4403,
			        10703,  16219,  23455,  30690,  37926,  45161 );

our @MOS_SLIT_PN_OFFSET  =  (     -19,    -11,    -22,     43,   -854,     38,
				  -24,      0,    -11,     51,      0,     33,
				   16,     41,     46,     60,     14,     33 );
#our @MOS_SLIT_PN_OFFSET  =  (      19,     11,     22,    -43,    854,    -38,
#				   24,      0,     11,    -51,      0,    -33,
#				  -16,    -41,    -46,    -60,    -14,    -33 );
#Offsets updated 13 Nov 2001 SNR at NOAO
our @PN_MOS_SLIT;
for(my $i=0; $i < 18; $i++){
  $PN_MOS_SLIT[$i] = $MOS_SLIT_PN_BASE[$i] + $MOS_SLIT_PN_OFFSET[$i];
}

#overpositive range
our @MOS_SLIT_OPOS_NAMES = qw(      K    9pix       H   20pix       F   
				12pix       D    2pix	    B    6pix  );

our @MOS_SLIT_OPOS_BASE  =  (   56015,  59640,  63258,  66876,  71307,  
				74120,  77738,  81355,  84973,  88883  );

our @MOS_SLIT_OPOS_MDPTS =  (   57828,  61449,  65067,  69092,  72714,
				75929,  79547,  83164,  86928,  93221  );

our @MOS_SLIT_OPOS_OFFSET = (       0,      0,      0,      0,      0,
				    0,      0,      0,      0,      0  );

#overnegative range
our @MOS_SLIT_ONEG_NAMES = qw(      L       M       N       O       
				    P       Q       R  );

our @MOS_SLIT_ONEG_BASE  =   ( -48779,  -56015, -63250, -70486,
			       -77721,  -84957, -88753 );

our @MOS_SLIT_ONEG_MDPTS =   ( -54206,  -59633, -66868, -74104, 
			       -81339,  -86855, -92378 );

our @MOS_SLIT_ONEG_OFFSET =  (      0,       0,      0,      0,
				    0,       0,      0 );

#Build final vectors
our @OPOS_MOS_SLIT;
for(my $i=0; $i < 10; $i++){
  $OPOS_MOS_SLIT[$i] = $MOS_SLIT_OPOS_BASE[$i] + $MOS_SLIT_OPOS_OFFSET[$i];
}

our @ONEG_MOS_SLIT;
for(my $i=0; $i < 7; $i++){
  $ONEG_MOS_SLIT[$i] = $MOS_SLIT_ONEG_BASE[$i] + $MOS_SLIT_ONEG_OFFSET[$i];
}

########Subs
our $str_quote = "\"";
our $space     = " ";
our $ampersand = " & ";

our $cmd_head = $ENV{UFMOTOR}.$space.
                $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
                $ENV{CLIENT_QUIET}.$space;

use constant PI => 4 * atan2 1, 1;

sub get_mos_status{
  my ( $actual_pos, $motion, $closest_pos, $closest_name, $difference, $closest_index );

  print "\n";
  print "Asking for the mos wheel's present position & motion....\n\n";
  ($actual_pos, $motion) = get_mos_position_and_motion();
  print "\n\n";

  ($closest_pos, $closest_name, $difference, $closest_index) = det_mos_nearest_posn( $actual_pos);

  return ($actual_pos,  $motion,
	  $closest_pos, $closest_name, $difference, $closest_index);

}#Endsub get_mos_status


sub get_mos_position_and_motion{
  my $cmd_str = $cmd_head . $str_quote."bZ & b^".$str_quote;
  print "Executing\n$cmd_str\n";
  my $reply = `$cmd_str`;
  sleep 1.0;

  if( $reply eq "" ){
    print "Probably fatal invalid reply from previous command.\n";
    print "Continue anyways? ";
    my $cont = GetYN::get_yn();
    if( !$cont ){ die "Exiting.\n\n" }
  }

  my $z_index  = index $reply, 'bZ';
  my $m_index  = index $reply, 'b^';
  my $l_index  = length $reply;

  my $z_substr = substr $reply, $z_index;
  my $m_substr = substr $reply, $m_index, $l_index - $m_index;

  my $z_dot = index $z_substr, "\.";
  my $z_val = substr $z_substr, 2, $z_dot -2;

  #print "z_val, up to decimal point, is $z_val\n";
  #print "m_substr is $m_substr\n";

  my $m_dot = index $m_substr, "\n";
  my $m_val = substr $m_substr, 2, $m_dot - 2;

  my $interpretation = Interpret_Motion($m_val);
    
  return ($z_val, $interpretation);
}#endsub ge_mos_position_and_motion


sub Interpret_Motion{
  my $input = $_[0];

  #print "input is $input\n";
  if( $input == 0  ){ return "Stationary" }
  if( $input == 1  ){ return "Moving"     }
  if( $input == 10 ){ return "Homing, Constant Vel" }
  if( $input == 42 ){ return "Homing, Constant Vel, Ramping" }

}#Endsub Interpret_Motion


sub det_mos_nearest_posn{
  my $pos = $_[0];

  my $guess_index = 0;
  my @wheel_vals  = @PN_MOS_SLIT;
  my @wheel_names = @MOS_SLIT_PN_NAMES;
  my @wheel_mdpts = @MOS_SLIT_PN_MDPTS;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #check for too positive or too negative
  if( $pos >= $MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos - $MOS_SLIT_PN_REV;
      if( $pos < $MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }elsif( $pos <= -$MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos + $MOS_SLIT_PN_REV;
      if( $pos > -$MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }

  my $actual_pos = $pos;
  if( $pos >= $KL_MDPT_NEG and $pos <= $KL_MDPT_POS ){
    #print ">>>Checking against +- range\n";

    for( my $i = 1; $i < $num_posn; $i++ ){
      #print "i=$i, guess_index=$guess_index:  ";
      #print "checking posn=$pos against hp".($i-1).
      #      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";
      
      if( ($pos >= $wheel_mdpts[$i - 1]) and 
	  ($pos < $wheel_mdpts[$i] ) ){
	#print "in if $i\n";
	$guess_index = $i-1;
      }elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
	      $pos <= $KL_MDPT_POS ){
	#This checks between last position and home
	#print "checking upto KL_MDPT_POS\n";
	$guess_index = $num_posn - 1;
      }elsif( $pos < $wheel_mdpts[0] and
	      $pos >= $KL_MDPT_NEG      ){
	#print "checking upt KL_MDPT_NEG\n";
	$guess_index = 0;
      }
    }

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos > $KL_MDPT_POS and $pos < $MOS_SLIT_PN_REV ){
    #print "overplus\n";
    my $num_op = @OPOS_MOS_SLIT;
    
    for( my $i = 1; $i < $num_op; $i++ ){
      my $low_pos = $MOS_SLIT_OPOS_MDPTS[$i-1];
      my $hi_pos  = $MOS_SLIT_OPOS_MDPTS[$i];
      #print "i=$i, guess_index=$guess_index:  ";
      #print "checking pos=$pos against hp".($i-1).
      #	    "=$low_pos and hp$i=$hi_pos\n";

      if( $pos >= $low_pos and $pos < $hi_pos ){
	#print "in if $i\n";
	$guess_index = $i ;

	$closest_pos  = $OPOS_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_OPOS_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }elsif( $pos >= $MOS_SLIT_OPOS_MDPTS[$num_op -1] and
	      $pos < $MOS_SLIT_PN_REV ){
	$guess_index = 10;

	$closest_pos  = $PN_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }
    }

  }elsif( $pos < $KL_MDPT_NEG and $pos > -$MOS_SLIT_PN_REV ){
    #print "overneg\n";
    my $num_op = @ONEG_MOS_SLIT;
    
    #print "$pos, $ONEG_MOS_SLIT[0], $MOS_SLIT_ONEG_MDPTS[0]\n";

    if( $pos >= $ONEG_MOS_SLIT[0]  ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;

    }elsif( $pos <= $ONEG_MOS_SLIT[0] and $pos > $MOS_SLIT_ONEG_MDPTS[0] ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;
    }else{
      for( my $i = 1; $i < $num_op; $i++ ){
	my $low_pos = $MOS_SLIT_ONEG_MDPTS[$i-1];
	my $hi_pos  = $MOS_SLIT_ONEG_MDPTS[$i];
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking pos=$pos against hp".($i-1).
	#  "=$low_pos and hp$i=$hi_pos\n";
	
	if( $pos <= $low_pos and $pos > $hi_pos ){
	  #print "in if $i\n";
	  $guess_index = $i-1 ;
	  
	  $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}elsif( $pos <= $MOS_SLIT_ONEG_MDPTS[$num_op -1] and
		$pos > -$MOS_SLIT_PN_REV ){
	  $guess_index = 10;
	  
	  $closest_pos  = $PN_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}
      }
    }
  }

  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff, $guess_index );

}#Endsub det_mos_nearest_posn


sub print_mos_info{
  my ( $motor, $wheel, $actual_pos, $closest_pos, $closest_name, 
       $difference, $motion, $home_type ) = @_;

  #system("clear");
  print "\n\n";
  print 
">>>>>>>>>>>>>>>>>>>>>   PRESENT MOS-SLIT WHEEL STATUS   <<<<<<<<<<<<<<<<<<\n";
  print 
"__________________________________________________________________________\n";

  print "\t\t\t ACTUAL\t\tCLOSEST LIKELY\n";
  print "MOTOR\tWHEEL\t\tPOSITION\tPOSITION\  NAME   DIFFERENCE  MOTION".
        "\tHOME-TYPE\n";
  print "_____\t_____\t\t________\t____________________________________".
        "_________________\n";

  #PRINT OUT MOS_SLIT INFO
  print " $motor\t$wheel\t$actual_pos\t\t";
  print "$closest_pos\t$closest_name\t     $difference\t";
  print "     $motion\t";  

  if( $home_type == 0 ){
    print "Near-home\n";
  }elsif( $home_type == 1 ){
    print "Limit-switch\n";
  }

  print 
"__________________________________________________________________________\n\n";

}#Endsub print_mos_info


sub select_new_position{
  my $wheel_name = "MOS_SLIT";
  my @POS_NAMES = @MOS_SLIT_PN_NAMES;
  my @POS_VALS  = @PN_MOS_SLIT;
  my $num_pos   = @POS_NAMES;
  my $num_pos0  = $num_pos - 1;

  my $new_pos_val = PI;#this is the value returned if choose not to change
  my $new_pos_num = 0;

  my $is_valid = 0;

  print "\n\n";
  print "Known $wheel_name positions\n";
  print "Position #\tPosition name\n";
  for( my $i = 0; $i < $num_pos; $i++ ){
    if( $i >= 0 ){#if loop for when need to restrict range
      print "$i -------------- $POS_NAMES[$i]\n";
    }
  }

  until( $is_valid ){
    print "Select a position number: ";
    $new_pos_num = <STDIN>; chomp $new_pos_num;
    
    my $input_len = length $new_pos_num;
    if( $input_len == 0 ){
      print "Not changing $wheel_name position.\n";
      $is_valid = 1;
    }else{
      if( $new_pos_num >= 0 and $new_pos_num < $num_pos ){
	#print "if = $new_pos_num\n";
	$new_pos_val = $POS_VALS[$new_pos_num];
	$is_valid    = 1;
      }else{
	print "Please enter a number between 0 and $num_pos0:\n";
      }
    }
  }#End until is_valid
  return ( $new_pos_val, $new_pos_num );
  
}#Endsub select_new_position


sub print_mos_exec_msg{
  
  print "________________________________________________________________\n";

  print "Flamingos has had problems with it's wheels in the past, and\n";
  print "it has been useful to listen to them while they move.  If there's\n";
  print "an intercom or microphone near the dewar, consider turning it on.\n\n";

  print "There are two failure modes:\n";
  print "1) If a wheel moves by first homing to the limit switch, and\n";
  print "the switch fails, then the wheel will turn endlessly.\n\n";

  print "2)  If a wheel binds the griding noise should be audible\n";
  print "(likely candidate for this is the mos_slit wheel).\n\n";

  print ">>>>>  WHAT TO DO IF A FAILURE OCCURS\n";
  print "Type Ctrl-C to kill the script, and then type the following:\n";
  print "ufmotor $ENV{CLIENT_HOST} ".
        "$ENV{HOST} $ENV{CLIENT_PORT} $ENV{CLIENT_UFMOTOR_PORT}\n\n";

  print "At the ufmotor> prompt type b@\n";
  print "This should stop the mos motor\n";
  print "Then type exit to quit ufmotor.\n\n";

  print "In the event that this fails, log into the baytech and\n";
  print "cycle the power on the motor controller:\n";
  print "telnet baytech,  username> guest,  passwd>16Mb,img\n";
  print "reboot 3\n";
  print "logout\n\n";


  print "(Note that cycling the power on the motor controller will reset)\n";
  print "(the current position of all motors to zero.  Any wheels that  )\n";
  print "(use the Near home technique will probably be lost as well as  )\n";
  print "(the motor that requires this drastic action.                  )\n";
  print "________________________________________________________________\n\n";

  
}#Endsub print_mos_exec_msg


sub print_mos_selection{
  my ( $motor, $wheel, $closest_name, $new_name, $new_pos) = @_;

  print "The new configuation:\n";
  print "\t\t\tMOVE\t\tMOVE TO\n";
  print "Motor\tWheel\t\tFROM\t\tPOSITION   VALUE\n";
  print "______________________________________________________\n";

  print "$motor\t$wheel\t$closest_name\t\t$new_name\t$new_pos\n";
  print "_____________________________________________________\n";

}#Endsub print_mos_seletction

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

MosWheel - Perl extension for Flamingos mos-slit wheel perl motor code

=head1 SYNOPSIS

  use MosWheel qw/:all/;

=head1 DESCRIPTION



=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: MosWheel.pm,v 0.1 2003/05/22 15:28:30 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR Oct 2001

=head1 SEE ALSO

perl(1).

=cut
