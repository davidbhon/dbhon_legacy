package Constants;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Constants ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( PI
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
use constant PI => 4 * atan2 1, 1;

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Constants - Perl extension for Flamingos

=head1 SYNOPSIS

  use Constants qw/:all/;


=head1 DESCRIPTION

  Export the constant PI, required for wheel codes

=head2 EXPORT

None by default.


=head1 REVISION & LOCKER

$Name:  $

$Id: Constants.pm,v 0.1 2003/05/22 15:15:51 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR 10 Nov 2001

=head1 SEE ALSO

perl(1).

=cut
