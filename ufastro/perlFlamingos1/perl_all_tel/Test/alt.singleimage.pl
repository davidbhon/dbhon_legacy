#!/usr/local/bin/perl -w

use strict;
use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use MCE4_acquisition qw/:all/;
use GemTCSinfo qw/:all/;
use kpnoTCSinfo qw/:all/;
use mmtTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = Fitsheader::param_value( 'EXP_TIME' );
my $nreads          = Fitsheader::param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

my $orig_dir  = Fitsheader::param_value( 'ORIG_DIR' );
my $filebase  = Fitsheader::param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $next_index = $last_index + 1;
print "This image's index will be $next_index\n";

ImgTests::set_filename_in_header( $filebase, $next_index, $header );

#print "SHOULD BE SETTING UP MCE4\n\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

#>>>>Get TCS info
my $use_tcs  = Fitsheader::param_value( 'USE_TCS' );
my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n";
if( $use_tcs ){
  ####call to tcs subroutines goes here
  ####Must match all 18 characters in header
  if( $telescop eq 'Gemini-South      ' ){
    GemTCSinfo::getGemTCSinfo( $header );

  }elsif( $telescop eq '4m                ' ){
    kpnoTCSinfo::getKPNOtcsinfo( $header );

  }elsif( $telescop eq '2m                ' ){
    kpnoTCSinfo::getKPNOtcsinfo( $header );

  }elsif( $telescop eq 'MMT               ' ){
    mmtTCSinfo::getMMTtcsinfo( $header );

  }
}elsif( !$use_tcs ){
  put_date_time_in_header();
}

#>>>Trigger system to acquire image
my $acq_cmd = MCE4_acquisition::nods9_acquisition_cmd( $header, $lut_name, 
				$file_hint, $next_index, $net_edt_timeout);
MCE4_acquisition::acquire_image( $acq_cmd );
idle_lut_status( $expt, $nreads);

#move_file_for_index_sep( $file_hint, $next_index );#Not used now
my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
if( $unlock_param ){
  ImgTests::make_unlock_file( $file_hint, $next_index );
}

display_image( $file_hint, $next_index );

exit;#That's That & That's the remark

#_______________________________________________________________________
####
####SUBS FOLLOW
####
sub display_image{
  my ($file_hint, $this_index) = @_;

  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};
  my $index_str = parse_index( $this_index );
  my $netfilename = $file_hint.$index_sep.$index_str.$file_ext;
  print "This will be $netfilename\n";

  my $frame;
  print "this index = $this_index\n";
  my $len = length $this_index;
  my $last = substr $this_index, ($len - 1);
  print "last char is $last\n";

  if( $last == '1' or $last == '3' or $last == '5' or $last == '7' or $last == '9'){
    $frame = 1;
    #print "odd, frame = $frame\n";
  }elsif( $last == '0' or $last == '2' or $last == '4' or $last == '6' or $last == '8' ){
    $frame = 2;
    #print "even, frame = $frame\n";
  }

  my $xpaset_frame = "xpaset -p ds9 frame $frame";
  my $xpaset_file  = "xpaset -p ds9 file $netfilename";

  #print "SHOULD BE: Executing $xpaset_frame\n";
  print "Executing $xpaset_frame\n";
  my $rep1 = `$xpaset_frame`;

  #print "SHOULD BE: Executing $xpaset_file\n";
  print "Executing $xpaset_file\n";
  my $rep2 = `$xpaset_file`;

}#Endsub display_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	  "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n\n";
	print "IF it looks like it has stalled while applying the LUT,\n";
	print "run ufstop.pl -clean, and start over\n\n";
	print "Else if it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";
	exit;
      }#end time expired
    }

    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub put_date_time_in_header{
  my $date = `date -u '+%Y-%m-%d'`;
  chomp( $date );
  setStringParam("DATE",$date);

  my $time = `date -u '+%H:%M:%S'`;
  chomp( $time );
  setStringParam("TIME",$time);

  print "Writing date and time from $ENV{HOST} into header\n";
  print "$header\n\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub put_date_time_in_header

#rcsId = q($Name:  $ $Id: alt.singleimage.pl 14 2008-06-11 01:49:45Z hon $);
