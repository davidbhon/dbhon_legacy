#!/usr/local/bin/perl -w



force_cont_reseting();


sub force_cont_reseting{
  ###Set mce_fixed_pars
  my $str_quote   = "\"";
  my $space       = " ";

  my $mce_head = $ENV{UFDC_DO_BASE}.
                 $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                 $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                 $ENV{CLIENT_RAW}.$space;

  ###CT 0 Idle mode
  print "Start mce idling in constant reset mode (CT 0)\n";

  my $submit_list = $str_quote . "CT 0 & START" . $str_quote;
  $mce_cmd = $mce_head . $submit_list;

  print "SHOULD BE DOING:  $mce_cmd\n";
  #$reply = `$mce_cmd`;

}#Endsub force_cont_reseting

#rcsId = q($Name:  $ $Id: test.reset.pl 14 2008-06-11 01:49:45Z hon $);
