#!/usr/local/bin/perl -w

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use GemTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = param_value( 'EXP_TIME' );
my $nreads          = param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

#>>>Get dither info from header
my $dpattern = param_value( 'DPATTERN' );
my $d_scale  = param_value( 'D_SCALE'  );
my $d_rptpos = param_value( 'D_RPTPOS' );
my $d_rptpat = param_value( 'D_RPTPAT' );
my $startpos = param_value( 'STARTPOS' );

my $usenudge = param_value( 'USENUDGE' );
my $nudgesiz = param_value( 'NUDGESIZ' );

my $nod_sky = param_value( 'NOD_SKY'  );
my $throw    = param_value( 'THROW'    );
my $throw_pa = param_value( 'THROW_PA' );

#Load dither patterns
my $pat_name_len = length $dpattern;
print "pattern is $dpattern; length $pat_name_len\n";
my $ar_pat_x = load_pattern_x( $dpattern );
my $ar_pat_y = load_pattern_y( $dpattern );
my @pat_x = @$ar_pat_x;
my @pat_y = @$ar_pat_y;
my $len_pat = @pat_x;

for( my $i = 0; $i < $len_pat; $i++){
  print "$i: $pat_x[$i], $pat_y[$i]\n";
}

#>>>Get Filename info from header
my $orig_dir  = param_value( 'ORIG_DIR' );
my $filebase  = param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = does_dir_exist( $orig_dir );
my $last_index = whats_last_index($orig_dir, $filebase);
my $next_index = $last_index + 1;
print "This image's index will be $next_index\n";

#set_filename_in_header( $filebase, $next_index, $header );

#setup_mce4( $expt, $nreads );



####SUBS
sub load_pattern_x{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_X;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    $ret_ar_ref = \@pat_3x3_X;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_X;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_X;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_X;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_X;
  }

  return $ret_ar_ref;
}#Endsub load_pattern_x


sub load_pattern_y{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_Y;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_Y;
  }

  return $ret_ar_ref;

}#Endsub load_pattern_y

#rcsId = q($Name:  $ $Id: dither.object.sky.pl 14 2008-06-11 01:49:45Z hon $);
