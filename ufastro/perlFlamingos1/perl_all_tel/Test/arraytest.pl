#!/usr/local/bin/perl -w

use strict;

my @vect_x = ( 0, 1, 2, 3 );
my @vect_y = ( 1, 2, 3, 4 );

my @arr = ( \@vect_x, \@vect_y );

for( my $i = 0; $i < 4; $i++){
  #print "(x,y) $i = $vect_x[$i], $vect_y[$i] \n";
  print "array values:\n";
  print "($i,$i) = $arr[$i][$i]\n";
}

#rcsId = q($Name:  $ $Id: arraytest.pl 14 2008-06-11 01:49:45Z hon $);
