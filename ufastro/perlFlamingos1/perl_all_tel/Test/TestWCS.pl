#!/usr/bin/perl -w

use strict;
use Fitsheader qw/:all/;

my $header = q(/home/raines/Perl/flamingos.headers.lut/Flamingos.fitsheader);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

print "Start with:\n";
printFitsHeader( "CRVAL1" );
printFitsHeader( "CRVAL2" );

use WCS_Info qw( &set_wcs_info );
#use WCS_Info qw/:all/;

my $ra  = '21:36:51';
my $dec = '+57:28:02';

WCS_Info::set_wcs_info( $ra, $dec, $header );

print "Now have:\n";
printFitsHeader( "CRVAL1" );
printFitsHeader( "CRVAL2" );


#rcsId = q($Name:  $ $Id: TestWCS.pl 14 2008-06-11 01:49:45Z hon $);
