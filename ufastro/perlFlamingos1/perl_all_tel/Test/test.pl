#!/usr/local/bin/perl -w

use strict;

my @in_array = (70, 71, 72, -4.2, 73, 74);
my $min = find_min(\@in_array);
print "returned min = $min\n";


sub find_min{
  my $ar_in_array = $_[0];
  my @in_array = @$ar_in_array;

  my $min = $in_array[0];
  for( my $i = 1; $i<@in_array; $i++){
    print "Comparing last min, $min, to $in_array[$i]: ";
    if( ($in_array[$i] > 0) and ($in_array[$i] <= $min) ){
      print "min now = $in_array[$i]";
      $min = $in_array[$i];
    }
    print "\n";
  }

  my $integer = sprintf "%d", $min;
  print "integer value = $integer\n";

  return $integer;
}#Endsub find_min
