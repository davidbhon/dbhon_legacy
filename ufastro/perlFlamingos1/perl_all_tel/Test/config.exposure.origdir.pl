#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: config.exposure.origdir.pl 14 2008-06-11 01:49:45Z hon $);

###############################################################################
#
#Configure fits header parameters for next exposure
#object, filebase, exp_time, and nreads
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\nPresent exposure parameters are:\n";
get_present_dir();
printFitsHeader( "OBS_TYPE", "OBJECT", "FILEBASE",
		 "EXP_TIME",  "NREADS" );

my $didmod = 0;
my $redo = 0;

until( $redo ){
  print "\n\nChange DIRECTORY TO WRITE TO (y/n)";
  my $change_dir = get_yn();
  if( $change_dir ){
    change_dir_to_write_to();
  }

  if( modStringParams( "OBS_TYPE", "OBJECT", "FILEBASE" ) > 0 ){
    $didmod = 1 ;
  }

  my $expt = set_expt();
  if( $expt > 0 ){
    setNumericParam("EXP_TIME", $expt);
    $didmod += 1;
  }

  my $nreads ;
  if( $expt > 0 ){
    $nreads = set_nreads($expt);
  }else{
    my $unchanged_expt = get_unchanged_param('EXP_TIME');
    #print "Unchanged expt is $unchanged_expt\n";
    $nreads = set_nreads($unchanged_expt);
  }

  #print "set_nreads returned $nreads\n";
  if( $nreads > 0 ){
    setNumericParam("NREADS", $nreads);
    $didmod += 1;
  }

  set_CT();

  if( $didmod > 0 ){
    print "\n\nThe new set of exposure parameters are:\n";
    get_present_dir();
    printFitsHeader( "OBS_TYPE", "OBJECT", "FILEBASE",
		     "EXP_TIME",  "NREADS", "CYCLETYP" );
    print "\nAccept changes (y/n)?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

    #my $dothis1 = "cp -p $fitsheader_file /data1/flamingos ";
    #my $dothis2 = "cp -p $fitsheader_file /data2/flamingos ";
    #system($dothis1);
    #print "Copying FITS header to /data1/flamingos \n";
    #system($dothis2);
    #print "Copying FITS header to /data2/flamingos \n";

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub get_unchanged_param{
  #NOTE only works for numeric fields
  ##should use param_value() routine in Fitsheader
  my $input = $_[0];
  my $iparm = param_index( $input );
  #print "exptime index is $iparm\n";
  return $fh_pvalues[$iparm];

}#Endsub get_unchanged_exp


sub set_expt{
  my $iparm = param_index( 'EXP_TIME' );
  #print "exptime index is $iparm\n";
  print $fh_comments[$iparm] . "\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of seconds\n";
      }elsif( $new_value >= 2 ){
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter an exposure time >= 2 seconds\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }else{
      #print "Leaving EXP_TIME unchanged\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub set_expt


sub set_nreads{
  my $expt = $_[0];
  #print "Input expt is $expt\n";
  my $iparm = param_index( 'NREADS' );
  print $fh_comments[$iparm] . "\n";
  my $unchanged_nreads = $fh_pvalues[$iparm];
  #print "my unchanged nreads is $unchanged_nreads\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    #print "input len is $input_len\n";
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of reads\n";
      }elsif( $new_value <= $expt/2 ){
	#print ">expt is $expt; nreads is $new_value\n";
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter an integer number of reads <= EXP_TIME/2\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }elsif( $input_len == 0  ){
	#print "nreads was unchanged; check if allowed by expt\n";
	if( $unchanged_nreads > $expt/2 ){
	  print "Please enter an integer number of reads <= EXP_TIME/2\n";
	}
	$is_valid = 1;
    }else{
      #print "Leaving NREADS unchanged\n";
      $is_valid = 1;
    }
  }
  return $response;
}#Endsub set_nreads


sub set_CT{
  my $nreads = get_unchanged_param('NREADS');
  #print "nreads in set_ct is $nreads\n";
  if( $nreads == 1 ){
    setNumericParam("CYCLETYP", 40);
  }elsif( $nreads > 1 ){
    setNumericParam("CYCLETYP", 42);
  }else{
    print ">>>>> WARNING   WARNING <<<<<\n";
    print "Unable to set CYCLETYP in Fitsheader\n";
    print "nreads is not valid:  $nreads\n\n";
}

}#Endsub set_CT


sub change_dir_to_write_to{
  my $first_line = "#Please do not delete nor edit this file";

  print "Enter the absolute pathname of the directory to which data will be writtne\n";
  print ">>> CREATE DIR YOURSELF FIRST <<<\n";
  print ">>>  INCLUDE TRAILING SLASH   <<<\n";
  my $test_input = <STDIN>;
  chomp $test_input;
  print "test input is $test_input\n";
  
  $test_input = "'" . $test_input . "'";
  print "now $test_input\n";
  
  my $new_dir = $test_input;
  
  open OUT, ">original_file_dir.txt" or die "cannot open $!\n";
  print OUT $first_line;
  print OUT "\n";
  print OUT $new_dir;
  close OUT;

}#Endsub change_dir_to_write_to


sub get_present_dir{
  my $infile = 'original_file_dir.txt';
  my $line_input;
  
  open IN, $infile or die "Cannot open $!\n";
  while( <IN> ){
    $line_input = $line_input . $_;
    #print;
  }
  close IN;
  #print "\n";
  
  my $left = index $line_input, '\'';
  $left += 1;
  my $right = rindex $line_input, '\'';
  
  #print "left index = $left\nright index = $right\n";
  
  my $dir_name = substr $line_input, $left, $right-$left;
  
  print "DIRECTORY WRITTEN TO = ";
  print "$dir_name\n";

}#Endsub get_present_dir

