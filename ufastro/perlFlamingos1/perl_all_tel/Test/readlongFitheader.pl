#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: readlongFitheader.pl 14 2008-06-11 01:49:45Z hon $);

use Getopt::Long;

my $Input_fits_file = '';
GetOptions ('file=s' => \$Input_fits_file);

#$FITheaderFile = "Flamingos.fitsheader";
$FITheaderFile = $Input_fits_file;

print "\ninfile is $FITheaderFile\n";

open( IN, $FITheaderFile );
while( <IN> ) { $header = $_; }
close( IN );

$slen = length( $header );
print "\n # bytes in FITS header = $slen\n";

$nrec = $slen/80;
print "\n Nrecords = $nrec\n\n";

for( $i=0; $i<$nrec; $i++ )
  {
    push( @records, substr( $header, $i*80, 80 ) );
  }

$nrecs = @records;

if( $nrec ne $nrecs ) {print "number of records is not what was expected\n";}

for( $i=0; $i<$nrecs-1; $i++ )
  {
    $loceq = index($records[$i],'=');
    $tag = substr( $records[$i], 0, $loceq );
    print "$tag= ";
    $locslash = rindex($records[$i],'/ ');
    $value = substr( $records[$i], $loceq+2, $locslash-$loceq-3 );
    print "$value / ";
    $comment = substr( $records[$i], $locslash+2 );
    print "$comment\n";
  }

$tlen = length($tag);
$vlen = length($value);
$clen = length($comment);

print "$tlen\n";
print "$vlen\n";
print "$clen\n";

exit;

