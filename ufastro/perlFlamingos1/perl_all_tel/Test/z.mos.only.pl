#!/usr/local/bin/perl -w

use strict;
use Getopt::Long;
use MosWheel qw/:all/;

#----Load Variables-----------------------------------------------------------#
my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Fitsheader qw/:all/;
my $header = Fitsheader::select_header($DEBUG);
#$header = "/export/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
$header = "/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
my $home_type = Fitsheader::param_value( 'HT_1' );
$home_type = 1;
print "home_type is $home_type\n";
my $motor    = qw( b );
my $wheel    = qw( mos_or_slit );
my ( $actual_pos, $closest_pos, $closest_name, $difference, $motion );

#----Get Status, ask if want to change wheel----------------------------------#
#($actual_pos,  $motion,
# $closest_pos, $closest_name, $difference) = MosWheel::get_mos_status();

#MosWheel::print_mos_info( $motor, $wheel, 
#			  $actual_pos, $closest_pos, $closest_name,
#			  $difference, $motion );

#use GetYN;
#print "\n\nChange the mos-slit wheel configuration? ";
#my $change = GetYN::get_yn();
#if( $change == 0 ){
#  die "Exiting\n\n";
#}

#----Proceed with wheel change------------------------------------------------#
my $new_pos    = PI;
my $new_index  =  0;
my $new_name   = qw( nc );

#main::selection_loop();
#main::update_header();

#----Build home and motion command strings------------------------------------#
my $num_waits = 0;
my $case = "unknown";
my $backlash = $ENV{BACKLASH_1};

my $i_v_cmd  = $ENV{"I_1"}.$MosWheel::ampersand.$ENV{"V_1"}.
                           $MosWheel::ampersand;
my $home_cmd = $ENV{"NEW_FV_0"};

my @home_and_move;
my @motion_amount;

#main::test_limit_cases(); die "\n\n";
#main::test_near_home_cases();   die "\n\n";
#main::test_case_inputs(2);
main::test_near_home_case_inputs(13);

if( $home_type == 1 ){
  #USE LIMIT SWTICH
  my ($ar_home_and_move, $ar_motion_amount) = main::build_limit_home_and_move();
  @home_and_move = @$ar_home_and_move;
  @motion_amount = @$ar_motion_amount;

  #----Following loop for test purposes
  $num_waits = $home_and_move[0];
  print "\n\nCase = $case: Starting position = $actual_pos, ".
        "Requested new position = $new_pos\n\n";
  print "This request will take $home_and_move[0] steps,\n";
  print "consisting of the following motor controller sequences:\n\n";
  for(my $i = 1; $i <= $num_waits; $i++){
    #print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    print "$home_and_move[$i]\n";
  }
  print "\n\n";

}elsif( $home_type == 0 ){
  #USE NEAR HOME METHOD
  my ($ar_home_and_move, $ar_motion_amount) = main::build_near_home_and_move();
  @home_and_move = @$ar_home_and_move;
  @motion_amount = @$ar_motion_amount;


  #----Following loop for test purposes
  $num_waits = $home_and_move[0];
  print "\n\nCase = $case\n";
  print "Starting position = $actual_pos, ".
        "Requested new position = $new_pos\n\n";
  print "This request will take $home_and_move[0] steps,\n";
  print "consisting of the following motor controller sequences:\n\n";
  for(my $i = 1; $i <= $num_waits; $i++){
    #print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    print "$home_and_move[$i]\n";
  }
  print "\n\n";

}
#----Execute each motion and then wait for stationary twice in a row----------#
die;
move_and_wait_loop( \@home_and_move, \@motion_amount );


####SUBS######################################################################
sub selection_loop{
  my $redo       =  0;
  until( $redo ){
    ($new_pos, $new_index) = MosWheel::select_new_position();
    
    $new_name = $MosWheel::MOS_SLIT_PN_NAMES[$new_index];
    
#*****MosWheel::print_mos_exec_msg();
    MosWheel::print_mos_selection( $motor, $wheel, $closest_name, 
				   $new_name, $new_pos);
    
    print "\nEnter y to update the fits header template and ".
	  "move the mos-slit wheel, or\n";
    print "Enter n to redo your selection, or\n";
    print "Enter Ctrl-C to cancel\n";
    print "Entry:  ";
    
    $redo = GetYN::get_yn();
  }
}#Endsub selection_loop


sub update_header{
  main::set_MOS_SLIT_params();  
  print "\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub update_header


sub set_MOS_SLIT_params{
  my $name = $new_name;
  my $pos  = $new_pos;

  if( $name =~ m/imaging/ ){
    setStringParam( "SLIT", $name );
    setStringParam( "MOS",  $name );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\tPOS_B=$pos\n";
    print "New header param: MOS   =$name, ";
    print "\tPOS_B=$pos\n";
  }elsif( $name =~ m/pix/ ){
    setStringParam( "SLIT",   $name );
    setStringParam( "MOS",   'long-slit' );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\t\tPOS_B=$pos\n";
  }else{
    setStringParam( "MOS",  $name );
    setStringParam( "SLIT", 'mos' );
    setNumericParam( "POS_B", $pos );
    print "New header param: MOS   =$name, ";
    print "\t\tPOS_B=$pos\n";
  }

}#Endsub set_MOS_SLIT_params


sub build_limit_home_and_move{
  my @home_and_move;
  my @motion_amount;

  if( ($actual_pos < 0) and (abs($actual_pos) > $backlash) and ($new_pos < 0) ){
    limit_1a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) > $backlash) and 
	  ($new_pos >= 0) ){
    limit_1bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) < $backlash) and 
	  ($new_pos < 0 )){
    limit_2a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) < $backlash) and
	  ($new_pos >= 0 )){
    limit_2bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos < 0) ){
    limit_3a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos == 0) ){
    #case 3b
    #take out backlash and move by 0
    limit_3bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos > 0) ){
    limit_3bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos < 0) ){
    limit_4a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos == 0) ){
    limit_4bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos > 0) ){
    #case 4b and 4c do the same thing
    limit_4bc( \@home_and_move, \@motion_amount);

  }#end ifelse case block

  return( \@home_and_move, \@motion_amount );
}#Endsub build_limit_home_and_move


sub build_near_home_and_move{
  my @home_and_move;
  my @motion_amount;

  if( ($actual_pos < 0) and ($new_pos <= $actual_pos) ){
    near_home_1ab( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and ($new_pos > $actual_pos) and ($new_pos < 0) ){
    near_home_1c( \@home_and_move, \@motion_amount);
    #-----------
  }elsif( ($actual_pos < 0) and ($new_pos == 0) ){
    near_home_2( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and ($new_pos > 0) ){
    near_home_3( \@home_and_move, \@motion_amount);
  #============
  }elsif( ($actual_pos == 0) and ($new_pos < 0) ){
    near_home_4( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos == 0) ){
    near_home_5( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos > 0) ){
    near_home_6( \@home_and_move, \@motion_amount);
  #===========
  }elsif( ($actual_pos > 0) and ($new_pos < 0) ){
    near_home_7( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos == 0) ){
    near_home_8( \@home_and_move, \@motion_amount);
    #---------
  }elsif( ($actual_pos > 0) and ($new_pos <= $actual_pos) and ($new_pos > 0  )){
    near_home_9ab( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos > $actual_pos) ){
    near_home_9c( \@home_and_move, \@motion_amount);

  }#end ifelse case block

  return( \@home_and_move, \@motion_amount );
}#Endsub build_near_home_and_move


sub limit_1a{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < backlash < 0; new < 0
  $case = "1a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = abs( $actual_pos ) - $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b+$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_1a


sub limit_1bc{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < backlash < 0; new >= 0
  $case = "1bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = abs( $actual_pos ) - $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b+$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_1bc


sub limit_2a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual < 0; new < 0
  $case = "2a";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd.$home_cmd.$MosWheel::ampersand. "b-$motion1";

  my $motion2 = $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = "b+$motion2";

}#Endsub limit_2a


sub limit_2bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual < 0; new >= 0
  $case = "2bc";

  $$ar_hm[0] = 1; #number of waits

  my $motion1 = $new_pos;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd.$home_cmd.$MosWheel::ampersand."b+$motion1";


}#Endsub limit_2bc


sub limit_3a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual = 0; new < 0
  $case = "3a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_3a


sub limit_3bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual = 0; new >= 0
  $case = "3bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_3c


sub limit_4a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < 0 < actual; new < 0
  $case = "4a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = abs( $actual_pos ) + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b-$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_4a


sub limit_4bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < 0 < actual; new >= 0
  $case = "4bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = $actual_pos + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2]  = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_4bc

#-----
sub near_home_1ab{
  my ( $ar_hm, $ar_m ) = @_;
  #new < actual <= 0
  $case = "near_home_1ab; new < actual < 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_1ab


sub near_home_1c{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < new < 0
  $case = "near_home_1c; actual < new < 0";

  $$ar_hm[0] = 1;

  my $motion1 = abs( $new_pos );
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1";

}#Endsub near_home_1c


sub near_home_2{
  my ( $ar_hm, $ar_m ) = @_;
  #actua < new = 0
  $case = "near_home_2; actual < new = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $actual_pos );
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";;

}#Endsub near_home_2a


sub near_home_3{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < 0 < new
  $case = "near_home_3; actual < 0 < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $actual_pos ) + $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";;

}#Endsub near_home_2b


sub near_home_4{
  my ( $ar_hm, $ar_m ) = @_;
  #new < actual = 0
  $case = "near_home_4; new < actual = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1


  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_4


sub near_home_5{
  my ( $ar_hm, $ar_m ) = @_;
  #new = actual = 0
  $case = "near_home_5; new = actual = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1


  my $motion1 = $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_5


sub near_home_6{
  my ( $ar_hm, $ar_m ) = @_;
  #actual = 0 < new 
  $case = "near_home_6; actual = 0 < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";

}#Endsub near_home_6


sub near_home_7{
  my ( $ar_hm, $ar_m ) = @_;
  #new < 0 < actual
  $case = "near_home_7; new < 0 < actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_7


sub near_home_8{
  my ( $ar_hm, $ar_m ) = @_;
  #new = 0 < actual
  $case = "near_home_8; new = 0 < actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_8


sub near_home_9ab{
  my ( $ar_hm, $ar_m ) = @_;
  #0 < new <= actual
  $case = "near_home_9ab; 0 < new <= actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos - $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN+$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_9ab


sub near_home_9c{
  my ( $ar_hm, $ar_m ) = @_;
  #0 < actual < new
  $case = "near_home_9c; 0 < actual < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN+$motion1";

}#Endsub near_home_9c


#-----
sub test_limit_cases{

  for( my $i = 1; $i <= 12; $i++ ){
    test_case_inputs( $i );

    my ( $ar_home_and_move, $ar_motion_amount ) = main::build_limit_home_and_move();
    my @home_and_move = @$ar_home_and_move;
    my @motion_amount = @$ar_motion_amount;
    
    $num_waits = $home_and_move[0];
    print "---------\nCase $i = $case\n";
    print "actual = $actual_pos; new = $new_pos; bl = $backlash\n";
    print "Number of wait = $home_and_move[0]\n";
    for(my $i = 1; $i <= $num_waits; $i++){
      print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    }
  }
}#End test_limit_cases


sub test_case_inputs{
  my $i = $_[0];

  #set 1 cases; P << backlash
  if   ( $i == 1 ){ #case 1a
    $actual_pos = -1000; $new_pos = -2000; }
  elsif( $i == 2 ){ #case 1b
    $actual_pos = -1000; $new_pos = 0;     }
  elsif( $i == 3 ){ #case 1c
    $actual_pos = -1000; $new_pos = 2000;  }

  #set 2 cases; P > backlash
  elsif( $i == 4 ){ #case 2a
    $actual_pos = -50; $new_pos = -2000; }
  elsif( $i == 5 ){ #case 2b
    $actual_pos = -50; $new_pos = 0;     }
  elsif( $i == 6 ){ #case 2c
    $actual_pos = -50; $new_pos = 2000;  }

  #set 3 cases; P == 0;
  elsif( $i == 7 ){ #case 3a
    $actual_pos = 0; $new_pos = -2000; }
  elsif( $i == 8 ){ #case 3b
    $actual_pos = 0; $new_pos = 0;     }
  elsif( $i == 9 ){ #case 3c
    $actual_pos = 0; $new_pos = 2000;  }

  #set 4 cases; P > 0;
  elsif( $i == 10 ){ #case 4a
    $actual_pos = 1000; $new_pos = -2000; }
  elsif( $i == 11 ){ #case 4b
    $actual_pos = 1000; $new_pos = 0;     }
  elsif( $i == 12 ){ #case 4c
    $actual_pos = 1000; $new_pos = 2000; }

}#Endsub test_case_inputs


#-----
sub test_near_home_cases{

  for( my $i = 1; $i <= 13; $i++ ){
    test_near_home_case_inputs( $i );

    my ( $ar_home_and_move, $ar_motion_amount ) = main::build_near_home_and_move();
    my @home_and_move = @$ar_home_and_move;
    my @motion_amount = @$ar_motion_amount;
    
    $num_waits = $home_and_move[0];
    print "---------\nCase $i = $case\n";
    print "actual = $actual_pos; new = $new_pos; bl = $backlash\n";
    print "Number of wait = $home_and_move[0]\n";
    for(my $i = 1; $i <= $num_waits; $i++){
      print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    }
  }
}#End test_near_home_cases


sub test_near_home_case_inputs{
  my $i = $_[0];

  if   ( $i == 1 ){ #case 1ab
    $actual_pos = -1000; $new_pos = -2000; }

  elsif( $i == 2 ){ #case 1ab
    $actual_pos = -1000; $new_pos = -1000;     }

  elsif( $i == 3 ){ #case 1c
    $actual_pos = -1000; $new_pos = -500;  }

  elsif( $i == 4 ){ #case 2
    $actual_pos = -1000; $new_pos = 0; }

  elsif( $i == 5 ){ #case 3
    $actual_pos = -1000; $new_pos = 2000;     }

  elsif( $i == 6 ){ #case 4
    $actual_pos = 0; $new_pos = -2000;  }


  elsif( $i == 7 ){ #case 5
    $actual_pos = 0; $new_pos = 0; }

  elsif( $i == 8 ){ #case 6
    $actual_pos = 0; $new_pos = 2000;     }

  elsif( $i == 9 ){ #case 7
    $actual_pos = 1000; $new_pos = -2000;  }


  elsif( $i == 10 ){ #case 8
    $actual_pos = 1000; $new_pos = 0; }

  elsif( $i == 11 ){ #case 9ab
    $actual_pos = 2000; $new_pos = 1000;     }

  elsif( $i == 12 ){ #case 9ab
    $actual_pos = 2000; $new_pos = 2000;     }

  elsif( $i == 13 ){ #case 9c
    $actual_pos = 1000; $new_pos = 2000; }

}#Endsub test_near_home_case_inputs


sub move_and_wait_loop{
  my ($ar_home_and_move, $ar_motion_amount) = @_;
  my @home_and_move = @$ar_home_and_move;
  my @motion_amount = @$ar_motion_amount;
  
  my $wait = $home_and_move[0];
  
  for( my $which_wait = 1; $which_wait <= $wait; $which_wait++ ){
    my $timer = 0;
    my $sleep_time = 5; #seconds
    my $stationary_twice = 0;
    my $time_elapsed = 0;
    my $time_estimate = 
      compute_time_estimate($motion_amount[$which_wait], $sleep_time);

    my $motion_cmd = $MosWheel::cmd_head.$MosWheel::str_quote.
                     $home_and_move[ $which_wait ].$MosWheel::str_quote;

    print "****************************************************************\n";
    print "SHOULD BE EXECUTING:\n";
    print "Motion # $which_wait of $wait\n";
    print "$motion_cmd\n\n";
    ####my reply = `$motion_cmd`;###This does the motion

    until( $stationary_twice == 2 or $time_elapsed == $time_estimate ){
      print "This should not take longer than ";
      printf "%d", $time_estimate;
      print " seconds.\n";

      print ">>Sleeping $sleep_time before checking status\n\n";
      sleep $sleep_time; $timer += $sleep_time;

      print "Have waited $timer seconds so far.\n\n";

      ($actual_pos,  $motion,
       $closest_pos, $closest_name, $difference) = MosWheel::get_mos_status();
      
      MosWheel::print_mos_info( $motor, $wheel, 
				$actual_pos, $closest_pos, $closest_name,
				$difference, $motion );
      if( $motion =~ m/Stationary/ ){ $stationary_twice += 1; }

      if( $timer >= ($time_estimate + 2*$sleep_time) ){
	print "warning: time elapsed\n";die;
      }

    }#End until loop

  }#End for loop
  print "\t\t>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<\n";
  print "\t\t>>>>>Mos-Slit Wheel Motion Done<<<<<\n";
  print "\t\t>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<\n\n";
}#Endsub move_and_wait_loop


sub compute_time_estimate{
  my ($motion, $sleep_time) = @_;
  my $vel = $ENV{MOVE_V_1};
  my $full_turn = $MosWheel::MOS_SLIT_PN_REV;

  my $time = $vel * ( $motion / $full_turn );
  if( $time <= $sleep_time ){
    $time = $sleep_time;
  }

  return ($time + $sleep_time);
}#Endsub compute_time_estimate

#rcsId = q($Name:  $ $Id: z.mos.only.pl 14 2008-06-11 01:49:45Z hon $);
