#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: nomovelyot.config.all.but.mos.wheel.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;
use AllbutMosWheel qw/:all/;
use Constants      qw/:all/;

#----Load Variables-----------------------------------------------------------#
my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Fitsheader qw/:all/;
my $header = Fitsheader::select_header($DEBUG);
#$header = "/export/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
#$header = "/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

##>>>>Get Present Positions and Motion of Wheels<<<<<##
#The following arrays will have 4 entries that correspond to @wheels
my ( $ar1, $ar2, $ar3, $ar4, 
     $ar5, $ar6, $ar7 ) =  AllbutMosWheel::get_wheel_status();

#####****Have it return the correct index#####
my @motors       = @$ar1;
my @wheels       = @$ar2;
my @actual_pos   = @$ar3;
my @closest_pos  = @$ar4;
my @closest_name = @$ar5;
my @difference   = @$ar6;
my @motion       = @$ar7;

my $num_motors = 4;
my @home_types;
get_home_types( \@home_types );

AllbutMosWheel::PrintAllbutMosinfo( \@motors, \@wheels, 
				    \@actual_pos, \@closest_pos, 
				    \@closest_name, \@difference, \@motion,
				    \@home_types
				  );

use GetYN;
print "\n\nChange any wheel configuration? ";
my $change = get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

##      motor     =      a   c   d   e
my @new_pos       =   ( PI, PI, PI, PI );
my @new_index     =   (  0,  0,  0,  0 );
my @new_name      = qw( nc  nc  nc  nc );

main::selection_loop();
main::update_header( \@new_pos, \@new_name,
		     \@closest_pos, \@closest_name, $header );

my $motion = build_home_and_move_commands( \@motors, \@new_pos, \@home_types );

my $max_time_est = compute_time_est( \@new_pos );
if( $max_time_est == 0 ){ die "Nothing to move; max time estimate = 0";}

execute_motion( $motion, $max_time_est );

####>>>>>>> SUBS <<<<<<<<####
sub selection_loop{
  my $redo = 0;
  until( $redo ){
    query_change_what( \@new_pos, \@new_index, \@new_name );

    print_selections( \@motors,       \@wheels,
		      \@closest_name, \@new_name, \@home_types );

    print_exec_msg();
    print "Update the header to the target positions and move the wheels? (y)\n";
    print "Or, redo the new wheel configuration?                          (n)\n";
    print "Enter: ";

    $redo = GetYN::get_yn();
  }
}#Endsub selection_loop


sub query_change_what{
  my ( $ar_new_pos, $ar_new_index, $ar_new_name ) = @_;
  my $ready = 0;

  until( $ready ){
    print "Enter a motor name (a, c, d, or e) to configure\n";
    print "or enter q to quit without making changes:  ";
    my $new_motor = <STDIN>; chomp $new_motor;

    if( $new_motor =~ m/q/i ){
      die "Exiting\n\n";
    }

    if( $new_motor =~ m/d/i ){
      die "\n\nMotion of the lyot wheel is not allowed!\n\n";
    }

    if( $new_motor =~ m/([aA]|[c-eC-E])/ ){
      if( $new_motor =~ m/a/i ){
	
	( $$ar_new_pos[0], $$ar_new_index[0] )
	  = select_new_position( \@DECKER_NAMES, \@DECKER, "DECKER");

	$$ar_new_name[0] = $DECKER_NAMES[$$ar_new_index[0]];
	print "\nWill send motor a to $$ar_new_name[0] = ".
	      "$$ar_new_pos[0]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }elsif( $new_motor =~ m/c/i ){

	( $$ar_new_pos[1], $$ar_new_index[1] )
	  = select_new_position( \@FILTER_NAMES, \@FILTER, "FILTER");

	$$ar_new_name[1] = $FILTER_NAMES[$$ar_new_index[1]];
	print "\nWill send motor c to $FILTER_NAMES[$$ar_new_index[1]]= ".
	      "$$ar_new_pos[1]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}

      }elsif( $new_motor =~ m/d/i ){

	( $$ar_new_pos[2], $$ar_new_index[2] )
	  = select_new_position( \@LYOT_NAMES, \@LYOT, "LYOT");

	$$ar_new_name[2] = $LYOT_NAMES[$$ar_new_index[2]];
	print "\nWill send motor d to $LYOT_NAMES[$$ar_new_index[2]]= ".
	      "$$ar_new_pos[2]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }elsif( $new_motor =~ m/e/i ){

	( $$ar_new_pos[3], $$ar_new_index[3] )
	  = select_new_position( \@GRISM_NAMES, \@GRISM, "GRISM");

	$$ar_new_name[3] = $GRISM_NAMES[$$ar_new_index[3]];
	print "\nWill send motor e to $GRISM_NAMES[$$ar_new_index[3]]= ".
	      "$$ar_new_pos[3]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }
    }else{
      print "Please enter motor name from the list a, c, d, or e\n";
    }
  }

}#Endsub query_change_what


sub update_header{
  my ( $ar_new_pos, $ar_new_name, 
       $ar_closest_pos, $ar_closest_name,$header ) = @_;
  my @new_pos = @$ar_new_pos;
  my @new_name = @$ar_new_name;
  my @closest_pos = @$ar_closest_pos;
  my @closest_name = @$ar_closest_name;

  my $num_mot = $num_motors;

  for( my $i = 0; $i < $num_mot; $i++ ){
    if( $new_pos[$i] == PI ){
      #Put unchanged old names and positions in header
      #print "calling update_params with ".
      #	    "$i, $closest_name[$i], $closest_pos[$i]\n";
      update_params( $i, $closest_name[$i], $closest_pos[$i] );
    }elsif( $new_pos[$i] != PI ){
      #Put new target names and positions in header
      #print "calling update_params with  $i, $new_name[$i], $new_pos[$i]\n";
      update_params( $i, $new_name[$i], $new_pos[$i] );
    }
  }
  print "\n";

  #print "SHOULD BE WRITING HEADER\n\n";
  writeFitsHeader( $header );
}#Endsub update_header


sub update_params{
  my ( $i, $name, $pos ) = @_;
     
  if( $i == 0 ){
    #decker
    setStringParam(  "DECKER", $name );
    setNumericParam( "POS_A",  $pos );
    print "New header param: DECKER=$name,\t\tPOS_A=$pos\n";
  }elsif( $i == 1 ){
    #filter
    setStringParam(  "FILTER", $name );
    setNumericParam( "POS_C",  $pos );
    print "New header params FILTER=$name,\t\tPOS_C=$pos\n";
  }elsif( $i == 2 ){
    #lyot
    setStringParam(  "LYOT",  $name );
    setNumericParam( "POS_D", $pos );
    print "New header params LYOT  =$name,\t\tPOS_D=$pos\n";
  }elsif( $i == 3 ){
    #grism
    setStringParam(  "GRISM", $name );
    setNumericParam( "POS_E", $pos );
    print "New header params GRISM =$name,\t\tPOS_E=$pos\n";
  }

}#Endsub update_params


sub build_home_and_move_commands{
  my ( $ar_motors, $ar_new_pos, $ar_home_types ) = @_;
  my @motors  = @$ar_motors;
  my @new_pos = @$ar_new_pos;
  my @home_types = @$ar_home_types;

  my $motion;
  my $ampersand = " & ";

  my $num_to_home_and_move = how_many_to_home_and_move( \@new_pos );

  my $set_i_v = set_i_v( \@new_pos, $num_to_home_and_move );
  my $turn_on_hold_current = turn_on_hold_current();

  #test subroutine
  my $home_and_move_cmds = 
    build_new_motion( \@motors, \@home_types,
		      $num_to_home_and_move,
		      \@actual_pos, \@new_pos );

  $motion = $set_i_v.$ampersand.$turn_on_hold_current.$ampersand.$home_and_move_cmds;

  #print "final motion string: $motion\n";

  return $motion;
}#Endsub build_home_and_move_commands


sub turn_on_hold_current{
  #Turn on hold current of all motors at beginning of motion
  my $ampersand = " & ";
  my $c_y_cmd = $ENV{MOVING_CURRENT_C};
  my $d_y_cmd = $ENV{MOVING_CURRENT_D};
  my $e_y_cmd = $ENV{MOVING_CURRENT_E};

  my $all_y_cmd = $c_y_cmd.$ampersand.$d_y_cmd.$ampersand.$e_y_cmd;
  return $all_y_cmd;
}#Endsub turn_on_hold_current


sub turn_off_hold_current{
  #Turn on hold current of all motors at beginning of motion
  my $ampersand = " & ";
  my $quote  = "\"";

  my $c_y_cmd = $ENV{NOT_MOVING_CURRENT_C};
  my $d_y_cmd = $ENV{NOT_MOVING_CURRENT_D};
  my $e_y_cmd = $ENV{NOT_MOVING_CURRENT_E};

  my $all_y_cmd = $c_y_cmd.$ampersand.$d_y_cmd.$ampersand.$e_y_cmd;
  my $net_cmd = $AllbutMosWheel::cmd_head.$quote.$all_y_cmd.$quote;

  return $net_cmd;
}#Endsub turn_on_hold_current


sub build_new_motion{
  my ( $ar_motors, $ar_home_types, $num_to_home_and_move, 
       $ar_actual_pos, $ar_new_pos ) = @_;
  my @motors     = @$ar_motors;
  my $num_motors = @motors;
  my @home_types = @$ar_home_types;
  my @actual_pos = @$ar_actual_pos;
  my @new_pos    = @$ar_new_pos;

  my $ampersand = " & ";
  my $motion; my $nh_motion;
  my $which_not_pie = 1;
  for( my $i = 0; $i < $num_motors; $i++ ){
    if( $home_types[$i] == 0 ){
      #Near Home type
      #New positions are != PI;
      if( $new_pos[$i] != PI ){
	if( $which_not_pie == 1 ){
	  $motion = make_nh_motion( $i, $motors[$i],
				    $actual_pos[$i], $new_pos[$i] );

	  $which_not_pie++;
	}elsif( $which_not_pie > 1 and $which_not_pie <= $num_to_home_and_move ){
	  $nh_motion = 
	    make_nh_motion( $i, $motors[$i], $actual_pos[$i], $new_pos[$i] );
	  $motion = $motion.$ampersand.$nh_motion;
	
	  $which_not_pie++;
	}
      }else{
	#DO NOTHING
      }

    }elsif( $home_types[$i] == 1 ){
      #Limit Home type
      #New positions are != PI;
      if( $new_pos[$i] != PI ){
	if( $which_not_pie == 1 ){
	  if( $i == 0 ){
	    $motion = $ENV{ "FV_0" }.$ampersand."$motors[$i]+$new_pos[$i]";
	  }else{
	    $motion = $ENV{ "FV_".($i+1) }.$ampersand."$motors[$i]+$new_pos[$i]";
	  }
	  $which_not_pie++;

	}elsif( $which_not_pie > 1 and $which_not_pie <= $num_to_home_and_move ){
	  my $tmp;
	  if( $i == 0 ){
	    $tmp = $ENV{ "FV_0" }.$ampersand."$motors[$i]+$new_pos[$i]";
	  }else{
	    $tmp = $ENV{ "FV_".($i+1) }.$ampersand."$motors[$i]+$new_pos[$i]";
	  }
	  $motion = $motion.$ampersand.$tmp;
	  $which_not_pie++;

	}
      }else{
	#DO NOTHING
      }

    }#End home-type if-else
  }#End for-loop

  return $motion;
}#Endsub build_new_motion


sub make_nh_motion{
  my ( $i, $motor, $actual_pos, $new_pos ) = @_;
  my $nh_motion;

  my $backlash;
  if( $i == 0 ){ 
    $backlash = $ENV{ "BACKLASH_0" };
  }elsif( $i > 1 ){
    $backlash = $ENV{ "BACKLASH_".($i+1) };
  }

  if( $actual_pos == 0 and $new_pos == 0 ){
    #Do nothing

  }elsif( $actual_pos == $new_pos ){
    #Do nothing again

  }elsif( $actual_pos >= 0 and $new_pos > $actual_pos ){
    $nh_motion = $motor."N+$new_pos";

  }elsif( 0 <= $new_pos and $new_pos < $actual_pos ){
    my $move_to_pos = $new_pos - $backlash;
    if( $move_to_pos <= 0 ){
      $move_to_pos = abs( $move_to_pos );
      $nh_motion = $motor."N-$move_to_pos & $motor+$backlash";

    }elsif( $move_to_pos > 0 ){
      $nh_motion = $motor."N+$move_to_pos & $motor+$backlash";

    }
  }

  return $nh_motion;
}#Endsub make_nh_motion


sub how_many_to_home_and_move{
  my $ar_new_pos = $_[0];
  my @new_pos = @$ar_new_pos;
  my $num_to_home_and_move = 0;

  for( my $i = 0; $i < $num_motors; $i++ ){
    if( $new_pos[$i] != PI ){
      $num_to_home_and_move++;
    }
  }

  return $num_to_home_and_move;
}#Endsub how_many_to_home_and_move


sub set_i_v{
  my ( $ar_new_pos, $num_to_home_and_move ) = @_;
  my @new_pos = @$ar_new_pos;
  my $ampersand = " & ";
  my $which_not_pie = 1;
  my $set_i_v;

  for( my $i = 0; $i < $num_motors; $i++ ){
    my $which_motor;
    if( $i == 0 ){ $which_motor = 0;}
    else{ $which_motor = $i + 1;}

    if( $new_pos[$i] != PI ){
      if( $which_not_pie == 1 ){
	$set_i_v = $ENV{"I_".$which_motor}.$ampersand.$ENV{"V_".$which_motor};
	$which_not_pie++;
      }elsif( $which_not_pie > 1 and
	      $which_not_pie <= $num_to_home_and_move ){
	$which_not_pie++;
	$set_i_v = $set_i_v.$ampersand.
	           $ENV{"I_".$which_motor}.$ampersand.$ENV{"V_".$which_motor};
      }
    }
  }
  #print "$set_i_v\n";
  return $set_i_v;
}#Endsub set_i_v


sub execute_motion{
  my ( $motion, $max_time_est) = @_;
  my $space  = " ";
  my $quote  = "\"";

  my $cmd = $AllbutMosWheel::cmd_head.$quote.$motion.$quote;

  #print "\n\nSHOULD BE Executing command to home and move wheels:\n$cmd\n\n";
  print "\n\nExecuting command to home and move wheels:\n$cmd\n\n";
  my $reply = `$cmd`;

  wait_loop( $max_time_est );
  
}#Endsub execute_motion


sub compute_time_est{
  my $ar_new_pos = $_[0];
  my @new_pos = @$ar_new_pos;
  my $max_time_est;

  my $a   = 0;
  my $cde = 0;
  for( my $i=0; $i < $num_motors; $i++ ){
    my $this_pos = $new_pos[$i];
    if( $this_pos != PI ){
      if( $i == 0 ){
	$a = 1;
      }elsif( $i > 0 ){
	$cde = 1;
      }
    }
  }

  my $vel;
  if( $a == 1 ){
    $vel = substr $ENV{V_0}, 2;
    $max_time_est = 2 * $DECKER_REV / $vel;

  }elsif( $cde == 1 ){
    $vel = substr $ENV{V_2}, 2;
    $max_time_est = 2 * $FILTER_REV / $vel;

  }else{
    $max_time_est = 0;
  }
  #print "max time is $max_time_est\n";

  return $max_time_est;
}#Endsub compute_time_est


sub print_exec_msg{
  
  print "________________________________________________________________\n";

  print "Flamingos has had problems with it's wheels in the past, and\n";
  print "it has been useful to listen to them while they move.  If there's\n";
  print "an intercom or microphone near the dewar, consider turning it on.\n\n";

  print "There are two failure modes:\n";
  print "1) If a wheel moves by first homing to the limit switch, and\n";
  print "the switch fails, then the wheel will turn endlessly.\n\n";

  print "2)  If a wheel binds the griding noise should be audible\n";
  print "(likely candidate for this is the mos_slit wheel).\n\n";

  print ">>>>>  WHAT TO DO IF A FAILURE OCCURS\n";
  print "Type Ctrl-C to kill the script, and then type the following:\n";
  print "ufmotor $ENV{CLIENT_HOST} ".
        "$ENV{HOST} $ENV{CLIENT_PORT} $ENV{CLIENT_UFMOTOR_PORT}\n\n";

  print "At the ufmotor> prompt type, e.g. a@, or whatever motor\n";
  print "name is appropriate (a=decker, b=mos, c=filter, d=lyot, e=grism).\n\n";
  print "Type quit to exit\n";
  print "Then type $ENV{TURN_OFF_HOLD_CURRENT}\n\n";

  print "In the event that this fails, log into the baytech and\n";
  print "cycle the power on the motor controller:\n";
  print "telnet baytech,  username> guest,  passwd>16Mb,img\n";
  print "reboot 3\n";
  print "logout\n\n";


  print "(Note that cycling the power on the motor controller will reset)\n";
  print "(the current position of all motors to zero.  Any wheels that  )\n";
  print "(use the Near home technique will probably be lost as well as  )\n";
  print "(the motor that requires this drastic action.                  )\n";
  print "________________________________________________________________\n\n";

  
}#Endsub print_exec_msg


sub wait_loop{
  my ( $max_time_est ) = @_;

  my $timer = 0;
  my $wait_time = 5; #seconds
  my $stationary_twice = 0;
  my $time_elapsed = 0;
  until( $stationary_twice == 2 or $time_elapsed == 1 ){
    print "\n\n*********************************************************\n";
    print "This should not take longer than ";
    printf "%d", $max_time_est;
    print " seconds.\n";
    print "Have waited $timer seconds so far.\n\n";
    my ( $ar1, $ar2, $ar3, $ar4, $ar5, $ar6, $ar7 ) =
      AllbutMosWheel::get_wheel_status();

    my @motors       = @$ar1;
    my @wheels       = @$ar2;
    my @actual_pos   = @$ar3;
    my @closest_pos  = @$ar4;
    my @closest_name = @$ar5;
    my @difference   = @$ar6;
    my @motion       = @$ar7;


    AllbutMosWheel::PrintAllbutMosinfo( \@motors, \@wheels, 
					\@actual_pos, \@closest_pos, 
					\@closest_name, \@difference, \@motion,
					\@home_types
				      );

    my $number_stationary = get_stationary( \@motion );
    if( $number_stationary == @motors ){
      $stationary_twice += 1;
    }

    if( $timer >= $max_time_est ){
      $time_elapsed = 1;
    }
    sleep $wait_time;
    $timer += $wait_time;
  }

  if( $time_elapsed ){
    print ">>>>>>     WARNING   WARNING  WARNGIN  WARNING  WARNING   <<<<<<\n";
    print ">>>>>>     Maximum estimated time ($max_time_est seconds) <<<<<<\n";
    print ">>>>>>     for wheel(s) to move has elapsed!              <<<<<<\n";

    print_exec_msg();
  }elsif( !$time_elapsed ){
    my $turn_off_hold_current_cmd = turn_off_hold_current();
    print "\n\n";
    print "Set hold current to zero for camera motors:\n";
    print "Executing $turn_off_hold_current_cmd\n";
    my $reply = `$turn_off_hold_current_cmd`;
    #system( $turn_off_hold_current_cmd );
  }

  print "\n\n>>>>>  Wheel motion is done.\n\n";
}#Endsub move_loop


sub get_stationary{
  my $ar = $_[0];
  my @motion = @$ar;
  my $number_stationary = 0;

  for( my $i = 0; $i < $num_motors; $i++ ){
    if( $motion[$i] =~ m/Stationary/ ){
      $number_stationary += 1;
    }
  }

  return $number_stationary;
}#Endsub get_stationary


sub get_home_types{
  my $ar_home_types = $_[0];
  my $num_mot = $num_motors;

  $$ar_home_types[0] = param_value( "HT_0" );#motor a = decker
  $$ar_home_types[1] = param_value( "HT_2" );#motor c = filter
  $$ar_home_types[2] = param_value( "HT_3" );#motor d = lyot
  $$ar_home_types[3] = param_value( "HT_4" );#motor e = grism
}#Endsub get_home_types


sub interp_home_cmd{
  my ( $motor, $input, $i ) = @_;
  my $cmd_str;

  print "force near home:\n";$input = 0;
  #print "actual pos for motor i=$i is $actual_pos[$i]\n";
  #print "motor = $motor, input home type = $input, i=$i\n";
  if( $input == 0 ){
    #limit switch doesn't work;
    $cmd_str = $motor . "Z";

  }elsif( $input == 1 ){
    #limit switch and near home command work
    #acquires strings like aI100 & aV500 & aF500
    if( $i == 0 ){
      $cmd_str = $ENV{ "FV_0" };
    }elsif( $i >= 1 ){
      $cmd_str = $ENV{ "FV_".($i+1) };
    }
  }

  return $cmd_str;
}#Endsub interp_home_cmd


sub print_selections{
  my ( $ar_motors,       $ar_wheels, 
       $ar_closest_name, $ar_new_name, $ar_home_types ) = @_;

  my @motors = @$ar_motors;
  my @wheels = @$ar_wheels;
  my @closest_name = @$ar_closest_name;
  my @new_name   = @$ar_new_name;
  my @home_types = @$ar_home_types;

  my $num_mot = @motors;;

  print "The new configuation:\n";
  print "\t\t\tMOVE\t\tMOVE\n";
  print "Motor\tWheel\t\tFROM\t\tTO\tHOME-TYPE\n";
  print "_____________________________________________________________\n";
  for( my $i = 0; $i < $num_mot; $i++ ){
    if( $new_pos[$i] != PI ){
      print "$motors[$i]\t$wheels[$i]\t\t";
      print "$closest_name[$i]\t\t$new_name[$i]\t";

      if( $home_types[$i] == 0 ){
	print "Near-home\n";
      }elsif( $home_types[$i] == 1 ){
	print "Limit switch\n";
      }
    }
  }
  print "____________________________________________________________\n";

}#Endsub print_selections

sub select_new_position{
  my ( $ar1, $ar2, $wheel_name ) = @_;
  my @POS_NAMES = @$ar1;
  my @POS_VALS  = @$ar2;
  my $num_pos   = @POS_NAMES;
  my $num_pos0  = $num_pos - 1;

  my $new_pos_val = PI;#this is the value returned if choose not to change
  my $new_pos_num = 0;

  my $is_valid = 0;

  print "\n\n";
  print "Known $wheel_name positions\n";
  print "Position #\tPosition name\n";
  for( my $i = 0; $i < $num_pos; $i++ ){
    print "$i -------------- $POS_NAMES[$i]\n";
  }

  until( $is_valid ){
    print "Select a position number: ";
    $new_pos_num = <STDIN>; chomp $new_pos_num;

    my $input_len = length $new_pos_num;
    if( $input_len == 0 ){
      print "Not changing $wheel_name position.\n";
      $is_valid = 1;
    }else{
      if( $new_pos_num !~ m/[a-zA-Z]/ ){
	if( $new_pos_num >= 0 and $new_pos_num < $num_pos ){
	  #print "if = $new_pos_num\n";
	  $new_pos_val = $POS_VALS[$new_pos_num];
	  $is_valid    = 1;
	}else{
	  print "Please enter a number between 0 and $num_pos0:\n";
	}
      }else{
	print "Please enter a number between 0 and $num_pos0:\n";
      }
    }
  }#End until is_valid

  return ( $new_pos_val, $new_pos_num );
}#Endsub select_new_position




