#!/usr/local/bin/perl -w

use strict;

my $cnt = @ARGV;
if( $cnt < 1 ){
  die "\nPlease enter an integer.\n\n";
}

my $input = shift;

print "\n";

my $reply = check_alpha(   $input );
if( !$reply ){ exit; }

$reply = check_integer( $input );
if( !$reply ){ exit; }

print "Valid entry: $input\n\n";

####
sub check_alpha{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/[a-zA-Z]/ ){
    print "You entered $input.\n";
    print "Please enter an integer value.\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_alpha


sub check_integer{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/\.+/ ){
    print "Please enter an integer (no decimal point).\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_integer

#rcsId = q($Name:  $ $Id: test.integer.pl 14 2008-06-11 01:49:45Z hon $);
