#!/usr/local/bin/perl -w

use strict;

my $infile = 'original_file_dir.txt';
my $line_input;

open IN, $infile or die "Cannot open $!\n";
while( <IN> ){
  $line_input = $line_input . $_;
  #print;
}
close IN;
#print "\n";

my $left = index $line_input, '\'';
$left += 1;
my $right = rindex $line_input, '\'';

#print "left index = $left\nright index = $right\n";

my $dir_name = substr $line_input, $left, $right-$left;

print "dir name is\n$dir_name\n";

#rcsId = q($Name:  $ $Id: read_orig_dir.pl 14 2008-06-11 01:49:45Z hon $);
