#!/usr/local/bin/perl -w

my $response = 0;
my $is_valid = 0;

  until($is_valid){
    print "Enter i for imaging  mode (Vbias = 1.0  V), or\n";
    print "Enter s for spectral mode (Vbias = 0.75 V), or\n";
    print("Enter a value between 0 and 1.0 (use leading zero, e.g. 0.5)\n ");
    chomp ($response = <STDIN>);

    if($response =~ m/i/i || $response =~ m/s/i){
      if($response eq 'i' || $response eq 'I'){
	print "Imaging bias value = 1.0 V\n";
	$is_valid = 1;
	$response = 1.0;
      }elsif($response eq 's' || $response eq 'S'){
	print "Spectra bias value = 0.75 V\n";
	$is_valid = 1;
	$response = 0;
      }
    }elsif( $response =~ m/\d{0,1}\.\d{1,2}/ || $response =~ m/\d{1}\.\d{0,2}/ ||
	    $response =~ m/0/                || $response =~ m/1/
	  ){
      #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
      print "response was $response\n";
      $is_valid = 1;
    }
  }
  print "\n";
  #return $response;

#rcsId = q($Name:  $ $Id: inputtest.pl 14 2008-06-11 01:49:45Z hon $);
