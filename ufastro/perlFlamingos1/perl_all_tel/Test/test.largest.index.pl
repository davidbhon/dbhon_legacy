#!/usr/local/bin/perl -w

use strict;
use Getopt::Long;
use GetYN;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Fitsheader qw/:all/;
my $header = select_header($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header);

my $filebase = param_value( 'FILEBASE' );

my $dir_path = param_value( 'ORIG_DIR' );
print "dir path is $dir_path\n";

my $filenm = $dir_path . $filebase . "_";

print "searching for $filenm\n";

my $i=1;
my $last_found = 0;
for($i = 0; $i <= 9999; $i += 1){
  my $matchname = $filenm;
  if( $i > 0 && $i <= 9){
    $matchname = $matchname . "000$i.fits";
    #print "searching for $matchname\n";
    if( -e $matchname ){
      $last_found = $i;
    }
  }elsif( $i > 9 && $i <= 99){
    $matchname = $matchname . "00$i.fits";
    if( -e $matchname ){
      $last_found = $i;
    }
  }elsif( $i > 99 && $i <= 999 ){
    $matchname = $matchname . "0$i.fits";
    if( -e $matchname ){
      $last_found = $i;
    }
  }elsif( $i > 999 && $i <= 9999){
    $matchname = $matchname . "$i.fits";
    if( -e $matchname ){
      $last_found = $i;
    }
  }
}
print "last found is $last_found\n";


#rcsId = q($Name:  $ $Id: test.largest.index.pl 14 2008-06-11 01:49:45Z hon $);
