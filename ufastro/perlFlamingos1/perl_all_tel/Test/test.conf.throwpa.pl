#!/usr/local/bin/perl -w

use strict;
use  Fitsheader qw/:all/;

my $header = "/export/home/raines/Perl/flamingos.headers.lut/Flamingos.fitsheader";
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);


my $didmod = mod_THROW_PA_for_gem();


sub mod_THROW_PA_for_gem{
  #interactively modify string parameter values of FITS header.
  
  print "\n";
  print 
"__________________________________________________________________________\n";
  print "Set THROW PA, where -180 <= THROW_PA <= +180\n";
  print "You must include either a - or a + sign\n";
    
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  $iparm = param_index( 'THROW_PA' );
  
  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      
      if( $new_pvalue =~ m/^(\-|\+)[0-9]*$/ ){
	if( -180 <= $new_pvalue and $new_pvalue <= 180 ){
	  print "new_pvalue = $new_pvalue\n";
	  setNumericParam( "THROW_PA", $new_pvalue );
	  $is_valid = 1;
	  $didmod = 1;
	}else{
	  print "\nPlease enter a value between -180 and +180, including the sign\n";
	}
      }else{
	print "\nPlease enter a number between -180 and +180, including the sign\n";
      }
    }else{
      print ">>>      WARNGING      WARNGING      <<<\n";
      print "Cannot find ORIG_DIR keyword in FITS header\n";
      print "NONE of the data taking scripts will work without this field\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $didmod;
}#Endsub mod_THROW_PA_for_gem

#rcsId = q($Name:  $ $Id: test.conf.throwpa.pl 14 2008-06-11 01:49:45Z hon $);
