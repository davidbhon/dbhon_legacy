#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use MCE4_acquisition qw/:all/;
use GemTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = param_value( 'EXP_TIME' );
my $nreads          = param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

my $orig_dir  = param_value( 'ORIG_DIR' );
my $filebase  = param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = does_dir_exist( $orig_dir );
my $last_index = whats_last_index($orig_dir, $filebase);
my $next_index = $last_index + 1;
print "This image's index will be $next_index\n";

set_filename_in_header( $filebase, $next_index, $header );

ct12_setup_mce4( $expt, $nreads );

#>>>>Get TCS info
my $use_tcs = param_value( 'USE_TCS' );
my $telescop = param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n";
if( $use_tcs ){
  ####call to tcs subroutines goes here
  ####Must match all 18 characters in header
  if( $telescop eq 'Gemini-South      ' ){
    getGemTCSinfo( $header );
  }
}

#>>>Trigger system to acquire image
my $acq_cmd = acquisition_cmd( $header, $lut_name, 
                               $file_hint, $next_index, $net_edt_timeout);
acquire_image( $acq_cmd );
idle_lut_status( $expt, $nreads);

#move_file_for_index_sep( $file_hint, $next_index );#Not used now
my $unlock_param = param_value( 'UNLOCK' );
if( $unlock_param ){
  ImgTests::make_unlock_file( $file_hint, $next_index );
}

if( Fitsheader::param_value( 'DISPDS92' ) ){
  ImgTests::display_image( $file_hint, $next_index );
}else{
  print "The better way to display images is off; see config.output.pl\n";
}

exit;#That's That & That's the remark

#_______________________________________________________________________
####
####SUBS FOLLOW
####
sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";
      
    }elsif( $sleep_count > $expt + $nreads + $lut_timeout ){

      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........".
            "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n";

      #stop take
   #   my $stop_take = "ufstop.pl -stop";
   #   system( $stop_take );
      #reset mce4 to ct 0
   #   system("ufidle.pl");
       exit;
       
    }#end time expired
    
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status

__END__

=head1 NAME

engineering.ct12.singleimage.pl

=head1 DESCRIPTION

Runs cycle type 12 on MCE4 (multiple reads), so that you get
the first coadded readout of the array.

=head1 REVISION & LOCKER

$Name:  $

$Id: engineering.ct12.singleimage.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
