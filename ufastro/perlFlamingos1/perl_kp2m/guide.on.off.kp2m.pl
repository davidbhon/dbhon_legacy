#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Gdr_Rpc qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage: guide.on.off.kp2m.pl on|off\n\n";

}else{
  my $state = shift;

  if( $state =~ m/^on$/i) {
    print "Turning guiding on\n";
    Gdr_Rpc::guide_on( $gdrrpc );

  }elsif( $state =~ m/^off$/i) {
    print "Turning guiding off\n";
    Gdr_Rpc::guide_off( $gdrrpc );

  }else{
    die "\n\n\tPlease enter guide.on.off.kp2m.pl on or guide.on.off.kp2m.pl off\n\n";
    
  }
  
}#End


__END__

=head1 NAME

guide.on.off.kp2m.pl

=head1 Description

Turns on and off guiding, just as you can do with
the GUI.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.on.off.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
