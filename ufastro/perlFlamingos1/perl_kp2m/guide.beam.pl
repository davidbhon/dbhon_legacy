#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Gdr_Rpc qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cnt = @ARGV;
if( $cnt < 1 ) {
  usage();
}else{
  my $state = shift;

  if( $state =~ m/^1$/i){
    Gdr_Rpc::select_guide_pos1( $gdrrpc );

  }elsif( $state =~ m/^2$/i){
    Gdr_Rpc::select_guide_pos2( $gdrrpc );

  }elsif( $state =~ m/^3$/i){
    Gdr_Rpc::select_guide_pos3( $gdrrpc );

  }elsif( $state =~ m/^4$/i){
    Gdr_Rpc::select_guide_pos4( $gdrrpc );

  }else{
    usage();

  }

}#End


sub usage{

   die "\n\tUsage: guide.select.pos.kp2m.pl 1|2|3|4".
       "\n\tThis will move the guide box to the selected guide position.".
       "\n\tNOTES:  1) Guiding must be off.".
       "\n\t        2) This script will not turn guiding on.\n\n";

}#Endsub usage


__END__

=head1 NAME

guide.beam.pl

=head1 Description

Selects which beam to guide in.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.beam.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
