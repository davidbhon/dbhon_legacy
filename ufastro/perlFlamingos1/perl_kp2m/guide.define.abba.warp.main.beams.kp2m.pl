#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use KPNO_offsets qw/:all/;
use Math::Round qw( round );
use RW_Header_Params qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

print_msg();

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my ($m_rptpat, $m_ndgsz, $m_throw, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

### Use these for telescope offsetting
my ( $A_dra_asec, $A_ddec_asec, $B_dra_asec, $B_ddec_asec ) =
   compute_offsets( $pa_slit_on_sky, $m_throw );

print "\n";
print "Telescope offsets in arcsec\n";
print "---------------------------\n";
print "Beam\tdra\tddec\n";
print "A\t$A_dra_asec\t$A_ddec_asec\n";
print "B\t$B_dra_asec\t$B_ddec_asec\n";
print "\n";

### These we'll grab from the guider info
my $A_dra_pix;
my $A_ddec_pix;
my $B_dra_pix;
my $B_ddec_pix;

GetYN::query_ready();

### Step 1 Guiding off
print "<><><><><><><><><><><><><><><><><><><><>\n";
print "Step 1) Clear offsets.\n";
print "        If the telescope offsets are not zero, then they need to be zeroed.\n";
print "        Clear the offsets? ";
my $reply = GetYN::get_yn();
if( $reply ){
  KPNO_offsets::clear_offsets();
}

print "Step 2: Guiding off.\n";
Gdr_Rpc::guide_off( $gdrrpc );

print "\n";
#print "SHOULD BE DOING: Step 3a) Offset telescope to beam B.\n";
print "Step 3a) Offset telescope to beam B.\n";
KPNO_offsets::do_offset( $B_dra_asec, $B_ddec_asec );
KPNO_offsets::offset_wait();
print "\n";
print "Step 3b) Warp guide and AOI boxes to guide star.\n";
Gdr_Rpc::warp_boxes_to_star( $gdrrpc );

print "\n";
print "Step 3c) Get guide box position info & \n";
print "         define beam B = lock pos3; mark position.\n";
( $B_dra_pix, $B_ddec_pix ) = Gdr_Rpc::get_current_position( $gdrrpc );

main::did_it_work();

Gdr_Rpc::define_lock_position( $gdrrpc, "3" );
my $gpos3_marker_y = Gdr_Rpc::flip_y( $B_ddec_pix );
Gdr_Rpc::mark_current_position( $gdrrpc, $B_dra_pix, $gpos3_marker_y );

print "Should have marked beam B as defpos3.\n";
main::did_it_work();

print "\n\n";
#print "SHOULD BE DOING: Step 4a) Offset telescope to beam A.\n";
print "Step 4a) Offset telescope to beam A.\n";
KPNO_offsets::do_offset( $A_dra_asec, $A_ddec_asec );
KPNO_offsets::offset_wait();
print "\n";
print "Step 4b) Warp guide and AOI boxes to guide star.\n";
Gdr_Rpc::warp_boxes_to_star( $gdrrpc );

print "\n";
print "Step 4c) Get guide box position info & \n";
print "         define beam A = lock pos2; mark position.\n";
( $A_dra_pix, $A_ddec_pix ) = Gdr_Rpc::get_current_position( $gdrrpc );

main::did_it_work();

Gdr_Rpc::define_lock_position( $gdrrpc, "2" );
my $gpos2_marker_y = Gdr_Rpc::flip_y( $A_ddec_pix );
Gdr_Rpc::mark_current_position( $gdrrpc, $A_dra_pix, $gpos2_marker_y );

print "Should have marked beam A as defpos2.\n";
main::did_it_work();

write_box_positions( $A_dra_pix, $gpos2_marker_y, $B_dra_pix, $gpos3_marker_y );

print "\n";
print "Step 5) Start guiding in beam A? \n";

$reply = GetYN::get_yn();
if( !$reply ){
  die "\n\nExiting.\n\n";
}

Gdr_Rpc::select_guide_pos2( $gdrrpc );
Gdr_Rpc::guide_on( $gdrrpc );

####################################
### Subs
sub compute_offsets{
  my ($pa_slit_on_sky, $beam_separation) = @_;
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $beam_separation * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $beam_separation * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub did_it_work{
  print "Did it do what was expected? ";
  my $reply = GetYN::get_yn();

  if( !$reply ){
    print "Continue? ";
    my $reply2 = GetYN::get_yn();
    if( !$reply2 ){
      die "\n\nExiting\n\n";
    }
  }
}#Endsub did_it_work


sub print_msg{
  print "\n\n";
  print "\tBefore you start:\n";
  print "\t1) Make sure the MOS alignment stars are centered on their boxes.\n";
  print "\t2) The present position is defined as lock position 1.\n";
  print "\t3) The telescope has been guiding.\n";
  print "\t4) The telescope has zero offsets.\n";
  print "\n";
  print "\tThis script will define lock positions 2 & 3 as beams A & B,\n";
  print "\tand place markers at each location.\n";
  print "\n";
  print "\tOrder of action:\n";
  print "\t1)  Offer to clear the offsets, in case they're not already zeroed.\n";
  print "\t2)  Turn off guiding.\n";
  print "\t3a) Offset telescope to beam B.\n";
  print "\t3b) Warp guide and AOI boxes guide star in to beam B.\n";
  print "\t3c) Define position as pos3, and place marker.\n";
  print "\t4a) Offset telescope to beam A.\n";
  print "\t4b) Warp guide and AOI boxes guide star in to beam A.\n";
  print "\t4c) Define position as pos3, and place marker.\n";
  print "\t5)  Turn on guiding.\n";
  print "\n\n";
}#Endsub print_msg


sub write_box_positions{
  my ($gpos2_x, $gpos2_y, $gpos3_x, $gpos3_y) = @_;

  print "Writing box position header keywords\n";

  Fitsheader::setNumericParam( 'GPOS2_X', $gpos2_x );
  Fitsheader::setNumericParam( 'GPOS2_Y', $gpos2_y );

  Fitsheader::setNumericParam( 'GPOS3_X', $gpos3_x );
  Fitsheader::setNumericParam( 'GPOS3_Y', $gpos3_y );

  Fitsheader::printFitsHeader( 'GPOS2_X', 'GPOS2_Y' );
  print "\n";
  Fitsheader::printFitsHeader( 'GPOS3_X', 'GPOS3_Y' );

  Fitsheader::writeFitsHeader( $header );
  print "\n";
}#Endsub write_box_positions

__END__

=head1 NAME

guide.define.abba.warp.main.beams.kp2m.pl

=head1 Description

Probably not a usable script.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.define.abba.warp.main.beams.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
