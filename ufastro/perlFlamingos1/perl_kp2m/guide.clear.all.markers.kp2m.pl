#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use GetYN;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

print "\nThis will clear _ALL_ of the markers.\n";
GetYN::query_ready();

my $cmd_clear = $gdrrpc." marker clear";

#print "SHOULD BE: Executing $cmd_clear\n";
print "Executing $cmd_clear\n";
my $cmd_reply = `$cmd_clear`;

__END__

=head1 NAME

guide.clear.all.markers.kp2m.pl

=head1 Description

Clears the markers from the Guider VDU.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.clear.all.markers.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
