#!/usr/local/bin/perl -w

###############################################################################
#
#Configure fits header parameters for next mos dither sequence
#
##############################################################################
use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = Fitsheader::select_header($DEBUG);

use Fitsheader qw/:all/;
Fitsheader::Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
Fitsheader::readFitsHeader($header);

print "\n\n".
      ">>>---------------------Present exposure parameters are".
      "---------------------<<<\n";

Fitsheader::printFitsHeader( "M_RPTPAT", "M_THROW" );
print "\n";
Fitsheader::printFitsHeader( "M_NDGSZ", "USEMNUDG" );
print "\n";
Fitsheader::printFitsHeader( "CHIP-PA", "SLIT-CPA" );
print "\n";

my $pa_chip_on_sky_for_rot0  = Fitsheader::param_value( "CHIP-PA" );
my $pa_rotator               = Fitsheader::param_value( "ROT_PA" );
my $chip_pa_on_sky_this_rotator_pa = $pa_rotator + $pa_chip_on_sky_for_rot0;

my $pa_slit_on_chip = Fitsheader::param_value( "SLIT-CPA" );

my $pa_slit_on_sky  = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
                                                    $pa_slit_on_chip );

print "PA of slit on sky --------------------- = $pa_slit_on_sky (deg)\n";

print "_____________________________________________________________".
      "__________________\n\n";

my $didmod = 0;
my $redo = 0;

print "CHIP-PA  and SLIT_CPA are changed with a different config scripts.\n";
print "\nChange any of the other paramters?  ";
my $change = get_yn();
if( $change ){
  until( $redo ){
    my $chg_bigger_than_zero = chg_bigger_than_zero( "M_RPTPAT" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "M_RPTPAT", $chg_bigger_than_zero );
      $didmod += 1;
    }

    $chg_bigger_than_zero = get_new_throw( "M_THROW" );
    #$chg_bigger_than_zero = chg_bigger_than_zero( "M_THROW" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "M_THROW", $chg_bigger_than_zero );
      $didmod += 1;
    }

    my $chg_zero_or_one = chg_zero_or_one( "USEMNUDG" );
    if( $chg_zero_or_one > -1 ){
      Fitsheader::setNumericParam( "USEMNUDG", $chg_zero_or_one );
      $didmod += 1;
    }

    $chg_bigger_than_zero = get_new_nudge( "M_NDGSZ" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "M_NDGSZ", $chg_bigger_than_zero );
      $didmod += 1;
    }

    if( $didmod > 0 ){
      print "\n\nThe new set of dither parameters are:\n";
      Fitsheader::printFitsHeader( "M_RPTPAT", "M_THROW" );
      print "\n";
      Fitsheader::printFitsHeader( "M_NDGSZ", "USEMNUDG" );
      print "\n";
      Fitsheader::printFitsHeader( "CHIP-PA","SLIT-CPA" );

      my $new_rotator_pa = Fitsheader::param_value( "ROT_PA" );
      my $chip_pa_on_sky_w_new_rotator_pa = $new_rotator_pa + $pa_chip_on_sky_for_rot0;
      my $pa_slit_on_sky  = main::compute_pa_slit_on_sky( $chip_pa_on_sky_w_new_rotator_pa,
                                                          $pa_slit_on_chip );
      print "\n";
      print "PA of slit on sky --------------------- = $pa_slit_on_sky (deg)\n";
      print "_____________________________________________________________".
	"__________________\n";
      print "\nAccept changes?";
      $redo = get_yn();
    }else{
      $redo = 1;
    }
  }
}elsif( !$change ){
  #do nothing
}


if( $didmod > 0 ){
  if( !$DEBUG ){
    Fitsheader::writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    Fitsheader::writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub chg_bigger_than_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value > 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_bigger_than_zero


sub get_new_nudge{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print "Enter an integer number of guider pixels to\n";
  print "shift the ABBA pattern on pattern repeats:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub get_new_nudge


sub get_new_throw{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );
  my $offset_arcsec = $fh_pvalues[$iparm];
  my $gdr_ps = $ENV{GDR_PS};
  my $offset_pix = $offset_arcsec / $gdr_ps;
  $offset_pix = sprintf "%.2f", $offset_pix;

  print  $fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ]." = $offset_arcsec arcseconds";
  print "\t";
  print "= $offset_pix guider pixels\n";
  #print "\n";
  print "Enter a real-valued offset > 0.  It is suggested that the offset\n";
  print "correspond to an integer number of pixels on the guider:  ";

  until($is_valid){
    chomp ($response = <STDIN>);

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-zA-Z]/ ){
	print "Enter a real number, e.g. 1.5, or 90. (include leading zero & decimal point):  ";
      }else{
	if( $response =~ m/^\d{1,3}\.{1}\d*$/ ){
	  print "response = $response, matched test 1\n";
	  $is_valid = 1;
	}else{
	  print "Enter a real number, e.g. 1.5, or 90. (include leading zero & decimal point):  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub get_new_throw


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub chg_zero_or_one{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter either 0 (no) or 1 (yes):  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please either 0 or 1:  ";
      }else{
	if( $response == 0 or $response == 1 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter either 0 or 1:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_zero_or_one


__END__

=head1 NAME

config.mos.dither.kp2m.pl

=head1 Description

Setup for dithering in an ABBA pattern at the KPNO 2.1-m telescope.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.mos.dither.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
