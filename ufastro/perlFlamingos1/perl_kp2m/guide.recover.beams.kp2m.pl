#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";

use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use KPNO_offsets qw/:all/;
use Math::Round qw( round );
use RW_Header_Params qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);


my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my ($m_rptpat, $m_ndgsz, $m_throw, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

### Convert to guider plate scale
my $gdr_ps = $ENV{GDR_PS};

my $beam_separation_pix = $m_throw / $gdr_ps;

my ( $x1, $y1, $x2, $y2, $x3, $y3 ) = main::get_previous_lock_positions( $header );

print "\n";
print "Last known lock positions definitions on the guider\n";
print "-----------------------------------------------------\n";
print "Position         X         Y\n";
print "____________________________________\n";
print "Beam B       $x3, $y3\n";
print "Slit center  $x1  $y1\n";
print "Beam A       $x2, $y2\n";
print "-----------------------------------------------------\n";

print "Redefining all lock positions.\n";
main::redefine_all_lock_positions( $x1, $y1, $x2, $y2, $x3, $y3 );

my $crash_pos = query_crash_location();

main::restart_guiding( $crash_pos, $gdrrpc );

print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
print "DONE!\n";
print "NOW wait about 30 to 60 seconds to make sure that the guider\n";
print "has pulled the star back to the correct guide center.\n\n";

#### SUBS ####
sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub get_previous_lock_positions{
  my $header = $_[0];

  my $x1 = Fitsheader::param_value( 'GPOS1_X' );
  my $y1 = Fitsheader::param_value( 'GPOS1_Y' );

  my $x2 = Fitsheader::param_value( 'GPOS2_X' );#beam A
  my $y2 = Fitsheader::param_value( 'GPOS2_Y' );

  my $x3 = Fitsheader::param_value( 'GPOS3_X' );#beam B
  my $y3 = Fitsheader::param_value( 'GPOS3_Y' );

  return ( $x1, $y1, $x2, $y2, $x3, $y3 );
}#Endsub get_previous_lock_positions


sub query_crash_location{
  print "What lock position were you in when the guider crashed?\n";
  print "Enter A or B for beam names, or C for slit center, or q to quit: ";

  my $entry = 0;
  my $is_valid = 0;
  until( $is_valid ){
    chomp ( $entry = <STDIN> );
    if( $entry eq "A" or $entry eq "a" or
	$entry eq "B" or $entry eq "b" or
	$entry eq "C" or $entry eq "c" or
	$entry eq "Q" or $entry eq "q" ){
      $is_valid = 1;
      last;
    }
    print "invalid entry.\n";
    print "Enter A or B for beam names, or C for slit center, or q to quit: ";
  }

  print "Entry = $entry\n";
  return $entry;
}#Endsub query_crash_location


sub redefine_all_lock_positions{
  my ( $x1, $y1, $x2, $y2, $x3, $y3 ) = @_;

  print "Redefining lock position #1 = slit center\n";
  Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "1", $x1, $y1 );
  sleep 0.5;
  print "\n\n";

  print "Redefining lock position #2 = Beam A\n";
  Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "2", $x2, $y2 );
  sleep 0.5;
  print "\n\n";

  print "Redefining lock position #3 = Beam B\n";
  Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "3", $x3, $y3 );
  sleep 0.5;
  print "\n\n";

}#Endsub redefine_all_lock_positions


sub restart_guiding{
  my ( $crash_pos, $gdrrpc ) = @_;

  if( $crash_pos eq "c" or $crash_pos eq "C" ){
    Gdr_Rpc::select_guide_pos1( $gdrrpc );
    sleep 1;
    Gdr_Rpc::guide_on( $gdrrpc );

  }elsif( $crash_pos eq "a" or $crash_pos eq "A" ){
    Gdr_Rpc::select_guide_pos2( $gdrrpc );
    sleep 1;
    Gdr_Rpc::guide_on( $gdrrpc );

  }elsif( $crash_pos eq "b" or $crash_pos eq "B" ){
    Gdr_Rpc::select_guide_pos3( $gdrrpc );
    sleep 1;
    Gdr_Rpc::guide_on( $gdrrpc );

  }

}#Endsub restart_guiding


__END__

=head1 NAME

guide.recover.beams.kp2m.pl

=head1 Description

A script to attempt to recover from a guider
crash.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.recover.beams.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
