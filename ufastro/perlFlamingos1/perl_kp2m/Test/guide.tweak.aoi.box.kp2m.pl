#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.tweak.aoi.box.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Gdr_Rpc qw/:all/;

my $cnt = @ARGV;
my $gdr_ps = $ENV{GDR_PS};
if( $cnt < 2 ){
  die "\n\n".
      "USAGE:  .guide.tweak.aoi.box.kp2m.pl x_offset y_offset\n".
      "        Offsets are relative to last present position,\n".
      "        and in integer guider pixels.\n".
      "\n".
      "WHAT IT WILL DO:\n".
      "        1) Turn off guiding.\n".
      "        2) Offset the aoi box only.\n".
      "        3) Resume guiding.\n\n".
      "\n";
}

my $x_offset  = shift;
my $y_offset = shift;

if( $x_offset =~ m/[a-zA-Z]/ or $y_offset =~ m/[a-zA-Z]/ ){
  die "\nPlease enter numerical offsets.\n\n";
}

if( $x_offset !~ m/^-{0,1}\d*$/ or $y_offset !~ m/^-{0,1}\d*$/ ){
  die "Please enter integer pixel offsets.\n\n";
}

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
Gdr_Rpc::guide_off( $gdrrpc );
Gdr_Rpc::move_aoi_box( $gdrrpc, $x_offset, $y_offset );
guide_wait();
Gdr_Rpc::guide_on( $gdrrpc );

### SUBS
sub guide_wait{
  my $wait = $ENV{GUIDE_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for guider\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait
