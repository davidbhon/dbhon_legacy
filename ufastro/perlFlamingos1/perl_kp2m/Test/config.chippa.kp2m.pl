#!/usr/local/bin/perl -w

###############################################################################
#
#Configure fits header parameters for next mos dither sequence
#
##############################################################################

my $rcsId = q($Name:  $ $Id: config.chippa.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Fitsheader::Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
Fitsheader::readFitsHeader($header);

#print_msg();

print "\n\n".
      ">>>---------------------The present chip pa on the sky is".
      "---------------------<<<\n\n";

Fitsheader::printFitsHeader( "CHIP-PA");
print "\n\n";

print "Change value of CHIP-PA ";
my $reply = GetYN::get_yn();
if( !$reply ){ die "Exiting\n\n" }

my $didmod = 0;
my $redo = 0;
until( $redo ){

  my $mode = select_entry_mode();

  my $chip_pa_on_sky = chg_chip_pa( "CHIP-PA", $mode );
  if( $chip_pa_on_sky != 720 ){
    Fitsheader::setNumericParam( "CHIP-PA", $chip_pa_on_sky  );
    $didmod += 1;
  }


  if( $didmod > 0 ){
    print "\n\nThe new value is: \n";
    Fitsheader::printFitsHeader( "CHIP-PA");
    print "_____________________________________________________________".
      "__________________\n";
    print "\nAccept change?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){
    Fitsheader::writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    Fitsheader::writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub print_msg{
  print "\n\n";
  print "-----------------------------------------------------------------------------\n";
  print "This will compute the Chip PA on the sky and update the CHIP-PA header field.\n";
  print "It assumes the 2.1m telescope's rotator plate has been removed so that there\n";
  print "is no way to rotate the dewar.\n\n";
  print "It further assumes that the dewar is mounted with the slits oriented N-S.\n\n";

  print "It assumes that you have measured the XY cordinates of a star imaged before and\n";
  print "after applying a DEC offset.\n\n";
  print "It will ask for the star's xy coordinates in the two images.\n\n";
}#Endsub print_msg


sub select_entry_mode{
  print "You can enter the chip's PA on the sky in one of two ways:\n";
  print "1) Enter the value directly, or\n";
  print "2) Measure stellar positions on chip after a N-S dither, and \n";
  print "   have this code compute the Chip PA\n";
  print "\n";
  print "Enter 1 or 2 to select entry mode, or q to quit: ";

  my $valid = 0;
  my $reply;
  until( $valid ){
    $reply = <>;
    chomp $reply;

    if( $reply =~ m/^1$/ ){
      $valid = 1;
    }elsif( $reply =~ m/^2$/ ){
      $valid = 1;
    }elsif( $reply =~ m/^q$/i ){
      die "Exiting\n";
    }else{
      print "Enter either 1, 2, or q\n";
    }
  }
  print "\n\n";
  return $reply;
}#Endsbu select_entry_mode


sub chg_chip_pa{
  my ($param, $mode) = @_;

  my $chip_pa_on_sky = 720;
  if( $mode == 1 ){
    #Enter measured value
    $chip_pa_on_sky = enter_chip_pa( $param );

  }elsif( $mode == 2 ){
    #Measure star position
    $chip_pa_on_sky = measure_chip_pa( $param );

  }else{
    die "Unknown entry mode\n";
  }

  return $chip_pa_on_sky;
}#Endsbu chg_chip_pa


sub enter_chip_pa{
  my $pa_chip_on_sky = 720;
  my $is_valid = 0;


  print "Assuming a Flamingos image displayed in ds9 has the following properites:\n\n";
  print "1) East is clockwise with respect to North:  N^\n";
  print "   (centered on the array)                    |\n";
  print "                                              +-->E\n";
  print "\n\n";
  print "2) The chip quadrants are:   -----------\n";
  print "                             |    |    |         (where the center of   )\n";
  print "                             | q1 | q4 |         (of rotation of the   )\n";
  print "                             |---------|         (mos wheel is at the   )\n";
  print "                             |    |    |         (bottom of the display,)\n";
  print "                             | q2 | q3 |         (below q2 and q3       )\n";
  print "                             -----------\n";
  print "                              Mos Hub   \n";
  print "\n";
  print "Enter the angle between North and the line separating\n";
  print "the left (q1 and q2) and right (q4 and q3) halves of the array.\n";
  print "Enter a real number, e.g. 1.5, or 90. (include leading zero & decimal point):  ";

  my $response = 0;
  my $valid    = 0;
  until( $valid ){
    chomp( $response = <> );
    if( $response =~ m/[a-zA-Z]/ ){
      print "Enter a real number, e.g. 1.5, or 90. (include leading zero & decimal point):  ";
    }else{
      if( $response =~ m/^-{0,1}\d{1,3}\.{1}\d*$/ ){
	print "response = $response, matched test 1\n";
	$pa_chip_on_sky = $response;
	$valid = 1;
      }else{
	print "Enter a real number, e.g. 1.5, or 90. (include leading zero & decimal point):  ";
      }
    }
  }

  return $pa_chip_on_sky;
}#Endsub enter_chip_pa


sub measure_chip_pa{
  my $pa_chip_on_sky = 720;
  my $is_valid = 0;

  my $iparm = param_index( 'CHIP-PA' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm]."\n\n";

  my $star_angle_on_chip = compute_star_angle_on_chip();

  $pa_chip_on_sky = -1 * $star_angle_on_chip;
  print "star angle on chip = $star_angle_on_chip\n";
  print "pa of chip on sky  = $pa_chip_on_sky\n";

  return $pa_chip_on_sky;
}#Endsub chg_chip_pa


sub compute_star_angle_on_chip{
  print "Enter the XY coordinates of the star imaged at the\n".
        "top of the array (larger Y values)\n";
  print "X-coordinate:  ";
  my $xtop = input_pos_real();

  print "Y-coordinate:  ";
  my $ytop = input_pos_real();

  print "\n";
  print "Enter the XY coordinates of the star imaged at the\n".
         "bottom of the array (smaller Y values)\n";
  print "X-coordinate:  ";
  my $xbot = input_pos_real();

  print "Y-coordinate:  ";
  my $ybot = input_pos_real();

  if( $ybot == $ytop ){
    die "\n\nExiting\n".
        "Please use images of the same star imaged at two different Y values.\n\n";
  }

  print "\n";
  print "xy top = $xtop, $ytop; xy bot = $xbot, $ybot\n";

  my $dx = $xtop - $xbot; my $dy = $ytop - $ybot;
  use Math::Trig;
  my $star_angle_on_chip_rad = atan( $dx / $dy );
  my $star_angle_on_chip_deg = rad2deg( $star_angle_on_chip_rad );
  $star_angle_on_chip_deg = sprintf "%.2f", $star_angle_on_chip_deg;

  return $star_angle_on_chip_deg
}#Endsub compute_star_angle_on_chip


sub input_pos_real{
  my $response = 0; my $is_valid = 0;
  until( $is_valid ){
    chomp( $response = <> );

    if( $response =~ m/[a-zA-Z]/ ){
      print "Enter a positive real number:  ";
    }elsif( $response =~ m/^\.$/ ){
      print "Enter a positive real number:  ";
    }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response > 0 ){
	    #print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a value > 0\n";}
	}
      }
  }
  return $response;
}#Endsub input_pos_real
