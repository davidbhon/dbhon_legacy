#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.info.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Math::Round qw/round/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cmd_info = $gdrrpc." info";
print "\n";
print "Asking for present info\n";
my $info_reply = `$cmd_info`;

chomp $info_reply;

my @info = split / /,$info_reply;
my $status            = $info[0];
my $guide_center_x    = $info[1];
my $guide_center_y    = $info[2];
my $total_counts      = $info[3];
my $fwhm              = $info[4];
my $bgd               = $info[5];
my $max               = $info[6];
my $iterations        = $info[7];
my $leaky_frames_avgd = $info[8];
my $box_size          = $info[9];
my $mode              = $info[10];
my $algorithm         = $info[11];
my $camera            = $info[12];
my $box_x             = $info[13];
my $box_y             = $info[14];

#for( my $i = 0; $i < @info; $i++ ){
#  print "$i = $info[$i]\n";
#}

#print "Un-rounded guide-center = $box_x, $box_y\n";
$box_x = Math::Round::round( $box_x );
$box_y = Math::Round::round( $box_y );

my $guiding;
if( $status eq "0" ){
  $guiding = "0 = off";
}elsif( $status eq "1" ){
  $guiding = "1 = calc mode";

}elsif( $status eq "2" ){
  $guiding = "2 = on";
}

print "\n";
print "Guiding          = $guiding\n";
print "Guiding center   = $guide_center_x, $guide_center_y\n";
print "Guide box Center = $box_x, $box_y\n";
print "Guide box size   = $box_size\n";
print "\n";
print "Total counts     = $total_counts\n";
print "bgd              = $bgd\n";
print "FWHM             = $fwhm\n";
print "\n";
