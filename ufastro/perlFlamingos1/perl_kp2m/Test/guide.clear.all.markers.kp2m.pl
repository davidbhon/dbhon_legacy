#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.clear.all.markers.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use GetYN;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

print "\nThis will clear _ALL_ of the markers.\n";
GetYN::query_ready();

my $cmd_clear = $gdrrpc." marker clear";

#print "SHOULD BE: Executing $cmd_clear\n";
print "Executing $cmd_clear\n";
my $cmd_reply = `$cmd_clear`;
