#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.select.pos.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cnt = @ARGV;
if( $cnt < 1 ) {
  usage();
}else{
  my $state = shift;

  if( $state =~ m/^1$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos1( $gdrrpc );

  }elsif( $state =~ m/^2$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos2( $gdrrpc );

  }elsif( $state =~ m/^3$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos3( $gdrrpc );

  }elsif( $state =~ m/^4$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos4( $gdrrpc );

  }else{
    usage();

  }

  print "Turn guiding on at this location? ";
  my $reply = GetYN::get_yn();
  if( !$reply ){
    die "Exiting without enabling guiding.\n";
  }else{
    Gdr_Rpc::guide_on( $gdrrpc );
  }
}#End


sub usage{

   die "\n\tUsage: guide.select.pos.kp2m.pl 1|2|3|4".
       "\n\tThis will move the guide box to the selected guide position.".
       "\n\tNOTES:  1) Guiding will be turned off.".
       "\n\t        2) This script will not turn guiding on, but will prompt you.\n\n";

}#Endsub usage
