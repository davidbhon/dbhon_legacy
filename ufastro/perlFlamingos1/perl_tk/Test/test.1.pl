#!/usr/local/bin/perl -w

my $rcdId = q($Name:  $ $Id: test.1.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Tk;

my $mw = MainWindow->new;

$mw -> title( "Hello World" );
$mw -> Button( -text => "Done", -command => sub { exit } )
    -> pack;

MainLoop;
