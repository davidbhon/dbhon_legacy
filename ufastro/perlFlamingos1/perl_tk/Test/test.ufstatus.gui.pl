#!/usr/local/bin/perl -w

my $rcdId = q($Name:  $ $Id: test.ufstatus.gui.pl 14 2008-06-11 01:49:45Z hon $);

use Getopt::Long;
use strict;
use Tk;

use AllbutMosWheel qw/:all/;
use Fitsheader     qw/:all/;
use MosWheel       qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = Fitsheader::select_header( $DEBUG );
Fitsheader::Find_Fitsheader( $header );
Fitsheader::readFitsHeader( $header );

my $array_temp  = -0;
my $fanout_temp = -1;
my $mos_temp    = -2;

my @bias = ( "0", "unknown" );

my (@net_motors, @net_wheels);
my (@net_actual_pos, @net_closest_pos, @net_difference);
my (@net_closest_name, @net_motion);
my @net_home_types;

@net_motors = qw( a b c d e );
@net_wheels = qw( decker mos_or_slit filter lyot grism );
@net_actual_pos  = (  -0,  -1,  -2,  -3,  -4 );
@net_closest_pos = ( -10, -11, -12, -13, -14 );
@net_difference  = (  10,  10,  10,  10,  10 );
@net_closest_name = qw( _home _home _home _home _home );
@net_motion       = qw( _stationary _stationary _stationary _stationary _stationary );
@net_home_types   = qw( _Limit _Limit _Limit _Limit _Limit );
my @net_home_types_values = ( 1, 1, 1, 1, 1 );

my $update_text = "Update All Items";

my $mw = MainWindow->new;
$mw -> title( "UFSTATUS GUI" );


my $menu_f = $mw -> Frame( -relief => 'ridge', -borderwidth => 2 );
$menu_f    -> pack( -side => 'top', -anchor => 'n', -expand => 1, -fill => 'x' );


my $quit_mb   = $menu_f -> Menubutton( -text    => "Quit",
				       -tearoff => 0,
				       -menuitems => [ ['command' => "Exit Program",
						       -command  => \&Quit]
						      ]
				       ) -> pack( -side => 'left' );

my $update_mb = $menu_f -> Menubutton( -text    => "Update",
				       -tearoff => 0,
				       -menuitems => [
						      [ 'command' => "Bias", 
						        -command  => [\&Get_Bias,
								      \@bias] ],

						      [ 'command' => "Temperatures",
							-command  => \&Get_Temps ],

						      [ 'command' => "Wheel Info",
							-command  => [\&Get_Motor_Status,
								      \@net_motors,
								      \@net_wheels,
								      \@net_actual_pos,
								      \@net_closest_pos,
								      \@net_difference,
								      \@net_closest_name,
								      \@net_motion,
								      \@net_home_types,
								      \@net_home_types_values,
								      \$header ] ]
						     ]
				     ) -> pack( -side => 'left' );

my $engineering_mb   = $menu_f -> Menubutton( -text    => "Engineering",
				       -tearoff => 0,
				       -menuitems => [ ['command' => "Run ufstatus.pl in parent xterm",
						       -command  => \&Run_ufstatus]
						      ]
				       ) -> pack( -side => 'left' );

Motors( \@net_motors,
        \@net_wheels,
	\@net_actual_pos,
	\@net_closest_pos,
	\@net_difference,
	\@net_closest_name,
	\@net_motion,
	\@net_home_types,
	\@net_home_types_values,
	\$header,
      );

Temps();

my $update_f = $mw -> Frame(
			    #-background => "blue",
			    ) -> pack( -side => 'left', -fill => 'both', -expand => 1 );

my $update_b = $update_f -> Button(
				   -background => 'light green',
				   -command => \&Update_All,
				   #-text => "Update All Items",
				   -textvariable => \$update_text,
				  ) -> pack( -side => 'top', -fill => 'x' );


Bias( \@bias );

MainLoop;

##############
sub Bias{
  my $ar_bias = $_[0];

  my $bias_f   = $mw -> Frame(
			       -background  => "black",
			       -relief      => "groove",
			       -borderwidth => "5"
			       ) -> pack( -side => 'right', -anchor => "w",
					  -fill => 'y');

  my $bias_ll = $bias_f -> Label(
				 -background => "light blue",
				 -text   => "Array Bias (V)",
				 -relief => 'flat'
				) -> grid( -row => 0, -column => 0,
					   -sticky => "nsew" );

  my $bias_value_ll = $bias_f -> Label(
				       -textvariable => \$$ar_bias[0],
				       -relief       => "sunken"
				       ) -> grid( -row => 1, -column => 0,
						  -sticky => "nsew" );

  my $bias_interp_ll = $bias_f -> Label(
					-textvariable => \$$ar_bias[1],
					-relief       => "sunken"
					) -> grid( -row => 2, -column => 0,
						   -sticky => "nsew" );

  my $blank_ll = $bias_f -> Label() -> grid( -row => 3, -column => 0,
					     -sticky => "nsew" );

}#Endsub Bias


sub Get_Bias{
  my $ar_bias = $_[0];

  my $reply = `ufdc -q dac_dump_bias`;
  #print $reply . "\n";
  my @input_array = split /\.\.\.\./, $reply;
  my @split_input = split /=/, $input_array[1];
  my @resplit_input = split /\(/, $split_input[3];
  my $bin_bias = $resplit_input[0];
  #print( $bin_bias / 255 . "\n" );

  my $bias;
  my $bias_interpretation;
  $bias = $bin_bias / 255;

  if(  $bias == 1    ){ $bias_interpretation = "imaging" };
  if(  $bias == 0.75 ){ $bias_interpretation = "spectroscopy" };
  if( ($bias != 1) && ($bias != 0.75) ){ $bias_interpretation = "User's choice"};

  $$ar_bias[0] = $bias;
  $$ar_bias[1] = $bias_interpretation;

}#Endsub Get_Bias


sub Get_Home_Types{
  my ($header, $ar_net_home_types_values, $ar_net_home_types ) = @_;

  $$ar_net_home_types_values[0] = Fitsheader::param_value( "HT_0" );#motor a = decker
  $$ar_net_home_types_values[1] = Fitsheader::param_value( "HT_1" );#motor b = mos
  $$ar_net_home_types_values[2] = Fitsheader::param_value( "HT_2" );#motor c = filter
  $$ar_net_home_types_values[3] = Fitsheader::param_value( "HT_3" );#motor d = lyot
  $$ar_net_home_types_values[4] = Fitsheader::param_value( "HT_4" );#motor e = grism

  for(my $i = 0; $i<5; $i++){
    if( $$ar_net_home_types_values[$i] == 0 ){
      $$ar_net_home_types[$i] = "Near";
    }elsif( $$ar_net_home_types_values[$i] == 1 ){
      $$ar_net_home_types[$i] = "Limit";
    }
  }
}#Endsub get_home_types


sub Get_Motor_Status{
  my ( $ar_net_motors, $ar_net_wheels, $ar_net_actual_pos,
       $ar_net_closest_pos, $ar_net_difference, $ar_net_closest_name,
       $ar_net_motion, $ar_net_home_types, $ar_net_home_types_values,
       $sr_header) = @_;

  print "Should be getting Motor Status\n";

  my ( $ar1, $ar2, $ar3, $ar4,
      $ar5, $ar6, $ar7 ) =  AllbutMosWheel::get_wheel_status();

  my ($mos_actual_pos, $mos_motion, $mos_closest_pos, $mos_closest_name,
      $mos_difference) = MosWheel::get_mos_status();

  my @motors       = @$ar1; my @wheels      = @$ar2;
  my @actual_pos   = @$ar3; my @closest_pos = @$ar4;
  my @closest_name = @$ar5; my @difference  = @$ar6;
  my @motion       = @$ar7;

  $$ar_net_motors[0] = $motors[0];
  $$ar_net_motors[1] = qw( b );
  $$ar_net_motors[2] = $motors[1];
  $$ar_net_motors[3] = $motors[2];
  $$ar_net_motors[4] = $motors[3];

  $$ar_net_wheels[0] = $wheels[0];
  $$ar_net_wheels[1] = qw( mos_or_slit );
  $$ar_net_wheels[2] = $wheels[1];
  $$ar_net_wheels[3] = $wheels[2];
  $$ar_net_wheels[4] = $wheels[3];

  $$ar_net_actual_pos[0] = $actual_pos[0];
  $$ar_net_actual_pos[1] = $mos_actual_pos;
  $$ar_net_actual_pos[2] = $actual_pos[1];
  $$ar_net_actual_pos[3] = $actual_pos[2];
  $$ar_net_actual_pos[4] = $actual_pos[3];

  $$ar_net_closest_pos[0] = $closest_pos[0];
  $$ar_net_closest_pos[1] = $mos_closest_pos;
  $$ar_net_closest_pos[2] = $closest_pos[1];
  $$ar_net_closest_pos[3] = $closest_pos[2];
  $$ar_net_closest_pos[4] = $closest_pos[3];

  $$ar_net_difference[0] = $difference[0];
  $$ar_net_difference[1] = $mos_difference;
  $$ar_net_difference[2] = $difference[1];
  $$ar_net_difference[3] = $difference[2];
  $$ar_net_difference[4] = $difference[3];

  $$ar_net_closest_name[0] = $closest_name[0];
  $$ar_net_closest_name[1] = $mos_closest_name;
  $$ar_net_closest_name[2] = $closest_name[1];
  $$ar_net_closest_name[3] = $closest_name[2];
  $$ar_net_closest_name[4] = $closest_name[3];

  $$ar_net_motion[0] = $motion[0];
  $$ar_net_motion[1] = $mos_motion;
  $$ar_net_motion[2] = $motion[1];
  $$ar_net_motion[3] = $motion[2];
  $$ar_net_motion[4] = $motion[3];

  main::Get_Home_Types( $header, $ar_net_home_types_values, $ar_net_home_types );

}#Endsub Get_Motor_Status


sub Get_Temps{

  my $reply = `uflsc -q 1`;
  chomp( $reply );
  my @temps = split / /, $reply;
  shift @temps;
  $temps[0] =~ s/^1,//;
  $temps[1] =~ s/^2,//;
  $temps[2] =~ s/^6,//;
  #print "array = $temps[0], fanout = $temps[1], mos = $temps[2]\n";

  $array_temp  = $temps[0];
  $fanout_temp = $temps[1];
  $mos_temp    = $temps[2];

  #return( $array_temp, $fanout_temp, $mos_temp );
}#Endsub Get_Temps


sub Motors{
  my ( $ar_net_motors, $ar_net_wheels, $ar_net_actual_pos,
       $ar_net_closest_pos, $ar_net_difference, $ar_net_closest_name,
       $ar_net_motion, $ar_net_home_types, $ar_net_home_types_values,
       $sr_header ) = @_;

  my $motor_f         = $mw -> Frame(
				     -background  => 'black',
				     -relief      => 'groove',
				     -borderwidth => '5',
				    ) -> pack();

  #my $motor_status_b = $motor_f -> Button(
  #					  -text    => "Get Motor Status",
  #					  -command => [\&Get_Motor_Status,
  #						       $ar_net_motors,
  #						       $ar_net_wheels,
  #						       $ar_net_actual_pos,
  #						       $ar_net_closest_pos,
  #						       $ar_net_difference,
  #						       $ar_net_closest_name,
  #						       $ar_net_motion,
  #						       $ar_net_home_types,
  #						       $ar_net_home_types_values,
  #						       $sr_header,
  #  						      ]
  #					 )
  #                     -> grid( -row => 6, -column => 3,);

  my $motor_title_ll = $motor_f -> Label(
					 -background => 'light blue',
					 -text => "STATUS OF MOTORS & WHEELS",
					 -relief => 'flat'
					 ) -> grid( -row => 0, -column => 0,
						    -columnspan => 8, -sticky => "nsew" );

  my $motor_ll       = $motor_f -> Label(
					 -text  => "Motor",
					 -relief => 'flat',
					)
                       -> grid( -row => 1, -column => 0,
				-sticky => "nsew");

  my $motor_a_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_motors[0],
					 -relief       => 'sunken',
					)
                       -> grid( -row => 2, -column => 0,
				-sticky => "nsew",
			      );

  my $motor_b_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_motors[1],
					 -relief       => 'sunken',
					)
                       -> grid( -row => 3, -column => 0,
					 -sticky => "nsew",
			      );

  my $motor_c_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_motors[2],
					 -relief       => 'sunken',
					)
                       -> grid( -row => 4, -column => 0,
				-sticky => "nsew",
			      );

  my $motor_d_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_motors[3],
					 -relief       => 'sunken',
					)
                       -> grid( -row => 5, -column => 0,
				-sticky => "nsew",
			      );

  my $motor_e_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_motors[4],
					 -relief       => 'sunken',
					)
                       -> grid( -row => 6, -column => 0,
				-sticky => "nsew",
			      );

  #----------------
  my $wheel_name_ll       = $motor_f -> Label(
					      -text  => "Wheel",
					      -relief => 'flat',
					     ) -> grid( -row => 1, -column => 1,
							-sticky => "nsew", );

  my $wheel_a_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_wheels[0],
					 -relief       => 'sunken',
					) -> grid( -row => 2, -column => 1,
						   -sticky => "nsew", );

  my $wheel_b_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_wheels[1],
					 -relief       => 'sunken',
					) -> grid( -row => 3, -column => 1,
						   -sticky => "nsew", );

  my $wheel_c_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_wheels[2],
					 -relief       => 'sunken',
					) -> grid( -row => 4, -column => 1,
						   -sticky => "nsew", );

  my $wheel_d_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_wheels[3],
					 -relief       => 'sunken',
					) -> grid( -row => 5, -column => 1,
						   -sticky => "nsew", );

  my $wheel_e_vl     = $motor_f -> Label(
					 -textvariable => \$$ar_net_wheels[4],
					 -relief       => 'sunken',
					) -> grid( -row => 6, -column => 1,
						   -sticky => "nsew", );

  #----------------
  my $wheel_posn_ll       = $motor_f -> Label(
					      -text  => "Actual\nPosition",
					      -relief => 'flat',
					     ) -> grid( -row => 1, -column => 2,
							-sticky => "nsew", );

  my $wheel_posn_a_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_actual_pos[0],
					      -relief       => 'sunken',
					) -> grid( -row => 2, -column => 2,
						   -sticky => "nsew", );

  my $wheel_posn_b_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_actual_pos[1],
					      -relief       => 'sunken',
					     ) -> grid( -row => 3, -column => 2,
							-sticky => "nsew", );

  my $wheel_posn_c_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_actual_pos[2],
					      -relief       => 'sunken',
					     ) -> grid( -row => 4, -column => 2,
							-sticky => "nsew", );

  my $wheel_posn_d_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_actual_pos[3],
					      -relief       => 'sunken',
					     ) -> grid( -row => 5, -column => 2,
							-sticky => "nsew", );

  my $wheel_posn_e_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_actual_pos[4],
					      -relief       => 'sunken',
					     ) -> grid( -row => 6, -column => 2,
							-sticky => "nsew", );

  #----------------
  my $closest_posn_ll       = $motor_f -> Label(
						-text  => "Closest\nPosition",
						-relief => 'flat',
					       ) -> grid( -row => 1, -column => 3,
							  -sticky => "nsew", );

  my $closest_posn_a_vl     = $motor_f -> Label(
						-textvariable => \$$ar_net_closest_pos[0],
						-relief       => 'sunken',
					       ) -> grid( -row => 2, -column => 3,
							  -sticky => "nsew", );

  my $closest_posn_b_vl     = $motor_f -> Label(
						-textvariable => \$$ar_net_closest_pos[1],
						-relief       => 'sunken',
					       ) -> grid( -row => 3, -column => 3,
							  -sticky => "nsew", );

  my $closest_posn_c_vl     = $motor_f -> Label(
						-textvariable => \$$ar_net_closest_pos[2],
						-relief       => 'sunken',
					       ) -> grid( -row => 4, -column => 3,
							  -sticky => "nsew", );

  my $closest_posn_d_vl     = $motor_f -> Label(
						-textvariable => \$$ar_net_closest_pos[3],
						-relief       => 'sunken',
					       ) -> grid( -row => 5, -column => 3,
							  -sticky => "nsew", );

  my $closest_posn_e_vl     = $motor_f -> Label(
						-textvariable => \$$ar_net_closest_pos[4],
						-relief       => 'sunken',
					       ) -> grid( -row => 6, -column => 3,
							  -sticky => "nsew", );

  #----------------
  my $likely_name_ll       = $motor_f -> Label(
					       -text  => "Likely\nName",
					       -relief => 'flat',
					      ) -> grid( -row => 1, -column => 4,
							 -sticky => "nsew", );

  my $likely_name_a_vl     = $motor_f -> Label(
					       -textvariable => \$$ar_net_closest_name[0],
					       -relief       => 'sunken',
					      ) -> grid( -row => 2, -column => 4,
							 -sticky => "nsew", );

  my $likely_name_b_vl     = $motor_f -> Label(
					       -textvariable => \$$ar_net_closest_name[1],
					       -relief       => 'sunken',
					      ) -> grid( -row => 3, -column => 4,
							 -sticky => "nsew", );

  my $likely_name_c_vl     = $motor_f -> Label(
					       -textvariable => \$$ar_net_closest_name[2],
					       -relief       => 'sunken',
					      ) -> grid( -row => 4, -column => 4,
							 -sticky => "nsew", );

  my $likely_name_d_vl     = $motor_f -> Label(
					       -textvariable => \$$ar_net_closest_name[3],
					       -relief       => 'sunken',
					      ) -> grid( -row => 5, -column => 4,
							 -sticky => "nsew", );

  my $likely_name_e_vl     = $motor_f -> Label(
					       -textvariable => \$$ar_net_closest_name[4],
					       -relief       => 'sunken',
					      ) -> grid( -row => 6, -column => 4,
							 -sticky => "nsew", );

  #----------------
  my $difference_ll       = $motor_f -> Label(
					      -text  => "Difference",
					      -relief => 'flat',
					     ) -> grid( -row => 1, -column => 5,
							-sticky => "nsew", );

  my $difference_a_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_difference[0],
					      -relief       => 'sunken',
					     ) -> grid( -row => 2, -column => 5,
							-sticky => "nsew", );

  my $difference_b_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_difference[1],
					      -relief       => 'sunken',
					     ) -> grid( -row => 3, -column => 5,
							-sticky => "nsew", );

  my $difference_c_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_difference[2],
					      -relief       => 'sunken',
					     ) -> grid( -row => 4, -column => 5,
							-sticky => "nsew", );

  my $difference_d_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_difference[3],
					      -relief       => 'sunken',
					     ) -> grid( -row => 5, -column => 5,
							-sticky => "nsew", );

  my $difference_e_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_difference[4],
					      -relief       => 'sunken',
					     ) -> grid( -row => 6, -column => 5,
							-sticky => "nsew", );

  #----------------
  my $motion_ll       = $motor_f -> Label(
					  -text  => "Motion",
					  -relief => 'flat',
					 ) -> grid( -row => 1, -column => 6,
						    -sticky => "nsew", );

  my $motion_a_vl     = $motor_f -> Label(
					  -textvariable => \$$ar_net_motion[0],
					  -relief       => 'sunken',
					 ) -> grid( -row => 2, -column => 6,
						    -sticky => "nsew", );

  my $motion_b_vl     = $motor_f -> Label(
					  -textvariable => \$$ar_net_motion[1],
					  -relief       => 'sunken',
					 ) -> grid( -row => 3, -column => 6,
						    -sticky => "nsew", );

  my $motion_c_vl     = $motor_f -> Label(
					  -textvariable => \$$ar_net_motion[2],
					  -relief       => 'sunken',
					 ) -> grid( -row => 4, -column => 6,
						    -sticky => "nsew", );

  my $motion_d_vl     = $motor_f -> Label(
					  -textvariable => \$$ar_net_motion[3],
					  -relief       => 'sunken',
					 ) -> grid( -row => 5, -column => 6,
						    -sticky => "nsew", );

  my $motion_e_vl     = $motor_f -> Label(
					  -textvariable => \$$ar_net_motion[4],
					  -relief       => 'sunken',
					 ) -> grid( -row => 6, -column => 6,
						    -sticky => "nsew", );

  #----------------
  my $home_types_ll       = $motor_f -> Label(
					      -text  => "Home\nType",
					      -relief => 'flat',
					     ) -> grid( -row => 1, -column => 7,
							-sticky => "nsew", );

  my $home_types_a_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_home_types[0],
					      -relief       => 'sunken',
					     ) -> grid( -row => 2, -column => 7,
							-sticky => "nsew", );

  my $home_types_b_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_home_types[1],
					      -relief       => 'sunken',
					     ) -> grid( -row => 3, -column => 7,
							-sticky => "nsew", );

  my $home_types_c_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_home_types[2],
					      -relief       => 'sunken',
					     ) -> grid( -row => 4, -column => 7,
							-sticky => "nsew", );

  my $home_types_d_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_home_types[3],
					      -relief       => 'sunken',
					     ) -> grid( -row => 5, -column => 7,
							-sticky => "nsew", );

  my $home_types_e_vl     = $motor_f -> Label(
					      -textvariable => \$$ar_net_home_types[4],
					      -relief       => 'sunken',
					     ) -> grid( -row => 6, -column => 7,
							-sticky => "nsew", );

}#Endsub Motors


sub Run_ufstatus{
  print "Should running ufstatus\n";
  system("ufstatus.pl");

}#Endsub Run_ufstatus


sub Temps{
  my $temp_f         = $mw -> Frame(
				    -background  => "black",
				    #-label       => "Temperatures (K)",
				    #-labelPack   => [ -side   => 'top',
				    #		  ],
				    -relief      => 'groove',
				    -borderwidth => '5',
				   ) -> pack( -side => 'left', -anchor => "w");

 # my $temps_b        = $temp_f -> Button(
 #					 -text      => "Get Temps",
 #					 -takefocus => 0,
 #					 -command   => [\&Get_Temps,
 #						       ],
 #					) -> pack( -side   => 'bottom',
 #						   -fill   => 'both',
 #						   #-anchor => 'w',
 #						 );

  my $temps_title_ll = $temp_f -> Label(
					-background => 'light blue',
					-text => "TEMPERATURES (K)",
					-relief => 'flat'
					) -> grid( -row => 0, -column => 0,
						    -columnspan => 2, -sticky => "nsew" );

  my $array_temp_ll  = $temp_f -> Label(
					 -text      => "Array",
					 -relief    => 'flat',
					) -> grid( -row => 1, -column => 0,
						   -sticky => "nsew" );

  my $array_temp_vl  = $temp_f -> Label(
					 -textvariable => \$array_temp,
					 -relief       => 'sunken',
				       ) -> grid( -row => 1, -column => 1,
						  -sticky => "nsew" );


  my $fanout_temp_ll  = $temp_f -> Label(
					  -text   => "Fanout",
					  -relief => 'flat',
					 ) -> grid( -row => 2, -column => 0,
						    -sticky => "nsew" );

  my $fanout_temp_vl  = $temp_f -> Label(
					  -textvariable => \$fanout_temp,
					  -relief       => 'sunken',
					 ) -> grid( -row => 2, -column => 1,
						    -sticky => "nsew" );

  my $mos_temp_ll  = $temp_f -> Label(
				       -text   => "Mos",
				       -relief => 'flat',
				      ) -> grid( -row => 3, -column => 0,
					         -sticky => "nsew" );

  my $mos_temp_vl  = $temp_f -> Label(
				       -takefocus    => 0,
				       -textvariable => \$mos_temp,
				       -relief       => 'sunken',
				      ) -> grid( -row => 3, -column => 1,
					         -sticky => "nsew" );
}#Endsub Temps



sub Quit{
  exit;
}#Endsub Quit


sub Update_All{

  my $old_update_text = $update_text;
  $update_text = "Acquiring Information...";
  $update_b -> update();

  Get_Bias( \@bias );
  Get_Temps();
  Get_Motor_Status( \@net_motors,
  		    \@net_wheels,
  		    \@net_actual_pos,
  		    \@net_closest_pos,
  		    \@net_difference,
  		    \@net_closest_name,
  		    \@net_motion,
  		    \@net_home_types,
  		    \@net_home_types_values,
  		    \$header );

  $update_text = $old_update_text;
  $update_b -> update();
}#Endsub Update_All
