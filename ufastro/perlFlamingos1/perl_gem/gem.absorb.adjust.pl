#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use GetYN;
use ufgem qw/:all/;

print "\n";
print "The offsets shown on the TSD are the net sum of two types of offsets:\n";
print "\n";
print "  1) offsets       (e.g., executed with gem.offset.pl or gem.handset.pl)\n";
print "  2) offsetAdjusts (e.g., executed with gem.offsetAdjust.instrument.pl)\n";
print "\n";
print "This will _NOT_ move the telescope, but will absorb the offsetAdjusts\n";
print "into the base position, such that the current pointing center has net\n";
print "zero offsetAdjusts.\n";


GetYN::query_ready();

my $soc = ufgem::viiconnect();

ufgem::absorb_adjusts( $soc );

close( $soc );


__END__

=head1 NAME

gem.absorb.adjust.pl

=head1 Description

Absorbs (into the target position) the adjustment offsets only.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.absorb.adjust.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
