package GemTCSinfo;

require 5.005_62;
use strict;
use warnings;
use ufgem qw/:all/;
use Fitsheader qw/:all/;
use WCS_Info qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use GemTCSinfo ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( &getGemTCSinfo	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);



# Preloaded methods go here.
our @numeric_header_keywords =
qw( 
  AIRMASS  TELFOCUS  HA  HUMID  INSTRAA  INSTRPA  MJD  
  DEC_OFF    RA_OFF  ROTATOR  ROTERROR
  RA_BASE  DEC_BASE  EPOCH  EQUINOX  
  USRFOC  ZD
);


our @returned_numeric_keywords =
qw( 
  airmass  focus   ha  humidity  instraa  instrpa  mjd  
  offsetdec  offsetra  rotator  roterror
  targetra  targetdec  targetepoch  targetequinox
  userfocus  zd
);

our @string_header_keywords =
qw( 
   AZIMUTH  AZERROR BEAM  EL  ELERROR  FRAMEPA  LOCTIME  LST  
   OBJECT  TARGFRM  TARGSYS  RA_TEL  DEC_TEL  UTC  UTCDATE
);


our @returned_string_keywords =
qw( 
  azimuth  azerror beam  elevation  elerror  framepa  localTime  lst  
  targetname  targetframe  targetradecsys  telra  teldec  utc  utcdate
);

###> The arrays with the final output from the tcs, 
###> formatted for the FITS header.
our @numeric_key_vals;
our @string_key_vals;

###The sub that does all the work
sub getGemTCSinfo{
  my $header = $_[0];

  my $soc = ufgem::viiconnect();

  get_numeric_vals( \@returned_numeric_keywords, \@numeric_key_vals, $soc );
  format_numeric_vals(\@numeric_key_vals);
  set_numeric_vals( \@numeric_header_keywords, \@numeric_key_vals );

  get_string_vals( \@returned_string_keywords, \@string_key_vals, $soc );
  set_string_vals(  \@string_header_keywords,  \@string_key_vals  );

  my $RA      = $numeric_key_vals[11];
  my $DEC     = $numeric_key_vals[12];
  my $instrpa = $numeric_key_vals[5];
  WCS_Info::gem_set_wcs_info( $RA, $DEC, $instrpa, $header );

  print "Writing Gemini South TCS info into header\n";
  print "$header\n\n";
  writeFitsHeader( $header );

}#Endsub getGemTCSinfo


###SUBS
sub get_numeric_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my @returned_numeric_keywords = @$aref1;
  # $aref2 will be the actual array of final values = @numeric_key_vals

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_num = @returned_numeric_keywords;
  
  for( my $i = 0; $i < $num_num; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_numeric_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_numeric_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$numeric_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }

}#Endsub get_numeric_vals


sub get_string_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my $returned_string_keywords = @$aref1;

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_str = @returned_string_keywords;
  
  for( my $i = 0; $i < $num_str; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_string_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_string_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$string_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }
}#Endsub get_string_values


sub format_numeric_vals{
  my $aref = $_[0];
  my @numeric_key_vals = @$aref;
  #NUMERIC values:
  #recast output as float with same precision,
  #and right justified to 20 char long.

  my $num_num = @numeric_key_vals;
  for( my $i = 0; $i < $num_num; $i++ ){
    
    my $item = $numeric_key_vals[$i];
    my $len_val = length $item;
    if( $len_val > 20 ){
      #this one's for mjd which is a long exponential format number
      $item = sprintf "%.20f", $numeric_key_vals[$i];
    }
    my $loc_dec = index $item, ".";
    
    #print "$i: item = $item; len = $len_val; dec loc = $loc_dec\n";
    
    my $recast_format;
    if( $len_val > 20 ){
      $recast_format = "%20." . (20 - $loc_dec - 1) . "f";
    }else{
      $recast_format = "%20." . ($len_val - 1 - $loc_dec) . "f";
    }
    my $recast_val = sprintf $recast_format, $item;
    
    my $len_recast_val = length $recast_val;
    
    #if( $i == 0 ){
    #  print "---------------12345678901234567890----------\n";
    #}
    #print "$i: recast_val =$recast_val, leng = $len_recast_val\n";
    $$aref[$i] = $recast_val;
  }

}#Endsub format_numeric_vals

sub print_numeric_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @numeric_header_keywords   = @$aref1;
  my @returned_numeric_keywords = @$aref2;
  my @numeric_key_vals          = @$aref3;

  my $num_num = @returned_numeric_keywords;
  print "\n\nNumeric Values\n";
  for( my $i = 0; $i < $num_num; $i++ ){
    #  print "$i = $numeric_header_keywords[$i] =\t ".
    #    "$returned_numeric_keywords[$i] =12345678901234567890\n";
    print "$i = $numeric_header_keywords[$i] =\t ".
      "$returned_numeric_keywords[$i] =$numeric_key_vals[$i]\n";
  }
  
}#End print_numeric_vals


sub print_string_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @string_header_keywords   = @$aref1;
  my @returned_string_keywords = @$aref2;
  my @string_key_vals          = @$aref3;

  my $num_str = @returned_string_keywords;
  print "\n\nString Values\n";
  for( my $i = 0; $i < $num_str; $i++ ){
    print "$i = $string_header_keywords[$i] =\t ".
      "$returned_string_keywords[$i] =$string_key_vals[$i]\n";
  }
  print "\n\n";

}#Endsub print_string_vals


sub print_present_settings{
  my $aref = $_[0];
  my @header_keywords = @$aref;

  my $num_items = @header_keywords;
  for( my $i = 0; $i < $num_items; $i++){
    printFitsHeader( $header_keywords[$i] );
  }

}#Endsub print_present_settings


sub set_numeric_vals{
  my ( $aref1, $aref2 ) = @_;
  my @numeric_header_keywords = @$aref1;
  my @numeric_key_vals        = @$aref2;

  my $num_num = @numeric_header_keywords;
  #print "$num_num\n";

  for( my $i = 0; $i < $num_num; $i++){
    setNumericParam( $numeric_header_keywords[$i], 
    		     $numeric_key_vals[$i] );

  }

}#Endsub set_numeric_vals


sub set_string_vals{
  my ( $aref1, $aref2 ) = @_;
  my @string_header_keywords = @$aref1;
  my @string_key_vals        = @$aref2;

  my $num_str = @string_header_keywords;

  for( my $i = 0; $i < $num_str; $i++ ){
    #The vii returns strings with a leading space
    #strip them off if present
    #
    my $val = $string_key_vals[$i];
    my $space = index $val, " ";
    if( $space == 0 ){
      my $len = length $val;
      $string_key_vals[$i] = substr $val, 1, ($len - 1);
    }

    setStringParam( $string_header_keywords[$i],
    		    $string_key_vals[$i] );    
    #print "$i $string_header_keywords[$i] =$string_key_vals[$i]\n";    
  }

}#Endsub set_string_vals




# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

GemTCSinfo - Perl extension for Flamingos

=head1 SYNOPSIS

  use GemTCSinfo qw/:all/;
  

=head1 DESCRIPTION

Gets numeric and string parameters for Fits header from 
Gemini tcs

=head2 EXPORT

getGemTCSinfo( $header );


=head1 REVISION & LOCKER

$Name:  $

$Id: GemTCSinfo.pm,v 0.1 2003/05/22 15:39:51 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR Aug/Sep 2001

=head1 SEE ALSO

perl(1).

=cut
