#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;
use IO::Socket;

my $soc = ufgem::viiconnect();

print $soc "get rotator $ufgem::terminate";
$soc->flush();
my $rotator = <$soc>;
chomp $rotator;

print $soc "get parangle $ufgem::terminate";
$soc->flush();
my $parangle = <$soc>;
chomp $parangle;

print $soc "get instrAA $ufgem::terminate";
$soc->flush();
my $instrAA = <$soc>;
chomp $instrAA;

print $soc "get instrPA $ufgem::terminate";
$soc->flush();
my $instrPA = <$soc>;
chomp $instrPA;

close($soc);

print "\n";
print "rotator  = $rotator\n";
print "parangle = $parangle\n";
print "instr AA = $instrAA\n";

print "Should have PA = parangle - rotator + align_angle\n";
print "instr PA = $instrPA\n";


__END__

=head1 NAME

gem.get.angles.pl

=head1 Description

Asks the TCS for angle info.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.get.angles.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
