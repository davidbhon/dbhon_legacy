#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: gettcsinfo.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

BEGIN {
  use lib "/export/home/raines/Perl/perl_gem";
  require "gemsouth.plib";
}

#my $soc = ufgem::viiconnect();
#my $reply = ufgem::getStatus($soc);

#sample output when telescope off at zenith
my $reply = 
"got airmass 1.000
got azerror +00:00:00.02
got azimuth +90:42:18.64
got basey  0.00000000000000e+00
got basey  0.00000000000000e+00
got beam A
got chopbeam A
got chopdutycycle 0.00
got chopfreq 0.000
got choppa 0.000000
got chopping NO
got chopthrow 0.000
got elevation +90:02:36.49
got elerror -00:02:36.48
got focus 0.001
got framepa FK5
got ha 0.0031
got health BAD
got humidity 40.0
got instraa 0.00
got instrpa 0.00
got localtime 17:27:58.1
got lst 14:57:53.9
got mjd  5.21458944226985e+04
got nodmode standard
got nodpa 0.0
got nodthrow 0.0
got nodtype tangent plane
got offsetdec -0.000
got offsetra 0.000
got offsetx  0.00000000000000e+00
got offsety  0.00000000000000e+00
got rotator -89.999
got roterror -0.000
got targetname None set
got targetra 0.00000000
got targetdec 0.00000000
got targetepoch 2000.0
got targetequinox 2000.0
got targetframe FK5
got targetradecsys FK5
got telescope Gemini-South
got telra 14:57:38.287
got teldec -30:13:59.30
got userfocus 0.000
got utc 21:27:58.1
got zd -0.0435
";

print $reply;

my $working_copy = $reply;
my $got_str = "got ";
my $len_got_str = length $got_str;
my $this_got_index;
my $trim_length = length $reply;

my $first_got = index $reply, $got_str;
substr $working_copy, 0, $len_got_str, "";

print "trim_length = $trim_length\n";
my $second_got_index = index $working_copy, $got_str;
my $first_var_str   = substr $working_copy, 0, $second_got_index;

print "first_var_str = $first_var_str\n";

substr $working_copy, 0, $second_got_index + $len_got_str, "";
print $working_copy;
#until($trim_length == 0){
#  print "trim_length = $rleng\n";
#  $this_got_index = index $working_copy, $got_str;
#  my $this_var = 
#}


my @all_gemini_header_keys =

qw( 
  AIRMASS
  AZIMUTH
  AZERROR
  DATALAB
  EL
  ELERROR
  TELFOCUS
  FRAMEPA
  GEMPRGID
  HA
  HUMID
  INSTRAA
  INSTRPA
  LOCTIME
  LST
  MJD
  OBSID
  OBSERVAT
  OBSERVER
  DEC_OFF
  RA_OFF
  ORIGIN
  PIXSCALE
  RAWBG
  RAWCC
  RAWIQ
  RAWGEMQA
  RAWPIREQ
  RAWWV
  ROTATOR
  ROTERROR
  SSA
  OBJECT
  RA_BASE
  DEC_BASE
  EPOCH
  EQUINOX
  TARGFRM
  TARGSYS
  TELESCOP
  RA_TEL
  DEC_TEL
  USRFOC
  UTC
  DATE-OBS
  ZD
);


my @synonyms_for_desired_gemini_header_keys =

qw( 
  airmass
  azimuth
  azerror
  none-datalab
  elevation
  elerror
  focus
  framepa
  none-gemprgid
  ha
  humidity
  instraa
  instrpa
  'localtime'
  lst
  mjd
  none-obsid
  none-observat
  none-observer
  offsetdec
  offsetra
  none-origin
  none-pixscale
  none-rawbg
  none-rawcc
  none-rawiq
  none-rawgemqa
  none-rawpireq
  none-wwv
  rotator
  roterror
  none-ssa
  targetname
  targetra
  targetdec
  targetepoch
  targetequinox
  targetframe
  targetradecsys
  telescop
  telra
  teldec
  userfocus
  utc
  none-date-obs
  zd
);

my $num_all_keys = @all_gemini_header_keys;
my $num_desired_keys = @synonyms_for_desired_gemini_header_keys;

print "length of all keys = " . $num_all_keys
      ."\n";

print "length of desired keys = ".
       $num_desired_keys
       ."\n";
