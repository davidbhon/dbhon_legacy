#!/usr/local/bin/perl -w

## Execute dither patterns at Gemini South
## This script uses relative offsets wrt to the
## current pointing center

use strict;

use Dither_patterns qw/:all/;
use ufgem qw/:all/;

my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii

my $dpattern = 'pat_2x2           ';
my $startpos = 0;
my $net_scale_factor = 0.167;
my $d_rptpos = 1;
my $d_rptpat = 1;
my $usenudge = 1;
my $nudgesiz = 1;

#### Load dither patterns
my @pat_x        = Dither_patterns::load_pattern_rel_x(   $dpattern );
my @pat_y        = Dither_patterns::load_pattern_rel_y(   $dpattern );
my @rel_offset_x = Dither_patterns::load_pat_rel_Xorigin( $dpattern );
my @rel_offset_y = Dither_patterns::load_pat_rel_Yorigin( $dpattern );
my @wrap_x_y     = Dither_patterns::load_pattern_wrap(    $dpattern );

@pat_x        = Dither_patterns::scale_pattern_x_or_y( \@pat_x,        $net_scale_factor );
@pat_y        = Dither_patterns::scale_pattern_x_or_y( \@pat_y,        $net_scale_factor );
@rel_offset_x = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_x, $net_scale_factor );
@rel_offset_y = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_y, $net_scale_factor );
@wrap_x_y     = Dither_patterns::scale_pattern_wrap(   \@wrap_x_y,     $net_scale_factor );

my $last_index = 1;
my $this_index = $last_index + 1;

print_setup();

my $soc;
if( $VIIDO ){
  print "Connecting to the VII\n";
  $soc = ufgem::viiconnect();
  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
  }
}else{
  print "Viido = $VIIDO; should be doing vii test connection\n\n";
}

my $probe_name = ufgem::query_probe_name();
ufgem::prompt_start_guiding_beam_A();

my $x_nudge = 0;
my $y_nudge = 0;
my $next_index = $this_index;

my $X;
my $Y;
my $X_origin;
my $Y_origin;
my $last_pos_num  = @pat_x - 2;#num moves is @pat_x-1, then -1 to start count at 0
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ($x_nudge, $y_nudge) = Dither_patterns::nudge_pointing($d_rptpat, $rpt_pat, $usenudge, $nudgesiz);
  $X_origin = $rel_offset_x[$startpos] + $x_nudge;
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position

    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){ 
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "offset to position $startpos: $X, $Y\n";
      ufgem::setOffset_radec( $soc, $X, $Y );
      ufgem::offset_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "***offset to position $startpos: $X, $Y\n";
	   print "***"; ufgem::offset_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    $X = $wrap_x_y[0] + $x_nudge;
    $Y = $wrap_x_y[1] + $y_nudge;
    if( $VIIDO ){
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "handset by $X, $Y, back to position $startpos\n";
      ufgem::setHandset_radec( $soc, $X, $Y );
      ufgem::handset_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "handset by $X, $Y, back to position $startpos\n";
	   print "***"; ufgem::handset_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	 }

  }

  print "SHOULD BE: Should image $d_rptpos times here\n\n";
  $next_index = take_an_image( $d_rptpos, $next_index );

  my $this_pos;
  for( my $i = $startpos; $i < $last_pos_num; $i+=1){
    #-------------first position in object beam---------
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];

    $X_origin = $rel_offset_x[$this_pos] + $x_nudge; 
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;

    if( $VIIDO ){ 
	print "handset to position $this_pos: $X, $Y\n";
	print "guide off\n";
	ufgem::guideOff( $soc );
	ufgem::setHandset_radec( $soc, $X, $Y );
	ufgem::handset_wait();
	print "guide on\n";
	ufgem::guideOn( $soc );
	ufgem::guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***guideOff\n";
	   print "***handset to position $this_pos: $X, $Y\n";
	   print "***"; ufgem::handset_wait();
	   print "***guideOn\n";
	   print "***"; ufgem::guide_wait();
	 }

    print "SHOULD BE: Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $d_rptpos, $next_index );

  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){
    print "offset to origin\n";
    print "guide off\n";
    ufgem::guideOff( $soc );
    ufgem::setOffset_radec( $soc, 0, 0 );
    ufgem::offset_wait();
    print "guide on\n";
    ufgem::guideOn( $soc );
    ufgem::guide_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin\n";
	 print "***guide off\n";
	 print "***offset to position $startpos: 0, 0\n";
	 print "***"; ufgem::offset_wait();
	 print "***guide on\n";
	 print "***"; ufgem::guide_wait();
       }


####SUBS
sub take_an_image{
  my ($d_rptpos, $this_index ) = @_;

  for( my $i = 0; $i < $d_rptpos; $i++ ){
    main::getGemTCSinfo();

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image

sub print_setup{
  my $num_moves = @pat_x - 1;
  my $last_pos_num = $num_moves - 1;

  print "\n";
  print "----------------------------------------------\n\n";
  print "The default dither pattern is $dpattern.\n\n";
  #Dither_patterns::scale_pattern_msg();
  print "The default pattern of absolute offsets wrt the present pointing center,:\n";
  print "and the offsets wrt the first image are:\n\n";

  print "pos:      Xabs       Yabs     X_wrt_Im1    Y_wrt_Im1\n";
  my @base_pat_x = Dither_patterns::load_pattern_x( $dpattern );
  my @base_pat_y = Dither_patterns::load_pattern_y( $dpattern );

  @base_pat_x = Dither_patterns::scale_pattern_x_or_y( \@base_pat_x, $net_scale_factor );
  @base_pat_y = Dither_patterns::scale_pattern_x_or_y( \@base_pat_y, $net_scale_factor );

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $base_pat_x[$i];print ", ";
    printf '% 10.3f', $base_pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "The pattern of relative offsets to be executed wrt the present pointing center,:\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xrel       Yrel     X_wrt_Im1    Y_wrt_Im1\n";
  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $pat_x[$i];print ", ";
    printf '% 10.3f', $pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "Number of images per position--------------  $d_rptpos\n";
  print "Number of times through pattern------------  $d_rptpat\n";
  print "Start position, first time through pattern-  $startpos\n";
  print "num moves = $num_moves, lastpos= $last_pos_num\n";
  print "----------------------------------------------\n\n";
}#Endsub print_setup



###SUBS from GemTCSinfo.pm
my @numeric_header_keywords =
qw( 
  AIRMASS  TELFOCUS  HA  HUMID  INSTRAA  INSTRPA  MJD  
  DEC_OFF    RA_OFF  ROTATOR  ROTERROR
  RA_BASE  DEC_BASE  EPOCH  EQUINOX  
  USRFOC  ZD
);


my @returned_numeric_keywords =
qw( 
  airmass  focus   ha  humidity  instraa  instrpa  mjd  
  offsetdec  offsetra  rotator  roterror
  targetra  targetdec  targetepoch  targetequinox
  userfocus  zd
);

my @string_header_keywords =
qw( 
  AZIMUTH  AZERROR   EL  ELERROR  FRAMEPA  LOCTIME  LST  
  OBJECT  TARGFRM  TARGSYS  RA_TEL  DEC_TEL  UTC  UTCDATE
);


my @returned_string_keywords =
qw( 
  azimuth  azerror  elevation  elerror  framepa  localTime  lst  
  targetname  targetframe  targetradecsys  telra  teldec  utc  utcdate
);

###> The arrays with the final output from the tcs, 
###> formatted for the FITS header.
my @numeric_key_vals;
my @string_key_vals;

sub getGemTCSinfo{
  my $soc = ufgem::viiconnect();
  get_numeric_vals( \@returned_numeric_keywords, \@numeric_key_vals, $soc );
  format_numeric_vals(\@numeric_key_vals);
  set_numeric_vals( \@numeric_header_keywords, \@numeric_key_vals );

  get_string_vals( \@returned_string_keywords, \@string_key_vals, $soc );
  set_string_vals(  \@string_header_keywords,  \@string_key_vals  );

}#Endsub getGemTCSinfo


sub get_numeric_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my @returned_numeric_keywords = @$aref1;
  # $aref2 will be the actual array of final values = @numeric_key_vals

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_num = @returned_numeric_keywords;
  
  for( my $i = 0; $i < $num_num; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_numeric_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_numeric_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$numeric_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }

}#Endsub get_numeric_vals


sub get_string_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my $returned_string_keywords = @$aref1;

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_str = @returned_string_keywords;
  
  for( my $i = 0; $i < $num_str; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_string_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_string_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$string_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }
}#Endsub get_string_values


sub format_numeric_vals{
  my $aref = $_[0];
  my @numeric_key_vals = @$aref;
  #NUMERIC values:
  #recast output as float with same precision,
  #and right justified to 20 char long.

  my $num_num = @numeric_key_vals;
  for( my $i = 0; $i < $num_num; $i++ ){
    
    my $item = $numeric_key_vals[$i];
    my $len_val = length $item;
    if( $len_val > 20 ){
      #this one's for mjd which is a long exponential format number
      $item = sprintf "%.20f", $numeric_key_vals[$i];
    }
    my $loc_dec = index $item, ".";
    
    #print "$i: item = $item; len = $len_val; dec loc = $loc_dec\n";
    
    my $recast_format;
    if( $len_val > 20 ){
      $recast_format = "%20." . (20 - $loc_dec - 1) . "f";
    }else{
      $recast_format = "%20." . ($len_val - 1 - $loc_dec) . "f";
    }
    my $recast_val = sprintf $recast_format, $item;
    
    my $len_recast_val = length $recast_val;
    
    #if( $i == 0 ){
    #  print "---------------12345678901234567890----------\n";
    #}
    #print "$i: recast_val =$recast_val, leng = $len_recast_val\n";
    $$aref[$i] = $recast_val;
  }

}#Endsub format_numeric_vals

sub print_numeric_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @numeric_header_keywords   = @$aref1;
  my @returned_numeric_keywords = @$aref2;
  my @numeric_key_vals          = @$aref3;

  my $num_num = @returned_numeric_keywords;
  print "\n\nNumeric Values\n";
  for( my $i = 0; $i < $num_num; $i++ ){
    #  print "$i = $numeric_header_keywords[$i] =\t ".
    #    "$returned_numeric_keywords[$i] =12345678901234567890\n";
    print "$i = $numeric_header_keywords[$i] =\t ".
      "$returned_numeric_keywords[$i] =$numeric_key_vals[$i]\n";
  }
  
}#End print_numeric_vals


sub print_string_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @string_header_keywords   = @$aref1;
  my @returned_string_keywords = @$aref2;
  my @string_key_vals          = @$aref3;

  my $num_str = @returned_string_keywords;
  print "\n\nString Values\n";
  for( my $i = 0; $i < $num_str; $i++ ){
    print "$i = $string_header_keywords[$i] =\t ".
      "$returned_string_keywords[$i] =$string_key_vals[$i]\n";
  }
  print "\n\n";

}#Endsub print_string_vals


sub print_present_settings{
  my $aref = $_[0];
  my @header_keywords = @$aref;

  my $num_items = @header_keywords;
  for( my $i = 0; $i < $num_items; $i++){
    printFitsHeader( $header_keywords[$i] );
  }

}#Endsub print_present_settings


sub set_numeric_vals{
  my ( $aref1, $aref2 ) = @_;
  my @numeric_header_keywords = @$aref1;
  my @numeric_key_vals        = @$aref2;

  my $num_num = @numeric_header_keywords;
  #print "$num_num\n";

  for( my $i = 0; $i < $num_num; $i++){
    setNumericParam( $numeric_header_keywords[$i], 
    		     $numeric_key_vals[$i] );

  }

}#Endsub set_numeric_vals


sub set_string_vals{
  my ( $aref1, $aref2 ) = @_;
  my @string_header_keywords = @$aref1;
  my @string_key_vals        = @$aref2;

  my $num_str = @string_header_keywords;

  for( my $i = 0; $i < $num_str; $i++ ){
    #The vii returns strings with a leading space
    #strip them off if present
    #
    my $val = $string_key_vals[$i];
    my $space = index $val, " ";
    if( $space == 0 ){
      my $len = length $val;
      $string_key_vals[$i] = substr $val, 1, ($len - 1);
    }

    setStringParam( $string_header_keywords[$i],
    		    $string_key_vals[$i] );    
    #print "$i $string_header_keywords[$i] =$string_key_vals[$i]\n";    
  }

}#Endsub set_string_vals
