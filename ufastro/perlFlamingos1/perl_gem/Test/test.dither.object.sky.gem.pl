#!/usr/local/bin/perl -w

## Execute dither patterns at Gemini South, doing 2 positions of
## the pattern in beam A before nodding to beam B, to repeat the
## same positions, and then repeating the entire process until the
## pattern is completed
## Note the offsets in each beam are relative to the beam pointing
## center.

my $rcsId = q($Name:  $ $Id: test.dither.object.sky.gem.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use IdleLutStatus;
use ImgTests;
use RW_Header_Params qw/:all/;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use UseTCS qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($dpattern, $startpos, $net_scale_factor, $d_rptpos,
    $d_rptpat, $usenudge, $nudgesiz) = RW_Header_Params::get_dither_params();

#### Insist that it pattern at position 0
$startpos = 0;

#Load dither patterns
my @pat_x        = Dither_patterns::load_pattern_rel_x(   $dpattern );
my @pat_y        = Dither_patterns::load_pattern_rel_y(   $dpattern );
my @rel_offset_x = Dither_patterns::load_pat_rel_Xorigin( $dpattern );
my @rel_offset_y = Dither_patterns::load_pat_rel_Yorigin( $dpattern );
my @wrap_x_y     = Dither_patterns::load_pattern_wrap(    $dpattern );

@pat_x        = Dither_patterns::scale_pattern_x_or_y( \@pat_x,        $net_scale_factor );
@pat_y        = Dither_patterns::scale_pattern_x_or_y( \@pat_y,        $net_scale_factor );
@rel_offset_x = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_x, $net_scale_factor );
@rel_offset_y = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_y, $net_scale_factor );
@wrap_x_y     = Dither_patterns::scale_pattern_wrap(   \@wrap_x_y,     $net_scale_factor );

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

print_setup();
print "The above pattern will be performed two offsets at a time, per beam.\n";

my $soc = UseTCS::make_socket_connection( $telescop, $VIIDO );
my $probe_name = ufgem::query_probe_name();
ufgem::prompt_start_guiding_beam_A();

#### Begin taking data
#### Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $x_nudge = 0;
my $y_nudge = 0;
my $next_index = $this_index;

my $X;
my $Y;
my $X_origin;
my $Y_origin;
my $last_pos_num  = @pat_x - 2;#num moves is @pat_x-1, then -1 to start count at 0
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ($x_nudge, $y_nudge) = Dither_patterns::nudge_pointing($d_rptpat, $rpt_pat, $usenudge, $nudgesiz);
  $X_origin = $rel_offset_x[$startpos] + $x_nudge;
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;
  RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position

    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){ 
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "offset to position $startpos: $X, $Y, in beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      ufgem::setOffset_radec( $soc, $X, $Y );
      ufgem::offset_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "***offset to position $startpos: $X, $Y, in beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; ufgem::offset_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    #Should already be in beam A this time, so don't nod
    $X = $wrap_x_y[0] + $x_nudge;
    $Y = $wrap_x_y[1] + $y_nudge;
    if( $VIIDO ){
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "handset by $X, $Y, back to position $startpos in beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      ufgem::setHandset_radec( $soc, $X, $Y );
      ufgem::handset_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***Probe $probe_name follow on\n";
	   print "***guide off\n";
	   print "***handset by $X, $Y, back to position $startpos in beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; ufgem::handset_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	 }

  }

  print "Should image $d_rptpos times here\n\n";
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  my $this_pos; my $nod_alpha; my $nod_delta;
  for( my $i = $startpos; $i <= $last_pos_num; $i+=2 ){
    #------------------Nod to sky beam---------------
    #-------------first position in sky beam---------
    ( $nod_alpha, $nod_delta ) = 
      compute_nod_pos_wrt_origin( $rel_offset_x[$i], $rel_offset_y[$i]);

    $X_origin = $nod_alpha + $x_nudge;
    $Y_origin = $nod_delta + $y_nudge;
    #RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    if( $VIIDO ){
      print "m1Corrections off\n";
      ufgem::m1CorrectionsOff( $soc );
      print "mountguide off\n";
      ufgem::mountGuideOff( $soc );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "Probe $probe_name follow off\n";
      ufgem::probe_follow_off( $soc, $probe_name );
      print "probeGuideConfig off\n";
      ufgem::probeGuideConfig( $soc, "p1", "off" );
      ufgem::probeGuideConfig( $soc, "p2", "off" );
      print "Nodding to position $i, beam=B\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      ufgem::nodbeam( $soc, "B" );
      ufgem::nod_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***m1CorrectionsOff\n";
	   print "***mountguide off\n";
	   print "***guide off\n";
	   print "***Probe $probe_name follow off\n";
	   print "***probeGuideConfig off\n";
	   print "***Nodding to position $i, beam=B\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; ufgem::nod_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #-------------second position in sky beam---------
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];

    ( $nod_alpha, $nod_delta ) = 
      compute_nod_pos_wrt_origin( $rel_offset_x[$this_pos], $rel_offset_y[$this_pos]);

    $X_origin = $nod_alpha + $x_nudge;
    $Y_origin = $nod_delta + $y_nudge;
    #RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    if( $VIIDO ){
      print "handset to position $this_pos: $X, $Y, beam=B\n";
      print "position relative to origin:  $X_origin, $Y_origin\n";
      ufgem::setHandset_radec( $soc, $X, $Y );
      ufgem::handset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***handset to position $this_pos: $X, $Y, beam=B\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; ufgem::handset_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #------------------Nod to object beam---------------
    #-------------first position in object beam---------
    $X_origin = $rel_offset_x[$this_pos] + $x_nudge; 
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;
    #RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    if( $VIIDO ){
      print "Nodding to position $this_pos, beam=A\n";
      print "position relative to first position:  $X_origin, $Y_origin\n";
      ufgem::nodbeam( $soc, "A" );
      print "probeGuideConfig on\n";
      ufgem::probeGuideConfig( $soc, $probe_name, "on" );
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      ufgem::probe_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
      print "mountguide on\n";
      ufgem::mountGuideOn( $soc );
      print "m1Corrections on\n";
      ufgem::m1CorrectionsOn( $soc );
    }else{ print "***Should be doing the following:\n";
	   print "***Nodding to position $this_pos, beam=A\n";
	   print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***probeGuideConfig on\n";
	   print "***Probe $probe_name follow on\n";
	   print "***"; ufgem::probe_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	   print "***mountguide on\n";
	   print "***m1Corrections\n";
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #-------------second position in object beam---------
    $this_pos = $i+2;
    if( $this_pos <= $last_pos_num ){
      $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];
      $X_origin = $rel_offset_x[$this_pos] + $x_nudge; 
      $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;
      #RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

      if( $VIIDO ){
	print "handset to position $this_pos: $X, $Y, beam=A\n";
	print "position relative to first position:  $X_origin, $Y_origin\n";
	print "guide off\n";
	ufgem::guideOff( $soc );
	ufgem::setHandset_radec( $soc, $X, $Y );
	ufgem::handset_wait();
	print "guide on\n";
	ufgem::guideOn( $soc );
	ufgem::guide_wait();
      }else{ print "***Should be doing the following:\n";
	     print "***handset to position $this_pos: $X, $Y, beam=A\n";
	     print "***position relative to first position:  $X_origin, $Y_origin\n";
	     print "***"; ufgem::handset_wait();
	   }
      print "Should image $d_rptpos times here\n\n";
      $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		     $file_hint, $next_index, $net_edt_timeout, $use_tcs);
    }
  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){
    print "offset to origin in beam=A\n";
    print "guide off\n";
    ufgem::guideOff( $soc );
    ufgem::setOffset_radec( $soc, 0, 0 );
    ufgem::offset_wait();
    print "guide on\n";
    ufgem::guideOn( $soc );
    ufgem::guide_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin in beam A\n";
	 print "***guide off\n";
	 print "***"; ufgem::offset_wait();
	 print "***guide on\n";
	 print "***"; ufgem::guide_wait();
       }


####SUBS
sub compute_rel_offsets{
  my $ar_vec = $_[0];
  my @vector = @$ar_vec;

  my $first_pos = $vector[0];
  my @output_vector; $output_vector[0] = 0;
  my $vec_len = @vector;
  for( my $i = 1; $i < $vec_len; $i++){
    $output_vector[$i] = $first_pos - $vector[$i];
    #print "$i: $output_vector[$i]\n";
  }
  
  return \@output_vector;
}#Endsub compute_rel_offsets


sub print_setup{
  my $num_moves = @pat_x - 1;
  my $last_pos_num = $num_moves - 1;

  print "\n";
  print "----------------------------------------------\n\n";
  print "The default dither pattern is $dpattern.\n\n";
  Dither_patterns::scale_pattern_msg();
  print "The default pattern of absolute offsets wrt the present pointing center,:\n";
  print "and the offsets wrt the first image are:\n\n";

  print "pos:      Xabs       Yabs     X_wrt_Im1    Y_wrt_Im1\n";
  my @base_pat_x = Dither_patterns::load_pattern_x( $dpattern );
  my @base_pat_y = Dither_patterns::load_pattern_y( $dpattern );

  @base_pat_x = Dither_patterns::scale_pattern_x_or_y( \@base_pat_x, $net_scale_factor );
  @base_pat_y = Dither_patterns::scale_pattern_x_or_y( \@base_pat_y, $net_scale_factor );

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $base_pat_x[$i];print ", ";
    printf '% 10.3f', $base_pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "The pattern of relative offsets to be executed wrt the present pointing center,:\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xrel       Yrel     X_wrt_Im1    Y_wrt_Im1\n";
  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $pat_x[$i];print ", ";
    printf '% 10.3f', $pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "Number of images per position--------------  $d_rptpos\n";
  print "Number of times through pattern------------  $d_rptpat\n";
  print "Start position, first time through pattern-  $startpos\n";
  print "num moves = $num_moves, lastpos= $last_pos_num\n";
  print "----------------------------------------------\n\n";
}#Endsub print_setup


sub setup_nod{
  my $soc = $_[0];
  my $throw    = param_value( 'THROW'    );
  my $throw_pa = param_value( 'THROW_PA' );

  ufgem::nodConfig( $soc, $throw, $throw_pa );

}#Endsub setup_nod


sub compute_nod_pos_wrt_origin{
  my ($alpha_offset, $dec_offset) = @_;
  my $throw    = param_value( 'THROW'    );
  my $throw_pa = param_value( 'THROW_PA' );
  use Math::Trig;

  my $pa_rad = deg2rad( $throw_pa );
  my $d_alpha = $throw * sin( $pa_rad );
  my $d_delta = $throw * cos( $pa_rad );

  my $net_alpha_offset = $alpha_offset + $d_alpha;
  my $net_dec_offset   = $dec_offset   + $d_delta;

  return( $net_alpha_offset, $net_dec_offset );
}#Endsub compute_nod_pos_wrt_origin


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    #print "take image with index= $this_index\n";
    #UseTCS::use_tcs( $use_tcs, $header, $telescop );

    #print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    #RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    #my $acq_cmd = acquisition_cmd( $header, $lut_name, 
    #		   $file_hint, $this_index, $net_edt_timeout);

    #print "acq cmd is \n$acq_cmd\n\n";
    #acquire_image( $acq_cmd );

    #IdleLutStatus::idle_lut_status_slashr( $expt, $nreads);
    #main::my_idle_lut_status( $expt, $nreads );
    #move_file_for_index_sep( $file_hint, $next_index );#Not used

    #my $unlock_param = param_value( 'UNLOCK' );
    #if( $unlock_param ){
    #  ImgTests::make_unlock_file( $file_hint, $this_index );
    #}

    #if( Fitsheader::param_value( 'PAD_DATA' ) ){
    #  ImgTests::pad_data( $file_hint, $next_index );
    #}

    #if( Fitsheader::param_value( 'DISPDS92' ) ){
    #  ImgTests::display_image( $file_hint, $next_index );
    #}else{
    #  print "The better way to display images is off; see config.output.pl\n";
    #}

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub my_idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	  "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n\n";
	print "IF it looks like it has stalled while applying the LUT,\n";
	print "run ufstop.pl -clean, and start over\n\n";
	print "Else if it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";
	exit;
      }#end time expired      
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub my_idle_lut_status
