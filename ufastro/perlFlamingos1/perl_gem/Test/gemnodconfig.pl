#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: gemnodconfig.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $cnt = @ARGV;
my $reply;

if( $cnt < 2 ) { # assume -help
   print "usage testnodconfig.pl throw (arcsec) pa (deg)\n";
   exit(0);
}
else {
  my $throw = shift;
  my $pa    = shift;
  $reply = ufgem::nodConfig($soc, $throw, $pa);
  print "$reply\n";
}


