#!/usr/bin/perl -w
my $rcsId = q($Name:  $ $Id: gem.status.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

BEGIN { require "/usr/local/flamingos/perl_gem/gemsouth.plib"; }

# status items
# syntax: "get $gemstat[i] $ufgem::terminate";
# or fpr epics? syntax: "caget $gemstat[i] $ufgem::terminate";

my $soc = ufgem::viiconnect();
my $val;
my $cnt = @ARGV;
my $stat;

if( $cnt < 1 ) {
  $val = ufgem::getStatus($soc); # get All
}
else {
  $stat = shift;
  $val = ufgem::getStatus($soc, $stat);
}

print "$val";

close($soc);
