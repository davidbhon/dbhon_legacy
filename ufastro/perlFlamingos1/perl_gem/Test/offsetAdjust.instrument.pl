#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: offsetAdjust.instrument.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $cnt = @ARGV;
my $reply;

if( $cnt < 2 ) { # assume -help
   print "\nUsage:\noffsetAdjust.instrument.pl instr_x_offset instr_y_offset\n";
   print "(offsets in arcsec)\n\n";
   exit(0);
}
else {
  my $soc = ufgem::viiconnect();
  my $off1 = shift;
  my $off2 = shift;
  my $offset_adj_cmd = 
  "do offsetAdjust frame=instrumentxy off1=$off1 off2=$off2 $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  #print "\nShould be:  Executing $offset_adj_cmd\n";
  print "\nExecuting $offset_adj_cmd\n";
  print $soc "$offset_adj_cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  my $done = <$soc>;
  
  parse_gem_ack_done( "offsetAdjust", $ack, $done );

}

