#!/usr/local/bin/perl -w

## Execute dither patterns at Gemini South
## This script uses relative offsets wrt to the
## current pointing center

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use IdleLutStatus;
use ImgTests;
use RW_Header_Params qw/:all/;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use UseTCS qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($m_rptpat,
    $nudge,
    $m_throw,
    $use_mnudg,
    $chip_pa_on_sky_for_rot0,
    $pa_rotator,
    $pa_slit_on_chip,
    $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my $A_dx = 0;  my $A_dy = 0.5 * $m_throw;
my $B_dx = 0;  my $B_dy = -1 * $A_dy;

my $Ap_dx = 0; my $Ap_dy = $A_dy + $nudge;
my $Bp_dx = 0; my $Bp_dy = $B_dy + $nudge;

my $An_dx = 0; my $An_dy = $A_dy - $nudge;
my $Bn_dx = 0; my $Bn_dy = $B_dy - $nudge;


print_info($A_dx,  $A_dy,  $B_dx,  $B_dy,
	   $Ap_dx, $Ap_dy, $Bp_dx, $Bp_dy,
	   $An_dx, $An_dy, $Bn_dx, $Bn_dy,
	   $use_mnudg);


my $soc = UseTCS::make_socket_connection( $telescop, $VIIDO );
main::prompt_setup( $soc );

my $probe_name = ufgem::query_probe_name();
main::prompt_should_be_guiding_beam_A();

ufgem::probeGuideConfig_noda_chopa( $soc, "p2", "on" );
ufgem::probeGuideConfig_noda_chopb( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopa( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopb( $soc, "p2", "off" );

#### Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $nameA = "A_centered";
my $nameB = "B_centered";

my $Aycen_offset = 0;
my $Bycen_offset = -1*$m_throw;

my $Aypos_offset = $nudge;
my $Bypos_offset = $Bycen_offset + $nudge;

my $Ayneg_offset = -1*$nudge;
my $Byneg_offset = $Bycen_offset - $nudge;


if( $VIIDO ){
  print "Probe $probe_name follow on.\n";
  ufgem::probe_follow_on( $soc, $probe_name );
}else{
    print "SHOULD BE: Probe $probe_name follow on.\n";
}

main::move_center_to_A( $nameA, $A_dx, $A_dy );

my $nudge_sign = 0;
for( my $i = 1; $i <= $m_rptpat; $i++ ){
  if( !$use_mnudg ){
    #not nudged
    print "\n\n";
    print "NOT Nudged.\n\n";
    $next_index = do_pattern( $header, $lut_name,
			      $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
			      $nameA, $nameB, $Aycen_offset, $Bycen_offset );

  }elsif( $use_mnudg ){
    $nudge_sign = select_nudge_direction( $i, $m_rptpat );

    if( $nudge_sign == 0 ){
      if( $i > 1 ){
	$nameA = "A_centered";
	$nameB = "B_centered";

	#nudge up from previous negative beam
	my $net_nudge = $nudge;
	print "\n\n";
	print "Nudge pattern by $net_nudge.\n";
	main::nudge_beam_A( 0, $net_nudge);
      }
      print "Not nudged ABBA pattern\n";
      $next_index = do_pattern( $header, $lut_name,
				$file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
			        $nameA, $nameB, $Aycen_offset, $Bycen_offset );

    }elsif( $nudge_sign == 1 ){
      $nameA = "A_positive";
      $nameB = "B_positive";

      #shift beam A by positive nudge
      my $net_nudge = $nudge_sign * $nudge;
      print "\n\n";
      print "Nudge pattern by $net_nudge.\n";
      main::nudge_beam_A( 0, $net_nudge);

      print "Do positive nudged ABBA pattern\n";
      $next_index = do_pattern( $header, $lut_name,
				$file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
			        $nameA, $nameB, $Aypos_offset, $Bypos_offset );

    }elsif( $nudge_sign == -1 ){
      $nameA = "A_negative";
      $nameB = "B_negative";

      #shift beam A by negative nudge
      my $net_nudge = 2 * $nudge_sign * $nudge;
      print "\n\n";
      print "Nudge pattern by $net_nudge.\n";
      main::nudge_beam_A( 0, $net_nudge );
      print "Do negative nudged ABBA pattern\n";
      $next_index = do_pattern( $header, $lut_name,
				$file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
			        $nameA, $nameB, $Ayneg_offset, $Byneg_offset );
    }
  }#End big if

}#End for loop

#print "nudge_sign = $nudge_sign\n";
main::move_A_to_center( $m_throw, $nudge_sign, $nudge );

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";

####SUBS
sub base{
  my $index = $_[0];

  my $value = 4*$index - $index + 1;

  return $value;
}#Endsub base


sub compute_centered_offsets{
  my ( $pa_slit_on_sky, $m_throw ) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_centered_offsets


sub compute_nudged_offsets{
  my ($pa_slit_on_sky, $beam_sep, $nudge) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $An_dra  =      (0.5 * $beam_sep + $nudge) * sin( $pa_sky_rad );
  my $An_ddec =      (0.5 * $beam_sep + $nudge) * cos( $pa_sky_rad );

  my $Bn_dra  = -1 * (0.5 * $beam_sep - $nudge) * sin( $pa_sky_rad );
  my $Bn_ddec = -1 * (0.5 * $beam_sep - $nudge) * cos( $pa_sky_rad );

  $An_dra  = sprintf '%.2f', $An_dra;
  $An_ddec = sprintf '%.2f', $An_ddec;

  $Bn_dra  = sprintf '%.2f', $Bn_dra;
  $Bn_ddec = sprintf '%.2f', $Bn_ddec;

  return( $An_dra, $An_ddec, $Bn_dra, $Bn_ddec );
}#Endsub compute_nudged_offsets


sub do_pattern{
  my ($header, $lut_name, 
      $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
      $nameA, $nameB, $this_Ay_offset, $this_By_offset ) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";
  print "Take 1 Spectrum in Beam $nameA\n";
  print "PRESENT OFFSETS wrt beam A_centered: XOFFSET = 0\n";
  print "                                     YOFFSET = $this_Ay_offset\n";
  RW_Header_Params::write_rel_offset_to_header( $header, 0, $this_Ay_offset);

  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_from_beam_A_to_beam_B( $nameA, $nameB, $m_throw );

  print "PRESENT OFFSETS wrt beam A_centered: XOFFSET = 0\n";
  print "                                     YOFFSET = $this_By_offset\n";
  RW_Header_Params::write_rel_offset_to_header( $header, 0, $this_By_offset);

  print "Take 2 Spectra in beam $nameB\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_from_beam_B_to_beam_A( $nameA, $nameB, $m_throw );

  print "PRESENT OFFSETS wrt beam A_centered: XOFFSET = 0\n";
  print "                                     YOFFSET = $this_Ay_offset\n";
  RW_Header_Params::write_rel_offset_to_header( $header, 0, $this_Ay_offset);

  print "Take 1 Spectrum in Beam $nameA\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);


  return $next_index;
}#Endsub do_pattern


sub my_idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) .
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	  "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n\n";
	print "IF it looks like it has stalled while applying the LUT,\n";
	print "run ufstop.pl -clean, and start over\n\n";
	print "Else if it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";
	exit;
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub my_idle_lut_status


sub dither_slit_motion{
  my ( $to_beam, $offset_x, $offset_y ) = @_;

  if( $VIIDO ){
    #Done once at beginning
    #print "Probe $probe_name follow on.\n";
    #ufgem::probe_follow_on( $soc, $probe_name );

    print "guide off\n";
    ufgem::guideOff( $soc );

    print "offsetAdjust to beam $to_beam: $offset_x, $offset_y\n";
    ufgem::offsetAdjust_instrument( $soc, $offset_x, $offset_y );

    ufgem::offset_wait();

    print "guide on\n";
    ufgem::guideOn( $soc );

    ufgem::guide_wait();
  }else{
    #print "SHOULD BE: Probe $probe_name follow on.\n";
    print "SHOULD BE: guide off\n";
    print "offsetAdjust to beam $to_beam: $offset_x, $offset_y\n";
    print "SHOULD BE: do offsetAdjust frame=instrumentxy off1=$offset_x off2=$offset_y $ufgem::terminate";
    print "SHOULD BE: offset waiting\n";
    ufgem::offset_wait();
    print "SHOULD BE: guide on\n";
    print "SHOULD BE: guide waiting\n";
    ufgem::guide_wait();
  }
}#Endsub dither_slit_motion


sub move_A_to_center{
  my( $m_throw, $nudge_sign, $this_nudge ) = @_;

  my $reverse_nudge = -1*$nudge_sign * $this_nudge;
  my $net_offset_to_A = -1*$m_throw/2 + $reverse_nudge;

  print "\n";
  print "***************************************\n";
  print "Final ABBA pattern finished.\n";

  print "\n";
  print "Moving back to slit center\n";
  main::dither_slit_motion( "Center", 0, $net_offset_to_A );
}#Endsub move_A_to_center


sub move_center_to_A{
  my ($nameA, $thisA_dx, $thisA_dy) = @_;

  print "Moving to beam $nameA.\n";
  print "---------------------------\n";
  main::dither_slit_motion( $nameA, $thisA_dx, $thisA_dy );

}#Endsub move_center_to_A


sub move_from_beam_A_to_beam_B{
  my ( $nameA, $nameB, $this_throw ) = @_;

  print "Move from beam $nameA to beam $nameB\n";
  print "------------------------------------\n";
  main::dither_slit_motion( $nameB, 0, -1*$this_throw );

}#Endsub move_from_beam_A_to_beam_B


sub move_from_beam_B_to_beam_A{
  my ( $nameA, $nameB, $this_throw ) = @_;

  print "Move to from beam $nameB to beam $nameA\n";
  print "---------------------------------------\n";
  main::dither_slit_motion( $nameA, 0, +1*$this_throw );

}#Endsub move_from_beam_B_to_beam_A


sub nudge_beam_A{
  my ( $this_nudge_dx, $this_nudge_dy ) = @_;

  print "Move beam A by $this_nudge_dy\n";
  print "-------------------------\n";
  main::dither_slit_motion( "Nudging", $this_nudge_dx, $this_nudge_dy )

}#Endsub nudge_beam_A


sub select_nudge_direction{
  my ($tps, $rpt_pat) = @_;

  my $center = "0";
  my $pos    = "+1";
  my $neg    = "-1";
  my $set;

  my $index = 0;
  my $index_matched = "";
  my $base_matched = "";
  until( $index >= $rpt_pat ){
    my $base = base( $index );
    #print "index = $index; tps = $tps; base = $base\n";

    if( $tps == $base ){
      $set = $center;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+1 ){
      $set = $pos;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+2 ){
      $set = $neg;
      $index = $rpt_pat + 1;

    }else{
      $index++;
    }
  }

  return $set;
}#Endsub select_nudge_direction


sub print_info{
  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "----------------PRESENT PARAMETERS FOR MOS DITHER----------------\n";
  print "\n";

  print_offsets($A_dx,  $A_dy,  $B_dx,  $B_dy,
		$Ap_dx, $Ap_dy, $Bp_dx, $Bp_dy,
		$An_dx, $An_dy, $Bn_dx, $Bn_dy,
		$use_mnudg);

  print "\n";
  print "Number of times through pattern------------------------ $m_rptpat\n";
  print "Distance (arcsec) between A & B beams------------------ $m_throw\n";
  print "Distance (arcsec) to shift ABBA on repeats of pattern-- $nudge\n";
  print "\n";
  #print "Rotator Position Angle (ROT_PA, deg)------------------- $pa_rotator\n";
  #print "Chip PA on sky for this Rotator angle (deg)------------ ".
  #       "$chip_pa_on_sky_this_rotator_pa\n";
  print "\n";

  if( $use_mnudg ){
    print "Will nudge ABBA pattern on pattern repeats.\n";
    print "Nudged pattern order---------------------------- center, positive, negative\n";
  }else{
    print "Will _NOT_ nugde pattern on repeats from center.\n";
  }

  print "-----------------------------------------------------------------\n";

}#Endsub print_info


sub print_offsets{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
      $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec,
      $use_mnudg) = @_;

  if( $use_mnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_A";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_B";
    write STDOUT;
    $~ = $old_stdout_format;

  }elsif( !$use_mnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS_NOT_NUDGED";
    write STDOUT;
    $~ = "OFFSETS_NOT_NUDGED";
    write STDOUT;
    $~ = $old_stdout_format;
  }
}#Endsub print_offsets


sub prompt_should_be_guiding_beam_A{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "The telescope should already be guiding in NOD BEAM=A\n\n";
  print "About to begin executing the dither pattern.";

  GetYN::query_ready();
}#Endsub prompt_setup


sub prompt_setup{
  my $soc = $_[0];

  print "\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern:\n\n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type ABSORB to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $absorb_home = <>;chomp $absorb_home;
  my $valid = 0;
  until( $valid ){
    if( $absorb_home =~ m/absorb/i ){
      $valid = 1;
    }elsif( $absorb_home =~ m/home/i ){
      $valid = 1;
    }elsif( $absorb_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either ABSORB or HOME ";
      $absorb_home = <>; chomp $absorb_home;
    }
  }

  if( $absorb_home =~ m/absorb/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    ufgem::absorb_offsets( $soc );
    ufgem::absorb_adjusts( $soc );

  }elsif( $absorb_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    if( $VIIDO ){
      UseTCS::use_tcs( $use_tcs, $header, $telescop );
    }else{
      print "SHOULD BE GETTING TCSINFO\n";
    }

    print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECUTING:  acq cmd\n";
    print "acq cmd is \n$acq_cmd\n\n";
    acquire_image( $acq_cmd );

    #print "SHOULD be executing idle_lut_status loop\n";
    IdleLutStatus::idle_lut_status_slashr( $expt, $nreads);
    #main::my_idle_lut_status( $expt, $nreads );

    my $unlock_param = param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    #print "SHOULD BE padding data.\n";
    if( Fitsheader::param_value( 'PAD_DATA' ) ){
      ImgTests::pad_data( $file_hint, $this_index );
    }

    #print "SHOULD BE displaying image\n";
    if( Fitsheader::param_value( 'DISPDS92' ) ){
      ImgTests::display_image( $file_hint, $this_index );
    }else{
      print "The better way to display images is off; see config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


#### FORMATS
format OFFSET_HEADINGS = 
                         Telescope Offsets wrt slit center
               Centered              Positive              Negative
Beam        dRa       dDec        dRa       dDec        dRa       dDec
---------------------------------------------------------------------------------------
.
format OFFSET_HEADINGS_NOT_NUDGED = 
      Offsets Centered wrt slit center
              Telescope
Beam        dRa       dDec
--------------------------------------
.
format OFFSETS_NOT_NUDGED =
 A    @#####.##  @#####.## (arcsec)
$A_dx, $A_dy
 B    @#####.##  @#####.## (arcsec)
$B_dx, $B_dy
.
format TELE_OFFSETS_BEAM_A = 
 A    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$A_dx, $A_dy, $Ap_dx, $Ap_dy, $An_dx, $An_dy
.
format TELE_OFFSETS_BEAM_B = 
 B    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$B_dx, $B_dy, $Bp_dx, $Bp_dy, $Bn_dx, $Bn_dy
.

__END__

=head1 NAME

dither.mos.gem.test.pl

=head1 Description

Executes the mos dither pattern.

=head1 REVISION & LOCKER

$Name:  $

$Id: dither.mos.gem.test.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
