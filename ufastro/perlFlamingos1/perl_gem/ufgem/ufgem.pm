package ufgem;

require 5.005_62;
use strict;
use warnings;

use IO::Socket;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use ufgem ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ 
     qw(
	&viiconnect 
	&guideOn          &guideOff
	&mountGuideOn     &mountGuideOff
	&nodConfig        &nodbeam
	&p1FollowOn       &p1FollowOff
	&p2FollowOn       &p2FollowOff
	&probeGuideConfig &offsetAdjust_instrument
	&probeGuideConfig_noda_chopa
	&probeGuideConfig_noda_chopb

	&probeGuideConfig_nodb_chopa
	&probeGuideConfig_nodb_chopb

	&m1CorrectionsOn  &m1CorrectionsOff
	&clear_adjusts    &aborb_adjusts
	&absorb_offsets   &clear_offsets
	&handsetAbsorb    &handsetClear
	&setOffset_radec  &setHandset_radec
	&handset_wait     &offset_wait        &guide_wait
	&probe_wait       &nod_wait
	&getOffset_radec  &getStatus          &printAllStatus
	&parse_gem_ack_done
	&prompt_start_guiding_beam_A
	&query_probe_name
	&probe_follow_on
	&probe_follow_off
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
$ufgem::terminate = "\r\n";
@ufgem::allstat = (
"Airmass",
"azError",
"Azimuth",
"baseY",
"baseY",
"beam",
"chopBeam",
"chopDutyCycle",
"chopfreq",
"choppa",
"chopping",
"chopthrow",
"Elevation",
"elError",
"focus",
"framePA",
"HA",
"Health",
"Humidity",
"instrAA",
"instrPA",
"localTime",
"LST",
"MJD",
"nodmode",
"nodpa",
"nodthrow",
"nodtype",
"offsetDec",
"offsetRA",
"offsetX",
"offsetY",
"rotator",
"rotError",
"targetName",
"targetRA",
"targetDec",
"targetEpoch",
"targetEquinox",
"targetFrame",
"targetRaDecSys",
"Telescope",
"telRA",
"telDec",
"userFocus",
"UTC",
"zd"
);

@ufgem::allcmd = ( 
"chop state = On/Off",
"chopBeam = A/B/C",
"chopConfig sync = SCS/OSCIR> freq = Hz throw = arcsec PA = deg. frame = FK5/FK4/AZEL Equinox = J2000",
"focus offset = mm",
"guide state = On/Off",
"handset Type = radec/azel/tangent-plane dRA = arcsec dDec = arcsec",
"handsetAbsorb",
"handsetClear",
"m2GuideConfig source = PWFS1/PWFS2/O1WFS chopbeam A/B",
"mountGuide state = On/Off",
"nod beam = A/B",
"nodConfig mode = standard/offset type = radec/azel/tangent-plane throw = arcsec pa = deg.",
"offset type = radec/azel/tangent-plane dRA = arcsec dDec = arcsec",
"xyOffset dx = mm dy = mm",
"xyOffsetClear",
"xyOffsetAbsorb",
"rotator PA = deg.-east frame = FK5/FK4/AZEL Equinox = J2000.0 iia = deg.",
"source name = * frame = FK5/FK4/AZEL RA = * Dec = * Equinox = J2000.0 Epoch = J2000.0 \
               parallax = arcsec pmRA = sec/yr pmDec = sec/yr rv = km/s"
);

########################################### package ufgem subroutines #################################

sub viiconnect {
#  local $ufgem::host = "vii-sim.hi.gemini.edu";
  #local $ufgem::host = "172.17.2.10"; # icarus
  local $ufgem::host = $ENV{VII_HOST_IP};
  local $ufgem::argc = @_;
  if( $ufgem::argc > 0 ) { $ufgem::host = shift; }
  local $ufgem::port = $ENV{VII_HOST_PORT};
  local $ufgem::soc = IO::Socket::INET->new( PeerAddr => $ufgem::host, 
					     PeerPort => $ufgem::port,
					     Proto => 'tcp', 
					     Type => IO::Socket::SOCK_STREAM );
  print "soc= $ufgem::soc\n";

  if( !defined($ufgem::soc) ) {
    print "failed to connect to $ufgem::host on port $ufgem::port\n";
    exit(0);
  }
  else {
    local $ufgem::ack = <$ufgem::soc>;
    print "connected to $ufgem::host on port $ufgem::port, ";
    print "$ufgem::ack";
  }
  print $ufgem::soc "enable ack $ufgem::terminate";
  print $ufgem::soc "enable done $ufgem::terminate";
  return $ufgem::soc;
}#Endsub viiconnect


sub guideOn {
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do guide state=On $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "guide", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "guide", $done );
  }

}#Endsub guideOn


sub guideOff {
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do guide state=Off $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "guide", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "guide", $done );
  }

}#Endsub guideOff


sub mountGuideOn {
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do mountGuide state=On $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "mountGuide", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "mountGuide", $done );
  }

}#Endsub mountGuideOn


sub mountGuideOff {
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do mountGuide state=Off $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "mountGuide", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "mountGuide", $done );
  }

}#Endsub mountGuideOff


sub nodConfig{
  my ( $soc, $throw, $throw_pa ) = @_;
  my $nod_mode  = "mode = offset";
  my $nod_type  = "type = tangent plane";
  my $nod_throw = "throw = $throw";
  my $nod_pa    = "pa = $throw_pa";

  my $wait_timed_out = 0;
  my $nod_config_cmd =
    "do nodConfig $nod_mode $nod_type $nod_throw $nod_pa $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $nod_config_cmd\n";
  print $soc "$nod_config_cmd"; 
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "nodConfig", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "nodConfig", $done );
  }

}#Endsub nodConfig


sub nodbeam{
  my ( $soc, $beam ) = @_;

  my $wait_timed_out = 0;
  my $nod_beam_cmd = "do nod beam = $beam $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $nod_beam_cmd\n";
  print $soc "$nod_beam_cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "nod", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "nod", $done );
  }

}#Endsub nodbeam


sub p1FollowOn{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do p1Follow state=On $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "p1Follow", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "p1Follow", $done );
  }

}#Endsub p1FollowOn


sub p1FollowOff{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do p1Follow state=Off $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "p1Follow", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "p1Follow", $done );
  }

}#Endsub p1FollowOn


sub p2FollowOn{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do p2Follow state=On $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "p2Follow", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "p2Follow", $done );
  }

}#Endsub p2FollowOn


sub p2FollowOff{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do p2Follow state=Off $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "p2Follow", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "p2Follow", $done );
  }

}#Endsub p2FollowOn


sub probeGuideConfig{
  my ($soc, $probe_name, $state) = @_;

  my $wait_timed_out = 0;
  my $cmd;

  if( $probe_name eq "p1" ){
    if( $state eq "on" ){
      $cmd = "do p1GuideConfig nodachopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p1GuideConfig nodachopa=Off $ufgem::terminate";
    }
  }elsif( $probe_name eq "p2" ){
    if( $state eq "on" ){
      $cmd = "do p2GuideConfig nodachopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p2GuideConfig nodachopa=Off $ufgem::terminate";
    }
  }

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  if( $probe_name eq "p1" ){
    $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
  }elsif( $probe_name eq "p2" ){
    $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
  }

  if( !$wait_timed_out ){
    my $done = <$soc>;
    if( $probe_name eq "p1" ){
      $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
    }elsif( $probe_name eq "p2" ){
      $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
    }
  }

}#Endsub probeGuideConfig



sub probeGuideConfig_noda_chopa{
  my ($soc, $probe_name, $state) = @_;

  my $wait_timed_out = 0;
  my $cmd;

  if( $probe_name eq "p1" ){
    if( $state eq "on" ){
      $cmd = "do p1GuideConfig nodachopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p1GuideConfig nodachopa=Off $ufgem::terminate";
    }
  }elsif( $probe_name eq "p2" ){
    if( $state eq "on" ){
      $cmd = "do p2GuideConfig nodachopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p2GuideConfig nodachopa=Off $ufgem::terminate";
    }
  }

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  if( $probe_name eq "p1" ){
    $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
  }elsif( $probe_name eq "p2" ){
    $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
  }

  if( !$wait_timed_out ){
    my $done = <$soc>;
    if( $probe_name eq "p1" ){
      $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
    }elsif( $probe_name eq "p2" ){
      $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
    }
  }

}#Endsub probeGuideConfig_noda_chopa



sub probeGuideConfig_noda_chopb{
  my ($soc, $probe_name, $state) = @_;

  my $wait_timed_out = 0;
  my $cmd;

  if( $probe_name eq "p1" ){
    if( $state eq "on" ){
      $cmd = "do p1GuideConfig nodachopb=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p1GuideConfig nodachopb=Off $ufgem::terminate";
    }
  }elsif( $probe_name eq "p2" ){
    if( $state eq "on" ){
      $cmd = "do p2GuideConfig nodachopb=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p2GuideConfig nodachopb=Off $ufgem::terminate";
    }
  }

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  if( $probe_name eq "p1" ){
    $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
  }elsif( $probe_name eq "p2" ){
    $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
  }

  if( !$wait_timed_out ){
    my $done = <$soc>;
    if( $probe_name eq "p1" ){
      $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
    }elsif( $probe_name eq "p2" ){
      $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
    }
  }

}#Endsub probeGuideConfig_noda_chopb



sub probeGuideConfig_nodb_chopa{
  my ($soc, $probe_name, $state) = @_;

  my $wait_timed_out = 0;
  my $cmd;

  if( $probe_name eq "p1" ){
    if( $state eq "on" ){
      $cmd = "do p1GuideConfig nodbchopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p1GuideConfig nodbchopa=Off $ufgem::terminate";
    }
  }elsif( $probe_name eq "p2" ){
    if( $state eq "on" ){
      $cmd = "do p2GuideConfig nodbchopa=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p2GuideConfig nodbchopa=Off $ufgem::terminate";
    }
  }

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  if( $probe_name eq "p1" ){
    $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
  }elsif( $probe_name eq "p2" ){
    $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
  }

  if( !$wait_timed_out ){
    my $done = <$soc>;
    if( $probe_name eq "p1" ){
      $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
    }elsif( $probe_name eq "p2" ){
      $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
    }
  }

}#Endsub probeGuideConfig_noda_chopa



sub probeGuideConfig_nodb_chopb{
  my ($soc, $probe_name, $state) = @_;

  my $wait_timed_out = 0;
  my $cmd;

  if( $probe_name eq "p1" ){
    if( $state eq "on" ){
      $cmd = "do p1GuideConfig nodbchopb=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p1GuideConfig nodbchopb=Off $ufgem::terminate";
    }
  }elsif( $probe_name eq "p2" ){
    if( $state eq "on" ){
      $cmd = "do p2GuideConfig nodbchopb=On $ufgem::terminate";
    }elsif( $state eq "off" ){
      $cmd = "do p2GuideConfig nodbchopb=Off $ufgem::terminate";
    }
  }

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  if( $probe_name eq "p1" ){
    $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
  }elsif( $probe_name eq "p2" ){
    $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
  }

  if( !$wait_timed_out ){
    my $done = <$soc>;
    if( $probe_name eq "p1" ){
      $wait_timed_out = parse_gem_ack_or_done( "p1GuideConfig", $ack );
    }elsif( $probe_name eq "p2" ){
      $wait_timed_out = parse_gem_ack_or_done( "p2GuideConfig", $ack );
    }
  }

}#Endsub probeGuideConfig_noda_chopb



sub m1CorrectionsOn{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do m1Corrections state=On $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "m1Corrections", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "m1Corrections", $done );
  }

}#Endsub m1CorrectionsOn


sub m1CorrectionsOff{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do m1Corrections state=Off $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $cmd\n";
  print $soc "$cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "m1Corrections", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "m1Corrections", $done );
  }

}#Endsub m1CorrectionsOff


sub absorb_offsets{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do absorboffset $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "absorboffset", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "absorboffset", $done );
  }

}#Endsub absorb_offsets


sub absorb_adjusts{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do absorbadjust $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "absorbadjust", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "absorbadjustt", $done );
  }

}#Endsub absorb_adjusts


sub clear_offsets{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do clearoffset $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "clearoffset", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "clearoffset", $done );
  }

}#Endsub clear_offsets


sub clear_adjusts{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do clearadjust $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "clearadjust", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "clearadjust", $done );
  }

}#Endsub clear_adjusts


sub handsetAbsorb{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do handsetAbsorb $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "handsetAbsorb", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "handsetAbsorb", $done );
  }

}#Endsub handsetAbsorb


sub handsetClear{
  my $soc = $_[0];

  my $wait_timed_out = 0;
  my $cmd = "do handsetClear $ufgem::terminate";

  if( !defined ($soc) || $soc eq "" ){
    print "?Bad socket connnection to Gemini VII server.\n";
    return;
  }

  print "Executing $cmd\n";
  print $soc "$cmd";
  $soc->flush();
  my $ack = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "handsetClear", $ack );

  if(!$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "handsetClear", $done );
  }

}#Endsub handsetClear


sub setOffset_radec {
  my ( $soc, $ra, $dec ) = @_;

  my $wait_timed_out = 0;
  my $offset_cmd = 
    "do offset type=tangent plane dra=$ra ddec=$dec $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $offset_cmd\n";
  print $soc "$offset_cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "offset", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "offset", $done );
  }

}#Endsub setOffset_radec


sub setHandset_radec {
  my ( $soc, $ra, $dec ) = @_;

  my $wait_timed_out = 0;
  my $handset_cmd = 
    "do handset type=tangent plane dra=$ra ddec=$dec $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "\nExecuting $handset_cmd\n";
  print $soc "$handset_cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "handset", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "handset", $done );
  }

}#Endsub setHandset_radec


sub offsetAdjust_instrument{
  my ( $soc, $x_offset, $y_offset ) = @_;

  my $wait_timed_out = 0;
  my $offset_adj_cmd = 
    "do offsetAdjust frame=instrumentxy ".
    "off1=$x_offset off2=$y_offset $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  #print "\nShould be:  Executing $offset_adj_cmd\n";
  print "\nExecuting $offset_adj_cmd\n";
  print $soc "$offset_adj_cmd"; 
  $soc->flush();
  my $ack  = <$soc>;
  $wait_timed_out = parse_gem_ack_or_done( "offsetAdjust", $ack );

  if( !$wait_timed_out ){
    my $done = <$soc>;
    $wait_timed_out = parse_gem_ack_or_done( "offsetAdjust", $ack );
  }

}#Endsub offsetAdjust_instrument



sub parse_gem_ack_or_done{
  my ( $cmd, $ack_or_done) = @_;

  my $wait_timed_out = 0;

  chomp $ack_or_done;
  {  local $/ = "\r"; chomp $ack_or_done; }

  #index returns -1 on failure to find pattern
  my $failure_index = index $ack_or_done, ' -1 ';
  my $success_index = index $ack_or_done, ' 0 ';

  if( $failure_index == -1 and $success_index != -1 ){
    print "$cmd acknowledged or done successfully:\n";
    print "$ack_or_done\n\n";

  }elsif( $failure_index != -1 and $success_index == -1 ){
    if( $ack_or_done =~ m/the(\s+)wait(\s+)has(\s+)timed(\s+)out/ ){
      print "\n";
      print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
      print "The VII returned 'the wait has timed out'\n";
      print "Chris Mayer says $cmd should have executed successfully.\n";
      print "Continue ";
      use GetYN;
      my $reply = get_yn();
      if( !$reply ){
	die "Exiting.\n\n";
      }elsif( $reply ){
	$wait_timed_out = 1;
      }
    }else{
      print "$cmd not acknowledged or done successfuly:\n";
      print "$ack_or_done\n\n";
      die "Exiting\n\n";
    }
  }else{
    print "Not sure if $cmd executed successfully\n";
    print "$ack_or_done\n";
    print "Continue ";
    use GetYN;
    my $reply = get_yn();
    if( !$reply ){
      die "Exiting\n\n";
    }
  }

  return $wait_timed_out;
}#Endsub parse_gem_ack_or_done


sub handset_wait{
  my $wait = $ENV{HANDSET_WAIT};

  print "Waiting $wait sec for handset\n";
  sleep( $wait );

}#Endsub handset_wait


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  print "Waiting $wait sec for offset\n";
  sleep( $wait );

}#Endsub offset_wait


sub guide_wait{
  my $wait = $ENV{GUIDE_WAIT};

  print "Waiting $wait sec for guide\n";
  sleep( $wait );

}#Endsub offset_wait


sub nod_wait{
  my $wait = $ENV{NOD_WAIT};

  print "Waiting $wait sec for nod\n";
  sleep( $wait );

}#Endsub offset_wait


sub probe_wait{
  my $wait = $ENV{PROBE_WAIT};

  print "Waiting $wait sec for probe\n";
  sleep( $wait );

}#Endsub offset_wait


###________________###
sub getOffset_radec {
# tangent plane takes arcsec ra/dec
  local $ufgem::argc = @_;
  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

#  print "get offsetra $ufgem::terminate";
  print $ufgem::soc "get offsetra $ufgem::terminate"; $ufgem::soc->flush();
  local $ufgem::got = <$ufgem::soc>; print "$ufgem::got";

#  print "get offsetdec $ufgem::terminate";
  print $ufgem::soc "get offsetdec $ufgem::terminate"; $ufgem::soc->flush();
  $ufgem::got = <$ufgem::soc>; print "$ufgem::got";
  close($ufgem::soc);
  return $ufgem::got;
}#Endsub getOffset_radec


sub getStatus {
  local $ufgem::soc;
  local $ufgem::s = "ALL";

  local $ufgem::argc = @_;
  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
  if( $ufgem::argc > 1 ) { $ufgem::s = shift; }

  local $ufgem::reply = "";
  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }
  if( $ufgem::s ne "ALL" ) {
    print $ufgem::soc "get $ufgem::s $ufgem::terminate";
    $ufgem::soc->flush();
    $ufgem::reply = <$ufgem::soc>;
    #usually comment this one out
    #print "getstatus Gemini VII reply: $ufgem::reply";
    return $ufgem::reply;
  }

  foreach $ufgem::i (@ufgem::allstat) {
    print $ufgem::soc "get $ufgem::i $ufgem::terminate";
    $ufgem::soc->flush();
    $ufgem::rep = <$ufgem::soc>;
    $ufgem::reply = $ufgem::reply . $ufgem::rep;
  }
  return $ufgem::reply;
}#Endsub getStatus


sub printAllStatus {
  local $ufgem::soc;
  local $ufgem::argc = @_;
  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
  local $ufgem::reply;
  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
    print "bad socket connectio to gemini vii server\n";
    return;
  }
  foreach $ufgem::i (@ufgem::allstat) {
    print $ufgem::soc "get $ufgem::i $ufgem::terminate";
    $ufgem::soc->flush();
    $ufgem::reply = <$ufgem::soc>;
    print $ufgem::reply;
  }
}#Endsub printAllStatus


sub prompt_start_guiding_beam_A{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "Please have the SSA start guiding on the source pointing center\n";
  print "(no offsets) and set up in NOD BEAM=A\n\n";
  print "About to begin executing the dither pattern.";

  GetYN::query_ready();
}#Endsub prompt_setup


sub query_probe_name{
  print "Please tell me what guide probe you are using.\n";
  print "This code knows of the following probes:\n";
  print "Probe #\tProbe Name\n";
  print "  1    \t   p1\n";
  print "  2    \t   p2\n\n";
  print "Enter 1 or 2: ";

  my $probe_num;
  my $probe_name;
  my $is_valid = 0;
  until( $is_valid ){
    my $input = <STDIN>; chomp $input;
    $probe_num = $input;

    if( $probe_num == 1 or $probe_num == 2 ){
      $is_valid = 1;
      if( $probe_num == 1 ){
	$probe_name = "p1";
      }else{
	$probe_name = "p2";
      }
    }else{
      print "Please enter 1 or 2 ";
    }
  }
  print "Probe name is $probe_name\n\n";

  return $probe_name;

}#Endsub query_probe_name


sub probe_follow_on{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOn( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOn( $soc );
  }

}#Endsub probe_follow_on


sub probe_follow_off{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOff( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOff( $soc );
  }

}#Endsub probe_follow_off



####David's versions--I've modified them above
#sub setOffset_radec {
## tangent plane takes arcsec ra/dec
##  print "$ufgem::offset\n";
#  local $ufgem::argc = @_;
#  local $ufgem::ra;
#  local $ufgem::dec;
#  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
#  if( $ufgem::argc > 1 ) { $ufgem::ra = shift; }
#  if( $ufgem::argc > 2 ) { $ufgem::dec = shift; }
#  local $ufgem::offset = 
#    "do offset type=tangent plane dra=$ufgem::ra ddec=$ufgem::dec $ufgem::terminate";
#
#  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
#    print "?Bad socket connection to Gemini VII server\n";
#    return;
#  }
#  local $ufgem::reply = "";
#
##  print "get offsetra $ufgem::terminate";
#  print $ufgem::soc "get offsetra $ufgem::terminate";  $ufgem::soc->flush();
#  local $ufgem::gotra = <$ufgem::soc>; print "$ufgem::gotra";
#
##  print "get offsetdec $ufgem::terminate";
#  print $ufgem::soc "get offsetdec $ufgem::terminate"; $ufgem::soc->flush();
#  local $ufgem::gotdec = <$ufgem::soc>; print "$ufgem::gotdec";
#
#  print "$ufgem::offset";
#  print $ufgem::soc "$ufgem::offset"; $ufgem::soc->flush();
#  local $ufgem::ack = <$ufgem::soc>; print "$ufgem::ack";
#  local $ufgem::done = <$ufgem::soc>; print "$ufgem::done";
#
##  print "get offsetra $ufgem::terminate";
#  print $ufgem::soc "get offsetra $ufgem::terminate";  $ufgem::soc->flush();
#  $ufgem::gotra = <$ufgem::soc>; print "$ufgem::gotra";
#
##  print "get offsetdec $ufgem::terminate";
#  print $ufgem::soc "get offsetdec $ufgem::terminate"; $ufgem::soc->flush();
#  $ufgem::gotdec = <$ufgem::soc>; print "$ufgem::gotdec";
#
#  return $ufgem::gotra . $ufgem::gotdec;
#}
#
#
#sub guideOn {
## tangent plane takes arcsec ra/dec
#  local $ufgem::guide = "do guide state=On $ufgem::terminate";
##  print "$ufgem::guide\n";
#  local $ufgem::argc = @_;
#  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
#
#  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
#    print "?Bad socket connection to Gemini VII server\n";
#    return;
#  }
#  local $ufgem::reply = "";
#  print $ufgem::soc "$ufgem::guide";  $ufgem::soc->flush();
#  local $ufgem::ack = <$ufgem::soc>; print "$ufgem::ack";
#  $ufgem::reply = $ufgem::ack;
#  #local $ufgem::done = <$ufgem::soc>; print "$ufgem::done";
#  #$ufgem::reply = $ufgem::done;
#  return $ufgem::reply;
#}
#
#sub guideOff {
## tangent plane takes arcsec ra/dec
#  local $ufgem::guide = "do guide state=Off $ufgem::terminate";
##  print "$ufgem::guide\n";
#  local $ufgem::argc = @_;
#  if( $ufgem::argc > 0 ) { $ufgem::soc = shift; }
#
#  if( !defined ($ufgem::soc) || $ufgem::soc eq "" ) {
#    print "?Bad socket connection to Gemini VII server\n";
#    return;
#  }
#  local $ufgem::reply = "";
#  print $ufgem::soc "$ufgem::guide";  $ufgem::soc->flush();
#  local $ufgem::ack = <$ufgem::soc>; print "$ufgem::ack";
#  $ufgem::reply = $ufgem::ack;
#  #local $ufgem::done = <$ufgem::soc>; print "$ufgem::done";
#  #$ufgem::reply = $ufgem::done;
#  return $ufgem::reply;
#}

#sub parse_gem_ack_done{
#  my ( $cmd, $ack, $done ) = @_;
#
#  chomp $ack; chomp $done;
#  {  local $/ = "\r"; chomp $ack; chomp $done; }
#  my $where_to_strip_from = length "ack $cmd ";
#  my $ack_str = substr $ack, $where_to_strip_from;
#     $ack_str = substr $ack_str, 0, 1;
#
#     $where_to_strip_from = length "done ";
#  my $done_str = substr $done, $where_to_strip_from;
#     $done_str = substr $done_str, 0, 1;
#  
#  print "ack string from vii is $ack_str\n";
#  if( $ack_str >= 0 ){
#    print "$ack\n";
#  }else{
#    print "Possible problem issuing $cmd: $ack\n";
#  }
#
#  if( $done_str >= 0 ){
#    print "$done\n";
#  }else{
#    print "Not done yet?: $done_str\n";
#  }
#  print "\n";
#}#Endsub parse_gem_ack_done


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

ufgem - Perl extension for Flamingos

=head1 SYNOPSIS

  use ufgem;

=head1 DESCRIPTION

Use to get tcs info from vii at Gemini South

=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: ufgem.pm,v 0.1 2003/05/22 15:43:19 raines Exp $

$Locker:  $


=head1 AUTHOR

Originally by David Hon, turned into a module by SNR.

=head1 SEE ALSO

perl(1).

=cut
