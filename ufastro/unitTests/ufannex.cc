#if !defined(__ufannex_cc__)
#define __ufannex_cc__ "$Name:  $ $Id: ufannex.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufannex_cc__;

#include "UFAnnexIO.h"
__UFAnnexIO_H__(ufannex_cc);

int main(int argc, char** argv) {
  // just call UFAnnexIO main:
  return UFAnnexIO::main(argc, argv);
}

#endif // __ufannex_cc__
