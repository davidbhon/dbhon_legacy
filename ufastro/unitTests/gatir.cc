#include "unistd.h"
#include "stdio.h"
#include "fcntl.h"
#include "termio.h"

#include "iostream.h"
#include "string"

using namespace std ;

int main(int argc, char** argv) {
  static struct termio orig, raw;
  string dev;
#if defined(LINUX) || defined(Linux)
  dev = "/dev/ttyS1";
#else
  dev =  "/dev/term/a"; // Solaris
#endif

  int fd = open(dev.c_str(),O_RDWR);
	
  int io = ioctl(fd,TCGETA,&orig);
  io = ioctl(fd,TCGETA,&raw);

  raw.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
  raw.c_lflag &= ~ICANON;
  raw.c_lflag &= ~ECHO;
  raw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  //raw.c_cc[VTIME] = 1; // 0.1 second for timeout
  raw.c_cc[VTIME] = 0; // infinite timeout?

  io = ioctl(fd,TCSETAF,&raw);

  string bootprompt("-- blah, blah, blah -- GATIR Vx.yz >>");
  int nb = write(fd, bootprompt.c_str(), strlen(bootprompt.c_str()));
  clog<<"sent boot prompt: "<<bootprompt<<endl;

  char nl = 10;
  char cr = 13;
  char buf = 0;
  string ret;
  if( argc > 1 && argv[1][1] == 'n' ) {
    clog<<"send a newline..."<<endl;
    nb = write(fd, &nl, 1);
  }
  if( argc > 1 && argv[1][1] == 'c' ) {
    clog<<"send a carriage return..."<<endl;
    nb = write(fd, &cr, 1);
  }
  clog<<"read one command  at a time, and reply..."<<endl;
  string cmdin("");
  string status("");
  while( cmdin != "shutdown" ) {
    nb = read( fd, &buf, 1);
    if( buf == nl ) {
      clog<<"got newline..."<<endl;
    }
    if( buf == cr ) {
      clog<<"got carriage return..."<<endl;
    }
    if( buf == cr || buf == nl ) { 
      // gatir command terminator
      status = cmdin;
      status += "\r\n -- GATIR Vx.yz > ";
      // write command status return
      clog<<"got complete command, sending back status reply: "<<status<<endl;
      write(fd, status.c_str(), strlen(status.c_str()));
      cmdin = "";
    }
    if( buf != cr && buf != nl ) { // append to cmdin buf:
      cmdin += buf;
      clog<<"append another character: "<<cmdin<<endl;
    }
  }

  io = ioctl(fd,TCSETAF,&orig);

  close(fd);

  return 0;
}
