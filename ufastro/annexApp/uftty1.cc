#include "unistd.h"
#include "stdio.h"
#include "fcntl.h"
#include "termio.h"

#include "iostream.h"
#include "string"

#include "UFRuntime.h"

int main(int argc, char** argv) {
  bool _cr = false; // replace '\n' with '\r'?
  if( argc > 1 ) 
    if( strcmp(argv[1], "-cr") == 0 )
      _cr = true;

  static struct termio orig, raw, stdinraw;
  string dev;
#if defined(LINUX) || defined(Linux)
  dev = "/dev/ttyS1";
#else
  dev =  "/dev/term/b"; // Solaris
#endif

  int fd = ::open(dev.c_str(), O_RDWR);
  if( fd <= 0 ) {
    clog<<"open failed: "<<dev<<endl;
    return -1;
  }
  FILE* fdev = ::fdopen(fd, "w+");

  int io = ::ioctl(fd, TCGETA, &orig);
  io = ::ioctl(fd, TCGETA, &raw);
  raw.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
  raw.c_lflag &= ~ICANON;
  raw.c_lflag &= ~ECHO;
  raw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  raw.c_cc[VTIME] = 0; // infinite timeout? = 1 for 0.1 sec. timout
  io = ::ioctl(fd, TCSETAF, &raw);

  // reset stdin
  io = ::ioctl(0, TCGETA, &stdinraw);
  stdinraw.c_cflag = CLOCAL | CREAD;
  stdinraw.c_lflag &= ~ICANON;
  stdinraw.c_lflag &= ~ECHO;  // no echo for stdin
  stdinraw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  stdinraw.c_cc[VTIME] = 0; // infinite timeout? = 1 for 0.1 sec. timout
  io = ::ioctl(0, TCSETAF, &stdinraw);

  // i/o loop
  string cmd = "", cmdline = "";
  string serial = "";
  int c, nb= 0, avail= 0;
  while( true ) {
    avail = UFRuntime::available(0); // anything from stdin?
    // user input
    for( int in = 0; in < avail && avail > 0; ++in ) {
      nb = ::read( 0, &c, 1);
      if( nb <= 0 ) {
        clog<<"read stdin failed."<<endl;
        return -1;
      }
      cmd += c;
      if( c == '\n' ) {
	cmdline = cmd;
	cmd = "";
        clog<<"cmdline: "<<cmdline<<endl;
        if( cmdline.find("quit") != string::npos || cmdline.find("exit") != string::npos ) {
          io = ::ioctl(fd, TCSETAF, &orig);
          ::close(fd);
          return 0;
        }
      }
      if( c == '\n' && _cr ) c = '\r'; // replace nl with cr
      nb = ::write(fd, &c, 1); // send it to serial dev.
      if( nb <= 0 ) {
        clog<<"write failed: "<<dev<<endl;
        return -1;
      }
      ::fflush(fdev); // try to flush the output (buffer)?
      /*
      if( c == '\n' )
        clog<<"wrote newline to fd: "<<fd<<endl;
      else if( c == '\r' )
        clog<<"wrote carriage return to fd: "<<fd<<endl;
      else
        clog<<"wrote '"<<c<<"' to fd: "<<fd<<endl;
      */
    }
    // device input
    avail = UFRuntime::available(fd); // anything from serial device?
    //if( avail > 0 )
    //  clog<<"serial dev. char. avail= "<<avail<<endl;
    for( int sin = 0; sin < avail && avail > 0; ++sin ) {
      nb = ::read( fd, &c, 1);
      //if( c == '\r' ) c = '\n'; // always replace cr with nl?
      ::write(1, &c, 1); // send it to stdout
    }
  }

  return 0;
}
