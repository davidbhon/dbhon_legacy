#if !defined(__UFClientSocket_h__)
#define __UFClientSocket_h__ "$Name:  $ $Id: UFClientSocket.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFClientSocket_H__(arg) const char arg##UFClientSocket_h__rcsId[] = __UFClientSocket_h__; 

#include "UFSocket.h" // inherit send, recv, status methods
#include "cstring"

class UFClientSocket : public UFSocket {
protected:
  
  string _host;
  
  string _ipAddr;

public:

  UFClientSocket(int port= -1);

  UFClientSocket(const UFClientSocket& rhs);

  inline virtual ~UFClientSocket() {}

  virtual int connect(const string& host, int portNo= -1, bool block= true);

  inline string peerHost() { return _host; }
};

#endif // __UFClientSocket_h__
/**
 * A client interface to UFSocket, supporting connect() and friends.
 * Attributes inherited from UFSocket:
 * - int _portNo
 * - UFSocketInfo _sockinfo
 */
  /**
   * Construct a new UFClientSocket with the given port number
   *@param port The default port number, default to -1
   */
  /**
   * Copy constructor.
   *@param rhs The source UFClientSocket to copy
   */
  /**
   * Default destructor relies on UFSocket destructor
   */
  /**
   * Attempt to connect to the given host, blocking if specified
   *@param host The destination host of the connect
   *@param portNo The destination port of the connect
   *@param block If true, use blocking connect, nonblocking otherwise
   *@return The socket descriptor of the connected socket, or -1 on error
   */
  /**
   * Return the peer's host
   *@return The peer's host
   */
