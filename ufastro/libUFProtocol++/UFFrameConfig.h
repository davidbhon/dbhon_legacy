#if !defined(__UFFrameConfig_h__)
#define __UFFrameConfig_h__ "$Name:  $ $Id: UFFrameConfig.h,v 0.7 2003/09/25 21:25:14 varosi beta $"
#define __UFFrameConfig_H__(arg) const char arg##FrameConfig_h__rcsId[] = __UFFrameConfig_h__;

#include "UFInts.h"

class UFFrameConfig: public virtual UFInts {
protected:
  void _init(int w, int h, int d=32, bool little=false, int dmaCnt=0, int imageCnt=0,
	     int coAdds=0, int pixelSort=0, int frameObsSeqNo=0, int frameObsSeqTot=0,
	     int ChopBeam=0, int SaveSet=0, int NodBeam=0, int NodSet=0, 
             int offset= 0, int pixcnt= 0, int* vals=0);

  inline UFFrameConfig() {}

public:
  struct Status {
    int w; int h; int d; int littleEnd;
    int dmaCnt; int imageCnt; int coAdds; int pixelSort;
    int frameObsSeqNo; int frameObsSeqTot;
    int ChopBeam; int SaveSet; int NodBeam; int NodSet; int frameWriteCnt; int frameSendCnt;
    int bgADUs; float bgWellpc; float sigmaFrmNoise;
    int offADUs; float offWellpc; float sigmaReadNoise;
    int frameCoadds; int chopSettleFrms; int chopCoadds; float frameTime; float savePeriod;
    int offset; int pixcnt;
    int bgADUmin; int bgADUmax; float sigmaFrmMin; float sigmaFrmMax;
    int rdADUmin; int rdADUmax; float sigmaReadMin; float sigmaReadMax;

    inline Status(const Status& rhs) { *this = rhs; }
    inline Status() : w(320), h(240), d(32), littleEnd(0), dmaCnt(0), imageCnt(0),
                      coAdds(0), pixelSort(0), frameObsSeqNo(0), frameObsSeqTot(0),
	              ChopBeam(0), SaveSet(0), NodBeam(0), NodSet(0),
                      frameWriteCnt(0), frameSendCnt(0),
                      bgADUs(0),  bgWellpc(0.0),  sigmaFrmNoise(0.0),
                      offADUs(0),  offWellpc(0.0),  sigmaReadNoise(0.0),
                      frameCoadds(0),  chopSettleFrms(0),  chopCoadds(0),
                      frameTime(0.0), savePeriod(0.0), offset(0), pixcnt(0),
                      bgADUmin(0), bgADUmax(0), sigmaFrmMin(0.0), sigmaFrmMax(0.0),
                      rdADUmin(0), rdADUmax(0), sigmaReadMin(0.0), sigmaReadMax(0.0) {}
  };

  inline virtual string description() const { return string(__UFFrameConfig_h__); }

  // be sure to be consistent here!
  inline static int statusElements() { return sizeof(UFFrameConfig::Status)/sizeof(int); }

  // UFFrameConfig ctor for create function
  inline UFFrameConfig(const UFProtocolAttributes& pa) : UFInts() {
    _paInit(pa);
    if( _pa->_type != _FrameConfig_ || _pa->_elem != statusElements() ) {
      clog << "UFrameConfig::> ? type mismatch, _pa->_type, _elem = "
	   << _pa->_type << ", " << _pa->_elem << endl;
      _pa->_type=_FrameConfig_;
      _pa->_elem = statusElements();
    }
    _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
    if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new int[_pa->_elem];
    _currentTime();
  }
  
  inline UFFrameConfig(int w, int h, int d=32, bool little=false, int dmaCnt=0, int imageCnt=0,
		       int coAdds=0, int pixelSort=0, int frameObsSeqNo=0, int frameObsSeqTot=0,
		       int ChopBeam=0, int SaveSet=0, int NodBeam=0, int NodSet=0, int offset= 0, int pixcnt= 0) : UFInts() {
    _paInit(_FrameConfig_, "UFFrameConfig");
    _init(w, h, d, little,  dmaCnt, imageCnt, coAdds,
          pixelSort, frameObsSeqNo, frameObsSeqTot, ChopBeam, SaveSet, NodBeam, NodSet, offset, pixcnt);
  }

  inline UFFrameConfig(const string& name,
		       int w, int h, int d=32, bool little=false, int dmaCnt=0, int imageCnt=0,
		       int coAdds=0, int pixelSort=0, int frameObsSeqNo=0, int frameObsSeqTot=0,
		       int ChopBeam=0, int SaveSet=0, int NodBeam=0, int NodSet=0, int offset= 0, int pixcnt= 0) : UFInts() {
    _paInit(_FrameConfig_, name);
    _init(w, h, d,little,  dmaCnt, imageCnt, coAdds,
          pixelSort, frameObsSeqNo, frameObsSeqTot, ChopBeam, SaveSet, NodBeam, NodSet, offset, pixcnt);
  }

  inline UFFrameConfig(const UFFrameConfig::Status& s) {
    _paInit(_FrameConfig_, "UFFrameConfig");
    _init(s.w, s.h, s.d, s.littleEnd, s.dmaCnt, s.imageCnt, s.coAdds, s.pixelSort,
	  s.frameObsSeqNo, s.frameObsSeqTot, s.ChopBeam, s.SaveSet, s.NodBeam, s.NodSet, s.offset, s.pixcnt);
  }

  // const UFints ctor makes use of shallow copy
  inline UFFrameConfig(const UFInts& ufi,
		       int w, int h, int d= 32, bool little= false,
		       int dmaCnt=0, int imageCnt=0 ,int coAdds= 0,
		       int pixelSort=0, int frameObsSeqNo=0, int frameObsSeqTot=0,
		       int ChopBeam=0, int SaveSet=0,
		       int NodBeam=0, int NodSet=0, int offset= 0, int pixcnt= 0) : UFInts() {
    _paInit(_FrameConfig_, ufi.attributesPtr()); // shallow copy convertor?
    // reset "shallow" copy values
    _init(w, h, d,little,  dmaCnt, imageCnt, coAdds,
          pixelSort, frameObsSeqNo, frameObsSeqTot,
          ChopBeam, SaveSet, NodBeam, NodSet, offset, pixcnt);
  }

  inline UFFrameConfig( const UFInts& ufi ) : UFInts() {
    _paInit( _FrameConfig_, ufi.attributesPtr() ); // shallow copy convertor?
  }

  inline UFFrameConfig( UFInts& ufi ) : UFInts() {
    _paInit( _FrameConfig_, ufi.attributesPtr() ); // shallow copy convertor?
  }

  // UFFrameConfig copy ctors:
  
  // deep copy of vals:
  UFFrameConfig( const string& name, const char* vals, const string& timeStamp, bool shared=false );

  inline UFFrameConfig(const UFFrameConfig& ufc) : UFInts() { // shallow copy
    clog << "UFFrameConfig::> shallow copy!" << endl;
    _paInit( _FrameConfig_, ufc.attributesPtr() ); // shallow copy helper
    _currentTime();
    if( _pa->_elem != statusElements() )
      clog<<"UFFrameConfig::> ? ctor error _elem= "<<_pa->_elem<<" : "<<elements()<<endl;
    _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
  }

  inline UFFrameConfig(UFFrameConfig& ufc) : UFInts() { // deep copy (not working?)
    clog << "UFFrameConfig::> deep copy!" << endl;
    printHeader(ufc.attributesPtr());
    _paInit( ufc.attributesRef() ); // copy helper
    _currentTime();
    deepCopy( ufc );
    if( _pa->_elem != statusElements() )
      clog<<"UFFrameConfig::> ? ctor error _elem= "<<_pa->_elem<<" : "<<elements()<<endl;
    _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
    printHeader(_pa);
    clog << "UFFrameConfig::> deep copy is done!" << endl;
  }

  // UFFrameConfig convertor ctor
  inline UFFrameConfig(const UFProtocol& ufp) : UFInts() { // shallow copy
    _paInit(_FrameConfig_, ufp.attributesPtr()); // shallow copy helper
    _currentTime();
    if( _pa->_elem != statusElements() )
      clog<<"UFFrameConfig::> ? ctor error _elem= "<<_pa->_elem<<" : "<<elements()<<endl;
    _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
  }

  // convenience functions
  inline int width() const { return ((int*)_pa->_values)[0]; }
  inline int setWidth(int w) { return ((int*) _pa->_values)[0] = w; }
  inline int height() const { return ((int*) _pa->_values)[1]; }
  inline int setHeight(int h) { return ((int*) _pa->_values)[1] = h; }
  inline int depth() const { return ((int*) _pa->_values)[2]; }
  inline int setDepth(int d) { return ((int*) _pa->_values)[2] = d; }
  inline int littleEndian() const { return ((int*) _pa->_values)[3]; }
  inline int setLittleEndian(int l) { return ((int*) _pa->_values)[3] = l; }
  inline int dmaCnt() const { return ((int*) _pa->_values)[4]; }
  inline int setDMACnt(int d) { return ((int*) _pa->_values)[4] = d; }
  inline int imageCnt() const { return ((int*) _pa->_values)[5]; }
  inline int setImageCnt(int d) { return ((int*) _pa->_values)[5] = d; }
  inline int coAdds() const { return ((int*) _pa->_values)[6]; }
  inline int setCoAdds(int d) { return ((int*) _pa->_values)[6] = d; }
  inline int pixelSort() const { return ((int*) _pa->_values)[7]; }
  inline int setPixelSort(int d) { return ((int*) _pa->_values)[7] = d; }
  inline int frameObsSeqNo() const { return ((int*) _pa->_values)[8]; }
  inline int setFrameObsSeqNo(int d) { return ((int*) _pa->_values)[8] = d; }
  inline int frameObsSeqTot() const { return ((int*) _pa->_values)[9]; }
  inline int setFrameObsSeqTot(int d) { return ((int*) _pa->_values)[9] = d; }
  inline int ChopBeam() const { return ((int*) _pa->_values)[10]; }
  inline int setChopBeam(int d) { return ((int*) _pa->_values)[10] = d; }
  inline int SaveSet() const { return ((int*) _pa->_values)[11]; }
  inline int setSaveSet(int d) { return ((int*) _pa->_values)[11] = d; }
  inline int NodBeam() const { return ((int*) _pa->_values)[12]; }
  inline int setNodBeam(int d) { return ((int*) _pa->_values)[12] = d; }
  inline int NodSet() const { return ((int*) _pa->_values)[13]; }
  inline int setNodSet(int d) { return ((int*) _pa->_values)[13] = d; }
  inline int frameWriteCnt() const { return ((int*) _pa->_values)[14]; }
  inline int setFrameWriteCnt(int d) { return ((int*) _pa->_values)[14] = d; }
  inline int frameSendCnt() const { return ((int*) _pa->_values)[15]; }
  inline int setFrameSendCnt(int d) { return ((int*) _pa->_values)[15] = d; }
  inline int bgADUs() const { return ((int*)_pa->_values)[16]; }
  inline void bgADUs(int i) { ((int*)_pa->_values)[16] = i; }
  inline float bgWellpc() const { return ((float*)_pa->_values)[17]; }
  inline void bgWellpc(float f) { ((float*)_pa->_values)[17] = f; }
  inline float sigmaFrmNoise() const { return ((float*)_pa->_values)[18]; }
  inline void sigmaFrmNoise(float f) { ((float*)_pa->_values)[18] = f; }
  inline int offADUs() const { return ((int*)_pa->_values)[19]; }
  inline void offADUs(int i) { ((int*)_pa->_values)[19] = i; }
  inline float offWellpc() const { return ((float*)_pa->_values)[20]; }
  inline void offWellpc(float f) { ((float*)_pa->_values)[20] = f; }
  inline float sigmaReadNoise() const { return ((float*)_pa->_values)[21]; }
  inline void sigmaReadNoise(float f) { ((float*)_pa->_values)[21] = f; }
  inline int frameCoadds() const { return ((int*)_pa->_values)[22]; }
  inline void frameCoadds(int i) { ((int*)_pa->_values)[22] = i; }
  inline int chopSettleFrms() const { return ((int*)_pa->_values)[23]; }
  inline void chopSettleFrms(int i) { ((int*)_pa->_values)[23] = i; }
  inline int chopCoadds() const { return ((int*)_pa->_values)[24]; }
  inline void chopCoadds(int i) { ((int*)_pa->_values)[24] = i; }
  inline float frameTime() const { return ((float*)_pa->_values)[25]; }
  inline void frameTime(float f) { ((float*)_pa->_values)[25] = f; }
  inline float savePeriod() const { return ((float*)_pa->_values)[26]; }
  inline void savePeriod(float f) { ((float*)_pa->_values)[26] = f; }
  inline int offset() const { return ((int*) _pa->_values)[27]; }
  inline int setOffset(int o) { if( o < 0 || o >= width() * height() ) o = 0; return ((int*) _pa->_values)[27] = o; }
  inline int pixCnt() const { return ((int*) _pa->_values)[28]; }
  inline int setPixCnt(int c) { if( c <= 0 ) c = width()*height(); return ((int*) _pa->_values)[28] = c; }
  inline int bgADUmin() const { return ((int*)_pa->_values)[29]; }
  inline void bgADUmin(int i) { ((int*)_pa->_values)[29] = i; }
  inline int bgADUmax() const { return ((int*)_pa->_values)[30]; }
  inline void bgADUmax(int i) { ((int*)_pa->_values)[30] = i; }
  inline float sigmaFrmMin() const { return ((float*)_pa->_values)[31]; }
  inline void sigmaFrmMin(float f) { ((float*)_pa->_values)[31] = f; }
  inline float sigmaFrmMax() const { return ((float*)_pa->_values)[32]; }
  inline void sigmaFrmMax(float f) { ((float*)_pa->_values)[32] = f; }
  inline int rdADUmin() const { return ((int*)_pa->_values)[33]; }
  inline void rdADUmin(int i) { ((int*)_pa->_values)[33] = i; }
  inline int rdADUmax() const { return ((int*)_pa->_values)[34]; }
  inline void rdADUmax(int i) { ((int*)_pa->_values)[34] = i; }
  inline float sigmaReadMin() const { return ((float*)_pa->_values)[35]; }
  inline void sigmaReadMin(float f) { ((float*)_pa->_values)[35] = f; }
  inline float sigmaReadMax() const { return ((float*)_pa->_values)[36]; }
  inline void sigmaReadMax(float f) { ((float*)_pa->_values)[36] = f; }

  //alternate accessor & mutator for imageCnt:
  inline int frameGrabCnt() const { return ((int*)_pa->_values)[5]; }
  inline void frameGrabCnt(int d) { ((int*)_pa->_values)[5] = d; }

  //alternate accessor & mutators:
  inline int rdADUs() const { return ((int*)_pa->_values)[19]; }
  inline void rdADUs(int i) { ((int*)_pa->_values)[19] = i; }
  inline float rdWellpc() const { return ((float*)_pa->_values)[20]; }
  inline void rdWellpc(float f) { ((float*)_pa->_values)[20] = f; }

  //alternate mutators:
  inline void frameObsSeqNo(int d) { ((int*) _pa->_values)[8] = d; }
  inline void frameObsSeqTot(int d) { ((int*) _pa->_values)[9] = d; }
  inline void frameWriteCnt(int d) { ((int*) _pa->_values)[14] = d; }
  inline void frameSendCnt(int d) { ((int*) _pa->_values)[15] = d; }

  //accessors & mutators for storing results of method computeStats() in Frm Acq server:


  // assuming the name field is used for buffer update info. with the
  // designated delimiter:
  // return the number and names of most recently set/updateed
  int getUpdatedBuffNames(vector< string >& bufnames, const string& delim= "^");
  // chop/nod buffers in the form "instrum:bufname"
  int getUpdatedBuffNames(const string& instrum, vector< string >& bufnames, const string& delim="^");
  // set the names
  int setUpdatedBuffNames(const vector< string >& bufnames, const string& delim= "^");

  // additional functions support configuration info i/o
  // these are not intrinsically part of the UFProtocol
  // return <= 0 on failure, num. bytes i/o on success
  static int writeConfig(int fd, const UFFrameConfig& c);
  static int readConfig(int fd, UFFrameConfig& c); // resets c
  static int sendConfig(UFSocket& soc, const UFFrameConfig& c);
  static int recvConfig(UFSocket& soc, UFFrameConfig& c); // resets c
  static int reqConfig(UFSocket& soc, UFFrameConfig& c); // resets c
};
    
#endif // UFFrameConfig
    
