#if !defined(__UFFITSClient_h__)
#define __UFFITSClient_h__ "$Name:  $ $Id: UFFITSClient.h,v 0.5 2005/04/05 18:19:33 hon Exp $";
#define __UFFITSClient_H__(arg) const char arg##UFFITSClient_h__rcsId[] = __UFFITSClient_h__;

// c++
using namespace std;
#include "string"
#include "vector"

#include "UFDaemon.h"
#include "UFStrings.h"
#include "UFFITSheader.h"

// although technically not a daemon, a client app. can make use 
// of the daemon runtime funcs:
class UFFITSClient : public UFDaemon {
public:
  struct AgentLocation {
    int port;
    string host;

    
    inline AgentLocation() : port(-1) { host = UFRuntime::hostname(); }

    
    inline AgentLocation(string host, int port= -1) : port(port), host(host) {}
  };
  //typedef std::map< string, UFSocket* > ConnectTable;
  typedef map< string, string > FITSElements;
  typedef map< string, AgentLocation* > AgentLoc;

  
  inline UFFITSClient(const string& name) : UFDaemon(name) {}

  
  inline UFFITSClient(const string& name, int argc,
		      char** argv, char** envp) : UFDaemon(name, argc, argv, envp) {}

  
  inline ~UFFITSClient() {}

  // unit test main

  
  static int main(const string& name, int argc, char** argv, char** envp);


  
  inline virtual string description() const { return __UFFITSClient_h__; }

  // Agent connections

  
  int locateAgents(const string& host, UFFITSClient::AgentLoc& loc, int port= -1);
  // connect to all agents if port < 0, or individual agent otherwise...

  
  virtual int connectAgents(UFFITSClient::AgentLoc& loc, UFSocket::ConnectTable& connections,
			    int port= -1, bool block= false);

  
  UFSocket* connect(const string& agent, const UFFITSClient::AgentLocation& aloc, bool block= false);

  // Each agent provides it FITS fragment contribution

  
  UFStrings* fetchAgentsFITS(UFSocket::ConnectTable& connections, float timeOut= 0.1);

  
  UFStrings* fetchFITS(UFSocket* soc, float timeOut= 0.1);

  
  UFStrings* fetchAllFITS(UFSocket::ConnectTable& connections, float timeOut= 0.1, bool obs= false);

  
  int fetchAndPrint(UFSocket::ConnectTable& connections, float timeOut= 0.1);

  // this establishes the nominal list of Telescope/Observatory stuff

  
  virtual int clearObservatoryFITS(FITSElements& vals, FITSElements& comments);
  // format observatory FITS output

  
  int observatoryFITS(FITSElements& vals, FITSElements& comments);
  
  static bool _verbose;
  static bool _observatory;
  static string _host;

protected:
  //std::map< string, string > _agenthosts;
  //std::map< string, int > _agentports;
  //UFSocket::ConnectTable _connections;
};


#endif // __UFFITSClient_h__
    /**
     *
     */
    /**
     *
     *@param host TBD
     *@param port TBD
     */
  /**
   *
   *@param name TBD
   */
  /**
   *
   *@param name TBD
   *@param argc TBD
   *@param argv TBD
   *@param envp TBD
   */
  /**
   *
   */
  /**
   *
   *@param name TBD
   *@param argc TBD
   *@param argv TBD
   *@param envp TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param host TBD
   *@param loc TBD
   *@param port TBD
   *@return TBD
   */
  /**
   *
   *@param loc TBD
   *@param connections TBD
   *@param port TBD
   *@param block TBD
   *@return TBD
   */
  /**
   *
   *@param agent TBD
   *@param aloc TBD
   *@param block TBD
   *@return TBD
   */
  /**
   *
   *@param connections TBD
   *@param timeOut TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@param timeOut TBD
   *@return TBD
   */
  /**
   *
   *@param connections TBD
   *@param timeOut TBD
   *@param obs TBD
   *@return TBD
   */
  /**
   *
   *@param connections TBD
   *@param timeOut TBD
   *@return TBD
   */
  /**
   *
   *@param vals TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param vals TBD
   *@param comments TBD
   *@return TBD
   */
