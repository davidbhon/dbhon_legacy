#if !defined(__UFFlamObsConf_cc__)
#define __UFFlamObsConf_cc__ "$Name:  $ $Id: UFFlamObsConf.cc 14 2008-06-11 01:49:45Z hon $"
#include "UFFlamObsConf.h"
__UFFlamObsConf_H__(__UFFlamObsConf_cc);

#include "UFRuntime.h"
#include "UFInts.h"
#include "string"
#include "vector"
#include "map"


// UFFlamObsConf ctor for create function:
UFFlamObsConf::UFFlamObsConf() : UFFloats() {
  _paInit(_FlamObsConf_, "Start||Test");
  if( _pa->_type != _FlamObsConf_ ) {
    clog<<"UFFlamObsConf::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type = _FlamObsConf_;
  }
  if( _pa->_values ) delete [] (float*) _pa->_values;
  _pa->_values = (void*) _init();
  _currentTime();
}

UFFlamObsConf::UFFlamObsConf(float exptime, const string& label, int frmmode) : UFFloats() {
  _paInit(_FlamObsConf_, "Start||Test");
  if( !label.empty() ) relabel(label);
  if( _pa->_values ) delete [] (float*) _pa->_values;
  _pa->_values = (void*) _init(exptime, frmmode);
  _currentTime();
  //clog<<"UFFlamObsConf::UFFlamObsConf> type: "<<typeId()<<", elem: "<<elements()<<", name: "<<name()<<endl;
  clog<<"UFFlamObsConf::UFFlamObsConf> directive: "<<directive()<<", datalabel: "<<datalabel()
      <<", frmmode: "<<frmmode<<", _values ptr: "<<(long*)_pa->_values<<endl;
  clog<<"UFFlamObsConf::UFFlamObsConf> exptime: "<<expTime()<<", width, height: "<<width()<<", "<<height()<<endl;
  //printHeader(_pa);
}

// construct form command queue
UFFlamObsConf::UFFlamObsConf(const deque< string >& cmds) {
  _paInit(_FlamObsConf_, "CmdDeque");
  //if( _pa->_values ) delete [] (float*) _pa->_values;
  float exptime= 1.0;
  int frmmode= 1;
  for( size_t i = 0; i < cmds.size()/2; i += 2) {
    string cmd = cmds[i], param = cmds[i+1];
    UFRuntime::upperCase(cmd); 
    if( cmd.find("OBS") != string::npos ) { 
      rename(param);
      clog<<"UFFlamObsConf::> rename: "<<param<<endl;
      continue;
    }
    if( cmd.find("LABEL") != string::npos ) {
      relabel(param);
      clog<<"UFFlamObsConf::> relabel: "<<param<<endl;
      continue;
    }
    if( cmd.find("TIME") != string::npos ) {
      exptime = atof(param.c_str());
      clog<<"UFFlamObsConf::> exptime: "<<param<<" == "<<exptime<<endl;
      continue;
    }
    if( cmd.find("CNT") != string::npos ) {
      frmmode = atoi(param.c_str());
      clog<<"UFFlamObsConf::> expcnt: "<<param<<" == "<<frmmode<<endl;
      continue;
    }
  }

  _pa->_values = (void*) _init(exptime, frmmode);
  _currentTime();
}

// UFFlamObsConf ctor for create function:
UFFlamObsConf::UFFlamObsConf(const UFProtocolAttributes& pa) : UFFloats() {
  _paInit(pa);
  if( _pa->_type != _FlamObsConf_ ) {
    clog<<"UFFlamObsConf::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type = _FlamObsConf_;
  }
  if( _pa->_values ) delete [] (float*) _pa->_values;
  _pa->_values = (void*) _init();
}

// UFFlamObsConf ctor for create function:
UFFlamObsConf::UFFlamObsConf(const UFFlamObsConf& oc) : UFFloats() {
  _paInit(_FlamObsConf_, oc._pa);
  if( _pa->_type != _FlamObsConf_ ) {
    clog<<"UFFlamObsConf::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type = _FlamObsConf_;
  }
  if( _pa->_values ) delete [] (float*) _pa->_values;
  _pa->_values = (void*) _init();
  memcpy(_pa->_values, oc._pa->_values, oc.elements()*sizeof(float));
}

string UFFlamObsConf::directive() const { 
  string nm = name(); size_t pos = nm.find("||");
  if( pos == string::npos || pos == 0 )
    return "null";

  return nm.substr(0,pos);
}

string UFFlamObsConf::datalabel() const { 
  string nm = name(); size_t pos = nm.rfind("|");
  if( pos == string::npos )
    return "null";
  if( ++pos >= nm.length() )
    return "null";
  return nm.substr(pos);
}

void UFFlamObsConf::setConfig(const UFFlamObsConf::Config& c, const string& label) {
  memcpy(valFloats(0), &c._exptime, sizeof(float)); 
  *(valFloats(1)) = c._w; *(valFloats(2)) = c._h; *(valFloats(3)) = c._d; *(valFloats(4)) = c._littleEndian;
  *(valFloats(5))= c._frmspernod; *(valFloats(6))= c._nodsperobs; *(valFloats(7)) = c._nodIdx; *(valFloats(8))= c._nodIdxF;
  if( !label.empty() ) relabel(label);
}

string UFFlamObsConf::getConfig(UFFlamObsConf::Config& c) {
  memcpy(&c._exptime, valFloats(0), sizeof(float));
  c._w= *valInts(1); c._h= *valInts(2); c._d= *valInts(3); c._littleEndian =* valInts(4);
  c._frmspernod = *valInts(5); c._nodsperobs = *valInts(6); c._nodIdx = *valInts(6); c._nodIdxF = *valInts(8);
  return name();
}

string UFFlamObsConf::abortObs(string label) {
  if( label.empty() ) label = datalabel();
  string nm = "Abort||"+label;
  return rename(nm);
}

string UFFlamObsConf::stopObs(string label) {
  if( label.empty() ) label = datalabel();
  string nm = "Stop||"+label;
  return rename(nm);
}

string UFFlamObsConf::startObs(float exptime, string label, int idx0, int idxF, int nods) {
  if( exptime > 0 ) setExpTime(exptime);
  if( idx0 > 0 ) *(valFloats(5))= idx0;
  if( idxF > 0 ) *(valFloats(6))= idxF;
  if( nods > 0 ) *(valFloats(7))= nods;
  if( label.empty() ) label = datalabel();
  string nm = "Start||"+label;
  return rename(nm);
}

string UFFlamObsConf::discardObs(string label, int idx0, int idxF, int nods) {
  if( idx0 > 0 ) *(valFloats(5))= idx0;
  if( idxF > 0 ) *(valFloats(6))= idxF;
  if( nods > 0 ) *(valFloats(7))= nods;
  if( label.empty() ) label = datalabel();
  string nm = "Discard||"+label;
  return rename(nm);
}

// if name/datalabel contains 'abort', this is an aborted obs:
bool UFFlamObsConf::abort() const { 
  string nm = name(); 
  if( nm.find("abort") == string::npos &&
      nm.find("Abort") == string::npos && nm.find("ABORT") == string::npos )
  return false;
  return true;
}

// if name/datalabel contains 'stop', this is a stopped obs:
bool UFFlamObsConf::stop() const { 
  string nm = name(); 
  if( nm.find("stop") == string::npos &&
      nm.find("Stop") == string::npos && nm.find("STOP") == string::npos )
  return false;
  return true;
}

// if name/datalabel contains 'discard', this obs data is not to be saved:
// (also indicates start obs)
bool UFFlamObsConf::discard() const { 
  string nm = name(); 
  if( nm.find("discard") == string::npos &&
      nm.find("Discard") == string::npos && nm.find("DISCARD") == string::npos )
  return false;
  return true;
}

// if name/datalabel contains 'start', this is a new obs:
bool UFFlamObsConf::start() const { 
  string nm = name();
  if( discard() ) return true;
  if( nm.find("start") == string::npos &&
      nm.find("Start") == string::npos && nm.find("START") == string::npos )
  return false;
  return true;
}

//protected:
float* UFFlamObsConf::_init(float exptime, int frmmode, int cnt) {
  //clog<<"UFFlamObsConf::_init> exptime, frmmode: "<<exptime<<", "<<frmmode<<endl;
  _shared = _shallow = false;
  Config c(exptime, frmmode);
  float* vals = new float[cnt]; memset(vals, 0, cnt*sizeof(float));
  memcpy(&vals[0], &c._exptime, sizeof(float));
  vals[1] = c._w; vals[2] = c._h; vals[3] = c._d; vals[4] = c._littleEndian = 1;
  vals[5] = c._frmspernod; vals[6] = c._nodsperobs; vals[7] = c._nodIdx; vals[8] = c._nodIdxF;
  _pa->_elem = cnt;
  _pa->_length = minLength() + cnt*sizeof(float);
  return vals;
}

#endif // __UFFlamObsConf_cc__

