#if !defined(__UFFITSheader_cc__)
#define __UFFITSheader_cc__ "$Name:  $ $Id: UFFITSheader.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFITSheader_cc__;

#include "UFFITSheader.h"
__UFFITSheader_H__(__UFFITSheader_cc);

#include "math.h"
#include "cstring"

int UFFITSheader::_verbose= 0;

// helper for ctors: adds one standard rec containing SIMPLE keyword:
int UFFITSheader::Simple( const string& comment, bool simple ) {
  string cmt = comment.substr(0,48);
  rmJunk(cmt);
  int nc= 0;
  if( simple )
    nc = ::sprintf( _FITSrecord, "SIMPLE  =                    T /%-48s", cmt.c_str() );
  else
    nc = ::sprintf( _FITSrecord, "SIMPLE  =                    F /%-48s", cmt.c_str() );

  if( nc < 0 )
    clog<<"UFFITSheader::Simple> truncation required..."<<endl;

  push_front( _FITSrecord );

  return size();
}

// ctor creates FITS header with one standard rec containing SIMPLE keyword:
UFFITSheader::UFFITSheader( const string& comment, bool simple ) {
  Simple( comment, simple );
}

// ctors create basic FITS header with SIMPLE and NAXIS... keywords (so file is readable):

UFFITSheader::UFFITSheader( UFFrameConfig& fc, const string& comment, bool simple ) {
  Simple( comment, simple );
  add("BITPIX", fc.depth()    , "Bits per pixel" );
  add("NAXIS" , 2, " " );
  add("NAXIS1", fc.width()    , "X dimension of array" );
  add("NAXIS2", fc.height()   , "Y dimension of array" );
  date();
}

UFFITSheader::UFFITSheader( UFFrameConfig& fc, UFObsConfig& oc, const string& comment, bool simple ) {
  Simple( comment, simple );
  add("BITPIX", fc.depth()    , "Bits per pixel" );
  add("NAXIS" , 6, " " );
  add("NAXIS1", fc.width()    , "X dimension of array" );
  add("NAXIS2", fc.height()   , "Y dimension of array" );
  add("NAXIS3", oc.chopBeams(), "Number of chop positions" );
  add("NAXIS4", oc.saveSets() , "Number of savesets per nod phase" );
  add("NAXIS5", oc.nodBeams() , "Number of nod positions" );
  add("NAXIS6", oc.nodSets()  , "Number of nod cycles" );
  date();
}

UFFITSheader::UFFITSheader(UFFlamObsConf& oc, const string& comment, bool simple) {
  Simple( comment, simple );
  add("BITPIX", oc.depth()    , "Bits per pixel" );
  add("NAXIS" , 2, " " );
  add("NAXIS1", oc.width()    , "X dimension of array" );
  add("NAXIS2", oc.height()   , "Y dimension of array" );
  date();
}

// method to add current UT date/time to the FITS header:
int UFFITSheader::date( const string& comment ) {
  string timestamp = UFRuntime::currentTime();
  string time = timestamp.substr(0,17);
  return add("DATE_FH", time, comment );
}

// methods to add FITS records (keyword = value / comment) to the FITS header:
int UFFITSheader::add( const string& keyword, const string& value, const string& comment ) {
  string valq = "'" + value.substr(0,18) + "'";
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(valq); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %-20s /%-48s", key.c_str(), valq.c_str(), cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  push_back( _FITSrecord );
  return size();
}

int UFFITSheader::add( const string& keyword, const int value, const string& comment ) {
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %20d /%-48s", key.c_str(), value, cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  push_back( _FITSrecord );
  return size();
}

int UFFITSheader::add( const string& keyword, const double value, const string& comment ) {
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %20g /%-48s", key.c_str(), value, cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  push_back( _FITSrecord );
  return size();
}

int UFFITSheader::_replace(const string& key) {
  size_t cnt = size();
  bool replaced = false;
  for( size_t i = 0; i < cnt && !replaced; ++i ) {
    string entry = (*this)[i];
    if( entry.find(key) != string::npos ) {
      (*this)[i] = _FITSrecord;
      replaced = true;
    }
  }
  if( !replaced ) // then just add it?
    push_back( _FITSrecord );
  //clog<<"UFFITSheader::_replace> (true/false): "<<replaced<<", rec: "<<_FITSrecord<<endl;
  return size();
}

int UFFITSheader::replace( const string& keyword, const string& value, const string& comment ) {
  string valq = "'" + value.substr(0,18) + "'";
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(valq); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %-20s /%-48s", key.c_str(), valq.c_str(), cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  return _replace(key);
}

int UFFITSheader::replace( const string& keyword, const int value, const string& comment ) {
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %20d /%-48s", key.c_str(), value, cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  return _replace(key);
}

int UFFITSheader::replace( const string& keyword, const double value,  const string& comment ) {
  string key = keyword.substr(0,8);
  string cmt = comment.substr(0,48);
  rmJunk(key); rmJunk(cmt);
  UFStrings::upperCase(key); // force all keys to upper case

  memset(_FITSrecord, 0, sizeof(_FITSrecord));
  int nc = ::sprintf( _FITSrecord, "%-8s= %20g /%-48s", key.c_str(), value, cmt.c_str() );
  if( nc < 0 )
    clog<<"UFFITSheader::add> truncation required..."<<endl;

  return _replace(key);
}
  
// presumably this is a string already in FITS header (single entry) format:
int UFFITSheader::add( string& s ) {
  if( s.find("=") != 8 ) // clearly not a FITS header entry?
    return size();

  if( s.length() > 80 )
    s = s.substr(0, 80); // truncate just in case
  else if( s.length() < 80 ) { // or add spaces
    int nsp = 80 - s.length();
    for( int i = 0; i < nsp; ++i )
      s += " ";
  }
  push_back( s );
  return size();
}

// presumably this is a UFStrings already in FITS header format:
int UFFITSheader::add( UFStrings* ufs ) {
  if( ufs == 0 )
    return size();

  string s;
  for( int i = 0; i < ufs->numVals(); ++i ) {
    s = (*ufs)[i];
    if( !s.empty() )
      add(s);
  }
  return size();
}

// this method terminates the FITS header with "END" record:
int UFFITSheader::end() {
  int nc = ::sprintf( _FITSrecord, "END%-77s", " " );
  if( nc < 0 )
    clog<<"UFFITSheader::end> truncation required..."<<endl;

  push_back( _FITSrecord );
  return unique();
  //return size();
}

// insure that each entry is unique and in order:
int UFFITSheader::unique() {
  int cnt = size();
  if( cnt <= 1 )
    return cnt;

  // for now just insure that SIMPLE appears only once (at the front):
  string f1 = (*this)[0];
  string f2 = (*this)[1];
  string key1 = f1.substr(0, 8);
  string key2 = f2.substr(0, 8);
  if( key1 == key2 )
     pop_front(); // should erase f1
  
  // erase of an element at "it" invaliditaes "it"?
  // see if this works:
  /* nope!
  deque< string >::iterator it = begin();
  for( int i = 0; i < cnt-1; ++i, ++it ) { // just compare adjacent strings
    string f1 = (*this)[i];
    string f2 = (*this)[i+1];
    string key1 = f1.substr(0, 8);
    string key2 = f1.substr(0, 8);
    if( key1 == key2 )
      erase(it); // should erase f1
  }
  */
  /* this is commented for now, because it never returns?
  map< string, int > order;
  map< string, string > uniq;
  int ucnt= 0;
  for( int i = 0; i < cnt; ++i ) {
    string f = (*this)[i];
    string key = f.substr(0, 8);
    if( uniq.find(key) != uniq.end() ) {
      uniq[key] = f;
      order[key] = ucnt++;
    }
  }
  resize(ucnt);
  map< string, string >::iterator it = uniq.begin();
  while( it != uniq.end() ) {
    string key = it->first;
    string val = it->second;
    ++it;
    int i = order[key];
    (*this)[i] = val;
  }
  */
  return size();
}   

int UFFITSheader::add( map< string, string >& valhash, map< string, string >& comments ) {
  if( valhash.empty() )
    return size();

  string key, val, comment;
  map< string, string >::iterator i = valhash.begin();
  do {
    key = i->first;
    val = i->second;
    if( comments.find(key) != comments.end() ) 
      comment = comments[key];
    else
      comment = "no comment";
    add(key, val, comment);
  } while( ++i != valhash.end() );

  return size();
}

int UFFITSheader::add( map< string, int >& valhash, map< string, string >& comments ) {
  if( valhash.empty() )
    return size();

  string key, comment;
  int val;
  map< string, int >::iterator i = valhash.begin();
  do {
    key = i->first;
    val = i->second;
    if( comments.find(key) != comments.end() ) 
      comment = comments[key];
    else
      comment = "no comment";
    add(key, val, comment);
  } while( ++i != valhash.end() );

  return size();
}

int UFFITSheader::add( map< string, double >& valhash, map< string, string >& comments ) {
  if( valhash.empty() )
    return size();

  string key, comment;
  double val;
  map< string, double >::iterator i = valhash.begin();
  do {
    key = i->first;
    val = i->second;
    if( comments.find(key) != comments.end() ) 
      comment = comments[key];
    else
      comment = "no comment";
    add(key, val, comment);
  } while( ++i != valhash.end() );

  return size();
}

UFStrings* UFFITSheader::asStrings(map< string, string >& valhash, map< string, string >& comments ) {
  UFFITSheader _fitshdr;
  int sz = _fitshdr.add(valhash, comments);
  if( sz <= 0 )
    return 0;

  return _fitshdr.Strings();
}

UFStrings* UFFITSheader::asStrings(map< string, int >& valhash, map< string, string >& comments ) {
  UFFITSheader _fitshdr;
  int sz = _fitshdr.add(valhash, comments);
  if( sz <= 0 )
    return 0;

  return _fitshdr.Strings();
}

UFStrings* UFFITSheader::asStrings(map< string, double >& valhash, map< string, string >& comments ) {
  UFFITSheader _fitshdr;
  int sz = _fitshdr.add(valhash, comments);
  if( sz <= 0 )
    return 0;

  return _fitshdr.Strings();
}

UFStrings* UFFITSheader::fetchFITS(const string& clientname, UFSocket& soc) {
  if( !soc.validConnection() )
    return 0;

  // formulate standard agent client key, value request:
  vector< string > vs;
  vs.push_back("STATUS"); vs.push_back("FITS");
  UFStrings req(clientname, vs);
  req.sendTo(soc);
  UFStrings* reply = dynamic_cast< UFStrings* > (UFProtocol::createFrom(soc));
  return reply;
}

int UFFITSheader::fitsInt(const string& key, char* fitsHdrEntry) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdrEntry, 80);
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

UFStrings* UFFITSheader::readPrmHdr(FILE* fs, int& total) {
  int w, h, nods, nodsets;
  return readPrmHdr(fs, w, h, total, nods, nodsets);
}

UFStrings* UFFITSheader::readPrmHdr(FILE* fs, int& total, int& nods) {
  int w, h, nodsets;
  return readPrmHdr(fs, w, h, total, nods, nodsets);
}

UFStrings* UFFITSheader::readPrmHdr(FILE* fs, int& total, int& nods, int& nodsets) {
  int w, h;
  return readPrmHdr(fs, w, h, total, nods, nodsets);
}

UFStrings* UFFITSheader::readPrmHdr(FILE* fs, int& w, int& h, int& total, int& nnods, int& nnodsets) {
  w = h = total = nnods = nnodsets = 0;
  char entry[81]; ::memset(entry, 0, sizeof(entry));
  char end[4]; ::memset(end, 0, sizeof(end)); 
  strcpy(end, "END");
  int ne = strlen(end);
  char naxis[7]; ::memset(naxis, 0, sizeof(naxis));
  int na = strlen("NAXIS0");
  char nods[6]; ::memset(nods, 0, sizeof(nods));
  int nn= strlen("NNODS");
  char nodsets[9]; ::memset(nodsets, 0, sizeof(nodsets));
  int ns= strlen("NNODSETS");
  string s, key, endkey;
  int fits_sz= 0, fitsunit = 80*36; // check for integral multiple of 2880
  int maxheadsz = 10*fitsunit; // if no END in sight...
  vector< string > vs;
  do {
    if( ::fread(entry, 80, 1, fs ) != 1 ) {
      clog<<"UFFITSheader::readPrmFitsHdr> fread of primary header failed."<<endl;
      if( ::feof(fs) )
        clog<<"UFFITSheader::readPrmFitsHdr> end of file."<<endl;
      return 0;
    }
    fits_sz += 80;
    s = entry; 
    vs.push_back(s);
    if( _verbose )
      clog<<"UFFITSheader::readPrmFitsHdr> cnt: "<<vs.size()<<", entry: " <<s<<endl;
    ::strncpy(naxis, entry, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, entry);
    if( key == "NAXIS2" )
      h = fitsInt(key, entry);

    // nod info:
    ::strncpy(nodsets, entry, ns);
    key = nodsets;
    if( key == "NNODSETS" ) {
      nnodsets = fitsInt(key, entry);
    }
    else {
      ::strncpy(nods, entry, nn);
      key = nods;
      if( key == "NNODS" )
        nnods = fitsInt(key, entry);
    }

    // end:
    ::strncpy(end, entry, ne); // just END chars
    endkey = end;
  } while( endkey != "END" && fits_sz < maxheadsz );

  total = nnods * nnodsets;

  if( _verbose )
    clog<<"UFFITSheader::readPrmFitsHdr> char cnt: "<<fits_sz<<", total extensions/frames expected: "<<total<<endl;

  if( fits_sz >= maxheadsz && endkey != "END" ) {
    clog<<"UFFITSheader::nreadPrmFitsHdr> No 'END' entry, improper or no FITS header? fits_sz= "
        <<fits_sz<<", endkey= "<<endkey<<endl;
    return 0;
  }

  // before returning header, may need to seek to end of current 2880 block:
  // number of bytes to pad out to integral 2880:
  double foo = 1.0*fits_sz/fitsunit;
  double floo = ::floor(foo);
  double padfrac = 1.0 - (foo - floo);
  int pad = (int) ::ceil(padfrac * fitsunit);
  if( _verbose )
    clog<<"UFFITSheader::readPrmFitsHdr> pad cnt: "<<pad<<", total header sz (+pad): "<<pad+fits_sz<<endl;
  ::fseek(fs, pad, SEEK_CUR);
  return new UFStrings("PrimaryHeader", vs);
}

UFInts* UFFITSheader::readExtData(FILE* fs, UFStrings*& hdr, int& w, int& h) {
  int chops= 0, savesets= 0;
  return readExtData(fs, hdr, w, h, chops, savesets);
}

UFInts* UFFITSheader::readExtData(FILE* fs, UFStrings*& hdr,
				  int& w, int& h, int& chops, int& savesets) {
  w = h = 0; savesets = chops= 1;
  char entry[81]; ::memset(entry, 0, sizeof(entry));
  char end[4]; ::memset(end, 0, sizeof(end));
  strcpy(end, "END");
  int ne = strlen(end);
  char naxis[7]; ::memset(naxis, 0, sizeof(naxis));
  int na = strlen("NAXIS0");
  string s, key, endkey;
  int fits_sz= 0, fitsunit = 80*36; // check for integral multiple of 2880
  int maxexthdsz = 4 * fitsunit; // allow 4x2880 (4 block extension headers)?
  vector< string > vs;
  do {
    if( ::fread(entry, 80, 1, fs ) != 1 ) {
      clog<<"UFFITSheader::readFitsExt> fread of extension header failed."<<endl;
      if( ::feof(fs) )
        clog<<"UFFITSheader::readFitsExt> end of file."<<endl;
      return 0;
    }
    fits_sz += 80;
    s = entry;
    if( entry[0] == ' ' && entry[8] == ' ' )
      clog<<"UFFITSheader::readExtData> blank line, char cnt: "<<fits_sz<<endl;
    else if( _verbose ) 
      clog<<"UFFITSheader::readExtData> char cnt: "<<fits_sz<<" -- "<<s<<endl;
    vs.push_back(s);
    ::strncpy(naxis, entry, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, entry);
    if( key == "NAXIS2" )
      h = fitsInt(key, entry);
    if( key == "NAXIS3" )
      chops = fitsInt(key, entry);
    if( key == "NAXIS4" )
      savesets = fitsInt(key, entry);
    ::strncpy(end, entry, ne); // just END chars
    endkey = end;
  } while( endkey != "END" && fits_sz < maxexthdsz );

  if( _verbose )
    clog<<"UFFITSheader::readFitsExt> char cnt: "<<fits_sz<<endl;

  if( fits_sz >=  maxexthdsz && endkey != "END" ) {
    clog<<"UFFITSheader::readFitsExt> No 'END' entry, improper or no FITS header? fits_sz= "
        <<fits_sz<<", endkey= "<<endkey<<endl;
    return 0;
  }

  // before returning ext. header & data, may need to seek to end of current 2880 block:
  // number of bytes to pad out to integral 2880:
  double foo = 1.0*fits_sz/fitsunit;
  double floo = ::floor(foo);
  double padfrac = 1.0 - (foo - floo);
  int pad = (int) ::ceil(padfrac * fitsunit);
  ::fseek(fs, pad, SEEK_CUR);

  int nvals = w*h*chops*savesets;
  UFInts* ufi = new UFInts(string("Frame"), nvals);
  int nvalrd= ::fread((char*)ufi->valData(), sizeof(int), nvals, fs);
  if( nvalrd != nvals ) {
    clog<<"UFFITSheader::readFitsExt> fread of image frame failed, nvalrd: "<<nvalrd<<", expected: "<<nvals<<endl;
    delete ufi;
    return 0;
  }

  fits_sz = nvalrd * sizeof(int); 
  foo = 1.0*fits_sz/fitsunit;
  floo = ::floor(foo);
  padfrac = 1.0 - (foo - floo);
  if( padfrac < 0.999999 ) { // no need to pad if this is 1.0
    pad = (int) ::ceil(padfrac * fitsunit);
    ::fseek(fs, SEEK_CUR, pad);
  }
  hdr = new UFStrings(string("ExtHeader"), vs);
  return ufi; // to be deleted by caller
}

#endif // __UFFITSheader_cc__
