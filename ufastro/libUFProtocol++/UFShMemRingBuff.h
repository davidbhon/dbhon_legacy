#if !defined(__UFShMemRingBuff_h__)
#define __UFShMemRingBuff_h__ "$Name:  $ $Id: UFShMemRingBuff.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFShMemRingBuff_H__(arg) const char arg##Socket_h__rcsId[] = __UFShMemRingBuff_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx
#include "UFRingBuff.h"
#include "UFPosixRuntime.h"

#include "string"

using std::string ;

class UFShMemRingBuff : public UFRingBuff {
protected:
  // this gets set only once and each process has an (identical) copy
  // ditto for the active index semaphore
  static string _activeIndexSemName;
  // this is useful? within the context of an individual process
  // it is not shared, so its use is only for convenience

  
  bool _locked(const string& name = _activeIndexSemName);
  // keep the active buff index in shared memory
  // each process should have this point to the shared value
  // and must access it through the above semaphore
  int* _theActiveIndex;

  
  int _getActiveIndex(bool& isLocked);

  
  int _setActiveIndex(int);

  // init helper

  
  UFProtocol* createShared(string& name, int bufsize= 1, int type= UFProtocol::_Ints_);

  // also keep a flag associated with the active index in shared memory
  // when active buff is being written, this flag should be set to
  // the pid of the process performing the write, otherwise it should
  // be clear (indicating it is safe to read the active/newest dma buff)
  pid_t* _theFlag;

public:
  static vector< string > _bufShmNames;

  
  inline UFShMemRingBuff(const string& name) : UFRingBuff(name), _theActiveIndex(0), _theFlag(0) {}

  
  inline virtual ~UFShMemRingBuff() {}


  
  inline string activeShmName() const { return _bufShmNames[_activeIdx]; }

  
  void sanityCheck(UFProtocol* p);


  
  virtual int init(vector< string >& names, int bufsize, int type= UFProtocol::_Ints_);

  
  virtual int attach(vector< string >& names, int bufsize, bool reset = false);

  // override these inherited virtuals:

  
  virtual UFProtocol* lockActiveBuf(int& index);

  
  virtual int unlockActiveBuf();

  
  virtual int activeIndex(bool& locked);

  
  virtual int nextActiveBuf();

  
  virtual UFProtocol* waitForNewData(int& index);
  // r/w active buf & r/w buf[index]

  
  virtual int write(const UFProtocol* data, int index=-1);
  // note that write functions set _activeIndex, reads do not.

  
  virtual int read(UFProtocol* data, int index=-1);
};

#endif // __UFShMemRingBuff_h__
  /**
   *
   *@param name TBD
   *@return TBD
   */
  /**
   *
   *@param isLocked TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param name TBD
   *@param bufsize TBD
   *@param type TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   */
  /**
   *
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param p TBD
   */
  /**
   *
   *@param names TBD
   *@param bufsize TBD
   *@param type TBD
   *@return TBD
   */
  /**
   *
   *@param names TBD
   *@param bufsize TBD
   *@param reset TBD
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param locked TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */
