#if !defined(__UFServerSocket_cc__)
#define __UFServerSocket_cc__ "$Name:  $ $Id: UFServerSocket.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFServerSocket_cc__;

#include "UFServerSocket.h"
__UFServerSocket_H__(__UFServerSocket_cc);

#include "UFRuntime.h"
__UFRuntime_H__(__UFServerSocket_cc);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif

#include "new"
#include "cstdio"
#include "cstring"
#include "cerrno"
#include "cstdlib"
#include "unistd.h"
#include "netdb.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "sys/uio.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/socket.h"
#include "arpa/inet.h"
#include "netinet/in.h" // IP_

#if defined(LINUX)
#include "netinet/tcp.h" // TCP_
#include "asm/ioctls.h" // FIONREAD
#endif
#if defined (SOLARIS)
#include "xti_inet.h" // TCP_
#endif

// c++ streams
#include "iostream"
#include "fstream"
#include "strstream"


UFServerSocket::UFServerSocket(int port) :  UFSocket(port), _host("none"),
       				            _ipAddr("none"), _ephem(false)  {}

UFServerSocket::UFServerSocket(const UFServerSocket& rhs) : UFSocket((const UFSocket&) rhs),
							    _host(rhs._host),
							    _ipAddr(rhs._ipAddr), 
							    _ephem(false) {}

// if bound with ephemeral port number, provide the kernel allocated port:
bool UFServerSocket::ephemPort(int& port) {
  int s = _sockinfo.listenFd;
  if( s <= 0 || !_ephem ) 
    return false;
  port = _portNo;
  return true;
}

// funcs to listen & accept connections
// this is clearly not reentrant (mthread-safe)!
UFSocketInfo UFServerSocket::listen(int port, int backlog) {
  if( _portNo > 0 && _sockinfo.listenFd > 0 && _sockinfo.addr != 0 ) {
    // already initialized for listening/accepts?
    if( port < 0 || port == _portNo ) // definitely don't need to re-init...
      return _sockinfo;
  }

  // start by putting server name here
  //if( _host == "none" ) {
    _host = UFRuntime::hostname();
  //}

  if( port >= 0 )
    _portNo = port;

  if( _portNo < 0 ) {
    clog<<"UFServerSocket::listen> ? bad portNo= " <<_portNo<<endl;
    return _sockinfo;
  }
  else if( _portNo == 0 ) {
    clog<<"UFServerSocket::listen> req. bind to ephemeral portNo= " <<_portNo<<endl;
    _ephem = true;
  }
//#ifdef LINUX
  socklen_t len;
//#else
//  int len ;
//#endif
  len = sizeof(sockaddr_in); 

  _sockinfo.listenFd = ::socket(AF_INET, SOCK_STREAM, 0);
  if( _sockinfo.listenFd < 0 ) {
    cerr << "UFSocketServer::listen> unable to open stream socket: " 
	<< strerror( errno ) << endl ;
  }
  struct linger setLinger;
  setLinger.l_onoff = 0; // don't linger after close!
  setLinger.l_linger = 0;
  ::setsockopt(_sockinfo.listenFd, SOL_SOCKET, SO_LINGER,
	       reinterpret_cast< const char * >( &setLinger ),
	       sizeof(setLinger));

  // should allow painless re-binds to the socket:
  int bindFlag = 1;
  ::setsockopt(_sockinfo.listenFd, SOL_SOCKET, SO_REUSEADDR,
	       reinterpret_cast< const char* >( &bindFlag ),
	       sizeof(int));
 
  _sockinfo.addr = new (nothrow) sockaddr_in;
  if( NULL == _sockinfo.addr ) {
    clog << "UFServerSocket::listen> Memory allocation failure\n";
    errno = ENOMEM;
    return UFSocketInfo();
  }

  ::memset(reinterpret_cast< char * >( _sockinfo.addr ), 0, len);
  _sockinfo.addr->sin_family = AF_INET;
  _sockinfo.addr->sin_port = htons(_portNo);
  _sockinfo.addr->sin_addr.s_addr = htonl(INADDR_ANY);
 
  struct sockaddr *socaddr = reinterpret_cast< struct sockaddr * >( _sockinfo.addr ); 
  int stat = ::bind(_sockinfo.listenFd, socaddr, len);
  if( stat < 0 ) {
    clog << "UFServerSocket::listen> unable to bind, abort: "
	<< strerror( errno ) << endl ;
    exit(1);
  }
  if( _ephem ) {
    _portNo = ntohs(_sockinfo.addr->sin_port);
    clog<<"UFServerSocket::listen> bound to ephemeral portNo= " <<_portNo<<endl;
    stat = ::getsockname(_sockinfo.fd, socaddr, &len);
    _portNo = ntohs(_sockinfo.addr->sin_port);
    clog<<"UFServerSocket::listen> bound to ephemeral portNo= " <<_portNo<<endl;
  }
  stat = ::listen(_sockinfo.listenFd, backlog); // listen for the client to connect
  if( stat < 0 ) {
    clog << "UFServerSocket::listen> unable to listen, abort: "
	<< strerror( errno ) << endl ;
    exit(1);
  }
  if( _sockinfo.listenFd > _sockinfo.maxFd ) 
    _sockinfo.maxFd = _sockinfo.listenFd; 

  return _sockinfo;
} // listen

// helper wrapper to select readfs for pending client connection
bool UFServerSocket::pendingConnection(float timeOut) {
  if( _sockinfo.listenFd < 0 ) {
    clog << "UFSocketInfo::selectable> bad socFd: "<<_sockinfo.listenFd << endl;
    return false;
  }
  if ( !isSock(_sockinfo.listenFd) ) {
    clog << "UFSocketInfo::valid> not a socket Fd: "<<_sockinfo.listenFd << endl;
    return false;
  }
  if( UFSocket::_verbose )
    clog<<"UFSocketInfo::valid> check UFRuntime::available on socFd: "<<_sockinfo.listenFd <<endl;

  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(_sockinfo.listenFd, &readfds);

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  int fdcnt= -1, trycnt= 3; //UFPosixRuntime::_MaxInterrupts_;
  if( UFSocket::_verbose )
    clog<<"UFSocketInfo::valid> perform select rd on all fd <= listenFd: "<<_sockinfo.listenFd<<endl;
  do {
    // since select may modify this area of memory, always provide fresh input:
    struct timeval to, *pto= 0;
    if( usetimeout ) { to = *usetimeout; pto = &to; }
    fdcnt = ::select(1+_sockinfo.listenFd, &readfds, 0, 0, pto);
    //fdcnt = select(FD_SETSIZE, &readfds, &writefds, &excptfds, &timeout);
  } while( (errno == 0 || errno == EINTR) && --trycnt >= 0 );

  if( FD_ISSET(_sockinfo.listenFd, &readfds) )
    return true;

  return false;
}

// this will block
int UFServerSocket::accept(UFSocket& client) {
  if( _sockinfo.listenFd < 0 || _sockinfo.addr == 0 ) {
    clog << "UFSocketServer::accept> listen socket not initialized?" << endl;
    return -1;
  }
//#ifdef LINUX
  socklen_t len;
//
//#else
//  int len ;
//#endif
//
  len = sizeof(sockaddr_in); 
  do {
    _sockinfo.fd = ::accept(_sockinfo.listenFd,
			    reinterpret_cast< struct sockaddr * >( _sockinfo.addr ),
			    &len);
    if( _sockinfo.fd <= 0 ) UFPosixRuntime::sleep(0.1); // UFPosixRuntime::yield();
  } while( _sockinfo.fd <= 0 && errno == EINTR );

  //::close(_sockinfo.listenFd); // now accept more than one connection
 
  if( _sockinfo.fd < 0 ) {
    clog << "UFSocketServer::accept> unable to accept connection? "<<strerror(errno)<<endl;
    return -1;
  }
  if( _sockinfo.fd > _sockinfo.maxFd ) 
    _sockinfo.maxFd = _sockinfo.fd; 

  client = UFSocket(_sockinfo, _portNo); // copy ctor may not work?
  return _sockinfo.fd;
} // accept

// this will block
int UFServerSocket::accept(UFSocket*& client) {
  client = 0;
  if( _sockinfo.listenFd < 0 || _sockinfo.addr == 0 ) {
    clog << "UFSocketServer::accept> listen socket not initialized?" << endl;
    return -1;
  }

  //int trycnt= 3;
  socklen_t len = sizeof(sockaddr_in);
  UFSocketInfo::setBlocking(_sockinfo.listenFd, true); // block, dont' check errno for EAGAIN
  do {
    _sockinfo.fd = ::accept(_sockinfo.listenFd,
			    reinterpret_cast< struct sockaddr * >( _sockinfo.addr ),
			    &len);
    if( _sockinfo.fd <= 0 && errno == EAGAIN ) UFPosixRuntime::sleep(0.01); // UFPosixRuntime::yield();
  } while( _sockinfo.fd <= 0 && (errno == EINTR) ); // || errno == EAGAIN) );

 
  if( _sockinfo.fd < 0 ) {
    clog << "UFSocketServer::accept> unable to accept connection? "<<strerror(errno)<<endl;
    return -2;
  }
  if( _sockinfo.fd > _sockinfo.maxFd ) 
    _sockinfo.maxFd = _sockinfo.fd; 

  client = new (nothrow) UFSocket(_sockinfo, _portNo);
  return _sockinfo.fd;
} // accept


// this will not block -- only call accept if client is connecting:
int UFServerSocket::acceptClient(UFSocket& client, float timeOut) {
  // select on the listener socket, not the i/o socket!
  //if( pendingIO(timeOut, _sockinfo.listenFd) > 0 )
  //  return accept(client);
  if( _sockinfo.listenFd < 0 || _sockinfo.addr == 0 ) {
    clog << "UFSocketServer::accept> listen socket not initialized?" << endl;
    return -1;
  }
  bool clientpending = pendingConnection(timeOut);
  if( !clientpending ) {
    return 0;
  }
  //clog << "UFSocketServer::accept> check listen socket readable/selectable?" << endl;
  //if( readable(timeOut, _sockinfo.listenFd) <= 0 )
  //  return -1;
  int trycnt= 3;
  _sockinfo.fd = -1;
  socklen_t len = sizeof(sockaddr_in);
  //UFSocketInfo::setBlocking(_sockinfo.listenFd, false); // don't block, check errno for EAGAIN
  do {
    //clog << "UFSocketServer::accept> check listen socket readable/selectable?" << endl;
    _sockinfo.fd = ::accept(_sockinfo.listenFd,
			    reinterpret_cast< struct sockaddr * >( _sockinfo.addr ),
			    &len);
    //if( _sockinfo.fd < 0 && errno == EAGAIN ) UFPosixRuntime::sleep(timeOut/3); // UFPosixRuntime::yield();
  } while( _sockinfo.fd < 0 && --trycnt > 0 && (errno == EINTR) ); // || errno == EAGAIN) );
 
  if( _sockinfo.fd < 0 ) {
    //clog << "UFSocketServer::accept> accept connection failed? "<<strerror(errno)<<endl;
    return -2;
  }
  if( _sockinfo.fd > _sockinfo.maxFd ) 
    _sockinfo.maxFd = _sockinfo.fd; 

  client = UFSocket(_sockinfo, _portNo); // will default assignment operator/ copy ctor work?
  return _sockinfo.fd;

}

// different signature: allocates client soc
int UFServerSocket::acceptClient(UFSocket*& client, float timeOut) {
  client = 0;
  if( _sockinfo.listenFd < 0 || _sockinfo.addr == 0 ) {
    clog << "UFSocketServer::accept> listen socket not initialized?" << endl;
    client = 0;
    return -1;
  }
  //clog << "UFSocketServer::accept> check listen socket readable/selectable?" << endl;
  //if( readable(timeOut, _sockinfo.listenFd) <= 0 )
  //  return -1;
  bool clientpending = pendingConnection(timeOut);
  if( !clientpending ) {
    client = 0;
    return 0;
  }

  int trycnt= 3;
  _sockinfo.fd = -1;
  socklen_t len = sizeof(sockaddr_in);
  //UFSocketInfo::setBlocking(_sockinfo.listenFd, false); // don't block, check errno for EAGAIN
  do {
    //clog << "UFSocketServer::accept> Ok listen socket readable/selectable, proceeding with accept" << endl;
    _sockinfo.fd = ::accept(_sockinfo.listenFd,
			    reinterpret_cast< struct sockaddr * >( _sockinfo.addr ),
			    &len);
    //if( _sockinfo.fd <= 0 && errno == EAGAIN ) UFPosixRuntime::sleep(timeOut/3); // UFPosixRuntime::yield();
  } while( _sockinfo.fd <= 0 && --trycnt > 0 && (errno == EINTR) ); // || errno == EAGAIN) );
 
  if( _sockinfo.fd < 0 ) {
    //clog << "UFSocketServer::accept> unable to accept connection? "<<strerror(errno)<<endl;
    return -2;
  }
  if( _sockinfo.fd > _sockinfo.maxFd ) 
    _sockinfo.maxFd = _sockinfo.fd; 

  client = new (nothrow) UFSocket(_sockinfo, _portNo);
  return _sockinfo.fd;
}

// new signature uses new copy ctor to return UFSocket by value! may not work...
UFSocket UFServerSocket::listenAndAccept(int port) {
  bool connected = false;
  UFSocket retval;
  do {
    UFSocketInfo sockinfo = listen(port); // should set _sockinfo == sockinfo
    int fd = accept(retval);
    if( fd >= 0 && _sockinfo.validConnection() ) {
      connected = true;
    }
    else {
      clog << "UFSocketServer::listenAndAccept> invalid connection? "
	   << "close _sockinfo..." <<endl;
      _sockinfo.close();
    }
  } while( !connected );

  return retval;
} // listenAndAccept


#endif // __UFServerSocket_cc__
