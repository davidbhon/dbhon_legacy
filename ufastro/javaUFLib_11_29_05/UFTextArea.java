package javaUFLib;

//Title:        UFTextArea: extension of JTextArea with built in auto-limit of memory usage.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2005
//Authors:      Frank Varosi
//Company:      University of Florida
//Description:  If document size exceeds limit then cut in half, also scroll to bottom.

import javax.swing.*;

//===============================================================================
/**
 * Extends the JTextField class to add some simple useful options,
 * mainly, to guard against running out of memory by imposing limit on document size,
 * and also to automatically scroll to bottom of area after appending new text.
 */

public class UFTextArea extends JTextArea
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextArea.java,v 1.2 2005/02/28 23:02:50 varosi Exp $";

    protected final int MB = 1024*1024;
    protected int maxDocLength = MB, NmsgDisp=0;
    protected boolean scrollToBottom = true;
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     */
    public UFTextArea(int nMegaBytes) {
	if( nMegaBytes < 1 ) nMegaBytes = 1;
	this.maxDocLength = nMegaBytes*this.MB;
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     */
    public UFTextArea(int nMegaBytes, int rows, int cols) {
	super(rows, cols);
	if( nMegaBytes < 1 ) nMegaBytes = 1;
	this.maxDocLength = nMegaBytes*this.MB;
    }
//-------------------------------------------------------------------------------

    public void setMaxDocLength(int nMB) {
	if( nMB < 1 ) nMB = 1;
	this.maxDocLength = nMB*this.MB;
    }

    public void autoScrollToBottom(boolean b) { this.scrollToBottom = b; }

    public int NmsgDisp() { return NmsgDisp; }

//-------------------------------------------------------------------------------
    /**
     * Override append method to check document length and limit it,
     * and optionall scroll to bottom of area.
     */
    public void append(String newText)
    {
	if( this.getDocument().getLength() > this.maxDocLength ) {
	    //cut text area contents in half to stay within limit:
	    String oldText = this.getText();
	    this.setText( oldText.substring( oldText.length()/2 ) );
	    NmsgDisp /= 2;
	    oldText = null;
	}

	super.append( newText );
	++NmsgDisp;
	if( scrollToBottom ) this.setCaretPosition( this.getDocument().getLength() );
    }
} //end of class UFTextArea
