package javaUFLib;

//Title:        UFTextField extension of JTextField with history log of messages (errors, status, etc.).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003-5
//Authors:      Frank Varosi and Craig Warner
//Company:      University of Florida
//Description:  Keeps track of state with color, uses UFMessageLog class for history of messages.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Vector;

//===============================================================================
/**
 * Extends the JTextField class to add some simple useful options,
 * like changing color depending on previous/current state.
 */

public class UFTextField extends JTextField implements FocusListener, KeyListener
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextField.java,v 1.35 2005/08/11 18:21:06 drashkin Exp $";

    protected String newText = "", prevText = "";
    protected String Name, Label="", Units="";
    protected Vector _alarmItems = new Vector();

    protected boolean commitOnEnter = false;
    protected boolean showInToolTip = false;

    protected JFrame _LogFrame;
    protected int _Hsize, _Vsize;
    public JPopupMenu popupMenu;
    protected UFMessageLog _Log;

//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFTextField() {
	try {
	    createComponent("TextField");
	}
	catch(Exception ex) {
	    System.out.println("Error creating a UFTextField: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextField(String description) {
	try {
	    createComponent(description);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     */
    public UFTextField( String description, String text ) {
	try {
	    newText = text;
	    prevText = text;
	    createComponent( description );
	    this.setText( text );
	    _Log.addMessage(text);
	    _Log.appendLastMessage();
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor: special case with specified # of columns and no focuslistener.
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     *@param Ncolumns    int: # of columns to set for field
     */
    public UFTextField( String description, String text, int Ncolumns ) {
	try {
	    if( Ncolumns > 0 ) this.setColumns( Ncolumns );
	    newText = text;
	    prevText = text;
	    createComponent( description );
	    this.setText( text );
	    _Log.addMessage(text);
	    _Log.appendLastMessage();
	    removeFocusListener(this);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param editable    boolean: state of editing
     */
    public UFTextField( String description, boolean editable ) {
	try {
	    createComponent(description);
	    this.setEditable( editable );
	    if( !editable ) removeFocusListener(this);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param editable    boolean: state of editing
     *@param Ncolumns    int: # of columns to set for field
     */
    public UFTextField( String description, boolean editable, int Ncolumns ) {
	try {
	    if( Ncolumns > 0 ) this.setColumns( Ncolumns );
	    createComponent(description);
	    this.setEditable( editable );
	    if( !editable ) removeFocusListener(this);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void createComponent( String description ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	this.Label = description;
	addFocusListener(this);
	addKeyListener(this);
	setToolTipText();

	this._Log = new UFMessageLog(1000);
        popupMenu = new JPopupMenu();
        JMenuItem viewLogSmall = new JMenuItem("View Log (small)");
        JMenuItem viewLogLarge = new JMenuItem("View Log (bigger)");
        JMenuItem viewLogHuge = new JMenuItem("View Log (full size)");
        popupMenu.add(viewLogSmall);
        popupMenu.add(viewLogLarge);
        popupMenu.add(viewLogHuge);

        viewLogSmall.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { viewLogAction(600,200); } });

        viewLogLarge.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { viewLogAction(650,500); } });

        viewLogHuge.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { viewLogAction(700,900); } });

        this.addMouseListener(new java.awt.event.MouseListener() {

            public void mousePressed(java.awt.event.MouseEvent evt) {
                if(( evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
                    if(evt.isPopupTrigger()) {
                        popupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
                    }
                }
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {}
            public void mouseReleased(java.awt.event.MouseEvent evt) {}
            public void mouseEntered(java.awt.event.MouseEvent evt) {}
            public void mouseExited(java.awt.event.MouseEvent evt) {}
        });
    }
//-------------------------------------------------------------------------------

    private void viewLogAction(int hsize, int vsize)
    {
	if (_LogFrame != null) {
	    if( _Hsize == hsize && _Vsize == vsize ) {
		_LogFrame.show();
		_LogFrame.setState( Frame.NORMAL );
		return;
	    }
	    else _LogFrame.dispose();
	}

	_Hsize = hsize;
	_Vsize = vsize;
	JPanel logPanel = new JPanel();
	JScrollPane logScroll = new JScrollPane(_Log);
	logScroll.setPreferredSize(new Dimension(hsize,vsize));
	logPanel.add( logScroll );

	JPanel controlPanel = new JPanel();
	JButton closeButton = new JButton("Close");
	controlPanel.add(closeButton);
	closeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _LogFrame.dispose();
		}
	    });
	final JButton refresh = new JButton("Clear Dups");
	controlPanel.add(refresh);
	refresh.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _Log.showAllMessages();
		}
	    });
	final JCheckBox AutoScroll = new JCheckBox("AutoScroll ",true); 
	controlPanel.add(AutoScroll);
	AutoScroll.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _Log.autoScrollToBottom(AutoScroll.isSelected());
		}
	    });
	final JCheckBox timestamps = new JCheckBox("Timestamps ",true); 
	controlPanel.add(timestamps);
	timestamps.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _Log.setTimestamps(timestamps.isSelected());
		    _Log.showAllMessages();
		}
	    });
	final JCheckBox duplicates = new JCheckBox("Duplicates ",false); 
	controlPanel.add(duplicates);
	duplicates.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _Log.addDuplicates(duplicates.isSelected());
		    _Log.showDuplicates(duplicates.isSelected());
		}
	    });
	final String[] nSave = {"1000","2000","4000","8000"};
	final JComboBox maxSave = new JComboBox(nSave);
	controlPanel.add(new JLabel("Max Save="));
	controlPanel.add(maxSave);
	maxSave.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _Log.setMaxLogSize( Integer.parseInt( (String)maxSave.getSelectedItem() ) );
		}
	    });

	_LogFrame = new JFrame("Log: " + Label);
	_LogFrame.getContentPane().setLayout(new BorderLayout()); 
	_LogFrame.getContentPane().add( logPanel, BorderLayout.NORTH );
	_LogFrame.getContentPane().add( controlPanel, BorderLayout.SOUTH );
	_LogFrame.pack();
	_LogFrame.setVisible(true);
	_Log.showAllMessages();
    }
//-------------------------------------------------------------------------------
    /**
     *UFTextField#setAlarmValue
     *@param alarmVal     String: text value for which orange background color should be indicated.
     */
    public void setAlarmValue( String alarmVal ) { if( alarmVal != null ) _alarmItems.add( alarmVal ); }

//-------------------------------------------------------------------------------
    /**
     *UFTextField#setCommitOnEnter
     */
    public void setCommitOnEnter(boolean commit) { commitOnEnter = commit; }
    public void setCommitOnEnter() { commitOnEnter = true; }

//-------------------------------------------------------------------------------
    /**
     * Sets the text for the tool tip -- returns no arguments
     */
    protected void setToolTipText() {
	if( showInToolTip ) 
	    this.setToolTipText( Name + " = " + newText + " " + Units 
				 + " -- Click right mouse button to view log.");
	else
	    this.setToolTipText("Click right mouse button to view log.");
    }

    public void showInToolTip(boolean show) {
	showInToolTip = show;
	setToolTipText();
    }

//-------------------------------------------------------------------------------

    public String name() { return this.Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }
    public String Label() { return Label; }
    public String getNewText() { return newText; }
    public String getPrevText() { return prevText; }

//-------------------------------------------------------------------------------

    public void setNewState()
    {
	boolean alarmSet = false;

	for( int i=0; i < _alarmItems.size(); i++ ) {
	    if( newText.equalsIgnoreCase( (String)_alarmItems.elementAt(i) ) ) {
		alarmSet = true;
		setBackground( Color.orange );
	    }
	}

	prevText = newText;
	setToolTipText();

	if( newText.indexOf("ERR") >= 0 ||
	    newText.indexOf("FAIL") >= 0 || newText.indexOf("Fail") >= 0 )
	    {
		Toolkit.getDefaultToolkit().beep();
		if( !alarmSet ) setBackground( Color.yellow );
	    }
	else if( !alarmSet ) {
	    if( newText.indexOf("WARN") >= 0 )
		setBackground( Color.yellow );
	    else
		setBackground( Color.white );
	}
    }
//-------------------------------------------------------------------------------

    public void setNewText( String newText )
    {
	this.newText = newText.trim();
	setText( this.newText );
	_Log.addMessage( this.newText );
	_Log.appendLastMessage();

	if( this.newText.equals( this.prevText ) )
	    setBackground( Color.white );
	else
	    setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    public void setNewState( String newText )
    {
	this.newText = newText.trim();
	setText( this.newText );
	setNewState();
	_Log.addMessage( newText );
	_Log.appendLastMessage();
    }

    public void setNewState( int newVal ) { setNewState( Integer.toString( newVal ) ); }
    public void setNewState( float newVal ) { setNewState( UFLabel.truncFormat( newVal ) ); }
    public void setNewState( double newVal ) { setNewState( UFLabel.truncFormat( newVal ) ); }

//-------------------------------------------------------------------------------
    /**
     * Event Handling Methods
     */
    public void focusGained(FocusEvent e) 
    { 
	if( prevText.trim().equals("") ) prevText = getText().trim();
    }
//-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	newText = getText().trim();

	if( commitOnEnter ) {
	    prevText = newText;
	    setToolTipText();
	}

	if( newText.equals( prevText ) )
	    setBackground( Color.white );
	else
	    setBackground( Color.yellow );
    }

//-------------------------------------------------------------------------------

    public void keyTyped(KeyEvent ke) { }
    public void keyReleased(KeyEvent ke) { }

//-------------------------------------------------------------------------------

    public void keyPressed(KeyEvent ke)
    {
	setBackground( Color.yellow );

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		newText = getText().trim();
		_Log.addMessage( newText );
		_Log.appendLastMessage();

		if( commitOnEnter ) {
		    prevText = newText;
		    setToolTipText();
		}

		if( newText.equals( prevText ) )
		    setBackground( Color.white );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prevText );
		setBackground( Color.white );
	    }
    }
} //end of class UFTextField
