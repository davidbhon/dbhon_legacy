package javaUFLib;

//Title:        UFTextArea: extension of JTextArea with built in auto-limit of memory usage.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2005
//Authors:      Frank Varosi
//Company:      University of Florida
//Description:  If document size exceeds limit then cut in half, also scroll to bottom.

import javax.swing.*;

//===============================================================================
/**
 * Extends the JTextField class to add some simple useful options,
 * mainly, to guard against running out of memory by imposing limit on document size,
 * and also to automatically scroll to bottom of area after appending new text.
 */

public class UFTextArea extends JTextArea
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextArea.java,v 1.3 2006/01/17 23:21:43 varosi Exp $";

    protected final int _MB = 1024*1024;
    protected int _maxDocLength = _MB, _NmsgDisp=0;
    protected boolean _scrollToBottom = true;
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     */
    public UFTextArea(int nMegaBytes) {
	if( nMegaBytes < 1 ) nMegaBytes = 1;
	this._maxDocLength = nMegaBytes*_MB;
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     */
    public UFTextArea(int nMegaBytes, int rows, int cols) {
	super(rows, cols);
	if( nMegaBytes < 1 ) nMegaBytes = 1;
	this._maxDocLength = nMegaBytes*_MB;
    }
//-------------------------------------------------------------------------------

    public void setMaxDocLength(int nMB) {
	if( nMB < 1 ) nMB = 1;
	this._maxDocLength = nMB*_MB;
    }

    public void autoScrollToBottom(boolean b) { _scrollToBottom = b; }

    public int NmsgDisp() { return _NmsgDisp; }

//-------------------------------------------------------------------------------
    /**
     * Override append method to check document length and limit it,
     * and optionall scroll to bottom of area.
     */
    public void append(String newText)
    {
	if( this.getDocument().getLength() > _maxDocLength ) {
	    //cut text area contents in half to stay within limit:
	    String oldText = this.getText();
	    this.setText( oldText.substring( oldText.length()/2 ) );
	    _NmsgDisp /= 2;
	    oldText = null;
	}

	super.append( newText );
	++_NmsgDisp;
	if( _scrollToBottom ) this.setCaretPosition( this.getDocument().getLength() );
    }
} //end of class UFTextArea
