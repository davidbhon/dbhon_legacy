
#include "string"
#include "vector"

#include "UFProtocol.h"
#include "UFThreadedServ.h"

using std::string ;
using std::vector ;

class UFThreadedTest : public UFThreadedServ
{

public:
  UFThreadedTest( int argc, char** argv ) ;
  virtual ~UFThreadedTest() ;

  virtual void* exec( void* p = 0 ) ;

  virtual vector< UFProtocol* > handleObject( UFProtocol* ) ;


protected:

} ;

UFThreadedTest::UFThreadedTest( int argc, char** argv )
  : UFThreadedServ( argc, argv )
{


}

UFThreadedTest::~UFThreadedTest()
{}

void* UFThreadedTest::exec( void* p )
{
  // Parse command line options etc


  return UFThreadedServ::exec( p ) ;
}

vector< UFProtocol* > UFThreadedTest::handleObject( UFProtocol* request )
{
  vector< UFProtocol* > retMe ;


  return retMe ;
}


int main( int argc, char** argv )
{
  // UFThreadedTest( argc, argv ).exec() ;
  UFThreadedTest serv( argc, argv ) ;
  serv.exec() ;
  return 0 ;
}
