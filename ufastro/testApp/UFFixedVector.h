#if !defined(__UFFixedVector_h__)
#define __UFFixedVector_h__ "$Name:  $ $Id: UFFixedVector.h,v 0.0 2003/01/24 16:26:55 hon Developmental $"
#define __UFFixedVector_H__(arg) const char arg##RingBuff_h__rcsId[] = __UFFixedVector_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx

// c++ incs
#include "iostream"
#include "strstream"
#include "cstdio"
#include "string"
#include "list"

#include "UFProtocol.h"

using namespace std;


class UFFixedVector : public list< UFProtocol* > {
protected:
  string _name;
  size_type maxElementsInList;

public:
  virtual ~UFFixedVector();
  UFFixedVector(const string& name= "Anonymous", size_type size=100);

  void push_front(UFProtocol* obj);

  inline string name() const { 
    return _name;
  }

};

UFFixedVector::~UFFixedVector() {

  for (iterator ptr = begin(); ptr != end(); ptr++) {
    delete *ptr;
  }

}


UFFixedVector::UFFixedVector(const string& name, size_type size) : 
  list< UFProtocol* >(), _name(name), maxElementsInList(size) {

}

// Add object to front of vector
// If we are already at maxElems, remove object in
// last place of vector
void UFFixedVector::push_front(UFProtocol* obj) {

  if (size() == maxElementsInList) {
    delete back();
    pop_back();
  }

  list<UFProtocol*>::push_front(obj);

}


#endif // __UFFixedVector_h__
