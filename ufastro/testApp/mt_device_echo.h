/* mt_device_echo.h */

#ifndef __mt_device_echo_h__
#define __mt_device_echo_h__ "$Name:  $ $Id: mt_device_echo.h,v 0.0 2003/01/24 16:26:55 hon Developmental $"
#define __mt_device_echo_H__(arg) const char arg##mt_device_echo_h__rcsId[] = __mt_device_echo_h__ ;

#include	<string>
#include	<iostream>
#include	<vector>

#include	"UFSocket.h"
__UFSocket_H__(__mt_device_echo_h__) ;

#include	"UFClientSocket.h"
__UFClientSocket_H__(__mt_device_echo_h__) ;

#include	"UFStrings.h"
__UFStrings_H__(__mt_device_echo_h__) ;

using std::string ;
using std::vector ;

// TODO: Add client_id
/**
 * Type used to store information about a currently connected client.
 */
struct client
{
  client( UFSocket& _sock )
    : sock( _sock )
  {}
  virtual ~client()
  { sock.close() ; }

  UFSocket	sock ;

} ;

/**
 * Type used to store information about a message received from or to be
 * sent to a client.
 * TODO: Add jobID (globalSeq).
 */
struct clientMessage
{
  clientMessage( client* _client, UFStrings* _message )
    : theClient( _client ), theMessage( _message )
  {}
  clientMessage( const clientMessage& rhs )
    : theClient( rhs.theClient ), theMessage( rhs.theMessage )
  {}

  // theClient is persistent beyond the existence of any
  // instance of this class.
  virtual ~clientMessage()
  { delete theMessage ; }

  client*		theClient ;
  UFStrings*	theMessage ;

} ;

/**
 * This class holds an individual command to be executed
 * on behalf of a client.  The sequence number is the
 * sequence number of the associated composite command
 * issued by the client.
 */
class clientCommand
{

 public:
  clientCommand( client* _client, const unsigned int& _sequence,
		 const string& _command )
    : theClient( _client ), sequence( _sequence ), command( _command )
    {}
  ~clientCommand() {}

  client* getClient() const
    { return theClient ; }
  const unsigned int& getSequence() const
    { return sequence ; }
  const string& getCommand() const
    { return command ; }

 protected:
  client* theClient ;
  unsigned int sequence ;
  string command ;

} ;

/**
 * Class used to store information about any available command
 * for any particular device.
 */
class commInfo
{
 public:
  typedef unsigned int optionType ;
  static const optionType OPT_WAIT = 0x01 ;
  static const optionType OPT_BAD = 0x02 ; // bad command

  string          key ;
  string          format ;
  string          name ;
  int             numArgs ;
  optionType      options ;

  commInfo() : numArgs( 0 ), options( 0 ) {}

  void parseOptions( const string& ) ;
  bool getOption( const optionType& whichOpt ) const
    { return (options & OPT_WAIT) ; }
  friend std::ostream& operator<<( std::ostream& out, const commInfo& rhs )
    { out	<< "Key: " << rhs.key
		<< ", format: " << rhs.format
		<< ", description: " << rhs.name ;
    return out ;
    }
  friend std::ostream& operator<<( std::ostream& out, const commInfo* rhs )
    { out << *rhs ; return out ; }

} ;

/**
 * Class used to store information about a named position.
 * This is an immutable class.
 */
class namedPosition
{
 public:
  namedPosition( const int& _offset, const string& _name )
    : offset( _offset ), name( _name )
    {}
  namedPosition( const namedPosition& rhs )
    : offset( rhs.offset ), name( rhs.name )
    {}

  inline const int& getOffset() const
    { return offset ; }
  inline const string& getName() const
    { return name ; }

 protected:
  int		offset ;
  string       	name ;

} ;

class Command
{

 public:
  Command( client* _theClient, const string& _commName,
	   const string& _args, const string& _options )
    : theClient( _theClient ),
    commName( _commName ),
    args( _args ),
    options( _options )
    {}


 protected:
  client* theClient ;
  string commName ;
  string args ;
  string options ;

} ;

class UFDevice
{

 public:

  virtual string prepareCommand( const string& ) = 0 ;

 protected:


} ;

void		usage( const string& ) ;
void		sigHandler( int ) ;
void            prepareCommand( client*, const UFStrings* ) ;
void*		annexThread( void* ) ;
void*		servThread( void* ) ;
void*		toClientThread( void* ) ;
void*		fromClientThread( void* ) ;
 
bool		checkCommand( const UFStrings*, vector< string >& ) ;
bool		readDeviceFile( const string& ) ; 
bool		findClient( const client* ) ;
bool            isServerCommand( const string& ) ;

UFStrings*       buildReply( clientCommand*, const string& ) ;

std::ostream& operator<<( std::ostream&, const UFStrings& ) ;

#endif // __mt_device_echo_h__
