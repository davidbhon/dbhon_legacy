/* serialClient.cc
 */

#include	"string"
#include	"cerrno"
#include	"cstring"

#include	"UFSerialPort.h"

using namespace std ;

int main( int argc, const char** argv )
{

if( argc < 2 )
	{
	clog << "Usage: " << argv[ 0 ] << " <port num>\n" ;
	return 0 ;
	}

UFSerialPort* serialPort = UFSerialPort::getInstance() ;
int portNum = atoi( argv[ 1 ] ) ;

if( serialPort->open( portNum ) < 0 )
	{
	clog	<< "Unable to open port " << portNum
		<< ": " << strerror( errno ) << endl ;
	return 0 ;
	}
else
	{
	clog	<< "Port opened successfully\n" ;
	}

bool portOpen = true ;

cout << "\nEnter \"quit\" to end.\n" ;
cout << "All other lines of text will be sent to the serial port\n\n" ;

int lineCount = 1 ;

while( true )
	{	

	cout << "[" << lineCount << "]> " ;

	char buffer[ 1000 ] ;
	memset( buffer, 0, sizeof( buffer ) ) ;

	cin.getline( buffer, 999 ) ;
	string command( buffer ) ;

	if( command == "quit" )
		{
		break ;
		}

	if( command == "receive" )
		{

		string inBuf( 1000, '\0' ) ;

		if( serialPort->recv( inBuf, portNum ) < 0 )
			{
			clog	<< "Read error: " << strerror( errno )
				<< endl ;
			continue ;
			}

		cout << "Read: " << inBuf ;

		}

	if( serialPort->send( command + "\r\n", portNum ) < 0 )
		{
		cout	<< "Write error: " << strerror( errno )
			<< endl ;
		}


	lineCount++ ;

	}

if( 0 == serialPort->close( portNum ) )
	{
	cout << "Closed port successfully\n" ;
	}
else
	{
	cout << "Error closing port: " << strerror( errno ) << endl ;
	}

return 0 ;

}
