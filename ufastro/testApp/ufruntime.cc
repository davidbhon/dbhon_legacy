#include	<string>
#include	<iostream>
#include	<ctime>
#include	<cerrno>

#include	<unistd.h>

#include	<UFRuntime.h>

void	do_child( int ) ;
string	currentTime() ;

string name( "/semtest" ) ;

int main( void )
{

int id = UFRuntime::sem5Delete( name ) ;

if( -1 == id )
	{
	cout << "Delete FAILED\n" ;
	cout << "Error: " << strerror( errno ) << endl ;
	}
else
	{
	cout << "Delete successful\n" ;
	}

id = UFRuntime::sem5Create( name ) ;

if( id < 0 )
	{
	cout << "sem5Create FAILED\n" ;
	cout << "Error: " << strerror( errno ) << endl ;
	return 0 ;
	}

cout << "Created semaphore\n" ;
cout << "Creating children\n" ;

cout << "Attaching parent\n" ;
UFRuntime::sem5Attach( name ) ;

for( int i = 0 ; i < 5 ; i++ )
	{
	pid_t fork_id = fork() ;

	if( 0 == fork_id )
		{
		// child
		do_child( i + 1 ) ;
		return 0 ;
		}
	
	}

for( int i = 0 ; i < 15 ; i++ )
        {

        cout << "Parent taking: " << currentTime() << endl ;
        UFRuntime::sem5Take( name ) ;
        cout << "Parent got it: " << currentTime() << endl ;
//        timespec ts = { 1, 1000 } ;
//        nanosleep( &ts, &ts ) ;
	cout << "Parent releasing: " << currentTime() << endl ;
	UFRuntime::sem5Release( name ) ;
	cout << "Parent gone... " << currentTime() << endl ;

        }

cout << "Parent exiting\n" ;

return 0 ;

}

void do_child( int childNum )
{

cout << childNum << " :Child Attaching: " << endl ;
UFRuntime::sem5Attach( name ) ;

for( int i = 0 ; i < 15 ; i++ )
	{

	cout << childNum << ": Taking: " << currentTime() << endl ;
	UFRuntime::sem5Take( name ) ;
	cout << childNum << ": Got it: " << currentTime() << endl ;
//	timespec ts = { childNum, childNum * 1000 } ;
//	nanosleep( &ts, &ts ) ;
	cout << childNum << ": Releasing: " << currentTime() << endl ;
	UFRuntime::sem5Release( name ) ;
	cout << childNum << ": Gone... " << currentTime() << endl ;

	}

cout << "Child exiting: " << childNum << endl ;

exit( 0 ) ;

}

string currentTime()
{
return UFRuntime::currentTime() ;
}
