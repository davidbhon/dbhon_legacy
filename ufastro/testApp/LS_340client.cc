/* LS_340client.cc
 */

#include	"iostream"
#include	"string"
#include	"vector"

#include	"cstdlib"
#include	"csignal"

#include	"UFClientSocket.h"
#include	"UFStrings.h"

using namespace std ;

void sigHandler( int ) ;
int sendToServer( UFClientSocket&, const vector< string >& ) ;

int main( int argc, const char** argv )
{

if( argc != 3 ) {
  clog << "Usage: " << argv[ 0 ] << " <server> <port>\n" ;
  return 0 ;
}

string server = argv[ 1 ] ;
int portNo = atoi( argv[ 2 ] ) ;

signal( SIGINT, sigHandler ) ;

UFClientSocket uplink ;

clog << "Connecting to: " << server << ", port " << portNo << endl ;

if( uplink.connect( server, portNo ) < 0 ) {
  clog << "Failed to connect to server\n" ;
  return 0 ;
}

vector< string > outBuffer ;
unsigned int lineCount = 1 ;

clog << "Connected\n" ;
clog << "\nWelcome to the Lakeshore 340 client.\n" ;
clog << "Please enter text to send to the server.\n" ;
clog << "Enter \"flush\" to flush the buffered lines of text\n" ;
clog << "to the server.\n" ;
clog << "Enter \"quit\" to end\n\n" ;

while( true ) {
  char buffer[ 1000 ] ;

  cout << "[" << lineCount << "]> " ;
  cin.getline( buffer, 999 ) ;

  string command = buffer ;

  if( command == "quit" ) {
    break ;
  }

  if( command == "flush" ) {
    if( sendToServer( uplink, outBuffer ) < 0 )
      {
	clog << "Failed to send to server\n" ;
      }
    else
      {
	clog	<< "Sent " << outBuffer.size() << " lines to "
		<< "server\n" ;
	clog	<< "Waiting on reply...";

	UFProtocol *ufp = UFProtocol::createFrom(uplink);

	clog << endl;
	if( ufp == 0 )
	  clog << "Error> createFrom returned null" << endl;

	UFStrings *ufs = dynamic_cast<UFStrings*>(ufp);
	if( ufs ) {
	  clog<<"received reply:\n name= "
	      <<ufs->cname()<<", time= "<<ufs->timestamp()
	      <<", elems= "<<ufs->elements()<<", nvals= "<<ufs->numVals()<<endl;
	  for( int i = 0; i < ufs->elements(); ++i )
	    clog<<ufs->valData(i)<<endl;
	}
	else {
	  clog<<"Error> server did not return UFStrings?, typ= "
	      <<ufp->typeId()<<endl;
	}

	delete ufp; // Clean up with base pointer
	ufs = 0;    // Set dynamic cast pointer to null to be safe
      }
    outBuffer.clear() ;
    lineCount = 1 ;
    continue ;
  }

  // Else, buffer the line
  outBuffer.push_back( command ) ;

  lineCount++ ;

}

uplink.close() ;
return 0 ;

}

int sendToServer( UFClientSocket& uplink, const vector< string >& outBuffer )
{
  strstream s ;
  s << time( 0 ) ;
  string name ;
  s >> name ;
  delete[] s.str() ;
 
UFStrings outStrings( name, outBuffer ) ;
 return outStrings.sendTo( uplink ) ;
}

void sigHandler( int )
{
  cout << "\nEnter \"quit\" to end\n" ;
}
