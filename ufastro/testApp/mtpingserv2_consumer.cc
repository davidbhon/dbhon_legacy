/* mtpingserv2_consumer */

#include <new>
#include <iostream>
#include <string>
#include <strstream>
#include <typeinfo>
#include <algorithm>

#include <unistd.h>
#include <ctime>
#include <cerrno>
#include <cstdlib>
#include <csignal>
#include <cctype>

#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFStringTokenizer.h"
#include "UFFrameConfig.h"

using std::string ;
using std::clog ;
using std::endl ;
using std::strstream ;
using std::ends ;

bool getFrames( int ) ;
void sigHandler( int ) ;

UFProtocol* handleObject( UFInts* ) ;
UFProtocol* handleObject( UFStrings* ) ;
UFProtocol* handleObject( UFTimeStamp* ) ;
UFProtocol* handleObject( UFProtocol* ) ;

volatile bool keepRunning = true ;
volatile bool runRandom = false ;

vector< string > bufferNames ;
string uplinkName( "localhost" ) ;
string commandPrompt( "$" ) ;
unsigned short int uplinkPort = 5555  ;

UFClientSocket* uplinkSock = 0 ;

std::ostream& operator<<( std::ostream& out, const UFStrings& strings )
{
  for( int i = 0 ; i < strings.elements() ; ++i )
    {
      out << strings[ i ] << endl ;
    }
  return out ;
}

void string_toLower( string& s )
{
  for( string::iterator ptr = s.begin() ; ptr != s.end() ; ++ptr )
    {
      *ptr = tolower( *ptr ) ;
    }
}

void string_toUpper( string& s )
{
  for( string::iterator ptr = s.begin() ; ptr != s.end() ; ++ptr )
    {
      *ptr = toupper( *ptr ) ;
    }
}

void usage( const string& progName )
{
  clog << endl ;
  clog << "Usage: " << progName << " [options]\n" ;
  clog << "-c <prompt>: Change command prompt ("
       << commandPrompt << ")\n" ;
  clog << "-h: Print help menu\n" ;
  clog << "-p <port num>: Specify port to which to connect ("
       << uplinkPort << ")\n" ;
  clog << "-u <uplink>: Specify uplink hostname/IP ("
       << uplinkName << ")\n" ;
  clog << endl ;
}

void printHelp()
{
  clog << endl ;
  clog << "Available commands:\n" ;
  clog << "quit: Exit the program\n" ;
  clog << "bn: Request buffer names\n" ;
  clog << "fc: Request FrameConfig object from server\n" ;
  clog << "get <buffer name>: Request buffer. Note that this does \n"
       << "not perform a read back, use \"recv\" to read the image "
       << "from the server\n" ;
  clog << "recv: Read from the uplink socket.\n" ;
  clog << "getrandom <num>: Retrieve <num> images from random buffers\n" ;
  clog << endl ;
}

int main( int argc, char** argv )
{

  int c = EOF ;
  while( (c = ::getopt( argc, argv, "c:hp:u:" )) != EOF )
    {
      switch( c )
	{
        case 'c':
          // Pure vanity.
          commandPrompt = optarg ;
          break ;
	case 'p':
	  uplinkPort = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
	  break ;
	case 'u':
	  uplinkName = optarg ;
	  break ;
	case 'h':
	default:
	  usage( argv[ 0 ] ) ;
	  return 0 ;
	}
    }

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGINT\n" ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGTERM, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGTERM\n" ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGPIPE, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGPIPE\n" ;
      return 0 ;
    }

  clog << "Connecting to " << uplinkName << " on port " << uplinkPort << endl ;

  try
    {
      uplinkSock = new UFClientSocket ;
    }
  catch( std::bad_alloc )
    {
      clog << "main> Memory allocation failure\n" ;
      return 0 ;
    }

  if( uplinkSock->connect( uplinkName, uplinkPort ) < 0 )
    {
      clog << "main> Unable to connect to " << uplinkName
	   << ':' << uplinkPort << endl ;
      delete uplinkSock ; uplinkSock = 0 ;
      return 0 ;
    }

  clog << "Requesting image list from server...\n" ;
  srand( time( 0 ) ) ;
 
  UFProtocol* recvMe = 0 ;
  unsigned int lineNumber = 0 ;

  printHelp() ;
  while( keepRunning )
    {

      ++lineNumber ;
      clog << "[" << lineNumber << "]" << commandPrompt << " " ;

      string command ;
      getline( cin, command ) ;

      string_toLower( command ) ;
      if( command.empty() )
        {
          printHelp() ;
          continue ;
        }

      UFStringTokenizer st( command ) ;
      command = st[ 0 ] ;

      if( command == "get" || command == "fc" || command == "bn" )
        {
          string name = st[ 0 ] ;

          if( command == "get" )
            {
              if( st.size() != 2 )
                {
                  clog << "get: Invalid number of arguments\n" ;
                  continue ;
                }
              name = st[ 1 ] ;
            }
          else
            {
              string_toUpper( name ) ;
            }

          // First send request
          UFTimeStamp* t = new (nothrow) UFTimeStamp( name ) ;
          if( NULL == t )
            {
              clog << "Memory allocation failure\n" ;
              keepRunning = false ;
              continue ;
            }

          clog << "Sending request...\n" ;
          if( uplinkSock->send( t ) <= 0 )
            {
              clog << "Write error: " << strerror( errno ) << endl ;
              delete t ;
              keepRunning = false ;
              continue ;
            }
          clog << "Request sent successfully...\n" ;
        }
      else if( command == "recv" )
        {
          int readable = uplinkSock->readable() ;
          if( readable < 0 )
            {
              clog << "main> readable() error: "
                   << strerror( errno ) << endl ;
              keepRunning = false ;
              continue ;
            }
          else if( 0 == readable )
            {
              clog << "No data available to read\n" ;
              continue ;
            }

          clog << "Receiving image from uplink...\n" ;
          
          recvMe = UFProtocol::createFrom( *uplinkSock ) ;
          if( NULL == recvMe )
            {
              clog << "Read error from uplink: "
                   << strerror( errno ) << endl ;
              keepRunning = false ;
              continue ;
            }

          // Handle the object
          // If a reply is returned from the handleObject() method,
          // then go ahead and write it back to the client.
          UFProtocol* reply = handleObject( recvMe ) ;
          if( NULL == reply )
            {
              delete recvMe ; recvMe = 0 ;
              continue ;
            }
        }
      else if( command == "getrandom" )
        {
          if( st.size() != 2 )
            {
              clog << "Usage: getrandom <num>\n" ;
              continue ;
            }
          int randNum = ::atoi( st[ 1 ].c_str() ) ;
          if( !getFrames( randNum ) )
            {
              clog << "Read error, shutting down\n" ;
              keepRunning = false ;
            }
        }
      else if( command == "quit" || command == "exit" )
        {
          keepRunning = false ;
          continue ;
        }
      else
        {
          clog << "Unrecognized command.\n" ;
          printHelp() ;
        }
      delete recvMe ; recvMe = 0 ;
    }

  clog << "Shutting down...\n" ; 

  delete recvMe ; recvMe = 0 ;
  uplinkSock->close() ;
  delete uplinkSock ; uplinkSock = 0 ;

  return 0 ;
}

bool getFrames( int numFrames )
{

  if( bufferNames.empty() )
    {
      clog << "Please retrieve the buffer names first\n" ;
      return true ;
    }

  runRandom = true ;
  for( int i = 0 ; (i < numFrames) && keepRunning && runRandom ; ++i )
    {
      int randNum = rand() % bufferNames.size() ;

      // Request the image
      UFTimeStamp* t = new (nothrow) UFTimeStamp( bufferNames[ randNum ] ) ;
      if( NULL == t )
        {
          clog << "getFrames> Memory allocation failure\n" ;
          ::exit( 0 ) ;
        }

      if( uplinkSock->send( t ) <= 0 )
        {
          clog << "getFrames> Write error: " << strerror( errno ) << endl ;
          delete t ;
          keepRunning = false ;
          return false ;
        }

      delete t ; t = 0 ;

      // Sent successfully
      clog << "Requested buffer " << bufferNames[ randNum ] << endl ;

      // Retrieve the reply
      UFProtocol* reply = UFProtocol::createFrom( *uplinkSock ) ;
      if( NULL == reply )
        {
          clog << "getFrames> Read error: " << strerror( errno ) << endl ;
          keepRunning = false ;
          return false ;
        }

      handleObject( reply ) ;
      delete reply ; reply = 0 ;

      // Infinite?
      if( -1 == numFrames )
        {
          -- i ;
        }

    }

  return true ;
}

void sigHandler( int whichSig )
{
  clog << "Caught signal" ;
  switch( whichSig )
    {
    case SIGINT:
      if( runRandom ) runRandom = false ;
      else keepRunning = false ;
      break ;
    default:
      keepRunning = false ;
      break ;
    }

  if( !keepRunning )
    {
      clog << ", shutting down" ;
    }
  clog << endl ;
}

UFProtocol* handleObject( UFTimeStamp* handleMe )
{
  clog << "Received UFTimeStamp: " << handleMe->name()
       << endl ;
  return 0 ;
}

UFProtocol* handleObject( UFFrameConfig* fc )
{
  clog << "Received UFFrameConfig:\n" ;
  clog << "Width: " << fc->width() << endl ;
  clog << "Height: " << fc->height() << endl ;
  clog << "Depth: " << fc->depth() << endl ;
  clog << "Frame Observation Sequence Number: "
       << fc->frameObsSeqNo() << endl ;
  return 0 ;
}

UFProtocol* handleObject( UFStrings* handleMe )
{
  clog << "Received UFStrings\n" ;
  // Buffer names
  bufferNames.clear() ;
  for( int i = 0 ; i < handleMe->elements() ; ++i )
    {
      bufferNames.push_back( (*handleMe)[ i ] ) ;
    }
  clog << "handleObject(UFStrings)> Got buffer names:\n" ;
  std::copy( bufferNames.begin(), bufferNames.end(),
	     ostream_iterator< string >( clog, ", " ) ) ;
  clog << endl ;
  return 0 ;
}

UFProtocol* handleObject( UFInts* handleMe )
{
  clog << "Received UFInts of size " << handleMe->elements()
       << endl ;
  clog << "Name: " << handleMe->name() << endl ;
  return 0 ;
}

UFProtocol* handleObject( UFProtocol* handleMe )
{
  UFProtocol* reply = 0 ;

  // This could be done using the typeid() operator, but
  // using the switch here is actually a bit more compact
  // (typeid() requires if/else if/...)
  //
  switch( handleMe->typeId() )
    {
    case UFProtocol::_Ints_:
      reply = handleObject( dynamic_cast< UFInts* >( handleMe ) ) ;
      break ;
    case UFProtocol::_TimeStamp_:
      reply = handleObject( dynamic_cast< UFTimeStamp* >( handleMe ) ) ;
      break ;
    case UFProtocol::_Strings_:
      // It's the image list
      reply = handleObject( dynamic_cast< UFStrings* >( handleMe ) ) ;
      break ;
    case UFProtocol::_FrameConfig_:
      reply = handleObject( dynamic_cast< UFFrameConfig* >( handleMe ) ) ;
      break ;
    default:
      clog << "handleObject> Received unknown object\n" ;
      break ;
    } // switch()

 return reply ;
}
