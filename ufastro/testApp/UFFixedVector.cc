#if !defined(__UFFixedVector_cc__)
#define __UFFixedVector_cc__ "$Name:  $ $Id: UFFixedVector.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFixedVector_cc__;
#include "UFFixedVector.h"
__UFFixedVector_H__(__UFFixedVector_cc);

#include "UFProtocol.h"
#include "UFStrings.h"

#include "algorithm"
#include "functional"

using namespace std;


template<class T>
class Print: public unary_function<T, void> {
public:
  void operator()(T& arg1) {
    cout << arg1->name() << endl;

  }

};


int main(int argc, char** argv) {

   Print<UFProtocol*> DoPrint;

  cout << "This is UFFixedVector.cc!" << endl;
  cout << "This is UFFixedVector.cc!" << endl;

  UFFixedVector uffv("My Vector", 4);

  uffv.push_front(new UFStrings("- 1 -"));
  uffv.push_front(new UFStrings("- 2 -"));
  uffv.push_front(new UFStrings("- 3 -"));
  uffv.push_front(new UFStrings("- 4 -"));
  uffv.push_front(new UFStrings("- 5 -"));

  cout << "Name of UFFixedVector: " << uffv.name() << endl;

  for (list<UFProtocol*>::iterator ptr = uffv.begin(); ptr != uffv.end(); ptr++) {
    cout << (*ptr)->name()  << endl;
  }

  cout << "Using for_each:" << endl;
  for_each(uffv.begin(), uffv.end(), DoPrint);

//   copy(uffv.begin(), uffv.end(), ostream_iterator<UFProtocol*>(cout, "\n"));

  //  uffv.push_back();

}


#endif // __UFFixedVector_cc__
