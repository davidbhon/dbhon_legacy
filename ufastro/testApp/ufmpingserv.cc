const char rcsId[] = "$Id: ufmpingserv.cc 14 2008-06-11 01:49:45Z hon $";

#include "iostream.h"
#include "string"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"

static UFSocket theConnection; // must be globally accessed by sigHandler

static void lostConnection(int sig) {
  if( sig == SIGPIPE ) {
    theConnection.close();
  }
  if( sig == SIGINT ) {
    theConnection.close();
    exit(0);
  }
  UFPosixRuntime::defaultSigHandler(sig);
}

void uflisten(UFServerSocket& server) {
  static bool initialized = false;
  if( ! initialized ) {
    server.listen();
    initialized = true;
  }
  clog<<"ufpingserv> waiting for client connection on: "<<server.description()<<endl;

  bool connected = false;
  int fd= -1;
  while( ! connected ) {
    fd = server.acceptClient(theConnection);
    if( fd >= 0 ) 
      connected = true;
    else
      sleep(1);
  } 
  clog<<"ufpingserv> got client connection..."<<endl;

  return;
}
  
int main(int argc, char** argv) {
  string port("55555");
  if( argc > 1 ) { //port supplied
    port = argv[1];
  }

  int portNo = atoi(port.data());
  clog << "portNo = " << portNo << endl;
  UFServerSocket theServer(portNo);

  UFPosixRuntime::setSignalHandler(lostConnection);
  clog << "established signal handler for SIGPIPE = " << SIGPIPE << endl;
  // enter recv/send loop
  unsigned long msgcnt=0;
  string newnam;
  do {
    if( !theConnection.validConnection() ) { // test the valid function
      clog<<"ufpingserv> ? no client connection ? listen for new client"<<endl;
      //theConnection = uflisten(theServer);
      uflisten(theServer);
    }

    if( theConnection.readable() > 0 ) { // something is available in the read buffer
      clog<<"ufpingserv> msgcnt= "<<msgcnt++<<", reading client ping mesg..."<<endl;
      UFProtocol *ufp = UFProtocol::createFrom(theConnection);
      if( ufp == 0 ) {
        clog<<"ufpingserv> UFProtocol::createFrom failed?..."<<endl;
        ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
      }
      clog<<"ufpingserv> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
	  <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
	  <<" length = "<<ufp->length()<<endl;
      newnam =  ufp->name() + " -- Alive! -- ";
      ufp->rename(newnam);
      ufp->stampTime(UFRuntime::currentTime());
      clog<<"ufpingserv> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
      if( theConnection.writable() > 0 ) {
        theConnection.send(ufp);
      }
      else {
        clog<<"ufpingserv> ? socket not writable"<<endl;
	theConnection.close();
      }
      delete ufp;
    }
    else {
      //clog<<"ufpingserv> sleeping due to lack of available data..."<<endl;
      sleep(1);
    }
  } while( true );
}


      
