
#ifndef __device_echo_cc__
#define __device_echo_cc__ "$Name:  $ $Id: device_echo.cc 14 2008-06-11 01:49:45Z hon $"

/**
 * This file's rcdID, for version tracking and management.
 */
const char rcsId[] = __device_echo_cc__ ;

#include	<new>
#include	<string>
#include	<map>
#include	<iostream>
#include	<fstream>
#include        <vector>
#include	<algorithm>

#include	<sys/types.h>
#include	<unistd.h>
#include	<csignal>
#include        <cstring>
#include	<sys/socket.h>

#include        "device_echo.h"
__device_echo_H__(__device_echo_cc__) ;

#include	"UFStrings.h"
__UFStrings_H__(__device_echo_cc__) ;

#include	"UFClientSocket.h"
__UFClientSocket_H__(__device_echo_cc__) ;

#include	"UFServerSocket.h"
__UFServerSocket_H__(__device_echo_cc__) ;

#include	"UFStringTokenizer.h"
__UFStringTokenizer_H__(__device_echo_cc__) ;

static const char device_echo_rcsId[] = "$Name:  $ $Id: device_echo.cc 14 2008-06-11 01:49:45Z hon $" ;

using std::clog ;
using std::endl ;
using std::map ;
using std::ifstream ;
using std::vector ;
using std::ends ;
using std::strstream ;

/**
 * This variable is true while all threads are to remain running.
 * It may be set to false due to a terminal error by any thread,
 * or by main() as the result of an administrator request, or
 * during a rehash.
 */
static volatile bool keepRunning = true ;

/**
 * This variable is set to true when a rehash request is received.
 */
static volatile bool doRehash = false ;

/**
 * This variable is true if a client is currently
 * connected.
 */
static volatile bool isConnected = true ;

/**
 * This variable represents the server's internal counter for job
 * tracking.  It is incremented by one each time a new UFStrings
 * object is received from a client, whether the object represents
 * a valid command or not.
 */
int jobId = -1 ;

/**
 * This structure maintains the history of all UFStrings objects sent
 * to the server.
 * It's index is the jobId of the objects.
 */
vector< jobStatusType > jobStatus ;

/**
 * This structure stores information about each of the devices defined
 * in the parameters file.
 */

vector< device* > devices ;

/**
 * This is the type used for a translation table.  This table translates
 * from a device's identifier ('a','b','c',...) to an integer index into
 * the device table (0,1,2,...).
 */
typedef map< char, vector< device* >::size_type > deviceTranslationType ;

/**
 * This is the translation table to convert device identifiers into indices
 * into the device table.
 */
deviceTranslationType deviceTranslation ;

/**
 * This is the TCP port number on which to listen for incoming client
 * connections.
 */
int		localPort = 5551 ;

/**
 * This is the TCP port number to which to connect on the annex.
 */
int             remotePort = 7003 ;

/**
 * This is a binary predicate that performs a case insensitive
 * operator< for two std::string's.
 */
struct ltstr
{
  bool operator()( const string& lhs, const string& rhs )
  {
    return (strcasecmp( lhs.c_str(), rhs.c_str() ) < 0) ;
  }
} ;

/**
 * This is the type used for storing the command table.
 */
typedef map< string, commInfo*, ltstr > mapType ;

/**
 * This is the command table for the devices present on the annex
 * chain.
 */
mapType commandTable ;

/**
 * This is the name of the command files.
 */
string commandFileName( "command.txt" ) ;

/**
 * This is the name of the parameters file.l
 */
string paramFileName( "mot_param.txt" ) ;

/**
 * This is the IP/hostname of the annex.
 */
string annexHost( "192.168.111.101" ) ;

/**
 * This is the delimiting character (could also be a string) to append to
 * all commands sent to any device on the annex.  This is also the
 * delimiting character (string) expected when reading data back from
 * the annex.
 */
char delimiter = '\n' ;

/**
 * If this variable is set to true, then devices on the annex chain will
 * be initialized to default values (and sent to home) when the server
 * starts.
 */
bool doInitDevices = false ;

/**
 * This is the default timeout period, in micro seconds, to wait for
 * a reply from a device on the annex.
 * Note that each command may specify its own timeout in the command
 * table.
 */
unsigned int defaultTimeOut = 1000000 ;

/**
 * The UFStrings representation of the parameters file, minus empty lines
 * and comment lines.
 */
UFStrings* paramFile = 0 ;

/**
 * This socket is used to connect to the annex.
 */
UFClientSocket* annexSock = 0 ;

/**
 * This method is used by class commInfo to parse a string of options
 * found in the command file.
 */
void commInfo::parseOptions( const string& optString )
{
  // Parse out comma separated options.
  UFStringTokenizer st( optString, ',' ) ;
  if( st.empty() )
    {
      return ;
    }

  for( UFStringTokenizer::size_type i = 0 ; i < st.size() ; ++i )
    {
      // Parse out key=value pairs
      UFStringTokenizer valueTokens( st[ i ], '=' ) ;

      if( valueTokens[ 0 ] == "wait" )
	{
	  if( valueTokens.size() < 2 )
	    {
	      // Parse error
	      clog << "Parse error with option wait\n" ;
	      return ;
	    }

	  // Get the wait time, convert to microseconds.
	  waitTime = static_cast< time_t >( 1000 * atoi( valueTokens[ 1 ].c_str() ) ) ;
	  clog << "Parsed waitTime for command " << key << ": "
	       << waitTime << endl ;
	}
      else if( st[ i ] == "bad" )
	{
	  // bad command, disallow use
	  options |= OPT_BAD ;
	}
    }
}

/**
 * This is a comment, yes, it really is.
 */
int main( int argc, char** argv )
{

  /*
    if( SIG_ERR == signal( SIGINT, sigHandler ) )
    {
    clog	<< "Unable to establish signal handler for SIGINT\n" ;
    return 0 ;
    }
  */

  // Trap signal SIGHUP, rehash
  if( SIG_ERR == signal( SIGHUP, sigHandler ) )
    {
      clog	<< "Unable to establish signal handler for SIGHUP\n" ;
      return 0 ;
    }

  // Trap signal SIGPIPE, write error (usually)
  if( SIG_ERR == signal( SIGPIPE, sigHandler ) )
    {
      clog	<< "Unable to establish signal handler for SIGHUP\n" ;
      return 0 ;
    }

  // Parse command line options
  int c = EOF ;
  while( (c = getopt( argc, argv, "a:c:d:hil:r:p:w:" )) != EOF )
    {
      switch( c )
	{
	case 'a':
	  annexHost = optarg ;
	  break ;
	case 'c':
	  commandFileName = optarg ;
	  break ;
	case 'd':
	  delimiter = atoi( optarg ) ;
	  break ;
	case 'i':
	  doInitDevices = true ;
	  break ;
	case 'l':
	  localPort = atoi( optarg ) ;
	  break ;
	case 'r':
	  remotePort = atoi( optarg ) ;
	  break ;
	case 'p':
	  paramFileName = optarg ;
	  break ;
	case 'w':
	  defaultTimeOut = static_cast< unsigned long >( atoi( optarg ) ) ;
	  break ;
	case 'h':
	default:
	  usage( argv[ 0 ] ) ;
	  return 0 ;
	}
    } // close while

  // Read the device command file
  if( !readCommandFile( commandFileName ) )
    {
      clog	<< "Failed to read command file: " << commandFileName << endl ; 
      return 0 ;
    }

  // Read the parameters file
  if( !readParamFile( paramFileName ) )
    {
      clog << "Failed to read parameters file: " << paramFileName << endl ;
      return 0 ;
    }

//   clog << commandTable.size() << " available commands...\n" ;
//   for( mapType::const_iterator ptr = commandTable.begin() ;
//        ptr != commandTable.end() ; ++ptr )
//     {
//       clog	<< "Key: " << ptr->first << ", "
// 		<< "Format: " << ptr->second->format << ", "
// 		<< "Name: " << ptr->second->name
// 		<< endl ;
//     }

//  clog << "Parameters file:\n" << *paramFile << endl ;

  // Instantiate the annexSock and listenSock
  UFServerSocket* listenSock = 0 ;
  try
    {
      annexSock = new UFClientSocket( remotePort ) ;
      listenSock = new UFServerSocket( localPort ) ;
    }
  catch( std::bad_alloc )
    {
      clog	<< "Memory allocation failure for ClientSocket\n" ;
      return 0 ;
    }

  // Connect to the annex
  if( annexSock->connect( annexHost, remotePort ) < 0 )
    {
      clog	<< "Unable to connect to: " << annexHost
		<< " on port " << remotePort << endl ;
      return 0 ;
    }

  clog << "Connected to annex\n" ;

  // Should we initialize the devices?
  if( doInitDevices )
    {
      clog << "Initializing devices...\n" ;

      // Attempt to initialize the devices
      if( !initDevices() )
	{
	  clog << "Failed to initialize devices\n" ;
	  keepRunning = false ;
	}
    }

  clog << "Retrieving device state(s)...\n" ;

  // Get motor states...
  if( !getDeviceStates() )
    {
      clog << "Failed to retrieve device states\n" ;
      keepRunning = false ;
    }

  // Attept to listen on localPort for incoming connections
  listenSock->listen( localPort, 1 ) ;

  clog	<< "Listening on port " << localPort << endl ;
  clog	<< "Rerouting incoming connections to " << annexHost
	<< " on port " << remotePort << endl ;

  // Continue while keepRunning is true
  while( keepRunning )
    {

      // Build a new UFSocket for a new incoming connection
      UFSocket clientSock ;

      // Attempt to accept a new client connection.
      // This is a blocking call.
      if( listenSock->accept( clientSock ) < 0 )
	{
	  clog	<< "Failed to capture client: "
		<< strerror( errno ) << endl ;
	  continue ;
	}

      clog << "Got connection\n" ;

      // We got a client connection, set isConnected to true
      isConnected = true ;

      // Continue processing this client while the client is connected
      // and while the server keeps running
      while( isConnected && keepRunning )
	{

	  // Read a UFStrings object from the client
	  UFStrings* s = dynamic_cast< UFStrings* >( UFProtocol::createFrom( clientSock ) ) ;

	  // Was the read successful?
	  if( NULL == s )
	    {
	      // Nope, lost the client, attempt to move on with life.
	      clog	<< "Read error to client\n" ;

	      isConnected = false ;
	      continue ;
	    }

	  // Got a new UFStrings object, so tag it with a new jobId.
	  jobId++ ;
	  jobStatus.push_back( JOB_PENDING ) ;

	  clog << "Read from client:\n" << *s << endl ;

	  // Prepare the reply string
	  // verifyCommands() is responsible also for checking the
	  // syntax/etc of all commands.
	  errorNum whichError = verifyCommands( s ) ;

	  // Get a copy of the options sent with this UFStrings command object.
	  string newOptions = (*s)[ 0 ] ;

	  // Check the return from verifyCommands().
	  if( whichError != ESUCCESS && whichError != ESERVERCOMM )
	    {
	      // Bad command, tag it as rejected.
	      clog << "Found a bad command\n" ;
	      newOptions += " reject " ;
	      jobStatus[ jobId ] = JOB_REJECT ;
	    }
	  else
	    {
	      // Otherwise, the command set has been accepted and
	      // needs to be processed.
	      newOptions += " accept " ;
	      jobStatus[ jobId ] = JOB_EXECUTING ;
	    }

	  // Append the jobId to the options string so the client (and the
	  // server) have a common means of referencing this object.
	  newOptions += getJobId() ;

	  // tempVec will hold the reply to the client.
	  // This reply is essentially a replica of the original
	  // UFStrings object sent to the server, except that command
	  // status information (accept/reject/pending) has been
	  // appended to the values[ 0 ] field.
	  vector< string > tempVec ;

	  // Place the updated options fied in values[ 0 ].
	  tempVec.push_back( newOptions ) ;

	  // Skip values[ 0 ] here, and mark the rest as pending.
	  for( int i = 1 ; i < s->elements() ; ++i )
	    {
	      // TODO: Only mark as pending those commands that are to
	      // be executed.  If the command object has been rejected,
	      // no sense in marking these as pending.
	      tempVec.push_back( *s->stringAt( i ) ) ;
	      // tempVec[ i ] += " | pending" ;
	    }

	  // Send the reply back to the client.
	  // SIGPIPE is caught by the below sigHandler metehod.  If a SIGPIPE
	  // signal is delivered during a send() here, that method will be
	  // invoked by the system asynchronously (and hopefully quickly)
	  // and isConnected will be set to false.
	  if( UFStrings( s->name(), tempVec ).sendTo( clientSock ) < 0 || !isConnected )
	    {
	      // Write error to client, connection invalid.
	      // Free memory and continue.
	      clog << "Write error to client\n" ;
	      isConnected = false ;
	      delete s ;
	      continue ;
	    }

	  // We've sent the reply.
	  // If the reply was a rejection notice, halt processing
	  // of this UFStrings object.
	  if( whichError != ESUCCESS && whichError != ESERVERCOMM )
	    {
	      // Bad command request, deallocate the command object
	      // and continue.
	      delete s ;
	      continue ;
	    }
	  else if( ESERVERCOMM == whichError )
	    {
	      // If it's a server command, then no I/O is performed
	      // to the annex.
	      // handleServerCommand() is passed isConnected because
	      // it may have to write to the client, perhaps receiving
	      // a write error -- it may need to alter the client's
	      // connection state.
	      handleServerCommand( clientSock, s ) ;
	      delete s ;
	      continue ;
	    }

	  // This vector will hold the reply from the annex.
	  // Each command will be sent to the annex, a wait will occur,
	  // and a string will be read back from the annex.  The
	  // strings read back from the annex are placed into the
	  // annexReply vector.
	  vector< string > annexReply ;

	  // Send each command to the annex.
	  for( int i = 1 ; i < s->elements() && isConnected ; ++i )
	    {

	      // Format the command for the annex device protocol.
	      string writeMe = formatCommand( (*s)[ i ] ) ;

	      // Did it succeed?
	      if( writeMe.empty() )
		{
		  clog	<< "Error: Problems formatting: "
			<< (*s)[ i ] << endl ;
		  continue ;
		}

	      // Send as raw data to the annex.
	      // This is required because control characters/etc may be
	      // sent to the device.
	      int bytesWritten = annexSock->send( reinterpret_cast< const unsigned
						  char* >( writeMe.c_str() ), writeMe.size() ) ;

	      // Check for write error to annex.
	      if( bytesWritten < 0 )
		{
		  // Write error to annex.
		  // Not much we can do here, the annex connection is critical.
		  clog << "Write error to annex\n" ;
		  keepRunning = isConnected = false ;
		  continue ;
		}
	      clog	<< "Wrote (" << bytesWritten << ") bytes to annex: "
			<< writeMe << endl ;

	      // Read a reply from the annex.
	      // TODO: Sleep for particular amount of time.
	      UFStringTokenizer commTokens( (*s)[ i ] ) ;
	      unsigned long waitTimeout = waitTime( commTokens[ 2 ] ) ;
	     
	      // clog << "main, waitTimeout> " << waitTimeout << endl ;

	      usleep( waitTimeout ) ;

	      // Check for available data to be read from the annex.
	      int bytesAvailable = annexSock->available() ;

	      // Was the poll successful?
	      if( bytesAvailable < 0 )
		{
		  // Nope, connection to annex is invalid.
		  // The annex connection is critical, abort.
		  clog	<< "Unable to retrieve number of bytes available "
			<< "from annex\n" ;
		  isConnected = keepRunning = false ;
		  continue ;
		}

	      // Allocate a small C buffer for reading from the annex.
	      char* inBuf = 0 ;
	      try
		{
		  // Note that this assumes that no new data has arrived
		  // from the annex since available() was called.
		  inBuf = new char[ bytesAvailable + 1 ] ;
		}
	      catch( std::bad_alloc )
		{
		  // doh!
		  clog	<< "Memory allocation failure reading from annex\n" ;
		  keepRunning = isConnected = false ;
		  continue ;
		}

	      // Make sure the buffer is empty.
	      memset( inBuf, 0, bytesAvailable + 1 ) ;

	      // Attempt to read from the annex.
	      int readBytes = annexSock->recv( reinterpret_cast< unsigned char* >
					       ( inBuf ), bytesAvailable ) ;

	      // Did the read succeed?
	      if( readBytes < 0 )
		{
		  // Read error to annex.
		  // The connection to the annex is critical, abort.
		  clog << "Read error from annex\n" ;

		  // Deallocate the input buffer.
		  delete[] inBuf ;
		  isConnected = keepRunning = false ;
		  continue ;
		}

	      // Null terminate the input buffer.
	      inBuf[ readBytes ] = 0 ;

	      clog << "Read from annex: " << inBuf << endl ;

	      // Parse the reply from the annex.
	      parseAnnexReply( string( inBuf ) ) ;

	      // Add this reply from the annex to the reply vector.
	      annexReply.push_back( string( inBuf ) ) ;

	      // Deallocate the input buffer.
	      delete[] inBuf ;

	    } // close for loop

	  // Obtain the name field of this command object.
	  string name = s->name() ;

	  // The job has completed.
	  newOptions = (*s)[ 0 ] + " " + jobStatusStrings[ JOB_COMPLETE ] ;
	  newOptions += " " + getJobId() ;
	  jobStatus[ jobId ] = JOB_COMPLETE ;

	  // Add the options field to the beginning of the
	  // annexReply vector.  The order of the elements in
	  // the annexReply vector is maintained when calling
	  // the UFStrings constructor.
	  annexReply.insert( annexReply.begin(), newOptions ) ;

	  // Deallocate the command object.
	  delete s ;

	  // Just for good measure.
	  s = 0 ;

	  // Prepare to send the final reply to the client.
	  // Instantiation of this object is separated here so
	  // that the output may be examined.
	  UFStrings sendMe( name, annexReply ) ;
	  clog << "Sending to client:\n" << sendMe << endl ;

	  // Send the command reply from the annex back to the client.
	  if( sendMe.sendTo( clientSock ) < 0 || !isConnected )
	    {
	      // Write error to client.
	      clog	<< "Write error to client\n" ;
	      isConnected = false ;
	      continue ;
	    }

	  clog << "Finished write to client\n" ;

	} // while( isConnected )

      // This client is no longer connected.
      // Close the socket just to be safe.
      clientSock.close() ;

    } // while( true )

  /**
   * When we reach here, keepRunning is false.
   */

  // Close and deallocate the annexSock.
  annexSock->close() ;
  delete annexSock ; annexSock = 0 ;

  // Close and deallocate the listenSock.
  listenSock->close() ;
  delete listenSock ; listenSock = 0 ;

  // Deallocate all commands in the command table.
  for( mapType::iterator ptr = commandTable.begin() ; ptr != commandTable.end() ;
       ++ptr )
    {
      delete ptr->second ;
      ptr->second = 0 ;
    }

  // Deallocate all devices in the device table.
  for( vector< device* >::iterator ptr = devices.begin() ; ptr != devices.end() ;
       ++ptr )
    {
      delete *ptr ;
    }

  return 0 ;

} // main()

/**
 * This method is responsible for reading in the command file.
 * Each command will be parsed, and a new commInfo object added
 * the command table.
 */
bool readCommandFile( const string& fileName )
{

  // Open the command file.
  ifstream inFile( fileName.c_str() ) ;

  // Did the open succeed?
  if( !inFile.is_open() )
    {
      clog	<< "Unable to open command file: " << fileName << endl ;
      return false ;
    }

  // This is a text file, read it one line at a time.
  string inLine ;

  // Keep track of the line numbers for error reporting.
  int lineNumber = 0 ;

  // Continue while the input stream is valid.
  while( getline( inFile, inLine ) )
    {

      // Update the line number at the beginning of the loop
      // to avoid being skipped with a continue statement.
      lineNumber++ ;

      // Check if there is valid data on this line.
      if( inLine.empty() || '#' == inLine[ 0 ] )
	{
	  // Comment line, skip this line.
	  continue ;
	}

      // Tokenize the input.
      UFStringTokenizer st( inLine, ':' ) ;

      // Each line must have exactly 4 arguments.
      if( st.size() != 4 )
	{
	  // Parse error, invalid number of arguments given.
	  clog	<< lineNumber << ": Invalid number of arguments\n" ;
	  return false ;
	}

      // Create a new commInfo object for this command.
      commInfo* newComm = 0 ;
      try
	{
	  newComm = new commInfo ;
	}
      catch( std::bad_alloc )
	{
	  // uh-oh
	  clog	<< "Memory allocation failure\n" ;
	  return false ;
	}

      // command_name:command_format:options:command_description

      // Parse the command specifications.
      newComm->key = st[ 0 ] ;
      newComm->format = st[ 1 ] ;
      newComm->parseOptions( st[ 2 ] ) ;
      newComm->name = st[ 3 ] ;
      newComm->numArgs = count( st[ 1 ].begin(), st[ 1 ].end(), '%' ) ;

      // Add this command to the command table.
      if( !commandTable.insert( mapType::value_type( newComm->key, newComm ) ).second )
	{
	  clog	<< lineNumber << ": Unable to insert key: " << newComm->key << endl ;
	  return false ;
	}

    } // close while( getline() )

  // Close the command file.
  inFile.close() ;

  // The file has been read without problems, return true.
  return true ;

}

/**
 * Output to the user the syntax and available options for
 * using this program.
 */
void usage( const string& progName )
{
  clog << "\nUsage: " << progName << " [options]\n\n" ;
  clog << "-a <hostname/IP>: Annex host name\n" ;
  clog << "-c <file name>: Specify command file\n" ;
  clog << "-d <delimiter code>: Specify new delimiter ASCII integer code\n" ;
  clog << "-h: Print this menu.\n" ;
  clog << "-i: Initialize devices (experimental)\n" ;
  clog << "-l <port>: Listen on local port number <port>\n" ;
  clog << "-p <file name>: Specify motor parameters file\n" ;
  clog << "-r <port>: Remote (annex) port to connect to\n" ;
  clog << "-w <wait time>: Specify the default read timeout in milliseconds\n" ;
  clog << endl ;
}

/**
 * Signal handler.
 */
void sigHandler( int whichSig )
{
  switch( whichSig )
    {
    case SIGINT:
      clog << "Caught SIGINT\n" ;
      keepRunning = false ;
      break ;
    case SIGHUP:
      clog << "Caught SIGHUP\n" ;
      doRehash = true ;
      break ;
    case SIGPIPE:
      clog << "Caught SIGPIPE\n" ;
      isConnected = true ;
      break ;
    default:
      clog << "Caught signal: " << whichSig << endl ;
      keepRunning = false ;
      break ;
    }
}

/**
 * Verify that a set of commands is valid (syntax/permissions/etc).
 * If any one of the commands is invalid, return an error.
 * Command syntax is as follows:
 * c name arg1 arg2 arg3
 * c is a single character representing the device number in the chain
 * There must be at least two arguments.
 */
errorNum verifyCommands( const UFStrings* s )
{

  for( int i = 1 ; i < s->elements() ; ++i )
    {
      UFStringTokenizer st( *s->stringAt( i ) ) ;
      if( st.size() < 2 )
	{
	  return ESYNTAX ;
	}

      // First token must be exactly one character, it represents
      // the device type (could refer to server).
      if( st[ 0 ].size() != 1 )
	{
	  return ESYNTAX ;
	}

      // If the first character is 'R', then the command
      // is a server command.
      if( 'R' == st[ 0 ][ 0 ] || isServerCommand( st[ 1 ] ) )
	{
	  //	  clog << "Found server command: " << st << endl ;
	  if( i != 1 )
	    {
	      return ECOMMANDMIX ;
	    }
	  return verifyServerCommands( s ) ;
	}

      // Otherwise it's not a server command.
      // Argument #2 must then be a single character, which
      // represents the specific device in the device chain.
      if( st[ 1 ].size() != 1 )
	{
	    return ESYNTAX ;
	}

      // HL (high level move) is a special command.
      if( st[ 2 ] == "HL" )
	{
	  if( st.size() != 4 )
	    {
	      return ESYNTAX ;
	    }

	  deviceTranslationType::iterator ptr = deviceTranslation.find( st[ 1 ][ 0 ] ) ;
	  if( ptr == deviceTranslation.end() )
	    {
	      // No such device.
	      return ENODEVICE ;
	    }

	  deviceTranslationType::size_type whichPosition =
	    static_cast< deviceTranslationType::size_type >( atoi( st[ 3 ].c_str() ) ) ;

	  if( whichPosition < 1 || whichPosition > devices[ ptr->second ]->namedPositions_size() )
	    {
	      // out of bounds
	      return ESYNTAX ;
	    }

	  return ESUCCESS ;
	}

      // Argument #3 is the command name.
      // Attempt to find this command in the command table.
      mapType::const_iterator ptr = commandTable.find( st[ 2 ] ) ;

      // Was the command found?
      if( ptr == commandTable.end() )
	{
	  // Command NOT found.
	  return EUNKNOWN ;
	}

      // Command was found.

      // Check to see if the command is allowed to be executed.
      if( ptr->second->getOption( commInfo::OPT_BAD ) )
	{
	  // Hazardous command, disallow execution of this command.
	  return EHAZARD ;
	}
    }

  // All checks pass, return success.
  return ESUCCESS ;
}

/**
 * This method serves the same function as verifyCommands(), except that it
 * operates on server commands instead of device commands.
 */
errorNum verifyServerCommands( const UFStrings* s )
{

  for( int i = 1 ; i < s->elements() ; i++ )
    {
      UFStringTokenizer st( (*s)[ i ] ) ;

      if( st.size() < 2 )
	{
	  return ESYNTAX ;
	}

      else if( st[ 1 ] == "GP" )
	{
	}

      else if( st[ 1 ] == "STATUS" || st[ 1 ] == "CANCEL" )
	{
	  if( st.size() != 3 )
	    {
	      return ESYNTAX ;
	    }
	  int whichJob = atoi( st[ 2 ].c_str() ) ;
	  if( whichJob < 0 ||
	      static_cast< vector< jobStatusType >::size_type >( whichJob ) >= jobStatus.size() )
	    {
	      return ESYNTAX ;
	    }
	}
      else
	{
	  return EUNKNOWN ;
	}
    }
  return ESERVERCOMM ;
}

/**
 * This method is responsible for properly formatting a command
 * to be sent to a device on the annex chain.
 * All commands are known to be of proper syntax, and to be
 * non-hazardous.
 * The commString argument passed to this method is the actual
 * command string sent by the client.
 * It is of the format:
 *  C C COMMNAME [options]
 * where the first character is the type identified
 * the second argument is the device identifier
 * the commname is the name of the command (in client protocol)
 * and options are any options for the command.
 * Here, the type identifier is known to NOT be 'R', which is
 * a server command.
 * The string to be sent to the annex is returned.  If an error
 * occurs, an empty string is returned.
 */
string formatCommand( const string& commString )
{

  // Tokenize the command string.
  UFStringTokenizer st( commString ) ;

  // Obtain the device identifier.
  char deviceChar = st[ 1 ][ 0 ] ;

  // Store the command name.
  string commName = st[ 2 ] ;

  // If it's a high level command, handle it specially.
  if( commName == "HL" )
    {
      return formatHL( commString ) ;
    }

  // Look up the command.
  // This command is known to be in the table already because
  // of checkCommands() above, but we'll check it here anyway :)
  mapType::iterator ptr = commandTable.find( commName ) ;
  if( ptr == commandTable.end() )
    {
      clog << "formatCommand> Unable to find command: " << commName << endl ;
      return string() ;
    }

  // Obtain a pointer to the commInfo object for this command.
  commInfo* infoPtr = ptr->second ;

  // buf is the actual string that will be sent to the annex.
  // Start it off with the device identifier.
  string buf ;

  // type string has no constructor receiving type char
  buf = deviceChar ;

  string::size_type formatPos = 2 ;
  string::size_type tokenizerPos = 3 ;

  // Iterate through the command string and the tokenizer object.
  // This is the equivalent of a poor man's sprintf() with '%'
  // substitution.
  while( formatPos < infoPtr->format.size() && tokenizerPos < st.size() )
    {
      if( infoPtr->format[ formatPos ] == '%' )
	{
	  formatPos += 2 ;
	  buf += st[ tokenizerPos++ ] ;
	}
      else
	{
	  buf += infoPtr->format[ formatPos++ ] ;
	}
    }

  clog << "formatCommand> Parsed command: " << buf << endl ;

  // Append the delimiter to the buffer.
  buf += delimiter ;

  // Return the string to be sent to the device.
  return buf ;

}

/**
 * Format a high level command (HL).
 * M motor_id HL position_number
 * position_number is 1-based
 * Syntax has already been checked.
 */
string formatHL( const string& commString )
{

  UFStringTokenizer st( commString ) ;

  device* ptr = devices[ deviceTranslation.find( st[ 1 ][ 0 ] )->second ] ;
  namedPosition* npPtr = ptr->namedPositions[ atoi( st[ 3 ].c_str() ) ] ;

  int destination = npPtr->getPosition() ;
  int current = ptr->currentPosition ;

  string retMe ;
  retMe += ptr->deviceID ;

  if( current == destination )
    {
      return (retMe += '^') ;
    }

  if( current > destination )
    {
      // Need to go backwards
      retMe += '-' ;
    }
  else
    {
      // Need to go forwards
      retMe += '+' ;
    }

  int diff = current - destination ;
  if( diff < 0 ) diff = -diff ;

  strstream s ;
  s << diff << ends ;
  retMe += s.str() ;
  delete[] s.str() ;

  retMe += delimiter ;
  clog << "formatHL> " << retMe << endl ;
  return retMe ;

}

/**
 * Read the parameters file.
 * This includes allocating the UFStrings* fileParams object.
 * Also, setup the device table.
 */
bool readParamFile( const string& fileName )
{

  ifstream inFile( fileName.c_str() ) ;

  if( !inFile )
    {
      clog << "readParamFile> Unable to open: " << fileName << endl ;
      return false ;
    }

  // Do it the easy way, just read a non-empty/non-commented lines into
  // a vector and go from there.
  string inLine ;
  vector< string > fileVec ;

  while( getline( inFile, inLine ) )
    {
      if( inLine.empty() || ';' == inLine[ 0 ] )
	{
	  // Comment line
	  continue ;
	}
      fileVec.push_back( inLine ) ;
    }

  try
    {
      paramFile = new UFStrings( fileName, fileVec ) ;
    }
  catch( std::bad_alloc )
    {
      clog << "readParamFile> Memory allocation failure\n" ;
      inFile.close() ;
      return false ;
    }

  inFile.close() ;

  // Parse the file for devices etc.
  // Comment lines already removed.

  // First line is the number of devices
  int numDevices = atoi( fileVec[ 0 ].c_str() ) ;
  int currentLine = 1 ;

  // Read the main info string for each device.
  for( int i = 0 ; i < numDevices ; i++, currentLine++ )
    {
      UFStringTokenizer st( fileVec[ currentLine ] ) ;
      if( st.size() < 9 )
	{
	  clog << "Parse error at significant line: " << currentLine
	       << endl ;
	  return false ;
	}

      // Add this device to the device chain.
      device* newDevice = 0 ;
      try
	{
	 newDevice = new device(
				atoi( st[ 1 ].c_str() ), // IS
				atoi( st[ 2 ].c_str() ), // TS
				atoi( st[ 3 ].c_str() ), // A
				atoi( st[ 4 ].c_str() ), // D
				atoi( st[ 5 ].c_str() ), // HC
				atoi( st[ 6 ].c_str() ), // DC
				st[ 7 ][ 0 ], // AN
				st.assemble( 8 ) // Name
				) ;
	}
      catch( std::bad_alloc )
	{
	  clog << "readParamFile> Memory allocation failure\n" ;
	  return false ;
	}

  devices.push_back( newDevice ) ;
  //  clog << "Added new device:\n" << newDevice << endl ;
    
  // Add a translator for this device char.
  // Note that this assumes that the devices in the file are
  // given in non-descending aplhabetic order.
  deviceTranslation.insert( deviceTranslationType::value_type(
		            st[ 7 ][ 0 ], devices.size() - 1 ) ) ;

    } // for( int i = 0 ; i < numDevices ; i++, currentLine++ )

  // Now read named positions for each device
  for( int whichDevice = 1 ; whichDevice <= numDevices ; whichDevice++ )
    {

      device* devicePtr = devices[ whichDevice - 1 ] ;
      if( NULL == devicePtr )
	{
	  clog << "readParamFile> Unable to find device\n" ;
	  return false ;
	}

      // Read number of positions for this device
      UFStringTokenizer st( fileVec[ currentLine++ ].c_str() ) ;
      int numPositions = atoi( st[ 1 ].c_str() ) ;

      // Handle the HL named positions.
      for( int i = 0 ; i < numPositions ; i++, currentLine++ )
	{
	  UFStringTokenizer np( fileVec[ currentLine ] ) ;

	  devicePtr->addNamedPosition( new namedPosition(
							 atoi( np[ 0 ].c_str() ),
							 np[ 1 ] ) ) ;
	}

      // Output this device's information to the user.
      clog << "Device:\n" << devicePtr << endl ;

    }

 return true ;

}

/**
 * This method is responsible for handling any server commands.
 * This is a primitive implementation, nothing fancy.
 */
void handleServerCommand( UFSocket& clientSock , const UFStrings* comm )
{

  //  clog << "handleServerCommand()\n" ;

  vector< string > replyVector ;
  string name = comm->name() ;

  replyVector.push_back( (*comm)[ 0 ] + " " + getJobId() ) ;

  // Tokenize the command.
  UFStringTokenizer st( (*comm)[ 1 ] ) ;

  // This UFStrings object will be written to the
  // client at the end of the method, if it has
  // been allocated. (It will also be deallocated
  // afterwards.)
  UFStrings* writeMe = 0 ;

  // Parse out the command old-fashion style.
  if( st[ 1 ] == "GP" )
    {
      // Get Parameters
      for( int i = 0 ; i < paramFile->elements() ; i++ )
	{
	  replyVector.push_back( (*paramFile)[ i ] ) ;
	}
    }
  else if( st[ 1 ] == "STATUS" )
    {
      int checkJobId = atoi( st[ 2 ].c_str() ) ;
      replyVector.push_back( jobStatusStrings[ jobStatus[ checkJobId ] ] ) ; 
    }
  else if( st[ 1 ] == "CANCEL" )
    {
      // Do nothing, synchronous server.
    }
  else
    {
    // Unknown command
    }

  if( replyVector.size() != 1 )
    {
      clog << "Allocating object to send to client\n" ;
      writeMe = new (nothrow) UFStrings( name, replyVector ) ;
    }

  // Was this UFStrings object allocated?
  if( 0 == writeMe )
    {
      // This either means that no data was requested to be sent to
      // the client, or that the allocation failed.
      if( replyVector.size() != 1 )
	{
	  clog << "handleServerCommand> Memory allocation failure\n" ;
	}
      return ;
    }

  clog << "handleServerCommand> Sending to client\n" ;

  // writeMe was allocated -- send it to the client.
  if( writeMe->sendTo( clientSock ) < 0 || !isConnected )
    {
      clog << "handleServerCommand> Write error to client\n" ;
      // Write error, client connection is now invalid.
      isConnected = false ;
    }

  // Deallocate the UFStrings object.
  delete writeMe ;

}

/**
 * Return true if the given command is a server command, false otherwise.
 */
bool isServerCommand( const string& comm )
{
  return ( (comm == "GP") || (comm == "STATUS") || (comm == "CANCEL") ) ;
}

/**
 * Return a std::string representation of the current jobId.
 */
string getJobId()
{
  strstream s ;
  s << jobId << ends ;
  string retMe( s.str() ) ;
  delete[] s.str() ;
  //  clog << "getJobId()> " << retMe << endl ;
  return retMe ;
}

/**
 * Parse a reply from the annex.  This method is used for updating
 * the state of any/all devices on the annex chain.
 */
void parseAnnexReply( const string& reply )
{
  // cF200+
  // c+100

//   if( reply.size() < 2 )
//     {
//       clog << "parseAnnexReply> Bad reply length\n" ;
//       return ;
//     }

  char command = reply[ 1 ] ;
  if( command != 'F' && command != '+' && command != '-' )
    {
      // We don't care about this command.
      return ;
    }

  char deviceId = reply[ 0 ] ;
  deviceTranslationType::iterator ptr = deviceTranslation.find( deviceId ) ;
  if( ptr == deviceTranslation.end() )
    {
      clog << "parseAnnexReply> Unable to find deviceID: "
	   << deviceId << endl ;
      return ;
    }

  device* devicePtr = devices[ ptr->second ] ;

  if( 'F' == command )
    {
      devicePtr->currentPosition = 0 ;
      return ;
    }

  int amount = atoi( reply.substr( 2, reply.size() - 2 ).c_str() ) ;
  
  if( '+' == command )
    {
      devicePtr->currentPosition += amount ;
    }
  else if( '-' == command )
    {
      devicePtr->currentPosition -= amount ;
    }

  clog << "parseAnnexReply> New position: "
       << devicePtr->currentPosition << endl ;

}

/**
 * Initialize all devices on the annex chain to the state given in
 * the parameters file.  Also, set the position of each motor to home.
 * BUG: I believe that the tokenization of the reply string is not
 *  functioning properly.
 */
bool initDevices()
{

  // Iterate through each device in the device table.
  for( vector< device* >::iterator ptr = devices.begin() ;
       ptr != devices.end() ; ++ptr )
    {

      string writeMe ;
      writeMe = (*ptr)->deviceID ;

      writeMe += "I " ;
      {
	strstream s ;
	s << (*ptr)->IS << ends ;
	writeMe += s.str() ;
	delete[] s.str() ;
      }

      writeMe += delimiter ;

      clog << "initDevices> Sending: " << writeMe << endl ;

      isConnected = true ;
      if( annexSock->send( reinterpret_cast< const unsigned char* >( writeMe.c_str() ), writeMe.size() ) < 0 || !isConnected )
	{
	  clog << "initDevices> Write error to annex\n" ;
	  return false ;
	}

      usleep( waitTime( "IS" ) ) ;

      writeMe = (*ptr)->deviceID ;
      writeMe += "V " ;

      {
	strstream s ;
	s << (*ptr)->TS << ends ;
	writeMe += s.str() ;
	delete[] s.str() ;
      }

      writeMe += delimiter ;

      clog << "initDevices> Sending: " << writeMe << endl ;

      if( annexSock->send( reinterpret_cast< const unsigned char* >( writeMe.c_str() ), writeMe.size() ) < 0 || !isConnected )
	{
	  clog << "initDevices> Write error to annex\n" ;
	  return false ;
	}

      usleep( waitTime( "TS" ) ) ;

      writeMe = (*ptr)->deviceID ;
      writeMe += "K " ;

      {
	strstream s ;
	s << (*ptr)->A << " " << (*ptr)->D << ends ;
	writeMe += s.str() ;
	delete[] s.str() ;
      }

      writeMe += delimiter ;

      clog << "initDevices> Sending: " << writeMe << endl ;

      isConnected = true ;
      if( annexSock->send( reinterpret_cast< const unsigned char* >( writeMe.c_str() ), writeMe.size() ) < 0 || !isConnected )
	{
	  clog << "initDevices> Write error to annex\n" ;
	  return false ;
	}

      usleep( waitTime( "A" ) ) ;

      writeMe = (*ptr)->deviceID ;
      writeMe += "Y " ;

      {
	strstream s ;
	s << (*ptr)->HC << " " << (*ptr)->DC << ends ;
	writeMe += s.str() ;
	delete[] s.str() ;
      }

      writeMe += delimiter ;

      clog << "initDevices> Sending: " << writeMe << endl ;

      isConnected = true ;
      if( annexSock->send( reinterpret_cast< const unsigned char* >( writeMe.c_str() ), writeMe.size() ) < 0 || !isConnected )
	{
	  clog << "initDevices> Write error to annex\n" ;
	  return false ;
	}

      usleep( waitTime( "C" ) ) ;

      writeMe = (*ptr)->deviceID ;
      writeMe += "F 100 0" ;

      writeMe += delimiter ;

      clog << "initDevices> Sending: " << writeMe << endl ;

      isConnected = true ;
      if( annexSock->send( reinterpret_cast< const unsigned char* >( writeMe.c_str() ), writeMe.size() ) < 0 || !isConnected )
	{
	  clog << "initDevices> Write error to annex\n" ;
	  return false ;
	}

      usleep( waitTime( "HOME" ) ) ;

      int bytesAvailable = annexSock->available() ;
      if( bytesAvailable < 0 )
	{
	  clog << "initDevices> Error on annex socket: "
	       << strerror( errno ) << endl ;
	  return false ;
	}

      clog << "initDevices> available: " << bytesAvailable << endl ;

      if( 0 == bytesAvailable )
	{
	  clog << "initDevices> No data available, no device?" << endl ;
	  continue ;
	}

      // Let's try to read back a response from the annex.
      char* tmpBuf = 0 ;
      try
	{
	  tmpBuf = new char[ bytesAvailable + 1 ] ;
	}
      catch( std::bad_alloc )
	{
	  clog << "initDevices> Memory allocation failure\n" ;
	  return false ;
	}

      // Erase the temp buffer
      memset( tmpBuf, 0, sizeof( tmpBuf ) ) ;

      // Read from the annex
      if( annexSock->recv( reinterpret_cast< unsigned char* >( tmpBuf ),
			   bytesAvailable ) < 0 )
	{
	  clog << "initDevices> Read error to annex\n" ;
	  delete[] tmpBuf ;
	  return false ;
	}

      // NULL terminate the buffer
      tmpBuf[ bytesAvailable ] = 0 ;

      clog << "initDevices> Read from annex> " << tmpBuf << endl ;

      string reply( tmpBuf ) ;
      clog << "initDevices> Read from annex2> " << reply << endl ;

      // Deallocate the temp buffer
      delete[] tmpBuf ;

      string::size_type theCount = count( reply.begin(), reply.end(), delimiter ) ;
      clog << "initDevices> Found " << theCount << " instances of "
	   << "delimiter in the reply string\n" ;

      UFStringTokenizer st( tmpBuf, delimiter ) ;
      for( UFStringTokenizer::size_type i = 0 ; i < st.size() ; ++i )
	{
	  clog << "initDevices> Parsing: " << st[ i ] << endl ;
	  parseAnnexReply( st[ i ] ) ;
	}

    }

  isConnected = false ;
  return true ;
}

bool getDeviceStates()
{



  return true ;
}

unsigned long waitTime( const string& command )
{

  mapType::const_iterator commPtr = commandTable.find( command ) ;
  if( commPtr == commandTable.end() )
    {
      clog << "waitTime> Returning default: " << defaultTimeOut << endl ;
      return defaultTimeOut ;
    }

  return commPtr->second->waitTime ;

}

#endif // __device_echo_cc__
