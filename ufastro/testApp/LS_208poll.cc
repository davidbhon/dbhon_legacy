const char rcsId[] = "$Id: LS_208poll.cc 14 2008-06-11 01:49:45Z hon $";

// This program constantly monitors the Lakeshore 208
// Temperature Monitor and prints to stdout.
//
// A program can exec this process with popen to get the
// output stream and grab all the data.
// 
// Data is constantly output in this format:
//     ChannelNumber Temperature Newline
//
// where ChannelNumber can be 1-8
//       Temperature is degrees in Celsius?
//       Newline is '\n'
//
//
// ASCII text strings are used to communicate with the LS 340.
//
// The LS 208 must use a certain line terminator?
//   -- LF, CR, CR/LF, or LF/CR ?
// (LF = ascii value 10, CR = ascii value 13)
// 


#include "iostream.h"
#include "string"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFClientSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"

static UFSocket theConnection; // globally accessed by sigHandler
static UFClientSocket _client;

enum UNITS {CELSIUS=1,FAHRENHEIT=2,KELVIN=3,VOLTS=4};

static void lostConnection(int sig) {
  if( sig == SIGPIPE ) {
    cout << "lostConnection> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
  }
  if( sig == SIGINT ) {
    cout << "lostConnection> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
    exit(0);
  }
  UFPosixRuntime::defaultSigHandler(sig);
}

char* getData() {
  unsigned char* retVal = 0;
  int numAvail = 0;
  int numRead = 0;


  // We will never get more than this from the device
  retVal = new unsigned char[256];

  bool keepGoing = true;
  while (keepGoing) {

    // See how many bytes are available on socket
    numAvail = _client.available();

    if ((numAvail+numRead) > 256) {
      cerr<<"getData> numAvail too much! numAvail = "<<numAvail<<endl;
      return (char *)retVal = 0;
    }

    if (numAvail < 0)  // If nothing there, wait for one only (Blocking I/O)
      numRead += _client.recv(retVal+numRead, 1);
    else
      numRead += _client.recv(retVal+numRead, numAvail);

    if (retVal[numRead-1] == '\n')
      keepGoing = false;

  }

//   if (numAvail < 0) {
//     cerr<<"getData> Can't retrieve bytes available from _client"<<endl;
//   }
//   else if (numAvail == 0) {
//     cerr<<"getData> No data available from client"<<endl;
//   }
//   else {
//     retVal = new unsigned char[numAvail+1];
//     numRead = _client.recv(retVal, numAvail);
//     if (numRead < 0) {
//       cerr<<"getData> Error reading from device"<<endl;
//       retVal = 0;
//     }
//     else {
//       cerr<<"Setting retVal["<<numAvail<<"] = "<<'\0'<<endl;
//       retVal[numAvail] = '\0';
//       cerr<<"retVal = "<<retVal<<endl;
//     }
//   }

  return (char*)retVal;
}

void uflisten(UFServerSocket& server) {
  cerr<<"LS_340serv> waiting for client connection on: "<<server.description()<<endl;
  theConnection = server.listenAndAccept();
  cerr<<"LS_340serv> got client connection..."<<endl;

  theConnection.readable();
  return;
}

void changeChannel(int channelNum) {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "YC%d\n", channelNum);
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  
  return;
}

void requestInfo() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "WS\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  
  return;
}

void toCelsius() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0C\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toFahrenheit() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0F\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toKelvin() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0K\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toVolts() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0V\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

  
int main(int argc, char** argv) {

  // define defaults
  string annexIP("192.168.111.101");
  string annexPort("7003");

//   if( argc > 1 ) { //port supplied
//     port = argv[1];
//   }

  if( argc < 3) {
    cout << "usage> LS_208poll annexIP annexPort" << endl;
    exit(0);
  }

  // Should we check for arguments on command line and use defaults
  // if arguments not there?  Maybe later...
  annexIP = argv[1];
  annexPort = argv[2];

  int annexPortNo = atoi(annexPort.data());
  cerr << "annexIP = " << annexIP << endl;
  cerr << "annexPort = " << annexPortNo << endl;

  cerr << "LS_208serv> connecting to Lakeshore 208 at IP " << annexIP
       << " on port " << annexPortNo << endl ;
  if (_client.connect( annexIP.c_str(), annexPortNo ) < 0 ) {
    cout << "LS_208serv> Can't connect to Lakeshore 208" << endl;
    exit(0);
  }

  UFPosixRuntime::setSignalHandler(lostConnection);
  cerr << "established signal handler for SIGPIPE = " << SIGPIPE << endl;
  cerr << "established signal handler for SIGINT = " << SIGINT << endl;

  // enter recv/send loop
  //  unsigned long msgcnt=0;
  string newnam;
  char sendString[255] = "";  // string to send to annex
  char* retData = 0;     // will hold return data from annex
  int channel = 1;
  enum UNITS units = CELSIUS;

  // Start with first channel
  changeChannel(channel);
  UFPosixRuntime::sleep(3.00); // sleep for 4 seconds
  do {

    switch(units) {
    case (CELSIUS)     : toCelsius();
                         cerr << "Set to Celsius" << endl;
			 units = FAHRENHEIT;
                         break;
    case (FAHRENHEIT)  : toFahrenheit();
                         cerr << "Set to Fahrenheit" << endl;
			 units = KELVIN;
                         break;
    case (KELVIN)      : toKelvin();
                         cerr << "Set to Kelvin" << endl;
			 units = VOLTS;
                         break;
    case (VOLTS)       : toVolts();
                         cerr << "Set to Volts" << endl;
			 units = CELSIUS;
                         break;
    default:        cerr << "case default: bad deal..." << endl;

    }

    // Wait for units to be set up and then get info
    UFPosixRuntime::sleep(3.00); // sleep for 4 seconds
    requestInfo();
    UFPosixRuntime::sleep(3.00); // sleep for 4 seconds

    if ((retData = getData()) == 0) {
      cerr<<"Can't get any data from device"<<endl;
    }
    else {
      cerr<<"Got reply from device: "<<retData<<endl;
      delete[] retData;
    }

    // If we have done the celsius reading, move on to next channel
    if (units == CELSIUS) {
      channel++;
      // If we have incremented past end of last channel,
      // go back to first channel
      if (channel == 9)
	channel = 1;
      changeChannel(channel);
      UFPosixRuntime::sleep(3.00); // sleep for 4 seconds
    }



  } while( true );  // loop forever

} // end of main


      
