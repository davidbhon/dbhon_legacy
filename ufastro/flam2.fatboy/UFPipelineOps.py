#!/usr/bin/python
#Functions used by Flamingos 2 data pipeline
import math, pyfits
from numarray import *
import numarray.strings
from pyraf import iraf
from iraf import stsdas
import curses.ascii
from datetime import datetime
import Numeric

def linearity(data, coeffs):
   data = array(data+0.)
   output = zeros(shape(data), "Float32")
   n = 0
   for j in coeffs:
      n+=1
      output += j*data**n             #j * data^n
   return output

def arraymedian(input, axis="both"):
   n = input.nelements()
   if (n == 0): return 0
   if (axis == "both"):
      data = reshape(input, n).copy()
      data.sort()
      if (n%2 == 1): return data[int(n/2)]
      else: return (data[n/2] + data[n/2-1])/2.0
   elif (axis == "Y"):
      n = input.shape[0]
      data = input.copy()
      data.sort(0)
      if (n%2 == 1): return data[int(n/2),:]
      else: return (data[n/2,:] + data[n/2-1,:])/2.0
   elif (axis == "X"):
      n = input.shape[1]
      data = input.copy()
      data.sort(1)
      if (n%2 == 1): return data[:,int(n/2)]
      else: return (data[:,n/2] + data[:,n/2-1])/2.0

class badPixelMask:
   #Members array badPixels
   #tuple arrayShape
   #int arraySize
   #String filter

   def __init__(self, inputMask = None, filename = None, filter="none"):
      self.filter = filter
      if (filename != None): self.loadBadPixelMask(filename)
      if (inputMask != None): self.createBadPixelMask(inputMask)

   def createBadPixelMask(self, inputMask):
      self.arrayShape = shape(inputMask)
      self.arraySize = inputMask.nelements()
      self.badPixels = compress(reshape(inputMask, self.arraySize), arange(self.arraySize))

   def getBadPixelMask(self):
      mask = zeros(self.arraySize)
      mask[self.badPixels] = 1
      mask = reshape(mask, self.arrayShape)
      return mask

   def getGoodPixelMask(self):
      mask = ones(self.arraySize)
      mask[self.badPixels] = 0
      mask = reshape(mask, self.arrayShape)
      return mask

   def saveBadPixelMask(self, filename):
      fitsobj = pyfits.HDUList()
      hdu = pyfits.PrimaryHDU()
      hdu.data = self.getBadPixelMask()
      hdu.header.update('FILTER',self.filter)
      fitsobj.append(hdu)
      fitsobj.verify('silentfix')
      fitsobj.writeto(filename)
      fitsobj.close()

   def saveGoodPixelMask(self, filename):
      fitsobj = pyfits.HDUList()
      hdu = pyfits.PrimaryHDU()
      hdu.data = self.getGoodPixelMask()
      fitsobj.append(hdu)
      fitsobj.verify('silentfix')
      fitsobj.writeto(filename)
      fitsobj.close()

   def loadBadPixelMask(self, filename):
      fitsobj = pyfits.open(filename)
      self.createBadPixelMask(fitsobj[0].data)
      fitsobj.close()

def sigmaFromClipping(origData, sig, iter):
   n = 0
   oldstddev = 0 
   data = origData.copy()
   while (n < iter and len(data) > 0):
      mean = data.mean()
      stddev = data.stddev()
      if (stddev == oldstddev): break
      lo = mean-sig*stddev
      hi = mean+sig*stddev
      oldstddev = stddev
      data = compress((where(data > lo, 1, 0)*where(data < hi, 1, 0)), data)
      n+=1
   med = arraymedian(data)
   return [mean, med, stddev]

def removeCosmicRaysOld(data, npass):
   data = 0.+data
   nx = shape(data)[0]
   ny = shape(data)[1]
   initys = 1+arange((nx-2)*(ny-2), shape=(nx-2,ny-2))%(ny-2)
   initxs = 1+arange((nx-2)*(ny-2), shape=(nx-2,ny-2))/(nx-2)
   for k in range(npass):
      print 'Pass ',k
      sq = data**2
      cols = data[0:nx-2,:]+data[1:nx-1,:]+data[2:nx,:]
      cols = cols[:,0:ny-2]+cols[:,1:ny-1]+cols[:,2:ny]
      cols = (cols-data[1:nx-1,1:ny-1])/8.
      sqcols = sq[0:nx-2,:]+sq[1:nx-1,:]+sq[2:nx,:]
      sqcols = sqcols[:,0:ny-2]+sqcols[:,1:ny-1]+sqcols[:,2:ny]
      sqcols = (sqcols-sq[1:nx-1,1:ny-1])/8.
      sd = sqrt(sqcols-cols**2)
      b = where(abs(data[1:nx-1,1:ny-1]-cols) > 5*sd, 1, 0)
      ys = compress(b, initys)
      xs = compress(b, initxs)
      newData = data.copy()
      ict = 0
      for i in range(xs.nelements()):
	j = xs[i]
	l = ys[i]
        ict+=1
        temp = array([data[j-1,l-1],data[j,l-1],data[j+1,l-1], data[j-1,l],data[j+1,l],data[j-1,l+1],data[j,l+1],data[j+1,l+1]])
        temp.sort()
        newData[j,l] = (temp[3]+temp[4])/2.0
      data = newData
      print ict,' replaced.'
   return newData

def removeCosmicRays(data, npass, logfile='none'):
   data = 0.+data
   nx = shape(data)[0]
   ny = shape(data)[1]
   for k in range(npass):
      print 'Pass ',k
      if (os.access(logfile, os.F_OK)):
	f = open(logfile,'ab')
	f.write('\tPass '+str(k)+'\n')
	f.close()
      sq = data**2
      cols = data[0:nx-2,:]+data[1:nx-1,:]+data[2:nx,:]
      cols = cols[:,0:ny-2]+cols[:,1:ny-1]+cols[:,2:ny]
      cols = (cols-data[1:nx-1,1:ny-1])/8.
      sqcols = sq[0:nx-2,:]+sq[1:nx-1,:]+sq[2:nx,:]
      sqcols = sqcols[:,0:ny-2]+sqcols[:,1:ny-1]+sqcols[:,2:ny]
      sqcols = (sqcols-sq[1:nx-1,1:ny-1])/8.
      sd = sqrt(sqcols-cols**2)
      b = where(abs(data[1:nx-1,1:ny-1]-cols) > 5*sd)
      newData = data.copy()
      ict = 0
      xs = b[0]+1
      ys = b[1]+1
      for i in range(len(xs)):
        j = xs[i]
        l = ys[i]
        ict+=1
        temp = array([data[j-1,l-1],data[j,l-1],data[j+1,l-1], data[j-1,l],data[j+1,l],data[j-1,l+1],data[j,l+1],data[j+1,l+1]])
        temp.sort()
        newData[j,l] = (temp[3]+temp[4])/2.0
      data = newData
      print ict,' replaced.'
      if (os.access(logfile, os.F_OK)):
        f = open(logfile,'ab')
        f.write('\t'+str(ict)+' replaced.\n')
        f.close()
   return newData

def getRADec(s,warnfile='none',rel='no',dec=False):
   if (type(s) == type(0.)):
      if (rel == 'yes'):
	if (dec):
	   return s/3600.
	else:
	   return s/(3600.*15)
      else:
	if (dec):
	   return s
	else:
	   return s/15.
   try:
      n = s.find(':')
      x = float(s[:n])
      x = x+(x/abs(x))*float(s[n+1:s.find(':',n+1)])/60.
      x = x+(x/abs(x))*float(s[s.rfind(':')+1:])/3600.
   except Exception:
      print 'Error: malformed RA or Dec'
      if (os.access(warnfile, os.F_OK)):
	f = open(warnfile, 'ab')
	f.write('Error: malformed RA or Dec\n')
	f.write(s+'\n')
	f.write('Time: '+str(datetime.today())+'\n\n')
	f.close() 
      x = 0.
   return x 

def selectSkyFiles(prefix, files, j, rng, diff):
   n = len(files)
   befFiles = []
   aftFiles = []
   temp = pyfits.open(files[j])
   objRA = getRADec(temp[0].header['RA'])
   objDec = getRADec(temp[0].header['Dec'])
   temp.close()
   diff = diff/3600.0
   lastRA = 0
   lastDec = 0
   if (j > 0):
     for l in range(j-1,-1,-1):
        if (j != l and files[j][:files[j].find('.')] == files[l][:files[l].find('.')]):
           temp = pyfits.open(files[l])
           currRA = getRADec(temp[0].header['RA'])
           currDec = getRADec(temp[0].header['Dec'])
           temp.close()
           if ((abs(currRA-objRA) > diff or abs(currDec-objDec) > diff) and (abs(currRA-lastRA) > diff or abs(currDec-lastDec) > diff)):
              lastRA = currRA
              lastDec = currDec
              befFiles.append(files[l])
   if (j < n-1):
     for l in range(j+1,n,1):
        if (j != l and files[j][:files[j].find('.')] == files[l][:files[l].find('.')]):
           temp = pyfits.open(files[l])
           currRA = getRADec(temp[0].header['RA'])
           currDec = getRADec(temp[0].header['Dec'])
           temp.close()
           if ((abs(currRA-objRA) > diff or abs(currDec-objDec) > diff) and (abs(currRA-lastRA) > diff or abs(currDec-lastDec) > diff)):
              lastRA = currRA
              lastDec = currDec
              aftFiles.append(files[l])
   bef = len(befFiles)
   aft = len(aftFiles)
   if (rng == 0):
     nbef = bef
     naft = aft
   else:
     nbef = min(rng, bef)
     naft = min(rng, aft)
     if (bef < rng):
        naft = 2*rng-bef
     elif (aft < rng):
        nbef = 2*rng-aft
     if (nbef+naft < 2*rng):
        print "Warning: Only able to use ",nbef+naft," files for sky."
   skyfiles = ""
   for l in range(nbef):
     skyfiles+=prefix+befFiles[l]+","
   for l in range(naft):
     skyfiles+=prefix+befFiles[l]+","
   skyfiles=skyfiles.strip(",")
   print skyfiles

def extObjSky(image, msky):
   #Find median of master sky and image, scale and subtract sky.
   msmed = arraymedian(msky[where(msky != 0)])
   immed = arraymedian(image[where(image != 0)])
   ss_image = image-(immed/msmed)*msky

   #Create array of 100 stddevs of subimages
   xsize = int(ss_image.shape[0]/10)
   ysize = int(ss_image.shape[1]/10)
   sds = zeros(100)
   for j in range(10):
      for l in range(10):
	temp = ss_image[j*xsize:(j+1)*xsize,l*ysize:(l+1)*ysize]
        if len(temp[where(temp != 0)] != 0):
           sds[j*10+l] = temp[where(temp != 0)].stddev()
        else: sds[j*10+l] = 0.
	#sds[j*10+l]=ss_image[j*xsize:(j+1)*xsize,l*ysize:(l+1)*ysize].stddev()
   stddev = arraymedian(sds)

   good = where(ss_image < 5*stddev, 1, 0)*where(ss_image != 0, 1, 0)
   immed = arraymedian(compress(good, image))
   change = 0
   while (change < 0.99 or change > 1.01):
      oldGood = good
      stddev = ss_image[where(ss_image != 0)].stddev()
      ss_image = image-(immed/msmed)*msky
      good = where(ss_image < 5*stddev, 1, 0)*where(ss_image != 0, 1, 0)
      immed = arraymedian(compress(good, image))
      change = compress(oldGood, image).nelements()/float(compress(good, image).nelements())
   return immed

def smooth1d(data, width, niter):
   if (niter == 0): return data
   n = len(data)
   if (width % 2 == 0): width+=1
   w = width/2
   for k in range(niter):
      newdata = data+0.
      for j in range(1,w+1):
	temp = data+0.
	temp[w:-w] = data[w-j:-j-w]
	newdata+=temp
	temp = data+0.
	if (-w+j == 0):
	   temp[w:-w] = data[j+w:]
	else:
	   temp[w:-w] = data[j+w:-w+j]
	newdata+=temp
      newdata/=(w*2+1)
      data = newdata
   return data

def smooth(data, width, niter, type = "Float32"):
   if (niter == 0): return data
   m = data.shape[0]
   n = data.shape[1]
   if (width % 2 == 0): width+=1
   w = width/2
   tot = zeros((n+2*w,m+2*w), type)
   sdata = zeros(data.shape, type)
   a = zeros(data.shape, "Float32")
   b = zeros(data.shape, "Float32")
   c = zeros(data.shape, "Float32")
   d = zeros(data.shape, "Float32")
   for i in range(niter):
      for l in range(n):
	if (l > 0):
	   tot[w:-w,l+w] = tot[w:-w,l+w-1] + data[:,l]
        else:
           tot[w:-w,l+w] = data[:,l]
      for l in range(w):
        tot[:,l] = tot[:,w+1]
        tot[:,n+w+l] = tot[:,n+w-1]
      for j in range(m-2, -1, -1):
	tot[j+w,:]+=tot[j+w+1,:]
      for j in range(w):
        tot[m+w+j,:] = tot[m+w-1,:]
        tot[j,:] = tot[w+1,:]
      a[:,:] = tot[0:n,2*w:]
      b[0:-w-1,:] = tot[2*w+1:n+w,2*w:]
      c[:,w+1:] = tot[0:n,w:m-1]
      d[0:-w-1,w+1:] = tot[2*w+1:n+w,w:m-1]
      j = arange(n*m, shape=(n,m))%m
      l = transpose(j)
      jw1 = j+w+1
      jw = j-w
      lw = l+w
      lw1 = l-(w+1)
      jw1[:,-w:] = m
      jw[:,:w] = 0
      lw[-w:,:] = n-1
      lw1[:w,:] = -1
      pts = (jw1-jw)*(lw-lw1)
      sdata[:,:] = (a-b-c+d)/pts
      data = sdata
   return sdata

def linterp(data, x, gpm, iter=100, logfile='none'):
   nx = shape(data)[0]
   ny = shape(data)[1]
   z = -1
   initys = arange(nx*ny, shape=(nx,ny))%ny
   initxs = arange(nx*ny, shape=(nx,ny))/nx 
   p = 0
   print 'Interpolating across ' + str(x) + "'s"
   if (os.access(logfile, os.F_OK)):
      f = open(logfile,'ab')
      f.write('\tInterpolating across ' + str(x) + "'s\n")
      f.close()
   while (z != 0 and p < iter):
      p+=1
      print 'Pass ',p
      if (os.access(logfile, os.F_OK)):
	f = open(logfile,'ab')
        f.write('\tPass '+str(p)+'\n')
        f.close()
      b = where(data == x, 1, 0)*gpm
      ys = compress(b, initys)
      xs = compress(b, initxs)
      newData = data.copy()
      if (len(xs) == 0):
	z = 0
	break
      ict = 0
      for i in range(xs.nelements()):
        j = xs[i]
        l = ys[i]
	temp = []
	for k in range(-2,3,1):
	   for r in range(-2,3,1):
	      if (k == 0 and r == 0):
		continue
	      xc = k+j
	      yc = r+l
	      if (xc >= 0 and xc < nx and yc >= 0 and yc < nx):
		if (data[xc,yc] != 0):
		   temp.append(data[xc,yc])
	if (len(temp) != 0):
	   temp = array(temp)
	   newData[j,l] = arraymedian(temp)
	   ict+=1
      data = newData
      print ict,' replaced.'
      if (os.access(logfile, os.F_OK)):
        f = open(logfile,'ab')
        f.write('\t'+str(ict)+' replaced.\n')
        f.close()
      if (ict == 0):
	break
   return newData

def space(x):
   return ' '*x

def nebObjSky(prefix, filenames, isSky, useSky, nsky, utkey, datekey):
   objDates = []
   skyDates = []
   objTimes = []
   skyTimes = []
   skies = []
   obj = []
   nebImmeds = []
   sUseSky = []
   oUseSky = []
   #read in dates and times from each file
   #separate objects and skies
   for j in range(len(filenames)):
      temp = pyfits.open(prefix+filenames[j])
      date = temp[0].header[datekey]
      ut = getRADec(temp[0].header[utkey])*60.
      temp.close()
      if (isSky[j] == 1):
	skyDates.append(date)
	skyTimes.append(ut)
	skies.append(prefix+filenames[j])
	sUseSky.append(useSky[j][0])
      else:
	objDates.append(date)
	objTimes.append(ut)
	obj.append(prefix+filenames[j])
	oUseSky.append(useSky[j][0])
   objTimes = array(objTimes)
   skyTimes = array(skyTimes)
   objDates = numarray.strings.array(objDates)
   skyDates = numarray.strings.array(skyDates)
   sUseSky = array(sUseSky)
   oUseSky = array(oUseSky)
   c = 0
   for j in range(len(filenames)):
      if (isSky[j] == 1):
	nebImmeds.append(0)
	continue
      #For each object, find nsky skies closest in time on same day
      deltat = abs(objTimes[c]-skyTimes)
      deltat += where(skyDates != objDates[c], 1, 0)*1e+6
      deltat += where(sUseSky != oUseSky[c], 1, 0)*1e+6
      skyIndices = deltat.argsort()[0:nsky]
      medtotal = 0.0
      weight = 0.0
      #Perform weighted average of medians of skies
      for l in skyIndices:
	temp = pyfits.open(skies[l])
	medtotal += arraymedian(temp[0].data[where(temp[0].data != 0)])/deltat[l]
	weight += 1/deltat[l]
	#print skies[l],medtotal,weight,arraymedian(compress(where(temp[0].data != 0, 1, 0), temp[0].data)), deltat[l]
	temp.close()
      medtotal = medtotal/weight
      print prefix+filenames[j], medtotal
      nebImmeds.append(medtotal)
      c = c + 1
   return nebImmeds

def extractSpectra(data, sigma, width, nspec=0, sort='no'):
   n = data.nelements()
   ind = arange(n)
   b = ones(n)
   nold = n+1
   b = where(data != 0, 1, 0)
   g = [-1]
   while (n < nold and len(where(b == 1)[0]) > 1):
      m = arraymedian(compress(b, data))
      sd = compress(b, data).stddev()
      #if (n == len(b)): sd = compress(b*where(data < m, 1, 0), data).stddev()
      b = where(data < m+sigma*sd, 1, 0)*where(data > m-sigma*sd, 1, 0)*where(data != 0, 1, 0)
      g = where(data > m+sigma*sd)
      nold = n
      n = compress(b, data).nelements()
   c = 0
   if (len(g) == 1 and size(g) > 1): g = g[0]
   last = g[0]
   list = []
   for j in range(1, len(g)):
      if (g[j]-last != 1):
	if (c >= width):
	   list.append([j-c-1,j-1])
	c = 0
      else: c+=1
      last = g[j]
   if (c >= width):
      list.append([len(g)-c-1, len(g)-1])
   if (len(list) > 0):
      if (nspec == 0):
	if (sort == 'no'):
	   return g[list]
	else:
	   sig = []
	   for j in range(len(list)):
	      sig.append(data[g[list[j][0]]:g[list[j][1]]+1].mean())
	      b = array(sig).argsort()
	   newlist = []
	   for j in range(len(b)-1, -1, -1):
	      newlist.append(list[b[j]])
	   return g[newlist]
      else:
	sig = []
	for j in range(len(list)):
	   sig.append(data[g[list[j][0]]:g[list[j][1]]+1].mean())
	b = array(sig).argsort()
	newlist = []
	for j in range(len(b)-1, len(b)-1-min(nspec,len(list)), -1):
	   newlist.append(list[b[j]])
	return g[newlist]
   else: return [[-1,-1]]

def extractTroughs(data, sigma, width):
   n = data.nelements()
   ind = arange(n)
   b = ones(n)
   nold = n+1
   i = 0
   b = where(data != 0, 1, 0)
   while (n < nold and len(where(b == 1)[0]) > 1):
      i+=1
      m = arraymedian(compress(b, data))
      sd = compress(b, data).stddev()
      #if (n == len(b)): sd = compress(b*where(data < m, 1, 0), data).stddev()
      b = where(data < m+sigma*sd, 1, 0)*where(data > m-sigma*sd, 1, 0)*where(data != 0, 1, 0)
      g = where(data < m-sigma*sd)
      nold = n
      n = compress(b, data).nelements()
   c = 0
   if (len(g) == 1 and size(g) > 1): g = g[0]
   last = g[0]
   list = []
   for j in range(1, len(g)):
      if (g[j]-last != 1):
        if (c > 10):
           list.append([j-c-1,j-1])
        c = 0
      else: c+=1
      last = g[j]
   if (c >= width):
      list.append([len(g)-c-1, len(g)-1])
   if (len(list) > 0):
      return g[list]
   else: return [[-1,-1]]

def specCosmicRays(data, npass):
   hotcount = zeros(npass)
   coldcount = zeros(npass)
   maskfix = zeros((data.shape[0],data.shape[1]))
   medimg = arraymedian(data)
   nsig = int(data.shape[0]/5)*int(data.shape[1]/5)
   sigma = zeros(nsig)

   #create sigma array
   tmp = zeros(25)
   for i in range(nsig):
      yind = int(i/(data.shape[1]/5))*5+2
      xind = (i%(data.shape[0]/5))*5+3
      for j in range(5):
	tmp[j*5:(j+1)*5] = data[yind+j-2, xind-2:xind+3]
      tmp.sort()
      minslope = 1000.
      for j in range(7,18):
	slope = (tmp[j+7]-tmp[j-7])/1.68
	if (slope <= minslope): minslope = slope
      sigma[i] = minslope

   #get median sigma and sigma sigma
   medsig = arraymedian(sigma)
   sigma.sort()
   minslope = 1000.
   for j in range(nsig/3, nsig*2/3):
      slope = (sigma[j+nsig/3]-sigma[j-nsig/3])/1.92
      if (slope <= minslope): minslope = slope
   sigsig = minslope

   for k in range(npass):
      print "Pass "+str(k)
      #loop over 5x5 arrays
      imax = int(data.shape[1]/5)-2-int(((k+1)*2.0)/5.0)
      jmax = int(data.shape[0]/5)-2-int(((k+1)*2.0)/5.0)

      for i in range(imax):
	for j in range(jmax):
	   xind = 5*(i+1)+(k+1)*2
	   yind = 5*(j+1)+(k+1)*2
	   tmparr = data[yind-2:yind+3, xind-2:xind+3]

	   #find hot and cold pixels
	   b = where(tmparr == tmparr.min())
	   coldx = b[1][0]+xind-2
	   coldy = b[0][0]+yind-2
	   b = where(tmparr == tmparr.max())
	   hotx = b[1][0]+xind-2
	   hoty = b[0][0]+yind-2

	   #Hot loop
	   sec = data[hoty-2:hoty+3, hotx-2:hotx+3]
	   #get medians of 8, 16 surrounding pixels
	   tmp = reshape(data[hoty-1:hoty+2, hotx-1:hotx+2],9)
	   med8 = arraymedian(concatenate((tmp[0:4],tmp[5:])))
	   tmp = reshape(sec,25)
           med16 = arraymedian(concatenate((tmp[0:6],tmp[9:11],tmp[14:16],tmp[19:])))
	   #get sigma
	   sec = reshape(sec, 25)
	   sec.sort()
	   minslope = 1000.
	   for l in range(7,18):
	      slope = (sec[l+7]-sec[l-7])/1.68
	      if (slope <= minslope): minslope = slope
	   hotsig = minslope
           sec = reshape(sec, (5,5))

	   #replace hotsig if necessary
	   if (hotsig > (medsig+2.0*sigsig)):
	      sig1 = medsig+2*sigsig
	      sig2 = math.sqrt(5.0+0.21*abs(med8))
	      hotsig = max(sig1, sig2)

	   #pixel replacement
	   if (((med8 + 2.0*med16)/3.0*medimg) > 2.0*hotsig):
	      #hot pixel on hot feature case
	      if (sec[2,2] > (2.0*med8-medimg+3.0*hotsig)):
		data[hoty,hotx] = med8
		maskfix[hoty,hotx] = 1
		hotcount[k]+=1
	      else:
		#non-feature case
		if (sec[2,2] - ((med8+2.0*med16)/3.0) > 5.0*hotsig):
		   data[hoty,hotx] = med8
		   maskfix[hoty,hotx] = 1
		   hotcount[k]+=1

           #Cold loop
           sec = data[coldy-2:coldy+3, coldx-2:coldx+3]
           #get medians of 8, 16 surrounding pixels
           tmp = reshape(data[coldy-1:coldy+2, coldx-1:coldx+2],9)
           med8 = arraymedian(concatenate((tmp[0:4],tmp[5:])))
	   print sec.shape, coldy, coldx
           tmp = reshape(sec,25)
           med16 = arraymedian(concatenate((tmp[0:6],tmp[9:11],tmp[14:16],tmp[19:])))
           #get sigma
	   sec = reshape(sec,25)
           sec.sort()
           minslope = 1000.
           for l in range(7,18):
              slope = (sec[l+7]-sec[l-7])/1.68
              if (slope <= minslope): minslope = slope
           coldsig = minslope
	   sec = reshape(sec, (5,5))

           #replace coldsig if necessary
           if (coldsig > (medsig+2.0*sigsig)):
              sig1 = medsig+2*sigsig
              sig2 = math.sqrt(5.0+0.21*abs(med8))
              coldsig = max(sig1, sig2)

           #pixel replacement
           if (((med8 + 2.0*med16)/3.0*medimg) < -2.0*coldsig):
              #cold pixel on cold feature case
              if (sec[2,2] < (2.0*med8-medimg-3.0*coldsig)):
                data[coldy,coldx] = med8
                maskfix[coldy,coldx] = 1
                coldcount[k]+=1
              else:
                #non-feature case
                if (sec[2,2] - ((med8+2.0*med16)/3.0) < -5.0*coldsig):
                   data[coldy,coldx] = med8
                   maskfix[coldy,coldx] = 1
                   coldcount[k]+=1

      print 'Replaced: '+str(hotcount[k])+' hot, '+str(coldcount[k])+' cold.' 
   coldsum = sum(coldcount)
   hotsum = sum(hotcount)
   print 'Total replaced: '+str(hotsum)+' hot, '+str(coldsum)+' cold.'
   return data

def drihizzle(data, coeffs, interp="linear"):
   if (curses.ascii.isalpha(coeffs[0])):
      f = open(coeffs, 'rb')
      temp = f.read().split('\n')
      f.close()
      temp.remove('')
      coeffs = []
      for j in range(1, len(temp)):
	coeffs.append(float(temp[j]))
   coeffs = array(coeffs)
   n = len(coeffs)
   order = 0
   while (n > 0):
      n -= (order+1)
      if (n > 0): order+=1
   ny = data.shape[0]
   nx = data.shape[1]
   xin = arange(ny*nx, shape=(ny,nx))%nx+0.
   yin = arange(ny*nx, shape=(ny,nx))/ny+0.
   yout = zeros((ny,nx))+0.
   n = 0
   for j in range(order+1):
      for l in range(j+1):
	yout+=coeffs[n]*xin**(j-l)*yin**l
	n+=1
   ymin = int(yout.min())
   ymax = int(yout.max())+2
   newdata = zeros((ymax-ymin, nx))+0.
   temp = floor(yout-ymin).astype("Int32")
   frac = yout-ymin-temp
   xs = range(nx)
   for j in range(ny):
      #print j
      if (interp == 'linear'):
	newdata[temp[j,:],xs] += data[j,:]*(1.0-frac[j,:])
	newdata[temp[j,:]+1,xs] += data[j,:]*frac[j,:]
   return newdata

def drihizzle3d(data, ysize, temp, frac, mask = [], interp="linear", axis="Y"):
   if (len(mask) == 0):
      mask = ones(data.shape)
   if (axis != "Y"):
      xsize = ysize
      ysize = data.shape[0]
   else:
      xsize = data.shape[1]
   if (axis != "Y"):
      nx = data.shape[1]
      ys = range(ysize)
   else:
      ny = data.shape[0]
      xs = range(xsize)
   newdata = zeros((ysize,xsize))+0.
   if (axis != "Y"):
      for j in range(nx):
	if (interp == 'linear'):
           newdata[ys,temp[:,j]] += data[:,j]*(1.0-frac[:,j])*mask[:,j]
           newdata[ys,temp[:,j]+1] += data[:,j]*frac[:,j]*mask[:,j]
   else:
      for j in range(ny):
        #print j
        if (interp == 'linear'):
           newdata[temp[j,:],xs] += data[j,:]*(1.0-frac[j,:])*mask[j,:]
           newdata[temp[j,:]+1,xs] += data[j,:]*frac[j,:]*mask[j,:]
   newdata[where(newdata != 0+newdata)] = 0.
   return newdata

def drihizzle3d_full(data, z, coeffs, interp="linear"):
   if (curses.ascii.isalpha(coeffs[0])):
      f = open(coeffs, 'rb')
      temp = f.read().split('\n')
      f.close()
      temp.remove('')
      coeffs = []
      for j in range(1, len(temp)):
        coeffs.append(float(temp[j]))
   coeffs = array(coeffs)
   n = len(coeffs)
   order = 0
   nterms = 0
   while (n > 0):
      nterms += (order+1)
      n -= nterms
      if (n > 0): order+=1
   ny = data.shape[0]
   nx = data.shape[1]
   xin = arange(ny*nx, shape=(ny,nx))%nx+0.
   yin = arange(ny*nx, shape=(ny,nx))/ny+0.
   yout = zeros((ny,nx))+0.
   n = 0
   for j in range(order+1):
      for l in range(1,j+2):
	for k in range(1,l+1):
	   yout+=coeffs[n]*xin**(j-l+1)*yin**(l-k)*z**(k-1)
           n+=1
   ymin = int(yout.min())
   ymax = int(yout.max())+2
   newdata = zeros((ymax-ymin, nx))+0.
   temp = floor(yout-ymin).astype("Int32")
   frac = yout-ymin-temp
   xs = range(nx)
   for j in range(ny):
      #print j
      if (interp == 'linear'):
	newdata[temp[j,:],xs] += data[j,:]*(1.0-frac[j,:])
        newdata[temp[j,:]+1,xs] += data[j,:]*frac[j,:]
   return newdata

def linResiduals(p, x, out):
   f = (p[0]+p[1]*x)+0.
   x = x+0.
   err = out-f
   err = Numeric.array(err.tolist())
   return err

def gaussResiduals(p, x, out):
   f = zeros(len(x))+0.
   x = x+0.
   z = (x-p[1])/p[2]
   f = p[3]+p[0]*math.e**(-z**2/2)
   err = out-f
   err = Numeric.array(err.tolist())
   return err

def gaussResiduals2(p, x, out):
   f = zeros(len(x))+0.
   x = x+0.
   z = (x-p[1])/p[2]
   f = p[0]*math.e**(-z**2/2)
   err = out-f
   return err

def surfaceResiduals(p, x, y, out, order):
   f = zeros(len(x))+0.
   x = x+0.
   y = y+0.
   n = 1
   for j in range(1,order+1):
     for l in range(j+1):
        f+=p[n]*x**(j-l)*y**l
        n+=1
   err = out - f
   err = Numeric.array(err.tolist())
   return err

def polyResiduals(p, x, out, order):
   f = zeros(len(x))+0.
   x = x+0.
   for j in range(0,order+1):
     f+=p[j]*x**j
   err = out-f
   err = Numeric.array(err.tolist())
   return err

def surface3dResiduals(p, x, y, z, out, order):
   f = zeros(len(x))+0.
   n = 1
   for j in range(1,order+1):
     for l in range(1,j+2):
        for k in range(1,l+1):
           f+=p[n]*x**(j-l+1)*y**(l-k)*z**(k-1)
           n+=1
   err = out - f
   err = Numeric.array(err.tolist())
   return err

def surface3d(coeffs, x, y, z, order):
   out = zeros(len(x))+0.
   i = 0
   for j in range(order+1):
      for l in range(1,j+2):
        for k in range(1,l+1):
	   out+=coeffs[i]*x**(j-l+1)*y**(l-k)*z**(k-1)
	   i+=1
   return out

def lacos_spec(infile, outfile, outmask, gain=4.1, readn=30.0, xorder = 9, yorder = 3, sigclip = 4.5, sigfrac = 0.5, objlim = 1, niter = 1, verbose=False, logfile='none'):
   bpos = infile.find('[')
   if (os.access(outfile, os.F_OK)):
      os.unlink(outfile)
   if (os.access(outmask, os.F_OK)):
      os.unlink(outmask)
   if (bpos == -1):
      fname = infile
   else:
      fname = infile[:bpos]
   temp = pyfits.open(fname)
   if (not curses.ascii.isdigit(str(gain)[0])):
      gain = float(temp[0].header[gain])
   if (not curses.ascii.isdigit(str(readn)[0])):
      readn = float(temp[0].header[readn])
   temp.close()
   temp = 0

   # setup names for temp files
   blk = 'lacos_TEMP_blk.fits'
   if (os.access(blk, os.F_OK)): os.unlink(blk)
   lapla = 'lacos_TEMP_lapla.fits'
   if (os.access(lapla, os.F_OK)): os.unlink(lapla)
   deriv2 = 'lacos_TEMP_deriv2.fits'
   if (os.access(deriv2, os.F_OK)): os.unlink(deriv2)
   kernel = 'lacos_TEMP_kernel.dat'
   gkernel = 'lacos_TEMP_gkernel.dat'
   med3 = 'lacos_TEMP_med3.fits'
   if (os.access(med3, os.F_OK)): os.unlink(med3)
   med5 = 'lacos_TEMP_med5.fits'
   if (os.access(med5, os.F_OK)): os.unlink(med5)
   med7 = 'lacos_TEMP_med7.fits'
   if (os.access(med7, os.F_OK)): os.unlink(med7)
   sub5 = 'lacos_TEMP_sub5.dat'
   sub5abs = 'lacos_TEMP_sub5abs.dat'
   imstatout = 'lacos_TEMP_imstatout.dat'
   noise = 'lacos_TEMP_noise.fits'
   if (os.access(noise, os.F_OK)): os.unlink(noise)
   sigmap = 'lacos_TEMP_sigmap.fits'
   if (os.access(sigmap, os.F_OK)): os.unlink(sigmap)
   firstsel = 'lacos_TEMP_firstsel.fits'
   if (os.access(firstsel, os.F_OK)): os.unlink(firstsel)
   starreject = 'lacos_TEMP_starreject.fits'
   if (os.access(starreject, os.F_OK)): os.unlink(starreject)
   gfirstsel = 'lacos_TEMP_gfirstsel.fits'
   if (os.access(gfirstsel, os.F_OK)): os.unlink(gfirstsel)
   finalsel = 'lacos_TEMP_finalsel.fits'
   if (os.access(finalsel, os.F_OK)): os.unlink(finalsel)
   inputmask = 'lacos_TEMP_inputmask.fits'
   if (os.access(inputmask, os.F_OK)): os.unlink(inputmask)
   oldoutput = 'lacos_TEMP_oldoutput.fits'
   if (os.access(oldoutput, os.F_OK)):
      os.unlink(oldoutput)
   sigma = 'lacos_TEMP_sigma.dat'
   galaxy = 'lacos_TEMP_galaxy.fits'
   if (os.access(galaxy, os.F_OK)):
      os.unlink(galaxy)
   skymod = 'lacos_TEMP_skymod.fits'
   if (os.access(skymod, os.F_OK)):
      os.unlink(skymod)

   # set some parameters in standard IRAF tasks
   iraf.images.imfilter.convolve.bilinear = 'no'
   iraf.images.imfilter.convolve.radsym = 'no'

   # create Laplacian kernel
   f = open(kernel, 'wb')
   f.write('0 -1 0;\n')
   f.write('-1 4 -1;\n')
   f.write('0 -1 0;\n')
   f.close()

   # create growth kernel
   f = open(gkernel, 'wb')
   f.write('1 1 1;\n')
   f.write('1 1 1;\n')
   f.write('1 1 1;\n')
   f.close()

   # initialize loop
   i = 1
   stop = False
   previous = 0
   iraf.images.imutil.imcopy(infile, oldoutput, verb="no")
   temp = pyfits.open(oldoutput)
   temp[0].data[:,:] = 0.
   temp.writeto(outmask)
   temp.close()
   temp = 0

   #subtract object spectra if desired
   if (xorder > 0):
      if (verbose): print "   Subtracting object spectra..."
      iraf.images.imfit.fit1d(oldoutput,galaxy,"fit",axis=1,order=xorder,func="leg",low=4.,high=4.,nav=1,inter="no",sample="*",niter=3,grow=0,cursor="")
      temp = pyfits.open(oldoutput,"update")
      tempgxy = pyfits.open(galaxy)
      temp[0].data-=tempgxy[0].data
      temp.flush()
      temp.close()
      temp = 0
   else:
      tempgxy = pyfits.open(oldoutput)
      tempgxy[0].data[:,:] = 0.
      tempgxy.writeto(galaxy)

   #subtract sky lines
   if (yorder > 0):
      if (verbose): print "   Subtracting sky lines..."
      iraf.images.imfit.fit1d(oldoutput,skymod,"fit",axis=2,order=yorder,func="leg",low=4.,high=4.,inter="no",sample="*",nav=1,niter=3,grow=0,cursor="")
      temp = pyfits.open(oldoutput,"update")
      tempsmod = pyfits.open(skymod)
      temp[0].data-=tempsmod[0].data
      temp.flush()
      temp.close()
      temp = 0
   else:
      tempsmod = pyfits.open(oldoutput)
      tempsmod[0].data[:,:] = 0.
      tempsmod.writeto(skymod)

   #add object spectra to sky model
   tempsmod[0].data+=tempgxy[0].data
   tempgxy.close()
   tempgxy = 0
   if (os.access(skymod, os.F_OK)): os.unlink(skymod)
   tempsmod.writeto(skymod)

   #start iterations
   while (not stop):
      print "   Iteration ",i,"..."
      if (os.access(logfile, os.F_OK)):
	f = open(logfile,'ab')
	f.write('\tIteration '+str(i)+'...\n')
	f.close()
      #add median of residuals to sky model
      iraf.images.imfilter.median(oldoutput,med5,5,5,zlor='INDEF',zhir='INDEF',verb="no")
      temp = pyfits.open(med5,"update")
      temp[0].data+=tempsmod[0].data
      temp.flush()
      temp.close()
      temp = 0
      tempsmod.close()
      tempsmod = 0
      # take second-order derivative (Laplacian) of input image
      # kernel is convolved with subsampled image, in order to remove negative
      # pattern around high pixels
      if (verbose): print "   Convolving with Laplacian Kernel..."
      iraf.images.imgeom.blkrep(oldoutput,blk,2,2)
      iraf.images.imfilter.convolve(blk,lapla,kernel)
      temp = pyfits.open(lapla,"update")
      temp[0].data*=where(temp[0].data >= 0, 1, 0)
      temp.flush()
      temp.close()
      temp = 0
      iraf.images.imgeom.blkavg(lapla,deriv2,2,2,option="average")
      #create noise model
      if (verbose): print "   Creating noise model..."
      temp = pyfits.open(med5)
      temp[0].data = temp[0].data.astype('Float64')
      temp[0].data = sqrt(abs(temp[0].data*gain+readn**2+0.))/gain
      temp.writeto(noise)
      temp.close()
      temp = pyfits.open(med5,"update")
      b = where(temp[0].data < 0)
      temp[0].data[b] = 0.00001
      temp.flush()
      temp.close()
      temp = 0
      #divide Laplacian by noise model
      temp = pyfits.open(deriv2)
      tempns = pyfits.open(noise)
      temp[0].data/=tempns[0].data
      temp[0].data/=2.
      temp.writeto(sigmap)
      tempns.close()
      tempns = 0
      #Laplacian of blkreplicated image counts edges twice
      temp.close()
      temp = 0
      #removal of large structure (bright, extended objects)
      os.unlink(med5)
      iraf.images.imfilter.median(sigmap,med5,5,5,zlo='INDEF',zhi='INDEF',verb="no")
      tempm5 = pyfits.open(med5)
      tempsig = pyfits.open(sigmap,"update")
      tempsig[0].data-=tempm5[0].data
      tempsig.flush()
      tempsig.close()
      tempsig = 0
      tempm5.close()
      tempm5 = 0
      #find all candidate cosmic rays
      #this selection includes sharp features such as stars and HII regions
      if (verbose): print "   Selecting candidate cosmic rays..."
      #iraf.images.imutil.imcopy(sigmap,firstsel,verb="no")
      #iraf.images.imutil.imreplace(firstsel,0,upper=sigclip,lower='INDEF')
      #iraf.images.imutil.imreplace(firstsel,1,lower=0.1,upper='INDEF')
      temp = pyfits.open(sigmap)
      temp[0].data*=where(temp[0].data > sigclip, 1, 0)
      temp[0].data = temp[0].data.astype('Float64')
      b = where(temp[0].data > 0.1)
      temp[0].data[b] = 1.
      temp.writeto(firstsel)
      temp.close()
      temp = 0
      #iraf.images.imutil.imreplace(firstsel,1,lower=0.1,upper='INDEF')
      #compare candidate CRs to median filtered image
      #this step rejects bright, compact sources from the initial CR list
      if (verbose): print "   Removing suspected emission lines..."
      #subtract background and smooth component of objects
      iraf.images.imfilter.median(oldoutput,med3,3,3,zlo='INDEF',zhi='INDEF',verb='no')
      iraf.images.imfilter.median(med3,med7,7,7,zlo='INDEF',zhi='INDEF',verb='no')
      tempm3 = pyfits.open(med3,"update")
      tempm7 = pyfits.open(med7)
      tempm3[0].data = tempm3[0].data.astype('Float64')
      tempm3[0].data-=tempm7[0].data
      tempm7.close()
      tempm7 = 0
      tempns = pyfits.open(noise)
      tempm3[0].data/=tempns[0].data
      tempns.close()
      tempns = 0
      b = where(tempm3[0].data < 0.01)
      tempm3[0].data[b] = 0.01
      tempm3.flush()
      #compare CR flux to object flux
      tempfs = pyfits.open(firstsel,"update")
      tempsigstar = pyfits.open(sigmap)
      tempsigstar[0].data = tempsigstar[0].data.astype('Float64')
      tempsigstar[0].data = (tempfs[0].data*tempsigstar[0].data)/tempm3[0].data
      tempm3.close()
      tempm3 = 0
      #discard if CR flux <= objlim * object flux
      tempsigstar[0].data*=where(tempsigstar[0].data > objlim, 1, 0)
      b = where(tempsigstar[0].data > 0.5)
      tempsigstar[0].data[b] = 1.
      tempsigstar.writeto(starreject)
      tempfs[0].data*=tempsigstar[0].data
      tempfs.flush()
      tempsigstar.close()
      tempsigstar = 0
      tempfs.close()
      tempfs = 0
      #grow CRs by one pixel and check in original sigma map
      iraf.images.imfilter.convolve(firstsel,gfirstsel,gkernel)
      tempgf = pyfits.open(gfirstsel,"update")
      tempgf[0].data = tempgf[0].data.astype('Float64')
      b = where(tempgf[0].data > 0.5)
      tempgf[0].data[b] = 1.
      tempsm = pyfits.open(sigmap)
      tempgf[0].data*=tempsm[0].data
      tempgf[0].data*=where(tempgf[0].data > sigclip, 1, 0)
      b = where(tempgf[0].data > 0.1)
      tempgf[0].data[b] = 1.
      tempgf.flush()
      tempgf.close()
      tempgf = 0
      #grow CRs by one pixel and lower detection limit
      sigcliplow = sigfrac*sigclip
      if (verbose): print "   Finding neighboring pixels affected by cosmic rays..."
      iraf.images.imfilter.convolve(gfirstsel,finalsel,gkernel)
      tempfs = pyfits.open(finalsel, "update")
      tempfs[0].data = tempfs[0].data.astype('Float64')
      b = where(tempfs[0].data > 0.5)
      tempfs[0].data[b] = 1.
      tempfs[0].data*=tempsm[0].data
      tempsm.close()
      tempsm = 0
      tempfs[0].data*=where(tempfs[0].data > sigclip, 1, 0)
      b = where(tempfs[0].data > 0.1)
      tempfs[0].data[b] = 1.
      tempfs.flush()
      #determine number of CRs found in this iteration
      os.unlink(gfirstsel)
      tempom = pyfits.open(outmask, "update")
      tempfs[0].data = (1-tempom[0].data)*tempfs[0].data
      tempfs.writeto(gfirstsel)
      b = where(tempfs[0].data >= 0.5)
      npix = len(b[0])
      tempfs.close()
      tempfs = 0
      #create cleaned output image; use 3x3 median with CRs excluded
      if (verbose): print "   Creating output..."
      os.unlink(med5)
      tempfs = pyfits.open(finalsel)
      tempom[0].data = tempom[0].data.astype('Float64')
      tempom[0].data+=tempfs[0].data
      tempfs.close()
      tempfs = 0
      b = where(tempom[0].data > 1)
      tempom[0].data[b] = 1.
      tempom.flush()
      tempoo = pyfits.open(oldoutput)
      tempoo[0].data = (1-10000.*tempom[0].data)*tempoo[0].data
      tempoo.writeto(inputmask)
      tempoo.close()
      tempoo = 0
      iraf.images.imfilter.median(inputmask,med5,5,5,zloreject=-9999,zhi='INDEF',verb="no")
      tempm5 = pyfits.open(med5,"update")
      tempm5[0].data*=tempom[0].data
      tempm5.flush()
      if (i > 1): os.unlink(outfile)
      tempout = pyfits.open(oldoutput)
      tempout[0].data = (1-tempom[0].data)*tempout[0].data+tempm5[0].data
      tempm5.close()
      tempm5 = 0
      tempom.close()
      tempom = 0
      #add sky and object spectra back in
      os.unlink(oldoutput)
      tempout.writeto(oldoutput)
      tempsm = pyfits.open(skymod)
      tempout[0].data+=tempsm[0].data
      tempout.writeto(outfile)
      tempsm.close()
      tempsm = 0
      tempout.close()
      tempout = 0
      #clean up and get ready for next iteration
      print "   Found ",npix," cosmic rays..."
      if (os.access(logfile, os.F_OK)):
        f = open(logfile,'ab')
        f.write('\tFound '+str(npix)+' cosmic rays...\n')
        f.close()
      if (npix == 0): stop = True
      i+=1
      if (i > niter): stop = True
      #delete temp files
      os.unlink(blk)
      os.unlink(lapla)
      os.unlink(deriv2)
      os.unlink(med5)
      os.unlink(med3)
      os.unlink(med7)
      os.unlink(noise)
      os.unlink(sigmap)
      os.unlink(firstsel)
      os.unlink(starreject)
      os.unlink(gfirstsel)
      os.unlink(finalsel)
      os.unlink(inputmask)
      os.unlink(galaxy)
      os.unlink(gkernel)
      os.unlink(kernel)
      os.unlink(oldoutput)
      os.unlink(skymod)

def isValidFilter(filter):
   invalid = ['open','clear','none']
   if (invalid.count(filter.lower()) > 0):
      return False
   return True
