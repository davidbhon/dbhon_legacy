#!/usr/bin/python -u
import math, pyfits, sys, os
from numarray import *
import numarray.strings
from numarray.image.combine import * 
from UFPipelineOps import *
from pyraf import iraf
from iraf import stsdas
from iraf import dither 
import curses.ascii
import glob
from datetime import datetime

class flam2pipeline:

   #Members:
   #list filenames, symLinks, exptimes, filters, darkfiles, darktimes
   #list flatfiles, flattimes, flatfilters 
   #badPixelMask bpMask
   #dict params
   #String prefix, delim, bpmFile
   #bool useBPMask

   def __init__(self, infile, logfile, warnfile, guiMessages):
      self.guiMessages = guiMessages
      if (not os.access('.', os.W_OK)):
        print "ERROR: No write access to current working directory!"
        if (self.guiMessages):
           print "GUI_ERROR: No write access to current working directory!"
        sys.exit()
      if (self.guiMessages):
        print "STATUS: Initialization..."
        print "PROGRESS: 0"
      self.prefix = ""
      self.useBPMask = False 
      self.bpMask = []
      #Read input parameter file
      f = open(infile, "rb")
      self.params = f.read()
      f.close()
      #Setup log, warning files
      self.logfile = logfile
      self.warnfile = warnfile
      f = open(self.logfile,'wb')
      f.write('LOG of '+infile+' at ' +  str(datetime.today()) + '\n')
      f.write('Run at path: '+os.getcwd()+'\n')
      f.write('--------Parameter List--------\n')
      f.write(self.params)
      f.write('------------------------------\n')
      f.close()
      self.params = self.params.split("\n")
      try: self.params.remove('') 
      except ValueError:
        dummy = ''
      for j in range(len(self.params)-1, -1, -1):
	if (self.params[j].strip() == ''): self.params.pop(j)
	elif (not curses.ascii.isalpha(self.params[j][0])): self.params.pop(j)
      self.params = dict([(j[0:j.find("=")].strip().upper(), j[j.find("=")+1:].strip()) for j in self.params]) 
      self.setDefaultParams()
      self.delim = self.params['PREFIX_DELIMITER']
      f = open(self.warnfile,'wb')
      f.write('Warnings and errors for '+infile+' at '+ str(datetime.today())+'\n')
      f.close()
      #Create Filelist
      filelist = self.params['FILES']
      if (filelist.endswith(',')):
	filelist = filelist[:-1]
      if (os.path.isdir(filelist)):
	self.filenames = os.listdir(filelist)
	if (filelist[-1] != '/'):
	   filelist += '/'
	for j in range(len(self.filenames)):
	   self.filenames[j] = filelist+self.filenames[j]
      elif (os.path.isfile(filelist) and not filelist.lower().endswith('.fits') and not filelist.lower().endswith('.fit')):
        f = open(filelist, "rb")
        self.filenames = f.read()
        f.close()
        self.filenames = self.filenames.split()
      elif (filelist.count(',') != 0):
        self.filenames = filelist.split(',')
	try: self.filenames.remove('')
	except ValueError:
	   dummy = ''
	for j in range(len(self.filenames)):
	   self.filenames[j] = self.filenames[j].strip()
      else:
        self.filenames = glob.glob(filelist)
      self.filenames.sort()
      print "Found ", len(self.filenames), " files..."
      f = open(self.logfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      f = open(self.warnfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      self.readAll()
      if (self.guiMessages): print "PROGRESS: 5"

   def setDefaultParams(self):
      #Set Default Parameters
      self.params.setdefault('FILES','files.dat')
      self.params.setdefault('GROUP_OUTPUT_BY','filename')
      self.params.setdefault('SPECIFY_GROUPING','')
      self.params.setdefault('MEF_EXTENSION','')
      if (self.params['MEF_EXTENSION'] == ''):
        self.params['MEF_EXTENSION']='-1'
      if (not curses.ascii.isdigit(self.params['MEF_EXTENSION'][0])):
	self.params['MEF_EXTENSION']='-1'
      try:
	self.params['MEF_EXTENSION'] = int(self.params['MEF_EXTENSION'])
      except ValueError:
	self.params['MEF_EXTENSION'] = -1
      self.params.setdefault('DARK_FILE_LIST','')
      self.params.setdefault('FLAT_FILE_LIST','')
      self.params.setdefault('OVERWRITE_FILES','no')
      self.params.setdefault('QUICK_START_FILE','')
      self.params.setdefault('CLEAN_UP','no')
      self.params.setdefault('PREFIX_DELIMITER','.')
      self.params.setdefault('EXPTIME_KEYWORD','EXP_TIME')
      self.params.setdefault('OBSTYPE_KEYWORD','OBS_TYPE')
      self.params.setdefault('FILTER_KEYWORD','FILTER')
      self.params['FILTER_KEYWORD'] = self.params['FILTER_KEYWORD'].replace(' ','').split(',')
      self.params.setdefault('RA_KEYWORD','RA')
      self.params.setdefault('DEC_KEYWORD','DEC')
      self.params.setdefault('RELATIVE_OFFSET_ARCSEC','no')
      self.params.setdefault('PIXSCALE_KEYWORD','PIXSCALE')
      self.params.setdefault('ROT_PA_KEYWORD','ROT_PA')
      self.params.setdefault('PIXSCALE_UNITS','arcsec')
      self.params.setdefault('UT_KEYWORD','UTC')
      self.params.setdefault('DATE_KEYWORD','DATE-OBS')
      self.params.setdefault('MIN_FRAME_VALUE','0')
      self.params.setdefault('MAX_FRAME_VALUE','0')
      if (self.params['MIN_FRAME_VALUE'] == ''):
	self.params['MIN_FRAME_VALUE'] = '0'
      if (self.params['MAX_FRAME_VALUE'] == ''):
        self.params['MAX_FRAME_VALUE'] = '0'
      self.params['MIN_FRAME_VALUE'] = float(self.params['MIN_FRAME_VALUE'])
      self.params['MAX_FRAME_VALUE'] = float(self.params['MAX_FRAME_VALUE'])
      self.params.setdefault('IGNORE_FIRST_FRAMES','no')
      self.params.setdefault('IGNORE_AFTER_BAD_READ','no')
      self.params.setdefault('DO_LINEARIZE','yes')
      self.params.setdefault('LINEARITY_COEFFS','1')
      self.params['LINEARITY_COEFFS'] = self.params['LINEARITY_COEFFS'].split()
      for j in range(len(self.params['LINEARITY_COEFFS'])):
	self.params['LINEARITY_COEFFS'][j] = float(self.params['LINEARITY_COEFFS'][j])
      self.params.setdefault('DO_DARK_COMBINE','yes')
      self.params.setdefault('DO_DARK_SUBTRACT','yes')
      self.params.setdefault('DEFAULT_MASTER_DARK','')
      if (self.params['DEFAULT_MASTER_DARK'].count('.dat') != 0):
        f = open(self.params['DEFAULT_MASTER_DARK'],'rb')
        s = f.read().split('\n')
        f.close()
	try: s.remove('')
        except ValueError:
           dummy = ''
        self.params['DEFAULT_MASTER_DARK'] = s
      elif (self.params['DEFAULT_MASTER_DARK'].count(',') != 0):
        self.params['DEFAULT_MASTER_DARK'] = self.params['DEFAULT_MASTER_DARK'].split(',')
        for j in range(len(self.params['DEFAULT_MASTER_DARK'])):
           self.params['DEFAULT_MASTER_DARK'][j] = self.params['DEFAULT_MASTER_DARK'][j].strip()
      else:
        self.params['DEFAULT_MASTER_DARK'] = self.params['DEFAULT_MASTER_DARK'].split()
      self.params.setdefault('PROMPT_FOR_MISSING_DARK','yes')
      self.params.setdefault('DO_FLAT_COMBINE','yes')
      self.params.setdefault('FLAT_TYPE','dome')
      self.params.setdefault('FLAT_METHOD_DOME','on-off')
      self.params.setdefault('FLAT_LAMP_OFF_FILES','off')
      self.params.setdefault('FLAT_SKY_REJECT_TYPE','none')
      self.params.setdefault('FLAT_SKY_NLOW','1')
      self.params.setdefault('FLAT_SKY_NHIGH','1')
      self.params.setdefault('FLAT_SKY_LSIGMA','5')
      self.params.setdefault('FLAT_SKY_HSIGMA','5')
      self.params.setdefault('DO_BAD_PIXEL_MASK','yes')
      self.params.setdefault('DEFAULT_BAD_PIXEL_MASK','')
      if (self.params['DEFAULT_BAD_PIXEL_MASK'].count('.dat') != 0):
	f = open(self.params['DEFAULT_BAD_PIXEL_MASK'],'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           dummy = ''
	self.params['DEFAULT_BAD_PIXEL_MASK'] = s
      elif (self.params['DEFAULT_BAD_PIXEL_MASK'].count(',') != 0):
	self.params['DEFAULT_BAD_PIXEL_MASK'] = self.params['DEFAULT_BAD_PIXEL_MASK'].split(',')
	for j in range(len(self.params['DEFAULT_BAD_PIXEL_MASK'])):
	   self.params['DEFAULT_BAD_PIXEL_MASK'][j] = self.params['DEFAULT_BAD_PIXEL_MASK'][j].strip()
      else:
	self.params['DEFAULT_BAD_PIXEL_MASK'] = self.params['DEFAULT_BAD_PIXEL_MASK'].split()
      self.params.setdefault('BAD_PIXEL_MASK_CLIPPING','values')
      self.params.setdefault('BAD_PIXEL_HIGH','2.0')
      self.params.setdefault('BAD_PIXEL_LOW','0.5')
      self.params.setdefault('BAD_PIXEL_SIGMA','5')
      self.params.setdefault('EDGE_REJECT','5')
      self.params.setdefault('RADIUS_REJECT','0')
      self.params['BAD_PIXEL_HIGH'] = float(self.params['BAD_PIXEL_HIGH'])
      self.params['BAD_PIXEL_LOW'] = float(self.params['BAD_PIXEL_LOW'])
      self.params['BAD_PIXEL_SIGMA'] = float(self.params['BAD_PIXEL_SIGMA'])
      self.params['EDGE_REJECT'] = int(self.params['EDGE_REJECT'])
      self.params['RADIUS_REJECT'] = float(self.params['RADIUS_REJECT'])
      self.params.setdefault('DO_FLAT_DIVIDE','yes')
      self.params.setdefault('DEFAULT_MASTER_FLAT','')
      if (self.params['DEFAULT_MASTER_FLAT'].count('.dat') != 0):
        f = open(self.params['DEFAULT_MASTER_FLAT'],'rb')
        s = f.read().split('\n')
        f.close()
	try: s.remove('')
        except ValueError:
           dummy = ''
        self.params['DEFAULT_MASTER_FLAT'] = s
      elif (self.params['DEFAULT_MASTER_FLAT'].count(',') != 0):
        self.params['DEFAULT_MASTER_FLAT'] = self.params['DEFAULT_MASTER_FLAT'].split(',')
        for j in range(len(self.params['DEFAULT_MASTER_DARK'])):
           self.params['DEFAULT_MASTER_FLAT'][j] = self.params['DEFAULT_MASTER_FLAT'][j].strip()
      else:
        self.params['DEFAULT_MASTER_FLAT'] = self.params['DEFAULT_MASTER_FLAT'].split()
      self.params.setdefault('DO_SKY_SUBTRACT','yes')
      self.params.setdefault('SKY_SUBTRACT_METHOD','remove_objects')
      self.params.setdefault('SKY_REJECT_TYPE','sigclip')
      self.params.setdefault('SKY_NLOW','1')
      self.params.setdefault('SKY_NHIGH','1')
      self.params.setdefault('SKY_LSIGMA','5')
      self.params.setdefault('SKY_HSIGMA','5')
      self.params.setdefault('USE_SKY_FILES','all')
      self.params.setdefault('SKY_FILES_RANGE','3')
      self.params.setdefault('SKY_DITHERING_RANGE','2')
      self.params.setdefault('SKY_OFFSOURCE_RANGE','240')
      if (self.params['SKY_FILES_RANGE'] == ''):
	self.params['SKY_FILES_RANGE'] = '3'
      if (self.params['SKY_DITHERING_RANGE'] == ''):
	self.params['SKY_DITHERING_RANGE'] = '2'
      if (self.params['SKY_OFFSOURCE_RANGE'] == ''):
	self.params['SKY_OFFSOURCE_RANGE'] = '240'
      self.params['SKY_FILES_RANGE'] = int(self.params['SKY_FILES_RANGE'])
      self.params['SKY_DITHERING_RANGE'] = int(self.params['SKY_DITHERING_RANGE'])
      self.params['SKY_OFFSOURCE_RANGE'] = int(self.params['SKY_OFFSOURCE_RANGE'])
      self.params.setdefault('SKY_OFFSOURCE_METHOD','auto')
      self.params.setdefault('SKY_OFFSOURCE_MANUAL','')
      #Backwards Compatibility
      if (self.params.has_key('KEEP_OFFSOURCE_SKIES') and not self.params.has_key('KEEP_SKIES')): self.params['KEEP_SKIES'] = self.params['KEEP_OFFSOURCE_SKIES']
      self.params.setdefault('KEEP_OFFSOURCE_SKIES','no')
      self.params.setdefault('KEEP_SKIES','no') 
      self.params.setdefault('SKY_NEB_FILE_RANGE','5')
      self.params['SKY_NEB_FILE_RANGE']=int(self.params['SKY_NEB_FILE_RANGE'])
      self.params.setdefault('SEXTRACTOR_PATH','sex')
      self.params.setdefault('SEXTRACTOR_CONFIG_PATH', 'sextractor-2.3.2/config')
      self.params.setdefault('SEXTRACTOR_CONFIG_PREFIX','default')
      self.params.setdefault('TWO_PASS_OBJECT_MASKING','no')
      self.params.setdefault('TWO_PASS_DETECT_THRESH','10')
      self.params.setdefault('TWO_PASS_DETECT_MINAREA','50')
      self.params.setdefault('TWO_PASS_BOXCAR_SIZE','51')
      self.params.setdefault('TWO_PASS_REJECT_LEVEL','0.95')
      self.params['TWO_PASS_BOXCAR_SIZE'] = int(self.params['TWO_PASS_BOXCAR_SIZE'])
      self.params['TWO_PASS_REJECT_LEVEL'] = float(self.params['TWO_PASS_REJECT_LEVEL'])
      self.params.setdefault('INTERP_ZEROS_SKY', 'no')
      self.params.setdefault('REMOVE_COSMIC_RAYS','no')
      self.params.setdefault('COSMIC_RAY_PASSES','3')
      self.params['COSMIC_RAY_PASSES'] = int(self.params['COSMIC_RAY_PASSES'])
      self.params.setdefault('FIT_SKY_SUBTRACTED_SURF','no')
      self.params.setdefault('DO_ALIGN_STACK','yes')
      self.params.setdefault('TWO_PASS_ALIGNMENT','no')
      self.params.setdefault('XYXYMATCH_TOLERANCE','3.0')
      self.params['XYXYMATCH_TOLERANCE'] = float(self.params['XYXYMATCH_TOLERANCE'])
      self.params.setdefault('ALIGN_BOX_SIZE','256')
      self.params.setdefault('ALIGN_BOX_CENTER_X','1024')
      self.params.setdefault('ALIGN_BOX_CENTER_Y','1024')
      self.params['ALIGN_BOX_SIZE'] = int(self.params['ALIGN_BOX_SIZE'])
      self.params['ALIGN_BOX_CENTER_X']=int(self.params['ALIGN_BOX_CENTER_X'])
      self.params['ALIGN_BOX_CENTER_Y']=int(self.params['ALIGN_BOX_CENTER_Y'])
      self.params.setdefault('DRIZZLE_DROPSIZE','0.01')
      self.params['DRIZZLE_DROPSIZE'] = float(self.params['DRIZZLE_DROPSIZE'])
      self.params.setdefault('GEOM_TRANS_COEFFS','')
      self.params.setdefault('DRIZZLE_KERNEL','point')
      self.params.setdefault('DRIZZLE_IN_UNITS','counts')
      self.params.setdefault('DRIZZLE_OR_IMCOMBINE','drizzle')
      self.params.setdefault('KEEP_INDIV_IMAGES','no')
      self.params.setdefault('STACK_REJECT_TYPE','sigclip')
      self.params.setdefault('STACK_NLOW','1')
      self.params.setdefault('STACK_NHIGH','1')
      self.params.setdefault('STACK_LSIGMA','3')
      self.params.setdefault('STACK_HSIGMA','3')

   def readAll(self):
      #Read Files, setup arrays
      self.darkfiles = []
      self.darktimes = []
      self.flatfiles = []
      self.flattimes = []
      self.flatfilters = []
      self.exptimes = []
      self.filters = []
      self.symLinks = []
      delFiles = []
      lastPrefix = ""
      currExp = 0
      manDarks = False
      manFlats = False
      qsfilenames = []
      qsmedians = []
      if (os.access(self.params['QUICK_START_FILE'], os.F_OK)):
	f = open(self.params['QUICK_START_FILE'], 'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           dummy = ''
	for j in range(len(s)):
	   qsfilenames.append(s[j].split()[0])
	   qsmedians.append(float(s[j].split()[1]))
	qsfilenames = numarray.strings.array(qsfilenames)
      if (os.access(self.params['DARK_FILE_LIST'], os.F_OK) and self.params['DARK_FILE_LIST'].lower().count('.fit') > 0):
        tmpdarks = [self.params['DARK_FILE_LIST']]
        manDarks = True
        if (tmpdarks[0].find('/') != -1):
           tmpdarks[0] = tmpdarks[0].replace('/','_')
      elif (os.access(self.params['DARK_FILE_LIST'], os.F_OK)):
	f = open(self.params['DARK_FILE_LIST'], 'rb')
	tmpdarks = f.read().split('\n')
	f.close()
        try: tmpdarks.remove('')
        except ValueError:
           dummy = ''
	manDarks = True
      elif (self.params['DARK_FILE_LIST'].count(',') != 0):
        tmpdarks = self.params['DARK_FILE_LIST'].split(',')
        manDarks = True
        try: tmpdarks.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(tmpdarks)):
           tmpdarks[j] = tmpdarks[j].strip()
           if (tmpdarks[j].find('/') != -1):
              tmpdarks[j] = tmpdarks[j].replace('/','_')
      if (os.access(self.params['FLAT_FILE_LIST'], os.F_OK) and self.params['FLAT_FILE_LIST'].lower().count('.fit') > 0):
        tmpflats = [self.params['FLAT_FILE_LIST']]
        manFlats = True
        if (tmpflats[0].find('/') != -1):
           tmpflats[0] = tmpflats[0].replace('/','_')
      elif (os.access(self.params['FLAT_FILE_LIST'], os.F_OK)):
        f = open(self.params['FLAT_FILE_LIST'], 'rb')
        tmpflats = f.read().split('\n')
        f.close()
	try: tmpflats.remove('')
        except ValueError:
           dummy = ''
        manFlats = True
      elif (self.params['FLAT_FILE_LIST'].count(',') != 0):
        tmpflats = self.params['FLAT_FILE_LIST'].split(',')
        manFlats = True
        try: tmpflats.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(tmpflats)):
           tmpflats[j] = tmpflats[j].strip()
           if (tmpflats[j].find('/') != -1):
              tmpflats[j] = tmpflats[j].replace('/','_')
      if (os.access(self.params['FLAT_TYPE'], os.F_OK)):
	f = open(self.params['FLAT_TYPE'], 'rb')
	temp = f.read().split('\n')
	f.close()
	try: temp.remove('')
        except ValueError:
           dummy = ''
	tmpflattypes = []
	for j in range(len(temp)):
	   tmpflattypes.append(temp[j].split())
	self.params['FLAT_TYPE'] = 'mix'
      self.flattypes = []
      self.isSkyFlat = []
      groupType = 'filename'
      if (self.params['GROUP_OUTPUT_BY'].lower() == 'from manual file' and os.access(self.params['SPECIFY_GROUPING'], os.F_OK)):
	groupType = 'manual'
	f = open(self.params['SPECIFY_GROUPING'], 'rb')
	groupManual = f.read().split('\n')
	f.close()
	for l in range(groupManual.count('')):
	   try: groupManual.remove('')
	   except ValueError:
	      dummy = ''
      elif (self.params['GROUP_OUTPUT_BY'].lower() == 'fits keyword'):
	try:
	   temp = pyfits.open(self.filenames[0])
	   if (temp[0].header.has_key(self.params['SPECIFY_GROUPING'])):
	      groupType = 'keyword'
	   temp.close()
	except Exception:
	   groupType = 'filename'
      for j in range(len(self.filenames)):
	if (self.filenames[j].endswith('.gz')):
	   if (os.access(self.filenames[j], os.W_OK)):
	      os.system('gunzip '+self.filenames[j])
	      self.filenames[j] = self.filenames[j][:-3]
	   else:
	      if (not os.access("unzipped", os.F_OK)):
		os.mkdir("unzipped",0755)
	      newfilename = 'unzipped/'+self.filenames[j][self.filenames[j].rfind('/')+1:-3]
	      os.system('gunzip -c '+self.filenames[j]+' > '+newfilename)
	      self.filenames[j] = newfilename 
	if (self.filenames[j].find('/') != -1 or groupType != 'filename'):
	   newname = self.filenames[j].replace('/','_')
	   if (groupType == 'manual'):
	      for l in range(len(groupManual)):
		mangrp = groupManual[l].split()
		sindex = mangrp[1] 
		mangrp[1] = int(mangrp[1])
		mangrp[2] = int(mangrp[2])
		if (len(mangrp) > 4):
		   mangrp[4] = int(mangrp[4])-mangrp[1]
		fileprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
		fileindex = int(self.filenames[j][self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))+len(self.delim):self.filenames[j].rfind('.')])
		if (fileprefix.find(mangrp[0]) != -1 and fileindex >= mangrp[1] and fileindex <= mangrp[2]):
		   if (len(mangrp) > 4):
		      newfi = str(mangrp[4]+fileindex)
		      zeros = '0000'
		      newfi = zeros[len(newfi):]+newfi
		      newname = mangrp[3]+'.'+newfi+'.fits'
		   elif (len(mangrp) > 3):
		      newname = mangrp[3]+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
		   else:
		      newname = fileprefix.replace('/','_')+'.'+sindex+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
	   elif (groupType == 'keyword'):
	      temp = pyfits.open(self.filenames[j])
	      if (temp[0].header.has_key(self.params['SPECIFY_GROUPING'])):
                fileprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
		newname = fileprefix.replace('/','_')+'_'+str(temp[0].header[self.params['SPECIFY_GROUPING']]).replace(' ','_')+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
	      temp.close()
           if (len(newname) > 40):
	      newname = newname[-40:]
	      newname = newname[newname.index('_')+1:]
	      while (newname[0] == '.' or newname[0] == '_'):
		newname = newname[1:]
	   if (not os.access(newname, os.F_OK)):
	      os.symlink(self.filenames[j], newname) 
	   if (self.filenames.count(newname) > 0):
	      print 'WARNING: Filename '+newname+' already exists!'
              f = open(self.logfile,'ab')
              f.write('WARNING: Filename '+newname+' already exists!\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write('WARNING: Filename '+newname+' already exists!\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   self.filenames[j] = newname 
	   self.symLinks.append(newname)
      keepNext = True
      makeqs = False
      if (self.params['QUICK_START_FILE'] != ''):
        if (not os.access(self.params['QUICK_START_FILE'], os.F_OK)):
           mode = 'wb'
        else:
           mode = 'ab'
        if (self.params['MIN_FRAME_VALUE'] != 0 or self.params['MAX_FRAME_VALUE'] != 0):
           qsfile = open(self.params['QUICK_START_FILE'], mode)
           makeqs = True
      startPrefix = ''
      if (not os.access(self.filenames[0], os.F_OK)):
	if (self.params['DO_DARK_COMBINE'].lower() == "yes" and os.access('linearized/lin_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'linearized/lin_'
	elif (self.params['DO_DARK_SUBTRACT'].lower() == "yes" and os.access('linearized/lin_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'linearized/lin_'
	elif (self.params['DO_FLAT_COMBINE'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'darkSubtracted/ds_'
	elif (self.params['DO_BAD_PIXEL_MASK'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'darkSubtracted/ds_'
	elif (self.params['DO_FLAT_DIVIDE'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'darkSubtracted/ds_'
	elif (self.params['DO_SKY_SUBTRACT'].lower() == "yes" and os.access('flatDivided/fd_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'flatDivided/fd_'
	elif (self.params['DO_ALIGN_STACK'].lower() == "yes" and os.access('skySubtracted/ss_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'skySubtracted/ss_'
      guiCount = 0.0
      defExp = 0
      self.mef = self.params['MEF_EXTENSION']
      for j in self.filenames:
	if (keepNext == True):
	   keep = True
	else:
	   keep = False
	   keepNext = True
        if (os.access(startPrefix+j, os.F_OK) and not os.access(startPrefix+j, os.W_OK)):
           if (not os.access("copies", os.F_OK)): os.mkdir("copies",0755)
           temp = pyfits.open(startPrefix+j)
           temp.verify('silentfix')
           temp.writeto('copies/'+j)
           temp.close()
           os.unlink(startPrefix+j)
           os.symlink('copies/'+j, startPrefix+j)
           print 'File '+startPrefix+j+' is not writeable.  Making local copy.'
           f = open(self.logfile,'ab')
           f.write('File '+startPrefix+j+' is not writeable.  Making local copy.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('File '+startPrefix+j+' is not writeable.  Making local copy.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
        try:
           temp = pyfits.open(startPrefix+j,"update")
        except Exception:
           print 'Error: file '+startPrefix+j+' not found!  Ignoring file.'
           f = open(self.logfile,'ab')
           f.write('Error: file '+startPrefix+j+' not found!  Ignoring file.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: file '+startPrefix+j+' not found!  Ignoring file.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           keep = False
           delFiles.append(j)
	   continue
	#Check for MEF extensions if set to auto
	if (self.mef < 0):
	   for l in range(len(temp)):
	      if (temp[l].data != None):
		self.mef = l
		break
	#Check for missing FITS Keywords
	if (not temp[0].header.has_key(self.params['OBSTYPE_KEYWORD'])):
	   temp[0].header.update(self.params['OBSTYPE_KEYWORD'], 'none')
        #Check flags for min, max frame values, ignore first frames
        if (self.params['MIN_FRAME_VALUE'] != 0 or self.params['MAX_FRAME_VALUE'] != 0):
	   if (len(qsfilenames) != 0):
	      b = where(qsfilenames == j)
	      if (len(b[0]) != 0):
		med = qsmedians[b[0][0]]
	      else:
		med = arraymedian(temp[self.mef].data)
		if (makeqs):
		   qsfile.write(j+'\t'+str(med)+'\n')
	   else:
              med = arraymedian(temp[self.mef].data)
	      if (makeqs):
		qsfile.write(j+'\t'+str(med)+'\n')
	   if (med < self.params['MIN_FRAME_VALUE'] and temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'dark'):
	      keep = False
	   if (self.params['MAX_FRAME_VALUE'] != 0 and med > self.params['MAX_FRAME_VALUE']):
	      keep = False
	      if (self.params['IGNORE_AFTER_BAD_READ'].lower() == 'yes'):
		keepNext = False
	prefix = j[:j.rfind(self.delim,0,j.rfind('.'))]
        if (not temp[0].header.has_key(self.params['EXPTIME_KEYWORD'])):
	   if (curses.ascii.isdigit(self.params['EXPTIME_KEYWORD'][0])):
	      currExp = int(self.params['EXPTIME_KEYWORD'])
	      defExp = int(self.params['EXPTIME_KEYWORD'])
	      self.params['EXPTIME_KEYWORD'] = 'EXP_TIME'
	   else:
	      currExp = defExp
	else:
	   currExp = temp[0].header[self.params['EXPTIME_KEYWORD']]
	if (self.params['IGNORE_FIRST_FRAMES'].lower() == 'yes' and (prefix != lastPrefix or currExp != lastExp)):
	   keep = False
	lastPrefix = prefix
	lastExp = currExp
	if (keep):
	   if (manDarks):
	      if (tmpdarks.count(j) > 0):
		if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'dark'):
		   temp[0].header.update('OBS_ORIG',temp[0].header[self.params['OBSTYPE_KEYWORD']])
		   temp[0].header.update(self.params['OBSTYPE_KEYWORD'],'dark')
		tmpdarks.remove(j)
	   if (manFlats):
	      if (tmpflats.count(j) > 0):
		currflattype = ''
		if (self.params['FLAT_TYPE'] != 'mix'):
		   currflattype = self.params['FLAT_TYPE'].lower()
		else:
		   for l in range(len(tmpflattypes)):
		      for i in range(len(self.params['FILTER_KEYWORD'])):
			if (temp[0].header[self.params['FILTER_KEYWORD'][i]].lower() == tmpflattypes[l][0].lower()):
			   currflattype = tmpflattypes[l][len(tmpflattypes[l])-1].lower()
			   break
                if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'flat' and currflattype != 'sky'):
                   temp[0].header.update('OBS_ORIG',temp[0].header[self.params['OBSTYPE_KEYWORD']])
                   temp[0].header.update(self.params['OBSTYPE_KEYWORD'],'flat')
		tmpflats.remove(j)
		if (currflattype == 'sky'):
		   self.isSkyFlat.append(True)
		else:
		   self.isSkyFlat.append(False)
	      else:
		self.isSkyFlat.append(False)
	   else:
	      currflattype = ''
              if (self.params['FLAT_TYPE'] != 'mix'):
		currflattype = self.params['FLAT_TYPE'].lower()
              else:
                for l in range(len(tmpflattypes)):
		   for i in range(len(self.params['FILTER_KEYWORD'])):
		      if (temp[0].header[self.params['FILTER_KEYWORD'][i]].lower() == tmpflattypes[l][0].lower()):
			currflattype = tmpflattypes[l][len(tmpflattypes[l])-1].lower()
                        break
	      if (currflattype == 'sky'):
		self.isSkyFlat.append(True)
	      else:
		self.isSkyFlat.append(False)
	   if (curses.ascii.isdigit(self.params['ROT_PA_KEYWORD'][0]) and not temp[0].header.has_key('ROT_PA')):
	      temp[0].header.update('ROT_PA', float(self.params['ROT_PA_KEYWORD']))
           if (curses.ascii.isdigit(self.params['PIXSCALE_KEYWORD'][0]) and not temp[0].header.has_key('PIXSCALE')):
              temp[0].header.update('PIXSCALE', float(self.params['PIXSCALE_KEYWORD']))
           if (not temp[0].header.has_key(self.params['EXPTIME_KEYWORD'])):
	      temp[0].header.update(self.params['EXPTIME_KEYWORD'], defExp)
	      print "Using exposure time "+str(defExp)+" for file "+j
              f = open(self.logfile,'ab')
              f.write("Warning: Using exposure time "+str(defExp)+" for file "+j+'\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Using exposure time "+str(defExp)+" for file "+j+'\n') 
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   self.exptimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
	   for i in range(len(self.params['FILTER_KEYWORD'])):
	      if (isValidFilter(temp[0].header[self.params['FILTER_KEYWORD'][i]])):
		self.filters.append(temp[0].header[self.params['FILTER_KEYWORD'][i]])
		break
	   if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() == 'dark'):
	      self.darkfiles.append(j)
              self.darktimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
              self.isSkyFlat[len(self.isSkyFlat)-1] = False
	   if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower().find('flat') != -1):
	      self.flatfiles.append(j)
	      self.flattimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
	      for i in range(len(self.params['FILTER_KEYWORD'])):
		if (isValidFilter(temp[0].header[self.params['FILTER_KEYWORD'][i]])):
		   self.flatfilters.append(temp[0].header[self.params['FILTER_KEYWORD'][i]])
		   break
	      self.isSkyFlat[len(self.isSkyFlat)-1] = False
              if (self.params['FLAT_TYPE'] != 'mix'):
		self.flattypes.append(self.params['FLAT_TYPE'].lower())
              else:
                for l in range(len(tmpflattypes)):
		   doBreak = False
                   for i in range(len(self.params['FILTER_KEYWORD'])):
                      if (temp[0].header[self.params['FILTER_KEYWORD'][i]].lower() == tmpflattypes[l][0].lower()):
			self.flattypes.append(tmpflattypes[l][len(tmpflattypes[l])-1].lower())
			doBreak = True
			break
		   if (doBreak): break
	else:
           print 'Warning: Ignoring file '+startPrefix+j
           f = open(self.logfile,'ab')
           f.write('Warning: Ignoring file '+startPrefix+j+'.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: Ignoring file '+startPrefix+j+'.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	   delFiles.append(j)
	temp.verify('silentfix')
	temp.close()
	guiCount+=1.
	if (self.guiMessages): print "PROGRESS: "+str(int(guiCount/len(self.filenames)*5))
      if (makeqs): qsfile.close()
      if (curses.ascii.isdigit(self.params['ROT_PA_KEYWORD'][0])):
	self.params['ROT_PA_KEYWORD'] = 'ROT_PA'
      if (curses.ascii.isdigit(self.params['PIXSCALE_KEYWORD'][0])):
        self.params['PIXSCALE_KEYWORD'] = 'PIXSCALE'
      for j in delFiles:
	if self.filenames.count(j) != 0:
	   self.filenames.remove(j)

   def convertMEFs(self):
      print "Converting MEFs to single extension FITS files..."
      if (self.guiMessages): print "STATUS: Converting MEFs..."
      f = open(self.logfile,'ab')
      f.write("Converting MEFs to single extension FITS files...\n")
      f.close()
      if (not os.access("singleExtFits", os.F_OK)): os.mkdir("singleExtFits",0755)
      for j in self.filenames:
	seffile = "singleExtFits/sef_"+j
	if (os.access(seffile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(seffile)
	if (not os.access(seffile, os.F_OK)):
	   temp = pyfits.open(j)
	   head = temp[self.mef].header.ascardlist()
	   for l in range(len(head)):
	      temp[0].header.update(head.keys()[l], temp[self.mef].header[l])
	   temp[0].data = temp[self.mef].data
	   while (len(temp) > 1):
	      temp.remove(temp[1])
	   temp[0].header.update('HISTORY','Converted to single extension FITS')
	   temp[0].header.update('FILENAME',seffile)
	   temp.verify('silentfix')
	   temp.writeto(seffile)
	   temp.close()
      self.prefix = "singleExtFits/sef_"
	   

   def linearizeAll(self, coeffs):
      #Perform Linearity correction on all files
      print "Performing Linearity Correction..."
      if (self.guiMessages): print "STATUS: Linearity Correction..."
      f = open(self.logfile,'ab')
      f.write("Performing Linearity Correction...\n")
      f.close()
      n = 0
      if (not os.access("linearized", os.F_OK)): os.mkdir("linearized",0755) 
      guiCount = 0.
      for j in self.filenames:
        linfile = "linearized/lin_"+j
        if (os.access(linfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(linfile) 
        if (not os.access(linfile, os.F_OK)):
	   temp = pyfits.open(j)
	   #Convert MEFs if necessary
	   if (self.mef != 0):
	      head = temp[self.mef].header.ascardlist()
              for l in range(len(head)):
		temp[0].header.update(head.keys()[l], temp[self.mef].header[l])
              temp[0].data = temp[self.mef].data
              temp[0].header.update('HISTORY','Converted to single extension FITS')
           while (len(temp) > 1):
	      temp.remove(temp[1])
	   temp[0].data = linearity(temp[0].data, coeffs)
	   temp[0].header.update('HISTORY','linearized')
	   temp[0].header.update('FILENAME', linfile)
	   temp.verify('silentfix')
	   temp.writeto(linfile)
	   temp.close()
	n+=1
        guiCount+=1.
        if (self.guiMessages):
	   print "PROGRESS: "+str(int(5+guiCount/len(self.filenames)*15))
	   print "FILE: "+linfile
      self.prefix = "linearized/lin_"

   def darkCombine(self):
      if (self.guiMessages): print "STATUS: Combining Darks..."
      mdarks = []
      mdarktimes = []
      tmpdarktimes = array(self.darktimes)
      n = len(self.darkfiles)
      if (not os.access("masterDarks", os.F_OK)): os.mkdir("masterDarks",0755)
      while (n > 0):
	exptime = tmpdarktimes[0] 
	tmpdarktimes = tmpdarktimes[where(tmpdarktimes != exptime)]
        print "Creating Master Dark for exposure time: ",exptime,"..."
        f = open(self.logfile,'ab')
        f.write('Creating Master Dark for exposure time: '+str(exptime)+'...\n')
        f.close()
        darks = ""
	for j in range(len(self.darkfiles)):
	   if (self.darktimes[j] == exptime):
	      darks+=","+self.prefix+self.darkfiles[j]
	      n-=1
        darks = darks.strip(",")
	mdfilename="masterDarks/mdark-exp"+str(exptime)+".fits"
        if (os.access(mdfilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mdfilename)
	if (not os.access(mdfilename, os.F_OK)):
	   #Use IRAF's imcombine to combine darks
           iraf.unlearn('imcombine')
	   #imcombine can't handle strings longer than 1024 bytes
	   if (len(darks) > 1023):
	      tmpFile = open("tmpIRAFFile.dat", "wb")
	      tmpFile.write(darks.replace(",","\n"))
	      tmpFile.close()
	      iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mdfilename, combine="median")
	      os.unlink("tmpIRAFFile.dat")
	   else:
	      iraf.images.immatch.imcombine(darks, mdfilename, combine="median")
           masterDark = pyfits.open(mdfilename, "update")
	   masterDark[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Dark")
           masterDark[0].header.update('FILENAME', mdfilename)
	   masterDark.verify('silentfix')
	   masterDark.flush()
	   masterDark.close()
	mdarks.append(mdfilename)
	mdarktimes.append(exptime)
	if (self.guiMessages):
	   print "PROGRESS: "+str(int(25-(len(tmpdarktimes)+0.)/len(self.darktimes)*5))
	   print "FILE: "+mdfilename
      #Update filename and exptime lists
      for j in self.darkfiles:
	if (self.params['CLEAN_UP'].lower() == 'yes'):
	   if (os.access('linearized/lin_'+j, os.F_OK)):
	      os.unlink('linearized/lin_'+j)
	i = self.filenames.index(j)
	self.filenames.pop(i)
	self.exptimes.pop(i)
	self.filters.pop(i)
	self.isSkyFlat.pop(i)
      self.darkfiles = mdarks
      self.darktimes = mdarktimes

   def darkSubtract(self):
      n = len(self.filenames)
      if (not os.access("darkSubtracted", os.F_OK)): os.mkdir("darkSubtracted",0755)
      tmpexptimes = array(self.exptimes)
      print "Subtracting Master Darks..."
      if (self.guiMessages): print "STATUS: Subtracting Darks..."
      f = open(self.logfile,'ab')
      f.write("Subtracting Master Darks...\n")
      f.close()
      defexptimes = []
      for j in range(len(self.params['DEFAULT_MASTER_DARK'])):
	if (os.access(self.params['DEFAULT_MASTER_DARK'][j], os.F_OK)):
	   temp = pyfits.open(self.params['DEFAULT_MASTER_DARK'][j])
	   defexptimes.append(float(temp[0].header[self.params['EXPTIME_KEYWORD']]))
	   temp.close()
	else:
	   defexptimes.append(-1000)
      defexptimes = array(defexptimes)
      guiCount = 0.
      while (n > 0):
        exptime = tmpexptimes[0]
	tmpexptimes = tmpexptimes[where(tmpexptimes != exptime)]
        mdfilename="masterDarks/mdark-exp"+str(exptime)+".fits"
	#Search for master dark
        if (not os.access(mdfilename, os.R_OK)):
	   print "Master Dark for exposure "+str(exptime)+" not found!"
           f = open(self.logfile,'ab')
           f.write("Master Dark for exposure "+str(exptime)+" not found!\n")
           f.close()
	   #Try default master dark
	   if (len(self.params['DEFAULT_MASTER_DARK']) > 0):
	      b = where(defexptimes == exptime)[0]
	      if (len(b) != 0):
		if (os.access(self.params['DEFAULT_MASTER_DARK'][b[0]], os.R_OK)):
	           mdfilename = self.params['DEFAULT_MASTER_DARK'][b[0]]
	           print "Using " + mdfilename + " instead."
           	   f = open(self.logfile,'ab')
           	   f.write("Using " + mdfilename + " instead.\n")
           	   f.close()
	   elif (self.params['PROMPT_FOR_MISSING_DARK'] == 'no'):
	      if (len(self.darktimes) != 0):
	        b = where(abs(array(self.darktimes)-exptime) == min(abs(array(self.darktimes)-exptime)))[0][0]
		mdfilename = self.darkfiles[b]
	      elif (len(defexptimes) != 0):
		b = where(abs(defexptimes-exptime) == min(abs(defexptimes-exptime)))[0][0]
		if (os.access(self.params['DEFAULT_MASTER_DARK'][b], os.F_OK)):
		   mdfilename = self.params['DEFAULT_MASTER_DARK'][b]
	      print "Using " + mdfilename + " instead."
              f = open(self.logfile,'ab')
              f.write("Using " + mdfilename + " instead.\n")
              f.close()
              f = open(self.warnfile,'ab')
	      f.write("WARNING: Master dark for exposure "+str(exptime)+" not found!\n")
              f.write("Using " + mdfilename + " instead.\n")
	      nwarn = 0
	      for i in range(len(self.filenames)):
		if (self.exptimes[i] == exptime):
		   nwarn+=1
	      f.write(str(nwarn)+" files affected.\n")
	      f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   #Prompt User for dark file
	   else:
	      print "List of darks and exposure times:"
	      for j in range(len(self.darkfiles)):
		print self.darkfiles[j],self.darktimes[j]
	      if (guiMessages):
		print "INPUT: File: Select a filename to use as a dark for exposure time "+str(exptime)
	      tmp = raw_input("Select a filename to use as a dark: ")
	      if (guiMessages): print tmp
	      if (os.access(tmp, os.R_OK)):
		mdfilename = tmp
	      else:
		print "Dark not subtracted for exposure time "+str(exptime)
	        f = open(self.logfile,'ab')
      	        f.write("Dark not subtracted for exposure time "+str(exptime)+'\n')
      		f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Dark not subtracted for exposure time "+str(exptime)+'\n') 
                nwarn = 0
		for j in range(len(self.filenames)):
		   if (self.exptimes[j] == exptime): n-=1
		   nwarn+=1
                f.write(str(nwarn)+" files affected.\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
	masterDark = pyfits.open(mdfilename)
	#Subtract master dark from remaining files
        for j in range(len(self.filenames)):
           if (self.exptimes[j] == exptime):
	      dsfile = "darkSubtracted/ds_"+self.filenames[j]
	      if (os.access(dsfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(dsfile)
              if (not os.access(dsfile, os.F_OK)):
		image = pyfits.open(self.prefix+self.filenames[j])
		image[0].data -= masterDark[0].data
		image[0].header.update('HISTORY','Dark subtracted using '+mdfilename)
		image[0].header.update('FILENAME', dsfile)
	        image.verify()
		image.writeto(dsfile)
		image.close()
              n-=1
	      guiCount+=1.
              if (self.guiMessages):
		print "PROGRESS: "+str(int(25+guiCount/len(self.filenames)*5))
		print "FILE: "+dsfile
	masterDark.close()
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
	   if (os.access('linearized/lin_'+j, os.F_OK)):
	      os.unlink('linearized/lin_'+j)
      self.prefix = "darkSubtracted/ds_"

   def flatCombine(self):
      if (self.guiMessages): print "STATUS: Combining Flats..."
      mflats = []
      mflattimes = []
      mflatfilters = []
      n = len(self.flatfiles)
      tmpflattimes = array(self.flattimes)
      tmpflatfilters = numarray.strings.array(self.flatfilters)
      tmpflattypes = numarray.strings.array(self.flattypes)
      if (not os.access("masterFlats", os.F_OK)): os.mkdir("masterFlats",0755)
      #Process Dome Flats
      if (self.params['FLAT_TYPE'].lower() == "dome" or self.params['FLAT_TYPE'].lower() == "mix"):
	if (self.params['FLAT_TYPE'].lower() == "mix"):
	   tmpflattimes = []
	   tmpflatfilters = []
	   tmpflattypes = []
	   for j in range(len(self.flattypes)):
	      if (self.flattypes[j] == 'on' or self.flattypes[j] == 'on-off'):
		tmpflattimes.append(self.flattimes[j])
	        tmpflatfilters.append(self.flatfilters[j])
	        tmpflattypes.append(self.flattypes[j])
	   tmpflattimes = array(tmpflattimes)
	   tmpflatfilters = numarray.strings.array(tmpflatfilters)
	   tmpflattypes = numarray.strings.array(tmpflattypes)
	   n = len(tmpflattimes)
        method = self.params['FLAT_METHOD_DOME'].lower()
	offselect = self.params['FLAT_LAMP_OFF_FILES']
	while (n > 0):
           exptime = tmpflattimes[0]
	   filter = tmpflatfilters[0]
	   if (self.params['FLAT_TYPE'].lower() == "mix"):
	      method = tmpflattypes[0]
	   b = (where(tmpflattimes != exptime, 1, 0)+where(tmpflatfilters != filter, 1, 0)+1)/2
           tmpflattimes = compress(b, tmpflattimes)
	   tmpflatfilters = compress(b, tmpflatfilters)
	   tmpflattypes = compress(b, tmpflattypes)
           print "Creating Master Flat for exposure time: "+str(exptime)+" and filter "+filter+"..."
	   print "Using method "+method
           f = open(self.logfile,'ab')
           f.write("Creating Master Flat for exposure time: "+str(exptime)+" and filter "+filter+"...\n")
	   f.write("Using method "+method+"\n")
           f.close()
	   if (method == "on-off"):
	      onflats = ""
	      offflats = ""
	   elif (method == "on"):
              flats = "" 

	   #Create a list of off flats if .dat file supplied
	   if (method == "on-off" and offselect.count('.dat') != 0):
              f = open(offselect, "rb")
              s = f.read().split("\n")
              f.close()
              try: s.remove('')
      	      except ValueError:
		dummy = ''
              tempofffiles = []
              for j in range(len(s)):
		manoff = s[j].split()
		try:
		   manoff[1] = int(manoff[1])
		   manoff[2] = int(manoff[2])
		except IndexError:
		   print "Your flat lamp off file is incorrectly set up."
		   print "Terminating program!"
		   f = open(self.logfile,'ab')
		   f.write("Warning: unable to process off flats!\n")
		   f.close()
		   f = open(self.warnfile,'ab')
		   f.write("WARNING: unable to process off flats!\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
		   f.close()
		   sys.exit()
                for l in range(len(self.flatfiles)):
                   offprefix = self.flatfiles[l][:self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))]
                   #offindex = int(self.flatfiles[l][self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind(self.delim))+1:self.flatfiles[l].rfind('.')])
		   offindex = int(self.flatfiles[l][self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))+len(self.delim):self.flatfiles[l].rfind('.')])
                   if (offprefix.find(manoff[0]) != -1 and offindex >= manoff[1] and offindex <= manoff[2]):
		      tempofffiles.append(self.flatfiles[l])

           for j in range(0,len(self.flatfiles),1):
              if (self.flattimes[j] == exptime and self.flatfilters[j] == filter):
		#Detect off flats from naming convention if .dat file not
		#supplied.
		if (method == "on-off" and offselect.count('.dat') == 0):
		   if (self.flatfiles[j].find(offselect) != -1):
		      offflats+=self.prefix+self.flatfiles[j]+","
		   else: onflats+=self.prefix+self.flatfiles[j]+","
		#Check list of off flats if .dat file supplied
		elif (method == "on-off" and offselect.count('.dat') != 0):
		   tempind = -1
		   for l in range(len(tempofffiles)):
		      if (self.flatfiles[j].find(tempofffiles[l][-31:]) != -1):
			tempind = l
		   #if (tempofffiles.count(self.flatfiles[j]) != 0):
		   if (tempind != -1):
		      offflats+=self.prefix+self.flatfiles[j]+","
		   else: onflats+=self.prefix+self.flatfiles[j]+","
		elif (method == "on"):
                   flats+=self.prefix+self.flatfiles[j]+","
                n-=1
	   if (method == "on-off"):
	      onflats = onflats.strip(",")
	      offflats = offflats.strip(",")
	   elif (method == "on"):
	      flats = flats.strip(",")
           mffilename="masterFlats/mdomeflat-"+method+"-exp"+str(exptime)+filter+".fits"
           if (os.access(mffilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mffilename)
           if (not os.access(mffilename, os.F_OK)):
              iraf.unlearn('imcombine')
	      #Combine Flats with IRAF's imcombine
	      if (method == "on-off"):
                #imcombine can't handle strings longer than 1024 bytes
                if (len(onflats) > 1023):
                   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(onflats.replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffilename, combine="median")
                   os.unlink("tmpIRAFFile.dat")
		elif (onflats == ""):
		   print "No On flats found for exposure time "+str(exptime)+" and filter "+filter
	           f = open(self.logfile,'ab')
      		   f.write("No On flats found for exposure time "+str(exptime)+" and filter "+filter+'\n')
      		   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: No On flats found for exposure time "+str(exptime)+" and filter "+filter+'\n')
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
		   continue
                else:
                   iraf.images.immatch.imcombine(onflats, mffilename, combine="median")
                if (len(offflats) > 1023):
                   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(offflats.replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffilename+".off.fits", combine="median")
                   os.unlink("tmpIRAFFile.dat")
		elif (offflats == ""):
                   print "No Off flats found for exposure time "+str(exptime)+" and filter "+filter
                   f = open(self.logfile,'ab')
                   f.write("No Off flats found for exposure time "+str(exptime)+" and filter "+filter+'\n')
                   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: No Off flats found for exposure time "+str(exptime)+" and filter "+filter+'\n')
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
		   os.unlink(mffilename)
                   continue
                else:
		   iraf.images.immatch.imcombine(offflats, mffilename+".off.fits", combine="median")
		#Subtract lamp on - lamp off
		masterFlat = pyfits.open(mffilename, "update")
		masterOff = pyfits.open(mffilename+".off.fits")
		masterFlat[0].data -= masterOff[0].data
		masterOff.close()
		os.unlink(mffilename+".off.fits")
              elif (method == "on"):
                #imcombine can't handle strings longer than 1024 bytes
                if (len(flats) > 1023):
		   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(flats.replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffilename, combine="median")
                   os.unlink("tmpIRAFFile.dat")
                else:
		   iraf.images.immatch.imcombine(flats, mffilename, combine="median")
                masterFlat = pyfits.open(mffilename, "update")
	      #Normalize Master Flat and update header
	      med = arraymedian(masterFlat[0].data)
	      masterFlat[0].data = masterFlat[0].data/med
              masterFlat[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Flat")
              masterFlat[0].header.update('FILENAME', mffilename)
	      masterFlat[0].header.update('HISTORY',"Normalized by: "+str(med))
              masterFlat.verify('silentfix')
              masterFlat.flush()
              masterFlat.close()
           mflats.append(mffilename)
	   mflattimes.append(exptime)
	   mflatfilters.append(filter)
	   if (self.guiMessages): print "FILE: "+mffilename

      #Process sky flats
      if (self.params['FLAT_TYPE'].lower() == "sky" or self.params['FLAT_TYPE'].lower() == "mix"):
	skyflatfiles = []
	skyflatfilters = []
        tmpflatfilters = []
	skyflattimes = []
	for j in range(len(self.isSkyFlat)):
	   if (self.isSkyFlat[j]):
	      skyflatfiles.append(self.filenames[j])
              skyflatfilters.append(self.filters[j])
	      skyflattimes.append(self.exptimes[j])
           tmpflatfilters = numarray.strings.array(skyflatfilters)
	   tmpflattimes = array(skyflattimes)
	   n = len(skyflatfiles)
        while (n > 0):
           filter = tmpflatfilters[0]
           exptime = tmpflattimes[0]
           b = where(tmpflatfilters != filter, 1, 0)
           tmpflatfilters = compress(b, tmpflatfilters)
	   tmpflattimes = compress(b, tmpflattimes)
           print "Creating Master sky Flat for filter "+filter+"..."
           f = open(self.logfile,'ab')
           f.write("Creating Master sky Flat for filter "+filter+"...\n")
           f.close()
           flats = ""
           for j in range(0,len(skyflatfiles),1):
              if (skyflatfilters[j] == filter):
                flats+=self.prefix+skyflatfiles[j]+","
                n-=1
           flats = flats.strip(",")
           mffilename="masterFlats/mskyflat-exp"+str(exptime)+filter+".fits"
           if (os.access(mffilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mffilename)
           if (not os.access(mffilename, os.F_OK)):
              iraf.unlearn('imcombine')
              #Combine Flats with imcombine.  Scale each flat by the
	      #reciprocal of its median and then median combine them.
              #imcombine can't handle strings longer than 1024 bytes
              if (len(flats) > 1023):
		tmpFile = open("tmpIRAFFile.dat", "wb")
                tmpFile.write(flats.replace(",","\n"))
                tmpFile.close()
                iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffilename, combine="median", scale="median", reject=self.params['FLAT_SKY_REJECT_TYPE'], nlow=self.params['FLAT_SKY_NLOW'], nhigh=self.params['FLAT_SKY_NHIGH'], lsigma=self.params['FLAT_SKY_LSIGMA'], hsigma=self.params['FLAT_SKY_HSIGMA'])
                os.unlink("tmpIRAFFile.dat")
              else:
                iraf.images.immatch.imcombine(flats, mffilename, combine="median", scale="median", reject=self.params['FLAT_SKY_REJECT_TYPE'], nlow=self.params['FLAT_SKY_NLOW'], nhigh=self.params['FLAT_SKY_NHIGH'], lsigma=self.params['FLAT_SKY_LSIGMA'], hsigma=self.params['FLAT_SKY_HSIGMA'])
              masterFlat = pyfits.open(mffilename, "update")
              #Normalize Master Flat and Update Header
              med = arraymedian(masterFlat[0].data)
              masterFlat[0].data = masterFlat[0].data/med
              masterFlat[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Flat")
              masterFlat[0].header.update('FILENAME', mffilename)
              masterFlat.verify('silentfix')
              masterFlat.flush()
              masterFlat.close()
           mflats.append(mffilename)
	   mflattimes.append(0)
	   mflatfilters.append(filter)
           if (self.guiMessages): print "FILE: "+mffilename

      #Process twilight flats
      if (self.params['FLAT_TYPE'].lower() == "twilight" or self.params['FLAT_TYPE'].lower() == "mix"):
        if (self.params['FLAT_TYPE'].lower() == "mix"):
           tmpflattimes = []
           tmpflatfilters = []
           tmpflattypes = []
           for j in range(len(self.flattypes)):
              if (self.flattypes[j] == 'twilight'):
                tmpflattimes.append(self.flattimes[j])
                tmpflatfilters.append(self.flatfilters[j])
                tmpflattypes.append(self.flattypes[j])
           tmpflattimes = array(tmpflattimes)
           tmpflatfilters = numarray.strings.array(tmpflatfilters)
           tmpflattypes = numarray.strings.array(tmpflattypes)
	   n = len(tmpflattimes)
        while (n > 0):
           exptime = tmpflattimes[0]
           filter = tmpflatfilters[0]
           #b = (where(tmpflattimes != exptime, 1, 0)+where(tmpflatfilters != filter, 1, 0)+1)/2
	   b = where(tmpflatfilters != filter, 1, 0)
           tmpflattimes = compress(b, tmpflattimes)
           tmpflatfilters = compress(b, tmpflatfilters)
           print "Creating Master Flat for exposure time: "+str(exptime)+" and filter "+filter+"..."
           f = open(self.logfile,'ab')
           f.write("Creating Master Flat for exposure time: "+str(exptime)+" and filter "+filter+"...\n")
           f.close()
           flats = ""
	   twiflats = []
           for j in range(0,len(self.flatfiles),1):
              #if (self.flattimes[j] == exptime and self.flatfilters[j] == filter):
	      if (self.flatfilters[j] == filter):
		twiflats.append(self.prefix+self.flatfiles[j])
		n-=1
	   if (len(twiflats) == 1):
	      print "Only 1 twilight flat found!  Master flat not created!"
      	      f = open(self.logfile,'ab')
              f.write("Only 1 twilight flat found!  Master flat not created!\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Only 1 twilight flat found for exptime "+str(exptime)+" and filter "+filter+'!  Master flat not created!\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      continue
	   for j in range(len(twiflats)-1):
	      twiname = "tempTwiFlat"+str(j)+".fits"
	      temp1 = pyfits.open(twiflats[j])
	      temp2 = pyfits.open(twiflats[j+1])
	      temp1[0].data = abs(temp1[0].data-temp2[0].data)
	      temp1.verify('silentfix')
	      temp1.writeto(twiname)
	      temp1.close()
	      temp2.close()
	      flats+=twiname+","
           flats = flats.strip(",")
           mffilename="masterFlats/mtwilightflat-exp"+str(exptime)+filter+".fits"
           if (os.access(mffilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mffilename)
           if (not os.access(mffilename, os.F_OK)):
              iraf.unlearn('imcombine')
              #Combine Flats with imcombine.  Scale each flat by the
              #reciprocal of its median and then median combine them.
              #imcombine can't handle strings longer than 1024 bytes
              if (len(flats) > 1023):
                tmpFile = open("tmpIRAFFile.dat", "wb")
                tmpFile.write(flats.replace(",","\n"))
                tmpFile.close()
                iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffilename, combine="median", scale="median")
                os.unlink("tmpIRAFFile.dat")
              else:
                iraf.images.immatch.imcombine(flats, mffilename, combine="median", scale="median")
              masterFlat = pyfits.open(mffilename, "update")
              #Normalize Master Flat and Update Header
              med = arraymedian(masterFlat[0].data)
              masterFlat[0].data = masterFlat[0].data/med
              masterFlat[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Flat")
              masterFlat[0].header.update('FILENAME', mffilename)
              masterFlat.verify('silentfix')
              masterFlat.flush()
              masterFlat.close()
           tmpFiles = flats.split(",")
           for j in tmpFiles:
              os.unlink(j)
           mflats.append(mffilename)
           mflattimes.append(exptime)
           mflatfilters.append(filter)
           if (self.guiMessages): print "FILE: "+mffilename

      #Replace individual flats with master flats in file list
      for j in self.flatfiles:
        if (self.params['CLEAN_UP'].lower() == 'yes'):
           if (os.access('darkSubtracted/ds_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_'+j)
        i = self.filenames.index(j)
        self.filenames.pop(i)
        self.exptimes.pop(i)
	self.filters.pop(i)
      self.flatfiles = mflats
      self.flattimes = mflattimes 
      self.flatfilters = mflatfilters
      self.flattypes = []
      self.isSkyFlat = []

   def badPixelMask(self):
      #Generate bad pixel masks
      if (self.guiMessages): print "STATUS: Bad Pixel Masks..."
      self.useBPMask = True
      if (not os.access("badPixelMasks", os.F_OK)):
	os.mkdir("badPixelMasks", 0755)
      defBPMask = self.params['DEFAULT_BAD_PIXEL_MASK']
      edge = self.params['EDGE_REJECT']
      radius = self.params['RADIUS_REJECT']
      #Load from file if specified
      if (len(defBPMask) != 0):
	self.bpMask = []
	self.bpmFile = []
	for j in range(len(defBPMask)):
	   if (os.access(defBPMask[j], os.F_OK)):
	      print "Using "+defBPMask[j]+" as bad pixel mask..."
      	      f = open(self.logfile,'ab')
      	      f.write("Using "+defBPMask[j]+" as bad pixel mask...\n")
      	      f.close()
	      temp = pyfits.open(defBPMask[j])
	      hasFilter = False
	      for i in range(len(self.params['FILTER_KEYWORD'])):
		if (temp[0].header.has_key(self.params['FILTER_KEYWORD'][i])):
		   if (isValidFilter(temp[0].header[self.params['FILTER_KEYWORD'][i]])):
		      tmpfilter = temp[0].header[self.params['FILTER_KEYWORD'][i]]
		      hasFilter = True
	      if (hasFilter):
		junk = ''
	      elif (temp[0].header.has_key('FILTER')):
		tmpfilter = temp[0].header['FILTER']
	      else:
		temppos = defBPMask[j].find('exp')+3
                if (temppos != -1):
		   while (curses.ascii.isdigit(defBPMask[j][temppos]) or defBPMask[j][temppos] == '.'):
		      temppos+=1
		   tmpfilter = defBPMask[j][temppos:defBPMask[j].rfind('.')]
                else:
                   tmpfilter = "none"
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: Bad Pixel Mask "+defBPMask[j]+" has no FILTER keyword.\n")
                   f.write("\tIt will not be applied to any data.\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
	      temp.close()
	      self.bpMask.append(badPixelMask(filename=defBPMask[j], filter=tmpfilter))
	      self.bpmFile.append(defBPMask[j])
      #Otherwise create from a flat field or dark
      else:
	if (len(self.flatfiles) > 0):
	   bpmsource = self.flatfiles
	elif (len(self.darkfiles) > 0):
	   bpmsource = self.darkfiles
	else:
	   print "No flats or darks found!"
      	   f = open(self.logfile,'ab')
      	   f.write("No flats or darks found!\n")
      	   f.close()
           if (guiMessages):
	      print "INPUT: File: Enter a file to use to generate BPM:"
	   tmp = raw_input("Enter a file to use to generate BPM: ")
	   if (guiMessages): print tmp
	   if (os.access(tmp, os.R_OK)):
	      bpmsource = [tmp]
	   else:
	      print "File not found!  BPM not generated!"
              f = open(self.logfile,'ab')
              f.write("File not found!  BPM not generated!\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: No flats or darks found!  BPM not generated!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      return
	self.bpMask = []
	self.bpmFile = []
	for j in range(len(bpmsource)):
           print "Generating bad pixel mask from "+bpmsource[j]+"..."
           f = open(self.logfile,'ab')
           f.write("Generating bad pixel mask from "+bpmsource[j]+"...\n")
           f.close()
           masterBPM = pyfits.open(bpmsource[j])
	   for i in range(len(self.params['FILTER_KEYWORD'])):
	      if (isValidFilter(masterBPM[0].header[self.params['FILTER_KEYWORD'][i]])):
		bpmFilter = masterBPM[0].header[self.params['FILTER_KEYWORD'][i]]
		break
           bpmFile = "badPixelMasks/BPM_"+bpmsource[j][bpmsource[j].rfind("/")+1:]
           #Set rejection threshold based on clipping type
	   if (self.params['BAD_PIXEL_MASK_CLIPPING'] == "values"):
	      lo = self.params['BAD_PIXEL_LOW']
	      hi = self.params['BAD_PIXEL_HIGH']
	   elif (self.params['BAD_PIXEL_MASK_CLIPPING'] == "sigma"):
	      sig = self.params['BAD_PIXEL_SIGMA']
	      sigclip = sigmaFromClipping(masterBPM[0].data, sig, 5)
              med = sigclip[1]
	      stddev = sigclip[2] 
              lo = med-sig*stddev
              hi = med+sig*stddev
           #Create bad pixel mask
	   b = (where(masterBPM[0].data< lo, 1, 0)+where(masterBPM[0].data > hi, 1, 0)+1)/2
	   if (edge > 0):
	      b[0:edge,:] = 1
	      b[:,0:edge] = 1
	      b[-1*edge:,:] = 1
	      b[:,-1*edge:] = 1
	   if (radius > 0):
	      xs = b.shape[0]
	      ys = b.shape[1]
	      for r in range(xs):
		ry = radius**2-(xs/2.-j)**2
		if (ry < 0):
		   b[r,:] = 1
		else:
		   b[r,0:int(ys/2.-sqrt(ry)+1)] = 1
		   b[r,int(ys/2.+sqrt(ry)+1):ys] = 1
           self.bpMask.append(badPixelMask(inputMask = b, filter=bpmFilter))
           if (os.access(bpmFile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(bpmFile)
           if (not os.access(bpmFile, os.F_OK)):
	      self.bpMask[j].saveBadPixelMask(bpmFile)
	      print "Bad Pixel Mask written to "+bpmFile
      	      f = open(self.logfile,'ab')
      	      f.write("Bad Pixel Mask written to "+bpmFile+'\n')
      	      f.close()
	   self.bpmFile.append(bpmFile)
	   if (guiMessages):
	      print "PROGRESS: "+str(int(35+(j+0.)/len(bpmsource)*5))
	      print "FILE: "+bpmFile

   def flatDivide(self):
      if (self.guiMessages): print "STATUS: Dividing Flats..."
      n = len(self.filenames)
      if (not os.access("flatDivided", os.F_OK)): os.mkdir("flatDivided",0755)
      tmpfilters = numarray.strings.array(self.filters)
      deffilters = []
      for j in range(len(self.params['DEFAULT_MASTER_FLAT'])):
        if (os.access(self.params['DEFAULT_MASTER_FLAT'][j], os.F_OK)):
           temp = pyfits.open(self.params['DEFAULT_MASTER_FLAT'][j])
           for i in range(len(self.params['FILTER_KEYWORD'])):
	      if (isValidFilter(temp[0].header[self.params['FILTER_KEYWORD'][i]])):
		deffilters.append(temp[0].header[self.params['FILTER_KEYWORD'][i]])
		break
           temp.close()
        else:
           deffilters.append('null')
      deffilters = numarray.strings.array(deffilters)
      print "Applying Bad Pixel Mask and Dividing By Master Flats..."
      f = open(self.logfile,'ab')
      f.write("Applying Bad Pixel Mask and Dividing By Master Flats...\n")
      f.close()
      guiCount = 0.
      while (n > 0):
        filter = tmpfilters[0]
	tmpfilters = tmpfilters[where(tmpfilters != filter)]
	#Find master flat for same filter
	if (self.flatfilters.count(filter) > 0):
	   mffilename = self.flatfiles[self.flatfilters.index(filter)]
        else:
           print "Master Flat for filter "+filter+" not found!"
           f = open(self.logfile,'ab')
           f.write("Master Flat for filter "+filter+" not found!\n")
           f.close()
           #Try default master flat
           if (len(self.params['DEFAULT_MASTER_FLAT']) > 0):
              b = where(deffilters == filter)[0]
              if (len(b) != 0):
                if (os.access(self.params['DEFAULT_MASTER_FLAT'][b[0]], os.R_OK)):
                   mffilename = self.params['DEFAULT_MASTER_FLAT'][b[0]]
                   print "Using " + mffilename + " instead."
      		   f = open(self.logfile,'ab')
      		   f.write("Using " + mffilename + " instead.\n")
      		   f.close()
           #Prompt User for flat file
           else:
              print "List of flats and filters:"
              for j in range(len(self.flatfiles)):
                print self.flatfiles[j],self.flatfilters[j]
              if (guiMessages):
                print "INPUT: File: Select a filename to use as a flat for filter "+filter
              tmp = raw_input("Select a filename to use as a flat: ")
	      if (guiMessages): print tmp
              if (os.access(tmp, os.R_OK)):
                mffilename = tmp
              else:
                print "Flat not divided for filter "+filter
      		f = open(self.logfile,'ab')
      		f.write("Flat not divided for filter "+filter+'\n')
      		f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Master Flat for filter "+filter+" not found!  Flat not divided for filter "+filter+"!\n")
		nwarn = 0
                for j in range(len(self.filenames)):
                   if (self.filters[j] == filter): n-=1
		   nwarn+=1
                continue
                f.write(str(nwarn)+" files affected.\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
        masterFlat = pyfits.open(mffilename)
	#Apply Bad Pixel Mask to Master Flat and renormalize Master Flat
	if (not self.useBPMask):
	   self.bpMask = [badPixelMask(zeros(shape(masterFlat[0].data)))]
	   nbpm = 0
	else:
	   for j in range(len(self.bpMask)):
	      if (self.bpMask[j].filter == filter): nbpm = j
        try:
           mfmed = arraymedian(compress(self.bpMask[nbpm].getGoodPixelMask(), masterFlat[0].data))
        except Exception:
           mfmed = arraymedian(masterFlat[0].data)
           f = open(self.warnfile,'ab')
           f.write("WARNING: Bad pixel mask not found for filter "+filter+"\n")
           f.write("\tduring flat division.  Bad pixels will not be masked\n")
           f.write("\tout of flat-divided images.\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           self.bpMask = [badPixelMask(zeros(shape(masterFlat[0].data)))]
           nbpm = 0
	masterFlat[0].data = masterFlat[0].data / mfmed
	#Set bad pixels to 0
	masterFlat[0].data = masterFlat[0].data*self.bpMask[nbpm].getGoodPixelMask()
        #Divide remaining files by master flat
        for j in range(len(self.filenames)):
           if (self.filters[j] == filter):
              fdfile = "flatDivided/fd_"+self.filenames[j]
              if (os.access(fdfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(fdfile)
              if (not os.access(fdfile, os.F_OK)):
                image = pyfits.open(self.prefix+self.filenames[j])
		image[0].data = image[0].data/masterFlat[0].data
		#Set bad pixels to 0
		image[0].data = reshape(image[0].data, self.bpMask[nbpm].arraySize)
		image[0].data[self.bpMask[nbpm].badPixels] = 0
		bp2 = reshape(where(masterFlat[0].data == 0, 1, 0), masterFlat[0].data.nelements())
                image[0].data[compress(bp2, arange(masterFlat[0].data.nelements()))] = 0
		image[0].data = reshape(image[0].data, self.bpMask[nbpm].arrayShape)
		image[0].header.update('HISTORY','Flat Divided')
		image[0].header.update('FILENAME', fdfile)
		image.verify('silentfix')
                image.writeto(fdfile)
                image.close()
              n-=1
	      guiCount+=1.
              if (guiMessages):
		print "PROGRESS: "+str(int(40+guiCount/len(self.filenames)*5))
		print "FILE: "+fdfile
	masterFlat.close()
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
           if (os.access('darkSubtracted/ds_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_'+j)
      self.prefix = "flatDivided/fd_"

   def selectSkyFiles(self, prefix, files, j, rng, diff, filters, exptimes):
      #Select out from a filelist those files with matching prefixes, 
      #RA or DEC at least diff arcsec away, and
      #optionally within a certain range (e.g. +/-3).  Setting
      #rng to 0 selects all files that match the first two criteria
      #Use these as the sky.
      n = len(files)
      befFiles = []
      aftFiles = []
      temp = pyfits.open(files[j])
      objDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
      objRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
      temp.close()
      lastRA = 0
      lastDec = 0
      #Find matching files before the current object
      if (j > 0):
        for l in range(j-1,-1,-1):
	   if (filters[j] != filters[l]): continue
	   if (exptimes[j] != exptimes[l]): continue
           if (j != l and files[j][:files[j].rfind(self.delim,0,files[j].rfind('.'))] == files[l][:files[l].rfind(self.delim,0,files[l].rfind('.'))]):
              temp = pyfits.open(prefix+files[l])
              currDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              currRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
	      diffRA = (currRA-objRA)*math.cos(objDec*math.pi/180)
	      diffLRA = (currRA-lastRA)*math.cos(lastDec*math.pi/180)
              if ((abs(diffRA) > diff or abs(currDec-objDec) > diff) and (abs(diffLRA) > diff or abs(currDec-lastDec) > diff)):
                 lastRA = currRA
                 lastDec = currDec
                 befFiles.append(files[l])
      #Find matching files after the current object
      if (j < n-1):
        for l in range(j+1,n,1):
	   if (filters[j] != filters[l]): continue
           if (exptimes[j] != exptimes[l]): continue
	   if (not os.access(prefix+files[l], os.F_OK)): continue
           if (j != l and files[j][:files[j].rfind(self.delim,0,files[j].rfind('.'))] == files[l][:files[l].rfind(self.delim,0,files[j].rfind('.'))]):
              temp = pyfits.open(prefix+files[l])
              currDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              currRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
              diffRA = (currRA-objRA)*math.cos(objDec*math.pi/180)
              diffLRA = (currRA-lastRA)*math.cos(lastDec*math.pi/180)
              if ((abs(diffRA) > diff or abs(currDec-objDec) > diff) and (abs(diffLRA) > diff or abs(currDec-lastDec) > diff)):
                 lastRA = currRA
                 lastDec = currDec
                 aftFiles.append(files[l])
      bef = len(befFiles)
      aft = len(aftFiles)
      if (rng == 0):
        nbef = bef
        naft = aft
      else:
        nbef = min(rng, bef)
        naft = min(rng, aft)
        if (bef < rng):
           naft = min(2*rng-bef,aft)
        elif (aft < rng):
           nbef = min(2*rng-aft,bef)
        if (nbef+naft < 2*rng):
           print "Warning: Only able to use ",nbef+naft," files for sky."
           f = open(self.logfile,'ab')
           f.write("Warning: Only able to use "+str(nbef+naft)+" files for sky.\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Only able to use "+str(nbef+naft)+" files for sky.\n")
	   f.write("Image: "+self.filenames[j]+"\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
      skyfiles = ""
      for l in range(nbef):
	if (os.access(prefix+befFiles[l], os.F_OK)):
           skyfiles+=prefix+befFiles[l]+","
      for l in range(naft):
        if (os.access(prefix+aftFiles[l], os.F_OK)):
           skyfiles+=prefix+aftFiles[l]+","
      skyfiles=skyfiles.strip(",")
      return skyfiles

   def selectOffSkyFiles(self, prefix, files, diff):
      #Select out from a filelist those files with RA or DEC at least
      #diff arcsec away. Use these as the sky.
      n = len(files)
      lastRA = 0
      lastDec = 0
      skyfiles = ""
      #Find matching files
      for j in range(n):
	if (not os.access(prefix+files[j], os.F_OK)): continue
	temp = pyfits.open(prefix+files[j])
        currDec=getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
        currRA=getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
        temp.close()
        diffRA = (currRA-lastRA)*math.cos(lastDec*math.pi/180)
        if ((abs(diffRA) > diff or abs(currDec-lastDec) > diff)):
	   lastRA = currRA
           lastDec = currDec
           skyfiles += prefix+files[j]+","
      skyfiles=skyfiles.strip(",")
      return skyfiles

   def skySubtract(self):
      if (not os.access("skySubtracted", os.F_OK)): os.mkdir("skySubtracted",0755)
      if (not os.access("onsourceSkies", os.F_OK)): os.mkdir("onsourceSkies",0755)
      print "Performing Sky Subtraction..."
      if (self.guiMessages): print "STATUS: Sky Subtraction..."
      f = open(self.logfile,'ab')
      f.write("Performing Sky Subtraction...\n")
      f.close()
      rmcr = self.params['REMOVE_COSMIC_RAYS'].lower()
      crpass = self.params['COSMIC_RAY_PASSES']
      if (self.params['USE_SKY_FILES'].lower() == "all"):
	rng = 0
      elif (self.params['USE_SKY_FILES'].lower() == "range"):
        rng = self.params['SKY_FILES_RANGE']
      diff = self.params['SKY_DITHERING_RANGE']/3600.
      method = self.params['SKY_SUBTRACT_METHOD'].lower()
      methods = self.findSkyMethods(self.filenames, self.params['SKY_SUBTRACT_METHOD'])
      guiCount = 0.
      if (methods.count('rough') > 0 or methods.count('remove_objects') > 0):
	print "Creating master skies from other images and subtracting..."
        f = open(self.logfile,'ab')
        f.write("Creating master skies from other images and subtracting...\n")
        f.close()
	n = len(self.filenames)
	for j in range(n):
	   if (methods[j] != 'rough' and methods[j] != 'remove_objects'):
	      continue
	   ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           if (not os.access(ssfile, os.F_OK)):
              #Find all filenames with prefix matching the object
	      #and other matching criteria
              #Use these as skies
	      if (methods[j] == 'remove_objects'):
		ssfile = "skySubtracted/ss1_"+self.filenames[j]
		if (os.access(ssfile, os.F_OK)): continue
                skyfilename = 'onsourceSkies/sky_'+self.filenames[j]
                if (os.access(skyfilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(skyfilename)
                if (os.access(skyfilename, os.F_OK)): continue
		skyfilename = 'tempSky.fits'
	      else:
		skyfilename = 'onsourceSkies/sky_'+self.filenames[j]
                if (os.access(skyfilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(skyfilename)
              skyfiles = self.selectSkyFiles(self.prefix, self.filenames, j, rng, diff, self.filters, self.exptimes)
	      if (skyfiles == ''):
		print "Only one image found for object ",self.filenames[j]
		print "Sky NOT subtracted!"
      		f = open(self.logfile,'ab')
      		f.write("Only one image found for object "+self.filenames[j]+'\n')
		f.write('Sky NOT subtracted!\n')
      		f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
                f.write("Sky NOT subtracted!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		self.filenames[j] = 'REMOVE'
		continue
	      #skyfilename = 'tempSky.fits'
	      if (skyfilename == 'tempSky.fits' and os.access(skyfilename, os.F_OK)): os.unlink(skyfilename)
              if (not os.access(skyfilename, os.F_OK)):
		iraf.unlearn('imcombine')
                #Combine Skys with imcombine.  Scale each sky by the
                #reciprocal of its median and then median combine them.
                #imcombine can't handle strings longer than 1024 bytes
                if (len(skyfiles) > 1023):
		   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(skyfiles.replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
                   os.unlink("tmpIRAFFile.dat")
                else:
                   iraf.images.immatch.imcombine(skyfiles, skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
              masterSky = pyfits.open(skyfilename)
	      image = pyfits.open(self.prefix+self.filenames[j])
              #Scale master sky to median of image and subtract
	      msmed = arraymedian(masterSky[0].data[where(masterSky[0].data != 0)])
	      immed = arraymedian(image[0].data[where(image[0].data != 0)])
	      masterSky[0].data = masterSky[0].data * immed/msmed
	      image[0].data = image[0].data-masterSky[0].data
	      #Remove Cosmic Rays with 3 passes if specified
	      if (rmcr=="yes" and methods[j] == 'rough'):
	        print "Removing Cosmic Rays..."
      		f = open(self.logfile,'ab')
      		f.write("Removing Cosmic Rays...\n")
      		f.close()
	        image[0].data = removeCosmicRays(image[0].data, crpass, logfile=self.logfile)
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
	      image[0].header.update('FILENAME', ssfile) 
              image.verify('silentfix')
	      image.writeto(ssfile)
	      image.close()
	      masterSky.close()
              if (self.params['KEEP_SKIES'].lower() == 'no'):
                os.unlink(skyfilename)
	   guiCount+=1.
	   if (guiMessages):
	      if (methods[j] == 'rough'):
		guiProg = str(int(45+guiCount/len(self.filenames)*40))
		print "FILE: "+ssfile
	      else:
		guiProg = str(int(45+guiCount/len(self.filenames)*15))
	      print "PROGRESS: "+guiProg
	while self.filenames.count('REMOVE'):
	   index = where(numarray.strings.array(self.filenames) == 'REMOVE')[0][0]
	   self.filenames.pop(index)
	   self.filters.pop(index)
	   self.exptimes.pop(index)

      if (methods.count('remove_objects') > 0):
        print "Finding Objects and Creating Object Masks..."
        f = open(self.logfile,'ab')
        f.write("Finding Objects and Creating Object Masks...\n")
        f.close()
	sxtPath = self.params['SEXTRACTOR_PATH']
	sxtConf = self.params['SEXTRACTOR_CONFIG_PATH']+"/"+self.params['SEXTRACTOR_CONFIG_PREFIX']
	#Run SExtractor on rough sky subtracted images to extract sources
        #Create object mask.  Treat as bad pixel mask and apply to flat
	#divided images.
	guiCount = 0.
	for j in self.filenames:
	   if (methods[self.filenames.index(j)] != 'remove_objects'):
	      continue
	   ssfile = "skySubtracted/ss_"+j
	   sxtfile = "TEMPsxt_"+j
	   nbpm = -1
	   for l in range(len(self.bpMask)):
              if (self.bpMask[l].filter == self.filters[self.filenames.index(j)]): nbpm = l 
	   if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
	      os.unlink('TEMPsxt_wimg.fits')
	   if (nbpm >= 0):
	      self.bpMask[nbpm].saveGoodPixelMask('TEMPsxt_wimg.fits')
           if (os.access(sxtfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(sxtfile)
           if (not os.access(ssfile, os.F_OK)):
	      skyfilename = 'onsourceSkies/sky_'+j
	      if (os.access(skyfilename, os.F_OK)): continue
	      ssfile = "skySubtracted/ss1_"+j
	      if (nbpm < 0):
		temp = pyfits.open(ssfile)
		temp[0].data[:,:] = 1
		temp.writeto('TEMPsxt_wimg.fits')
		temp.close()
	      sxtCom = self.params['SEXTRACTOR_PATH']+" " + ssfile + " -c " + sxtConf + ".sex -CATALOG_NAME temp.cat -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE SEGMENTATION -CHECKIMAGE_NAME " + sxtfile + " -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
	      if (guiMessages):
		sxtCom+=" -VERBOSE_TYPE QUIET"
		print "Sextracting "+ssfile
	      os.system(sxtCom)
              try:
                temp = pyfits.open(sxtfile)
              except Exception:
                f = open(self.warnfile,'ab')
                f.write("WARNING: sextractor not successful.  Check to see that it is properly\n")
                f.write('\tinstalled.  As a result, objects have not been removied in the second\n')
                f.write('\tpass of sky subtraction for the file '+ssfile+'\n')
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
                temp = pyfits.open(ssfile)
                temp[0].data[:,:] = 0.
	      objMask = badPixelMask(inputMask = where(temp[0].data > 0, 1, 0))
	      temp.close()
	      os.unlink(sxtfile)
	      temp = pyfits.open(self.prefix+j)
	      temp[0].data = temp[0].data * objMask.getGoodPixelMask()
	      temp.writeto(sxtfile)
	      temp.close()
	      if (self.params['TWO_PASS_OBJECT_MASKING'].lower() == 'yes'):
		if (os.access('temp.cat', os.F_OK)): os.unlink('temp.cat')
		sxtCom = self.params['SEXTRACTOR_PATH']+" " + ssfile + " -c " + sxtConf + ".sex -DETECT_MINAREA " + self.params['TWO_PASS_DETECT_MINAREA'] + " -DETECT_THRESH " + self.params['TWO_PASS_DETECT_THRESH'] + " -CATALOG_NAME temp.cat -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE SEGMENTATION -CHECKIMAGE_NAME TWOPASSTEMPsxt.fits -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
		if (guiMessages):
                   sxtCom+=" -VERBOSE_TYPE QUIET"
                   print "Sextracting "+ssfile+" for two pass object masking"
		os.system(sxtCom)
                try:
                   temp = pyfits.open('TWOPASSTEMPsxt.fits')
                except Exception:
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: sextractor not successful.  Check to see that it is properly\n")
                   f.write('\tinstalled.  As a result, object masking can not be done\n')
                   f.write('\tfor the file '+ssfile+'\n')
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   temp = pyfits.open(ssfile)
                   temp[0].data[:,:] = 0.
		mask2 = badPixelMask(inputMask = where(temp[0].data > 0,1,0)).getGoodPixelMask()
		mask2 = smooth(mask2, self.params['TWO_PASS_BOXCAR_SIZE'], 1)
		mask2 = where(mask2 > self.params['TWO_PASS_REJECT_LEVEL'],1,0)
		temp.close()
                if (os.access('TWOPASSTEMPsxt.fits',os.F_OK)): os.unlink('TWOPASSTEMPsxt.fits')
		temp = pyfits.open(sxtfile,'update')
		temp[0].data = temp[0].data * mask2
		temp.flush()
		temp.close()
	      if (os.access(ssfile, os.F_OK)): os.unlink(ssfile)
	   guiCount+=1.
	   if (guiMessages): print "PROGRESS: "+str(int(60+guiCount/len(self.filenames)*10))
	if (os.access('temp.cat',os.F_OK)): os.unlink('temp.cat')
        if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
	   os.unlink('TEMPsxt_wimg.fits')

        print "Creating master skies from other images with objects removed..."
        f = open(self.logfile,'ab')
        f.write("Creating master skies from other images with objects removed...\n")
        f.close()
        n = len(self.filenames)
	guiCount = 0.
        for j in range(n):
	   if (methods[j] != 'remove_objects'):
	      continue
           ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           if (not os.access(ssfile, os.F_OK)):
	      skyfilename = 'onsourceSkies/sky_'+self.filenames[j]
	      if (not os.access(skyfilename, os.F_OK)):
		#Find all filenames with prefix matching the object
                #and other matching criteria
                #Use these as skies
                skyfiles = self.selectSkyFiles("TEMPsxt_", self.filenames, j, rng, diff, self.filters, self.exptimes)
                if (skyfiles == ''):
                   print "Only one image found for object ",self.filenames[j]
                   print "Sky NOT subtracted!"
      		   f = open(self.logfile,'ab')
      		   f.write("Only one image found for object "+self.filenames[j]+'\n')
		   f.write('Sky NOT subtracted!\n')
      		   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
                   f.write("Sky NOT subtracted!\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   self.filenames[j] = 'REMOVE'
                   continue
                skyfilename = 'onsourceSkies/sky_'+self.filenames[j]
                #if (os.access(skyfilename, os.F_OK)): os.unlink(skyfilename)
                iraf.unlearn('imcombine')
                #Combine Skys with imcombine.  Scale each sky by the
                #reciprocal of its median and then median combine them.
                #imcombine can't handle strings longer than 1024 bytes
                if (len(skyfiles) > 1023):
                   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(skyfiles.replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'], lthreshold=1e-5)
                   os.unlink("tmpIRAFFile.dat")
                else:
                   iraf.images.immatch.imcombine(skyfiles, skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'], lthreshold=1e-5)
	      if (not os.access(skyfilename, os.F_OK)):
		print "ERROR: Sky not found for " + self.filenames[j]+ "!  Continuing with next image."
                f = open(self.logfile,'ab')
                f.write("ERROR: Sky not found for " + self.filenames[j]+ "!\n")
                f.write("Continuing with next image.\n")
                f.close()
		f = open(self.warnfile,'ab')
		f.write("ERROR: Sky not found for " + self.filenames[j]+ "!\n")
		f.write("Continuing with next image.\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
              masterSky = pyfits.open(skyfilename)
              image = pyfits.open(self.prefix+self.filenames[j])
	      #Interpolate over holes in master sky
              nbpm = -1
              for l in range(len(self.bpMask)):
		if (self.bpMask[l].filter == self.filters[j]): nbpm = l
	      if (self.params['INTERP_ZEROS_SKY'].lower() == 'yes'):
                if (nbpm != -1):
                   masterSky[0].data = linterp(masterSky[0].data, 0, self.bpMask[nbpm].getGoodPixelMask(), logfile=self.logfile)
                else:
                   masterSky[0].data = linterp(masterSky[0].data, 0, ones(shape(masterSky[0].data)), logfile = self.logfile)
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: Bad pixel mask not found for filter "+self.filters[j]+"\n")
                   f.write("\tduring sky subtraction.  Bad pixels will not be masked\n")
                   f.write("\tout when interpolating zeros across sky.\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()

              #Scale master sky to median of image and subtract
	      msmed = arraymedian(masterSky[0].data[where(masterSky[0].data != 0)])
	      immed = arraymedian(image[0].data[where(image[0].data != 0)])
              masterSky[0].data = masterSky[0].data * immed/msmed
              image[0].data = image[0].data-masterSky[0].data
              #Remove Cosmic Rays with 3 passes if specified
              if (rmcr=="yes"):
                print "Removing Cosmic Rays..."
      		f = open(self.logfile,'ab')
      		f.write("Removing Cosmic Rays...\n")
      		f.close()
                image[0].data = removeCosmicRays(image[0].data, crpass, logfile = self.logfile)
	      #Fit sky subtracted image with surface if specified to remove
	      #any gradients.
	      if (self.params['FIT_SKY_SUBTRACTED_SURF'].lower() == 'yes'):
		sxtfile = "TEMPsxt_"+self.filenames[j]
		if (os.access(sxtfile, os.F_OK)):
		   objMask = pyfits.open(sxtfile)
		   putmask(objMask[0].data, where(objMask[0].data != 0, 1, 0), 1)
		   objMask[0].data *= image[0].data
		   objMask.writeto('tempimmask.fits')
		   objMask.close()
		   if (os.access('imsurfit.fits', os.F_OK)): os.unlink('imsurfit.fits')
		   iraf.imfit.imsurfit('tempimmask.fits', 'imsurfit.fits', xorder=2, yorder=2, type_output='fit', function='legendre', niter=5, lower=2.5, upper=2.5)
                try:
                   os.unlink('tempimmask.fits')
                   objMask = pyfits.open('imsurfit.fits')
                   objMask[0].data *= self.bpMask[nbpm].getGoodPixelMask()
                   image[0].data -= objMask[0].data
                   objMask.close()
                   os.unlink('imsurfit.fits')
                except Exception:
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: The IRAF task imsurfit must have crashed.\n")
                   f.write("\tSurface not fit to sky subtracted image and removed.\n")
		   f.write("\tIf you are keeping on-source skies, try overwriting them so that\n")
		   f.write("\ta surface can be fit on-the-fly when creating skies.\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
              if (self.params['KEEP_SKIES'].lower() == 'no'):
		os.unlink(skyfilename)
           guiCount+=1.
           if (guiMessages):
	      print "PROGRESS: "+str(int(70+guiCount/len(self.filenames)*15))
	      print "FILE: "+ssfile
	for j in self.filenames:
           sxtfile = "TEMPsxt_"+j
	   if (os.access(sxtfile, os.F_OK)): os.unlink(sxtfile)
        while self.filenames.count('REMOVE'):
           index = where(numarray.strings.array(self.filenames) == 'REMOVE')[0][0]
           self.filenames.pop(index)
           self.filters.pop(index)
           self.exptimes.pop(index)

      if (methods.count('offsource') > 0 or methods.count('offsource_extended') > 0 or methods.count('offsource_neb') > 0):
	print "Creating master skies from off source images and subtracting..."
        f = open(self.logfile,'ab')
        f.write("Creating master skies from off source images and subtracting...\n")
        f.close()
	shift = self.params['SKY_OFFSOURCE_RANGE']/3600.
	n = len(self.filenames)
	tempskyfiles = []
	objRA = []
	objDec = []
	useSky = []
	isSky = zeros(n)
	if (self.params['SKY_OFFSOURCE_METHOD'].lower() == "auto"):
	   nsky = 0
	   #Create array with prefixes, RA and Dec for first object
	   #with each prefix (assumed to be an on-source).
	   #Create useSky array which contains the index of the correct
	   #sky to use for each image.
	   for j in range(n):
	      skyprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
	      if (tempskyfiles.count(skyprefix) == 0):
		tempskyfiles.append(skyprefix)
		temp = pyfits.open(self.prefix+self.filenames[j])
                tempDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
                objDec.append(tempDec)
                objRA.append(getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15)
		useSky.append([nsky])
		nsky+=1
		temp.close()
	      else:
		useSky.append([tempskyfiles.index(skyprefix)])

	elif (self.params['SKY_OFFSOURCE_METHOD'].lower() == 'manual'):
	   mfile = self.params['SKY_OFFSOURCE_MANUAL']
	   if (not os.access(mfile, os.R_OK)):
	      print "File "+mfile+" not found!"
              f = open(self.logfile,'ab')
              f.write("File "+mfile+" not found!\n")
              f.close()
              if (guiMessages):
                print "INPUT: File: Enter a file with manual offsource sky info:"
	      tmp = raw_input("Enter a file with manual offsource sky info: ")
	      if (guiMessages): print tmp
	      if (os.access(tmp, os.R_OK)):
		mfile = tmp
	      else:
		print "File " + tmp + "not found!"
		print "Sky not subtracted!"
      		f = open(self.logfile,'ab')
      		f.write("File " + tmp + "not found!\n")
		f.write("Sky not subtracted!\n")
      		f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: File "+mfile+" and file " + tmp + "not found!\n")
                f.write("Sky NOT subtracted!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		return
	   f = open(mfile, "rb")
	   s = f.read().split("\n")
	   f.close()
           try: s.remove('')
           except ValueError:
	      dummy = ''
	   tempskyfiles = []
	   for j in range(n):
	      useSky.append([])
	   isSky = zeros(n)
	   for j in range(len(s)):
	      mansky = s[j].split()
	      mansky[1] = int(mansky[1])
              mansky[2] = int(mansky[2])
              mansky[4] = int(mansky[4])
              mansky[5] = int(mansky[5])
	      tempskyfiles.append(mansky[3]+str(j))
	      for l in range(n):
		skyprefix = self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]
		skyindex = int(self.filenames[l][self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))+len(self.delim):self.filenames[l].rfind('.')])
		if (skyprefix.find(mansky[0]) != -1 and skyindex >= mansky[1] and skyindex <= mansky[2]):
		   useSky[l].append(j)
		if (skyprefix.find(mansky[3]) != -1 and skyindex >= mansky[4] and skyindex <= mansky[5]):
		   useSky[l].append(j)
		   isSky[l] = 1

	for j in range(n):
	   if (len(useSky[j]) == 0):
	      useSky[j].append(0)
	#Create master sky for each prefix/group
	guiCount = 0.
	for j in range(len(tempskyfiles)):
	   skyfiles = []
	   for l in range(n):
	      if (methods[l][0:9] != 'offsource'):
		continue
	      if (useSky[l].count(j) > 0):
		if (self.params['SKY_OFFSOURCE_METHOD'].lower() == "auto"):
		   temp = pyfits.open(self.prefix+self.filenames[l])
                   skyDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
                   skyRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              	   diffRA = (skyRA-objRA[j])*math.cos(skyDec*math.pi/180)
		   if (abs(diffRA) > shift or abs(skyDec-objDec[j]) > shift):
		      skyfiles.append(self.filenames[l])
		      isSky[l] = 1
		   temp.close()
		elif (self.params['SKY_OFFSOURCE_METHOD'].lower() == 'manual'):
		   if (isSky[l] == 1):
		      skyfiles.append(self.filenames[l])
	   if (os.access('offsourceSkies/sky_'+tempskyfiles[j]+'.fits', os.F_OK)): continue
	   if (len(skyfiles) == 0): continue
	   skyfiles = self.selectOffSkyFiles(self.prefix, skyfiles, diff) 
	   skyfilename = 'tempSky_'+tempskyfiles[j]+'.fits'
	   if (os.access(skyfilename, os.F_OK)): os.unlink(skyfilename)
           iraf.unlearn('imcombine')
           #Combine Skys with imcombine.  Scale each sky by the
           #reciprocal of its median and then median combine them.
           #imcombine can't handle strings longer than 1024 bytes
           if (len(skyfiles) > 1023):
	      tmpFile = open("tmpIRAFFile.dat", "wb")
              tmpFile.write(skyfiles.replace(",","\n"))
              tmpFile.close()
              iraf.images.immatch.imcombine("@tmpIRAFFile.dat", skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
              os.unlink("tmpIRAFFile.dat")
           else:
              iraf.images.immatch.imcombine(skyfiles, skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
           guiCount+=1.
           if (guiMessages): print "PROGRESS: "+str(int(45+guiCount/len(tempskyfiles)*10))

	#Apply master sky to each off source image
        print "Finding Objects and Creating Object Masks..."
      	f = open(self.logfile,'ab')
      	f.write("Finding Objects and Creating Object Masks...\n")
      	f.close()
        sxtPath = self.params['SEXTRACTOR_PATH']
        sxtConf = self.params['SEXTRACTOR_CONFIG_PATH']+"/"+self.params['SEXTRACTOR_CONFIG_PREFIX']
        #Run SExtractor on off source sky subtracted images to extract sources
        #Create object mask.  Treat as bad pixel mask and apply to flat
        #divided images.
	lastRA = 0
	lastDec = 0
	guiCount = 0.
        for j in range(n):
           ssfile = "skySubtracted/ss_"+self.filenames[j]
	   if (os.access(ssfile, os.F_OK)): continue
           if (isSky[j] == 1):
              image = pyfits.open(self.prefix+self.filenames[j])
              currDec = getRADec(image[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              currRA = getRADec(image[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              diffRA = (currRA-lastRA)*math.cos(lastDec*math.pi/180)
              if ((abs(diffRA) < diff and abs(currDec-lastDec) < diff)):
		image.close()
		continue
	      lastDec = currDec
	      lastRA = currRA
	      if (os.access('offsourceSkies/sky_'+tempskyfiles[useSky[j][0]]+'.fits', os.F_OK)): continue
              skyfilename = 'tempSky_'+tempskyfiles[useSky[j][0]]+'.fits'
              masterSky = pyfits.open(skyfilename)
              #Scale master sky to median of image and subtract
              msmed = arraymedian(compress(where(masterSky[0].data != 0, 1, 0), masterSky[0].data))
              immed = arraymedian(compress(where(image[0].data != 0, 1, 0), image[0].data))
              masterSky[0].data = masterSky[0].data * immed/msmed
              image[0].data = image[0].data-masterSky[0].data
              image.verify('silentfix')
	      if (os.access('tempSky.fits',os.F_OK)): os.unlink('tempSky.fits')
              image.writeto('tempSky.fits')
              image.close()
              masterSky.close()
              sxtfile = "TEMPsxt_"+self.filenames[j]
              for l in range(len(self.bpMask)):
		if (self.bpMask[l].filter == self.filters[j]): nbpm = l
              if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
                os.unlink('TEMPsxt_wimg.fits')
              if (nbpm >= 0): self.bpMask[nbpm].saveGoodPixelMask('TEMPsxt_wimg.fits')
              if (os.access(sxtfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(sxtfile)
              if (not os.access(sxtfile, os.F_OK)):
                if (nbpm < 0):
                   temp = pyfits.open('tempSky.fits')
                   temp[0].data[:,:] = 1
                   temp.writeto('TEMPsxt_wimg.fits')
                   temp.close()
		sxtCom = self.params['SEXTRACTOR_PATH']+" tempSky.fits -c " + sxtConf + ".sex -CATALOG_NAME temp.cat -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE SEGMENTATION -CHECKIMAGE_NAME " + sxtfile + " -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
		if (guiMessages):
                   sxtCom+=" -VERBOSE_TYPE QUIET"
                   print "Sextracting tempSky.fits" 
                os.system(sxtCom)
              try:
                temp = pyfits.open(sxtfile)
              except Exception:
                f = open(self.warnfile,'ab')
                f.write("WARNING: sextractor not successful.  Check to see thatit is properly\n")
                f.write('\tinstalled.  As a result, objects have not been removied in the\n')
                f.write('\toffsource sky file '+ssfile+'\n')
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
                temp = pyfits.open(ssfile)
                temp[0].data[:,:] = 0.
              objMask = badPixelMask(inputMask = where(temp[0].data > 0, 1, 0))
              temp.close()
              os.unlink(sxtfile)
              temp = pyfits.open(self.prefix+self.filenames[j])
              temp[0].data = temp[0].data * objMask.getGoodPixelMask()
              temp.writeto(sxtfile)
              temp.close()
              if (self.params['TWO_PASS_OBJECT_MASKING'].lower() == 'yes'):
                if (os.access('temp.cat', os.F_OK)): os.unlink('temp.cat')
                sxtCom = self.params['SEXTRACTOR_PATH']+" tempSky.fits -c " +sxtConf + ".sex -DETECT_MINAREA " + self.params['TWO_PASS_DETECT_MINAREA'] + " -DETECT_THRESH " + self.params['TWO_PASS_DETECT_THRESH'] + " -CATALOG_NAME temp.cat -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE SEGMENTATION -CHECKIMAGE_NAME TWOPASSTEMPsxt.fits -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
		if (guiMessages):
                   sxtCom+=" -VERBOSE_TYPE QUIET"
                   print "Sextracting tempSky.fits for two pass object masking"
                os.system(sxtCom)
                try:
                   temp = pyfits.open('TWOPASSTEMPsxt.fits')
                except Exception:
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: sextractor not successful.  Check to see that it is properly\n")
                   f.write('\tinstalled.  As a result, object masking can not be done\n')
                   f.write('\tfor the file '+ssfile+'\n')
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   temp = pyfits.open(ssfile)
                   temp[0].data[:,:] = 0.
                mask2 = badPixelMask(inputMask = where(temp[0].data > 0,1,0)).getGoodPixelMask()
                mask2 = smooth(mask2, self.params['TWO_PASS_BOXCAR_SIZE'], 1)
                mask2 = where(mask2 > self.params['TWO_PASS_REJECT_LEVEL'],1,0)
                temp.close()
                if (os.access('TWOPASSTEMPsxt.fits', os.F_OK)): os.unlink('TWOPASSTEMPsxt.fits')
                temp = pyfits.open(sxtfile,'update')
                temp[0].data = temp[0].data * mask2
                temp.flush()
                temp.close()
           guiCount+=1.
           if (guiMessages): print "PROGRESS: "+str(int(55+guiCount/len(self.filenames)*10))
        if (os.access('temp.cat', os.F_OK)): os.unlink('temp.cat')
        if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
           os.unlink('TEMPsxt_wimg.fits')

	#Pass 2: create master skies with objects removed
        print "Creating master skies from other images with objects removed..."
      	f = open(self.logfile,'ab')
      	f.write("Creating master skies from other images with objects removed...\n")
      	f.close()
        if (not os.access("offsourceSkies", os.F_OK)):
	   os.mkdir("offsourceSkies",0755)
	guiCount = 0.
        for j in range(len(tempskyfiles)):
	   if (os.access('offsourceSkies/sky_'+tempskyfiles[j]+'.fits', os.F_OK)): continue
	   skyfiles = []
           for l in range(n):
	      if (useSky[l].count(j) > 0 and isSky[l] == 1):
		skyfiles.append(self.filenames[l])
	   if (len(skyfiles) == 0): continue
           skyfiles = self.selectOffSkyFiles("TEMPsxt_", skyfiles, diff)
           skyfilename = 'offsourceSkies/sky_'+tempskyfiles[j]+'.fits'
           if (os.access(skyfilename, os.F_OK)): os.unlink(skyfilename)
           iraf.unlearn('imcombine')
           #Combine Skys with imcombine.  Scale each sky by the
           #reciprocal of its median and then median combine them.
           #imcombine can't handle strings longer than 1024 bytes
           if (len(skyfiles) > 1023):
	      tmpFile = open("tmpIRAFFile.dat", "wb")
              tmpFile.write(skyfiles.replace(",","\n"))
              tmpFile.close()
              iraf.images.immatch.imcombine("@tmpIRAFFile.dat", skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'], lthreshold=1e-5)
              os.unlink("tmpIRAFFile.dat")
           else:
              iraf.images.immatch.imcombine(skyfiles, skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'], lthreshold=1e-5)
           guiCount+=1.
           if (guiMessages): print "PROGRESS: "+str(int(65+guiCount/len(tempskyfiles)*10))

	#Subtract final master skies from images
	print "Subtracting master skies from images..."
        f = open(self.logfile,'ab')
        f.write("Subtracting master skies from images...\n")
        f.close()
        #Interpolate over holes in master sky
        if (self.params['INTERP_ZEROS_SKY'].lower() == 'yes'):
	   for j in range(len(tempskyfiles)):
              skyfilename = 'offsourceSkies/sky_'+tempskyfiles[j]+'.fits'
	      masterSky = pyfits.open(skyfilename,'update')
              nbpm = -1
              for l in range(len(self.bpMask)):
		for i in range(len(self.params['FILTER_KEYWORD'])):
		   if (self.bpMask[l].filter == masterSky[0].header[self.params['FILTER_KEYWORD'][i]]):
		      nbpm = l
		      break
              if (nbpm != -1):
                masterSky[0].data = linterp(masterSky[0].data, 0, self.bpMask[nbpm].getGoodPixelMask(), logfile=self.logfile)
              else:
                masterSky[0].data = linterp(masterSky[0].data, 0, ones(shape(masterSky[0].data)), logfile = self.logfile)
                f = open(self.warnfile,'ab')
		msfilter = 'None'
		for i in range(len(self.params['FILTER_KEYWORD'])):
		   if (isValidFilter(masterSky[0].header[self.params['FILTER_KEYWORD'][i]])):
		      msfilter = masterSky[0].header[self.params['FILTER_KEYWORD'][i]]
		      break
                f.write("WARNING: Bad pixel mask not found for filter "+msfilter+"\n")
                f.write("\tduring sky subtraction.  Bad pixels will not be masked\n")
                f.write("\tout when interpolating zeros across sky.\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
	      masterSky.flush()
	      masterSky.close()
	#Find median scaling factors if offsource_neb
	if (methods.count('offsource_neb') > 0):
           try:
              nebImmeds = nebObjSky(self.prefix, self.filenames, isSky, useSky, self.params['SKY_NEB_FILE_RANGE'], self.params['UT_KEYWORD'], self.params['DATE_KEYWORD'])
           except Exception:
              nebImmeds = zeros(n)
	guiCount = 0.
	for j in range(n):
           ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           if (not os.access(ssfile, os.F_OK) and isSky[j] != 1):
	      skyfilename = 'offsourceSkies/sky_'+tempskyfiles[useSky[j][0]]+'.fits'
              masterSky = pyfits.open(skyfilename)
	      image = pyfits.open(self.prefix+self.filenames[j])
              #Scale master sky to median of image and subtract
              msmed = arraymedian(compress(where(masterSky[0].data != 0, 1, 0), masterSky[0].data))
	      if (methods[j] == 'offsource_extended'):
		immed = extObjSky(image[0].data, masterSky[0].data)
	      elif (methods[j] == 'offsource_neb'):
		if (nebImmeds[j] != 0):
                   immed = nebImmeds[j]
                else:
                   immed = arraymedian(compress(where(image[0].data != 0, 1, 0), image[0].data))
	      else:
                immed = arraymedian(compress(where(image[0].data != 0, 1, 0), image[0].data))
	      masterSky[0].data = masterSky[0].data * immed/msmed
	      image[0].data = image[0].data-masterSky[0].data
	      #Remove Cosmic Rays with 3 passes if specified
	      if (rmcr=="yes"):
	        print "Removing Cosmic Rays..."
      		f = open(self.logfile,'ab')
      		f.write("Removing Cosmic Rays...\n")
      		f.close()
	        image[0].data = removeCosmicRays(image[0].data, crpass, logfile = self.logfile)
              #Fit sky subtracted image with surface if specified to remove
              #any gradients.
              if (self.params['FIT_SKY_SUBTRACTED_SURF'].lower() == 'yes'):
		image.verify('silentfix')
	        image.writeto(ssfile)
                sxtfile = "TEMPsxt_"+self.filenames[j]
                for l in range(len(self.bpMask)):
		   if (self.bpMask[l].filter == self.filters[j]): nbpm = l
                if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
                  os.unlink('TEMPsxt_wimg.fits')
                if (nbpm >= 0): self.bpMask[nbpm].saveGoodPixelMask('TEMPsxt_wimg.fits')
                if (os.access(sxtfile, os.F_OK)): os.unlink(sxtfile)
                if (nbpm < 0):
                   temp = pyfits.open(ssfile)
                   temp[0].data[:,:] = 1
                   temp.writeto('TEMPsxt_wimg.fits')
                   temp.close()
                sxtCom = self.params['SEXTRACTOR_PATH']+" " + ssfile + " -c " + sxtConf + ".sex -CATALOG_NAME temp.cat -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE SEGMENTATION -CHECKIMAGE_NAME " + sxtfile + " -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
		if (guiMessages):
                   sxtCom+=" -VERBOSE_TYPE QUIET"
                   print "Sextracting "+ssfile
                os.system(sxtCom)
                if (os.access('TEMPsxt_wimg.fits',os.F_OK)): os.unlink('TEMPsxt_wimg.fits')
                if (os.access(ssfile, os.F_OK)): os.unlink(ssfile)
                try:
                   objMask = pyfits.open(sxtfile)
                except Exception:
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: sextractor not successful.  Check to see thatit is properly\n")
                   f.write('\tinstalled.  As a result, objects have not been removied before fitting\n')
                   f.write('\tthe sky background level '+ssfile+'\n')
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   objMask = pyfits.open(ssfile)
                   objMask[0].data[:,:] = 0.
                putmask(objMask[0].data, where(objMask[0].data != 0, 1, 0), 1)
		objMask[0].data = 1 - objMask[0].data
                objMask[0].data *= image[0].data
                objMask.writeto('tempimmask.fits')
                objMask.close()
                if (os.access('imsurfit.fits', os.F_OK)): os.unlink('imsurfit.fits')
                iraf.imfit.imsurfit('tempimmask.fits', 'imsurfit.fits', xorder=2, yorder=2, type_output='fit', function='legendre', niter=5, lower=2.5, upper=2.5)
                try:
                   os.unlink('tempimmask.fits')
                   objMask = pyfits.open('imsurfit.fits')
                   objMask[0].data *= self.bpMask[nbpm].getGoodPixelMask()
                   image[0].data -= objMask[0].data
                   objMask.close()
                   os.unlink('imsurfit.fits')
                except Exception:
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: The IRAF task imsurfit must have crashed.\n")
                   f.write("\tSurface not fit to sky subtracted image and removed.\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                if (os.access(sxtfile, os.F_OK)): os.unlink(sxtfile)
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile) 
              image.verify('silentfix')
	      image.writeto(ssfile)
	      image.close()
	      masterSky.close()
           guiCount+=1.
           if (guiMessages and isSky[j] != 1):
	      print "PROGRESS: "+str(int(75+guiCount/len(self.filenames)*10))
	      print "FILE: "+ssfile

	if (os.access('TEMPoffsourceSXT.fits', os.F_OK)):
	   os.unlink('TEMPoffsourceSXT.fits')
	if (os.access('tempSky.fits',os.F_OK)): os.unlink('tempSky.fits')
	for j in tempskyfiles:
           skyfilename = 'tempSky_'+j+'.fits'
	   if (os.access(skyfilename,os.F_OK)): os.unlink(skyfilename)
	if (self.params['KEEP_SKIES'].lower() == 'no'):
	   for j in tempskyfiles:
	      skyfilename = 'offsourceSkies/sky_'+j+'.fits'
	      if (os.access(skyfilename,os.F_OK)): os.unlink(skyfilename)
	for j in self.filenames:
           sxtfile = "TEMPsxt_"+j
	   if (os.access(sxtfile,os.F_OK)): os.unlink(sxtfile)
	for j in range(len(self.filenames)-1,-1,-1):
	   if (isSky[j] == 1):
	      if (self.params['CLEAN_UP'].lower() == 'yes'):
		if (os.access('flatDivided/fd_'+self.filenames[j], os.F_OK)):
		   os.unlink('flatDivided/fd_'+self.filenames[j])
	      self.filenames.pop(j)
	      self.filters.pop(j)
	      self.exptimes.pop(j)
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
           if (os.access('flatDivided/fd_'+j, os.F_OK)):
              os.unlink('flatDivided/fd_'+j)
      self.prefix = "skySubtracted/ss_"

   def alignStack(self, infile):
      if (self.guiMessages): print "STATUS: Aligning and Stacking..."
      prefixes = numarray.strings.array(self.filenames)
      tmpfilters = numarray.strings.array(self.filters)
      for j in range(len(prefixes)):
	prefixes[j] = prefixes[j][:prefixes[j].rfind(self.delim,0,prefixes[j].rfind('.'))]
      if (not os.access("alignedStacked", os.F_OK)): os.mkdir("alignedStacked",0755)
      print "Performing Geom. Distortion Correction, Image Alignment, and Stacking..." 
      f = open(self.logfile,'ab')
      f.write("Performing Geom. Distortion Correction, Image Alignment, and Stacking...\n")
      f.close()
      boxsize = self.params['ALIGN_BOX_SIZE']
      xboxcen = self.params['ALIGN_BOX_CENTER_X']
      yboxcen = self.params['ALIGN_BOX_CENTER_Y']
      n = len(self.filenames)
      guiCount = 0.
      while (n > 0):
	#Select all objects with the same prefix for processing
        pfix = prefixes[0] 
        filter = tmpfilters[0]
	b = (where(prefixes != pfix, 1, 0)+where(tmpfilters != filter, 1, 0)+1)/2
        prefixes = compress(b, prefixes)
	tmpfilters = compress(b, tmpfilters)
        print "Processing object: ",pfix," with filter "+filter+"..."
      	f = open(self.logfile,'ab')
      	f.write('Processing object: '+pfix+' with filter '+filter+'...\n')
      	f.close()
	frames = []
	isSameName = False
        for j in range(len(self.filenames)):
           if (self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))] == pfix):
	      if (self.filters[j] == filter):
		frames.append(self.filenames[j])
                n-=1
	      else:
		isSameName = True
	if (len(frames) == 1):
	  print "Object ",frames[0]," has only one frame!"
          print "Alignment and Stacking NOT done!"
      	  f = open(self.logfile,'ab')
      	  f.write('Object '+frames[0]+' has only one frame!\n')
	  f.write('Alignment and Stacking NOT done!\n')
      	  f.close()
          f = open(self.warnfile,'ab')
          f.write("WARNING: Object "+frames[0]+' has only one frame!\n')
          f.write("Alignment and Stacking NOT done!\n")
          f.write('Time: '+str(datetime.today())+'\n\n')
          f.close()
	  continue
	if (not isSameName):
	   asfile = "alignedStacked/as_"+pfix+'.fits'
	   expfile = "alignedStacked/expmap_"+pfix+'.fits'
	   objfile = "alignedStacked/objmap_"+pfix+'.fits'
	else:
	   asfile = "alignedStacked/as_"+pfix+"_"+filter+".fits"
           expfile = "alignedStacked/expmap_"+pfix+"_"+filter+".fits"
           objfile = "alignedStacked/objmap_"+pfix+"_"+filter+".fits"
        if (os.access(asfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(asfile)
	if (os.access(expfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(expfile)
        if (os.access(objfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(objfile)
	if (os.access(asfile, os.F_OK)):
           if (self.guiMessages): print "FILE: "+asfile
	   continue
	#Read info from the first file of the object
	temp = pyfits.open(self.prefix+frames[0])
	xsize = temp[0].data.shape[0]
	ysize = temp[0].data.shape[1]
	for i in range(len(self.params['FILTER_KEYWORD'])):
	   if (isValidFilter(temp[0].header[self.params['FILTER_KEYWORD'][i]])):
	      filter = temp[0].header[self.params['FILTER_KEYWORD'][i]]
	      break
        for j in range(len(self.bpMask)):
           if (self.bpMask[j].filter == filter): nbpm = j
        refDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
        refRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
	scale = float(temp[0].header[self.params['PIXSCALE_KEYWORD']])
        if (self.params['PIXSCALE_UNITS'].lower() == "degrees"):
	   scale*=3600.
	temp.close()
        xrref = self.prefix+frames[0]
	xrin = '' 
	#Create coords.dat file which uses RA and Dec to create a rough guess
	#at the offsets to input to xregister.
	crdfile = open('coords.dat','wb')
	crdfile.write(str(xboxcen)+"\t"+str(yboxcen)+"\n")
	diffX = [0]
	diffY = [0]
	for j in range(1, len(frames), 1):
	   temp=pyfits.open(self.prefix+frames[j])
	   theta=temp[0].header[self.params['ROT_PA_KEYWORD']]*math.pi/180.
           currDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
           currRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
           diffDec = 3600*(currDec-refDec)/scale
           diffRA = 3600*(currRA-refRA)/scale*math.cos(refDec*math.pi/180)
	   diffY.append(diffDec*math.cos(theta)+diffRA*math.sin(theta))
	   diffX.append(-1*diffDec*math.sin(theta)+diffRA*math.cos(theta))
	   crdfile.write(str(xboxcen+diffX[j])+"\t"+str(yboxcen+diffY[j])+"\n")
	   temp.close()
	   xrin += self.prefix+frames[j]+","
	crdfile.close()
	if (self.params['TWO_PASS_ALIGNMENT'].lower() == 'yes'):
           crdfile = open('coords.dat','wb')
           crdfile.write(str(xboxcen)+"\t"+str(yboxcen)+"\n")
           sxtPath = self.params['SEXTRACTOR_PATH']
           sxtConf = self.params['SEXTRACTOR_CONFIG_PATH']+"/"+self.params['SEXTRACTOR_CONFIG_PREFIX']
	   for j in range(len(frames)):
	      sxtfile = 'TEMPsxt_'+frames[j]+'.cat'
	      for l in range(len(self.bpMask)):
                if (self.bpMask[l].filter == self.filters[self.filenames.index(frames[j])]): nbpm = l
              if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
                os.unlink('TEMPsxt_wimg.fits')
	      if (os.access(sxtfile, os.F_OK)): os.unlink(sxtfile)
              if (nbpm >= 0): self.bpMask[nbpm].saveGoodPixelMask('TEMPsxt_wimg.fits')
              if (os.access(sxtfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(sxtfile)
              if (nbpm < 0):
                temp = pyfits.open(self.prefix+frames[j])
                temp[0].data[:,:] = 1
                temp.writeto('TEMPsxt_wimg.fits')
                temp.close()
              sxtCom = self.params['SEXTRACTOR_PATH']+" " + self.prefix+frames[j] + " -c " + sxtConf + ".sex -CATALOG_NAME " + sxtfile + " -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE NONE -WEIGHT_IMAGE TEMPsxt_wimg.fits -WEIGHT_TYPE MAP_WEIGHT"
              if (guiMessages):
                sxtCom+=" -VERBOSE_TYPE QUIET"
                print "Sextracting "+self.prefix+frames[j]
              os.system(sxtCom)
              if (not os.access(sxtfile, os.F_OK)):
                f = open(self.warnfile,'ab')
                f.write("WARNING: sextractor not successful.  Check to see that it is properly\n")
                f.write('\tinstalled.  As a result, alignment and stacking has not been done\n')
                f.write('\tfor the object '+pfix+'\n')
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
	      if (j == 0):
		f = open(sxtfile, 'rb')
		s = f.read().split('\n')
		f.close()
		xcol = -1
		ycol = -1
		fcol = -1
		for l in range(len(s)-1, -1, -1):
		   if (s[l] == '' or s[l][0] == '#'):
		      temp = s.pop(l)
		      if (temp.find('X_IMAGE') != -1): xcol = l
		      if (temp.find('Y_IMAGE') != -1): ycol = l
		      if (temp.find('FLUX_AUTO') != -1): fcol = l
		if (xcol == -1):
		   print 'Error: could not find X_IMAGE in sextractor catalog.'
      		   f = open(self.logfile,'ab')
      		   f.write('Error: could not find X_IMAGE in sextractor catalog.\n')
      		   f.close()
                   f = open(self.warnfile,'ab')
          	   f.write("ERROR: could not find X_IMAGE in sextractor catalog.\n")
          	   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
		   f.write(str(len(frames))+" files affected.")
          	   f.write('Time: '+str(datetime.today())+'\n\n')
          	   f.close()
		   break
		if (ycol == -1):
		   print 'Error: could not find Y_IMAGE in sextractor catalog.'
                   f = open(self.logfile,'ab')
                   f.write('Error: could not find Y_IMAGE in sextractor catalog.\n')
                   f.close()
                   f.write("ERROR: could not find Y_IMAGE in sextractor catalog.\n")
                   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
                   f.write(str(len(frames))+" files affected.")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   break
		if (fcol == -1):
                   print 'Error: could not find FLUX_AUTO in sextractor catalog.'
                   f = open(self.logfile,'ab')
                   f.write('Error: could not find FLUX_AUTO in sextractor catalog.\n')
                   f.close()
                   f.write("ERROR: could not find FLUX_AUTO in sextractor catalog.\n")
                   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
                   f.write(str(len(frames))+" files affected.")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   break
		refXs = []
		refYs = []
		refFlux = []
		for l in s:
		   refXs.append(float(l.split()[xcol]))
		   refYs.append(float(l.split()[ycol]))
		   refFlux.append(float(l.split()[fcol]))
		refXs = array(refXs)
		refYs = array(refYs)
		refFlux = array(refFlux)
		b = (refFlux.argsort()[::-1])[0:200]
		refXs = refXs[b]
		refYs = refYs[b]
		refFlux = refFlux[b]
		f = open('refsxtcoords.dat','wb')
		for l in range(len(refXs)):
		   s = str(refXs[l])+'\t'+str(refYs[l])+'\n'
		   f.write(s)
		f.close()
	      else:
                f = open(sxtfile, 'rb')
                s = f.read().split('\n')
                f.close()
                xcol = -1
                ycol = -1
                fcol = -1
                for l in range(len(s)-1, -1, -1):
                   if (s[l] == '' or s[l][0] == '#'):
                      temp = s.pop(l)
                      if (temp.find('X_IMAGE') != -1): xcol = l
                      if (temp.find('Y_IMAGE') != -1): ycol = l
                      if (temp.find('FLUX_AUTO') != -1): fcol = l
                if (xcol == -1):
                   print 'Error: could not find X_IMAGE in sextractor catalog.'
                   f = open(self.logfile,'ab')
                   f.write('Error: could not find X_IMAGE in sextractor catalog.\n')
                   f.close()
                   f.write("ERROR: could not find X_IMAGE in sextractor catalog.\n")
                   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
                   f.write(str(len(frames))+" files affected.")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()       
                   break
                if (ycol == -1):
                   print 'Error: could not find Y_IMAGE in sextractor catalog.'
                   f = open(self.logfile,'ab')
                   f.write('Error: could not find Y_IMAGE in sextractor catalog.\n')
                   f.close()
                   f.write("ERROR: could not find Y_IMAGE in sextractor catalog.\n")
                   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
                   f.write(str(len(frames))+" files affected.")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   break
                if (fcol == -1):
                   print 'Error: could not find FLUX_AUTO in sextractor catalog.'
                   f = open(self.logfile,'ab')
                   f.write('Error: could not find FLUX_AUTO in sextractor catalog.\n')
                   f.close()
                   f.write("ERROR: could not find FLUX_AUTO in sextractor catalog.\n")
                   f.write("Alignment and Stacking NOT done for prefix "+pfix+"!\n")
                   f.write(str(len(frames))+" files affected.")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   break
                refXs = []
                refYs = []
                refFlux = []
                for l in s:
                   refXs.append(float(l.split()[xcol]))
                   refYs.append(float(l.split()[ycol]))
                   refFlux.append(float(l.split()[fcol]))
                refXs = array(refXs)
                refYs = array(refYs)
                refFlux = array(refFlux)
                b = (refFlux.argsort()[::-1])[0:200]
                refXs = refXs[b]
                refYs = refYs[b]
                refFlux = refFlux[b]
                f = open('tempsxtcoords.dat','wb')
                for l in range(len(refXs)):
                   s = str(refXs[l])+'\t'+str(refYs[l])+'\n'
                   f.write(s)
                f.close()
		iraf.unlearn('xyxymatch')
		xycat = frames[j]+'XYXY.dat'
		if (os.access(xycat, os.F_OK)): os.unlink(xycat)
		iraf.images.immatch.xyxymatch('tempsxtcoords.dat','refsxtcoords.dat',xycat,self.params['XYXYMATCH_TOLERANCE'],xref=-1*diffX[j], yref=-1*diffY[j], matching='triangles')
		f = open(xycat, 'rb')
		s = f.read().split('\n')
		f.close()
		for l in range(len(s)-1, -1, -1):
		   if (s[l] == '' or s[l][0] == '#'):
		      s.pop(l)
		refXs = []
		refYs = []
		for l in s:
		   refXs.append(float(l.split()[0])-float(l.split()[2]))
		   refYs.append(float(l.split()[1])-float(l.split()[3]))
		refXs = array(refXs)
		refYs = array(refYs)
		if (len(refXs) > 0):
		   diffX[j] = arraymedian(refXs)
		   diffY[j] = arraymedian(refYs)
                   crdfile.write(str(xboxcen-diffX[j])+"\t"+str(yboxcen-diffY[j])+"\n")
		else:
		   xrin = xrin.replace(","+self.prefix+frames[j],"")
		   xrin = xrin.replace(self.prefix+frames[j]+",","")
                   print "IRAF task xyxymatch failed for frame "+frames[j]
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: IRAF task xyxymatch failed for frame "+frames[j]+"\n")
                   f.write("\tThis frame has been discarded and will not be used in the\n")
                   f.write("\taligned and stacked image "+pfix+"!\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   frames[j] = 'REMOVE'
	        os.unlink(xycat)
	      os.unlink(sxtfile)
	   if (os.access('refsxtcoords.dat', os.F_OK)):
	      os.unlink('refsxtcoords.dat')
           if (os.access('tempsxtcoords.dat', os.F_OK)):
	      os.unlink('tempsxtcoords.dat')
           if (os.access('TEMPsxt_wimg.fits',os.F_OK)):
              os.unlink('TEMPsxt_wimg.fits')                                  
           crdfile.close()
	   for j in range(len(frames)-1,-1,-1):
	      if (frames[j] == 'REMOVE'):
		frames.pop(j)
		diffX.pop(j)
		diffY.pop(j)

	#Use xregister to refine the offsets and write them to shifts.dat.
	xrin = xrin.strip(",")
	xrreg = '['+str(int(xboxcen-boxsize/2))+':'+str(int(xboxcen+boxsize/2)-1)+','+str(int(yboxcen-boxsize/2))+':'+str(int(yboxcen+boxsize/2)-1)+']'
	iraf.unlearn('xregister')
	if (os.access('shifts.dat', os.F_OK)): os.unlink('shifts.dat')
        #xregister can't handle strings longer than 1024 bytes
        if (len(xrin) > 1023):
	   tmpFile = open("tmpIRAFFile.dat", "wb")
           tmpFile.write(xrin.replace(",","\n"))
           tmpFile.close()
           iraf.images.immatch.xregister("@tmpIRAFFile.dat",xrref,xrreg,'shifts.dat',databasefmt='no',coords='coords.dat', background='median') 
           os.unlink("tmpIRAFFile.dat")
        else:
	   iraf.images.immatch.xregister(xrin,xrref,xrreg,'shifts.dat',databasefmt='no',coords='coords.dat', background='median')
	os.unlink('coords.dat')
	f = open('shifts.dat','rb')
	s = f.read().split()
	f.close()
	os.unlink('shifts.dat')
	#Read the shifts into two lists
	xshifts = [0]
	yshifts = [0]
	for j in range(len(s)/3):
	   xshifts.append(float(s[j*3+1]))
	   yshifts.append(float(s[j*3+2]))
           if (xshifts[j] == int(xshifts[j]) or yshifts[j] == int(yshifts[j])):
              print "WARNING: Alignment may be off for frame "+frames[j+1]
              f = open(self.warnfile,'ab')
              f.write("WARNING: Alignment may be off for frame "+frames[j+1]+"\n")
              f.write("\tCheck your ALIGN_BOX and consider TWO_PASS_ALIGNMENT!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
        #Read the geometric transformation file, if one, and use it to help
        #compute the final image size
	if (self.params['GEOM_TRANS_COEFFS'] != ''):
	   f = open(self.params['GEOM_TRANS_COEFFS'], 'rb')
	   try: order = int(f.readline().strip('poly'))
	   except ValueError:
	      order = -1
	      print "WARNING: Geometric Trans Coeffs file "+self.params['GEOM_TRANS_COEFFS']+"is misformatted!  It will NOT be used!"
	      f = open(self.warnfile,'ab')
	      f.write("WARNING: Geometric Trans Coeffs file "+self.params['GEOM_TRANS_COEFFS']+"is misformatted!\n")
	      f.write("It will NOT be used!\n")
	      f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      self.params['GEOM_TRANS_COEFFS'] = ''
	   terms = 0
	   for j in range(order+2): terms+=j
	   xtran = zeros(terms)
	   ytran = zeros(terms)
	   try:
	      for j in range(terms): xtran[j] = float(f.readline())
	      f.readline()
              for j in range(terms): ytran[j] = float(f.readline())
	   except Exception:
              order = -1
	      terms = 0
              print "WARNING: Geometric Trans Coeffs file "+self.params['GEOM_TRANS_COEFFS']+"is misformatted!  It will NOT be used!"
              f = open(self.warnfile,'ab')
              f.write("WARNING: Geometric Trans Coeffs file "+self.params['GEOM_TRANS_COEFFS']+"is misformatted!\n")
              f.write("It will NOT be used!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              self.params['GEOM_TRANS_COEFFS'] = ''
	   #xcin = [-1*min(xshifts),-1*min(xshifts),xsize+max(xshifts),xsize+max(xshifts)]
	   #ycin = [-1*min(yshifts),ysize+max(yshifts),-1*min(yshifts),ysize+max(yshifts)]
	   xcin = [0,0,xsize,xsize]
	   ycin = [0,ysize,0,ysize]
	   xcout = zeros(4)
	   ycout = zeros(4)
	   for i in range(4):
	      nterm = 0
	      for j in range(order+1):
		for l in range(j+1):
		   xcout[i]+=xtran[nterm]*xcin[i]**(j-l)*ycin[i]**l
		   ycout[i]+=ytran[nterm]*xcin[i]**(j-l)*ycin[i]**l
		   nterm+=1
	   if (max(xcout) >= xsize-1): xsize+=int(max(xcout)-xsize+5)
	   if (max(ycout) >= ysize-1): ysize+=int(max(ycout)-ysize+5)
	   if (min(xcout) <= 0): xsize+=int(5-min(xcout))
	   if (min(ycout) <= 0): ysize+=int(5-min(ycout))
	#Use the shifts to further determine the final image size
        xsize=int(xsize+max(xshifts)-min(xshifts)+11)
        ysize=int(ysize+max(yshifts)-min(yshifts)+11)
	xshift0 = -1*int(max(xshifts)+min(xshifts))/2
	yshift0 = -1*int(max(yshifts)+min(yshifts))/2
	#Drizzle each frame into the final image and exposure mask
	#or drizzle frames into individual files.
	iraf.unlearn('drizzle')
        for l in range(len(self.bpMask)):
           if (self.bpMask[l].filter == filter): nbpm = l
        if (os.access('TEMPdriz_inmask.fits',os.F_OK)):
           os.unlink('TEMPdriz_inmask.fits')
        if (nbpm >= 0): self.bpMask[nbpm].saveGoodPixelMask('TEMPdriz_inmask.fits')
	for j in range(len(frames)):
           if (nbpm < 0):
              temp = pyfits.open(self.prefix+frames[j])
              temp[0].data[:,:] = 1
              temp.writeto('TEMPsxt_wimg.fits')
              temp.close()
	   if (self.params['DRIZZLE_OR_IMCOMBINE'].lower() == 'imcombine'):
	      if (os.access('alignedStacked/driz_'+frames[j], os.F_OK)):
		continue
	      iraf.stsdas.analysis.dither.drizzle(self.prefix+frames[j], 'alignedStacked/driz_'+frames[j], outweig='alignedStacked/exp_'+frames[j], in_mask='TEMPdriz_inmask.fits', wt_scl='exptime',outnx=xsize, outny=ysize, xsh=xshift0+xshifts[j], ysh=yshift0+yshifts[j], expkey=self.params['EXPTIME_KEYWORD'], pixfrac=self.params['DRIZZLE_DROPSIZE'], coeffs=self.params['GEOM_TRANS_COEFFS'], kernel=self.params['DRIZZLE_KERNEL'], in_un=self.params['DRIZZLE_IN_UNITS'], out_un='cps') 
              #Set bad pixels to -1e+7 so they will be rejected by imcombine
              tempDriz = pyfits.open('alignedStacked/driz_'+frames[j], 'update')
              putmask(tempDriz[0].data, where(tempDriz[0].data == 0, 1, 0), -1e+7)
              tempDriz.verify('silentfix')
              tempDriz.flush()
              tempDriz.close()
           else:
	      iraf.stsdas.analysis.dither.drizzle(self.prefix+frames[j], asfile, outweig=expfile, in_mask = 'TEMPdriz_inmask.fits', wt_scl='exptime',outnx=xsize, outny=ysize, xsh=xshift0+xshifts[j], ysh=yshift0+yshifts[j], expkey=self.params['EXPTIME_KEYWORD'], pixfrac=self.params['DRIZZLE_DROPSIZE'], coeffs=self.params['GEOM_TRANS_COEFFS'], kernel=self.params['DRIZZLE_KERNEL'], in_un=self.params['DRIZZLE_IN_UNITS'], out_un='cps')
           guiCount+=1.
           if (guiMessages):print "PROGRESS: "+str(int(85+guiCount/len(self.filenames)*14))
        if (os.access('TEMPdriz_inmask.fits',os.F_OK)):
           os.unlink('TEMPdriz_inmask.fits')
        #Use IRAF's imcombine to combine temp drizzled files and exp masks
	#if this option is selected
        if (self.params['DRIZZLE_OR_IMCOMBINE'].lower() == 'imcombine'):
	   framelist = ''
	   explist = ''
           for j in frames:
              framelist+='alignedStacked/driz_'+j+','
	      explist+='alignedStacked/exp_'+j+','
           framelist = framelist.strip(',')
	   explist = explist.strip(',')
           iraf.unlearn('imcombine')
           #imcombine can't handle strings longer than 1024 bytes
           if (os.access('exp.fits.pl', os.F_OK)): os.unlink('exp.fits.pl')
	   if (os.access('tempEXP.fits', os.F_OK)): os.unlink('tempEXP.fits')
           if (len(framelist) > 1023):
              tmpFile = open("tmpIRAFFile.dat", "wb")
              tmpFile.write(framelist.replace(",","\n"))
              tmpFile.close()
              iraf.images.immatch.imcombine("@tmpIRAFFile.dat", asfile, combine="average", reject=self.params['STACK_REJECT_TYPE'], nlow=self.params['STACK_NLOW'], nhigh=self.params['STACK_NHIGH'], lsigma=self.params['STACK_LSIGMA'], hsigma=self.params['STACK_HSIGMA'], lthreshold=-1e+6, expmasks="exp.fits", expname=self.params['EXPTIME_KEYWORD'])
              tmpFile = open("tmpIRAFFile.dat", "wb")
              tmpFile.write(explist.replace(",","\n"))
              tmpFile.close()
              iraf.images.immatch.imcombine("@tmpIRAFFile.dat", 'tempEXP.fits', combine="sum")
              os.unlink("tmpIRAFFile.dat")
           else:
              iraf.images.immatch.imcombine(framelist, asfile, combine="average",reject=self.params['STACK_REJECT_TYPE'], nlow=self.params['STACK_NLOW'], nhigh=self.params['STACK_NHIGH'], lsigma=self.params['STACK_LSIGMA'], hsigma=self.params['STACK_HSIGMA'], lthreshold=-1e+6, expmasks="exp.fits", expname=self.params['EXPTIME_KEYWORD'])
              iraf.images.immatch.imcombine(explist, 'tempEXP.fits', combine="sum")
           if (not os.access(expfile, os.F_OK)):
              iraf.images.imutil.imcopy('exp.fits.pl',expfile)
           os.unlink('exp.fits.pl')
	   imexpmap = pyfits.open(expfile, 'update')
	   expdiff = zeros(imexpmap[0].data.shape,"Float32")
	   for j in frames:
              expmap = pyfits.open('alignedStacked/exp_'+j)
	      fullexp = expmap[0].header[self.params['EXPTIME_KEYWORD']]
	      b = where(expmap[0].data > fullexp, 1, 0)
	      expdiff+=b*(expmap[0].data-fullexp)
	      expmap.close()
	   imexpmap[0].data+=expdiff
	   expmap = pyfits.open('tempEXP.fits')
	   b = where(expmap[0].data < imexpmap[0].data, 1, 0)
	   imexpmap[0].data-=b*(imexpmap[0].data-expmap[0].data)
	   expmap.close()
	   imexpmap.flush()
	   imexpmap.close()
	   os.unlink('tempEXP.fits')
	   if (self.params['KEEP_INDIV_IMAGES'].lower() == 'no'):
	      for j in frames:
		if (os.access('alignedStacked/driz_'+j, os.F_OK)):
		   os.unlink('alignedStacked/driz_'+j)
		if (os.access('alignedStacked/exp_'+j, os.F_OK)):
		   os.unlink('alignedStacked/exp_'+j)

	if (self.guiMessages): print "FILE: "+asfile
	#Update headers of newly created files.
	sumDriz = pyfits.open(asfile, 'update')
        sumDriz[0].header.update('FILENAME', asfile)
	sumDriz.verify('silentfix')
        sumExp = pyfits.open(expfile, 'update')
        sumExp[0].header.update('FILENAME', expfile)
        sumExp.verify('silentfix')
        sumExp.flush()
	sumExp.close()
        sumDriz.flush()
        sumDriz.close()
	#Create object mask.
        sumExp = pyfits.open(expfile)
	sumExp[0].data = where(sumExp[0].data != 0, 1, 0)
	sumExp[0].header.update('FILENAME', objfile)
	sumExp.verify('silentfix')
	sumExp.writeto(objfile)
	sumExp.close()
	paramfile = "alignedStacked/as_"+pfix+".config"
	f = open(infile, "rb")
	f2 = open(paramfile, "wb")
	f2.write(f.read())
	f.close()
	f2.close()
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
           if (os.access('skySubtracted/ss_'+j, os.F_OK)):
              os.unlink('skySubtracted/ss_'+j)

   def findSkyMethods(self, files, ssm):
      methods = []
      if (os.access(ssm, os.F_OK)):
        f = open(ssm, 'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        tmpprefix = []
        tmpmeth = []
        for j in range(len(s)):
           temp = s[j].split()
           tmpprefix.append(temp[0])
           tmpmeth.append(temp[1])
        for j in range(len(files)):
           currMeth = 'rough'
           for l in range(len(tmpprefix)):
              if (files[j].find(tmpprefix[l]) != -1):
                currMeth = tmpmeth[l]
                continue
           methods.append(currMeth)
      elif (ssm.lower() == 'remove_objects'):
        for j in range(len(files)): methods.append('remove_objects')
      elif (ssm.lower() == 'offsource'):
        for j in range(len(files)): methods.append('offsource')
      elif (ssm.lower() == 'offsource_extended'):
        for j in range(len(files)): methods.append('offsource_extended')
      elif (ssm.lower() == 'offsource_neb'):
        for j in range(len(files)): methods.append('offsource_neb')
      else:
        for j in range(len(files)): methods.append('rough')
      return methods

   def cleanUp(self):
      for j in self.symLinks:
	if (os.access(j, os.F_OK)): os.unlink(j)

#Main
if (len(sys.argv) > 1): infile = sys.argv[1]
else: infile = 'pipelineparams.dat'
if (len(sys.argv) > 2): logfile = sys.argv[2]
else: logfile = 'pipeline.log'
if (len(sys.argv) > 3): warnfile = sys.argv[3]
else: warnfile = 'pipeline.warn'
if (len(sys.argv) > 4):
   if (sys.argv[4] == "-gui"): guiMessages = True
   else: guiMessages = False
else: guiMessages = False
a = flam2pipeline(infile, logfile, warnfile, guiMessages)
if (a.params['DO_LINEARIZE'].lower() == "yes"):
   a.linearizeAll(a.params['LINEARITY_COEFFS'])
elif (os.access("linearized/lin_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("linearized/lin"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'linearized/lin_'
elif (a.mef != 0):
   a.convertMEFs()
if (guiMessages): print "PROGRESS: 20"
if (a.params['DO_DARK_COMBINE'].lower() == "yes"):
   a.darkCombine()
else:
   for j in a.darkfiles:
      i = a.filenames.index(j)
      a.filenames.pop(i)
      a.exptimes.pop(i)
      a.filters.pop(i)
      a.isSkyFlat.pop(i)
if (guiMessages): print "PROGRESS: 25"
if (a.params['DO_DARK_SUBTRACT'].lower() == "yes"):
   a.darkSubtract()
elif (os.access("darkSubtracted/ds_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("darkSubtracted/ds_"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'darkSubtracted/ds_'
if (guiMessages): print "PROGRESS: 30"
if (a.params['DO_FLAT_COMBINE'].lower() == "yes"):
   a.flatCombine()
else:
   for j in a.flatfiles:
      i = a.filenames.index(j)
      a.filenames.pop(i)
      a.exptimes.pop(i)
      a.filters.pop(i)
if (guiMessages): print "PROGRESS: 35"
if (a.params['DO_BAD_PIXEL_MASK'].lower() == "yes"):
   a.badPixelMask()
else:
   temp = pyfits.open(a.prefix+a.filenames[0])
   temp[0].data *= 0.
   for j in range(len(a.filters)):
      addBPM = True
      for l in range(len(a.bpMask)):
	if (a.bpMask[l].filter == a.filters[j]):
	   addBPM = False
      if (addBPM):
	a.bpMask.append(badPixelMask(inputMask=temp[0].data, filter=a.filters[j]))
   temp.close()
   temp = 0
   a.useBPMask = True
if (guiMessages): print "PROGRESS: 40"
if (a.params['DO_FLAT_DIVIDE'].lower() == "yes"):
   a.flatDivide()
elif (os.access("flatDivided/fd_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("flatDivided/fd_"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'flatDivided/fd_'
if (guiMessages): print "PROGRESS: 45"
if (a.params['DO_SKY_SUBTRACT'].lower() == "yes"):
   a.skySubtract()
elif (os.access("skySubtracted/ss_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("skySubtracted/ss__"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'skySubtracted/ss_'
if (guiMessages): print "PROGRESS: 85"
if (a.params['DO_ALIGN_STACK'].lower() == "yes"):
   a.alignStack(infile)
if (guiMessages):
   print "PROGRESS: 100"
   print "STATUS: Done!"
f = open(a.logfile,'ab')
f.write('End time: '+str(datetime.today())+'\n')
f.close()
a.cleanUp()
