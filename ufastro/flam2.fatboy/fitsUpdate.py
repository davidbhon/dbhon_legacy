#!/usr/bin/python
import pyfits
from numarray import *
import curses.ascii

file = sys.argv[1]
if (file.lower().find('.fit') == -1):
   f = open(file,'rb')
   filelist = f.read().split('\n')
   f.close()
   filelist.remove('')
else:
   filelist = [file]
for j in filelist:
   isDigit = True
   ndot = 0
   nexp = 0
   npm = 0
   for l in range(len(sys.argv[3])):
      if (curses.ascii.isdigit(sys.argv[3][l])):
	continue
      if (sys.argv[3][l] == '.' and ndot == 0):
	ndot = 1
	continue
      if (sys.argv[3][l].lower() == 'e' and nexp == 0 and l != 0):
	nexp = 1
        continue
      if ((sys.argv[3][l] == '+' or sys.argv[3][l] == '-') and npm == 0):
	npm = 1
	continue
      isDigit = False
      break
   if (isDigit):
      value = float(sys.argv[3])
   else:
      value = sys.argv[3]
   temp = pyfits.open(j,'update') 
   temp[0].header.update(sys.argv[2],value)
   temp.flush()
   temp.close()
