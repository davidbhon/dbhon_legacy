import java.io.*;
import javax.imageio.stream.*;

public class FATBOYshorts extends FATBOYdata 
{
    protected short[] _values=null;

    private void _init() {
	_type = MsgTyp._Shorts_;
    }

    public FATBOYshorts() {
	_init();
    }

    public FATBOYshorts(String name, int nelem) {
	_init();
	_name = new String(name);
	_elem = nelem;
    }

    public FATBOYshorts(String name, int nelem, int width, int height) {
        this(name, nelem);
        this.width = width;
        this.height = height;
    }

    public FATBOYshorts(String name, short[] vals) {
	_init();
	_name = new String(name);
	_elem = vals.length;
	_values = vals;
    }

    public FATBOYshorts(String name, short[] vals, int width, int height) {
        this(name, vals);
        this.width = width;
        this.height = height;
    }

    // all methods declared abstract by UFProtocal can be defined here

    public String description() { return new String("FATBOYshorts"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 2;
    }

    public double[] values() { return UFArrayOps.castAsDoubles(_values); }

    public short valData(int index) { return _values[index]; }

    public short maxVal() {
	short max=-32768;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public short minVal() {
	short min=32767;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inps) {
	try {
/*
	    byte[] byteStream = new byte[2*_elem];
	    inps.readFully( byteStream );

	    _values = new short[_elem];
	    int bi = 0;
	    //shift and mask each 2 byte group into a short integer:

	    for( int i=0; i<_elem; i++ ) {
		Integer val = new Integer( (( (short)byteStream[bi++] << 8 ) & 0xff00 ) |
					   (  (short)byteStream[bi++]        & 0x00ff ) );
		_values[i] = val.shortValue();
	    }

	    return byteStream.length;
*/

            _values = new short[_elem];
            MemoryCacheImageInputStream mciis = new MemoryCacheImageInputStream(inps);
            mciis.readFully( _values, 0, _elem );
            mciis.close();
            return 2*_elem;
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYshorts::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("FATBOYshorts::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYshorts::recvData> "+e.toString());
	}
	return 0;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream outps) {
	int retval=0;
	try {
	    _elem = _values.length;
	    byte[] byteStream = new byte[2*_elem];
	    int bi = 0;
	    //shift and mask each 16-bit integer into a 2 byte group:
	    
	    for( int ie=0; ie < _elem; ie++ ) {
		short vi = _values[ie];
		byteStream[bi++] = (byte)( (vi & 0x0000ff00) >> 8 );
		byteStream[bi++] = (byte)(  vi & 0x000000ff );
	    }
	    int bcp = outps.size();
	    outps.write( byteStream, 0, byteStream.length );
	    outps.flush();
	    return outps.size() - bcp;
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYshorts::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("FATBOYshorts::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYshorts::sendData> "+e.toString());
	}
	return retval;
    }

    public void flipFrame()
    {
        int nrow = height;
        int ncol = width;

        for( int irow=0; irow < (nrow/2); irow++ )
            {
                int iflip = nrow - irow - 1;
                int kp = irow * ncol;
                int kf = iflip * ncol;

                for( int k=0; k<ncol; k++ ) {
                    short pixval = _values[kp];
                    _values[kp++] = _values[kf];
                    _values[kf++] = pixval;
                }
            }
    }
}

