import java.io.*;
import javax.imageio.stream.*;

public class FATBOYints extends FATBOYdata
{
    protected int[] _values=null;

    protected int _min =  2147483647;
    protected int _max = -2147483648;
    protected int _ixMin = -1;
    protected int _ixMax = -1;

    private void _init() {
        _type = MsgTyp._Ints_;
    }

    public FATBOYints() {
	_init();
    }

    public FATBOYints(String name) {
	_init();
	_name = new String(name);
    }

    public FATBOYints(String name, int nelem) {
	_init();
	_name = new String(name);
	_elem = nelem;
    }

    public FATBOYints(String name, int nelem, int width, int height) {
	this(name, nelem);
	this.width = width;
	this.height = height;
    }

    public FATBOYints(String name, int[] vals) {
	_init();
	_name = new String(name);
	_elem = vals.length;
	_values = vals;
    }

    public FATBOYints(String name, int[] vals, int width, int height) {
	this(name, vals);
	this.width = width;
	this.height = height;
    }

    // all methods declared abstract by UFProtocal can be defined here:

    public String description() { return new String("FATBOYints"); }
 
    public double[] values() { return UFArrayOps.castAsDoubles(_values); }

    public int valData(int index) { return _values[index]; }

    public int maxVal() {
	int max=-2147483648;
	if( _max > max ) return _max;

	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];

	_max = max;
	return max;
    }

    public int minVal() {
	int min=2147483647;
	if( _min < min ) return _min;

	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];

	_min = min;
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inps)
    {
	if( inps == null ) {
	    System.err.println("FATBOYints::recvData> NULL input stream!");
	    return 0;
	}

	if( _elem < 1 ) {
	    System.err.println("FATBOYints::recvData> # elements is zero!");
	    return 0;
	}

	try {
/*
	    byte[] byteStream = new byte[4*_elem];
	    inps.readFully( byteStream );

	    _values = new int[_elem];
	    int bi = 0;
	    //shift and mask each 4 byte group into a 32-bit integer:

	    for( int i=0; i<_elem; i++ ) {
		_values[i] =
		    (( (int)byteStream[bi++] << 24 ) & 0xff000000 ) |
		    (( (int)byteStream[bi++] << 16 ) & 0x00ff0000 ) |
		    (( (int)byteStream[bi++] <<  8 ) & 0x0000ff00 ) |
		    (  (int)byteStream[bi++]         & 0x000000ff);
	    }

	    return byteStream.length;
*/

            _values = new int[_elem];
            MemoryCacheImageInputStream mciis = new MemoryCacheImageInputStream(inps);
            mciis.readFully( _values, 0, _elem );
            mciis.close();
            return 4*_elem;

	}
	catch(EOFException eof) {
	    System.err.println("FATBOYints::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("FATBOYints::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYints::recvData> "+e.toString());
	}

	return 0;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream outps)
    {
	if( outps == null ) {
	    System.err.println("FATBOYints::sendData> NULL output stream!");
	    return 0;
	}

	if( _values == null || _values.length < 1 ) {
	    System.err.println("FATBOYints::sendData> _values array is empty!");
	    return 0;
	}

	try {
	    _elem = _values.length;
	    byte[] byteStream = new byte[4*_elem];
	    int bi = 0;
	    //shift and mask each 32-bit integer into a 4 byte group:
	    
	    for( int ie=0; ie < _elem; ie++ ) {
		int vi = _values[ie];
		byteStream[bi++] = (byte)( (vi & 0xff000000) >> 24 );
		byteStream[bi++] = (byte)( (vi & 0x00ff0000) >> 16 );
		byteStream[bi++] = (byte)( (vi & 0x0000ff00) >> 8 );
		byteStream[bi++] = (byte)(  vi & 0x000000ff );
	    }
	    int bcp = outps.size();
	    outps.write( byteStream, 0, byteStream.length );
	    outps.flush();
	    return outps.size() - bcp;
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYints::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("FATBOYints::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYints::sendData> "+e.toString());
	}

	return 0;
    }

    public void flipFrame()
    {
        int nrow = height;
        int ncol = width;

        for( int irow=0; irow < (nrow/2); irow++ )
            {
                int iflip = nrow - irow - 1;
                int kp = irow * ncol;
                int kf = iflip * ncol;

                for( int k=0; k<ncol; k++ ) {
                    int pixval = _values[kp];
                    _values[kp++] = _values[kf];
                    _values[kf++] = pixval;
                }
            }
    }

}
