import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;

public class FATBOYDisplayFrame extends JFrame {
    
    static JPanel imPanelsHolder;
    static JTextField zMax = new JTextField("0");
    static JTextField zMin = new JTextField("0");
    static JComboBox scaleBox;
    static UFColorCombo zModeBox;
    //final ImagePanel[] imagePanels;
    public FATBOYImagePanel imagePanel;
    protected final JLabel pixelDataVal = new JLabel("x=0, y=0, Data = 0");
    public boolean autoSelect = true;
    public ColorMapDialog colorMapDialog;
    public Vector dirs, filenames, fullNames;
    public JComboBox dirsCombo;
    public JList fileList;
    public UFFITS img;
    public JRadioButton autoButton, manButton;
    
    public FATBOYDisplayFrame()
    {
        super();
	this.setTitle("FATBOY Data Display");
        this.setSize( 1280, 1024 );	
        this.setLocation( 0, 0 );
	this.setUndecorated(false);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        img = new UFFITS();

	dirs = new Vector();
	filenames = new Vector();
	fullNames = new Vector();

	dirsCombo = new JComboBox(dirs);
	fileList = new JList();
	fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        dirsCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                updateFilenames();
            }
        });

	fileList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent ev) { 
		if (ev.getValueIsAdjusting() == true) return;
		int i = fileList.getSelectedIndex();
		if (i >= 0) {
		    updateBuffer(new ImageBuffer(img.readFITSfile((String)fullNames.get(i))), (String)zModeBox.getSelectedItem());
		}
            }
	});

	colorMapDialog = new ColorMapDialog(this);
	IndexColorModel colorModel = colorMapDialog.colorModel; 

        imagePanel = new FATBOYImagePanel(this, colorModel); 

	Container content = getContentPane();

	//DataFullDisplayFrame components:
	content.setLayout(new RatioLayout());
	
	imPanelsHolder = new JPanel();
	imPanelsHolder.setLayout(new RatioLayout());
	imPanelsHolder.add( "0.0,0.0;1.0,1.0", imagePanel );

	imPanelsHolder.setBorder(BorderFactory.createLineBorder(Color.green, 1));
	
        final JLabel zMaxLabel = new JLabel();
        final JLabel zMinLabel = new JLabel();
        zMaxLabel.setText("Z max");
        zMinLabel.setText("Z min");

	String zMode[] = {"Auto","Manual","Zscale","Zmax","Zmin"}; 
	//zModeBox = new JComboBox(zMode);
	zModeBox = new UFColorCombo(zMode, UFColorCombo.COLOR_SCHEME_LIGHT_GRAY);
	zModeBox.setMaximumRowCount(zMode.length);
	//zModeBox.setBackground(Color.lightGray);
	  
	zMax.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        zMax.setEditable(false);
	zMin.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        zMin.setEditable(false);
	
	String scaleValues[] = {"Linear","Log","Power"}; 
	scaleBox = new JComboBox(scaleValues);
	scaleBox.setMaximumRowCount(scaleValues.length);
	scaleBox.setSelectedItem("Linear");
	scaleBox.setVisible(false);

	final UFColorButton applyZ = new UFColorButton("Apply");
	applyZ.setVisible(false);
	applyZ.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
	applyZ.setForeground(Color.blue);
        applyZ.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String zModeBoxSelect = (String)zModeBox.getSelectedItem();
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(zModeBoxSelect.indexOf("Manual") >= 0) {
	      if (scaleBoxSelect.indexOf("Linear") >= 0) {
	        imagePanel.applyLinearScale(
	                                   Float.parseFloat(zMin.getText()), 
	   		  	           Float.parseFloat(zMax.getText())) ;
	        imagePanel.doLinear=true;
	        imagePanel.doLog   =false;
	        imagePanel.doPower =false;
	      } else if (scaleBoxSelect.indexOf("Log") >= 0) {
	        imagePanel.applyLogScale(
	                                   Double.parseDouble(zMax.getText())) ;
	      
	        imagePanel.doLinear=false;
	        imagePanel.doLog   =true;
	        imagePanel.doPower =false;
	      } else if (scaleBoxSelect.indexOf("Power") >= 0) {
	        imagePanel.applyPowerScale(
	                                   Double.parseDouble(zMax.getText()), 
	   		  	           Double.parseDouble(zMin.getText())) ;
	        imagePanel.doLinear=false;
	        imagePanel.doLog   =false;
	        imagePanel.doPower =true;
	      }
           }
	  }
        });

        zModeBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String zModeBoxSelect = (String)zModeBox.getSelectedItem();
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(zModeBoxSelect.indexOf("Manual") >= 0) {
	      if (scaleBoxSelect.indexOf("Log") >= 0) {
	        zMin.setVisible(false);
	        zMinLabel.setVisible(false);
                zMax.setText("10");
              }
	      if (scaleBoxSelect.indexOf("Power") >= 0) {
                zMax.setText("10");
                zMin.setText("0.1");
             }
	      zMax.setEditable(true);
              zMin.setEditable(true);
	      applyZ.setVisible(true);
	      //zModeBox.setBackground(Color.orange);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_ORANGE);
	      scaleBox.setVisible(true);
           } else if (zModeBoxSelect.indexOf("Auto") >= 0) {
              zMaxLabel.setText("Z max");
              zMinLabel.setText("Z min");
              zMinLabel.setVisible(true);;
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
	      applyZ.setVisible(false);
	      //zModeBox.setBackground(Color.lightGray);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_LIGHT_GRAY);
	      scaleBox.setVisible(false);
	      imagePanel.applyLinearScale( ) ;
	   } else if (zModeBoxSelect.indexOf("Zscale") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.GREEN);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_GREEN);
              scaleBox.setVisible(false);
              imagePanel.applyZScale() ;
	   } else if (zModeBoxSelect.indexOf("Zmax") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.YELLOW);
              zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_YELLOW);
              scaleBox.setVisible(false);
              imagePanel.applyZScale("Zmax");
	   } else if (zModeBoxSelect.indexOf("Zmin") >= 0) {
              zMinLabel.setVisible(true);
              zMax.setEditable(false);
              zMin.setEditable(false);
              zMin.setVisible(true);
              applyZ.setVisible(false);
              //zModeBox.setBackground(Color.MAGENTA);
	      zModeBox.updateColorGradient(UFColorCombo.COLOR_SCHEME_MAGENTA);
              scaleBox.setVisible(false);
              imagePanel.applyZScale("Zmin");
           }
	  }
        });
	
        scaleBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
	    String scaleBoxSelect = (String)scaleBox.getSelectedItem();
	    if(scaleBoxSelect.indexOf("Linear") >= 0) {
              zMaxLabel.setText("Z max");
              zMinLabel.setText("Z min");
              zMinLabel.setVisible(true);;
              zMin.setVisible(true);
            } else if (scaleBoxSelect.indexOf("Log") >= 0) {
              zMaxLabel.setText("       >");
              zMinLabel.setVisible(false);
              zMax.setText("10");
              zMin.setVisible(false);
            } else if (scaleBoxSelect.indexOf("Power") >= 0) {
              zMaxLabel.setText("       >");
              zMinLabel.setText("Power");
              zMinLabel.setVisible(true);;
              zMax.setText("10");
              zMin.setText("0.1");
              zMin.setVisible(true);
	    }
	  }
        });

	String[] dragObjectOptions = {"Hidden", "H Line", "V Line", "+", "X", "Circle"};
	int[] dragObjectColor = {UFColorCombo.COLOR_SCHEME_RED, UFColorCombo.COLOR_SCHEME_YELLOW, UFColorCombo.COLOR_SCHEME_GREEN, UFColorCombo.COLOR_SCHEME_CYAN}; 
	final UFColorCombo[] dragObjectBox = new UFColorCombo[4];
	for (int j = 0; j < 4; j++) {
	    dragObjectBox[j] = new UFColorCombo(dragObjectOptions, dragObjectColor[j]);
	    //dragObjectBox[j].setBackground(dragObjectColor[j]);
	    final int objectNum = j;
	    dragObjectBox[j].addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    imagePanel.dragObjectStatus[objectNum]=(String)dragObjectBox[objectNum].getSelectedItem(); 
		    imagePanel.repaint();
		}
	    });
	}

	final UFColorButton rulerButton = new UFColorButton("Show ruler");
	rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
	rulerButton.setText("Show ruler");
        rulerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	     if(rulerButton.getText().indexOf("Show") >= 0) {
	        rulerButton.setText("Hide ruler");
	        rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_ORANGE);
	        imagePanel.showRuler = true;
	        imagePanel.repaint();
             }
	     else if(rulerButton.getText().indexOf("Hide") >= 0) {
	        rulerButton.setText("Show ruler");
	        rulerButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
	        imagePanel.showRuler = false;
	        imagePanel.repaint();
	     }
            }
        });	

        JPanel toolsPanel = new JPanel();
	JLabel scale = new JLabel("Scale:", JLabel.CENTER);
	scale.setForeground(Color.darkGray);
	
	toolsPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
	toolsPanel.setLayout(new RatioLayout());
	toolsPanel.add("0.02,0.02, 0.96, 0.10", new JLabel("Display tools"));
		
	toolsPanel.add("0.02,0.15, 0.26, 0.14", scale);
	toolsPanel.add("0.02,0.31, 0.28, 0.14", zModeBox);
	toolsPanel.add("0.32,0.15, 0.16, 0.14", zMaxLabel);
	toolsPanel.add("0.48,0.15, 0.22, 0.14", zMax);
	toolsPanel.add("0.32,0.31, 0.16, 0.14", zMinLabel);
	toolsPanel.add("0.48,0.31, 0.22, 0.14", zMin);
	
	toolsPanel.add("0.71,0.15, 0.27, 0.14", scaleBox);
	toolsPanel.add("0.71,0.31, 0.27, 0.14", applyZ);

        toolsPanel.add("0.02,0.49, 0.29, 0.14", dragObjectBox[0]);
        toolsPanel.add("0.35,0.49, 0.29, 0.14", dragObjectBox[1]);
        toolsPanel.add("0.67,0.49, 0.29, 0.14", dragObjectBox[2]);
        toolsPanel.add("0.06,0.66, 0.38, 0.14", dragObjectBox[3]);

	toolsPanel.add("0.51,0.66, 0.40, 0.14", rulerButton);
	toolsPanel.add("0.02,0.83, 0.90, 0.14", pixelDataVal);
	
	JButton loadMOSmask = new JButton("Load MOS");
	final FileDialog fd = new FileDialog(this, "Please choose a file:", FileDialog.LOAD);
		
	final JButton showMOSmask = new JButton();	
	showMOSmask.setText("Show MOS");
        showMOSmask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
	     if(showMOSmask.getText().indexOf("Show") >= 0) {
	        showMOSmask.setText("Hide MOS");
	        imagePanel.showMOS = true;
	        imagePanel.repaint();
             }
	     else if(showMOSmask.getText().indexOf("Hide") >= 0) {
	        showMOSmask.setText("Show MOS");
	        imagePanel.showMOS = false;
	        imagePanel.repaint();
	     }
            }
        });	
 
        JPanel MOSPanel = new JPanel();
	MOSPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
	MOSPanel.setLayout(new RatioLayout());
	MOSPanel.add("0.02,0.02, 0.96, 0.20", new JLabel("MOS mask"));	
	MOSPanel.add("0.06,0.24, 0.88, 0.32", loadMOSmask);
	MOSPanel.add("0.06,0.62, 0.88, 0.32", showMOSmask);

	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new RatioLayout());
	JButton hidePanel = new JButton("Hide Panel");
	hidePanel.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ev) {
		setVisible(false);
	    }
	});

	JButton colorMapButton = new JButton("Color Map Control");
	colorMapButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
		colorMapDialog.setVisible(true);
                colorMapDialog.setState(Frame.NORMAL);
            }
        });

	autoButton = new JRadioButton("Auto");
	autoButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
		if (autoButton.isSelected()) autoSelect = true; else autoSelect = false;
            }
	});

	manButton = new JRadioButton("Manual", true);
        manButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                if (autoButton.isSelected()) autoSelect = true; else autoSelect = false;
            }
        });

	ButtonGroup bg = new ButtonGroup();
	bg.add(autoButton);
	bg.add(manButton);

	buttonPanel.add("0.06,0.02, 0.88,0.32", hidePanel);
        buttonPanel.add("0.06,0.43, 0.88,0.25", autoButton);
        buttonPanel.add("0.06,0.73, 0.88,0.25", manButton);

        loadMOSmask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		fd.setVisible(true);
	        String fileMOS = fd.getFile();
                if (fileMOS == null) {
	           // no file selected
	        } else {
		   Vector v = new Vector();
		   String currLine = " ";
		   String mosColor = "";
		   String mosText = "";
		   int i1, i2;
		   boolean hasMoreTokens;
		   boolean displayText = true;
		   try {
		      BufferedReader r = new BufferedReader(new FileReader(fd.getDirectory() + File.separator+fileMOS));
		      while (currLine != null) {
			currLine = r.readLine();
			if (currLine == null) break;
			if (currLine.startsWith("#")) continue;
			if (currLine.startsWith("global")) {
			   i1 = currLine.indexOf("color=")+6;
			   i2 = currLine.indexOf(" ",i1+1);
			   if (i1 != 5 && i2 > 0) mosColor = currLine.substring(i1,i2).trim();
			   else if (i1 != 5 && i2 == -1) mosColor = currLine.substring(i1).trim();
			   i1 = currLine.indexOf("text=")+5;
			   i2 = currLine.indexOf(" ",i1+1);
			   if (i1 != 4 && i2 > 0) {
			      if (currLine.substring(i1,i2).trim().equals("0"))
				displayText = false;
			   }  else if (i1 != 4 && i2 == -1) {
			      if (currLine.substring(i1).trim().equals("0"))
				displayText = false;
			   }
			   continue;
			}
			v.add(currLine);
		      }
		   } catch (IOException exc) {
		     System.out.println("Error Reading From File.");
		   } 
		   MOSMask[] mosMasks = new MOSMask[v.size()];
		   for (int j = 0; j < v.size(); j++) {
		      currLine = (String)v.elementAt(j);
		      if (currLine.startsWith("image;box")) {
			float[] tokens = new float[5];
			mosText = "";
			i1 = 10;
			hasMoreTokens = true;
			int l = 0;
			while (hasMoreTokens && l < 5) {
			   i2 = currLine.indexOf(",",i1);
			   if (i2 < 0) i2 = 10000;
			   if (i2 > currLine.indexOf(")",i1)) {
			      i2 = currLine.indexOf(")",i1);
			      hasMoreTokens = false;
			   }
			   tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
			   l++;
			   i1 = i2+1;
			}
			i1 = currLine.indexOf("text={")+6;
			if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
			mosMasks[j] = new RectMOSMask(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4],mosText);
			mosMasks[j].setTextMode(displayText);
			if (mosColor != "") mosMasks[j].setColor(mosColor);
		      } else if (currLine.startsWith("image;circle")) {
                        float[] tokens = new float[3];
                        mosText = "";
                        i1 = 13;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 3) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],tokens[2],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
		      } else if (currLine.startsWith("image;ellipse")) {
                        float[] tokens = new float[4];
                        mosText = "";
                        i1 = 14;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 4) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],tokens[2],tokens[3],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
                      } else if (currLine.startsWith("image;point")) {
                        float[] tokens = new float[2];
                        mosText = "";
                        i1 = 12;
                        hasMoreTokens = true;
                        int l = 0;
                        while (hasMoreTokens && l < 2) {
                           i2 = currLine.indexOf(",",i1);
                           if (i2 < 0) i2 = 10000;
                           if (i2 > currLine.indexOf(")",i1)) {
                              i2 = currLine.indexOf(")",i1);
                              hasMoreTokens = false;
                           }
                           tokens[l] = Float.parseFloat(currLine.substring(i1,i2));
                           l++;
                           i1 = i2+1;
                        }
                        i1 = currLine.indexOf("text={")+6;
                        if (i1 != 5) mosText = currLine.substring(i1,currLine.indexOf("}",i1+1));
                        mosMasks[j] = new EllipMOSMask(tokens[0],tokens[1],mosText);
                        mosMasks[j].setTextMode(displayText);
                        if (mosColor != "") mosMasks[j].setColor(mosColor);
                      }
		   }
		   imagePanel.mosMasks = mosMasks;
	       }
	       showMOSmask.setText("Hide MOS");
	       imagePanel.showMOS = true;
	       imagePanel.repaint();

	    }
        });	

	content.add("0.005, 0.01, 0.21, 0.24", toolsPanel);
	content.add("0.005, 0.255, 0.10, 0.11", MOSPanel);
	content.add("0.11, 0.255, 0.10, 0.11", buttonPanel);
	content.add("0.03, 0.37, 0.16, 0.04", colorMapButton);
	content.add("0.03, 0.43, 0.16, 0.03", dirsCombo);
	content.add("0.01, 0.47, 0.20, 0.30", new JScrollPane(fileList));

        content.add("0.22, 0.00, 0.78, 1.00", imPanelsHolder);
	
        //this.getContentPane().setLayout(new RatioLayout());
        //this.getContentPane().add( "0,0,1,1", fullDisplPanel );
 	
	addComponentListener(new ComponentAdapter() {
	   public void componentResized(ComponentEvent ev) {
	      imagePanel.updateImage(imagePanel.savedImgBuff);
              String zModeBoxSel = (String)zModeBox.getSelectedItem();
	      updateScale(imagePanel, zModeBoxSel);
	      double ratio = 0.8;
	      int h = getHeight();
	      int w = (int)(h/ratio);
              setSize((int)(h/ratio), h);
	   }
	});

	validate();
	pack();
    }  

    public void updateScale(FATBOYImagePanel imPanel, String mode) {
	if (mode.indexOf("Auto") >= 0) {
	    imPanel.applyLinearScale();
	} else if (mode.indexOf("Manual") >= 0) {
            imPanel.applyLinearScale(Double.parseDouble(zMin.getText()), Double.parseDouble(zMax.getText()));
	} else if (mode.startsWith("Z")) {
	    imPanel.applyZScale(mode);
	}
    }

    public void setTitleBarText() {
	String title = "You're looking at: ";
    }

    public void updateBuffer(ImageBuffer imgBuff, String zModeBoxSel) {
	imagePanel.updateImage(imgBuff);
	if (zModeBoxSel.indexOf("Auto") >= 0) {
            imagePanel.applyLinearScale();
	} else if (zModeBoxSel.indexOf("Manual") >= 0) {
            imagePanel.applyLinearScale(Double.parseDouble(zMin.getText()), Double.parseDouble(zMax.getText()));
	} else if (zModeBoxSel.startsWith("Z")) {
	    imagePanel.applyZScale(zModeBoxSel);
	}
    }

    public void changeColorModel(IndexColorModel colorModel)
    {
	imagePanel.updateImage( colorModel );
    }

    public void addFile(String filename) {
	int pos1 = filename.lastIndexOf('/');
	int pos2 = filename.substring(0,pos1-1).lastIndexOf('/');
	String dir = filename.substring(pos2+1,pos1+1);
	if (!dirs.contains(dir)) {
	    dirsCombo.addItem(dir);
	    dirsCombo.repaint();
	    if (autoButton.isSelected()) dirsCombo.setSelectedItem(dir);
	}
	if (!filenames.contains(filename)) {
	    filenames.add(filename);
	    updateFilenames();
	}
    }

    public void updateFilenames() {
	String currDir = (String)dirsCombo.getSelectedItem();
	Vector v = new Vector();
	fullNames = new Vector();
	for (int j = 0; j < filenames.size(); j++) {
	    String temp = (String)filenames.get(j);
	    if (temp.indexOf(currDir) != -1) {
		v.add(temp.substring(temp.lastIndexOf("/")+1));
		fullNames.add(temp);
	    }
	}
	fileList.setListData(v);
	if (autoButton.isSelected()) fileList.setSelectedIndex(v.size()-1);
    }

/*
    protected void processWindowEvent(WindowEvent wev) {
        if (wev.getID() == WindowEvent.WINDOW_CLOSING ) {
            Object[] exitOptions = {"Exit","Cancel"};
            int n = JOptionPane.showOptionDialog(this, "Are you sure you want to quit?", "Exit JDD?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
            if (n == 0) {
                System.out.println("JDD exiting...");
                System.exit(0);
            }
        }
        else
            super.processWindowEvent(wev);
    }
*/
   public static void main(String[] args) {
      FATBOYDisplayFrame display = new FATBOYDisplayFrame();
      display.setVisible(true);
   }

}
