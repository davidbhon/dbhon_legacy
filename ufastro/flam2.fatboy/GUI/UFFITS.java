import java.io.*;
import java.net.*;
import java.util.*;
import java.text.*;
import java.awt.*;
import javax.swing.*;

public class UFFITS extends LinkedList {

    protected static boolean _verbose = false;
    protected static File _path = null;

    protected String  _FITSrecord = "";
    protected String  _FITSkey = "";

    protected byte[] _blanks = new byte[80];
    protected String _blankLine;

    public UFFITS() {
	_init();
	_FITSrecord = "";
    }
    public UFFITS(String comment, boolean simple) {
	_init();
	_start(comment,simple);
    }

    private void _init() {
	for( int i=0; i < _blanks.length; i++ ) _blanks[i] = 32;
	_blankLine = new String( _blanks );
    }

    public int[] getPix()
    {
	int bitpix = getInt("BITPIX");
	int nax = getInt("NAXIS");
	int npx = getInt("NAXIS1");
	int npy = 1;
	if( nax > 1 ) npy = getInt("NAXIS2");

	int[] temp = {npx, npy, bitpix};

	return temp;
    }

//----------------------------------------------------------------------------------------------------
    // write FITS header and data to a file:

    public int writeFITSfile( FATBOYdata data, String filename, String[] moreHeaderInfo )
    {
	if( data == null ) {
	    System.err.println("UFFITS::writeFITSfile> data object is empty.");
	    return 0;
	}

	if( moreHeaderInfo != null ) addrecs( moreHeaderInfo );
	this.end();

	if( filename == null ) return 0;

	FileOutputStream foutps = writeFITSheader( filename );

	if( foutps != null ) {
	    if(_verbose) System.out.println("UFFITS::writeFITSfile> writing data...");
	    try {
		int nbwritten = data.sendData( new DataOutputStream( foutps ) );
		foutps.close();
		System.out.println("UFFITS::writeFITSfile> " + nbwritten +
				   " bytes of data written to: " + filename);
		return nbwritten;
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITS::writeFITSfile> " + ioe.toString());
		ioe.printStackTrace();
		try{ foutps.close(); } catch(Exception ex) {}
		return 0;
	    }
	}
	else {
	    System.err.println("UFFITS::writeFITSfile> File Output Stream is NULL.");
	    return 0;
	}
    }

    public int writeFITSfile( int[] data, String dataname, String filename, String[] moreHeaderInfo )
    {
	return writeFITSfile( new FATBOYints(dataname, data), filename, moreHeaderInfo );
    }

    public int writeFITSfile( int[] data, String filename, String[] moreHeaderInfo )
    {
	return writeFITSfile( data, "data", filename, moreHeaderInfo );
    }
//----------------------------------------------------------------------------------------------------
    // read FITS header and data from a file:

    public FATBOYdata readFITSfile( String filename, String name )
    {
	if( filename == null ) return null;

	FileInputStream finps = readFITSheader( filename );
	int[] info = getPix();
	int npix = info[0]*info[1];
	int depth = info[2];

	if( finps != null ) {

	    String dname = filename.substring(filename.lastIndexOf("/")+1);
	    FATBOYdata data = null;

	    switch( depth )
		{
		case 16:
		    data = new FATBOYshorts( dname, npix, info[0], info[1]); break;
		case 32:
		    data = new FATBOYints( dname, npix, info[0],info[1]); break;
		case -32:
		    data = new FATBOYfloats( dname, npix, info[0], info[1]); break;
		case -64:
		    data = new FATBOYdoubles( dname, npix, info[0], info[1]); break;
		default:
		    data = new FATBOYints( dname, npix, info[0], info[1]);
		}

	    System.out.println("UFFITS::readFITSfile> reading " + data.elements() + " of "
			       + depth + " bit pixels of data from file: " + filename + "...");
	    try {
		int nbread = data.recvData( new DataInputStream( finps ) );
		finps.close();
		System.out.println("UFFITS::readFITSfile> " + nbread +
				   " bytes of data read from: " + filename);
		return (FATBOYdata)data;
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITS::readFITSfile> " + ioe.toString());
		ioe.printStackTrace();
		try{ finps.close(); } catch(Exception ex) {}
		return null;
	    }
	}
	else {
	    System.err.println("UFFITS::readFITSfile> File Input Stream is NULL.");
	    return null;
	}
    }

    public FATBOYdata readFITSfile( String filename ) { return readFITSfile( filename, "data" ); }

    public FATBOYdata readFITSfile() { return readFITSfile( null, "data" ); }

//----------------------------------------------------------------------------------------------------

    public FileOutputStream writeFITSheader(String filename)
    {
	try {
	    File theFile = new File( filename );

	    if( !theFile.createNewFile() ) {
		System.err.println("UFFITS::writeFITSheader> failed creating file: " + filename
				   + ", does file already exist ?");
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(null,"File [" + filename + "] already exists.",
					      "ERROR", JOptionPane.ERROR_MESSAGE);		
		return null;
	    }

	    FileOutputStream foutps = new FileOutputStream( theFile );
	    ListIterator Lit = this.listIterator();

	    while( Lit.hasNext() ) {
		String frec = (String)Lit.next();
		if( frec.length() > 80 )
		    foutps.write( frec.getBytes(), 0, 80 );
		else {
		    foutps.write( frec.getBytes() );
		    if( frec.length() < 80 ) foutps.write( _blanks, 0, 80 - frec.length() );
		}
	    }

	    if(_verbose) System.out.println("UFFITS::writeFITSheader> wrote " + size() + " FITS recs.");

	    if( this.size() % 36 > 0 ) {
		int nextra = 36 - this.size() % 36;
		for( int i=0; i < nextra; i++ ) foutps.write( _blanks );
		if(_verbose) System.out.println("UFFITS::writeFITSheader> wrote "+nextra+" blank recs.");
	    }

	    return foutps;
	}
	catch( Exception e ) {
	    System.err.println("UFFITS::writeFITSheader> "+e.toString());
	    return null;
	}
    }
//----------------------------------------------------------------------------------------------------

    public FileInputStream readFITSheader(String filename)
    {
	try {
	    File theFile = new File( filename );

	    if( !theFile.canRead() ) {
		System.err.println("UFFITS::readFITSheader> cannot read file: " + filename);
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(null,"File [" + filename + "] is not readable.",
					      "ERROR", JOptionPane.ERROR_MESSAGE);		
		return null;
	    }

	    FileInputStream finps = new FileInputStream( theFile );
	    this.clear();

	    try {
		byte[] frec = new byte[80];
		boolean end = false;
		int nrec=0;

		while( !end ) {
		    finps.read( frec );
		    ++nrec;
		    String Line = new String( frec );
		    if( Line.indexOf("END") == 0 ) end = true;

		    if( end )
			this.end();
		    else if( this.addrec( Line ) < 0 ) {
			System.err.println("UFFITS::readFITSheader> rec # " + nrec + " is bad ? ");
			System.err.println("UFFITS::readFITSheader> rec = " + Line);
		    }
		}

		if( nrec % 36 > 0 ) {
		    int nextra = 36 - nrec % 36;
		    for( int i=0; i < nextra; i++ ) finps.read( frec );
		    if(_verbose)
			System.out.println("UFFITS::readFITSheader> read "+nextra+" blank recs.");
		}
	    }
	    catch(EOFException eof) {
		System.err.println("UFFITS::readFITSheader> " + eof.toString());
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITS::readFITSheader> "+ ioe.toString());
	    }
	    catch( Exception e ) {
		System.err.println("UFFITS::readFITSheader> " + e.toString());
	    }

	    if(_verbose) System.out.println("UFFITS::readFITSheader> read " + size() + " FITS recs.");

	    return finps;
	}
	catch( Exception e ) {
	    System.err.println("UFFITS::readFITSheader> "+e.toString());
	    return null;
	}
    }
//----------------------------------------------------------------------------------------------------
    // methods to add FITS records (keyword = value / comment) to the FITS header:

    public int addrec(String keyword, String value, String comment) {
	return _addrec( keyword, value, comment, true );
    }

    public int addrec(String keyword, int value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int addrec(String keyword, float value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int addrec(String keyword, double value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int replace(String keyword, String value, String comment) {
	_setrec( keyword, value, comment, true );
	return _replace();
    }
    public int replace(String keyword, int value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
    public int replace(String keyword, float value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
    public int replace(String keyword, double value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
//----------------------------------------------------------------------------------------------------

    ///helper for ctors: sets the value of _FITSrecord and _FITSkey.

    protected int _setrec(String keyword, String value, String comment, boolean quote)
    {
	_FITSkey = rmJunk(keyword).toUpperCase();
	String valq = rmJunk(value);
	String cmt = rmJunk(comment);
	int vlen = valq.length();
	int klen = _FITSkey.length();
	int clen = cmt.length();

	if( quote ) {
	    if( vlen > 18)
		valq = valq.substring(0,18);
	    else if( vlen < 18)
		valq += _blankLine.substring( 0, 18 - vlen );
	    valq = "'" + valq + "'";
	}
	else {
	    if( vlen > 20)
		valq = valq.substring(0,20);
	    else if( vlen < 20)
		valq = _blankLine.substring( 0, 20 - vlen ) + valq;
	}

	if( klen > 8 )
	    _FITSkey = _FITSkey.substring(0,8);
	else if( klen < 8 )
	    _FITSkey += _blankLine.substring( 0, 8 - klen );

	if( clen > 48)
	    cmt = cmt.substring(0,48);
	else if( clen < 48)
	    cmt += _blankLine.substring( 0, 48 - clen );

	_FITSrecord = _FITSkey + "= " + valq + " /" + cmt;

	if(_verbose) System.out.println("UFFITS::_setrec> _FITSrecord=["
					+ _FITSrecord + "] " + _FITSrecord.length());

	return _FITSrecord.length();
    }

    protected int _addrec(String keyword, String value, String comment, boolean quote)
    {
	if( _setrec( keyword, value, comment, quote ) == 80 ) addLast( _FITSrecord );
	return this.size();
    }

    ///helper for ctors: adds one standard rec containing SIMPLE keyword
    protected int _start(String comment, boolean simple)
    {
	this.clear();

	if( simple )
	    return _addrec("SIMPLE","T", comment, false );
	else
	    return _addrec("SIMPLE","F", comment, false );
    }

    ///helper for replace funcs: uses current value of attrib _FITSkey.
    protected int _replace() {
	int cnt = this.size();
	boolean replaced = false;

	for( int i=0; i<cnt && !replaced; ++i ) {
	    String entry = (String)this.get(i);
	    if( entry.indexOf(_FITSkey) == 0 ) {
		remove(i);
		add( i, _FITSrecord );
		replaced = true;
	    }
	}

	if( !replaced ) addLast(_FITSrecord); // then just add it?

	return this.size();
    }
//----------------------------------------------------------------------------------------------------

    public int addrec(Map valhash, Map comments) {
	if (valhash.isEmpty())
	    return this.size();
	Iterator i  = valhash.keySet().iterator();
	String key, val, comment;
	while (i.hasNext()) {
	    key = (String) i.next();
	    val = (String) valhash.get(key);
	    if (comments.containsKey(key))
		comment = (String) comments.get(key);
	    else
		comment = "no comment";
	    addrec(key, val, comment);
	}
	return this.size();
    }

    // assuming args are in FITS header entry format
    // trucation or extension to 80 chars may occur:
    public int addrec(String s)
    {
	if (s.indexOf("=") != 8) return(-1); // clearly not a FITS header entry?
	int slen = s.length();

	if( slen > 80)
	    s = s.substring(0,80);
	else if( slen < 80)
	    s += _blankLine.substring( 0, 80 - slen );

	addLast(s);
	return this.size();
    }

    public int addrecs(String[] sar) {
	if( sar == null )
	    return this.size();
	
	for( int i=0; i < sar.length ; ++i ) addrec( sar[i] );
	return this.size();
    }

    public int end() { // this adds the "END" record to header
	_FITSrecord = "END";
	_FITSrecord += _blankLine.substring( 0, 80 - _FITSrecord.length() );
	addLast(_FITSrecord);
	return this.size();
    }

    public int date(String comment) {
	SimpleDateFormat df = new SimpleDateFormat("yyyy:DDD:HH:mm:ss");
	String time = df.format(new Date());
	return addrec("DATE_FH", time, comment);
    }

    public int date() { return this.date("UT of header creation (YYYY:DAY:HH:MM:SS)"); }

//----------------------------------------------------------------------------------------------------

    public String getRecord( String key ) {

	ListIterator Lit = this.listIterator();
	String Key = key.toUpperCase();

	while( Lit.hasNext() ) {
	    String rec = (String)Lit.next();
	    if( rec.indexOf( Key ) == 0 ) return rec;
	}

	System.err.println("UFFITS::getRecord> key [" + Key + "] not found.");
	return null;
    }

    public String getParam( String key ) {

	String rec = getRecord( key );

	if( rec != null ) {
	    int ieq = rec.indexOf("=");
	    if( ++ieq > 1 )
		return rec.substring( ieq, 21+ieq ).trim();
	    else {
		System.err.println("UFFITS::getParam> equal sign for key [" + key + "] not found.");
		return null;
	    }
	}
	else return null;
    }

    public int getInt( String key ) {

	String param = getParam( key );
	if( param == null ) return(-1);

	try {
	    return Integer.parseInt( param );
	}
	catch( NumberFormatException nfe ) {
	    System.err.println("UFFITS::getInt> key=" + key +
			       ": param=" + param + ": " + nfe.toString() );
	    return(-1);
	}
    }

    public float getFloat( String key ) {

	String param = getParam( key );
	if( param == null ) return(-1);

	try {
	    return Float.parseFloat( param );
	}
	catch( NumberFormatException nfe ) {
	    System.err.println("UFFITS::getFloat> key=" + key +
			       ": param=" + param + ": " + nfe.toString() );
	    return(-1);
	}
    }
//----------------------------------------------------------------------------------------------------
    // Static methods:
    /**
     * This next public class used for functions that require more than 1 return value
     * @see readPrmHdr(...), readExtHdr(...)
     */
    public static String rmJunk(String st)
    {
	StringBuffer s = new StringBuffer(st);
	if( s.length() <= 0 ) {
	    System.err.println("UFFITS::rmJunk> empty string ?");
	    return "";
	}
	
	int pos = s.toString().indexOf("\n");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\n", 1+pos);
	}
	pos = s.toString().indexOf("\r");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\r", 1+pos);
	}
	
	// eliminate white (adjacent) multiples
	for( pos = 0; pos < s.length(); ++pos ) {
	    while ( (s.charAt(pos) == ' ' || s.charAt(pos) == '\t') && pos+1 < s.length()
		    && (s.charAt(pos+1) == ' ' || s.charAt(pos+1) == '\t'))
		s.deleteCharAt(pos);
	}

	return s.toString().trim(); // remove leading and trailing whitespace and return
    }
    
    // convenience functions for reading and parsing an MEF (muli-extension FITS file):
    // this parses an integer valued by key:
    public static int fitsInt(String key, String fitsHdrEntry) {
	String s = new String(fitsHdrEntry);
	int pos = s.indexOf(key);
	if (pos == -1)
	    return -1;
	pos = 1+s.indexOf("=",pos);
	if (pos == -1)
	    return -1;
	s = s.substring(1+pos);
	while (s.charAt(0) == ' ') s = s.substring(1); // eliminate leading white sp
	pos = s.indexOf(" ");
	if (pos == -1)
	    pos = s.indexOf("/");
	if (pos != -1) {
	    try {
		return Integer.parseInt(s.substring(0,pos));
	    } catch (NumberFormatException nfe) {
		System.err.println("UFFITS::fitsInt> Poorly formatted fits header???");
		return -1;
	    }
	}
	return -1;
    }

}
