import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class FATBOY_GUI extends javax.swing.JFrame {
   int xdim, ydim;
   JPanel topPanel, bottomPanel;
   JMenuBar jmb;
   JMenu fileMenu, helpMenu;
   JMenuItem fileLoad, fileSave, fileExit, helpAbout;
   JTabbedPane currentPane, imagingPane, astromPane, specPane;
   JTabbedPane rectPane, rectMOSPane;
   JPanel[] imagingPanels, astromPanels, specPanels;
   JPanel[] rectPanels, rectMOSPanels;
   JLabel fatboyLabel, modeLabel, presetsLabel, commentsLabel;
   JComboBox mode, presets;
   JTextArea comments;
   Border labelBorder = new EtchedBorder(Color.RED, Color.YELLOW);
   Container content;
   String currentMode;
   File defaultDir = new File(".");
   Vector presetFiles;

   FATBOYParam logFile, warnFile;
   JLabel lStatus, lProgress;
   JTextField tStatus;
   UFColorButton bShowMessages, bGo, bQuit, bShowDisplay;
   JProgressBar progressBar;
   FATBOYExec fexec;
   FATBOYDisplayFrame display;

   LinkedHashMap imagingHash, astromHash, specHash, rectHash, rectMOSHash;
   LinkedHashMap currentHash;

   //Imaging parameters
   FATBOYParam pFiles, pDarkFileList, pFlatFileList, pQuickStartFile;
   FATBOYParam pOverwriteFiles, pCleanUp, pPrefixDelimiter, pExptimeKeyword;
   FATBOYParam pObstypeKeyword, pFilterKeyword, pRAKeyword, pDecKeyword;
   FATBOYParam pPixscaleKeyword, pPixscaleUnits, pIgnoreFirstFrames;
   FATBOYParam pIgnoreAfterBadRead, pRotPAKeyword, pUTKeyword, pDateKeyword;
   FATBOYParam pMinFrameValue, pMaxFrameValue, pGroupOutputBy, pSpecifyGrouping;
   FATBOYParam pMEFExtension, pRelativeOffsetArcsec;

   FATBOYParam pDoLinearize, pLinearityCoeffs, pDoDarkCombine, pDoDarkSubtract;
   FATBOYParam pDefaultMasterDark, pPromptDark;

   FATBOYParam pDoFlatCombine, pFlatType, pFlatMethodDome, pFlatLampOffFiles;
   FATBOYParam pFlatSkyRejectType, pFlatSkyNlow, pFlatSkyNhigh,pFlatSkyLsigma;
   FATBOYParam pFlatSkyHsigma, pDoFlatDivide, pDefaultMasterFlat;

   FATBOYParam pDoBadPixelMask, pDefaultBadPixelMask, pBadPixelMaskClipping;
   FATBOYParam pBadPixelHigh, pBadPixelLow, pBadPixelSigma, pEdgeReject;
   FATBOYParam pRadiusReject, pRemoveCosmicRays, pCosmicRayPasses;

   FATBOYParam pDoSkySubtract, pSkySubtractMethod, pSkyRejectType, pSkyNlow;
   FATBOYParam pSkyNhigh, pSkyLsigma, pSkyHsigma, pUseSkyFiles, pSkyFilesRange;
   FATBOYParam pSkyDitheringRange, pSkyOffsourceMethod, pSkyOffsourceRange;
   FATBOYParam pSkyOffsourceManual, pKeepSkies, pSkyNebFileRange;
   FATBOYParam pSextractorPath, pSextractorConfigPath, pSextractorConfigPrefix;
   FATBOYParam pTwoPassObjectMasking, pTwoPassDetectThresh;
   FATBOYParam pTwoPassDetectMinarea, pTwoPassBoxcarSize, pTwoPassRejectLevel;
   FATBOYParam pInterpZerosSky, pFitSkySubtractedSurf;

   FATBOYParam pDoAlignStack, pTwoPassAlignment, pAlignBoxSize;
   FATBOYParam pAlignBoxCenterX, pAlignBoxCenterY, pDrizzleDropsize;
   FATBOYParam pGeomTransCoeffs, pDrizzleKernel, pDrizzleInUnits;
   FATBOYParam pDrizzleOrImcombine, pKeepIndivImages, pStackRejectType;
   FATBOYParam pStackNlow, pStackNhigh, pStackLsigma, pStackHsigma;

   FATBOYParam pqsFiles, pqsDarkFileList, pqsFlatFileList, pqsQuickStartFile;
   FATBOYParam pqsMinFrameValue, pqsMaxFrameValue, pqsDoLinearize;
   FATBOYParam pqsLinearityCoeffs, pqsDoDarkCombine, pqsDoDarkSubtract;
   FATBOYParam pqsDoFlatCombine, pqsFlatType, pqsDoFlatDivide;
   FATBOYParam pqsDoBadPixelMask, pqsRemoveCosmicRays, pqsDoSkySubtract;
   FATBOYParam pqsSkySubtractMethod, pqsUseSkyFiles, pqsTwoPassObjectMasking;
   FATBOYParam pqsDoAlignStack, pqsAlignBoxSize, pqsAlignBoxCenterX;
   FATBOYParam pqsAlignBoxCenterY, pqsGeomTransCoeffs, pqsGroupOutputBy, pqsSpecifyGrouping;

   //Astrometry parameters
   FATBOYParam aFiles, aOverwriteFiles, aFilterKeyword, aRAKeyword;
   FATBOYParam aDecKeyword, aPixscaleKeyword, aPixscaleUnits, aRotPAKeyword;
   FATBOYParam aSextractorPath, aSextractorConfigPath, aSextractorConfigPrefix;
   FATBOYParam aCatalog, aRadius, aNObjectsImage, aNObjectsCatalog;
   FATBOYParam aXYXYMatchTolerance, aXYXYMatchNmatch, aUpdateRADec;
   FATBOYParam aDoPlateSolution, aTwoPassSolution, aMatchingType;
   FATBOYParam aCCXYMatchTolerance, aDefaultPlateSolution;

   //Longslit rectification paramters
   FATBOYParam lsFileList, lsDarkFrame, lsSetupList, lsXCenter, lsListOutput;
   FATBOYParam lsDoRectify, lsListInput, lsFitOrder, lsSkyFileList;
   FATBOYParam lsSkyDarkFrame, lsSkySetupList, lsSkySigma;
   FATBOYParam lsSkyTwoPassDetection, lsSkyBoxSize, lsSkyListOutput;
   FATBOYParam lsSkyDoRectify, lsSkyListInput, lsSkyFitOrder, lsFitOutput;

   //MOS rectification parameters
   FATBOYParam mRegFileList, mDoRegmassage, mRegmassagePath,mRegmassageOffset;
   FATBOYParam mSetupList, mFlatFieldList, mDarkFramesForFlats, mFileList;
   FATBOYParam mMaxSlitWidth, mBoxCenter, mBoxWidth, mSpectralThreshold;
   FATBOYParam mMaxSpectralWidth, mListOutput, mDoRectify, mListInput;
   FATBOYParam mMinCoverageFraction, mDatabaseFile, mAppendDatabase,mFitOrder;

   //Spectroscopy parameters
   FATBOYParam sFiles, sDarkFileList, sFlatFileList, sArclampList;
   FATBOYParam sQuickStartFile, sOverwriteFiles, sCleanUp, sDoNoisemaps;
   FATBOYParam sPrefixDelimiter, sExptimeKeyword, sObstypeKeyword;
   FATBOYParam sFilterKeyword, sRAKeyword, sDecKeyword, sObjectKeyword;
   FATBOYParam sGainKeyword, sReadnoiseKeyword, sGrismKeyword, sMinFrameValue;
   FATBOYParam sMaxFrameValue, sIgnoreFirstFrames, sIgnoreAfterBadRead;
   FATBOYParam sRotationAngle, sGroupOutputBy, sSpecifyGrouping;
   FATBOYParam sMEFExtension, sRelativeOffsetArcsec;

   FATBOYParam sDoLinearize, sLinearityCoeffs, sDoDarkCombine, sDoDarkSubtract;
   FATBOYParam sDefaultMasterDark, sPromptDark;

   FATBOYParam sDoFlatCombine, sFlatSelection, sFlatMethod, sFlatLampOffFiles;
   FATBOYParam sFlatLowThresh, sFlatLowReplace, sFlatHiThresh, sFlatHiReplace;
   FATBOYParam sDoArclampCombine, sArclampSelection;
   FATBOYParam sDoFlatDivide, sDefaultMasterFlat;

   FATBOYParam sRemoveCosmicRays, sCosmicRaySigma, sCosmicRayPasses;
   FATBOYParam sDoBadPixelMask, sDefaultBadPixelMask, sImagingFramesForBpm;
   FATBOYParam sBadPixelMaskClipping, sBadPixelHigh, sBadPixelLow;
   FATBOYParam sBadPixelSigma, sEdgeReject, sRadiusReject;

   FATBOYParam sDoSkySubtract, sSkySubtractMethod, sSkyRejectType, sSkyNlow;
   FATBOYParam sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange;
   FATBOYParam sSkyDitheringRange, sSkyOffsourceMethod, sSkyOffsourceRange;
   FATBOYParam sSkyOffsourceManual, sIgnoreOddFrames;

   FATBOYParam sDoRectify, sDataType, sLongslitRectCoeffs, sMosRectDb;
   FATBOYParam sMosRectOrder, sMosRegionFile, sMosYSlitBufferLow;
   FATBOYParam sMosYSlitBufferHigh, sMosMaxSlitWidth, sMosRectUseArclamps;
   FATBOYParam sMosSkylineSigma, sMosSkyTwoPass, sMosSkyBoxSize;
   FATBOYParam sMosSkyFitOrder, sDoDoubleSubtract, sSumBoxWidth;
   FATBOYParam sSumBoxCenter, sDoShiftAdd, sMaskOutTroughs;

   FATBOYParam sDoMosAlignSkylines, sReferenceSlit, sNebularEmissionCheck;
   FATBOYParam sReverseOrderOfSegments, sMedianFilter1D, sMosAlignUseArclamps;
   FATBOYParam sMosAlignNlines, sMosAlignSigma, sDoWavelengthCalibrate;
   FATBOYParam sWavelengthCalibrateUseArclamps, sCalibrationInfoFile;
   FATBOYParam sCalibrationMinSigma, sCalibrationFitOrder;
   FATBOYParam sMosAlignFitOrder;

   FATBOYParam sDoExtractSpectra, sExtractMethod, sExtractFilename;
   FATBOYParam sExtractBoxWidth, sExtractBoxCenter, sExtractNSpectra;
   FATBOYParam sExtractSigma, sExtractMinWidth, sExtractBoxGuessFile;
   FATBOYParam sExtractWeighting, sDoCalibrationStarDivide;
   FATBOYParam sCalibrationStarFile, sAngstromsPerUnit;

   FATBOYParam sqsFiles, sqsDoNoisemaps, sqsDoLinearize, sqsDoDarkCombine;
   FATBOYParam sqsDoDarkSubtract, sqsDoFlatCombine, sqsFlatSelection;
   FATBOYParam sqsDoFlatDivide, sqsDoArclampCombine, sqsArclampSelection;
   FATBOYParam sqsDoBadPixelMask, sqsRemoveCosmicRays, sqsDoSkySubtract;
   FATBOYParam sqsSkySubtractMethod, sqsDoRectify, sqsDataType;
   FATBOYParam sqsDoDoubleSubtract, sqsDoShiftAdd, sqsDoMosAlignSkylines;
   FATBOYParam sqsDoWavelengthCalibrate, sqsCalibrationInfoFile;
   FATBOYParam sqsDoExtractSpectra, sqsExtractMethod;
   FATBOYParam sqsDoCalibrationStarDivide, sqsCalibrationStarFile;
   FATBOYParam sqsGroupOutputBy, sqsSpecifyGrouping;

   public FATBOY_GUI() {
      this(1024,768);
   }

   public FATBOY_GUI(int xdim, int ydim) {
      super("FATBOY: The Flamingos II Data Pipeline");
      this.xdim = xdim;
      this.ydim = ydim;
      setSize(xdim, ydim);
      setDefaultCloseOperation(3);
      content = getContentPane();
      content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
      addWindowListener(new WindowAdapter() {
	public void windowClosing(WindowEvent ev) {
	   FATBOY_GUI.this.dispose();
	}
      });
      //Prepare menu bar
      jmb = new JMenuBar();
      fileMenu = new JMenu("File");
      fileLoad = new JMenuItem("Load");
      fileLoad.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           JFileChooser jfc = new JFileChooser(defaultDir);
           int returnVal = jfc.showOpenDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
              String filename = jfc.getSelectedFile().getAbsolutePath();
              defaultDir = jfc.getCurrentDirectory();
              loadParams(filename);
           }
        }
      });
      fileMenu.add(fileLoad);
      fileSave = new JMenuItem("Save");
      fileSave.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   JFileChooser jfc = new JFileChooser(defaultDir);
	   int returnVal = jfc.showSaveDialog((Component)ev.getSource());
	   if (returnVal == JFileChooser.APPROVE_OPTION) {
	      String filename = jfc.getSelectedFile().getAbsolutePath();
	      if (new File(filename).exists()) {
                Object[] saveOptions = {"Overwrite","Cancel"};
                int n = JOptionPane.showOptionDialog(FATBOY_GUI.this, filename+" already exists.", "File exists!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, saveOptions, saveOptions[1]);
                if (n == 1) {
		   return;
                }
	      }
	      defaultDir = jfc.getCurrentDirectory();
	      saveParams(filename);
	   }
	}
      });
      fileMenu.add(fileSave);
      fileExit = new JMenuItem("Exit");
      fileExit.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
           Object[] exitOptions = {"Exit","Cancel"};
           int n = JOptionPane.showOptionDialog(FATBOY_GUI.this, "Are you sure you want to quit and terminate any running processes?", "Exit FATBOY?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
           if (n == 0) {
              System.exit(0);
           }
	}
      });
      fileMenu.add(fileExit);
      helpMenu = new JMenu("Help");
      helpAbout = new JMenuItem("About");
      helpAbout.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   FATBOYHelpBrowser browser = new FATBOYHelpBrowser(FATBOY_GUI.class.getResource("help.html"));
	}
      });
      helpMenu.add(helpAbout);
      jmb.add(fileMenu);
      jmb.add(helpMenu);
      this.setJMenuBar(jmb);

      //Prepare top panel
      topPanel = new JPanel();
      topPanel.setPreferredSize(new Dimension(xdim, 150));
      topPanel.setBackground(Color.white);
      SpringLayout topLayout = new SpringLayout();
      topPanel.setLayout(topLayout);
      URL url = FATBOY_GUI.class.getResource("fatboy_gui_logo.gif");
      fatboyLabel = new JLabel(new ImageIcon(url));
      topPanel.add(fatboyLabel);
      topLayout.putConstraint(SpringLayout.WEST, fatboyLabel, 0, SpringLayout.WEST, topPanel);
      topLayout.putConstraint(SpringLayout.NORTH, fatboyLabel, 0, SpringLayout.NORTH, topPanel);

      modeLabel = new JLabel("Select a mode:");
      topPanel.add(modeLabel);
      topLayout.putConstraint(SpringLayout.WEST, modeLabel, 15, SpringLayout.EAST, fatboyLabel);
      topLayout.putConstraint(SpringLayout.NORTH, modeLabel, 5, SpringLayout.NORTH, topPanel); 

      String [] modeOptions = {"Imaging", "Astrometry", "Spectroscopy", "Longslit Rectification", "MOS Rectification"};
      mode = new JComboBox(modeOptions);
      mode.setSelectedIndex(0);
      mode.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   changeMode();
	}
      });
      topPanel.add(mode);
      topLayout.putConstraint(SpringLayout.WEST, mode, 15, SpringLayout.EAST, fatboyLabel);
      topLayout.putConstraint(SpringLayout.NORTH, mode, 5, SpringLayout.SOUTH, modeLabel);

      presetsLabel = new JLabel("Instrument Presets:");
      topPanel.add(presetsLabel);
      topLayout.putConstraint(SpringLayout.WEST, presetsLabel, 75, SpringLayout.EAST, mode);
      topLayout.putConstraint(SpringLayout.NORTH, presetsLabel, 5, SpringLayout.NORTH, topPanel);

      presets = new JComboBox();
      presets.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (!ev.getActionCommand().equals("do presets")) return;
	   if (presets.getSelectedIndex() == -1) return;
	   if (currentMode == null) return;
	   if (presetFiles.size() > presets.getSelectedIndex()) {
	      loadParams((URL)presetFiles.get(presets.getSelectedIndex()));
	   }
	}
      });
      setupPresets((String)mode.getSelectedItem());
      topPanel.add(presets);
      topLayout.putConstraint(SpringLayout.WEST, presets, 75, SpringLayout.EAST, mode);
      topLayout.putConstraint(SpringLayout.NORTH, presets, 5, SpringLayout.SOUTH, presetsLabel);

      commentsLabel = new JLabel("Comments:");
      topPanel.add(commentsLabel);
      topLayout.putConstraint(SpringLayout.WEST, commentsLabel, 15, SpringLayout.EAST, fatboyLabel);
      topLayout.putConstraint(SpringLayout.NORTH, commentsLabel, 15, SpringLayout.SOUTH, presets);

      comments = new JTextArea(4, 35);
      JScrollPane spComments = new JScrollPane(comments);
      topPanel.add(spComments);
      topLayout.putConstraint(SpringLayout.WEST, spComments, 12, SpringLayout.EAST, commentsLabel);
      topLayout.putConstraint(SpringLayout.NORTH, spComments, 15, SpringLayout.SOUTH, presets);

      //Define Panes for modes
      imagingPane = new JTabbedPane();
      astromPane = new JTabbedPane();
      specPane = new JTabbedPane();
      rectPane = new JTabbedPane();
      rectMOSPane = new JTabbedPane();

      //Setup Imaging Pane
      imagingPanels = new JPanel[7];
      SpringLayout[] imagingLayout = new SpringLayout[7];
      for (int j = 0; j < 7; j++) {
	imagingPanels[j] = new JPanel();
        imagingPanels[j].setPreferredSize(new Dimension(xdim,ydim-250));
	imagingLayout[j] = new SpringLayout();
	imagingPanels[j].setLayout(imagingLayout[j]);
      }

      //Imaging Initialization
      imagingHash = new LinkedHashMap(100);
      pFiles = new FATBOYTextAreaParam("FILES:", FATBOYLabel.RYBEVEL, 5, 20, "Add Files", "Clear");
      pFiles.addBrowse(true);
      pFiles.addClear();
      pFiles.addFocusBorder();
      pFiles.addTo(imagingPanels[1]);
      pFiles.setConstraints(imagingLayout[1], 10, 5, 190, 5, 10, imagingPanels[1], imagingPanels[1], false, false, true);
      imagingHash.put("FILES",pFiles);

      pGroupOutputBy = new FATBOYComboParam("GROUP_OUTPUT_BY:", FATBOYLabel.YGBEVEL,"filename,FITS keyword,from manual file");
      pGroupOutputBy.addFocusBorder();
      pGroupOutputBy.addTo(imagingPanels[1]);
      pGroupOutputBy.setConstraints(imagingLayout[1], 12, 5, 190, imagingPanels[1], pFiles.spParam);
      imagingHash.put("GROUP_OUTPUT_BY", pGroupOutputBy);

      pSpecifyGrouping = new FATBOYTextBrowseParam("SPECIFY_GROUPING:", FATBOYLabel.NONE, 20, "Browse");
      pSpecifyGrouping.addBrowse();
      pSpecifyGrouping.addTo(imagingPanels[1]);
      pSpecifyGrouping.setConstraints(imagingLayout[1], 12, 5, 190, 5, imagingPanels[1], pGroupOutputBy.cbParam); 
      imagingHash.put("SPECIFY_GROUPING", pSpecifyGrouping);

      pDarkFileList = new FATBOYTextAreaParam("DARK_FILE_LIST:", FATBOYLabel.YGBEVEL, 4, 20, "Add Files", "Clear");
      pDarkFileList.addBrowse(true);
      pDarkFileList.addClear();
      pDarkFileList.addFocusBorder();
      pDarkFileList.addTo(imagingPanels[1]);
      pDarkFileList.setConstraints(imagingLayout[1], 25, 5, 190, 5, 10, imagingPanels[1], pSpecifyGrouping.tParam);
      imagingHash.put("DARK_FILE_LIST", pDarkFileList);

      pFlatFileList = new FATBOYTextAreaParam("FLAT_FILE_LIST:", FATBOYLabel.YGBEVEL, 4, 20, "Add Files", "Clear");
      pFlatFileList.addBrowse(true);
      pFlatFileList.addClear();
      pFlatFileList.addFocusBorder();
      pFlatFileList.addTo(imagingPanels[1]);
      pFlatFileList.setConstraints(imagingLayout[1], 25, 5, 190, 5, 10, imagingPanels[1], pDarkFileList.spParam);
      imagingHash.put("FLAT_FILE_LIST", pFlatFileList);

      pQuickStartFile = new FATBOYTextBrowseParam("QUICK_START_FILE:", FATBOYLabel.YGBEVEL, 20, "Browse");
      pQuickStartFile.addBrowse();
      pQuickStartFile.addFocusBorder();
      pQuickStartFile.addTo(imagingPanels[1]);
      pQuickStartFile.setConstraints(imagingLayout[1], 25, 5, 190, 5, imagingPanels[1], pFlatFileList.spParam);
      imagingHash.put("QUICK_START_FILE", pQuickStartFile);

      pOverwriteFiles = new FATBOYRadioParam("OVERWRITE_FILES:", FATBOYLabel.NONE, "Yes", "No", false);
      pOverwriteFiles.addTo(imagingPanels[1]);
      pOverwriteFiles.setConstraints(imagingLayout[1], 25, 5, 190, 15, imagingPanels[1], pQuickStartFile.tParam);
      imagingHash.put("OVERWRITE_FILES", pOverwriteFiles);

      pCleanUp = new FATBOYRadioParam("CLEAN_UP:", FATBOYLabel.NONE, "Yes", "No", false);
      pCleanUp.addTo(imagingPanels[1]);
      pCleanUp.setConstraints(imagingLayout[1], 15, 5, 190, 15, imagingPanels[1], pOverwriteFiles.yParam);
      imagingHash.put("CLEAN_UP", pCleanUp);

      pPrefixDelimiter = new FATBOYTextParam("PREFIX_DELIMITER:", FATBOYLabel.NONE, 5, ".");
      pPrefixDelimiter.addTo(imagingPanels[1]);
      pPrefixDelimiter.setConstraints(imagingLayout[1], 15, 5, 190, imagingPanels[1], pCleanUp.yParam);
      imagingHash.put("PREFIX_DELIMITER", pPrefixDelimiter);

      pExptimeKeyword = new FATBOYTextParam("EXPTIME_KEYWORD:", FATBOYLabel.NONE, 10, "EXP_TIME");
      pExptimeKeyword.addTo(imagingPanels[1]);
      pExptimeKeyword.setConstraints(imagingLayout[1], 10, 564, 768, imagingPanels[1], imagingPanels[1], true);
      imagingHash.put("EXPTIME_KEYWORD", pExptimeKeyword);

      pObstypeKeyword = new FATBOYTextParam("OBSTYPE_KEYWORD:", FATBOYLabel.NONE, 10, "OBS_TYPE");
      pObstypeKeyword.addTo(imagingPanels[1]);
      pObstypeKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pExptimeKeyword.tParam);
      imagingHash.put("OBSTYPE_KEYWORD", pObstypeKeyword);

      pFilterKeyword = new FATBOYTextParam("FILTER_KEYWORD:", FATBOYLabel.NONE, 10, "FILTER");
      pFilterKeyword.addTo(imagingPanels[1]);
      pFilterKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pObstypeKeyword.tParam);
      imagingHash.put("FILTER_KEYWORD", pFilterKeyword);

      pRAKeyword = new FATBOYTextParam("RA_KEYWORD:", FATBOYLabel.NONE, 10, "RA");
      pRAKeyword.addTo(imagingPanels[1]);
      pRAKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pFilterKeyword.tParam);
      imagingHash.put("RA_KEYWORD", pRAKeyword);

      pDecKeyword = new FATBOYTextParam("DEC_KEYWORD:", FATBOYLabel.NONE, 10, "DEC");
      pDecKeyword.addTo(imagingPanels[1]);
      pDecKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pRAKeyword.tParam);
      imagingHash.put("DEC_KEYWORD", pDecKeyword);

      pRelativeOffsetArcsec = new FATBOYRadioParam("RELATIVE_OFFSET_ARCSEC:", "Yes", "No", false);
      pRelativeOffsetArcsec.addTo(imagingPanels[1]);
      pRelativeOffsetArcsec.setConstraints(imagingLayout[1], 8, 564, 792, 15, imagingPanels[1], pDecKeyword.tParam);
      imagingHash.put("RELATIVE_OFFSET_ARCSEC", pRelativeOffsetArcsec);

      pPixscaleKeyword = new FATBOYTextParam("PIXSCALE_KEYWORD:", FATBOYLabel.NONE, 10, "PIXSCALE");
      pPixscaleKeyword.addTo(imagingPanels[1]);
      pPixscaleKeyword.setConstraints(imagingLayout[1], 8, 564, 768, imagingPanels[1], pRelativeOffsetArcsec.yParam);
      imagingHash.put("PIXSCALE_KEYWORD", pPixscaleKeyword);

      pPixscaleUnits = new FATBOYRadioParam("PIXSCALE_UNITS:", "Arcsec", "Degrees");
      pPixscaleUnits.addTo(imagingPanels[1]);
      pPixscaleUnits.setConstraints(imagingLayout[1], 8, 564, 768, 15, imagingPanels[1], pPixscaleKeyword.tParam);
      imagingHash.put("PIXSCALE_UNITS", pPixscaleUnits);

      pRotPAKeyword = new FATBOYTextParam("ROT_PA_KEYWORD:", FATBOYLabel.NONE, 10, "ROT_PA");
      pRotPAKeyword.addTo(imagingPanels[1]);
      pRotPAKeyword.setConstraints(imagingLayout[1], 8, 564, 768, imagingPanels[1], pPixscaleUnits.yParam);
      imagingHash.put("ROT_PA_KEYWORD", pRotPAKeyword);

      pUTKeyword = new FATBOYTextParam("UT_KEYWORD:", FATBOYLabel.NONE, 10, "UTC");
      pUTKeyword.addTo(imagingPanels[1]);
      pUTKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pRotPAKeyword.tParam);
      imagingHash.put("UT_KEYWORD", pUTKeyword);

      pDateKeyword = new FATBOYTextParam("DATE_KEYWORD:", FATBOYLabel.NONE, 10, "DATE-OBS");
      pDateKeyword.addTo(imagingPanels[1]);
      pDateKeyword.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pUTKeyword.tParam);
      imagingHash.put("DATE_KEYWORD", pDateKeyword);

      pMinFrameValue = new FATBOYTextParam("MIN_FRAME_VALUE:", FATBOYLabel.YGBEVEL, 10);
      pMinFrameValue.addFocusBorder();
      pMinFrameValue.addTo(imagingPanels[1]);
      pMinFrameValue.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pDateKeyword.tParam);
      imagingHash.put("MIN_FRAME_VALUE", pMinFrameValue);

      pMaxFrameValue = new FATBOYTextParam("MAX_FRAME_VALUE:", FATBOYLabel.YGBEVEL, 10);
      pMaxFrameValue.addFocusBorder();
      pMaxFrameValue.addTo(imagingPanels[1]);
      pMaxFrameValue.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pMinFrameValue.tParam);
      imagingHash.put("MAX_FRAME_VALUE", pMaxFrameValue);

      pIgnoreFirstFrames = new FATBOYRadioParam("IGNORE_FIRST_FRAMES:", FATBOYLabel.NONE, "Yes", "No", false);
      pIgnoreFirstFrames.addTo(imagingPanels[1]);
      pIgnoreFirstFrames.setConstraints(imagingLayout[1], 12, 564, 792, 15, imagingPanels[1], pMaxFrameValue.tParam);
      imagingHash.put("IGNORE_FIRST_FRAMES", pIgnoreFirstFrames);

      pIgnoreAfterBadRead = new FATBOYRadioParam("IGNORE_AFTER_BAD_READ:", FATBOYLabel.NONE, "Yes", "No", false);
      pIgnoreAfterBadRead.addTo(imagingPanels[1]);
      pIgnoreAfterBadRead.setConstraints(imagingLayout[1], 8, 564, 792, 15, imagingPanels[1], pIgnoreFirstFrames.yParam);
      imagingHash.put("IGNORE_AFTER_BAD_READ", pIgnoreAfterBadRead);

      pMEFExtension = new FATBOYTextParam("MEF_EXTENSION:", FATBOYLabel.NONE, 5);
      pMEFExtension.addTo(imagingPanels[1]);
      pMEFExtension.setConstraints(imagingLayout[1], 12, 564, 768, imagingPanels[1], pIgnoreAfterBadRead.yParam);
      imagingHash.put("MEF_EXTENSION", pMEFExtension);

      //Imaging Linearity & Darks
      pDoLinearize = new FATBOYRadioParam("DO_LINEARIZE:", "Yes", "No");
      pDoLinearize.addTo(imagingPanels[2]);
      pDoLinearize.setConstraints(imagingLayout[2], 10, 5, 200, 15, imagingPanels[2], imagingPanels[2], false, true);
      imagingHash.put("DO_LINEARIZE", pDoLinearize);

      pLinearityCoeffs = new FATBOYTextParam("LINEARITY_COEFFS:", FATBOYLabel.YGBEVEL, 15, "1");
      pLinearityCoeffs.addFocusBorder();
      pLinearityCoeffs.addTo(imagingPanels[2]);
      pLinearityCoeffs.setConstraints(imagingLayout[2], 12, 5, 200, imagingPanels[2], pDoLinearize.yParam);
      imagingHash.put("LINEARITY_COEFFS", pLinearityCoeffs);

      pDoDarkCombine = new FATBOYRadioParam("DO_DARK_COMBINE:", "Yes", "No"); 
      pDoDarkCombine.addTo(imagingPanels[2]);
      pDoDarkCombine.setConstraints(imagingLayout[2], 36, 5, 200, 15, imagingPanels[2], pLinearityCoeffs.tParam);
      imagingHash.put("DO_DARK_COMBINE", pDoDarkCombine);

      pDoDarkSubtract = new FATBOYRadioParam("DO_DARK_SUBTRACT:", "Yes", "No");
      pDoDarkSubtract.addTo(imagingPanels[2]);
      pDoDarkSubtract.setConstraints(imagingLayout[2], 36, 5, 260, 15, imagingPanels[2], pDoDarkCombine.yParam);
      imagingHash.put("DO_DARK_SUBTRACT", pDoDarkSubtract);

      pDefaultMasterDark = new FATBOYTextBrowseParam("DEFAULT_MASTER_DARK:", FATBOYLabel.NONE, 20, "Browse");
      pDefaultMasterDark.addBrowse();
      pDefaultMasterDark.addTo(imagingPanels[2]);
      pDefaultMasterDark.setConstraints(imagingLayout[2], 12, 5, 260, 5, imagingPanels[2], pDoDarkSubtract.yParam);
      imagingHash.put("DEFAULT_MASTER_DARK", pDefaultMasterDark);

      pPromptDark = new FATBOYRadioParam("PROMPT_FOR_MISSING_DARK:", FATBOYLabel.NONE, "Yes", "No");
      pPromptDark.addTo(imagingPanels[2]);
      pPromptDark.setConstraints(imagingLayout[2], 12, 5, 260, 15, imagingPanels[2], pDefaultMasterDark.tParam);
      imagingHash.put("PROMPT_FOR_MISSING_DARK", pPromptDark);

      //Imaging Flat Fields
      pDoFlatCombine = new FATBOYRadioParam("DO_FLAT_COMBINE:", "Yes", "No");
      pDoFlatCombine.addTo(imagingPanels[3]);
      pDoFlatCombine.setConstraints(imagingLayout[3], 10, 5, 220, 15, imagingPanels[3], imagingPanels[3], false, true);
      imagingHash.put("DO_FLAT_COMBINE", pDoFlatCombine);

      pFlatType = new FATBOYComboBrowseParam("FLAT_TYPE:", FATBOYLabel.RYBEVEL,"dome,sky,twilight,from file", 20, "Browse");
      pFlatType.addBrowse();
      pFlatType.addFocusBorder();
      pFlatType.addTo(imagingPanels[3]);
      pFlatType.setConstraints(imagingLayout[3], 12, 5, 220, 12, 220, 5, imagingPanels[3], pDoFlatCombine.yParam);
      imagingHash.put("FLAT_TYPE", pFlatType);

      pFlatMethodDome = new FATBOYRadioParam("FLAT_METHOD_DOME:", "On-Off", "On");
      pFlatMethodDome.addTo(imagingPanels[3]);
      pFlatMethodDome.setConstraints(imagingLayout[3], 12, 5, 220, 15, imagingPanels[3], pFlatType.tParam);
      imagingHash.put("FLAT_METHOD_DOME", pFlatMethodDome);

      pFlatLampOffFiles = new FATBOYTextBrowseParam("FLAT_LAMP_OFF_FILES:", 20, "Browse", "off");
      pFlatLampOffFiles.addBrowse();
      pFlatLampOffFiles.addTo(imagingPanels[3]);
      pFlatLampOffFiles.setConstraints(imagingLayout[3], 12, 5, 220, 5, imagingPanels[3], pFlatMethodDome.yParam);
      imagingHash.put("FLAT_LAMP_OFF_FILES", pFlatLampOffFiles);

      pFlatSkyRejectType = new FATBOYComboParam("FLAT_SKY_REJECT_TYPE:", "avsigclip,minmax,none,sigclip", "none");
      pFlatSkyRejectType.addTo(imagingPanels[3]);
      pFlatSkyRejectType.setConstraints(imagingLayout[3], 20, 5, 220, imagingPanels[3], pFlatLampOffFiles.tParam);
      imagingHash.put("FLAT_SKY_REJECT_TYPE", pFlatSkyRejectType);

      pFlatSkyNlow = new FATBOYTextParam("FLAT_SKY_NLOW:", 10, "1");
      pFlatSkyNlow.addTo(imagingPanels[3]);
      pFlatSkyNlow.setConstraints(imagingLayout[3], 12, 5, 220, imagingPanels[3], pFlatSkyRejectType.cbParam);
      imagingHash.put("FLAT_SKY_NLOW", pFlatSkyNlow);

      pFlatSkyNhigh = new FATBOYTextParam("FLAT_SKY_NHIGH:", 10, "1");
      pFlatSkyNhigh.addTo(imagingPanels[3]);
      pFlatSkyNhigh.setConstraints(imagingLayout[3], 12, 5, 220, imagingPanels[3], pFlatSkyNlow.tParam);
      imagingHash.put("FLAT_SKY_NHIGH", pFlatSkyNhigh);

      pFlatSkyLsigma = new FATBOYTextParam("FLAT_SKY_LSIGMA:", 10, "3");
      pFlatSkyLsigma.addTo(imagingPanels[3]);
      pFlatSkyLsigma.setConstraints(imagingLayout[3], 12, 5, 220, imagingPanels[3], pFlatSkyNhigh.tParam);
      imagingHash.put("FLAT_SKY_LSIGMA", pFlatSkyLsigma);

      pFlatSkyHsigma = new FATBOYTextParam("FLAT_SKY_HSIGMA:", 10, "3");
      pFlatSkyHsigma.addTo(imagingPanels[3]);
      pFlatSkyHsigma.setConstraints(imagingLayout[3], 12, 5, 220, imagingPanels[3], pFlatSkyLsigma.tParam);
      imagingHash.put("FLAT_SKY_HSIGMA", pFlatSkyHsigma);

      pDoFlatDivide = new FATBOYRadioParam("DO_FLAT_DIVIDE:", "Yes", "No");
      pDoFlatDivide.addTo(imagingPanels[3]);
      pDoFlatDivide.setConstraints(imagingLayout[3], 36, 5, 220, 15, imagingPanels[2], pFlatSkyHsigma.tParam);
      imagingHash.put("DO_FLAT_DIVIDE", pDoFlatDivide);

      pDefaultMasterFlat = new FATBOYTextBrowseParam("DEFAULT_MASTER_FLAT:", FATBOYLabel.NONE, 20, "Browse");
      pDefaultMasterFlat.addBrowse();
      pDefaultMasterFlat.addTo(imagingPanels[3]);
      pDefaultMasterFlat.setConstraints(imagingLayout[3], 12, 5, 220, 5, imagingPanels[3], pDoFlatDivide.yParam);
      imagingHash.put("DEFAULT_MASTER_FLAT", pDefaultMasterFlat);

      //Imaging Bad Pixels 
      pDoBadPixelMask = new FATBOYRadioParam("DO_BAD_PIXEL_MASK:", "Yes", "No");
      pDoBadPixelMask.addTo(imagingPanels[4]);
      pDoBadPixelMask.setConstraints(imagingLayout[4], 10, 5, 260, 15, imagingPanels[4], imagingPanels[4], false, true);
      imagingHash.put("DO_BAD_PIXEL_MASK", pDoBadPixelMask);

      pDefaultBadPixelMask = new FATBOYTextBrowseParam("DEFAULT_BAD_PIXEL_MASK:", FATBOYLabel.NONE, 20, "Browse");
      pDefaultBadPixelMask.addBrowse();
      pDefaultBadPixelMask.addTo(imagingPanels[4]);
      pDefaultBadPixelMask.setConstraints(imagingLayout[4], 12, 5, 260, 5, imagingPanels[4], pDoBadPixelMask.yParam);
      imagingHash.put("DEFAULT_BAD_PIXEL_MASK", pDefaultBadPixelMask);

      pBadPixelMaskClipping = new FATBOYRadioParam("BAD_PIXEL_MASK_CLIPPING:", "Values","Sigma");
      pBadPixelMaskClipping.addTo(imagingPanels[4]);
      pBadPixelMaskClipping.setConstraints(imagingLayout[4], 12, 5, 260, 15, imagingPanels[4], pDefaultBadPixelMask.tParam);
      imagingHash.put("BAD_PIXEL_MASK_CLIPPING", pBadPixelMaskClipping);

      pBadPixelHigh = new FATBOYTextParam("BAD_PIXEL_HIGH:",10,"2.0");
      pBadPixelHigh.addTo(imagingPanels[4]);
      pBadPixelHigh.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pBadPixelMaskClipping.yParam);
      imagingHash.put("BAD_PIXEL_HIGH", pBadPixelHigh);

      pBadPixelLow = new FATBOYTextParam("BAD_PIXEL_LOW:",10,"0.5");
      pBadPixelLow.addTo(imagingPanels[4]);
      pBadPixelLow.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pBadPixelHigh.tParam);
      imagingHash.put("BAD_PIXEL_LOW", pBadPixelLow);

      pBadPixelSigma = new FATBOYTextParam("BAD_PIXEL_SIGMA:",10,"5");
      pBadPixelSigma.addTo(imagingPanels[4]);
      pBadPixelSigma.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pBadPixelLow.tParam);
      imagingHash.put("BAD_PIXEL_SIGMA", pBadPixelSigma);

      pEdgeReject = new FATBOYTextParam("EDGE_REJECT:",5,"5");
      pEdgeReject.addTo(imagingPanels[4]);
      pEdgeReject.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pBadPixelSigma.tParam);
      imagingHash.put("EDGE_REJECT", pEdgeReject);

      pRadiusReject = new FATBOYTextParam("RADIUS_REJECT:",5,"0");
      pRadiusReject.addTo(imagingPanels[4]);
      pRadiusReject.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pEdgeReject.tParam);
      imagingHash.put("RADIUS_REJECT", pRadiusReject);

      pRemoveCosmicRays = new FATBOYRadioParam("REMOVE_COSMIC_RAYS:", FATBOYLabel.YGBEVEL, "Yes","No");
      pRemoveCosmicRays.addFocusBorder();
      pRemoveCosmicRays.addTo(imagingPanels[4]);
      pRemoveCosmicRays.setConstraints(imagingLayout[4], 36, 5, 260, 15, imagingPanels[4], pRadiusReject.tParam);
      imagingHash.put("REMOVE_COSMIC_RAYS", pRemoveCosmicRays);

      pCosmicRayPasses = new FATBOYTextParam("COSMIC_RAY_PASSES:",5,"3");
      pCosmicRayPasses.addTo(imagingPanels[4]);
      pCosmicRayPasses.setConstraints(imagingLayout[4], 12, 5, 260, imagingPanels[4], pRemoveCosmicRays.yParam);
      imagingHash.put("COSMIC_RAY_PASSES", pCosmicRayPasses);


      //Imaging Sky Subtraction 
      pDoSkySubtract = new FATBOYRadioParam("DO_SKY_SUBTRACT:", "Yes", "No");
      pDoSkySubtract.addTo(imagingPanels[5]);
      pDoSkySubtract.setConstraints(imagingLayout[5], 10, 5, 240, 15, imagingPanels[5], imagingPanels[5], false, true);
      imagingHash.put("DO_SKY_SUBTRACT", pDoSkySubtract);

      pSkySubtractMethod = new FATBOYComboParam("SKY_SUBTRACT_METHOD:", FATBOYLabel.RYBEVEL,"rough,remove_objects,offsource,offsource_extended,offsource_neb");
      pSkySubtractMethod.addFocusBorder();
      pSkySubtractMethod.addTo(imagingPanels[5]);
      pSkySubtractMethod.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pDoSkySubtract.yParam);
      imagingHash.put("SKY_SUBTRACT_METHOD", pSkySubtractMethod);

      pSkyRejectType = new FATBOYComboParam("SKY_REJECT_TYPE:", "avsigclip,minmax,none,sigclip", "sigclip");
      pSkyRejectType.addTo(imagingPanels[5]);
      pSkyRejectType.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkySubtractMethod.cbParam);
      imagingHash.put("SKY_REJECT_TYPE", pSkyRejectType);

      pSkyNlow = new FATBOYTextParam("SKY_NLOW:", 10, "1");
      pSkyNlow.addTo(imagingPanels[5]);
      pSkyNlow.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyRejectType.cbParam);
      imagingHash.put("SKY_NLOW", pSkyNlow);

      pSkyNhigh = new FATBOYTextParam("SKY_NHIGH:", 10, "1");
      pSkyNhigh.addTo(imagingPanels[5]);
      pSkyNhigh.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyNlow.tParam);
      imagingHash.put("SKY_NHIGH", pSkyNhigh);

      pSkyLsigma = new FATBOYTextParam("SKY_LSIGMA:", 10, "3");
      pSkyLsigma.addTo(imagingPanels[5]);
      pSkyLsigma.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyNhigh.tParam);
      imagingHash.put("SKY_LSIGMA", pSkyLsigma);

      pSkyHsigma = new FATBOYTextParam("SKY_HSIGMA:", 10, "3");
      pSkyHsigma.addTo(imagingPanels[5]);
      pSkyHsigma.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyLsigma.tParam);
      imagingHash.put("SKY_HSIGMA", pSkyHsigma);

      pUseSkyFiles = new FATBOYRadioParam("USE_SKY_FILES:", FATBOYLabel.YGBEVEL, "All", "Range");
      pUseSkyFiles.addFocusBorder();
      pUseSkyFiles.addTo(imagingPanels[5]);
      pUseSkyFiles.setConstraints(imagingLayout[5], 12, 5, 240, 15, imagingPanels[5], pSkyHsigma.tParam);
      imagingHash.put("USE_SKY_FILES", pUseSkyFiles);

      pSkyFilesRange = new FATBOYTextParam("SKY_FILES_RANGE:", 10, "3");
      pSkyFilesRange.addTo(imagingPanels[5]);
      pSkyFilesRange.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pUseSkyFiles.yParam);
      imagingHash.put("SKY_FILES_RANGE", pSkyFilesRange);

      pSkyDitheringRange = new FATBOYTextParam("SKY_DITHERING_RANGE:", 10, "2");
      pSkyDitheringRange.addTo(imagingPanels[5]);
      pSkyDitheringRange.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyFilesRange.tParam);
      imagingHash.put("SKY_DITHERING_RANGE", pSkyDitheringRange);

      pSkyOffsourceMethod = new FATBOYRadioParam("SKY_OFFSOURCE_METHOD:", "Auto", "Manual");
      pSkyOffsourceMethod.addTo(imagingPanels[5]);
      pSkyOffsourceMethod.setConstraints(imagingLayout[5], 12, 5, 240, 15, imagingPanels[5], pSkyDitheringRange.tParam);
      imagingHash.put("SKY_OFFSOURCE_METHOD", pSkyOffsourceMethod);

      pSkyOffsourceRange = new FATBOYTextParam("SKY_OFFSOURCE_RANGE:", 10, "240");
      pSkyOffsourceRange.addTo(imagingPanels[5]);
      pSkyOffsourceRange.setConstraints(imagingLayout[5], 12, 5, 240, imagingPanels[5], pSkyOffsourceMethod.yParam);
      imagingHash.put("SKY_OFFSOURCE_RANGE", pSkyOffsourceRange);

      pSkyOffsourceManual = new FATBOYTextBrowseParam("SKY_OFFSOURCE_MANUAL:", 16, "Browse");
      pSkyOffsourceManual.addBrowse();
      pSkyOffsourceManual.addTo(imagingPanels[5]);
      pSkyOffsourceManual.setConstraints(imagingLayout[5], 12, 5, 240, 5, imagingPanels[5], pSkyOffsourceRange.tParam);
      imagingHash.put("SKY_OFFSOURCE_MANUAL", pSkyOffsourceManual);

      pKeepSkies = new FATBOYRadioParam("KEEP_SKIES:", "Yes", "No", false);
      pKeepSkies.addTo(imagingPanels[5]);
      pKeepSkies.setConstraints(imagingLayout[5], 12, 5, 240, 15, imagingPanels[5], pSkyOffsourceManual.tParam);
      imagingHash.put("KEEP_SKIES", pKeepSkies);

      pSkyNebFileRange = new FATBOYTextParam("SKY_NEB_FILE_RANGE:", 10, "5");
      pSkyNebFileRange.addTo(imagingPanels[5]);
      pSkyNebFileRange.setConstraints(imagingLayout[5], 10, 480, 735, imagingPanels[5], imagingPanels[5], true);
      imagingHash.put("SKY_NEB_FILE_RANGE", pSkyNebFileRange);

      pSextractorPath = new FATBOYTextBrowseParam("SEXTRACTOR_PATH:", 16, "Browse", "sex");
      pSextractorPath.addBrowse();
      pSextractorPath.addTo(imagingPanels[5]);
      pSextractorPath.setConstraints(imagingLayout[5], 12, 480, 735, 5, imagingPanels[5], pSkyNebFileRange.tParam);
      imagingHash.put("SEXTRACTOR_PATH", pSextractorPath);

      pSextractorConfigPath = new FATBOYTextBrowseParam("SEXTRACTOR_CONFIG_PATH:", 16, "Browse", "sextractor-2.3.2/config");
      pSextractorConfigPath.addDirBrowse();
      pSextractorConfigPath.addTo(imagingPanels[5]);
      pSextractorConfigPath.setConstraints(imagingLayout[5], 12, 480, 735, 5, imagingPanels[5], pSextractorPath.tParam);
      imagingHash.put("SEXTRACTOR_CONFIG_PATH", pSextractorConfigPath);

      pSextractorConfigPrefix = new FATBOYTextParam("SEXTRACTOR_CONFIG_PREFIX:", 16, "default");
      pSextractorConfigPrefix.addTo(imagingPanels[5]);
      pSextractorConfigPrefix.setConstraints(imagingLayout[5], 12, 480, 735, imagingPanels[5], pSextractorConfigPath.tParam);
      imagingHash.put("SEXTRACTOR_CONFIG_PREFIX", pSextractorConfigPrefix);

      pTwoPassObjectMasking = new FATBOYRadioParam("TWO_PASS_OBJECT_MASKING:", FATBOYLabel.YGBEVEL, "Yes", "No", false);
      pTwoPassObjectMasking.addFocusBorder();
      pTwoPassObjectMasking.addTo(imagingPanels[5]);
      pTwoPassObjectMasking.setConstraints(imagingLayout[5], 12, 480, 735, 15, imagingPanels[5], pSextractorConfigPrefix.tParam);
      imagingHash.put("TWO_PASS_OBJECT_MASKING", pTwoPassObjectMasking);

      pTwoPassDetectThresh = new FATBOYTextParam("TWO_PASS_DETECT_THRESH:", 10, "10");
      pTwoPassDetectThresh.addTo(imagingPanels[5]);
      pTwoPassDetectThresh.setConstraints(imagingLayout[5], 12, 480, 735, imagingPanels[5], pTwoPassObjectMasking.yParam);
      imagingHash.put("TWO_PASS_DETECT_THRESH", pTwoPassDetectThresh);

      pTwoPassDetectMinarea = new FATBOYTextParam("TWO_PASS_DETECT_MINAREA:", 10, "50");
      pTwoPassDetectMinarea.addTo(imagingPanels[5]);
      pTwoPassDetectMinarea.setConstraints(imagingLayout[5], 12, 480, 735, imagingPanels[5], pTwoPassDetectThresh.tParam);
      imagingHash.put("TWO_PASS_DETECT_MINAREA", pTwoPassDetectMinarea);

      pTwoPassBoxcarSize = new FATBOYTextParam("TWO_PASS_BOXCAR_SIZE:", 10, "51");
      pTwoPassBoxcarSize.addTo(imagingPanels[5]);
      pTwoPassBoxcarSize.setConstraints(imagingLayout[5], 12, 480, 735, imagingPanels[5], pTwoPassDetectMinarea.tParam);
      imagingHash.put("TWO_PASS_BOXCAR_SIZE", pTwoPassBoxcarSize);

      pTwoPassRejectLevel = new FATBOYTextParam("TWO_PASS_REJECT_LEVEL:", 10, "0.95");
      pTwoPassRejectLevel.addTo(imagingPanels[5]);
      pTwoPassRejectLevel.setConstraints(imagingLayout[5], 12, 480, 735, imagingPanels[5], pTwoPassBoxcarSize.tParam);
      imagingHash.put("TWO_PASS_REJECT_LEVEL", pTwoPassRejectLevel);

      pInterpZerosSky = new FATBOYRadioParam("INTERP_ZEROS_SKY:", "Yes", "No", false);
      pInterpZerosSky.addTo(imagingPanels[5]);
      pInterpZerosSky.setConstraints(imagingLayout[5], 12, 480, 735, 15, imagingPanels[5], pTwoPassRejectLevel.tParam);
      imagingHash.put("INTERP_ZEROS_SKY", pInterpZerosSky);

      pFitSkySubtractedSurf = new FATBOYRadioParam("FIT_SKY_SUBTRACTED_SURF:", "Yes", "No", false);
      pFitSkySubtractedSurf.addTo(imagingPanels[5]);
      pFitSkySubtractedSurf.setConstraints(imagingLayout[5], 12, 480, 735, 15, imagingPanels[5], pInterpZerosSky.yParam);
      imagingHash.put("FIT_SKY_SUBTRACTED_SURF", pFitSkySubtractedSurf);


      //Imaging Alignment & Stacking 
      pDoAlignStack = new FATBOYRadioParam("DO_ALIGN_STACK:", "Yes", "No");
      pDoAlignStack.addTo(imagingPanels[6]);
      pDoAlignStack.setConstraints(imagingLayout[6], 10, 5, 220, 15, imagingPanels[6], imagingPanels[6], false, true);
      imagingHash.put("DO_ALIGN_STACK", pDoAlignStack);

      pTwoPassAlignment = new FATBOYRadioParam("TWO_PASS_ALIGNMENT:", "Yes", "No", false);
      pTwoPassAlignment.addTo(imagingPanels[6]);
      pTwoPassAlignment.setConstraints(imagingLayout[6], 12, 5, 220, 15, imagingPanels[6], pDoAlignStack.yParam);
      imagingHash.put("TWO_PASS_ALIGNMENT", pTwoPassAlignment);

      pAlignBoxSize = new FATBOYTextParam("ALIGN_BOX_SIZE:", FATBOYLabel.YGBEVEL, 10, "256");
      pAlignBoxSize.addFocusBorder();
      pAlignBoxSize.addTo(imagingPanels[6]);
      pAlignBoxSize.setConstraints(imagingLayout[6], 12, 5, 220, imagingPanels[6], pTwoPassAlignment.yParam);
      imagingHash.put("ALIGN_BOX_SIZE", pAlignBoxSize);

      pAlignBoxCenterX = new FATBOYTextParam("ALIGN_BOX_CENTER_X:", FATBOYLabel.YGBEVEL, 10, "1024");
      pAlignBoxCenterX.addFocusBorder();
      pAlignBoxCenterX.addTo(imagingPanels[6]);
      pAlignBoxCenterX.setConstraints(imagingLayout[6], 12, 5, 220, imagingPanels[6], pAlignBoxSize.tParam);
      imagingHash.put("ALIGN_BOX_CENTER_X", pAlignBoxCenterX);

      pAlignBoxCenterY = new FATBOYTextParam("ALIGN_BOX_CENTER_Y:", FATBOYLabel.YGBEVEL, 10, "1024");
      pAlignBoxCenterY.addFocusBorder();
      pAlignBoxCenterY.addTo(imagingPanels[6]);
      pAlignBoxCenterY.setConstraints(imagingLayout[6], 12, 5, 220, imagingPanels[6], pAlignBoxCenterX.tParam);
      imagingHash.put("ALIGN_BOX_CENTER_Y", pAlignBoxCenterY);

      pDrizzleDropsize = new FATBOYTextParam("DRIZZLE_DROPSIZE:", 10, "0.01");
      pDrizzleDropsize.addTo(imagingPanels[6]);
      pDrizzleDropsize.setConstraints(imagingLayout[6], 12, 5, 220, imagingPanels[6], pAlignBoxCenterY.tParam);
      imagingHash.put("DRIZZLE_DROPSIZE", pDrizzleDropsize);

      pGeomTransCoeffs = new FATBOYTextBrowseParam("GEOM_TRANS_COEFFS:", FATBOYLabel.RYBEVEL, 20, "Browse");
      String temp;
      try {
	temp = FATBOY_GUI.class.getResource("help.html").toString();
      } catch (Exception e) { temp = ""; }
      temp = temp.replaceAll("jar:file:","");
      temp = temp.replaceAll("FATBOY_GUI.jar!/help.html","");
      pGeomTransCoeffs.addBrowse(temp+"distortionMaps");
      pGeomTransCoeffs.addFocusBorder();
      pGeomTransCoeffs.addTo(imagingPanels[6]);
      pGeomTransCoeffs.setConstraints(imagingLayout[6], 12, 5, 220, 5, imagingPanels[6], pDrizzleDropsize.tParam);
      imagingHash.put("GEOM_TRANS_COEFFS", pGeomTransCoeffs);

      pDrizzleKernel = new FATBOYComboParam("DRIZZLE_KERNEL:", "square,point,turbo,gaussian,tophat,lanczos2,lanczos3","point");
      pDrizzleKernel.addTo(imagingPanels[6]);
      pDrizzleKernel.setConstraints(imagingLayout[6], 15, 5, 220, imagingPanels[6], pGeomTransCoeffs.tParam);
      imagingHash.put("DRIZZLE_KERNEL", pDrizzleKernel);

      pDrizzleInUnits = new FATBOYRadioParam("DRIZZLE_IN_UNITS:", "Counts", "cps");
      pDrizzleInUnits.addTo(imagingPanels[6]);
      pDrizzleInUnits.setConstraints(imagingLayout[6], 12, 5, 220, 15, imagingPanels[6], pDrizzleKernel.cbParam);
      imagingHash.put("DRIZZLE_IN_UNITS", pDrizzleInUnits);

      pDrizzleOrImcombine = new FATBOYRadioParam("DRIZZLE_OR_IMCOMBINE:", "Drizzle", "Imcombine");
      pDrizzleOrImcombine.addTo(imagingPanels[6]);
      pDrizzleOrImcombine.setConstraints(imagingLayout[6], 12, 5, 220, 15, imagingPanels[6], pDrizzleInUnits.yParam);
      imagingHash.put("DRIZZLE_OR_IMCOMBINE", pDrizzleOrImcombine);

      pKeepIndivImages = new FATBOYRadioParam("KEEP_INDIV_IMAGES:", "Yes", "No", false);
      pKeepIndivImages.addTo(imagingPanels[6]);
      pKeepIndivImages.setConstraints(imagingLayout[6], 10, 564, 768, 15, imagingPanels[6], imagingPanels[6], false, true);
      imagingHash.put("KEEP_INDIV_IMAGES", pKeepIndivImages);

      pStackRejectType = new FATBOYComboParam("STACK_REJECT_TYPE:", "avsigclip,minmax,none,sigclip", "sigclip");
      pStackRejectType.addTo(imagingPanels[6]);
      pStackRejectType.setConstraints(imagingLayout[6], 12, 564, 768, imagingPanels[6], pKeepIndivImages.yParam);
      imagingHash.put("STACK_REJECT_TYPE", pStackRejectType);

      pStackNlow = new FATBOYTextParam("STACK_NLOW:", 10, "1");
      pStackNlow.addTo(imagingPanels[6]);
      pStackNlow.setConstraints(imagingLayout[6], 12, 564, 768, imagingPanels[6], pStackRejectType.cbParam);
      imagingHash.put("STACK_NLOW", pStackNlow);

      pStackNhigh = new FATBOYTextParam("STACK_NHIGH:", 10, "1");
      pStackNhigh.addTo(imagingPanels[6]);
      pStackNhigh.setConstraints(imagingLayout[6], 12, 564, 768, imagingPanels[6], pStackNlow.tParam);
      imagingHash.put("STACK_NHIGH", pStackNhigh);

      pStackLsigma = new FATBOYTextParam("STACK_LSIGMA:", 10, "3");
      pStackLsigma.addTo(imagingPanels[6]);
      pStackLsigma.setConstraints(imagingLayout[6], 12, 564, 768, imagingPanels[6], pStackNhigh.tParam);
      imagingHash.put("STACK_LSIGMA", pStackLsigma);

      pStackHsigma = new FATBOYTextParam("STACK_HSIGMA:", 10, "3");
      pStackHsigma.addTo(imagingPanels[6]);
      pStackHsigma.setConstraints(imagingLayout[6], 12, 564, 768, imagingPanels[6], pStackLsigma.tParam);
      imagingHash.put("STACK_HSIGMA", pStackHsigma);

      //Add graying options
      pGroupOutputBy.addGrayComponents("filename",pSpecifyGrouping);
      pDoLinearize.addGrayComponents("No",pLinearityCoeffs);
      pDoDarkSubtract.addGrayComponents("No",pDefaultMasterDark, pPromptDark);
      pDoFlatCombine.addGrayComponents("No",pFlatType, pFlatMethodDome, pFlatLampOffFiles, pFlatSkyRejectType, pFlatSkyNlow, pFlatSkyNhigh, pFlatSkyLsigma, pFlatSkyHsigma);
      pFlatType.addGrayComponents("sky",pFlatMethodDome, pFlatLampOffFiles);
      pFlatType.addGrayComponents("twilight",pFlatMethodDome, pFlatLampOffFiles);
      pFlatType.addGrayComponents("from file",pFlatMethodDome);
      //pFlatType.addGrayComponents("from file",pFlatMethodDome, pFlatLampOffFiles);
      pFlatMethodDome.addGrayComponents("On",pFlatLampOffFiles);
      pFlatSkyRejectType.addGrayComponents("none",  pFlatSkyNlow, pFlatSkyNhigh, pFlatSkyLsigma, pFlatSkyHsigma);
      pFlatSkyRejectType.addGrayComponents("avsigclip", pFlatSkyNlow, pFlatSkyNhigh);
      pFlatSkyRejectType.addGrayComponents("sigclip", pFlatSkyNlow, pFlatSkyNhigh);
      pFlatSkyRejectType.addGrayComponents("minmax",  pFlatSkyLsigma, pFlatSkyHsigma);
      pDoFlatDivide.addGrayComponents("No", pDefaultMasterFlat);
      pDoBadPixelMask.addGrayComponents("No", pDefaultBadPixelMask, pBadPixelMaskClipping, pBadPixelHigh, pBadPixelLow, pBadPixelSigma, pEdgeReject, pRadiusReject);
      pBadPixelMaskClipping.addGrayComponents("Values", pBadPixelSigma);
      pBadPixelMaskClipping.addGrayComponents("Sigma", pBadPixelHigh, pBadPixelLow);
      pRemoveCosmicRays.addGrayComponents("No", pCosmicRayPasses);
      pDoSkySubtract.addGrayComponents("No", pSkySubtractMethod, pSkyRejectType, pSkyNlow, pSkyNhigh, pSkyLsigma, pSkyHsigma, pUseSkyFiles, pSkyFilesRange, pSkyDitheringRange, pSkyOffsourceMethod, pSkyOffsourceRange, pSkyOffsourceManual, pKeepSkies, pSkyNebFileRange, pSextractorPath, pSextractorConfigPath, pSextractorConfigPrefix, pTwoPassObjectMasking, pTwoPassDetectThresh, pTwoPassDetectMinarea, pTwoPassBoxcarSize, pTwoPassRejectLevel, pInterpZerosSky, pFitSkySubtractedSurf);
      pSkySubtractMethod.addGrayComponents("rough", pSkyOffsourceMethod, pSkyOffsourceRange, pSkyOffsourceManual, pSkyNebFileRange, pTwoPassObjectMasking, pTwoPassDetectThresh, pTwoPassDetectMinarea, pTwoPassBoxcarSize, pTwoPassRejectLevel, pInterpZerosSky);
      pSkySubtractMethod.addGrayComponents("remove_objects", pSkyOffsourceMethod, pSkyOffsourceRange, pSkyOffsourceManual, pSkyNebFileRange);
      pSkySubtractMethod.addGrayComponents("offsource", pSkyNebFileRange);
      pSkySubtractMethod.addGrayComponents("offsource_extended", pSkyNebFileRange);
      pSkyRejectType.addGrayComponents("none",  pSkyNlow, pSkyNhigh, pSkyLsigma, pSkyHsigma);
      pSkyRejectType.addGrayComponents("avsigclip", pSkyNlow, pSkyNhigh);
      pSkyRejectType.addGrayComponents("sigclip", pSkyNlow, pSkyNhigh);
      pSkyRejectType.addGrayComponents("minmax",  pSkyLsigma, pSkyHsigma);
      pUseSkyFiles.addGrayComponents("All", pSkyFilesRange);
      pTwoPassObjectMasking.addGrayComponents("No", pTwoPassDetectThresh, pTwoPassDetectMinarea, pTwoPassBoxcarSize, pTwoPassRejectLevel);
      pDoAlignStack.addGrayComponents("No", pTwoPassAlignment, pAlignBoxSize, pAlignBoxCenterX, pAlignBoxCenterY, pDrizzleDropsize, pGeomTransCoeffs, pDrizzleKernel, pDrizzleInUnits, pDrizzleOrImcombine, pKeepIndivImages, pStackRejectType, pStackNlow, pStackNhigh, pStackLsigma, pStackHsigma);
      pDrizzleOrImcombine.addGrayComponents("Drizzle", pKeepIndivImages);
      pStackRejectType.addGrayComponents("none",  pStackNlow, pStackNhigh, pStackLsigma, pStackHsigma);
      pStackRejectType.addGrayComponents("avsigclip", pStackNlow, pStackNhigh);
      pStackRejectType.addGrayComponents("sigclip", pStackNlow, pStackNhigh);
      pStackRejectType.addGrayComponents("minmax",  pStackLsigma, pStackHsigma);

      //Add tooltips
      ToolTipManager toolTips = ToolTipManager.sharedInstance();
      toolTips.setDismissDelay(30000);
      String toolTipOptions = "<body bgcolor=\"#f0f0df\" color=\"#0000dd\">";
      ((FATBOYParam)imagingHash.get("FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">files.dat</span><br><br>FILES can be 1) an ASCII text file containing a list of filenames, one per<br>line: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = files.dat</span><br><br>2) a directory name: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = /home/username/data/</span>,<br>in which case all files in the directory will be processed so make sure it<br>contains only FITS files.<br><br>3) a wildcard structure: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = a*.fits</span>.<br><br>4) If you are using the GUI, you can click the Add Files button and use a<br>file browser.<br></body></html>");
      ((FATBOYParam)imagingHash.get("GROUP_OUTPUT_BY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">GROUP_OUTPUT_BY = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">filename</span><br><br>If you are using unique filename prefixes for each object in your dataset<br>to be aligned and stacked then just ignore this paramater.  However,<br>if your filenames all have the same prefix, this parameter allows you<br>to specify which images should be grouped together to be aligned and<br>stacked.  You can specify a FITS keyword to group based on (in which<br>case the output filenames will contain the value of the given FITS<br>keyword) or enter the name of an ASCII text file of the format:<pre><br>input_prefix      start_index     stop_index      output_prefix</pre><br>For example:<pre><br>flam		0001		0005		mrk835<br>flam		6		11		ngc1569</pre><br>The fourth column is optional and if omitted, the output filenames will<br>contain the index of the first image in each group.  Leading and<br>trailing zeros may be given or omitted in the start and stop indices.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SPECIFY_GROUPING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SPECIFY_GROUPING = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If you have set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GROUP_OUTPUT_BY = FITS keyword</span><br>or <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GROUP_OUTPUT_BY = from manual file</span> then<br>specify the FITS keyword or ASCII file using this parameter.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DARK_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DARK_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If the OBS_TYPE keyword in your fits headers are not correctly set to <br>\"dark\" for dark frames, then you can enter a filename an ASCII text file<br>containing a list of filenames (that are also in the main file list above)<br>of dark frames.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If the OBS_TYPE keyword in your fits headers are not correctly set to<br>\"flat\" for flat fields, then you can enter a filename an ASCII text file<br>containing a list of filenames (that are also in the main file list above)<br>of flat fields.<br></body></html>");
      ((FATBOYParam)imagingHash.get("OVERWRITE_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OVERWRITE_FILES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>OVERWRITE_FILES controls the behaviour throughout the program.  If set to<br>no (the default), when a file that the program is about to create already<br>exists, that step is skipped.  For example, if you have been saving all<br>your intermediate files and stop the program after dark subtraction, when<br>you rerun the program it will see that the directory <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>darkSubtracted/</span> exists and each of the files it would create already<br>exist.  It would then skip over that step (and every step before it where<br>intermediate images exist) because the files already exist,<br>saving time.  Note that if you had stopped the program halfway through the<br>dark subtraction so that only half of those files existed, then that step<br>would be skipped for those images that have existing dark subtracted images<br>and performed on those images that did not.  If, however, OVERWRITE_FILES<br>is set to yes, the program will delete any existing files and recreate them.<br></body></html>");
      ((FATBOYParam)imagingHash.get("QUICK_START_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">QUICK_START_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>When rejecting frames (see below), it can take up to a second per image to<br>calculate the median value.  When running several hundred images in batch,<br>this amounts to several minutes.  If you enter a filename for<br>QUICK_START_FILE, then the image filename and median values are written<br>to this file if it doesn't exist and read from them if it does exist,<br>significantly speeding up the initialization.<br></body></html>");
      ((FATBOYParam)imagingHash.get("CLEAN_UP")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CLEAN_UP = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>CLEAN_UP determines whether the files from intermediate steps, such as<br>linearization, dark subtraction, and flat field division, are kept or deleted<br>on the fly (as soon as the next step is done).<br></body></html>");
      ((FATBOYParam)imagingHash.get("PREFIX_DELIMITER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PREFIX_DELIMITER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">.</span><br><br>When multiple objects are processed at once, the pipeline uses the filename<br>prefix to identify images of the same object to group together for sky<br>subtraction and alignment/stacking.  PREFIX_DELIMITER allows you to tell<br>the pipeline what your file naming convention is so that it can group <br>images of the same object together.  For instance, if you name your files<br>orion.0001.fits and specify <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PREFIX_DELIMITER = .</span>,<br>orion would be the prefix.  If you name your files orion_0001.fits, then you<br>would want to specify <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PREFIX_DELIMITER = _</span>.<br>When determining the prefix, the pipeline first strips out the trailing<br>'.fits' and then takes everything before the last occurance of<br>PREFIX_DELIMITER.  Note that PREFIX_DELIMITER can be more than one character<br>even though such naming conventions are uncommon.<br></body></html>");
      ((FATBOYParam)imagingHash.get("EXPTIME_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXPTIME_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXP_TIME</span><br><br>The keyword in the image header where the exposure time is stored.<br></body></html>");
      ((FATBOYParam)imagingHash.get("OBSTYPE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OBSTYPE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">OBS_TYPE</span><br><br>The keyword in the image header where the observation type (object, flat,<br>dark, etc.) is stored.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FILTER_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILTER_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILTER</span><br><br>The keyword in the image header where the filter (J, K, etc.) is stored.<br></body></html>");
      ((FATBOYParam)imagingHash.get("RA_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RA_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">RA</span><br><br>The keyword in the image header where the right ascension is stored.<br>If the RA does not change while dithering and the offset is stored<br>in another keyword, use that keyword here.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DEC_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEC_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DEC</span><br><br>The keyword in the image header where the declination is stored.<br>If the Dec does not change while dithering and the offset is stored<br>in another keyword, use that keyword here.<br></body></html>");
      ((FATBOYParam)imagingHash.get("RELATIVE_OFFSET_ARCSEC")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RELATIVE_OFFSET_ARCSEC = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Set this to yes if you are using a Gemini instrument, where the<br>relative offsets (in arcsec) from the object's RA and DEC are stored<br>in separate keywords when dithering.<br></body></html>");
      ((FATBOYParam)imagingHash.get("PIXSCALE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PIXSCALE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PIXSCALE</span><br><br>The keyword in the image header where the pixel scale is stored.<br>Or if the PIXSCALE keyword is not in your header, you may specify a value<br>to be used instead.<br></body></html>");
      ((FATBOYParam)imagingHash.get("PIXSCALE_UNITS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PIXSCALE_UNITS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">arcsec | degrees</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">arcsec</span><br><br>Whether the pixel scale is in degrees or arcsec.<br></body></html>");
      ((FATBOYParam)imagingHash.get("ROT_PA_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ROT_PA_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ROT_PA</span><br><br>The keyword in the image header where the rotation angle is stored. <br>Or if the ROT_PA keyword is not in your header, you may specify a value to<br>be used instead.<br></body></html>");
      ((FATBOYParam)imagingHash.get("UT_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">UT_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">UTC</span><br><br>The keyword in the image header where the observation time is stored. <br></body></html>");
      ((FATBOYParam)imagingHash.get("DATE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DATE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DATE-OBS</span><br><br>The keyword in the image header where the observation date is stored.<br></body></html>");
      ((FATBOYParam)imagingHash.get("MIN_FRAME_VALUE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MIN_FRAME_VALUE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MIN_FRAME_VALUE specifies a minimum threshold value for the median level<br>of an image.  If the median pixel in the image has a value below this<br>threshold, then it is regarded as a bad exposure and thrown out before any<br>processing is done.  The default is left blank, which translates to a minimum<br>of 0.  <i>Note that this minimum value will not apply to any dark frames.<br>Darks are identified from the image header and will never be rejected in<br>this step, allowing you to set a threshold of 10,000 counts for instance,<br>without worrying that you'd throw away all your darks.</i><br></body></html>");
      ((FATBOYParam)imagingHash.get("MAX_FRAME_VALUE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MAX_FRAME_VALUE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MAX_FRAME_VALUE similarly specifies a maximum threshold value for the median<br>level of an image.  If the median pixel in the image has a value above this<br>threshold, then it is regarded as a bad exposure and thrown out before any<br>processing is done.  The default is left blank, which translates to a maximum<br>of 0.  When the maximum is set to 0, the median is not even computed for<br>comparison and nothing is rejected.<br></body></html>");
      ((FATBOYParam)imagingHash.get("IGNORE_FIRST_FRAMES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IGNORE_FIRST_FRAMES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If IGNORE_FIRST_FRAMES is set to yes, then the first frame of each object<br>at the same exposure time is thrown out before any processing is done.  This<br>flag is useful if the first frame could generally contain bad data.  The<br>first frame is detected from the filename prefix (see PREFIX_DELIMITER above)<br>and the exposure time stored in the header (using the EXPTIME_KEYWORD set<br>above).  Note that the first dark frame for a given exposure time and the<br>first flat field for each given prefix and exposure time will be thrown out<br>in addition to the first object for each given prefix and exposure time.<br><br><br></body></html>");
      ((FATBOYParam)imagingHash.get("IGNORE_AFTER_BAD_READ")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IGNORE_AFTER_BAD_READ = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If IGNORE_AFTER_BAD_READ is set to yes and MAX_FRAME_VALUE is defined,<br>then not only will a bad frame with a median value higher than MAX_FRAME_VALUE<br>thrown out, but the next frame will also be thrown out.<br></body></html>");
      ((FATBOYParam)imagingHash.get("MEF_EXTENSION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MEF_EXTENSION = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MEF_EXTENSION specifies the extension containing your data<br>if you are processing multi-extension FITS data.<br>Leave blank or enter -1 for autodetection of the<br>first extension containing data.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_LINEARIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_LINEARIZE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to apply the linearity correction.  If DO_LINEARIZE is set<br>to no, then no linearity correction is applied, no intermediate images are<br>created, and it moves on to the next step without doing anything.<br></body></html>");
      ((FATBOYParam)imagingHash.get("LINEARITY_COEFFS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LINEARITY_COEFFS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>A whitespace separated list of the coefficients for the nth order polynomial<br>used to apply the linearity correction.  There is no 0th order term so the<br>first term listed is the coefficient for the first order polynomial term.<br>Thus, the default value of 1 means that a correction will be applied but the<br>data will not be changed at all (y = 1*x).  Example: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>LINEARITY_COEFFS = 0.8 0.1 0.03</span> results in a correction of <br>y = 0.8*x + 0.1*x^2 + 0.03*x^3 being applied.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_DARK_COMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_DARK_COMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to combine your darks into master darks.  If set to no,<br>this step is skipped.  Note that if you have already gone past this step<br>and then stopped, you can rerun the pipeline with the same file list and<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">OVERWRITE_FILES = no</span> and this step will be skipped<br>anyway for every master dark that already exists.  In this way, there is<br>no need to modify your file list or parameter file.  The same holds true<br>for all other steps. <br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_DARK_SUBTRACT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_DARK_SUBTRACT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform dark subtraction on all of your other images.  If <br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DEFAULT_MASTER_DARK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_MASTER_DARK = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify filename(s) for default master dark(s) to be used if<br>the program can not find a matching master dark corresponding to an<br>object's exposure time.  This can be a single filename, a comma or space<br>separated list of filenames, or an '.dat'  ASCII text file containing<br>a list of FITS files, one per line.  By default it is left blank, which<br>means the user will be prompted for a master dark filename if the program<br>can not find<br>one.<br></body></html>");
      ((FATBOYParam)imagingHash.get("PROMPT_FOR_MISSING_DARK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PROMPT_FOR_MISSING_DARK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>If there is no matching master dark or default master dark corresponding<br>to an object's exposure time, the default behavior is to prompt the user<br>for a file to use as a master dark.  If this flag is set to no, then<br>instead of prompting the user, the program automatically selects the<br>master dark (from either the list of ones it has created or from the list<br>of default master darks) with the exposure time nearest to that of the<br>object.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_FLAT_COMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_FLAT_COMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to combine your flats into master flats.  If this is set to<br>no, this step is skipped. <br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_TYPE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">dome | sky | twilight | file.dat</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">dome</span><br><br>The type of flat field can be one of four things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_TYPE = dome</span>: used for dome flats, either<br>lamp on-lamp off or lamp on (see below).<br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_TYPE = sky</span>: used for sky flats.  The<br>images themselves are used as flats.  Each set of images with the same filter<br>is combined to produce a master flat using IRAF's<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.  Each individual flat is scaled by<br>the reciprocal of its median pixel value and then the scaled individual<br>flats are median combined to produce the master flat.  For sky flats only,<br>you can use several keywords (see below) to set the rejection type and<br>criteria for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.  The master flat is then normalized by dividing by<br>its median pixel value.<br><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_TYPE = twilight</span>: used for twilight flats.<br>There must be at least two twilight flats for a given filter and exposure<br>time for a master flat to be created, otherwise a warning message is printed<br>and the program continues.  For n flats, n-1 temporary files are created,<br>each of which is the absolute value of the difference of flat i and flat i+1<br>(i.e., abs(flat1-flat2), abs(flat2-flat3), and so on).  These temporary files<br>are then combined to produce a master flat using <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.  Each individual<br>temporary file (difference between two flats) is scaled by the reciprocal<br>of its median pixel value and then the scaled individual images are median<br>combined to produce the master flat.  The master flat is then normalized by<br>dividing by its median pixel value.<br><br>4) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_TYPE = file.dat</span>: where file.dat is an<br>ASCII text file containing a list the type of flats for each filter.  The<br>first column in the text file is the filter (e.g. H, K, JH).  The second<br>column is the type of flat (dome, sky, or twilight).  And in the case of<br>dome flats, there should be a third column that specifies on or on-off.<br>For example:<pre><br>H       sky<br>K       dome    on-off<br></pre><br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_METHOD_DOME")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_METHOD_DOME = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">on-off | on</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">on-off</span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_TYPE = dome</span> then you must also specify<br>the method of dome flats used: lamp on - lamp off or just lamp on.<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD_DOME = on-off</span>: used when dome<br>flats have been taken with both the lamp on and the lamp off.  For each set<br>of flats with the same filter and exposure time, the lamp on flats are<br>combined to produce the master lamp on flat, then the lamp off flats are<br>combined to produce the master lamp off flat.  Finally, the master lamp<br>off flat is subtracted from the master lamp on flat to produce the master<br>flat (and the temporary files with the master lamp on and master lamp off<br>are destroyed).  Just like with the other types of flats, <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> is used<br>to combine the images and each flat is scaled by the reciprocal of its<br>median pixel value then the scaled individual flats are median combined to<br>produce the master (lamp on or lamp off) flat.  See below for how lamp ons<br>are differentiated from lamp offs.  The master flat is then normalized by<br>dividing by its median pixel value.<br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD_DOME = on</span>: used when dome flats<br>have been taken just with the lamp on.  Each set of flats with the same<br>filter and exposure time is combined to produce a master flat using IRAF's<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.  Each individual flat is scaled by the reciprocal of its median<br>pixel value and then the scaled individual flats are median combined to<br>produce the master flat.  The master flat is then normalized by dividing<br>by its median pixel value.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_LAMP_OFF_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_LAMP_OFF_FILES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">off</span><br><br>When using lamp on - lamp off dome flats, the FLAT_LAMP_OFF_FILES flag allows<br>you to specify which files are lamp off versus which are lamp on (since the<br>OBS_TYPE keyword in the header will just say flat).  There are two ways to<br>differentiate the lamp off flats from the lamp on flats: by a naming convention<br>or by specifying them in a separate ASCII text file.  If the value of<br>FLAT_LAMP_OFF_FILES contains '.dat' (i.e., lampoff.dat), this file should<br>have the format:<pre><br>prefix		start number		stop number</pre><br>For example:<pre><br>flat		11		20</pre><br>would use files named flat.*.fits, where * is between 11 and 20 (or 011 and<br>020, etc.), inclusive, as lamp off flats and all other flat fields for the<br>same filter as lamp on flats.  The number of leading zeros does not matter.<br>The file can have as many lines as necessary of the same format to specify<br>all lamp off flats.  The other option is to use a naming convention in the<br>filenames to specify lamp off flats.  This is the default behavior.  If<br>there is no '.dat' in FLAT_LAMP_OFF_FILES, then it is taken to be a filename<br>fragment that will be found only in lamp off flats.  For instance, if you<br>name your lamp off flats by the nomenclature flat_off.*.fits, you could enter<br>'off' or 'flat_off' for FLAT_LAMP_OFF_FILES.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_SKY_REJECT_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SKY_REJECT_TYPE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">none</span><br><br>When using sky flats, this controls the rejection type to be used by<br>IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.  See the help file for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> for details on options<br>for rejection types.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_SKY_NLOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SKY_NLOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter nlow when<br>using sky flats.  nlow is used for the minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_SKY_NHIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SKY_NHIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter nhigh when<br>using sky flats.  nhigh is used for the minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_SKY_LSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SKY_LSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter lsigma when<br>using sky flats.  lsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FLAT_SKY_HSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SKY_HSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter hsigma when <br>using sky flats.  hsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_BAD_PIXEL_MASK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_BAD_PIXEL_MASK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to created a bad pixel mask.  If this is set to no, this<br>step is skipped.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DEFAULT_BAD_PIXEL_MASK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_BAD_PIXEL_MASK = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify a filename for a default bad pixel mask (a FITS image<br>where bad pixels are 1 and everything else is 0).  If specified, this<br>bad pixel mask will be used instead of creating one(s) from flat fields or<br>darks.  This can also be set to a .dat file containing a list of FITS images<br>to be used as bad pixel masks, one per filter.  The bad pixel masks will<br>be matched up with the correct images by looking at the FILTER keyword in<br>each (specified by <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILTER_KEYWORD</span>.  By default,<br>this is left blank.<br></body></html>");
      ((FATBOYParam)imagingHash.get("BAD_PIXEL_MASK_CLIPPING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_MASK_CLIPPING = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">values | sigma</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">values</span><br><br>This flag specifies whether value thresholds or sigma clipping will be used<br>to determine bad pixels.  For flat fields, especially those that have been<br>normalized, values is recommended.  For darks, sigma clipping is recommended.<br>In values, everything below a lower threshold and above an upper threshold is<br>treated as a bad pixel.  In sigma, all points farther than n*sigma from the<br>mean are thrown out as bad pixels, and then the mean and standard deviation<br>are recalculated for the remaining pixels.  This is done iteratively until<br>the standard deviation converges.  Once this final standard deviation is found,<br>everything more than n*sigma away from the median is treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)imagingHash.get("BAD_PIXEL_HIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_HIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2.0</span><br><br>The high threshold for value clipping.  Anything above this value will be<br>treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)imagingHash.get("BAD_PIXEL_LOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_LOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0.5</span><br><br>The low threshold for value clipping.  Anything below this value will be<br>treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)imagingHash.get("BAD_PIXEL_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>The number of standard deviations away from the median that a point must be<br>to be treated as a bad pixel when sigma clipping is used (i.e., n in <br>n*sigma above).<br></body></html>");
      ((FATBOYParam)imagingHash.get("EDGE_REJECT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EDGE_REJECT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>The number of rows and columns around the edges to be thrown out as bad<br>pixels automatically.  For instance if you have a 2048x2048 image and set<br>this to 5, everything outside of the central 2038x2038 will be treated as<br>bad pixels.<br></body></html>");
      ((FATBOYParam)imagingHash.get("RADIUS_REJECT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RADIUS_REJECT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>A radius outside of which every pixel will be thrown out as bad pixels<br>automatically.  For instance if you have a 2048x2048 image and set this to<br>1000, everything where sqrt((x-1024)^2+(y-1024)^2) &gt; 1000 is rejected.  If<br>set to 0, nothing is rejected.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_FLAT_DIVIDE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_FLAT_DIVIDE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform flat division on all of your other images.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DEFAULT_MASTER_FLAT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_MASTER_FLAT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify a filename for a default master flat to be used if<br>the program can not find a matching master flat corresponding to an<br>object's filter.  By default it is left blank, which means the user<br>will be prompted for a master flat filename if the program can not find<br>one.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_SKY_SUBTRACT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_SKY_SUBTRACT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform sky subtraction on all of your other images.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_SUBTRACT_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_SUBTRACT_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">rough | remove_objects | offsource | offsource_extended<br>| offsource_neb</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">remove_objects</span><br><br>The method of sky subtraction can be one of five things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = rough</span>:<br>This method uses on-source skies, meaning that your images of objects<br>themselves are used as skies.  All other images of the same object<br>(or optionally a range of frames before and after the current one) are <br>scaled by the reciprocals of their median pixel values and median combined<br>using IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> to create a master sky.  For instance, if you have<br>five observations of an obejct, frames 2-5 will be combined to create the<br>master sky for frame 1; frames 1 and 3-5 will be combined to create the<br>master sky for frame 2, and so on.  If for some reason, there is only one<br>frame of an object, a warning message is printed and that object is not<br>processed.  The master sky is scaled by the ratio of the median pixel<br>value of the object frame to the median value of the master sky and then<br>subtracted from the object frame.<br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = remove_objects</span>:<br>This method performs a two-pass sky subtraction.  First, it does everything<br>described above for <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"> rough</span>.  It then runs the<br>program <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span> on each sky subtracted image<br>to detect objects.  These objects are then set to zero, masking them out,<br>in the previous flat divided images, and saved as temporary images.  Then<br>the whole process described above is then repeated with the temporary<br>images: a new master sky will be created for each object.  All other<br>temporary images of the same object are scaled by the recripocals of their<br>median pixel value (excluding where pixels are zero) and median combined<br>using IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> to create a new master sky.  This new master sky<br>is again scaled so that its median pixel value (excluding zeros) matches<br>the median pixel value of the object frame and then subtracted from the<br>object frame.  The temporary images are then destroyed.<br><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource</span>:<br>This method uses off-source skies.  It is used when you have taken<br>separate images for your skies in a different location than your object. <br>This is generally done in the case of an extended object.  It median combines<br>the offsource skies using IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> to create a master offsource<br>sky for each prefix/object/dither run (specified by <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_OFFSOURCE_METHOD</span>, see below).  Once these rough master skies have<br>been created, they are subtracted from the offsource images.  Then<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span> is run on each of these offsource<br>sky subtracted images to mask out objects in the images.  Just like in<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">remove_objects</span> above, a second pass of sky<br>subtraction is performed, with these object masks applied to the offsource<br>images.  IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> is used to median combine them into new master<br>skies.  These new master skies are then subtracted from the on-source images.<br>The median of the master sky is scaled to the median value of each<br>on-source image before subtraction.  The offsource skies are saved as<br>mentioned above.<br><br>4) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_extended</span>:<br>For use when the on-source images contain an extended object that takes up <br>a significant portion (though not most) of the image.  Everything is done<br>exactly as <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">offsource</span> above, up through creating<br>the final master offsource skies.  But because of the extended object,<br>simply calculating the median pixel value of the on-source image will not<br>work correctly for scaling the median value of the offsource skies.  So<br>instead, a method is called (extObjSky), which attempts to iteratively<br>find the proper sky level in the on-source image.  The master offsource<br>sky is then scaled to this level and subtracted.<br><br>5) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_neb</span>:<br>For use when the on-source images contain an extended object that takes up<br>all or most of the image and/or has significant nebulosity.  In this case,<br>there is no way to determine the sky background from the on-source image.<br>Instead a best guess is made by calculating a time weighted average of the<br>sky backgrounds of the offsource images that are closest in time to the<br>on-source image.  This is done by the method nebObjSky.  The master<br>offsource sky is then scaled to the level returned by nebObjSky and<br>subtracted.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_REJECT_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_REJECT_TYPE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sigclip</span><br><br>This controls the rejection type to be used by IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> when creating<br>master skies.  See the help file for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> for details on options<br>for rejection types.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_NLOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_NLOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter nlow when<br>creating master skies. nlow is used for the minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_NHIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_NHIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter nhigh when<br>creating master skies.  nhigh is used for the minmax rejection type by<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_LSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_LSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter lsigma when<br>creating master skies. lsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_HSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_HSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter hsigma when<br>creating master skies. hsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("USE_SKY_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">USE_SKY_FILES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">all | range</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">all</span><br><br>For on source skies, this parameter controls whether all other objects will<br>be combined into the master sky for each individual object, or just a range<br>of objects before and after the current one.  For instance, if you are<br>processing image number 5 out of 10 and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES =<br>all</span>, images 1-4 and 6-10 will be combined to create the master sky<br>for image number 5.  However if you set it to range, only a few selected<br>images contribute to the master sky (see <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_FILES_RANGE<br></span>, below).  This keyword only applies to on-source skies.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_FILES_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_FILES_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES = range</span>, this parameter controls<br>how many files will contribute to each master sky.  Using the example above,<br>if <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES = range</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_FILES_RANGE = 3</span> then for image number 5, images 2-4 and 6-8 will<br>be combined to create the master sky.  For <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_FILES_RANGE = n</span>, it will use the n images before and n after the<br>current frame to create the master sky for that image.  In cases where there<br>are not n before or n after (say image #1 for example), it will use the<br>closest 2*n images (i.e. for image #2, it would use 1 and 3-7 for the master<br>sky).  This keyword only applies to on-source skies.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_DITHERING_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_DITHERING_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>The behavior described above for <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES</span> and<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_FILES_RANGE</span> is not quite accurate because this<br>flag can also influence the behavior.  Say you have 8 images to use for<br>calculating the sky, but 5 of them are at almost exactly the same location.<br>In this case, the stars in those 5 frames would be included in the median<br>image.  Obviously, they shouldn't be, so if two frames are at almost exactly<br>the same location, then one is thrown out.  The threshold as to how close<br>they have to be is given by this parameter, in arcsec.  So the default<br>behavior is that if a frame is within 2 arcsec (in both RA and Dec) of<br>either the previous frame or the current object, it is not used in calculating<br>the sky.  If you want to use all frames regardless, simply set this to a<br>negative number.  This parameter is used by both on-source and off-source<br>skies.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_OFFSOURCE_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto | manual</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>This parameter controls whether offsource skies are automatically detected<br>or specified manually.  For automatic detection, it is assumed that the same<br>prefix is used for both the skies and the objects themselves.  It is also<br>assumed that the first frame is an object and not sky.  If neither of these<br>assumptions is correct for your data, you <i>must</i> use <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>manual</span>.  If you use <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span>, then the<br>offsource skies are detected by having the same prefix but being at least<br>a certain distance away from the first frame.  If you use <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>manual</span>, you will also give it a text file in a specific format that<br>contains information about which frames are skies and which objects they<br>match up with (see below).<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_OFFSOURCE_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">240</span><br><br>When using automatic detection for offsource skies, this parameter controls<br>how far away from the first frame, in arcsec, a frame must be to be considered<br>a sky.  The default value is 240 arcsec (4 arcmin).  So if your objects<br>and skies have the same prefix (i.e., orion.00*.fits), then orion0001.fits<br>will be assumed to be on-source as will every frame within 240 arcsec of<br>orion0001.fits in <i>both RA and Dec</i>.  Every frame orion.00*.fits that<br>is more than 240 arcsec away from orion0001.fits in either RA or Dec will<br>be assumed to be an off-source sky. <br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_OFFSOURCE_MANUAL")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_MANUAL = </span><br><br><br><b>Default value:</b><br><br>When using manual detection for offsource skies, this parameter gives an<br>input ASCII text file that specifies which frames are objects, which are sky,<br>and which skies correspond to which objects.  The file should have this<br>format:<pre><br>Object prefix     start number     stop number     Sky prefix     start number     stop number</pre><br>For example:<pre><br>trap_h		1	42	trap_h_sky	1	20</pre><br>would use files named trap_h.*.fits, where * is between 1 and 42 (with<br>or without leading zeros), inclusive, as objects and treat all files named<br>trap_h_sky.*.fits with * between 1 and 20 as offsource skies.  Another example:<br><pre>orion	1	10	orion	11	20</pre><br>would use orion.*.fits, where * is between 1 and 10 as objects and<br>orion.*.fits, where * is between 11 and 20 as offsource skies.  The master<br>offsource sky created from orion.0011.fits through orion.0020.fits will be<br>subtracted from only orion.0001.fits through orion.0010.fits.  The ASCII<br>text file can have as many lines as necessary of the same format to specify<br>all pairs of objects and offsource skies.<br></body></html>");
      ((FATBOYParam)imagingHash.get("KEEP_SKIES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">KEEP_SKIES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>This parameter controls whether or not skies are saved.  If set to yes,<br>onsource skies are saved in a directory named <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>onsourceSkies/</span> and offsource skies are saved in a<br>directory named <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">offsourceSkies/</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SKY_NEB_FILE_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_NEB_FILE_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>If using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_neb</span>,<br>this parameter controls how many of the offsource skies will be used<br>when creating the time weighted average of the sky background levels in the<br>offsource images.  By default, the 5 offsource skies nearest in time to each<br>on-source image are used.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SEXTRACTOR_PATH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_PATH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sex</span><br><br>This parameter should be set to the full path for the program<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span>.  By default, it is set to just<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sex</span>, meaning that <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>sextractor</span> is in your path.  Otherwise, you could set it to, for<br>example, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">/usr/local/bin/sex</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SEXTRACTOR_CONFIG_PATH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_CONFIG_PATH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sextractor-2.3.2/config</span><br><br>This parameter should be set to the path of the directory containing the<br>sextractor config files you wish to use.  The default assumes you have<br>a directory <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">sextractor-2.3.2/config</span> in your<br>current directory that contains configuration files.  You can set this to<br>any directory, for example <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>/usr/local/sextractor-2.2/config</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("SEXTRACTOR_CONFIG_PREFIX")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_CONFIG_PREFIX = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">default</span><br><br>The prefix for the sextractor config files you wish to use.  These files<br>must be found in the directory given by <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SEXTRACTOR_CONFIG_PATH</span>.  For example, using the default values, the<br>following files must be found in <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">sextractor-2.3.2/config<br></span>: <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">default.sex, default.param, default.conv,<br></span> and <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">default.nnw</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_OBJECT_MASKING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_OBJECT_MASKING = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>This parameter controls whether two passes are used when creating the object<br>masks during sky subtraction.  This option is available for all sky<br>subtraction methods except <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">rough</span>, which of course<br>does no object masking at all.  In two pass object masking, the first pass<br>is to run <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span> on the rough sky subtracted<br>images to pull out and mask the objects, exactly as described above.  Then<br>it runs <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span> again, with a higher threshold<br>that just finds the brightest objects.  It then uses a boxcar smoothing<br>function to grow the mask around these objects in an attempt to further mask<br>out the halos surrounding them.  This second mask is then combined with the<br>first and the combined mask is used.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_DETECT_THRESH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_DETECT_THRESH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">10</span><br><br>When using two pass object masking, this parameter sets the object detection<br>threshold for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span> for the second pass.<br>The detection threshold in the first pass is controlled by your sextractor<br>*.sex config file.  The default value of 10 ensures that only very bright<br>stars will be pulled out in the second pass.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_DETECT_MINAREA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_DETECT_MINAREA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">50</span><br><br>When using two pass object masking, this parameter sets the minimum area<br>threshold to be considered an object by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span><br>for the second pass.  The min area threshold in the first pass is controlled<br>by your sextractor *.sex config file.  The default value of 50 again, ensures<br>that only very bright stars that cover a relatively large area of the image<br>will be pulled out in the second pass.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_BOXCAR_SIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_BOXCAR_SIZE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">51</span><br><br>The size of the boxcar to be used to grow the mask around the objects in the<br>second pass of object masking.  By default, the boxcar is very large, 51<br>pixels, so that small objects will be thrown away and large objects will<br>be grown by a large enough amount to get rid of the halos that surround them.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_REJECT_LEVEL")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_REJECT_LEVEL = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0.95</span><br><br>After the boxcar smoothing function has been applied, every value below this<br>rejection threshold is set to zero, meaning it will be masked out when the<br>master skies are created.  Every value above this will be set to 1, meaning<br>those pixels will be included when creating the master skies.<br></body></html>");
      ((FATBOYParam)imagingHash.get("INTERP_ZEROS_SKY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">INTERP_ZEROS_SKY = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>This parameter applies whether one pass or two pass object masking has been<br>done.  In either case, there is the slight possibility that the masking will<br>result in a few pixels equal to zero in your master sky.  When this parameter<br>is set to yes, an iterative 2-d linear interpolation will be done across<br>all pixels int the master sky that are a) zero and b) not in the bad pixel<br>mask.<br></body></html>");
      ((FATBOYParam)imagingHash.get("REMOVE_COSMIC_RAYS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REMOVE_COSMIC_RAYS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If this parameter is set to <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span>, then cosmic<br>ray removal is done on-the-fly after the sky subtraction and before the<br>sky subtracted image is actually saved.  The cosmic ray removal routine<br>looks at each pixel.  Every pixel that is more than 5 sigma away from the<br>mean value of the 8 pixels surrounding it is replaced by the median of those<br>8 pixels.<br></body></html>");
      ((FATBOYParam)imagingHash.get("COSMIC_RAY_PASSES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">COSMIC_RAY_PASSES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The number of passes to perform if cosmic ray removal is turned on.<br>Generally, three passes is enough to get rid of all cosmic rays.<br></body></html>");
      ((FATBOYParam)imagingHash.get("FIT_SKY_SUBTRACTED_SURF")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FIT_SKY_SUBTRACTED_SURF = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Sometimes, there can still be a small gradient in the background level of<br>sky subtracted images.  If this parameter is set to <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>yes</span>, then IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imsurfit</span><br>will be used to fit a second order<br>legendre polynomial to the background of each sky subtracted image and<br>subtract it off on-the-fly, before the image is saved.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DO_ALIGN_STACK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_ALIGN_STACK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to align and stack your images.  If set to no, this step is<br>skipped.<br></body></html>");
      ((FATBOYParam)imagingHash.get("TWO_PASS_ALIGNMENT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_ALIGNMENT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>When aligning the sky subtracted images, the RA and Dec information in the<br>header is used to obtain a rough guess as to the offsets for each image.<br>These rough guesses are then given to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xregister<br></span>.  However, if the RA and Dec in the header are off by a lot, then<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xregister</span> will not be able to properly align the<br>images.  In this case, a two pass alignment can be done.  The first step<br>is to run sextractor on every frame, creating a catalog of object Xs and<br>Ys.  IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xyxymatch</span> is then used to<br>cross-correlate up to 100 of these in each frame relative to the first frame.<br>The cross-correlation provides a good initial guess as to the offset of<br>each frame relative to the first frame that can replace the erroneous RA<br>and Dec information.<br></body></html>");
      ((FATBOYParam)imagingHash.get("ALIGN_BOX_SIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ALIGN_BOX_SIZE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">256</span><br><br>The size of a box to be used by IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xregister</span><br>to do the image alignment.  The box will be a portion of the image that<br>will be used for cross-correlating the images.  By default, a 256x256 box<br>will be used.<br></body></html>");
      ((FATBOYParam)imagingHash.get("ALIGN_BOX_CENTER_X")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ALIGN_BOX_CENTER_X = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1024</span><br><br>The x-coordinate of the center of the box to be used by IRAF's<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xregister</span> to do the image alignment.<br></body></html>");
      ((FATBOYParam)imagingHash.get("ALIGN_BOX_CENTER_Y")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ALIGN_BOX_CENTER_Y = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1024</span><br><br>The y-coordinate of the center of the box to be used by IRAF's<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xregister</span> to do the image alignment.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DRIZZLE_DROPSIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DRIZZLE_DROPSIZE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0.01</span><br><br>The dropsize used by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle</span> when stacking the<br>images.  This parameter is passed directly to the drizzle parameter<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">pixfrac</span>.  Read the help file on<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle</span> for more information.<br></body></html>");
      ((FATBOYParam)imagingHash.get("GEOM_TRANS_COEFFS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">GEOM_TRANS_COEFFS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This parameter specifies an ASCII text file of the proper format, containing<br>information on the geometric distortion correction to be applied.  The filename<br>will be passsed to <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle</span> using the parameter<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">coeffs</span>.  This file can be generated using<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">geomap.py</span>.  It contains data for polynomial<br>transformations of the form:<br><br>xout = f(xin, yin)<br><br>yout = g(xin, yin)<br><br>The file should have &quot;poly n&quot; as its first line, where n is the<br>order of the polynomials.  Then the coefficients of f should be listed,<br>then a blank line, then the coefficients of g.  The coefficients should be<br>listed from lowest to highest order and xin to yin (i.e., for<br>a + b*x + c*y + d*x^2 + e*x*y + f*y^2... a should be listed first, then b<br>on the next line, c, d, e, f, and so on).  By default this is left blank,<br>meaning no geometric distortion correction is applied.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DRIZZLE_KERNEL")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DRIZZLE_KERNEL = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">point</span><br><br>The kernel shape used by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle</span> when stacking<br>the images.  This parameter is passed directly to the drizzle parameter<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">kernel</span>.  Read the help file on <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>drizzle</span> for more information on kernel shapes.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DRIZZLE_IN_UNITS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DRIZZLE_IN_UNITS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">counts | cps</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">counts</span><br><br>The units in the sky subtracted images that are input to <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>drizzle</span>: either counts or cps (counts per second).  This parameter<br>is passed directly to the drizzle parameter <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">in_un</span>.<br>Almost always, the input units will be <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">counts</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("DRIZZLE_OR_IMCOMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DRIZZLE_OR_IMCOMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">drizzle | imcombine</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">drizzle</span><br><br>This parameter controls the method of stacking the images:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = drizzle</span>:<br>In this method, all the images are simply drizzled together into one<br>final image.  The exposure map is created by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle<br></span> in the process.<br><br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine</span>:<br>The images are each drizzled into a separate, individual image with its<br>own exposure map.  <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> is then used to<br>combine these individual drizzled images into one final image.  The<br>individual exposure maps are then also added up, combined with the<br>exposure map from <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>, which will denote<br>any pixels that were excluded by rejection criteria.  The advantage of<br>this method is that it allows you to specify a rejection criteria to<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>, such as sigma clipping.  However,<br>if cosmic ray removal has been done during the sky subtraction phase, this<br>is probably not necessary and simply drizzling the images together is the<br>best way to go.<br></body></html>");
      ((FATBOYParam)imagingHash.get("KEEP_INDIV_IMAGES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">KEEP_INDIV_IMAGES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine</span>, this<br>parameter controls whether the individual drizzled images and exposure<br>maps are kept or deleted after they are combined into the final aligned<br>stacked images.  If they are kept, they are saved as <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>alignedStacked/driz_filename.fits</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>alignedStacked/exp_filename.fits</span>, respectively.<br></body></html>");
      ((FATBOYParam)imagingHash.get("STACK_REJECT_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">STACK_REJECT_TYPE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sigclip</span><br><br>This controls the rejection type to be used by IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>imcombine</span> when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine<br></span>.  See the help file for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> for<br>details on options for rejection types.<br></body></html>");
      ((FATBOYParam)imagingHash.get("STACK_NLOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">STACK_NLOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as<br>the parameter nlow when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine<br></span>.  nlow is used for the minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("STACK_NHIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">STACK_NHIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as<br>the parameter nhigh when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine<br></span>.  nhigh is used for the minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("STACK_LSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">STACK_LSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as<br>the parameter lsigma when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine<br></span>.  lsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");
      ((FATBOYParam)imagingHash.get("STACK_HSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">STACK_HSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as<br>the parameter hsigma when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DRIZZLE_OR_IMCOMBINE = imcombine<br></span>.  hsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br></body></html>");

      //Quick start panel
      pqsFiles = new FATBOYTextAreaParam("FILES:", 4, 16, "Add Files", "Clear");
      pqsFiles.addBrowse(true);
      pqsFiles.addClear();
      pqsFiles.addTo(imagingPanels[0]);
      pqsFiles.setConstraints(imagingLayout[0], 10, 5, 200, 5, 10, imagingPanels[0], imagingPanels[0], false, false, true);
      pqsFiles.addMirror(pFiles);
      pFiles.addMirror(pqsFiles);

      pqsGroupOutputBy = new FATBOYComboParam("GROUP_OUTPUT_BY:", "filename,fits keyword,from manual file");
      pqsGroupOutputBy.addTo(imagingPanels[0]);
      pqsGroupOutputBy.setConstraints(imagingLayout[0], 12, 5, 200, imagingPanels[1], pqsFiles.spParam);
      pqsGroupOutputBy.addMirror(pGroupOutputBy);
      pGroupOutputBy.addMirror(pqsGroupOutputBy);

      pqsSpecifyGrouping = new FATBOYTextBrowseParam("SPECIFY_GROUPING:", FATBOYLabel.NONE, 16, "Browse");
      pqsSpecifyGrouping.addBrowse();
      pqsSpecifyGrouping.addTo(imagingPanels[0]);
      pqsSpecifyGrouping.setConstraints(imagingLayout[0], 10, 5, 200, 5, imagingPanels[1], pqsGroupOutputBy.cbParam);
      pqsSpecifyGrouping.addMirror(pSpecifyGrouping);
      pSpecifyGrouping.addMirror(pqsSpecifyGrouping);

      pqsDarkFileList = new FATBOYTextAreaParam("DARK_FILE_LIST:", 4, 16, "Add Files", "Clear");
      pqsDarkFileList.addBrowse(true);
      pqsDarkFileList.addClear();
      pqsDarkFileList.addTo(imagingPanels[0]);
      pqsDarkFileList.setConstraints(imagingLayout[0], 14, 5, 200, 5, 10, imagingPanels[0], pqsSpecifyGrouping.tParam);
      pqsDarkFileList.addMirror(pDarkFileList);
      pDarkFileList.addMirror(pqsDarkFileList);

      pqsFlatFileList = new FATBOYTextAreaParam("FLAT_FILE_LIST:", 4, 16, "Add Files", "Clear");
      pqsFlatFileList.addBrowse(true);
      pqsFlatFileList.addClear();
      pqsFlatFileList.addTo(imagingPanels[0]);
      pqsFlatFileList.setConstraints(imagingLayout[0], 12, 5, 200, 5, 10, imagingPanels[0], pqsDarkFileList.spParam);
      pqsFlatFileList.addMirror(pFlatFileList);
      pFlatFileList.addMirror(pqsFlatFileList);

      pqsQuickStartFile = new FATBOYTextBrowseParam("QUICK_START_FILE:", 16, "Browse");
      pqsQuickStartFile.addBrowse();
      pqsQuickStartFile.addTo(imagingPanels[0]);
      pqsQuickStartFile.setConstraints(imagingLayout[0], 12, 5, 200, 5, imagingPanels[0], pqsFlatFileList.spParam);
      pqsQuickStartFile.addMirror(pQuickStartFile);
      pQuickStartFile.addMirror(pqsQuickStartFile);

      pqsMinFrameValue = new FATBOYTextParam("MIN_FRAME_VALUE:", 10);
      pqsMinFrameValue.addTo(imagingPanels[0]);
      pqsMinFrameValue.setConstraints(imagingLayout[0], 10, 5, 200, imagingPanels[0], pqsQuickStartFile.tParam);
      pqsMinFrameValue.addMirror(pMinFrameValue);
      pMinFrameValue.addMirror(pqsMinFrameValue);

      pqsMaxFrameValue = new FATBOYTextParam("MAX_FRAME_VALUE:", 10);
      pqsMaxFrameValue.addTo(imagingPanels[0]);
      pqsMaxFrameValue.setConstraints(imagingLayout[0], 10, 5, 200, imagingPanels[0], pqsMinFrameValue.tParam);
      pqsMaxFrameValue.addMirror(pMaxFrameValue);
      pMaxFrameValue.addMirror(pqsMaxFrameValue);

      pqsDoLinearize = new FATBOYRadioParam("DO_LINEARIZE:", "Yes", "No");
      pqsDoLinearize.addTo(imagingPanels[0]);
      pqsDoLinearize.setConstraints(imagingLayout[0], 12, 5, 200, 15, imagingPanels[0], pqsMaxFrameValue.tParam);
      pqsDoLinearize.addMirror(pDoLinearize);
      pDoLinearize.addMirror(pqsDoLinearize);

      pqsLinearityCoeffs = new FATBOYTextParam("LINEARITY_COEFFS:", 15, "1");
      pqsLinearityCoeffs.addTo(imagingPanels[0]);
      pqsLinearityCoeffs.setConstraints(imagingLayout[0], 5, 25, 200, imagingPanels[0], pqsDoLinearize.yParam);
      pqsLinearityCoeffs.addMirror(pLinearityCoeffs);
      pLinearityCoeffs.addMirror(pqsLinearityCoeffs);

      pqsDoDarkCombine = new FATBOYRadioParam("DO_DARK_COMBINE:", "Yes", "No");
      pqsDoDarkCombine.addTo(imagingPanels[0]);
      pqsDoDarkCombine.setConstraints(imagingLayout[0], 15, 5, 200, 15, imagingPanels[0], pqsLinearityCoeffs.tParam);
      pqsDoDarkCombine.addMirror(pDoDarkCombine);
      pDoDarkCombine.addMirror(pqsDoDarkCombine);

      pqsDoDarkSubtract = new FATBOYRadioParam("DO_DARK_SUBTRACT:", "Yes", "No");
      pqsDoDarkSubtract.addTo(imagingPanels[0]);
      pqsDoDarkSubtract.setConstraints(imagingLayout[0], 10, 5, 200, 15, imagingPanels[0], pqsDoDarkCombine.yParam);
      pqsDoDarkSubtract.addMirror(pDoDarkSubtract);
      pDoDarkSubtract.addMirror(pqsDoDarkSubtract);

      pqsDoFlatCombine = new FATBOYRadioParam("DO_FLAT_COMBINE:", "Yes", "No");
      pqsDoFlatCombine.addTo(imagingPanels[0]);
      pqsDoFlatCombine.setConstraints(imagingLayout[0], 10, 500, 768, 15, imagingPanels[0], imagingPanels[0], false, true);
      pqsDoFlatCombine.addMirror(pDoFlatCombine);
      pDoFlatCombine.addMirror(pqsDoFlatCombine);

      pqsFlatType = new FATBOYComboBrowseParam("FLAT_TYPE:", "dome,sky,twilight,from file", 14, "Browse");
      pqsFlatType.addBrowse();
      pqsFlatType.addTo(imagingPanels[0]);
      pqsFlatType.setConstraints(imagingLayout[0], 5, 525, 768, 12, 768, 5, imagingPanels[0], pqsDoFlatCombine.yParam);
      pqsFlatType.addMirror(pFlatType);
      pFlatType.addMirror(pqsFlatType);

      pqsDoFlatDivide = new FATBOYRadioParam("DO_FLAT_DIVIDE:", "Yes", "No");
      pqsDoFlatDivide.addTo(imagingPanels[0]);
      pqsDoFlatDivide.setConstraints(imagingLayout[0], 12, 500, 768, 15, imagingPanels[0], pqsFlatType.tParam); 
      pqsDoFlatDivide.addMirror(pDoFlatDivide);
      pDoFlatDivide.addMirror(pqsDoFlatDivide);

      pqsDoBadPixelMask = new FATBOYRadioParam("DO_BAD_PIXEL_MASK:", "Yes", "No");
      pqsDoBadPixelMask.addTo(imagingPanels[0]);
      pqsDoBadPixelMask.setConstraints(imagingLayout[0], 12, 500, 768, 15, imagingPanels[0], pqsDoFlatDivide.yParam);
      pqsDoBadPixelMask.addMirror(pDoBadPixelMask);
      pDoBadPixelMask.addMirror(pqsDoBadPixelMask); 

      pqsRemoveCosmicRays = new FATBOYRadioParam("REMOVE_COSMIC_RAYS:", "Yes","No");
      pqsRemoveCosmicRays.addTo(imagingPanels[0]);
      pqsRemoveCosmicRays.setConstraints(imagingLayout[0], 12, 500, 768, 15, imagingPanels[0], pqsDoBadPixelMask.yParam);
      pqsRemoveCosmicRays.addMirror(pRemoveCosmicRays);
      pRemoveCosmicRays.addMirror(pqsRemoveCosmicRays);

      pqsDoSkySubtract = new FATBOYRadioParam("DO_SKY_SUBTRACT:", "Yes", "No");
      pqsDoSkySubtract.addTo(imagingPanels[0]);
      pqsDoSkySubtract.setConstraints(imagingLayout[0], 12, 500, 768, 15, imagingPanels[0], pqsRemoveCosmicRays.yParam); 
      pqsDoSkySubtract.addMirror(pDoSkySubtract);
      pDoSkySubtract.addMirror(pqsDoSkySubtract);

      pqsSkySubtractMethod = new FATBOYComboParam("SKY_SUBTRACT_METHOD:", "rough,remove_objects,offsource,offsource_extended,offsource_neb");
      pqsSkySubtractMethod.addTo(imagingPanels[0]);
      pqsSkySubtractMethod.setConstraints(imagingLayout[0], 5, 525, 768, imagingPanels[0], pqsDoSkySubtract.yParam);
      pqsSkySubtractMethod.addMirror(pSkySubtractMethod);
      pSkySubtractMethod.addMirror(pqsSkySubtractMethod);

      pqsUseSkyFiles = new FATBOYRadioParam("USE_SKY_FILES:", "All", "Range");
      pqsUseSkyFiles.addTo(imagingPanels[0]);
      pqsUseSkyFiles.setConstraints(imagingLayout[0], 5, 525, 768, 15, imagingPanels[0], pqsSkySubtractMethod.cbParam);
      pqsUseSkyFiles.addMirror(pUseSkyFiles);
      pUseSkyFiles.addMirror(pqsUseSkyFiles);

      pqsTwoPassObjectMasking = new FATBOYRadioParam("TWO_PASS_OBJECT_MASKING:", "Yes", "No", false);
      pqsTwoPassObjectMasking.addTo(imagingPanels[0]);
      pqsTwoPassObjectMasking.setConstraints(imagingLayout[0], 5, 525, 768, 15, imagingPanels[0], pqsUseSkyFiles.yParam); 
      pqsTwoPassObjectMasking.addMirror(pTwoPassObjectMasking);
      pTwoPassObjectMasking.addMirror(pqsTwoPassObjectMasking);

      pqsDoAlignStack = new FATBOYRadioParam("DO_ALIGN_STACK:", "Yes", "No");
      pqsDoAlignStack.addTo(imagingPanels[0]);
      pqsDoAlignStack.setConstraints(imagingLayout[0], 15, 500, 768, 15, imagingPanels[0], pqsTwoPassObjectMasking.yParam);
      pqsDoAlignStack.addMirror(pDoAlignStack);
      pDoAlignStack.addMirror(pqsDoAlignStack);

      pqsAlignBoxSize = new FATBOYTextParam("ALIGN_BOX_SIZE:", 10, "256");
      pqsAlignBoxSize.addTo(imagingPanels[0]);
      pqsAlignBoxSize.setConstraints(imagingLayout[0], 5, 525, 768, imagingPanels[0], pqsDoAlignStack.yParam);
      pqsAlignBoxSize.addMirror(pAlignBoxSize);
      pAlignBoxSize.addMirror(pqsAlignBoxSize);

      pqsAlignBoxCenterX = new FATBOYTextParam("ALIGN_BOX_CENTER_X:", 10, "1024");
      pqsAlignBoxCenterX.addTo(imagingPanels[0]);
      pqsAlignBoxCenterX.setConstraints(imagingLayout[0], 5, 525, 768, imagingPanels[0], pqsAlignBoxSize.tParam);
      pqsAlignBoxCenterX.addMirror(pAlignBoxCenterX);
      pAlignBoxCenterX.addMirror(pqsAlignBoxCenterX);

      pqsAlignBoxCenterY = new FATBOYTextParam("ALIGN_BOX_CENTER_Y:", 10, "1024");
      pqsAlignBoxCenterY.addTo(imagingPanels[0]);
      pqsAlignBoxCenterY.setConstraints(imagingLayout[0], 5, 525, 768, imagingPanels[0], pqsAlignBoxCenterX.tParam);
      pqsAlignBoxCenterY.addMirror(pAlignBoxCenterY);
      pAlignBoxCenterY.addMirror(pqsAlignBoxCenterY);

      pqsGeomTransCoeffs = new FATBOYTextBrowseParam("GEOM_TRANS_COEFFS:", 14, "Browse");
      pqsGeomTransCoeffs.addBrowse();
      pqsGeomTransCoeffs.addTo(imagingPanels[0]);
      pqsGeomTransCoeffs.setConstraints(imagingLayout[0], 5, 525, 768, 5, imagingPanels[0], pqsAlignBoxCenterY.tParam);
      pqsGeomTransCoeffs.addMirror(pGeomTransCoeffs);
      pGeomTransCoeffs.addMirror(pqsGeomTransCoeffs);

      //add graying options for quickstart panel
      pqsGroupOutputBy.addGrayComponents("filename",pqsSpecifyGrouping);
      pqsDoLinearize.addGrayComponents("No",pqsLinearityCoeffs);
      pqsDoFlatCombine.addGrayComponents("No", pqsFlatType);
      pqsDoSkySubtract.addGrayComponents("No", pqsSkySubtractMethod, pqsUseSkyFiles, pqsTwoPassObjectMasking);
      pqsSkySubtractMethod.addGrayComponents("rough", pqsTwoPassObjectMasking);
      pqsDoAlignStack.addGrayComponents("No", pqsAlignBoxSize, pqsAlignBoxCenterX, pqsAlignBoxCenterY, pqsGeomTransCoeffs);

      imagingPane.setFont(new Font("Dialog",Font.BOLD,16));
      imagingPane.addTab("Quick Start", null, imagingPanels[0], "Commonly changed options");
      imagingPane.addTab("Initialization", null, imagingPanels[1], "Setup preprocessing options");
      imagingPane.addTab("Linearity & Darks", null, imagingPanels[2], "Set options for linearity correction and combination and subtraction of dark frames");
      imagingPane.addTab("Flats", null, imagingPanels[3], "Set options for combination and division of flat fields");
      imagingPane.addTab("Bad Pixels", null, imagingPanels[4], "Set options for creating bad pixel masks and rejecting cosmic rays");
      imagingPane.addTab("Sky Subtraction", null, imagingPanels[5], "Set options for sky subtraction");
      imagingPane.addTab("Alignment & Stacking", null, imagingPanels[6], "Set options for alignment and stacking of frames");


      //Setup Astrometry Pane
      astromPanels = new JPanel[1];
      SpringLayout[] astromLayout = new SpringLayout[1];
      for (int j = 0; j < 1; j++) {
	astromPanels[j] = new JPanel();
        astromPanels[j].setPreferredSize(new Dimension(xdim,ydim-250));
	astromLayout[j] = new SpringLayout();
	astromPanels[j].setLayout(astromLayout[j]);
      }

      //Astrometry Initialization
      astromHash = new LinkedHashMap(40);
      aFiles = new FATBOYTextAreaParam("FILES:", FATBOYLabel.RYBEVEL, 5, 20, "Add Files", "Clear");
      aFiles.addBrowse(true);
      aFiles.addClear();
      aFiles.addFocusBorder();
      aFiles.addTo(astromPanels[0]);
      aFiles.setConstraints(astromLayout[0], 10, 5, 180, 5, 10, astromPanels[0], astromPanels[0], false, false, true);
      astromHash.put("FILES",aFiles);

      aOverwriteFiles = new FATBOYRadioParam("OVERWRITE_FILES:", FATBOYLabel.NONE, "Yes", "No", false);
      aOverwriteFiles.addTo(astromPanels[0]);
      aOverwriteFiles.setConstraints(astromLayout[0], 15, 5, 180, 15, astromPanels[0], aFiles.spParam);
      astromHash.put("OVERWRITE_FILES", aOverwriteFiles);

      aFilterKeyword = new FATBOYTextParam("FILTER_KEYWORD:", FATBOYLabel.NONE, 10, "FILTER");
      aFilterKeyword.addTo(astromPanels[0]);
      aFilterKeyword.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aOverwriteFiles.yParam);
      astromHash.put("FILTER_KEYWORD", aFilterKeyword);

      aRAKeyword = new FATBOYTextParam("RA_KEYWORD:", FATBOYLabel.NONE, 10, "RA");
      aRAKeyword.addTo(astromPanels[0]);
      aRAKeyword.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aFilterKeyword.tParam);
      astromHash.put("RA_KEYWORD", aRAKeyword);

      aDecKeyword = new FATBOYTextParam("DEC_KEYWORD:", FATBOYLabel.NONE, 10, "DEC");
      aDecKeyword.addTo(astromPanels[0]);
      aDecKeyword.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aRAKeyword.tParam);
      astromHash.put("DEC_KEYWORD", aDecKeyword);

      aPixscaleKeyword = new FATBOYTextParam("PIXSCALE_KEYWORD:", FATBOYLabel.NONE, 10, "PIXSCALE");
      aPixscaleKeyword.addTo(astromPanels[0]);
      aPixscaleKeyword.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aDecKeyword.tParam);
      astromHash.put("PIXSCALE_KEYWORD", aPixscaleKeyword);

      aPixscaleUnits = new FATBOYRadioParam("PIXSCALE_UNITS:", FATBOYLabel.YGBEVEL, "Arcsec", "Degrees");
      aPixscaleUnits.addFocusBorder();
      aPixscaleUnits.addTo(astromPanels[0]);
      aPixscaleUnits.setConstraints(astromLayout[0], 12, 5, 180, 15, astromPanels[0], aPixscaleKeyword.tParam);
      astromHash.put("PIXSCALE_UNITS", aPixscaleUnits);

      aRotPAKeyword = new FATBOYTextParam("ROT_PA_KEYWORD:", FATBOYLabel.NONE, 10, "ROT_PA");
      aRotPAKeyword.addTo(astromPanels[0]);
      aRotPAKeyword.setConstraints(astromLayout[0], 10, 5, 180, astromPanels[0], aPixscaleUnits.yParam);
      astromHash.put("ROT_PA_KEYWORD", aRotPAKeyword);

      aSextractorPath = new FATBOYTextBrowseParam("SEXTRACTOR_PATH:", 16, "Browse", "sex");
      aSextractorPath.addBrowse();
      aSextractorPath.addTo(astromPanels[0]);
      aSextractorPath.setConstraints(astromLayout[0], 12, 5, 260, 5, astromPanels[0], aRotPAKeyword.tParam); 
      astromHash.put("SEXTRACTOR_PATH", aSextractorPath);

      aSextractorConfigPath = new FATBOYTextBrowseParam("SEXTRACTOR_CONFIG_PATH:", 16, "Browse", "sextractor-2.3.2/config");
      aSextractorConfigPath.addDirBrowse();
      aSextractorConfigPath.addTo(astromPanels[0]);
      aSextractorConfigPath.setConstraints(astromLayout[0], 12, 5, 260, 5, astromPanels[0], aSextractorPath.tParam);
      astromHash.put("SEXTRACTOR_CONFIG_PATH", aSextractorConfigPath);

      aSextractorConfigPrefix = new FATBOYTextParam("SEXTRACTOR_CONFIG_PREFIX:", 16, "default");
      aSextractorConfigPrefix.addTo(astromPanels[0]);
      aSextractorConfigPrefix.setConstraints(astromLayout[0], 12, 5, 260, astromPanels[0], aSextractorConfigPath.tParam);
      astromHash.put("SEXTRACTOR_CONFIG_PREFIX", aSextractorConfigPrefix);

      aCatalog = new FATBOYComboParam("CATALOG:", FATBOYLabel.YGBEVEL, "auto,2mass,usno", "auto");
      aCatalog.addFocusBorder();
      aCatalog.addTo(astromPanels[0]);
      aCatalog.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aSextractorConfigPrefix.tParam);
      astromHash.put("CATALOG", aCatalog);

      aRadius = new FATBOYTextParam("RADIUS:", FATBOYLabel.YGBEVEL, 8, "15");
      aRadius.addFocusBorder();
      aRadius.addTo(astromPanels[0]);
      aRadius.setConstraints(astromLayout[0], 12, 5, 180, astromPanels[0], aCatalog.cbParam);
      astromHash.put("RADIUS", aRadius);

      aNObjectsImage = new FATBOYTextParam("N_OBJECTS_IMAGE:", FATBOYLabel.YGBEVEL, 8, "100");
      aNObjectsImage.addFocusBorder();
      aNObjectsImage.addTo(astromPanels[0]);
      aNObjectsImage.setConstraints(astromLayout[0], 10, 528, 768, astromPanels[0], astromPanels[0], true);
      astromHash.put("N_OBJECTS_IMAGE", aNObjectsImage);

      aNObjectsCatalog = new FATBOYTextParam("N_OBJECTS_CATALOG:", FATBOYLabel.YGBEVEL, 8, "100");
      aNObjectsCatalog.addFocusBorder();
      aNObjectsCatalog.addTo(astromPanels[0]);
      aNObjectsCatalog.setConstraints(astromLayout[0], 12, 528, 768, astromPanels[0], aNObjectsImage.tParam);
      astromHash.put("N_OBJECTS_CATALOG", aNObjectsCatalog);

      aXYXYMatchTolerance = new FATBOYTextParam("XYXYMATCH_TOLERANCE:", 8, "3.0");
      aXYXYMatchTolerance.addTo(astromPanels[0]);
      aXYXYMatchTolerance.setConstraints(astromLayout[0], 12, 528, 768, astromPanels[0], aNObjectsCatalog.tParam);
      astromHash.put("XYXYMATCH_TOLERANCE", aXYXYMatchTolerance);

      aXYXYMatchNmatch = new FATBOYTextParam("XYXYMATCH_NMATCH:", 8, "30");
      aXYXYMatchNmatch.addTo(astromPanels[0]);
      aXYXYMatchNmatch.setConstraints(astromLayout[0], 10, 528, 768, astromPanels[0], aXYXYMatchTolerance.tParam);
      astromHash.put("XYXYMATCH_NMATCH", aXYXYMatchNmatch);

      aUpdateRADec = new FATBOYRadioParam("UPDATE_RA_DEC:", "Yes", "No");
      aUpdateRADec.addTo(astromPanels[0]);
      aUpdateRADec.setConstraints(astromLayout[0], 10, 528, 768, 15, astromPanels[0], aXYXYMatchNmatch.tParam); 
      astromHash.put("UPDATE_RA_DEC", aUpdateRADec);

      aDoPlateSolution = new FATBOYRadioParam("DO_PLATE_SOLUTION:", "Yes", "No");
      aDoPlateSolution.addTo(astromPanels[0]);
      aDoPlateSolution.setConstraints(astromLayout[0], 10, 528, 768, 15, astromPanels[0], aUpdateRADec.yParam); 
      astromHash.put("DO_PLATE_SOLUTION", aDoPlateSolution);

      aTwoPassSolution = new FATBOYRadioParam("TWO_PASS_SOLUTION:", FATBOYLabel.YGBEVEL, "Yes", "No");
      aTwoPassSolution.addFocusBorder();
      aTwoPassSolution.addTo(astromPanels[0]);
      aTwoPassSolution.setConstraints(astromLayout[0], 10, 528, 768, 15, astromPanels[0], aDoPlateSolution.yParam);
      astromHash.put("TWO_PASS_SOLUTION", aTwoPassSolution);

      aMatchingType = new FATBOYRadioParam("MATCHING_TYPE:", "tolerance", "triangles");
      aMatchingType.addTo(astromPanels[0]);
      aMatchingType.setConstraints(astromLayout[0], 10, 528, 768, 15, astromPanels[0], aTwoPassSolution.yParam);
      astromHash.put("MATCHING_TYPE", aMatchingType);

      aCCXYMatchTolerance = new FATBOYTextParam("CCXYMATCH_TOLERANCE:", 8, "2.0");
      aCCXYMatchTolerance.addTo(astromPanels[0]);
      aCCXYMatchTolerance.setConstraints(astromLayout[0], 10, 528, 768, astromPanels[0], aMatchingType.yParam);
      astromHash.put("CCXYMATCH_TOLERANCE", aCCXYMatchTolerance);

      aDefaultPlateSolution = new FATBOYTextBrowseParam("DEFAULT_PLATE_SOLUTION:", 14, "Browse");
      aDefaultPlateSolution.addBrowse();
      aDefaultPlateSolution.addTo(astromPanels[0]);
      aDefaultPlateSolution.setConstraints(astromLayout[0], 10, 528, 768, 5, astromPanels[0], aCCXYMatchTolerance.tParam);
      astromHash.put("DEFAULT_PLATE_SOLUTION", aDefaultPlateSolution);

      //Add graying options
      aDoPlateSolution.addGrayComponents("Yes", aDefaultPlateSolution);
      aDoPlateSolution.addGrayComponents("No", aTwoPassSolution, aMatchingType, aCCXYMatchTolerance);
      aTwoPassSolution.addGrayComponents("No", aMatchingType, aCCXYMatchTolerance);

      //Add Tooltips
      ((FATBOYParam)astromHash.get("FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">files.dat</span><br><br>FILES can be 1) an ASCII text file containing a list of filenames, one per<br>line: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = files.dat</span><br><br>2) a directory name: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = /home/username/data/</span>,<br>in which case all files in the directory will be processed so make sure it<br>contains only FITS files.<br><br>3) a wildcard structure: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = a*.fits</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("OVERWRITE_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OVERWRITE_FILES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>OVERWRITE_FILES controls the behaviour throughout the program.  If set to<br>no (the default), when a file that the program is about to create already<br>exists, that step is skipped.  If, however, OVERWRITE_FILES<br>is set to yes, the program will delete any existing files and recreate them.<br></body></html>");
      ((FATBOYParam)astromHash.get("FILTER_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILTER_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILTER</span><br><br>The keyword in the image header where the filter (J, K, etc.) is stored.<br></body></html>");
      ((FATBOYParam)astromHash.get("RA_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RA_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">RA</span><br><br>The keyword in the image header where the right ascension is stored.<br></body></html>");
      ((FATBOYParam)astromHash.get("DEC_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEC_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DEC</span><br><br>The keyword in the image header where the declination is stored.<br></body></html>");
      ((FATBOYParam)astromHash.get("PIXSCALE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PIXSCALE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PIXSCALE</span><br><br>The keyword in the image header where the pixel scale is stored.<br>Or if the PIXSCALE keyword is not in your header, you may specify a value<br>to be used instead.<br></body></html>");
      ((FATBOYParam)astromHash.get("PIXSCALE_UNITS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PIXSCALE_UNITS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">arcsec | degrees</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">arcsec</span><br><br>Whether the pixel scale is in degrees or arcsec.<br></body></html>");
      ((FATBOYParam)astromHash.get("ROT_PA_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ROT_PA_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ROT_PA</span><br><br>The keyword in the image header where the rotation angle is stored. <br>Or if the ROT_PA keyword is not in your header, you may specify a value to<br>be used instead.<br></body></html>");
      ((FATBOYParam)astromHash.get("SEXTRACTOR_PATH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_PATH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sex</span><br><br>This parameter should be set to the full path for the program<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor</span>.  By default, it is set to just<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sex</span>, meaning that <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>sextractor</span> is in your path.  Otherwise, you could set it to, for<br>example, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">/usr/local/bin/sex</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("SEXTRACTOR_CONFIG_PATH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_CONFIG_PATH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sextractor-2.3.2/config</span><br><br>This parameter should be set to the path of the directory containing the<br>sextractor config files you wish to use.  The default assumes you have<br>a directory <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">sextractor-2.3.2/config</span> in your<br>current directory that contains configuration files.  You can set this to<br>any directory, for example <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>/usr/local/sextractor-2.2/config</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("SEXTRACTOR_CONFIG_PREFIX")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SEXTRACTOR_CONFIG_PREFIX = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">default</span><br><br>The prefix for the sextractor config files you wish to use.  These files<br>must be found in the directory given by <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SEXTRACTOR_CONFIG_PATH</span>.  For example, using the default values, the<br>following files must be found in <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">sextractor-2.3.2/config<br></span>: <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">default.sex, default.param, default.conv,<br></span> and <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">default.nnw</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("CATALOG")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CATALOG = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto | 2mass | usno</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>Which catalog to query.  If set to auto, 2mass is queried first and then,<br>only if no objects are found in 2mass, usno is queried.<br></body></html>");
      ((FATBOYParam)astromHash.get("RADIUS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RADIUS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">15</span><br><br>Radius (in arcmin) from the RA and Dec listed in the image header, with<br>which to query the 2mass and/or usno catalogs.<br></body></html>");
      ((FATBOYParam)astromHash.get("N_OBJECTS_IMAGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">N_OBJECTS_IMAGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">100</span><br><br>The maximum number of objects found in your image by<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">sextractor </span> to be used in the cross-correlation.<br>Note that the objects have already been sorted by descending flux.<br></body></html>");
      ((FATBOYParam)astromHash.get("N_OBJECTS_CATALOG")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">N_OBJECTS_CATALOG = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">100</span><br><br>The maximum number of objects found in the catalog to be used in the<br>cross-correlation.  Note that the objects have already been sorted by<br>descending brightness.<br></body></html>");
      ((FATBOYParam)astromHash.get("XYXYMATCH_TOLERANCE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">XYXYMATCH_TOLERANCE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3.0</span><br><br>The tolerance threshold, in pixels, to be used by IRAF's<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xyxymatch</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("XYXYMATCH_NMATCH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">XYXYMATCH_NMATCH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">30</span><br><br>The maximum number of reference and input  coordinates  used  by the <br>\"triangles\"  pattern  matching  algorithm.  For more details, see the<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xyxymatch</span> documentation.  30 is strongly<br>recommended as a starting value.  If <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">xyxymatch</span><br>does not find any matches, however, you may wish to try 35 or 40, or<br>even a large number such as 65.  Be warned that even at 65, it will take<br>about 10x longer than with 30 (a matter of a couple minutes instead of<br>a few seconds).<br></body></html>");
      ((FATBOYParam)astromHash.get("UPDATE_RA_DEC")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">UPDATE_RA_DEC = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to update the RA and Dec in the image header.<br>If this is set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)astromHash.get("DO_PLATE_SOLUTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_PLATE_SOLUTION = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to calculate a plate solution.  If<br>set to no, this step is skipped and a previously calculated plate solution<br>can be applied in the next step.<br></body></html>");
      ((FATBOYParam)astromHash.get("TWO_PASS_SOLUTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">TWO_PASS_SOLUTION = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to use two passes and a second cross-correlation (by<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ccxymatch</span>) to expand the number of objects used in<br>calculating the plate solution.<br></body></html>");
      ((FATBOYParam)astromHash.get("MATCHING_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MATCHING_TYPE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">tolerance | triangles</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">tolerance</span><br><br>The matching algorithm for <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ccxymatch</span> to use<br>if <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">TWO_PASS_SOLUTION = yes</span>.  See the help file<br>on <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ccxymatch</span> for more details.<br></body></html>");
      ((FATBOYParam)astromHash.get("CCXYMATCH_TOLERANCE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CCXYMATCH_TOLERANCE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2.0</span><br><br>The matching tolerance threshold, in arcsec, to be used by<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ccxymatch</span> if <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">TWO_PASS_SOLUTION =<br>yes</span>.<br></body></html>");
      ((FATBOYParam)astromHash.get("DEFAULT_PLATE_SOLUTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_PLATE_SOLUTION = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DO_PLATE_SOLUTION = no</span>, then this parameter<br>should list a .db file from a plate solution previously calculated using<br>FATBOY (or <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ccmap</span> on its own).  If left blank,<br>the user will be prompted for a filename.  If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>DO_PLATE_SOLUTION = yes</span>, then this paramter will be ignored.<br></body></html>");

      astromPane.setFont(new Font("Dialog",Font.BOLD,16));
      astromPane.addTab("Astrometry Parameters", null, astromPanels[0]);

      //Setup Longslit Rectification Pane
      rectPanels = new JPanel[1];
      SpringLayout[] rectLayout = new SpringLayout[1];
      for (int j = 0; j < 1; j++) {
        rectPanels[j] = new JPanel();
        rectPanels[j].setPreferredSize(new Dimension(xdim,ydim-250));
        rectLayout[j] = new SpringLayout();
        rectPanels[j].setLayout(rectLayout[j]);
      }

      //Rectification Initialization
      rectHash = new LinkedHashMap(40);
      lsSetupList = new FATBOYRadioParam("SETUP_LIST:", FATBOYLabel.NONE, "Yes", "No");
      lsSetupList.addTo(rectPanels[0]);
      lsSetupList.setConstraints(rectLayout[0], 10, 5, 150, 15, rectPanels[0], rectPanels[0], false, true); 
      rectHash.put("SETUP_LIST", lsSetupList);

      lsFileList = new FATBOYTextAreaParam("FILE_LIST:",  FATBOYLabel.RYBEVEL, 5, 20, "Add Files", "Clear");
      lsFileList.addBrowse(true);
      lsFileList.addClear();
      lsFileList.addFocusBorder();
      lsFileList.addTo(rectPanels[0]);
      lsFileList.setConstraints(rectLayout[0], 12, 5, 150, 5, 10, rectPanels[0], lsSetupList.yParam);
      rectHash.put("FILE_LIST",lsFileList);

      lsDarkFrame = new FATBOYTextBrowseParam("DARK_FRAME:", FATBOYLabel.RYBEVEL, 20, "Browse");
      lsDarkFrame.addBrowse();
      lsDarkFrame.addFocusBorder();
      lsDarkFrame.addTo(rectPanels[0]);
      lsDarkFrame.setConstraints(rectLayout[0], 12, 5, 150, 5, rectPanels[0], lsFileList.spParam); 
      rectHash.put("DARK_FRAME", lsDarkFrame);

      lsListOutput = new FATBOYTextBrowseParam("LIST_OUTPUT:", 20, "Browse", "list.out");
      lsListOutput.addBrowse();
      lsListOutput.addTo(rectPanels[0]);
      lsListOutput.setConstraints(rectLayout[0], 12, 5, 150, 5, rectPanels[0], lsDarkFrame.tParam);
      rectHash.put("LIST_OUTPUT", lsListOutput);

      lsDoRectify = new FATBOYRadioParam("DO_RECTIFY:", "Yes", "No");
      lsDoRectify.addTo(rectPanels[0]);
      lsDoRectify.setConstraints(rectLayout[0], 12, 5, 150, 15, rectPanels[0], lsListOutput.tParam); 
      rectHash.put("DO_RECTIFY", lsDoRectify);

      lsListInput = new FATBOYTextBrowseParam("LIST_INPUT:", 20, "Browse");
      lsListInput.addBrowse();
      lsListInput.addTo(rectPanels[0]);
      lsListInput.setConstraints(rectLayout[0], 12, 5, 150, 5, rectPanels[0], lsDoRectify.yParam);
      rectHash.put("LIST_INPUT", lsListInput);

      lsXCenter = new FATBOYTextParam("X_CENTER:", FATBOYLabel.YGBEVEL, 10, "1024");
      lsXCenter.addFocusBorder();
      lsXCenter.addTo(rectPanels[0]);
      lsXCenter.setConstraints(rectLayout[0], 12, 5, 150, rectPanels[0], lsListInput.tParam);
      rectHash.put("X_CENTER", lsXCenter);

      lsFitOrder = new FATBOYTextParam("FIT_ORDER:", 10, "3");
      lsFitOrder.addTo(rectPanels[0]);
      lsFitOrder.setConstraints(rectLayout[0], 12, 5, 150, rectPanels[0], lsXCenter.tParam); 
      rectHash.put("FIT_ORDER", lsFitOrder);

      lsSkyFileList = new FATBOYTextAreaParam("SKY_FILE_LIST:",  FATBOYLabel.RYBEVEL, 5, 20, "Add Files", "Clear");
      lsSkyFileList.addBrowse(true);
      lsSkyFileList.addClear();
      lsSkyFileList.addFocusBorder();
      lsSkyFileList.addTo(rectPanels[0]);
      lsSkyFileList.setConstraints(rectLayout[0], 10, 512, 678, 5, 10, rectPanels[0], rectPanels[0], false, false, true); 
      rectHash.put("SKY_FILE_LIST",lsSkyFileList);

      lsSkyDarkFrame = new FATBOYTextBrowseParam("SKY_DARK_FRAME:", FATBOYLabel.RYBEVEL, 20, "Browse");
      lsSkyDarkFrame.addBrowse();
      lsSkyDarkFrame.addFocusBorder();
      lsSkyDarkFrame.addTo(rectPanels[0]);
      lsSkyDarkFrame.setConstraints(rectLayout[0], 12, 512, 678, 5, rectPanels[0], lsSkyFileList.spParam);
      rectHash.put("SKY_DARK_FRAME", lsSkyDarkFrame);

      lsSkySetupList = new FATBOYRadioParam("SKY_SETUP_LIST:", FATBOYLabel.NONE, "Yes", "No");
      lsSkySetupList.addTo(rectPanels[0]);
      lsSkySetupList.setConstraints(rectLayout[0], 12, 512, 678, 15, rectPanels[0], lsSkyDarkFrame.tParam); 
      rectHash.put("SKY_SETUP_LIST", lsSkySetupList);

      lsSkySigma = new FATBOYTextParam("SKY_SIGMA:", FATBOYLabel.YGBEVEL, 10, "3");
      lsSkySigma.addFocusBorder();
      lsSkySigma.addTo(rectPanels[0]);
      lsSkySigma.setConstraints(rectLayout[0], 12, 512, 678, rectPanels[0], lsSkySetupList.yParam);
      rectHash.put("SKY_SIGMA", lsSkySigma);

      lsSkyTwoPassDetection = new FATBOYRadioParam("SKY_TWO_PASS_DETECTION:", FATBOYLabel.YGBEVEL, "Yes", "No");
      lsSkyTwoPassDetection.addFocusBorder();
      lsSkyTwoPassDetection.addTo(rectPanels[0]);
      lsSkyTwoPassDetection.setConstraints(rectLayout[0], 12, 512, 768, 15, rectPanels[0], lsSkySigma.tParam);
      rectHash.put("SKY_TWO_PASS_DETECTION", lsSkyTwoPassDetection);

      lsSkyBoxSize = new FATBOYTextParam("SKY_BOX_SIZE:", 10, "6");
      lsSkyBoxSize.addTo(rectPanels[0]);
      lsSkyBoxSize.setConstraints(rectLayout[0], 12, 512, 678, rectPanels[0], lsSkyTwoPassDetection.yParam);
      rectHash.put("SKY_BOX_SIZE", lsSkyBoxSize);

      lsSkyListOutput = new FATBOYTextBrowseParam("SKY_LIST_OUTPUT:", 20, "Browse", "list.out");
      lsSkyListOutput.addBrowse();
      lsSkyListOutput.addTo(rectPanels[0]);
      lsSkyListOutput.setConstraints(rectLayout[0], 12, 512, 678, 5, rectPanels[0], lsSkyBoxSize.tParam);
      rectHash.put("SKY_LIST_OUTPUT", lsSkyListOutput);

      lsSkyDoRectify = new FATBOYRadioParam("SKY_DO_RECTIFY:", "Yes", "No");
      lsSkyDoRectify.addTo(rectPanels[0]);
      lsSkyDoRectify.setConstraints(rectLayout[0], 12, 512, 678, 15, rectPanels[0], lsSkyListOutput.tParam);
      rectHash.put("SKY_DO_RECTIFY", lsSkyDoRectify);

      lsSkyListInput = new FATBOYTextBrowseParam("SKY_LIST_INPUT:", 20, "Browse");
      lsSkyListInput.addBrowse();
      lsSkyListInput.addTo(rectPanels[0]);
      lsSkyListInput.setConstraints(rectLayout[0], 12, 512, 678, 5, rectPanels[0], lsSkyDoRectify.yParam);
      rectHash.put("SKY_LIST_INPUT", lsSkyListInput);

      lsSkyFitOrder = new FATBOYTextParam("SKY_FIT_ORDER:", 10, "5");
      lsSkyFitOrder.addTo(rectPanels[0]);
      lsSkyFitOrder.setConstraints(rectLayout[0], 12, 512, 678, rectPanels[0], lsSkyListInput.tParam);
      rectHash.put("SKY_FIT_ORDER", lsSkyFitOrder);

      lsFitOutput = new FATBOYTextBrowseParam("FIT_OUTPUT:", FATBOYLabel.RYBEVEL, 20, "Browse", "rect.dat");
      lsFitOutput.addBrowse();
      lsFitOutput.addFocusBorder();
      lsFitOutput.addTo(rectPanels[0]);
      lsFitOutput.setConstraints(rectLayout[0], 12, 512, 678, 5, rectPanels[0], lsSkyFitOrder.tParam);
      rectHash.put("FIT_OUTPUT", lsFitOutput);

      //Add graying options
      lsSetupList.addGrayComponents("No", lsFileList, lsDarkFrame, lsListOutput);
      lsDoRectify.addGrayComponents("No", lsListInput, lsXCenter, lsFitOrder);
      lsSkySetupList.addGrayComponents("No", lsSkyFileList, lsSkyDarkFrame, lsSkySigma, lsSkyTwoPassDetection, lsSkyBoxSize, lsSkyListOutput);
      lsSkyDoRectify.addGrayComponents("No", lsSkyListInput, lsSkyFitOrder);

      ((FATBOYParam)rectHash.get("SETUP_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SETUP_LIST = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to setup the list of continua, as described above.<br></body></html>");
      ((FATBOYParam)rectHash.get("FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">list.dat</span><br><br>An ASCII text file containing a list of image filenames, one per line,<br>to be used in this step to compile the list of image filenames and<br>continuum positions.<br></body></html>");
      ((FATBOYParam)rectHash.get("DARK_FRAME")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DARK_FRAME = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>A single dark frame of the same shape as all the images that will be<br>subtracted from them in order to bring out the continua better. <i>This<br>parameter is mandatory and will be used in both this step and the next<br>one!</i> <br></body></html>");
      ((FATBOYParam)rectHash.get("LIST_OUTPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LIST_OUTPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">list.out</span><br><br>The file to be created with the list of image filenames and continuum<br>positions.<br></body></html>");
      ((FATBOYParam)rectHash.get("DO_RECTIFY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_RECTIFY = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to trace out the continua and perform the rectification fit.<br>If set to no, this step is skipped.  If skipped, the continuum rectification<br>will be set to y_out = y_in in the final <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FIT_OUTPUT</span><br>file.<br></body></html>");
      ((FATBOYParam)rectHash.get("LIST_INPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LIST_INPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>An ASCII text file that lists three columns: image filename, central<br>y-position of the continuum, and width of the continuum.  If such a file<br>is specified, then it will be read in and used, <i>overriding any internal<br>arrays compiled in the <a href=\"#A2\">previous step</a></i>.  If this is<br>left blank, the arrays compiled in the <a href=\"#A2\">previous step</a><br>will be used.<br></body></html>");
      ((FATBOYParam)rectHash.get("X_CENTER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">X_CENTER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1024</span><br><br>The x-position to be used as the first position when tracing out the<br>continuum.  This must be an x-position where there is continuum data<br>so that the program can fit a Gaussian to it because of the rejection<br>criteria that subsequent datapoints must be within 2 pixels (in the<br>y-position) of the previous fit.  Usually, you would want to simply select<br>the center of your image, but if for instance, you have an absorption line<br>right in the center in most of your continua, you would want to select<br>another value (1600, 750, or wherever you have a continuum in every image).<br></body></html>");
      ((FATBOYParam)rectHash.get("FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The order of the 2-d polynomial used to fit y_out = f(x_in, y_in).  A third<br>order polynomial (10 terms) is used by default.  You would like to get<br>a sigma around 0.2 to 0.3 pixels or better in the residuals.  Therefore,<br>if third order gives you this, stick with that.  If not, try 4th or 5th<br>order.<br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">list.dat</span><br><br>Either one FITS file or an ASCII text file containing a list of FITS files.<br>The latter is recommended because median combining several images will<br>A) improve the S/N ratio and bring out the sky lines better and B)<br>get rid of the continua if they are not in the same location in all images.<br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_DARK_FRAME")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_DARK_FRAME = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>A dark frame to subtrace from the master sky image. <i>This parameter is<br>mandatory.</i><br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_SETUP_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_SETUP_LIST = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to setup the list of skylines, as described above. <br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>Skylines must be at least this many sigma brighter than the median value<br>of the smoothed 1-d cut to be selected.  The recommended value is 3.<br>Regardless of what value is set here, 2.5 sigma will be used for the<br>second pass of skyline detection (if<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_TWO_PASS_DETECTION = yes</span>).<br></pre><br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_TWO_PASS_DETECTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_TWO_PASS_DETECTION = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to use a two-pass method of detecting the skylines.  In<br>the first pass, the median and sigma are computed for the 1-d cut of<br>the entire image and lines above the sigma threshold are selected.  If<br>this parameter is set to yes, then a second pass is also done in which<br>each quarter of the 1-d cut is examined on its own.  The median and sigma<br>are then computed separately for each section and in this way additional<br>lines are found in the fainter regions of the image.<br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_BOX_SIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_BOX_SIZE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">6</span><br><br>The half-width, in pixels, of the box size to use in the next step, when<br>fitting Gaussian profiles to trace out the sky lines.  <i>The recommended value<br>is 6 and should not be changed unless the skylines exhibit an extremly<br>large curvature.</i><br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_LIST_OUTPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_LIST_OUTPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">list.out</span><br><br>The file to be created with the list of skyline positions. <br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_DO_RECTIFY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_DO_RECTIFY = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to trace out the skylines and perform the rectification fit.<br>If set to no, this step is skipped and the skyline rectification will be<br>set to x_out = x_in in the final <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FIT_OUTPUT</span><br>file.<br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_LIST_INPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_LIST_INPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>An ASCII text file containing an image filename (the master sky image) as<br>the first line, and then in each subsequent line two columns: <br>central x-position of a skyline and the width of the skyline.  If such<br>a file is specified, then it will be read in and used, <i>overriding<br>any internal arrays compiled in the <a href=\"#A5\">previous step</a></i>.<br>If this is left blank, the arrays compiled in the <a href=\"#A5\"><br>previous step</a> will be used.<br></body></html>");
      ((FATBOYParam)rectHash.get("SKY_FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>The order of the 2-d polynomial used to fit x_out = f(x_in, y_in).  A<br>fifth order polynomial (21 terms) is used by default.  You would like<br>to get a sigma around 0.2 to 0.3 pixels or better in the residuals.  Therefore,<br>if fifth order gives you this, stick with that (unless there are very few<br>skylines in your image, in which case you might want to try 3rd or 4th<br>order).  If 5th does not give a good enough fit, try 6th order. <br></body></html>");
      ((FATBOYParam)rectHash.get("FIT_OUTPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FIT_OUTPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">rect.dat</span><br><br>The filename to output both fits to, which will be used by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>drizzle</span> to simultaneously rectify in both directions.<br></body></html>");

      rectPane.setFont(new Font("Dialog",Font.BOLD,16));
      rectPane.addTab("Rectification Parameters", null, rectPanels[0]);

      //Setup MOS Rectification Pane
      rectMOSPanels = new JPanel[1];
      SpringLayout[] rectMOSLayout = new SpringLayout[1];
      for (int j = 0; j < 1; j++) {
        rectMOSPanels[j] = new JPanel();
        rectMOSPanels[j].setPreferredSize(new Dimension(xdim,ydim-250));
        rectMOSLayout[j] = new SpringLayout();
        rectMOSPanels[j].setLayout(rectMOSLayout[j]);
      }

      //MOS Rectification Initialization
      rectMOSHash = new LinkedHashMap(40);
      mRegFileList = new FATBOYTextBrowseParam("REG_FILE_LIST:", FATBOYLabel.RYBEVEL, 18, "Browse");
      mRegFileList.addBrowse();
      mRegFileList.addFocusBorder();
      mRegFileList.addTo(rectMOSPanels[0]);
      mRegFileList.setConstraints(rectMOSLayout[0], 10, 5, 220, 5, rectMOSPanels[0], rectMOSPanels[0], false, true); 
      rectMOSHash.put("REG_FILE_LIST", mRegFileList);

      mDoRegmassage = new FATBOYRadioParam("DO_REGMASSAGE:", FATBOYLabel.YGBEVEL, "Yes", "No", false);
      mDoRegmassage.addTo(rectMOSPanels[0]);
      mDoRegmassage.addFocusBorder();
      mDoRegmassage.setConstraints(rectMOSLayout[0], 15, 5, 220, 15, rectMOSPanels[0], mRegFileList.tParam);
      rectMOSHash.put("DO_REGMASSAGE", mDoRegmassage);

      mRegmassagePath = new FATBOYTextBrowseParam("REGMASSAGE_PATH:", 18, "Browse", "regmassage");
      mRegmassagePath.addBrowse();
      mRegmassagePath.addTo(rectMOSPanels[0]);
      mRegmassagePath.setConstraints(rectMOSLayout[0], 15, 5, 220, 5, rectMOSPanels[0], mDoRegmassage.yParam);
      rectMOSHash.put("REGMASSAGE_PATH", mRegmassagePath);

      mRegmassageOffset = new FATBOYTextParam("REGMASSAGE_OFFSET:", 10, "0");
      mRegmassageOffset.addTo(rectMOSPanels[0]);
      mRegmassageOffset.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mRegmassagePath.tParam);
      rectMOSHash.put("REGMASSAGE_OFFSET", mRegmassageOffset);

      mSetupList = new FATBOYRadioParam("SETUP_LIST:", FATBOYLabel.NONE, "Yes", "No");
      mSetupList.addTo(rectMOSPanels[0]);
      mSetupList.setConstraints(rectMOSLayout[0], 15, 5, 220, 15, rectMOSPanels[0], mRegmassageOffset.tParam);
      rectMOSHash.put("SETUP_LIST", mSetupList);

      mFlatFieldList = new FATBOYTextBrowseParam("FLAT_FIELD_LIST:", FATBOYLabel.RYBEVEL, 18, "Browse");
      mFlatFieldList.addBrowse();
      mFlatFieldList.addFocusBorder();
      mFlatFieldList.addTo(rectMOSPanels[0]);
      mFlatFieldList.setConstraints(rectMOSLayout[0], 15, 5, 220, 5, rectMOSPanels[0], mSetupList.yParam);
      rectMOSHash.put("FLAT_FIELD_LIST", mFlatFieldList);

      mDarkFramesForFlats = new FATBOYTextBrowseParam("DARK_FRAMES_FOR_FLATS:", FATBOYLabel.RYBEVEL, 16, "Browse");
      mDarkFramesForFlats.addBrowse();
      mDarkFramesForFlats.addFocusBorder();
      mDarkFramesForFlats.addTo(rectMOSPanels[0]);
      mDarkFramesForFlats.setConstraints(rectMOSLayout[0], 15, 5, 240, 5, rectMOSPanels[0], mFlatFieldList.tParam);
      rectMOSHash.put("DARK_FRAMES_FOR_FLATS", mDarkFramesForFlats);

      mMaxSlitWidth = new FATBOYTextParam("MAX_SLIT_WIDTH:", 10, "10");
      mMaxSlitWidth.addTo(rectMOSPanels[0]);
      mMaxSlitWidth.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mDarkFramesForFlats.tParam);
      rectMOSHash.put("MAX_SLIT_WIDTH", mMaxSlitWidth);

      mFileList = new FATBOYTextBrowseParam("FILE_LIST:", FATBOYLabel.RYBEVEL, 18, "Browse");
      mFileList.addBrowse();
      mFileList.addFocusBorder();
      mFileList.addTo(rectMOSPanels[0]);
      mFileList.setConstraints(rectMOSLayout[0], 15, 5, 220, 5, rectMOSPanels[0], mMaxSlitWidth.tParam);
      rectMOSHash.put("FILE_LIST", mFileList);

      mBoxCenter = new FATBOYTextParam("BOX_CENTER:", FATBOYLabel.YGBEVEL, 10, "1024");
      mBoxCenter.addFocusBorder();
      mBoxCenter.addTo(rectMOSPanels[0]);
      mBoxCenter.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mFileList.tParam);
      rectMOSHash.put("BOX_CENTER", mBoxCenter);

      mBoxWidth = new FATBOYTextParam("BOX_WIDTH:", FATBOYLabel.YGBEVEL, 10, "1000");
      mBoxWidth.addFocusBorder();
      mBoxWidth.addTo(rectMOSPanels[0]);
      mBoxWidth.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mBoxCenter.tParam);
      rectMOSHash.put("BOX_WIDTH", mBoxWidth);

      mSpectralThreshold = new FATBOYTextParam("SPECTRAL_THRESHOLD:", 10, "2");
      mSpectralThreshold.addTo(rectMOSPanels[0]);
      mSpectralThreshold.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mBoxWidth.tParam);
      rectMOSHash.put("SPECTRAL_THRESHOLD", mSpectralThreshold);

      mMaxSpectralWidth = new FATBOYTextParam("MAX_SPECTRAL_WIDTH:", 10, "20");
      mMaxSpectralWidth.addTo(rectMOSPanels[0]);
      mMaxSpectralWidth.setConstraints(rectMOSLayout[0], 15, 5, 220, rectMOSPanels[0], mSpectralThreshold.tParam);
      rectMOSHash.put("MAX_SPECTRAL_WIDTH", mMaxSpectralWidth);

      mListOutput = new FATBOYTextBrowseParam("LIST_OUTPUT:", 18, "Browse", "list.out");
      mListOutput.addBrowse();
      mListOutput.addTo(rectMOSPanels[0]);
      mListOutput.setConstraints(rectMOSLayout[0], 15, 5, 220, 5, rectMOSPanels[0], mMaxSpectralWidth.tParam);
      rectMOSHash.put("LIST_OUTPUT", mListOutput);

      mDoRectify = new FATBOYRadioParam("DO_RECTIFY:", "Yes", "No");
      mDoRectify.addTo(rectMOSPanels[0]);
      mDoRectify.setConstraints(rectMOSLayout[0], 10, 548, 728, 15, rectMOSPanels[0], rectMOSPanels[0], false, true); 
      rectMOSHash.put("DO_RECTIFY", mDoRectify);

      mListInput = new FATBOYTextBrowseParam("LIST_INPUT:", 18, "Browse");
      mListInput.addBrowse();
      mListInput.addTo(rectMOSPanels[0]);
      mListInput.setConstraints(rectMOSLayout[0], 15, 548, 728, 5, rectMOSPanels[0], mDoRectify.yParam);
      rectMOSHash.put("LIST_INPUT", mListInput);

      mMinCoverageFraction = new FATBOYTextParam("MIN_COVERAGE_FRACTION:", FATBOYLabel.YGBEVEL, 12, "70");
      mMinCoverageFraction.addFocusBorder();
      mMinCoverageFraction.addTo(rectMOSPanels[0]);
      mMinCoverageFraction.setConstraints(rectMOSLayout[0], 15, 548, 792, rectMOSPanels[0], mListInput.tParam);
      rectMOSHash.put("MIN_COVERAGE_FRACTION", mMinCoverageFraction);

      mDatabaseFile = new FATBOYTextBrowseParam("DATABASE_FILE:", FATBOYLabel.RYBEVEL, 18, "Browse", "mosrectdb.dat");
      mDatabaseFile.addFocusBorder();
      mDatabaseFile.addBrowse();
      mDatabaseFile.addTo(rectMOSPanels[0]);
      mDatabaseFile.setConstraints(rectMOSLayout[0], 15, 548, 728, 5, rectMOSPanels[0], mMinCoverageFraction.tParam);
      rectMOSHash.put("DATABASE_FILE", mDatabaseFile);

      mAppendDatabase = new FATBOYRadioParam("APPEND_DATABASE:", FATBOYLabel.YGBEVEL, "Yes", "No");
      mAppendDatabase.addFocusBorder();
      mAppendDatabase.addTo(rectMOSPanels[0]);
      mAppendDatabase.setConstraints(rectMOSLayout[0], 15, 548, 728, 15, rectMOSPanels[0], mDatabaseFile.tParam);
      rectMOSHash.put("APPEND_DATABASE", mAppendDatabase);

      mFitOrder = new FATBOYTextParam("FIT_ORDER:", 10, "3");
      mFitOrder.addTo(rectMOSPanels[0]);
      mFitOrder.setConstraints(rectMOSLayout[0], 15, 548, 728, rectMOSPanels[0], mAppendDatabase.yParam);
      rectMOSHash.put("FIT_ORDER", mFitOrder);

      //Add graying options
      mDoRegmassage.addGrayComponents("No", mRegmassagePath,mRegmassageOffset);
      mSetupList.addGrayComponents("No", mFlatFieldList, mDarkFramesForFlats, mMaxSlitWidth, mFileList, mBoxCenter, mBoxWidth, mSpectralThreshold, mMaxSpectralWidth, mListOutput);
      mDoRectify.addGrayComponents("No", mListInput, mMinCoverageFraction, mDatabaseFile, mAppendDatabase, mFitOrder);

      rectMOSPane.setFont(new Font("Dialog",Font.BOLD,16));
      rectMOSPane.addTab("MOS Rectification Parameters", null, rectMOSPanels[0]);

      //Setup tooltips
      ((FATBOYParam)rectMOSHash.get("REG_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REG_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This should be set to an ASCII text file containing a list of<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ds9</span> style region files, 1 per dataset.  For<br>an example of a region file, <a href=\"lk101f21.reg_2.reg\">click here</a>.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("DO_REGMASSAGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_REGMASSAGE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Whether or not to run <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">regmassage</span> on the region<br>files. <br></body></html>");
      ((FATBOYParam)rectMOSHash.get("REGMASSAGE_PATH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REGMASSAGE_PATH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">regmassage</span><br><br>The full path of the executable <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">regmassage</span>.<br>If it is in your current directory or your path, you can simply specify<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">REGMASSAGE_PATH = regmassage</span>.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("REGMASSAGE_OFFSET")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REGMASSAGE_OFFSET = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>The initial guess at the vertical offset to pass to<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">regmassage</span>.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("SETUP_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SETUP_LIST = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform this step and the next one<br>(<a href=\"#B4\">read in the files and setup a list of continua positions</a>).<br>Because this step must be done before the next one, there is no option to do<br>just one or the other.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("FLAT_FIELD_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_FIELD_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This should be set to an ASCII text file containing a list of flat fields,<br>1 per dataset.  Each flat field can be either one single on flat, a single<br>on-off flat, a master on flat, or a master on-off flat and is just used<br>for correcting offsets in the corresponding region file.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("DARK_FRAMES_FOR_FLATS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DARK_FRAMES_FOR_FLATS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This should be set to an ASCII text file containing a list of dark frames,<br>1 per dataset, that correspond to the flat fields listed in the previous<br>parameter.  The dark frames will simply be subtracted from the flat fields.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("MAX_SLIT_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MAX_SLIT_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">10</span><br><br>The maximum slit width -- anything wider than this will be considered a<br>guide star box and will be kept in the region file but thrown out of the<br>arrays containing slit positions because we want to use only datapoints from<br>continua and not the guide star boxes when compiling our rectification<br>database. <br></body></html>");
      ((FATBOYParam)rectMOSHash.get("FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILE_LIST = </span><br><br><br><b>Default value:</b><br><br>An ASCII text file containing a list of filenames, one per line, to be used<br>in this step to compile the list of image filenames, continua positions,<br>and x_slit positions.  <i>An A-B dither pattern is assumed.  Therefore,<br>this file list should have twice as many lines as the region file list,<br>flat field list, and dark frame list: an A and a B for each.</i>  The B will<br>be subtracted from the A (and vice versa) to get rid of the sky background<br>so that the continua can be better found.<br>For example, do an <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ls spec/lkha101f21_jhjh.0001.fits<br>spec/lkha101f21_jhjh.0002.fits > rectmosFiles.dat</span><br>and then set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILE_LIST = rectmosFiles.dat</span>.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("BOX_CENTER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BOX_CENTER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1024</span><br><br>The central x-pixel of the box to be used to detect the locations of continua<br>in the image.  The box will span the entire y-range of the image and<br>the median value of each row will be compiled into a 1-d cut of the image. <br></body></html>");
      ((FATBOYParam)rectMOSHash.get("BOX_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BOX_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1000</span><br><br>The width, in pixels, of the box to be used to detect the locations of<br>continua.  In images where the continua have a large curvature or slant,<br>this should be set to a smaller value, such as 400 or 500 pixels.  In images<br>with relatively little curvature though, a larger box of around 1000 pixels<br>can be used.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("SPECTRAL_THRESHOLD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SPECTRAL_THRESHOLD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>This parameter defines the sigma threshold to be used in an iterative<br>sigma clipping routine to detect continua.  The default value is 2 sigma.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("MAX_SPECTRAL_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MAX_SPECTRAL_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">20</span><br><br>The maximum width in pixels for the profile of a spectrum.  Occasionally,<br>regions of the image with very bright backgrounds can be mistaken for<br>actual continua, especially when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SPECTRAL_THRESHOLD</span><br>is set to a lower value, such as 2 sigma.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("LIST_OUTPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LIST_OUTPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">list.out</span><br><br>The ASCII text file to be created, listing filenames, continuum locations,<br>and x_slit positions.  The format of this file is: line 1 is a filename<br>followed by n, the number of continua found in that image.  The next n<br>lines have three columns: central y-position, width, and x_slit position.<br>Then the next filename is listed along with the number of continua found<br>in that image, and so on.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("DO_RECTIFY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_RECTIFY = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to trace out the continua and compile the rectification<br>database.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("LIST_INPUT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LIST_INPUT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>An ASCII text file, of the type created in the <a href=\"#B4\">previous<br>step</a>, containing image filenames, continuum positions, and x_slit<br>positions.  The format of the file is: line 1 is a filename followed by n,<br>the number of continua found in that image. The next n lines have three<br>columns: central y-position, width, and x_slit position. Then the next<br>filename is listed along with the number of continua found in that image,<br>and so on. <br></body></html>");
      ((FATBOYParam)rectMOSHash.get("MIN_COVERAGE_FRACTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MIN_COVERAGE_FRACTION = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">70</span><br><br>The minimum threshold for the fraction of datapoints where the continuum<br>was successfully fit divided by the number of total points in the array<br>of x-positions.  If the coverage fraction is greater than this paramter,<br>then the datapoints from that continuum are kept.  Otherwise, all datapoints<br>from that entire continuum are thrown out.  The default value is 70%,<br>although for fainter images, 60% is very reasonable.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("DATABASE_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DATABASE_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">mosrectdb.dat</span><br><br>The name of the ASCII text file that will be written, containg the database<br>of x_in, y_in, y_out, and x_slit positions.  This database file will then<br>be passed onto FATBOY as a parameter when processing MOS data.<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("APPEND_DATABASE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">APPEND_DATABASE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>If the database filename given by <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DATABASE_FILE</span><br>already exists, this parameter selects whether that file should be just<br>overwritten and only the new data kept (if <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>APPEND_DATABASE = no</span>) or if the existing datapoints should be kept<br>and the new data simply appended to the database file (if <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>APPEND_DATABASE = yes</span>).<br></body></html>");
      ((FATBOYParam)rectMOSHash.get("FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The order of the 3-d polynomial used to fit y_out = f(x_in, y_in, x_slit).<br>A third order polynomial (20 terms) is used by default.  This should suffice<br>for most data.  It is not recommended to try anything above 4th order<br>unless you have an extremely large number of datapoints.  Residuals will<br>have a higher sigma than in longslit data because of the introduction of<br>x_slit, so expect sigmas on the order of 0.4 to 0.6 pixels.  This fit will<br>not be saved at this point and is just used as a last rejection criterion,<br>when all datapoints with residuals of greater than 2 sigma from the<br>fit are thrown out.<br></body></html>");

      //Setup Spectroscopy Pane
      specPanels = new JPanel[9];
      SpringLayout[] specLayout = new SpringLayout[9];
      for (int j = 0; j < 9; j++) {
        specPanels[j] = new JPanel();
        specPanels[j].setPreferredSize(new Dimension(xdim,ydim-200));
        specLayout[j] = new SpringLayout();
        specPanels[j].setLayout(specLayout[j]);
      }

      //Spectroscopy Initialization
      specHash = new LinkedHashMap(150);
      sFiles = new FATBOYTextAreaParam("FILES:", FATBOYLabel.RYBEVEL, 5, 20, "Add Files", "Clear");
      sFiles.addBrowse(true);
      sFiles.addClear();
      sFiles.addFocusBorder();
      sFiles.addTo(specPanels[1]);
      sFiles.setConstraints(specLayout[1], 10, 5, 180, 5, 10, specPanels[1], specPanels[1], false, false, true);
      specHash.put("FILES",sFiles);

      sGroupOutputBy = new FATBOYComboParam("GROUP_OUTPUT_BY:", FATBOYLabel.YGBEVEL,"filename,FITS keyword,from manual file");
      sGroupOutputBy.addFocusBorder();
      sGroupOutputBy.addTo(specPanels[1]);
      sGroupOutputBy.setConstraints(specLayout[1], 12, 5, 180, specPanels[1], sFiles.spParam);
      specHash.put("GROUP_OUTPUT_BY", sGroupOutputBy);

      sSpecifyGrouping = new FATBOYTextBrowseParam("SPECIFY_GROUPING:", FATBOYLabel.NONE, 20, "Browse");
      sSpecifyGrouping.addBrowse();
      sSpecifyGrouping.addTo(specPanels[1]);
      sSpecifyGrouping.setConstraints(specLayout[1], 12, 5, 180, 5, specPanels[1], sGroupOutputBy.cbParam);
      specHash.put("SPECIFY_GROUPING", sSpecifyGrouping);

      sDarkFileList = new FATBOYTextAreaParam("DARK_FILE_LIST:", FATBOYLabel.YGBEVEL, 4, 20, "Add Files", "Clear");
      sDarkFileList.addBrowse(true);
      sDarkFileList.addClear();
      sDarkFileList.addFocusBorder();
      sDarkFileList.addTo(specPanels[1]);
      sDarkFileList.setConstraints(specLayout[1], 14, 5, 180, 5, 10, specPanels[1], sSpecifyGrouping.tParam);
      specHash.put("DARK_FILE_LIST", sDarkFileList);

      sFlatFileList = new FATBOYTextAreaParam("FLAT_FILE_LIST:", FATBOYLabel.YGBEVEL, 4, 20, "Add Files", "Clear");
      sFlatFileList.addBrowse(true);
      sFlatFileList.addClear();
      sFlatFileList.addFocusBorder();
      sFlatFileList.addTo(specPanels[1]);
      sFlatFileList.setConstraints(specLayout[1], 14, 5, 180, 5, 10, specPanels[1], sDarkFileList.spParam);
      specHash.put("FLAT_FILE_LIST", sFlatFileList);

      sArclampList = new FATBOYTextAreaParam("ARCLAMP_FILE_LIST:", FATBOYLabel.YGBEVEL, 4, 20, "Add Files", "Clear");
      sArclampList.addBrowse(true);
      sArclampList.addClear();
      sArclampList.addFocusBorder();
      sArclampList.addTo(specPanels[1]);
      sArclampList.setConstraints(specLayout[1], 14, 5, 180, 5, 10, specPanels[1], sFlatFileList.spParam);
      specHash.put("ARCLAMP_FILE_LIST", sArclampList);

      sQuickStartFile = new FATBOYTextBrowseParam("QUICK_START_FILE:", FATBOYLabel.YGBEVEL, 20, "Browse");
      sQuickStartFile.addBrowse();
      sQuickStartFile.addFocusBorder();
      sQuickStartFile.addTo(specPanels[1]);
      sQuickStartFile.setConstraints(specLayout[1], 14, 5, 180, 5, specPanels[1], sArclampList.spParam);
      specHash.put("QUICK_START_FILE", sQuickStartFile);

      sOverwriteFiles = new FATBOYRadioParam("OVERWRITE_FILES:", FATBOYLabel.NONE, "Yes", "No", false);
      sOverwriteFiles.addTo(specPanels[1]);
      sOverwriteFiles.setConstraints(specLayout[1], 14, 5, 180, 15, specPanels[1], sQuickStartFile.tParam);
      specHash.put("OVERWRITE_FILES", sOverwriteFiles);

      sCleanUp = new FATBOYRadioParam("CLEAN_UP:", FATBOYLabel.NONE, "Yes", "No", false);
      sCleanUp.addTo(specPanels[1]);
      sCleanUp.setConstraints(specLayout[1], 8, 5, 180, 15, specPanels[1], sOverwriteFiles.yParam);
      specHash.put("CLEAN_UP", sCleanUp);

      sDoNoisemaps = new FATBOYRadioParam("DO_NOISEMAPS:", FATBOYLabel.NONE, "Yes", "No");
      sDoNoisemaps.addTo(specPanels[1]);
      sDoNoisemaps.setConstraints(specLayout[1], 8, 5, 180, 15, specPanels[1], sCleanUp.yParam);
      specHash.put("DO_NOISEMAPS", sDoNoisemaps);

      sPrefixDelimiter = new FATBOYTextParam("PREFIX_DELIMITER:", FATBOYLabel.NONE, 5, ".");
      sPrefixDelimiter.addTo(specPanels[1]);
      sPrefixDelimiter.setConstraints(specLayout[1], 8, 5, 180, specPanels[1], sDoNoisemaps.yParam);
      specHash.put("PREFIX_DELIMITER", sPrefixDelimiter);
      
      sExptimeKeyword = new FATBOYTextParam("EXPTIME_KEYWORD:", FATBOYLabel.NONE, 10, "EXP_TIME");
      sExptimeKeyword.addTo(specPanels[1]);
      sExptimeKeyword.setConstraints(specLayout[1], 10, 564, 768, specPanels[1], specPanels[1], true);
      specHash.put("EXPTIME_KEYWORD", sExptimeKeyword);

      sObstypeKeyword = new FATBOYTextParam("OBSTYPE_KEYWORD:", FATBOYLabel.NONE, 10, "OBS_TYPE");
      sObstypeKeyword.addTo(specPanels[1]);
      sObstypeKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sExptimeKeyword.tParam);
      specHash.put("OBSTYPE_KEYWORD", sObstypeKeyword);

      sFilterKeyword = new FATBOYTextParam("FILTER_KEYWORD:", FATBOYLabel.NONE, 10, "FILTER");
      sFilterKeyword.addTo(specPanels[1]);
      sFilterKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sObstypeKeyword.tParam);
      specHash.put("FILTER_KEYWORD", sFilterKeyword);

      sRAKeyword = new FATBOYTextParam("RA_KEYWORD:", FATBOYLabel.NONE, 10, "RA");
      sRAKeyword.addTo(specPanels[1]);
      sRAKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sFilterKeyword.tParam);
      specHash.put("RA_KEYWORD", sRAKeyword);

      sDecKeyword = new FATBOYTextParam("DEC_KEYWORD:", FATBOYLabel.NONE, 10, "DEC");
      sDecKeyword.addTo(specPanels[1]);
      sDecKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sRAKeyword.tParam);
      specHash.put("DEC_KEYWORD", sDecKeyword);

      sRelativeOffsetArcsec = new FATBOYRadioParam("RELATIVE_OFFSET_ARCSEC:", "Yes", "No", false);
      sRelativeOffsetArcsec.addTo(specPanels[1]);
      sRelativeOffsetArcsec.setConstraints(specLayout[1], 8, 564, 792, 15, specPanels[1], sDecKeyword.tParam);
      specHash.put("RELATIVE_OFFSET_ARCSEC", sRelativeOffsetArcsec);

      sObjectKeyword = new FATBOYTextParam("OBJECT_KEYWORD:", FATBOYLabel.NONE, 10, "OBJECT");
      sObjectKeyword.addTo(specPanels[1]);
      sObjectKeyword.setConstraints(specLayout[1], 8, 564, 768, specPanels[1], sRelativeOffsetArcsec.yParam);
      specHash.put("OBJECT_KEYWORD", sObjectKeyword);

      sGainKeyword = new FATBOYTextParam("GAIN_KEYWORD:", FATBOYLabel.NONE, 10, "GAIN_2");
      sGainKeyword.addTo(specPanels[1]);
      sGainKeyword.setConstraints(specLayout[1], 10, 564, 768, specPanels[1], sObjectKeyword.tParam);
      specHash.put("GAIN_KEYWORD", sGainKeyword);

      sReadnoiseKeyword = new FATBOYTextParam("READNOISE_KEYWORD:", FATBOYLabel.NONE, 10, "RDNOIS_2");
      sReadnoiseKeyword.addTo(specPanels[1]);
      sReadnoiseKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sGainKeyword.tParam);
      specHash.put("READNOISE_KEYWORD", sReadnoiseKeyword);

      sGrismKeyword = new FATBOYTextParam("GRISM_KEYWORD:", FATBOYLabel.NONE, 10, "GRISM");
      sGrismKeyword.addTo(specPanels[1]);
      sGrismKeyword.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sReadnoiseKeyword.tParam);
      specHash.put("GRISM_KEYWORD", sGrismKeyword);

      sMinFrameValue = new FATBOYTextParam("MIN_FRAME_VALUE:", FATBOYLabel.YGBEVEL, 10);
      sMinFrameValue.addFocusBorder();
      sMinFrameValue.addTo(specPanels[1]);
      sMinFrameValue.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sGrismKeyword.tParam);
      specHash.put("MIN_FRAME_VALUE", sMinFrameValue);

      sMaxFrameValue = new FATBOYTextParam("MAX_FRAME_VALUE:", FATBOYLabel.YGBEVEL, 10);
      sMaxFrameValue.addFocusBorder();
      sMaxFrameValue.addTo(specPanels[1]);
      sMaxFrameValue.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sMinFrameValue.tParam);
      specHash.put("MAX_FRAME_VALUE", sMaxFrameValue);

      sIgnoreFirstFrames = new FATBOYRadioParam("IGNORE_FIRST_FRAMES:", FATBOYLabel.NONE, "Yes", "No", false);
      sIgnoreFirstFrames.addTo(specPanels[1]);
      sIgnoreFirstFrames.setConstraints(specLayout[1], 12, 564, 792, 15, specPanels[1], sMaxFrameValue.tParam);
      specHash.put("IGNORE_FIRST_FRAMES", sIgnoreFirstFrames);

      sIgnoreAfterBadRead = new FATBOYRadioParam("IGNORE_AFTER_BAD_READ:", FATBOYLabel.NONE, "Yes", "No", false);
      sIgnoreAfterBadRead.addTo(specPanels[1]);
      sIgnoreAfterBadRead.setConstraints(specLayout[1], 8, 564, 792, 15, specPanels[1], sIgnoreFirstFrames.yParam);
      specHash.put("IGNORE_AFTER_BAD_READ", sIgnoreAfterBadRead);      

      sRotationAngle = new FATBOYTextParam("ROTATION_ANGLE:", FATBOYLabel.NONE, 10, "0");
      sRotationAngle.addTo(specPanels[1]);
      sRotationAngle.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sIgnoreAfterBadRead.yParam);
      specHash.put("ROTATION_ANGLE", sRotationAngle);

      sMEFExtension = new FATBOYTextParam("MEF_EXTENSION:", FATBOYLabel.NONE, 5);
      sMEFExtension.addTo(specPanels[1]);
      sMEFExtension.setConstraints(specLayout[1], 12, 564, 768, specPanels[1], sRotationAngle.tParam);
      specHash.put("MEF_EXTENSION", sMEFExtension);

      //Spectroscopy Linearity & Darks
      sDoLinearize = new FATBOYRadioParam("DO_LINEARIZE:", "Yes", "No");
      sDoLinearize.addTo(specPanels[2]);
      sDoLinearize.setConstraints(specLayout[2], 10, 5, 200, 15, specPanels[2], specPanels[2], false, true);
      specHash.put("DO_LINEARIZE", sDoLinearize);

      sLinearityCoeffs = new FATBOYTextParam("LINEARITY_COEFFS:", FATBOYLabel.YGBEVEL, 15, "1");
      sLinearityCoeffs.addFocusBorder();
      sLinearityCoeffs.addTo(specPanels[2]);
      sLinearityCoeffs.setConstraints(specLayout[2], 12, 5, 200, specPanels[2], sDoLinearize.yParam);
      specHash.put("LINEARITY_COEFFS", sLinearityCoeffs);

      sDoDarkCombine = new FATBOYRadioParam("DO_DARK_COMBINE:", "Yes", "No"); 
      sDoDarkCombine.addTo(specPanels[2]);
      sDoDarkCombine.setConstraints(specLayout[2], 36, 5, 200, 15, specPanels[2], sLinearityCoeffs.tParam);
      specHash.put("DO_DARK_COMBINE", sDoDarkCombine);

      sDoDarkSubtract = new FATBOYRadioParam("DO_DARK_SUBTRACT:", "Yes", "No");
      sDoDarkSubtract.addTo(specPanels[2]);
      sDoDarkSubtract.setConstraints(specLayout[2], 36, 5, 260, 15, specPanels[2], sDoDarkCombine.yParam);
      specHash.put("DO_DARK_SUBTRACT", sDoDarkSubtract);

      sDefaultMasterDark = new FATBOYTextBrowseParam("DEFAULT_MASTER_DARK:", FATBOYLabel.NONE, 20, "Browse");
      sDefaultMasterDark.addBrowse();
      sDefaultMasterDark.addTo(specPanels[2]);
      sDefaultMasterDark.setConstraints(specLayout[2], 12, 5, 260, 5, specPanels[2], sDoDarkSubtract.yParam);
      specHash.put("DEFAULT_MASTER_DARK", sDefaultMasterDark);

      sPromptDark = new FATBOYRadioParam("PROMPT_FOR_MISSING_DARK:", FATBOYLabel.NONE, "Yes", "No");
      sPromptDark.addTo(specPanels[2]);
      sPromptDark.setConstraints(specLayout[2], 12, 5, 260, 15, specPanels[2], sDefaultMasterDark.tParam);
      specHash.put("PROMPT_FOR_MISSING_DARK", sPromptDark);

      //Spectroscopy Flat Fields and Arclamps
      sDoFlatCombine = new FATBOYRadioParam("DO_FLAT_COMBINE:", "Yes", "No");
      sDoFlatCombine.addTo(specPanels[3]);
      sDoFlatCombine.setConstraints(specLayout[3], 10, 5, 220, 15, specPanels[3], specPanels[3], false, true);
      specHash.put("DO_FLAT_COMBINE", sDoFlatCombine);

      sFlatSelection = new FATBOYComboParam("FLAT_SELECTION:", FATBOYLabel.RYBEVEL,"all,auto,manual", "auto");
      sFlatSelection.addFocusBorder();
      sFlatSelection.addTo(specPanels[3]);
      sFlatSelection.setConstraints(specLayout[3], 12, 5, 220, specPanels[3], sDoFlatCombine.yParam);
      specHash.put("FLAT_SELECTION", sFlatSelection);

      sFlatMethod = new FATBOYRadioParam("FLAT_METHOD:", "On-Off", "On");
      sFlatMethod.addTo(specPanels[3]);
      sFlatMethod.setConstraints(specLayout[3], 12, 5, 220, 15, specPanels[3], sFlatSelection.cbParam);
      specHash.put("FLAT_METHOD", sFlatMethod);

      sFlatLampOffFiles = new FATBOYTextBrowseParam("FLAT_LAMP_OFF_FILES:", 20, "Browse", "off");
      sFlatLampOffFiles.addBrowse();
      sFlatLampOffFiles.addTo(specPanels[3]);
      sFlatLampOffFiles.setConstraints(specLayout[3], 12, 5, 220, 5, specPanels[3], sFlatMethod.yParam);
      specHash.put("FLAT_LAMP_OFF_FILES", sFlatLampOffFiles);

      sFlatLowThresh = new FATBOYTextParam("FLAT_LOW_THRESH:", 10, "0");
      sFlatLowThresh.addTo(specPanels[3]);
      sFlatLowThresh.setConstraints(specLayout[3], 12, 5, 220, specPanels[3], sFlatLampOffFiles.tParam);
      specHash.put("FLAT_LOW_THRESH", sFlatLowThresh);

      sFlatLowReplace = new FATBOYTextParam("FLAT_LOW_REPLACE:", 10, "1");
      sFlatLowReplace.addTo(specPanels[3]);
      sFlatLowReplace.setConstraints(specLayout[3], 12, 5, 220, specPanels[3], sFlatLowThresh.tParam);
      specHash.put("FLAT_LOW_REPLACE", sFlatLowReplace);

      sFlatHiThresh = new FATBOYTextParam("FLAT_HI_THRESH:", 10, "0");
      sFlatHiThresh.addTo(specPanels[3]);
      sFlatHiThresh.setConstraints(specLayout[3], 12, 5, 220, specPanels[3], sFlatLowReplace.tParam);
      specHash.put("FLAT_HI_THRESH", sFlatHiThresh);

      sFlatHiReplace = new FATBOYTextParam("FLAT_HI_REPLACE:", 10, "1");
      sFlatHiReplace.addTo(specPanels[3]);
      sFlatHiReplace.setConstraints(specLayout[3], 12, 5, 220, specPanels[3], sFlatHiThresh.tParam);
      specHash.put("FLAT_HI_REPLACE", sFlatHiReplace);

      sDoFlatDivide = new FATBOYRadioParam("DO_FLAT_DIVIDE:", "Yes", "No");
      sDoFlatDivide.addTo(specPanels[3]);
      sDoFlatDivide.setConstraints(specLayout[3], 36, 5, 220, 15, specPanels[2], sFlatHiReplace.tParam);
      specHash.put("DO_FLAT_DIVIDE", sDoFlatDivide);

      sDefaultMasterFlat = new FATBOYTextBrowseParam("DEFAULT_MASTER_FLAT:", FATBOYLabel.NONE, 20, "Browse");
      sDefaultMasterFlat.addBrowse();
      sDefaultMasterFlat.addTo(specPanels[3]);
      sDefaultMasterFlat.setConstraints(specLayout[3], 12, 5, 220, 5, specPanels[3], sDoFlatDivide.yParam);
      specHash.put("DEFAULT_MASTER_FLAT", sDefaultMasterFlat);

      sDoArclampCombine = new FATBOYRadioParam("DO_ARCLAMP_COMBINE:", "Yes", "No");
      sDoArclampCombine.addTo(specPanels[3]);
      sDoArclampCombine.setConstraints(specLayout[3], 36, 5, 220, 15, specPanels[3], sDefaultMasterFlat.tParam);
      specHash.put("DO_ARCLAMP_COMBINE", sDoArclampCombine);

      sArclampSelection = new FATBOYComboBrowseParam("ARCLAMP_SELECTION:", FATBOYLabel.RYBEVEL,"auto,from file", 20, "Browse");
      sArclampSelection.addBrowse();
      sArclampSelection.addFocusBorder();
      sArclampSelection.addTo(specPanels[3]);
      sArclampSelection.setConstraints(specLayout[3], 12, 5, 220, 12, 220, 5, specPanels[3], sDoArclampCombine.yParam);
      specHash.put("ARCLAMP_SELECTION", sArclampSelection);

      //Spectroscopy Bad Pixels 
      sDoBadPixelMask = new FATBOYRadioParam("DO_BAD_PIXEL_MASK:", "Yes", "No");
      sDoBadPixelMask.addTo(specPanels[4]);
      sDoBadPixelMask.setConstraints(specLayout[4], 10, 5, 260, 15, specPanels[4], specPanels[4], false, true);
      specHash.put("DO_BAD_PIXEL_MASK", sDoBadPixelMask);

      sDefaultBadPixelMask = new FATBOYTextBrowseParam("DEFAULT_BAD_PIXEL_MASK:", FATBOYLabel.NONE, 20, "Browse");
      sDefaultBadPixelMask.addBrowse();
      sDefaultBadPixelMask.addTo(specPanels[4]);
      sDefaultBadPixelMask.setConstraints(specLayout[4], 12, 5, 260, 5, specPanels[4], sDoBadPixelMask.yParam);
      specHash.put("DEFAULT_BAD_PIXEL_MASK", sDefaultBadPixelMask);

      sImagingFramesForBpm = new FATBOYTextBrowseParam("IMAGING_FRAMES_FOR_BPM:", FATBOYLabel.NONE, 20, "Browse");
      sImagingFramesForBpm.addBrowse();
      sImagingFramesForBpm.addTo(specPanels[4]);
      sImagingFramesForBpm.setConstraints(specLayout[4], 12, 5, 260, 5, specPanels[4], sDefaultBadPixelMask.tParam);
      specHash.put("IMAGING_FRAMES_FOR_BPM", sImagingFramesForBpm);

      sBadPixelMaskClipping = new FATBOYRadioParam("BAD_PIXEL_MASK_CLIPPING:", "Values","Sigma");
      sBadPixelMaskClipping.addTo(specPanels[4]);
      sBadPixelMaskClipping.setConstraints(specLayout[4], 12, 5, 260, 15, specPanels[4], sImagingFramesForBpm.tParam);
      specHash.put("BAD_PIXEL_MASK_CLIPPING", sBadPixelMaskClipping);

      sBadPixelHigh = new FATBOYTextParam("BAD_PIXEL_HIGH:",10,"2.0");
      sBadPixelHigh.addTo(specPanels[4]);
      sBadPixelHigh.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sBadPixelMaskClipping.yParam);
      specHash.put("BAD_PIXEL_HIGH", sBadPixelHigh);

      sBadPixelLow = new FATBOYTextParam("BAD_PIXEL_LOW:",10,"0.5");
      sBadPixelLow.addTo(specPanels[4]);
      sBadPixelLow.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sBadPixelHigh.tParam);
      specHash.put("BAD_PIXEL_LOW", sBadPixelLow);

      sBadPixelSigma = new FATBOYTextParam("BAD_PIXEL_SIGMA:",10,"5");
      sBadPixelSigma.addTo(specPanels[4]);
      sBadPixelSigma.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sBadPixelLow.tParam);
      specHash.put("BAD_PIXEL_SIGMA", sBadPixelSigma);

      sEdgeReject = new FATBOYTextParam("EDGE_REJECT:",5,"5");
      sEdgeReject.addTo(specPanels[4]);
      sEdgeReject.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sBadPixelSigma.tParam);
      specHash.put("EDGE_REJECT", sEdgeReject);

      sRadiusReject = new FATBOYTextParam("RADIUS_REJECT:",5,"0");
      sRadiusReject.addTo(specPanels[4]);
      sRadiusReject.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sEdgeReject.tParam);
      specHash.put("RADIUS_REJECT", sRadiusReject);

      sRemoveCosmicRays = new FATBOYRadioParam("REMOVE_COSMIC_RAYS:", FATBOYLabel.YGBEVEL, "Yes","No", false);
      sRemoveCosmicRays.addFocusBorder();
      sRemoveCosmicRays.addTo(specPanels[4]);
      sRemoveCosmicRays.setConstraints(specLayout[4], 36, 5, 260, 15, specPanels[4], sRadiusReject.tParam);
      specHash.put("REMOVE_COSMIC_RAYS", sRemoveCosmicRays);

      sCosmicRaySigma = new FATBOYTextParam("COSMIC_RAY_SIGMA:",5,"10");
      sCosmicRaySigma.addTo(specPanels[4]);
      sCosmicRaySigma.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sRemoveCosmicRays.yParam);
      specHash.put("COSMIC_RAY_SIGMA", sCosmicRaySigma);

      sCosmicRayPasses = new FATBOYTextParam("COSMIC_RAY_PASSES:",5,"1");
      sCosmicRayPasses.addTo(specPanels[4]);
      sCosmicRayPasses.setConstraints(specLayout[4], 12, 5, 260, specPanels[4], sCosmicRaySigma.tParam);
      specHash.put("COSMIC_RAY_PASSES", sCosmicRayPasses);

      //Spectroscopy Sky Subtraction 
      sDoSkySubtract = new FATBOYRadioParam("DO_SKY_SUBTRACT:", "Yes", "No");
      sDoSkySubtract.addTo(specPanels[5]);
      sDoSkySubtract.setConstraints(specLayout[5], 10, 5, 240, 15, specPanels[5], specPanels[5], false, true);
      specHash.put("DO_SKY_SUBTRACT", sDoSkySubtract);

      sSkySubtractMethod = new FATBOYComboBrowseParam("SKY_SUBTRACT_METHOD:", FATBOYLabel.RYBEVEL,"median,dither,ifu_onsource_dither,step,offsource_dither,offsource_multi_dither,from file", 20, "Browse", "dither");
      sSkySubtractMethod.addBrowse();
      sSkySubtractMethod.addFocusBorder();
      sSkySubtractMethod.addTo(specPanels[5]);
      sSkySubtractMethod.setConstraints(specLayout[5], 12, 5, 240, 10, 240, 5, specPanels[5], sDoSkySubtract.yParam);
      specHash.put("SKY_SUBTRACT_METHOD", sSkySubtractMethod);

      sSkyRejectType = new FATBOYComboParam("SKY_REJECT_TYPE:", "avsigclip,minmax,none,sigclip", "sigclip");
      sSkyRejectType.addTo(specPanels[5]);
      sSkyRejectType.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkySubtractMethod.tParam);
      specHash.put("SKY_REJECT_TYPE", sSkyRejectType);

      sSkyNlow = new FATBOYTextParam("SKY_NLOW:", 10, "1");
      sSkyNlow.addTo(specPanels[5]);
      sSkyNlow.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkyRejectType.cbParam);
      specHash.put("SKY_NLOW", sSkyNlow);

      sSkyNhigh = new FATBOYTextParam("SKY_NHIGH:", 10, "1");
      sSkyNhigh.addTo(specPanels[5]);
      sSkyNhigh.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkyNlow.tParam);
      specHash.put("SKY_NHIGH", sSkyNhigh);

      sSkyLsigma = new FATBOYTextParam("SKY_LSIGMA:", 10, "3");
      sSkyLsigma.addTo(specPanels[5]);
      sSkyLsigma.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkyNhigh.tParam);
      specHash.put("SKY_LSIGMA", sSkyLsigma);

      sSkyHsigma = new FATBOYTextParam("SKY_HSIGMA:", 10, "3");
      sSkyHsigma.addTo(specPanels[5]);
      sSkyHsigma.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkyLsigma.tParam);
      specHash.put("SKY_HSIGMA", sSkyHsigma);

      sUseSkyFiles = new FATBOYRadioParam("USE_SKY_FILES:", "All", "Range");
      sUseSkyFiles.addTo(specPanels[5]);
      sUseSkyFiles.setConstraints(specLayout[5], 12, 5, 240, 15, specPanels[5], sSkyHsigma.tParam);
      specHash.put("USE_SKY_FILES", sUseSkyFiles);

      sSkyFilesRange = new FATBOYTextParam("SKY_FILES_RANGE:", 10, "3");
      sSkyFilesRange.addTo(specPanels[5]);
      sSkyFilesRange.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sUseSkyFiles.yParam);
      specHash.put("SKY_FILES_RANGE", sSkyFilesRange);

      sSkyDitheringRange = new FATBOYTextParam("SKY_DITHERING_RANGE:", 10, "2");
      sSkyDitheringRange.addTo(specPanels[5]);
      sSkyDitheringRange.setConstraints(specLayout[5], 12, 5, 240, specPanels[5], sSkyFilesRange.tParam);
      specHash.put("SKY_DITHERING_RANGE", sSkyDitheringRange);

      sSkyOffsourceMethod = new FATBOYRadioParam("SKY_OFFSOURCE_METHOD:", "Auto", "Manual");
      sSkyOffsourceMethod.addTo(specPanels[5]);
      sSkyOffsourceMethod.setConstraints(specLayout[5], 12, 5, 240, 15, specPanels[5], sSkyDitheringRange.tParam);
      specHash.put("SKY_OFFSOURCE_METHOD", sSkyOffsourceMethod);

      sSkyOffsourceRange = new FATBOYTextParam("SKY_OFFSOURCE_RANGE:", 10, "240");
      sSkyOffsourceRange.addTo(specPanels[5]);
      sSkyOffsourceRange.setConstraints(specLayout[5], 10, 5, 240, specPanels[5], sSkyOffsourceMethod.yParam);
      specHash.put("SKY_OFFSOURCE_RANGE", sSkyOffsourceRange);

      sSkyOffsourceManual = new FATBOYTextBrowseParam("SKY_OFFSOURCE_MANUAL:", 16, "Browse");
      sSkyOffsourceManual.addBrowse();
      sSkyOffsourceManual.addTo(specPanels[5]);
      sSkyOffsourceManual.setConstraints(specLayout[5], 12, 5, 240, 5, specPanels[5], sSkyOffsourceRange.tParam);
      specHash.put("SKY_OFFSOURCE_MANUAL", sSkyOffsourceManual);

      sIgnoreOddFrames = new FATBOYRadioParam("IGNORE_ODD_FRAMES:", "Yes", "No");
      sIgnoreOddFrames.addTo(specPanels[5]);
      sIgnoreOddFrames.setConstraints(specLayout[5], 12, 5, 240, 15, specPanels[5], sSkyOffsourceManual.tParam);
      specHash.put("IGNORE_ODD_FRAMES", sIgnoreOddFrames);

      //Spectroscopy Rectification and Combining
      sDoRectify = new FATBOYRadioParam("DO_RECTIFY:", "Yes", "No");
      sDoRectify.addTo(specPanels[6]);
      sDoRectify.setConstraints(specLayout[6], 10, 5, 240, 15, specPanels[6], specPanels[6], false, true);
      specHash.put("DO_RECTIFY", sDoRectify);

      sDataType = new FATBOYComboBrowseParam("DATA_TYPE:", FATBOYLabel.RYBEVEL,"longslit,mos,ifu,from file", 16, "Browse");
      sDataType.addBrowse();
      sDataType.addFocusBorder();
      sDataType.addTo(specPanels[6]);
      sDataType.setConstraints(specLayout[6], 12, 5, 240, 10, 240, 5, specPanels[6], sDoRectify.yParam);
      specHash.put("DATA_TYPE", sDataType);

      sLongslitRectCoeffs = new FATBOYTextBrowseParam("LONGSLIT_RECT_COEFFS:", FATBOYLabel.YGBEVEL, 16, "Browse");
      sLongslitRectCoeffs.addBrowse();
      sLongslitRectCoeffs.addFocusBorder();
      sLongslitRectCoeffs.addTo(specPanels[6]);
      sLongslitRectCoeffs.setConstraints(specLayout[6], 12, 5, 240, 5, specPanels[6], sDataType.tParam);
      specHash.put("LONGSLIT_RECT_COEFFS", sLongslitRectCoeffs);

      sMosRectDb = new FATBOYTextBrowseParam("MOS_RECT_DB:", FATBOYLabel.YGBEVEL, 16, "Browse");
      sMosRectDb.addBrowse();
      sMosRectDb.addFocusBorder();
      sMosRectDb.addTo(specPanels[6]);
      sMosRectDb.setConstraints(specLayout[6], 12, 5, 240, 5, specPanels[6], sLongslitRectCoeffs.tParam);
      specHash.put("MOS_RECT_DB", sMosRectDb);

      sMosRectOrder = new FATBOYTextParam("MOS_RECT_ORDER:", 10, "3");
      sMosRectOrder.addTo(specPanels[6]);
      sMosRectOrder.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosRectDb.tParam);
      specHash.put("MOS_RECT_ORDER", sMosRectOrder);

      sMosRegionFile = new FATBOYTextBrowseParam("MOS_REGION_FILE:", FATBOYLabel.YGBEVEL, 16, "Browse");
      sMosRegionFile.addBrowse();
      sMosRegionFile.addFocusBorder();
      sMosRegionFile.addTo(specPanels[6]);
      sMosRegionFile.setConstraints(specLayout[6], 12, 5, 240, 5, specPanels[6], sMosRectOrder.tParam);
      specHash.put("MOS_REGION_FILE", sMosRegionFile);

      sMosYSlitBufferLow = new FATBOYTextParam("MOS_YSLIT_BUFFER_LOW:", 10, "");
      sMosYSlitBufferLow.addTo(specPanels[6]);
      sMosYSlitBufferLow.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosRegionFile.tParam);
      specHash.put("MOS_YSLIT_BUFFER_LOW", sMosYSlitBufferLow);

      sMosYSlitBufferHigh = new FATBOYTextParam("MOS_YSLIT_BUFFER_HIGH:", 10, "");
      sMosYSlitBufferHigh.addTo(specPanels[6]);
      sMosYSlitBufferHigh.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosYSlitBufferLow.tParam);
      specHash.put("MOS_YSLIT_BUFFER_HIGH", sMosYSlitBufferHigh);

      sMosMaxSlitWidth = new FATBOYTextParam("MOS_MAX_SLIT_WIDTH:", 10, "10");
      sMosMaxSlitWidth.addTo(specPanels[6]);
      sMosMaxSlitWidth.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosYSlitBufferHigh.tParam);
      specHash.put("MOS_MAX_SLIT_WIDTH", sMosMaxSlitWidth);

      sMosRectUseArclamps = new FATBOYRadioParam("MOS_RECT_USE_ARCLAMPS:", "Yes", "No", false);
      sMosRectUseArclamps.addTo(specPanels[6]);
      sMosRectUseArclamps.setConstraints(specLayout[6], 10, 5, 240, 15, specPanels[6], sMosMaxSlitWidth.tParam);
      specHash.put("MOS_RECT_USE_ARCLAMPS", sMosRectUseArclamps);

      sMosSkylineSigma = new FATBOYTextParam("MOS_SKYLINE_SIGMA:", 10, "10");
      sMosSkylineSigma.addTo(specPanels[6]);
      sMosSkylineSigma.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosRectUseArclamps.yParam);
      specHash.put("MOS_SKYLINE_SIGMA", sMosSkylineSigma);

      sMosSkyTwoPass = new FATBOYRadioParam("MOS_SKY_TWO_PASS:", "Yes", "No");
      sMosSkyTwoPass.addTo(specPanels[6]);
      sMosSkyTwoPass.setConstraints(specLayout[6], 10, 5, 240, 15, specPanels[6], sMosSkylineSigma.tParam);
      specHash.put("MOS_SKY_TWO_PASS", sMosSkyTwoPass);

      sMosSkyBoxSize = new FATBOYTextParam("MOS_SKY_BOX_SIZE:", 10, "6");
      sMosSkyBoxSize.addTo(specPanels[6]);
      sMosSkyBoxSize.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosSkyTwoPass.yParam);
      specHash.put("MOS_SKY_BOX_SIZE", sMosSkyBoxSize);

      sMosSkyFitOrder = new FATBOYTextParam("MOS_SKY_FIT_ORDER:", 10, "2");
      sMosSkyFitOrder.addTo(specPanels[6]);
      sMosSkyFitOrder.setConstraints(specLayout[6], 12, 5, 240, specPanels[6], sMosSkyBoxSize.tParam);
      specHash.put("MOS_SKY_FIT_ORDER", sMosSkyFitOrder);

      sDoDoubleSubtract = new FATBOYRadioParam("DO_DOUBLE_SUBTRACT:", "Yes", "No");
      sDoDoubleSubtract.addTo(specPanels[6]);
      sDoDoubleSubtract.setConstraints(specLayout[6], 10, 564, 768, 15, specPanels[6], specPanels[6], false, true);
      specHash.put("DO_DOUBLE_SUBTRACT", sDoDoubleSubtract);

      sSumBoxWidth = new FATBOYTextParam("SUM_BOX_WIDTH:", 10, "25");
      sSumBoxWidth.addTo(specPanels[6]);
      sSumBoxWidth.setConstraints(specLayout[6], 12, 564, 768, specPanels[6], sDoDoubleSubtract.yParam); 
      specHash.put("SUM_BOX_WIDTH", sSumBoxWidth);

      sSumBoxCenter = new FATBOYTextParam("SUM_BOX_CENTER:", 10, "1024");
      sSumBoxCenter.addTo(specPanels[6]);
      sSumBoxCenter.setConstraints(specLayout[6], 12, 564, 768, specPanels[6], sSumBoxWidth.tParam);
      specHash.put("SUM_BOX_CENTER", sSumBoxCenter);

      sDoShiftAdd = new FATBOYRadioParam("DO_SHIFT_ADD:", "Yes", "No");
      sDoShiftAdd.addTo(specPanels[6]);
      sDoShiftAdd.setConstraints(specLayout[6], 36, 564, 768, 15, specPanels[6], sSumBoxCenter.tParam);
      specHash.put("DO_SHIFT_ADD", sDoShiftAdd);

      sMaskOutTroughs = new FATBOYRadioParam("MASK_OUT_TROUGHS:", "Yes", "No");
      sMaskOutTroughs.addTo(specPanels[6]);
      sMaskOutTroughs.setConstraints(specLayout[6], 10, 564, 768, 15, specPanels[6], sDoShiftAdd.yParam);
      specHash.put("MASK_OUT_TROUGHS", sMaskOutTroughs);


      //Spectroscopy Alignment & Calibration
      sDoMosAlignSkylines = new FATBOYRadioParam("DO_MOS_ALIGN_SKYLINES:", "Yes", "No");
      sDoMosAlignSkylines.addTo(specPanels[7]);
      sDoMosAlignSkylines.setConstraints(specLayout[7], 10, 5, 280, 15, specPanels[7], specPanels[7], false, true);
      specHash.put("DO_MOS_ALIGN_SKYLINES", sDoMosAlignSkylines);

      sReferenceSlit = new FATBOYTextParam("REFERENCE_SLIT:", 10, "");
      sReferenceSlit.addTo(specPanels[7]);
      sReferenceSlit.setConstraints(specLayout[7], 10, 5, 280, specPanels[7], sDoMosAlignSkylines.yParam);
      specHash.put("REFERENCE_SLIT", sReferenceSlit);

      sNebularEmissionCheck = new FATBOYRadioParam("NEBULAR_EMISSION_CHECK:", "Yes", "No", false);
      sNebularEmissionCheck.addTo(specPanels[7]);
      sNebularEmissionCheck.setConstraints(specLayout[7], 12, 5, 280, 15, specPanels[7], sReferenceSlit.tParam);
      specHash.put("NEBULAR_EMISSION_CHECK", sNebularEmissionCheck);

      sReverseOrderOfSegments = new FATBOYRadioParam("REVERSE_ORDER_OF_SEGMENTS:", "Yes", "No", false);
      sReverseOrderOfSegments.addTo(specPanels[7]);
      sReverseOrderOfSegments.setConstraints(specLayout[7], 10, 5, 280, 15, specPanels[7], sNebularEmissionCheck.yParam);
      specHash.put("REVERSE_ORDER_OF_SEGMENTS", sReverseOrderOfSegments);

      sMedianFilter1D = new FATBOYRadioParam("MEDIAN_FILTER_1D:", "Yes", "No");
      sMedianFilter1D.addTo(specPanels[7]);
      sMedianFilter1D.setConstraints(specLayout[7], 10, 5, 280, 15, specPanels[7], sReverseOrderOfSegments.yParam);
      specHash.put("MEDIAN_FILTER_1D", sMedianFilter1D);

      sMosAlignUseArclamps = new FATBOYRadioParam("MOS_ALIGN_USE_ARCLAMPS:", "Yes", "No", false);
      sMosAlignUseArclamps.addTo(specPanels[7]);
      sMosAlignUseArclamps.setConstraints(specLayout[7], 8, 5, 280, 15, specPanels[7], sMedianFilter1D.yParam);
      specHash.put("MOS_ALIGN_USE_ARCLAMPS", sMosAlignUseArclamps);

      sMosAlignNlines = new FATBOYTextParam("MOS_ALIGN_NLINES:", 15, "25");
      sMosAlignNlines.addTo(specPanels[7]);
      sMosAlignNlines.setConstraints(specLayout[7], 8, 5, 280, specPanels[7], sMosAlignUseArclamps.yParam);
      specHash.put("MOS_ALIGN_NLINES", sMosAlignNlines);

      sMosAlignSigma = new FATBOYTextParam("MOS_ALIGN_SIGMA:", 15, "2");
      sMosAlignSigma.addTo(specPanels[7]);
      sMosAlignSigma.setConstraints(specLayout[7], 8, 5, 280, specPanels[7], sMosAlignNlines.tParam);
      specHash.put("MOS_ALIGN_SIGMA", sMosAlignSigma);

      sMosAlignFitOrder = new FATBOYTextParam("MOS_ALIGN_FIT_ORDER:", 10, "3");
      sMosAlignFitOrder.addTo(specPanels[7]);
      sMosAlignFitOrder.setConstraints(specLayout[7], 8, 5, 280, specPanels[7], sMosAlignSigma.tParam);
      specHash.put("MOS_ALIGN_FIT_ORDER", sMosAlignFitOrder);

      sDoWavelengthCalibrate = new FATBOYRadioParam("DO_WAVELENGTH_CALIBRATE:", "Yes", "No");
      sDoWavelengthCalibrate.addTo(specPanels[7]);
      sDoWavelengthCalibrate.setConstraints(specLayout[7], 26, 5, 280, 15, specPanels[7], sMosAlignFitOrder.tParam);
      specHash.put("DO_WAVELENGTH_CALIBRATE", sDoWavelengthCalibrate);

      sWavelengthCalibrateUseArclamps = new FATBOYRadioParam("WAVELENGTH_CALIBRATE_USE_ARCLAMPS:", "Yes", "No", false);
      sWavelengthCalibrateUseArclamps.addTo(specPanels[7]);
      sWavelengthCalibrateUseArclamps.setConstraints(specLayout[7], 8, 5, 370, 15, specPanels[7], sDoWavelengthCalibrate.yParam);
      specHash.put("WAVELENGTH_CALIBRATE_USE_ARCLAMPS", sWavelengthCalibrateUseArclamps);

      sCalibrationInfoFile = new FATBOYTextBrowseParam("CALIBRATION_INFO_FILE:", FATBOYLabel.RYBEVEL, 20, "Browse");
      sCalibrationInfoFile.addBrowse();
      sCalibrationInfoFile.addFocusBorder();
      sCalibrationInfoFile.addTo(specPanels[7]);
      sCalibrationInfoFile.setConstraints(specLayout[7], 10, 5, 280, 5, specPanels[7], sWavelengthCalibrateUseArclamps.yParam);
      specHash.put("CALIBRATION_INFO_FILE", sCalibrationInfoFile);

      sAngstromsPerUnit = new FATBOYTextParam("ANGSTROMS_PER_UNIT:", 10, "1");
      sAngstromsPerUnit.addTo(specPanels[7]);
      sAngstromsPerUnit.setConstraints(specLayout[7], 12, 5, 280, specPanels[7], sCalibrationInfoFile.tParam);
      specHash.put("ANGSTROMS_PER_UNIT", sAngstromsPerUnit);

      sCalibrationMinSigma = new FATBOYTextParam("CALIBRATION_MIN_SIGMA:", 10, "3");
      sCalibrationMinSigma.addTo(specPanels[7]);
      sCalibrationMinSigma.setConstraints(specLayout[7], 8, 5, 280, specPanels[7], sAngstromsPerUnit.tParam);
      specHash.put("CALIBRATION_MIN_SIGMA", sCalibrationMinSigma);

      sCalibrationFitOrder = new FATBOYTextParam("CALIBRATION_FIT_ORDER:", 10, "3");
      sCalibrationFitOrder.addTo(specPanels[7]);
      sCalibrationFitOrder.setConstraints(specLayout[7], 8, 5, 280, specPanels[7], sCalibrationMinSigma.tParam);
      specHash.put("CALIBRATION_FIT_ORDER", sMosAlignFitOrder);


      //Spectroscopy Spectra
      sDoExtractSpectra = new FATBOYRadioParam("DO_EXTRACT_SPECTRA:", "Yes", "No");
      sDoExtractSpectra.addTo(specPanels[8]);
      sDoExtractSpectra.setConstraints(specLayout[8], 10, 5, 240, 15, specPanels[8], specPanels[8], false, true);
      specHash.put("DO_EXTRACT_SPECTRA", sDoExtractSpectra);

      sExtractMethod = new FATBOYComboBrowseParam("EXTRACT_METHOD:", FATBOYLabel.RYBEVEL,"auto,full,manual,from file", 20, "Browse");
      sExtractMethod.addBrowse();
      sExtractMethod.addFocusBorder();
      sExtractMethod.addTo(specPanels[8]);
      sExtractMethod.setConstraints(specLayout[8], 12, 5, 240, 10, 240, 5, specPanels[8], sDoExtractSpectra.yParam);
      specHash.put("EXTRACT_METHOD", sExtractMethod);

      sExtractFilename = new FATBOYTextBrowseParam("EXTRACT_FILENAME:", 20, "Browse");
      sExtractFilename.addBrowse();
      sExtractFilename.addTo(specPanels[8]);
      sExtractFilename.setConstraints(specLayout[8], 12, 5, 240, 5, specPanels[8], sExtractMethod.tParam);
      specHash.put("EXTRACT_FILENAME", sExtractFilename);

      sExtractBoxWidth = new FATBOYTextParam("EXTRACT_BOX_WIDTH:", 10, "0");
      sExtractBoxWidth.addTo(specPanels[8]);
      sExtractBoxWidth.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractFilename.tParam);
      specHash.put("EXTRACT_BOX_WIDTH", sExtractBoxWidth);

      sExtractBoxCenter = new FATBOYTextParam("EXTRACT_BOX_CENTER:", 10, "0");
      sExtractBoxCenter.addTo(specPanels[8]);
      sExtractBoxCenter.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractBoxWidth.tParam);
      specHash.put("EXTRACT_BOX_CENTER", sExtractBoxCenter);

      sExtractNSpectra = new FATBOYTextParam("EXTRACT_N_SPECTRA:", 10, "0");
      sExtractNSpectra.addTo(specPanels[8]);
      sExtractNSpectra.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractBoxCenter.tParam);
      specHash.put("EXTRACT_N_SPECTRA", sExtractNSpectra);

      sExtractSigma = new FATBOYTextParam("EXTRACT_SIGMA:", 10, "2");
      sExtractSigma.addTo(specPanels[8]);
      sExtractSigma.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractNSpectra.tParam);
      specHash.put("EXTRACT_SIGMA", sExtractSigma);

      sExtractMinWidth = new FATBOYTextParam("EXTRACT_MIN_WIDTH:", 10, "5");
      sExtractMinWidth.addTo(specPanels[8]);
      sExtractMinWidth.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractSigma.tParam);
      specHash.put("EXTRACT_MIN_WIDTH", sExtractMinWidth);

      sExtractBoxGuessFile = new FATBOYTextBrowseParam("EXTRACT_BOX_GUESS_FILE:", 20, "Browse");
      sExtractBoxGuessFile.addBrowse();
      sExtractBoxGuessFile.addTo(specPanels[8]);
      sExtractBoxGuessFile.setConstraints(specLayout[8], 12, 5, 240, 5, specPanels[8], sExtractMinWidth.tParam);
      specHash.put("EXTRACT_BOX_GUESS_FILE", sExtractBoxGuessFile);

      sExtractWeighting = new FATBOYComboParam("EXTRACT_WEIGHTING:", FATBOYLabel.YGBEVEL,"gaussian,linear,median");
      sExtractWeighting.addFocusBorder();
      sExtractWeighting.addTo(specPanels[8]);
      sExtractWeighting.setConstraints(specLayout[8], 12, 5, 240, specPanels[8], sExtractBoxGuessFile.tParam);
      specHash.put("EXTRACT_WEIGHTING", sExtractWeighting);

      sDoCalibrationStarDivide = new FATBOYRadioParam("DO_CALIBRATION_STAR_DIVIDE:", "Yes", "No");
      sDoCalibrationStarDivide.addTo(specPanels[8]);
      sDoCalibrationStarDivide.setConstraints(specLayout[8], 36, 5, 280, 15, specPanels[8], sExtractWeighting.cbParam);
      specHash.put("DO_CALIBRATION_STAR_DIVIDE", sDoCalibrationStarDivide);

      sCalibrationStarFile = new FATBOYTextBrowseParam("CALIBRATION_STAR_FILE:", FATBOYLabel.RYBEVEL, 20, "Browse");
      sCalibrationStarFile.addBrowse();
      sCalibrationStarFile.addFocusBorder();
      sCalibrationStarFile.addTo(specPanels[8]);
      sCalibrationStarFile.setConstraints(specLayout[8], 10, 5, 280, 5, specPanels[8], sDoCalibrationStarDivide.yParam);
      specHash.put("CALIBRATION_STAR_FILE", sCalibrationStarFile);

      //Add graying options
      sDoLinearize.addGrayComponents("No",sLinearityCoeffs);
      sDoDarkSubtract.addGrayComponents("No",sDefaultMasterDark, sPromptDark);
      sDoFlatCombine.addGrayComponents("No",sFlatSelection, sFlatMethod, sFlatLampOffFiles, sFlatLowThresh, sFlatLowReplace, sFlatHiThresh, sFlatHiReplace);
      sFlatMethod.addGrayComponents("On", sFlatLowThresh, sFlatLowReplace,  sFlatHiThresh, sFlatHiReplace);
      sDoFlatDivide.addGrayComponents("No", sDefaultMasterFlat);
      sDoArclampCombine.addGrayComponents("No", sArclampSelection);
      sDoBadPixelMask.addGrayComponents("No", sDefaultBadPixelMask, sImagingFramesForBpm, sBadPixelMaskClipping, sBadPixelHigh, sBadPixelLow, sBadPixelSigma, sEdgeReject, sRadiusReject);
      sBadPixelMaskClipping.addGrayComponents("Values", sBadPixelSigma);
      sBadPixelMaskClipping.addGrayComponents("Sigma", sBadPixelHigh, sBadPixelLow);
      sRemoveCosmicRays.addGrayComponents("No", sCosmicRaySigma, sCosmicRayPasses);
      sDoSkySubtract.addGrayComponents("No", sSkySubtractMethod, sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange, sSkyDitheringRange, sSkyOffsourceMethod, sSkyOffsourceRange, sSkyOffsourceManual, sIgnoreOddFrames);
      sSkySubtractMethod.addGrayComponents("dither", sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange, sSkyOffsourceMethod, sSkyOffsourceRange, sSkyOffsourceManual);
      sSkySubtractMethod.addGrayComponents("median", sSkyOffsourceMethod, sSkyOffsourceRange, sSkyOffsourceManual, sIgnoreOddFrames);
      sSkySubtractMethod.addGrayComponents("ifu_onsource_dither", sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange, sSkyOffsourceMethod, sSkyOffsourceRange, sSkyOffsourceManual);
      sSkySubtractMethod.addGrayComponents("step", sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange, sSkyDitheringRange, sSkyOffsourceMethod, sSkyOffsourceRange, sSkyOffsourceManual, sIgnoreOddFrames);
      sSkySubtractMethod.addGrayComponents("offsource_dither", sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma, sUseSkyFiles, sSkyFilesRange);
      sSkySubtractMethod.addGrayComponents("offsource_multi_dither", sSkyRejectType, sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma);
      sSkyRejectType.addGrayComponents("none",  sSkyNlow, sSkyNhigh, sSkyLsigma, sSkyHsigma);
      sSkyRejectType.addGrayComponents("avsigclip", sSkyNlow, sSkyNhigh);
      sSkyRejectType.addGrayComponents("sigclip", sSkyNlow, sSkyNhigh);
      sSkyRejectType.addGrayComponents("minmax",  sSkyLsigma, sSkyHsigma);
      sUseSkyFiles.addGrayComponents("All", sSkyFilesRange);
      sSkyOffsourceMethod.addGrayComponents("Auto", sSkyOffsourceManual);
      sSkyOffsourceMethod.addGrayComponents("Manual", sSkyOffsourceRange);
      sDoRectify.addGrayComponents("No", sLongslitRectCoeffs, sMosYSlitBufferLow, sMosYSlitBufferHigh, sMosMaxSlitWidth, sMosRectUseArclamps, sMosSkylineSigma, sMosSkyTwoPass, sMosSkyBoxSize, sMosSkyFitOrder);
      sDataType.addGrayComponents("longslit", sMosRectDb, sMosRectOrder, sMosRegionFile,  sMosYSlitBufferLow, sMosYSlitBufferHigh, sMosMaxSlitWidth, sMosRectUseArclamps, sMosSkylineSigma, sMosSkyTwoPass, sMosSkyBoxSize, sMosSkyFitOrder);
      sDataType.addGrayComponents("mos", sLongslitRectCoeffs);
      //sDataType.addGrayComponents("ifu", sLongslitRectCoeffs);
      //sDataType.addGrayComponents("ifu", sMosRectDb, sMosRectOrder, sMosRegionFile,  sMosYSlitBufferLow, sMosYSlitBufferHigh, sMosMaxSlitWidth, sMosRectUseArclamps, sMosSkylineSigma, sMosSkyTwoPass, sMosSkyBoxSize, sMosSkyFitOrder);
      sDoDoubleSubtract.addGrayComponents("No", sSumBoxWidth, sSumBoxCenter);
      sDoShiftAdd.addGrayComponents("No", sMaskOutTroughs);
      sDoMosAlignSkylines.addGrayComponents("No", sReferenceSlit, sNebularEmissionCheck, sReverseOrderOfSegments, sMosAlignUseArclamps, sMosAlignNlines, sMosAlignSigma, sMosAlignFitOrder);
      sDoWavelengthCalibrate.addGrayComponents("No", sWavelengthCalibrateUseArclamps, sCalibrationInfoFile, sAngstromsPerUnit, sCalibrationMinSigma, sCalibrationFitOrder);
      sDoExtractSpectra.addGrayComponents("No", sExtractMethod, sExtractFilename, sExtractBoxWidth, sExtractBoxCenter, sExtractNSpectra, sExtractSigma, sExtractMinWidth, sExtractBoxGuessFile, sExtractWeighting);
      sExtractMethod.addGrayComponents("full", sExtractBoxWidth, sExtractBoxCenter, sExtractNSpectra, sExtractSigma, sExtractMinWidth, sExtractBoxGuessFile);
      sExtractMethod.addGrayComponents("manual", sExtractBoxWidth, sExtractBoxCenter, sExtractNSpectra, sExtractSigma, sExtractMinWidth, sExtractBoxGuessFile);
      sDoCalibrationStarDivide.addGrayComponents("No", sCalibrationStarFile);

      //add tooltips
      ((FATBOYParam)specHash.get("FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">files.dat</span><br><br>FILES can be 1) an ASCII text file containing a list of filenames, one per<br>line: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = files.dat</span><br><br>2) a directory name: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = /home/username/data/</span>,<br>in which case all files in the directory will be processed so make sure it<br>contains only FITS files.<br><br>3) a wildcard structure: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES = a*.fits</span>.  This<br>is available on UNIX/Linux only.<br></body></html>");
      ((FATBOYParam)specHash.get("GROUP_OUTPUT_BY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">GROUP_OUTPUT_BY = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">filename</span><br><br>If you are using unique filename prefixes for each object in your dataset<br>to be aligned and stacked then just ignore this paramater.  However,<br>if your filenames all have the same prefix, this parameter allows you<br>to specify which images should be grouped together to be aligned and<br>stacked.  You can specify a FITS keyword to group based on (in which<br>case the output filenames will contain the value of the given FITS<br>keyword) or enter the name of an ASCII text file of the format:<pre><br>input_prefix      start_index     stop_index      output_prefix</pre><br>For example:<pre><br>flam                0001            0005            mrk835<br>flam          6               11              ngc1569</pre><br>The fourth column is optional and if omitted, the output filenames will<br>contain the index of the first image in each group.  Leading and<br>trailing zeros may be given or omitted in the start and stop indices.<br></body></html>");
      ((FATBOYParam)specHash.get("SPECIFY_GROUPING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SPECIFY_GROUPING = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If you have set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GROUP_OUTPUT_BY = FITS keyword</span><br>or <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GROUP_OUTPUT_BY = from manual file</span> then<br>specify the FITS keyword or ASCII file using this parameter.<br></body></html>");
      ((FATBOYParam)specHash.get("DARK_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DARK_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If the OBS_TYPE keyword in your fits headers are not correctly set to <br>\"dark\" for dark frames, then you can enter a filename an ASCII text file<br>containing a list of filenames (that are also in the main file list above)<br>of dark frames.<br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If the OBS_TYPE keyword in your fits headers are not correctly set to<br>\"flat\" for flat fields, then you can enter a filename an ASCII text file<br>containing a list of filenames (that are also in the main file list above)<br>of flat fields.<br></body></html>");
      ((FATBOYParam)specHash.get("ARCLAMP_FILE_LIST")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ARCLAMP_FILE_LIST = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If the OBS_TYPE keyword in your fits headers are not correctly set to<br>\"lamp\" for arclamps, then you can enter a filename an ASCII text file<br>containing a list of filenames (that are also in the main file list above)<br>of arclamps.<br></body></html>");
      ((FATBOYParam)specHash.get("OVERWRITE_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OVERWRITE_FILES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>OVERWRITE_FILES controls the behaviour throughout the program.  If set to<br>no (the default), when a file that the program is about to create already<br>exists, that step is skipped.  For example, if you have been saving all<br>your intermediate files and stop the program after dark subtraction, when<br>you rerun the program it will see that the directory <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>darkSubtracted/</span> exists and each of the files it would create already<br>exist.  It would then skip over that step (and every step before it where<br>intermediate images exist) because the files already exist,<br>saving time.  Note that if you had stopped the program halfway through the<br>dark subtraction so that only half of those files existed, then that step<br>would be skipped for those images that have existing dark subtracted images<br>and performed on those images that did not.  If, however, OVERWRITE_FILES<br>is set to yes, the program will delete any existing files and recreate them.<br></body></html>");
      ((FATBOYParam)specHash.get("QUICK_START_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">QUICK_START_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>When rejecting frames (see below), it can take up to a second per image to<br>calculate the median value.  When running several hundred images in batch,<br>this amounts to several minutes.  If you enter a filename for<br>QUICK_START_FILE, then the image filename and median values are written<br>to this file if it doesn't exist and read from them if it does exist,<br>significantly speeding up the initialization.<br></body></html>");
      ((FATBOYParam)specHash.get("CLEAN_UP")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CLEAN_UP = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>CLEAN_UP determines whether the files from intermediate steps, such as<br>linearization, dark subtraction, and flat field division, are kept or deleted<br>on the fly (as soon as the next step is done).<br></body></html>");
      ((FATBOYParam)specHash.get("DO_NOISEMAPS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_NOISEMAPS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether noisemaps will be calculated and propagated throughout the reduction<br>process.  If set to yes, the noisemaps will be first calculated after the<br>linearity correction and then propagated through each ensuing step.<br></body></html>");
      ((FATBOYParam)specHash.get("PREFIX_DELIMITER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PREFIX_DELIMITER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">.</span><br><br>When multiple objects are processed at once, the pipeline uses the filename<br>prefix to identify images of the same object to group together for sky<br>subtraction and alignment/stacking.  PREFIX_DELIMITER allows you to tell<br>the pipeline what your file naming convention is so that it can group <br>images of the same object together.  For instance, if you name your files<br>orion.0001.fits and specify <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PREFIX_DELIMITER = .</span>,<br>orion would be the prefix.  If you name your files orion_0001.fits, then you<br>would want to specify <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">PREFIX_DELIMITER = _</span>.<br>When determining the prefix, the pipeline first strips out the trailing<br>'.fits' and then takes everything before the last occurance of<br>PREFIX_DELIMITER.  Note that PREFIX_DELIMITER can be more than one character<br>even though such naming conventions are uncommon.<br></body></html>");
      ((FATBOYParam)specHash.get("EXPTIME_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXPTIME_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXP_TIME</span><br><br>The keyword in the image header where the exposure time is stored.<br></body></html>");
      ((FATBOYParam)specHash.get("OBSTYPE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OBSTYPE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">OBS_TYPE</span><br><br>The keyword in the image header where the observation type (object, flat,<br>dark, etc.) is stored.<br></body></html>");
      ((FATBOYParam)specHash.get("FILTER_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FILTER_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILTER</span><br><br>The keyword in the image header where the filter (J, K, etc.) is stored.<br></body></html>");
      ((FATBOYParam)specHash.get("RA_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RA_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">RA</span><br><br>The keyword in the image header where the right ascension is stored.<br>If the RA does not change while dithering and the offset is stored<br>in another keyword, use that keyword here.<br></body></html>");
      ((FATBOYParam)specHash.get("DEC_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEC_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">DEC</span><br><br>The keyword in the image header where the declination is stored.<br>If the Dec does not change while dithering and the offset is stored<br>in another keyword, use that keyword here.<br></body></html>");
      ((FATBOYParam)specHash.get("RELATIVE_OFFSET_ARCSEC")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RELATIVE_OFFSET_ARCSEC = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Set this to yes if you are using a Gemini instrument, where the<br>relative offsets (in arcsec) from the object's RA and DEC are stored<br>in separate keywords when dithering.<br></body></html>");
      ((FATBOYParam)specHash.get("OBJECT_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">OBJECT_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">OBJECT</span><br><br>The keyword in the image header where the object's name is stored (e.g.,<br>ro05c2m2).  This is used to group flat fields together and associate<br>them with the proper object frames when<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = auto</span>.<br></body></html>");
      ((FATBOYParam)specHash.get("GAIN_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">GAIN_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GAIN_2</span><br><br>The keyword in the image header where the gain is stored.  Or if the<br>gain is not stored in the header, you may specify a value to be used<br>instead.  By default, the gain keyword is set to GAIN_2.  Be sure to change<br>this if your gain is listed under a different keyword.<br></body></html>");
      ((FATBOYParam)specHash.get("READNOISE_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">READNOISE_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">RDNOIS_2</span><br><br>The keyword in the image header where the read noise is stored.  Or if the<br>read noise is not stored in the header, you may specify a value to be used<br>instead.  By default, the read noise keyword is set to RDNOIS_2.  Be sure<br>to change this if your read noise is listed under a different keyword.<br></body></html>");
      ((FATBOYParam)specHash.get("GRISM_KEYWORD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">GRISM_KEYWORD = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">GRISM</span><br><br>The keyword in the image header where the grism (JH, HK, etc.) is stored.<br></body></html>");
      ((FATBOYParam)specHash.get("MIN_FRAME_VALUE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MIN_FRAME_VALUE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MIN_FRAME_VALUE specifies a minimum threshold value for the median level<br>of an image.  If the median pixel in the image has a value below this<br>threshold, then it is regarded as a bad exposure and thrown out before any<br>processing is done.  The default is left blank, which translates to a minimum<br>of 0.  <i>Note that this minimum value will not apply to any dark frames.<br>Darks are identified from the image header and will never be rejected in<br>this step, allowing you to set a threshold of 10,000 counts for instance,<br>without worrying that you'd throw away all your darks.</i><br></body></html>");
      ((FATBOYParam)specHash.get("MAX_FRAME_VALUE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MAX_FRAME_VALUE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MAX_FRAME_VALUE similarly specifies a maximum threshold value for the median<br>level of an image.  If the median pixel in the image has a value above this<br>threshold, then it is regarded as a bad exposure and thrown out before any<br>processing is done.  The default is left blank, which translates to a maximum<br>of 0.  When the maximum is set to 0, the median is not even computed for<br>comparison and nothing is rejected.<br></body></html>");
      ((FATBOYParam)specHash.get("IGNORE_FIRST_FRAMES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IGNORE_FIRST_FRAMES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If IGNORE_FIRST_FRAMES is set to yes, then the first frame of each object<br>at the same exposure time is thrown out before any processing is done.  This<br>flag is useful if the first frame could generally contain bad data.  The<br>first frame is detected from the filename prefix (see PREFIX_DELIMITER above)<br>and the exposure time stored in the header (using the EXPTIME_KEYWORD set<br>above).  Note that the first dark frame for a given exposure time and the<br>first flat field for each given prefix and exposure time will be thrown out<br>in addition to the first object for each given prefix and exposure time.<br><br><br></body></html>");
      ((FATBOYParam)specHash.get("IGNORE_AFTER_BAD_READ")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IGNORE_AFTER_BAD_READ = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no | yes</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>If IGNORE_AFTER_BAD_READ is set to yes and MAX_FRAME_VALUE is defined,<br>then not only will a bad frame with a median value higher than MAX_FRAME_VALUE<br>thrown out, but the next frame will also be thrown out.<br></body></html>");
      ((FATBOYParam)specHash.get("ROTATION_ANGLE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ROTATION_ANGLE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>The angle (in degress) by which all frames should be rotated.  Normally, this<br>should be set to zero, in which case no rotation is performed.  However, if<br>your spectra run vertically instead of horizontally across the image, you<br>will want to set this to 90 (or maybe 270) degrees.<br></body></html>");
      ((FATBOYParam)specHash.get("MEF_EXTENSION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MEF_EXTENSION = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>MEF_EXTENSION specifies the extension containing your data<br>if you are processing multi-extension FITS data.<br>Leave blank or enter -1 for autodetection of the<br>first extension containing data.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_LINEARIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_LINEARIZE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to apply the linearity correction.  If DO_LINEARIZE is set<br>to no, then no linearity correction is applied, no intermediate images are<br>created, and it moves on to the next step without doing anything.<br></body></html>");
      ((FATBOYParam)specHash.get("LINEARITY_COEFFS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LINEARITY_COEFFS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>A whitespace separated list of the coefficients for the nth order polynomial<br>used to apply the linearity correction.  There is no 0th order term so the<br>first term listed is the coefficient for the first order polynomial term.<br>Thus, the default value of 1 means that a correction will be applied but the<br>data will not be changed at all (y = 1*x).  Example: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>LINEARITY_COEFFS = 0.8 0.1 0.03</span> results in a correction of <br>y = 0.8*x + 0.1*x^2 + 0.03*x^3 being applied.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_DARK_COMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_DARK_COMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to combine your darks into master darks.  If set to no,<br>this step is skipped.  Note that if you have already gone past this step<br>and then stopped, you can rerun the pipeline with the same file list and<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">OVERWRITE_FILES = no</span> and this step will be skipped<br>anyway for every master dark that already exists.  In this way, there is<br>no need to modify your file list or parameter file.  The same holds true<br>for all other steps. <br></body></html>");
      ((FATBOYParam)specHash.get("DO_DARK_SUBTRACT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_DARK_SUBTRACT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform dark subtraction on all of your other images.  If <br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("DEFAULT_MASTER_DARK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_MASTER_DARK = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify filename(s) for default master dark(s) to be used if<br>the program can not find a matching master dark corresponding to an<br>object's exposure time.  This can be a single filename, a comma or space<br>separated list of filenames, or an '.dat'  ASCII text file containing<br>a list of FITS files, one per line.  By default it is left blank, which<br>means the user will be prompted for a master dark filename if the program<br>can not find<br>one.<br></body></html>");
      ((FATBOYParam)specHash.get("PROMPT_FOR_MISSING_DARK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">PROMPT_FOR_MISSING_DARK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>If there is no matching master dark or default master dark corresponding<br>to an object's exposure time, the default behavior is to prompt the user<br>for a file to use as a master dark.  If this flag is set to no, then<br>instead of prompting the user, the program automatically selects the<br>master dark (from either the list of ones it has created or from the list<br>of default master darks) with the exposure time nearest to that of the<br>object.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_FLAT_COMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_FLAT_COMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to combine your flats into master flats.  If this is set to<br>no, this step is skipped. <br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_SELECTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_SELECTION = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">all | auto | manual</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>The method of selecting frames for the flat fields can be one of three<br>things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = all</span>: All flat fields will<br>be combined into one master flat.  All objects will later be divided by this<br>one flat field.  If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD =<br>on-off</span> then <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_LAMP_OFF_FILES</span> must be<br>set to a filename fragment that can identify only the <i>off</i> flats<br>to separate them from the on flats (see below).<br><br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = auto</span>: The OBJECT_KEYWORD<br>will be used to group flat fields together and later to identify which master<br>flat should be used with which objects.  This method requires that the<br>OBJECT_KEYWORD be the same for the flat fields as it is in the objects they<br>correspond to.  For instance, if your objects, <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>ro05c2m2_jhjh.*.fits</span> have \"OBJECT = ro05c2m2\" in their headers, then<br>your flat fields, <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">ro05c2m2_jhjh_qtzoff.*.fits</span><br>and <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">ro05c2m2_jhjh_qtzon.*.fits</span> must both also<br>have \"OBJECT = ro05c2m2\" in their headers.  Once again, if<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD = on-off</span> then <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>FLAT_LAMP_OFF_FILES</span> must be set to a filename fragment that can<br>identify only the <i>off</i> flats to separate them from the on flats<br>(see below).<br><br><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = manual</span>: If neither of the<br>two automatic methods above applies (including if you have no filenaming<br>convention to differentiate between on and off flats if <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>FLAT_METHOD = on-off</span>), then use the manual method.  In this selection<br>method, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_LAMP_OFF_FILES</span> must be set to<br>a filename of an ASCII text file, manually listing which flats should<br>be combined and which ones apply to which objects.  See below for the<br>specifications of the format of this ASCII text file.<br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">on-off | on</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">on-off</span><br><br>In addition to the method of selecting frames, you must also specify<br>whether you have lamp on-lamp off or just lamp on flats. <br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD = on-off</span>: used when dome<br>flats have been taken with both the lamp on and the lamp off.  For each set<br>of flats, the lamp on flats are<br>combined to produce the master lamp on flat, then the lamp off flats are<br>combined to produce the master lamp off flat.  Finally, the master lamp<br>off flat is subtracted from the master lamp on flat to produce the master<br>flat (and the temporary files with the master lamp on and master lamp off<br>are destroyed).  <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> is used<br>to combine the images and each flat is scaled by the reciprocal of its<br>median pixel value then the scaled individual flats are median combined to<br>produce the master (lamp on or lamp off) flat.  See below for how lamp ons<br>are differentiated from lamp offs.  The master flat is then normalized by<br>dividing by its median pixel value.<br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD = on</span>: used when dome flats<br>have been taken just with the lamp on.  Each set of flats is combined to<br>produce a master flat using IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br>Each individual flat is scaled by the reciprocal of its median<br>pixel value and then the scaled individual flats are median combined to<br>produce the master flat.  The master flat is then normalized by dividing<br>by its median pixel value.<br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_LAMP_OFF_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_LAMP_OFF_FILES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">off</span><br><br>When using lamp on - lamp off flats, the FLAT_LAMP_OFF_FILES flag allows<br>you to specify which files are lamp off versus which are lamp on (since the<br>OBS_TYPE keyword in the header will just say flat).  <i>Additionally,<br>when <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = manual</span>, this parameter is<br>used to specify which flats correspond to which objects, even if <br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_METHOD = on</span>.</i><br>There are two ways to differentiate the lamp off flats from the lamp on<br>flats: by a naming convention or by specifying them in a separate ASCII text<br>file.  If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = all | auto</span> then<br>a naming convention must be used.  For instance, if your flat fields are<br>named <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">ro05c2m2_jhjh_qtzoff.*.fits</span> and<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">ro05c2m2_jhjh_qtzon.*.fits</span>, then you could enter <br>either \"qtzoff\" or just \"off\" or even \"ro05c2m2_jhjh_qtzoff\" (if this is<br>the only object you are processing) for FLAT_LAMP_OFF_FILES.  It must simply<br>be unique to <i>all</i> off flat filenames and not contained in any on<br>flat filenames.<br><br><br>If however, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_SELECTION = manual</span>, this<br>parameter should be set to an ASCII text file.<br>The file should have the format:<pre><br>obj_prefix  start_num  stop_num   on_prefix  start stop  off_prefix  start  stop</pre><br>Where obj_prefix is the filename prefix of the object that the master flat<br>will correspond to, on_prefix is the prefix of the \"on\" flats, off_prefix<br>is the prefix of the \"off\" flats, and the number of leading and trailing<br>zeros in the start and stop numbers does not matter.  For example:<pre><br>ro05c2m2   1   12    ro05c2m2_jhjh_qtzon   1   5   ro05c2m2_jhjh_qtzoff   1  5<br></pre> would use files named *ro05c2m2_jhjh_qtzon*.N.fits, where N is between<br>1 and 5 (or 001 and 005, etc.), inclusive, as lamp on flats and<br>files named *ro05c2m2_jhjh_qtzoff*.N.fits, where N is between 1 and 5, as<br>lamp off flats.  This master flat would then be associated with objects<br>with filenames *ro05c2m2*.N.fits, where n is between 1 and 12 (or 001 and<br>012, etc).  If you only have on flats, simply omit the last three columns:<br><pre><br>ro   1   12   qtzon   1   5</pre><br>would use files named *qtzon*.N.fits, where N is between 1 and 5, as lamp on<br>flats and combine them into a master flat, which would be associated<br>with the object files *ro*.N.fits, where N is between 1 and 12.<br>The file can have as many lines as necessary of the same format to specify<br>all flats.<br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_LOW_THRESH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_LOW_THRESH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br><i>Only applies to on-off flats</i>.  When doing on-off flats, this lets<br>you select a low threshold so that any value below this threshold in<br>any on-off flat will be replaced with a user-specified value.  <i>Note that<br>0, the default value, means that this feature is disabled.  It must be set<br>to a non-zero value.  Also, the threshold is applied after the<br>master flat is normalized.</i><br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_LOW_REPLACE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_LOW_REPLACE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br><i>Only applies to on-off flats</i>.<br>If using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_LOW_THRESH</span>, this is the value<br>that pixels below that threshold will be changed to.  Note that this is<br>done post-normalization, so 1 would set a pixel to the median value. <br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_HI_THRESH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_HI_THRESH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br><i>Only applies to on-off flats</i>.  When doing on-off flats, this lets<br>you select a high threshold so that any value above this threshold in<br>any on-off flat will be replaced with a user-specified value.  <i>Note that<br>0, the default value, means that this feature is disabled.  It must be set<br>to a non-zero value.  Also, the threshold is applied after the<br>master flat is normalized.</i><br></body></html>");
      ((FATBOYParam)specHash.get("FLAT_HI_REPLACE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">FLAT_HI_REPLACE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br><i>Only applies to on-off flats</i>.<br>If using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FLAT_HI_THRESH</span>, this is the value<br>that pixels above that threshold will be changed to.  Note that this is<br>done post-normalization, so 1 would set a pixel to the median value.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_ARCLAMP_COMBINE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_ARCLAMP_COMBINE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to combine any arclamps into master arclamps.  If this is set<br>to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("ARCLAMP_SELECTION")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ARCLAMP_SELECTION = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto | file.dat</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>The method of selecting frames for the arclamps can be one of two<br>things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ARCLAMP_SELECTION = auto</span>: The OBJECT_KEYWORD<br>will be used to group arclamps together and later to identify which master<br>arclamp should be used with which objects.  This method requires that the<br>OBJECT_KEYWORD be the same for the arclamps as it is in the objects they<br>correspond to.  For instance, if your objects, <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>ro05c2m2_jhjh.*.fits</span> have \"OBJECT = ro05c2m2\" in their headers, then<br>your arclamps, <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">ro05c2m2_jhjh_henear.*.fits</span><br>must also have \"OBJECT = ro05c2m2\" in their headers. <br><br><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ARCLAMP_SELECTION = file.dat</span>: If the<br>automatic method does not apply then use the manual method.  In this selection<br>method, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ARCLAMP_SELECTION</span> must be set to<br>a filename of an ASCII text file, manually listing which arclamps should<br>be combined and which ones apply to which objects.  The file should have the<br>format:<pre><br>obj_prefix   start_num   stop_num   lamp_prefix   start   stop<br></pre><br>Where obj_prefix is the filename prefix of the object that the master<br>arclamp will correspond to, lamp_prefix is the prefix of the arclamp files,<br>and the number of leading and trailing zeros in the start and stop numbers<br>does not matter. For example:<br><pre><br>r3c1m2z   1   8   henear   1   5<br></pre><br>would use files named *henear*.N.fits, where N is between 1 and 5, as<br>arclamps and combine them into a master arclamp, which would be associated<br>with the object files *r3c1m2z*.N.fits, where N is between 1 and 8.  The<br>file can have as  many lines as necessary of the same format to specify all<br>arclamps.<br></pre><br></body></html>");
      ((FATBOYParam)specHash.get("REMOVE_COSMIC_RAYS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REMOVE_COSMIC_RAYS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Whether or not to perform cosmic ray removal.<br></body></html>");
      ((FATBOYParam)specHash.get("COSMIC_RAY_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">COSMIC_RAY_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">10</span><br><br>The sigma clipping threshold for identifying cosmic rays.  The default is<br>10 sigma, which ensures that no actual data (e.g., emission lines)<br>are removed.<br></body></html>");
      ((FATBOYParam)specHash.get("COSMIC_RAY_PASSES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">COSMIC_RAY_PASSES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>The number of passes to perform if cosmic ray removal is turned on.<br>It is strongly recommended to keep this at 1 pass, particularly for MOS<br>data.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_BAD_PIXEL_MASK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_BAD_PIXEL_MASK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to created a bad pixel mask.  If this is set to no, this<br>step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("DEFAULT_BAD_PIXEL_MASK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_BAD_PIXEL_MASK = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify a filename for a default bad pixel mask (a FITS image<br>where bad pixels are 1 and everything else is 0).  If specified, this<br>bad pixel mask will be used instead of creating one(s) from flat fields or<br>darks.  By default, this is left blank.<br></body></html>");
      ((FATBOYParam)specHash.get("IMAGING_FRAMES_FOR_BPM")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IMAGING_FRAMES_FOR_BPM = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>An ASCII text file containing a list of dark frames or flat fields to use<br>to create a bad pixel mask, defined by the parameters listed below. <br></body></html>");
      ((FATBOYParam)specHash.get("BAD_PIXEL_MASK_CLIPPING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_MASK_CLIPPING = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">values | sigma</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">values</span><br><br>This flag specifies whether value thresholds or sigma clipping will be used<br>to determine bad pixels.  For flat fields, especially those that have been<br>normalized, values is recommended.  For darks, sigma clipping is recommended.<br>In values, everything below a lower threshold and above an upper threshold is<br>treated as a bad pixel.  In sigma, all points farther than n*sigma from the<br>mean are thrown out as bad pixels, and then the mean and standard deviation<br>are recalculated for the remaining pixels.  This is done iteratively until<br>the standard deviation converges.  Once this final standard deviation is found,<br>everything more than n*sigma away from the median is treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)specHash.get("BAD_PIXEL_HIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_HIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2.0</span><br><br>The high threshold for value clipping.  Anything above this value will be<br>treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)specHash.get("BAD_PIXEL_LOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_LOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0.5</span><br><br>The low threshold for value clipping.  Anything below this value will be<br>treated as a bad pixel.<br></body></html>");
      ((FATBOYParam)specHash.get("BAD_PIXEL_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">BAD_PIXEL_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>The number of standard deviations away from the median that a point must be<br>to be treated as a bad pixel when sigma clipping is used (i.e., n in <br>n*sigma above).<br></body></html>");
      ((FATBOYParam)specHash.get("EDGE_REJECT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EDGE_REJECT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>The number of rows and columns around the edges to be thrown out as bad<br>pixels automatically.  For instance if you have a 2048x2048 image and set<br>this to 5, everything outside of the central 2038x2038 will be treated as<br>bad pixels.<br></body></html>");
      ((FATBOYParam)specHash.get("RADIUS_REJECT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">RADIUS_REJECT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>A radius outside of which every pixel will be thrown out as bad pixels<br>automatically.  For instance if you have a 2048x2048 image and set this to<br>1000, everything where sqrt((x-1024)^2+(y-1024)^2) &gt; 1000 is rejected.  If<br>set to 0, nothing is rejected.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_SKY_SUBTRACT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_SKY_SUBTRACT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform sky subtraction on all of your other images.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_SUBTRACT_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_SUBTRACT_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">median | dither | step | offsource_dither | offsource_multi_dither | file.dat</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">dither</span><br><br>The method of sky subtraction can be one of four things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = median</span>:<br>This method used IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> to median combine<br>several other frames (with the same prefix) and create a master sky, which is<br>then subtracted from the current frame.  It is very similar to the way skies<br>are done in imaging.  This is also the only method that uses the following<br>paramters: <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_REJECT_TYPE, SKY_NLOW, SKY_NHIGH,<br>SKY_LSIGMA, SKY_HSIGMA, USE_SKY_FILES,</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_FILES_RANGE </span>.  The first 5 of those parameters are simply passed<br>to <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>, while the last two control how<br>many other frames and which ones will be used to create the master sky.<br>If for some reason, there is only one frame of an object, a warning message<br>is printed and that object is not processed.  The master sky is scaled by<br>the ratio of the median pixel value of the object frame to the median value<br>of the master sky and then subtracted from the object frame.  Using this<br>method, you will have the same number of sky subtracted frames as you <br>began with and these frames will <i>not</i> be double subtracted.<br><br><p><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = dither</span>:<br>This method should be used if you used any kind of dither pattern, whether<br>A-B, A-B-C, or any other kind.  For each image, all other images with<br>the same prefix are selected.  Out of these, the first image at a different<br>position is selected.  This is determined by the parameter<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_DITHERING_RANGE</span>, which lists the minimum<br>separation (in arcsec, for <i>either</i> RA or Dec) for two images to be<br>considered to be at different positions in the dither pattern.  These two<br>images are then paired up and flagged so that neither will be selected again.<br>They are then subtracted from each other to form the sky subtracted image<br>(and their noisemaps added in quadrature if noisemaps being generated).<br>As long as there are an even number of frames, you will end up with half<br>as many output sky subtracted frames as you had to begin with (i.e., 1-2,<br>3-4, and so on).  If there are an odd number of frames (for instance if<br>one frame got thrown out during initialization), then you have the option<br>of A) ignoring the odd frame or B) subtracting a frame at a different position<br>(which will now have been used twice) and simply using only the positive<br>of the sky subtracted frame (i.e. not performing double subtraction on that<br>frame).  This is controlled by the parameter <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>IGNORE_ODD_FRAMES</span>.<br><p><br>For example, say you have an A-B dither pattern<br>with 8 frames, where the odd frames are A frames and the even frames are B<br>frames.  Now suppose that frame 5 got thrown out due to a bad read.  The<br>algorithm will first pair up frames 1 and 2 and output 1-2 (an A-B frame).<br>Then it will match up 3 and 4 and output 3-4.  Since frame 5 no longer exists,<br>the next frame it will come across is frame 6 (a B frame).  It will pair this<br>up with frame 7, an A frame and produce 6-7 (a B-A frame).  Frame 8 is then<br>left by itself since all the other frames have been paired off and sky<br>subtracted.  In case A) frame 8 is simply ignored and you will have 3<br>sky subtracted images that will continue to be processed.  In case B) frame<br>8 will be matched up with frame 7, producing 8-7 (a B-A frame).  But this<br>frame will be flagged so that only it will not be double subtracted and only<br>the positive spectra will be used, ensuring that frame 7 is not used twice.<br><p><br>This method works equally well for 3 or more point dither patterns as well.<br>Take for example, an A-B-C dither pattern with 9 total frames (1, 4, 7 are<br>A frames; 2, 5, and 8 are Bs; and 3, 6, and 9 are Cs).  The algorithm will<br>first match up 1 and 2 and produce 1-2 (an A-B frame).  Then it will pair<br>up 3 and 4 and output 3-4 (a C-A frame).  Next, 5-6 (a B-C) and 7-8<br>(A-B) are produced.  Finally, frame 9 is by itself so it can be discarded,<br>or 9-8 (a C-B) can be produced but flagged so it will not be double subtracted. <br><p><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = step</span>:<br>This method should be used if you stepped the observations down the slit,<br>with each frame at a different position, rather than performing a dither<br>pattern.  This is much more common with longslit data than with MOS (because<br>the slits are not typically very long in MOS).  First, the images are grouped<br>together: all images with the same prefix are selected.  If there are an<br>even number of frames with that prefix, then the frames will be paired up<br>and subtracted as: 1-2, 3-4, ..., (n-1)-n.  Thus, you will end up with half<br>as many output sky subtracted frames as you had to begin with and these<br>frames will be double subtracted later.  However, if there are an odd<br>number of frames, they will be matched up and subtracted as:<br>1-2, 2-3, ..., (n-1)-n, n-1.  Thus, you will end up with the same number<br>of frames as you began with and these frames will <b>not</b> be double<br>subtracted later.<br><p><br>4) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_dither</span>:<br>This method should be used if you used off-source skies with an A-B-B-A or<br>A-B-A-B dither pattern (where A's are on-source and B's are skies).  Each<br>on-source frame will be matched up with an off-source sky (either<br>automatically or manually, see <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_OFFSOURCE_METHOD</span>).  If there is an odd sky frame, it is ignored.<br>If there is an odd object frame, the behavior is set by the parameter<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">IGNORE_ODD_FRAMES</span>.  Either the frame will be<br>ignored or the closest sky frame to it will be subtracted from it.<br><p><br>5) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_multi_dither</span>:<br>This method should be used if you used a dither pattern with off-source skies<br>but took several short exposures at each point.  For instance, 5 A's then<br>5 B's then 5 more A's then 5 more B's.  In this case, each group of short<br>on-source exposures will be median combined into one image, matched up with<br>the corresponding off-source skies (also median combined into one image), and<br>then the two will be subtracted.  Groups of images are defined either<br>automatically: consecutive frames with <i>exactly</i> the same RA and Dec in<br>the image headers, or manually.<br><p><br>6) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = file.dat</span>:<br>The last option is to give an ASCII text file, which specifies the<br>sky subtraction method for each dataset.  The file should have the format:<br><pre><br>prefix	method</pre><br>where prefix is the filename prefix (or a fragment of it) in the dataset<br>and method is median, dither, or step.  For example, if you have three<br>longslit data sets: hd25035*.*.fits, hd37414*.*.fits, and hd59488*.*.fits and<br>one MOS dataset: lkha1021*.*.fits and the longslit datasets all used step and<br>the MOS dataset used a dither pattern, you could setup your file:<pre><br>hd		step<br>lkha1021	dither</pre><br>Alternatively, you could use 4 lines and give the entire prefixes for the<br>longslit datasets but there is no need as they all begin with \"hd\", which<br>is found nowhere in the prefix of the MOS dataset.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_REJECT_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_REJECT_TYPE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">sigclip</span><br><br>This controls the rejection type to be used by IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\"><br>imcombine</span> when creating master skies.  See the help file for<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> for details on options for rejection<br>types.  Only used for the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_NLOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_NLOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span><br>as the parameter nlow when creating master skies. nlow is used for the<br>minmax rejection type by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br>Only used for the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_NHIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_NHIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter nhigh when<br>creating master skies.  nhigh is used for the minmax rejection type by<br><span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br>Only used for the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_LSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_LSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter lsigma when<br>creating master skies. lsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br>Only used for the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_HSIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_HSIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>This parameter will be passed to IRAF's <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span> as the parameter hsigma when<br>creating master skies. hsigma is used for the sigma clipping rejection types,<br>including sigclip, by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">imcombine</span>.<br>Only used for the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("USE_SKY_FILES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">USE_SKY_FILES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">all | range</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">all</span><br><br>When using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = median</span>,<br>this parameter controls whether all other objects in the same dataset will<br>be combined into the master sky for each individual object, or just a range<br>of objects before and after the current one.  For instance, if you are<br>processing frame number 5 out of 10 and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES =<br>all</span>, frames 1-4 and 6-10 will be combined to create the master sky<br>for frame number 5.  However if you set it to range, only a few selected<br>frames contribute to the master sky (see <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_FILES_RANGE<br></span>, below).  This keyword only applies to the median method of sky<br>subtraction. <br></body></html>");
      ((FATBOYParam)specHash.get("SKY_FILES_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_FILES_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES = range</span>, this parameter controls<br>how many files will contribute to each master sky.  Using the example above,<br>if <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">USE_SKY_FILES = range</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_FILES_RANGE = 3</span> then for frame number 5, frames 2-4 and 6-8 will<br>be combined to create the master sky.  For <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_FILES_RANGE = n</span>, it will use the n frames before and n after the<br>current frame to create the master sky for that image.  In cases where there<br>are not n before or n after (say frame #1 for example), it will use the<br>closest 2*n frames (i.e. for frame #2, it would use 1 and 3-7 for the master<br>sky).  This keyword only applies to the median method of sky subtraction.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_DITHERING_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_DITHERING_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>This parameter sets the minimum separation (in arcsec, for <i>either</i><br>RA or Dec) for two frames to be considered to be at different positions<br>in the dither pattern.  This parameter can be used for the median method of <br>sky subtraction: if a frame is within n arcsec (in both RA and Dec) of<br>the current image, it is not used in calculating the sky.  In this way,<br>it is ensured that even if several frames are taken in the same location,<br>the spectra themselves will not be included in the medianed master sky<br>frame.  For example, if you select the median method with a 5-point dither<br>pattern where frames 1-3 are at position A, 4-6 B, 7-9 C, 10-12 D, and 13-15<br>E and select to use <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_FILES_RANGE = 3</span> (meaning<br>use the 3 frames before and 3 after the current frame to produce the master <br>sky).  When processing frame #11, 10 and 12 are at the same position and<br>will not be included.  Thus, frames 7-9 and 13-15 will be used to create the<br>master sky.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_OFFSOURCE_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto | manual</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>If offsource skies are used (see above), this parameter controls the<br>method of determining which frames are on-source and which are offsource.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_OFFSOURCE_RANGE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_RANGE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">240</span><br><br>When using automatic detection for offsource skies, this parameter controls<br>how far away from the first frame, in arcsec, a frame must be to be considered<br>a sky.  The default value is 240 arcsec (4 arcmin).  So if your objects<br>and skies have the same prefix (i.e., orion.00*.fits), then orion0001.fits<br>will be assumed to be on-source as will every frame within 240 arcsec of<br>orion0001.fits in <i>both RA and Dec</i>.  Every frame orion.00*.fits that<br>is more than 240 arcsec away from orion0001.fits in either RA or Dec will<br>be assumed to be an off-source sky.<br></body></html>");
      ((FATBOYParam)specHash.get("SKY_OFFSOURCE_MANUAL")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SKY_OFFSOURCE_MANUAL = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>When using manual detection for offsource skies, this parameter gives an<br>input ASCII text file that specifies which frames are objects, which are sky,<br>and which skies correspond to which objects.  For <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>SKY_SUBTRACTION_METHOD = offsource_dither</span>, the file should be two <br>columns, listing the filenames of the on-source frames and the filenames<br>of their corresponding off-source skies:<pre><br>On-Source_Frame_List	Sky_Frame_List<br></pre><br>For <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">SKY_SUBTRACTION_METHOD = offsource_multi_dither</span>,<br>the file should have this format:<pre><br>On-source prefix     start number     stop number     Sky prefix     start number     stop number</pre><br>For example:<pre><br>trap_h		1	5	trap_h		6	10</pre><br>would use files named trap_h*.x.fits, where x is between 1 and 5 (with<br>or without leading zeros), inclusive, as on-source frames and median combine<br>them.  Files named trap_h*.x.fits, where x is between 6 and 10 will be<br>treated as offsource skies, median combined, and then subtracted from the<br>on-source image.  In either case, the ASCII text file can have as many lines<br>as necessary of the same format to specify all pairs of objects and offsource<br>skies.<br></body></html>");
      ((FATBOYParam)specHash.get("IGNORE_ODD_FRAMES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">IGNORE_ODD_FRAMES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>This parameter is only used for the dither method of sky subtraction and<br>only when there are an odd number of frames for an object.  If this<br>parameter is set to yes (as it is by default), any leftover odd frames<br>will be ignored (note that there can be more than one odd frame leftover.<br>For instance, if you have an A-B dither pattern with 8 frames, but two<br>of the B frames were thrown out during initialization, then it will only<br>be able to match up the remaining 2 B frames with the first 2 A frames,<br>leaving 2 A frames that cannot be subtracted from each other).  If this<br>parameter is set to no, these leftover odd frames will be processed by<br>subtracting a frame at a different position, which will now have been used<br>more than once, and simply using only the positive spectra in that sky<br>subtracted frame later on (i.e. not performing double subtraction).  In the<br>previous example, the two valid B frames would be again subtracted, this<br>time from the two leftover A frames, but double subtraction would only<br>be performed on the first two A-B frames and not the last two.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_RECTIFY")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_RECTIFY = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to rectify your images.  If set to no, this step is skipped.<br>If you have MOS data and just wish to rectify the sky lines and not the<br>continua, select yes for this parameter and just leave <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>MOS_RECT_DB</span> blank.<br></body></html>");
      ((FATBOYParam)specHash.get("DATA_TYPE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DATA_TYPE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">longslit | mos | file.dat</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">longslit</span><br><br>At some points, the pipeline will branch out to different methods depending<br>on if it is working on longslit or MOS data.  This parameter specifies<br>if your data is one or the other.  There is a third option to pass it<br>an ASCII text file listing whether each dataset is longslit or MOS.  In<br>this way, you are able to process longslit and MOS data in the same run<br>(which can be very useful if later on, your objects are MOS but your<br>calibration star is longslit).  The file should have the format:<pre><br>prefix	longslit_or_mos<br></pre> where prefix is the filename prefix (or a fragment of it) in the<br>dataset.  For example, if you had three longslit data sets: hd25035*.*.fits,<br>hd37414*.*.fits, and hd59488*.*.fits and one MOS dataset: lkha1021*.*.fits,<br>you could setup your file:<pre><br>hd		longslit<br>lkha1021	mos</pre><br>Alternatively, you could use 4 lines and give the entire prefixes for the<br>longslit datasets but there is no need as they all begin with \"hd\", which<br>is found nowhere in the prefix of the MOS dataset. <br></body></html>");
      ((FATBOYParam)specHash.get("LONGSLIT_RECT_COEFFS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">LONGSLIT_RECT_COEFFS = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">rect.dat</span><br><br>If processing longslit data, this specifies the filename of the coefficient<br>file output from <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">rectify.py</span> that will be passed<br>to <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">drizzle</span>.  It can also accept an ASCII text<br>file listing object prefixes and coefficient files for each if you are<br>processing multiple sets of longslit data with different rectification<br>transformations.  Using the same example as above, your ASCII text file<br>could be setup as:<pre><br>hd25035		rect_hd25035.dat<br>hd37414		rect_hd37414.dat<br>hd59488		rect_hd59488.dat</pre>.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_RECT_DB")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_RECT_DB = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If processing MOS data, this specifies the filename of the rectification<br>database produced by <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">rectifyMOS.py</span> that will<br>be used to calculate the rectification transformation.  Only one database<br>can be used, so you can not process multiple sets of MOS data in the<br>same run unless they share the same region file and rectification<br>transformation.  If this is left blank, then the continuum rectification<br>is defined to be y_out = y_in but the skylines are still rectified.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_RECT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_RECT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The order of the 3-d polynomial y_out = f(x_in, y_in, x_slit) that will<br>be fit to the rectification databased and used to apply the rectification<br>transformation.  The default is 3rd order (21 terms), which should be<br>sufficient for most data.  If it is not, try 4th order.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_REGION_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_REGION_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>The <span style=\"font: 16pt arial,helvetica,sans-serif; color: #22aa22;\">ds9</span> style region file that specifies the<br>locations of the slits in your MOS data.  For an example of a region file,<br><a href=\"lk101f21.reg_2.reg\">click here</a>.  The first four coordinates<br>for the box are x_center, y_center, width, and heighet.  Only one region<br>file can be used, so you can not process multiple sets of MOS data in the<br>same run unless they share the same region file and rectification<br>transformation.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_YSLIT_BUFFER_LOW")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_YSLIT_BUFFER_LOW = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This can be set if some continua in your image are at the extreme bottom<br>of some slits and/or if some slits overlap.  When extracting slits to<br>perform the rectification, it will pad bottom of the slit by this many pixels.<br>Thus, there will likely be some overlap in the rectified image where<br>some pixels in the input image are output into two different slits (this<br>data should be thrown away during the double subtraction anyway, unless it<br>contains your continuum).  It should not generally be necessary to set<br>this parameter.  If it is left blank, it defaults to 0.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_YSLIT_BUFFER_HIGH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_YSLIT_BUFFER_HIGH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>This can be set if some continua in your image are at the extreme top <br>of some slits and/or if some slits overlap.  When extracting slits to<br>perform the rectification, it will pad top of the slit by this many pixels.<br>Thus, there will likely be some overlap in the rectified image where<br>some pixels in the input image are output into two different slits (this<br>data should be thrown away during the double subtraction anyway, unless it<br>contains your continuum).  It should not generally be necessary to set<br>this parameter.  If it is left blank, it defaults to 0.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_MAX_SLIT_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_MAX_SLIT_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">10</span><br><br>The maximum slit width -- anything wider than this in the region file will<br>be considered a guide star box and will be thrown out during rectification.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_RECT_USE_ARCLAMPS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_RECT_USE_ARCLAMPS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Whether or not to use any master arclamps corresponding to your object, if<br>available, to find and trace out lines to calculate the rectification<br>transformation (instead of using skylines).  This is set to no by default,<br>but can be useful on short exposures in which not many skylines will be<br>found.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_SKYLINE_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_SKYLINE_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>For MOS data, skylines must be at least this many sigma brighter than the<br>median value of the smoothed 1-d cut to be selected.  The recommended value<br>(and default) is 3 sigma.  Regardless of what value is set here, the<br>brightest skyline in each quarter of the 1-d cut will be used if<br>the two pass method of skyline detection is selected (see below).<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_SKY_TWO_PASS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_SKY_TWO_PASS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to use a two-pass method of detecting skylines.  In the first<br>pass, the median and sigma are computed for the 1-d cut of the entire slit<br>and lines above the sigma threshold are selected. If this parameter is set to<br>yes, then a second pass is also done in which each quarter of the 1-d cut is<br>examined on its own.  The median and sigma are then computed separately for<br>each section and in this way additional lines are found in the fainter regions<br>of the slit.  Also, the brightest line in each section is always kept even if<br>it falls below the specified sigma threshold, providing better coverage of<br>the full slit in the x-direction. <br></body></html>");
      ((FATBOYParam)specHash.get("MOS_SKY_BOX_SIZE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_SKY_BOX_SIZE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">6</span><br><br>The half-width, in pixels, of the box size to use for rejection criteria<br>and when fitting Gaussian profiles to trace out the skylines (see above).<br><i>The recommended value is 6 and should not be changed unless the skylines<br>exhibit an extremly large curvature.</i><br></body></html>");
      ((FATBOYParam)specHash.get("MOS_SKY_FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_SKY_FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>The order of the 2-d polynomial used to fit the skylines in each slit:<br>x_out = f(x_in, y_in).  A 2nd order polynomial (6 terms) is used by default<br>because the slits are generally not very tall and we are thus dealing with<br>a relatively small amount of curvature compared to longslit data where the<br>skylines span the entire image.  However, if 2nd order does not provide<br>a good enough fit, try 3rd order.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_DOUBLE_SUBTRACT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_DOUBLE_SUBTRACT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform double subtraction on the applicable images.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("SUM_BOX_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SUM_BOX_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">25</span><br><br>The width of the box to be used when compressing the positive and negative<br>frames to 1-d cuts by taking the median value of each row.  These 1-d cuts<br>are then cross-correlated with each other to find the shift between them<br>(the amout that was dithered by).<br></body></html>");
      ((FATBOYParam)specHash.get("SUM_BOX_CENTER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">SUM_BOX_CENTER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1024</span><br><br>The central x-pixel of the box to be used when compressing the positive and<br>negative frames to 1-d cuts by taking the median value of each row.  These<br>1-d cuts are then cross-correlated with each other to find the shift between<br>them.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_SHIFT_ADD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_SHIFT_ADD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to shift and add the frames together.  If <br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("MASK_OUT_TROUGHS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MASK_OUT_TROUGHS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to mask out some of the darker troughs (negative spectra)<br>in the images when shifting and adding them.  Note that A) faint troughs<br>are unlikely to be able to be removed and B) the dark troughs are removed<br>for aesthetic reasons only.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_FLAT_DIVIDE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_FLAT_DIVIDE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform flat division on all of your other images.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("DEFAULT_MASTER_FLAT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DEFAULT_MASTER_FLAT = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Allows you to specify filenames for a default master flats for different<br>datasets.  This parameter can be set to one of two things: A) a FITS<br>file, which will be used as the master flat field for all frames in<br>all datasets; or B) an ASCII text file <i>with a filename ending in .dat</i><br>that contains a 2-column list of prefixes and filenames of default master<br>flats.  For example:<pre><br>lkha1021	masterFlats/default_lkha1021_flat.fits<br></pre><br>This allows you to specify a default master flat for each dataset. <br></body></html>");
      ((FATBOYParam)specHash.get("DO_MOS_ALIGN_SKYLINES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_MOS_ALIGN_SKYLINES = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to align the skylines between all slits in MOS data.  If<br>set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("REFERENCE_SLIT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REFERENCE_SLIT = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>As mentioned above, the slit with the central x_slit position in the region<br>file is assigned to be the reference slit.  That can be changed by this<br>parameter.  If this parameter is left blank (the default value) or set to -1,<br>then as usual, the slit with the central x_slit position will be the <br>reference slit.  It can however also be manually set to any slit number:<br>the bottom slit is slit #0, the one above it is slit #1 and so on.  Note that<br>guide star boxes should be ignored when counting the slits.  Finally, this<br>parameter can be set to <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">prompt</span>, in which case the<br>program will pause when it gets to this point, display the filename of the<br>arclamp or clean sky image to be \"jailbarred\", and prompt the user for a slit<br>number.  The user can then open the image in their favorite viewer and select<br>a slit number.<br></body></html>");
      ((FATBOYParam)specHash.get("NEBULAR_EMISSION_CHECK")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">NEBULAR_EMISSION_CHECK = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>In rare cases, your data may have nebular emission lines that appear in<br>some slits but not in others and may interfere with aligning the slits <br>properly.  In most cases, this should not actually cause a problem since <br>we are cross-correlating over a very narrow region of each slit and have<br>a very good initial guess as to the offset between the slits at the position<br>of each particular sky line.  However, a 101-pixel wide box is used to refine<br>the initial guesses from the region file for the brightest (and thus first<br>to be found) line in each segment (see above).  This means that bright nebular<br>emission within 50 pixels of the brightest sky line can cause problems.<br>If you believe this to be the case with your data, turn on this parameter.<br>Instead of just finding the brightest line within the 101-pixel box centered<br>at the initial guess for the corresponding line's location, the three<br>brightest lines in this box are found.  A linear fit is then made to the<br>offsets of previous lines (from other segments) found in the same slit as<br>a function of line position in the reference slit.  The line location that<br>best fits this linear fit (has the lowest standard deviation of residuals<br>when plugged into the series) is chosen to be the correct line.  Of course,<br>if the nebular emission is in your reference slit, things will still not<br>work out -- this is the reason for the parameter <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>REFERENCE_SLIT</span>, above.<br></body></html>");
      ((FATBOYParam)specHash.get("REVERSE_ORDER_OF_SEGMENTS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">REVERSE_ORDER_OF_SEGMENTS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Notice in the nebular emission check that a linear fit to the lines found in<br>previous segments is used to help select the correct sky line instead of<br>a nebular emission line.  Therefore, this method can not be used on the first<br>line of the first segment.  You must have at least one segment that has<br>already been properly aligned.  Generally, two segments will be used<br>(i.e., one for H and one for K or one for J and one for H).  As long as one<br>of those segments is good, they can be processed in either order so that<br>the good segment can be used in the nebular emission check to help align<br>the bad segment.  The normal order is to process the segments left to right.<br>This keyword can reverse that so that they are processed right to left.<br></body></html>");
      ((FATBOYParam)specHash.get("MEDIAN_FILTER_1D")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MEDIAN_FILTER_1D = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Sometimes the background level can be very bright relative to the sky lines.<br>In that case, a median filter can be used to subtract out the background<br>and bring out the sky lines better.  This will not be needed when using<br>arclamps to \"jailbar\" and wavelength calibrate but can be useful when using<br>OH lines from a \"clean sky image\".  The median of a 51-pixel wide box is<br>subtracted from each pixel.  If set to yes, this median filter will be<br>applied both during \"jailbarring\" and during <a href=\"#17\">wavelength<br>calibration</a>.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_ALIGN_USE_ARCLAMPS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_ALIGN_USE_ARCLAMPS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Whether or not to use any master arclamps corresponding to your object, if<br>available, to align the skylines in all the slits.  If set to yes, lines<br>will be found in the arclamps, if available, and used to calculate the<br>transformation for each slit.  If set to no, the \"clean sky image\" will<br>be used, as normal.  If set to yes, but there is no arclamp for a dataset,<br>the \"clean sky image\" will be used as normal. <br></body></html>");
      ((FATBOYParam)specHash.get("MOS_ALIGN_NLINES")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_ALIGN_NLINES = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">25</span><br><br>The maximum number of lines (per segment) to attempt to match up.  This<br>should be a whitespace separated list if you desire to split the frame<br>into more than one segment during this step.  For example, in jh data, the<br>skylines in h (on the right side) are so much brighter than the ones in j<br>(the left side) that the 25 brightest skylines would all be in h, on the<br>right side.  Thus, your fit to the skyline offsets would be hugely extrapolated<br>to the left side and would probably not be very good.  Therefore, in order<br>to get better wavelength coverage, split the frame into two segments<br>(the division between the two will be right in the middle).  By entering,<br>for example, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">MOS_ALIGN_NLINES = 10 30</span>, the frame<br>is split into two segments with up to 10 lines being found in the first one<br>(the j-band or left half) and up to 30 in the second one (the h-band or<br>right half).<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_ALIGN_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_ALIGN_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>The minimum local sigma threshold for a line.  If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>MOS_ALIGN_NLINES</span> is a whitespace separated list defining more than<br>one segment, then this too should be a whitespace separated list.  In the<br>example above, you may want to set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">MOS_ALIGN_SIGMA =<br>2 3</span>.  The default sigma threshold of 2 would be suitable for the<br>faint lines in the j-band, but for the bright lines of the h-band you can<br>easily move this threshold up to 3 sigma and still find as many lines as<br>you need.<br></body></html>");
      ((FATBOYParam)specHash.get("MOS_ALIGN_FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">MOS_ALIGN_FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The order of the polynomial x_out = f(x_in) to fit to the list of line<br>positions in each slit in order to transform them to match the line positions<br>in the reference slit.  The default is to fit a 3rd order polynomial, which<br>should generally be sufficient.  However, if your residuals seem to have<br>sigmas of more than 0.2-0.3 you might want to try 4th or 5th order.  It<br>is not recommended to go above 5th order since you are likely fitting only<br>20-30 datapoints.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_WAVELENGTH_CALIBRATE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_WAVELENGTH_CALIBRATE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to perform a wavelength calibration on your data.  If set<br>to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("WAVELENGTH_CALIBRATE_USE_ARCLAMPS")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">WAVELENGTH_CALIBRATE_USE_ARCLAMPS = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">no</span><br><br>Whether or not to use any master arclamps corresponding to your object, if<br>available, to perform the wavelength calibration.  If set to yes, lines<br>will be found in the arclamps, if available, and matched up with the<br>corresponding lines from the \"sky lines file\" for each dataset to produce<br>a wavelength calibration.  If set to no, the \"clean sky image\" will<br>be used, as normal.  If set to yes, but there is no arclamp for a dataset,<br>the \"clean sky image\" will be used as normal.<br></body></html>");
      ((FATBOYParam)specHash.get("CALIBRATION_INFO_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CALIBRATION_INFO_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>The calibration info file is a very important file that contains 7 or 8 columns<br>per line:<br><br><b>Option A: </b><br>1) the grism; 2) the filter; 3 and 4) two wavelengths (not pixel<br>values) within the frame; 5) the number of pixels between these two<br>wavelengths in the frame; 6) the approximate minimum wavelength of the<br>entire frame; 7) the approximate maximum wavelength of the entire image;<br>and 8) the sky lines file. <br><br><b>Option B: </b><br>1) a filename prefix or the beginning fragment of a prefix for a particular<br>dataset; 2 and 3) two wavelengths (not pixel values) within the frame; 4)<br>the number of <i>pixels</i> between these two wavelengths in the frame; 5)<br>the approximate minimum wavelength of the entire frame; 6) the approximate<br>maximum wavelength of the entire image; and 7) the sky lines file.<br></body></html>");
      ((FATBOYParam)specHash.get("ANGSTROMS_PER_UNIT")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">ANGSTROMS_PER_UNIT = </span><br><br><br><b>Default Value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">1</span><br><br>Since sky lines files may be in different units (see above), this parameter specifies the conversion between the units specified and Angstroms so that the dummy array can be created with one pixel per angstrom and then downsized (it would clearly not work to create an array of size 3 and try to upscale the size to match the size of the 1-d median of the image).  For instance, if your calibration info file was setup as follows:<pre><br>HK      HK      1.66916 1.58326 100     1.3500   2.2500   hklines_mod.dat</pre><br>hklines_mod.dat will be used to wavelength calibrate HK data.  This file clearly uses microns as its units.  Therefore <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ANGSTROMS_PER_UNIT = 10000</span>.  For the JH example above, <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">ANGSTROMS_PER_UNIT = 1</span>.<br></body></html>");
      ((FATBOYParam)specHash.get("CALIBRATION_MIN_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CALIBRATION_MIN_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The minimum local sigma threshold for a line to be kept.  The default value<br>is 3 sigma although for frames with very faint skylines you may want to<br>try 2 sigma.<br></body></html>");
      ((FATBOYParam)specHash.get("CALIBRATION_FIT_ORDER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CALIBRATION_FIT_ORDER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">3</span><br><br>The order of the polynomial wavelength = f(x_in) to fit to the list of<br>line positions in order to calculate the wavelength calibration.  The<br>default is a 3rd order polynomial, although if you are getting 20 or more<br>lines matched up, it should be safe to try 4th or 5th order and get a slightly<br>better fit.  It is not recommended to go above 5th order.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_EXTRACT_SPECTRA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_EXTRACT_SPECTRA = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to find the spectral locations and then <a href=\"#19\"><br>extract the spectra</a>.  This parameter controls both steps; neither<br>can be done alone.  If set to no, both steps are skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_METHOD")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_METHOD = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto | full | manual | filename.dat</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span><br><br>The method of finding the spectra can be one of four things:<br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = auto</span>:<br>This is the default option in which spectra are found automatically.  The<br>parameters <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_WIDTH</span> and<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_CENTER</span> control the size and location<br>of the extraction box(es) that will be used to sum the frame into a 1-d<br>cut.  <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_N_SPECTRA</span> controls the maximum<br>number of spectra that will be taken from each extraction box.  And<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_SIGMA</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>EXTRACT_MIN_WIDTH</span> establish rejection criteria of a minimum sigma<br>threshold and minimum width (in the y-direction) for the spectrum to be<br>extracted. <i>Note that all of the parameters listed above can be either<br>a single number or a whitespace separated list of numbers (in which case<br>multiple extraction boxes will be used)</i>.  Also note that if for instance<br>you set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_N_SPECTRA = 1</span>, for longslit data<br>only the brightest spectrum in the entire frame will be extracted.  But for<br>MOS data, the brightest spectrum in each slit will be extracted.  Furthermore,<br>the extraction boxes and other criteria defined above will be the same for<br><b>every dataset</b>.  If you wish to set different extraction boxes (or<br>other parameters) for different datasets, see <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>manual, filename.dat</span>, or 1B) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE<br></span>, below.<br><br><p><br>1B) <b>Semi-Automatic:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = auto</span><br><b>with</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE = file.dat</span><br><br><br>If the extraction method is set to <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">auto</span> and the<br>parameter <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE</span> is set to<br>the filename of an ASCII text file, then a semi-automatic routine is used.<br>This can be used if you know the approximate locations of all of your<br>spectra but want the program find the exact locations within the general<br>ranges you provide.  The guess file should be a 7-column ASCII text file<br>containing<pre><br>box_y1	box_y2	box_x1	box_x2	n_spec	sigma	width</pre><br>where an extraction box is formed by the four coordinates y1, y2, x1, and<br>x2, while n_spec, sigma, and width are the same as the parameters <br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_N_SPECTRA, EXTRACT_SIGMA,</span> and<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_MIN_WIDTH</span>, above.  <i>Note that if<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE</span> is defined, all of<br>the above parameters will be ignored even if defined</i>.  Furthermore,<br>note that just as above, the guess file can have multiple rows which will<br>define multiple extraction boxes for the same dataset.  However, unlike<br>above, MOS and longslit are treated the same since you are giving<br>absolute y-pixel values (i.e. if you give y1 and y2 values of 50 and 100 then<br>the program will look for a spectrum between the y-pixel values of 50 and<br>100 in the entire frame, not within each slit).  In addition, <b>the<br>parameters defined in the guess file will apply to only one<br>dataset instead of all datasets.  A blank line or a line beginning with<br>a letter in the guess file define the breaks between datasets</b>.  Of course,<br>the order of the rows in the extraction box should correspond to the order<br>of the datasets in your <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">FILES</span> filelist.  For<br>example:<pre><br>50	100	0	2048	1	2	5<br>375	495	1600	1850	1	2	5<br><br>750	900	400	1600	1	3	15<br>dataset 3 below<br>0	2048	0	2048	0	3	5<br></pre><br>In this example, the first two lines apply to the first dataset in your<br>filelist (which is clearly MOS data).  You first use the entire x-range<br>to create an extraction box and find the brightest spectrum in the y-range<br>of 50:100 pixels (that must also be at least 2 sigma for 5 consecutive <br>pixels within that y-range).  A second (presumably emission-line) spectrum<br>is also searched for within the same dataset.  A narrow extraction box<br>(1600:1850) is used to find a spectrum in the y-range of 375:495 pixels.<br>The blank line tells the program that this is all for the first dataset.<br>The fourth line applies to your second dataset (which may be longslit data).<br>You are looking for the brightest line between 750 and 900 pixels, but<br>the spectrum must now be at least 3 sigma over 15 consecutive pixels.  The<br>next line begins with a letter, signalling the program to move on to the<br>third dataset.  This time, you are using the entire frame as an extraction<br>box.  When n_spec = 0 that means that you are searching for <b>all</b><br>spectra that meet your sigma and minimum width requirements instead of<br>just the n brightest.<br><br><p><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = full</span>: If this method is<br>selected, the entire frame is defined as one spectrum for each dataset.<br><br><br><p><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = manual</span>: If you want to<br>specify the <i>exact</i> locations of each spectrum, you should use the<br>manual method and set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_FILENAME</span> to<br>the filename of an ASCII text file containing a list of y1, y2, x1, and x2<br>positions for each spectrum (the x1 and x2 positions are used later when <br><a href=\"#19\">extracting spectra</a> with Gaussian weighting and would<br>be the same x1 and x2 as if you used an extraction box with the auto method).<br>This ASCII file can have multiple rows which will define multiple spectra<br>for the same dataset.  A blank line or a line beginning with a letter<br>define the breaks between datasets.  Again, the order of the rows should<br>correspond to the order of the datasets in your filelist.  <i>Note that<br>when you set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD</span> to anything but<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">manual</span>, the spectral locations are written to<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_FILENAME</span>.</i>  Therefore, if you run<br>it once with the auto method, you will have a file that you can use with<br>the manual method the next time for the same datasets.  Here is an example<br>ASCII text file:<pre><br>1048    1062    0       2590<br>1163    1171    0       2590<br>1224    1229    0       2590<br><br>1192    1222    0       2048<br><br>1181    1213    0       2048<br></pre>Note that there are three spectra in the first frame and one spectrum<br>in each of the other two frames. <br><br><br><p><br>4) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = filename.dat</span>: If you wish<br>to use the auto method described in 1) but for multiple files, then you<br>must set <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD</span> to the name of an<br>ASCII text file.  This file should have 6 columns: filename prefix (or<br>fragment), extract box width, extract box center, n_spectra, sigma, and<br>minimum width (all the parameters you would specify with keywords normally<br>for the auto method).  Prefix is the filename prefix (or a fragment of it)<br>for a dataset.  Note that you can have more than one line with the same<br>prefix and in this way have multiple extraction boxes for each dataset.<br>For example:<pre><br>hd		0       0       1       5       15<br>hd32008    	1600    1800    2       3       15<br>lkha1021	0       0       1       2       5<br></pre><br>This file tells the programe first, for all datasets with 'hd' in the prefix,<br>to use the entire x-range as an extraction box (width = 0, center = 0 means<br>to use the entire range, see below) and extract the single brightest spectrum,<br>which must be a minimum of 5 sigma over 15 consecutive y-pixels.  Furthermore,<br>for hd32008*.*.fits only, <i>additionally</i> use the range of 1600:1800<br>as an extraction box and pick out the two brightest spectra (not including the<br>one already selected with the previous extraction box), which must be a<br>minimum of 3 sigma over 15 consecutive y-pixels.  Finally, for the dataset<br>with lkha1021 in its filename (which is a MOS dataset), use the whole x-range<br>as an extraction box and find the single brightest spectrum in each slit<br>(since it is MOS data).  Each spectrum must be a minimum of 2 sigma over<br>5 consecutive pixels to be selected.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_FILENAME")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_FILENAME = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">extract.dat</span><br><br>If <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = manual</span>, then this parameter<br>should be set to an ASCII text file containing a list of y1, y2, x1, and<br>x2 positions for each spectrum (see above).  It can have multiple rows<br>which will be used to define multiple spectra for the same dataset.  A<br>blank line or a line beginning with a letter define the breaks between<br>datasets.<br><br><br>For all other cases of <span calss=\"param\">EXTRACT_METHOD</span>, this<br>parameter should be set to a filename where an ASCII text file, as<br>described above, will be exported after finding the locations of the<br>spectra in all datasets.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_BOX_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_BOX_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>When using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = auto</span>, this parameter<br>controls the width of the extraction box.  Every row in the extraction<br>box will be summed to produce a 1-d cut vertically across the frame, which<br>will then be used to find the spectral locations.  This parameter can be<br>set to either a single number or a whitespace separated list of numbers (in<br>which case multiple extraction boxes will be used).  Note that setting this<br>to zero (its default value) and setting <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_CENTER<br></span> to zero results in the entire x-range of the frame being used for<br>the extraction box.  Furthermore, note that when using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>EXTRACT_METHOD = filename.dat</span> the extract box width column behaves<br>exactly the same as this parameter (including if it is set to zero), but<br>when using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE</span> or<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = manual</span>, setting these parameters<br>to zero will <b>not</b> default to the entire x-range of the frame.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_BOX_CENTER")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_BOX_CENTER = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>When using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = auto</span>, this parameter<br>controls the center of the extraction box.  Every row in the extraction<br>box will be summed to produce a 1-d cut vertically across the frame, which<br>will then be used to find the spectral locations.  This parameter can be<br>set to either a single number or a whitespace separated list of numbers (in<br>which case multiple extraction boxes will be used).  Note that setting this<br>to zero (its default value) and setting <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_WIDTH<br></span> to zero results in the entire x-range of the frame being used for<br>the extraction box.  Furthermore, note that when using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>EXTRACT_METHOD = filename.dat</span> the extract box center column behaves<br>exactly the same as this parameter (including if it is set to zero), but<br>when using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_BOX_GUESS_FILE</span> or<br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = manual</span>, setting these parameters<br>to zero will <b>not</b> default to the entire x-range of the frame.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_N_SPECTRA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_N_SPECTRA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">0</span><br><br>When using <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_METHOD = auto</span>, this parameter<br>controls the maximum number of spectra that will be extracted from the<br>frame (for longslit data) or from each slit (for MOS data) using each<br>extraction box.   This parameter can be set to either a single number or a<br>whitespace separated list of numbers (when multiple extraction boxes are used).<br>Note that if you set this parameter to a number, n, then the n <i>brightest</i><br>spectra in the current extraction box (<i>not including those already found<br>in the same dataset with a previous extraction box</i>) will be found.  A<br>spectrum is defined as already being found with a previous extraction box<br>if there is an overlap between the y-range found for the spectrum and any<br>of the y-ranges already stored in the array for that same dataset.  Also<br>note that <b>for every method</b>, setting this parameter to zero means<br>find <b>all</b> spectra using the current extraction box that pass the<br>rejection criteria (below).<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_SIGMA")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_SIGMA = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">2</span><br><br>When the spectra are found automatically, an iterative sigma clipping is<br>used to find them: the 1-d cut is passed to a routine, which discards<br>all values farther than +/- n sigma from the median value.  The median<br>and standard deviation are then re-calculated from the remaining pixels<br>and the process is repeated until no more points lie outside of +/-<br>n sigma.  This last median and standard deviation are then used to determine<br>which points are &gt; median+n*sigma, and these points are defined as<br>candidates for spectra.  However, there is a minimum width component as well:<br>w pixels in a row must be above the sigma threshold to be counted as a<br>spectrum; otherwise, those points will simply be thrown away.  Note that<br>the procedure for finding troughs in <a href=\"#11\">shifting and adding</a><br>is exactly the same except that points which are &lt; median-n*sigma for <br>at least w consecutive pixels are defined as troughs.  Furthermore, since<br>the troughs are just negative spectra, the same parameters <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"><br>EXTRACT_SIGMA</span> and <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_MIN_WIDTH</span><br>are used when finding troughs (see <a href=\"#11\">shift and add</a>).  Just<br>like the previous three parameters, this one can be either a single number<br>or a whitespace separated list of numbers (when multiple extraction boxes<br>are used).<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_MIN_WIDTH")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_MIN_WIDTH = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">5</span><br><br>This parameter controls the minimum number of consecutive pixels (in the<br>y-direction) that must be above the specified sigma threshold for a feature<br>to be classified a spectra in the iterative sigma clipping described above.<br>The default is 5 pixels, although for bright longslit data, 15 pixels is<br>quite reasonable.  Just like the previous parameters, this one can be either<br>a single number or a whitespace separated list of numbers (when multiple<br>extraction boxes are used).<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_BOX_GUESS_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_BOX_GUESS_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>If you prefer to employ the <b>semi-automatic</b> method of finding spectra,<br>set this paramter to an ASCII text file containing 7 columns per line:<pre><br>box_y1	box_y2	box_x1	box_x2	n_spec	sigma	width<br></pre>where an extraction box is formed by the four coordinates y1, y2, x1,<br>and x2; n_spec is the maximum number of spectra to find for this extraction<br>box; and sigma and width are the rejection criteria for selecting spectra<br>from this extraction box using the iterative sigma clipping described above.<br>Multiple extraction boxes can be listed per dataset and a blank line or a line<br>beginning with a letter defines the breaks between datasets.  See above<br>for more info and an example file.<br></body></html>");
      ((FATBOYParam)specHash.get("EXTRACT_WEIGHTING")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">EXTRACT_WEIGHTING = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">gaussian | linear | median</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">gaussian</span><br><br>This parameter controls the type of weighting that will be applied when<br>extracting 1-d spectra.  It can be set to one of three things:<br><br><br>1) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_WEIGHTING = gaussian</span>: this is the<br>default setting.  The entire frame within the <b>x-range</b> obtained<br>in the <a href=\"#18\">previous step</a> for the current spectrum (generally<br>the entire width of the frame unless the current spectrum is an emission<br>line spectrum and thus had a narrow extraction box) is compressed into<br>a 1-d cut by taking the median value of each row.  The part of this<br>1-d cut defined by the <b>y-range</b> of the current spectrum (obtained in<br>the <a href=\"#18\">previous step</a>) is then fit with a Gaussian profile.<br>The value of this Gaussian at each y-pixel is then used to weight that<br>row and all rows (across the whole frame) within the spectral y-range are<br>weighted and summed up to produce a 1-d spectrum.<br><br><p><br>2) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_WEIGHTING = linear</span>: a linear weight<br>is applied to each row in the spectral y-range, meaning that all the rows<br>in the y-range of the current spectrum are simply summed into a 1-d<br>spectrum.  For example, if a spectrum is found to have a y-range of 1192:1222,<br>then those 31 rows in the frame will be summed to produce a 1-d spectrum.<br><br><p><br>3) <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">EXTRACT_WEIGHTING = median</span>: instead of <br>summing up all the rows within the y-range of the current spectrum, the<br>median value of each column is taken, producing a 1-d spectrum.  Using the<br>above example, for every x-pixel, the median value of data[x,1192:x,1222]<br>will be taken, thus producing a 1-d spectrum.<br></body></html>");
      ((FATBOYParam)specHash.get("DO_CALIBRATION_STAR_DIVIDE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">DO_CALIBRATION_STAR_DIVIDE = </span><br><span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes | no</span><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\">yes</span><br><br>Whether or not to divide the spectra of your objects by the spectrum of a<br>calibration star.  If set to no, this step is skipped.<br></body></html>");
      ((FATBOYParam)specHash.get("CALIBRATION_STAR_FILE")).setToolTipText("<html>"+toolTipOptions+"<li><span style=\"font: 18pt arial,helvetica,sans-serif; font-weight: bold; color: #2222ff;\">CALIBRATION_STAR_FILE = </span><br><br><br><b>Default value:</b> <span style=\"font: 16pt arial,helvetica,sans-serif; font-weight: bold; color: #005577;\"></span><br><br>Either the filename of a FITS file (that has the 'PORDER' and 'PCOEFF_n'<br>keywords in its header so that its wavelength scale can be reconstructed)<br>to be used as a calibration star for <i>all</i> datasets or an ASCII text<br>file specifying a calibration star for each individual dataset.  The ASCII<br>text file should have the format:<pre><br>object_prefix	calib*_prefix</pre><br>where object_prefix is the prefix (or a fragment of it) of the filename of<br>the object and calib*_prefix is the prefix (or fragment) of the filename<br>of the calibration star.  For example:<pre><br>lkha101f21_jhjh 	hd29587_jhjh<br></pre>will use the file <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\">extractedSpectra/*hd29587_jhjh*.fits<br></span>as the calibration star for the object <span style=\"font: 16pt arial,helvetica,sans-serif; color: #ff2222;\"><br>extractedSpectra/*lkha101f21_jhjh*.fits</span>.<br></body></html>");


      //Quick start panel
      sqsFiles = new FATBOYTextAreaParam("FILES:", 5, 16, "Add Files", "Clear");
      sqsFiles.addBrowse(true);
      sqsFiles.addClear();
      sqsFiles.addTo(specPanels[0]);
      sqsFiles.setConstraints(specLayout[0], 10, 5, 215, 5, 10, specPanels[0], specPanels[0], false, false, true);
      sqsFiles.addMirror(sFiles);
      sFiles.addMirror(sqsFiles);

      sqsGroupOutputBy = new FATBOYComboParam("GROUP_OUTPUT_BY:", "filename,fits keyword,from manual file");
      sqsGroupOutputBy.addTo(specPanels[0]);
      sqsGroupOutputBy.setConstraints(specLayout[0], 12, 5, 215, specPanels[1], sqsFiles.spParam);
      sqsGroupOutputBy.addMirror(sGroupOutputBy);
      sGroupOutputBy.addMirror(sqsGroupOutputBy);

      sqsSpecifyGrouping = new FATBOYTextBrowseParam("SPECIFY_GROUPING:", FATBOYLabel.NONE, 16, "Browse");
      sqsSpecifyGrouping.addBrowse();
      sqsSpecifyGrouping.addTo(specPanels[0]);
      sqsSpecifyGrouping.setConstraints(specLayout[0], 10, 5, 215, 5, specPanels[1], sqsGroupOutputBy.cbParam);
      sqsSpecifyGrouping.addMirror(sSpecifyGrouping);
      sSpecifyGrouping.addMirror(sqsSpecifyGrouping);

      sqsDoNoisemaps = new FATBOYRadioParam("DO_NOISEMAPS:", FATBOYLabel.NONE, "Yes", "No");
      sqsDoNoisemaps.addTo(specPanels[0]);
      sqsDoNoisemaps.setConstraints(specLayout[0], 10, 5, 215, 15, specPanels[0], sqsSpecifyGrouping.tParam);
      sqsDoNoisemaps.addMirror(sDoNoisemaps);
      sDoNoisemaps.addMirror(sqsDoNoisemaps);

      sqsDoLinearize = new FATBOYRadioParam("DO_LINEARIZE:", "Yes", "No");
      sqsDoLinearize.addTo(specPanels[0]);
      sqsDoLinearize.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoNoisemaps.yParam);
      sqsDoLinearize.addMirror(sDoLinearize);
      sDoLinearize.addMirror(sqsDoLinearize);

      sqsDoDarkCombine = new FATBOYRadioParam("DO_DARK_COMBINE:", "Yes", "No");
      sqsDoDarkCombine.addTo(specPanels[0]);
      sqsDoDarkCombine.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoLinearize.yParam);
      sqsDoDarkCombine.addMirror(sDoDarkCombine);
      sDoDarkCombine.addMirror(sqsDoDarkCombine);

      sqsDoDarkSubtract = new FATBOYRadioParam("DO_DARK_SUBTRACT:", "Yes", "No");
      sqsDoDarkSubtract.addTo(specPanels[0]);
      sqsDoDarkSubtract.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoDarkCombine.yParam);
      sqsDoDarkSubtract.addMirror(sDoDarkSubtract);
      sDoDarkSubtract.addMirror(sqsDoDarkSubtract);

      sqsDoFlatCombine = new FATBOYRadioParam("DO_FLAT_COMBINE:", "Yes", "No");
      sqsDoFlatCombine.addTo(specPanels[0]);
      sqsDoFlatCombine.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoDarkSubtract.yParam);
      sqsDoFlatCombine.addMirror(sDoFlatCombine);
      sDoFlatCombine.addMirror(sqsDoFlatCombine);

      sqsFlatSelection = new FATBOYComboParam("FLAT_SELECTION:", "all,auto,manual", "auto");
      sqsFlatSelection.addTo(specPanels[0]);
      sqsFlatSelection.setConstraints(specLayout[0], 8, 25, 215, specPanels[0], sqsDoFlatCombine.yParam);
      sqsFlatSelection.addMirror(sFlatSelection);
      sFlatSelection.addMirror(sqsFlatSelection);

      sqsDoFlatDivide = new FATBOYRadioParam("DO_FLAT_DIVIDE:", "Yes", "No");
      sqsDoFlatDivide.addTo(specPanels[0]);
      sqsDoFlatDivide.setConstraints(specLayout[0], 12, 5, 215, 15, specPanels[0], sqsFlatSelection.cbParam);
      sqsDoFlatDivide.addMirror(sDoFlatDivide);
      sDoFlatDivide.addMirror(sqsDoFlatDivide);

      sqsDoArclampCombine = new FATBOYRadioParam("DO_ARCLAMP_COMBINE:", "Yes", "No");
      sqsDoArclampCombine.addTo(specPanels[0]);
      sqsDoArclampCombine.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoFlatDivide.yParam);
      sqsDoArclampCombine.addMirror(sDoArclampCombine);
      sDoArclampCombine.addMirror(sqsDoArclampCombine);

      sqsArclampSelection = new FATBOYComboBrowseParam("ARCLAMP_SELECTION:", "auto,from file", 16, "Browse");
      sqsArclampSelection.addBrowse();
      sqsArclampSelection.addTo(specPanels[0]);
      sqsArclampSelection.setConstraints(specLayout[0], 8, 25, 215, 8, 215, 5, specPanels[0], sqsDoArclampCombine.yParam);
      sqsArclampSelection.addMirror(sArclampSelection);
      sArclampSelection.addMirror(sqsArclampSelection);

      sqsDoBadPixelMask = new FATBOYRadioParam("DO_BAD_PIXEL_MASK:", "Yes", "No");
      sqsDoBadPixelMask.addTo(specPanels[0]);
      sqsDoBadPixelMask.setConstraints(specLayout[0], 12, 5, 215, 15, specPanels[0], sqsArclampSelection.tParam);
      sqsDoBadPixelMask.addMirror(sDoBadPixelMask);
      sDoBadPixelMask.addMirror(sqsDoBadPixelMask);

      sqsRemoveCosmicRays = new FATBOYRadioParam("REMOVE_COSMIC_RAYS:", "Yes","No");
      sqsRemoveCosmicRays.addTo(specPanels[0]);
      sqsRemoveCosmicRays.setConstraints(specLayout[0], 8, 5, 215, 15, specPanels[0], sqsDoBadPixelMask.yParam);
      sqsRemoveCosmicRays.addMirror(sRemoveCosmicRays);
      sRemoveCosmicRays.addMirror(sqsRemoveCosmicRays);

      sqsDoSkySubtract = new FATBOYRadioParam("DO_SKY_SUBTRACT:", "Yes", "No");
      sqsDoSkySubtract.addTo(specPanels[0]);
      sqsDoSkySubtract.setConstraints(specLayout[0], 10, 512, 768, 15, specPanels[0], specPanels[0], false, true); 
      sqsDoSkySubtract.addMirror(sDoSkySubtract);
      sDoSkySubtract.addMirror(sqsDoSkySubtract);

      sqsSkySubtractMethod = new FATBOYComboBrowseParam("SKY_SUBTRACT_METHOD:", FATBOYLabel.NONE, "median,dither,ifu_onsource_dither,step,offsource_dither,offsource_multi_dither,from file", 14, "Browse", "dither");
      sqsSkySubtractMethod.addBrowse();
      sqsSkySubtractMethod.addTo(specPanels[0]);
      sqsSkySubtractMethod.setConstraints(specLayout[0], 5, 535, 768, 8, 768, 5, specPanels[0], sqsDoSkySubtract.yParam);
      sqsSkySubtractMethod.addMirror(sSkySubtractMethod);
      sSkySubtractMethod.addMirror(sqsSkySubtractMethod);

      sqsDoRectify = new FATBOYRadioParam("DO_RECTIFY:", "Yes", "No");
      sqsDoRectify.addTo(specPanels[0]);
      sqsDoRectify.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsSkySubtractMethod.tParam);
      sqsDoRectify.addMirror(sDoRectify);
      sDoRectify.addMirror(sqsDoRectify);

      sqsDataType = new FATBOYComboBrowseParam("DATA_TYPE:", FATBOYLabel.NONE,"longslit,mos,ifu,from file", 14, "Browse");
      sqsDataType.addBrowse();
      sqsDataType.addTo(specPanels[0]);
      sqsDataType.setConstraints(specLayout[0], 5, 535, 768, 5, 768, 5, specPanels[0], sqsDoRectify.yParam);
      sqsDataType.addMirror(sDataType);
      sDataType.addMirror(sqsDataType);

      sqsDoDoubleSubtract = new FATBOYRadioParam("DO_DOUBLE_SUBTRACT:", "Yes", "No");
      sqsDoDoubleSubtract.addTo(specPanels[0]);
      sqsDoDoubleSubtract.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsDataType.tParam);
      sqsDoDoubleSubtract.addMirror(sDoDoubleSubtract);
      sDoDoubleSubtract.addMirror(sqsDoDoubleSubtract);

      sqsDoShiftAdd = new FATBOYRadioParam("DO_SHIFT_ADD:", "Yes", "No");
      sqsDoShiftAdd.addTo(specPanels[0]);
      sqsDoShiftAdd.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsDoDoubleSubtract.yParam);
      sqsDoShiftAdd.addMirror(sDoShiftAdd);
      sDoShiftAdd.addMirror(sqsDoShiftAdd);

      sqsDoMosAlignSkylines = new FATBOYRadioParam("DO_MOS_ALIGN_SKYLINES:", "Yes", "No");
      sqsDoMosAlignSkylines.addTo(specPanels[0]);
      sqsDoMosAlignSkylines.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsDoShiftAdd.yParam);
      sqsDoMosAlignSkylines.addMirror(sDoMosAlignSkylines);
      sDoMosAlignSkylines.addMirror(sqsDoMosAlignSkylines);

      sqsDoWavelengthCalibrate = new FATBOYRadioParam("DO_WAVELENGTH_CALIBRATE:", "Yes", "No");
      sqsDoWavelengthCalibrate.addTo(specPanels[0]);
      sqsDoWavelengthCalibrate.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsDoMosAlignSkylines.yParam);
      sqsDoWavelengthCalibrate.addMirror(sDoWavelengthCalibrate);
      sDoWavelengthCalibrate.addMirror(sqsDoWavelengthCalibrate);

      sqsCalibrationInfoFile = new FATBOYTextBrowseParam("CALIBRATION_INFO_FILE:", FATBOYLabel.NONE, 14, "Browse");
      sqsCalibrationInfoFile.addBrowse();
      sqsCalibrationInfoFile.addTo(specPanels[0]);
      sqsCalibrationInfoFile.setConstraints(specLayout[0], 5, 535, 768, 5, specPanels[0], sqsDoWavelengthCalibrate.yParam);
      sqsCalibrationInfoFile.addMirror(sCalibrationInfoFile);
      sCalibrationInfoFile.addMirror(sqsCalibrationInfoFile);

      sqsDoExtractSpectra = new FATBOYRadioParam("DO_EXTRACT_SPECTRA:", "Yes", "No");
      sqsDoExtractSpectra.addTo(specPanels[0]);
      sqsDoExtractSpectra.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsCalibrationInfoFile.tParam);
      sqsDoExtractSpectra.addMirror(sDoExtractSpectra);
      sDoExtractSpectra.addMirror(sqsDoExtractSpectra);

      sqsExtractMethod = new FATBOYComboBrowseParam("EXTRACT_METHOD:", FATBOYLabel.NONE,"auto,full,manual,from file", 14, "Browse");
      sqsExtractMethod.addBrowse();
      sqsExtractMethod.addTo(specPanels[0]);
      sqsExtractMethod.setConstraints(specLayout[0], 5, 535, 768, 5, 768, 5, specPanels[0], sqsDoExtractSpectra.yParam);
      sqsExtractMethod.addMirror(sExtractMethod);
      sExtractMethod.addMirror(sqsExtractMethod);

      sqsDoCalibrationStarDivide = new FATBOYRadioParam("DO_CALIBRATN_STAR_DIVIDE:", "Yes", "No");
      sqsDoCalibrationStarDivide.addTo(specPanels[0]);
      sqsDoCalibrationStarDivide.setConstraints(specLayout[0], 8, 512, 768, 15, specPanels[0], sqsExtractMethod.tParam);
      sqsDoCalibrationStarDivide.addMirror(sDoCalibrationStarDivide);
      sDoCalibrationStarDivide.addMirror(sqsDoCalibrationStarDivide);

      sqsCalibrationStarFile = new FATBOYTextBrowseParam("CALIBRATION_STAR_FILE:", FATBOYLabel.NONE, 14, "Browse");
      sqsCalibrationStarFile.addBrowse();
      sqsCalibrationStarFile.addTo(specPanels[0]);
      sqsCalibrationStarFile.setConstraints(specLayout[0], 5, 535, 768, 5, specPanels[0], sqsDoCalibrationStarDivide.yParam);
      sqsCalibrationStarFile.addMirror(sCalibrationStarFile);
      sCalibrationStarFile.addMirror(sqsCalibrationStarFile);

      //add graying options for quickstart panel
      sqsDoFlatCombine.addGrayComponents("No", sqsFlatSelection);
      sqsDoArclampCombine.addGrayComponents("No", sqsArclampSelection);
      sqsDoSkySubtract.addGrayComponents("No", sqsSkySubtractMethod);
      sqsDoWavelengthCalibrate.addGrayComponents("No", sqsCalibrationInfoFile);
      sqsDoExtractSpectra.addGrayComponents("No", sqsExtractMethod);
      sqsDoCalibrationStarDivide.addGrayComponents("No", sqsCalibrationStarFile);

      specPane.setFont(new Font("Dialog",Font.BOLD,16));
      specPane.addTab("Quick Start", null, specPanels[0], "Commonly changed options");
      specPane.addTab("Initialization", null, specPanels[1], "Setup preprocessing options");
      specPane.addTab("Linearity & Darks", null, specPanels[2], "Set options for linearity correction and combination and subtraction of dark frames");
      specPane.addTab("Flats & Arclamps", null, specPanels[3], "Set options for combination and division of flat fields and the combination of arclamps");
      specPane.addTab("Bad Pixels", null, specPanels[4], "Set options for creating bad pixel masks and rejecting cosmic rays");
      specPane.addTab("Sky Subtraction", null, specPanels[5], "Set options for sky subtraction");
      specPane.addTab("Rectification & Combining", null, specPanels[6], "Set options for rectification, double subtraction, and shifting & adding frames");
      specPane.addTab("Alignment & Calibration", null, specPanels[7], "Set options for MOS slit alignment by skylines and wavelength calibration"); 
      specPane.addTab("Spectra", null, specPanels[8], "Set options for finding and extracting spectra and dividing by a calibration star"); 

      currentMode = "Imaging";
      currentPane = imagingPane;
      currentHash = imagingHash;

      //Setup bottom panel
      bottomPanel = new JPanel();
      bottomPanel.setPreferredSize(new Dimension(xdim, 100));
      bottomPanel.setBackground(Color.white);
      SpringLayout bottomLayout = new SpringLayout();
      bottomPanel.setLayout(bottomLayout);

      logFile = new FATBOYTextBrowseParam("Log File:", 14, "Browse", "pipeline.log");
      logFile.addBrowse();
      logFile.addTo(bottomPanel);
      logFile.setConstraints(bottomLayout, 10, 5, 90, 5, bottomPanel, bottomPanel, false, true);

      warnFile = new FATBOYTextBrowseParam("Warn File:", 14, "Browse", "pipeline.warn");
      warnFile.addBrowse();
      warnFile.addTo(bottomPanel);
      warnFile.setConstraints(bottomLayout, 20, 5, 90, 5, bottomPanel, logFile.tParam);

      lStatus = new JLabel("Status:");
      bottomPanel.add(lStatus);
      bottomLayout.putConstraint(SpringLayout.WEST, lStatus, 20, SpringLayout.EAST, logFile.bParam);
      bottomLayout.putConstraint(SpringLayout.NORTH, lStatus, 10, SpringLayout.NORTH, bottomPanel);

      tStatus = new JTextField(19);
      tStatus.setEditable(false);
      tStatus.setBackground(Color.WHITE);
      bottomPanel.add(tStatus);
      bottomLayout.putConstraint(SpringLayout.WEST, tStatus, 5, SpringLayout.EAST, lStatus);
      bottomLayout.putConstraint(SpringLayout.NORTH, tStatus, 10, SpringLayout.NORTH, bottomPanel);

      lProgress = new JLabel("Progress:");
      bottomPanel.add(lProgress);
      bottomLayout.putConstraint(SpringLayout.WEST, lProgress, 20, SpringLayout.EAST, logFile.bParam);
      bottomLayout.putConstraint(SpringLayout.NORTH, lProgress, 20, SpringLayout.SOUTH, tStatus);

      progressBar = new JProgressBar(0,100);
      progressBar.setPreferredSize(new Dimension(200,20));
      progressBar.setValue(0);
      progressBar.setString("0%");
      progressBar.setStringPainted(true);
      bottomPanel.add(progressBar);
      bottomLayout.putConstraint(SpringLayout.WEST, progressBar, 5, SpringLayout.EAST, lProgress);
      bottomLayout.putConstraint(SpringLayout.NORTH, progressBar, 20, SpringLayout.SOUTH, tStatus);

      //bQuit = new JButton("Quit");
      //bQuit.setBackground(Color.RED);
      bQuit = new UFColorButton("Quit", UFColorButton.COLOR_SCHEME_RED);
      bQuit.setFont(new Font("Dialog", 0, 18));
      bQuit.setPreferredSize(new Dimension(80,40));
      bQuit.setToolTipText("Quit the program and terminate any running processes");
      bQuit.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
	   Object[] exitOptions = {"Exit","Cancel"};
	   int n = JOptionPane.showOptionDialog(FATBOY_GUI.this, "Are you sure you want to quit and terminate any running processes?", "Exit FATBOY?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
	   if (n == 0) {
	      if (fexec != null) {
		if (fexec.p != null) fexec.p.destroy();
	      }
	      System.exit(0);
	   }
        }
      });
      bottomPanel.add(bQuit);
      bottomLayout.putConstraint(SpringLayout.WEST, bQuit, 20, SpringLayout.EAST, tStatus);
      bottomLayout.putConstraint(SpringLayout.NORTH, bQuit, 10, SpringLayout.NORTH, bottomPanel);

      bShowMessages = new UFColorButton("<html><center>Show<br>Messages</center></html>");
      bShowMessages.setToolTipText("When FATBOY is running, display message window, including option to terminate the current process");
      bShowMessages.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (fexec != null) fexec.setVisible(true);
	}
      });
      bottomPanel.add(bShowMessages);
      bottomLayout.putConstraint(SpringLayout.WEST, bShowMessages, 15, SpringLayout.EAST, bQuit);
      bottomLayout.putConstraint(SpringLayout.NORTH, bShowMessages, 10, SpringLayout.NORTH, bottomPanel);

      bShowDisplay = new UFColorButton("<html><center>Show<br>Display</center></html>");
      bShowDisplay.setToolTipText("When FATBOY is running, display images as they are processed");
      bShowDisplay.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           if (display != null) {
	      display.setVisible(true);
	      display.setSize(1280,1024);
	   }
        }
      });
      bottomPanel.add(bShowDisplay);
      bottomLayout.putConstraint(SpringLayout.WEST, bShowDisplay, 10, SpringLayout.EAST, bShowMessages);
      bottomLayout.putConstraint(SpringLayout.NORTH, bShowDisplay, 10, SpringLayout.NORTH, bottomPanel);

      //bGo = new JButton("GO!");
      //bGo.setBackground(Color.GREEN);
      bGo = new UFColorButton("GO!", UFColorButton.COLOR_SCHEME_GREEN);
      bGo.setFont(new Font("Dialog", 0, 20));
      bGo.setPreferredSize(new Dimension(90,50));
      bGo.setToolTipText("Run FATBOY");
      bGo.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   String fatboyCommand = "flam2pipeline.py";
	   String options = "";
	   String filename = "tempFatboyParams.dat";
/*
	   Object[] goOptions = {"Save","Don't Save"};
	   int n = JOptionPane.showOptionDialog(FATBOY_GUI.this, "Do you want to save this parameter file before running FATBOY?", "Save parameters?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, goOptions, goOptions[1]);
	   if (n == 0) {
	      JFileChooser jfc = new JFileChooser(defaultDir);
              int returnVal = jfc.showSaveDialog((Component)ev.getSource());
              if (returnVal == JFileChooser.APPROVE_OPTION) {
		filename = jfc.getSelectedFile().getAbsolutePath();
		if (new File(filename).exists()) {
		   Object[] saveOptions = {"Overwrite","Don't Save","Abort"};
                   int ns = JOptionPane.showOptionDialog(FATBOY_GUI.this, filename+" already exists.", "File exists!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, saveOptions, saveOptions[1]);
                   if (ns == 2) return;
		   if (ns == 1) filename = "tempFatboyParams.dat";
		}
              }
              defaultDir = jfc.getCurrentDirectory();
           }
*/
	   saveParams(filename);
	   if (fexec != null) {
	      if (fexec.isRunning) {
		error("<html>FATBOY is already running!</html>");
		return;
	      } else fexec.dispose();
	   }
	   if (currentMode.equalsIgnoreCase("Imaging")) {
	      fatboyCommand = "flam2pipeline.py";
	      options = filename+" "+logFile.getParamValue()+" "+warnFile.getParamValue()+" -gui";
	   } else if (currentMode.equalsIgnoreCase("Astrometry")) {
	      fatboyCommand = "f2astrometry.py";
              options = filename+" "+logFile.getParamValue()+" "+warnFile.getParamValue()+" -gui";
	   } else if (currentMode.equalsIgnoreCase("Spectroscopy")) {
	      fatboyCommand = "f2spec.py";
              options = filename+" "+logFile.getParamValue()+" "+warnFile.getParamValue()+" -gui";
	   } else if (currentMode.equalsIgnoreCase("Longslit Rectification")) {
	      fatboyCommand = "rectify.py";
              options = filename+" "+logFile.getParamValue()+" -gui";
	   } else if (currentMode.equalsIgnoreCase("MOS Rectification")) {
	      fatboyCommand = "rectifyMOS.py";
              options = filename+" "+logFile.getParamValue()+" "+warnFile.getParamValue()+" -gui";
	   }
	   String path = UFExecCommand.getEnvVar("PATH");
	   String defPath = path.replaceAll(":"," ");
           String temp = "";
	   try {
	      temp = FATBOY_GUI.class.getResource("help.html").toString();
	   } catch (Exception e) { temp = ""; }
	   temp = temp.replaceAll("jar:file:","");
           temp = temp.replaceAll("FATBOY_GUI.jar!/help.html","");
	   File conf = new File(temp+"fatboy.config");
	   if (conf.exists()) {
	      try {
		BufferedReader r = new BufferedReader(new FileReader(conf));
		String s = " ";
		while (s != null) {
		   s = r.readLine();
		   if (s == null) break;
		   if (s.startsWith("PATH=")) {
		      path+=":"+s.substring(5);
		      defPath+=" "+s.substring(5);
		      fatboyCommand = s.substring(5)+"/"+fatboyCommand;
		   }
		}
	      } catch (IOException e) { System.err.println(e.toString()); }
	   }
           display = new FATBOYDisplayFrame();
	   fexec = new FATBOYExec(fatboyCommand, options, defPath, progressBar, tStatus, display);
	   fexec.setEnv("PATH",path);
           fexec.start();
	   bShowMessages.setText("<html>Show<br>Messages</html>");
	   bShowMessages.updateColorGradient(UFColorButton.COLOR_SCHEME_ORANGE);
	   bShowMessages.setToolTipText("Display message window for current FATBOY process, including option to terminate the current process");
           bShowDisplay.setText("<html>Show<br>Display</html>");
           bShowDisplay.updateColorGradient(UFColorButton.COLOR_SCHEME_ORANGE);
           bShowDisplay.setToolTipText("Display images currently being processed by FATBOY");
	}
      });
      bottomPanel.add(bGo);
      bottomLayout.putConstraint(SpringLayout.WEST, bGo, 15, SpringLayout.EAST, bShowDisplay);
      bottomLayout.putConstraint(SpringLayout.NORTH, bGo, 10, SpringLayout.NORTH, bottomPanel);

      content.add(topPanel, 0);
      content.add(new JScrollPane(currentPane), 1);
      content.add(bottomPanel, 2);
      pack();
      setVisible(true);
   }

   public void changeMode(String newMode) {
      mode.setSelectedItem(newMode);
      changeMode();
   }

   public void changeMode() {
      currentMode = (String)mode.getSelectedItem();
      setupPresets(currentMode);
      if (currentMode.equalsIgnoreCase("Imaging")) {
	currentPane = imagingPane;
	currentHash = imagingHash;
        warnFile.setGray(false);
      } else if (currentMode.equalsIgnoreCase("Astrometry")) {
	currentPane = astromPane;
	currentHash = astromHash;
        warnFile.setGray(false);
      } else if (currentMode.equalsIgnoreCase("Spectroscopy")) {
	currentPane = specPane;
	currentHash = specHash;
	warnFile.setGray(false);
      } else if (currentMode.equalsIgnoreCase("Longslit Rectification")) {
	currentPane = rectPane;
	currentHash = rectHash;
	warnFile.setGray(true);
      } else if (currentMode.equalsIgnoreCase("MOS Rectification")) {
	currentPane = rectMOSPane;
	currentHash = rectMOSHash;
	warnFile.setGray(false);
      }
      content.remove(1);
      content.add(new JScrollPane(currentPane),1);
      content.validate();
      repaint();
   }

   public void setupPresets(String currMode) {
      presets.setActionCommand("No action");
      presets.removeAllItems();
      presetFiles = new Vector();
      if (currMode.equals("Imaging")) {
	String[] presetOptions = {"Default", "Flamingos I", "Flamingos I - Extragalactic", "Flamingos I - Offsource Extended", "Flamingos II", "WIRC Palomar"};
	String[] temp = {"default.dat", "flam1.dat", "flam1-extragalactic.dat", "flam1-offsource-ext.dat","default.dat", "wirc-palomar.dat"};
	for (int j = 0; j < presetOptions.length; j++) {
	   presets.addItem(presetOptions[j]);
           presetFiles.add(FATBOY_GUI.class.getResource("preset-imaging-"+temp[j]));
	}
      }
      else if (currMode.equals("Astrometry")) {
        String[] presetOptions = {"Default", "Flamingos I - 2m", "Flamingos I - 4m", "Flamingos II"};
	String[] temp = {"default.dat", "flam1-2m.dat", "flam1-4m.dat", "default.dat"};
	for (int j = 0; j < presetOptions.length; j++) {
	   presets.addItem(presetOptions[j]);
	   presetFiles.add(FATBOY_GUI.class.getResource("preset-astrometry-"+temp[j]));
	}
      }
      else if (currMode.equals("Spectroscopy")) {
        String[] presetOptions = {"Default", "FISICA", "Flamingos I", "Flamingos II"};
        String[] temp = {"default.dat", "fisica.dat", "default.dat", "default.dat"};
	for (int j = 0; j < presetOptions.length; j++) {
	   presets.addItem(presetOptions[j]);
           presetFiles.add(FATBOY_GUI.class.getResource("preset-spectroscopy-"+temp[j]));
	}
      }
      else if (currMode.equals("Longslit Rectification")) {
        String[] presetOptions = {"Default", "Flamingos I", "Flamingos II"};
        String[] temp = {"default.dat", "default.dat", "default.dat"};
	for (int j = 0; j < presetOptions.length; j++) {
	   presets.addItem(presetOptions[j]);
           presetFiles.add(FATBOY_GUI.class.getResource("preset-lrect-"+temp[j]));
	}
      }
      else if (currMode.equals("MOS Rectification")) {
        String[] presetOptions = {"Default", "Flamingos I", "Flamingos I - Old Data", "Flamingos II"};
        String[] temp = {"default.dat", "default.dat", "flam1-olddata.dat", "default.dat"};
	for (int j = 0; j < presetOptions.length; j++) {
	   presets.addItem(presetOptions[j]);
           presetFiles.add(FATBOY_GUI.class.getResource("preset-mosrect-"+temp[j]));
	}
      }
      presets.setActionCommand("do presets");
   }

   public void saveParams(String filename) {
      String key;
      FATBOYParam param;
      PrintWriter p;
      try {
	p = new PrintWriter(new FileOutputStream(filename));
	p.println("#FATBOY "+currentMode);
	String[] theComments = comments.getText().split("\n");
	for (int j = 0; j < theComments.length; j++) {
	   if (!theComments[j].trim().equals(""))
	      p.println("#"+theComments[j]);
	}
	for (Iterator i = currentHash.keySet().iterator(); i.hasNext(); ) {
	   key = (String)i.next();
	   param = (FATBOYParam)currentHash.get(key);
	   p.println(key+" = "+param.getParamValue().trim().replace("\n",","));
	}
	p.close();
      } catch(IOException e) {
	error("<html>Error writing param file: "+filename+"<br>"+e.toString()+"</html>");
      }
   }

   public void loadParams(String filename) {
      int pos;
      String key, value;
      FATBOYParam param;
      BufferedReader b;
      boolean firstComment = true;
      comments.setText("");
      try {
	b = new BufferedReader(new FileReader(filename));
	String temp = b.readLine();
	if (temp.startsWith("#FATBOY ")) {
	   if (!currentMode.equals(temp.substring(8))) {
	      changeMode(temp.substring(8));
	   }
	} else if (!temp.startsWith("#") && temp.indexOf("=") != -1) {
	   pos = temp.indexOf("=");
	   key = temp.substring(0,pos).trim();
	   value = temp.substring(pos+1).trim().replace(",","\n");
           param = (FATBOYParam)currentHash.get(key);
	   if (param != null) {
	      param.setParamValue(value);
	      param.updateMirror();
	   }
        } else if (temp.startsWith("#") && firstComment) {
	   comments.setText(temp.substring(1)+"\n");
	   firstComment = false;
	}
	while (temp != null) {
	   temp = b.readLine();
	   if (temp == null) break;
	   if (!temp.startsWith("#") && temp.indexOf("=") != -1) {
	      pos = temp.indexOf("=");
              key = temp.substring(0,pos).trim();
              value = temp.substring(pos+1).trim().replace(",","\n");
	      if (currentHash.containsKey(key)) {
		param = (FATBOYParam)currentHash.get(key);
		if (param != null) {
		   param.setParamValue(value);
		   param.updateMirror();
                }
	      }
	   } else if (temp.startsWith("#")) {
	      if (firstComment) {
		comments.setText(temp.substring(1)+"\n");
		firstComment = false;
	      }
	      else comments.append(temp.substring(1)+"\n");
	   }
	}
	b.close();
      } catch(Exception e) {
        error("<html>Error reading param file: "+filename+"<br>"+e.toString()+"</html>");
      }
   }

   public void loadParams(URL url) {
      int pos;
      String key, value;
      FATBOYParam param;
      BufferedReader b;
      boolean firstComment = true;
      comments.setText("");
      try {
	URLConnection urlc = url.openConnection();
        b = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
	//b = new BufferedReader(new FileReader(new File(url.toURI())));
	String temp = b.readLine();
	if (temp.startsWith("#FATBOY ")) {
	   if (!currentMode.equals(temp.substring(8))) {
	      changeMode(temp.substring(8));
	   }
	} else if (!temp.startsWith("#") && temp.indexOf("=") != -1) {
	   pos = temp.indexOf("=");
	   key = temp.substring(0,pos).trim();
	   value = temp.substring(pos+1).trim().replace(",","\n");
	   if (currentHash.containsKey(key)) {
	      param = (FATBOYParam)currentHash.get(key);
              if (param != null) {
		param.setParamValue(value);
		param.updateMirror();
              }
           }
        } else if (temp.startsWith("#") && firstComment) {
	   comments.setText(temp.substring(1)+"\n");
	   firstComment = false;
	}
	while (temp != null) {
	   temp = b.readLine();
	   if (temp == null) break;
	   if (!temp.startsWith("#") && temp.indexOf("=") != -1) {
	      pos = temp.indexOf("=");
              key = temp.substring(0,pos).trim();
              value = temp.substring(pos+1).trim().replace(",","\n");
              param = (FATBOYParam)currentHash.get(key);
              if (param != null) {
		param.setParamValue(value);
		param.updateMirror();
              }
	   } else if (temp.startsWith("#")) {
	      if (firstComment) {
		comments.setText(temp.substring(1)+"\n");
		firstComment = false;
	      }
	      else comments.append(temp.substring(1)+"\n");
	   }
	}
	b.close();
      } catch(Exception e) {
        error("<html>Error reading param file: "+url.toString()+"<br>"+e.toString()+"</html>");
      }
   }

   public void error(String e) {
      JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
   }

   public boolean isValidMessage(String s) {
      if (s == null) return false;
      String[] invalid = {"card is too long","Warning: Encountered"};
      boolean isValid = true;
      for (int j = 0; j < invalid.length; j++) {
	if (s.startsWith(invalid[j])) isValid = false;
      }
      return isValid;
   }

   public static void main(String[] args) {
      FATBOY_GUI gui = new FATBOY_GUI();
   }

}
