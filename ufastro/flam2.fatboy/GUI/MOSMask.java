/**
 * Title:        MOSMask.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner
 * Company:      University of Florida
 * Description:  Creates Masks to overlay on top of the data in jdd
 */

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public abstract class MOSMask {

   String shape, text;
   Color color = null;
   float xcen, ycen;
   int imgxcen, imgycen;
   boolean displayText = false;
   Font f = new Font("Arial",0,12);

   public MOSMask() {}

   public abstract void drawMOSMask(Graphics g);

   public abstract void setScale(float xscale, float yscale);

   public abstract void setScale(float xscale, float yscale, float ysize);

   public abstract void addText(String text);

   public abstract void setTextMode(boolean b);

   public abstract void setFont(Font f);

   public abstract void setColor(String s);

}
