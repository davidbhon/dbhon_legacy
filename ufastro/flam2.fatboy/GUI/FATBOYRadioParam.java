/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class FATBOYRadioParam extends FATBOYParam {

   public FATBOYRadioParam(String labelText, String yText, String nText) {
      this(labelText, FATBOYLabel.NONE, yText, nText, true);
   }

   public FATBOYRadioParam(String labelText, int border, String yText, String nText) {
      this(labelText, border, yText, nText, true);
   }

   public FATBOYRadioParam(String labelText, String yText, String nText, boolean firstSelected) {
      this(labelText, FATBOYLabel.NONE, yText, nText, firstSelected);
   }

   public FATBOYRadioParam(String labelText, int border, String yText, String nText, boolean firstSelected) {
      lParam = new FATBOYLabel(labelText, border, true);
      if (firstSelected) {
	yParam = new JRadioButton(yText, true);
	nParam = new JRadioButton(nText);
      } else {
	yParam = new JRadioButton(yText);
	nParam = new JRadioButton(nText, true);
      }
      bgParam = new ButtonGroup();
      bgParam.add(yParam);
      bgParam.add(nParam);
   }

   public void setParamValue(String value) {
      if (value.equalsIgnoreCase(yParam.getText())) {
	if (!yParam.isSelected()) yParam.doClick();
      } else if (value.equalsIgnoreCase(nParam.getText())) {
	if (!nParam.isSelected()) nParam.doClick();
      }
   }

   public String getParamValue() {
      if (yParam.isSelected()) {
	return yParam.getText().toLowerCase();
      }
      return nParam.getText().toLowerCase();
   }

   public void addFocusBorder() {
      yParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
        public void focusLost(FocusEvent fe) {}
      });
      nParam.addFocusListener(new FocusListener() {
        public void focusGained(FocusEvent fe) {
           lParam.hideBorder();
        }
        public void focusLost(FocusEvent fe) {}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
	yParam.setEnabled(false);
	nParam.setEnabled(false);
      } else {
	yParam.setEnabled(true);
	nParam.setEnabled(true);
      }
   }

   public void addGrayComponents(String option, final FATBOYParam... paramList) {
      JRadioButton theButton;
      grayOptions.add(option);
      grayParams.add(paramList); 
      if (yParam.getText().equalsIgnoreCase(option)) {
	theButton = yParam;
      } else {
	theButton = nParam;
      }
      this.checkGrayState();
      theButton.addItemListener(new ItemListener() {
	public void itemStateChanged(ItemEvent ev) {
	   boolean doGray; 
	   if (ev.getStateChange() == ItemEvent.SELECTED) {
	      doGray = true;
	   } else {
	      doGray = false;
	   }
	   for (int j = 0; j < paramList.length; j++) {
	      paramList[j].setGray(doGray);
	   }
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].checkGrayState();
           }
	}
      });
   }

   public void checkGrayState() {
      JRadioButton theButton;
      for (int i = 0; i < grayOptions.size(); i++) {
	if (yParam.getText().equals(grayOptions.get(i))) {
	   theButton = yParam;
	} else {
	   theButton = nParam;
	}
	if (theButton.isSelected()) {
           FATBOYParam[] paramList = (FATBOYParam[])grayParams.get(i);
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].setGray(true);
           }
        }
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      yParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (!yParam.isSelected()) return;
	   String temp = yParam.getText();
	   if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	      mirrorParam.setParamValue(temp);
	   }
	}
      });
      nParam.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           if (!nParam.isSelected()) return;
           String temp = nParam.getText();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      if (yParam.isSelected()) {
	String temp = yParam.getText();
        if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	   mirrorParam.setParamValue(temp);
	}
      } else if (nParam.isSelected()) {
	String temp = nParam.getText();
        if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	   mirrorParam.setParamValue(temp);
        }
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(yParam);
      panel.add(nParam);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, x3, leftComponent, topComponent, false, false);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent, boolean useLeft3, boolean isTop) {
      String topPos, leftPos3;
      JComponent left3;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      if (useLeft3) {
	left3 = leftComponent;
	leftPos3 = SpringLayout.WEST;
      } else {
	left3 = yParam;
	leftPos3 = SpringLayout.EAST;
      }
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, yParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, yParam,y,topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, nParam, x3, leftPos3, left3);
      layout.putConstraint(SpringLayout.NORTH, nParam, y, topPos,topComponent);
   }

}
