/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FATBOYComboParam extends FATBOYParam {

   public FATBOYComboParam(String labelText, String comboOptions) {
      this(labelText, FATBOYLabel.NONE, comboOptions);
   }

   public FATBOYComboParam(String labelText, String comboOptions, int i) {
      this(labelText, FATBOYLabel.NONE, comboOptions, i);
   }

   public FATBOYComboParam(String labelText, String comboOptions, String i) {
      this(labelText, FATBOYLabel.NONE, comboOptions, i);
   }

   public FATBOYComboParam(String labelText, int border, String comboOptions) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
   }

   public FATBOYComboParam(String labelText, int border, String comboOptions, int i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      cbParam.setSelectedIndex(i);
   }

   public FATBOYComboParam(String labelText, int border, String comboOptions, String i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      cbParam.setSelectedItem(i);
   }

   public FATBOYComboParam(String labelText, String[] comboOptions) {
      this(labelText, FATBOYLabel.NONE, comboOptions);
   }

   public FATBOYComboParam(String labelText, int border, String[] comboOptions) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
   }

   public FATBOYComboParam(String labelText, String[] comboOptions, int i) {
      this(labelText, FATBOYLabel.NONE, comboOptions, i);
   }

   public FATBOYComboParam(String labelText, String[] comboOptions, String i) {
      this(labelText, FATBOYLabel.NONE, comboOptions, i);
   }

   public FATBOYComboParam(String labelText, int border, String[] comboOptions, int i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
      cbParam.setSelectedIndex(i);
   }

   public FATBOYComboParam(String labelText, int border, String[] comboOptions, String i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
      cbParam.setSelectedItem(i);
   }

   public void setParamValue(String value) {
      cbParam.setSelectedItem(value);
   }

   public void setParamValue(int index) {
      cbParam.setSelectedIndex(index);
   }

   public String getParamValue() {
      return (String)cbParam.getSelectedItem();
   }

   public int getParamIndex() {
      return cbParam.getSelectedIndex();
   }

   public void addFocusBorder() {
      cbParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
	public void focusLost(FocusEvent fe) {
	}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
        cbParam.setEnabled(false);
      } else {
        cbParam.setEnabled(true);
      }
   }

   public void addGrayComponents(final String option, final FATBOYParam... paramList) {
      grayOptions.add(option);
      grayParams.add(paramList);
      this.checkGrayState();
      cbParam.addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent ev) {
	   if (!ev.getItem().equals(option)) return;
           boolean doGray;
           if (ev.getStateChange() == ItemEvent.SELECTED) {
              doGray = true;
           } else {
              doGray = false;
           }
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].setGray(doGray);
           }
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].checkGrayState();
           }
        }
      });
   }

   public void checkGrayState() {
      for (int i = 0; i < grayOptions.size(); i++) {
	if (cbParam.getSelectedItem().equals(grayOptions.get(i))) {
	   FATBOYParam[] paramList = (FATBOYParam[])grayParams.get(i);
	   for (int j = 0; j < paramList.length; j++) {
	      paramList[j].setGray(true);
	   }
        }
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      cbParam.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           String temp = (String)cbParam.getSelectedItem();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      String temp = (String)cbParam.getSelectedItem();
      if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	mirrorParam.setParamValue(temp);
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(cbParam);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, leftComponent, topComponent, false);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent, boolean isTop) {
      String topPos;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, cbParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, cbParam,y,topPos,topComponent);
   }

}
