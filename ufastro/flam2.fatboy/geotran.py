#!/usr/bin/python
from numarray import *
from pyraf import iraf
import pyfits

mapfile = sys.argv[1]
record = sys.argv[2]
outfile = sys.argv[3]
if (len(sys.argv) > 4): xpix = int(sys.argv[4])
else: xpix = 2048
if (len(sys.argv) > 5): ypix = int(sys.argv[5])
else: ypix = 2048

fitsobj = pyfits.HDUList()
hdu = pyfits.PrimaryHDU()
hdu.data = zeros((ypix,xpix))
xin = []
yin = []
for j in range(100, xpix-100, 50):
   for l in range(100, ypix-100, 50):
      hdu.data[j,l] = 1
      xin.append(j)
      yin.append(l)
#f = open('testin_pin.dat')
#temp = f.read().split('\n')
#f.close()
#temp.remove('')
#for j in range(len(temp)):
   #ax = int(float(temp[j].split()[0]))
   #ay = int(float(temp[j].split()[1]))
   #hdu.data[ay,ax] = 1
   #xin.append(ax)
   #yin.append(ay)
fitsobj.append(hdu)
if (os.access('TEMPgeotran.fits',os.F_OK)): os.unlink('TEMPgeotran.fits')
if (os.access('TEMPout.fits',os.F_OK)): os.unlink('TEMPout.fits')
fitsobj.writeto('TEMPgeotran.fits')
fitsobj.close()

iraf.unlearn('geotran')
iraf.images.immatch.geotran('TEMPgeotran.fits','TEMPout.fits',mapfile,record, interpolant="nearest",fluxconserve="no")

os.unlink('TEMPgeotran.fits')
temp = pyfits.open('TEMPout.fits')
out = where(temp[0].data > 0)
xout = []
yout = []
keep = []
for j in range(len(xin)):
   #z = sqrt((out[1]-xin[j]*2)**2+(out[0]-yin[j]*2)**2)
   z = sqrt((out[1]-xin[j])**2+(out[0]-yin[j])**2)
   b = where(z < min(z)+5)
   if (min(z) > 25):
      keep.append(0)
   else: keep.append(1)
   #xout.append(out[1][b].mean()/2.)
   #yout.append(out[0][b].mean()/2.)
   xout.append(out[1][b].mean())
   yout.append(out[0][b].mean())
temp.close()
xout = array(xout)
yout = array(yout)
f = open(mapfile,'rb')
s = f.read().split('\n')
f.close()
s.remove('')
for j in range(len(s)):
   temp = s[j].split()
   if (temp[0] == 'xshift'):
      xshift = float(temp[1])
   if (temp[0] == 'yshift'):
      yshift = float(temp[1])
xout += xshift
yout += yshift
os.unlink('TEMPout.fits')
f = open(outfile,'wb')
for j in range(len(xin)):
   if (keep[j] == 0): continue
   f.write(str(xout[j])+'\t'+str(yout[j])+'\t'+str(xin[j])+'\t'+str(yin[j])+'\n')
f.close()
