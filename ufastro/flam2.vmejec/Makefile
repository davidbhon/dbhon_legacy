# RCS: 
# $Id: Makefile 14 2008-06-11 01:49:45Z hon $
#
# Macros:

SHELL := /bin/tcsh -f

ifndef GCCID
  GCCID = $(shell ../.gccv)
endif

ifndef RCSMASTER
  RCSMASTER := $(shell /bin/ls -l .. | grep RCS | awk '{print $$11}' | sed 's^/RCS*^^')
endif

include ../.makerules

ifndef EPICS
  EPICS := /gemini/epics
endif

JPKG := uffjec
FJECSRC := src
JPARAM := data
JSOUND := sounds
JIMAGE := images
JAVAC = javac

#JFLAGS = -deprecation -classpath .:/usr/local/swing/swing.jar:/usr/local/java/lib/classes.zip
#JFLAGS = -d $(JPKG) -deprecation
JFLAGS = -classpath $(UFINSTALL)/javalib/ufjca.jar:$(UFINSTALL)/javalib/javaUFProtocol.jar:. -d ./ -deprecation

RCSLIST = Makefile ReadMe ufstartfjec

FJECSOUNDS := \
$(JSOUND)/annoy.wav

FJECIMAGES := \
$(JIMAGE)/combo.jpg 		   $(JIMAGE)/fjec_splash.jpg \
$(JIMAGE)/worksurface.gif	   $(JIMAGE)/button2.jpg

FJECPARAMS := \
$(JPARAM)/detector_params.txt      \
$(JPARAM)/screen_loc_and_size.txt  $(JPARAM)/screen_locs.txt \
$(JPARAM)/status_recs.txt          $(JPARAM)/temperature_params.txt \
$(JPARAM)/FilterLookup.txt         $(JPARAM)/motor_param_V2.txt \
$(JPARAM)/epics_rec_names.txt	   $(JPARAM)/mos_barcode.txt

JSRC :=  \
$(FJECSRC)/fjec.java 		        $(FJECSRC)/version.java \
$(FJECSRC)/fjecEditorFrame.java		$(FJECSRC)/fjecError.java \
$(FJECSRC)/fjecFrameAboutBox.java	$(FJECSRC)/fjecFrame.java \
$(FJECSRC)/TextFile.java \
$(FJECSRC)/EPICSTextField.java		$(FJECSRC)/EPICSComboBox.java \
$(FJECSRC)/EPICS.java			$(FJECSRC)/EPICSLabel.java \
$(FJECSRC)/FJECMotor.java		$(FJECSRC)/JPanelMotorParameters.java \
$(FJECSRC)/JPanelMotorHighLevel.java	$(FJECSRC)/JPanelMotorLowLevel.java \
$(FJECSRC)/RatioLayout.java		$(FJECSRC)/ExtensionFilter.java \
$(FJECSRC)/JPanelTemperature.java	$(FJECSRC)/JGraphPanel.java \
$(FJECSRC)/TemperatureSensorParameter.java \
$(FJECSRC)/GraphElement.java		$(FJECSRC)/JPanelEPICSRecs.java \
$(FJECSRC)/JPanelDetectorHighLevel.java	$(FJECSRC)/JPanelDetectorLowLevel.java\
$(FJECSRC)/JPanelMOSBarcodeReader.java  $(FJECSRC)/JPanelEngObserve.java \
$(FJECSRC)/EPICSCarListener.java	$(FJECSRC)/EPICSCarCallback.java \
$(FJECSRC)/JPanelScripts.java		$(FJECSRC)/FJECSubSystemPanel.java \
$(FJECSRC)/FJECMotorAnimationPanel.java $(FJECSRC)/JPanelMonitor.java \
$(FJECSRC)/UFGraphicsPanel.java		$(FJECSRC)/JPanelMaster.java \
$(FJECSRC)/EDTTestButton.java		$(FJECSRC)/EPICSApplyButton.java \
$(FJECSRC)/UFRashSkinner.java		$(FJECSRC)/JPanelLVDT.java \
$(FJECSRC)/JPanelOIWFS.java		$(FJECSRC)/JPanelGIS.java \
$(FJECSRC)/JPanelIS.java		$(FJECSRC)/FJECSounds.java \
$(FJECSRC)/EPICSErrorWindow.java        $(FJECSRC)/EPICSLowIndexorBox.java \
$(FJECSRC)/JPanelMindReader.java	$(FJECSRC)/EPICSCadTester.java

JCLASS := $(patsubst %.java, %.class, $(JSRC))

$(JPKG)/%.class: $(FJECSRC)/%.java
	$(JAVAC) $(JFLAGS) $<

#targets:

install: initpub fjec.jar
	cp -f -p ufstartfjec $(UFINSTALL)/bin
	cd $(UFINSTALL)/bin; ln -s ufstartfjec uffjec
	cp -f -p $(FJECPARAMS) $(UFINSTALL)/vmefjec
	chmod 644 $(UFINSTALL)/vmefjec/screen_locs.txt $(UFINSTALL)/vmefjec/screen_loc_and_size.txt
	cp -f -p $(FJECIMAGES) $(UFINSTALL)/vmefjec 
	cp -f -p $(FJECSOUNDS) $(UFINSTALL)/vmefjec
	cp -f -p fjec.jar $(UFINSTALL)/vmefjec
	-@echo installed fjec.jar and param files

show:
	-@echo OS= $(OS)
	-@echo OS_VERSION= $(OS_VERSION)
	-@echo JAVAC = $(JAVAC)
	-@echo JFLAGS = $(JFLAGS)
	-@echo RCSLIST = $(RCSLIST)
	-@echo JCLASS = $(JCLASS)
	-@echo FJECPARAMS = $(FJECPARAMS)
	-@echo FJECSRC= $(FJECSRC)
	-@echo JPKG= $(JPKG)
	-@echo RCSMASTER= $(RCSMASTER)

init:
	mkdir -p $(FJECSRC) $(JPARAM) $(JIMAGE) $(JSOUND)
	cd $(FJECSRC); $(RM) RCS; ln -s $(RCSMASTER)/flam2.vmejec/$(FJECSRC)/RCS; co RCS/*,v
	cd $(JPARAM); $(RM) RCS; ln -s $(RCSMASTER)/flam2.vmejec/$(JPARAM)/RCS; co RCS/*,v
	cd $(JIMAGE); $(RM) RCS; ln -s $(RCSMASTER)/flam2.vmejec/$(JIMAGE)/RCS; co RCS/*,v
	cd $(JSOUND); $(RM) RCS; ln -s $(RCSMASTER)/flam2.vmejec/$(JSOUND)/RCS; co RCS/*,v
	co ufstartfjec

initpub:
	source ../.ufcshrc; mkdir -p $(UFINSTALL)/vmefjec; 

cleanpub:
	source ../.ufcshrc; $(RM) -r $(UFINSTALL)/vmefjec

pubclean: cleanpub initpub
	@echo cleaned and re-initialized $(UFINSTALL)/vmefjec

.uffjec: $(JSRC)
	$(JAVAC) $(JFLAGS) $(JSRC)

#.jca:	
#	pushd $(EPICS)/extensions/javalib; cp jca2.jar /tmp/jca$(USER).jar
#	mv /tmp/jca$(USER).jar ./jca.jar


fjec.jar: .uffjec
	echo 'Main-Class: uffjec.fjec' >! manifest
	echo 'Class-Path: fjec.jar ../javalib/ufjca.jar ../javalib/javaUFProtocol.jar' >> manifest
	jar cmf manifest $@ $(JPKG) 

clean:
	-$(RM) -rf $(JPKG) $(FJECSRC)/{*.class,*.jar} *.jar *.class *~ 

cleen: 
	-$(RM) -rf $(JPKG) $(FJECSRC)/{*.class,*.jar} *.jar *.class *~ 

rashkinstall: clean cleanpub install cleen
