package uffjec;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;

//===============================================================================
/**
 *Low Level tabbed pane
 */
public class JPanelMotorLowLevel extends JPanel{
    //-------------------------------------------------------------------------------
    /**
     *Default Constructor
     *@param fjecmotors FJECMotor[]: Array of FJECMotors 
     */
    public JPanelMotorLowLevel(FJECMotor [] fjecmotors) {
	setLayout(new RatioLayout());
	JPanel singleMotorPanel = new JPanel();
	singleMotorPanel.setLayout(new GridLayout(0,1));
	singleMotorPanel.add(FJECMotor.getMotorLowLevelLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) singleMotorPanel.add(fjecmotors[i].getMotorLowLevelPanel());
	//singleMotorPanel.add(new FJECSubSystemPanel("cc:"));
	this.add("0.01,0.01;0.99,0.80", singleMotorPanel);
	this.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	this.add(FJECSubSystemPanel.consistantLayout, new FJECSubSystemPanel("cc:"));
	JButton abortAllButton = new JButton("Abort All");
	abortAllButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:abort.All",EPICS.MARK);
		    EPICS.put(EPICS.prefix+"apply.DIR",EPICS.START);
		}
	    });
	JButton originAllButton = new JButton("Origin All");
	originAllButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:setup.Origin","All");
		    EPICS.put(EPICS.prefix+"apply.DIR",EPICS.START);
		}
	    });
	JButton statusAllButton = new JButton("Status All");
	statusAllButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:setup.Status","All");
		    EPICS.put(EPICS.prefix+"apply.DIR",EPICS.START);
		}
	    });
	add("0.76,0.90;0.08,0.08",abortAllButton);
	add("0.84,0.90;0.08,0.08",originAllButton);
	add("0.92,0.90;0.08,0.08",statusAllButton);
    }

    
}
