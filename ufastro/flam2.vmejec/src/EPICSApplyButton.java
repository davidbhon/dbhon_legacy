package uffjec;

import ufjca.*;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.plaf.*;


public class EPICSApplyButton extends JButton {


    public EPICSApplyButton() {
	this("Apply","");
    }
    

    public static final String consistantLayout = "0.01,0.90;0.18,0.10";
    
    public EPICSApplyButton(String title, final String markRec) {
	super(title);
	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (markRec != null && !markRec.trim().equals(""))
			EPICS.put(markRec,EPICS.MARK);
		    EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
		}
	    });
    }
}
