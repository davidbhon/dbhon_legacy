package uffjec;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;
import java.util.Date.*;
import java.sql.Timestamp;

//===============================================================================
/**
 *This Class handles the Temperature tabbed pane
 */
public class JPanelTemperature extends JPanel {
  TextFile pw;
  public static final String rcsID = "$Name:  $ $Id: JPanelTemperature.java,v 0.33 2005/09/16 19:15:25 drashkin Exp $";
  public String fileName;                 // file containing temperature sensors config.
  public String td_filename = "cc.td"; // default temperature graph data filename is cc.td

  // do NOT automatically put set point,
  // can cause bad temperature setting, so use set point PUT button.

  EPICSTextField set_Point ;
  EPICSTextField p_Text; // = new JTextField();
  EPICSTextField i_Text; // = new JTextField();
  EPICSTextField d_Text; // = new JTextField();
  JLabel p_Label = new JLabel("    P1 =");
  JLabel i_Label = new JLabel("    I1 =");
  JLabel d_Label = new JLabel("    D1 =");
  JLabel diffLabel1 = new JLabel("Difference = ");
  JLabel diff1 = new JLabel("0",JLabel.RIGHT);
  JLabel diffUnits1 = new JLabel("K");
    
  EPICSTextField set_Point2 ;
  EPICSTextField p_Text2; // = new JTextField();
  EPICSTextField i_Text2; // = new JTextField();
  EPICSTextField d_Text2; // = new JTextField();
  JLabel p_Label2 = new JLabel("    P2 =");
  JLabel i_Label2 = new JLabel("    I2 =");
  JLabel d_Label2 = new JLabel("    D2 =");
  JLabel diffLabel2 = new JLabel("Difference = ");
  JLabel diff2 = new JLabel("0",JLabel.RIGHT);
  JLabel diffUnits2 = new JLabel("K");

  JButton jButtonPID = new JButton("Hide  PID1");
  JButton jButtonPID2 = new JButton("Hide  PID2");
  JTextField actualTdet = new JTextField();
    TemperatureSensorParameter graphSensorParams[];
    TemperatureSensorParameter sensorParms[];
  TemperatureSensorParameter pressureSensor[];
    TemperatureSensorParameter detectorSensor[];
  GridLayout gridLayout1 = new GridLayout();
  GridLayout gridLayout2 = new GridLayout();
  GridLayout gridLayout3 = new GridLayout();
  JPanel jPanelLeft = new JPanel();
  JPanel jSetPUnitPanel = new JPanel();
  JPanel jColorPanel = new JPanel();
  JPanel jPanelSensorLabel = new JPanel();
  JPanel jPanelSensorTemp = new JPanel();
  JPanel jPanelSensorUnits = new JPanel(); 
  JPanel jCheckBoxPanel = new JPanel();
  JPanel jColorPanel2 = new JPanel();
  JPanel jPanelSensorLabel2 = new JPanel();
  JPanel jPanelSensorTemp2 = new JPanel();
  JPanel jPanelSensorUnits2 = new JPanel(); 
  JPanel jCheckBoxPanel2 = new JPanel();
  JPanel jPanelTempControl = new JPanel();
  JPanel jRatePanel = new JPanel();
  JPanel jPanelHeater = new JPanel();
  JPanel jRateUnitPanel = new JPanel();
  JPanel jGraphButtonPanel = new JPanel();
  JGraphPanel jGraphPanel;
  JButton jGraphStartButton = new JButton("Start Graph");
  JButton jGraphParamButton = new JButton("Graph Parameters");
  JButton jGraphClearButton = new JButton("Clear Graph Data");
  JButton jGraphLoadButton = new JButton("Load Graph Data");
  GraphThread graphThread;
    //  RateThread rateThread;
  JComboBox heaterComboBox;
    //  TemperatureSensorParameter Pressure = new TemperatureSensorParameter("Pressure","ec:setup:sad:LS218Kelvin1");
  JDialogGraphParameters dialogBox; // create dialog box after getting
                                    // graph parameters from file or UFlib
  JCheckBox jCheckBoxGraphGrid = new JCheckBox("Show Grid");
  JCheckBox jCheckBoxGraphAutoscale = new JCheckBox("Autoscale graph if empty");
  JLabel errMsgLabel = new JLabel();
    String prefix = EPICS.prefix;

    String dataOutputFormat = "CSV";
    

//-------------------------------------------------------------------------------
  /**
   *Constructs the frame
   *@param fileName string that contains the file name
   */
  public JPanelTemperature(String fileName) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      this.fileName = fileName;
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of JPanelTemperature

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception
  {
  
    this.setLayout(new RatioLayout());

    String [] items = {"Off (0)","Low (1)","Medium (2)","High (3)"};
    heaterComboBox = new JComboBox( items );
    ReadFile( fileName );
    // add new sensor parameter for the vacuum sensor
    //TemperatureSensorParameter [] tspTemp = new TemperatureSensorParameter[SensorParms.length+1];
    //for (int i=0; i<SensorParms.length; i++) tspTemp[i] = SensorParms[i];
    //tspTemp[tspTemp.length-1] = Pressure;
    //SensorParms = new TemperatureSensorParameter[tspTemp.length];
    //SensorParms = tspTemp;
    //Pressure.hotThreshold = 1.e-4;
    //Pressure.coldThreshold = 1.3e-6;
    //Pressure.units.setText("Torr");
    graphThread = new GraphThread();
    dialogBox = new JDialogGraphParameters();



//     JTextField set_Point = new JTextField();
//     JTextField p_Text = new JTextField();
//     JTextField i_Text = new JTextField();
//     JTextField d_Text = new JTextField();
//     JTextField set_Point2 = new JTextField();
//     JTextField p_Text2 = new JTextField();
//     JTextField i_Text2 = new JTextField();
//     JTextField d_Text2 = new JTextField();


    set_Point = new EPICSTextField(prefix+"ec:tempSetG.A","No Description");
    p_Text = new EPICSTextField(prefix+"ec:tempSetG.B","No Description");
    p_Text.setHorizontalAlignment(JTextField.RIGHT);
    i_Text = new EPICSTextField(prefix+"ec:tempSetG.C","No Description");
    i_Text.setHorizontalAlignment(JTextField.RIGHT);
    d_Text = new EPICSTextField(prefix+"ec:tempSetG.D","No Description");
    d_Text.setHorizontalAlignment(JTextField.RIGHT);
    set_Point2 = new EPICSTextField(prefix+"ec:tempSetG.E","No Description");
    p_Text2 = new EPICSTextField(prefix+"ec:tempSetG.F","No Description");
    p_Text2.setHorizontalAlignment(JTextField.RIGHT);
    i_Text2 = new EPICSTextField(prefix+"ec:tempSetG.G","No Description");
    i_Text2.setHorizontalAlignment(JTextField.RIGHT);
    d_Text2 = new EPICSTextField(prefix+"ec:tempSetG.H","No Description");
    d_Text2.setHorizontalAlignment(JTextField.RIGHT);


    actualTdet.setEditable(false);
    actualTdet.setBackground(new Color(175,175,175));
    //this.setMaximumSize(new Dimension(800, 470));
    //this.setMinimumSize(new Dimension(800, 470));
    //this.setPreferredSize(new Dimension(800, 470));
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    gridLayout1.setVgap(0);
    gridLayout2.setColumns(1);
    gridLayout2.setRows(0);
    gridLayout2.setVgap(0);
    gridLayout3.setColumns(1);
    gridLayout3.setRows(0);
    gridLayout3.setVgap(0);
    jPanelLeft.setLayout(gridLayout1);
    jSetPUnitPanel.setLayout(gridLayout1);
    jColorPanel.setLayout(gridLayout2);
    jPanelSensorLabel.setLayout(gridLayout2);
    jPanelSensorTemp.setLayout(gridLayout2);
    jPanelSensorUnits.setLayout(gridLayout2);
    jRateUnitPanel.setLayout(gridLayout2);
    jCheckBoxPanel.setLayout(gridLayout2);
    jColorPanel2.setLayout(gridLayout2);
    jPanelSensorLabel2.setLayout(gridLayout2);
    jPanelSensorTemp2.setLayout(gridLayout2);
    jPanelSensorUnits2.setLayout(gridLayout2);
    jCheckBoxPanel2.setLayout(gridLayout2);
    jRatePanel.setLayout(gridLayout2);
    jGraphPanel.setLayout(gridLayout1);
    jPanelTempControl.setLayout(gridLayout1);

    jButtonPID.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonPID_action(); } });
    jButtonPID2.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonPID2_action(); } });

    jGraphLoadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphLoadButton_action(e); } });

    jGraphStartButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphStartButton_action(e); } });

    jGraphClearButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {     
		if (graphThread.isAlive()) 
		    jGraphStartButton_action(e); // stop the graph thread before clearing data! 
		jGraphPanel.eraseGraphData();
		fjecFrame.jPanelTemperature.repaint();
	    }
	});

    jGraphParamButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphParamButton_action(e); } });


    jCheckBoxGraphGrid.setSelected(true);
    jCheckBoxGraphGrid.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { 
		jGraphPanel.setDrawGrid(jCheckBoxGraphGrid.isSelected()); 
		repaint();
	    } });
    jCheckBoxGraphAutoscale.setSelected(true);
    jCheckBoxGraphAutoscale.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		jGraphPanel.setAutoscale(jCheckBoxGraphAutoscale.isSelected());
	    }
	});
    jColorPanel.add(new JLabel(""));
    jPanelSensorLabel.add(new JLabel("Sensor Name"));
    jPanelSensorTemp.add(new JLabel("Temperature"));
    jPanelSensorUnits.add(new JLabel(""));
    jRatePanel.add(new JLabel(" Rate"));
    jRateUnitPanel.add(new JLabel(""));
    jCheckBoxPanel.add(new JLabel(""));

    jColorPanel2.add(new JLabel(""));
    jPanelSensorLabel2.add(new JLabel("Sensor Name"));
    jPanelSensorTemp2.add(new JLabel("Temperature"));
    jPanelSensorUnits2.add(new JLabel(""));
    jCheckBoxPanel2.add(new JLabel(""));

    for (int i=0; i<sensorParms.length/2; i++) {
      jColorPanel.add(sensorParms[i].statusColor);
      jPanelSensorLabel.add(sensorParms[i].label);
      jPanelSensorTemp.add(sensorParms[i].temp);
      jPanelSensorUnits.add(sensorParms[i].units);
      jRatePanel.add(sensorParms[i].graphElement.rate);
      jRateUnitPanel.add(sensorParms[i].graphElement.units);
      jCheckBoxPanel.add(sensorParms[i].graphElement.showme);
    }
    for (int i=sensorParms.length/2; i<sensorParms.length; i++) {
      jColorPanel2.add(sensorParms[i].statusColor);
      jPanelSensorLabel2.add(sensorParms[i].label);
      jPanelSensorTemp2.add(sensorParms[i].temp);
      jPanelSensorUnits2.add(sensorParms[i].units);
      jRatePanel.add(sensorParms[i].graphElement.rate);
      jRateUnitPanel.add(sensorParms[i].graphElement.units);
      jCheckBoxPanel2.add(sensorParms[i].graphElement.showme);
    }

    //jColorPanel.add(Pressure.statusColor);
    //jPanelSensorLabel.add(Pressure.label);
    //jPanelSensorTemp.add(Pressure.temp);
    //jPanelSensorUnits.add(Pressure.units);
    //jRatePanel.add( new JLabel());
    //jRateUnitPanel.add(new JLabel());
    //jCheckBoxPanel.add(Pressure.graphElement.showme);

   
    jPanelLeft.add(new JLabel()); //add dummy components for spacing
    jPanelTempControl.add(new JLabel("Detector  T"));
    jSetPUnitPanel.add(new JLabel()); // add dummy components for spacing

    jPanelLeft.add(new JLabel("Cam Set Point A="));
    jPanelTempControl.add(set_Point);
    set_Point.setHorizontalAlignment(JTextField.RIGHT);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(new JLabel("Actual Tmp 1="));
    jPanelTempControl.add(detectorSensor[0].temp);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(diffLabel1);
    jPanelTempControl.add(diff1);
    jSetPUnitPanel.add(diffUnits1);

    jPanelLeft.add(p_Label);
    jPanelTempControl.add(p_Text);

    jPanelLeft.add(i_Label);
    jPanelTempControl.add(i_Text);

    jPanelLeft.add(d_Label);
    jPanelTempControl.add(d_Text);

    for (int i=0; i<3; i++) jSetPUnitPanel.add(new JLabel(""));

    jPanelLeft.add(new JLabel("Cam Set Point B="));
    jPanelTempControl.add(set_Point2);
    set_Point2.setHorizontalAlignment(JTextField.RIGHT);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(new JLabel("Actual Tmp 2="));
    jPanelTempControl.add(detectorSensor[1].temp);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(diffLabel2);
    jPanelTempControl.add(diff2);
    jSetPUnitPanel.add(diffUnits2);

    jPanelLeft.add(p_Label2);
    jPanelTempControl.add(p_Text2);

    jPanelLeft.add(i_Label2);
    jPanelTempControl.add(i_Text2);

    jPanelLeft.add(d_Label2);
    jPanelTempControl.add(d_Text2);

    for (int h=0; h<3; h++) jSetPUnitPanel.add(new JLabel()); //add dummy components for spacing
    JPanel tcPanel = new JPanel();
    tcPanel.setLayout(new RatioLayout());
    tcPanel.setBorder(new EtchedBorder(0));
    JLabel trashTalker = new JLabel("LS332");
    trashTalker.setHorizontalAlignment(JLabel.CENTER);
    tcPanel.add("0.01,0.00;0.99,0.05", trashTalker);
    tcPanel.add("0.01,0.05;0.45,0.95",jPanelLeft);
    tcPanel.add("0.46,0.05;0.45,0.95",jPanelTempControl);
    tcPanel.add("0.91,0.05;0.09,0.95",jSetPUnitPanel);

    JPanel tmPanelMOSDewar = new JPanel();
    tmPanelMOSDewar.setLayout(new RatioLayout());
    tmPanelMOSDewar.setBorder(new EtchedBorder(0));
    JLabel trash1 = new JLabel("LS218 -- MOS Dewar");
    trash1.setHorizontalAlignment(JLabel.CENTER);
    tmPanelMOSDewar.add("0.01,0.00;0.99,0.08", trash1);
    tmPanelMOSDewar.add("0.01,0.08;0.10,0.92", jColorPanel);
    tmPanelMOSDewar.add("0.11,0.08;0.38,0.92", jPanelSensorLabel);
    tmPanelMOSDewar.add("0.49,0.08;0.32,0.92", jPanelSensorTemp);
    tmPanelMOSDewar.add("0.81,0.08;0.10,0.92", jPanelSensorUnits);
    tmPanelMOSDewar.add("0.91,0.08;0.06,0.92", jCheckBoxPanel);

    JPanel tmPanelCamera = new JPanel();
    tmPanelCamera.setLayout(new RatioLayout());
    tmPanelCamera.setBorder(new EtchedBorder(0));
    JLabel trash2 = new JLabel("LS218 -- Camera");
    trash2.setHorizontalAlignment(JLabel.CENTER);
    tmPanelCamera.add("0.01,0.00;0.99,0.08", trash2);
    tmPanelCamera.add("0.01,0.08;0.10,0.92", jColorPanel2);
    tmPanelCamera.add("0.11,0.08;0.38,0.92", jPanelSensorLabel2);
    tmPanelCamera.add("0.49,0.08;0.32,0.92", jPanelSensorTemp2);
    tmPanelCamera.add("0.81,0.08;0.10,0.92", jPanelSensorUnits2);
    tmPanelCamera.add("0.91,0.08;0.06,0.92", jCheckBoxPanel2);

    JPanel pressurePanel = new JPanel();
    pressurePanel.setBorder(new EtchedBorder(0));
    pressurePanel.setLayout(new GridLayout(0,1));
    JPanel pressurePanel_sub0 = new JPanel();
    pressurePanel_sub0.setLayout(new RatioLayout());
    JLabel trash3 = new JLabel("Pressure");
    trash3.setHorizontalAlignment(JLabel.CENTER);
    //pressurePanel_sub0.add("0.01,0.00;0.99,0.20",trash3);
    pressurePanel_sub0.add("0.01,0.01;0.48,0.99",pressureSensor[0].label);
    pressurePanel_sub0.add("0.49,0.01;0.32,0.99",pressureSensor[0].temp);
    pressurePanel_sub0.add("0.81,0.01;0.10,0.99",pressureSensor[0].units);
    pressurePanel_sub0.add("0.91,0.01;0.06,0.99",pressureSensor[0].graphElement.showme);
    JPanel pressurePanel_sub1 = new JPanel();
    pressurePanel_sub1.setLayout(new RatioLayout());
    //JLabel trash4 = new JLabel("Pressure");
    trash3.setHorizontalAlignment(JLabel.CENTER);
    ////pressurePanel_sub1.add("0.01,0.00;0.99,0.20",trash4);
    pressurePanel_sub1.add("0.01,0.01;0.48,0.99",pressureSensor[1].label);
    pressurePanel_sub1.add("0.49,0.01;0.32,0.99",pressureSensor[1].temp);
    pressurePanel_sub1.add("0.81,0.01;0.10,0.99",pressureSensor[1].units);
    pressurePanel_sub1.add("0.91,0.01;0.06,0.99",pressureSensor[1].graphElement.showme);
    pressurePanel.add(trash3);
    pressurePanel.add(pressurePanel_sub0);
    pressurePanel.add(pressurePanel_sub1);

    jPanelHeater.add(new JLabel("Control Loop Heater Power:"));
    jPanelHeater.add(heaterComboBox);

    errMsgLabel.setBorder(new EtchedBorder(0));

    JPanel pidButtonPanel = new JPanel();
    pidButtonPanel.setLayout(new GridLayout(0,1,10,10));

    pidButtonPanel.add(jButtonPID);
    pidButtonPanel.add(jButtonPID2);
    //jPanelPIDButtons.add(jButtonApplyPID);
    //jPanelPIDButtons.add(jButtonQueryPID);
    jGraphButtonPanel.setLayout(new GridLayout(0,1));
    jGraphButtonPanel.add(jGraphStartButton);
    jGraphButtonPanel.add(jGraphParamButton);
    jGraphButtonPanel.add(jGraphClearButton);
//      jGraphButtonPanel.add(jGraphSaveButton);
    jGraphButtonPanel.add(jGraphLoadButton);
    this.add("0.01,0.02;0.20,0.60", tcPanel);
    this.add("0.01,0.77;0.20,0.10", jPanelHeater);
    this.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
    this.add("0.05,0.65;0.12,0.10", pidButtonPanel);
    this.add("0.22,0.65;0.27,0.15", pressurePanel);
    this.add("0.22,0.02;0.27,0.30", tmPanelMOSDewar);
    this.add("0.22,0.32;0.27,0.30", tmPanelCamera);
    //    this.add("0.47,0.02;0.05,0.44", jRatePanel);
    //    this.add("0.525,0.02;0.04,0.44", jRateUnitPanel);
    this.add("0.52,0.02;0.47,0.73", jGraphPanel);
    this.add("0.60,0.78;0.10,0.05", jCheckBoxGraphGrid);
    this.add("0.60,0.83;0.10,0.05", jCheckBoxGraphAutoscale);
    this.add("0.77,0.78;0.18,0.20", jGraphButtonPanel);
    this.add("0.22,0.83;0.35;0.05", errMsgLabel);
    this.add(FJECSubSystemPanel.consistantLayout, new FJECSubSystemPanel("ec:"));
    errMsgLabel.setForeground(Color.RED);
    errMsgLabel.setText("ERROR: The quick brown fox jumped over the lazy dog");
    p_Label.setVisible(false);
    i_Label.setVisible(false);
    d_Label.setVisible(false);
    p_Text.setVisible(false);
    i_Text.setVisible(false);
    d_Text.setVisible(false);
    jButtonPID.setText("Show PID1");
    p_Label2.setVisible(false);
    i_Label2.setVisible(false);
    d_Label2.setVisible(false);
    p_Text2.setVisible(false);
    i_Text2.setVisible(false);
    d_Text2.setVisible(false);
    jButtonPID2.setText("Show PID2");
    
    GregorianCalendar gc = new GregorianCalendar();
    td_filename = "fjec_temperature_graph_log_"+gc.get(Calendar.MONTH) + "_"+gc.get(Calendar.DAY_OF_MONTH) + "_"+gc.get(Calendar.YEAR);
 
  } //end of jbInit


//-------------------------------------------------------------------------------
 /**
  *Event Handler for jGraphLoadButton
  *JPanelTemperature Graph load button action performed
  *@param e not used
  */
 void jGraphLoadButton_action (ActionEvent e) {
    int counter =0;
    try {
      JFileChooser fc = new JFileChooser(new File(fjec.data_path));
      ExtensionFilter t_graph_type = new ExtensionFilter (
		     "CC graph data files", new String[] {".td"});
      fc.setFileFilter (t_graph_type); // Initial filter setting
      fc.setDialogTitle("Load graph data from");
      fc.setSelectedFile(new File(td_filename));
      fc.setApproveButtonText("Load");
      int returnVal = fc.showOpenDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        String t_file_name = fc.getSelectedFile().getAbsolutePath();
      	if (!t_file_name.endsWith(".td")) t_file_name += ".td";
        TextFile inp = new TextFile(t_file_name,TextFile.IN);
	loadGraphDataCSV(inp);
	inp.close();
      }
    }
    catch (Exception ee) {
       //fjecFrameErrorLog.addError("JPanelTemperature","jGraphLoadButton_action()",ee.toString());
       fjecError.show("problem reading temperature data file : " + counter + " " +ee.toString());
    }
  } //end of jGraphLoadButton_action

    void loadGraphDataCSV(TextFile inp) {
	try {
	    jGraphPanel.eraseGraphData();	    
	    String line = "";
	    while ( (line = inp.nextUncommentedLine()) != null) {
		StringTokenizer st = new StringTokenizer(line,",");
		long timestamp = Long.parseLong(st.nextToken());
		jGraphPanel.currTime = timestamp;
		for (int i=0; i<graphSensorParams.length; i++) {


		    while (jGraphPanel.xValues[i].size() > 0 &&
			   (timestamp-((Long)jGraphPanel.xValues[i].getLast()).longValue())/1000 > jGraphPanel.timeMax) {
			//System.out.println((currTime-((Long)jGraphPanel.xValues[i].getLast()).longValue())+" "+jGraphPanel.timeMax);
			jGraphPanel.xValues[i].removeLast();
			jGraphPanel.yValues[i].removeLast();
		    }



		    double x = Double.parseDouble(st.nextToken());
		    jGraphPanel.yValues[i].addLast(new Integer((int)(x*100)));
		    jGraphPanel.xValues[i].addFirst(new Long(timestamp));
		}
	    }
	    jGraphPanel.repaint();
	} catch (Exception e) {
	    System.err.println("JPanelTemperature.loadGraphDataCSV> "+e.toString());
	}
    }

    void loadGraphDataXML() {
	
    }

//-------------------------------------------------------------------------------
  /**
   *Event handler for JGraphParamButton
   *JPanelTemperature Graph parameters button action performed
   *@param e not used
   */
  void jGraphParamButton_action (ActionEvent e) {
    Point origin = getLocation();
    Dimension totalSize = getSize();

    dialogBox.graphParams[0].setText(jGraphPanel.timeMin + "");
    dialogBox.graphParams[1].setText(jGraphPanel.timeMax + "");
    dialogBox.graphParams[2].setText(jGraphPanel.tempMin + "");
    dialogBox.graphParams[3].setText(jGraphPanel.tempMax + "");
    dialogBox.graphParams[4].setText(jGraphPanel.xTickDelta + "");
    dialogBox.graphParams[5].setText(jGraphPanel.xLabelDelta + "");
    dialogBox.graphParams[6].setText(jGraphPanel.yTickDelta + "");
    dialogBox.graphParams[7].setText(jGraphPanel.yLabelDelta + "");
    dialogBox.graphParams[8].setText(jGraphPanel.pollPeriod + "");

    dialogBox.setLocation((int)(origin.getX() + totalSize.getWidth()/2),
                           (int)(origin.getY() + totalSize.getHeight()/2));
    dialogBox.setVisible(true);
    repaint();
  } //end of jGraphParamButton


//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonPID
   *JPanelTemperature PID button action performed
   *@param e not used
   */
  void jButtonPID_action() {
    if (p_Label.isShowing()){
      p_Label.setVisible(false);
      i_Label.setVisible(false);
      d_Label.setVisible(false);
      p_Text.setVisible(false);
      i_Text.setVisible(false);
      d_Text.setVisible(false);
      jButtonPID.setText("Show PID1");
    }
    else {
      p_Label.setVisible(true);
      i_Label.setVisible(true);
      d_Label.setVisible(true);
      p_Text.setVisible(true);
      i_Text.setVisible(true);
      d_Text.setVisible(true);
      jButtonPID.setText("Hide  PID1");
    }
    jPanelLeft.repaint();
    jPanelTempControl.repaint();
  } //end of jButtonPID_action
//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonPID2
   *JPanelTemperature PID2 button action performed
   *@param e not used
   */
  void jButtonPID2_action() {
    if (p_Label2.isShowing()){
      p_Label2.setVisible(false);
      i_Label2.setVisible(false);
      d_Label2.setVisible(false);
      p_Text2.setVisible(false);
      i_Text2.setVisible(false);
      d_Text2.setVisible(false);
      jButtonPID2.setText("Show PID2");
    }
    else {
      p_Label2.setVisible(true);
      i_Label2.setVisible(true);
      d_Label2.setVisible(true);
      p_Text2.setVisible(true);
      i_Text2.setVisible(true);
      d_Text2.setVisible(true);
      jButtonPID2.setText("Hide  PID2");
    }
    jPanelLeft.repaint();
    jPanelTempControl.repaint();
  } //end of jButtonPID2_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for jGraphStartButton
   *JPanelTemperature start button action performed
   *@param e not used
   */
  void jGraphStartButton_action(ActionEvent e) {
    if (graphThread.isAlive()) {
      graphThread.keepRunning = false;
      jGraphStartButton.setText("Start Graph");
      while (graphThread.stillRunning);//wait for graphThread to exit
      printFooter(pw);
      pw.close();
    }
    else {
      try {
        JFileChooser fc = new JFileChooser(fjec.data_path);
  //      fc.setSelectedFile("T-ReCS.tgd");
        ExtensionFilter t_graph_type = new ExtensionFilter (
          "Flamingos-2 graph data files", new String[] {".td"});
        fc.setFileFilter (t_graph_type); // Initial filter setting
        fc.setDialogTitle("Save graph data to");
        fc.setSelectedFile(new File(td_filename));
        fc.setApproveButtonText("Save");
        int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          graphThread = new GraphThread();
          String t_file_name = fc.getSelectedFile().getAbsolutePath();
          if (!t_file_name.endsWith(".td")) t_file_name += ".td";
          pw = new TextFile(t_file_name,TextFile.OUT);
          //pw.println("; Temperature Graph Data file");
          //pw.println(";");
          //pw.println("; Poll Rate");
          //pw.println(jGraphPanel.pollPeriod+"");
//          pw.println("; Max number of data points");
//          pw.println(jGraphPanel.maxDataPoints+"");
//          pw.println("; Actual number of data points");
//          pw.println(jGraphPanel.size+"");
//          pw.println(";");
//          pw.println("; Data points have been scaled by 100 to preserve precision");
//          pw.println(";");
          //pw.println(";");
          td_filename = t_file_name;
	  printHeader(pw);
	  jGraphPanel.eraseGraphData();
          graphThread.start();
          jGraphStartButton.setText("Stop Graph");
        }
          /*for (int i=0; i<SensorParms.length; i++) {
            ListIterator lit = jGraphPanel.xValues[i].listIterator(0);
            String s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            pw.println(s);
            lit = jGraphPanel.yValues[i].listIterator(0);
            s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            pw.println(s);
          }
          pw.close();
        }   */
      }
      catch (Exception ee) {
        //fjecFrameErrorLog.addError("JPanelTemperature","jGraphStartButton_action()",ee.toString());
      }
    }
  } //end of jGraphStartButton_action

    public void printLine(TextFile outFile, String line) {
	if (dataOutputFormat.equals("CSV")) {
	    outFile.println(line);
	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println(" <row>");
	    int count = 0;
	    StringTokenizer st = new StringTokenizer(line,",");
	    while (st.hasMoreTokens()) {
		String s = st.nextToken();
		outFile.println("  <Col"+count+">"+s+"</Col"+(count++)+">");
	    }
	    outFile.println(" </row>");
	} else {
	    System.err.println("JPanelTemperature.printLine> Unknown data output format: "+dataOutputFormat);
	}
    }
    public void printHeader(TextFile outFile) {
	if (dataOutputFormat.equals("CSV")) {
	    
	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println("<document>");
	} else {
	    System.err.println("JPanelTemperature.printHeader> Unknown data output format: "+dataOutputFormat);
	}
    }
    public void printFooter(TextFile outFile) {
	if (dataOutputFormat.equals("CSV")) {

	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println("</document>");
	} else {
	    System.err.println("JPanelTemperature.printFooter> Unknown data output format: "+dataOutputFormat);	    
	}
    }

//===============================================================================
  /**
   *TBD
   */
//   public class RateThread extends Thread {

//     boolean keepRunning = true;
//     int sleepamount = jGraphPanel.pollPeriod;
//     double []oldValues = new double[SensorParms.length];

// //-------------------------------------------------------------------------------
//     /**
//      *TBD
//      */
//     public void run() {
//       for (int i=0; i<SensorParms.length; i++) oldValues[i] = 0.0;
//       while (keepRunning) {
//         for (int i=0; i<SensorParms.length; i++) {
//           double currRate = (SensorParms[i].currentTemp - oldValues[i]) / sleepamount;
//           double oldRate = 0.0;
//           try {
//             oldRate = Double.parseDouble(SensorParms[i].graphElement.rate.getText());
//           }
//           catch (Exception e){
//             oldRate = 0.0;
//           }
//           SensorParms[i].graphElement.rate.setText(( (oldRate+currRate) / 2.0) + "");
//           oldValues[i] = SensorParms[i].currentTemp;
//         }// end of for loop
//         try {
//           this.sleep(sleepamount*1000);
//         }
//         catch (Exception e) {
//           JOptionPane.showMessageDialog(null,e.toString(),"RateThread Error",JOptionPane.ERROR_MESSAGE);
//         }
//       }
//     }//end of run
//   }//end of class RateThread

//===============================================================================
  /**
   *TBD
   */
  public class GraphThread extends Thread {
    boolean keepRunning = true;
      boolean stillRunning = false;
//-------------------------------------------------------------------------------
    /**
     *Displays line on the graph when start graph is clicked
     *logs the graph data in the .tgd file
     *
     */
      
    public void run() {
      while( keepRunning )
        try {
            stillRunning = true;
	    long currTime = System.currentTimeMillis();
	    jGraphPanel.currTime = currTime;
	    String lineToPrint = currTime+",";
	    for (int i=0; i<graphSensorParams.length; i++) {
		//pw.print( SensorParms[i].currentTemp +":"+SensorParms[i].temp.getTimeStamp()+"$");
		lineToPrint += (graphSensorParams[i].currentTemp+",");
		while (jGraphPanel.xValues[i].size() > 0 &&
		       (currTime-((Long)jGraphPanel.xValues[i].getLast()).longValue())/1000 > jGraphPanel.timeMax) {
		    //System.out.println((currTime-((Long)jGraphPanel.xValues[i].getLast()).longValue())+" "+jGraphPanel.timeMax);
		    jGraphPanel.xValues[i].removeLast();
		    jGraphPanel.yValues[i].removeLast();
		}
		// Add Integers since linkedlists can only hold objects, not primitives.
		// Scale by 100 in order to preserve precision
		jGraphPanel.xValues[i].addFirst( new Long( graphSensorParams[i].temp.getTimeStamp() ) );
		jGraphPanel.yValues[i].addFirst( new Integer((int)(graphSensorParams[i].currentTemp*100)) );
	    }
	    String pidVals = p_Text.getText()+","+i_Text.getText()+","+d_Text.getText()+","+
		p_Text2.getText()+","+i_Text2.getText()+","+d_Text2.getText();
	    lineToPrint += pidVals;
	    printLine(pw,lineToPrint);
	    repaint();
	    if( jGraphPanel.pollPeriod <= 0 )
		keepRunning = false;
	    else
		this.sleep(jGraphPanel.pollPeriod*1000);
	    //System.out.println(jGraphPanel.size+"");
	}
      catch (Exception e) {
	  JOptionPane.showMessageDialog(null,e.toString(),"GraphThread Error",JOptionPane.ERROR_MESSAGE);
      }
      stillRunning = false;
    }
  } //end of class GraphThread

//-------------------------------------------------------------------------------
  /**
   *Reads the temperature parameter file
   *@param filename String: contains the name of the file name used
   */

//     public void ReadFile(String filename) {
// 	TextFile in_file = new TextFile(filename,TextFile.IN);
// 	try {
	    
// 	} catch (Exception e) {
// 	    System.err.println("JPanelTemperature.ReadFile> "+e.toString());
// 	}
//     }
  public void ReadFile(String filename) {

    TextFile in_file = new TextFile(filename,TextFile.IN);

    try {
      String line;
      StringTokenizer st;
      line = in_file.nextUncommentedLine();
      int numSensors=0; int maxLabelSize = 0;
      try {  numSensors = Integer.parseInt(line);
             sensorParms = new TemperatureSensorParameter[numSensors]; }
      catch (Exception e) {fjecError.show("Bad number of sensors parameter"); }
      for (int i=0; i<numSensors; i++) {
        line = in_file.nextUncommentedLine();
        st = new StringTokenizer(line," \t");
        // assume that all words are part of the sensor name
        // except the last three, which should be the epics rec name and Hot/Cold Threshold Values
        int numTokens = st.countTokens();
        String s = "";
        for (int j=0; j<numTokens-3; j++)
          s += st.nextToken() + " ";
        s = s.substring(0,s.length()-1); // strip off the last space
        if (s.length() > maxLabelSize) maxLabelSize = s.length();
        sensorParms[i] = new TemperatureSensorParameter (s,st.nextToken());
        try { sensorParms[i].hotThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { fjecError.show("Bad Hot Threshold Value: "+e.toString()); }
        try { sensorParms[i].coldThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { fjecError.show("Bad Cold Threshold Value: "+e.toString()); }
	sensorParms[i].checkThreshold();
      }
      // make label spacing uniform
      for (int i=0; i<numSensors; i++) {
        String s = sensorParms[i].label.getText();
        while (s.length() < maxLabelSize) s += " ";
        sensorParms[i].label.setText(s);
      }

      pressureSensor = new TemperatureSensorParameter[2];
      pressureSensor[0] = new TemperatureSensorParameter("Camera Pressure","sad:CAMVAC");
      pressureSensor[0].units.setText("Torr");
      pressureSensor[1] = new TemperatureSensorParameter("MOS Pressure","sad:MOSVAC");
      pressureSensor[1].units.setText("Torr");

      detectorSensor = new TemperatureSensorParameter[2];
      detectorSensor[0] = new TemperatureSensorParameter("MOS Setp","sad:LS332Kelvin1");
      detectorSensor[1] = new TemperatureSensorParameter("CAM Setp","sad:LS332Kelvin2");
      
      graphSensorParams = new TemperatureSensorParameter[sensorParms.length+pressureSensor.length+detectorSensor.length];
      
      int j = 0;
      for (int i=0; i<detectorSensor.length; i++) graphSensorParams[j++] = detectorSensor[i];
      for (int i=0; i<sensorParms.length; i++) graphSensorParams[j++] = sensorParms[i];
      for (int i=0; i<pressureSensor.length; i++) graphSensorParams[j++] = pressureSensor[i];

      //graph paramters
      line = in_file.nextUncommentedLine();
      st = new StringTokenizer(line," ");
      // create the graph panel object
      jGraphPanel = new JGraphPanel(graphSensorParams);
      try {
        jGraphPanel.timeMin = Double.parseDouble(st.nextToken());
        jGraphPanel.timeMax = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMin = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMax = Double.parseDouble(st.nextToken());
        jGraphPanel.xTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.xLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.pollPeriod = Integer.parseInt(st.nextToken());
      } catch (Exception e) {fjecError.show("Bad graph Parameters"); }

      in_file.close();
    }
    catch (Exception e) {
      fjecError.show("Couldn't read temperature data file: "+e.toString());
    }
  } //end of ReadFile

//===============================================================================
   /**
    *TBD
    */
    public class JDialogGraphParameters extends JDialog {
	JPanel labelpanel = new JPanel();
	JPanel textpanel = new JPanel();
	JPanel buttonpanel = new JPanel();
	JButton buttonOK = new JButton ("OK");
	JButton buttonCancel = new JButton ("Cancel");
	JTextField [] graphParams = new JTextField[9];
	double timeMin,timeMax,tempMin,tempMax;
	double xTickDelta,xLabelDelta,yTickDelta,yLabelDelta;
	JRadioButton xmlButton = new JRadioButton("XML");
	JRadioButton csvButton = new JRadioButton("CSV",true);
	
//-------------------------------------------------------------------------------
      /**
       *Default Constructor
       */
      JDialogGraphParameters () {
        super();
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        this.setModal(true);
        try  {
          jbInit();
        }
        catch(Exception e) {
          e.printStackTrace();
        }
      } //end of JDialogGraphParameters

//-------------------------------------------------------------------------------
      /**
       *Component Initialization
       */
      private void jbInit () {
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonOK_action(e);
          }
        });
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonCancel_action(e);
          }
        });
        labelpanel.setLayout(new GridLayout(0,1,0,10));
        textpanel.setLayout(new GridLayout(0,1,0,10));
        buttonpanel.setLayout(new FlowLayout());
        this.getContentPane().setLayout (new BorderLayout());
        labelpanel.add(new JLabel());
        labelpanel.add(new JLabel("Min Time"));
        labelpanel.add(new JLabel("Max Time"));
        labelpanel.add(new JLabel("Display Min Temp"));
        labelpanel.add(new JLabel("Display Max Temp"));
        labelpanel.add(new JLabel("x Tick Delta"));
        labelpanel.add(new JLabel("x Label Delta"));
        labelpanel.add(new JLabel("y Tick Delta"));
        labelpanel.add(new JLabel("y Label Delta"));
        labelpanel.add(new JLabel("Poll Period (sec)"));
	labelpanel.add(new JLabel("Savefile Format"));
	labelpanel.add(new JLabel());
        textpanel.add(new JLabel());
        for (int i=0; i<9; i++) {
           graphParams[i] = new JTextField("0.0",5);
           textpanel.add(graphParams[i]);
        }
	ButtonGroup bg = new ButtonGroup();
	bg.add(xmlButton);
	bg.add(csvButton);
	textpanel.add(csvButton);
	textpanel.add(xmlButton);
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
        buttonpanel.add(buttonOK);
        buttonpanel.add(buttonCancel);
        this.getContentPane().add(labelpanel,BorderLayout.WEST);
        this.getContentPane().add(textpanel,BorderLayout.EAST);
        this.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
        this.setTitle("Graph Parameters");
        buttonOK.grabFocus();
        this.setSize(new Dimension(200,350));
      } //end of jbInit

//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonOK
       *JDialogGraphParameters ok button action performed
       *@param e not used
       */
      void buttonOK_action(ActionEvent e) {
        int oldRate = jGraphPanel.pollPeriod;
        int newRate = Integer.parseInt(graphParams[8].getText());
        int i = -1;
	/*
        if (oldMaxPoints != newMaxPoints) {
          i = JOptionPane.showConfirmDialog(null,"These changes will erase graph data",
					    "Warning",JOptionPane.OK_CANCEL_OPTION);
          if (i == JOptionPane.CANCEL_OPTION) return;
          else jGraphPanel.eraseGraphData();
        }
	*/
        jGraphPanel.timeMin = Double.parseDouble(graphParams[0].getText());
        jGraphPanel.timeMax = Double.parseDouble(graphParams[1].getText());
        jGraphPanel.tempMin = Double.parseDouble(graphParams[2].getText());
        jGraphPanel.tempMax = Double.parseDouble(graphParams[3].getText());
        jGraphPanel.xTickDelta = Double.parseDouble(graphParams[4].getText());
        jGraphPanel.xLabelDelta = Double.parseDouble(graphParams[5].getText());
        jGraphPanel.yTickDelta = Double.parseDouble(graphParams[6].getText());
        jGraphPanel.yLabelDelta = Double.parseDouble(graphParams[7].getText());
        jGraphPanel.pollPeriod = Integer.parseInt(graphParams[8].getText());
	if (xmlButton.isSelected())
	    dataOutputFormat = "XML";
	else if (csvButton.isSelected())
	    dataOutputFormat = "CSV";
	else
	    dataOutputFormat = "???";
        this.setVisible(false);
      } //end of buttonOK_action

//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonCancel
       *JDialogGraphParameters cancel button action performed
       *@param e not used
       */
      void buttonCancel_action(ActionEvent e) {
        this.setVisible(false);
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
      } //end of buttonCancel_action

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param e WindowEvent: TBD
       */
      protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
          this.dispose();
        }
        super.processWindowEvent(e);
      } //end of processWindowEvent
   } //end of class JDialogGraphParameters
} //end of class JPanelTemperature





