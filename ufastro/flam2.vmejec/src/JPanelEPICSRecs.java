package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.lang.Integer;
import java.io.* ;
import java.util.* ;
import java.net.*;
import java.text.* ;

public class JPanelEPICSRecs extends JPanel {

    Vector listdata;
    JList recList;
    JLabel getLabel;
    JTextField manualRecnameField;
    JCheckBox manualOverride;
    JTextField selectedRecField;
    JTextField putTextField;

    JButton putButton;
    JButton getButton;
    JButton monButton;

    JTextField filterTextField;
    JCheckBox filterCase;
    JButton filterApplyButton;

    JButton getRecNamesButton;

    JButton breakOffButton;
    JButton saveMonListButton;
    JButton loadMonListButton;

    JPanelMonitor monitorPanel;
    
    public JPanelEPICSRecs(String filename) {
	recList = new JList();
	listdata = new Vector();
	putButton = new JButton("PUT");
	getButton = new JButton("GET");
	monButton = new JButton("-->");
	monitorPanel = new JPanelMonitor(new JPanel());
	putTextField = new JTextField("",5);
	manualRecnameField = new JTextField("",25);
	manualRecnameField.setEnabled(false);
	manualOverride = new JCheckBox("Manual Override",false);
	getLabel = new JLabel("\t");

	filterTextField = new JTextField("",10);
	filterApplyButton = new JButton("Apply Filter");
	filterCase = new JCheckBox("Case Sensitive",true);

	breakOffButton = new JButton("Break off");
	saveMonListButton = new JButton("Save Monitor List");
	loadMonListButton = new JButton("Load Monitor List");

	getRecNamesButton = new JButton("Get Rec Names from DB");

	getRecNamesButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    readRecShell();
		}
	    });
	monButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    addMonitor();
		}
	    });
	putButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    if (putTextField.getText().length() == 0) return;
		    if (manualOverride.isSelected()) {
			EPICS.put(manualRecnameField.getText(),putTextField.getText());
		    } else { 
			if (recList.getSelectedValue() == null) return;
			EPICS.put((String)recList.getSelectedValue(),putTextField.getText());
		    }
		}
	    });
	getButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    if (manualOverride.isSelected()) {
			String s = (String)manualRecnameField.getText();
			getLabel.setText(s+": "+EPICS.get(s));
		    } else {
			String s = (String)recList.getSelectedValue();
			if (s == null) return;
			getLabel.setText(s+": "+EPICS.get(s));
		    }
		}
	    });
	filterApplyButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    String s = filterTextField.getText().trim();
		    if (s == "") return;
		    Vector vec = new Vector();
		    for (int i=0; i<listdata.size(); i++) {
			if (!filterCase.isSelected()) {
			    if ( ((String)listdata.elementAt(i)).toLowerCase().indexOf(s.toLowerCase()) != -1)
				vec.add(listdata.elementAt(i));
			} else 
			    if ( ((String)listdata.elementAt(i)).indexOf(s) != -1 ) {
				vec.add(listdata.elementAt(i));
			    }
		    }
		    recList.setListData(vec);
		}
	    });
	recList.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent lse) {
		    manualRecnameField.setText((String)recList.getSelectedValue());
		    
		}
	    });
	manualOverride.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    recList.setEnabled(!manualOverride.isSelected());
		    manualRecnameField.setEnabled(manualOverride.isSelected());
		    manualRecnameField.setText((String)recList.getSelectedValue());
		}
	    });
	breakOffButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    JFrame newFrame = new JFrame();
		    newFrame.setSize(400,400);
		    newFrame.getContentPane().add(new JPanelMonitor(monitorPanel.getMonitorPanel()));
		    newFrame.setVisible(true);
		}
	    });
	//readRecFileFromISD(filename);
	readRecShell();
	JPanel botPanel = new JPanel();
	JPanel searchPanel = new JPanel();
	JPanel selectPanel = new JPanel();
	searchPanel.add(new JLabel("Filter:"));
	searchPanel.add(filterTextField);
	searchPanel.add(filterCase);
	searchPanel.add(filterApplyButton);
	selectPanel.setBorder(new EtchedBorder(0));
	selectPanel.setLayout(new GridLayout(0,1));
	JPanel subP1 = new JPanel();
	JPanel subP2 = new JPanel();
	JPanel subP3 = new JPanel();
	JPanel subP4 = new JPanel();
	subP4.setLayout(new GridLayout(1,0));
	subP1.add(new JLabel("Selected Rec:"));
	subP1.add(manualRecnameField);
        subP1.add(manualOverride);
	subP2.add(putButton);
	subP2.add(putTextField);
	subP3.add(getButton);
	subP3.add(getLabel);
	subP4.add(subP2);subP4.add(subP3);
	selectPanel.add(subP1);
	selectPanel.add(subP4);
	//botPanel.add(putTextField);
	//botPanel.add(putButton);
	//botPanel.add(getButton);
	//botPanel.add(getLabel);
	this.setLayout(new RatioLayout());
	this.add("0.01,0.01;0.40,0.06",new JLabel("EPICS Rec Names"));
	this.add("0.01,0.07;0.40,0.50",new JScrollPane(recList));
	this.add("0.48,0.25;0.07,0.08",monButton);
	//this.add("0.59,0.01;0.15,0.06",saveMonListButton);
	//this.add("0.75,0.01;0.15,0.06",loadMonListButton);
	this.add("0.90,0.01;0.10,0.06",breakOffButton);
	this.add("0.59,0.07;0.40,0.50",monitorPanel);	
	this.add("0.01,0.58;0.40,0.08",searchPanel);
	this.add("0.01,0.66;0.60,0.20",selectPanel);
    }
    
    public void addMonitor() {
	Object [] recnames = recList.getSelectedValues();
	for (int i=0;  i<recnames.length; i++) {
	    String recname = (String)recnames[i];
	    monitorPanel.addMonitor(recname);
	}
	monitorPanel.revalidate();
    }

    public void readRecShell() {
	try {
	    
	    //Process proc = Runtime.getRuntime().exec("uf"+EPICS.prefix.substring(0,EPICS.prefix.length()-1)+"isd -l -vv");
	    Process proc = Runtime.getRuntime().exec(fjec.dbPath+" -l -vv");
	    BufferedInputStream bis = 
		new BufferedInputStream(proc.getInputStream());
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    boolean keepGoing = true;
	    while (keepGoing) {
		byte [] b = new byte[bis.available()];
		bis.read(b,0,b.length);
		baos.write(b,0,b.length);

		//have to figure out if the process is still going
		//proc.exitValue will throw an exception if the 
		//process is still running, will return an integer
		//value otherwise.
		try { proc.exitValue(); keepGoing = false; }
		catch (Exception e) {}
	    }
	    BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray())));
	    readRecs(br);
	} catch (IOException ioe) {
	    System.err.println("JPanelEPICSRecs::readRecShell> "+
			       ioe.toString());
	    return;
	}
    }

    public void readRecs(BufferedReader br) {
	try {
	    listdata = new Vector();
	    while (br.ready()) {
		String sss = br.readLine();
		if (sss.indexOf(">") == -1) continue;
		sss = sss.substring(sss.indexOf(">"));
		sss = sss.substring(sss.indexOf(":")).trim();
		StringTokenizer st = new StringTokenizer(sss," ,");
		String lastToke = "";
		
		while (st.hasMoreTokens()) {
		    String toke = st.nextToken().trim();
		    if (toke.equals(lastToke)) continue;
		    if (toke.startsWith("flam:")) {
			listdata.add(EPICS.prefix+toke.substring(5));
		    } else if (toke.startsWith("foo:")) {
			listdata.add(EPICS.prefix+toke.substring(4));
		    }
		    lastToke = toke;
		}
	    }
	    recList.setListData(listdata);
	} catch (FileNotFoundException fnfe) {
	    System.err.println("JPanelEPICSRecs::readRecs> "+fnfe.toString());
	} catch (IOException ioe) {
	    System.err.println("JPanelEPICSRecs::readRecs> "+ioe.toString());
	}
    }

    public void readRecFileFromISD(String filename) {
	try {
	    BufferedReader br = new BufferedReader(new FileReader(filename));
	    readRecs(br);
	} catch (FileNotFoundException fnfe) {
	    System.err.println("JPanelEPICSRecs::readRecFileFromISD> "+fnfe.toString());
	}
    }

}
