package uffjec;

import javax.sound.sampled.*;
import java.io.*;

public class FJECSounds {

    public static boolean playSounds = true;

    public static void play(String filename) {
	try {
	    if (playSounds) {
		final Clip c = AudioSystem.getClip();
		c.open(AudioSystem.getAudioInputStream(new File(fjec.data_path+
								filename)));
		c.addLineListener(new LineListener(){
			public void update(LineEvent le) {
			    if (le.getType() == LineEvent.Type.STOP) c.close();
			}
		    });
		c.start();
	    }
	} catch (Exception e) {
	    System.err.println("FJECSounds.play> "+e.toString());
	}
    }

}
