package uffjec;

import ufjca.*;

public class EPICSCarCallback implements UFCAToolkit.MonitorListener{

    EPICSCarListener carListener = null;
    private String myName = null;


    public EPICSCarCallback(String carName,EPICSCarListener cl) {
	try {
	    EPICS.addMonitor(carName,this);
	    carListener = cl;
	    myName = carName;
	} catch (Exception ex) {
	    System.err.println("EPICSCarCallback> Problem setting up monitor:"
			       +ex.toString());
	}
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(myName,this);
	myName = dbname + myName.substring(myName.indexOf(":")+1);
	EPICS.addMonitor(myName,this);
    }

  

    public void monitorChanged(String value) {
	try {
	    if (value == null) { //now this really shouldn't happen. what-up, jca?
		System.err.println("EPICSCarCallback::monitorChanged> Null event object. Can ignore if just starting up.");
		return;
	    }
	    int x = Integer.parseInt(value.trim());
	    carListener.carTransition(x);
	} catch (Exception e) {
	    System.err.println("EPICSCarCallback::monitorChanged> "+
			       myName+" : "+e.toString());
	}
    }

}
