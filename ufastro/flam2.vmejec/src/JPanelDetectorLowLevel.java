package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class JPanelDetectorLowLevel extends JPanel {
    EPICSTextField [] preVals = new EPICSTextField[32];
    EPICSTextField [] biaVals = new EPICSTextField[4];
    EPICSComboBox cycleTypeBox;
    EPICSComboBox setIdleMode;
    EPICSComboBox integrationTime;
    EPICSLabel numPreResets;
    EPICSLabel numPostResets;
    EPICSComboBox numFramesOut;
    EPICSTextField pixelClockPeriod;
    

    public JPanelDetectorLowLevel() {
	JPanel prePanel = new JPanel();
	prePanel.setLayout(new RatioLayout());
	JPanel prePanel2 =new JPanel();
	prePanel2.setLayout(new RatioLayout());
	
 	for (int i=0; i<preVals.length/2; i++) {
 	    preVals[i] = new EPICSTextField(EPICS.prefix + "dc:setup.Preamp" + (i<9?"0"+(i+1):""+(i+1)),"",true);
 	    prePanel.add("0.01,"+(i/(preVals.length/2.0))+";0.49,"+(1.0/(preVals.length/2.0)),new JLabel("Preamp "+(i+1)));
 	    prePanel.add("0.51,"+(i/(preVals.length/2.0))+";0.49,"+(1.0/(preVals.length/2.0)),preVals[i]);
 	}
 	for (int i=preVals.length/2; i<preVals.length; i++) {
 	    preVals[i] = new EPICSTextField(EPICS.prefix + "dc:setup.Preamp" +(i<9?"0"+(i+1):""+(i+1)),"",true);
 	    prePanel2.add("0.01,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.49,"+(1.0/(preVals.length/2.0)),new JLabel("Preamp "+(i+1)));
 	    prePanel2.add("0.51,"+((i-preVals.length/2.0)/(preVals.length/2.0))+";0.49,"+(1.0/(preVals.length/2.0)),preVals[i]);
 	}
	

	JPanel biaPanel = new JPanel();
	biaPanel.setLayout(new RatioLayout());
	JPanel biaPanel2 = new JPanel();
	biaPanel2.setLayout(new RatioLayout());
// 	for (int i=0; i<biaVals.length/2; i++) {
// 	    biaVals[i] = new EPICSTextField(EPICS.prefix+"dc:setup.DACBias"+(i<10?"0"+i:""+i),"",true);
// 	    biaPanel.add("0.01,"+(i/(biaVals.length/2.0))+";0.49,"+(1.0/(biaVals.length/2.0)),new JLabel("Bias "+i));
// 	    biaPanel.add("0.51,"+(i/(biaVals.length/2.0))+";0.49,"+(1.0/(biaVals.length/2.0)),biaVals[i]);
// 	}
// 	for (int i=biaVals.length/2; i<biaVals.length; i++) {
// 	    biaVals[i] = new EPICSTextField(EPICS.prefix+"dc:setup.DACBias"+(i<10?"0"+i:""+i),"",true);
// 	    biaPanel2.add("0.01,"+((i-biaVals.length/2.0)/(biaVals.length/2.0))+";0.49,"+(1.0/(biaVals.length/2.0)),new JLabel("Bias "+i));
// 	    biaPanel2.add("0.51,"+((i-biaVals.length/2.0)/(biaVals.length/2.0))+";0.49,"+(1.0/(biaVals.length/2.0)),biaVals[i]);
// 	}
	biaVals[0] = new EPICSTextField(EPICS.prefix+"dc:setup.BiasGATE","",true);
	biaVals[1] = new EPICSTextField(EPICS.prefix+"dc:setup.BiasPWR","",true);
	biaVals[2] = new EPICSTextField(EPICS.prefix+"dc:setup.BiasVDD","",true);
	biaVals[3] = new EPICSTextField(EPICS.prefix+"dc:setup.BiasVRESET","",true);
	
	biaPanel.add("0.01,0.01;0.49,0.24",new JLabel("Bias GATE"));
	biaPanel.add("0.51,0.01;0.49,0.24",biaVals[0]);
	biaPanel.add("0.01,0.25;0.49,0.24",new JLabel("Bias PWR"));
	biaPanel.add("0.51,0.25;0.49,0.24",biaVals[1]);
	biaPanel.add("0.01,0.50;0.49,0.24",new JLabel("Bias VDD"));
	biaPanel.add("0.51,0.50;0.49,0.24",biaVals[2]);
	biaPanel.add("0.01,0.75;0.49,0.24",new JLabel("Bias VRESET"));
	biaPanel.add("0.51,0.75;0.49,0.24",biaVals[3]);

	pixelClockPeriod = new EPICSTextField(EPICS.prefix+"dc:setup.PixelBaseClock","");
	numPreResets = new EPICSLabel(EPICS.prefix+"sad:PRESETS","");
	numPostResets = new EPICSLabel(EPICS.prefix+"sad:POSTSETS","");
	String [] c = {"One Frame Out","Pair Frame Out"};
	numFramesOut =new EPICSComboBox(EPICS.prefix+"",c,EPICSComboBox.INDEX);

	JPanel resetPanel = new JPanel();
	resetPanel.setBorder(new EtchedBorder(0));
	resetPanel.setLayout(new GridLayout(0,1));
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	p1.add(new JLabel("Number of Pre Resets"));
	p1.add(numPreResets);
	p2.add(new JLabel("Number of Post Resets"));
	p2.add(numPostResets);
	resetPanel.add(p1);
	resetPanel.add(p2);
	JPanel framesOutPanel = new JPanel();
	framesOutPanel.setBorder(new EtchedBorder(0));
	framesOutPanel.add(new JLabel("Number of Frames Out"));
	framesOutPanel.add(numFramesOut);
	setLayout(new RatioLayout());
	prePanel.setBorder(new EtchedBorder(0));
	prePanel2.setBorder(new EtchedBorder(0));
	biaPanel.setBorder(new EtchedBorder(0));
	biaPanel2.setBorder(new EtchedBorder(0));
	JPanel pbcPanel = new JPanel();
	pbcPanel.setBorder(new EtchedBorder(0));
	pbcPanel.add(new JLabel("Pixel Clock Period (pbc)"));
	pbcPanel.add(pixelClockPeriod);

	String [] a = {"start","run","stop"};
	setIdleMode = new EPICSComboBox(EPICS.prefix+"",a,EPICSComboBox.ITEM);
	String [] b = {"ms timer (fast reading)","s timer (normal reading)"};
	integrationTime = new EPICSComboBox(EPICS.prefix+"",
					    b,EPICSComboBox.INDEX);
	
	JPanel idleModePanel = new JPanel();
	idleModePanel.setBorder(new EtchedBorder(0));
	idleModePanel.add(new JLabel("Idle Mode"));
	idleModePanel.add(setIdleMode);
	JPanel iTimePanel = new JPanel();
	iTimePanel.setBorder(new EtchedBorder(0));
	iTimePanel.add(new JLabel("Integration Time"));
	iTimePanel.add(integrationTime);

	/*
    -- Low Level Detector Panel
       -- # of pre resets
       -- # of post resets
       -- Pixel clock period (pbc)
	*/
	add("0.01,0.01;0.15,0.85",prePanel);
	add("0.16,0.01;0.15,0.85",prePanel2);
	add("0.33,0.01;0.20,0.30",biaPanel);
	//add("0.41,0.01;0.10,0.85",biaPanel2);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add("0.55,0.01;0.20,0.20",resetPanel);
	add("0.55,0.31;0.20,0.20",pbcPanel);
	add("0.79,0.01;0.20,0.20",idleModePanel);
	add("0.79,0.31;0.20,0.20",iTimePanel);
	add("0.55,0.61;0.20,0.20",framesOutPanel);
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("dc:"));
    
	numPreResets.setText("0");
	numPostResets.setText("0");
	pixelClockPeriod.setText("0           ");
    }
}
