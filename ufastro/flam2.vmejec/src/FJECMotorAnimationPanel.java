package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FJECMotorAnimationPanel extends JPanel {
    
    String [] posNames;
    int [] posSteps;
    int totalSteps;
    int currentStep;
    int oldMouseX = -1, oldMouseY = -1;

    public FJECMotorAnimationPanel(String [] posNames, int [] posSteps,
				   int totSteps) {
	this.posNames = posNames;
	this.posSteps = posSteps;
	totalSteps = totSteps;
	currentStep = 0;
	addMouseListener(new MouseListener() {
		public void mouseReleased(MouseEvent me) {

		}
		public void mousePressed(MouseEvent me) {}
		public void mouseExited(MouseEvent me) {}
		public void mouseEntered(MouseEvent me) {}
		public void mouseClicked(MouseEvent me) {}
	    });
	addMouseMotionListener(new MouseMotionListener() {
		public void mouseDragged(MouseEvent me) {
		    /*
		    boolean up = me.getY() < oldMouseY-10;
		    boolean left = me.getX() < oldMouseX-10;
		    boolean down = me.getY() > oldMouseY+10;
		    boolean right = me.getX() > oldMouseX+10;
		    if (oldMouseY != -1 && oldMouseX != -1) {
			if (currentStep <= totalSteps/4 ||
			    currentStep >= totalSteps*3/4){ 
			    if (left){ System.out.println("Left");}
			    else { System.out.println("Right");}
			}else if (currentStep <= totalSteps) {

			}
		    }
		    oldMouseY = me.getY();
		    oldMouseX = me.getX();
		    */
		}

		public void mouseMoved(MouseEvent me) {
		    
		}
	    });
    }

    public void setCurrentStep(int newStep) {
	currentStep = newStep;
	repaint();
    }

    public void paint(Graphics g) {
	super.paint(g);
	int sp = 3;
	g.drawOval(sp,sp,getWidth()-sp-1,getHeight()-sp-1);
	double radx = (getWidth()-sp-1)/2.0;
	double rady = (getHeight()-sp-1)/2.0;
	for (int i=0; i<posNames.length; i++) {
	    int x=(int)(Math.sin(Math.PI*2.0/totalSteps*posSteps[i])*radx+radx+sp);
	    int y=(int)((-Math.cos(Math.PI*2.0/totalSteps*posSteps[i]))*rady+rady+sp);
	    g.drawOval(x-2,y-2,4,4);
	}
	g.drawLine((int)(radx+sp),(int)(rady+sp),(int)(Math.sin(Math.PI*2.0/totalSteps*currentStep)*radx+radx+sp),(int)(-Math.cos(Math.PI*2.0/totalSteps*currentStep)*rady+rady+sp));
    }

    public static void main (String [] args) {
	JFrame j = new JFrame();
	String [] s = {"Pos1","Pos2","Pos3","Pos4","Pos5","Pos6"};
	int [] i = {0,100,200,300,400,500};
	FJECMotorAnimationPanel map = new FJECMotorAnimationPanel(s,i,600);
	j.getContentPane().add(map);
	j.setVisible(true);
	j.setDefaultCloseOperation(3);
	j.setSize(100,100);
	j.setLocation(100,100);
	for (int k=0; k<i.length; k++) {
	    map.setCurrentStep(i[k]);
	    map.repaint();
	    try { Thread.sleep(1000);}
	    catch (Exception e) { System.err.println(e.toString());}
	}
    }
}
