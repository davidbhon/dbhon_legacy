function BadFrame, frame, BADTHRES=BadThresh

	sz = size(frame)
	fs = frame[*,0:sz(2)-2]
	fs = fs - min( fs )

	dsum = fltarr( sz(2)-2 )
	if N_elements( BadThresh ) ne 1 then BadThresh = 1e4

	for c=0,15 do begin
		sc = total( fs[20*c:20*(c+1)<(sz[1]-1),*], 1 )
		dsum = dsum + abs( sc[1:*] - sc )
	  endfor

	if max( dsum ) gt BadThresh then return,1  else return,0
end
