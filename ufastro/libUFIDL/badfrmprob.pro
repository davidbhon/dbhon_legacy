function BadFrmProb, testname, MAXDSV=maxdsvec, DIRECTORY=dir, BADTHRES=BadThresh, SWAPBYTES=swapbytes

	if N_elements( dir ) ne 1 then dir = "/data/Test"
	if N_elements( testname ) ne 1 then testname = "chop_test"
	if N_elements( BadThresh ) ne 1 then BadThresh = 5e3

	files = findfile( dir + "/" + testname + "*" )

	Nbadf = 0
	Nframe = 0
	fh = bytarr(80,36)
	frame = lonarr( 320, 240 )
	maxdsvec = 0

	for n=0,N_elements( files )-1 do begin

		openr,Lun,files(n),/GET_LUN
		readu,Lun,fh
		print,files(n)

		while NOT EOF(Lun) do begin
			readu,Lun,frame
			if keyword_set( swapbytes ) then byteorder,frame,/LSWAP
			Nframe = Nframe+1
			maxds = CheckFrame( frame )
			if maxds ge BadThresh then Nbadf = Nbadf+1
			print,Nframe,maxds,Nbadf
			maxdsvec = [ maxdsvec, maxds ]
			if maxds ge BadThresh then stop
		  endwhile

		free_Lun,Lun
		BadProb = float( Nbadf )/Nframe
		help,Nframe,Nbadf,BadProb
	  endfor

return, BadProb
end
