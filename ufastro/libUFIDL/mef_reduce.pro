function MEF_reduce, MEF_file, hdmain,  DISPLAY=display, SEPERATE_NODS=sepnods, $
					NODBEAMS=nodbeams, NODSETS=nodsets, TVSHOW=tvshow

	fits_read, MEF_file, d, hdmain, EXTEN=0

	obsmode = sxpar( hdmain, "OBSMODE" )
	savesets = sxpar( hdmain, "SAVESETS" )
	ncoadds = sxpar(hdmain,"FRMCOADD") * ( sxpar(hdmain,"CHPCOADD") > 1 )

	if NOT keyword_set( nodsets ) then nodsets = sxpar( hdmain, "NODSETS" ) > 1
	if NOT keyword_set( nodbeams ) then nodbeams = sxpar( hdmain, "NNODS" ) > 1
	totNodBeams = nodbeams * nodsets

	help,obsmode,ncoadds,savesets,nodsets,nodbeams
	iex = 0

	for iset = 1, nodsets do begin
		for ibeam = 1, nodbeams do begin
			iex = iex+1
			fits_read, MEF_file, data, hd, EXTEN=iex
			nodbeam = strtrim( sxpar( hd, "NOD" ), 2 )
			print,"NodBeam=",nodbeam,iset,ibeam,iex
			help,data
			szd = size( data )
			nsav = szd[4]
			chopbeams = szd[3]
			dsum = total( data, 4 )/(nsav*ncoadds)

			if( chopbeams GT 1 ) then begin

				if( nodbeam eq "A" ) then $
					diff = dsum[*,*,0] - dsum[*,*,1] $
				  else	diff = dsum[*,*,1] - dsum[*,*,0]

			 endif else diff = dsum[*,*,0]

			if keyword_set( sepnods ) then begin
				if iex EQ 1 then chopdiffs = fltarr( szd[1], szd[2], nodbeams, nodsets )
				chopdiffs[*,*,ibeam,iset] = diff
			 endif else begin
			 	if iex EQ 1 then sig_image = fltarr( szd[1], szd[2] )
				sig_image = sig_image + diff
			  endelse

			if keyword_set( display ) then begin
				tvscl, hist_equal(diff)
				wait,display
			 endif else if keyword_set( tvshow ) then begin
				tvs, diff,/ERASE,/COL
				wait,tvshow
			  endif
		   endfor
	   endfor

	if keyword_set( sepnods ) then begin
		return,chopdiffs
	 endif else begin
		mkhdr, newhd, sig_image
		hdmain = [ newhd[0:5], hdmain[3:*] ]
		return, sig_image/totNodBeams
	  endelse
end
