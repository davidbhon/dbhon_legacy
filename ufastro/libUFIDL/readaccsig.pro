function readaccsig, filename, fits_header, fhead_struct, DIRECTORY=directory, VERBOSE=verbose

	if keyword_set( directory ) then begin
		sz = size( directory )
		if sz[sz[0]+1] ne 7 then directory = "/home/trecs/data"
		filename = pickfile( PATH=directory, FILTER="*.fits.accsig",/READ )
		if strlen( filename ) LT 9 then return,0
	   endif

	image = readfits( filename, fits_header )

	fhead_struct = trecsfitshead( filename, HEADER=fits_header )

	if keyword_set( verbose ) then help,/st,fhead_struct

return, image/float( fhead_struct.totcoadds )
end
