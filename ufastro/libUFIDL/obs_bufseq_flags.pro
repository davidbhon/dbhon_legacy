;+ 
; NAME: 
;  Obs_BufSeq_Flags 
; 
; PURPOSE:
;
; Compute buffer readout sequence and buffer instruction code:
; (an array of 16-bit flags) to control the frame preprocessor.
; 
; CALLING SEQUENCE: 
; 
;	bufseq_flags = Obs_BufSeq_Flags( ObsConf )
;
; INPUTS: 
;	ObsConf = structure with tags specifying:
;		ObsConf.ChopBeams
;		ObsConf.NodBeams
;		ObsConf.SaveSets
;		ObsConf.NodSets
; 
; KEYWORD PARAMETERS: 
;       None 
; 
; OUTPUTS: 
;       Returns array of 16-bit buffer sequence flags. 
; 
; MODIFICATION HISTORY: 
;        Written by:       Robert K. Pina, April, 1998.
;	Adapted by Frank Varosi Oct.2000, from cam_start_acq.pro.
;- 

function Obs_BufSeq_Flags, ObsConf
  
  totalframes  = ObsConf.NodBeams * ObsConf.ChopBeams * ObsConf.SaveSets * ObsConf.NodSets
  
  frameindex   = Lindgen(totalframes)
  framecount   = 1 + frameindex
  nodbeamindex = (frameindex/(ObsConf.ChopBeams*ObsConf.SaveSets)) mod ObsConf.NodBeams
  savesetindex = 1 + ((frameindex/ObsConf.ChopBeams) mod ObsConf.SaveSets)
  chpbeamindex = frameindex mod ObsConf.ChopBeams
  
  buffer = ObsConf.ChopBeams*nodbeamindex + (frameindex mod ObsConf.ChopBeams)
  
  bval = 2^indgen(16)  ; contains decimal values of each control bit

  clear = replicate(0,totalframes)
  index = where(savesetindex eq 1, count)
  if count ne 0 then $
    clear[index] = bval[2]
  
  d1   = replicate(0,totalframes)

  if (ObsConf.ChopBeams eq 2) then begin
    index = where((nodbeamindex eq 0) and ((framecount mod 2) eq 0), count)
    if count ne 0 then $
      d1[index] = bval[3]
    endif
  
  d2   = replicate(0,totalframes)

  if (ObsConf.ChopBeams eq 2) then begin
    index = where((nodbeamindex eq 1) and ((framecount mod 2) eq 0), count)
    if count ne 0 then $
      d2[index] = bval[4]
    endif
  
  d1a  = replicate(0,totalframes)

  if (ObsConf.ChopBeams eq 2) then begin
    index = where(d1 ne 0, count)
    if count ne 0 then $
      d1a[index] = bval[5]
    endif $
  else if (ObsConf.NodBeams eq 2) then begin
    index = where((framecount mod (2*ObsConf.SaveSets)) eq 0, count)
    if count ne 0 then $
      d1a[index] = bval[5]
    endif
  
  d2a  = replicate(0,totalframes)

  if (ObsConf.ChopBeams eq 2) then begin
    index = where(d2 ne 0, count)
    if count ne 0 then $
      d2a[index] = bval[6]
    endif
  
  sg  = replicate(0,totalframes)

  if (ObsConf.NodBeams eq 2) then begin
    index = where((framecount mod (ObsConf.NodBeams*ObsConf.ChopBeams*ObsConf.SaveSets)) eq 0, count)
    if count ne 0 then $
      sg[index] = bval[7]
    endif
  
  nodbeamindex = bval[8]*nodbeamindex
  chpbeamindex = bval[9]*chpbeamindex
  nodtoggle = replicate(0,totalframes)

  if (ObsConf.NodBeams eq 2) then begin
    index = where( (framecount mod (ObsConf.ChopBeams*ObsConf.SaveSets)) eq 0, count)
    if count ne 0 then $
      nodtoggle[index] = bval[10]
    endif
  
return, uint( buffer + clear + d1 + d2 + d1a + d2a + sg + nodbeamindex + chpbeamindex + nodtoggle )
end
