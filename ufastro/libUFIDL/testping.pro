;----Procedures to test ufIDL.c DLM with ufpingserv:
;----First connect to ufpingserv with IDL> testping, host  (see code at EOF)

;----Test UFsend/recv_Ints with ufpingserv ------------

pro tpsrfrm, fs, fr, fhdr, NPIXELS=npx

	szs = size( fs )

	if (szs(0) ne 2) or (szs(szs(0)+1) ne 3) then begin
		if N_elements( npx ) ne 1 then npx=128
		fs= round( dist(npx) )
	   endif

	szs = size( fs )
	if szs(szs(0)+1) ne 3 then fs = Long( fs )

	szs = size( fs )
	szr = size( fr )

	if max( abs( szs - szr ) ) gt 0 then fr = fs
	fr(*) = 0

	print," Nsent =", UFsend_Ints( fs, 'test', "status" )

	print," Nrecv =", UFrecv_Ints( fr, fhdr, "status" )

	print," Ndiff =", round( total( fs ne fr ) ), $
		N_elements(fs) - N_elements(fr)
end

;----Test UFsend_FrameConf and UFrecv_FrameConf with ufpingserv ------------

pro tpsr_fc, fc_send, fc_recv

	print," Nsent =", UFsend_FrameConf( fc_send, "status" )

	fc_recv = UFrecv_FrameConf( "status" )
end

pro tpclose
	print, UFagentclose("status")
end


;----Test UFsend_ObsConf and UFrecv_ObsConf with ufpingserv ------------

pro tpsr_oc, oc_send, oc_recv, SEND_OBSFLAGS=ofs, RECV_OBSFLAGS=ofr

	Nflags = oc_send.nodbeams * oc_send.chopbeams $
               * oc_send.savesets * oc_send.nodsets
	if N_elements( ofs ) ne Nflags then ofs = uintarr( Nflags )

	print," Nsent =", UFsend_ObsConf( oc_send, ofs, "status" )

	oc_recv = UFrecv_ObsConf( ofr, "status" )

	print," Ndiff =", fix( total( ofs ne ofr ) )
end

pro tpclose, status

	status = UFagentclose("status")
	print, status
end

;----Test UFagentconnect to ufpingserv, and test UFagentsend/recv ---------

pro testping, fd_ping, phdr, HOST=host

	if N_elements( host ) ne 1 then host = "doradus"

	fd_ping = UFagentconnect( "status", host, 55555 )
	help, fd_ping

	print,"Nbytes sent =", $
	   UFagentSend(["These strings are echoed by:","ufpingserv"], 'test', "status")

	print, UFagentRecv( phdr, "status" )," @time=",string(phdr.timestamp)
	print, phdr.elem," strings"
end
