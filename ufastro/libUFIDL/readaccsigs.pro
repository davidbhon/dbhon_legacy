function readaccsigs, fits_headers, fhead_structs, DIRECTORY=direct, DISPLAY=display, TVSHOW=tvshow

	fils = findfile( direct+"/*.fits.accsig", COUNT=nf )
	help,fils
	image = readaccsig( fils[0], fhd, fhst )
	help,fhd
	fits_headers = strarr( N_elements(fhd), nf )
	fhead_structs = replicate( fhst, nf )
	sz = size( image )
	data = fltarr( sz[1], sz[2], nf )

	for i=0,nf-1 do begin

		print," "
		print,fils[i]
		image = readaccsig( fils[i], fhd, fhst )
		fits_headers[*,i] = fhd
		fhead_structs[i] = fhst
		data[*,*,i] = image

		if keyword_set( display ) then begin
			erase
			tvscl,image
			empty
			wait,display
		 endif else if keyword_set( tvshow ) then begin
			tvs,image,/ERASE,/COL
			empty
			wait,tvshow
		  endif
	  endfor

return, data
end
