#!/usr/bin/perl -wT

use CGI;

my $q = new CGI;


$refresh = $q->param("REFRESH");
$maxtemp = $q->param("MAXTEMP");# || print "No Maxtemp val\n";
$mintemp = $q->param("MINTEMP");# || print "No Mintemp val\n";
$maxtime = $q->param("MAXTIME");# || print "No Maxtime val\n";
$mintime = $q->param("MINTIME");# || print "No Mintime val\n";
$autoscale = $q->param("AUTOSCALE");
$checkboxes[0] = $q->param("CAMVAC");
$checkboxes[1] = $q->param("MOSVAC");
$checkboxes[2]  = $q->param("MOSCOLD1");
$checkboxes[3]  = $q->param("MOSBENC2");
$checkboxes[4]  = $q->param("MOSBENC3");
$checkboxes[5]  = $q->param("MOSBENC4");
$checkboxes[6]  = $q->param("CAMCOLD5");
$checkboxes[7]  = $q->param("CAMBENC6");
$checkboxes[8]  = $q->param("CAMBENC7");
$checkboxes[9]  = $q->param("CAMBENC8");
$checkboxes[10]  = $q->param("CAMBENCA");
$checkboxes[11]  = $q->param("CAMDETCB");

if ($refresh < 1) {
    $refresh = -1;
}

print $q->header(-refresh=>"$refresh; URL=genpage.cgi?$ENV{QUERY_STRING}", -type => "text/html");

print '<HTML>';
print "\n";
print '<HEAD></HEAD>';
print "\n";
print "\n";

print "\n";
print '<table border="1">';
print "\n";
print "<tr><td>";
print '<IMG SRC="graphview.cgi';
#print "?MAXTEMP=$maxtemp&MINTEMP=$mintemp&MAXTIME=$maxtime&MINTIME=$mintime\">";
print "?$ENV{QUERY_STRING}\">";
print "\n";
print "</td>\n";
print '<td src="gettail.cgi">';
print "\n";
print "</td></tr></table>\n";
print '<FORM ACTION="genpage.cgi">';
print "\n";
print "MAX TEMP: <input TYPE=\"text\" VALUE=\"$maxtemp\" NAME=\"MAXTEMP\" SIZE=\"5\" MAXLENGTH=\"5\">";
print "\n";
print "MIN TEMP: <input TYPE=\"text\" VALUE=\"$mintemp\" NAME=\"MINTEMP\" SIZE=\"5\" MAXLENGTH=\"5\">";
print "\n";
print "<P>";
print "MAX TIME: <input TYPE=\"text\" VALUE=\"$maxtime\" NAME=\"MAXTIME\" SIZE=\"5\" MAXLENGTH=\"5\">";
print "\n";
print "MIN TIME: <input TYPE=\"text\" VALUE=\"$mintime\" NAME=\"MINTIME\" SIZE=\"5\" MAXLENGTH=\"5\">";
print "\n";
print "<P>\n";

print "<input TYPE=CHECKBOX NAME=\"CAMVAC\""; if(lc($q->param("CAMVAC")) eq "on") { print "CHECKED";} print " >CAMVAC";
print "<input TYPE=CHECKBOX NAME=\"MOSVAC\""; if(lc($q->param("MOSVAC")) eq "on") { print "CHECKED";} print " >MOSVAC";

print "<br>\n";
print "<input TYPE=CHECKBOX NAME=\"MOSCOLD1\""; if(lc($q->param("MOSCOLD1")) eq "on") { print "CHECKED";} print " >MOSCOLD1";
print "<input TYPE=CHECKBOX NAME=\"MOSBENC2\""; if(lc($q->param("MOSBENC2")) eq "on") { print "CHECKED";} print " >MOSBENC2";
print "<input TYPE=CHECKBOX NAME=\"MOSBENC3\""; if(lc($q->param("MOSBENC3")) eq "on") { print "CHECKED";} print " >MOSBENC3";
print "<input TYPE=CHECKBOX NAME=\"MOSBENC4\""; if(lc($q->param("MOSBENC4")) eq "on") { print "CHECKED";} print " >MOSBENC4";
print "<br>\n";
print "<input TYPE=CHECKBOX NAME=\"CAMCOLD5\""; if(lc($q->param("CAMCOLD5")) eq "on") { print "CHECKED";} print " >CAMCOLD5";
print "<input TYPE=CHECKBOX NAME=\"CAMBENC6\""; if(lc($q->param("CAMBENC6")) eq "on") { print "CHECKED";} print " >CAMBENC6";
print "<input TYPE=CHECKBOX NAME=\"CAMBENC7\""; if(lc($q->param("CAMBENC7")) eq "on") { print "CHECKED";} print " >CAMBENC7";
print "<input TYPE=CHECKBOX NAME=\"CAMBENC8\""; if(lc($q->param("CAMBENC8")) eq "on") { print "CHECKED";} print " >CAMBENC8";
print "<br>\n";
print "<input TYPE=CHECKBOX NAME=\"CAMBENCA\""; if(lc($q->param("CAMBENCA")) eq "on") { print "CHECKED";} print ">CAMBENCA";
print "<input TYPE=CHECKBOX NAME=\"CAMDETCB\""; if(lc($q->param("CAMDETCB")) eq "on") { print "CHECKED";} print ">CAMDETCB";

print "<br><br>\n";
print "<input TYPE=CHECKBOX NAME=\"AUTOSCALE\""; if(lc($q->param("AUTOSCALE")) eq "on") { print "CHECKED";} print " >AUTOSCALE";

print "__________Refresh: <input TYPE=\"text\" VALUE=\"$refresh\" NAME=\"REFRESH\" SIZE=\"5\" MAXLENGTH=\"5\">";
print "\n";
print "<P>(Enter a refresh value of -1 for no refresh (or to stop refreshing))"; 
print "<br>(Autoscale will override min/max temperature values)";
print "<P>";
print '<input TYPE=SUBMIT VALUE="submit">';
print "\n";
print '</FORM>';
print "\n";
print "\n";
print '</HTML>';
print "\n";

exit;
 
