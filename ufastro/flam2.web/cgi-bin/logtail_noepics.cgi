#!/usr/bin/perl -wT

use CGI;

BEGIN {
    $ENV{PATH} = '/bin:/usr/bin:/usr/local/bin';
    delete @ENV{ qw( IFS CDPATH ENV BASH_ENV ) };
}


my $q = new CGI;

print $q->header( -Refresh=>'10; URL=http://www.astro.ufl.edu/~drashkin/cgi-bin/logtail_noepics.cgi', -type=>"text/html");

my $ootput = `tail -n 100 /astro/data/doradus0/data/environment/current_noepics`;

$ootput =~ s/\n/<br>/g;

print "$ootput\n";

exit;
