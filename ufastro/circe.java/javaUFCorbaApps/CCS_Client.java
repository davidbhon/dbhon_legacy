import CCS.*;
import DAF.*;
import CCS.CanaricamPackage.*;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;

public class CCS_Client {

    static Canaricam canaricamImpl;

    public static void main (String args[] ) {
	try {
	    String host = "newton";
	    int port = 12344;
	    if (args.length == 0) {
		args = new String[2];
		args[0] = "-ORBInitRef";
		args[1] = "NameService=corbaloc:iiop:"+host+":"+port+
		    "/NameService";
	    } else {
		if (CMD.findArg(args,"-host")) {
		    host = CMD.getArg(args,"-host");
		    args = CMD.stripArg(args,"-host",true);
		}
		if (CMD.findArg(args,"-port")) {
		    port = Integer.parseInt(CMD.getArg(args,"-port"));
		    args = CMD.stripArg(args,"-port",true);
		}
		if (CMD.findArg(args,"-ORBInitRef","NameService")) {
		    args = CMD.stripArg(args,"-ORBInitRef","NameService");
		}
		args = CMD.addArg(args,"-ORBInitRef","NameService=corbaloc:"+
				  "iiop:"+host+":"+port+"/NameService");
	    }
	    // create and initialize the ORB
	    ORB orb = ORB.init(args,null);

	    // get the root naming context
	    org.omg.CORBA.Object objRef = 
		orb.resolve_initial_references("NameService");
	    // User NamingContextExt instead of NamingContext. This is
	    // part of the Interoperable naming Service.
	    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

	    // resolve the Object Refernce in Naming
	    String name = "Canaricam.CCS";
	    canaricamImpl = CanaricamHelper.narrow(ncRef.resolve_str(name));
	    
	    System.out.println(canaricamImpl.name());
	    try {
		canaricamImpl.beamSwitchDone();
		ObsConfig obsc = canaricamImpl.getObsConfig();
		System.out.println("ObsConfig.DataLabel: "+obsc.dataLabel);
		FrameConfig fc = canaricamImpl.getFrameConfig();
		System.out.println("FrameConfig.CoAdds: "+fc.coadds);
	    } catch (GCSException gcse) {
		System.err.println(gcse.toString());
	    } 
	    //countImpl.shutdown();
	} catch (Exception e) {
	    System.out.println("Uh-oh, spaghetti-o's: "+ e.toString());
	    e.printStackTrace(System.out);
	}
    }

}
