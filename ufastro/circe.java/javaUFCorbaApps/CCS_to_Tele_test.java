import javaUFProtocol.*;
import java.net.*;
import java.io.*;

public class CCS_to_Tele_test {

    public static void main(String [] args) {
	try {
	    String host = "localhost";
	    int port = 53000;
	    Socket s = new Socket(host,port);
	    while (true) {
		UFTimeStamp ufs = new UFTimeStamp("requestBeamSwitch");
		ufs.sendTo(s);
		UFStrings ufss = (UFStrings) UFProtocol.createFrom(s);
		System.out.println(ufss);
		//s = new Socket(host,port);
		ufs = new UFTimeStamp("getCurrentRotatorAngle");
		ufs.sendTo(s);
		ufss = (UFStrings) UFProtocol.createFrom(s);
		System.out.println(ufss);
		Thread.sleep(5000);
	    }
	    //s = new Socket(host,port);
	    //ufs = new UFTimeStamp("getCurrentNonexistantFunction");
	    //ufs.sendTo(s);
	    //ufss = (UFStrings) UFProtocol.createFrom(s);
	    //System.out.println(ufss);
	} catch (Exception e) {
	    System.err.println(e.toString());
	}
    }
}
