/*
 * ColorBar.java
 *
 * Created on May 4, 2004, 3:14 PM
 *
 * @author  ziad
 */

package ufjdd;

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.util.*;
import java.awt.event.*;

public class ColorBar extends JPanel {
    
    byte colorBar[];
    int width = 30;
    IndexColorModel colorModel;
    Image img;
    
    public ColorBar(IndexColorModel colorModel, int width) {
        
        this.colorModel = colorModel;
        this.width = width;
        int index = 512*width;
        colorBar = new byte[512*width];

        for(int i=0; i < 256; i++) {
            for (int j=0; j < width; j++) {
                colorBar[--index] = (byte)i;
                colorBar[--index] = (byte)i;
            }
        }
        img = createImage(new MemoryImageSource(width, 512, colorModel, colorBar, 0, width));
    }
    
    public void paintComponent( Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g.drawImage(img, 0, 0, this);
    }
    
    public void updateColorMap(IndexColorModel colorModel) {
        img = createImage(new MemoryImageSource(width, 512, colorModel, colorBar, 0, width));
        repaint();
    }
}
