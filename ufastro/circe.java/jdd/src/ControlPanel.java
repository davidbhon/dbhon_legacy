package ufjdd;

/**
 * Title:        Java Data Display (JDD): ControlPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For controlling the single image display of contents of a frame buffer.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ControlPanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ControlPanel.java,v 1.12 2004/10/04 22:31:21 varosi Exp $";
    
    String regBufferNames[] ;
    int index = 0;
    ImageBuffer imgBuffer;
    ImageDisplayPanel imgdp;
    FrameBuffer frmBuffer;
    JRadioButton radioButtonSingle = new JRadioButton("Single");
    JRadioButton radioButtonAccum = new JRadioButton("Accum");
    ButtonGroup radioButtons = new ButtonGroup();
    JComboBox bufferSelector;
    AdjustPanel adjPanel;
    
    public ControlPanel( ImageDisplayPanel idp,  AdjustPanel adjPanel, FrameBuffer frmBuffer,  String[] bufferNames ) {
        
        super(new GridLayout(1,4));
        this.imgdp = idp;
        this.frmBuffer = frmBuffer;
        this.adjPanel = adjPanel;
        
        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(320, 50));
	setBorder(BorderFactory.createTitledBorder(""));

        int numBufs = bufferNames.length;
        regBufferNames = new String[numBufs/2];
        
        for( int j= ((numBufs/2)); j < numBufs; j++) {
            regBufferNames[index++] = bufferNames[j];
        }

        bufferSelector = new JComboBox( regBufferNames );

        add(new JLabel("Data Buffer:"));
        add(bufferSelector);
        add(radioButtonSingle);
        add(radioButtonAccum);
  
        radioButtons.add(radioButtonSingle);
        radioButtons.add(radioButtonAccum);        
        
        ButtonModel bmod = radioButtonSingle.getModel();
        radioButtons.setSelected(bmod, true);

        // Register a listener for the radio buttons
        RadioListener myRadioListener = new RadioListener();
        radioButtonSingle.addActionListener(myRadioListener);
        radioButtonAccum.addActionListener(myRadioListener);
        
        bufferSelector.addActionListener(myRadioListener);        
    }
    
    // This method returns the selected radio button in a button group

    public static JRadioButton getSelection(ButtonGroup group) {
        for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }
    
    public void displayBuffer( String buffname, boolean accum )
    {
	if( accum )
	    radioButtons.setSelected( radioButtonAccum.getModel(), true);
	else
	    radioButtons.setSelected( radioButtonSingle.getModel(), true);

	bufferSelector.setSelectedItem( buffname );
	readFrameBuffer();
    }
    
    private void readFrameBuffer()
    {
        String radioSel = (String)((JRadioButton)getSelection(radioButtons)).getText();
        String selBufname = (String)bufferSelector.getSelectedItem();
        
        if( radioSel.equalsIgnoreCase("Accum") ) {
            imgdp.frameName = "Accum(" + selBufname + ")";
            imgBuffer = frmBuffer.readFrameBuffer( imgdp.frameName );
        }
        else {            
            imgdp.frameName = selBufname;
            imgBuffer = frmBuffer.readFrameBuffer( selBufname );
        }

	if( imgBuffer != null ) {
	    imgdp.createImage(imgBuffer);
	    adjPanel.updateMinMaxVal(imgBuffer.min, imgBuffer.max, imgBuffer.scaleFactor);
	    adjPanel.reset();
	}
    }
    
    class RadioListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {                        
            readFrameBuffer();
        }
    }
}


