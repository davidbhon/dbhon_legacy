package ufjdd;

import java.io.*;
import java.util.*;
import java.net.*;
import javaUFProtocol.*;

public class FrameBuffer {
    String bufferNames[];
    Socket socket = null;
    
    public FrameBuffer(Socket sock) {
        this.socket = sock;
    }
    
    public String[] readBufferNames( ) {
        UFProtocol ufp = null;
        UFStrings   ufs = null;
        int numOfFrames ;
        
        // Send the agent a timestamp with the command bn to read "Buffer Names"
        UFTimeStamp uft = new UFTimeStamp("bn");
        
        if(uft.sendTo(socket) <= 0) {
            System.out.println("Connect> UFTimeStamp Read ERROR");
        }
        
        if( (ufs = (UFStrings)UFProtocol.createFrom(socket)) == null) {
            System.out.println("Connect> UFStrings Read ERROR");
        }
        
        numOfFrames = ufs.numVals();
        bufferNames = new String[numOfFrames];
        
        for( int k=0; k < numOfFrames ; k++) {
            bufferNames[k] = ufs.valData(k);
        }
        
        return bufferNames;
    }
    
    public synchronized ImageBuffer readFrameBuffer( String buffName ) {
        UFProtocol ufp = null;
        UFFrameConfig  uff = null;
        UFInts ufi = null;
        ImageBuffer imgBuffer = null;
        
        // Send the agent a timestamp with the command bn for "Buffer Names"
        UFTimeStamp uft = new UFTimeStamp(buffName);
        
        if(uft.sendTo(socket) <= 0) {
            System.out.println("Connect> UFTimeStamp Read ERROR");
            return imgBuffer;
        }
        
        //System.out.println("uft ->" + uft.name());
        
        try {
            if( (uff =(UFFrameConfig)UFProtocol.createFrom(socket)) == null ) {
                System.out.println("Connect> UFFrameConfig Read ERROR");
                return imgBuffer;
            }
        } catch(ClassCastException cce) {
            System.out.println("Class Cast Exception " + cce);
            return null;
        }
        
        //System.out.println("UFFrameConfig read > " + uff.name());
        
        if( (uff.name()).indexOf("ERROR") >= 0 ) {
            System.out.println("No data in Frame Buffer > " + buffName);
            return imgBuffer;
        }
	else {
            
            if( (ufi =(UFInts)UFProtocol.createFrom(socket)) == null ) {
                System.out.println("ufi > UFInts Read ERROR");
            }

            imgBuffer = new ImageBuffer(uff, ufi);
            return imgBuffer;
        }
    }
    
    public Vector readFrameBuffers( String[] nameList) {
        String bufferName;
        ImageBuffer imgBuffer;
        int listLength = nameList.length;
        Vector v = new Vector(listLength);
        
        for ( int i=0 ; i < listLength ; i++) {
            bufferName = nameList[i];
            imgBuffer = readFrameBuffer(bufferName);
            v.add(imgBuffer);
        }
        return v;
    }
}
