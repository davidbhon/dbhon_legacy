package ufjdd;

/**
 * Title:        Java Data Display (JDD): ImagePanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For single image display of contents of a frame buffer from Data Acq. Server
 */

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import javaUFLib.*;

public class ImagePanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ImagePanel.java,v 1.3 2004/10/04 19:39:03 varosi Exp $";

    FrameBuffer frameBuffer ;
    String [] bufferNames;
    public ControlPanel controlPanel;
    public ImageDisplayPanel imgDisplayPanel;
    public AdjustPanel adjPanel;

    public ImagePanel(int i, jddFrame jddfrm, FrameBuffer frmBuf, String[] bufferNames,
		      JComboBox zoomFactor, IndexColorModel colorModel, ZoomPanel zoomPanel )
    {
	this.frameBuffer = frmBuf;
	this.bufferNames = bufferNames;
	this.setLayout(new RatioLayout());
	imgDisplayPanel = new ImageDisplayPanel(i, jddfrm, null, zoomFactor, colorModel, zoomPanel);
        adjPanel = new AdjustPanel(imgDisplayPanel);
	controlPanel = new ControlPanel(imgDisplayPanel, adjPanel, frmBuf, bufferNames);	        
	this.add("0.0,0.0;1.0,0.1", controlPanel);
	this.add("0.0,0.1;1.0,0.7", imgDisplayPanel);
        this.add("0.0,0.8;1.0,0.2", adjPanel);
	this.setBorder(BorderFactory.createLineBorder(Color.green, 1));
    }
}
