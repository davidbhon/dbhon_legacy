/*
 * zoomAdjustPanel.java
 *
 * Created on May 10, 2004, 4:03 PM
 */

package ufjdd;

/**
 *
 * @author  ziad
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class AdjustZoomPanel extends javax.swing.JPanel {
    
    private ZoomPanel zoompanel;
    int scalefactor = 1;
    int min = 0;
    int max = 0;
    int temp_min, temp_max;
    JButton origMinButton;
    JButton origMaxButton;
    JLabel minValLabel;
    JLabel maxValLabel;
    JButton newMinButton;
    JButton newMaxButton;
    JRadioButton linear_mode_button, log_mode_button;
    JTextField newMinTextField;
    JTextField newMaxTextField;
    ButtonModel model;
    JTextField thresholdTextField ;
    ButtonGroup scale_mode_group;
    
    /** Creates a new instance of parameterPanel */
    public AdjustZoomPanel( ZoomPanel zoompanel) {
        super(new GridLayout(3,4));
        this.zoompanel = zoompanel;
        origMinButton = new JButton("Min *");
        origMinButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        origMaxButton = new JButton("Max *");
        origMaxButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        minValLabel = new JLabel("", JLabel.LEFT);
        minValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        maxValLabel = new JLabel("", JLabel.LEFT);
        maxValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        newMinTextField = new JTextField();
        newMaxTextField = new JTextField();
        minValLabel.setText("" + 0);
        maxValLabel.setText("" + 0);
        
        newMinButton = new JButton("N_Min");
        newMaxButton = new JButton("N_Max");
        
        thresholdTextField = new JTextField("1");
        thresholdTextField.setEditable(false);
        
        scale_mode_group = new ButtonGroup();
        
        linear_mode_button = new JRadioButton("Linear");
        scale_mode_group.add(linear_mode_button);
        
        log_mode_button = new JRadioButton("Log");
        scale_mode_group.add(log_mode_button);
        
        RadioListener radioListener = new RadioListener();
        linear_mode_button.addActionListener(radioListener);
        log_mode_button.addActionListener(radioListener);
        
        model = linear_mode_button.getModel();
        scale_mode_group.setSelected(model, true);
        
        origMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMinButton.setText("Min *");
                newMinButton.setText("N_Min");
                applySetting();
            }
        });
        
        origMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMaxButton.setText("Max *");
                newMaxButton.setText("N_Max");
                applySetting();
            }
        });
        
        newMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(newMinTextField.getText().trim().length() == 0)
                    // Do nothing
                    ;
                else {
                    origMinButton.setText("Min");
                    newMinButton.setText("N_Min *");
                    applySetting();
                }
            }
        });
        
        newMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(newMinTextField.getText().trim().length() == 0)
                    // Do nothing
                    ;
                else {
                    origMaxButton.setText("Max");
                    newMaxButton.setText("N_Max *");
                    applySetting();
                }
            }
        });
        
        newMinTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if(c == KeyEvent.VK_ENTER) {
                    origMinButton.setText("Min");
                    newMinButton.setText("N_Min *");
                    applySetting();
                }
            }
        });
        
        newMaxTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if(c == KeyEvent.VK_ENTER) {
                    origMaxButton.setText("Max");
                    newMaxButton.setText("N_Max *");
                    applySetting();
                }
            }
        });
        
        thresholdTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if(c == KeyEvent.VK_ENTER) {
                    applySetting();
                }
            }
        });
        
        this.add("0.0, 0.0; 0.25, 0.32", origMinButton);
        this.add("0.25, 0.0; 0.25, 0.32", minValLabel);
        this.add("0.5, 0.0; 0.25, 0.32", origMaxButton);
        this.add("0.75, 0.0; 0.25, 0.32", maxValLabel);
        
        this.add("0.0, 0.33; 0.25, 0.32", newMinButton);
        this.add("0.25, 0.33; 0.25, 0.32", newMinTextField);
        this.add("0.5, 0.33; 0.25, 0.32", newMaxButton);
        this.add("0.75, 0.33; 0.25, 0.32", newMaxTextField);
        
        this.add("0.0, 0.67;  0.20, 0.32", linear_mode_button);
        this.add("0.20, 0.67; 0.20, 0.32", log_mode_button);
        this.add("0.40, 0.67; 0.25, 0.32", new JLabel("Threshold"));
        this.add("0.65, 0.67; 0.35, 0.32", thresholdTextField);
        
        setBorder(BorderFactory.createTitledBorder(""));
    }
    
    
    public void updateMinMaxVal(int min, int max, int sf) {
        minValLabel.setText(" " + min/sf);
        maxValLabel.setText(" " + max/sf);
        min = min;
        max = max;
        scalefactor = sf;
    }
    
    private void applySetting() {
        
        String scaleItem = (String)((JRadioButton)getSelection(scale_mode_group)).getText();
        
        int temp_min =0, temp_max = 0;
        
        if(scaleItem.equalsIgnoreCase("linear")) {
            if(origMinButton.getText().equals("Min *") && origMaxButton.getText().equals("Max *") ) {
                zoompanel.drawOriginalImage();
            }
            else if(origMinButton.getText().equals("Min") && origMaxButton.getText().equals("Max *")) {
                if(newMinTextField.getText().trim().length() == 0) {
                    temp_min = Integer.parseInt(minValLabel.getText().trim());
                    origMinButton.setText("Min *");
                    newMinButton.setText("N_Min");
                }
                else
                    temp_min = Integer.parseInt(newMinTextField.getText().trim());
                
                temp_max = Integer.parseInt(maxValLabel.getText().trim());
                zoompanel.applyNewMinMax((temp_min)*scalefactor, (temp_max)*scalefactor);
            }
            else if(origMinButton.getText().equals("Min *") && origMaxButton.getText().equals("Max")) {
                if(newMaxTextField.getText().trim().length() == 0) {
                    temp_max = Integer.parseInt(maxValLabel.getText().trim());
                    origMaxButton.setText("Max *");
                    newMinButton.setText("N_Max");
                }
                else
                    temp_max = Integer.parseInt(newMaxTextField.getText().trim());
                
                temp_min = Integer.parseInt(minValLabel.getText().trim());
                zoompanel.applyNewMinMax((temp_min)*scalefactor, (temp_max)*scalefactor);
            }
            else if(origMinButton.getText().equals("Min") && origMaxButton.getText().equals("Max")) {
                
                if(newMinTextField.getText().trim().length() == 0) {
                    temp_min = Integer.parseInt(minValLabel.getText().trim());
                    origMinButton.setText("Min *");
                    newMinButton.setText("N_Min");
                }
                else
                    temp_min = Integer.parseInt(newMinTextField.getText().trim());
                
                if(newMaxTextField.getText().trim().length() == 0) {
                    temp_max = Integer.parseInt(maxValLabel.getText().trim());
                    origMaxButton.setText("Max *");
                    newMaxButton.setText("N_Max");
                }
                else
                    temp_max = Integer.parseInt(newMaxTextField.getText().trim());
                
                zoompanel.applyNewMinMax((temp_min)*scalefactor, (temp_max)*scalefactor);
            }
        }
        else {
            
            float thrld = Float.parseFloat(thresholdTextField.getText());
            zoompanel.applyLogScale(thrld);
        }
        
    }
    
    private void ApplyOriginalSetting(java.awt.event.MouseEvent evt) {
        zoompanel.drawOriginalImage();
    }
    
    // This method returns the selected radio button in a button group
    public static JRadioButton getSelection(ButtonGroup group) {
        for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }
    
    class RadioListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            
            String scaleItem = (String)((JRadioButton)getSelection(scale_mode_group)).getText();
            
            
            if(scaleItem.equalsIgnoreCase("linear")) {
                thresholdTextField.setEditable(false);
                newMinTextField.setEditable(true);
                newMaxTextField.setEditable(true);
                //thresholdTextField.setText("1");
            }
            else {
                thresholdTextField.setEditable(true);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
            }
            
            applySetting();
        }
    }
    
    public void reset() {
        
        newMinTextField.setText("");
        newMaxTextField.setText("");
        origMinButton.setText("Min *");
        origMaxButton.setText("Max *");
        newMinButton.setText("N_Min");
        newMaxButton.setText("N_Max");
        scale_mode_group.setSelected(model, true);
        thresholdTextField.setEditable(false);
        thresholdTextField.setText("1");
        
    }
}
