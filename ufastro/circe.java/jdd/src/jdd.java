package ufjdd;

/**
 * Title:        Java Data Display  (JDD)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh, Frank Varosi
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Infrared Camera data stream.
 */

public class jdd {
    
    public static final
	String rcsID = "$Name:  $ $Id: jdd.java,v 1.9 2004/09/10 22:46:12 varosi Exp $";

    private boolean packFrame = false;
    
    public jdd(String hostname, int hostport, String tcshost, String tcsport,  String args[])
    {
        jddFrame frame = new jddFrame(hostname, hostport, tcshost, tcsport, args);
        
        if (packFrame)
            frame.pack();
        else
            frame.validate();
        
        frame.setVisible(true);
    }
    
    public static void main(String[] args)
    {
        String hostname = "kepler";
        String tcshost = "trifid";
        String tcsport = "12344";
        
        int hostport = 52000;
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-host")) {
                hostname = args[++i];
            } else if(args[i].equals("-tcshost")) {
                tcshost = args[++i];
            } else if(args[i].equals("-tcsport")) {
                tcsport = args[++i];
            }
        }

        new jdd(hostname, hostport, tcshost, tcsport, args);
    }
}

