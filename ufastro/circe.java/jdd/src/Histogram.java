/*
 * Histogram.java
 *
 * Created on May 4, 2004, 4:35 PM
 */

package ufjdd;

/**
 *
 * @author  ziad
 */
import java.util.*;
import javax.swing.*;
import java.awt.*;

class Histogram extends javax.swing.JPanel {
    
    int  [] hist = new int[512];
    int  [] new_hist = new int[512];
    int  [] norm_hist = new int[256];
    final int WIDTH = 120;
    String mode = "initial";
    int zero_index = -1 ;
    int underFlow = 0;
    int overFlow  = 0;
    jddFrame jddFrm;
    
    /** Creates a new instance of Histogram */
    public Histogram(jddFrame frm) {
        
        this.setPreferredSize(new Dimension(120, 512));
        this.setBackground(Color.WHITE);
        this.jddFrm = frm;
    }
    
    
    public void paintComponent( Graphics g) {
        
        int x= 512;
        int y=0;
        int height;
        //Clear graphics
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        
        for(int i=0; i< hist.length; i++) {
            g2d.setPaint(Color.black);
            if((2*zero_index == i) || ((2*zero_index -1) == i) ) {
                g2d.setPaint(Color.red);
            }
            if(mode.equalsIgnoreCase("initial"))
                height =  hist[i];
            else
                height = new_hist[i];
            --x;
            g2d.drawRect(y, x, height, 1);
        }
    }
    
    public void updateHisto( int dat[][], int min, int max) {
        
        float range;
        float f;
        zero_index = -1;
        range = max - min;
        underFlow = overFlow = 0;
        //System.out.println("Min : " + min + " Max : " + max + "Range : " + range);
        for( int i=0; i< hist.length; i++ )
             hist[ i ] = 0;
        
        if( (min < 0) && ( max > 0))
            zero_index = (int) (255 * (float)Math.abs(min)/(float) range) ;
        
        for ( int i=0; i < dat.length; i++)
            for (int j=0; j < dat.length; j++) {
                
                f = (float)(dat[i][j] - min)/(float) range;
                
                if(f<0) {
                    underFlow++ ;
                }
                else if(f>1) {
                    overFlow++;
                }
                else {
                    ++hist[ 2*Math.round(f*255) ];
                }
            }
        
        jddFrm.underFlowLabel.setText("UnderFlow = " + underFlow);
        jddFrm.overFlowLabel.setText("OverFlow = " + overFlow);
        
        for( int i=0; i<256; i++ )
            hist[ 2*i + 1 ] = hist[2*i];
        
        float tempf;
        int hist_max = hist[0];
        
        for(int i=0; i<hist.length; i++) {
            if( hist[i] > hist_max )
                hist_max = hist[i];
        }
        
        for(int i=0; i<hist.length; i++) {
            tempf = (float)(hist[i])/(float)hist_max;
            hist[i] = Math.round(tempf*WIDTH);
        }
        repaint();
    }
    
    
    public int[] normalize() {
        
        float f;
        int temp = 0;
        int temp_max;
        int  [] orig_hist = new int[256];
        int  [] acc_hist = new int[256];
        
        mode = "normal";
        
        for(int i=0; i < orig_hist.length; i++)
            orig_hist[i] = hist[2*i];
        
        for(int i=0; i<orig_hist.length; i++) {
            temp += orig_hist[i];
            acc_hist[i] = temp;
        }
        temp_max = acc_hist[orig_hist.length-1];
        
        for(int i=0; i<orig_hist.length; i++) {
            f = (float)(acc_hist[i])/(float) temp_max;
            if(f<0) {
                norm_hist[i] = 0; 
            }
            else if(f>1) {
                norm_hist[i] = 255;
            }
            else
                norm_hist[i] = Math.round(f*255);
        }
        
        for( int i=0; i<256; i++ ) {
            new_hist[ 2*i ] = norm_hist[i];
            new_hist[ 2*i + 1 ] = norm_hist[i];
        }
        
        repaint();
        return norm_hist;
    }
    
    public void applyOldSettings() {
        
        mode = "initial";
        repaint();
    }
}

