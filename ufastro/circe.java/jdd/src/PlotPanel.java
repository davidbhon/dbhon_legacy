package ufjdd;

// "Java Tech"
//  Code provided with book for educational purposes only.
//  No warranty or guarantee implied.
//  This code freely available. No copyright claimed.
//  2003


import java.awt.*;
import javax.swing.*;

/**
  * This abstract class extends JPanel and provides methods
  * to draw the frame, title, labels and scale values for
  * a display plot of some kind. The subclass will provide
  * the overriding methods to draw a plot within the frame.
  */
public abstract class PlotPanel extends JPanel
{

    // Colors for the plot components
  protected Color frameLineColor  = Color.BLACK;
  protected Color frameFillColor  = Color.LIGHT_GRAY;
  protected Color axesNumColor    = Color.GRAY;
  protected Color titleColor      = Color.BLACK;
  protected Color bgColor         = Color.WHITE;

  // Dimension and position variables
  protected int panelWidth, panelHeight;
  protected int frameWidth, frameHeight;
  protected int frameX,frameY;
  protected int titleX,titleY,titleWidth,titleHeight;
  protected int vertScaleX,vertScaleY,
                vertScaleWidth,vertScaleHeight;

  protected int horzScaleX, horzScaleY,
                horzScaleWidth,horzScaleHeight;

  protected int horzLabelX,horzLabelY,
                horzLabelHeight,horzLabelWidth;

  // These constants used in drawText() method
  // for placement of the text within a given
  // rectangular area.
  final int CENTER = 0;
  final int LEFT   = 1;
  final int RIGHT  = 2;

  // Limit below which scale values use decimal format,
  // above which they use scientific format.
  double yDecimalSci = 100.0;
  double xDecimalSci = 1000.0;

  // Limit above which scale values use decimal format,
  // below which they use scientific format.
  double yLoDecimalSci = 0.01;
  double xLoDecimalSci = 0.01;

  // Fraction of panel for the plot frame
  protected double FRAME_HT = 0.60;
  protected double FRAME_WD = 0.75;
  protected double FRAME_X  = 0.20;
  protected double FRAME_Y  = 0.15;

  // Fraction of vertical area for the title
  protected double TITLE_HT = 0.10;

  // Fractions of the panel for left scale values
  protected double VSCALE_X = 0.100;
  protected double VSCALE_HT= 0.075;
  protected double VSCALE_WD= 0.18;

  // Fraction of vertical for the horizontal scale values
  protected double HSCALE_HT= 0.07;

  /**
   * Determine positions for titles, labels
   * and numbers for the graph.
   * Use 60% of vertical space for the graph,
   *  "  15%  "    "      "     "  top title area
   *  "  25%  "    "      "     "  bot scale & label area
   * Use 75% of horizonal space for the graph,
   *   " 18%  "    "      "     "   vertical scale values
   *   "  5%  "    "      "     "   right margin
   *
   */
  public void getPositions()
  { // First obtain the panel dimensions
    panelWidth  = getSize().width;
    panelHeight = getSize().height;
    frameWidth = (int)(FRAME_WD * panelWidth);
    frameHeight= (int)(FRAME_HT * panelHeight);

    // Histogram frame location
    frameX = (int)(FRAME_X * panelWidth);
    frameY = (int)(FRAME_Y * panelHeight);

    // Coordinates for the title
    titleX = frameX ;
    titleHeight = (int)(TITLE_HT * panelHeight);
    titleY = frameY - titleHeight;
    titleWidth = frameWidth;

    // Coordinates for the vertical max value

    // Use all the horizontal room from the left side
    // up to the frame
    vertScaleX      = 0;
    vertScaleWidth  = (int)(VSCALE_WD * panelWidth);
    vertScaleHeight = (int)(VSCALE_HT * panelHeight);

    // Coordinates for the  horizontal values
    horzScaleHeight = (int)(HSCALE_HT * panelHeight);
    horzScaleY = frameY+frameHeight;


    // Coordinates for the horizontal label
    horzLabelY = horzScaleY+horzScaleHeight;
    horzLabelHeight = panelHeight - horzLabelY;
    horzLabelWidth = frameWidth;
    horzLabelX = frameX;
  }

  // Abstract methods to be overriden
  abstract String getTitle();
  abstract String getXLabel();
  abstract void getScaling();
  abstract void paintContents(Graphics g);

  /**
   *  Optional frame color settings
   *  @param line color of frame line.
   *  @param fill color of frame background
   *  @param title title above frame
   *  @param background background color outside
   *  of plot frame.
   */
  public void setFrameColors(Color line, Color fill,
                             Color numbers,
                             Color title,
                             Color background)
  {
    if( line != null)       frameLineColor = line;
    if( fill != null)       frameFillColor = fill;
    if( numbers != null)    axesNumColor = numbers;
    if( title != null)      titleColor = title;
    if( background != null) bgColor = background;
  }


  /**
   * Obtains coordinates for the frame and the title,
   * labels, etc. Draw title and x axis label.
   * Invokes the abstract paintContents(g),
   * a concrete version of which comes from a subclass.
   */
  public void paintComponent(Graphics g)
  {
    // First paint background
    g.setColor(bgColor);
    super.paintComponent(g);

    // Get the positions for the titles, labels, etc.
    getPositions();

    // Draw the top title
    g.setColor(titleColor);
    drawText(g, getTitle(),
                titleX, titleY, titleWidth, titleHeight,0,
                CENTER);

    // Draw the bottom label
    drawText(g, getXLabel(),
                horzLabelX, horzLabelY,
                horzLabelWidth, horzLabelHeight,0,
                CENTER);


    // Draw the plot frame.
    paintFrame(g);

    // Draw the the plot within in the frame.
    // This method must be overriden.
    paintContents(g);

  }

  /**
   *    Draw the plot frame.
   */
  public void paintFrame(Graphics g)
  {
    Color oldColor = g.getColor();
    g.setColor(frameLineColor);
    g.drawRect(frameX, frameY, frameWidth, frameHeight);
    g.setColor(oldColor);
  }

  /**
   * Draw scale numbers on the axes. Uses PlotFormat to
   * format the numbers.
   * @param g graphics context
   * @param xValue array of values for horizontal scale
   * axis.
   * @param yValue array of values for vertical scale axis.
   */
  void drawAxesNumbers(Graphics g,
                        double [] xValue,
                        double [] yValue)
  {
    if( xValue.length == 0 || yValue.length == 0 ) return;

    String strValue;
    int typeSize=0;

    g.setColor(axesNumColor);

    // Draw the horizontal axis numbers
    // Get the scale range.
    double xRange = xValue[xValue.length-1] - xValue[0];
    if( xRange <= 0.0 ) return;

    int horz;
    double dHorz = (double)frameWidth/(double)(xValue.length);

    for(int i=xValue.length-1; i >= 0 ; i--)
    {

        // Scale the xValue to the corresponding pixel value
        horz = (int)(frameWidth*((xValue[i]-xValue[0])/xRange)
                   - dHorz/2.0) + frameX;
        // Don't let string go too far to the right.
        if( (horz + dHorz) > panelWidth) dHorz = panelWidth-horz;

        // Convert the x value to a string
        strValue =
         PlotFormat.getFormatted(xValue[i],
                                 xDecimalSci,xLoDecimalSci,2);

        // Now draw the numbers on the axes
        if( i == xValue.length-1)
           typeSize = drawText(g, strValue,
             horz, horzScaleY, (int)dHorz ,horzScaleHeight,
             0,CENTER);
        else
             drawText(g, strValue,
             horz, horzScaleY, (int)dHorz ,horzScaleHeight,
             typeSize,CENTER);
    }
    // Draw the vertical axis number
    // Get the scale range.
    double yRange = yValue[yValue.length-1] - yValue[0];
    if( yRange <= 0.0 ) return;

    for(int i=yValue.length-1; i >= 0 ; i--)
    {
        // Convert the y value to a string
        strValue =
           PlotFormat.getFormatted(yValue[i],
                                   yDecimalSci,yLoDecimalSci,2);

         // Scale the yValue to the corresponding pixel value
         vertScaleY = frameY + frameHeight -
                 (int)(frameHeight*( (yValue[i]-yValue[0])/yRange))
                 - vertScaleHeight;


        if( i == yValue.length-1)
            typeSize = drawText(g, strValue,
                  vertScaleX, vertScaleY,
                  vertScaleWidth, vertScaleHeight,
                  0, RIGHT);
        else
           drawText(g, strValue,
                  vertScaleX, vertScaleY,
                  vertScaleWidth, vertScaleHeight,
                  typeSize, RIGHT);
    }

  }


  /**
   *   Draw a string in the center of a given box.
   *   Reduce the font size if necessary to fit. Can
   *   fix the type size to a value passed as an argument.
   *   The position of the string within the box passed
   *   as LEFT, CENTER or RIGHT constant value.
   *   Don't draw the strings if they do not fit.
   */
  int drawText( Graphics g, String msg,
                 int xBox, int yBox,
                 int boxWidth, int boxHeight,
                 int fixedTypeSizeValue,
                 int position)
  {
    boolean fixedTypeSize = false;
    int typeSize = 24;

      // Fixed to a particular type size.
    if(fixedTypeSizeValue > 0)
    {
      fixedTypeSize = true;
      typeSize = fixedTypeSizeValue;
    }

    int typeSizeMin = 8;
    int x=xBox,y=yBox;
    do
    {
      // Create the font and pass it to the  Graphics context
      g.setFont(new Font("Monospaced",Font.PLAIN,typeSize));

      // Get measures needed to center the message
      FontMetrics fm = g.getFontMetrics();

      // How many pixels wide is the string
      int msgWidth = fm.stringWidth(msg);

      // How tall is the text?
      int msgHeight = fm.getHeight();

      // See if the text will fit in the allotted
      // vertical limits
      if( msgHeight < boxHeight && msgWidth < boxWidth)
      {
          y = yBox + boxHeight/2 +(msgHeight/2);
            if( position == CENTER)
              x = xBox + boxWidth/2 - (msgWidth/2);
          else if(position == RIGHT)
            x = xBox + boxWidth - msgWidth;
          else
            x = xBox;

          break;
      }

      // If fixedTypeSize and wouldn't fit, don't draw.
      if( fixedTypeSize) return -1;

      // Try smaller type
      typeSize -= 2;

    } while (typeSize >= typeSizeMin);

    // Don't display the numbers if they did not fit
    if( typeSize < typeSizeMin) return -1;

    // Otherwise, draw and return positive signal.
    g.drawString(msg,x,y);
    return typeSize;
   }
}

