//Title:        UFTextField for Java Control Interface using UFLib Protocol
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JTextField;

//===============================================================================
/**
 * Extends the JTextField class to add some simple useful options,
 * like changing color depending on previous/current state.
 */

public class UFTextField extends JTextField implements FocusListener, KeyListener
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextField.java,v 1.17 2004/06/08 22:28:15 varosi Exp $";

    public String prev_text = "";
    String new_text = "";
    String Name, Label, Units="";
    String _alarmText = null;

//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFTextField() {
	try  {
	    jbInit("TextField");
	}
	catch(Exception ex) {
	    System.out.println("Error creating a UFTextField: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextField(String description) {
	try  {
	    jbInit(description);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     */
    public UFTextField( String description, String text ) {
	try  {
	    new_text = text;
	    prev_text = text;
	    jbInit( description );
	    this.setText( text );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor: special case with specified # of columns and no focuslistener.
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     *@param Ncolumns    int: # of columns to set for field
     */
    public UFTextField( String description, String text, int Ncolumns ) {
	try  {
	    if( Ncolumns > 0 ) this.setColumns( Ncolumns );
	    new_text = text;
	    prev_text = text;
	    jbInit( description );
	    this.setText( text );
	    removeFocusListener(this);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param editable    boolean: state of editing
     */
    public UFTextField( String description, boolean editable ) {
	try  {
	    jbInit(description);
	    this.setEditable( editable );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void jbInit( String description ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	this.Label = description;
	addFocusListener(this);
	addKeyListener(this);
	setToolTipText();
    }
//-------------------------------------------------------------------------------
    /**
     *UFTextField#setAlarmValue
     *@param alarmVal     String: text value for which orange background color should be indicated.
     */
    public void setAlarmValue( String alarmVal ) { if( alarmVal != null ) _alarmText = alarmVal; }

//-------------------------------------------------------------------------------
    /**
     * Sets the text for the tool tip -- returns no arguments
     */
    void setToolTipText() { this.setToolTipText( Name + " = " + new_text + " " + Units ); }

//-------------------------------------------------------------------------------

    public String name() { return this.Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }
    public String Label() { return Label; }

//-------------------------------------------------------------------------------

    public void focusGained(FocusEvent e) 
    { 
	if( prev_text.trim().equals("") ) prev_text = getText().trim();
	setToolTipText();
    }
//-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	new_text = getText().trim();

	if( new_text.equals( prev_text ) )
	    setBackground( Color.white );
	else
	    setBackground( Color.yellow );
    }

//-------------------------------------------------------------------------------
    /**
     * Event Handling Methods
     *@param ke KeyEvent: TBD
     */
    public void keyTyped (KeyEvent ke) { }
    public void keyReleased (KeyEvent ke) { }

//-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ke KeyEvent: TBD
     */
    public void keyPressed (KeyEvent ke)
    {
	setBackground( Color.yellow );

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		new_text = getText().trim();

		if( new_text.equals( prev_text ) )
		    setBackground( Color.white );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prev_text );
		setBackground( Color.white );
	    }
    }
//-------------------------------------------------------------------------------

    public void setNewState()
    {
	if( _alarmText != null ) {
	    if( new_text.equals( _alarmText ) )
		setBackground( Color.orange );
	    else
		setBackground( Color.white );
	}
	else setBackground( Color.white );

	prev_text = new_text;
	setToolTipText();
    }
//-------------------------------------------------------------------------------

    public void setNewState( String newText )
    {
	setText( newText );
	this.new_text = newText;
	setNewState();

          if( this.new_text.indexOf("WARN") >= 0 ) {
	      setBackground( Color.yellow );
	      Toolkit.getDefaultToolkit().beep();
	  }

          if( this.new_text.indexOf("ERR") >= 0 ) {
	      setBackground( Color.yellow );
	      Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	  }
    }
} //end of class UFTextField

