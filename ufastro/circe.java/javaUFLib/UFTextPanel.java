//Title:        UFTextPanel for Java Control Interface (JCI)
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  Creates 2 text fields with Label on left (default) or right, all in one Panel.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JTextField;

//===============================================================================
/**
 * Creates 2 text fields with Label on left (default) or right, all in one Panel,
 * together handling desired and active values, and changing color depending state (diff or eq).
 */

public class UFTextPanel extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextPanel.java,v 1.17 2004/08/20 23:54:06 varosi Exp $";

    String desiredVal = "";
    String activeVal = "";
    int desiredInt = -1;
    int activeInt = -1;

    String Name, Units="";
    boolean nameOnRight = false;
    boolean NOactiveField = false;
    boolean userEvent = false;
    boolean _checkBox = false; //if true then Label will be a JCheckBox for enabling/disabling selection.
    boolean _checkBoxState = false; //initially the JCheckBox will NOT be checked (selected).
    JCheckBox paramEnable;
    JLabel paramName;
    public JTextField desiredField = new JTextField();
    public JTextField activeField = new JTextField();
//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFTextPanel() {
	try  {
	    createPanel("parameter");
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextPanel(String description) {
	try  {
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param checkBoxState  boolean: if present a check box is created on left with starting state.
     *@param description    String: Information regarding the field
     */
    public UFTextPanel( boolean checkBoxState, String description ) {
	try  {
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description    String: Information regarding the field
     *@param checkBoxState  boolean: true causes param name to appear on right instead of left.
     */
    public UFTextPanel( String description, boolean nameRight ) {
	try  {
	    this.nameOnRight = nameRight;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param checkBoxState  boolean: if present a check box is created on left with starting state.
     *@param description    String: Information regarding the field
     *@param checkBoxState  boolean: true causes param name to appear on right instead of left.
     */
    public UFTextPanel( boolean checkBoxState, String description, boolean nameRight ) {
	try  {
	    this.nameOnRight = nameRight;
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextPanel( int numFields, String description ) {
	try  {
	    if( numFields < 2 ) this.NOactiveField = true;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void createPanel( String description ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	if( _checkBox )
	    {
		paramEnable = new JCheckBox( description );
		paramEnable.setSelected(_checkBoxState);
		desiredField.setEnabled(_checkBoxState);

		paramEnable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    if( paramEnable.isSelected() )
				desiredField.setEnabled(true);
			    else
				desiredField.setEnabled(false);
			}
		    });
	    }
	else {
	    paramName = new JLabel( description );
	    desiredField.setEditable(false);
	}

	desiredField.setBackground(Color.white);
	activeField.setBackground(Color.white);
	activeField.setEditable(false);
	this.setToolTipText();
	setLayout(new RatioLayout());

	if( nameOnRight )
	    {
		if( NOactiveField ) {
		    this.add("0.01,0.01;0.49,0.99", desiredField );
		    if( _checkBox )
			this.add("0.51,0.01;0.49,0.99", paramEnable);
		    else
			this.add("0.51,0.01;0.49,0.99", paramName);
		}
		else {
		    this.add("0.01,0.01;0.28,0.99", desiredField );
		    this.add("0.30,0.01;0.28,0.99", activeField );
		    if( _checkBox )
			this.add("0.59,0.01;0.41,0.99", paramEnable);
		    else
			this.add("0.59,0.01;0.41,0.99", paramName);
		}
	    }
	else  //default is name (Label) on Left:
	    {
		if( NOactiveField ) {
		    if( _checkBox )
			this.add("0.01,0.01;0.59,0.99", paramEnable);
		    else
			this.add("0.01,0.01;0.59,0.99", paramName);
		    this.add("0.60,0.01;0.39,0.99", desiredField );
		}
		else {
		    if( _checkBox )
			this.add("0.01,0.01;0.40,0.99", paramEnable);
		    else
			this.add("0.01,0.01;0.40,0.99", paramName);
		    this.add("0.41,0.01;0.28,0.99", desiredField );
		    this.add("0.70,0.01;0.29,0.99", activeField );
		}
	    }
    }
//-------------------------------------------------------------------------------

    public boolean isSelected()
    {
	if( _checkBox )
	    return paramEnable.isSelected();
	else
	    return true;
    }
//-------------------------------------------------------------------------------

    void setToolTipText() {
	if( NOactiveField )
	    setToolTipText( Name + " = " + desiredVal + " " + Units );
	else
	    setToolTipText( Name + ": Desired = " + desiredVal + " " + Units +
			    ",  Active = " + activeVal + " " + Units );
    }
//-------------------------------------------------------------------------------

    public String getActive() {	return activeVal.trim(); }
    public String getDesired() { return desiredVal.trim(); }

    public int getActiveInt() { return activeInt; }
    public int getDesiredInt() { return desiredInt; }

//-------------------------------------------------------------------------------

    public void setActive( String newVal )
    {
	activeVal = newVal.trim();
	setToolTipText();

	if( ! activeField.getText().equals( activeVal ) ) activeField.setText( activeVal );

	if( desiredVal.equals( activeVal ) ) desiredField.setBackground( Color.white );
    }
//-------------------------------------------------------------------------------

    public void setDesired( String newVal )
    {
	desiredVal = newVal.trim();
	setToolTipText();

	if( ! desiredField.getText().equals( desiredVal ) ) desiredField.setText( desiredVal );

	if( ! desiredVal.equals( activeVal ) ) desiredField.setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    public void setActive( String newVal, int newInt )
    {
	setActive( newVal );
	activeInt = newInt;
	if( desiredInt == activeInt ) desiredField.setBackground( Color.white );
    }
//-------------------------------------------------------------------------------

    public void setDesired( String newVal, int newInt )
    {
	setDesired( newVal );
	desiredInt = newInt;
	if( desiredInt != activeInt ) desiredField.setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    //for cases of NOactiveField = true, just set the text in visible field:

    public void setValue( String newVal )
    {
	desiredVal = newVal.trim();
	setToolTipText();
	desiredField.setText( desiredVal );
    }

    public void setValue( int newVal ) { setValue( Integer.toString(newVal) ); }
    public void setValue( float newVal ) { setValue( Float.toString(newVal) ); }
    public void setValue( double newVal ) { setValue( Double.toString(newVal) ); }

//-------------------------------------------------------------------------------

    public String name() { return Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }

} //end of class UFTextPanel

