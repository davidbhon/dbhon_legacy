//Title:        UFobsMonitor for Java Control Interface (JCI) using UFLib Protocol
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javaUFProtocol.*;

//===============================================================================
/**
 * Observation Status Monitor Panel
 * Custom panel for obs status monitor with Detector Control Agent or Data Acq. Server.
 * @author Frank Varosi
 */

public class UFobsMonitor extends UFLibPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFobsMonitor.java,v 1.1 2004/11/18 20:56:04 amarin Exp $";

    //used by recvStatus() method:
    protected int nullCnt = 0;
    protected DataInputStream NotifyStream;

    JProgressBar obsProgressBar = new JProgressBar(0,100);
    StatusMonitor obsMonitor;

    UFFrameConfig[] seqFrameConfs = new javaUFProtocol.UFFrameConfig[100];

    UFTextField acqServerStatus  = new UFTextField("acq. Server:",false); //editable = false.

    UFTextPanel frmTotal = new UFTextPanel(1,"Total Frames:"); //# fields = 1, so no active field.
    UFTextPanel frmDMAcnt = new UFTextPanel(1,"# DMAs:");
    UFTextPanel frmGrabCnt = new UFTextPanel(1,"# grabbed:");
    UFTextPanel frmProcCnt = new UFTextPanel(1,"# processed:");
    UFTextPanel frmWritCnt = new UFTextPanel(1,"# written:");
    UFTextPanel frmSendCnt = new UFTextPanel(1,"# sent:");

    UFTextPanel nodBeam = new UFTextPanel(1,"Beam:");
    UFTextPanel nodSet = new UFTextPanel(1,"  Nod:");
    UFTextPanel nodTotal = new UFTextPanel(1," of:");
    String[] BeamNames = {"A","B"};

    UFTextMinMax bgADUs = new UFTextMinMax("Bkgr.ADUs:");
    UFTextMinMax rdADUs = new UFTextMinMax("Read ADUs:");
    UFTextMinMax bgSigma = new UFTextMinMax("Bkgr.Noise:");
    UFTextMinMax rdSigma = new UFTextMinMax("Read Noise:");

    HashMap statusDisplay = new HashMap(4);  //container of following UFTextField objects:
    UFTextField observStatus = new UFTextField("observStatus",false);
    UFTextField dcState      = new UFTextField("dcState",false);
    UFTextField detType      = new UFTextField("detType",false);
    UFTextField archiveFile  = new UFTextField("FITS_FileName",false);

    UFLibPanel MasterPanel;   //used to get new params status.
    UFLibPanel DetectorPanel; //used to get same host and port as in Detector Panel.
    UFLibPanel BiasPanel;     //used to set status of bias power and levels.
    UFLibPanel DataDisplay;   //option for quick-look data display updates.
//-------------------------------------------------------------------------------
    /**
     * Default constructor.
     * Calls private method, initStatusPanel()
     */
    public UFobsMonitor() {
	super( "kepler", 52008 ); //to give default host and port to UFLibPanel.
	initStatusPanel();
    } 
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointer to DetectorPanel in JCI.
     * Calls private method, initStatusPanel().
     */
    public UFobsMonitor( UFLibPanel DetectorPanel )
    {
	this.DetectorPanel = DetectorPanel;
	setHostAndPort( DetectorPanel.agentHost, DetectorPanel.agentPort );
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointers to DetectorPanel and BiasPanel in JCI.
     * Calls private method, initStatusPanel().
     */
    public UFobsMonitor( UFLibPanel DetectorPanel, UFLibPanel BiasPanel )
    {
	this.DetectorPanel = DetectorPanel;
	this.BiasPanel = BiasPanel;
	setHostAndPort( DetectorPanel.agentHost, DetectorPanel.agentPort );
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointers to DetectorPanel and BiasPanel in JCI.
     * Calls private method, initStatusPanel().
     */
    public UFobsMonitor( UFLibPanel MasterPanel, UFLibPanel DetectorPanel, UFLibPanel BiasPanel )
    {
	this.MasterPanel = MasterPanel;
	this.DetectorPanel = DetectorPanel;
	this.BiasPanel = BiasPanel;
	setHostAndPort( DetectorPanel.agentHost, DetectorPanel.agentPort );
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointer to data display client.
     * Calls private method, initStatusPanel().
     */
    public UFobsMonitor( String hostName, int portNum, UFLibPanel dataDispClient )
    {
	this.DataDisplay = dataDispClient;
	setHostAndPort( hostName, portNum );
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Private method: initStatusPanel()
     * Calls private method createStatusPanel() to do all component initialization,
     *  and creates StatusMonitor object to wait for and recv status from agent/server.
     */
    private void initStatusPanel()
    {
	try {
	    createStatusPanel();
	}
	catch (Exception e) {
	    e.printStackTrace();
	    System.err.println("UFobsMonitor> Failed creating Obs.Status display panel!");
	}

	try {
	    obsMonitor = new StatusMonitor(1000);
	}
	catch (Exception e) {
	    e.printStackTrace();
	    System.err.println("UFobsMonitor> Failed creating agent/server status monitor!");
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void createStatusPanel() throws Exception
    {
	obsProgressBar.setValue(0);
	obsProgressBar.setBackground( Color.white );

	statusDisplay.put( observStatus.name(), observStatus );
	statusDisplay.put( dcState.name(), dcState );
	statusDisplay.put( detType.name(), detType );
	statusDisplay.put( archiveFile.name(), archiveFile );

	this.setLayout(new RatioLayout());
	this.add("0.01,0.03;0.09,0.17", new JLabel("obs. STATUS:") );
	this.add("0.10,0.03;0.42,0.17", observStatus );
	this.add("0.01,0.23;0.09,0.17", new JLabel("obs. Progress:") );
	this.add("0.10,0.23;0.42,0.17", obsProgressBar );
	this.add("0.01,0.43;0.09,0.17", new JLabel("DC - STATE:") );
	this.add("0.10,0.43;0.42,0.17", dcState );
	this.add("0.01,0.63;0.09,0.17", new JLabel(acqServerStatus.Label()) );
	this.add("0.10,0.63;0.42,0.17", acqServerStatus );
	this.add("0.01,0.83;0.09,0.17", new JLabel("FITS  File:") );
	this.add("0.10,0.83;0.42,0.17", archiveFile );

	JPanel frmCntsPanel = new JPanel();
	frmCntsPanel.setLayout(new GridLayout(0,1));
	frmCntsPanel.setBorder(new EtchedBorder(0));
	frmCntsPanel.add(frmTotal);
	frmCntsPanel.add(frmDMAcnt);
	frmCntsPanel.add(frmGrabCnt);
	frmCntsPanel.add(frmProcCnt);
	frmCntsPanel.add(frmWritCnt);
	frmCntsPanel.add(frmSendCnt);
	this.add("0.53,0.01;0.17,0.99", frmCntsPanel );

	JPanel nodbeamPanel = new JPanel();
	nodbeamPanel.setLayout(new GridLayout(1,0));
	nodbeamPanel.add( nodBeam );
	nodbeamPanel.add( nodSet );
	nodbeamPanel.add( nodTotal );
//	this.add("0.71,0.00;0.29,0.19", nodbeamPanel );

	JPanel statisticsPanel = new JPanel();
	statisticsPanel.setLayout(new GridLayout(0,1));
	statisticsPanel.setBorder(new EtchedBorder(0));
	JPanel titlePanel = new JPanel();
	titlePanel.setLayout(new RatioLayout());
	titlePanel.add("0.01,0.01;0.30,0.98", new JLabel(" Statistics:") );
	titlePanel.add("0.31,0.01;0.22,0.98", new JLabel(" current") );
	titlePanel.add("0.54,0.01;0.22,0.98", new JLabel(" min") );
	titlePanel.add("0.77,0.01;0.22,0.98", new JLabel(" max") );
	statisticsPanel.add( titlePanel );
	statisticsPanel.add( bgADUs );
	statisticsPanel.add( rdADUs );
	statisticsPanel.add( bgSigma );
	statisticsPanel.add( rdSigma );
//	this.add("0.71,0.19;0.29,0.81", statisticsPanel );
	this.add("0.71,0.01;0.29,0.99", statisticsPanel );

	// Add the connect status socket action to Detector Panel connect button,
	// so when user re-connects to DC agent it will do both cmdSocket and statusSocket:

	if( DetectorPanel != null ) {
	    DetectorPanel.connectButton.addActionListener( new ActionListener()
		{ public void actionPerformed(ActionEvent e) { connectToAgent(); } });
	}
	else if( DataDisplay != null ) {
	    DataDisplay.connectButton.addActionListener( new ActionListener()
		{ public void actionPerformed(ActionEvent e) { connectToAgent(); } });
	}
    }

//-------------------------------------------------------------------------------
    /**
     * UFobsMonitor#connectToAgent
     *  Connect status socket to Detector Control Agent and restart the status monitor.
     */
    public boolean connectToAgent()
    {
	obsMonitor.stopRunning();

	if( connectStatusSocket() )
	    {
		obsMonitor.resumeRunning();
		return true;
	    }
	else return false;
    }
//-------------------------------------------------------------------------------
    /**
     * UFobsMonitor#connectStatusSocket
     *  Connect via socket to the ancillary automatic status feature Detector Control Agent,
     *  for recving (only) status messages.
     */
    boolean connectStatusSocket()
    {
	if( agentSocket != null ) { // close connection if one exists
	    try {
		System.out.println(className+"::connectStatusSocket> closing status socket...");
		agentSocket.close();
		agentSocket = null;
		indicateConnectStatus(false);
	    }
	    catch (IOException ioe) {
		String message = className+"::connectStatusSocket> " + ioe.toString();
		System.err.println( message );
	    }
	    try { Thread.sleep(100);} catch ( Exception _e) {}
	}

	try {
	    if( DetectorPanel != null ) {
		String h = DetectorPanel.getHostField();
		if( h != null ) { if( h.length() > 0 ) agentHost = h; }
		int p = DetectorPanel.getPortField();
		if( p > 0 ) agentPort = p;
	    }
	    String message = className
		+ "::connectStatusSocket> port=" + agentPort + " @ host = " + agentHost;
	    System.out.println( message );
	    InetSocketAddress agentIPsoca = new InetSocketAddress( agentHost, agentPort );
	    agentSocket = new Socket();
	    agentSocket.connect( agentIPsoca, CONNECT_TIMEOUT );
	    agentSocket.setSoTimeout( HANDSHAKE_TIMEOUT );

	    //must send DC agent a timestamp with string "status" in it:
	    UFTimeStamp uft = new UFTimeStamp(clientName + ":STATUS");

	    //but if talking to Data Acq.Server then request the CameraType:
	    if( DataDisplay != null ) uft.rename("CT");

	    if( uft.sendTo(agentSocket) <= 0 ) {
	        connectError("Handshake Send");
		return false;
	    }

	    //get response from agent
	    UFProtocol ufp = null;

	    if( (ufp = UFProtocol.createFrom(agentSocket)) == null ) {
	        connectError("Handshake Read");
		return false;
	    }

	    if( DataDisplay != null ) {  //special case: request notification stream of FrameConfigs:
		message = "CameraType = " + ufp.name();
		dcState.setNewState( message );
		//now request current frame & obs configs and status monitor thread will recv:
		uft.rename("FC");
		if( uft.sendTo(agentSocket) <= 0 ) {
		    sendError("frame config");
		    return false;
		}
		uft.rename("OC");
		if( uft.sendTo(agentSocket) <= 0 ) {
		    sendError("obs. config");
		    return false;
		}
		//request notification stream of FrameConfig updates and status monitor thread will recv:
		uft.rename("NOTIFY");
		if( uft.sendTo(agentSocket) <= 0 ) {
		    sendError("notification stream");
		    return false;
		}
		//create input stream object for recvStatus() method to check for available notifies:
		NotifyStream = new DataInputStream( agentSocket.getInputStream() );
	    }
	    else message = "DC agent handshake = " + ufp.name();

	    System.out.println( message );
	    indicateConnectStatus(true);
	    //set normal infinite timeout for socket so it blocks on recv:
	    agentSocket.setSoTimeout(0);
	    return true;
	}
	catch (Exception x) {
	    indicateConnectStatus(false);
	    agentSocket = null;
	    String message = className+"::connectStatusSocket> " + x.toString();
	    System.err.println(message);
	    Toolkit.getDefaultToolkit().beep();
	    return false;
	}
    }
//-------------------------------------------------------------------------------

    public void connectError( String errmsg )
    {
	String message = className + "::connectStatusSocket> " + errmsg + " ERROR";
	System.err.println(message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);
	try {
	    agentSocket.close();
	    agentSocket = null;
	}
	catch (IOException ioe) {
	    message = "connectError> " + ioe.toString();
	    System.err.println(className + "::" + message);
	}
    }
//-------------------------------------------------------------------------------

    public void sendError( String errmsg )
    {
	String message = className + "::connectStatusSocket> " + errmsg + " request Send ERROR";
	System.err.println(message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);
	try {
	    agentSocket.close();
	    agentSocket = null;
	}
	catch (IOException ioe) {
	    message = "sendError> " + ioe.toString();
	    System.err.println(className + "::" + message);
	}
    }
//-------------------------------------------------------------------------------

    public void indicateConnectStatus( boolean connStatus )
    {
	String target = "?";

	if( DetectorPanel != null ) {
	    DetectorPanel.indicateConnectStatus( connStatus );
	    target = "DC Agent";
	}
	else if( DataDisplay != null ) {
	    DataDisplay.indicateConnectStatus( connStatus );
	    target = "Data Acquisition Server";
	}

	if( connStatus )
	    target = "Connected to " + target;
	else
	    target = "Failed connecting to " + target;

	target += " on port=" + agentPort + " @ host = " + agentHost;
	observStatus.setNewState( target );
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by inner class StatusMonitor (see next below).
     */
    void recvStatus()
    {
	UFProtocol ufp = UFProtocol.createFrom( agentSocket );

	if( ufp == null )
	    {
		String errmsg = className+"::recvStatus> recvd null object.";
		System.err.println( errmsg );
		System.out.println( errmsg );
		if( ++nullCnt > 3 ) {
		    connectToAgent();
		    nullCnt = 0;
		}
		else try { Thread.sleep( 700 ); } catch( Exception x ) {}
	    }
	else if( ufp instanceof UFFrameConfig && DataDisplay != null )
	    {
		UFFrameConfig frameConf =  (UFFrameConfig)ufp;
		checkObsStartOrEnd( frameConf );
		int notifyCnt = 0;
		seqFrameConfs[notifyCnt++] = frameConf;

		// Check for more FrameConfigs that may be queued in notification stream:
		// (set to short timeout so cannot get stuck, but > 0.3 sec for reliability)

		try {
		    agentSocket.setSoTimeout(1000);

		    while( NotifyStream.available() > 0 && notifyCnt < seqFrameConfs.length )
			{
			    ufp = UFProtocol.createFrom( agentSocket );

			    if( ufp == null ) {
				System.err.println( className+"::recvStatus> null object!" );
				break;
			    }

			    if( ufp instanceof UFFrameConfig )
				seqFrameConfs[notifyCnt++] = (UFFrameConfig)ufp;
			    else
				procStatusInfo( ufp );
			}
		    agentSocket.setSoTimeout(0); //reset to infinite timeout.
		}
		catch(IOException ioe) {
		    System.err.println( className+"::recvStatus>"+ioe.toString() );
		}
		catch( Exception x ) {
		    System.err.println( className+"::recvStatus>" + x.toString() );
		}

		// Process the sequence of FrameConfig notifications,
		// but check for duplicate name() of each and process only the final one
		// (since name() tells which frames are updated):

		for( int i=0; i < notifyCnt; i++ ) {
		    UFFrameConfig fc = seqFrameConfs[i];
		    if( fc != null ) {
			int k = i;
			for( int j=i+1; j < notifyCnt; j++ ) {
			    if( seqFrameConfs[j] != null ) {
				if( seqFrameConfs[j].name().equals( fc.name() ) ) {
				    seqFrameConfs[k] = null;
				    k = j;
				    fc = seqFrameConfs[j];
				}
			    }
			}
			procFrameConfig( fc );
			seqFrameConfs[i] = null;
		    }
		}
	    }
	else procStatusInfo( ufp );
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to check the FrameConfig for Start/Abort/End of an obs.
     */
    void checkObsStartOrEnd( UFFrameConfig fc )
    {
	if( fc.frameProcCnt == 1 ) {                     //new obs: request ObsConfig and File info...
	    observStatus.setNewState("STARTING...");
	    UFTimeStamp uft = new UFTimeStamp("OC");
	    if( uft.sendTo(agentSocket) <= 0 ) sendError("obs. config");
	}
	else if( fc.frameObsTotal < 0 ) {                //aborted obs.
	    observStatus.setNewState("ABORTED.");
	}
	else if( fc.frameGrabCnt == fc.frameObsTotal ) { //completed obs.
	    observStatus.setNewState("COMPLETED");
	}
	else if( fc.frameGrabCnt > 0 || fc.frameProcCnt >= 0 ){
	    observStatus.setNewState("Observing...");
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to process a UFFrameConfig object.
     */
    void procFrameConfig( UFFrameConfig obsFC )
    {
	int totalFrames = obsFC.frameObsTotal;
	if( totalFrames < 0 ) totalFrames = -totalFrames;
	if( totalFrames > 0 )
	    obsProgressBar.setValue( (100*obsFC.frameGrabCnt)/totalFrames );

	acqServerStatus.setNewState( obsFC.name() );
	frmTotal.setValue( obsFC.frameObsTotal );
	frmDMAcnt.setValue( obsFC.DMAcnt );
	frmGrabCnt.setValue( obsFC.frameGrabCnt );
	frmProcCnt.setValue( obsFC.frameProcCnt );
	frmWritCnt.setValue( obsFC.frameWriteCnt );
	frmSendCnt.setValue( obsFC.frameSendCnt );
	nodSet.setValue( obsFC.NodSet );

	if( obsFC.NodBeam >= 0 && obsFC.NodBeam < BeamNames.length )
	    nodBeam.setValue( BeamNames[obsFC.NodBeam] );
	else nodBeam.setValue("?");

	bgADUs.setCurrMinMax( obsFC.bgADUs, obsFC.bgADUmin, obsFC.bgADUmax );
	rdADUs.setCurrMinMax( obsFC.rdADUs, obsFC.rdADUmin, obsFC.rdADUmax );
	bgSigma.setCurrMinMax( obsFC.sigmaFrmNoise, obsFC.sigmaFrmMin, obsFC.sigmaFrmMax );
	rdSigma.setCurrMinMax( obsFC.sigmaReadNoise, obsFC.sigmaReadMin, obsFC.sigmaReadMax );

	if( DataDisplay != null ) DataDisplay.updateFrames( obsFC );
	if( MasterPanel != null ) MasterPanel.updateFrames( obsFC );
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to process the status info object.
     */
    void procStatusInfo( UFProtocol ufp )
    {
	if( ufp == null ) return;

	if( ufp instanceof UFFrameConfig )
	    {
		procFrameConfig( (UFFrameConfig)ufp );
	    }
	else if( ufp instanceof UFObsConfig )
	    {
		UFObsConfig obsConfig = (UFObsConfig)ufp;
		nodTotal.setValue( obsConfig.nodSets() );
	    }
	else if( ufp instanceof UFStrings )
	    {
		UFStrings ufs = (UFStrings)ufp;
		UFTextField statusToUpdate = (UFTextField)statusDisplay.get( ufs.name() );

		if( statusToUpdate == null )
		    {
			String name = ufs.name();
			if( name.indexOf("getStatus") >= 0 ) //check if new DC params available:
			    {
				String what = ufs.valData(0);
				if( what.indexOf("Param") > 0 ) {
				    if( MasterPanel != null )
					MasterPanel.getNewParams( clientName );
				    else if( DetectorPanel != null )
					DetectorPanel.getNewParams( clientName );
				    return;
				}
			    }
			else if( BiasPanel != null && name.indexOf("dcBias") >= 0 )
			    {
				BiasPanel.setNewStatus( ufs ); //status is for Bias Panel display
				return;
			    }
			else System.out.println(className+"::recvStatus> unknown UFStrings.name = "
						+ ufs.name() + ", value = " + ufs.valData(0));
		    }
		else statusToUpdate.setNewState( ufs.valData(0) );
	    }
	else {
	    String errmsg = className + "::recvStatus> recvd unexpected object: "
		+ ufp.description() + ", length=" + ufp.length() + ", name=" + ufp.name() + ".";
	    System.err.println( errmsg );
	    System.out.println( errmsg );
	    if( ufp.name().length() == 0 || ufp.length() <= 0 ) {
		if( ++nullCnt > 3 ) {
		    connectToAgent();
		    nullCnt = 0;
		}
		else try { Thread.sleep( 700 ); } catch( Exception x ) {}
	    }
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Inner class to monitor agent/server status messages in seperate thread
     */
    private class StatusMonitor extends Thread
    {
	private boolean keepRunning = false;
	private boolean didStart = false;
	private int sleepAmount = 1000;

	// Public constructor:

	public StatusMonitor( int sleepAmount ) {
	    this.sleepAmount = sleepAmount;
	    //if create and connect status socket to agent/server succeeds then start thread:
	    if( connectStatusSocket() ) {
		keepRunning = true;
		this.start();
		didStart = true;
	    }
	}

	// method to be run in seperate thread to recv status messages:

	public synchronized void run()
	{
	    while( true ) {
		while( keepRunning ) {
		    try {
			recvStatus();
		    }
		    catch (Exception e) {
			System.err.println(className+"::StatusMonitor.run> "+e.toString());
			try { this.sleep( sleepAmount ); } catch( Exception x ) {}
		    }
		}
		try { this.sleep( sleepAmount ); } catch( Exception x ) {}
	    }
	}

	public void stopRunning() { keepRunning = false; }
	public boolean isRunning() { return keepRunning;}
 
	public void resumeRunning() {
	    keepRunning = true;
	    if( !didStart ) {
		this.start();
		didStart = true;
	    }		
	}
   } // end of private class StatusMonitor.

} // end of UFobsMonitor class.
