//Title:        Extension of JAVA ComboBox class.
//Version:      2.0
//Copyright:    Copyright (c) Frank Varosi and David Rashkin
//Author:       Frank Varosi and David Rashkin, 2003
//Company:      University of Florida
//Description:  Combo box keeps track of selections and sets color based on prev/curr selection.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JComboBox;

//===============================================================================
/**
 * Combo box that sets its color depending on selection,
 * and can return either item or the index of item selected.
 * Note that requested list of items is always pre-pended with first item blank.
 */

public class UFComboBox extends JComboBox implements ItemListener
{
    public static final
	String rcsID = "$Name:  $ $Id: UFComboBox.java,v 1.18 2004/04/01 21:58:56 varosi Exp $";

    public static final int INDEX = 0;
    public static final int ITEM = 1;
    int item_OR_index = ITEM;

    String selected_item = "";
    String prev_item = "";
    String alarm_item = null;

//-------------------------------------------------------------------------------
    /**
     * Default Constructor
     */
    public UFComboBox()
    {
	try  {
	    jbInit( null );
	}
	catch(Exception x) { System.out.println("Error creating UFComboBox: " + x.toString());	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param items     Array of Strings: list of items to show in ComboBox
     */
    public UFComboBox( String[] items )
    {
	try  {
	    jbInit( items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboBox: " + x.toString());	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component creation -- returns no arguments
     *@param items          Array of Strings: list of items to show in ComboBox
     */
    private void jbInit( String[] items ) throws Exception
    {
	setMaximumRowCount(14);
       	addItemListener(this);
	setToolTipText();

	if (items != null) {
	    for( int i=0; i<items.length; i++ ) addItem( items[i] );
	}
    }
//-------------------------------------------------------------------------------
    /**
     *UFComboBox#setAlarmValue
     *@param alarmVal         String: item value for which red color should be indicated if selected.
     */
    public void setAlarmValue( String alarmVal ) { if( alarmVal != null ) alarm_item = alarmVal; }
//-------------------------------------------------------------------------------

    public void setIndexMethod() { item_OR_index = INDEX; }
    public void setItemMethod() { item_OR_index = ITEM; }

    public boolean indexMethod() { return( item_OR_index == INDEX ); }
    public String getSelection() { return selected_item; }

//-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ie ItemEvent
     */
    public void itemStateChanged(ItemEvent ie)
    {
      if( ie.getStateChange() == ItemEvent.SELECTED )
	  {
	      selected_item = _getSelectedItem();
	      setToolTipText();

	      if( selected_item.equals( prev_item ) )
		  setBackground( Color.white );
	      else
		  setBackground( Color.yellow );

	      if( alarm_item != null )
		  {
		      if( selected_item.equals( alarm_item ) ) setBackground( Color.red );
		  }
	  }
    }
//-------------------------------------------------------------------------------
    /**
     * Returns the String name of the selected item in the combo box,
     * with comments after blank removed, 
     * or, returns the integer index of the selected item in the combo box, as a string.
     */
    private String _getSelectedItem()
    {
	if( item_OR_index == ITEM )
	    {
		String item = (String)getSelectedItem();
		if (item == null) item = "";
		if( item.indexOf(" ") > 0 ) //anything after a blank is a comment so eliminate it:
		    item = item.substring( 0, item.indexOf(" ") );
		return item.trim();
	    }
	else return String.valueOf( getSelectedIndex() ).trim();
    }
//-------------------------------------------------------------------------------

    public void setNewState( String newValue )
    {
	if( selected_item.equals( newValue.trim() ) )
	    {
		setBackground( Color.white );
		prev_item = selected_item;
		setToolTipText();
	    }
	else setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------
    //Variation to first parse string for stuff after delimeter and then setNewState:

    public void setNewState( String statusString, String delimeter )
    {
	String[] words = statusString.split( delimeter );
	setNewState( words[words.length-1] );
    }
//-------------------------------------------------------------------------------

    void setToolTipText() {
	this.setToolTipText( "Desired=" + selected_item + "...Previous=" + prev_item );
    }
} //end of class UFComboBox
