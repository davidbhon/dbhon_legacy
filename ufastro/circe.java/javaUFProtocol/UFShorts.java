package javaUFProtocol;

import java.io.*;

public class UFShorts extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFShorts.java,v 1.8 2004/09/14 22:19:15 varosi Exp $";

    protected short[] _values=null;

    public UFShorts() {
	_type=MsgTyp._Shorts_;
    }

    public UFShorts(int length) {
	_length=length;
	_type=MsgTyp._Shorts_;
    }

    public UFShorts(String name, short[] vals) {
	_name = new String(name);
	_type=MsgTyp._Shorts_;
	_elem=vals.length;
	_values = vals;
	_length = _minLength() + 2*vals.length;
    }

    // all methods declared abstract by UFProtocal can be defined here

    public String description() { return new String("UFShorts"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 2;
    }

    public short[] values() { return _values; }
    public short valData(int index) { return _values[index]; }

    public short maxVal() {
	short max=-32768;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public short minVal() {
	short min=32767;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    protected void _copyNameAndVals(String s, short[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_values = new short[_elem];
	_length = _minLength();
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = vals[i];
	    _length += 2;
	}
    }

    public void setNameAndVals(String s, short[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_values = new short[vals.length];
	_length = _minLength();
	for (int i=0; i<_elem; i++) {
	    _values[i] = vals[i];
	    _length += 2;
	}
    }
 
    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	try {
	    byte[] byteStream = new byte[2*_elem];
	    inp.readFully( byteStream );

	    _values = new short[_elem];
	    short shift1 = 256;
	    int bi = 0;
	    //shift and mask each 2 byte group into a short integer:

	    for( int i=0; i<_elem; i++ ) {
		bi = i*2;
		Integer val = new Integer( ( (byteStream[bi]*shift1) & 0xff00 ) |
					   (  byteStream[bi+1]       & 0x00ff ) );
		_values[i] = val.shortValue();
	    }

	    return byteStream.length;
	}
	catch(EOFException eof) {
	    System.err.println("UFShorts::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFShorts::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFShorts::recvData> "+e.toString());
	}
	return 0;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	int retval=0;
	try {
	    if (_values != null && _values.length!= 0) {
		for (int i=0; i<_values.length; i++) {
		    out.writeShort(_values[i]);
		    retval += 2;
		}
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFShorts::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFShorts::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFShorts::sendData> "+e.toString());
	}
	return retval;
    }
}

