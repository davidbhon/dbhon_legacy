package javaUFProtocol;

public class UFFITSheader extends LinkedList {

    /**
     * Public class necessary for functions that require more than 1 return value
     * @see readPrmHdr(...), readExtHdr(...)
     */
    public class ReturnVals {
	int w,h,total,nnods,nnodsets,chops,savesets;
	ReturnVals() { w = h = total = nnods = nnodsets = chops = savesets = 0; }
	void clearVals() { w = h = total = nnods = nnodsets = 0; chops = savesets = 1; }
    }

    public static int _verbose = 0;

    protected String  _FITSrecord = "";

    public UFFITSheader() {
	_FITSrecord = "";
    }
    public UFFITSheader(String comment, boolean simple) {
	Simple(comment,simple);
    }
    public UFFITSheader(UFFrameConfig fc, String comment, boolean simple) {
	Simple(comment, simple);
	add("BITPIX",fc.depth, "Bits per pixel");
	add("NAXIS",2," ");
	add("NAXIS1", fc.width, "X dimension of array");
	add("NAXIS2", fc.height,"Y dimension of array");
	date();
    }
    public UFFITSheader(UFFrameConfig fc, UFObsConfig oc, String comment, boolean simple) {
	Simple(comment, simple);
	add("BITPIX", fc.depth,     "Bits per pixel");
	add("NAXIS" , 6,              " ");
	add("NAXIS1", fc.width,     "X dimension of array");
	add("NAXIS2", fc.height,    "Y dimension of array");
	add("NAXIS3", oc.chopBeams(), "Number of chop positions");
	add("NAXIS4", oc.saveSets(),  "Number of savesets per nod phase");
	add("NAXIS5", oc.nodBeams(),  "Number of nod positions");
	add("NAXIS6", oc.nodSets(),   "Number of nod cycles");
	date();
    }
    public int size() { return this.size(); }
    public String description() { return new String("UFFITSheader"/*__UFFITSheader_h__*/); }

    // methods to add FITS records (keyword = value / comment) to the FITS header:

    public int add(String keyword, String value, String comment) {
	String valq = "'" + value.substring(0,18) + "'";
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	rmJunk(key); rmJunk(valq); rmJunk(cmt);
	while (key.length() < 8) key += " ";
	while (valq.length() < 20) valq += " ";
	while (cmt.length() < 48) cmt += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + valq + " /" + cmt;
	addLast(_FITSrecord);
	return this.size();
    }
    public int add(String keyword, int value, String comment) {
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	rmJunk(key); rmJunk(cmt);
	String val = value + "";
	while (key.length() < 8) key += " ";
	while (cmt.length() < 48) cmt += " ";
	while (val.length() < 20) val += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + val + " /" + cmt;
	addLast(_FITSrecord);
	return this.size();
    }
    public int add(String keyword, double value, String comment) {
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	rmJunk(key); rmJunk(cmt);
	String val = value + "";
	while (key.length() < 8) key += " ";
	while (cmt.length() < 48) cmt += " ";
	while (val.length() < 20) val += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + val + " /" + cmt;
	addLast(_FITSrecord);
	return this.size();
    }

    public int replace(String keyword, String value, String comment) {
	String valq = "'" + value.substring(0,18) +"'";
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	rmJunk(key); rmJunk(valq); rmJunk(cmt);
	while (key.length() < 8) key += " ";
	while (cmt.length() < 48) cmt += " ";
	while (valq.length() < 20) valq += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + valq + " /" + cmt;
	return _replace(key);
    }
    public int replace(String keyword, int value, String comment) {
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	String val = value + "";
	rmJunk(key); rmJunk(cmt);
	while (key.length() < 8) key += " ";
	while (cmt.length() < 48) cmt += " ";
	while (val.length() < 20) val += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + val + " /" + cmt;
	return _replace(key);
    }
    public int replace(String keyword, double value, String comment) {
	String key = keyword.substring(0,8).toUpperCase();
	String cmt = comment.substring(0,48);
	String val = value + "";
	rmJunk(key); rmJunk(cmt);
	while (key.length() < 8) key += " ";
	while (cmt.length() < 48) cmt += " ";
	while (val.length() < 20) val += " ";
	_FITSrecord = "";
	_FITSrecord = key + "= " + val + " /" + cmt;
	return _replace(key);
    }

    public int add(Map valhash, Map comments) {
	if (valhash.isEmpty())
	    return this.size();
	Iterator i  = valhash.keySet().iterator();
	String key, val, comment;
	while (i.hasNext()) {
	    key = (String) i.next();
	    val = (String) valhash.get(key);
	    if (comments.containsKey(key))
		comment = (String) comments.get(key);
	    else
		comment = "no comment";
	    add(key, val, comment);
	}
	return this.size();
    }

    // assuming args are in FITS header entry format
    // trucation or extension to 80 chars may occur:
    public int add(String s) {
	if (s.indexOf("=") != 8) // clearly not a FITS header entry?
	    return this.size();
	
	if (s.length() > 80)
	    s = s.substring(0,80); // truncate just in case
	else while (s.length() < 80) s += " "; // or add spaces
	addLast(s);
	return this.size();
    }
    public int add(UFStrings ufs) {
	if (ufs == null)
	    return this.size();
	
	for (int i=0; i<ufs.numVals(); ++i)
	    add((String)ufs.valData(i));
	return this.size();
    }
    
    public int end() { // this adds the "END" record to header
	_FITSrecord = "END";
	while (_FITSrecord.length() < 80) _FITSrecord += " ";
	addLast(_FITSrecord);
	return unique();
    }
    public int unique() { // ensure each entry is unique, called by end()
	int cnt = this.size();
	if (cnt <= 1)
	    return cnt;
	//for now just insure that SIMPLE appears only once (at the front):
	String f1 = (String) this.get(0);
	String f2 = (String) this.get(1);
	String key1 = f1.substring(0,8);
	String key2 = f2.substring(0,8);
	if (key1.equals(key2))
	    removeFirst(); //should erase f1
	return this.size();
    }
    public int date(String comment) {
	SimpleDateFormat df = new SimpleDateFormat("yyyy:DDD:HH:mm:ss");
	String time = df.format(new Date());
	return add("DATE_FH", time, comment);
    }
    public int date() { return this.date("UT of header creation (YYYY:DAY:HH:MM:SS)"); }

    // convert UFFITSheader object into UFStrings object for client/server transactions:
    public UFStrings Strings(String name) {
	return new UFStrings(name,(String [])(this.toArray()));
    }
    public UFStrings Strings() { return this.Strings("FITSheader"); }

    // convenience function for use by UFDeviceConfig::statusFITS()
    public static UFStrings asStrings(Map valhash, Map comments) {
	UFFITSheader _fitshdr = new UFFITSheader();
	int sz = _fitshdr.add(valhash, comments);
	if (sz <= 0)
	    return null;
	return _fitshdr.Strings();
    }

    // convenience functions for client connection to an Agent to fetch FITS fragment
    public static UFStrings fetchFITS(String clientname, Socket soc) {
	if (soc == null)
	    return null;
	//formulate standard agent client key, request:
	String [] vs = { "STATUS", "FITS" };
	UFStrings req = new UFStrings(clientname, vs);
	req.sendTo(soc);
	UFStrings reply = (UFStrings) (UFProtocol.createFrom(soc));
	return reply;
    }

    public static String rmJunk(String st) {
	StringBuffer s = new StringBuffer(st);
	if( s.length() <= 0 ) {
	    System.err.println("UFFITSheader::rmJunk> ?empty string");
	    return "";
	}
	
	int pos = s.toString().indexOf("\n");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\n", 1+pos);
	}
	pos = s.toString().indexOf("\r");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\r", 1+pos);
	}
	
	// eliminate white (adjacent) multiples
	for( pos = 0; pos < s.length(); ++pos ) {
	    while ( (s.charAt(pos) == ' ' || s.charAt(pos) == '\t') && pos+1 < s.length()
		    && (s.charAt(pos+1) == ' ' || s.charAt(pos+1) == '\t'))
		s.deleteCharAt(pos);
	}
	return s.toString().trim(); // remove leading and trailing whitespace and return
    }
    
    // convenience functions for reading and parsing an MEF (muli-extension FITS file):
    // this parses an integer valued by key:
    public static int fitsInt(String key, String fitsHdrEntry) {
	String s = new String(fitsHdrEntry);
	int pos = s.indexOf(key);
	if (pos == -1)
	    return -1;
	pos = 1+s.indexOf("=",pos);
	if (pos == -1)
	    return -1;
	s = s.substring(1+pos);
	while (s.charAt(0) == ' ') s = s.substring(1); // eliminate leading white sp
	pos = s.indexOf(" ");
	if (pos == -1)
	    pos = s.indexOf("/");
	if (pos != -1) {
	    try {
		return Integer.parseInt(s.substring(0,pos));
	    } catch (NumberFormatException nfe) {
		System.err.println("UFFITSheader::fitsInt> Poorly formatted fits header???");
		return -1;
	    }
	}
	return -1;
    }

    //reads the primary header, skips to the next 2880 block,
    //and returns the entire header as a ufstrings obj:

    public static UFStrings readPrmHdr(FileReader fr, int total, ReturnVals rv){
	//create new rv for internal method storage if null
        if (rv == null) rv = (new UFFITSheader()).new ReturnVals();
	else rv.clearVals();
	char [] entry = new char[81]; Arrays.fill(entry,'\0');
	char []  end = new char[4]; Arrays.fill(end,'\0');
	end[0] = 'E'; end[1] = 'N'; end[2] = 'D';
	int ne = (new String("END")).length();
	char [] naxis = new char[7]; Arrays.fill(naxis,'\0');
	int na = (new String("NAXIS0")).length();
	char [] nods = new char[6]; Arrays.fill(nods,'\0');
	int nn = (new String("NNODS")).length();
	char [] nodsets = new char[9]; Arrays.fill(nodsets,'\0');
	int ns = (new String("NNODSETS")).length();
	String s, key, endkey;
	int fits_sz = 0, fitsunit = 80*36; // check for integral multiple of 2880
	int maxheadsz = 10*fitsunit; // if no END in sight...
	Vector vs = new Vector();
	do {
	    try {
		if ( fr.read(entry,0,80) != 80 ) {
		    System.err.println("UFFTISheader::readPrmFitsHdr> read of primary header failed.");
		    //if (x == -1)
		    //    System.err.println("UFFITSheader::readPrmFitsHdr> end of file.");
		    return null;
		}
	    } catch (IOException ioe) {
		System.err.println("UFFITSheader::readPrmFitsHdr> " + ioe.toString());
		return null;
	    }
	    fits_sz += 80;
	    vs.add(new String(entry));
	    if (_verbose != 0)
		System.out.println("UFFITSheader::readPrmFitsHdr> cnt: "+vs.size()+
				   ", entry: " + entry);
	    for (int i=0; i<na; i++) naxis[i] = entry[i];
	    key = new String(naxis);
	    if (key.equals("NAXIS1"))
		rv.w = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS2"))
		rv.h = fitsInt(key, new String(entry));

	    // nod info:
	    for (int i=0; i<ns; i++) nodsets[i] = entry[i];
	    key = new String(nodsets);
	    if (key.equals("NNODSETS")) 
		rv.nnodsets = fitsInt(key,new String(entry));
	    else {
		for (int i=0; i<nn; i++) nods[i] = entry[i];
		key = new String(nods);
		if (key.equals("NNODS"))
		    rv.nnods = fitsInt(key,new String(entry));
	    }
	    
	    // end:
	    for (int i=0; i<ne; i++) end[i] = entry[i];
	    endkey = new String(end);
	} while (!endkey.equals("END") && fits_sz < maxheadsz);
	total = rv.nnods + rv.nnodsets;
	if (_verbose != 0)
	    System.out.println("UFFITSheader::readPrmFitsHdr> char cnt: " + fits_sz+ 
			       ", total extensions/frames expected: " + total);
	if (fits_sz >= maxheadsz && !endkey.equals("END")) {
	    System.err.println("UFFITSheader::readPrmFitsHdr> No 'END' entry, improper or no FITS header? fits_sz= " + fits_sz + ", endKey= " + endkey);
	    return null;
	}
	// before returning header, may need to seek to end of current 2880 
	double padfrac = 1.0 - ( (1.0*fits_sz/fitsunit) - Math.floor(1.0*fits_sz/fitsunit) );
	int pad = (int) Math.ceil(padfrac * fitsunit);
	if (_verbose != 0)
	    System.out.println("UFFITSheader::readPrmFitsHdr> pad cnt: "+pad+
			       ", total header sz (+pad): " + (pad+fits_sz));
	try {
	    if (fr.skip(pad) != pad) 
		System.err.println("UFFITSheader::readPrmFitsHdr> Could not seek to next 2880 block");
	} catch (IOException ioe) {
	    System.err.println("UFFITSheader::readPrmFitsHdr> Could not seek to next 2880 block: "+
			       ioe.toString());
	}
	return new UFStrings("PrimaryHeader",(String [])(vs.toArray()));
    }
    ///this reads an extension header and the data block that follows it and skips to the next 2880 block:
    public static UFInts readExtData(DataInputStream dis, UFStrings exthdr, ReturnVals rv) {
	if (rv == null) rv = (new UFFITSheader()).new ReturnVals();
	else rv.clearVals();
	char [] entry = new char[81]; Arrays.fill(entry,'\0');
	char [] end   = new char[4];  Arrays.fill(end, '\0');
	end[0] = 'E'; end[1] = 'N'; end[2] = 'D';
	int ne = (new String("END")).length();
	char [] naxis = new char[7];  Arrays.fill(naxis,'\0');
	int na = (new String("NAXIS0")).length();
	String s = new String(); String key = new String(); String endkey = new String();
	int fits_sz = 0, fitsunit = 80*36; // check for integral multiple of 2880
	int maxexthdsz = 4 * fitsunit; // allow 4x2880 (4 block extension headers)?
	Vector vs = new Vector();
	do {
	    try {
		byte [] entryB = new byte[81]; for (int i=0; i<81; i++) entryB[i] = 0;
		if ( dis.read(entryB,0,80) != 80 ) {
		    System.err.println("UFFTISheader::readExtData> read of primary header failed.");
		    return null;
		}
		for (int i=0; i<81; i++) entry[i] = (char)entryB[i];
	    } catch (IOException ioe) {
		System.err.println("UFFITSheader::readExtData> " + ioe.toString());
		return null;
	    }
	    fits_sz += 80;
	    if (entry[0] == ' ' && entry[8] == ' ') 
		System.out.println("UFFITSheader::readExtData> blank line, char cnt: " + fits_sz);
	    else if (_verbose != 0)
		System.out.println("UFFITSheader::readExtData> char cnt: " + fits_sz + " -- " + entry);
	    vs.add(new String(entry));
	    for (int i=0; i<na; i++) naxis[i] = entry[i];
	    key = new String(naxis);
	    if (key.equals("NAXIS1")) rv.w = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS2")) rv.h = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS3")) rv.chops = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS4")) rv.savesets = fitsInt(key, new String(entry));
	    for (int i=0; i<ne; i++) end[i] = entry[i];
	    endkey = new String(end);
	} while ( !endkey.equals("END") && fits_sz < maxexthdsz);
	if (_verbose != 0)
	    System.out.println("UFFITSheader::readExtData> char cnt: " + fits_sz);
	if (fits_sz >= maxexthdsz && !endkey.equals("END")) {
	    System.out.println("UFFITSheader::readExtData> No 'END' entry, improper or "+
			       "no FITS header? fits_sz= " + fits_sz + ", endkey = " + endkey);
	    return null;
	}
	//before returning ext. header & data, may need to skip to end of current 2880 block:
	double foo = 1.0*fits_sz/fitsunit;
	double floo = Math.floor(foo);
	double padfrac = 1.0 - (foo - floo);
	int pad = (int) Math.ceil(padfrac * fitsunit);
	try {
	    if (dis.skip(pad) != pad) 
		System.err.println("UFFITSheader::readExtData> Could not seek to next 2880 block");
	} catch (IOException ioe) {
	    System.err.println("UFFITSheader::readExtData> Could not seek to next 2880 block: "+
			       ioe.toString());
	}
	int i=0,nvals = rv.w * rv.h * rv.chops * rv.savesets;
	int [] vals = new int[nvals];
	try { for (i=0; i<nvals; i++) vals[i] = dis.readInt(); }
	catch (IOException ioe) {
	    System.err.println("UFFITSheader::readExtData> read of image frame failed, nvalrd: " +i+
			       ", expected: " + nvals);
	    return null;
	}
	fits_sz = i * 4; // 4 bytes per integer
	foo = 1.0*fits_sz/fitsunit;
	floo = Math.floor(foo);
	padfrac = 1.0 - (foo - floo);
	if (padfrac < 0.999999) { // no need to pad if this is 1.0
	    pad = (int) Math.ceil(padfrac * fitsunit);
	    try { dis.skip(pad);}
	    catch (IOException ioe) { System.err.println("UFFITSheader::readExtData> "+ioe.toString());}
	}
	exthdr.setNameAndVals("ExtHeader",(String []) (vs.toArray()));
	UFInts ufi = new UFInts();
	ufi.setNameAndVals("Frame",vals);
	return ufi;
    }

    ///helper for ctors: adds one standard rec containing SIMPLE keyword
    protected int Simple (String comment, boolean simple) {
	String cmt = comment.substring(0,48);
	cmt = rmJunk(cmt);
	while (cmt.length() < 48) cmt += " ";
	_FITSrecord = "SIMPLE  =                    ";
	if (simple)
	    _FITSrecord += "T /" + cmt;
	else
	    _FITSrecord += "F /" + cmt;
	addFirst(new String(_FITSrecord));
	return this.size();
    }
    ///helper for replace funcs
    protected int _replace(String key) {
	int cnt = this.size();
	boolean replaced = false;
	for (int i=0; i<cnt && !replaced; ++i) {
	    String entry = (String) this.get(i);
	    if (entry.indexOf(key) == 0) {
		remove(i);
		add(i,_FITSrecord);
		replaced = true;
	    }
	}
	if (!replaced) // then just add it?
	    addLast(_FITSrecord);
	return this.size();
    }
}
