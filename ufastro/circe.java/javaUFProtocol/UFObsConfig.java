package javaUFProtocol;

import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

public class UFObsConfig extends UFShorts {

    //should be allocated in constructor, not here.
    protected static Vector _frameBufnames = new Vector();
    protected static Hashtable _frameBufs = new Hashtable();

    // Bit mask code for defining/extracting buffer sequencing information:

    protected static class BitMaskCode {
	static final int BMC_RAW        =    3;
	static final int BMC_CLR        =    4;
	static final int BMC_D1         =    8;
	static final int BMC_D2         =   16;
	static final int BMC_D1A        =   32;
	static final int BMC_D2A        =   64;
	static final int BMC_SG         =  128;
	static final int BMC_NOD_BEAM   =  256;
	static final int BMC_CHOP_BEAM  =  512;
	static final int BMC_NOD_TOGGLE = 1024;	
    }

    public class Config { 
	int NodBeams; int ChopBeams; int SaveSets; int NodSets; int CoaddsPerFrm;
	String ReadoutMode = new String();
	String NodPattern = new String();
    }

    public UFObsConfig() { 
	_length=_minLength();
	_type=MsgTyp._ObsConfig_;
	short [] x = new short[2*2*3*2];
	short i=0;
	while (i < 2*2*3*2) x[i] = i++;
	_init(2,2,3,2,9,x,false);
	_currentTime();
    }

    public UFObsConfig(int length) {
	super(length);
	_length=length;
	_type=MsgTyp._ObsConfig_;
	_currentTime();	
    }

    public UFObsConfig(int nb, int cb, int ss, int ns, int cpf, short [] vals) {
	_length=_minLength();
	_type=MsgTyp._ObsConfig_;
	_init(nb,cb,ss,ns,cpf,vals,/*shallow=*/false);
	_currentTime();
    }

    public UFObsConfig(String name, int nb, int cb, int ss, int ns, int cpf, short[] vals) {
	_type=MsgTyp._ObsConfig_;
	_name = new String(name);
	_length=_minLength();
	_init(nb,cb,ss,ns,cpf,vals,/*shallow=*/false);
	_currentTime();
    }

    //UFShorts ctor makes use of shallow initializations
    public UFObsConfig(UFShorts ufs, int nb, int cb, int ss, int ns, int cpf) {
	_type=MsgTyp._ObsConfig_;
	_timestamp = new String(ufs._timestamp);
	_elem=ufs._elem;
	_name = new String(ufs.name());
	_length=_minLength() + _elem*2;
	_seqCnt = ufs._seqCnt;
	_seqTot = ufs._seqTot;
	_duration = ufs._duration;
	_init(nb,cb,ss,ns,cpf,ufs._values,/*shallow=*/true);
    }

    //UFShorts ctor makes use of shallow initializations
    public UFObsConfig (UFShorts ufs) {
	_type=MsgTyp._ObsConfig_;
	_timestamp = new String(ufs._timestamp);
	_elem=ufs._elem;
	_name = new String(ufs.name());
	_length=_minLength() + _elem*2;
	_seqCnt = ufs._seqCnt;
	_seqTot = ufs._seqTot;
	_duration = ufs._duration;
	_values = ufs._values;
    }

    public UFObsConfig(UFObsConfig ufoc) {
	this(ufoc,false);
    }
    public UFObsConfig(UFObsConfig ufoc, boolean shallow) {
	_type=MsgTyp._ObsConfig_;
	_timestamp = new String(ufoc._timestamp);
	_elem=ufoc._elem; // should be increment to >= 1 
	_name = new String(ufoc.name());
	_length=_minLength() + _elem*2;
	_seqCnt = ufoc._seqCnt;
	_seqTot = ufoc._seqTot;
	_duration = ufoc._duration;
	_values = ufoc._values;
	_init(ufoc.nodBeams(),ufoc.chopBeams(),ufoc.saveSets(),ufoc.nodSets(),
	      ufoc.coaddsPerFrm(),ufoc._values,shallow);
    }

    public static void beams( String obsmode, int[] nb, int[] nc) {
	nb[0] = nc[0] = 1; // default is Stare mode
	if ( (obsmode.indexOf("chop") != -1 || obsmode.indexOf("Chop") != -1 ||
	      obsmode.indexOf("CHOP") != -1) &&
	     (obsmode.indexOf("nod")  != -1 || obsmode.indexOf("Nod")  != -1 ||
	      obsmode.indexOf("NOD")  != -1) ) { // Chop-Nod mode
	    nb[0] = nc[0] = 2;
	    return;
	}
	if( obsmode.indexOf("chop") != -1 ||
	    obsmode.indexOf("Chop") != -1 || 
	    obsmode.indexOf("CHOP") != -1) { // Chop-Only mode
	    nb[0] = 1; nc[0] = 2;
	    return;
	}
	if ( obsmode.indexOf("nod") != -1 ||
	     obsmode.indexOf("Nod") != -1 ||
	     obsmode.indexOf("NOD") != -1) { // Nod-Only mode
	    nb[0] = 2; nc[0] = 1;
	    return;
	}  
	//any other obsmode string reduces to Stare mode...
	return;
    }

    // estimated MCE4 EDT fiber transfer time in seconds (depends on readout mode)
    public float estXferTime() {
	int nfrmPerRdout = 1; // always at least one signal det. on sample.
	String readOutMode = readoutMode();
	if( readOutMode.indexOf("S1R1") != -1 ||
	    readOutMode.indexOf("1S1R") != -1 ||
	    readOutMode.indexOf("s1r1") != -1 ||
	    readOutMode.indexOf("1s1r") != -1 ) nfrmPerRdout = 2; //one ref sample.
	if( readOutMode.indexOf("S1R3") != -1 ||
	    readOutMode.indexOf("1S3R") != -1 ||
	    readOutMode.indexOf("s1r3") != -1 ||
	    readOutMode.indexOf("1s3r") != -1 ) nfrmPerRdout = 4; //three ref samples.
	return (float)(0.04 * nfrmPerRdout);
    }

    // convenience func. for evaluation of all TReCS image buffers
    public static int initFrameBufs(){
	_frameBufnames.clear();
	_frameBufs.clear();
	int [] trecsimg = new int[320*240];
	//insure a zero's initial buffer:
	for (int i=0; i<320*240; i++) trecsimg[i]=0;

	//these use buf from socket:
	_frameBufnames.add("src1"); //0 == buffId(FrameCount) -1
	//_frameBufs.put("src1",null); // illegal -- cannot put null value in a hashtable
	                               // trying a get with this key will still return null,
	                               // so this will still behave as expected 
	_frameBufnames.add("ref1"); //1 == buffId(FrameCount) -1
	//_frameBufs.put("ref1",null);
	_frameBufnames.add("src2"); //2 == buffId(FrameCount) -1
	//_frameBufs.put("src2",null);
	_frameBufnames.add("ref2"); //3 == buffId(FrameCount) -1
	//_frameBufs.put("ref2",null);

	String bufnam = "accum(src1)";
	_frameBufnames.add(bufnam); // 4
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(ref1)";
	_frameBufnames.add(bufnam); // 5
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(src2)";
	_frameBufnames.add(bufnam); // 6
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(ref2)";
	_frameBufnames.add(bufnam); // 7
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "dif1";
	_frameBufnames.add(bufnam); // 8
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "dif2";
	_frameBufnames.add(bufnam); // 9
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(dif1)";
	_frameBufnames.add(bufnam); // 10
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(dif2)";
	_frameBufnames.add(bufnam); // 11
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "sig";
	_frameBufnames.add(bufnam); // 12
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));
        bufnam = "accum(sig)";
	_frameBufnames.add(bufnam); // 13
	_frameBufs.put(bufnam,new UFInts(bufnam, trecsimg));

	return (int)_frameBufnames.size();
    }

    public int evalFrame(int frmIdx, UFInts frame, Hashtable bufs, boolean freeFrm){
	if (_frameBufnames.isEmpty())
	    initFrameBufs();
	bufs.clear();
	if (frame == null)
	    return 0;
	int nodpos = nodPosition(frmIdx);
	int chopos = chopPosition(frmIdx);
	String timeframe = new String(frame._timestamp);
	if (nodpos == 0) { // even nod phase
	    if (chopos == 0) { // even chop phase for event nod phase is on source
		if(freeFrm) _frameBufs.remove("src1");
		bufs.put("trecs:src1",frame);
		_frameBufs.put("src1",frame);
		UFInts as1 = (UFInts) _frameBufs.get("accum(src1)");
		as1._timestamp = new String(timeframe);
		as1.plusEquals(frame);
		bufs.put("trecs:accum(src1)",as1);
                _frameBufs.put("accum(src1)",as1);
	    }
	    else {
		if (freeFrm) _frameBufs.remove("ref1");
		bufs.put("trecs:ref1",frame);
		_frameBufs.put("ref1",frame);
		UFInts ar1 = (UFInts) _frameBufs.get("accum(ref1)");
		ar1._timestamp = new String(timeframe);
		ar1.plusEquals(frame);
		bufs.put("trecs:accum(ref1)",ar1);
		_frameBufs.put("accum(ref1)",ar1);
	    }
	}
	else { // odd nod phase
	    if (chopos != 0) { // odd chop phase for odd nod phase is on source
		if (freeFrm) _frameBufs.remove("src2");
		bufs.put("trecs:src2",frame);
		_frameBufs.put("src2",frame);
		UFInts as2 = (UFInts) _frameBufs.get("accum(src2)");
		as2._timestamp = new String(timeframe);
		as2.plusEquals(frame);
		bufs.put("trecs:accum(src2)",as2);
                _frameBufs.put("accum(src2)",as2);
	    }
	    else {
		if (freeFrm) _frameBufs.remove("ref2");
		bufs.put("trecs:ref2",frame);
		_frameBufs.put("ref2",frame);
		UFInts ar2 = (UFInts) _frameBufs.get("accum(ref2)");
		ar2._timestamp = new String(timeframe);
		ar2.plusEquals(frame);
		bufs.put("trecs:accum(ref2)",ar2);
		_frameBufs.put("accum(ref2)",ar2);
	    }
	}
	if (doDiff1(frmIdx)) {
	    UFInts s1 = (UFInts) _frameBufs.get("src1");
	    UFInts r1 = (UFInts) _frameBufs.get("ref1");
	    UFInts d1 = (UFInts) _frameBufs.get("dif1");
	    d1.diff(s1,r1);
	    d1._timestamp = new String(timeframe);
	    bufs.put("trecs:dif1",d1);
	    _frameBufs.put("dif1",d1);
	    if (doAccumDiff1(frmIdx)) {
		UFInts ad1 = (UFInts) _frameBufs.get("accum(dif1)");
		ad1.plusEquals(d1);
		ad1._timestamp = new String(timeframe);
		bufs.put("trecs:accum(dif1)",ad1);
		_frameBufs.put("accum(dif1)",ad1);
	    }
	}
	if (doDiff2(frmIdx)) {
	    UFInts s2 = (UFInts) _frameBufs.get("src2");
	    UFInts r2 = (UFInts) _frameBufs.get("ref2");
	    UFInts d2 = (UFInts) _frameBufs.get("dif2");
	    d2.diff(s2,r2);
	    d2._timestamp = new String(timeframe);
	    bufs.put("trecs:dif2",d2);
	    _frameBufs.put("dif2",d2);
	    if (doAccumDiff2(frmIdx)) {
		UFInts ad2 = (UFInts) _frameBufs.get("accum(dif2)");
		ad2.plusEquals(d2);
		ad2._timestamp = new String(timeframe);
		bufs.put("trecs:accum(dif2)",ad2);
		_frameBufs.put("accum(dif2)",ad2);
	    }
	}
	if(doSig(frmIdx)) {
	    UFInts d1 = (UFInts) _frameBufs.get("dif1");
	    UFInts d2 = (UFInts) _frameBufs.get("dif2");
	    UFInts sig = (UFInts) _frameBufs.get("sig");
	    UFInts asig = (UFInts) _frameBufs.get("accum(sig)");
	    sig.sum(d1,d2);
	    sig._timestamp = new String(timeframe);
	    asig.plusEquals(sig);
	    asig._timestamp = new String(timeframe);
	    bufs.put("trecs:sig",sig);
	    _frameBufs.put("sig",sig);
	    bufs.put("trecs:accum(sig)",asig);
	    _frameBufs.put("accum(sig)",asig);
	}
	return (int) bufs.size();
    }

    public int evalFrame(int frmIdx, UFInts frame, Hashtable bufs) {
	return evalFrame(frmIdx,frame,bufs,true);
    }

    // get all buffer names:
    public static int getBufNames( String instrum, Vector names) {
	names.clear();
	int i=0, cnt = (int) _frameBufnames.size();
	if (cnt <= 0) // need to init the buf names...
	    cnt = initFrameBufs();
	for ( ; i < cnt ; ++i) {
	    String bufnam = instrum + ':' + _frameBufnames.get(i);
	    names.add(bufnam);
	}
	return i;
    }
    public int getBufNames(Vector names) {
	names.clear();
	int i=0, cnt = (int)_frameBufnames.size();
	if (cnt <=0) // need to init the buf names...
	    cnt = initFrameBufs();
	for( ; i<cnt; i++)
	    names.add(_frameBufnames.get(i));
	return i;
    }
    // get buffer by name:
    public UFInts getBuf( String name) {
	return (UFInts) _frameBufs.get(name);
    }

    //note: the default cpf = 9 is the minimum coadds per frame that MCE will do.
    protected  void _init(int nb, int cb, int ss, int ns, int cpf, short[] vals, boolean shallow) {
	_elem = nb*cb*ss*ns;
	String nm = _initname(); // bogus name
	Config ufc = new Config();
	ufc.NodBeams = nb;
	ufc.ChopBeams = cb;
	ufc.SaveSets = ss;
	ufc.NodSets = ns;
	ufc.CoaddsPerFrm = cpf;
	ufc.ReadoutMode = "S1";

	String cs = setConfig(ufc); // this resets the name
	if (!shallow)
	    _values = null;
	_initVals(vals, _elem, shallow); // this better not reset the name!
	_length = _minLength() + _elem * 2;  // sizeof(short) = 2
    }

    protected void _initVals( short[] vals, int elem, boolean shallow){
	_elem = elem;
	if (vals != null) 
	    if (shallow)
		_values = vals;
	    else {
		_values = new short[vals.length];
		for(int i=0; i<vals.length; i++)
		    _values[i] = vals[i];  
	    }
	else if (_values == null) 
	    _initBufSeqFlags();
    }

    protected void _initVals() {
	_initVals(null,1,false);
    }

    //Compute vector of buffer sequencing flags (the nominal values of this object):
    protected void _initBufSeqFlags(){
      short [] pBufSeq = new short[_elem];
      _values = pBufSeq;
      Config obscon = config();
      int totFrameNodBeam = obscon.ChopBeams * obscon.SaveSets;
      int totFrameNodSet = obscon.NodBeams * totFrameNodBeam;

      for(int FrameIndex=0; FrameIndex < _elem; FrameIndex++) {
	  int FrameCount = 1+ FrameIndex;
	  int SaveSet = 1 + ((FrameIndex / obscon.ChopBeams) % obscon.SaveSets);
	  short NodBeam = (short)( (FrameIndex/totFrameNodBeam) % obscon.NodBeams  );
	  short ChpBeam = (short)( FrameIndex % obscon.ChopBeams  );

	  //Initialize buffer sequence vector with buffer index:
	  pBufSeq[FrameIndex] = (short)((NodBeam * obscon.ChopBeams) +
					(FrameIndex % obscon.ChopBeams));
	  //Zero bit registers:
	  short BitClr = 0;
	  short BitD1  = 0;
	  short BitD2  = 0;
	  short BitD1a = 0;
	  short BitD2a = 0;
	  short BitSg  = 0;
	  short BitNB  = 0;
	  short BitCB  = 0;
	  short BitNT  = 0;

	  if (SaveSet == 1) BitClr = BitMaskCode.BMC_CLR;
	  // Compute Dif1 or Dif2? 
	  if ( (obscon.ChopBeams == 2) && ((FrameCount % 2) == 0) ) {
	      if (NodBeam == 0) BitD1 = BitMaskCode.BMC_D1;
	      if (NodBeam == 1) BitD2 = BitMaskCode.BMC_D2;
	  }
	  
	  // Compute D1A ?
	  if (obscon.ChopBeams == 2) {
	      if (BitD1 != 0) BitD1a = BitMaskCode.BMC_D1A;
	  }
	  else if (obscon.NodBeams == 2) {
	      if (FrameCount % (2*obscon.SaveSets) == 0) BitD1a = BitMaskCode.BMC_D1A;
	  }

	  if (obscon.ChopBeams == 2) { // Compute D2A?
	      if (BitD2 != 0) BitD2a = BitMaskCode.BMC_D2A;
	  }
	  
	  if (obscon.NodBeams == 2) { // Compute SG?
	      if ((FrameCount % totFrameNodSet) == 0) BitSg = BitMaskCode.BMC_SG;
	  }
	  
	  BitNB = (short)(NodBeam*BitMaskCode.BMC_NOD_BEAM); // Indicate nod beam index
	  
	  BitCB = (short)(ChpBeam*BitMaskCode.BMC_CHOP_BEAM); // Indicate chop beam index
	  
	  if (obscon.NodBeams == 2) {  // Set nod toggle?
	      if ((FrameCount % totFrameNodBeam) == 0) BitNT = BitMaskCode.BMC_NOD_TOGGLE;
	  }

	  pBufSeq[FrameIndex] += (BitClr+BitD1+BitD2+BitD1a+BitD2a+BitSg+BitNB+BitCB+BitNT);


      }
    }

  // fetch config substr
  protected  String _config() {
      String c = new String(),nm = name();
      int posL = nm.indexOf("||");
      int posR = nm.lastIndexOf("||");
      if (posL == posR || posL == -1 || posR == -1) // poorly initialized object?
	  c = "||?|?|?|?|?|?||";
      else
	  c = nm.substring(posL,posR+1);
      return c;
  }

  // fetch name substr
  protected  String _name() {
      String nm = name();
      int posR = nm.lastIndexOf("||");
      if (posR != -1 && posR+2 < nm.length()) 
	  return nm.substring(posR+2);
      else 
	  return "?";
  }

  // insure _name is properly formatted
  protected  String _initname(){
      String c, nm = name();
      int posL = nm.indexOf("|");
      if (posL != 0) { // poorly initialized object?
	  c = "||?|?|?|?|?|?||";
	  rename(c+nm);
      }
      return name();
  }

  // override the inherited UFShorts description:
  public String description() { return new String("UFObsConfig"); }

  // convenience functions:
  
  public int totFrameCnt() { return _elem; }
  
  public Config config() {
      String c = _config();
      Config ufc = new Config();
      int posL = 2 + c.indexOf("|");
      int posR = c.indexOf("|",posL);
      String NodBeams = c.substring(posL, posR);
      posL = 1 + c.indexOf("|",posR); posR = c.indexOf("|",posL);
      String ChopBeams = c.substring(posL, posR);
      posL = 1 + c.indexOf("|",posR); posR = c.indexOf("|",posL);
      String SaveSets = c.substring(posL, posR);
      posL = 1 + c.indexOf("|",posR); posR = c.indexOf("|",posL);
      String NodSets = c.substring(posL, posR);
      posL = 1 + c.indexOf("|",posR); posR = c.indexOf("|",posL);
      String CoaddsPerFrm = c.substring(posL, posR);
      posL = 1 + c.indexOf("|",posR); posR = c.indexOf("|",posL);
      ufc.ReadoutMode = c.substring(posL, posR);
      ufc.NodBeams = Integer.parseInt(NodBeams);
      ufc.ChopBeams = Integer.parseInt(ChopBeams);
      ufc.SaveSets = Integer.parseInt(SaveSets);
      ufc.NodSets = Integer.parseInt(NodSets);
      ufc.CoaddsPerFrm = Integer.parseInt(CoaddsPerFrm);
      return ufc;
  }

  public String setConfig( Config c){
      setNodBeams(c.NodBeams);
      setChopBeams(c.ChopBeams);
      setSaveSets(c.SaveSets);
      setNodSets(c.NodSets);
      setCoaddsPerFrm(c.CoaddsPerFrm);
      return name();
  }

  public String setConfig( String cs){
      // cs must be of the form: "||[0-9]*|[0-9]*|[0-9]*|[0-9]*|[0-9]*|string||"
      // so count # of | (>=9) for simple verification
      int cnt,pos = cs.indexOf("|");
      if (pos == -1)
	  return "";
      for (cnt = 1; pos != -1; ++cnt)
	  pos = cs.indexOf("|", ++pos);

      String nm = _name();
      String c = "||?|?|?|?|?|?||";
      if (cnt < 9)
	  rename(c+nm);
      else
	  rename(cs+nm);
      return name();
  }

  public int nodBeams() {
    return config().NodBeams;
  }
  public int chopBeams() {
    return config().ChopBeams;
  }
  public int saveSets() {
    return config().SaveSets;
  }
  public int nodSets() {
    return config().NodSets;
  }
  public int coaddsPerFrm() {
    return config().CoaddsPerFrm;
  }
  public String readoutMode() {
    return config().ReadoutMode;
  }
  public String dataLabel() {
      String nm = name();
      int posR = nm.lastIndexOf("||");
      if (posR != -1 && posR+2 < nm.length())
	  return nm.substring(posR+2);
      else
	  return ("?");
  }

  public int setNodBeams(int nb){
      String c = _config();
      int pos = 2 + c.indexOf("||");
      pos = c.indexOf("|",pos);
      // discard the current NodBeams substring
      String s = new String();
      s = "||" + nb + c.substring(pos) + _name();
      rename(s);
      return name().length();
  }
  public int setChopBeams(int cb){
      String c = _config();
      int pos = 2 + c.indexOf("||");
      pos = 1 + c.indexOf("|",pos);
      // first preserve current ChopBeams substring
      String s = new String();
      s = c.substring(0,pos) + cb;
      // + remainder
      pos = c.indexOf("|",pos);
      s += c.substring(pos) + _name();
      rename(s);
      return name().length();
  }
  public int setSaveSets(int ss){
      String c = _config();
      // discard the current SaveSets subtring
      // first preserve current NodBeams & ChopBeams substring
      // NodBeams
      int pos = 2 + c.indexOf("||");
      pos = 1 + c.indexOf("|",pos);
      // ChopBeams
      pos = 1 + c.indexOf("|",pos);
      String tmp = c.substring(0,pos); 
      tmp += (ss+"");
      // + remainder
      pos = c.indexOf("|",pos);
      tmp += c.substring(pos);
      tmp += _name();
      rename(tmp);
      return name().length();
  }
  public int setNodSets(int ns){
      String c = _config();
      // discard the current NodSets subtring
      // first preserve current NodBeams & ChopBeams subtring
      // NodBeams
      int pos = 2 + c.indexOf("||");
      pos = 1 + c.indexOf("|",pos);
      // ChopBeams
      pos = 1 + c.indexOf("|",pos);
      // SaveSets
      pos = 1 + c.indexOf("|",pos);
      String tmp = c.substring(0,pos);
      tmp += (ns + "");
      pos = c.indexOf("|",pos);
      tmp += c.substring(pos);
      tmp += _name();
      rename (tmp);
      return name().length();
  }
  public int setCoaddsPerFrm(int cpf){
      String c = _config();
      // NodBeams
      int pos = 2 + c.indexOf("||");
      pos = 1 + c.indexOf("|",pos);
      // ChopBeams
      pos = 1 + c.indexOf("|",pos);
      // SaveSets
      pos = 1 + c.indexOf("|",pos);
      // NodSets
      pos = 1 + c.indexOf("|",pos);
      String tmp = c.substring(0,pos);
      tmp += (cpf+"");
      // + remainder
      pos = c.indexOf("|",pos);
      tmp += c.substring(pos);
      tmp += _name();
      rename(tmp);
      return name().length();
  }
  public int setReadoutMode( String rdout){
      String c = _config();
      //skip NodBeams, ChopBeams, SaveSets, NodSets, and CoaddsPerFrm
      int pos = 2 + c.indexOf("||");
      for (int i=0; i<5; i++) pos = 1 + c.indexOf("|",pos);
      String tmp = c.substring(0,pos);

      //set ReadoutMode + remainder
      rename(tmp + rdout + "||" + _name() );
      return name().length();
  }
  public int reLabel( String newLabel){
      rename(_config() + newLabel);
      return name().length();
  }

  // evaluate/set bits:
  public int buffId(int frmIdx) {
      return (1 + ((short) (0x03 & _values[frmIdx])));
  }
  public boolean clearBuff(int frmIdx) {
      return (((short)0x04 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doDiff1(int frmIdx) {
      return (((short)0x08 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doDiff2(int frmIdx) {
      return (((short)0x10 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doAccumDiff1(int frmIdx) {
      return (((short)0x20 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doAccumDiff2(int frmIdx) {
      return (((short)0x40 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doSig(int frmIdx) {
      return (((short)0x80 & _values[frmIdx]) == 0 ? false : true);
  }
  public int nodPosition(int frmIdx) {
      return (((short)0x100 & _values[frmIdx]) == 0 ? 0 : 1);
  }
  public int chopPosition(int frmIdx) {
      return (((short)0x200 & _values[frmIdx]) == 0 ? 0 : 1);
  }

  public int setBuffId(int id, int frmIdx){
      short idb = (short)(id-1); // assumes id > 0 !
      short bufId = (short)(0x03 & idb);
      _values[frmIdx] = (short) (bufId & _values[frmIdx]);
      return id;
  }
  //NOTE -- java booleans are 1 bit, c booleans are 32 bits
  public boolean setClear(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x08|0x10|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x04;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setDiff1(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x10|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x08;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setDiff2(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x10;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setAccumDiff1(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x20;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setAccumDiff2(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x80|0x100|0x200);
      short sb1 = (short) 0x40;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setSig(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x100|0x200);
      short sb1 = (short) 0x80;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public int setNodPosition(int pos, int frmIdx) {
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x200);
      short sb1 = (short) 0x100;
      if (pos!=0)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return pos;
  }
  public int setChopPosition(int pos, int frmIdx) {
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x100);
      short sb1 = (short) 0x200;
      if (pos!=0)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return pos;
  }
}
    
    
