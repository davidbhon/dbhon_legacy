;Motor Parameter File
; $Name:  $ $Id: motor_param.txt 14 2008-06-11 01:49:45Z hon $
;Number of Motors
5
;
;#  : Motor Number
;IS : Initial Speed
;TS : Terminal Speed
;A  : Acceleration
;D  : Deceleration
;HC : Hold Current
;DC : Drive Current
;AN : Axis Name (one character)
;DS : Datum Speed
;FDS: Final Datum Speed
;DD : Datum Direction 
;BL : Back-Lash correction param.
;Name : Motor name
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name
 40  80  10  10  0   5 A  80  14  0   10  Slit Wheel
POSITIONS
;#_Positions for Slit Wheel
 9 
;# Offset  Throughput Name (Comment)
 1 100     1.00      Open
 2 200     1.00      Half-size
 3 300     0.60      Slit1
 4 400     1.00      Slit2
 5 500     0.60      Slit3
 6 600     1.00      Slit4
 7 700     1.00      Slit5
 8   0     1.00      Datum
 9 900     1.00      Park
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0   5 C  80  14  0   10  Filter Wheel
POSITIONS
;#_Positions for Filter Wheel
 9 
;# Offset  Throughput  Name (Comment)
 1 100     1.00        Open
 2 200     1.00        J
 3 300     1.00        H
 4 400     1.00        K
 5 500     1.00        Ks
 6 600     1.00        TBD
 7 700     1.00        TBD
 8   0     1.00        Datum
 9 900     1.00        Park
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0   5 E  80  14  0   10  HWP Wheel
POSITIONS
;#_Positions for Half-wave Plate Wheel
 9
;# Offset  Throughput  Name (Comment)
 1 100     1.00        Open
 2 200     1.00        HWP
 3 300     0.95        TBD
 4 400     0.77        TBD
 5 500     0.95        TBD
 6 600     1.00        TBD
 7 700     1.00        TBD
 8   0     1.00        Datum
 9 900     1.00        Park
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0   5 H  80  14  0   10  HWP-Angle
POSITIONS
;#_Positions for Half-wave Plate Angle
 6 
;# Offset  Throughput  Name (Comment)
 1 100     1.00        00.0
 2 200     1.00        22.5
 3 300     1.00        45.0
 4 400     1.00        67.5
 5   0     1.00        Datum
 6 600     1.00        Park
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0   5 I  80  14  0   10  Grism Wheel
POSITIONS
;#_Positions for Filter Wheel 2 
 9
;# Offset  Throughput  Name (Comment)
 1  100    1.00        Open
 2  200    0.85        Closed
 3  300    0.97        JH
 4  400    0.94        HK
 5  500    1.00        JHK
 6  600    1.00        TBD
 7  700    1.00        TBD
 8    0    1.00        Datum
 9  900    1.00        Park
END_REC
;
