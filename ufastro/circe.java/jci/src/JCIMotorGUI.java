//Title:        JCIMotorGUI
//Version:      2.0
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Extend UFComboxPanel class to specific JCIMotor class.

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javaUFLib.*;

/**
 * Extend UFComboPanel class with embedded JCIMotor object(s), using motor positions as the items,
 * setting the motor object to use ComboPanel for display of position, etc.
 */

public class JCIMotorGUI extends UFComboPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JCIMotorGUI.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";

    protected JCIMotor Motor = null;
    protected JCIMotor Motor1 = null;
    protected JCIMotor Motor2 = null;
    protected Vector M1posNams = new Vector(9);
    protected Vector M2posNams = new Vector(9);

    String Motor1pos = "";
    String Motor2pos = "";

    //for the case of two wheels the default tandem is Open,
    //and the wheel that does not have desired position on it will move to tandemPos.
    protected String defTandemPos = "Open";
    protected String tandemPos = defTandemPos;
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param motor    JCIMotor object
     */
    public JCIMotorGUI( JCIMotor motor )
    {
	this( motor.name, motor );
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description  String: Information regarding the field
     *@param motor        JCIMotor object
     */
    public JCIMotorGUI( String description, JCIMotor motor )
    {
	super( description, motor.getPositionNames() );
	this.Motor = motor;
	Motor.setMasterGUI( this );
	checkStatus();
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description   String: Information regarding the field
     *@param checkBoxState boolean: if present Label is JCheckBox with initial check state specified.
     *@param motor         JCIMotor object
     */
    public JCIMotorGUI( String description, boolean checkBoxState, JCIMotor motor )
    {
	super( description, checkBoxState, motor.getPositionNames() );
	this.Motor = motor;
	Motor.setMasterGUI( this );
	checkStatus();
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor for special case of 2 physical motors controlled logically as one wheel.
     *@param description  String: Information regarding the field
     *@param motor1       JCIMotor object
     *@param motor2       JCIMotor object to be logically merged with motor1.
     */
    public JCIMotorGUI( String description, JCIMotor motor1, JCIMotor motor2 )
    {
	super( description, motor1.sortedPositionNames(), motor2.sortedPositionNames() );
	this.Motor1 = motor1;
	this.Motor2 = motor2;
	Motor1.setMasterGUI( this );
	Motor2.setMasterGUI( this );
	checkStatus();

	String[] posNams = Motor1.getPositionNames();
	for( int i=0; i<posNams.length; i++ ) M1posNams.add( posNams[i] );
	posNams = Motor2.getPositionNames();
	for( int i=0; i<posNams.length; i++ ) M2posNams.add( posNams[i] );
    }
//-------------------------------------------------------------------------------------

    public void moveToPosition()
    {
	if( Motor != null ) {
	    Motor.namedLocationBox.setSelectedItem( this.getSelectedItem() );
	    Motor.doHiMoveThread();
	}
	else if( Motor1 != null && Motor2 != null ) {
	    String desPosit = this.getSelectedItem();
	    if( M1posNams.contains( desPosit ) ) {
		Motor1.namedLocationBox.setSelectedItem( desPosit );
		Motor1.doHiMoveThread();
		Motor2.updateMasterGUI( false ); //temporarily stop updates of active field from motor2
		Motor2.namedLocationBox.setSelectedItem( tandemPos );
		Motor2.doHiMoveThread();
	    }
	    else if( M2posNams.contains( desPosit ) ) {
		Motor2.namedLocationBox.setSelectedItem( desPosit );
		Motor2.doHiMoveThread();
		Motor1.updateMasterGUI( false ); //temporarily stop updates of active field from motor1
		Motor1.namedLocationBox.setSelectedItem( tandemPos );
		Motor1.doHiMoveThread();
	    }
	    else this.setActive("unknown position!");
	    tandemPos = defTandemPos;
	}
	else this.setActive("no motor hooks?");
    }
//-------------------------------------------------------------------------------------

    public void moveToPositionWhenReady( JCIMotor masterMotor )
    {
	if( Motor != null ) {
	    Motor.namedLocationBox.setSelectedItem( this.getSelectedItem() );
	    Motor.doHiMoveWhenReady( masterMotor );
	}
    }
//-------------------------------------------------------------------------------------

    public boolean moveToPosition( UFTextPanel enterWaveLength )
    {
	String desPosit = this.getSelectedItem();

	if( desPosit.indexOf("High") >= 0 ) {
	    String cenWaveLength = enterWaveLength.desiredField.getText();
	    try {
		double waveLen = Double.parseDouble( cenWaveLength );
		JCIMotorGrating gratingWheel = (JCIMotorGrating)Motor;
		JCIhighResGrating[] hrGratings = gratingWheel.highResGratings;
		for( int k=0; k < hrGratings.length; k++ ) {
		    if( hrGratings[k].gratingName.equals( desPosit ) )
			hrGratings[k].setDesiredWavelen( waveLen );
		}
	    }
	    catch (NumberFormatException nfe) {
		enterWaveLength.setActive("* Bad Value ! *");
		System.err.println("Central Wavelength: "+ nfe.toString()); 
		return false;
	    }
	    enterWaveLength.setActive( cenWaveLength );
	}
	else enterWaveLength.setActive(" ");

	moveToPosition();
	return true;
    }
//-------------------------------------------------------------------------------------

    public boolean setTandemPos( String tandemPos )
    {
	//need to check if request is consistent.
	this.tandemPos = tandemPos;
	return true;
    }
//-------------------------------------------------------------------------------------

    public void checkStatus()
    {
	if( Motor != null ) {
	    String[] statwords = Motor.hiStatusLabel.getText().split("==");
	    this.setActive( statwords[statwords.length-1].trim() );
	}
	else if( Motor1 != null && Motor2 != null ) {
	    String[] statwords = Motor1.hiStatusLabel.getText().split("==");
	    Motor1pos = statwords[statwords.length-1].trim();
	    statwords = Motor2.hiStatusLabel.getText().split("==");
	    Motor2pos = statwords[statwords.length-1].trim();

	    if( Motor1pos.equals("Open") || Motor1pos.equals("Block") || Motor1pos.indexOf("Pup") >= 0 )
		this.setActive( Motor2pos );
	    else
		this.setActive( Motor1pos );
	}
	else this.setActive("no motor hooks?");
    }
//-------------------------------------------------------------------------------------

    public float getThroughput( String posName, JCIMotor motor )
    {
	for( int i=0; i < motor.positionNames.length-1; i++ ) {
	    if( posName.equals( motor.positionNames[i] ) ) return motor.throughPuts[i];
	}

	return(0);
    }
//-------------------------------------------------------------------------------------

    public float selectedThroughput()
    {
	if( Motor != null ) {
	    return getThroughput( this.getSelectedItem(), Motor );
	}
	else if( Motor1 != null && Motor2 != null )
	    {
		String desPos = this.getSelectedItem();

		if( M1posNams.contains( desPos ) ) {
		    return getThroughput( desPos, Motor1 ) * getThroughput( tandemPos, Motor2 );
		}
		else if( M2posNams.contains( desPos ) ) {
		    return getThroughput( tandemPos, Motor1 ) * getThroughput( desPos, Motor2 );
		}
		else return(0);
	    }
	else return(0);
    }
}
