package ufjci;

import javax.swing.*;
import java.awt.*;
import java.io.* ;
import java.util.StringTokenizer ;
import java.util.Properties;
import javaUFLib.*;

/**
*  Title:       Java Control Interface (JCI)
*  Version:     (see rcsId)
*  Copyright:   Copyright (c) 2003
*  Author:      Frank Varosi, David Rashkin
*  Company:     University of Florida
*  Description: for control and monitor of CanariCam infrared camera system.
*/

//===============================================================================
/**
 * main class
 */
public class jci {
  public static final
      String rcsID = "$Name:  $ $Id: jci.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

  public static String data_path = "./";
  public static final int MAX_SCREEN_LOCS = 10;
  public static int n_screen_locs;
  public static String screen_name[] = new String[MAX_SCREEN_LOCS];
  public static Point screen_loc[] = new Point[MAX_SCREEN_LOCS];
  public static Dimension screen_size[] = new Dimension[MAX_SCREEN_LOCS];

  boolean packFrame = false;

//-------------------------------------------------------------------------------
  /**
   *default constructor for the application
   *@param sim boolean simulation mode (true or false)
   */
  public jci(String hostname) {
    n_screen_locs = 1;
    screen_name[0] = "jciFrame";
    screen_loc[0] = new Point(50,50);
    screen_size[0] = new Dimension(400,400);

    load_screen_locs();
    jciFrame frame = new jciFrame(hostname);

    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame)
	frame.pack();
    else
	frame.validate();

    frame.setVisible(true);

    //try to redirect System.err.println's to jciError window
    PrintStream origErr = System.err;
    System.setErr(new PrintStream(origErr) {
	    //override System.err.println method
	    public void println(String x) {
		jciError.show(x);
	    }
    });
  } //end of jci

//-------------------------------------------------------------------------------
  /**
   *Main method
   *@param args: -host LCU agents hostname; -tsport: motor agent port; -tshost: motor agent host.
   * default is to read values out of mot_param.txt
   */
  public static void main(String[] args) {
    String hostname = null;
    load_ini();

    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-host")) {
	  if (i < args.length-1) 
	      hostname = args[i+1];
	  else
	      System.err.println("Must enter a valid hostname after -host");
      }
    }

    new jci(hostname);
  }

//-------------------------------------------------------------------------------
  /**
   *void load_init()
   * sets the data_path field from system property "jci.data_path" (default = "./data/")
   */
  static void load_ini() {
      Properties props = new Properties(System.getProperties()) ;
      System.setProperties(props) ;
      data_path = props.getProperty("jci.data_path") ;
      if (data_path == null) data_path = "./data/" ;
      System.out.println("jci.data_path = " + data_path) ;
  } // end of load_ini


//-------------------------------------------------------------------------------
  /**
   * Sets the screen locations for the window
   *@param name String: name in the title bar
   *@param loc Point: location of window
   *@param size Dimension: size of window
   */
  public static void set_screen_loc(String name, Point loc, Dimension size) {
    int i;
    for (i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        screen_loc[i] = loc;
        screen_size[i] = size;
        return;
      }
    }
    // name was not found, add it
    if (i < MAX_SCREEN_LOCS) {
      n_screen_locs++;
      screen_name[i] = name;
      screen_loc[i] = loc;
      screen_size[i] = size;
    }
    else {
      jciError.show("Too many screen_locs. Remove some old ones from screen_locs.txt");
    }
  } // end of set_screen_loc


//-------------------------------------------------------------------------------
  /**
   * Returns the point location of the window with a given name
   *@param name String: window name
   */
  public static Point get_screen_loc(String name) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_loc[i];
      }
    }
    // name was not found
    return new Point(50, 50);
  } // end of set_screen_size


//-------------------------------------------------------------------------------
  /**
   * Returns the size fo the window with a given name
   * If window with given name isn't found, Returns Dimension(400, 600)
   *@param name String: window name
   */
  public static Dimension get_screen_size(String name) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_size[i];
      }
    }
    // name was not found
    return new Dimension(400, 600);
  } // end of get_screen_size


//-------------------------------------------------------------------------------
  /**
   * Returns the size fo the window with a given name
   * If window with given name isn't found, Returns init_size parameter
   *@param name String: window name
   *@param init_size Dimension: size to be set if name not found
   */
  public static Dimension get_screen_size(String name, Dimension init_size) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_size[i];
      }
    }
    // name was not found
    return init_size;
  } // end of get_screen_size


//-------------------------------------------------------------------------------
  /**
   * Saves the screen locations to a text file: "screen_locs.txt"
   */
  static void save_screen_locs() {
    TextFile out_file = new TextFile(jci.data_path + "screen_locs.txt", TextFile.OUT);
    if (!out_file.ok()) {
      System.out.println("Error saving screen_locs.txt");
      return;
    }
    for (int i = 0; i < n_screen_locs; i++) {
      out_file.println(screen_name[i]);
      out_file.println(screen_loc[i].toString());
      out_file.println(screen_size[i].toString());
    }
    out_file.close();
  }

//-------------------------------------------------------------------------------
  /**
   * Loads the screen locations from the file: "screen_locs.txt"
   */
  static void load_screen_locs() {
    String line;
    TextFile in_file = new TextFile(jci.data_path + "screen_locs.txt", TextFile.IN);
    if (!in_file.ok()) {
      System.out.println("Cannot open screen_locs.txt");
      int response = JOptionPane.showConfirmDialog(null,
          "Cannot open screen_locs.txt. Do you want to try to continue running JCI?",
          "DATA FILE MISSING", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
      if (response != JOptionPane.YES_OPTION) {
        System.exit(1);
      }
    }
    // read the file
    line = in_file.readLine();
    int i = 0;
    while (line != null) {
	System.out.println( "jci.load_screen_locs[" + i +"]: " + line );
	if( line.trim().length() > 0 ) {
	    screen_name[i] = line;
	    line = in_file.readLine();
	    System.out.println( "jci.load_screen_locs[" + i +"]: " + line );
	    screen_loc[i] = new Point(nth_int_in_String(line, 1), nth_int_in_String(line, 2));
	    line = in_file.readLine();
	    System.out.println( "jci.load_screen_locs[" + i +"]: " + line );
	    screen_size[i] = new Dimension(nth_int_in_String(line, 1), nth_int_in_String(line, 2));
	    i++;
	}
	line = in_file.readLine();
	if (i == MAX_SCREEN_LOCS) line = null;
    }
    n_screen_locs = i;
    in_file.close();
  } // end of load_screen_locs


//-------------------------------------------------------------------------------
  /**
   *returns the n'th integer in s. e.g. when n = 2 and s = "[100,200]", returns 200
   *@param s string, line of integers delimited by ','
   *@param n int, number of numbers to move over
   */
  static int nth_int_in_String(String s, int n) {
    // returns the n'th integer in s. e.g. when n = 2 and s = "[100,200]", returns 200
    int p1 = 0;
    int p2 = 0;
    for (int i = 0; i < n; i++) {
      p1 = p2;
      while (!Character.isDigit(s.charAt(p1))) p1++;
      p2 = p1 + 1;
      while (Character.isDigit(s.charAt(p2))) p2++;
    }
    int answer = Integer.parseInt(s.substring(p1,p2));
    return answer;
  } // end of nth_int_in_String

} //end of class jci

