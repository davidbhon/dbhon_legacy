//Title:        JPanelPreamp for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Authors:      Ziad Saleh and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

package ufjci ;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.SwingConstants;
import javax.swing.border.*;
import java.text.DecimalFormat;
import javaUFProtocol.*;
import javaUFLib.*;

/**
 * Custom panel for setting PreAmp offset voltages via communication with Detector Control Agent 
 * @authors Ziad Saleh and Frank Varosi
 */

//===============================================================================
// Panel for showing desired/active offset voltages, and methods to access/modify:

class JPanelOffsetV extends JPanel
{  
    public  String P_Name;
    public  int P_Register ;
    public  int P_LatchOrder;
    public  double P_OffsetVoltage;
    public  double P_DacScaleFactor ;
    public  int P_DacOffset;

    UFTextPanel P_PreampPanel;

    public JPanelOffsetV( String name, String register, String latchorder,
		      String offsetVoltage, String dacscalefactor, String dacoffset)
    {
	try {
	    P_Name = name;
	    P_Register = Integer.parseInt(register);
	    P_LatchOrder= Integer.parseInt(latchorder);
	    P_OffsetVoltage = Double.valueOf(offsetVoltage.trim()).doubleValue() ;
	    P_DacScaleFactor = Double.valueOf(dacscalefactor.trim()).doubleValue();
	    P_DacOffset = Integer.parseInt(dacoffset);      
	    P_PreampPanel = new UFTextPanel(name, true);	    
	    P_PreampPanel.setDesired( offsetVoltage);
	    P_PreampPanel.desiredField.setEditable(true);
	    this.setLayout(new GridLayout(1,1));
	    this.add(P_PreampPanel);
	}
	catch(Exception e)
	    { JOptionPane.showMessageDialog(null,e,"Error in Offset Def?",JOptionPane.ERROR_MESSAGE); }    
    }

    public int computeDAC( double Voltage )
    {
	return (int)Math.round(Voltage*P_DacScaleFactor + P_DacOffset);
    }

    public double computeVolts( int DACval ) 
    { 
	return ( ( DACval - P_DacOffset )/P_DacScaleFactor );
    }

    public void setVolts( String DACval ) 
    {
	int DacValue = Integer.parseInt(DACval);
	// Convert the Dac value into voltage
	double Voltage = computeVolts(DacValue);
	String volts = Double.toString( Voltage );
	int ndigits = volts.indexOf(".") + 4;
	if( ndigits < 4 ) ndigits = 4;
	if( ndigits > volts.length() ) ndigits = volts.length();
	P_PreampPanel.setActive(volts.substring(0, ndigits), DacValue );
    }
    
    public String getVolts() { return P_PreampPanel.getActive(); };

    public String getVdefault() { return P_PreampPanel.getDesired(); };
    
    public String get_Name() { return P_PreampPanel.name() ; }
  	   
} //end of Class JPanelOffsetV.

//===============================================================================
// Main Tabbed Pane for controlling preamp offset voltages (uses JPanelTitleV from JPanelBias.java):

public class JPanelPreamp  extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JPanelPreamp.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

    public static final int NvOffsets=16; //must be divisible by 2, for equal Left & Right panels.
    final private JPanelOffsetV jPanelOffsets[];
    private JPanel P_LeftPanel, P_MidPanel, P_RightPanel;
    private JButton readAllButton, resetButton;
    public  JPanel P_jPanelVoltageSet;
    JTextField setAllVolts = new JTextField("9.0");
    JTextField adjustAllVolts = new JTextField("1.0");
    JPanelDetector DetectorPanel;

    //Transaction info Labels (added at bottom of frame):
    UFLabel statusAction   = new UFLabel("Action___:");
    UFLabel statusResponse = new UFLabel("Response:");

    public JPanelPreamp( JPanelDetector DetectorPanel )
    {
	this.DetectorPanel = DetectorPanel;
	JButton readAllButton = new JButton("READ ALL offsets");
	readAllButton.setDefaultCapable(false);
	JButton resetButton = new JButton("RESET to MCE defaults");
	resetButton.setDefaultCapable(false);
	JButton setAllButton = new JButton("Set ALL preamp offsets");
	JButton adjustAllButton = new JButton("Adjust ALL offsets");
	setAllButton.setDefaultCapable(false);
	adjustAllButton.setDefaultCapable(false);
	JButton P_Apply = new JButton("APPLY Desired Offset Voltages");
	P_Apply.setDefaultCapable(false);
	jPanelOffsets = new JPanelOffsetV[NvOffsets];

	setLayout(new RatioLayout());
	int jc = 0;
	String record = null ;

	//read file of parameters and create buttons for each preamp channel voltage offset:
	try {
	    FileReader fin = new FileReader(jci.data_path + "dc_preamp_param.txt");
	    BufferedReader bin = new BufferedReader(fin);
	    P_LeftPanel = new JPanel();
	    P_LeftPanel.setBorder(new EtchedBorder(0));
	    P_LeftPanel.setLayout(new GridLayout(9,1));
	    P_LeftPanel.add(new JPanelTitleV());
	    record = bin.readLine();         //read until first offset voltage:	 
	    while( record.indexOf("Offset_") < 0 ) record = bin.readLine();

	    for( int i=0; i < NvOffsets/2; i++ ) {
		StringTokenizer tokens = new StringTokenizer(record);
		jPanelOffsets[jc] = new JPanelOffsetV(tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken());
		final int ix = jc;

		P_LeftPanel.add( jPanelOffsets[jc++] );
		record = bin.readLine();
	    }

	    P_RightPanel = new JPanel();
	    P_RightPanel.setBorder(new EtchedBorder(0));
	    P_RightPanel.setLayout(new GridLayout(9,1));
	    P_RightPanel.add(new JPanelTitleV());

	    for( int i=0; i < NvOffsets/2; i++ ) {
		StringTokenizer tokens = new StringTokenizer(record);
		jPanelOffsets[jc] = new JPanelOffsetV(tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken(),
		 				      tokens.nextToken(),tokens.nextToken());
		final int ix = jc;

		P_RightPanel.add( jPanelOffsets[jc++] );
		record = bin.readLine();
	    }
	    bin.close();  //done reading file and creating buttons for each channel voltage offset.
	}
	catch(Exception e)
	    { JOptionPane.showMessageDialog(null,e,"Error in reading file?"+jc,JOptionPane.ERROR_MESSAGE); }    

	P_Apply.addActionListener ( new ActionListener() {
		public void actionPerformed(ActionEvent e) { applyDesiredValues(); } } );
	    
	resetButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { resetAll_action(); } } );
	    
	readAllButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { readAll_action(); } } );

	setAllButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { setAllVolts_action(); } } );

	adjustAllButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { adjustVolts_action(); } } );

	this.add("0.07,0.09;0.4,0.55",P_LeftPanel);
	this.add("0.55,0.09;0.4,0.55",P_RightPanel);
	this.add("0.02,0.69;0.45,0.07",P_Apply);
	this.add("0.02,0.79;0.21,0.07",readAllButton);
	this.add("0.25,0.79;0.21,0.07",resetButton);
	this.add("0.55,0.69;0.21,0.08",setAllButton);
	this.add("0.76,0.69;0.08,0.08",setAllVolts);
	this.add("0.55,0.79;0.21,0.08",adjustAllButton);
	this.add("0.76,0.79;0.08,0.08",adjustAllVolts);

	this.add("0.02,0.90;0.64,0.04", statusAction);
	this.add("0.02,0.94;0.64,0.04", statusResponse);

    } //end of method JPanelPreamp().
//-------------------------------------------------------------------------------

    void readAll_action() { readAll_action(true); }

    void readAll_action( boolean showActionResponse )
    {
	// Construct the command that reads all the voltages
	String Command[] = {"READ All PreAmp Offsets", "ARRAY_OFFSET_READBACK_PREAMP"};
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds, showActionResponse );
    }
//-------------------------------------------------------------------------------

    void resetAll_action()  
    { 
	int no = JOptionPane.showConfirmDialog(this,"Reset Preamp Voltages to MCE defaults ?","Warning",
					       JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	if( no != 1 ) {
	    String Command[] = {"RESET All PreAmp Offsets", "ARRAY_OFFSET_DEFAULTS_PREAMP"};
	    UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	    commandAgent( ufsDCcmds );
	}

	// Read all preamp offsets quietly:
	readAll_action(false);
    }
//-------------------------------------------------------------------------------

    void setAllVolts_action()  
    { 
	double Val = 0.0;
	String errMsg = "";
	String Command[] = new String[2];
	String GlobVal = setAllVolts.getText();
			
	try {
	    Val = Double.parseDouble( GlobVal) ;
	    // Convert from voltage to Dac Value
	    int DacVal = jPanelOffsets[0].computeDAC( Val );
	    Command[0] = "Set ALL PreAmp Offset Voltages";
	    Command[1] = "ARRAY_OFFSET_GLOBAL_PREAMP  " + DacVal ;
	}
	catch ( NumberFormatException nfe ) {
	    errMsg = "ERR: invalid (double) value: " + GlobVal;
	}
	
	if( errMsg.length() > 0 )
	    statusResponse.setText(errMsg);
	else {
	    UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	    commandAgent( ufsDCcmds );
	    readAll_action(false); // read all preamp offsets quietly
	}
    }
//-------------------------------------------------------------------------------

    void adjustVolts_action()  
    { 
	double Val = 0.0;
	String errMsg = "";
	String Command[] = new String[2];
	String AdjVal = adjustAllVolts.getText();
			
	try {
	    Val = Double.parseDouble( AdjVal) ;
	    // Convert from voltage to Dac Value
	    int DacVal = jPanelOffsets[0].computeDAC(Val);
	    // Construct the command to adjust preamp voltages
	    Command[0] = "Incremental Adjust of PreAmp Offsets";
	    Command[1] = "ARRAY_OFFSET_ADJUST_PREAMP   " + DacVal ;
	} 
	catch ( NumberFormatException nfe ) {
	    errMsg ="ERR: invalid (double) value: " + AdjVal;
	}
	
	if( errMsg.length() > 0 )
	    statusResponse.setText(errMsg);
	else {
	    UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	    commandAgent( ufsDCcmds );
	    readAll_action(false); // read all preamp offsets quietly
	}
    }
//-------------------------------------------------------------------------------

    void applyDesiredValues( )
    {
	String Name = "";
	String Value = "";
	double Val = 0.0;
	String errMsg = "";
	String Command[] = new String[NvOffsets*2];

	// Read all the desired voltage values and convert them into Dac values
	for ( int i=0; i < NvOffsets; i++)
	    {
		Name = jPanelOffsets[i].get_Name();
		Value = jPanelOffsets[i].P_PreampPanel.desiredField.getText();

		try {
		    Val = Double.parseDouble( Value ) ;
		    // Convert from voltage to Dac Value
		    int DacVal = jPanelOffsets[i].computeDAC( Val );
		    Command[2*i] = "Set PreAmp Offset Voltages";
		    Command[2*i +1] = "DAC_SET_SINGLE_OFFSET  " + i  + "  " + DacVal ;
		    jPanelOffsets[i].P_PreampPanel.setDesired(Value, DacVal);
		}
		catch ( NumberFormatException nfe ) {
		    errMsg = "ERR: " + Name + " has invalid (double) value: " + Value;
		    break;
		}
	    }

	if( errMsg.length() > 0 )
	    statusResponse.setText(errMsg);
	else {
	    UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	    commandAgent( ufsDCcmds );
	    readAll_action(false); // read all preamp offsets quietly
	}
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelPreamp#commandAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     */
    void commandAgent( UFStrings agentRequest )
    {
	commandAgent( agentRequest, true );
    }

    void commandAgent( UFStrings agentRequest, boolean showActionResponse )
    {
	final boolean ShowAR = showActionResponse;
	final String panel = "JCI:JPanelPreAmp:";
	final String action = agentRequest.name();
	agentRequest.rename( panel + action );
	final UFStrings AgentRequest = agentRequest;
	String msg = " sending " + action + " directive to DC agent...";
	System.out.println( panel + msg );

	if( showActionResponse ) {
	    statusAction.setText( msg );
	    statusResponse.setText(" waiting for response... ");
	}

	Runnable cmd_DC_agent = new Runnable() {
		public void run() {
		    try {
			String task = action + " >  ";
			UFStrings agentReply = DetectorPanel.sendRecvAgent( AgentRequest, panel+task );

			if( agentReply == null )
			    statusResponse.setText( task + "transaction ERROR: recvd NO response ?");
			else {
			    String statmsg = decodeAgentResponse( agentReply );

			    if( statmsg == null ) statmsg = "WARN: failed decoding response ?";
			    System.out.println( panel + task + statmsg );
			    if( ShowAR )
				statusResponse.setText( task + statmsg );
			}
		    }
		    catch( Exception x ) { x.printStackTrace(); }
		}
	    };

	SwingUtilities.invokeLater( cmd_DC_agent );
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelPreamp#decodeAgentResponse
     *@param agentReply : UFStrings object containing reply from DC agent.
     */
    String decodeAgentResponse( UFStrings agentReply )
    {
	String errMsg = "";
	boolean verbose = false;
	String msg = "DC agent response OK." ;

	if( agentReply == null ) return("ERROR: agent did not respond ?");

	if( verbose ) System.out.println( agentReply.toString() );

	for( int i=0; i < agentReply.numVals(); i++ )
	    {
		String param = agentReply.valData(i);
		if( verbose)
		    System.out.println("Agent reply(" + i + ") = " + param);

		if( param.indexOf("Channel #") >= 0 )
		    {
			String Val = param.substring(param.indexOf('=') + 1, param.indexOf('(')).trim() ;
			if (Val != null ) jPanelOffsets[i].setVolts(Val);
		    }
		else if( param.indexOf("simulation") >= 0 )
		    msg = "DC Agent in simulation mode";		    
		else {
		    errMsg = "ERR: unknown response from agent!";
		    break;
		}
	    }

	if( errMsg.length() > 0 ) 
	    return errMsg;
	else 
	    return msg;
    }
} //end of Class JPanelPreamp.



