//Title:        JCIhighResGrating class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin 
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;

//===============================================================================
/**
 * Objects needed by JCIMotorGrating class for special case of Hi-Level control of Grating Wheel.
 * Special processing is need for central wavelength positioning of each High Resolution grating.
 */
public class JCIhighResGrating
{
    String gratingName;
    double actualWavelen;
    double desiredWavelen;
    double fiducialAngle;
    int fiducialSteps;
    double fiducialAngleDef;
    int fiducialStepsDef;

    /* Polynomial coefficients for power series approximation of data */
    double pCoeff[] = { 7.5111263, 2.9258179, -0.03931878, 0.00261846 };
    int Ncoef = 4;
    double K2blazeAng = 16.8, mOrder = 1.0, Dgroove=11.11, stepsRev=9000.0;

    //Extra Visual Components
    JTextField desiredWavelenField;
    JTextField actualWavelenField;
    JTextField fiducialAngleField;
    JTextField fiducialStepsField;
    JCheckBox fiducialAngleCheck;
    JCheckBox fiducialStepsCheck;
    JButton angleResetButton;
    JButton stepsResetButton;
//---------------------------------------------------------------------------------
    //ctor:

    public JCIhighResGrating( String name, int fiducialSteps, double fiducialAngle )
    {
	this.gratingName = name;
	this.fiducialStepsDef = fiducialSteps;
	this.fiducialAngleDef = fiducialAngle;
	this.fiducialSteps = fiducialSteps;
	this.fiducialAngle = fiducialAngle;
    }
//---------------------------------------------------------------------------------
    public int gratingOffsetCalc()
    {
	try {
	    desiredWavelen = Double.parseDouble(desiredWavelenField.getText());
	    fiducialAngle = Double.parseDouble(fiducialAngleField.getText());
	    fiducialSteps = Integer.parseInt(fiducialStepsField.getText());
	} catch (NumberFormatException nfe) {
	    System.err.println("JCIhighResGrating( "+gratingName+" ).gratingOffsetCalc> "+nfe.toString());
	    return(-1);
	}

	double predictedAngle = pCoeff[0];
	double gwp = 1.;
	    
	for( int i=1; i<Ncoef; i++ ) {
	    gwp *= desiredWavelen;
	    predictedAngle += (pCoeff[i] * gwp);
	}

	double predictedStep = (stepsRev/360.0) * (predictedAngle - fiducialAngle) + fiducialSteps;
	double roundedStep = Math.round(predictedStep);
	double roundedAngle = (360.0/stepsRev) * (roundedStep - fiducialSteps) + fiducialAngle;
	double degToRad = 2 * Math.PI / 360.0;
	double adjWaveLength = (Dgroove/mOrder) * (Math.sin(roundedAngle*degToRad) +
						   Math.sin((roundedAngle-K2blazeAng)*degToRad));
	actualWavelenField.setText( adjWaveLength + "");
	actualWavelenField.setCaretPosition(0);
	return (int)roundedStep;
    }

    public void setFiducialAngle(double angle) {
	fiducialAngleField.setText(angle+"");
	fiducialAngle = angle;
    }
    public void setFiducialSteps(int steps) {
	fiducialStepsField.setText(steps+"");
	fiducialSteps = steps;
    }
    public void setDesiredWavelen(double wavelen) {
	desiredWavelenField.setText(wavelen+"");
	desiredWavelen = wavelen;
    }

    //accessor methods
    public double getFiducialAngle() { return fiducialAngle; }
    public int    getFiducialSteps() { return fiducialSteps; }
    public double getDesiredWavelen() { return desiredWavelen; }

    public void createVisualComponents() {
	String wavelen = gratingName.substring( gratingName.indexOf("-")+1 );
	desiredWavelenField = new JTextField(wavelen);
	actualWavelenField = new JTextField();
	actualWavelenField.setEditable(false);
	fiducialAngleField = new JTextField(fiducialAngle+"");
	fiducialAngleField.setEditable(false);
	fiducialStepsField = new JTextField(fiducialSteps+"");
	fiducialStepsField.setEditable(false);
	fiducialAngleCheck = new JCheckBox("fiducial Angle =",false);
	fiducialStepsCheck = new JCheckBox("fiducial Steps =",false);
	angleResetButton = new JButton("Reset");
	stepsResetButton = new JButton("Reset");
	fiducialAngleCheck.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    fiducialAngleField.setEditable(fiducialAngleCheck.isSelected());
		}
	});
	fiducialStepsCheck.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    fiducialStepsField.setEditable(fiducialStepsCheck.isSelected());
		}
	});
	angleResetButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    setFiducialAngle( fiducialAngleDef );
		}
	});
	stepsResetButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    setFiducialSteps( fiducialStepsDef );
		}
	});
    }

    public JPanel getHiResGratingPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.setBorder(BorderFactory.createEtchedBorder());
	panel.add("0.00,0.00;0.30,0.25", new JLabel(gratingName + " :") );
	panel.add("0.10,0.30;0.30,0.35", new JLabel("Desired  Central  Wavelength =") );
	panel.add("0.40,0.30;0.08,0.35", desiredWavelenField );
	panel.add("0.48,0.30;0.05,0.35", new JLabel("um") );
	panel.add("0.58,0.30;0.22,0.35", fiducialAngleCheck );
	panel.add("0.80,0.30;0.10,0.35", fiducialAngleField );
	panel.add("0.90,0.30;0.10,0.35", angleResetButton );
	panel.add("0.10,0.65;0.30,0.35", new JLabel("Actual  Central  Wavelength =") );
	panel.add("0.40,0.65;0.08,0.35", actualWavelenField );
	panel.add("0.48,0.65;0.05,0.35", new JLabel("um") );
	panel.add("0.58,0.65;0.22,0.35", fiducialStepsCheck );
	panel.add("0.80,0.65;0.10,0.35", fiducialStepsField );
	panel.add("0.90,0.65;0.10,0.35", stepsResetButton );
	return panel;
    } 
}
