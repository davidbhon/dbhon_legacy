/**
 *Title:       Java Control Interface (JCI)
 *Version:     (see rcsId)
 *Copyright:   Copyright (c) 2003
 *Author:      David Rashkin, Frank Varosi, and Ziad Saleh
 *Company:     University of Florida
 *Description: for control and monitor of CanariCam infrared camera system.
 */

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.lang.*;
import java.io.* ;
import java.util.* ;
import java.text.* ;
import javaUFLib.*;

//===============================================================================
/**
 * The main frame of JCI: creates all the tabbed panes and panels, and menus,
 * and importantly, has motor moving counter and status accessed by jciMotors.
 */
public class jciFrame extends JFrame
{
  public static final
      String rcsID = "$Name:  $ $Id: jciFrame.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

    public static UFLabel camStatus = new UFLabel(" Status: ");
    public static UFLabel motorStatus = new UFLabel(" Motors: ");
    public static int[] motorsMoving;
    JCIMotor[] jciMotors;

    JTabbedPane jciTabbedPane = new JTabbedPane();

    public static JPanelMotorParameters jPanelMotorParams;
    public static JPanelMotorLowLevel jPanelMotorLowLevel;
    public static JPanelMotorHighLevel jPanelMotorHighLevel;
    public static JPanelTemperature jPanelTemperature;
    //public static JPanelSystem jPanelSystem;
    public static JPanelDetector jPanelDetector;
    public static JPanelBias jPanelBias;  
    public static JPanelPreamp jPanelPreamp;                                                  
    public static JPanelMaster jPanelMaster;
    public static UFobsMonitor jPanelObsMonitor;

  public static String motorFileName;
  public static String temperatureFileName;
  public static String detBiasFileName;
  public static String detPreAmpFileName;

  public static JCheckBox jCheckBoxPanelLock = new JCheckBox("Panel Lock",false);
  public static JCheckBox jCheckBoxLCUhost = new JCheckBox("LCU Hostname:",false);
  public static int panelLockFlags = 0;

  public static String hostName = new String("kepler");
  public JTextField jTextFieldHostname = new JTextField(hostName,10);
//-------------------------------------------------------------------------------
  /**
   *Construct the frame
   *@param hostname     String: optional name LCU host on which agents are running
   *                            (if hostname = null then default hostname = kepler).
   */
  public jciFrame(String hostname)
  {
    System.out.println("Initializing JCI...");
    motorFileName = "motor_param.txt";
    detBiasFileName = "dc_bias_param.txt";
    detPreAmpFileName = "dc_preamp_param.txt";
    temperatureFileName = "temperature_params.txt";

    if( hostname != null ) {
	this.hostName = new String(hostname);
	jTextFieldHostname.setText(hostname);
    }
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);

    try {
	jciFrameInit();
    }
    catch(Exception e) { e.printStackTrace(); }
  }
//-------------------------------------------------------------------------------
  /**
   *Component initializations from parameter files, etc.
   */
  private void jciFrameInit() throws Exception
  {
    JMenuBar menuBar = new JMenuBar();
    JMenu menuFile = new JMenu();
    JMenuItem menuFileExit = new JMenuItem();
    JMenuItem menuFileEdit = new JMenuItem();
    JMenu menuHelp = new JMenu();
    JMenuItem menuHelpAbout = new JMenuItem();
    JMenuItem menuHelpEdit = new JMenuItem();

    JMenu menuOption = new JMenu();
    menuOption.setText("Look & Feel");
    String[] Looks = {"Motif Look","Metal Look"};

    for( int i=0; i < Looks.length; i++ )
	{
	    JMenuItem menuItem = new JMenuItem(Looks[i]);
	    menuOption.add(menuItem);
	    menuItem.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) { change_look(e); }
		});
	}

    menuFile.setText("File");
    menuFileEdit.setText("Edit a file");
    menuFileEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        fileEdit_action(e);
      }
    });
    menuFileExit.setText("Exit");
    menuFileExit.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        fileExit_action(e);
      }
    });
    menuHelp.setText("Help");
    menuHelpAbout.setText("About");
    menuHelpAbout.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        helpAbout_action(e);
      }
    });
    menuHelpEdit.setText("Edit jci_doc.txt");
    menuHelpEdit.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        helpEdit_action(e);
      }
    });

    menuFile.add(menuFileEdit);
    menuFile.add(menuFileExit);
    menuHelp.add(menuHelpAbout);
    menuHelp.add(menuHelpEdit);
    menuBar.add(menuFile);
    menuBar.add(menuHelp);
    menuBar.add(menuOption);
    menuBar.add( jCheckBoxPanelLock );
    menuBar.add( jCheckBoxLCUhost );
    menuBar.add( jTextFieldHostname );
    jTextFieldHostname.setBackground(new Color(240,230,220));
    jTextFieldHostname.setEditable(false);
    JButton LCUHostnameApplyButton = new JButton("Connect to all LCU agents");
    menuBar.add(LCUHostnameApplyButton);
    menuBar.add(camStatus);
    menuBar.add(motorStatus);

    jciMotors = JCIMotor.createMotors( motorFileName );
    motorsMoving = new int[jciMotors.length];
    motorStatus.setText("IDLE        ");

    jPanelMotorParams = new JPanelMotorParameters(jciMotors);
    jPanelMotorHighLevel = new JPanelMotorHighLevel(jciMotors);
    jPanelMotorLowLevel = new JPanelMotorLowLevel(jciMotors);
    jPanelTemperature = new JPanelTemperature( temperatureFileName, hostName );
    jPanelDetector = new JPanelDetector(hostName);
    jPanelBias = new JPanelBias(jPanelDetector);
    jPanelPreamp = new JPanelPreamp(jPanelDetector);
    jPanelMaster = new JPanelMaster( jPanelDetector, jciMotors );
    jPanelObsMonitor = new UFobsMonitor( jPanelMaster, jPanelDetector, jPanelBias );
    
    jciTabbedPane.add(jPanelMaster,"Master");
    jciTabbedPane.add(jPanelDetector,"Detector");
    jciTabbedPane.add(jPanelBias,"Bias");
    jciTabbedPane.add(jPanelPreamp,"PreAmp");
    jciTabbedPane.add(jPanelTemperature,"Temperature");
    jciTabbedPane.add(jPanelMotorHighLevel,"Motor High-Level");
    jciTabbedPane.add(jPanelMotorLowLevel,"Motor Low-Level");
    jciTabbedPane.add(jPanelMotorParams,"Motor Parameters");

    this.setTitle("JCI:  Java  Control  Interface  for  CanariCam  (version " + jciVersion.version + ")");
    this.setLocation( jci.screen_loc[0] );
    this.setSize( jci.screen_size[0] );
    this.setJMenuBar( menuBar );
    this.getContentPane().setLayout(new RatioLayout());
    this.getContentPane().add("0.0,0.01;1.0,0.79", jciTabbedPane );
    this.getContentPane().add("0.0,0.80;1.0,0.20", jPanelObsMonitor );

    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        this_componentResized(e);
      }
    });

    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        this_componentMoved(e);
      }
    });

    jCheckBoxPanelLock.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
	      panelLock_action(e);
      }
    });

    jCheckBoxLCUhost.addActionListener( new ActionListener() {
      public void actionPerformed(ActionEvent e) {
	  if( jCheckBoxLCUhost.isSelected() ) {
	      jTextFieldHostname.setBackground(Color.white);
	      jTextFieldHostname.setEditable(true);
	  }
	  else {
	      jTextFieldHostname.setBackground(new Color(240,230,220));
	      jTextFieldHostname.setEditable(false);
	  }
      }
    });

    jciTabbedPane.addChangeListener( new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        jciTabbedPane_stateChanged(e);
      }
    });

    LCUHostnameApplyButton.addActionListener( new ActionListener() {
	    public void actionPerformed(ActionEvent ae) {
		if( JOptionPane.showConfirmDialog(null, "Re-Connect to all LCU agents ?")
		    == JOptionPane.YES_OPTION )
		    {
			hostName = jTextFieldHostname.getText();
			jPanelTemperature.setHost(hostName);
			jPanelTemperature.reconnect();
			jPanelMotorParams.setHost(hostName);
			jPanelMotorParams.reConnect();
			jPanelDetector.setHost(hostName);
			jPanelDetector.connectToAgent();
			jPanelMaster.getNewParams();
			jPanelObsMonitor.connectToAgent();
			camStatus.setText("done re-connecting.");
		    }
	    }
	});
  } //end of jciFrameInit

//-------------------------------------------------------------------------------
    /**
     * Check flags for panel lock. Check/uncheck panel Lock checkbox based on current
     *  state of flags and current tab selected
     *@param e TBD
     */
    public void jciTabbedPane_stateChanged(ChangeEvent e)
    {
	int pindex = jciTabbedPane.getSelectedIndex();

	if(( panelLockFlags & (int)Math.pow(2,pindex) ) == 0)
	    jCheckBoxPanelLock.setSelected(false);
	else
	    jCheckBoxPanelLock.setSelected(true);

	if(jciTabbedPane.getTitleAt(pindex)=="Bias") {
	    //Always Lock the Bias Panel:
	    //recurseLock( jPanelBias, false );
	    //jCheckBoxPanelLock.setSelected(true);
	}
    }
//-------------------------------------------------------------------------------
  /**
   * Locks components of a Panel
   *@param jp JPanel to be locked
   *@param enable boolean: lock(yes or no)
   */
  public void recurse_update(Container jp) {
    Component [] cmp = jp.getComponents();
    for (int i=0; i<cmp.length; i++) {
      try {
        if (!cmp[i].getClass().getName().equals("javax.swing.JButton"))
	    {
		JButton _jb = (JButton) cmp[i];
		_jb.setDefaultCapable(false);
	    }
        Container _jp = (Container) cmp[i];
        recurse_update(_jp);
      }
      catch(ClassCastException cce) {
      }
      catch(Exception e) {
        jciError.show(e.toString());
      }
    }
  }
//-------------------------------------------------------------------------------
  /**
   * Locks components of a Panel
   *@param jp JPanel to be locked
   *@param enable boolean: lock(yes or no)
   */
  public void recurseLock(Container jp, boolean enable) {
    Component [] cmp = jp.getComponents();
    for (int i=0; i<cmp.length; i++) {
      try {
        if (!cmp[i].getClass().getName().equals("javax.swing.JLabel") &&
            cmp[i].getName()==null)  cmp[i].setEnabled(enable);
        Container _jp = (Container) cmp[i];
        recurseLock(_jp,enable);
      }
      catch(ClassCastException cce) {
      }
      catch(Exception e) {
        jciError.show(e.toString());
      }
    }
  }
//-------------------------------------------------------------------------------
  /**
   * Action performed for the lock checkbox
   *@param e TBD
   */
  public void panelLock_action(ActionEvent e) {
    JPanel cm = (JPanel) jciTabbedPane.getSelectedComponent();
    int s = jciTabbedPane.getSelectedIndex();
    panelLockFlags ^= (int)Math.pow(2,s);
    if( jCheckBoxPanelLock.isSelected() )
	recurseLock(cm,false);
    else
	recurseLock(cm,true);
    cm.repaint();
  }
//-------------------------------------------------------------------------------
  /**
   *File | Exit action performed
   * Simply exits the application
   *@param e not used
   */
  public void fileExit_action(ActionEvent e) {
    System.out.println("JCI saving screen params and Exiting...");
    jci.save_screen_locs();
    System.exit(0);
  }
//-------------------------------------------------------------------------------
  /**
   *Replaces all occurrences of s1 with s2 in line
   *@param line String: line for replacment
   *@param s1 String: to be replaced
   *@param s2 String: replacment to s1
   */
  String rep_all(String line, String s1, String s2) {
    int p;
    while ((p = line.indexOf(s1)) > -1) {
      line = line.substring(0, p) + s2 + line.substring(p + s1.length());
    }
    return line;
  }
//-------------------------------------------------------------------------------
  /**
   *Help | About action performed
   *Brings up the about dialog window
   *@param e not used
   */
  public void helpAbout_action(ActionEvent e) {
    jciAboutBox dlg = new jciAboutBox(this);
    Dimension dlgSize = dlg.getSize();
    Dimension frmSize = getSize();
    dlg.setLocation(100,100);
    dlg.setModal(true);
    dlg.show();
  } //end of helpAbout_action


//-------------------------------------------------------------------------------
  /**
   *Help | Edit action performed
   *Brings up the jci_doc.txt document for viewing and editing
   *@param e not used
   */
  public void helpEdit_action(ActionEvent e) {
    new jciEditorFrame(jci.data_path + "jci_doc.txt");
  }
//-------------------------------------------------------------------------------
  /**
   *File | Edit action performed
   *brings up the jci_doc.txt document for viewing and editing
   *@param e not used
   */
  public void fileEdit_action(ActionEvent e) {
    new jciEditorFrame(jci.data_path);
  }
//-------------------------------------------------------------------------------
  /**
   *Overridden so we can exit on System Close
   *@param e TBD
   */
  protected void processWindowEvent(WindowEvent e) {
      super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      fileExit_action(null);
    }
  }
//-------------------------------------------------------------------------------
  /**
   * Component Event Handler??
   *@param e not used
   */
  void this_componentMoved(ComponentEvent e) {
    jci.screen_loc[0] = this.getLocation();
    // System.out.println("screen_loc = " + jci.screen_loc[0].toString());
  }
//-------------------------------------------------------------------------------
  /**
   * Component Event Handler
   *@param e not used
   */
  void this_componentResized(ComponentEvent e) {
    jci.screen_size[0] = this.getSize();
    // System.out.println("size = " + jci.screen_size[0].toString());
  }
//---------------------------------------------------------------------------------------

  void change_look(ActionEvent e) {
    if (e.getActionCommand()=="Motif Look") 
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}

    if (e.getActionCommand()=="Metal Look") 
	try {
	    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}

    if (e.getActionCommand()=="Windows Look") 
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}
  }
} //end of class jciFrame




