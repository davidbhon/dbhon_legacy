[schematic2]
uniq 183
[tools]
[detail]
w -38 1883 100 0 n#168 hwin.hwin#162.in -48 1872 32 1872 ecalcouts.ecalcouts#170.INPA
w 418 1819 100 0 n#175 ecalcouts.ecalcouts#170.VAL 352 1808 544 1808 eaos.eaos#169.DOL
w 378 1883 100 0 n#166 ecalcouts.ecalcouts#170.FLNK 352 1872 464 1872 464 1776 544 1776 eaos.eaos#169.SLNK
w 824 1755 100 0 n#165 eaos.eaos#169.OUT 800 1744 896 1744 hwout.hwout#164.outp
w 824 1355 100 0 n#156 ebos.ebos#160.OUT 800 1344 896 1344 hwout.hwout#157.outp
w 418 1419 100 0 n#155 ecalcouts.ecalcouts#171.VAL 352 1408 544 1408 ebos.ebos#160.DOL
w 442 1387 100 0 n#155 ecalcouts.ecalcouts#171.FLNK 352 1472 400 1472 400 1376 544 1376 ebos.ebos#160.SLNK
w -24 1483 100 0 n#153 hwin.hwin#159.in -32 1472 32 1472 ecalcouts.ecalcouts#171.INPA
w 962 1067 100 0 n#151 trecsOneShot.trecsOneShot#146.RUNNING 864 1056 1120 1056 ebos.ebos#149.DOL
w 450 1067 100 0 n#147 ecalcouts.ecalcouts#172.FLNK 352 1056 608 1056 trecsOneShot.trecsOneShot#146.RESET
w -30 1067 100 0 n#143 hwin.hwin#145.in -32 1056 32 1056 ecalcouts.ecalcouts#172.INPA
w 866 811 100 0 n#139 hwin.hwin#133.in 672 608 800 608 800 800 992 800 ecalcs.ecalcs#129.INPA
w 898 779 100 0 n#137 eaos.eaos#76.VAL 800 384 864 384 864 768 992 768 ecalcs.ecalcs#129.INPB
w 866 427 100 0 n#136 eaos.eaos#76.FLNK 800 416 992 416 ecalcs.ecalcs#129.SLNK
w 466 395 100 0 n#177 ecalcouts.ecalcouts#173.FLNK 416 512 448 512 448 384 544 384 eaos.eaos#76.SLNK
w 432 459 100 0 n#84 ecalcouts.ecalcouts#173.VAL 416 448 496 448 496 416 544 416 eaos.eaos#76.DOL
w 18 427 100 0 n#182 hwin.hwin#62.in 32 416 64 416 64 480 96 480 ecalcouts.ecalcouts#173.INPB
w 34 523 100 0 n#178 hwin.hwin#60.in 32 512 96 512 ecalcouts.ecalcouts#173.INPA
w 936 203 100 0 n#78 eaos.eaos#76.OUT 800 352 864 352 864 192 1056 192 hwout.hwout#77.outp
w 824 -21 100 0 n#58 ebos.ebos#54.OUT 800 -32 896 -32 hwout.hwout#57.outp
w 472 11 100 0 n#56 ecalcouts.ecalcouts#174.FLNK 416 96 448 96 448 0 544 0 ebos.ebos#54.SLNK
w 450 43 100 0 n#180 ecalcouts.ecalcouts#174.VAL 416 32 544 32 ebos.ebos#54.DOL
w 34 107 100 0 n#181 hwin.hwin#52.in 32 96 96 96 ecalcouts.ecalcouts#174.INPA
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2002/02/23
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS TCS state reader
s 2320 -240 100 1792 Rev: D
s 2096 -240 100 1792 2001/12/20
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsControl.sch
[cell use]
use ecalcouts 96 -89 100 0 ecalcouts#174
xform 0 256 32
p 168 -56 100 0 -1 CALC:A
p 928 494 100 0 0 OOPT:On Change
p 184 144 100 0 1 SCAN:Event
p 160 -88 100 0 -1 name:$(top)tcsIpMonitor
use ecalcouts 96 327 100 0 ecalcouts#173
xform 0 256 448
p 168 360 100 0 -1 CALC:A+B
p 928 910 100 0 0 OOPT:On Change
p 184 560 100 0 1 SCAN:Event
p 160 328 100 0 -1 name:$(top)rhMonitor
use ecalcouts 32 871 100 0 ecalcouts#172
xform 0 192 992
p 104 904 100 0 -1 CALC:A
p 864 1454 100 0 0 OOPT:On Change
p 120 1104 100 0 1 SCAN:Event
p 96 872 100 0 -1 name:$(top)tcsHbMonitor
use ecalcouts 32 1287 100 0 ecalcouts#171
xform 0 192 1408
p 104 1320 100 0 -1 CALC:A
p 864 1870 100 0 0 OOPT:On Change
p 120 1520 100 0 1 SCAN:Event
p 96 1288 100 0 -1 name:$(top)tcsIpMonitor
use ecalcouts 32 1687 100 0 ecalcouts#170
xform 0 192 1808
p 104 1720 100 0 -1 CALC:A
p 864 2270 100 0 0 OOPT:On Change
p 120 1920 100 0 1 SCAN:Event
p 96 1688 100 0 -1 name:$(top)tcsIpMonitor
use eaos 544 295 100 0 eaos#76
xform 0 672 384
p 608 256 100 0 1 OMSL:closed_loop
p 720 288 100 1024 1 name:$(top)windowRh
p 800 352 75 768 -1 pproc(OUT):PP
use eaos 544 1687 100 0 eaos#169
xform 0 672 1776
p 608 1648 100 0 1 OMSL:closed_loop
p 720 1680 100 1024 1 name:$(top)windowRh
p 800 1744 75 768 -1 pproc(OUT):PP
use hwout 896 1703 100 0 hwout#164
xform 0 992 1744
p 1120 1744 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use hwout 896 1303 100 0 hwout#157
xform 0 992 1344
p 1120 1344 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use hwout 1056 151 100 0 hwout#77
xform 0 1152 192
p 1280 192 100 0 -1 val(outp):$(sad)windowRh.VAL PP NMS
use hwout 896 -73 100 0 hwout#57
xform 0 992 -32
p 1120 -32 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use hwin -240 1831 100 0 hwin#162
xform 0 -144 1872
p -480 1872 100 0 -1 val(in):tcs:sad:inPosition
use hwin -224 1431 100 0 hwin#159
xform 0 -128 1472
p -464 1472 100 0 -1 val(in):tcs:sad:inPosition
use hwin -160 375 100 0 hwin#62
xform 0 -64 416
p -448 416 100 0 -1 val(in):$(top)ec:tempMonG.VALM
use hwin -160 471 100 0 hwin#60
xform 0 -64 512
p -432 512 100 0 -1 val(in):tcs:sad:currentRH.VAL
use hwin -160 55 100 0 hwin#52
xform 0 -64 96
p -400 96 100 0 -1 val(in):tcs:sad:inPosition
use hwin 480 567 100 0 hwin#133
xform 0 576 608
p 208 608 100 0 -1 val(in):$(top)kBrRhLimit.VAL
use hwin -224 1015 100 0 hwin#145
xform 0 -128 1056
p -464 1056 100 0 -1 val(in):tcs:sad:heartbeat
use ebos 544 1287 100 0 ebos#160
xform 0 672 1376
p 544 1248 100 0 1 OMSL:closed_loop
p 688 1280 100 1024 1 name:$(top)tcsInPosition
p 800 1344 75 768 -1 pproc(OUT):PP
use ebos 544 -89 100 0 ebos#54
xform 0 672 0
p 544 -128 100 0 1 OMSL:closed_loop
p 688 -96 100 1024 1 name:$(top)tcsInPosition
p 800 -32 75 768 -1 pproc(OUT):PP
use ebos 1120 935 100 0 ebos#149
xform 0 1248 1024
p 1120 896 100 0 1 OMSL:closed_loop
p 1120 864 100 0 1 SCAN:1 second
p 1120 928 100 0 1 name:$(top)tcsIsAlive
p 1376 992 75 768 -1 pproc(OUT):NPP
use changeBar 1984 2023 100 0 changeBar#141
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#140
xform 0 2336 2032
use trecsOneShot 608 935 100 0 trecsOneShot#146
xform 0 736 1056
p 608 880 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 608 912 100 0 1 setTimer:timer $(top)tcsHbTimer
use ecalcs 992 327 100 0 ecalcs#129
xform 0 1136 592
p 1056 288 100 0 1 CALC:B>A
p 1056 320 100 0 1 name:$(top)rhTooHigh
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsTcsState.sch,v 0.0 2003/04/25 15:21:16 hon Developmental $
