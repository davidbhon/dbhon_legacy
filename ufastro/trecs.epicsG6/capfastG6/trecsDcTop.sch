[schematic2]
uniq 214
[tools]
[detail]
s 2512 -240 100 1792 trecsDcTop.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/02/06
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 T-Recs DC Top Level
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2016 2064 100 1792 A
s 2224 2064 100 1792 Initial Layout
s 2464 2064 100 1536 WNR
s 2608 2064 100 1792 2000/11/21
s 2016 2032 100 1792 B
s 2224 2032 100 1792 Removed control and status
s 2464 2032 100 1536 WNR
s 2608 2032 100 1792 2001/02/06
[cell use]
use changeBar 1984 2023 100 0 changeBar#212
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#213
xform 0 2336 2032
use trecsDcStatus 1424 743 100 0 trecsDcStatus#211
xform 0 1552 1104
use trecsDcCommand 416 743 100 0 trecsDcCommand#210
xform 0 544 1104
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsDcTop.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
