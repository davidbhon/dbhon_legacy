[schematic2]
uniq 28
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-Recs SAD Top Level
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/03
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsSadTop.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 YYYY/MM/DD
[cell use]
use trecsSadMain 608 807 100 0 trecsSadMain#27
xform 0 1024 1136
p 608 736 100 0 1 setCc:dc trecs:cc:
p 608 704 100 0 1 setDc:dc trecs:dc:
p 608 768 100 0 1 setTop:top trecs:sad:
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadTop.sch,v 0.2 2003/02/17 17:42:17 trecs beta $
