[schematic2]
uniq 97
[tools]
[detail]
w 24 1131 100 0 n#96 hwin.hwin#95.in 0 1120 96 1120 ecad2.ecad2#48.INPB
w 112 1547 -100 0 c#89 ecad2.ecad2#48.DIR 96 1408 32 1408 32 1536 240 1536 240 1856 128 1856 inhier.DIR.P
w 80 1579 -100 0 c#90 ecad2.ecad2#48.ICID 96 1376 0 1376 0 1568 208 1568 208 1728 128 1728 inhier.ICID.P
w 352 1547 -100 0 c#91 ecad2.ecad2#48.VAL 416 1408 480 1408 480 1536 272 1536 272 1856 416 1856 outhier.VAL.p
w 384 1579 -100 0 c#92 ecad2.ecad2#48.MESS 416 1376 512 1376 512 1568 304 1568 304 1728 416 1728 outhier.MESS.p
w 1532 523 100 0 n#83 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IVAL 1440 416 1536 416 1536 640 1696 640 trecsSubSysCombine.trecsSubSysCombine#77.DC_VAL
w 1596 459 100 0 n#82 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IMSS 1440 352 1600 352 1600 576 1696 576 trecsSubSysCombine.trecsSubSysCombine#77.DC_MESS
w 1592 707 100 0 n#81 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IMSS 1440 800 1536 800 1536 704 1696 704 trecsSubSysCombine.trecsSubSysCombine#77.CC_MESS
w 1496 867 100 0 n#80 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IVAL 1440 864 1600 864 1600 768 1696 768 trecsSubSysCombine.trecsSubSysCombine#77.CC_VAL
w 2008 651 100 0 n#93 trecsSubSysCombine.trecsSubSysCombine#77.CAR_IMSS 1952 640 2112 640 ecars.ecars#53.IMSS
w 2008 715 100 0 n#78 trecsSubSysCombine.trecsSubSysCombine#77.CAR_IVAL 1952 704 2112 704 ecars.ecars#53.IVAL
w 888 923 100 0 n#76 efanouts.efanouts#13.LNK4 832 912 992 912 992 320 1152 320 trecsSubSysCommand.trecsSubSysCommand#57.START
w 1176 1243 100 0 n#71 efanouts.efanouts#13.LNK2 832 976 1056 976 1056 1232 1344 1232 estringouts.estringouts#68.SLNK
w 1592 1227 100 0 n#59 estringouts.estringouts#68.OUT 1600 1216 1632 1216 hwout.hwout#60.outp
w 920 955 100 0 n#52 efanouts.efanouts#13.LNK3 832 944 1056 944 1056 768 1152 768 trecsSubSysCommand.trecsSubSysCommand#56.START
w 1144 1451 100 0 n#94 efanouts.efanouts#13.LNK1 832 1008 992 1008 992 1440 1344 1440 elongouts.elongouts#14.SLNK
w 480 939 100 0 n#49 ecad2.ecad2#48.STLK 416 928 592 928 efanouts.efanouts#13.SLNK
w 1592 1419 100 0 n#19 elongouts.elongouts#14.OUT 1600 1408 1632 1408 hwout.hwout#18.outp
w 1272 1483 100 0 n#17 hwin.hwin#16.in 1248 1536 1248 1472 1344 1472 elongouts.elongouts#14.DOL
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS datum Command
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/05
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsDatum.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/05
[cell use]
use hwin 1056 1495 100 0 hwin#16
xform 0 1152 1536
p 896 1536 100 0 -1 val(in):$(CAD_MARK)
use hwin -192 1079 100 0 hwin#95
xform 0 -96 1120
p -400 1120 100 0 -1 val(in):$(top)state.VAL
use outhier 432 1728 100 0 MESS
xform 0 400 1728
use outhier 432 1856 100 0 VAL
xform 0 400 1856
use inhier 48 1728 100 0 ICID
xform 0 128 1728
use inhier 64 1856 100 0 DIR
xform 0 128 1856
use trecsSubSysCombine 1696 487 100 0 trecsSubSysCombine#77
xform 0 1824 672
p 1696 352 100 0 1 setCmd:cmd datum
use estringouts 1344 1159 100 0 estringouts#68
xform 0 1472 1232
p 1424 1120 100 0 0 OMSL:supervisory
p 1408 1120 100 0 1 VAL:DATUM
p 1536 1152 100 1024 1 name:$(top)datumDcCmd
use hwout 1632 1175 100 0 hwout#60
xform 0 1728 1216
p 1856 1216 100 0 -1 val(outp):$(top)dc:acqControl.A
use hwout 1632 1367 100 0 hwout#18
xform 0 1728 1408
p 1856 1408 100 0 -1 val(outp):$(top)cc:datum.DIR PP NMS
use elongouts 1344 1351 100 0 elongouts#14
xform 0 1472 1440
p 1408 1312 100 0 0 OMSL:supervisory
p 1536 1344 100 1024 1 name:$(top)datumCcMark
p 1600 1408 75 768 -1 pproc(OUT):PP
use trecsSubSysCommand 1152 135 100 0 trecsSubSysCommand#57
xform 0 1296 320
p 1152 96 100 0 1 setCommand:cmd datum
p 1152 128 100 0 1 setSystem:sys dc
use trecsSubSysCommand 1152 583 100 0 trecsSubSysCommand#56
xform 0 1296 768
p 1152 544 100 0 1 setCommand:cmd datum
p 1152 576 100 0 1 setSystem:sys cc
use ecars 2112 423 100 0 ecars#53
xform 0 2272 592
p 2272 416 100 1024 1 name:$(top)datumC
use ecad2 96 839 100 0 ecad2#48
xform 0 256 1152
p 160 800 100 0 1 INAM:trecsIsNullInit
p 160 752 100 0 1 SNAM:trecsIsDatumProcess
p 256 832 100 1024 1 name:$(top)datum
p 432 928 75 1024 -1 pproc(STLK):PP
use efanouts 592 791 100 0 efanouts#13
xform 0 712 944
p 736 784 100 1024 1 name:$(top)datumFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsDatum.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
