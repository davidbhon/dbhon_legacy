
/* ===================================================================== */
/* INDENT OFF */
/*+
 *
 * FILENAME
 * -------- 
 * trecsIsCad.c
 *
 *
 * PURPOSE
 * -------
 * Support code for T-ReCS instrument command CAD records.
 *
 * 
 * FUNCTION NAME(S)
 * ---------------- 
 * trecsIsAbortProcess        - abort command processing
 * trecsIsContinueProcess     - continue command processing
 * trecsIsDataModeProcess     - dataMode command processing
 * trecsIsDatumProcess        - datum command processing
 * trecsIsDebugProcess        - debug command processing
 * trecsIsInitProcess         - init command processing
 * trecsIsInsSetupProcess     - instrumentSetup command processing
 * trecsIsNullInit            - generic NULL initialization function
 * trecsIsObsSetupProcess     - observiationSetup command processing
 * trecsIsObserveProcess      - observe command processing
 * trecsIsParkProcess         - park command processing
 * trecsIsPauseProcess        - pause command processing
 * trecsIsRebootProcess       - reboot command processing
 * trecsIsSetDhsInfoProcess   - setDhsInfo command processing
 * trecsIsSetWcsProcess       - setWcs command processing
 * trecsIsStopProcess         - stop command processing
 * trecsIsTestProcess         - test command processing
 *
 *
 * DEPENDENCIES
 * ------------
 * None
 *
 *
 * LIMITATIONS
 * -----------
 * None
 *
 * 
 * AUTHOR
 * ------
 * William Rambold  (wrambold@gemini.edu)
 * 
 *
 * HISTORY
 * -------
 * 2000/12/11  WNR  Template coding
 * 2001/12/24  WNR  Changed filter and device lookup file handling.
 */
/* INDENT ON */
/* ===================================================================== */


/*
 *  Include files
 */

#include <stdioLib.h>
#include <stdlib.h>
#include <string.h>

#include <sysLib.h>
#include <logLib.h>
#include <tickLib.h>

#include <dbEvent.h>
#include <recSup.h>

#include <cad.h>
#include <cadRecord.h>

#include "ufdbl.h"
#include <trecs.h>
#include <trecsIsCad.h>

#include <trecsPositionLookup.h>


/*
 *  Define a set of bit manipulation macros to keep track of the 
 *  configuration being applied so that conflicting commands can
 *  be rejected.
 */

#define TRX_INIT            0x00000001
#define TRX_DATUM           0x00000002
#define TRX_PARK            0x00000004
#define TRX_TEST            0x00000008
#define TRX_REBOOT          0x00000010
#define TRX_DEBUG           0x00000020
#define TRX_DATA_MODE       0x00000100
#define TRX_DHS_INFO        0x00000200
#define TRX_SET_WCS         0x00000400
#define TRX_INS             0x00001000
#define TRX_OBS             0x00002000
#define TRX_START_OBS       0x00010000
#define TRX_STOP_OBS        0x00020000
#define TRX_ABORT_OBS       0x00040000
#define TRX_CONTINUE_OBS    0x00080000

#define TRX_SETUP TRX_DEBUG|TRX_DATA_MODE|TRX_DHS_INFO|TRX_SET_WCS|TRX_INS|TRX_OBS

#define TRX_MARK_COMMAND(a)   trecsCommandMask |= (a);
#define TRX_CLEAR_COMMAND(a)  trecsCommandMask &= ~(a);
#define TRX_CMD_CONFLICT(a)   trecsCommandMask & ~(a)


/*
 *  Define a macro to print debugging information to the VxWorks logging
 *  system.  If the current debugging level set by the debug command is
 *  greater than or equal to the debugging threshold given to the macro then 
 *  the given information message string is sent to the logging task.
 *
 *  The string consists of the system tick counter followd by the
 *  name of the record and then a formatted string containing one
 *  integer variable.   For example:
 *
 *  DEBUG(TRX_DEBUG_FULL,
 *        "<%d> %s:initializing with %s simulation\n", pcr->a);
 *  
 *  Would result in the following log message if debugging is set to FULL:
 *
 *  <312456> trecs:init: Starting FULL simulation initialization
 *
 *  WARNING!! assumes that the CAD record struct pointer is called "pcr"
 */

#define DEBUG(l,FMT,V) if (l <= trecsIsDebugLevel)             \
                            logMsg (FMT,                       \
                                    (int) tickGet(),           \
                                    (int) pcr->name,           \
                                    (int) V, 0, 0, 0);


#define NAME_SIZE 32


typedef struct
{
    char cameraMode[NAME_SIZE];
    char imagingMode[NAME_SIZE];
    char aperture[NAME_SIZE];
    char filter[NAME_SIZE];
    char grating[NAME_SIZE];
    char wavelength[NAME_SIZE];
    char lyot[NAME_SIZE];
    char lens[NAME_SIZE];
    char sector[NAME_SIZE];
    char slit[NAME_SIZE];
    char window[NAME_SIZE];
    char filter1[NAME_SIZE];
    char filter2[NAME_SIZE];
    float lambdaMin;
    float lambdaMax;
    float throughput;
} TRECS_CONFIG;


/*
 *  Local data definitions
 */

static unsigned long trecsCommandMask = 0;  /* command tracking word    */
static long trecsIsDebugLevel = 0;          /* current debugging level  */

static char errorMessage[64];               /* error message buffer     */


/*
 *  ----------------- Public Access Functions ------------------
 */
    

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsAbortProcess
 * 
 * Purpose:
 * Check that it is safe abort an exposure in progress at this time and, 
 * if so, start the abort observation sequence.
 *
 * Invocation:
 * status = trecsIsAbortProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsAbortProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_ABORT_OBS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_ABORT_OBS);

            if (TRX_CMD_CONFLICT(TRX_ABORT_OBS))
            {
                strncpy (pcr->mess,
                         "Command conflict, clear configuration", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_ABORT_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting abort command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsContinueProcess
 * 
 * Purpose:
 * Check that it is safe to continue an observation at this time and,
 * if so, signal that it can be continued.
 *
 * Invocation:
 * status = trecsIsContinueProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2002/11/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsContinueProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_CONTINUE_OBS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_CONTINUE_OBS);

            if (TRX_CMD_CONFLICT(TRX_CONTINUE_OBS))
            {
                strncpy (pcr->mess,
                         "Can not mix configuration and observation", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_CONTINUE_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting Continue command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsDataModeProcess
 * 
 * Purpose:
 * Check that the requested data saving mode is valid and, if so, launch 
 * the data saving mode change sequence.
 *
 * Invocation:
 * status = trecsIsDataModeProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired save or discard data flag
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated save or discard data flag
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsDataModeProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_DATA_MODE);
            break;

        case CAD_STOP:
            break;

        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:

            if ( strcmp (pcr->a, "save") == 0 ||
                 strcmp (pcr->a, "no-dhs") == 0 ||
                 strcmp (pcr->a, "discard") == 0 ||
                 strcmp (pcr->a, "discard-all") == 0 )
            {
                strcpy (pcr->vala, pcr->a);
            }
            else
            {
                strcpy (pcr->mess, "Data Mode must be: save/discard/discard-all/no-dhs");
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_DATA_MODE);

             DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting dataMode %s command\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsDatumProcess
 * 
 * Purpose:
 * Check that it is safe to send all devices to their reference positions
 * at this time and, if so, launch the system datumming sequence.
 * 
 * Invocation:
 * status = trecsIsDatumProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsDatumProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_DATUM);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks the validity of the command
         */

        case CAD_PRESET:        

            TRX_MARK_COMMAND(TRX_DATUM);

            if (TRX_CMD_CONFLICT(TRX_DATUM))
            {
                strncpy (pcr->mess,
                         "Command conflict, clear configuration", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't datum while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:

            TRX_CLEAR_COMMAND(TRX_DATUM);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting datum command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsDebugProcess
 * 
 * Purpose:
 * Check that the requested debugging level is valid and, if so, launch
 * the system debugging level changing sequence.
 *
 * Invocation:
 * status = trecsIsDebugProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired debug level
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated debug level
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  ! trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word

 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsDebugProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_DEBUG);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_DEBUG);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->a, "NONE") == 0)
            {
                trecsIsDebugLevel = TRX_DEBUG_NONE;
            }

            else if (strcmp (pcr->a, "MIN") == 0)
            {
                trecsIsDebugLevel = TRX_DEBUG_MIN;
            }

            else if (strcmp (pcr->a, "FULL") == 0)
            {
                trecsIsDebugLevel = TRX_DEBUG_FULL;
            }

            else
            {
                strcpy (pcr->mess, "Debug must be [NONE|MIN|FULL]");
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_DEBUG);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: starting debug %s command\n", pcr->a);

            strcpy (pcr->vala, pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsInitProcess
 * 
 * Purpose:
 * Check that it is safe to re-initialize the controller at this time and, 
 * if so, launch the initialization sequence.
 *
 * Invocation:
 * status = trecsIsInitProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired simulation level
 *      > pcr->b     string  system state
 *      > pcr->c     string  pvload file name
 *      > pcr->d     string  filter lookup file name 
 *      > pcr->e     string  device lookup file name
 *
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  simulation level
 *      < pcr->valb  string  pvload file name
 *      < pcr->valc  string  filter lookup file name
 *      < pcr->vald  string  device lookup file name
 *      < pcr->vale  string  simulation level for DC
 *      < pcr->valf  string  simulation level for CC
 *      < pcr->valg  string  simulation level for EC
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Allowed init in DISCONNECTED and UNINITIALIZED states
 * 2001/09/14  WNR  Changed DC initialization to match CC and EC.
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsInitProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    char simmLevel[8];

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_INIT);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_INIT);

            if (TRX_CMD_CONFLICT(TRX_INIT))
            {
                strncpy (pcr->mess,
                         "Clear configuration before initializing",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && 
                strcmp (pcr->b, "ERROR") &&
                strcmp (pcr->b, "UNINITIALIZED") &&
                strcmp (pcr->b, "DISCONNECTED"))
            {
                strcpy (errorMessage, "Can't initialize while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }


            /*
             *  Match input simulation level and reject if not valid.
             */

            if (strcmp (pcr->a, "NONE") == 0)
            {
                strcpy (simmLevel, "0");
            }

            else if (strcmp (pcr->a, "VSM") == 0)
            {
                strcpy (simmLevel, "1");
            }

            else if (strcmp (pcr->a, "FAST") == 0)
            {
                strcpy (simmLevel, "2");
            }

            else if (strcmp (pcr->a, "FULL") == 0)
            {
                strcpy (simmLevel, "3");
            }

            else
            {
                strncpy (pcr->mess, 
                         "Mode must be [NONE|VSM|FAST|FULL]",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }


            /*
             *  Copy to the outputs so that the sub-systems can
             *  check the validity as well.
             */

            strcpy (pcr->vala, pcr->a);
            strcpy (pcr->valb, pcr->c);
            strcpy (pcr->valc, pcr->d);
            strcpy (pcr->vald, pcr->e);
            strcpy (pcr->vale, simmLevel);
            strcpy (pcr->valf, simmLevel);
            strcpy (pcr->valg, simmLevel);

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_INIT);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting %s simulation initialization\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsInsSetupProcess
 * 
 * Purpose:
 * Check that the requested optical element names are valid and that it
 * is safe to re-configure the elements in the optical path at this time.  
 * If so, launch the re-configuration sequence.
 *
 * Invocation:
 * status = trecsIsInsSetupProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired camera mode
 *      > pcr->b     string  desired imaging mode
 *      > pcr->c     string  desired aperture name
 *      > pcr->d     string  desired filter name
 *      > pcr->e     string  desired grating name
 *      > pcr->f     string  desired central wavelength
 *      > pcr->g     string  desired lens name
 *      > pcr->h     string  desired Lyot stop name
 *      > pcr->i     string  desired sector name
 *      > pcr->j     string  desired slit width
 *      > pcr->k     string  aperture override flag
 *      > pcr->l     string  filter override flag
 *      > pcr->m     string  lens override flag
 *      > pcr->n     string  window override flag
 *      > pcr->o     string  desired window setting
 *
 *      > pcr->p     string  loRes10Blocker filter name
 *      > pcr->q     string  loRes20Blocker filter name
 *      > pcr->r     string  hiRes20Blocker filter name
 *      > pcr->s     string  humidity too high at window
 *      > pcr->t     string  current instrument state
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated camera mode
 *      < pcr->valb  string  validated imaging mode
 *      < pcr->valc  string 
 *      < pcr->vald  string
 *      < pcr->vale  string  validated filter name
 *      < pcr->valf  string  validated central wavelength
 *      < pcr->valg  string  translated aperture name
 *      < pcr->valh  string  
 *      < pcr->vali  string  translated filter 1 name
 *      < pcr->valj  string  translated filter 2 name
 *      < pcr->valk  string  translated grating name
 *      < pcr->vall  string  translated lyot stop name
 *      < pcr->valm  string  translated pupil name
 *      < pcr->valn  string  translated sector name
 *      < pcr->valo  string  translated slit name
 *      < pcr->valp  string  translated window name
 *      < pcr->valq  string  calculated throughput
 *      < pcr->valr  string  calculated minimum wavelength
 *      < pcr->vals  string  calculated maximum wavelength
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2002/01/10  WNR  Replaced filter and position lookup
 *                  code with final versions.
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsInsSetupProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    TRECS_CONFIG config;        /* configuration structure          */
    char position[NAME_SIZE];   /* position string return           */
    float throughput;           /* optical throughput               */
    float lambdaMin;            /* minimum wavelength limit         */
    float lambdaMax;            /* maximum wavelength limit         */
    char *endPtr;               /* end of conversion ptr for strtod */
    long status;                /* function status return           */
    int datumcnt = ufGetDatumCnt("sys");

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_INS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_INS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands",
                         MAX_STRING_SIZE);

                return CAD_REJECT;
            }

            if (strcmp (pcr->t, "IDLE") && strcmp (pcr->t, "ERROR"))
            {
                strcpy (errorMessage, "Can't re-configure while ");
                strcat (errorMessage, pcr->t);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }
	    /* require initial datum before first mechanism configuration */
            if( datumcnt == 0 ) {
                strncpy (pcr->mess, "Can't configure before first datum.", MAX_STRING_SIZE);
                return CAD_REJECT;
            }
 
            /*
             *  Start by copying the input attributes into a configuration
             *  structure.
             */

            strcpy (config.cameraMode, pcr->a);
            strcpy (config.imagingMode, pcr->b);
            strcpy (config.aperture, pcr->c);
            strcpy (config.filter, pcr->d);
            strcpy (config.grating, pcr->e);
            strcpy (config.wavelength, pcr->f);
            strcpy (config.lens, pcr->g);
            strcpy (config.lyot, pcr->h);
            strcpy (config.sector, pcr->i);
            strcpy (config.slit, pcr->j);
            strcpy (config.window, pcr->k);

            /*
             *  Now analyze the configuration...
             */

            /*
             *  Camera mode overrides grating and filter selections
             */

            if (strcmp (config.cameraMode, "imaging") == 0)
            {
                strcpy (config.grating, "Mirror");
            }

            /*
             *  Use the appropriate blocking filter for the grating
             *  chosen.
             */
 
            else if (strcmp (config.cameraMode, "spectroscopy") == 0)
            {
                if (strcmp (config.grating, "LowRes-10") == 0)
                {
                    strcpy (config.filter, pcr->p);
                }

                else if (strcmp (config.grating, "LowRes-20") == 0)
                {
                    strcpy (config.filter, pcr->q);
                }
 
                else if (strcmp (config.grating, "HighRes-10") == 0)
                {
                    strcpy (config.filter, pcr->r);
                }

                else 
                {
                    strncpy (pcr->mess, 
                             "grating not [LowRes-10 | LowRes-20 | HighRes-10]",
                             MAX_STRING_SIZE);
                    return CAD_REJECT;
                }

                /*
                 *  Make sure that the central wavelength is
                 *  numeric.
                 */

                strtod (config.wavelength, &endPtr);
                if (*endPtr != '\0')
                {
                    strncpy (pcr->mess,
                             "central wavelength must be numeric",
                             MAX_STRING_SIZE);
                    return CAD_REJECT;
                }

            }

            /*
             *  Imaging mode overrides aperture and lens selection
             */

            if (strcmp(config.imagingMode, "field") == 0)
            {
                strcpy(config.aperture, "Matched");
                strcpy(config.lens, "Open-1");
            }

            else if (strcmp(config.imagingMode, "pupil") == 0)
            {
                strcpy(config.aperture, "Matched");
                strcpy(config.lens, "Pupil_Imager");
            }

            else if (strcmp(config.imagingMode, "window") == 0)
            {
                strcpy(config.aperture, "Window_Imager");
                strcpy(config.lens, "Open-1");
            }

            else
            {
                strncpy (pcr->mess,
                         "imaging mode not [field | pupil | window]",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            /*
             *  check for manual overrides.  If the aperture, filter or 
             *  lens override flags are set then use the input values
             *  directly instead.
             */

            if (strcmp (pcr->l, "TRUE") == 0)
            {
                strcpy(config.aperture, pcr->c);
            }

            if (strcmp (pcr->m, "TRUE") == 0)
            {
                strcpy(config.filter, pcr->d);
            }

            if (strcmp (pcr->n, "TRUE") == 0)
            {
                strcpy (config.lens, pcr->g);
            }
  
            /*
             *  Split filter name into two filter wheel components and
             *  select the appropriate window via the filter lookup
             *  table;
             */

            status = trecsFilterLookup (config.filter,
					config.filter1,
					config.filter2,
					config.window);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter: ");
                 strcat (errorMessage, config.filter);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }


            /*
             *  If the window override flag is set then use the
             *  input value directly instead.
             */

            if (strcmp (pcr->o, "TRUE") == 0)
            {
                strcpy(config.window, pcr->k);
            }

            /*
             *  Override a KBr window selection if the humidity is too high.
             */

            if (strcmp (config.window, "KBr") == 0)
            {
                 if (strcmp (pcr->s, "0") != 0)
                 {
                      strcpy (config.window, "KRS5");
                 }
            }

            /*
             *  Finally check to see that all the names are valid and
             *  calculate the overall efficiency.
             */

            config.throughput = 1.0;
            config.lambdaMin = 0.0;
            config.lambdaMax = 1000000000000.0;

            status = trecsPositionLookup ("aprtrWhl", 
                                          config.aperture,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized aperture: ");
                 strcat (errorMessage, config.aperture);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("fltrWhl1",
                                          config.filter1,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter: ");
                 strcat (errorMessage, config.filter1);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("fltrWhl2",
                                          config.filter2,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter: ");
                 strcat (errorMessage, config.filter2);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("grating",
                                          config.grating,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized grating: ");
                 strcat (errorMessage, config.grating);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("lyotWhl",
                                          config.lyot,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized Lyot: ");
                 strcat (errorMessage, config.lyot);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("pplImg",
                                          config.lens,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized lens: ");
                 strcat (errorMessage, config.lens);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("sectWhl",
                                          config.sector,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized sector: ");
                 strcat (errorMessage, config.sector);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("slitWhl",
                                          config.slit,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized slit: ");
                 strcat (errorMessage, config.slit);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = trecsPositionLookup ("winChngr",
                                          config.window,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized window: ");
                 strcat (errorMessage, config.window);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            /*
             *  Everything is valid, update the input fields so that
             *  the user sees the results of all the translations and the
             *  output fields so that the sub-systems can check
             *  the results as well.
             */

            strcpy (pcr->vala, config.cameraMode);
            strcpy (pcr->valb, config.imagingMode);
            strcpy (pcr->vale, config.filter);
            strcpy (pcr->valf, config.wavelength);
            strcpy (pcr->valg, config.aperture);
            strcpy (pcr->vali, config.filter1);
            strcpy (pcr->valj, config.filter2);
            strcpy (pcr->valk, config.grating);
            strcpy (pcr->vall, config.lyot);
            strcpy (pcr->valm, config.lens);
            strcpy (pcr->valn, config.sector);
            strcpy (pcr->valo, config.slit);
            strcpy (pcr->valp, config.window);
            sprintf (pcr->valq, "%.6f", config.throughput);
            sprintf (pcr->valr, "%.3f", config.lambdaMin);
            sprintf (pcr->vals, "%.3f", config.lambdaMax);

            if (strcmp (config.aperture, pcr->c) != 0)
            {
                strcpy (pcr->c, config.aperture);
                db_post_events(pcr, &pcr->c, DBE_VALUE);
            }

            if (strcmp (config.filter, pcr->d) != 0)
            {
                strcpy (pcr->d, config.filter);
                db_post_events(pcr, &pcr->d, DBE_VALUE);
            }

            if (strcmp (config.grating, pcr->e) != 0)
            {
                strcpy (pcr->e, config.grating);
                db_post_events(pcr, &pcr->e, DBE_VALUE);
            }

            if (strcmp (config.lens, pcr->g) != 0)
            {
                strcpy (pcr->g, config.lens);
                db_post_events(pcr, &pcr->g, DBE_VALUE);
            }

            if (strcmp (config.window, pcr->k) != 0)
            {
                strcpy (pcr->k, config.window);
                db_post_events(pcr, &pcr->k, DBE_VALUE);
            }

            break;

        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_INS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting instrument re-configuration %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}
     
 

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsNullInit
 * 
 * Purpose:
 * Generic do-nothing CAD record initialization function.
 *
 * Invocation:
 * status = trecsIsNullInit (cadRecord *pcr);
 *
 * Parameters in:
 * 
 * Parameters out:
 * 
 * Return value:
 *      < status     long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsNullInit
(
    cadRecord *pcr              /* cad record structure             */
)
{
    
    return OK;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsObsSetupProcess
 * 
 * Purpose:
 * Check that all requested configuration parameters are valid and that
 * it is safe to set up an observation configuration at this time.  If 
 * so, launch the observation configuration sequence.
 *
 * Invocation:
 * status = trecsIsObsSetupProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired observing mode
 *      > pcr->b     string  desired photon collecting time
 *      > pcr->c     string  desired secondary throw setting
 *      > pcr->d     string  desired sky noise setting
 *      > pcr->e     string  desired sky background setting
 *      > pcr->f     string  desired sky airmass setting
 *      > pcr->g     string  desired temperature setting
 *      > pcr->h     string  desired emissivity setting
 *      > pcr->i     string  desired rotator rate setting
 *      > pcr->j     string  desired nod dwell time
 *      > pcr->k     string  desired nod settling time 
 *      > pcr->l     string  desired readout mode
 *      > pcr->m     string  desired save frequency 
 *      > pcr->n     string  desired frame time
 *      > pcr->o     string  desired auto temp update flag 
 *      > pcr->p     string  desired override nod parameters
 *      > pcr->q     string  desired override save frequency 
 *      > pcr->r     string  desired override frame time
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated observing mode
 *      < pcr->valb  string  validated photon collecting time
 *      < pcr->valc  string  validated secondary throw setting 
 *      > pcr->vald  string  validated sky noise setting
 *      > pcr->vale  string  validated sky background setting
 *      > pcr->valf  string  validated sky airmass setting
 *      > pcr->valg  string  validated temperature setting
 *      > pcr->valh  string  validated emissivity setting
 *      > pcr->vali  string  validated rotator rate setting
 *      > pcr->valj  string  spare 
 *      > pcr->valk  string  validated readout mode 
 *      > pcr->vall  string  nod dwell time override
 *      > pcr->valm  string  nod settling time override
 *      > pcr->valn  string  save frequency override
 *      > pcr->valo  string  frame time override 
 *      > pcr->valp  string  spare 
 *      > pcr->valq  string  spare 
 *      > pcr->valr  string  array temperature update flag
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Added secondary throw input
 * 2002/01/23  WNR  Removed flux background and flux noise inputs
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsObsSetupProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    double fval;
    char *endPtr;

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_OBS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_OBS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }


            /* 
             *  Sanity check for photon observing mode
             */

            if (strcmp (pcr->a, "chop-nod") != 0 &&
                strcmp (pcr->a, "stare") != 0 &&
                strcmp (pcr->a, "chop") != 0 &&
                strcmp (pcr->a, "nod") != 0)
            {
                strcpy (pcr->mess, "Mode must be [chop-nod|stare|chop|nod]");
                return CAD_REJECT;
            }


            /*
             *  Sanity check for photon collecting time
             */

            fval = strtod (pcr->b, &endPtr);
            if (*endPtr != NULL) 
            {
                strcpy (pcr->mess, "Photon time must be a number");
                return CAD_REJECT;
            }

            /*
             *  Sanity check for secondary throw
             */

            fval = strtod (pcr->c, &endPtr);
            if (*endPtr != NULL) 
            {
                strcpy (pcr->mess, "Chop throw must be a number");
                return CAD_REJECT;
            }

            /* 
             *  Sanity check for override modes
             */

            if (strcmp (pcr->o, "TRUE") != 0 &&
                strcmp (pcr->o, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Auto temp update not [TRUE|FALSE]");
                return CAD_REJECT;
            }

            if (strcmp (pcr->p, "TRUE") != 0 &&
                strcmp (pcr->p, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Nod override not [TRUE|FALSE]");
                return CAD_REJECT;
            }

            if (strcmp (pcr->q, "TRUE") != 0 &&
                strcmp (pcr->q, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Save freq override not [TRUE|FALSE]");
                return CAD_REJECT;
            }

            if (strcmp (pcr->r, "TRUE") != 0 &&
                strcmp (pcr->r, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Frame rate overide not [TRUE|FALSE]");
                return CAD_REJECT;
            }

            /* 
             *  Sanity check for temperature update enable 
             */

            if (strcmp (pcr->o, "TRUE") != 0 &&
                strcmp (pcr->o, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Temp control must be [TRUE|FALSE]");
                return CAD_REJECT;
            }

/* hon -- add sanity check of readout mode */
/* fv -- simplified and allow "Automatic", since if invalid DC agent will choose */

            if (strcmp(pcr->l, "S1") != 0 && strstr(pcr->l, "Auto") !=0 &&
                strstr(pcr->l, "S1R1") != 0 && strstr (pcr->l, "S1R3") != 0 )
	      {
                strcpy (pcr->mess, "Invalid Array ReadOut Mode");
                return CAD_REJECT;
	      }


            strcpy (pcr->vala, pcr->a);
            strcpy (pcr->valb, pcr->b);
            strcpy (pcr->valc, pcr->c);
            strcpy (pcr->vald, pcr->d);
            strcpy (pcr->vale, pcr->e);
            strcpy (pcr->valf, pcr->f);
            strcpy (pcr->valg, pcr->g);
            strcpy (pcr->valh, pcr->h);
            strcpy (pcr->vali, pcr->i);

/* following corrected by D.Hon */

            strcpy (pcr->valk, pcr->l);  /* readout mode */
            strcpy (pcr->vall, "");
            strcpy (pcr->valm, "");
            strcpy (pcr->valn, "");
            strcpy (pcr->valo, "");

	    /* overrides: */

            if (strcmp (pcr->p, "TRUE") == 0)
            {
	      strcpy (pcr->vall, pcr->j);  /* nod dwell time */
	      strcpy (pcr->valm, pcr->k);  /* nod settle time */
            }
            if (strcmp (pcr->q, "TRUE") == 0)
            {
	      strcpy (pcr->valn, pcr->m);  /* save freq. */
            }
            if (strcmp (pcr->r, "TRUE") == 0)
            {
	      strcpy (pcr->valo, pcr->n);  /* frame time */
            }

            break;

        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting observation re-configuration %c\n", ' ');

            if (strcmp (pcr->o, "TRUE") == 0) 
            {
                *(long *)(pcr->valr) = 0;
            }
            else
            {
                *(long *)(pcr->valr) = 1;
            }

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}

     
/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsObserveProcess
 * 
 * Purpose:
 * Check that the requested data label is valid and that it is safe to
 * start a new observation at this time.  If so, launch the start-of-
 * observation sequence.
 *
 * Invocation:
 * status = trecsIsObserveProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired data label
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated data label
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsObserveProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_START_OBS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_START_OBS);

            if (TRX_CMD_CONFLICT(TRX_START_OBS))
            {
                strncpy (pcr->mess,
                         "Can not mix configuration and observation", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't expose while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

          
            /*
             *  how do we check the data mode???
             */

            strcpy (pcr->vala, pcr->a);

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_START_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting observation %s\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}
       

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name: 
 * trecsIsParkProcess
 * 
 * Purpose:
 * Check that it is safe to park all moving devices and leave the 
 * instrument in a shut-down condition at this time.  If so, launch the
 * parking sequence.
 *
 * Invocation:
 * status = trecsIsParkProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsParkProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_PARK)
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_PARK);

            /*
             *  ***** command verification stuff goes here ******
             */

            if (TRX_CMD_CONFLICT(TRX_PARK))
            {
                strncpy (pcr->mess,
                         "Clear configuration before parking", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't park while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:

            TRX_CLEAR_COMMAND(TRX_PARK);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting park command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsPauseProcess
 * 
 * Purpose:
 * Reject all pause requests.
 *
 * Invocation:
 * status = trecsIsPauseProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2002/11/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsPauseProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset rejects the command.
         */

        case CAD_PRESET:
            strncpy (pcr->mess,
                     "Exposures can not be paused!", MAX_STRING_SIZE);
            return CAD_REJECT;
            break;


        /*
         *  Start should never happen! 
         */

        case CAD_START:
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name: 
 * trecsIsRebootProcess
 * 
 * Purpose:
 * Check that it is safe to reboot the system at this time and, if so,
 * launch the system reboot sequence.
 *
 * Invocation:
 * status = trecsIsRebootProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsRebootProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_REBOOT);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_REBOOT);

            if (TRX_CMD_CONFLICT(TRX_REBOOT))
            {
                strncpy (pcr->mess,
                         "Clear configuration before rebooting",
                          MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_REBOOT);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting reboot command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsSetDhsInfoProcess
 * 
 * Purpose:
 * Check that the requested DHS quick-look stream enable is valid and, 
 * if so, launch stream request sequence.
 *
 * Invocation:
 * status = trecsIsSetDhsInfoProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired source quick-look stream enable 
 *      > pcr->b     string  desired refrernce quick-look stream enable 
 *      > pcr->c     string  desired diff1 quick-look stream enable 
 *      > pcr->d     string  desired diff2 quick-look stream enable 
 *      > pcr->e     string  desired signal quick-look stream enable 
 *      > pcr->f     string  desired signal_a quick-look stream enable 
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated quick-look stream ID
 *      > pcr->valb  string  validated refrernce quick-look stream enable 
 *      > pcr->valc  string  validated diff1 quick-look stream enable 
 *      > pcr->vald  string  validated diff2 quick-look stream enable 
 *      > pcr->vale  string  validated signal quick-look stream enable 
 *      > pcr->valf  string  validated signal_a quick-look stream enable 
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Added five more quick look enables.
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsSetDhsInfoProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    char bitmap[TRX_MAX_QL_STREAMS + 1];
    char *pField;
    int i;

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_DHS_INFO);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:

            /*
             * Insure that quick look stream enable inputs all have
             * valid values.  At the same time pack the individual
             * enable flags into a single bitmap string that will be
             * sent to the DC.  There are a total of 14 possible quick-look
             * streams.  The cad record fields are used to enable or
             * disable each of these streams individually by mapping the
             * .A field to first output and so on...  Writing
             * the string "enable" to the field will tell the detector 
             * controller to send the associated data to the pre-defined
             * quick-look stream.  Writing the string "disable" will
             * tell the detector controller to stop sending data on the
             * associated stream. 
             */

             pField = pcr->a;

             for (i = 0; i < TRX_MAX_QL_STREAMS; i++)
             {
                 if (strcmp(pField, "enable") == 0)
                 {
                     bitmap[i] = '1';
                 }
                 else if (strcmp(pField, "disable") == 0)
                 {
                     bitmap[i] = '0';
                 }
                 else
                 {
                     sprintf (pcr->mess, "%s %d %s",
                          "QL stream", i, "must be [enable | disable]");
                     return CAD_REJECT;
                 }

                 pField++;
             }

             bitmap[i] = '\0';

             strcpy (pcr->vala, bitmap);

             break;


        /*
         *  Start executes the command by triggering the start link 
         */
 
        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_DHS_INFO);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting setDhsInfo %s command\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsSetWcsProcess
 * 
 * Purpose:
 * Check that it is safe to update the world coordinate system information
 * at this time and, if so, launch the WCS updating sequence.
 *
 * Invocation:
 * status = trecsIsSetWcsProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsSetWcsProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_SET_WCS);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_SET_WCS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:
            TRX_CLEAR_COMMAND(TRX_SET_WCS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting setWcs command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsStopProcess
 * 
 * Purpose:
 * Check that it is safe to stop an observation in progress at this time 
 * and, if so, launch the stop exposure sequence.
 *
 * Invocation:
 * status = trecsIsStopProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsStopProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_STOP_OBS);
            break;

        case CAD_STOP:
            break;

        /*
         *  Preset checks command validity
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_STOP_OBS);

            if (TRX_CMD_CONFLICT(TRX_STOP_OBS))
            {
                strncpy (pcr->mess,
                         "Clear configuration before stopping", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:

            TRX_CLEAR_COMMAND(TRX_STOP_OBS);
            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting stop command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * trecsIsTestProcess
 * 
 * Purpose:
 * Check that it is safe to test the entire system at this time 
 * and, if so, launch the testing sequence.
 *
 * Invocation:
 * status = trecsIsTestProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > trecsIsDebugLevel       module debugging level flag
 *  ! trecsCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsTestProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case CAD_MARK:
            break;

        case CAD_CLEAR:
            TRX_CLEAR_COMMAND(TRX_TEST);
            break;

        case CAD_STOP:
            break;


        /*
         *  Preset checks command validity 
         */

        case CAD_PRESET:
            TRX_MARK_COMMAND(TRX_TEST);

            if (TRX_CMD_CONFLICT(TRX_TEST))
            {
                strncpy (pcr->mess,
                         "Clear configuration before testing", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't test while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }
            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case CAD_START:

            TRX_CLEAR_COMMAND(TRX_TEST);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting test command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}






