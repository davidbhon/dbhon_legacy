[schematic2]
uniq 177
[tools]
[detail]
w 50 1259 100 0 n#170 hwin.hwin#138.in 32 1248 128 1248 ecad8.ecad8#145.INPB
w 450 939 100 0 n#169 ecad8.ecad8#145.OUTG 448 928 512 928 hwout.hwout#157.outp
w 450 1003 100 0 n#168 ecad8.ecad8#145.OUTF 448 992 512 992 hwout.hwout#65.outp
w 450 1067 100 0 n#167 ecad8.ecad8#145.OUTE 448 1056 512 1056 hwout.hwout#60.outp
w 978 1643 100 0 n#164 ecad8.ecad8#145.VALD 448 1152 800 1152 800 1632 1216 1632 egenSub.egenSub#98.INPD
w 946 1707 100 0 n#163 ecad8.ecad8#145.VALC 448 1216 736 1216 736 1696 1216 1696 egenSub.egenSub#98.INPC
w 914 1771 100 0 n#162 ecad8.ecad8#145.VALB 448 1280 672 1280 672 1760 1216 1760 egenSub.egenSub#98.INPB
w 1756 203 100 0 n#153 trecsSubSysCommand.trecsSubSysCommand#144.CAR_IMSS 1504 0 1760 0 1760 416 1824 416 trecsSubSysCombine.trecsSubSysCombine#77.EC_MESS
w 1692 267 100 0 n#152 trecsSubSysCommand.trecsSubSysCommand#144.CAR_IVAL 1504 64 1696 64 1696 480 1824 480 trecsSubSysCombine.trecsSubSysCombine#77.EC_VAL
w 930 635 100 0 n#151 efanouts.efanouts#13.LNK5 864 624 1056 624 1056 -32 1216 -32 trecsSubSysCommand.trecsSubSysCommand#144.START
w 1756 1307 100 0 n#149 trecsSubSysCombine.trecsSubSysCombine#77.IS_VAL 1824 864 1760 864 1760 1760 1504 1760 egenSub.egenSub#98.OUTB
w 506 683 100 0 n#146 ecad8.ecad8#145.STLK 448 672 624 672 efanouts.efanouts#13.SLNK
w 1692 1307 100 0 n#136 trecsSubSysCombine.trecsSubSysCombine#77.IS_MESS 1824 800 1696 800 1696 1824 1504 1824 egenSub.egenSub#98.OUTA
w 882 1835 100 0 n#133 egenSub.egenSub#98.INPA 1216 1824 608 1824 junction
w 1170 1955 100 0 n#133 ecad8.ecad8#145.VALA 448 1344 608 1344 608 1952 1792 1952 estringouts.estringouts#69.DOL
w 2026 1907 100 0 n#132 estringouts.estringouts#69.OUT 2048 1904 2064 1904 hwout.hwout#131.outp
w 930 731 100 0 n#127 efanouts.efanouts#13.LNK2 864 720 1056 720 1056 1152 1216 1152 egenSub.egenSub#98.SLNK
w 144 1675 -100 0 c#89 ecad8.ecad8#145.DIR 128 1536 64 1536 64 1664 272 1664 272 1984 160 1984 inhier.DIR.P
w 112 1707 -100 0 c#90 ecad8.ecad8#145.ICID 128 1504 32 1504 32 1696 240 1696 240 1856 160 1856 inhier.ICID.P
w 384 1675 -100 0 c#91 ecad8.ecad8#145.VAL 448 1536 512 1536 512 1664 304 1664 304 1984 448 1984 outhier.VAL.p
w 416 1707 -100 0 c#92 ecad8.ecad8#145.MESS 448 1504 544 1504 544 1696 336 1696 336 1856 448 1856 outhier.MESS.p
w 1672 611 100 0 n#83 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IVAL 1504 480 1568 480 1568 608 1824 608 trecsSubSysCombine.trecsSubSysCombine#77.DC_VAL
w 1698 547 100 0 n#147 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IMSS 1504 416 1632 416 1632 544 1824 544 trecsSubSysCombine.trecsSubSysCombine#77.DC_MESS
w 1666 675 100 0 n#155 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IMSS 1504 832 1568 832 1568 672 1824 672 trecsSubSysCombine.trecsSubSysCombine#77.CC_MESS
w 1704 739 100 0 n#80 trecsSubSysCommand.trecsSubSysCommand#56.CAR_IVAL 1504 896 1632 896 1632 736 1824 736 trecsSubSysCombine.trecsSubSysCombine#77.CC_VAL
w 2114 619 100 0 n#113 trecsSubSysCombine.trecsSubSysCombine#77.CAR_IMSS 2080 608 2208 608 ecars.ecars#53.IMSS
w 2114 683 100 0 n#112 trecsSubSysCombine.trecsSubSysCombine#77.CAR_IVAL 2080 672 2208 672 ecars.ecars#53.IVAL
w 952 667 100 0 n#76 efanouts.efanouts#13.LNK4 864 656 1088 656 1088 384 1216 384 trecsSubSysCommand.trecsSubSysCommand#57.START
w 1020 1331 100 0 n#137 efanouts.efanouts#13.LNK1 864 752 1024 752 1024 1920 1792 1920 estringouts.estringouts#69.SLNK
w 952 699 100 0 n#52 efanouts.efanouts#13.LNK3 864 688 1088 688 1088 800 1216 800 trecsSubSysCommand.trecsSubSysCommand#56.START
s -160 1056 100 0 device file name
s -160 1120 100 0 filter file name
s -160 1184 100 0 pv file name
s 2624 2064 100 1792 2000/12/05
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 800 1840 100 0 simulationMode
s 800 1648 100 0 device file
s 800 1712 100 0 filter file
s 800 1776 100 0 pv file
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS Init Command
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/12/25
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsInit.sch
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Add initializaton file names
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/12/25
s -160 1312 100 0 simulationMode
[cell use]
use changeBar 1984 1991 100 0 changeBar#166
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#165
xform 0 2336 2064
use hwout 2064 1863 100 0 hwout#131
xform 0 2160 1904
p 2288 1904 100 0 -1 val(outp):$(sad)simulation.VAL PP NMS
use hwout 512 1015 100 0 hwout#60
xform 0 608 1056
p 736 1056 100 0 -1 val(outp):$(top)dc:init.A
use hwout 512 951 100 0 hwout#65
xform 0 608 992
p 736 992 100 0 -1 val(outp):$(top)cc:init.A
use hwout 512 887 100 0 hwout#157
xform 0 608 928
p 736 928 100 0 -1 val(outp):$(top)ec:init.A
use hwin -160 1207 100 0 hwin#138
xform 0 -64 1248
p -368 1248 100 0 -1 val(in):$(top)state.VAL
use ecad8 128 583 100 0 ecad8#145
xform 0 288 1088
p 192 528 100 0 1 INAM:trecsIsNullInit
p 192 496 100 0 1 SNAM:trecsIsInitProcess
p 192 576 100 0 1 name:$(top)init
use trecsSubSysCommand 1216 -217 100 0 trecsSubSysCommand#144
xform 0 1360 -32
p 1216 -256 100 0 1 setCommand:cmd init
p 1216 -224 100 0 1 setSystem:sys ec
use trecsSubSysCommand 1216 199 100 0 trecsSubSysCommand#57
xform 0 1360 384
p 1216 160 100 0 1 setCommand:cmd init
p 1216 192 100 0 1 setSystem:sys dc
use trecsSubSysCommand 1216 615 100 0 trecsSubSysCommand#56
xform 0 1360 800
p 1216 576 100 0 1 setCommand:cmd init
p 1216 608 100 0 1 setSystem:sys cc
use egenSub 1216 1063 100 0 egenSub#98
xform 0 1360 1488
p 993 837 100 0 0 FTA:STRING
p 993 837 100 0 0 FTB:STRING
p 993 805 100 0 0 FTC:STRING
p 993 773 100 0 0 FTD:STRING
p 993 837 100 0 0 FTVA:STRING
p 993 837 100 0 0 FTVB:LONG
p 1280 1024 100 0 1 INAM:trecsIsInitGInit
p 1280 992 100 0 1 SNAM:trecsIsInitGProcess
p 1376 1056 100 1024 1 name:$(top)initG
p 1504 1770 75 0 -1 pproc(OUTB):PP
use estringouts 1792 1847 100 0 estringouts#69
xform 0 1920 1920
p 1856 1808 100 0 1 OMSL:closed_loop
p 1984 1840 100 1024 1 name:$(top)SimulationMode
p 2048 1904 75 768 -1 pproc(OUT):PP
use outhier 464 1856 100 0 MESS
xform 0 432 1856
use outhier 464 1984 100 0 VAL
xform 0 432 1984
use inhier 80 1856 100 0 ICID
xform 0 160 1856
use inhier 96 1984 100 0 DIR
xform 0 160 1984
use trecsSubSysCombine 1824 455 100 0 trecsSubSysCombine#77
xform 0 1952 640
p 1840 320 100 0 1 setCmd:cmd init
use ecars 2208 391 100 0 ecars#53
xform 0 2368 560
p 2368 384 100 1024 1 name:$(top)initC
use efanouts 624 535 100 0 efanouts#13
xform 0 744 688
p 768 528 100 1024 1 name:$(top)initFanout
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsInit.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
