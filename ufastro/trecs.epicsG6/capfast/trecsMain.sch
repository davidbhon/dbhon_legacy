[schematic2]
uniq 15
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS Main Overview
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/03
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsMain.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/22/03
[cell use]
use trecsDhsStatus 1472 295 100 0 trecsDhsStatus#14
xform 0 1696 464
p 1492 268 100 0 1 setDhs:dhs $(top)dhs:
use trecsBoot 1072 375 100 0 trecsBoot#13
xform 0 1152 480
p 928 320 100 0 1 setExec:exec $(top)eg:
use trecsEcTop 928 679 100 0 trecsEcTop#12
xform 0 1152 848
p 928 672 100 0 1 setEc:ec $(top)ec:
use trecsControl 0 679 100 0 trecsControl#10
xform 0 128 1040
use trecsStatus 2048 679 100 0 trecsStatus#9
xform 0 2176 1040
use trecsDcTop 1360 679 100 0 trecsDcTop#6
xform 0 1696 848
use trecsCcTop 272 679 100 0 trecsCcTop#4
xform 0 608 848
use trecsIsTop 432 1143 100 0 trecsIsTop#3
xform 0 1136 1232
p 1568 1376 100 0 1 setBegin:START_TIMEOUT 5
p 1568 1312 100 0 1 setRun:CMD_TIMEOUT 1800
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsMain.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
