pro CloseAgent, agent, ALL_AGENTS=closeAll

   common AgentConnections, connections
 
	szc = size( connections )

	if keyword_set( closeAll ) then begin
		if szc[szc[0]+1] eq 8 then begin
			agents = connections.agent
			for i=0,N_elements( agents )-1 do CloseAgent, agents[i]
		   endif
		return
	   endif

	if N_elements( agent ) ne 1 then agent="MCE4"
	print, UFagentClose( agent )

	if szc[szc[0]+1] eq 8 then begin
		wa = where( agent eq connections.agent, nw )
	 endif else nw = 0

	if nw GT 0 then begin
		wna = where( agent ne connections.agent, nota )
		if nota gt 0 then begin
			connections = connections[wna]
		 endif else connections = 0
	   endif
end
