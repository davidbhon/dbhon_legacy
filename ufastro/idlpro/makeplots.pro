pro MakePlots, hours, LS218=LS218, LS340=LS340, VACUUM=vac, LAST_DATE=datend, START_DATE=datstart, $
					TRAN=Tran, HRAN=Hrang, PRAN=Pran, PLOG=PLog, TSTART=Tstart, $
	CF_CHAN=cf_chan, DD_CHAN=dd_chan, NSKIPC=nskipc, NSKIPW=nskipw, HARDCOPY=hardcopy, PAUSE=pause

ndat = N_elements(hours)
if N_elements( nskipc ) ne 1 then nskipc = 2
if N_elements( nskipw ) ne 1 then nskipw = 2
if N_elements( Tran ) ne 2 then Tran = [0,250]
if N_elements( Pran ) ne 2 then Pran = [1e-7,1e-4]
if N_elements( PLog ) ne 1 then PLog = 1
date = ""
if N_elements( datstart ) eq 1 then date = "start: " + datstart

if N_elements( Tstart ) eq 1 then begin
	TCP = LS218[*,1]
	wt = where( (TCP le Tstart) and (TCP gt 0) )
	Hstart = hours[wt[0]]
	help,Hstart
	if N_elements( datend ) eq 1 then date = "end: " + datend
	h = hours - Hstart
 endif else begin
	h = hours
	tstart = 400
  endelse

if N_elements( Hrang ) eq 2 then  Hran=Hrang  else  Hran = [0,max(h)]
save_pxyz

if keyword_set(hardcopy) then begin
	sz = size(hardcopy)
	if sz[sz[0]+1] eq 7 then psfile = hardcopy else	psfile = "TrecsEnvirLog"
	psfile = psfile + ".ps"
	psport,fil=psfile,/sq
	message,"saving plots to file: " + psfile,/INFO
   endif

if !D.name eq "PS" then begin
	Psym=8
	plotsym,0,0.2,/FILL
 endif else begin
	Psym=8
	plotsym,0,0.1
  endelse

;;-----------LakeShore218 plots:----------------------------------

get_window,1,/SHOW,XS=450,YS=450,/ERASE
plot_LSdata, h, LS218, ch=2, xr=Hran, PS=Psym, TR=Tran<Tstart, NSKIP=nskipc, TIT="Cold Plate, "+date

text=""
if keyword_set(pause) then read,"press ENTER to continue:",text
get_window,2,/SHOW,XS=450,YS=450,/ERASE
plot_LSdata, h, LS218, ch=1, xr=Hran, PS=Psym, TR=Tran, NSKIP=nskipw, TIT="Cold Head, "+date

if keyword_set(pause) then read,"press ENTER to continue:",text
get_window,3,/SHOW,XS=450,YS=450,/ERASE
plot_LSdata, h, LS218, ch=3, xr=Hran, PS=Psym, TR=Tran, NSKIP=nskipw, TIT="Grating Motor, "+date

;;-----------LakeShore340 plots:----------------------------------

if keyword_set(pause) then read,"press ENTER to continue:",text
get_window,0,/SHOW,XS=450,YS=450,/ERASE
if N_elements( cf_chan ) ne 1 then cf_chan=2
plot_LSdata, h, LS340, ch=cf_chan, xr=Hran, PS=Psym, TR=Tran<Tstart, NSKIP=nskipc, TIT="Cold Finger, "+date

if keyword_set(pause) then read,"press ENTER to continue:",text
get_window,6,/SHOW,XS=450,YS=450,/ERASE,XP=400,YP=545
if N_elements( dd_chan ) ne 1 then dd_chan=1
plot_LSdata, h, LS340, ch=dd_chan, xr=Hran, PS=Psym, TR=Tran<Tstart, NSKIP=nskipc, TIT="Detector, "+date

if keyword_set(pause) then read,"press ENTER to continue:",text

;;-----------Pressure plot:-----------------------------------------

if N_elements( Vac ) gt 0 then begin
	w = where( (Vac gt 0) and (Vac lt 1), np )
	if( np gt 0) then begin
		get_window,4,/SHOW,/ERASE,XS=450,YS=440,XP=400,YP=90
		plot, h[w], Vac[w], XTIT="Hours", YTIT="Pressure (Torr)", TIT="GP  Vacuum, "+date, $
								YR=Pran, PS=3, XR=Hran, YLOG=PLog
	   endif
   endif

if keyword_set(hardcopy) then psclose
save_pxyz,/RESTORE
end
