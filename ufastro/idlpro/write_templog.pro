pro write_TempLog, hours, LS340, LS218, Vacp, HSTART=hstart, HEND=hend, FILE=file, DATE=date, SKIP=iskip

	if N_elements( date ) ne 1 then date = "A"
	if N_elements( file ) NE 1 then file="TempLog_" + date + ".txt"
	if N_elements( hstart ) ne 1 then hstart = 0.0
	if N_elements( hend ) ne 1 then hend = 100.0
	if N_elements( iskip ) ne 1 then iskip = 2

	w = where( (hours GE hstart) and (hours LE hend), nw )

	if nw LE 0 then begin
		message,"no hours in range!",/INFO
		return
	   endif

	minutes = 60*(hours-hstart)
	i = w[0]
	iend = w[nw-1]
	openw,LUN,file,/GET
	print," writing to file: ",file
	print,"  minutes     Detector  Cold-Finger  Cold-Head  Cold-Plate  Grating   Pressure"
	printf,LUN,"  minutes     Detector  Cold-Finger  Cold-Head  Cold-Plate  Grating   Pressure"

	while ( i LE iend ) do begin
		print,minutes[i],reform(ls340[i,*]),reform(ls218[i,0:2]),Vacp[i],FORM="(6F11.3,g13.4)"
		printf,LUN,minutes[i],reform(ls340[i,*]),reform(ls218[i,0:2]),Vacp[i],FORM="(6F11.3,g13.4)"
		i = i+iskip
	  endwhile

	print," closing Log file: ",file
	free_Lun,LUN
end
