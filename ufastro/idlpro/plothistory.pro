pro PlotHistory, tsec, Temp, AGENT=agent, PORT=port, HOST=host, CHANNEL=chan, HARDCOPY=hardcopy

	if ConnectAgent( AGENT=agent, PORT=port, HOST=host ) LE 0 then return

	print, UFagentSend( ["hist",'1000'], "history", agent )
	thist = UFagentRecv( phdr, agent )
	thw = get_words( thist[0], DEL=[",",";"] )
	help,thist,thw

	nchan = N_elements(thw)
	ntime=N_elements(thist)
	tchans=fltarr(nchan,ntime)
	for i=0,ntime-1 do tchans[*,ntime-i-1] = float( get_words( thist[i], DEL=[",",";"] ) )

	tsec=findgen(ntime)
	tchans=transpose(tchans)

	if N_elements( chan ) LT 1 then chan=1
	Temp=tchans[*,chan-1]

	title = agent + " chan " + strtrim(chan,2)+": "+string(phdr.timestamp)

	get_window,0,/SHOW
	plotsym,0,0.1
	plot,tsec,Temp,psym=8,XTIT="Seconds", YTIT="T (K)", TIT=title, YSTY=16

	get_window,1,/SHOW
	plot,hv,histo(Temp,hv),ps=10,XTIT="T (K)", YTIT="Frequency", TIT=title

	if keyword_set(hardcopy) then begin
		sz = size(hardcopy)
		if sz[sz[0]+1] eq 7 then psfile = hardcopy  else psfile = "PlotHistory.ps"
		message,"saving plots to PSfile: " + psfile,/INFO
		psport,/SQUARE,FILE=psfile
		plotsym,0,0.2,/FILL
		plot,tsec,Temp,psym=8,XTIT="Seconds", YTIT="T (K)", TIT=title, YSTY=16
		plot,hv,histo(Temp,hv),ps=10,XTIT="T (K)", YTIT="Frequency", TIT=title
		psclose
	   endif
end

