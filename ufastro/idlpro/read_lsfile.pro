pro read_LSfile, hours, LS218=LS218, LS340=LS340, LSV340=LS340V, VACUUM=vacuum, $
			LAST_DATE=datend, START_DATE=datstart, $
			FILE=LSfile, DIRECTORY=directory, FDATE=fdate, FTYPE=ftype

	if N_elements( directory ) ne 1 then directory ="/net/newton/data/"
	if N_elements( fdate ) ne 1 then fdate ="Sep10"
	if N_elements( ftype ) ne 1 then ftype ="logidl"
	if N_elements( LSfile ) ne 1 then LSfile = directory + "trecsCryoTest" + fdate + "." + ftype

	openr,Lun,/get, LSfile
	on_ioerror, EOF

	Line=""
	seconds = fltarr( 20000 )
	vacuum =  fltarr( 20000 )
	hour = 0
	LS218 = fltarr( 8, 20000 )
	LS340 = fltarr( 2, 20000 )
	LS340V = fltarr( 2, 20000 )
	ndat = 0L

	while NOT EOF(Lun) do begin

		if N_elements( seconds ) LE ndat then seconds = [ seconds, fltarr(10000) ]
		if N_elements( vacuum ) LE ndat then vacuum = [ vacuum, fltarr(10000) ]
		sz = size( LS218 )
		if ndat ge sz[2] then  LS218 = [ [LS218], [fltarr( 8, 10000 )] ]
		sz = size( LS340 )
		if ndat ge sz[2] then  LS340 = [ [LS340], [fltarr( 2, 10000 )] ]
		sz = size( LS340V )
		if ndat ge sz[2] then  LS340V = [ [LS340V], [fltarr( 2, 10000 )] ]

		readf,Lun,Line
		date = Line
		if ndat eq 0 then datstart = date

		lw = get_words( Line, del=[":"," "] )
		seconds[ndat] = 3600L*(24*lw[2]+lw[3]) + 60*lw[4] + lw[5]
		newhour = lw[3]

		readf,Lun,Line
		while (strpos( Line, "Vac" ) LT 0) and (strtrim( Line, 2 ) ne '0') do readf,Lun,Line

		vacuum[ndat] = -1
		Line = strmid( Line, strpos(Line,":")+1, strlen(Line) )
		lw=get_words( Line, del=[":"," ",","] )

		if N_elements(lw) ge 2 then begin
			vacuum[ndat] = float(lw[1])
		   endif else vacuum[ndat] = float(lw[0])

		readf,Lun,Line
		while strpos( Line, "LakeShore 218" ) LT 0 do readf,Lun,Line

		Line = strmid( Line, strpos(Line,":")+1, strlen(Line) )
		lw = get_words( Line, del=[","," "] )
		if N_elements( lw ) ge 3 then LS218[*,ndat] = float( lw )

		readf,Lun,Line
		while strpos( Line, "LakeShore 340" ) LT 0 do readf,Lun,Line

		Line340 = Line
		Line = strmid( Line, strpos(Line,":")+1, strlen(Line) )
		lw = get_words( Line, del=[","," ",";"] )
		if N_elements( lw ) ge 2 then LS340[*,ndat] = float( lw )

		readf,Lun,Line
		while (strpos(strlowcase(Line),"volts") LT 0) $
			and (strpos(strlowcase(Line),"sensor") LT 0) $
			and (strpos(Line,"===") LT 0) do readf,Lun,Line

		if strpos(Line,"===") LT 0 then begin
			if (strpos(strlowcase(Line),"volts") GT 0) or $
			   (strpos(strlowcase(Line),"sensor") GT 0) then begin
				Line = strmid( Line, strpos(Line,":")+1, strlen(Line) )
				lw = get_words( Line, del=[","," ",";"] )
				if N_elements( lw ) ge 2 then LS340V[*,ndat] = float( lw )
			   endif
			readf,Lun,Line
			while (strpos(Line,"===") LT 0) do readf,Lun,Line
		   endif

		ndat = ndat+1

		if newhour ne hour then begin
			print,date + "  # " + strtrim(ndat,2) + " = " + Line340
			hour = newhour
		   endif
	  endwhile

	EOF: free_Lun,Lun
	datend = date
	print,datend + "  # " + strtrim(ndat,2) + " = " + Line340
	print,Line

	hours = seconds[0:ndat-1]/3600
	Hours = hours - hours[0]
	w = where( hours LT 0, nw )
	if nw GT 0 then hours[w] = hours[w] + max(hours) - min(hours)
	vacuum = vacuum[0:ndat-1]

	LS218 = transpose( LS218[*,0:ndat-1] )
	LS340 = transpose( LS340[*,0:ndat-1] )
	LS340V = transpose( LS340V[*,0:ndat-1] )
end
