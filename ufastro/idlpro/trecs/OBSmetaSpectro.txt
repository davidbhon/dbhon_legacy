PARM
META
ObsMode !! Error sending ObsMode
chop-nod
ReadoutMode !! Error sending ReadoutMode
S1R3
CameraMode !! Error sending CameraMode
spectroscopy
OnSourceTime !! Error sending OnSourceTime
1.0
ChopThrow !! Error sending ChopThrow
60
ChopFrequency !! Error sending ChopFrequency
4.0
Filter !! Error sending Filter
N
Throughput !! Error sending Throughput
0.5
Grating !! Error sending Grating
LowRes-10
CentralWavelength !! Error sending CentralWavelength
0.0
LyotStop !! Error sending LyotStop
Clear1
Sector !! Error sending Sector
Open
SlitWidth !! Error sending SlitWidth
Clear
AmbientTemperature !! Error sending AmbientTemperature
7
TelescopeEmissivity !! Error sending TelescopeEmissivity
0.08
AirMass !! Error sending AirMass
1.0
WaterVapour  !! Error sending WaterVapour
1.0
RotatorRate !! Error sending RotatorRate
1.2
