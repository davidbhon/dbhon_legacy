/*  SDS Include file                                         */
/*                                                           */
/*  Jeremy Bailey      1998  Jul 13                          */
/*                                                           */

#ifndef _SDS_INCLUDE_
#define _SDS_INCLUDE_ 1

#ifdef __cplusplus
extern "C" {
#endif

#ifdef VxWorks
#include "vxWorks.h"
#endif

#include "status.h"

/*    SDS version code    */

#define SDS_VERSION 100


/*
 *   Is const acceptable on this machine.
 */
#if defined(__cplusplus) || defined(__STDC__) || defined (VAXC)
#     define SDSCONST const
#else
#     define SDSCONST  /* */
#endif


/*  Machine architecture information  */

/*  See also definitions in sds.c for formats of each indivdual data type  */


/*  Does the architecture support 64 bit long integers - if not longs are assumed 
    to be 32 bit  */

#ifdef __alpha
#define LONG__64 1
#endif


/*  Are integers big endian or little endian  */


#ifdef VAX
#define   SDS_BIGEND 0
#elif defined MIPSEL
#define   SDS_BIGEND 0

#elif defined(M_I86) || defined(__i386__)
#ifndef M_I86
#define M_I86 1
#endif
#define   SDS_BIGEND 0

#elif defined __alpha
#define   SDS_BIGEND 0
#else
#define   SDS_BIGEND 1
#endif
/*
 * Under Windows, we may need to export or import the functions, depending
 * on if we are building the DLL or linking against it.
 */
#ifdef WIN32
#ifdef DRAMA_DLL  /* Am building DRAMA DLL */
#define SDSEXTERN extern __declspec(dllexport)
#define SDSCLASS  __declspec(dllexport) class
#elif defined(DRAMA_STATIC) /* Building a static library */
#define SDSEXTERN extern
#define SDSCLASS  class
#else           /* Am building application with DLL */
#define SDSEXTERN extern __declspec(dllimport)
#define SDSCLASS  __declspec(dllimport) class
#endif

#else
#define SDSEXTERN extern
#define SDSCLASS  class
#endif



/*    Status codes  */

#include "sds_err.h"

/*    Type codes   */

#define SDS_STRUCT   0              /*  Structure   */
#define SDS_CHAR     1              /*  Character   */
#define SDS_UBYTE    2              /*  Unsigned byte */
#define SDS_BYTE     3              /*  byte          */
#define SDS_USHORT   4              /*  Unsigned short */
#define SDS_SHORT    5              /*  short  */
#define SDS_UINT     6              /*  Unsigned int  */
#define SDS_INT      7              /*  int  */
#define SDS_FLOAT    8              /*  float  */
#define SDS_DOUBLE   9              /*  double  */
#define SDS_I64      11             /*  Int 64  */
#define SDS_UI64     12             /*  Unsigned Int 64  */

/*  Name length  */

#define SDS_C_NAMELEN 16

typedef long  SdsIdType;

typedef long  SdsCodeType;

SDSEXTERN void SdsCreate(SDSCONST char *name, long nextra, SDSCONST char *extra, 
		SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsInfo(SdsIdType id,char *name, SdsCodeType *code, long *ndims, 
               unsigned long* dims, StatusType * SDSCONST status);

SDSEXTERN void SdsNew(SdsIdType parent_id, SDSCONST char *name, long nextra, 
	       SDSCONST char *extra, SdsCodeType code, long ndims,  
	       SDSCONST unsigned long *dims, 
               SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsIndex(SdsIdType parent_id, long index, SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsFind(SdsIdType parent_id, SDSCONST char *name, SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsFindByPath(SdsIdType parent_id, SDSCONST char *name, SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsPointer(SdsIdType id, void **data, unsigned long *length, StatusType * SDSCONST status);

SDSEXTERN void SdsGet(SdsIdType id, unsigned long length, unsigned long offset, 
	void *data, unsigned long *actlen, StatusType * SDSCONST status);

SDSEXTERN void SdsPut(SdsIdType id, unsigned long length, unsigned long offset, 
		SDSCONST void *data,  StatusType * SDSCONST status);


SDSEXTERN void SdsCell(SdsIdType array_id, long nindices, 
		SDSCONST unsigned long *indices, 
                SdsIdType *id, StatusType * SDSCONST status);


SDSEXTERN void SdsInsertCell(SdsIdType array_id, long nindices, 
		SDSCONST unsigned long *indices, 
                SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsDelete(SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsSize(SdsIdType id, unsigned long *bytes, StatusType * SDSCONST status);
SDSEXTERN void SdsExport(SdsIdType id, unsigned long length, void *data, StatusType * SDSCONST status);

SDSEXTERN void SdsImport(SDSCONST void *data, SdsIdType *id, 
			StatusType * SDSCONST status);

SDSEXTERN void SdsAccess(void *data, SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsExtract(SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsInsert(SdsIdType parent_id, SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsCopy(SdsIdType id, SdsIdType *copy_id, StatusType * SDSCONST status);

SDSEXTERN void SdsResize(SdsIdType id, long ndims, SDSCONST unsigned long *dims,
		 StatusType * SDSCONST status);

SDSEXTERN void SdsRename(SdsIdType id, SDSCONST char* name, StatusType * SDSCONST status);

SDSEXTERN void SdsGetExtra(SdsIdType id, long length, char* extra, unsigned long *actlen, 
StatusType * SDSCONST status);

SDSEXTERN void SdsPutExtra(SdsIdType id, long length, SDSCONST char *extra, 
					StatusType * SDSCONST status);
    
SDSEXTERN void SdsExportDefined(SdsIdType id, unsigned long length, void *data, StatusType * SDSCONST status);    
      
SDSEXTERN void SdsSizeDefined(SdsIdType id, unsigned long *bytes, StatusType * SDSCONST status);

SDSEXTERN void SdsIsExternal(SdsIdType id, int *external, StatusType * SDSCONST status);

SDSEXTERN void SdsGetExternInfo(SdsIdType id, void **data, StatusType * SDSCONST status);
           
SDSEXTERN void SdsList(SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsWrite(SdsIdType id, SDSCONST char *filename, StatusType * SDSCONST status);

SDSEXTERN void SdsFreeId(SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsFlush(SdsIdType id, StatusType * SDSCONST status);

SDSEXTERN void SdsRead(SDSCONST char *filename, SdsIdType *id, StatusType * SDSCONST status);

SDSEXTERN void SdsFillArray(SdsIdType array_id, SdsIdType elem_id,
                StatusType * SDSCONST status);


SDSEXTERN void SdsReadFree(SdsIdType id, StatusType * SDSCONST status);
SDSEXTERN SDSCONST char *SdsTypeToStr(SdsCodeType code, int mode);


SDSEXTERN void Sds__MarkFree(SdsIdType id);

SDSEXTERN void Sds__Free(SdsIdType id, StatusType * SDSCONST status);


#define SDS_COMP_MESS_ERROR 1
#define SDS_COMP_MESS_WARN  2

SDSEXTERN void SdsCompiler(SDSCONST char *string, int messages, int intaslong, SdsIdType *id, StatusType *status);


/*  32 bit integer types - On most machines  int  is a 32 bit
    integer. However on some compilers long int is required to give
    a 32 bit integer. We can't use long int in all cases because this
    will sometimes give 64 bits (e.g. on Alpha). Under VxWorks, this values
    are already defined
  */


#ifdef macintosh
   typedef unsigned long int UINT32;
#elif !defined(VxWorks)
   typedef unsigned int UINT32;
#endif

#ifdef macintosh
   typedef long int INT32;
#elif !defined(VxWorks)
   typedef int INT32;
#endif





#ifdef LONG__64
   typedef long int INT64;
#else
   typedef struct INT64
    {
      UINT32 i1;
      UINT32 i2;
    } INT64;
#endif

#ifdef LONG__64
   typedef long int UINT64;
#else
   typedef struct UINT64
    {
      UINT32 i1;
      UINT32 i2;
    } UINT64;
#endif


SDSEXTERN INT64 SdsSetI64(long high, unsigned long low);

SDSEXTERN UINT64 SdsSetUI64(unsigned long high, unsigned long low);

SDSEXTERN void SdsGetI64(INT64 i64, long *high, unsigned long *low);

SDSEXTERN void SdsGetUI64(UINT64 i64, unsigned long *high, unsigned long *low);


#ifdef __cplusplus
} /* extern "C" */

/*
 * C++ only section.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  Here is the a C++ interface to SDS. No object code is required since
 *  all code is inline.
 *
 *  The class SdsId provides an assortment of constructors used for
 *  allocating Sds id's.  Various operations can be done using the
 *  Object (such as Get, Put and GetName etc.)
 *        
 *  The destructor will optionally delete and free the Sds id.  These
 *  options are normally dependent on how the item was constructed, but can
 *  be changed if required.
 *        
 *  Assignment and copying of these items is prohibited by making the
 *  operators private.  This is done since it is not clear what
 *  the user will expect from some operations. (for example, in a copy
 *  constructor, should the item be copied with SdsCopy or just a
 *  new id to the same item generated.  Sds does not even support the
 *  later.  Also, management of status is a problem). 
 *        
 *  You can pass items to subroutines by reference or pointer to get 
 *  around the copy problem.  A constructor which creates a copy of an 
 *  existing object can be used in the place of assignment in many cases.
 *  You can use the member functions ShallowCopy (which just copies the 
 *  Sds id itself) and DeepCopy, to get a copy in an explicit maner in other
 *  cases.
 */

SDSCLASS SdsId {
    private:
    	SdsIdType id;	/* Actual Sds id.  If 0, then no id is allocated */
    	struct {
    		bool free : 1;/* Set true to cause destructor to free the id*/
    		bool del  : 1;/* If free is true, destructor will do SdsDelete*/
    		bool readfree : 1;/* if free and del, to SdsReadFree */
    		} flags;

	/* Private routine to clean up, will be called by destructor and 
	   Shallow and Deep copy operations */
    	void CleanUp() {
    	    /* If we have an id and the free flag is set */
	    /* In the future, we should consider do a throw if status goes */
	    /* bad, instead of just ignoring it			*/
    	    
    	    if ((id)&&(flags.free))
    	    {
    	    	/* If del and readfree, we need to do SdsReadFree */
    	        if ((flags.del)&&(flags.readfree))
    	        {
    		    StatusType ignore = STATUS__OK;
    		    SdsReadFree(id,&ignore);
    	        }
    	        /* If del and not readfree, we need to to SdsDelete */
    	        else if (flags.del)
    	        {
    		    StatusType ignore = STATUS__OK;
    		    SdsDelete(id,&ignore);
    	        }
    	        /* Free the id */
    		StatusType ignore = STATUS__OK;
    		SdsFreeId(id,&ignore);
            }
    	}
    	/* Assignment and Copy operators, made private to avoid misuse */	
    	SdsId& operator=(const SdsId& a); /* Assignment */
    	SdsId(const SdsId&); /* Copy */
    public:
    	/* A Constructor that takes an existing id.  Free indicates
    	   we should free id when we destory it, del that we should delete
    	   the Sds item before freeing the id and readfree that the item
    	   was allocated by SdsRead.  Note that the id = 0 case (equivalent
    	   to a default constructor) is normally only used when we want an 
    	   argument to pass to a routine which wants to return an SdsId value.
    	   In which case, the ShallowCopy or DeepCopy operation would be used 
    	   to set the id.
    	   
    	 */
    	SdsId(const SdsIdType item = 0, const bool free=false, 
    			const bool del = false, const bool readfree = false) :
    		id(item) {
    		 flags.free = free;
    		 flags.del = del; 
    		 flags.readfree = readfree;
    	}
    	/* Constructor that does an access or import operation to get the id */
    	/* We use the one constructor since both operations take the same */
    	/* arguments */
    	SdsId(void * const data, StatusType * const status, 
    		   const bool import = false) :
    		id(0) {
    	    flags.free = true;
    	    flags.readfree = false;
            if (import)
            {
    	        SdsImport(data,&id,status);
    	        flags.del = true;
    	    }
            else
            {
    	        SdsAccess(data,&id,status);
    	        flags.del = false; 
    	    }
    	}
    	/* Alternative to the above where the data is a const.  */
    	/* In this case, only import is possible since access will */
    	/* modify the data array				*/
    	SdsId(const void * const data, StatusType * const status) : id(0) {
    	    flags.free = true;
    	    flags.readfree = false;
    	    flags.del = true;
    	    SdsImport(data,&id,status);
    	}
    	/* Constructor which returns an Id after reading a structure from */
	/* a file */
    	SdsId(const char * const filename, StatusType * const status) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = true;
    	    flags.readfree = true;
    	    SdsRead(filename,&id,status);
    	}
    	/* Constructor which creates a new (non-array) child item */
    	SdsId(const SdsId &parent_id, const char * const name, 
    	    const SdsCodeType code,
    	    StatusType * const status, const long nextra = 0, 
    	    const char * const extra = 0) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = false;
    	    flags.readfree = false;
     	    SdsNew(parent_id.id,name,nextra,extra,code,0,0,&id,status);
    	}    
    	/* Constructor which creates a new (non-array) top-level item */
    	SdsId(const char * const name, const SdsCodeType code,
    	    StatusType * const status, const long nextra = 0, 
    	    const char * const extra = 0) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = true;
    	    flags.readfree = false;
     	    SdsNew(0,name,nextra,extra,code,0,0,&id,status);
    	}    
    	/* Constructor which creates a new array child item */
    	SdsId(const SdsId &parent_id, const char * const name, 
    	    const SdsCodeType code,
    	    const long ndims, const unsigned long *dims,
    	    StatusType * const status, const long nextra = 0, 
    	    const char * const extra = 0) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = false;
    	    flags.readfree = false;
     	    SdsNew(parent_id.id,name,nextra,extra,code,ndims,dims,&id,status);
    	}    
    	/* Constructor which creates a new array top-level item */
    	SdsId(const char * const name, const SdsCodeType code,
    	    const long ndims, const unsigned long *dims,
    	    StatusType * const status, const long nextra = 0, 
    	    const char * const extra = 0) :
    		id(0) {
     	    flags.free = true;
    	    flags.del = true;
    	    flags.readfree = false;
    	    SdsNew(0,name,nextra,extra,code,ndims,dims,&id,status);
    	}    
    	
    	/* Constructor that returns a cell of an existing array id */
    	SdsId(const SdsId &array_id, const long nindicies, 
    	    const unsigned long * const indicies, StatusType * const status) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = false;
    	    flags.readfree = false;
     	    SdsCell(array_id.id, nindicies, indicies, &id,status);	
    	}
    	/* Copy constructor, copies an existing structure. */
    	SdsId(const SdsId& source, StatusType * const status):
    		id(0) {
    	    flags.free = true;
    	    flags.del = true;
    	    flags.readfree = false;
     	    SdsCopy(source.id, &id, status);
    	}
    	/* Constructor which returns an id to a named item of another item */
    	SdsId(const SdsId& source, const char * const name, 
    		StatusType * const status) :
    		id(0) {
    	    flags.free = true;
    	    flags.del = false;
    	    flags.readfree = false;
     	    SdsFind(source.id,name,&id,status);
    	}
    	
    	/* Constructor which returns an id to a structured item indexed 
    	   by position*/
    	SdsId(const SdsId& source, const long index, StatusType * const status):
    		id(0) {
    	    flags.free = true;
    	    flags.del = false;
    	    flags.readfree = false;
     	    SdsIndex(source.id,index,&id,status);
    	}
    	
    	/* Destructor.  Just invoke the private cleanup routine */
    	virtual ~SdsId() {
    	    CleanUp();
    	}
    	/* Modify flags */
    	void SetFree() { flags.free = true; }
    	void SetDelete() { flags.del = true; }
    	void ClearDelete() { flags.del = false; }

    	/* Forces actual Sds Id to outlive the SdsId variable */
    	void Outlive() { flags.free = false; flags.del = false; 
    			   flags.readfree = false; }
    	
/*
 *	General operations, as per similar Sds operation.
 *	Currently only Delete, Get and Put are virtual.
 */
    	
    	
    	virtual void Delete(StatusType * const status) {
    	    SdsDelete(id,status);
    	    if (*status == STATUS__OK)
    	        flags.del = false;
    	}
    	
    	void Export(const unsigned long length, 
    		    void * const data,
    		    StatusType * const status) {
    	    SdsExport(id,length,data,status);
        }
    	void Extract(StatusType * const status) {
    	    SdsExtract(id,status);
    	    /* If we succeed, we need to modify the flags to appropiate for
    	       and indepent toplevel item */
    	    if (*status == STATUS__OK)
    	    {
    	        flags.free = true;
    	        flags.del = true;
    	        flags.readfree = false;
    	    }
        }
        void Flush(StatusType * const status) {
            SdsFlush(id,status);
        }
        
        virtual void Get(const unsigned long length,
        	 void * const data,
        	 StatusType * const status,
        	 unsigned long *actlen = 0,
        	 const unsigned long offset=0) const
        {
            unsigned long myactlen;
            if (!actlen) actlen = &myactlen;
            SdsGet(id,length,offset,data,actlen,status);
        }
        void GetExtra(const unsigned long length,
        	 char * const extra,
        	 StatusType * const status,
        	 unsigned long *actlen = 0) const
        {
            unsigned long myactlen;
            if (!actlen) actlen = &myactlen;
            SdsGetExtra(id,length,extra,actlen,status);
        }
        void Info(char * const name,
        	  SdsCodeType * const code,
        	  long * const ndims,
        	  unsigned long * const dims,
        	  StatusType * const status) const {
            SdsInfo(id,name,code,ndims,dims,status);
        }
        /* Give access to individual things normally returned by SdsInfo*/
        void GetName(char * const name,
        	     StatusType * const status) const {
            SdsCodeType code;
            long ndims;
            unsigned long dims[7];
            SdsInfo(id,name,&code,&ndims,dims,status);
        }
        void Code(SdsIdType * const code,
        		StatusType * const status) const {
            char name[16];
            long ndims;
            unsigned long dims[7];
            SdsInfo(id,name,code,&ndims,dims,status);
        }
        void Dims(long * const ndims, unsigned long * const dims,
        		StatusType * const status) const {
            char name[16];
            SdsCodeType code;
            SdsInfo(id,name,&code,ndims,dims,status);
        }
        /* Insert a new object into our object */
        void Insert(SdsId & to_insert, StatusType * const status) {
            SdsInsert(id,to_insert.id,status);
            /* If we succeed, the new child should not be deleted when
            	it's id is destroyed, so clear the flag */
            if (*status == STATUS__OK)
                to_insert.flags.del = false;
        }
        /* Insert a object into a structured array */
        void Insert(SdsId & to_insert, const long ndims,
			const unsigned long * const dims, 
			StatusType * const status) {
            SdsInsertCell(id,ndims,dims,to_insert.id,status);
            /* If we succeed, the new child should not be deleted when
            	it's id is destroyed, so clear the flag */
            if (*status == STATUS__OK)
                to_insert.flags.del = false;
        }

	void FillArray(const SdsId &elem, StatusType * const status) {
	    SdsFillArray(id,elem.id,status);
	}

        void Pointer(void **data, StatusType * const status, 
        	     unsigned long * length = 0) {
            unsigned long mylength;
            if (!length) length = &mylength;
            SdsPointer(id,data,length,status);
        }
        virtual void Put(const unsigned long length,
        	 void * const data,
        	 StatusType * const status,
        	 const unsigned long offset=0)
        {
            SdsPut(id,length,offset,data,status);
        }
        void PutExtra(const long nextra,
        	 const char * const extra,
        	 StatusType * const status)
        {
            SdsPutExtra(id,nextra,extra,status);
        }
        void Rename(const char * const name, 
        		StatusType * const status) {
            SdsRename(id,name,status);
        }
        void Resize(const long ndims, const unsigned long *dims, 
        		StatusType * const status) {
             SdsResize(id,ndims,dims,status);
        }
        void Size(unsigned long * const bytes, 
        	  StatusType * const status) const {
            SdsSize(id,bytes,status);
        }
        void List(StatusType * const status) const {
            SdsList(id,status);
        }
	void Write(const char * const filename,
		   StatusType * const status) const {
	    SdsWrite(id,filename,status);
	}        

	
	/* Operator which returns the actual id */
	/* Note, use of (void) here instead of () is due to a bug in Gcc 2.7.0*/
	operator SdsIdType(void) const { return id; }
	
	/* Operator which tests if an item is valid.  Note, if there is
	   no bool type, we can just use the above operator which returns
	   the Sds id. Similary for Borland Cpp which does not like
           this operator (ambigious with the above operator */
#	if (!defined(CPP_NOBOOL))  && (!defined(BORLAND))
	operator bool(void) const { return (id != 0); }
#	endif
	
	/* Setup for and obtain details of this item for return of this	*/
	/* item using an SdsIdType.  If outlives is true, the SdsIdType */
	/* will outlive this item and we leave the freeing of the id etc */
	/* up to the caller, who can use the other variable to work out */
	/* what to do.  If outlives is false, can be used as an enquiry */
	SdsIdType COut(const bool outlives, bool * const free = 0,
		bool * const del= 0, bool * const readfree = 0) {
	    if (free)
	    	*free = flags.free;
	    if (del)
	    	*del  = flags.del;
	    if (readfree)
	    	*readfree = flags.readfree;
	    if (outlives)
	        flags.free = flags.del = flags.readfree = false;
	    return (SdsId::id);
	}
	/* Obsolete version 	*/
	void COut(const bool outlives, SdsIdType *item, bool * const free = 0,
		bool * const del= 0, bool * const readfree = 0) {
	    *item = COut(outlives,free,del,readfree);
	}

	/* Shallow copy from SdsId.  If we will outlive the source, then we copy
	   the source's flags and set the sources flags to false. 	*/
	void ShallowCopy (SdsId & source, const bool outlives) {
	    if (this != &source)
	    {
	        CleanUp();	/* Run destructor on old id */
	        id = source.id;
	        if (outlives)
	        {
	    	    flags.free = source.flags.free;
	     	    flags.del  = source.flags.del;
	    	    flags.readfree  = source.flags.readfree;

	    	    source.flags.free = source.flags.del = 
	    	    			source.flags.readfree = false;
	        }
	        else
	        {
	    	    flags.free = flags.del = flags.readfree = false;
	        }
	    }
	}
	/* Deep copy from SdsId.  We use SdsCopy to create a copy */
	void DeepCopy (const SdsId &source, StatusType * const status)
	{
	    if (this != &source)
	    {
	        CleanUp();	/* Run destructor code on old id */
	        id = 0;
	        SdsCopy(source.id,&id,status);
	        flags.free = true;
	        flags.del = true;
	        flags.readfree = false;
	    }
	}
	
	/* Shallow copy from SdsIdType.  Need to set the flags as appropiate
	   the source's flags and set the sources flags to false. 	*/
	void ShallowCopy (const SdsIdType source, const bool free=false, 
    			const bool del = false, const bool readfree = false)
    	{
	    CleanUp();	/* Run destructor on old id */
    	    flags.free = free;
    	    flags.del = del; 
    	    flags.readfree = readfree;
	    id = source;
	}
	/* Deep copy from SdsIdType.  We use SdsCopy to create a copy */
	void DeepCopy (const SdsIdType source, StatusType *status)
	{
	    CleanUp();	/* Run destructor code on old id */
	    id = 0;
  	    SdsCopy(source,&id,status);
	    flags.free = true;
	    flags.del = true;
	    flags.readfree = false;
	}
}; /* class SdsId */

SDSEXTERN const SdsId SdsNull;
 
#endif

/* Define SdsMalloc and SdsFree.  These are normally just defined
   to malloc and free but sometimes when debugging we want different
   versions
 */
 
#define SdsMalloc(_size) (void *)malloc(_size)
#define SdsFree(_where)   (void)free((void *)(_where))

#endif


