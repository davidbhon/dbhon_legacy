#if !defined(__UFCAputarr_cc__)
#define __UFCAputarr_cc__ "$Name:  $ $Id: ufcaputarr.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAputarr_cc__;

#include "string"
#include "UFDaemon.h"
#include "UFCAwrap.h"

// this & UFCAWrap are in a bad need of rewrite to use vector< string >& instead of
// pointers. UFCAWrap would benefit from inheritance from UFDaemon...
// note that the ctor for UFCAwrap will attempt a connection to epics chan. acc. service:
class UFCAputarr : public UFDaemon, public UFCAwrap {
public:
  inline UFCAputarr(int argc, char** argv) : UFDaemon(argc, argv), UFCAwrap(argc, argv),
                                          _verbose(false), _confirm(false) {}
  inline virtual ~UFCAputarr() {}
  inline virtual string description() const { return string(rcsId); }

  static int main(int argc, char** argv);
  virtual void* exec(void* p = 0);
  static int readline(string& line); // read stdin

private:
  bool _verbose;
  bool _confirm;
};

int UFCAputarr::main(int argc, char** argv) {
  UFCAputarr ufc(argc, argv);
  string usage= "echo -n 'epicsdb:record.field=a&b&c&d' | ufputarr [-v] [-c]";
  string arg = ufc.findArg("-help");
  if( arg != "false" ) {
    clog<<usage<<endl;
    return 0;
  }    
  ufc.exec();
  return 0;
}

int UFCAputarr::readline(string& line) { // read stdin
  line = "";
  char c[2] = "\n";
  do {
    read(0, c, 1);
    line += c;
  } while( c[0] != '\n' );

  return (int)c[0];
}

void* UFCAputarr::exec(void* p) {
  // check if epics channel acc. connect failed
  if( UFCAwrap::hasErrors > 0 ) {
    clog<<"aborting, init. channel access failed..."<<endl;
    return 0;
  }
  string arg;
  arg = findArg("-vca");
  if( arg != "false" ) {
    _verbose = true;
    UFCAwrap::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  string myname = (*_args)[0];
  string instrm_name= "trecs:";
  string record_name= instrm_name+"tempCont:ch_08";
  //string line= "trecs:tempCont:ch_08=70.77 tempCont:ch_09=17.71";
  string line= "trecs:tempCont:ch_08=70.77"; // just 1 db field at a time for now
  string puts;

  // check for one-time put:
  arg = findArg("-p");
  if( arg != "false" && arg != "true" )
    line = puts = arg;

  while ( !cin.eof() ) {
    if( line.empty() || puts != line ) {
      getline(cin, line);
    }
    else if( line.find("quit") == 0 || line.find("exit") == 0 ) {
      if( _verbose ) clog<<"UFCAputarr> quit..."<<endl;
      return 0;
    }
    else if( line.find("=") == string::npos || line.length() <= 1 ) {
      clog<<"UFCAputarr> ignoring badly formatted req.: "<<line<<endl;
      //clog<<"UFCAputarr> continue? (hit anything)" <<endl;
      //getchar();
      continue;
    }
    vector< string > vallist;
    size_t pos = line.find("=");
    string v, name = line.substr(0, pos);
    if( _confirm )
      clog<<"UFCAputarr> edb name: "<<name<<", from input line: '" << line << "'"<<endl;

    size_t ppos = pos;
    do {
      pos = ppos;
      ppos = line.find("&", 1+pos);
      if( ppos == string::npos || ppos == line.length()-1 ) { // must be last entry
        v = line.substr(1+pos, line.length()-pos-1);
	if( v.rfind("&") == v.length()-1 )
	  v = v.substr(0,v.length()-1);
      }
      else {
	v = line.substr(1+pos, ppos-pos-1);
      }
      size_t sv = v.size();
      if( sv <= 0 ) {
	clog<<"ufcaputarr> edb value string is empty for edb name: "<<name<<endl;
	continue;
      }
      char* cv = (char*) v.c_str();
      bool whitespace = true;
      while( *cv == ' ' || *cv == '\t' && *cv != '\0' )
	++cv;
      if( *cv != ' ' && *cv != '\t' && *cv != '\0' )
	whitespace = false;
      if( !whitespace )
        vallist.push_back(v);
      else
	clog<<"ufcaputarr> all whitespaces in db value? name: "<<name<<", value: "<<v<<endl;
    } while( ppos != string::npos ); 
    if( vallist.size() <= 0 )
      continue;
    int np = putArray(name, vallist);
    if( np < (int) vallist.size() ) {
      clog<<"ufcaputarr> failed to put list: name= "<<name<<", list size= "<<vallist.size()<<", nput= "<<np<<endl;
      continue;
    }
    if( _confirm ) {
      vector< string > cfmlist;
      np = getArray(name, cfmlist);
      clog<<"ufcaputarr> confirm, dbchan name= "<<name<<", list size (elements)= "<<cfmlist.size()<<endl;
      for( int i = 0; i < (int)cfmlist.size(); ++i )
        clog<<"ufcaputarr> confirmed: "<<cfmlist[i]<<endl;
    }      
    if( puts == line ) // assume -p option, all done
      return 0;
  } // while
  return 0;
}

int main(int argc, char** argv) {
  return UFCAputarr::main(argc, argv);
}

#endif //  __UFCAputarr_cc__
