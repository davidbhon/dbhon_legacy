#if !defined(__UFCAwrap_h__)
#define __UFCAwrap_h__ "$Id: UFCAwrap.h,v 0.4 2005/07/10 19:30:55 hon Exp $"
#define __UFCAwrap_H__(arg) static const char arg##UFCAwrap_h__rcsId[] = __UFCAwrap_h__;
// system 
#include "unistd.h"
#include "stdio.h"
#if defined(LINUX)
#define __restrict
#include "sys/time.h"
#undef __restrict
#else
#include "sys/time.h"
#endif

// epics 3.13.3,.4 & 3.14.x now support C++ headers:
//#if defined(__cplusplus)
//extern "C" {
//#endif
#include "cadef.h"
#include "fdmgr.h"
//#if defined(__cplusplus)
//}
//#endif

// c++
using namespace std;
#include "iostream"
#include "string"
#include "vector"
// there is a collision here with Solaris /usr/include/sys/map.h !
// when accessed by epics fdmgr.h:
#include "map"
//#include "multimap.h"
 

class UFCAwrap {
public:
  typedef void (*monEventHandler)(struct event_handler_args args);
  typedef std::map<string, chid> chidmap;
    
  static string usage;
  static void defaultMonEventHandler(struct event_handler_args args);
  static bool _verbose;
    
protected:
  int _argc;
  char** _argv;
  bool _loop;
  char _cabuff[MAX_STRING_SIZE];
  chidmap _chids;
  string _getCurrent(chid chan, const string& name);
  int _getCurrentArray(chid chan, const string& name, vector< string >& array);
 
  static double _pendio;
  static fdctx* _pfdctx; /* file descriptor manager id */
  static void _caFDCallBack(void *pUser);
  static void _fd_register(void *pfdctx, int fd, int opened);
  static void _capollfunc(void *pParam);
	
public:
  int hasErrors;
  UFCAwrap(int argc= 0, char** argv= 0);
  virtual ~UFCAwrap();

  // from argv:
  int checkOpt(char* opt);
  int cmdLine(string*& name, string*& val, int vcnt= 1); //allocate name & val(s)

  // from line:
  int checkOpt(char* opt, const string& line);
  //allocate name & val
  int cmdLine(const string& cmdline, string*& name, string*& val, int vcnt= 1); 

  chid searchAndConnect(const string& name);

  // gets of multiple rec/fields, this older implementation
  // uses pointers because of some old problems with stl & solaris & epics
  // that have gone away with 3.13.3 and newer, so this should be updated
  // to use vector<string> (someday):
  chid get(string* name, string* val, int num= 1);

  // new func. that gets an array from a single rec/field:
  int getArray(const string& name, vector <string>& vals);
 
  // old multi-rec/field put:
  chid put(string* name_val, string* put_val, int num= 1);

  // new array put (to single rec/field):
  int putArray(const string& name, vector <string>& vals);

  // old monitor func.
  void mon(string* name_val, int num= 1,
	   monEventHandler handler=UFCAwrap::defaultMonEventHandler);  

  static FILE* UFCAwrap::pipeToCAPut();
  static int UFCAwrap::writeToCAPut(const FILE* fp, const string& s);
};

#endif // __UFCAwrap_h__
