#include "iostream"
#include "map.h"
#include "UFSADFITS.h"
#include "UFStrings.h"

using namespace std;

int main(int argc, char** argv, char** env) {
  string epics = "flam", datalabel= "test";
  UFSADFITS sadfits(epics);
  std::vector< string > allsad;
  int ns = sadfits.allSAD(allsad);
  if( argc <= 1 ) {
    for( int i = 0; i < ns; ++i ) {
      if( i < 10 )
        cout<<"00"<<i<<".) "<<allsad[i]<<endl;
      else if( i < 100 )
	cout<<"0"<<i<<".) "<<allsad[i]<<endl;
      else
	cout<<i<<".) "<<allsad[i]<<endl;
	
    }
    return 0; // no arg
  }
  // [-whatever] arg:
  std::map< string, string > sadvals;
  clog<<"ufsadfits> "<<ends; system("date +%Y:%j:%H:%M:%S");
  UFStrings* fitsHdr = sadfits.fitsHeader(sadvals, epics);
  fitsHdr->relabel(datalabel);
  cout<<"name: "<<fitsHdr->name()<<", elem: "<<fitsHdr->elements()<<endl;
  clog<<"ufsadfits> set FITS header "<<ends; system("date +%Y:%j:%H:%M:%S");
  for( int i = 0; i < fitsHdr->elements(); ++i ) {
    cout<<i<<" : "<<(*fitsHdr)[i]<<endl;
  }
  clog<<"ufsadfits> all done "<<ends; system("date +%Y:%j:%H:%M:%S");
  clog<<"ufsadfits> "<<ends; system("date +%Y:%j:%H:%M:%S");

  fitsHdr = sadfits.fitsHeader(sadvals, epics);
  fitsHdr->relabel(datalabel);
  cout<<"name: "<<fitsHdr->name()<<", elem: "<<fitsHdr->elements()<<endl;
  clog<<"ufsadfits> set FITS header "<<ends; system("date +%Y:%j:%H:%M:%S");
  for( int i = 0; i < fitsHdr->elements(); ++i ) {
    cout<<i<<" : "<<(*fitsHdr)[i]<<endl;
  }
  clog<<"ufsadfits> all done "<<ends; system("date +%Y:%j:%H:%M:%S");
  clog<<"ufsadfits> "<<ends; system("date +%Y:%j:%H:%M:%S");

  fitsHdr = sadfits.fitsHeader(sadvals, epics);
  fitsHdr->relabel(datalabel);
  cout<<"name: "<<fitsHdr->name()<<", elem: "<<fitsHdr->elements()<<endl;
  clog<<"ufsadfits> set FITS header "<<ends; system("date +%Y:%j:%H:%M:%S");
  for( int i = 0; i < fitsHdr->elements(); ++i ) {
    cout<<i<<" : "<<(*fitsHdr)[i]<<endl;
  }
  clog<<"ufsadfits> all done "<<ends; system("date +%Y:%j:%H:%M:%S");
  return 0;
}
