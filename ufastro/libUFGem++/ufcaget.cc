#if !defined(__UFCAget_cc__)
#define __UFCAget_cc__ "$Name:  $ $Id: ufcaget.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAget_cc__;

#include "string"
#include "UFDaemon.h"
#include "UFCAwrap.h"

// note that the ctor for UFCAwrap will attempt a connection to epics chan. acc. service:
class UFCAget : public UFDaemon, public UFCAwrap {
public:
  inline UFCAget(int argc, char** argv) : UFDaemon(argc, argv), UFCAwrap(argc, argv),
                                          _verbose(false), _quiet(false) {}
  inline virtual ~UFCAget() {}
  inline virtual string description() const { return string(rcsId); }

  static int main(int argc, char** argv);
  virtual void* exec(void* p = 0);

private:
  bool _verbose, _quiet;
};

int UFCAget::main(int argc, char** argv) {
  //UFPosixRuntime::setDefaultSigHandler();
  UFCAget ufc(argc, argv);
  ufc.exec();
  return 0;
}

void* UFCAget::exec(void* p) {
  // check if epics channel acc. connect failed
  if( UFCAwrap::hasErrors > 0 ) {
    clog<<"aborting, init. channel access failed..."<<endl;
    return 0;
  }
  string arg;
  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  int vcnt= 0;
  arg = findArg("-cnt");
  if( arg != "false" && arg != "true" )
    vcnt = atoi(arg.c_str());

  string myname = (*_args)[0];
  string instrm_name= "flam:"; // or foo: or fu: or trecs: or miri:
  string record_name= instrm_name + "tempCont:ch_08";
  string line= instrm_name + "heartbeat";
  //string *vnamelist= 0, *vvallist= 0;
  string gets;
  // check for one-time get:
  arg = findArg("-g");
  if( arg != "false" && arg != "true" ) 
    line = gets = arg;
  arg = findArg("-q");
  if( arg == "true" ) 
    _quiet = true;
  // quiet version of -g should only print out the dbrec.field value (1 line)
  if( arg != "false" && arg != "true" ) {
    _quiet = true; 
    line = gets = arg;
  }
  else if( _args->size() > 1 ) { // check for abbreviated version of -q/-g dbrec that lacks: -:
    string dbrec = (*_args)[1];
    if( dbrec.find(":") != string::npos ) { // likely to be a channel name
      _quiet = true;
      line = gets = dbrec;
    }
  }
  else if( _args->size() > 2 ) { // check for abbreviated version of -q/-g dbrec that lacks: -:
    string dbrec = (*_args)[2];
    if( dbrec.find(":") != string::npos ) { // likely to be a channel name
      _quiet = true;
      line = gets = dbrec;
    }
  }

  if( line != "" ) {
    string get= myname + " -g " + line;
    string *vnamelist= 0, *vvallist= 0;
    int num = UFCAwrap::cmdLine(get, vnamelist, vvallist, 0); // alloc. & return  name & val lists
    if( num <= 0 ) {
      clog << "UFCAget::exec> ?no channel name?"<<endl;
      return 0;
    }
    vector <string> vals;
    for( int n = 0; n < num ; ++n ) {
      vcnt = getArray(vnamelist[n], vals);
      if( vcnt == 0 ) cout<<"UFCAget> failed "<<vnamelist[vcnt]<<endl;
      for( int i = 0; i<vcnt; ++i ) {
        if( _quiet )
          cout<<vals[i]<<endl;
	else
          cout<<"UFCAget> "<<vnamelist[n]<<"='" <<vals[i]<<"'"<<endl;
      }
    }
    return 0;
  }

  while ( !cin.eof() ) {
    if( _verbose ) clog<<"UFCAget> waiting for input..." << endl;
    if( gets != line ) {
      getline(cin, line);
    }
    else if( line.find("quit") == 0 || line.find("exit") == 0) {
      if( _verbose ) clog<<"UFCAputarr> quit..."<<endl;
      return 0;
    }
    string get= myname + " -g " + line;
    if( line == "" || line.find("quit") == 0 || line.find("exit") == 0 ) {
      if( _verbose ) clog<<"UFCAget> quit..."<<endl;
      exit(0);
    }
    if( _verbose ) clog<<"UFCAget> '" << line << "'" << endl;
    if( line != "" ) {
      string *vnamelist= 0, *vvallist= 0;
      int num = UFCAwrap::cmdLine(get, vnamelist, vvallist, 0); // alloc. & return  name & val lists
      if( num <= 0 ) {
	clog << "UFCAget::exec> ?no channel name?"<<endl;
	continue;
      }
      vector <string> vals;
      for( int n = 0; n < num ; ++n ) {
        vcnt = getArray(vnamelist[n], vals);
	if( vcnt == 0 ) cout<<"UFCAget> failed "<<vnamelist[vcnt]<<endl;
        for( int i = 0; i<vcnt; ++i ) {
          if( _quiet )
            cout<<vals[i]<<endl;
	  else
            cout<<"UFCAget> "<<vnamelist[n]<<"='" <<vals[i]<<"'"<<endl;
	}
      }
    }
    if( gets == line ) // assume -g option, all done
      return 0;
  }
  return 0;
}

int main(int argc, char** argv) {
  UFCAget::main(argc, argv);
}

#endif //  __UFCAget_cc__
