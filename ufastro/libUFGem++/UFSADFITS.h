#if !defined(__UFSADFITS_h__)
#define __UFSADFITS_h__ "$Id: UFSADFITS.h,v 0.5 2005/08/18 04:43:10 hon Exp $"
#define __UFSADFITS_H__(arg) const char arg##SADFITS_h__rcsId[] = __UFSADFITS_h__;
 
#include "UFCAwrap.h"
#include "UFFITSheader.h"

struct UFSADFITS : public UFCAwrap, public UFFITSheader {
  /// create instrument FITS primary and secondary headers from EPICS SAD
  inline UFSADFITS(const string& instrum= "flam") : UFCAwrap(0, 0), UFFITSheader()
    { _instrum = new string(instrum); _sad = new std::map< string, string >; initSADHash(*_sad); }
  inline virtual ~UFSADFITS() { delete _instrum; delete _sad; _instrum = 0; _sad = 0; }

  int fetchAllSAD(std::map< string, string >& sad, const string& instrum= "flam");
  UFStrings* fitsHeader(std::map< string, string >& sad, const string& datalabel= "Test");

  static int initSADHash(std::map< string, string >& sad, const string& instrum= "flam");
  inline static int initSADHash(const string& instrum= "flam")
    { std::map< string, string > sad; return initSADHash(sad, instrum); }

  // any agent may use these to set a default list of all sads:
  static int allSAD(std::vector< string >& sad, const string& instrum= "flam");

  // any agent may use these to set a default list of observation sads:
  static int obsSAD(std::vector< string >& sad, const string& instrum= "flam");

  // portescap motor indexor agent may use this to set its default list of sads:
  // also defines datumcnt rec (first one in list!)
  static int ccSAD(std::vector< string >& sad, const string& instrum= "flam");

  // mce4 detector control agent may use this to set its default list of sads:
  static int dcSAD(std::vector< string >& sad, const string& instrum= "flam");

  // ec status is distributed across multiple agents; agentname disambiguates lists
  static int ecSAD(std::vector< string >& sad, const string& agentname, const string& instrum= "flam");

  static string* _instrum;
  static std::map< string, string >* _sad;
  static std::vector< string >* _ccsad;
  static std::vector< string >* _ccsymbarcdsad;
  static std::vector< string >* _dcsad;
  static std::vector< string >* _ecls218sad;
  static std::vector< string >* _ecls33xsad;
  static std::vector< string >* _ecpfvacsad;
  static std::vector< string >* _obssad;
  static std::vector< string >* _eclvdtsad;
  static string* _ecplcsad;
};  

#endif // __UFSADFITS_h__
