#!/usr/local/bin/perl
$organization = "University of Florida";
$department = "Department of Astronomy";

# Read in the arguments
read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
  ($name, $value) = split(/=/, $pair);
       
  # Un-Webify plus signs and %-encoding
  $value =~ tr/+/ /;
  $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
  push(@search_tags, $value), next if ($name eq 'search_tags');

  $FORM{$name} = $value
} 

$command = $FORM{'command'};
$query = $FORM{'query'};
$mvmotor = $FORM{'mvmotor'};

if( $command eq "" && $query eq "" && $action eq "" ) {
  &empty_query;
  exit 1;
}

if( $command eq "" ) {
  if( $query ne "" ) {
    $command = $query;
  }
  if( $action ne "" ) {
    $command = $mvmotor;
  }
}

$passwd = $FORM{'passwd'};
if( $passwd ne "gatir" ) {
  &html_header("UFMCE4 Reply:" );
  print "Sorry, bad password, request denied for: $command";
  &html_trailer;
  exit 2;
}

$output = `env LD_LIBRARY_PATH=/usr/local/lib:/opt/EDTpdv /usr/local/uf2001sgi/sbin/ufmotor -port 52004 -raw '"$command"'`;
$output =~ s/\n\r/<br>/g;
$output =~ s/\r\n/<br>/g;

&html_header("UFPortescap Motor Reply:" );

print <<EOF;
<p> $output </p>
EOF

&html_trailer;

# This subroutine takes the document title as a command
# line parameter and adds header information to the top
# of the HTML document to be returned.
sub html_header {
  $document_title = $_[0];
  print "Content-type: text/html\n\n";
  print "<HTML>\n";
  print "<HEAD>\n";
  print "<TITLE>$document_title</TITLE>\n";
  print "</HEAD>\n";
  print '<Body BGColor="#ffffff">';
  print "<H3><u>$document_title</H3></u>\n";
}
 
# This subroutine prints a suitable HTML trailer
sub html_trailer {
  print "<P>\n";
  print "$organization<br>\n";
  print "$department<P>\n";
  print "</body>\n</html>\n";
  exit;
}

sub empty_query {
  &html_header( "No Arguments Given" ) ;
  &html_trailer;
}
