#!/usr/local/bin/perl
my $rcsId = '$Name:  $; $Id: flmconf.pl 14 2008-06-11 01:49:45Z hon $';

use strict;
#use CGI;
#use CGI::Carp qw( fatalsToBrowser );
#my $q = new CGI;

my $ufmotor = "env LD_LIBRARY_PATH=/usr/local/lib:/opt/EDTpdv /usr/local/uf/sbin/ufmotor -port 52004 -raw ";
my $reply = "";
my $flam = "";

# options:
my $command = "";
my $status = "";
my $imgOrdark = "";
my $mos = "";
my $slit = "";
my $filter = "";
my $lyot = "";
my $grism = "";

my %args = @ARGV; # assuming all options are provided in pairs, create hash args
$command = $args{'-cmd'};
$status = $args{'-stat'};
$imgOrdark = $args{'-conf'};
$mos = $args{'-mos'};
$slit = $args{'-slit'};
$filter = $args{'-filt'};
$lyot = $args{'-lyot'};
$grism = $args{'-grism'};

if( !$command && !$status && !$imgOrdark && !$mos && 
    !$slit && !$filter && !$lyot && !$grism ) {
  &usage;
  exit 1;
}

my $output = "Sorry, ambiguous request, please reset & try again.";
if( $command ) {
  $output = raw_cmd( $command ); # execute raw/uninterpretted command
}

if( $status ) {
  $output = query_status( $status ); # all or specific indexor a,b,c,d,e
}

if( $imgOrdark ) {
  $output = config( $imgOrdark ); # image or dark
}

if( $mos ) {
  my %MOS = ("1"=>"1540", "2"=>"2440", "3"=>"3320", "4"=>"4220", "5"=>"5110", "6"=>"6000", 
          "7"=>"6890", "8"=>"7781", "9"=>"8671", "10"=>"5962", "11"=>"10452" );
  $output = mos_select( $MOS{$mos} );
}

if( $slit ) {
  my %SLIT = ("1p"=>"10007", "2p"=>"1083", "3p"=>"10933", "4p"=>"9117", "6p"=>"8226",
	   "12p"=>"7336");
  $output = slit_select( $SLIT{$slit} );
}

if( $filter ) {
  my %FILT = ("j"=>"0", "h"=>"208", "k"=>"416", "hk"=>"624", "jh"=>"832", "dark"=>"1040");
  $output = filter_select( $FILT{$filter} );
}

if( $lyot ) {
  my %LYOT = ("dark1"=>"0", "2.1m"=>"178", "4m"=>"358", "mmt"=>"537", "gem"=>"716", 
	   "dark2"=>"895", "dark3"=>"1074");
  $output = lyot_select( $LYOT{$lyot} );
}

if( $grism ) {
  my %GRISM = ("hk"=>"0", "open"=>"250", "dark1"=>"500", "dark2"=>"750", "dark3"=>"1000");
  $output = grism_select( $GRISM{$grism} );
}

$output =~ s/\r//g;
$output =~ s/\n/<br>/g;

print <<EOF;
$output
EOF

sub usage {
  print "usage: flmconf.pl [-cmd command] [-stat a/b/c/d/e/all] [-conf image/dark] [-mos 1/2/3/.../11]\
        [-slit 1p/2p/3p/4p/6p/12p] [-filt j/h/k/hk/jh/dark] [-lyot 2.1m/4m/mmt/gem/dark1/dark2/dark3]\
        [-grism hk/open/dark1/dark2/dark3]\n";
}

###################### flamingos functions ########################

sub raw_cmd {
  my ( $cmd ) = @_;

  $reply = $cmd."\n";
  $flam = `$ufmotor '"$cmd"'`;
  $reply = $reply.$flam;

  return $reply;
}

sub query_status { 
# "all" or "Astatus" or "Bstatus" or "Cstatus" or "Dstatus "or "Estatus"
  my ( $query ) = @_;
  my $m = "";

  $reply = $query." ".$m."\n";

  if( $query eq "Astatus" ) {
    $m = "a";
  }
  if( $query eq "Bstatus" ) {
    $m = "b";
  }
  if( $query eq "Cstatus" ) {
    $m = "c";
  }
  if( $query eq "Dstatus" ) {
    $m = "d";
  }
  if( $query eq "Estatus" ) {
    $m = "e";
  }
  if( $m ne "" ) {
    $flam = `$ufmotor '"$m [ 1990 23"'`;
    $reply = $reply.$flam;
  }
  else {
    $flam = `$ufmotor '"a [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"d [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e [ 1990 23"'`;
    $reply = $reply.$flam;
  }
  return $reply;    
}

sub config {
# "image" or "dark"
  my ( $imgOrdark ) = @_;
  $reply = "Unknown Configuration selection: $imgOrdark";
  if( $imgOrdark eq "image" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+2250"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+250"'`;
    $reply = $reply.$flam;
  }
  elsif( $imgOrdark eq "dark" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"cF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"dF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c+1040"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+500"'`;
    $reply = $reply.$flam;
  }
  return $reply;
}

sub mos_select {
# MOS 1 == "1540", 2 == "2440", 3 == "3320", 4 == "4220", 5 == "5110", 6 == "6000",
# MOS 7 == "6890", 8 == "7781", 9 == "8671", 10 == "9562", 11 == "10452"
  my ( $mos ) = @_;
  $reply = "Unknown MOS selection: $mos";
  if( $mos eq "1540" || $mos eq "2440" || $mos eq "3320" || $mos eq "4220" ||
      $mos eq "5110" || $mos eq "6000" || $mos eq "6890" || $mos eq "7781" ||
      $mos eq "8671" || $mos eq "9562" || $mos eq "10452" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+4500"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b+$mos"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub slit_select {
# slit 2Pix == "1083", 3Pix == "10933", 1Pix == "10007", 4Pix == "9117",
# slit 6Pix == "8226", 12Pix == "7336"
  my ( $slit ) = @_;
  $reply = "Unknown Slit selection: $slit";
  if( $slit eq "1083" || $slit eq "10933" || $slit eq "10007" ||
      $slit eq "9117" || $slit eq "8226" || $slit eq "7336 " ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+6750"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b+$slit"'`;
    $reply = $reply.$flam;
  }
  return $reply;
}


sub filter_select {
# filt J == "0", H == "208", K == "416", HK == "624", JH == "832", Dark == "1040"
  my ( $filt ) = @_;
  $reply = "Unknown Fliter selection: $mos";
  if( $filt eq "0" || $filt eq "208" || $filt eq "416" ||
      $filt eq "624" || $filt eq "832" || $filt eq "1040 " ) {
    $reply= "";
    $flam = `$ufmotor '"cF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c+$filt"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub lyot_select {
# lyot Dark == "0", 2.1m == "178", 4m == "358", MMT == "537",
# lyot Gemini == "716", Dark2 == "895", Dark3 == "1074"
  my ( $lyot ) = @_;
  $reply = "Unknown Lyot selection: $lyot";
  if( $lyot eq "0" || $lyot eq "178" || $lyot eq "358" ||
      $lyot eq "537" || $lyot eq "716"  || $lyot eq "895" || $lyot eq "1074 " ) {
    $reply= "";
    $flam = `$ufmotor '"dF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"d+$lyot"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub grism_select {
# grism HK == "0", Open == "250", Dark == "500", Dark1 == "750", Dark2 == "1000"
  my ( $grism ) = @_;
  $reply = "Unknown Grism selection: $lyot";
  if( $grism eq "0" || $grism eq "250" || $grism eq "500" ||
      $grism eq "750" || $grism eq "1000" ) {
    $reply= "";
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+$grism"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}
