#!/usr/local/bin/perl

$organization = "University of Florida";
$department = "Department of Astronomy";

# Read in the arguments
read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

@pairs = split(/&/, $buffer);
foreach $pair (@pairs)
{
    ($name, $value) = split(/=/, $pair);
       
    # Un-Webify plus signs and %-encoding
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    push(@search_tags, $value), next if ($name eq 'search_tags');

    $FORM{$name} = $value
} 

$command = $FORM{'command'};
if( $command eq "" )
	{
	&empty_query ;
	exit 1 ;
	}

#$input = `./tcs2m '"$command"' 2>/dev/null`;
$input = `ls -alFq`; 
$input =~ s/\n/<br>/g;

#buffer: $buffer<br>
#command: $command<br>

&html_header( "Telescope Reply" ) ;

print <<EOF;

The result of your query <b>$command</b> is:

<p>
$input
</p>

EOF
&html_trailer;

# This subroutine takes the document title as a command
# line parameter and adds header information to the top
# of the HTML document to be returned.
sub html_header   
{
    $document_title = $_[0];
    print "Content-type: text/html\n\n";
    print "<HTML>\n";
    print "<HEAD>\n";
    print "<TITLE>$document_title</TITLE>\n";
    print "</HEAD>\n";
    print "<H3>$document_title</H3>\n";
}
 
sub html_trailer
# This subroutine prints a suitable HTML trailer
{
 
print "<P>\n";
print "$organization<br>\n";
print "$department<P></body>\n";
print "</body>\n</html>\n";
exit;
}

sub empty_query
{
&html_header( "No Arguments Given" ) ;
&html_trailer;
}
