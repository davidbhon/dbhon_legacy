#!/usr/local/bin/perl
#use CGI qw(:standard);
use CGI;
use CGI::Carp qw(fatalsToBrowser);

print $cgi = new CGI;

print $cgi->header( -type => "text/html", -expires => "now", -refresh => "$refresh" );
print $cgi->start_html( -title=> "Test", -bgcolor=> "#ffffff");
print $cgi->start_form();
print $cgi->p("Enter your name: ", $cgi->textfield(-name=>'name',-value=>'unnamed'));
print $cgi->p("Choose the combination: ",
              $cgi->checkbox_group(-name=>'words',
	      -values=>['eenie','meenie','minie','moe'],
	      -defaults=>['eenie','minie']));
print $cgi->p("Indicate your favorite color: ",
	       $cgi->popup_menu(-name=>'color',
	       -values=>['none', 'red','green','blue','chartreuse']));
print $cgi->submit("Go"), $cgi->reset(), $cgi->defaults();
print $cgi->end_form();


print $cgi->hr;

if ( $cgi->param() ) {
  print $cgi->p("Params: ", $cgi->em($cgi->param()));
  print $cgi->p("Your name is", $cgi->em($cgi->param('name')));
  print $cgi->p("The keywords are: ", $cgi->em(join(", ", $cgi->param('words'))));
  print $cgi->p("Your favorite color is ", $cgi->em($cgi->param('color')), $cgi->hr);
}

$error = $cgi->cgi_error;
if ($error) {
  print $cgi->header( -type => "text/html", -expires => "now", -status=>$error);
  print $cgi->start_html( -title=>'Problems');
  print $cgi->h2('Request not processed');
  print $cgi->strong($error); 
  print $cgi->end_html();
  exit 0;
}

print $cgi->end_html();


