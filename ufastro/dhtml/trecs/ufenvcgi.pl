#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufenv;

$ufenv::rcsId = q($Name:  $ $Id: ufenvcgi.pl 14 2008-06-11 01:49:45Z hon $);
$ufenv::cgi = new CGI;
$ufenv::organization = "University of Florida";
$ufenv::department = "Department of Astronomy";
$ufenv::refresh = 30;
$ufenv::doctitle = "TReCS Cryostat Environment";

print $ufenv::cgi->header( -type => "text/html", -expires => "now", -refresh => "$ufenv::refresh" );
print $ufenv::cgi->start_html( -title=> "$ufenv::doctitle", -bgcolor=> "#ffffff" );
($ufenv::s,$ufenv::m,$ufenv::h,$ufenv::md,$ufenv::mo,$ufenv::y,$ufenv::wd,$ufenv::yd,$ufenv::isd) = localtime(time());
$ufenv::y = $ufenv::y + 1900; $ufenv::yd = $ufenv::yd + 1;
if( $ufenv::s < 10 ) { $ufenv::s = "0$ufenv::s"; }
if( $ufenv::m < 10 ) { $ufenv::m = "0$ufenv::m"; }
if( $ufenv::h < 10 ) { $ufenv::h = "0$ufenv::h"; }
$ufenv::output = "$ufenv::y:$ufenv::yd:$ufenv::h:$ufenv::m:$ufenv::s TReCS Environment (30 sec. Updates):\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "==========================================\n";
print $ufenv::cgi->p("$ufenv::output");

$ufenv::run = "env LD_LIBRARY_PATH=/usr/local/uftrecs/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv /usr/local/uftrecs/bin";
$ufenv::gp354 = `$ufenv::run/ufvac -port 52004 -host newton -q -raw rd`;
chomp $ufenv::gp354;
@ufenv::vac = split / /,$ufenv::gp354;
$ufenv::output = "Cryostat Vacuum (Torr):     $ufenv::vac[1]";
print $ufenv::cgi->p("$ufenv::output");
#
$ufenv::ls340 = `$ufenv::run/uflsc -port 52003 -host newton -q -raw 'krdg?a;krdg?b'`;
chomp $ufenv::ls340; chomp $ufenv::ls340;
@ufenv::cryotemp2 = split /;/,$ufenv::ls340;
$ufenv::ls340 = `$ufenv::run/uflsc -port 52003 -host newton -q -raw 'srdg?a;srdg?b'`;
chomp $ufenv::ls340; chomp $ufenv::ls340;
@ufenv::cryosens2 = split /;/,$ufenv::ls340;
$ufenv::output = "Detector Array (Kelvin):           $ufenv::cryotemp2[0] ($ufenv::cryosens2[0] mV)\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Finger (Kelvin):         $ufenv::cryotemp2[1] ($ufenv::cryosens2[1] mV)\n";
print $ufenv::cgi->p("$ufenv::output");
#
$ufenv::ls218 = `$ufenv::run/uflsc -port 52002 -host newton -q -raw 'krdg?0'`;
chomp $ufenv::ls218;
@ufenv::cryotemp8 = split /,/,$ufenv::ls218;
$ufenv::output = "CryoCooler Stage1 (Kelvin):       $ufenv::cryotemp8[0]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = " Cold Plate (Kelvin):       $ufenv::cryotemp8[1]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Grating Turret (Kelvin):       $ufenv::cryotemp8[2]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Passive Shield (Kelvin):      $ufenv::cryotemp8[3]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Slit Wheel Motor (Kelvin):       $ufenv::cryotemp8[4]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Strap (Kelvin):  $ufenv::cryotemp8[5]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Edge (Kelvin):   $ufenv::cryotemp8[6]\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "Cold Surface Center (Kelvin): $ufenv::cryotemp8[7]";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "==========================================\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::cgi->end_html();
