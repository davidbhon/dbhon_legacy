#!/usr/local/bin/perl
use GD::Graph::points;

# use 2axes (plot2axes) for @vac & @cryostg1 and @vac & @coldplt
# use 1axis (plot5) for detarr & coldfng & crystg1 & coldplt & slitwhl
# and another 1axis plot5 for passvsh & etc...
@vac;
@detarr; # ls340[0]
@coldfng; # ls340[1]
@cryostg1; #ls218[0]
@coldplt; # ls218[1]
@slitwhl; # ls218[2]
@passvsh; # ls218[3]
@winturr; # ls218[4]
@coldstrp; # ls218[5]
@coldedg; # ls218[6]
@coldctr; # ls218[7]

$vaccnt = 0; @vacx;
$ls218cnt = 0; @ls218x;
$ls340cnt = 0; @ls340x;

open(LOG, "</data/trecsCryoTestJun15.log"); 

while( <LOG> ) {
  $line = $_; $line =~ s/\r//; $line =~ s/\n//;
  if( index($line,"Vacuum") >= 0 ) {
    push @vacx, "$vaccnt"; $vaccnt++; 
    $v = substr($line, 1+rindex($line,' '));
    #print "(vac) line = $line : v = $v\n";
    if( $v ne "" ) { 
      $not = rindex($v, "9.99E+09");
      if( $not >= 0 ) { $v = "1.00E-03"; }
      push @vac, $v;
    }
    else { # repeat prior value
      $cnt = @vac;
      if( $cnt > 0 ) {
	$v = pop @vac; push @vac, $v; push @vac, $v;
      }
      else {
	$v = "1.00E-03"; push @vac, $v;
      }
    }
  }
  if( index($line,"LakeShore 340") >= 0 && index($line, "elvin") >= 0 ) {
    push @ls340x, "$ls340cnt"; $ls340cnt++;
    $ls340 = substr($line, index($line, '+'));
    $psc =  index($ls340, ';');
    $v1 = substr($ls340, 0, $psc);
    $v2 = substr($ls340, $psc + 1);
    #print "(coldfng) ls340 = $ls340 : v1 = $v1 : v2 = $v2\n";
    if( $v1 ne "" &&  $v2 ne "") {
      if( $v1 > 300 ) { $v1 = 300; } # try to normalize the scale to room temperature max
      if( $v2 > 300 ) { $v2 = 300; } # try to normalize the scale to room temperature max
      push @detarr, $v1;
      push @coldfng, $v2;
    }
    else { # repeat prior value
      $cnt = @detarr;
      if( $cnt > 0 ) {
        $v1 = pop @detarr; push @detarr, $v1; push @detarr, $v1;
      }
      else {
	$v1 = 300; push @detarr, $v1;
      }
      $cnt = @coldfng;
      if( $cnt > 0 ) {
        $v2 = pop @coldfng; push @coldfng, $v2; push @coldfng, $v2;
      }
      else {
	$v2 = 300; push @coldfng, $v2;
      }
    }
  }
  if( index($line,"LakeShore 218") >= 0 && index($line, "elvin") >= 0 ) {
    push @ls218x, "$ls218cnt"; $ls218cnt++;
    $ls218 = substr($line, index($line, '+'));
    $pc = index($ls218, ',');
    $ppc = 0;
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len);
    #print "(cryostg1) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @cryostg1, $v;
    }
    else { # repeat prior value
      $cnt = @cryostg1;
      if( $cnt > 0 ) {
        $v = pop @cryostg1; push @cryostg1, $v; push @cryostg1, $v;
      }
      else {
	$v = 300; push @cryostg1, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    #print "(coldplt) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) { 
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @coldplt, $v;
    }
    else { # repeat prior value
      $cnt = @coldplt;
      if( $cnt > 0 ) {
        $v = pop @coldplt; push @coldplt, $v; push @coldplt, $v;
      }
      else {
	$v = 300; push @coldplt, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v =substr($ls218, $ppc, $len); 
    #print "(slitwhl) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @slitwhl, $v;
    }
    else { # repeat prior value
      $cnt = @slitwhl;
      if( $cnt > 0 ) {
        $v = pop @slitwhl; push @slitwhl, $v; push @slitwhl, $v;
      }
      else {
	$v = 300; push @slotwhl, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    #print "(passvsh) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @passvsh, $v;
    }
    else { # repeat prior value
      $cnt = @passvsh;
      if( $cnt > 0 ) {
        $v = pop @passvsh; push @passvsh, $v; push @passvsh, $v;
      }
      else {
	$v = 300; push @passvsh, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    #print "(winturr) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @winturr, $v;
    }
    else { # repeat prior value
      $cnt = @winturr;
      if( $cnt > 0 ) {
        $v = pop @winturr; push @winturr, $v; push @winturr, $v;
      }
      else {
	$v = 300; push @winturr, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    #print "(coldstrp) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @coldstrp, $v;
    }
    else { # repeat prior value
      $cnt = @coldstrp;
      if( $cnt > 0 ) {
        $v = pop @coldstrp; push @coldstrp, $v; push @coldstrp, $v;
      }
      else {
	$v = 300; push @coldstrp, $v;
      }
    }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    #print "(coldedg) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @coldedg, $v;
    }
    else { # repeat prior value
      $cnt = @coldedg;
      if( $cnt > 0 ) {
        $v = pop @coldedg; push @coldedg, $v; push @coldedg, $v;
      }
      else {
	$v = 300; push @coldedg, $v;
      }
    }

    # final value...
    $ppc = 1 + $pc;
    $v = substr($ls218, $ppc); 
    #print "(coldctr) len = $len, ls218 = $ls218 : v = $v\n";
    if( $v ne "" ) {
      if( $v > 300 ) { $v = 300; } # try to normalize the scale to room temperature max
      push @coldctr, $v;
    }
    else { # repeat prior value
      $cnt = @coldctr;
      if( $cnt > 0 ) {
        $v = pop @coldctr; push @coldctr, $v; push @coldctr, $v;
      }
      else {
	$v = 300; push @coldctr, $v;
      }
    }
  }
}

close(LOG);

print "vac: $vac[0] -- $vac[$vaccnt-1]\n";
print "detarr: $detarr[0] -- $detarr[$ls340cnt-1]\n"; # ls340 1
print "coldfng: $coldfng[0] -- $coldfng[$ls340cnt-1]\n"; # ls340 2
print "cryostg1: $cryostg1[0] -- $cryostg1[$ls218cnt-1]\n"; # ls218 1
print "coldplt: $coldplt[0] -- $coldplt[$ls218cnt-1]\n"; # ls218 2
print "slitwhl: $slitwhl[0] -- $slitwhl[$ls218cnt-1]\n"; # ls218 3
print "passvsh: $passvsh[0] -- $passvsh[$ls218cnt-1]\n"; # ls218 4
print "winturr: $winturr[0] -- $winturr[$ls218cnt-1]\n"; # ls218 5
print "coldstrp: $coldstrp[0] -- $coldstrp[$ls218cnt-1]\n"; # ls218 6
print "coldedg: $coldedg[0] -- $coldedg[$ls218cnt-1]\n"; # ls218 7
print "coldctr: $coldctr[0] -- $coldctr[$ls218cnt-1]\n"; # ls218 8

#$file = "/data/vac" . dateString() . ".png";
#pointPlot(\@vacx, \@vac, 'Time', 'Vacuum Pressure in Torr',
#          'UF TReCS CryoStat Vacuum Pressure', "$file");

#$file = "/data/cryotemp1-5_" . dateString() . ".png";
$file = "/data/cryotemp1-5.png";
if( $ls218cnt < $ls340cnt ) {
  $rx = \@ls218x;
}
else {
  $rx = \@ls340x;
}

pointPlot5($rx, \@detarr, \@coldfng, \@cryostg1, \@coldplt, \@slitwhl, 
	   'Time', 'Temperature (Kelvin)', 
	   'UF TReCS CryoStat Temperatures: Detector, ColdFinger, Stage1, ColdPlate, SlitWheel',
	   "$file");

#$file = "/data/cryotemp6-10_" . dateString() . ".png";
$file = "/data/cryotemp6-10.png";
pointPlot5(\@ls218x, \@passvsh, \@winturr, \@coldstrp, \@coldedg, \@coldctr,
	   'Time', 'Temperature (Kelvin)', 
	   'UF TReCS CryoStat Temperatures: PassiveShield, WindowTurret, ColdStrap, ColdEdge, ColdCenter',
	   $file);

#$file = "/data/vac-stage1" . dateString() . ".png";
$file = "/data/vac-stage1.png";
if( $vaccnt < $ls340cnt ) {
  $rx = \@vacx;
  $xcnt = @$rx;
  print "vac xcnt = $xcnt\n";
}
else {
  $rx = \@ls340x;
  $xcnt = @$rx;
  print "ls340 xcnt = $xcnt\n";
}

pointPlot2axes($rx, \@vac, \@cryostg1, 'Time',
	       'Vacuum Pressure (Torr)', 'Cryo-Stage1 Temperature (Kelvin)',
               'UF TReCS CryoStat Vacuum & Cryo-Stage1 Temperature', $file);

#$file = "/data/vac-coldplate" . dateString() . ".png";
$file = "/data/vac-coldplate.png";
pointPlot2axes($rx, \@vac, \@coldplt, 'Time',
	       'Vacuum Pressure (Torr)', 'Cold Plate Temperature (Kelvin)',
               'UF TReCS CryoStat Vacuum & Cold Plate Temperature', $file);


sub dateString() {
  ($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime(time());
  $y = $y + 1900; $yd = $yd + 1;
  if( $s < 10 ) { $s = "0$s"; }
  if( $m < 10 ) { $m = "0$m"; }
  if( $h < 10 ) { $h = "0$h"; }
  $date = "$y:$yd:$h:$m:$s";
  return $date;
}

sub pointPlot1() {
  local ( $x, $y, $xlabel, $ylabel, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot1> xcnt = $xcnt, ymin: $y->[0]\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y_label => $ylabel,
	   title => $title,
	   two_axes => 0,
	   y_max_value => $y->[0],
	   y_label_skip => 2,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );

  @data = ( [@$x], [@$y] );
  $img = $g->plot(\@data);

  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub pointPlot2() {
  local ( $x, $y1, $y2, $xlabel, $ylabel, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot2> xcnt = $xcnt, y0s: $y1->[0], $y2->[0]\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y_label => $ylabel,
	   title => $title,
	   two_axes => 0,
	   y_max_value => 0.5*($y1->[0] + $y2->[0]),
	   y_label_skip => 2,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );

  @data = ( [@$x], [@$y1], [@$y2] );
  $img = $g->plot(\@data);

  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub pointPlot2axes() {
  local ( $x, $y1, $y2, $xlabel, $y1label, $y2label, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot2axes> xcnt = $xcnt, y0s: $y1->[0], $y2->[0]\n"; 
  $y1cnt = @$y1; $y2cnt = @$y2;
  print "pointPlot5> y1cnt= $y1cnt, y2cnt= $y2cnt, yFs: $y1->[$y1cnt-1], $y2->[$y2cnt-1]\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y1_label => $y1label,
	   y2_label => $y2label,
	   title => $title,
	   two_axes => 1,
	   y1_max_value => $y1->[0],
	   y2_max_value => $y2->[0],
	   y1_label_skip => 1,
	   y2_label_skip => 1,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );
  $xcnt--;
  @yA = @$y1[0..$xcnt]; $a = @yA;
  @yB = @$y2[0..$xcnt]; $b = @yB;
  print "pointPlot5> a= $a, b= $b, yA0= $yA[0], yAF= $yA[$a-1], yB0= $yB[0], yBF= $yB[$b-1]\n";
  @data = ( [@$x], [@yA], [@yB] );
#  @data = ( [@$x], [@$y1[0..$xcnt]], [@$y2[0..$xcnt]] );
#  @data = ( [@$x], [@$y1], [@$y2] );
  $img = $g->plot(\@data);
  $datacnt = @data;
  print "pointPlot2axes> datacnt = $datacnt\n"; 
  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub pointPlot3() {
  local ( $x, $y1, $y2, $y3, $xlabel, $ylabel, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot3> xcnt = $xcnt, y0s: $y1->[0], $y2->[0], $y3->[0]\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y_label => $ylabel,
	   title => $title,
	   two_axes => 0,
	   y_max_value => 0.33*($y1->[0] + $y2->[0] + $y3->[0]),
	   y_label_skip => 2,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );

  @data = ( [@$x], [@$y1], [@$y2], [@$y3] );
  $img = $g->plot(\@data);

  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub pointPlot4() {
  local ( $x, $y1, $y2, $y3, $y4, $xlabel, $ylabel, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot4> xcnt = $xcnt, y0s: $y1->[0], $y2->[0], $y3->[0], $y4->[0]\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y_label => $ylabel,
	   title => $title,
	   two_axes => 0,
	   y_max_value => 0.25*($y1->[0] + $y2->[0] + $y3->[0] + $y4->[0]),
	   y_label_skip => 2,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );

  @data = ( [@$x], [@$y1], [@$y2], [@$y3], [@$y4] );
  $img = $g->plot(\@data);

  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub pointPlot5() {
  local ( $x, $y1, $y2, $y3, $y4, $y5, $xlabel, $ylabel, $title, $file ) = @_;
  $xcnt = @$x;
  print "pointPlot5> xcnt = $xcnt, y0s: $y1->[0], $y2->[0], $y3->[0], $y4->[0], $y5->[0]\n";
  $y1cnt = @$y1; $y2cnt = @$y2; $y3cnt = @$y3; $y4cnt = @$y4; $y5cnt = @$y5;
  #print "pointPlot5> y1cnt= $y1cnt, y2cnt= $y2cnt, y3cnt= $y3cnt, y4cnt= $y4cnt, y5cnt= $y5cnt\n"; 
  $g = new GD::Graph::points(600, 400);
  $g->set( x_label => $xlabel,
	   y_label => $ylabel,
	   title => $title,
	   two_axes => 0,
	   y_max_value => 0.20*($y1->[0] + $y2->[0] + $y3->[0] + $y4->[0] + $y5->[0]),
	   y_label_skip => 1,
	   x_label_skip => $xcnt/10,
	   x_labels_vertical => 0,
	   x_label_position => 1/2,
	   markers => [ 1, 8 ],
	   marker_size => 0,
	   transparent => 1,
	   t_margin => 1, 
	   b_margin => 1, 
	   l_margin => 1, 
	   r_margin => 1 );
  $xcnt--;
#  @data = ( [@$x], [@$y1[0..$xcnt]], [@$y2[0..$xcnt]], [@$y3[0..$xcnt]], [@$y4[0..$xcnt]], [@$y5[0..$xcnt]] );
  @data = ( [@$x], [@$y1], [@$y2], [@$y3], [@$y4], [@$y5] );
  $img = $g->plot(\@data);

  $file = ">".$file;
  open(IMG, $file);
  binmode IMG;
  print IMG $img->png;
  close(IMG);
}

sub plotVacuum() {
  $gvac = new GD::Graph::points(600, 400);
  $gvac->set( x_label => 'Time',
	      y_label => 'Vacuum Pressure in Torr',
	      title => "UF TReCS CryoStat Vacuum Pressure",
              two_axes => 0,
	      y_max_value => $vac[0],
	      y_label_skip => 1,
	      x_label_skip => @vac/10,
	      x_labels_vertical => 0,
	      x_label_position => 1/2,
	      markers => [ 1, 8 ],
	      marker_size => 0,
	      transparent => 1,
	      t_margin => 1, 
	      b_margin => 1, 
	      l_margin => 1, 
	      r_margin => 1 );

  @data = ( [q(@vacx)], [@vac] );
  $imgvac = $gvac->plot(\@data);
  open(IMG, ">/data/vac.png");
  binmode IMG;
  print IMG $imgvac->png;
  close(IMG);
}

sub plotColdFinger() {
$gcoldfng = new GD::Graph::points(600, 400);
$gcoldfng->set( x_label => 'Time',
	        y_label => 'Detector Array & Cold Finger Temperatures in Kelvin',
	        title => 'UF TReCS CryoStat Detector Array & Cold Finger Temperatures',
                two_axes => 0,
	        y_max_value => $coldfng[0],
	        y_label_skip => 1,
	        x_label_skip => @coldfng/10,
	        x_labels_vertical => 0,
	        x_label_position => 1/2,
	        markers => [ 1, 8 ],
	        marker_size => 0,
	        transparent => 1,
	        t_margin => 1, 
	        b_margin => 1, 
	        l_margin => 1, 
	        r_margin => 1 );

  @data = ( [q(@ls340x2)], [@coldfng] );
  $imgcoldfng = $gcoldfng->plot(\@data);
  open(IMG, ">/data/coldfng.png");
  binmode IMG;
  print IMG $imgcoldfng->png;
  close(IMG);
}

sub plotColdPlate() {
$gcoldplt = new GD::Graph::points(600, 400);
$gcoldplt->set( x_label => 'Time',
	        y_label => 'Cold Plate Temperature in Kelvin',
	        title => 'UF TReCS CryoStat Cold Plate Temperature',
                two_axes => 0,
	        y_max_value => $coldplt[0],
	        y_label_skip => 1,
	        x_label_skip => @coldplt/10,
	        x_labels_vertical => 0,
	        x_label_position => 1/2,
	        markers => [ 1, 8 ],
	        marker_size => 0,
	        transparent => 1,
	        t_margin => 1, 
	        b_margin => 1, 
	        l_margin => 1, 
	        r_margin => 1 );

  @data = ( [q(@ls218x2)], [@coldplt] );
  $imgcoldplt = $gcoldplt->plot(\@data);
  open(IMG, ">/data/coldplt.png");
  binmode IMG;
  print IMG $imgcoldplt->png;
  close(IMG);
}

