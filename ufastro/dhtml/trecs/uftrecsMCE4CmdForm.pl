#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufmcecmd;

$ufmcecmd::rcsId = q($Name:  $ $Id: uftrecsMCE4CmdForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufmcecmd::cgi = new CGI;
$ufmcecmd::organization = "University of Florida";
$ufmcecmd::department = "Department of Astronomy";
$ufmcecmd::refresh = 10;
$ufmcecmd::doctitle = "TReCS Cryostat Environment";

print $ufmcecmd::cgi->header( -type => "text/html", -expires => "now");
print $ufmcecmd::cgi->start_html( -title=> "$ufmcecmd::doctitle", -bgcolor=> "#ffffff" );
($ufmcecmd::s,$ufmcecmd::m,$ufmcecmd::h,$ufmcecmd::md,$ufmcecmd::mo,$ufmcecmd::y,$ufmcecmd::wd,$ufmcecmd::yd,$ufmcecmd::isd) = localtime(time());
$ufmcecmd::y = $ufmcecmd::y + 1900; $ufmcecmd::yd = $ufmcecmd::yd + 1;
if( $ufmcecmd::s < 10 ) { $ufmcecmd::s = "0$ufmcecmd::s"; }
if( $ufmcecmd::m < 10 ) { $ufmcecmd::m = "0$ufmcecmd::m"; }
if( $ufmcecmd::h < 10 ) { $ufmcecmd::h = "0$ufmcecmd::h"; }
$ufmcecmd::output = "$ufmcecmd::y:$ufmcecmd::yd:$ufmcecmd::h:$ufmcecmd::m:$ufmcecmd::s TReCS Environment (10sec. Updates):\n";
print $ufmcecmd::cgi->p("$ufmcecmd::output");
$ufmcecmd::output = "==========================================\n";
print $ufmcecmd::cgi->p("$ufmcecmd::output");
$ufmcecmd::cgi->end_html();
