#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use GD::Graph::linespoints;
use GD::Graph::Error;
package ufenv;

$ufenv::rcsId = q($Name:  $ $Id: ufenvplotcgi.pl 14 2008-06-11 01:49:45Z hon $);
$ufenv::cgi = new CGI;
$ufenv::organization = "University of Florida";
$ufenv::department = "Department of Astronomy";
$ufenv::refresh = 10;
$ufenv::doctitle = "TReCS Cryostat Environment";
$ufenv::run = "env LD_LIBRARY_PATH=/usr/local/uf/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv /usr/local/uf/bin";

plot_env(100);

sub plot_env {
  $ufenv::cnt = shift;
  $ufenv::title = "Cold Plate last 100 readings";
  $ufenv::ls218 = `$ufenv::run/uflsc -port 52002 -host newton -q $ufenv::cnt`;
  @ufenv::ls218K = split /\n/,$ufenv::ls218;
  chomp @ufenv::ls218K;
  @ufenv::hist;
  @ufenv::xaxis;
  $ufenv::xval = 0;
  foreach $ufenv::k8 (@ufenv::ls218K) {
    $ufenv::k = substr($ufenv::k8, 0, index($ufenv::k8,","));
    if( $ufenv::k != "" ) { push(@ufenv::hist, $ufenv::k); push(@ufenv::xaxis,$ufenv::xval++); }
  }
  print "@ufenv::xaxis\n";
  print "@ufenv::hist\n";
#  @ufenv::data = ( "@ufenv::xaxis", @ufenv::hist );
#  @ufenv::data = ( @ufenv::xaxis, @ufenv::hist );
  @ufenv::data = ( "@ufenv::hist", @ufenv::hist, @ufenv::hist );
  $ufenv::min = min( @ufenv::hist );
  $ufenv::max = max( @ufenv::hist );
#  $ufenv::graph = new GD::Graph::linespoints(400, 300);
  $GD::Graph::Error::Debug = 10; 
  $ufenv::graph = new GD::Graph::points(400, 300);
  $ufenv::graph->set( title => "$ufenv::title", x_label => "History", y_label => "Kelvin", long_ticks => 1,
		      y_max_value => $ufenv::max, y_min_value => $ufenv::min,
		      y_tick_number => 8, y_label_skip => 2 );
  $ufenv::graph->set_legend("Cold Plate");
  $ufenv::expfmt = $ufenv::graph->export_format;
  $ufenv::img = $ufenv::graph->plot(\@ufenv::data) or die $ufenv::graph::plot->error;
  print "export format = $ufenv::expfmt\n";
  open(IMG, ">/data/ufenv.$ufenv::expfmt") or die $!;
  binmode IMG;
#  print IMG $ufenv::img->$ufenv::expfmt;
  print IMG $ufenv::img->png;
  close(IMG);
  print $ufenv::cgi->header( -type => "image/$ufenv::expfmt", -expires => "now" );
  binmode STDOUT;
#  print $ufenv::img->$ufenv::expfmt;
  print $ufenv::img->$ufenv::png;
}

sub min {
  $ufenv::mn = $_[0];
  foreach $ufenv::m ( @_ ) {
    if( $ufenv::m < $ufenv::mn ) { $ufenv::mn = $ufenv::m; }
  }
  return $ufenv::mn;
}

sub max {
  $ufenv::mx = $_[0];
  foreach $ufenv::m ( @_ ) {
    if( $ufenv::m > $ufenv::mx ) { $ufenv::mx = $ufenv::m; }
  }
  return $ufenv::mx;
}
