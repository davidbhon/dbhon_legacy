#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: F2EnvCtrl.cgi,v 0.7 2004/04/23 19:59:38 hon Exp $';

use lib "$UFINSTALL/perl";
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use EZCA;

$organization = "University of Florida";
$department = "Department of Astronomy";
$refresh = 30;
$doctitle = "UF Flamingos-2 Environment Control";

print $cgi = new CGI;

#print $cgi->header( -type => "text/html", -expires => "now", -refresh => "$refresh" );
print $cgi->header( -type => "text/html", -expires => "now" );
print $cgi->start_html( -title=> $doctitle, -bgcolor=> "#ffffff", -author=>"hon@astro.ufl.edu");
print $cgi->h3("Datum the Temperature Control or Provide New Parameters");

print $cgi->start_form();
print $cgi->p("Enter Epics Instrument name: ", $cgi->textfield(-name=>'instrum',-size=>10,-value=>'flam'),
	      $cgi->br, "First Time (EC/Sys) Init?: ", $cgi->checkbox_group(-name=>'epinit',
#	      -values=>['ECInit','SysInit'], -defaults=>['ECInit']));
	      -values=>['ECInit','SysInit']));

print $cgi->hr;

print $cgi->table({-border=>0}, $cgi->Tr({-align=>CENTER,-valign=>TOP},
		  [$cgi->th(['Datum MOS/Camera:', $cgi->popup_menu(-name=>'datum',
	           -values=>['Neither MOS nor Camera', 'Both MOS & Camera', 'Camera Only', 'MOS Only'])])]));

print $cgi->hr;

print $cgi->table({-border=>0}, $cgi->caption($cgi->strong('F2 Environment Control Parameter Input')),
		  $cgi->Tr({-align=>CENTER,-valign=>TOP},
		  [$cgi->th(['', 'SetPoint', 'P', 'I', 'D']),
		  $cgi->th('Camera:').$cgi->td([$cgi->textfield(-name=>'setA',-size=>8,-value=>'71.1'),
				      $cgi->textfield(-name=>'PA',-size=>8,-value=>'100'),
				      $cgi->textfield(-name=>'IA',-size=>8,-value=>'500'),
				      $cgi->textfield(-name=>'DA',-size=>8,-value=>'100')]),
		  $cgi->th('MOS:').$cgi->td([$cgi->textfield(-name=>'setB',-size=>8,-value=>'72.2'),
				      $cgi->textfield(-name=>'PB',-size=>8,-value=>'100'),
				      $cgi->textfield(-name=>'IB',-size=>8,-value=>'500'),
				      $cgi->textfield(-name=>'DB',-size=>8,-value=>'100')]),
		  $cgi->th($cgi->hr).$cgi->td([$cgi->hr, $cgi->hr, $cgi->hr, $cgi->hr]),
		  $cgi->th($cgi->reset()).$cgi->td(['', $cgi->defaults(), '', $cgi->submit('Go')])]));

print $cgi->end_form();


print $cgi->hr;

#$cam = undef; $mos = undef;

if ( $cgi->param() ) {
  $instrum = $cgi->param('instrum');
  $initepics = $cgi->param('epinit');
  @kelv = ( $cgi->param('setA'), $cgi->param('setB') );
  @pid1 = ( $cgi->param('PA'), $cgi->param('IA'), $cgi->param('DA') );
  @pid2 = ( $cgi->param('PB'), $cgi->param('IB'), $cgi->param('DB') );
  print $cgi->p("Instrum: $instrum", "Init: $initepics");

  if( $initepics eq 'ECInit') { epicsInit($instrum, "ec:init"); }
  if( $initepics eq 'SysInit') { epicsInit($instrum, "init"); }

  print $cgi->p("Datum: ", $cgi->em($cgi->param('datum')));
  if( $cgi->param('datum') eq 'Both MOS & Camera' ) {
    $cam = 1; $mos = 1; epicsECDatum($instrum);
  }
  if( $cgi->param('datum') eq 'Camera Only' ) {
    $cam = 1; $mos = undef; epicsECDatum($instrum);
  }
  if( $cgi->param('datum') eq 'MOS Only' ) {
    $cam = undef; $mos = 1; epicsECDatum($instrum);
  }
  if( $cgi->param('datum') eq 'Neither MOS nor Camera' ) {
    print $cgi->p("SetPoint Camera: $kelv[0]", "Camera PID: $pid1[0] $pid1[1] $pid1[2]");
    print $cgi->p("SetPoint MOS: $kelv[1]", "MOS PID: $pid2[0] $pid2[1] $pid2[2]");
    $cam = undef; $mos = undef;  epicsECSetup($instrum, \@kelv, \@pid1, \@pid2);
  }

  print $cgi->hr;
}

$error = $cgi->cgi_error;
if ($error) {
  print $cgi->header( -type => "text/html", -expires => "now", -status=>$error);
  print $cgi->start_html( -title=>'Problems');
  print $cgi->h2('Request not processed');
  print $cgi->strong($error);
  print $cgi->end_html();
  exit 0;
}

print $cgi->p("$organization $department, Author: ", $cgi->a({-href=>'mailto:hon@astro.ufl.edu'}, 'hon@astro.ufl.edu'));
print $cgi->p($rcsId);
print $cgi->end_html();

########################################## sub epicsInit() ###########################
sub epicsInit {
$instr = shift;
$init = shift;

$Mark = 0;
$Idle= 0;
$Clear = 1;
$Preset = 2;
$Busy = 2;
$Start = 3;
$Error = 3;
$exposing = 0;
$datuming = 0;
$applying = 0;

$flam2a = "192.168.111.222";
$ENV{'EPICS_CA_ADDR_LIST'} = $flam2a;
#EZCA::AutoErrorMessageOff();
EZCA::SetTimeout(0.02);
EZCA::SetRetryCount(3);

$timeout = EZCA::GetTimeout();
$retry = EZCA::GetRetryCount();

$init = "$instr:$init.host";
$apply = "$instr:apply.dir";

print $cgi->p($apply, $init);

$ini = EZCA::Put($init, "ezcaString", 1, $flam2a);

$app = EZCA::Put($apply, "ezcaInt", 1, $Start);

}

########################################## sub epicsECDatum() ###########################
sub epicsECDatum {
$instr = shift;

$Mark = 0;
$Idle= 0;
$Clear = 1;
$Preset = 2;
$Busy = 2;
$Start = 3;
$Error = 3;
$exposing = 0;
$datuming = 0;
$applying = 0;

$flam2a = "192.168.111.222";
$ENV{'EPICS_CA_ADDR_LIST'} = $flam2a;
#EZCA::AutoErrorMessageOff();
EZCA::SetTimeout(0.02);
EZCA::SetRetryCount(3);

$timeout = EZCA::GetTimeout();
$retry = EZCA::GetRetryCount();
$apply = "$instr:apply.dir";

if( $cam ) {
  print $cgi->p($apply, "$instr:ec:datum.Camera");
  $cam = EZCA::Put("$instr:ec:datum.Camera", "ezcaInt", 1, $cam);
}
if( $mos ) {
  print $cgi->p($apply, "$instr:ec:setup.MOS");
  $mos = EZCA::Put("$instr:ec:datum.MOS", "ezcaInt", 1, $mos);
}

$app = EZCA::Put($apply, "ezcaInt", 1, $Start);

}

########################################## sub epicsECSetup() ###########################
sub epicsECSetup {
$instr = shift;
# the following arrays are passed by ref:
$r = shift;
@kelv = @$r;
$r = shift;
@pid1 = @$r;
$r = shift;
@pid2 = @$r;

$Mark = 0;
$Idle= 0;
$Clear = 1;
$Preset = 2;
$Busy = 2;
$Start = 3;
$Error = 3;
$exposing = 0;
$datuming = 0;
$applying = 0;

$flam2a = "192.168.111.222";
$ENV{'EPICS_CA_ADDR_LIST'} = $flam2a;
#EZCA::AutoErrorMessageOff();
EZCA::SetTimeout(0.02);
EZCA::SetRetryCount(3);

$timeout = EZCA::GetTimeout();
$retry = EZCA::GetRetryCount();
$apply = "$instr:apply.dir";

print $cgi->p($apply, "$instr:ec:setup.kelvin1= $kelv[0]", "$instr:ec:setup.kelvin2= $kelv[1]");
print $cgi->p("$instr:ec:setup.P1= $pid1[0]", "$instr:ec:setup.I1= $pid1[1]", "$instr:ec:setup.D1= $pid1[2]");
print $cgi->p("$instr:ec:setup.P2= $pid2[0]", "$instr:ec:setup.I2= $pid2[1]", "$instr:ec:setup.D2= $pid2[2]");

$cam = EZCA::Put("$instr:ec:setup.kelvin1", "ezcaDouble", 1, $kelv[0]);
$cam = EZCA::Put("$instr:ec:setup.P1", "ezcaInt", 1, $pid1[0]);
$cam = EZCA::Put("$instr:ec:setup.I1", "ezcaInt", 1, $pid1[1]);
$cam = EZCA::Put("$instr:ec:setup.I1", "ezcaInt", 1, $pid1[2]);

$mos = EZCA::Put("$instr:ec:setup.kelvin2", "ezcaDouble", 1, $kelv[1]);
$mos = EZCA::Put("$instr:ec:setup.P2", "ezcaInt", 1, $pid2[0]);
$mos = EZCA::Put("$instr:ec:setup.I2", "ezcaInt", 1, $pid2[1]);
$mos = EZCA::Put("$instr:ec:setup.I2", "ezcaInt", 1, $pid2[2]);

$app = EZCA::Put($apply, "ezcaInt", 1, $Start);

}
