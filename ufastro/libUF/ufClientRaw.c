#if !defined(__UFClientRaw_c__)
#define __UFClientRaw_c__ "$Name:  $ $Id: ufClientRaw.c,v 0.4 2003/12/30 23:09:53 varosi beta $"
static const char rcsIdUFClientRaw[] = __UFClientRaw_c__;

#include "ufClient.h"
__UFClient_H__(ClientRaw_c);

#include "ufLog.h"
__UFLog_H__(ClientRaw_c);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 

#define  MAXNAMLEN  512
#define  MSG_WAITALL    0x40    /* wait for full request or error in recv (ufRecv) */

#else

#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "netdb.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "inttypes.h"
#include "math.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "sys/socket.h"
#include "netinet/in.h" /* IP_ */
#include "string.h"

#ifdef __APPLE__
#include "sys/ioctl.h"
#endif

#ifdef LINUX
#include "sys/ioctl.h"
#include "asm/ioctls.h" /* FIONREAD */
#include "netinet/tcp.h" /* TCP_ */
 #if !defined(MSG_WAITALL)
  #define MSG_WAITALL 0x100
 #endif
#endif

#ifdef SOLARIS
#include "sys/filio.h" /* FIONREAD */
#include "xti_inet.h" /* TCP_ */
#endif

#endif

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
#define LittleEndian
#endif

static int maxTry = 70;       /* max # of checks for socket data available in ufRecv*/
static int minTry = 3;        /* min # of checks for socket data available in ufRecv*/
static int maxTrySave = 70;   /* NOTE: anything less than 0.3 secs may not be reliable */
static float sleeptime = 0.1; /* default sleep time (seconds) between each check */
static int TotAvailChecks=0;  /* count total # checks for socket data available */
static int Nrecvs = 0;        /* count # times ufRecv is called */
static int MaxAttempts = 64;  /*max # of attempts to recv/send on a socket */

static char _UFerrmsg[MAXNAMLEN + 1];

static char _UFIPAddr[] = "000.000.000.000";

static int _ufmin2i(int a, int b) { return ((a < b) ? a : b); }
static int _ufmax2i(int a, int b) { return ((a > b) ? a : b); }

/***************************** basic os functions **************************/

/* this can be used for sockets or fifos */

int ufAvailable(int fd)
{
  int retval= 0, stat;
  
  stat = ioctl(fd, FIONREAD, &retval);
  
  if( stat != 0 ) {
    sprintf(_UFerrmsg,"ufAvailable> fd= %d, error= %s", fd, strerror(errno) );
    _uflog(_UFerrmsg);
    return(-1);
  }
  return retval;
}/*--------------------------------------------------------------------------*/

int ufFifoAvailable(const char* fifoname)
{
  int fd, retval= 0;
  
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  fd = open(fifoname, O_RDONLY | O_NONBLOCK, 0777);
#else
  fd = open(fifoname, O_RDONLY | O_NONBLOCK);
#endif
  
  if( fd >= 0 ) {
    int stat = ioctl(fd, FIONREAD, &retval);
    close(fd);
    if( stat < 0 )
      retval = stat;
    else
      return retval;
  }
  sprintf(_UFerrmsg,"ufFifoAvailable> fd= %d, error= %s", fd, strerror(errno) );
  _uflog(_UFerrmsg);

  return retval;
}/*--------------------------------------------------------------------------*/

const char* ufHostTime(char* tz) {
  static char *s=0, ts[] = "::yyyy:ddd:hh:mm:ss.uuuuuu";
  struct tm *t, tbuf;
  struct timespec tspec;
  time_t sec;
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  int rv;
#endif

#if defined(SOLARIS)
  int stat = clock_gettime(CLOCK_REALTIME, &tspec);
  if( stat != 0 ) _uflog("ufHostTime> failed to get system time");
  sec = tspec.tv_sec;
#else
  sec = time(0);
#endif

  if( tz == 0 ) {
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
    rv = localtime_r(&sec, &tbuf);
    t = &tbuf;
#else
    t = localtime_r(&sec, &tbuf);
#endif
    tz = getenv("TZ");
  }
  else if( strcmp("utc", tz) == 0 || strcmp("UTC", tz) == 0 ||
           strcmp("gmt", tz) == 0 || strcmp("GMT", tz) == 0 ) { /* use localtime */
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
    rv = localtime_r(&sec, &tbuf); /* mt-safe */
    t = &tbuf;
#else
    t = localtime_r(&sec, &tbuf); /* mt-safe */
#endif
    tz = getenv("TZ");
  }
  else {
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
    rv = gmtime_r(&sec, &tbuf); /* mt-safe */
    t = &tbuf;
#else
    t = gmtime_r(&sec, &tbuf); /* mt-safe */
#endif
    tz = "UTC";
  }

  if( tz == 0 ) tz = "LOCAL"; /* if this is still empty, don't guess... */

  if( s == 0 )
    s = calloc(1+strlen(tz)+strlen(ts), sizeof(char));

  sprintf(s, "%s::%04d:%03d:%02d:%02d:%02d.%06d", tz,
          1900+t->tm_year, t->tm_yday, t->tm_hour,
	  t->tm_min, t->tm_sec, (int)tspec.tv_nsec/1000);

  return s;
}/*--------------------------------------------------------------------------*/

const char* ufHostName() {
  static char host[MAXNAMLEN + 1];
  memset(host,0,sizeof(host));
  gethostname(host, sizeof(host));
  return host;
}/*--------------------------------------------------------------------------*/

/* posix nanosleep */ 

void ufSleep(float time) {
  struct timespec timeout;
  timeout.tv_sec = (long) floor(time);
  timeout.tv_nsec = (long) floor(999999999*(time-timeout.tv_sec));
  nanosleep( &timeout, 0 );
}/*--------------------------------------------------------------------------*/

/******************************** bsd socket i/o *****************************/

int ufIsIPAddress(const char* host)
{
  unsigned short int count = 0 ;

  for( ; host && *host ; ++host )
    {
      if( '.' == *host )
          ++count;
      else
          if( !isdigit( (int) *host ) ) return 0;
    }

  return (3 == count) ? 1 : 0 ;
}/*--------------------------------------------------------------------------*/

int ufSetSocket(int socFd, struct sockaddr_in *addr, int portNo)
{
  int stat, flags;
  struct linger setLinger;
  int optval = 1;

  memset(addr, 0, sizeof(struct sockaddr_in));
  addr->sin_family = AF_INET;
  addr->sin_port = htons(portNo);

  /* explicitly set socket to blocking */

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  /* flags = u_fcntl( socFd, F_GETFL, 0 ); */
#else
  flags = fcntl( socFd, F_GETFL, 0 );
#endif

  if( flags < 0 ) {
	sprintf( _UFerrmsg, "ufSetSocket> Failed to get sock flags "
		" because %s", strerror( errno ) ) ;
	_uflog( _UFerrmsg ) ;
  }

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  /* flags = u_fcntl( socFd, F_SETFL, flags & ~O_NONBLOCK ) ; */
#else
  flags = fcntl( socFd, F_SETFL, flags & ~O_NONBLOCK ) ;
#endif

  if( flags < 0 ) {
    _uflog("ufSetSocket> failed to set ~O_NONBLOCK");
  }

  setLinger.l_onoff = 0;
  setLinger.l_linger = 0;

  stat = setsockopt( socFd, SOL_SOCKET, SO_LINGER,
		     (char *)&setLinger, sizeof(setLinger));
  /* detect dead connection */
  stat = setsockopt(socFd, SOL_SOCKET, SO_KEEPALIVE,
		    (char *)&optval, sizeof (optval));
  /* immediately deliver msg */
  /* stat = setsockopt(socFd, IPPROTO_TCP, TCP_NODELAY,
                       (char *)&optval, sizeof (optval)); */
  /* allow fast re-binds */
  stat = setsockopt(socFd, SOL_SOCKET, SO_REUSEADDR,
		    (char *)&optval, sizeof (optval)); 

  /* use max. buffer size */
  optval = 1024*1024;
  stat = setsockopt(socFd, SOL_SOCKET, SO_SNDBUF,
		    (char *)&optval, sizeof (optval)); 
  stat = setsockopt(socFd, SOL_SOCKET, SO_RCVBUF,
		    (char *)&optval, sizeof (optval)); 

  if( stat < 0 ) {
    sprintf(_UFerrmsg,"ufSetSocket> failed to set an option "
    		"because %s, errno= %d", strerror( errno ), errno );
    _uflog(_UFerrmsg);
  }
  else {
    _uflog("ufSetSocket> set options succeeded.");
  }

  return stat;
}/*--------------------------------------------------------------------------*/

int ufClose(int socFd) {
  return close(socFd);
}/*--------------------------------------------------------------------------*/

int ufConnect(const char* host, int portNo)
{
  int conStat, socFd;
  struct sockaddr_in addr;
#if !defined(vxWorks) && !defined(VxWorks) && !defined(VXWORKS) && !defined(Vx)
  struct hostent *hostEntry= 0;
  struct in_addr in;
  char **paddr;
#endif

  sprintf(_UFerrmsg,"ufConnect> attempting connect to host: %s, via port: %d", host, portNo);
  _uflog(_UFerrmsg);

  socFd = socket(AF_INET, SOCK_STREAM, 0);

  if( socFd < 0 ) {
    _uflog("ufConnect> unable to create socket!");
    return socFd;
  }

  ufSetSocket(socFd, &addr, portNo); /* allocates _socket.addr */

  if( ufIsIPAddress(host) ){  /* host string is actually IP addr. string... */
    strcpy(_UFIPAddr,host);
  }
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  else {
    _uflog("ufConnect> must specify host by IP address!");
  }
#else
  else {  /* find host in domain and use system call inet_ntoa */

    hostEntry = gethostbyname(host);

    if( hostEntry == NULL ) {
      _uflog("ufConnect> unable to find host name in domain!");
      close(socFd);
      socFd = -1;
      return socFd;
    }
    else _uflog("ufConnect> found host name in domain...");

    paddr = hostEntry->h_addr_list;
    memcpy(&in.s_addr, *paddr, sizeof (in.s_addr));
    sprintf(_UFIPAddr,"%s", inet_ntoa(in));
    sprintf(_UFerrmsg,"ufConnect> host IP address: %s", _UFIPAddr);
    _uflog(_UFerrmsg);
  }
#endif

  addr.sin_addr.s_addr = inet_addr(_UFIPAddr);

  conStat = connect(socFd, (struct sockaddr *) &addr, sizeof(addr));
  
  if( conStat < 0 ){
    sprintf(_UFerrmsg,"ufConnect> unable to connect to %s : %d", host, portNo);
    _uflog(_UFerrmsg);
    close(socFd);
    socFd = -1;
  }
  else {
    sprintf(_UFerrmsg,"ufConnect> connected to %s : %d", host, portNo);
    _uflog(_UFerrmsg);
  }

  return socFd;
}/*--------------------------------------------------------------------------*/

/************* RECEIVE functions: ****************/

int ufRecv( int socFd, unsigned char* rbuf, int buflen )     /* raw bytes recv */
{
  int nbToRecv = buflen;
  int nbrecvd = 0, offset = 0, nbad=0;
  int trys=0;  /* count number of attempts to check for socket data available */
               /* this is limited by global variable: maxTry (set by ufSetTimeout)*/
  int cnt= MaxAttempts;  /* limit number of attempts to read socket */
  int socflags= 0;

  if( buflen <= 0 ) return(0);
  socflags = socflags | MSG_WAITALL;

  if( ufAvailable(socFd) <= 0 ) {
    while( ++trys < maxTry && ufAvailable(socFd) <= 0 ) ufSleep( sleeptime );
  }

  TotAvailChecks += trys;
  Nrecvs++;

  if( trys >= maxTry ) {
    if( maxTry < minTry ) {        /* this was a quick check for unexpected data */
      maxTry = _ufmax2i( minTry, maxTrySave ); /* always reset to reliable value */
      return(0);     /* indicates nothing found in quick check for possible data */
    }
    else if( ufAvailable(socFd) <= 0 ) { /* normal mode so check one last time */
      sprintf(_UFerrmsg,"ufRecv> nothing available on socket %d after %4.1f seconds.",
	      socFd, sleeptime*trys );
      _uflog(_UFerrmsg);
      return(-1); /* indicates nothing found in normal check for expected data */
    }
  }

  /* if maxTry was set very small then reset to previous larger value (for reliability) */
  if( maxTry < minTry ) {
    maxTry = _ufmax2i( minTry, maxTrySave );
  }
  else if( trys > maxTry/2  ) {
    sprintf(_UFerrmsg,"ufRecv> data available after %4.1f seconds...avg.delay=%6.3f sec.",
	                               sleeptime*trys, (sleeptime*TotAvailChecks)/Nrecvs );
    _uflog(_UFerrmsg);
  }

  do {
    nbrecvd = recv( socFd, rbuf + offset, nbToRecv, socflags );
    if( nbrecvd > 0 ) {
      nbToRecv -= nbrecvd;
      offset += nbrecvd;
    } else {
      if( nbrecvd < 0 ) nbad++;
      trys = 0;
      while( trys++ < maxTry && ufAvailable(socFd) <= 0 ) ufSleep( sleeptime );
      TotAvailChecks += trys;
    }
  } while( --cnt > 0 && nbToRecv > 0 && (errno == 0 || errno == EINTR) );

  if( cnt < (MaxAttempts/2) || nbad > (MaxAttempts-cnt)/2 ) {
   sprintf(_UFerrmsg,"ufRecv> recvd from socket %d times (%d bad)",MaxAttempts-cnt,nbad);
   _uflog(_UFerrmsg);
   sprintf(_UFerrmsg, "ufRecv> nbLastRecvd= %d, nbToRecv= %d, error: %s",
                                  nbrecvd, nbToRecv, strerror( errno ) );
   _uflog(_UFerrmsg);
  }

  return( buflen - nbToRecv );
}/*--------------------------------------------------------------------------*/

int ufSetTimeout(float timeout)
{
  if( timeout < 0.0 ) return maxTry;    /* option to check maxTry */
  if( timeout > 100.0 ) timeout = 100.0;
  if( maxTry >= minTry ) maxTrySave = maxTry; /* do not save temporary small setting */
  maxTry = (int)( timeout/sleeptime );
  if( maxTry < 1 )
    maxTry = 1;
  else {
    sprintf(_UFerrmsg,"ufSetTimeout> timeout = %4.1f sec.", maxTry * sleeptime );
    _uflog(_UFerrmsg);
  }
  return maxTry;
}/*--------------------------------------------------------------------------*/

short ufRecvShort(int socFd)
{
  short val=0;
  int nb = ufRecv( socFd, (unsigned char*) &val, sizeof(val) );
  
  if( nb < sizeof(val) ) {
    if( nb == 0 ) return(0); /* means that quick check yielded nothing available */
    _uflog("ufRecvShort> failed to recv value.");
    sprintf( _UFerrmsg, "ufRecvShort> read error: %s", strerror( errno ) ) ;
    _uflog( _UFerrmsg ) ;
    return(-1);
  }   
#if defined(LittleEndian)
  val = ntohs( (const unsigned short)val );
#endif
  return val;
}/*--------------------------------------------------------------------------*/

int ufRecvShorts(int socFd, short* buf, int cnt)
{
  int nb = ufRecv( socFd, (unsigned char*) buf, cnt*sizeof(short) );

  if( nb < cnt*sizeof(short) )
    _uflog("ufRecvShorts> failed to recv all values");

#if defined(LittleEndian)
  while( --cnt >= 0 ) buf[cnt] = ntohs( (const unsigned short)buf[cnt] );
#endif

  if( nb < 0 )
    return nb;
  else
    return (nb/sizeof(short));
}/*--------------------------------------------------------------------------*/

int ufRecvInt(int socFd)
{
  int val= -1;
  int nb = ufRecv( socFd, (unsigned char*) &val, sizeof(val) );
  
  if( nb < sizeof(val) ) {
    if( nb == 0 ) return(0); /* means that quick check yielded nothing available */
    _uflog("ufRecvInt> failed to recv value.");
    sprintf( _UFerrmsg, "ufRecvInt> read error: %s", strerror( errno ) ) ;
    _uflog( _UFerrmsg ) ;
    return(-1);
  }
#if defined(LittleEndian)
  val = (int) ntohl((const unsigned long) val);
#endif

  return val;
}/*--------------------------------------------------------------------------*/

int ufRecvInts(int socFd, int* buf, int cnt)
{
  int nb = ufRecv(socFd, (unsigned char*) buf, cnt*sizeof(int));

  if( nb < cnt*sizeof(int) ) {
    sprintf(_UFerrmsg,"ufRecvInts> recvd %d out of %d expected values", nb/sizeof(int), cnt);
    _uflog( _UFerrmsg ) ;
  }

#if defined(LittleEndian)
  while( --cnt >= 0 ) buf[cnt] = ntohl((const unsigned long) buf[cnt] );
#endif

  if( nb < 0 )
    return nb;
  else
    return (nb/sizeof(int));
}/*--------------------------------------------------------------------------*/

float ufRecvFloat(int socFd)
{
  float val= 0;
  int nb = ufRecv( socFd, (unsigned char*) &val, sizeof(val) );
  
  if( nb < sizeof(val) ) {
    if( nb == 0 ) return(0); /* means that quick check yielded nothing available */
    _uflog("ufRecvFloat> failed to recv value");
    sprintf( _UFerrmsg, "ufRecvFloat> read error: %s", strerror( errno ) ) ;
    _uflog( _UFerrmsg ) ;
  }
#if defined(LittleEndian)
  val = ntohl( (unsigned long) val ) ;
#endif

  return val;
}/*--------------------------------------------------------------------------*/

int ufRecvFloats(int socFd, float* buf, int cnt)
{
#if defined(LittleEndian)
  unsigned long *tmp = (unsigned long*) buf;
#endif

  int nb = ufRecv(socFd, (unsigned char*) buf, cnt*sizeof(float));
  
  if( nb < cnt*sizeof(int) )
    _uflog("ufRecvFloats> failed to recv all values");

#if defined(LittleEndian)
  while( --cnt >= 0 ) tmp[cnt] = ntohl( tmp[cnt] ) ;
#endif

  if( nb < 0 )
    return nb;
  else
    return (nb/sizeof(float));
}/*--------------------------------------------------------------------------*/

int ufRecvCstr(int socFd, char* rbuf, int buflen)   /*recv variable length char. string*/
{
  int nb=0, slen;
  char* tmpbuf=0;
  
  slen = ufRecvInt(socFd);  /* first read the string length */

  if( slen < 0 || slen > USHRT_MAX ) {
    sprintf(_UFerrmsg,"ufRecvCstr> bad string length: %d, on socket: %d.", slen, socFd);
    _uflog(_UFerrmsg);
    return(-1);
  }
  else if( slen == 0 ) {
    sprintf(_UFerrmsg,"ufRecvCstr> zero length string on socket: %d.", socFd);
    _uflog(_UFerrmsg);
    return slen;
  }

  /* ok to read the string */
  nb = 4;
  tmpbuf = calloc( 1+slen, sizeof(char) );  /* alloc & clear string memory */
  
  if( NULL == tmpbuf ) {
      sprintf(_UFerrmsg, "ufRecvCstr> Memory allocation failure" ) ;
      _uflog(_UFerrmsg);
      return(-1);
    }
    
  nb += ufRecv( socFd, tmpbuf, slen );

  /* make sure to clear any old stuff */
  memset(rbuf, 0, buflen);

  /* truncate if tmpbuf is longer than rbuf */
  strncpy(rbuf, tmpbuf, _ufmin2i(buflen, slen));

  free(tmpbuf);
  return nb;
}/*--------------------------------------------------------------------------*/

/* for use with UFStrings: caller must free memory allocated here: */

int ufRecvNewCstr( int socFd, char** rbuf )
{
  int nb=0, slen;
  
  slen = ufRecvInt(socFd);  /* first read the string length */

  if( slen < 0 || slen > USHRT_MAX ) {
    *rbuf = 0;
    sprintf(_UFerrmsg,"ufRecvNewCstr> bad string length: %d, on socket: %d.", slen, socFd);
    _uflog(_UFerrmsg);
    return(-1);
  }
  else if( slen == 0 ) {
    sprintf(_UFerrmsg,"ufRecvNewCstr> zero length string on socket: %d.", socFd);
    _uflog(_UFerrmsg);
    return slen;
  }

  nb = 4;
  *rbuf = calloc( 1+slen, sizeof(char) );  /* alloc memory for string */
  
  if( NULL == *rbuf ) {
    sprintf(_UFerrmsg, "ufRecvNewCstr> Memory allocation failure" );
    _uflog(_UFerrmsg);
    return(-1);
  }

  return ufRecv( socFd, *rbuf, slen ) + nb;
}/*--------------------------------------------------------------------------*/

/********** SEND functions: ***********/

int ufSend( int socFd, const unsigned char* sndbuf, int nb )   /*raw bytes send*/
{
  int nbtosend = nb;
  int nbsent = 0, offset = 0, nbad=0;
  int cnt= MaxAttempts; /* limit number of attempts */
  
  do { nbsent = send( socFd, sndbuf + offset, nbtosend, 0 );
       if( nbsent > 0 ) { nbtosend -= nbsent; offset += nbsent; }
       if( nbsent < 0 ) nbad++;
       if( nbtosend > 0 ) ufSleep( 0.01 );  /* wait just a little */
  } while( (--cnt > 0) &&
	   (nbtosend > 0) &&
	   (errno == 0 || errno == EINTR) );
 
  if( cnt < (MaxAttempts/2) || nbad > 0 ) {
    sprintf(_UFerrmsg,"ufSend> sent to socket %d times (%d bad)", MaxAttempts-cnt, nbad);
    _uflog(_UFerrmsg);
    sprintf( _UFerrmsg, "ufSend> nbLastSent= %d, nbtosend= %d, error: %s",
                                  nbsent, nbtosend, strerror( errno ) );
    _uflog( _UFerrmsg );
  }
  
  return (nb - nbtosend);
}/*--------------------------------------------------------------------------*/

int ufSendCstr(int socFd, const char* cstr)   /* send variable length string */
{
  int nb, slen = strlen(cstr);

  nb = ufSendInt(socFd, slen); /* first send int string length, then send string */
  
  return ufSend( socFd, (const unsigned char*)cstr, slen ) + nb;
}/*--------------------------------------------------------------------------*/

int ufSendShort(int socFd, short val) {
#if defined(LittleEndian)
  val = (unsigned short) htons((const unsigned short) val);
#endif
  return ufSend(socFd, (const unsigned char*)&val, sizeof(val));
}/*--------------------------------------------------------------------------*/

int ufSendShorts(int socFd, short* buf, int cnt)
{
  int nb, nbc = cnt*sizeof(short);

#if defined(LittleEndian)
  int icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = htons( (const unsigned short)buf[icnt] );
#endif

  nb = ufSend(socFd, (const unsigned char*) buf, nbc);
  
  if( nb < nbc ) _uflog("ufSendShorts> failed to send all values");

#if defined(LittleEndian)
  icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = ntohs( (const unsigned short)buf[icnt] );
#endif

  return (nb/sizeof(short));
}/*--------------------------------------------------------------------------*/

int ufSendInt(int socFd, int val) {
#if defined(LittleEndian)
  val = (int) htonl( (const unsigned long)val );
#endif
  return ufSend(socFd, (const unsigned char*)&val, sizeof(val));
}/*--------------------------------------------------------------------------*/

int ufSendInts(int socFd, int* buf, int cnt)
{
  int nb, nbc = cnt*sizeof(int);

#if defined(LittleEndian)
  int icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = htonl( (const unsigned long)buf[icnt] );
#endif

  nb = ufSend(socFd, (const unsigned char*) buf, nbc);
  
  if( nb < nbc ) _uflog("ufSendInts> failed to send all values");

#if defined(LittleEndian)
  icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = ntohl( (const unsigned long)buf[icnt] );
#endif
  return (nb/sizeof(int));
}/*--------------------------------------------------------------------------*/

int ufSendFloat(int socFd, float val) {
#if defined(LittleEndian)
  val = (float) htonl( (unsigned long) val ) ;
#endif
  return ufSend(socFd, (const unsigned char*)&val, sizeof(val));
}/*--------------------------------------------------------------------------*/

int ufSendFloats(int socFd, float* buf, int cnt)
{
  int nb, nbc = cnt*sizeof(int);

#if defined(LittleEndian)
  int icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = (float) htonl( (unsigned long) buf[icnt] ) ;
#endif

  nb = ufSend(socFd, (const unsigned char*) buf, nbc);
  
  if( nb < nbc ) _uflog("ufSendFloats> failed to send all values");

#if defined(LittleEndian)
  icnt = cnt;
  while( --icnt >= 0 ) buf[icnt] = (float) ntohl( (unsigned long) buf[icnt] ) ;
#endif
  return (nb/sizeof(float));
}

#endif /* __UFClientRaw_c__ */
