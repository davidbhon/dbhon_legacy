Building OIWFS DB:

for some reason, some of the applications used in the build process require libstdc++.so.2.10.0.  I was able to symlink /usr/local/lib/libstdc++.so.6 to $UFINSTALL/lib/libstdc++.so.2.10.0 on irflam2a, and the build seemed to work ok.


when renaming, make sure that the top level name is ONLY 2 CHARACTERS!!! (f2, uf, fu, etc)
make sure, when doing search and replace, that you search for "f2:", not "f2:wfs:".  Most places use "f2:wfs:", but there are a couple of places where "f2:" is used, and should be replaced with whatever you want the db to be called  (if you just searched for "f2:wfs:" you wouldn't find the "f2:" referecnes.

USE THE FOLLOWING COMPILER FLAG FOR > 32 Meg of RAM:
-mlongcall


see the following files in ufrcs/flam2.oiwfs:
  ccdhelper
  ccdhelper.exp  -- note, if you get "cannot find executable" or something
		    like that when trying to run my expect scripts, (or perl
		    scripts, for that matter), it could mean that the
		    'expect' (or 'perl') executable wasn't found where it
		    was specified (in the top line of the script using the
		    #! notation)
  save.history
  changing_db_prefix.txt



UFGraphPanel:
javaUFLib/UFGraphPanel.java:
Used by flam2helper
Looks at log files -- /nfs/irdoradus/share/data/environment/current
  (or localhost where ufstart was run from -- /share/data/environment/current)
Issues: doesn't work -- maybe replace with UFPlotPanel
	look at parsing of log file
	auto update?
	GatorPlot range features -- plot all data, zoom in on range

ufrcs/flam2.scripts/starttemperatureloggers.pl:
!!!  should change hard-coded irdoradus stuff!!!
!!!  make clo or just use local fs!!!

WebStuff:
ufrcs/flam2.web/*
	requires the perlGD module to be installed.(does not have to be installed system-wide -- you can tell your perl cgi-script where to look for perl modules:

use lib '/astro/homes/drashkin/local';

)


Issues: consider A) Staying in perl; B) use tomcat; C) php
B or C would not be on polaris!
Sanity check on # of lines
genpage takes input, sends to graphview which outputs image
pretty up graph -- settings, ticks, colors, etc.



Task 0: FJEC - Portable vs VME EPICS database:
EPICSRecs.java - constructs hashmap with values dependent on which type of database.

Will be used to replace any switch/if-else statements i.e.:
      if (EPICS.PORTABLE) { ... } else { ... } (see JPanelIS.java)
All entries in hashmap can update their values
Consider being able to switch between types of databases.


A note on EPICS databases:
(really an electronic circuit not a database! -- get automatic updates on quantities from it)
sad: read-only records such as temperature (from client side)
cad: setup stuff
car: response (3 different states -- idle, busy, error -- 0, 2, 3)
     uses EPICS Monitors instead of whiles --
     monitor notifies when values change
See FJECSubSystemPanel.java: EPICSCarCallback
BUSY -> IDLE: clear CAR_FLAG (000 | 001 = 001, 001 & 110 = 000)
IDLE -> BUSY: bitwise OR (identity) so that it will only re-enable buttons when all monitors show IDLE
ERRORS same as IDLEs for now -- mostly timeouts and don't want buttons disabled



Running Executables

only sbin binaries work on linux, make sure that UFINSTALL/sbin is before UFINSTALL/bin in your path if working on linux.

before issuing any commands that might connect to an epics db, make sure that the EPICS_CA_ADDR_LIST environment variable is set correctly.  If you get a bunch of error messages about connecting to process variables, it could mean that this env var is not set correctly (could also mean that the epics db has crashed).













Make System

don't forget to do a manual 'make .install' in the ufrcs/flam2.epics directory if you plan on using the test epics databases (uffooepicsd, uffuepicsd, etc)

ufrcs/flam2.scripts/UFCA:  the perl module doesn't always build properly.  Note that the UFCA make file is actually in the ufrcs/flam2.scripts dir, not the ufrcs/flam2.scripts/UFCA dir.  This is because the perl module want to use its own Makefile, and doesn't know that we want to build the java version of the UFCA library as well.  










FJEC

Ignore any CAC TCP Errors
EPICS Componenets Color Schemes:
   The color scheme assumes that monitors always trigger when a value is put to a PV, even if the value hasn't changed (ie, you PUT the same value that was there before).

use the "-wfsprefix" flag to change the oiwfs db prefix (make sure that you include the ":wfs:" after the top level name (ie, "f2:wfs:", or "uf:wfs:").

04/11/2006

ufsimfoo   -> start foo database in sim mode
ufstartfjec foo:
all green buttons do a top level apply

Issue: Temperature and Pressure - Pressure - autodetermine micro/milli/torr
and scale

sourcecode core dump:

several monitor callback functions assume that the value returned is an
integer/float number.  In the portable db, to avoid NumberFormatExceptions,
these values are first checked to see if they equal the string "null" before
trying to do a parseInt or parseDouble.  The "null" string is inserted 
by the UFCAToolkit when a MIN_INT is detected.  This behavior (MIN_INT being
used as a 'cleared' or 'null' value) may or may not be present in Roberto's
vme database.

jbInit methods are constructor helpers

fjecFrame:
  nativeProcesses are half-implemented
  topClear -- if CARs are busy when they shouldn't be, this will
	      clear them out.

fjecSubsystem:
  cc: component control
  ec: environment control
  dc: detector control


FJECMotor:
  note that in the portable db, 'shadow' records are added to some of the textfields and labels (motor parameters from the cc:setup cad are being shadowed in the cc:datum cad).  THIS HAS NOT BEEN DONE for the VME db.  Not sure if this is 'shadowing' is going to be implemented by roberto in the vme db, but if it is, then the shadow recs need to be added to the EPICSTextfields and EPICSLabels in the same way (see FJECMotor, around line 220)


Reconfiguring VME guide on 4U high
192.168.111.222 - irflam2a (now f2sparc NOT flam2sparc)
login as f2wfs
cd /export/gemini/f2oiwfs/f2oiwfsccd
startup script = iocStartup
telnet 192.168.111.125 7014
login as ufastro

if kernel booted: c
if not: bootChange

if get 2 lines at a time it is in revraw instead of revtel mode
open web browser to 192.168.111.125 and change to revtel (admin/superuser)

CTRL+x = reboot
CTRL+] = exit


*****

If fu or foo not found:
ln -s ufflam2epicsd uffuepicsd

****
jdd - Regular connects as regular client and only tries to read data when notified by edt daemon
      Replicant -- connects as replication client, blocks on socket


UFCA stuff:
ufca.c
ufca.h
	All functions defined in ufca.h can be called from java UFCA object after library is loaded

	chid = reference to connection to EPICS record (PV)
	subscribe = adds a monitor to a PV

Makefile.j = builds java stuff
-D_REENTRANT flag in makefile defines REENTRANT for preprocessor
if not defined or 0, some code not compiled re: thread safety in connectPV
set to 0 if getting deadlock in UFCA

In UFCAToolkit.java main loop -- checks monitors on each chid and if a monitor has occurred, then we get the value and update all monitorChanged methods.

idxOfThread(): returns index in thread ID array

put methods in ufca.c need to be thread safe

*Make sure swig is installed*

----------------------------------
Starting up everything:
tedtflam (initcam)
ufsimfoo/ufsimfu or ufstart -a (sim or real mode)
ufstartfjec foo: (or flam: or fu:)
ufstartjdd



---------------------------------
FJEC for Kevin
-After agents are running: start fjec

*Observe: Instrument Sequencer Tab -- Init -- Datum -- ReadoutMode=SCI -- Observe

*Low-level Detector Panel: MCE Raw commands
Clear DC -- Init DC
If connected to MCE4, check Lab Def and Datum DC
If sim card is hooked up, don't bother.
NOT TESTED, a couple things (Idle Mode, Int Time) not hooked up to real databases
Enter an Action, ENTER, Apply

*JDD stuff after image is taken*