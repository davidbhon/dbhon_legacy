1.  Starting the JEI Detector Agent

1.	Switch on MCE4 and wait for the boot sequence to finish
2.	Start the detector agent, either by restarting all agents (note ufstop does not always stop the detector 
agent, so use the script below or kill �9 it or use ufstop -9) or using  
ufrestartDCagent �epics trecs
3.	On the JEI mater panel, hit the clear, init then apply buttons (if you're planning to use the 
Master Panel)
4.	Connect to the agent using the connect button on the JEI detector panel
5.	Compute, configure and click the start button to start observing
2.  Starting the Biases

1.	On the JEI, go to the bias panel.
2.	Hit the connect button
3.	Confirm that the array well depth and detector background are set (i.e. deep and 
medium respectively).  Hit the apply button.
4.	Hit the power on box
3.  Setting the vgate

1.	THIS IS NO LONGER NEEDED, BUT LEFT HERE FOR RECORD ONLY.
2.	Telnet to triffid as trecs, usual password
3.	cd uf2001/idlpro/trecs
4.	Type idl, you should get the IDL> prompt
5.	Type @statmce
6.	Type @setvgate

3A.  Setting the PreAmp
1.	Change to the PreAmp tab of the JEI
2.	Hit the panel unlock checkbox
3.	Hit the connect button
4.  Starting the IDL Data Display

1.	ssh onto trifid and set the display env appropriately
2.	Change the host from ufkepler to newton
3.	Type idl_ddc
4.	Wait for all windows to appear
5.	Initiate polling to connect to the detector agent


5.  Running up the Windows Box with Double Sampling

1.	THIS IS NO LONGER NEEDED, BUT LEFT HERE FOR RECORD ONLY.
2.	Start up windows and before starting the acquisition server�
3.	On the desktop start the hyper terminal.
4.	Hit the send text file option.  The file to send it at c:/ and is called 
trecs_default_boot_script_double_sample.txt.
5.	Close the terminal when finished.
6.	Startup the acquisition server as usual and accept all pop-ups.
Start-up Sequence

1.	With current configuration (20020628), 1,2,3,4
6.  Grating Datum'ing

1.	Assuming all other wheels are datumed.
2.	Insert Aperture Wheel Spot Mask
3.	Insert N filter only
4.	Datum Turret
5.	Move -1500
6.	Turn crosshairs on array
7.	Move +497 (this should be short of center)
8.	Move +1 until spots centered.
9.	ORIGIN turret
10.	Now the pull downs for the turret work just fine
7.  Running the Frame Grabber on Dwarf
1.	Login on to the dwarf
2.	In an xterm, ssh trifid
3.	On trifid, type xdwarf (this will pop up an xterm from trifid)
4.	In the trifid xterm, type idl_cdc (this will start frame grabbing software).  
5.	IDL will also start XCD which allow you to change directories.
6.	I have been saving all data in /net/newton/data
8.  To start another IDL session

1.	If the x-windows on the NT box is down then ....
2.	Start Xwin32
3.	Start trifid session
4.	Login in as trecs
5.	In an xterm, type idl (NOT idl_cdc)
6.	Again IDL will start XCD so you can move to the same directory that you are saving to.
7.	To read in a fits file from disk, at the IDL command line, type im = readfits(filename)


A.  Rebooting the EPICS database

1.	Kill other tip sessions using pkill tip
2.	Connect to the com a port using tip coma and wait for the prompt
3.	Type reboot
4.	The database will now reboot automatically, hence the commands below are now redundant but are 
left for posterity.
5.	Wait for ~2 minutes for it to boot
6.	Type cd "/share/local/uf/ppc.epics/bin/svgm5"
7.	Type <startup
B.  Edit & Make Changes to mot_param.txt

1.	Logon to newton
2.	cd uf2002/trecs.epics/pv	
3.	co -l mot_param.txt to rcs -lock checkout the file
4.	xemacs mot_param.txt and edit it
5.	ci -u mot_param.txt to check it back into rcs
6.	cd ..
7.	make newpv (since the trecs epics db runs on the ppc)
8.	That takes care of the jei on newton; you must now do a make installppc anywhere else you want to 
run the jei, and you MUST to a make installppc on 'irnebula' (formerly nebula, but it was hacked and 
is now on the private net)�so�
9.	rlogin irnebula � note that the login is slow due to its failure to nfs mount across the 
private/public subnets... just ^C  a few seconds into the login and you'll be ok
10.	cd uf2002/trecs.epics
11.	Type make newmp
12.	Now reboot the EPICS database (as discussed in A above)
13.	Restart the jei using ufstartjei trecs
14.	Next hit the init followed by the apply button the JEI to get the JEI to reinitialize the epics db
15.	Congratulations, you've updated the record!!!



C.  Useful Tips

1.	To start the JEI, on newton type ufstartjei trecs
2.	To (re)start the motor agents only, on newton type restartmotord
3.	To stop the all agents, on newton type ufstop or ufstop �9
4.	To start all agents (i.e. motor, temp, etc.), on newton type ufstart
5.	There are more than one mot_param.txt files in the usual directory.  The standard use one is m=1 
where the grating positions are at 1st order.  The 2nd is m=0 where the grating positions are in 0th 
order.  Note that to use the 0th order we must input the wavelength of observation to 8.6um and the 
fiducial steps to 6027.

Page 4 of 4; 1/14/2003
