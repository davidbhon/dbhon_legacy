#if !defined(__UFGEMGENSUBCOMMDHS_C__)
#define __UFGEMGENSUBCOMMDHS_C__ "RCS: $Name:  $Id: "
static const char rcsIdufGEMGENSUBCOMMCDHS[] = __UFGEMGENSUBCOMMDHS_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
/* #include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <registryFunction.h>
#include <epicsExport.h>

#include <dbStaticLib.h>
#include <dbBase.h>

#include "flam.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"

static char dhs_DEBUG_MODE[6];

/******************* INAM for expStatusG ******************/
static long ufexpStatusGinit (genSubRecord * pgs)
{
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubDHSPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /* 
   *  Create a private control structure for this gensub record
   */
  /* trx_debug("Creating private structure",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubDHSPrivate *) malloc (sizeof (genSubDHSPrivate));
  if (pPriv == NULL)
    {
      trx_debug ("Can't create private structure", pgs->name, "NONE", "NONE");
      return -1;
    }

  /* 
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
   */

  /* trx_debug("Locating CAR Information",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status)
    {
      trx_debug ("can't locate CAR IVAL field", pgs->name, "NONE", "NONE");
      return -1;
    }

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status)
    {
      trx_debug ("can't locate CAR IMSS field", pgs->name, "NONE", "NONE");
      return -1;
    }

  strcpy (pgs->a, "");
  strcpy (pgs->b, "");
  strcpy (pgs->j, "");
  strcpy (pgs->vala, "");
  strcpy (pgs->valb, "");
  strcpy (pgs->valc, "");
  strcpy (pgs->vald, "");
  strcpy (pPriv->CurrParam.data_set_name, "");
  pPriv->CurrParam.total_frames = -1;
  pPriv->CurrParam.frame_count = -1;
  strcpy (pPriv->dhs_inp.data_set_name, "");
  pPriv->dhs_inp.total_frames = -1;
  pPriv->dhs_inp.frame_count = -1;

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL)
    {
      trx_debug ("can't create a callback", pgs->name, "NONE", "NONE");
      return -1;
    }

  pPriv->pCallback->pRecord = (struct dbCommon *) pgs;

  pPriv->commandState = TRX_GS_DONE;

  pgs->dpvt = (void *) pPriv;

  return OK;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/

/******************* SNAM for expStatusG ******************/
static long ufexpStatusGproc (genSubRecord * pgs)
{

  /* strcpy(pgs->vala,pgs->a) ; strcpy(pgs->valb,pgs->b) ;
     strcpy(pgs->valc,pgs->j) ; */
  /* Gensub Private Structure Declaration */
  char *original;
  char *zz;
  genSubDHSPrivate *pPriv;
  /* Status Variable */
  long status = OK;
  char response[40];
  char temp_inp[40];
  long some_num;
  long ticksNow;

  char *endptr;
  long timeout;
  double numnum;


  pPriv = (genSubDHSPrivate *) pgs->dpvt;
  switch (pPriv->commandState)
    {

    case TRX_GS_INIT:

      break;

      /* 
       *  In idle state we wait for one of the inputs to 
       *  change indicating that a new command has arrived. Status
       *  and updating from the Agent comes during BUSY state.
       */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */
      trx_debug ("Processing from DONE state", pgs->name, "FULL",
		 dhs_DEBUG_MODE);
      trx_debug ("Getting inputs A-F", pgs->name, "FULL", dhs_DEBUG_MODE);
      /* printf("I am in a DONE State from %s
         \n",pgs->name) ; */

      strcpy (pPriv->dhs_inp.data_set_name, pgs->a);
      pPriv->dhs_inp.total_frames = atol (pgs->b);
      if (strcmp (pgs->j, "") != 0)
	{
	  original = pgs->j;
	  zz = pgs->j;
	  strncpy (temp_inp, zz, 39);
	  temp_inp[39] = 0;
	  pPriv->dhs_inp.frame_count = atol (temp_inp);

	}

      /* printf("I have on inp a %s \n", (char *)pgs->a) ;
         printf("inp b %s %ld \n", (char
         *)pgs->b,pPriv->dhs_inp.total_frames) ;
         printf("inp c %s %ld \n", (char
         *)pgs->j,pPriv->dhs_inp.frame_count) ; */

      if ((strcmp (pPriv->dhs_inp.data_set_name, "") != 0) ||
	  (pPriv->dhs_inp.total_frames != -1) ||
	  (pPriv->dhs_inp.frame_count != -1))
	{
	  /* A new command has arrived so set the CAR to
	     busy */
	  /* printf("Woo Hoo.  We have some input \n") ; */

	  strcpy (pPriv->errorMessage, "");
	  strcpy (pPriv->dhs_error_message, "");
	  status = dbPutField (&pPriv->carinfo.carMessage,
			       DBR_STRING, pPriv->errorMessage, 1);
	  if (status)
	    {
	      trx_debug ("can't clear CAR message", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	      return OK;
	    }
	  trx_debug ("Setting the car to BUSY.", pgs->name, "FULL",
		     dhs_DEBUG_MODE);
	  some_num = CAR_BUSY;
	  status = dbPutField (&pPriv->carinfo.carState,
			       DBR_LONG, &some_num, 1);
	  if (status)
	    {
	      trx_debug ("can't set CAR to busy", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	      return OK;
	    }
	  /* printf("the car should be busy now so am I") ; 
	   */
	  pPriv->commandState = TRX_GS_BUSY;

	  /* update the current parameters and output links 
	   */
	  strcpy (pPriv->CurrParam.data_set_name,
		  pPriv->dhs_inp.data_set_name);
	  pPriv->CurrParam.total_frames = pPriv->dhs_inp.total_frames;
	  pPriv->CurrParam.frame_count = pPriv->dhs_inp.frame_count;

	  strcpy (pgs->vala, pgs->a);
	  strcpy (pgs->valb, pgs->b);
	  strcpy (pgs->valc, "PROCESSING");
	  strcpy (pgs->vald, temp_inp);
	  strcpy (pgs->vale, "");
	  strcpy (pgs->valf, "");
	  strcpy (pgs->valg, "");
	  strcpy (pgs->valh, "");
	  strcpy (pgs->vali, "");
	  strcpy (pgs->valj, "");
	  strcpy (pgs->valk, "");
	  strcpy (pgs->vall, "");
	  strcpy (pgs->valm, "");
	  strcpy (pgs->valn, "");
	  strcpy (pgs->valo, "");
	  strcpy (pgs->valp, "");
	  strcpy (pgs->valq, "");
	  strcpy (pgs->valr, "");
	  strcpy (pgs->vals, "");
	  strcpy (pgs->valt, "");
	  strcpy (pgs->valu, "");

	  /* Clear input fields */
	  strcpy ((char *) pgs->a, "");	/* */
	  strcpy ((char *) pgs->b, "");	/* */
	  /* Clear the J Field */
	  strcpy (pgs->j, "");
	  /* Clear the input structure */
	  strcpy (temp_inp, "");
	  pPriv->dhs_inp.frame_count = -1;

	  strcpy (pPriv->dhs_inp.data_set_name, "");
	  pPriv->dhs_inp.total_frames = -1;
	  strcpy (pPriv->errorMessage, "");
	  strcpy (pPriv->dhs_error_message, "");
	  /* establish a timeout CallBack */
	  pPriv->time_out_value = 1800;	/* 30 second to
					   timeout before
					   the next frame
					   count */
	  pPriv->startingTicks = tickGet ();
	  strcpy (pPriv->dhs_error_message, "Frame Count Timed Out");
	  requestCallback (pPriv->pCallback, pPriv->time_out_value);
	}
      break;

    case TRX_GS_SENDING:

      break;

    case TRX_GS_BUSY:
      trx_debug ("Processing from BUSY state", pgs->name, "FULL",
		 dhs_DEBUG_MODE);
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need 
         for 0.5 delay */
      /* establish a CALLBACK */

      strcpy (response, pgs->j);
      if (strcmp (response, "") != 0)
	{
	  /* printf("??????? %s\n",response) ; */
	  trx_debug (response, pgs->name, "MID", dhs_DEBUG_MODE);
	}

      if (strcmp (response, "") == 0)
	{
	  /* is this a CALLBACK from ERROR? */
	  ticksNow = tickGet ();
	  timeout = pPriv->time_out_value;
	  if ((ticksNow >= (pPriv->startingTicks + timeout)) &&
	      (strcmp (pPriv->errorMessage, "CALLBACK") == 0))
	    {

	      /* set Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      /* set the CAR to ERR */
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->dhs_error_message, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	      strcpy (pgs->valc, pPriv->dhs_error_message);
	    }
	  else
	    {
	      /* Quit playing with the buttons. Can't you
	         see I am BUSY? */
	      trx_debug ("Unexpected processing:BUSY", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	    }

	}
      else
	{
	  /* What do we have from the Agent? */
	  /* if Agent sending an ABORT then post it and go
	     to Done State */

	  if (strcmp (response, "ABORT") == 0)
	    {
	      cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */
	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pgs->valc, "ABORTED");
	      some_num = CAR_IDLE;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to IDLE", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	    }
	  else
	    {
	      /* else do an update if you can and exit */
	      numnum = strtod (response, &endptr);
	      if (*endptr != '\0')
		{		/* we do not have a number */
		  /* setup a Callback of 0.5 to post the
		     Error */
		  strcpy (pPriv->dhs_error_message, response);
		  cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */
		  pPriv->startingTicks = tickGet ();
		  pPriv->time_out_value = 30;
		  requestCallback (pPriv->pCallback, pPriv->time_out_value);

		}
	      else
		{		/* update the out link */

		  cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */

		  original = pgs->j;
		  zz = pgs->j;
		  strncpy (temp_inp, zz, 39);
		  temp_inp[39] = 0;
		  pPriv->dhs_inp.frame_count = atol (temp_inp);
		  pPriv->CurrParam.frame_count = pPriv->dhs_inp.frame_count;
		  strcpy (pgs->vald, temp_inp);
		  strcpy (pgs->vale, "");
		  strcpy (pgs->valf, "");
		  strcpy (pgs->valg, "");
		  strcpy (pgs->valh, "");
		  strcpy (pgs->vali, "");
		  strcpy (pgs->valj, "");
		  strcpy (pgs->valk, "");
		  strcpy (pgs->vall, "");
		  strcpy (pgs->valm, "");
		  strcpy (pgs->valn, "");
		  strcpy (pgs->valo, "");
		  strcpy (pgs->valp, "");
		  strcpy (pgs->valq, "");
		  strcpy (pgs->valr, "");
		  strcpy (pgs->vals, "");
		  strcpy (pgs->valt, "");
		  strcpy (pgs->valu, "");

		  /* Check to see if we are done */
		  if (pPriv->dhs_inp.frame_count ==
		      pPriv->CurrParam.total_frames)
		    {
		      pPriv->commandState = TRX_GS_DONE;
		      strcpy (pgs->valc, "COMPLETE");
		      some_num = CAR_IDLE;
		      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to IDLE", pgs->name,
				     "NONE", dhs_DEBUG_MODE);
			  return OK;
			}
		    }

		  pPriv->time_out_value = 1800;	/* 30
						   second
						   to
						   timeout
						   before
						   the next 
						   frame
						   count */
		  pPriv->startingTicks = tickGet ();
		  strcpy (pPriv->dhs_error_message, "Frame Count Timed Out");
		  requestCallback (pPriv->pCallback, pPriv->time_out_value);
		}
	    }
	}
      /* Clear the J Field */
      strcpy (pgs->j, "");
      break;
    }				/* switch
				   (pPriv->commandState) */

  return OK;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/

/******************* INAM for qlkStatusG ******************/
long
ufqlkStatusGinit (genSubRecord * pgs)
{
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubQlkPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
  int i;


  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /* 
   *  Create a private control structure for this gensub record
   */
  /* trx_debug("Creating private structure",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubQlkPrivate *) malloc (sizeof (genSubQlkPrivate));
  if (pPriv == NULL)
    {
      trx_debug ("Can't create private structure", pgs->name, "NONE", "NONE");
      return -1;
    }

  /* 
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
   */

  /* trx_debug("Locating CAR Information",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status)
    {
      trx_debug ("can't locate CAR IVAL field", pgs->name, "NONE", "NONE");
      return -1;
    }

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status)
    {
      trx_debug ("can't locate CAR IMSS field", pgs->name, "NONE", "NONE");
      return -1;
    }

  strcpy (pgs->a, "");
  strcpy (pgs->j, "");
  strcpy (pgs->vala, "");
  strcpy (pgs->valb, "");
  strcpy (pgs->valc, "");
  strcpy (pgs->vald, "");
  strcpy (pgs->vale, "");
  strcpy (pgs->valf, "");
  strcpy (pgs->valg, "");
  strcpy (pgs->valh, "");
  strcpy (pgs->vali, "");
  strcpy (pgs->valj, "");
  strcpy (pgs->valk, "");
  strcpy (pgs->vall, "");
  strcpy (pgs->valm, "");
  strcpy (pgs->valn, "");
  strcpy (pgs->valo, "");
  strcpy (pgs->valp, "");
  strcpy (pgs->valq, "");
  strcpy (pgs->valr, "");
  strcpy (pgs->vals, "");
  strcpy (pgs->valt, "");
  strcpy (pgs->valu, "");

  pgs->noj = 14;

  strcpy (pPriv->CurrParam.data_set_name, "");
  pPriv->CurrParam.total_frames = -1;
  for (i = 0; i < 14; i++)
    {
      pPriv->CurrParam.frame_count[i] = -1;
      pPriv->qlk_inp.frame_count[i] = -1;
    }
  strcpy (pPriv->qlk_inp.data_set_name, "");
  pPriv->qlk_inp.total_frames = -1;

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback",
     pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL)
    {
      trx_debug ("can't create a callback", pgs->name, "NONE", "NONE");
      return -1;
    }

  pPriv->pCallback->pRecord = (struct dbCommon *) pgs;
  pPriv->commandState = TRX_GS_DONE;
  pgs->dpvt = (void *) pPriv;
  return OK;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/

/******************* SNAM for qlkStatusG ******************/
long
ufqlkStatusGproc (genSubRecord * pgs)
{

  /* char yy[40] ; */
  char *original;
  char *zz;
  char temp_inp[14][40];

  /* Gensub Private Structure Declaration */
  genSubQlkPrivate *pPriv;
  /* Status Variable */
  long status = OK;
  int i;
  char response[40];
  long some_num;
  long ticksNow;

  char *endptr;
  long timeout;
  double numnum;

  /* if (strcmp(pgs->j,"") != 0) { original = pgs->j ; zz = 
     pgs->j ; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valh,yy) ; *//* 1 flam:src1 */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->vali,yy) ; *//* 2 flam:ref1 */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valj,yy) ; *//* 3 flam:src2 */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valk,yy) ; *//* 4 flam:ref2 */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->vall,yy) ; *//* 5 flam:accum(sig) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valm,yy) ; *//* 6 flam:sig */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valn,yy) ; *//* 7 flam:accum(src1) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valo,yy) ; *//* 8 flam:accum(ref1) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valp,yy) ; *//* 9 flam:accum(src2) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valq,yy) ; *//* 10 flam:accum(ref2) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valr,yy) ; *//* 11 flam:accum(dif1) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->vals,yy) ; *//* 12 flam:acum(dif2) */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valt,yy) ; *//* 13 flam:dif1 */
  /* zz = zz+40; strncpy(yy,zz,39) ; yy[39] = 0 ;
     strcpy(pgs->valu,yy) ; *//* 14 flam:dif2 */
  /* strcpy(pgs->vala,pgs->a) ; */
  /* } */

  pPriv = (genSubQlkPrivate *) pgs->dpvt;

  switch (pPriv->commandState)
    {

    case TRX_GS_INIT:

      break;


      /* 
       *  In idle state we wait for one of the inputs to 
       *  change indicating that a new command has arrived. Status
       *  and updating from the Agent comes during BUSY state.
       */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */
      trx_debug ("Processing from DONE state", pgs->name, "FULL",
		 dhs_DEBUG_MODE);
      trx_debug ("Getting inputs A-F", pgs->name, "FULL", dhs_DEBUG_MODE);

      strcpy (pPriv->qlk_inp.data_set_name, pgs->a);
      pPriv->qlk_inp.total_frames = atol (pgs->b);
      if (strcmp (pgs->j, "") != 0)
	{
	  original = pgs->j;
	  zz = pgs->j;
	  strncpy (temp_inp[0], zz, 39);
	  temp_inp[0][39] = 0;	/* 1 flam:src1 */
	  zz = zz + 40;
	  strncpy (temp_inp[1], zz, 39);
	  temp_inp[1][39] = 0;	/* 2 flam:ref1 */
	  zz = zz + 40;
	  strncpy (temp_inp[2], zz, 39);
	  temp_inp[2][39] = 0;	/* 3 flam:src2 */
	  zz = zz + 40;
	  strncpy (temp_inp[3], zz, 39);
	  temp_inp[3][39] = 0;	/* 4 flam:ref2 */
	  zz = zz + 40;
	  strncpy (temp_inp[4], zz, 39);
	  temp_inp[4][39] = 0;	/* 5 flam:accum(sig) */
	  zz = zz + 40;
	  strncpy (temp_inp[5], zz, 39);
	  temp_inp[5][39] = 0;	/* 6 flam:sig */
	  zz = zz + 40;
	  strncpy (temp_inp[6], zz, 39);
	  temp_inp[6][39] = 0;	/* 7 flam:accum(src1) */
	  zz = zz + 40;
	  strncpy (temp_inp[7], zz, 39);
	  temp_inp[7][39] = 0;	/* 8 flam:accum(ref1) */
	  zz = zz + 40;
	  strncpy (temp_inp[8], zz, 39);
	  temp_inp[8][39] = 0;	/* 9 flam:accum(src2) */
	  zz = zz + 40;
	  strncpy (temp_inp[9], zz, 39);
	  temp_inp[9][39] = 0;	/* 10 flam:accum(ref2) */
	  zz = zz + 40;
	  strncpy (temp_inp[10], zz, 39);
	  temp_inp[10][39] = 0;	/* 11 flam:accum(dif1) */
	  zz = zz + 40;
	  strncpy (temp_inp[11], zz, 39);
	  temp_inp[11][39] = 0;	/* 12 flam:acum(dif2) */
	  zz = zz + 40;
	  strncpy (temp_inp[12], zz, 39);
	  temp_inp[12][39] = 0;	/* 13 flam:dif1 */
	  zz = zz + 40;
	  strncpy (temp_inp[13], zz, 39);
	  temp_inp[13][39] = 0;	/* 14 flam:dif2 */
	  for (i = 0; i < 14; i++)
	    {
	      pPriv->qlk_inp.frame_count[i] = atol (temp_inp[i]);
	    }
	}

      if ((strcmp (pPriv->qlk_inp.data_set_name, "") != 0) ||
	  (pPriv->qlk_inp.total_frames != -1) ||
	  (pPriv->qlk_inp.frame_count[5] != -1))
	{
	  /* A new command has arrived so set the CAR to
	     busy */
	  strcpy (pPriv->errorMessage, "");
	  status = dbPutField (&pPriv->carinfo.carMessage,
			       DBR_STRING, pPriv->errorMessage, 1);
	  if (status)
	    {
	      trx_debug ("can't clear CAR message", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	      return OK;
	    }
	  trx_debug ("Setting the car to BUSY.", pgs->name, "FULL",
		     dhs_DEBUG_MODE);
	  some_num = CAR_BUSY;
	  status = dbPutField (&pPriv->carinfo.carState,
			       DBR_LONG, &some_num, 1);
	  if (status)
	    {
	      trx_debug ("can't set CAR to busy", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	      return OK;
	    }
	  pPriv->commandState = TRX_GS_BUSY;

	  /* update the current parameters and output links 
	   */
	  strcpy (pPriv->CurrParam.data_set_name,
		  pPriv->qlk_inp.data_set_name);
	  pPriv->CurrParam.total_frames = pPriv->qlk_inp.total_frames;

	  for (i = 0; i < 14; i++)
	    {
	      pPriv->CurrParam.frame_count[i] = pPriv->qlk_inp.frame_count[i];
	    }

	  strcpy (pgs->vala, pgs->a);
	  strcpy (pgs->valb, pgs->b);
	  strcpy (pgs->valc, "PROCESSING");
	  strcpy (pgs->vald, "");
	  strcpy (pgs->vale, "");
	  strcpy (pgs->valf, "");
	  strcpy (pgs->valg, "");
	  strcpy (pgs->valh, temp_inp[0]);
	  strcpy (pgs->vali, temp_inp[1]);
	  strcpy (pgs->valj, temp_inp[2]);
	  strcpy (pgs->valk, temp_inp[3]);
	  strcpy (pgs->vall, temp_inp[4]);
	  strcpy (pgs->valm, temp_inp[5]);
	  strcpy (pgs->valn, temp_inp[6]);
	  strcpy (pgs->valo, temp_inp[7]);
	  strcpy (pgs->valp, temp_inp[8]);
	  strcpy (pgs->valq, temp_inp[9]);
	  strcpy (pgs->valr, temp_inp[10]);
	  strcpy (pgs->vals, temp_inp[11]);
	  strcpy (pgs->valt, temp_inp[12]);
	  strcpy (pgs->valu, temp_inp[13]);

	  /* Clear input fields */
	  strcpy ((char *) pgs->a, "");	/* */
	  strcpy ((char *) pgs->b, "");	/* */
	  /* Clear the J Field */
	  strcpy (pgs->j, "");
	  /* Clear the input structure */
	  for (i = 0; i < 14; i++)
	    {
	      strcpy (temp_inp[i], "");
	      pPriv->qlk_inp.frame_count[i] = -1;
	    }
	  strcpy (pPriv->qlk_inp.data_set_name, "");
	  pPriv->qlk_inp.total_frames = -1;
	  strcpy (pPriv->errorMessage, "");

	  /* establish a timeout CallBack */
	  pPriv->time_out_value = 1800;	/* 30 second to
					   timeout before
					   the next frame
					   count */
	  pPriv->startingTicks = tickGet ();
	  strcpy (pPriv->qlk_error_message, "Frame Count Timed Out");
	  requestCallback (pPriv->pCallback, pPriv->time_out_value);

	}
      break;

    case TRX_GS_SENDING:

      break;

    case TRX_GS_BUSY:
      trx_debug ("Processing from BUSY state", pgs->name, "FULL",
		 dhs_DEBUG_MODE);
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need 
         for 0.5 delay */
      /* establish a CALLBACK */

      strcpy (response, pgs->j);
      if (strcmp (response, "") != 0)
	{
	  /* printf("??????? %s\n",response) ; */
	  trx_debug (response, pgs->name, "MID", dhs_DEBUG_MODE);
	}

      if (strcmp (response, "") == 0)
	{
	  /* is this a CALLBACK from ERROR? */
	  ticksNow = tickGet ();
	  timeout = pPriv->time_out_value;
	  if ((ticksNow >= (pPriv->startingTicks + timeout)) &&
	      (strcmp (pPriv->errorMessage, "CALLBACK") == 0))
	    {
	      /* set Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      /* set the CAR to ERR */
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->qlk_error_message, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	    }
	  else
	    {
	      /* Quit playing with the buttons. Can't you
	         see I am BUSY? */
	      trx_debug ("Unexpected processing:BUSY", pgs->name, "NONE",
			 dhs_DEBUG_MODE);
	    }

	}
      else
	{
	  /* What do we have from the Agent? */
	  /* if Agent sending an ABORT then post it and go
	     to Done State */

	  if (strcmp (response, "ABORT") == 0)
	    {
	      cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */
	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pgs->valc, "ABORTED");
	      some_num = CAR_IDLE;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to IDLE", pgs->name, "NONE",
			     dhs_DEBUG_MODE);
		  return OK;
		}
	    }
	  else
	    {
	      /* else do an update if you can and exit */
	      numnum = strtod (response, &endptr);
	      if (*endptr != '\0')
		{		/* we do not have a number */
		  /* setup a Callback of 0.5 to post the
		     Error */
		  strcpy (pPriv->qlk_error_message, response);
		  cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */
		  pPriv->startingTicks = tickGet ();
		  pPriv->time_out_value = 30;
		  requestCallback (pPriv->pCallback, pPriv->time_out_value);
		}
	      else
		{		/* update the out link */
		  cancelCallback (pPriv->pCallback);	/* the 
							   time 
							   out 
							   callback 
							 */

		  original = pgs->j;
		  zz = pgs->j;
		  strncpy (temp_inp[0], zz, 39);
		  temp_inp[0][39] = 0;	/* 1 flam:src1 */
		  zz = zz + 40;
		  strncpy (temp_inp[1], zz, 39);
		  temp_inp[1][39] = 0;	/* 2 flam:ref1 */
		  zz = zz + 40;
		  strncpy (temp_inp[2], zz, 39);
		  temp_inp[2][39] = 0;	/* 3 flam:src2 */
		  zz = zz + 40;
		  strncpy (temp_inp[3], zz, 39);
		  temp_inp[3][39] = 0;	/* 4 flam:ref2 */
		  zz = zz + 40;
		  strncpy (temp_inp[4], zz, 39);
		  temp_inp[4][39] = 0;	/* 5
					   flam:accum(sig) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[5], zz, 39);
		  temp_inp[5][39] = 0;	/* 6 flam:sig */
		  zz = zz + 40;
		  strncpy (temp_inp[6], zz, 39);
		  temp_inp[6][39] = 0;	/* 7
					   flam:accum(src1) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[7], zz, 39);
		  temp_inp[7][39] = 0;	/* 8
					   flam:accum(ref1) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[8], zz, 39);
		  temp_inp[8][39] = 0;	/* 9
					   flam:accum(src2) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[9], zz, 39);
		  temp_inp[9][39] = 0;	/* 10
					   flam:accum(ref2) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[10], zz, 39);
		  temp_inp[10][39] = 0;	/* 11
					   flam:accum(dif1) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[11], zz, 39);
		  temp_inp[11][39] = 0;	/* 12
					   flam:acum(dif2) 
					 */
		  zz = zz + 40;
		  strncpy (temp_inp[12], zz, 39);
		  temp_inp[12][39] = 0;	/* 13 flam:dif1 */
		  zz = zz + 40;
		  strncpy (temp_inp[13], zz, 39);
		  temp_inp[13][39] = 0;	/* 14 flam:dif2 */
		  for (i = 0; i < 14; i++)
		    {
		      pPriv->qlk_inp.frame_count[i] = atol (temp_inp[i]);
		      pPriv->CurrParam.frame_count[i] =
			pPriv->qlk_inp.frame_count[i];
		    }
		  strcpy (pgs->valh, temp_inp[0]);
		  strcpy (pgs->vali, temp_inp[1]);
		  strcpy (pgs->valj, temp_inp[2]);
		  strcpy (pgs->valk, temp_inp[3]);
		  strcpy (pgs->vall, temp_inp[4]);
		  strcpy (pgs->valm, temp_inp[5]);
		  strcpy (pgs->valn, temp_inp[6]);
		  strcpy (pgs->valo, temp_inp[7]);
		  strcpy (pgs->valp, temp_inp[8]);
		  strcpy (pgs->valq, temp_inp[9]);
		  strcpy (pgs->valr, temp_inp[10]);
		  strcpy (pgs->vals, temp_inp[11]);
		  strcpy (pgs->valt, temp_inp[12]);
		  strcpy (pgs->valu, temp_inp[13]);

		  /* Check to see if we are done */
		  if (pPriv->qlk_inp.frame_count[5] ==
		      pPriv->CurrParam.total_frames)
		    {
		      pPriv->commandState = TRX_GS_DONE;
		      strcpy (pgs->valc, "COMPLETE");
		      some_num = CAR_IDLE;
		      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to IDLE", pgs->name,
				     "NONE", dhs_DEBUG_MODE);
			  return OK;
			}
		    }

		  pPriv->time_out_value = 1800;	/* 30
						   second
						   to
						   timeout
						   before
						   the next 
						   frame
						   count */
		  pPriv->startingTicks = tickGet ();
		  requestCallback (pPriv->pCallback, pPriv->time_out_value);
		  strcpy (pPriv->qlk_error_message, "Frame Count Timed Out");
		}
	    }
	}
      /* Clear the J Field */
      strcpy (pgs->j, "");
      break;
    }				/* switch
				   (pPriv->commandState) */



  return OK;
}

#endif /* __UFGEMGENSUBCOMMDHS_C__ */

epicsRegisterFunction(ufexpStatusGinit);
epicsRegisterFunction(ufexpStatusGproc);
