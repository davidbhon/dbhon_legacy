#if !defined(__UFGEMCADCOMMDC_C__)
#define __UFGEMCADCOMMDC_C__ "RCS: $Name:  $ $Id: ufGemCadCommDC.c,v 1.1 2006/09/14 22:28:18 gemvx Exp $"
static const char rcsIdufGEMCADCOMMCDC[] = __UFGEMCADCOMMDC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"
/* #include "ufdbl.h" */

#include <stdioLib.h>
#include <string.h>

#include <registryFunction.h>
#include <epicsExport.h>

#include <cad.h>
#include <cadRecord.h>

#include "flam.h"
#include "ufGemComm.h"

/**
 * Detector  Controller CAD's
 */
long
DCInitCommand (cadRecord * pcr)
{
  char *endptr = 0;
  long temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  temp_long = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  strcpy (pcr->vala, "INIT");
	  strcpy (pcr->valc, pcr->a);
	  strcpy (pcr->valb, pcr->a);
	}
      else
	{
	  strcpy (pcr->valc, "");
	  strcpy (pcr->valb, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
dcHrdwrCommand (cadRecord * pcr)
{

  long out_vala = -1, out_valb = -1, out_valc = -1, out_vald = -1, out_vale = -1;
  long out_valf = -1, out_valg = -1, out_valh = -1, out_vali = -1, out_valj = -1;
  long out_valk = -1, out_valn = -1;
  char out_vall[40], out_valm[40];
  char *endptr = 0;
  long temp_long = 0;

  memset( out_vall, 0, sizeof( out_vall ) ) ;
  memset( out_valm, 0, sizeof( out_valm ) ) ;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC1", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = (long) strtod (pcr->b, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad frame coadd", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  out_valc = (long) strtod (pcr->c, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop sett freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  out_vald = (long) strtod (pcr->d, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop coad", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  out_vale = (long) strtod (pcr->e, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod settl c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  out_valf = (long) strtod (pcr->f, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod settl f", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->g, "") != 0)
	{
	  out_valg = (long) strtod (pcr->g, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad save sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  out_valh = (long) strtod (pcr->h, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod sets", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  out_vali = (long) strtod (pcr->i, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad pixclock", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  out_valj = (long) strtod (pcr->j, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad pre chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  out_valk = (long) strtod (pcr->k, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad post chops c", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);

	  if ((strcmp (out_vall, "chop-nod") != 0) &&
	      (strcmp (out_vall, "chop") != 0) &&
	      (strcmp (out_vall, "nod") != 0) &&
	      (strcmp (out_vall, "stare") != 0))
	    {
	      strncpy (pcr->mess, "invalid obs mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  out_valn = (long) strtod (pcr->n, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valn, 0, 1) == -1)
	    {
	      strncpy (pcr->mess, "invalid flag on input N", MAX_STRING_SIZE);
	    }
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{
	  *(long *) pcr->vala = out_vala;
	  *(long *) pcr->valb = out_valb;
	  *(long *) pcr->valc = out_valc;
	  *(long *) pcr->vald = out_vald;
	  *(long *) pcr->vale = out_vale;
	  *(long *) pcr->valf = out_valf;
	  *(long *) pcr->valg = out_valg;
	  *(long *) pcr->valh = out_valh;
	  *(long *) pcr->vali = out_vali;
	  *(long *) pcr->valj = out_valj;
	  *(long *) pcr->valk = out_valk;
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  *(long *) pcr->valn = out_valn;

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
dcPhysicalCommand (cadRecord * pcr)
{

  long out_vala;
  double out_valb, out_valc, out_vald, out_vale;
  double out_valf, out_valg, out_valh, out_vall, out_valm;
  char out_vali[40], out_valj[40];
  long out_valk;
  char out_valp[40];
  char *endptr;
  long temp_long;

  out_vala = -1;
  out_valb = -1.0;
  out_valc = -1.0;
  out_vald = -1.0;
  out_vale = -1.0;
  out_valf = -1.0;
  out_valg = -1.0;
  out_valh = -1.0;
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  out_valk = -1;
  out_vall = -1.0;
  out_valm = -1.0;
  strcpy (out_valp, "");

  temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC2", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = strtod (pcr->b, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad frame time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  out_valc = strtod (pcr->c, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad save freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  out_vald = strtod (pcr->d, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad exposure time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  out_vale = strtod (pcr->e, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad chop freq", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  out_valf = strtod (pcr->f, &endptr);
	  strcpy (out_valp, pcr->f);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "bad SCS DtyCycl", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valf, 0, 100) == -1)
	    {
	      strncpy (pcr->mess, "invalid SCS DtyCycl", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->g, "") != 0)
	{
	  out_valg = strtod (pcr->g, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod dwel time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  out_valh = strtod (pcr->h, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad nod stl time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);

	  if ((strcmp (out_vali, "chop-nod") != 0) &&
	      (strcmp (out_vali, "chop") != 0) &&
	      (strcmp (out_vali, "nod") != 0) &&
	      (strcmp (out_vali, "stare") != 0))
	    {
	      strncpy (pcr->mess, "invalid obs mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  out_valk = (long) strtod (pcr->k, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad flag", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_valk, 0, 1) == -1)
	    {
	      strncpy (pcr->mess, "invalid flag on input K", MAX_STRING_SIZE);
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  out_vall = strtod (pcr->l, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad preValid Chop Time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  out_valm = strtod (pcr->m, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad postValid Chop Time", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{
	  *(long *) pcr->vala = out_vala;
	  *(double *) pcr->valb = out_valb;
	  *(double *) pcr->valc = out_valc;
	  *(double *) pcr->vald = out_vald;
	  *(double *) pcr->vale = out_vale;
	  *(double *) pcr->valf = out_valf;
	  *(double *) pcr->valg = out_valg;
	  *(double *) pcr->valh = out_valh;
	  *(long *) pcr->valk = out_valk;
	  *(double *) pcr->vall = out_vall;
	  *(double *) pcr->valm = out_valm;
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valp, out_valp);

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	  strcpy (pcr->c, "");
	  strcpy (pcr->d, "");
	  strcpy (pcr->e, "");
	  strcpy (pcr->f, "");
	  strcpy (pcr->g, "");
	  strcpy (pcr->h, "");
	  strcpy (pcr->i, "");
	  strcpy (pcr->j, "");
	  strcpy (pcr->k, "");
	  strcpy (pcr->l, "");
	  strcpy (pcr->m, "");
	  strcpy (pcr->n, "");
	  strcpy (pcr->o, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

static long dcAcqCommand (cadRecord * pcr)
{
  char out_vala[40], out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40];

  char *endptr;
  long temp_long, out_valo;

  strcpy (out_vala, "");
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  strcpy (out_valn, "");

  out_valo = *(long *) pcr->valo;

  temp_long = 0;

  printf("I'm in dcAcqControl \n");

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "CONFIGURE") != 0) &&
	      (strcmp (out_vala, "START") != 0) &&
	      (strcmp (out_vala, "STOP") != 0) &&
	      (strcmp (out_vala, "ABORT") != 0) &&
	      (strcmp (out_vala, "INIT") != 0) &&
	      (strcmp (out_vala, "PARK") != 0) &&
	      (strcmp (out_vala, "DATUM") != 0) &&
	      (strcmp (out_vala, "TEST") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);	/* obs ID */
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1; 
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;	/* archive */
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);	/* dither */
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  temp_long = (long) strtod (pcr->f, &endptr);
	  printf (" dcAcqCommand> temp_long %ld \n",temp_long);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC3", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      if (temp_long == 2)
		out_valo = 1;
	      else
		out_valo = 0; 
	      temp_long = 1;
	      strcpy (out_valf, pcr->f);
	    }
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);	/* path */
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);	/* file name */
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);	/* comment */
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  if ((strcmp (out_valk, "TRUE") != 0) &&
	      (strcmp (out_valk, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valk, "true") != 0) &&
	      (strcmp (out_valk, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid remote archive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);	/* rem host */
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);	/* rem path */
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);	/* rem file name */
	  temp_long = 1;
	}
      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{

	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  *(long *) pcr->valo = out_valo;

	  strcpy (pcr->f, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**
 * Detector  Controller CAD's
 */
long
LVDTInitCommand (cadRecord * pcr)
{
  char *endptr = 0;
  long temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  temp_long = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  strcpy (pcr->vala, "INIT");
	  strcpy (pcr->valc, pcr->a);
	  strcpy (pcr->valb, pcr->a);
	}
      else
	{
	  strcpy (pcr->valc, "");
	  strcpy (pcr->valb, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
lvdtCommand (cadRecord * pcr)
{
  char out_vala[40], out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40];

  char *endptr;
  long temp_long, out_valo;

  strcpy (out_vala, "");
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");

  out_valo = *(long *) pcr->valo;

  printf("lvdtCommand> I'm here \n");

  temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "CONFIGURE") != 0) &&
	      (strcmp (out_vala, "START") != 0) &&
	      (strcmp (out_vala, "STOP") != 0) &&
	      (strcmp (out_vala, "ABORT") != 0) &&
	      (strcmp (out_vala, "INIT") != 0) &&
	      (strcmp (out_vala, "PARK") != 0) &&
	      (strcmp (out_vala, "DATUM") != 0) &&
	      (strcmp (out_vala, "TEST") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);	/* power */
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)     /* voltage  */
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1; 
	}

      if (strcmp (pcr->d, "") != 0)     /* frequency  */
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;	
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);	/* limit vel  */
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  temp_long = (long) strtod (pcr->f, &endptr);
	  printf (" dcAcqCommand> temp_long %ld \n",temp_long);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC3", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      if (temp_long == 2)
		out_valo = 1;
	      else
		out_valo = 0;
	      temp_long = 1;
	      strcpy (out_valf, pcr->f);
	    }
	}

      if (strcmp (pcr->g, "") != 0)
	{
	  strcpy (out_valf, pcr->g);	/* steps */
	  temp_long = 1;
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valg, pcr->h);	/* steps Vel */
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_valh, pcr->i);	/* motor name */
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_vali, pcr->j);	/* current pos */
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valj, pcr->k);	/* steps To Move */
	  temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_valk, pcr->l);	/* backlash  */
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_vall, pcr->m);	/* previous pos  */
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_vala, "DATUM");	/* datum  */
	  temp_long = 1;
	}

      if (strcmp (pcr->o, "") != 0)
	{
	  strcpy (out_vala, "PARK");	/* park  */
	  temp_long = 1;
	}

      if (strcmp (pcr->p, "") != 0)
	{
	  strcpy (out_vala, "ABORT");	/* abort  */
	  temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{

	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);

	  strcpy (pcr->f, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


long
dcDatumCommand (cadRecord * pcr)
{
  char out_vala[40], out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40];

  char *endptr;
  long temp_long, out_valo;

  strcpy (out_vala, "");
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  strcpy (out_valn, "");

  out_valo = *(long *) pcr->valo;

  temp_long = 0;

  printf("I'm in dcDatumControl \n");

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "CONFIGURE") != 0) &&
	      (strcmp (out_vala, "START") != 0) &&
	      (strcmp (out_vala, "STOP") != 0) &&
	      (strcmp (out_vala, "ABORT") != 0) &&
	      (strcmp (out_vala, "INIT") != 0) &&
	      (strcmp (out_vala, "PARK") != 0) &&
	      (strcmp (out_vala, "DATUM") != 0) &&
	      (strcmp (out_vala, "TEST") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);	/* obs ID */
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1; 
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;	/* archive */
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);	/* dither */
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  temp_long = (long) strtod (pcr->f, &endptr);
	  printf (" dcAcqCommand> temp_long %ld \n",temp_long);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC3", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      if (temp_long == 2)
		out_valo = 1;
	      else
		out_valo = 0;
	      temp_long = 1;
	      strcpy (out_valf, pcr->f);
	    }
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);	/* path */
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);	/* file name */
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);	/* comment */
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  if ((strcmp (out_valk, "TRUE") != 0) &&
	      (strcmp (out_valk, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valk, "true") != 0) &&
	      (strcmp (out_valk, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid remote archive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);	/* rem host */
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);	/* rem path */
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);	/* rem file name */
	  temp_long = 1;
	}
      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{

	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  *(long *) pcr->valo = out_valo;

	  strcpy (pcr->f, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}
long
dcObsCommand (cadRecord * pcr)
{

  long out_vala = -1;
  char out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40], out_valo[40];
  char out_valp[40], out_valq[40], out_valr[40], out_vals[40], out_valt[40];
  char *endptr = 0;
  long temp_long = 0;

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
               pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
        {
          out_vala = (long) strtod (pcr->a, &endptr);
          if (*endptr != '\0')
            {
              strncpy (pcr->mess, "Bad init directive", MAX_STRING_SIZE);
              return CAD_REJECT;
            }
          if (UFcheck_long_r (out_vala, 0, 3) == -1)
            {
              strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
              return CAD_REJECT;
            }
          else
            temp_long = 1;
        }

      if (strcmp (pcr->b, "") != 0)
        {
          strcpy (out_valb, pcr->b);
          temp_long = 1;
        }
      if (strcmp (pcr->c, "") != 0)
        {
          strcpy (out_valc, pcr->c);
          temp_long = 1;
        }
      if (strcmp (pcr->d, "") != 0)
        {
          strcpy (out_vald, pcr->d);
          temp_long = 1;
        }
      if (strcmp (pcr->e, "") != 0)
        {
          strcpy (out_vale, pcr->e);
          temp_long = 1;
        }
      if (strcmp (pcr->f, "") != 0)
        {
          strcpy (out_valf, pcr->f);
          temp_long = 1;
        }

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
        {
          *(long *) pcr->vala = out_vala;
          strcpy (pcr->valb, out_valb);
          strcpy (pcr->valc, out_valc);
          strcpy (pcr->vald, out_vald);
          strcpy (pcr->vale, out_vale);
          strcpy (pcr->valf, out_valf);

          strcpy (pcr->a, "");
          strcpy (pcr->b, "");
          strcpy (pcr->c, "");
          strcpy (pcr->d, "");
          strcpy (pcr->e, "");
          strcpy (pcr->f, "");
        }
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}



long
DCbipaInitCommand (cadRecord * pcr)
{
  long out_vala;
  char *endptr;
  long temp_long;

  printf("DCbipaInitCommand> (%s) pcr->a: %s \n",pcr->name, pcr->a );

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

	  out_vala = (long) strtod (pcr->a, &endptr);
          printf("DCbipaInitCommand> out_vala (%ld) menuDirectiveSTART \n",out_vala);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long DCbipaDatumCommand( cadRecord * pcr )
{
  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long DCbipaPowerCommand( cadRecord * pcr )
{
  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  if ((strcmp (pcr->a, "ON") != 0) &&
	      (strcmp (pcr->a, "on") != 0) &&
	      (strcmp (pcr->a, "On") != 0) &&
	      (strcmp (pcr->a, "off") != 0) &&
	      (strcmp (pcr->a, "Off") != 0) &&
	      (strcmp (pcr->a, "OFF") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      strcpy (pcr->vala, pcr->a);
	      strcpy (pcr->a, "");
	    }
	}

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long DCbipaParkCommand( cadRecord * pcr )
{
  switch (pcr->dir)
    {
      /* Mark, Clear and Stop do nothing so can be accepted immediately */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;
      if (strcmp (pcr->a, "") != 0)
	  *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long DCbipaReadAllCommand( cadRecord * pcr )
{
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long DCbipaLatchCommand( cadRecord * pcr )
{
  long out_vala;
  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s", pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Invalid Latch command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 23) == -1)
	    {
	      strncpy (pcr->mess, "invalid Latch command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else  *(long *) pcr->vala = -1;

      break;

    default:
      strncpy (pcr->mess, "Invalid latch command received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbipaSetCommand (cadRecord * pcr)
{
  long out_vala = 0;
  double out_valb = 0.0;
  char *endptr = 0;
  long temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      printf("DCbipaSetCommand>  (%s) menuDirectiveSTART, pcr->a: %s, pcr->b: %s", pcr->name, pcr->a, pcr->b ) ;
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s, pcr->b: %s",
	       pcr->name, pcr->a, pcr->b ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad Bias ID", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (out_vala, 0, 23) == -1)
	    {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) UFcheck_long_r() failed",
		       pcr->name ) ;
	      ufLog( _UFerrmsg ) ;

	      strncpy (pcr->mess, "invalid Bias ID", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  out_valb = strtod (pcr->b, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad Voltage", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_double (out_valb, -30.0, 255.0) == -1)
	    {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) UFcheck_double() failed",
		       pcr->name ) ;
	      ufLog( _UFerrmsg ) ;

	      strncpy (pcr->mess, "invalid voltage", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{
	  ufLog( __HERE__ "> Looks good, processing" ) ;

	  *(long *) pcr->vala = out_vala;
	  *(double *) pcr->valb = out_valb;

	  strcpy (pcr->a, "");
	  strcpy (pcr->b, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;

}

long
DCbipaGlobSetCommand (cadRecord * pcr)
{
  double out_val = 0.0;
  char *endptr = 0;
  double temp_val = 0.0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.  
       */
    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert Global Set Value",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_val = out_val;
	  if (UFcheck_double (temp_val, -30.0, 30.0) == -1)
	    {
	      strncpy (pcr->mess, "Global Set Value is not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  ufLog( __HERE__ "> All checks valid, accepting CAD" ) ;
	  *(double *) pcr->vala = temp_val;
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbipaAdjustCommand (cadRecord * pcr)
{
  double out_val = 0.0;
  char *endptr = 0;
  double temp_val = 0.0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */
    case menuDirectivePRESET:
    case menuDirectiveSTART:
       sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

     if (strcmp (pcr->a, "") != 0)
	{
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert adjust Value",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_val = out_val;
	  if (UFcheck_double (temp_val, -30.0, 30.0) == -1)
	    {
	      strncpy (pcr->mess, "Adjust value is not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(double *) pcr->vala = temp_val;
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbipaVGateCommand (cadRecord * pcr)
{
  char out_vala[40];

  memset( out_vala, 0, sizeof( out_vala ) ) ;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
       sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

     if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbipaVWellCommand (cadRecord * pcr)
{
  char out_vala[40];

  memset( out_vala, 0, sizeof( out_vala ) ) ;
  /*  strcpy (pcr->vala, "");*/

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;
      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  /*	  *(long*) pcr->vala = atoi( pcr->a ) ; */
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbipaVBiasCommand (cadRecord * pcr)
{
  char out_vala[40];
  memset( out_vala, 0, sizeof( out_vala ) ) ;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */
    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
DCbiPasswordCommand (cadRecord * pcr)
{
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (pcr->vala, pcr->a);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

long
dcEngCommand (cadRecord * pcr)
{
  char out_vala[40], out_valb[40], out_valc[40], out_vald[40], out_vale[40];
  char out_valf[40], out_valg[40], out_valh[40], out_vali[40], out_valj[40];
  char out_valk[40], out_vall[40], out_valm[40], out_valn[40];

  char *endptr;
  long temp_long, out_valo;

  strcpy (out_vala, "");
  strcpy (out_valb, "");
  strcpy (out_valc, "");
  strcpy (out_vald, "");
  strcpy (out_vale, "");
  strcpy (out_valf, "");
  strcpy (out_valg, "");
  strcpy (out_valh, "");
  strcpy (out_vali, "");
  strcpy (out_valj, "");
  strcpy (out_valk, "");
  strcpy (out_vall, "");
  strcpy (out_valm, "");
  strcpy (out_valn, "");

  out_valo = *(long *) pcr->valo;

  temp_long = 0;

  printf("I'm in dcEngControl \n");

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      sprintf( _UFerrmsg, __HERE__ "> (%s) menuDirectiveSTART, pcr->a: %s",
	       pcr->name, pcr->a ) ;
      ufLog( _UFerrmsg ) ;

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "CONFIGURE") != 0) &&
	      (strcmp (out_vala, "START") != 0) &&
	      (strcmp (out_vala, "STOP") != 0) &&
	      (strcmp (out_vala, "ABORT") != 0) &&
	      (strcmp (out_vala, "INIT") != 0) &&
	      (strcmp (out_vala, "PARK") != 0) &&
	      (strcmp (out_vala, "DATUM") != 0) &&
	      (strcmp (out_vala, "TEST") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->b, "") != 0)
	{
	  strcpy (out_valb, pcr->b);	/* obs ID */
	  temp_long = 1;
	}

      if (strcmp (pcr->c, "") != 0)
	{
	  strcpy (out_valc, pcr->c);
	  temp_long = 1; 
	}

      if (strcmp (pcr->d, "") != 0)
	{
	  strcpy (out_vald, pcr->d);
	  temp_long = 1;	/* archive */
	}

      if (strcmp (pcr->e, "") != 0)
	{
	  strcpy (out_vale, pcr->e);	/* dither */
	  temp_long = 1;
	}

      if (strcmp (pcr->f, "") != 0)
	{
	  temp_long = (long) strtod (pcr->f, &endptr);
	  printf (" dcEngCommand> temp_long %ld \n",temp_long);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Bad init directive DC3", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid sim mode", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    {
	      if (temp_long == 2)
		out_valo = 1;
	      else
		out_valo = 0;
	      temp_long = 1;
	      strcpy (out_valf, pcr->f);
	    }
	}

      if (strcmp (pcr->h, "") != 0)
	{
	  strcpy (out_valh, pcr->h);	/* path */
	  temp_long = 1;
	}

      if (strcmp (pcr->i, "") != 0)
	{
	  strcpy (out_vali, pcr->i);	/* file name */
	  temp_long = 1;
	}

      if (strcmp (pcr->j, "") != 0)
	{
	  strcpy (out_valj, pcr->j);	/* comment */
	  temp_long = 1;
	}

      if (strcmp (pcr->k, "") != 0)
	{
	  strcpy (out_valk, pcr->k);
	  if ((strcmp (out_valk, "TRUE") != 0) &&
	      (strcmp (out_valk, "FALSE") != 0) &&
	      (strcmp (out_valf, "True") != 0) &&
	      (strcmp (out_valf, "False") != 0) &&
	      (strcmp (out_valk, "true") != 0) &&
	      (strcmp (out_valk, "false") != 0))
	    {
	      strncpy (pcr->mess, "invalid remote archive", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if (strcmp (pcr->l, "") != 0)
	{
	  strcpy (out_vall, pcr->l);	/* rem host */
	  temp_long = 1;
	}

      if (strcmp (pcr->m, "") != 0)
	{
	  strcpy (out_valm, pcr->m);	/* rem path */
	  temp_long = 1;
	}

      if (strcmp (pcr->n, "") != 0)
	{
	  strcpy (out_valn, pcr->n);	/* rem file name */
	  temp_long = 1;
	}
      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{

	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->valb, out_valb);
	  strcpy (pcr->valc, out_valc);
	  strcpy (pcr->vald, out_vald);
	  strcpy (pcr->vale, out_vale);
	  strcpy (pcr->valf, out_valf);
	  strcpy (pcr->valg, out_valg);
	  strcpy (pcr->valh, out_valh);
	  strcpy (pcr->vali, out_vali);
	  strcpy (pcr->valj, out_valj);
	  strcpy (pcr->valk, out_valk);
	  strcpy (pcr->vall, out_vall);
	  strcpy (pcr->valm, out_valm);
	  strcpy (pcr->valn, out_valn);
	  *(long *) pcr->valo = out_valo;

	  strcpy (pcr->f, "");

	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

#endif /* __UFGEMCADCOMMDC_C__ */

epicsRegisterFunction(dcAcqCommand); 
