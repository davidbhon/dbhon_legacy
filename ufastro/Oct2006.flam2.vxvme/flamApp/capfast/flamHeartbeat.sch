[schematic2]
uniq 116
[tools]
[detail]
w 1378 1195 100 0 n#104 ecalcs.ecalcs#92.VAL 1568 896 1632 896 1632 1184 1184 1184 1184 1088 1280 1088 ecalcs.ecalcs#92.INPA
w 1650 971 100 0 n#104 elongouts.elongouts#86.DOL 1728 960 1632 960 junction
w 994 491 100 0 n#107 ecalcs.ecalcs#92.INPD 1280 992 1120 992 1120 480 928 480 flamOneShot.flamOneShot#109.RUNNING
w 1074 1035 100 0 n#106 ecalcs.ecalcs#92.INPC 1280 1024 928 1024 flamOneShot.flamOneShot#108.RUNNING
w 994 1547 100 0 n#105 ecalcs.ecalcs#92.INPB 1280 1056 1120 1056 1120 1536 928 1536 flamOneShot.flamOneShot#89.RUNNING
w 1618 939 100 0 n#103 ecalcs.ecalcs#92.FLNK 1568 928 1728 928 elongouts.elongouts#86.SLNK
w 210 523 100 0 n#115 hwin.hwin#102.in 224 512 256 512 ecalcouts.ecalcouts#112.INPA
w 576 523 100 0 n#99 ecalcouts.ecalcouts#112.FLNK 576 512 624 512 624 480 672 480 flamOneShot.flamOneShot#109.RESET
w 576 1067 100 0 n#98 ecalcouts.ecalcouts#111.FLNK 576 1056 624 1056 624 1024 672 1024 flamOneShot.flamOneShot#108.RESET
w 210 1067 100 0 n#114 hwin.hwin#95.in 224 1056 256 1056 ecalcouts.ecalcouts#111.INPA
w 576 1579 100 0 n#94 ecalcouts.ecalcouts#110.FLNK 576 1568 624 1568 624 1536 672 1536 flamOneShot.flamOneShot#89.RESET
w 210 1579 100 0 n#113 hwin.hwin#52.in 224 1568 256 1568 ecalcouts.ecalcouts#110.INPA
w 1992 907 100 0 n#87 elongouts.elongouts#86.OUT 1984 896 2048 896 hwout.hwout#88.outp
s 2624 2032 100 1792 2001/01/11
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 flamHeartbeat.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/01/11
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos Heartbeat Generator
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use ecalcouts 256 327 100 0 ecalcouts#112
xform 0 416 448
p 328 360 100 0 -1 CALC:A
p 1088 910 100 0 0 OOPT:On Change
p 344 560 100 0 1 SCAN:Passive
p 320 328 100 0 -1 name:$(top)ecHbMonitor
p 240 520 75 0 -1 palrm(INPA):NMS
p 208 520 75 0 -1 pproc(INPA):CPP
use ecalcouts 256 871 100 0 ecalcouts#111
xform 0 416 992
p 328 904 100 0 -1 CALC:A
p 1088 1454 100 0 0 OOPT:On Change
p 344 1104 100 0 1 SCAN:Passive
p 320 872 100 0 -1 name:$(top)dcHbMonitor
p 240 1064 75 0 -1 palrm(INPA):NMS
p 208 1064 75 0 -1 pproc(INPA):CPP
use ecalcouts 256 1383 100 0 ecalcouts#110
xform 0 416 1504
p 328 1416 100 0 -1 CALC:A
p 1088 1966 100 0 0 OOPT:On Change
p 344 1616 100 0 1 SCAN:Passive
p 320 1384 100 0 -1 name:$(top)ccHbMonitor
p 240 1576 75 0 -1 palrm(INPA):NMS
p 208 1576 75 0 -1 pproc(INPA):CPP
use flamOneShot 688 1335 100 0 flamOneShot#89
xform 0 800 1536
p 672 1376 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 1408 100 0 1 setTimer:timer $(top)ccHbTimer
use flamOneShot 688 823 100 0 flamOneShot#108
xform 0 800 1024
p 672 864 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 896 100 0 1 setTimer:timer $(top)dcHbTimer
use flamOneShot 688 279 100 0 flamOneShot#109
xform 0 800 480
p 672 320 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 352 100 0 1 setTimer:timer $(top)ecHbTimer
use hwin 32 1527 100 0 hwin#52
xform 0 128 1568
p -288 1568 100 0 -1 val(in):$(top)cc:heartbeat.VAL CPP
use hwin 32 1015 100 0 hwin#95
xform 0 128 1056
p -256 1056 100 0 -1 val(in):$(top)dc:heartbeat.VAL CPP
use hwin 32 471 100 0 hwin#102
xform 0 128 512
p -272 512 100 0 -1 val(in):$(top)ec:heartbeat.VAL CPP
use ecalcs 1280 615 100 0 ecalcs#92
xform 0 1424 880
p 1280 576 100 0 1 CALC:(B&&C&&D)?A+1:0
p 1280 544 100 0 1 SCAN:1 second
p 1424 608 100 1024 1 name:$(top)heartbeatCalc
use hwout 2048 855 100 0 hwout#88
xform 0 2144 896
p 2272 896 100 0 -1 val(outp):$(sad)heartbeat.VAL PP NMS
use elongouts 1728 839 100 0 elongouts#86
xform 0 1856 928
p 1792 800 100 0 1 OMSL:closed_loop
p 1920 832 100 1024 1 name:$(top)heartbeat
p 1984 896 75 768 -1 pproc(OUT):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamHeartbeat.sch,v 1.1 2006/09/14 22:27:49 gemvx Exp $
