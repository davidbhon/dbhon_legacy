[schematic2]
uniq 499
[tools]
[detail]
w -2630 5227 100 0 n#490 elongins.elongins#485.VAL -2624 5216 -2576 5216 -2576 5280 -2528 5280 ecalcouts.ecalcouts#497.INPA
w -2214 5291 100 0 n#489 ecalcouts.ecalcouts#497.FLNK -2208 5280 -2160 5280 -2160 5248 -2112 5248 flamOneShot.flamOneShot#487.RESET
w -1638 4075 100 0 n#488 flamOneShot.flamOneShot#487.RUNNING -1856 5248 -1712 5248 -1712 4064 -1504 4064 ecalcs.ecalcs#263.INPG
w -1630 4043 100 0 n#481 flamOneShot.flamOneShot#482.RUNNING -1856 4896 -1856 4880 -1696 4880 -1696 4032 -1504 4032 ecalcs.ecalcs#263.INPH
w -2214 4939 100 0 n#480 ecalcouts.ecalcouts#483.FLNK -2208 4928 -2160 4928 -2160 4896 -2112 4896 flamOneShot.flamOneShot#482.RESET
w -2630 4875 100 0 n#479 elongins.elongins#484.VAL -2624 4864 -2576 4864 -2576 4928 -2528 4928 ecalcouts.ecalcouts#483.INPA
w 2442 3179 100 0 n#478 egenSub.egenSub#440.VALC 2368 3168 2576 3168 2576 3072 2688 3072 ecars.$(top)ec:applyC.SDIS
w 570 2531 100 0 n#457 egenSub.$(top)$(dev)arrayG.OUTE 496 2528 704 2528 hwout.hwout#458.outp
w 1826 2283 100 0 n#455 ecars.ecars#385.FLNK 1728 2272 1984 2272 junction
w 1506 2571 100 0 n#455 ecars.ecars#363.FLNK 1728 2656 1760 2656 1760 2560 1312 2560 junction
w 1316 2555 100 0 n#455 ecars.ecars#423.FLNK 1728 3040 1760 3040 1760 2944 1312 2944 1312 2176 1984 2176 1984 2592 2080 2592 egenSub.egenSub#440.SLNK
w 2546 3307 100 0 n#453 egenSub.egenSub#440.OUTB 2368 3200 2464 3200 2464 3296 2688 3296 ecars.$(top)ec:applyC.IVAL
w 2418 3267 100 0 n#452 egenSub.egenSub#440.OUTA 2368 3264 2528 3264 2528 3232 2688 3232 ecars.$(top)ec:applyC.IMSS
w 1852 3003 100 0 n#446 ecars.ecars#363.VAL 1728 2880 1856 2880 1856 3136 2080 3136 egenSub.egenSub#440.INPC
w 1884 2939 100 0 n#449 ecars.ecars#363.OMSS 1728 2816 1888 2816 1888 3072 2080 3072 egenSub.egenSub#440.INPD
w 1916 2747 100 0 n#444 ecars.ecars#385.VAL 1728 2496 1920 2496 1920 3008 2080 3008 egenSub.egenSub#440.INPE
w 1948 2683 100 0 n#450 ecars.ecars#385.OMSS 1728 2432 1952 2432 1952 2944 2080 2944 egenSub.egenSub#440.INPF
w 1874 3211 100 0 n#442 ecars.ecars#423.OMSS 1728 3200 2080 3200 egenSub.egenSub#440.INPB
w 1874 3275 100 0 n#441 ecars.ecars#423.VAL 1728 3264 2080 3264 egenSub.egenSub#440.INPA
w -2670 3083 100 0 n#438 ecad8.$(top)ec:tempSet.MESS -1600 2432 -1536 2432 -1536 2560 -1504 2560 -1504 2592 -1408 2592 -1408 3296 -2208 3296 -2208 3072 -3072 3072 -3072 2688 -2880 2688 eapply.eapply#183.INMB
w -2638 3051 100 0 n#437 ecad8.$(top)ec:tempSet.VAL -1600 2464 -1568 2464 -1568 2592 -1536 2592 -1536 2624 -1440 2624 -1440 3264 -2176 3264 -2176 3040 -3040 3040 -3040 2720 -2880 2720 eapply.eapply#183.INPB
w -2606 3019 100 0 n#436 ecad2.ecad2#169.MESS -1600 3104 -1536 3104 -1536 3232 -2144 3232 -2144 3008 -3008 3008 -3008 2752 -2880 2752 eapply.eapply#183.INMA
w -2574 2987 100 0 n#435 ecad2.ecad2#169.VAL -1600 3136 -1568 3136 -1568 3200 -2112 3200 -2112 2976 -2976 2976 -2976 2784 -2880 2784 eapply.eapply#183.INPA
w -2108 2251 100 0 n#434 eapply.eapply#183.FLNK -2496 2912 -2112 2912 -2112 1600 -2080 1600 -2080 1472 -1408 1472 -1408 1568 -736 1568 flamLinkEnable.flamLinkEnable#327.SLINK
w -2318 2795 100 0 n#433 eapply.eapply#183.OUTA -2496 2784 -2080 2784 -2080 3136 -1920 3136 ecad2.ecad2#169.DIR
w -2302 2763 100 0 n#432 eapply.eapply#183.OCLA -2496 2752 -2048 2752 -2048 3104 -1920 3104 ecad2.ecad2#169.ICID
w -2302 2723 100 0 n#431 eapply.eapply#183.OUTB -2496 2720 -2048 2720 -2048 2464 -1920 2464 ecad8.$(top)ec:tempSet.DIR
w -2318 2691 100 0 n#430 eapply.eapply#183.OCLB -2496 2688 -2080 2688 -2080 2432 -1920 2432 ecad8.$(top)ec:tempSet.ICID
w -1628 3819 100 0 n#429 flamOneShot.flamOneShot#426.RUNNING -1856 3520 -1632 3520 -1632 4128 -1504 4128 ecalcs.ecalcs#263.INPE
w -2158 3531 100 0 n#425 ecalcouts.ecalcouts#463.FLNK -2144 3552 -2144 3520 -2112 3520 flamOneShot.flamOneShot#426.RESET
w -2542 3563 100 0 n#465 elongins.elongins#428.VAL -2624 3488 -2560 3488 -2560 3552 -2464 3552 ecalcouts.ecalcouts#463.INPA
w -206 99 100 0 n#384 ecad2.ecad2#375.STLK -288 96 -64 96 -64 112 -16 112 efanouts.efanouts#382.SLNK
w 324 387 100 0 n#383 efanouts.efanouts#382.LNK1 224 192 320 192 320 592 416 592 estringouts.estringouts#378.SLNK
w 338 971 100 0 n#381 efanouts.efanouts#379.LNK1 320 960 416 960 416 1056 352 1056 352 1120 448 1120 elongouts.elongouts#377.SLNK
w 10 891 100 0 n#380 ecad2.ecad2#376.STLK -64 864 0 864 0 880 80 880 efanouts.efanouts#379.SLNK
w 530 451 100 0 n#372 estringouts.estringouts#378.FLNK 672 608 768 608 768 448 352 448 352 224 junction
w 1436 523 100 0 n#372 elongouts.elongouts#377.FLNK 704 1152 1440 1152 1440 -96 352 -96 352 224 416 224 flamLinkEnable.flamLinkEnable#373.ENABLE
w 732 795 100 0 n#370 estringouts.estringouts#378.OUT 672 576 736 576 736 1024 896 1024 egenSubB.egenSubB#374.C
w -30 387 100 0 n#369 ecad2.ecad2#375.VALA -288 384 288 384 288 624 416 624 estringouts.estringouts#378.DOL
w 316 11 100 0 n#368 efanouts.efanouts#382.LNK2 224 160 320 160 320 -128 junction
w -100 283 100 0 n#368 efanouts.efanouts#379.LNK2 320 928 416 928 416 704 -96 704 -96 -128 384 -128 384 160 416 160 flamLinkEnable.flamLinkEnable#373.SLINK
w 764 283 100 0 n#366 flamLinkEnable.flamLinkEnable#373.FLINK 736 192 768 192 768 384 896 384 egenSubB.egenSubB#374.SLNK
w 770 1091 100 0 n#365 elongouts.elongouts#377.OUT 704 1088 896 1088 egenSubB.egenSubB#374.A
w 162 1155 100 0 n#364 ecad2.ecad2#376.VALA -64 1152 448 1152 elongouts.elongouts#377.DOL
w -2446 195 100 0 n#359 ecad2.ecad2#349.STLK -2528 192 -2304 192 -2304 208 -2256 208 efanouts.efanouts#357.SLNK
w -1916 483 100 0 n#358 efanouts.efanouts#357.LNK1 -2016 288 -1920 288 -1920 688 -1824 688 estringouts.estringouts#351.SLNK
w -1902 1059 100 0 n#356 efanouts.efanouts#354.LNK1 -1920 1056 -1824 1056 -1824 1120 -1888 1120 -1888 1184 -1792 1184 elongouts.elongouts#350.SLNK
w -2230 987 100 0 n#355 ecad2.ecad2#340.STLK -2304 960 -2240 960 -2240 976 -2160 976 efanouts.efanouts#354.SLNK
w -1710 547 100 0 n#346 estringouts.estringouts#351.FLNK -1568 704 -1472 704 -1472 544 -1888 544 -1888 320 junction
w -804 619 100 0 n#346 elongouts.elongouts#350.FLNK -1536 1216 -1504 1216 -1504 1248 -800 1248 -800 0 -1888 0 -1888 320 -1824 320 flamLinkEnable.flamLinkEnable#341.ENABLE
w -1508 891 100 0 n#360 estringouts.estringouts#351.OUT -1568 672 -1504 672 -1504 1120 -1344 1120 egenSubB.egenSubB#343.C
w -2270 483 100 0 n#352 ecad2.ecad2#349.VALA -2528 480 -1952 480 -1952 720 -1824 720 estringouts.estringouts#351.DOL
w -1924 139 100 0 n#347 efanouts.efanouts#357.LNK2 -2016 256 -1920 256 -1920 32 junction
w -2372 411 100 0 n#347 efanouts.efanouts#354.LNK2 -1920 1024 -1824 1024 -1824 800 -2368 800 -2368 32 -1856 32 -1856 256 -1824 256 flamLinkEnable.flamLinkEnable#341.SLINK
w -1476 379 100 0 n#362 flamLinkEnable.flamLinkEnable#341.FLINK -1504 288 -1472 288 -1472 480 -1344 480 egenSubB.egenSubB#343.SLNK
w -1502 1155 100 0 n#454 elongouts.elongouts#350.OUT -1536 1152 -1408 1152 -1408 1184 -1344 1184 egenSubB.egenSubB#343.A
w -2030 1219 100 0 n#344 ecad2.ecad2#340.VALA -2304 1248 -2208 1248 -2208 1216 -1792 1216 elongouts.elongouts#350.DOL
w -804 2411 100 0 n#328 elongouts.elongouts#309.FLNK -992 2880 -800 2880 -800 1952 -608 1952 -608 1728 -768 1728 -768 1632 -736 1632 flamLinkEnable.flamLinkEnable#327.ENABLE
w -926 2563 100 0 n#328 eaos.eaos#311.FLNK -992 2560 -800 2560 junction
w -580 1851 100 0 n#336 flamLinkEnable.flamLinkEnable#327.FLINK -416 1600 -352 1600 -352 1728 -576 1728 -576 1984 -768 1984 -768 2112 -512 2112 egenSub.$(top)$(dev)tempSetG.SLNK
w -366 1955 100 0 n#339 estringouts.$(top)ec:debug.OUT -224 1824 -128 1824 -128 1952 -544 1952 -544 2016 -736 2016 -736 2368 -512 2368 egenSub.$(top)$(dev)tempSetG.H
w -1390 2851 100 0 n#320 ecad2.ecad2#169.STLK -1600 2656 -1472 2656 -1472 2848 -1248 2848 elongouts.elongouts#309.SLNK
w -1166 2315 100 0 n#319 elongouts.elongouts#310.FLNK -992 2208 -928 2208 -928 2304 -1344 2304 -1344 2528 -1248 2528 eaos.eaos#311.SLNK
w -1444 1883 100 0 n#318 ecad8.$(top)ec:tempSet.STLK -1600 1600 -1440 1600 -1440 2176 -1248 2176 elongouts.elongouts#310.SLNK
w -836 2411 100 0 n#317 elongouts.elongouts#310.OUT -992 2144 -832 2144 -832 2688 -512 2688 egenSub.$(top)$(dev)tempSetG.C
w -718 2755 100 0 n#338 eaos.eaos#311.OUT -992 2496 -864 2496 -864 2752 -512 2752 egenSub.$(top)$(dev)tempSetG.B
w -782 2819 100 0 n#337 egenSub.$(top)$(dev)tempSetG.A -512 2816 -992 2816 elongouts.elongouts#309.OUT
w -1454 2883 100 0 n#314 ecad2.ecad2#169.VALB -1600 2880 -1248 2880 elongouts.elongouts#309.DOL
w -1444 2411 100 0 n#321 ecad8.$(top)ec:tempSet.VALA -1600 2272 -1440 2272 -1440 2560 -1248 2560 eaos.eaos#311.DOL
w -1454 2211 100 0 n#312 ecad8.$(top)ec:tempSet.VALB -1600 2208 -1248 2208 elongouts.elongouts#310.DOL
w 402 4451 100 0 n#296 egenSub.egenSub#252.OUTB 320 4448 544 4448 hwout.hwout#295.outp
w -1604 3787 100 0 n#394 ecad2.ecad2#169.VALA -1600 2944 -1472 2944 -1472 3152 -1504 3152 -1504 3488 -1600 3488 -1600 4096 -1504 4096 ecalcs.ecalcs#263.INPF
w 570 2659 100 0 n#292 egenSub.$(top)$(dev)arrayG.OUTC 496 2656 704 2656 hwout.hwout#289.outp
w 570 2595 100 0 n#291 egenSub.$(top)$(dev)arrayG.OUTD 496 2592 704 2592 hwout.hwout#290.outp
w 402 4515 100 0 n#284 egenSub.egenSub#252.OUTA 320 4512 544 4512 hwout.hwout#280.outp
w -798 4075 100 0 n#279 elongouts.elongouts#265.OUT -832 4064 -704 4064 hwout.hwout#266.outp
w -1422 4331 100 0 n#278 ecalcs.ecalcs#263.VAL -1216 4064 -1184 4064 -1184 4320 -1600 4320 -1600 4256 -1504 4256 ecalcs.ecalcs#263.INPA
w -1166 4139 100 0 n#278 junction -1184 4128 -1088 4128 elongouts.elongouts#265.DOL
w -1182 4107 100 0 n#277 ecalcs.ecalcs#263.FLNK -1216 4096 -1088 4096 elongouts.elongouts#265.SLNK
w -1668 4019 100 0 n#276 flamOneShot.flamOneShot#259.RUNNING -1856 3872 -1856 3888 -1664 3888 -1664 4160 -1504 4160 ecalcs.ecalcs#263.INPD
w -1636 4395 100 0 n#275 flamOneShot.flamOneShot#256.RUNNING -1856 4576 -1632 4576 -1632 4224 -1504 4224 ecalcs.ecalcs#263.INPB
w -1646 4195 100 0 n#274 flamOneShot.flamOneShot#258.RUNNING -1856 4224 -1728 4224 -1728 4192 -1504 4192 ecalcs.ecalcs#263.INPC
w -2214 4619 100 0 n#273 ecalcouts.ecalcouts#460.FLNK -2208 4608 -2160 4608 -2160 4576 -2112 4576 flamOneShot.flamOneShot#256.RESET
w -2214 4267 100 0 n#272 ecalcouts.ecalcouts#461.FLNK -2208 4256 -2160 4256 -2160 4224 -2112 4224 flamOneShot.flamOneShot#258.RESET
w -2166 3915 100 0 n#271 ecalcouts.ecalcouts#473.FLNK -2144 3904 -2128 3904 -2128 3872 -2112 3872 flamOneShot.flamOneShot#259.RESET
w -2542 3915 100 0 n#468 elongins.elongins#249.VAL -2624 3840 -2560 3840 -2560 3904 -2464 3904 ecalcouts.ecalcouts#473.INPA
w -2590 4267 100 0 n#471 elongins.elongins#498.VAL -2624 4192 -2592 4192 -2592 4256 -2528 4256 ecalcouts.ecalcouts#461.INPA
w -2630 4555 100 0 n#472 elongins.elongins#247.VAL -2624 4544 -2576 4544 -2576 4608 -2528 4608 ecalcouts.ecalcouts#460.INPA
w -1454 1859 100 0 n#251 ecad8.$(top)ec:tempSet.OUTG -1600 1856 -1248 1856 hwout.hwout#250.outp
w 570 2787 100 0 n#238 egenSub.$(top)$(dev)arrayG.OUTA 496 2784 704 2784 hwout.hwout#233.outp
w 570 2723 100 0 n#237 egenSub.$(top)$(dev)arrayG.OUTB 496 2720 704 2720 hwout.hwout#234.outp
w -1454 1795 100 0 n#236 ecad8.$(top)ec:tempSet.OUTH -1600 1792 -1248 1792 hwout.hwout#235.outp
w 2130 3555 100 0 n#227 egenSubB.$(top)$(dev)tempMonG.OUTP 1824 3808 2016 3808 2016 3552 2304 3552 hwout.hwout#200.outp
w 2146 3619 100 0 n#226 egenSubB.$(top)$(dev)tempMonG.OUTO 1824 3840 2048 3840 2048 3616 2304 3616 hwout.hwout#199.outp
w 1922 3875 100 0 n#225 egenSubB.$(top)$(dev)tempMonG.OUTN 1824 3872 2080 3872 2080 3680 2304 3680 hwout.hwout#198.outp
w 1938 3907 100 0 n#224 egenSubB.$(top)$(dev)tempMonG.OUTM 1824 3904 2112 3904 2112 3744 2304 3744 hwout.hwout#197.outp
w 1954 3939 100 0 n#223 egenSubB.$(top)$(dev)tempMonG.OUTL 1824 3936 2144 3936 2144 3808 2304 3808 hwout.hwout#196.outp
w 1970 3971 100 0 n#222 egenSubB.$(top)$(dev)tempMonG.OUTK 1824 3968 2176 3968 2176 3872 2304 3872 hwout.hwout#195.outp
w 1986 4003 100 0 n#221 egenSubB.$(top)$(dev)tempMonG.OUTJ 1824 4000 2208 4000 2208 3936 2304 3936 hwout.hwout#194.outp
w 2002 4035 100 0 n#220 egenSubB.$(top)$(dev)tempMonG.OUTI 1824 4032 2240 4032 2240 4000 2304 4000 hwout.hwout#193.outp
w 1986 4099 100 0 n#219 egenSubB.$(top)$(dev)tempMonG.OUTG 1824 4096 2208 4096 2208 4128 2304 4128 hwout.hwout#207.outp
w 1970 4131 100 0 n#218 egenSubB.$(top)$(dev)tempMonG.OUTF 1824 4128 2176 4128 2176 4192 2304 4192 hwout.hwout#206.outp
w 1954 4163 100 0 n#217 egenSubB.$(top)$(dev)tempMonG.OUTE 1824 4160 2144 4160 2144 4256 2304 4256 hwout.hwout#205.outp
w 1938 4195 100 0 n#216 egenSubB.$(top)$(dev)tempMonG.OUTD 1824 4192 2112 4192 2112 4320 2304 4320 hwout.hwout#204.outp
w 1922 4227 100 0 n#215 egenSubB.$(top)$(dev)tempMonG.OUTC 1824 4224 2080 4224 2080 4384 2304 4384 hwout.hwout#203.outp
w 2146 4451 100 0 n#214 egenSubB.$(top)$(dev)tempMonG.OUTB 1824 4256 2048 4256 2048 4448 2304 4448 hwout.hwout#202.outp
w 2130 4515 100 0 n#213 egenSubB.$(top)$(dev)tempMonG.OUTA 1824 4288 2016 4288 2016 4512 2304 4512 hwout.hwout#201.outp
w 2034 4067 100 0 n#212 hwout.hwout#208.outp 2304 4064 1824 4064 egenSubB.$(top)$(dev)tempMonG.OUTH
w -126 2691 100 0 n#228 egenSub.$(top)$(dev)tempSetG.VALC -224 2688 32 2688 32 2784 208 2784 egenSub.$(top)$(dev)arrayG.INPA
s -992 1184 120 0 command mode
s -992 1152 120 0 socket number
s -400 3840 120 0 heartbeat records
s -400 3904 120 0 status update
s -400 3968 120 0 status gensubs
s 388 4426 100 0 to cool instrument
s -144 4544 100 0 pressure upper
s 388 4456 100 0 cryostat pressure OK
s -2096 2211 120 0 heater power
s -2058 2268 120 0 set point
s -1376 1808 120 0 heater power
s 1894 4296 100 0 cold1
s 1894 4263 100 0 cold2
s 1893 4074 100 0 middle
s 1894 4233 100 0 active
s 1895 4203 100 0 passive
s 1896 4168 100 0 window
s 1893 4137 100 0 strap
s 1893 4104 100 0 edge
s 1352 4059 100 0 middle upper
s 1365 4124 100 0 strap upper
s 1375 4094 100 0 edge upper
s 67 2688 100 0 array lower
s 65 2752 100 0 array upper
s -718 2828 120 0 command mode
s 2368 0 240 0 envCont13.sch 2/13/2002 1:25 PM EST
s -718 2767 120 0 set point
s -718 2632 120 0 P value
s -718 2569 120 0 I value
s -718 2507 120 0 D value
s -718 2702 120 0 heater power
s -718 2442 120 0 auto tune
s -211 2698 120 0 set point
s -211 2558 120 0 P value
s -211 2495 120 0 I value
s -211 2433 120 0 D value
s -211 2624 120 0 heater power
s -211 2373 120 0 auto tune
s 1367 4291 100 0 cold1 upper
s 1364 4251 100 0 cold2 upper
s 1353 4220 100 0 active upper
s 1345 4188 100 0 passive upper
s 1355 4156 100 0 window upper
s 56 2626 100 0 finger upper
s 1894 4041 100 0 cold1 OK
s 1894 4008 100 0 cold2 OK
s 1893 3819 100 0 middle OK
s 565 2794 100 0 Detector (current)
s 1894 3978 100 0 active OK
s 1895 3948 100 0 passive OK
s 1896 3913 100 0 window OK
s 568 2732 100 0 finger
s 1893 3882 100 0 strap OK
s 1893 3849 100 0 edge OK
s -210 2814 120 0 command mode
s -210 2750 120 0 socket number
s -1376 1872 120 0 set point
s 569 2667 100 0 Detector OK
s 569 2604 100 0 finger OK
s 388 4520 100 0 cryostat pressure
s 1248 1088 120 0 command mode
s 1248 1056 120 0 socket number
s -988 1120 120 0 power status
s 1247 1026 120 0 power status
s 569 2540 100 0 det deadband
[cell use]
use elongins -2880 4135 100 0 elongins#498
xform 0 -2752 4208
p -2768 4128 100 1024 -1 name:$(ec)L218heartbeat
use ecalcouts -2528 5095 100 0 ecalcouts#497
xform 0 -2368 5216
p -2456 5128 100 0 -1 CALC:A
p -1696 5678 100 0 0 OOPT:On Change
p -2440 5328 100 0 1 SCAN:Passive
p -2464 5096 100 0 -1 name:$(ec)vacHbMonitor
p -2544 5288 75 0 -1 palrm(INPA):
p -2576 5288 75 0 -1 pproc(INPA):CPP
use ecalcouts -2528 4743 100 0 ecalcouts#483
xform 0 -2368 4864
p -2456 4776 100 0 -1 CALC:A
p -1696 5326 100 0 0 OOPT:On Change
p -2440 4976 100 0 1 SCAN:Passive
p -2464 4744 100 0 -1 name:$(ec)LVDTHbMonitor
p -2544 4936 75 0 -1 palrm(INPA):
p -2576 4936 75 0 -1 pproc(INPA):CPP
use ecalcouts -2464 3719 100 0 ecalcouts#473
xform 0 -2304 3840
p -2392 3752 100 0 -1 CALC:A
p -1632 4302 100 0 0 OOPT:On Change
p -2376 3952 100 0 1 SCAN:Passive
p -2400 3720 100 0 -1 name:$(ec)gp354HbMonitor
p -2480 3912 75 0 -1 palrm(INPA):
p -2512 3912 75 0 -1 pproc(INPA):CPP
use ecalcouts -2464 3367 100 0 ecalcouts#463
xform 0 -2304 3488
p -2392 3400 100 0 -1 CALC:A
p -1632 3950 100 0 0 OOPT:On Change
p -2376 3600 100 0 1 SCAN:Passive
p -2400 3368 100 0 -1 name:$(ec)ps6440HbMonitor
p -2480 3560 75 0 -1 palrm(INPA):
p -2512 3560 75 0 -1 pproc(INPA):CPP
use ecalcouts -2528 4071 100 0 ecalcouts#461
xform 0 -2368 4192
p -2456 4104 100 0 -1 CALC:A
p -1696 4654 100 0 0 OOPT:On Change
p -2440 4304 100 0 1 SCAN:Passive
p -2464 4072 100 0 -1 name:$(ec)l218HbMonitor
p -2544 4264 75 0 -1 palrm(INPA):
p -2576 4264 75 0 -1 pproc(INPA):CPP
use ecalcouts -2528 4423 100 0 ecalcouts#460
xform 0 -2368 4544
p -2456 4456 100 0 -1 CALC:A
p -1696 5006 100 0 0 OOPT:On Change
p -2440 4656 100 0 1 SCAN:Passive
p -2464 4424 100 0 -1 name:$(ec)ls340HbMonitor
p -2544 4616 75 0 -1 palrm(INPA):
p -2576 4616 75 0 -1 pproc(INPA):CPP
use flamOneShot -2112 5127 100 0 flamOneShot#487
xform 0 -1984 5248
p -2456 5128 100 0 -1 CALC:A
p -1696 5678 100 0 0 OOPT:On Change
p -2440 5328 100 0 1 SCAN:Passive
p -2464 5096 100 0 -1 name:$(ec)ls340HbMonitor
p -2544 5288 75 0 -1 palrm(INPA):
p -2576 5288 75 0 -1 pproc(INPA):CPP
p -2111 5059 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p -2111 5092 100 0 1 setTimer:timer $(ec)VacHbTimer
use flamOneShot -2112 4775 100 0 flamOneShot#482
xform 0 -1984 4896
p -2111 4707 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p -2111 4740 100 0 1 setTimer:timer $(ec)LVDTHbTimer
use flamOneShot -2112 4455 100 0 flamOneShot#256
xform 0 -1984 4576
p -2111 4387 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p -2111 4420 100 0 1 setTimer:timer $(ec)ls340HbTimer
use flamOneShot -2112 4103 100 0 flamOneShot#258
xform 0 -1984 4224
p -2111 4069 100 0 1 setTimer:timer $(ec)l218HbTimer
p -2111 4038 100 0 1 setTimout:timeout $(HB_TIMEOUT)
use flamOneShot -2112 3751 100 0 flamOneShot#259
xform 0 -1984 3872
p -2113 3719 100 0 1 setTimer:timer $(ec)gp354HbTimer
p -2111 3687 100 0 1 setTimout:timeout $(HB_TIMEOUT)
use flamOneShot -2112 3399 100 0 flamOneShot#426
xform 0 -1984 3520
p -2112 3360 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p -2112 3392 100 0 1 setTimer:timer $(ec)ps6440HbTmr
use elongins -2880 5159 100 0 elongins#485
xform 0 -2752 5232
p -3091 5433 100 0 0 DESC:Heartbeat from LS340 Agent
p -2768 5152 100 1024 -1 name:$(ec)Vacheartbeat
use elongins -2880 4807 100 0 elongins#484
xform 0 -2752 4880
p -3091 5081 100 0 0 DESC:Heartbeat from LS340 Agent
p -2768 4800 100 1024 -1 name:$(ec)LVDTheartbeat
use elongins -2880 3783 100 0 elongins#249
xform 0 -2752 3856
p -3091 4057 100 0 0 DESC:Heartbeat from GP332 Agent
p -2768 3776 100 1024 -1 name:$(ec)L332heartbeat
use elongins -2880 4487 100 0 elongins#247
xform 0 -2752 4560
p -3091 4761 100 0 0 DESC:Heartbeat from LS340 Agent
p -2768 4480 100 1024 -1 name:$(ec)PLCheartbeat
use elongins -2880 3431 100 0 elongins#428
xform 0 -2752 3504
p -2768 3424 100 1024 -1 name:$(ec)Barheartbeat
use hwout 544 4407 100 0 hwout#295
xform 0 640 4448
p 747 4439 100 0 -1 val(outp):$(sad)cryostatPressureOK.VAL PP NMS
use hwout -1248 1751 100 0 hwout#235
xform 0 -1152 1792
p -1048 1782 100 0 -1 val(outp):$(sad)heaterPower.VAL PP NMS
use hwout 704 2679 100 0 hwout#234
xform 0 800 2720
p 904 2712 100 0 -1 val(outp):$(sad)tSFinger.VAL PP NMS
use hwout 704 2743 100 0 hwout#233
xform 0 800 2784
p 904 2776 100 0 -1 val(outp):$(sad)tSDetector.VAL PP NMS
use hwout 2304 4023 100 0 hwout#208
xform 0 2400 4064
p 2503 4056 100 0 -1 val(outp):$(sad)tSMiddle.VAL PP NMS
use hwout 2304 4087 100 0 hwout#207
xform 0 2400 4128
p 2503 4122 100 0 -1 val(outp):$(sad)tSEdge.VAL PP NMS
use hwout 2304 4151 100 0 hwout#206
xform 0 2400 4192
p 2503 4186 100 0 -1 val(outp):$(sad)tSStrap.VAL PP NMS
use hwout 2304 4215 100 0 hwout#205
xform 0 2400 4256
p 2503 4248 100 0 -1 val(outp):$(sad)tSWindow.VAL PP NMS
use hwout 2304 4279 100 0 hwout#204
xform 0 2400 4320
p 2503 4312 100 0 -1 val(outp):$(sad)tSPassiveShield.VAL PP NMS
use hwout 2304 4343 100 0 hwout#203
xform 0 2400 4384
p 2503 4376 100 0 -1 val(outp):$(sad)tSActiveShield.VAL PP NMS
use hwout 2304 4407 100 0 hwout#202
xform 0 2400 4448
p 2503 4440 100 0 -1 val(outp):$(sad)tSColdhead2.VAL PP NMS
use hwout 2304 4471 100 0 hwout#201
xform 0 2400 4512
p 2503 4504 100 0 -1 val(outp):$(sad)tSColdhead1.VAL PP NMS
use hwout 2304 3511 100 0 hwout#200
xform 0 2400 3552
p 2503 3542 100 0 -1 val(outp):$(sad)tSMiddleOK.VAL PP NMS
use hwout 2304 3575 100 0 hwout#199
xform 0 2400 3616
p 2503 3608 100 0 -1 val(outp):$(sad)tSEdgeOK.VAL PP NMS
use hwout 2304 3639 100 0 hwout#198
xform 0 2400 3680
p 2503 3673 100 0 -1 val(outp):$(sad)tSStrapOK.VAL PP NMS
use hwout 2304 3703 100 0 hwout#197
xform 0 2400 3744
p 2503 3736 100 0 -1 val(outp):$(sad)tSWindowOK.VAL PP NMS
use hwout 2304 3767 100 0 hwout#196
xform 0 2400 3808
p 2503 3799 100 0 -1 val(outp):$(sad)tSPassiveShieldOK.VAL PP NMS
use hwout 2304 3831 100 0 hwout#195
xform 0 2400 3872
p 2503 3863 100 0 -1 val(outp):$(sad)tSActiveShieldOK.VAL PP NMS
use hwout 2304 3895 100 0 hwout#194
xform 0 2400 3936
p 2503 3927 100 0 -1 val(outp):$(sad)tSColdhead2OK.VAL PP NMS
use hwout 2304 3959 100 0 hwout#193
xform 0 2400 4000
p 2503 3994 100 0 -1 val(outp):$(sad)tSColdhead1OK.VAL PP NMS
use hwout -1248 1815 100 0 hwout#250
xform 0 -1152 1856
p -1048 1849 100 0 -1 val(outp):$(sad)heaterSetpoint.VAL PP NMS
use hwout -704 4023 100 0 hwout#266
xform 0 -608 4064
p -505 4057 100 0 -1 val(outp):$(sad)ecHeartbeat.VAL PP NMS
use hwout 544 4471 100 0 hwout#280
xform 0 640 4512
p 745 4504 100 0 -1 val(outp):$(sad)cryostatPressure.VAL PP NMS
use hwout 704 2615 100 0 hwout#289
xform 0 800 2656
p 904 2648 100 0 -1 val(outp):$(sad)tSDetectorOK.VAL PP NMS
use hwout 704 2551 100 0 hwout#290
xform 0 800 2592
p 904 2586 100 0 -1 val(outp):$(sad)tSFingerOK.VAL PP NMS
use hwout 704 2487 100 0 hwout#458
xform 0 800 2528
p 904 2522 100 0 -1 val(outp):$(sad)detTempErrorBand.VAL PP NMS
use estringins 64 3463 100 0 estringins#456
xform 0 192 3536
p 176 3456 100 1024 -1 name:$(ec)log
use egenSub -704 3175 100 0 egenSub#288
xform 0 -560 3600
p -861 3915 100 0 0 DESC:Configuration of EC
p -927 2949 100 0 0 FTVA:STRING
p -927 2917 100 0 0 FTVC:STRING
p -992 3582 100 0 0 INAM:ufecconfigGinit
p -992 3550 100 0 0 SNAM:ufecconfigGproc
p -592 3168 100 1024 -1 name:$(ec)configG
use egenSub 208 2023 -100 0 $(top)$(dev)arrayG
xform 0 352 2448
p 51 2763 100 0 0 DESC:Array Temp Status Gensub
p -15 1541 100 0 0 FTJ:STRING
p -15 1797 100 0 0 FTVA:STRING
p -15 1797 100 0 0 FTVB:STRING
p -15 1765 100 0 0 FTVC:STRING
p -15 1733 100 0 0 FTVD:STRING
p -15 1701 100 0 0 FTVE:STRING
p -80 2430 100 0 0 INAM:ufarrayGinit
p -80 2398 100 0 0 SNAM:ufarrayGproc
p 320 2016 100 1024 -1 name:$(ec)arrayG
p 160 2794 75 0 -1 pproc(INPA):NPP
p 160 2730 75 0 -1 pproc(INPB):NPP
p 160 2666 75 0 -1 pproc(INPC):NPP
p 496 2794 75 0 -1 pproc(OUTA):PP
p 496 2730 75 0 -1 pproc(OUTB):PP
p 496 2666 75 0 -1 pproc(OUTC):PP
p 496 2602 75 0 -1 pproc(OUTD):PP
p 496 2538 75 0 -1 pproc(OUTE):PP
use egenSub -512 2023 -100 0 $(top)$(dev)tempSetG
xform 0 -368 2448
p -669 2763 100 0 0 DESC:Temp Control Gensub
p -735 1797 100 0 0 FTA:LONG
p -735 1765 100 0 0 FTC:LONG
p -735 1701 100 0 0 FTE:DOUBLE
p -735 1637 100 0 0 FTF:DOUBLE
p -735 1637 100 0 0 FTG:LONG
p -735 1605 100 0 0 FTH:STRING
p -735 1541 100 0 0 FTJ:STRING
p -735 1797 100 0 0 FTVA:LONG
p -735 1797 100 0 0 FTVB:LONG
p -735 1733 100 0 0 FTVD:LONG
p -735 1637 100 0 0 FTVG:DOUBLE
p -735 1605 100 0 0 FTVH:LONG
p -735 1573 100 0 0 FTVI:LONG
p -735 1541 100 0 0 FTVJ:STRING
p -800 2430 100 0 0 INAM:uftempSetGinit
p -800 2574 100 0 0 PREC:5
p -800 2398 100 0 0 SNAM:uftempSetGproc
p -400 2016 100 1024 -1 name:$(ec)tempSetG
p -224 2730 75 0 -1 pproc(OUTB):NPP
use egenSub 32 3751 100 0 egenSub#252
xform 0 176 4176
p -125 4491 100 0 0 DESC:Pressure Status Gensub
p -191 3269 100 0 0 FTJ:STRING
p -191 3525 100 0 0 FTVA:STRING
p -191 3525 100 0 0 FTVB:STRING
p -191 3493 100 0 0 FTVC:STRING
p -191 3461 100 0 0 FTVD:STRING
p -256 4158 100 0 0 INAM:ufpressMonGinit
p -256 4126 100 0 0 SNAM:ufpressMonGproc
p 144 3744 100 1024 -1 name:$(ec)pressMonG
p 320 4522 75 0 -1 pproc(OUTA):PP
p 320 4458 75 0 -1 pproc(OUTB):PP
p 320 4394 75 0 -1 pproc(OUTC):PP
p 320 4330 75 0 -1 pproc(OUTD):PP
use egenSub 2080 2503 100 0 egenSub#440
xform 0 2224 2928
p 1808 3264 100 0 0 FTA:LONG
p 1857 2277 100 0 0 FTB:STRING
p 1808 2880 100 0 0 FTC:LONG
p 1857 2213 100 0 0 FTD:STRING
p 1824 2496 100 0 0 FTE:LONG
p 1857 2117 100 0 0 FTF:STRING
p 1857 2277 100 0 0 FTVA:STRING
p 1857 2277 100 0 0 FTVB:LONG
p 1857 2245 100 0 0 FTVC:LONG
p 1857 2213 100 0 0 FTVD:STRING
p 1857 2181 100 0 0 FTVE:STRING
p 1792 2910 100 0 0 INAM:ufflamNullGInit
p 1792 2878 100 0 0 SNAM:ufflamCmdCombineGProcess
p 2192 2496 100 1024 -1 name:$(ec)MrgCarG
p 2368 3210 75 0 -1 pproc(OUTB):PP
p 2368 3082 75 0 -1 pproc(OUTD):PP
p 2368 3018 75 0 -1 pproc(OUTE):PP
use ecars 1408 2983 100 0 ecars#423
xform 0 1568 3152
p 1520 2976 100 1024 -1 name:$(ec)tempSetC
use ecars 2688 3015 -100 0 $(top)ec:applyC
xform 0 2848 3184
p 2784 3232 100 0 0 DESC:EC CAR record
p 2800 3008 100 1024 -1 name:$(ec)applyC
use ecars 1408 2599 100 0 ecars#363
xform 0 1568 2768
p 1520 2592 100 1024 -1 name:$(ec)chPowerC
use ecars 1408 2215 100 0 ecars#385
xform 0 1568 2384
p 1520 2208 100 1024 -1 name:$(ec)vacPowerC
use efanouts -2160 839 100 0 efanouts#354
xform 0 -2040 992
p -2048 832 100 1024 -1 name:$(ec)chInitFn
use efanouts -2256 71 100 0 efanouts#357
xform 0 -2136 224
p -2144 64 100 1024 -1 name:$(ec)chPwrFn
use efanouts 80 743 100 0 efanouts#379
xform 0 200 896
p 192 736 100 1024 -1 name:$(ec)VacIntFn
use efanouts -16 -25 100 0 efanouts#382
xform 0 104 128
p 96 -32 100 1024 -1 name:$(ec)vacPwrFn
use estringouts -1824 615 100 0 estringouts#351
xform 0 -1696 688
p -1792 576 100 0 1 OMSL:closed_loop
p -1792 608 100 768 -1 name:$(ec)chPwrWrt
use estringouts -480 1767 -100 0 $(top)ec:debug
xform 0 -352 1840
p -453 1880 100 0 0 DESC:EC Debug Record
p -368 1760 100 1024 -1 name:$(ec)debug
use estringouts 416 519 100 0 estringouts#378
xform 0 544 592
p 448 480 100 0 1 OMSL:closed_loop
p 448 512 100 768 -1 name:$(ec)vacPwrWrt
use elongouts -1792 1095 100 0 elongouts#350
xform 0 -1664 1184
p -1760 1056 100 0 1 OMSL:closed_loop
p -1760 1088 100 768 -1 name:$(ec)chInitWrt
use elongouts -1248 2087 100 0 elongouts#310
xform 0 -1120 2176
p -1363 2521 100 0 0 DESC:EC Heartbeat
p -1184 2048 100 0 1 OMSL:closed_loop
p -1184 2080 100 0 1 name:$(ec)powerWrite
p -992 2144 75 768 -1 pproc(OUT):NPP
use elongouts -1248 2759 100 0 elongouts#309
xform 0 -1120 2848
p -1363 3193 100 0 0 DESC:EC Heartbeat
p -1184 2720 100 0 1 OMSL:closed_loop
p -1184 2752 100 0 1 name:$(ec)simLevelWrite
p -992 2816 75 768 -1 pproc(OUT):NPP
use elongouts -1088 4007 100 0 elongouts#265
xform 0 -960 4096
p -1203 4441 100 0 0 DESC:EC Heartbeat
p -1024 3968 100 0 1 OMSL:closed_loop
p -880 4000 100 1024 1 name:$(ec)heartbeat
p -832 4064 75 768 -1 pproc(OUT):PP
use elongouts 448 1031 100 0 elongouts#377
xform 0 576 1120
p 480 992 100 0 1 OMSL:closed_loop
p 480 1024 100 768 -1 name:$(ec)vacInitWrt
use ecad2 -2848 103 100 0 ecad2#349
xform 0 -2688 416
p -2752 128 100 0 0 SNAM:ecVacChPowerCommand
p -2736 96 100 1024 -1 name:$(ec)chPower
use ecad2 -2624 871 100 0 ecad2#340
xform 0 -2464 1184
p -2528 1184 100 0 0 FTVA:LONG
p -2528 896 100 0 0 SNAM:ecVacChInitCommand
p -2512 864 100 1024 -1 name:$(ec)chInit
use ecad2 -1920 2567 100 0 ecad2#169
xform 0 -1760 2880
p -1824 3040 100 0 0 DESC:Init CAD for EC
p -1824 2880 100 0 0 FTVA:DOUBLE
p -1824 2848 100 0 0 FTVB:LONG
p -1824 2816 100 0 0 INAM:ecinitCadInit
p -1824 2592 100 0 0 SNAM:ecinitCadProc
p -1808 2560 100 1024 -1 name:$(ec)init
use ecad2 -608 7 100 0 ecad2#375
xform 0 -448 320
p -512 32 100 0 0 SNAM:ecVacChPowerCommand
p -496 0 100 1024 -1 name:$(ec)vacPower
use ecad2 -384 775 100 0 ecad2#376
xform 0 -224 1088
p -288 1088 100 0 0 FTVA:LONG
p -288 800 100 0 0 SNAM:ecVacChInitCommand
p -272 768 100 1024 -1 name:$(ec)vacInit
use egenSubB -1344 391 100 0 egenSubB#343
xform 0 -1200 816
p -1567 165 100 0 0 FTA:LONG
p -1567 133 100 0 0 FTC:STRING
p -1567 69 100 0 0 FTE:STRING
p -1567 -91 100 0 0 FTJ:STRING
p -1567 165 100 0 0 FTVA:LONG
p -1567 165 100 0 0 FTVB:LONG
p -1567 133 100 0 0 FTVC:STRING
p -1632 798 100 0 0 INAM:ufdevPowerGinit
p -1632 766 100 0 0 SNAM:ufdevPowerGproc
p -1232 384 100 1024 -1 name:$(ec)chPowerG
use egenSubB 1536 3495 -100 0 $(top)$(dev)tempMonG
xform 0 1680 3920
p 1379 4235 100 0 0 DESC:Temperature Status Gensub
p 1313 3013 100 0 0 FTJ:STRING
p 1313 3269 100 0 0 FTVA:STRING
p 1313 3269 100 0 0 FTVB:STRING
p 1313 3237 100 0 0 FTVC:STRING
p 1313 3205 100 0 0 FTVD:STRING
p 1313 3173 100 0 0 FTVE:STRING
p 1313 3109 100 0 0 FTVF:STRING
p 1313 3109 100 0 0 FTVG:STRING
p 1313 3077 100 0 0 FTVH:STRING
p 1313 3045 100 0 0 FTVI:STRING
p 1313 3013 100 0 0 FTVJ:STRING
p 1313 3013 100 0 0 FTVK:STRING
p 1313 3013 100 0 0 FTVL:STRING
p 1313 3013 100 0 0 FTVM:STRING
p 1313 3013 100 0 0 FTVN:STRING
p 1313 3013 100 0 0 FTVO:STRING
p 1313 3013 100 0 0 FTVP:STRING
p 1248 3902 100 0 0 INAM:uftempMonGinit
p 1248 3870 100 0 0 SNAM:uftempMonGproc
p 1648 3488 100 1024 -1 name:$(ec)tempMonG
p 1824 4298 75 0 -1 pproc(OUTA):PP
p 1824 4266 75 0 -1 pproc(OUTB):PP
p 1824 4234 75 0 -1 pproc(OUTC):PP
p 1824 4202 75 0 -1 pproc(OUTD):PP
p 1824 4170 75 0 -1 pproc(OUTE):PP
p 1824 4138 75 0 -1 pproc(OUTF):PP
p 1824 4106 75 0 -1 pproc(OUTG):PP
p 1824 4074 75 0 -1 pproc(OUTH):PP
p 1824 4042 75 0 -1 pproc(OUTI):PP
p 1824 4010 75 0 -1 pproc(OUTJ):PP
p 1824 3978 75 0 -1 pproc(OUTK):PP
p 1824 3946 75 0 -1 pproc(OUTL):PP
p 1824 3914 75 0 -1 pproc(OUTM):PP
p 1824 3882 75 0 -1 pproc(OUTN):PP
p 1824 3850 75 0 -1 pproc(OUTO):PP
p 1824 3818 75 0 -1 pproc(OUTP):PP
use egenSubB 896 295 100 0 egenSubB#374
xform 0 1040 720
p 673 69 100 0 0 FTA:LONG
p 673 37 100 0 0 FTC:STRING
p 673 -27 100 0 0 FTE:STRING
p 673 -187 100 0 0 FTJ:STRING
p 673 69 100 0 0 FTVA:LONG
p 673 69 100 0 0 FTVB:LONG
p 673 37 100 0 0 FTVC:STRING
p 608 702 100 0 0 INAM:ufdevPowerGinit
p 608 670 100 0 0 SNAM:ufdevPowerGproc
p 1008 288 100 1024 -1 name:$(ec)vacPowerG
use flamLinkEnable -1824 167 100 0 flamLinkEnable#341
xform 0 -1664 288
p -1824 160 100 0 1 setSystem:system ec:ch
use flamLinkEnable -736 1479 100 0 flamLinkEnable#327
xform 0 -576 1600
p -736 1472 100 0 1 setSystem:system $(ec)
use flamLinkEnable 416 71 100 0 flamLinkEnable#373
xform 0 576 192
p 416 64 100 0 1 setSystem:system ec:vac
use eaos -1248 2439 100 0 eaos#311
xform 0 -1120 2528
p -1184 2400 100 0 1 OMSL:closed_loop
p -1184 2432 100 0 1 name:$(ec)setpointWrite
use ecalcs -1504 3783 100 0 ecalcs#263
xform 0 -1360 4048
p -1440 3728 100 0 1 CALC:((F=2)||(B&&C&&D&&E&&G&&H))?A+1:0
p -1711 4214 100 0 0 DESC:calculation of EC Heartbeat
p -1440 3696 100 0 1 SCAN:1 second
p -1280 3760 100 1024 1 name:$(ec)heartbeatCalc
use eapply -2880 2247 100 0 eapply#183
xform 0 -2688 2608
p -2784 2592 100 0 0 DESC:EC Apply Record
p -2768 2240 100 1024 -1 name:$(ec)apply
use ecad8 -1920 1511 -100 0 $(top)ec:tempSet
xform 0 -1760 2016
p -1824 2368 100 0 0 DESC:Temp Control CAD
p -1824 2208 100 0 0 FTVA:DOUBLE
p -1824 2176 100 0 0 FTVB:LONG
p -1824 2144 100 0 0 FTVC:DOUBLE
p -1824 2112 100 0 0 FTVD:DOUBLE
p -1824 2080 100 0 0 FTVE:LONG
p -1824 2048 100 0 0 FTVF:LONG
p -1824 2016 100 0 0 FTVG:STRING
p -1824 1728 100 0 0 SNAM:ectempSetCadProc
p -1808 1504 100 1024 -1 name:$(ec)tempSet
p -1600 1920 75 768 -1 pproc(OUTF):NPP
p -1600 1856 75 768 -1 pproc(OUTG):PP
p -1600 1792 75 768 -1 pproc(OUTH):PP
p -1600 1610 75 0 -1 pproc(STLK):PP
[comments]
RCS: "$Name:  $ $Id: flamEcTop.sch,v 1.1 2006/09/14 22:27:48 gemvx Exp $"
