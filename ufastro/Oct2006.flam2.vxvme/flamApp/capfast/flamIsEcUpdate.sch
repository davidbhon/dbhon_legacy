[schematic2]
uniq 58
[tools]
[detail]
w 8 1195 -100 0 ENABLE inhier.ENABLE.P -32 1184 96 1184 96 1072 208 1072 efanouts.efanouts#24.SDIS
w 58 1051 -100 0 START inhier.START.P -32 1040 208 1040 efanouts.efanouts#24.SLNK
w 1304 715 -100 0 CAR_IVAL efanouts.efanouts#24.LNK5 448 992 560 992 560 704 2144 704 outhier.FLNK.p
w 840 1067 100 0 n#30 efanouts.efanouts#24.LNK3 448 1056 1280 1056 elongouts.elongouts#14.SLNK
w 928 1323 100 0 n#27 efanouts.efanouts#24.LNK2 448 1088 624 1088 624 1312 1280 1312 estringouts.estringouts#52.SLNK
w 896 1579 100 0 n#31 efanouts.efanouts#24.LNK1 448 1120 560 1120 560 1568 1280 1568 estringouts.estringouts#51.SLNK
w 1200 1099 100 0 n#17 hwin.hwin#18.in 1168 1152 1168 1088 1280 1088 elongouts.elongouts#14.DOL
w 1566 1307 100 0 n#15 estringouts.estringouts#52.OUT 1536 1296 1632 1296 hwout.hwout#16.outp
w 1560 1035 100 0 n#10 elongouts.elongouts#14.OUT 1536 1024 1632 1024 hwout.hwout#13.outp
w 1166 1347 100 0 n#9 hwin.hwin#54.in 1088 1440 1088 1344 1280 1344 estringouts.estringouts#52.DOL
w 1566 1563 100 0 n#6 estringouts.estringouts#51.OUT 1536 1552 1632 1552 hwout.hwout#4.outp
w 1206 1611 100 0 n#5 hwin.hwin#57.in 1168 1664 1168 1600 1280 1600 estringouts.estringouts#51.DOL
s 2624 2000 100 1792 2002/01/28
s 2480 2000 100 1792 LTF
s 2240 2000 100 1792 Revised sub-system interfaces
s 2016 2000 100 1792 C
s 2624 2064 100 1792 2001/01/20
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamIsEcUpdate.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2002/09/17
s 2320 -240 100 1792 Rev: D
s 2432 -192 100 256 Flamingos Environment Controller Update
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s 2624 2032 100 1792 2001/02/10
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Revised sub-system interfaces
s 2016 2032 100 1792 B
s 2624 1968 100 1792 2002/09/17
s 2480 1968 100 1792 WNR
s 2240 1968 100 1792 Added enable input
s 2016 1968 100 1792 D
[cell use]
use hwin 976 1623 100 0 hwin#57
xform 0 1072 1664
p 979 1656 100 0 -1 val(in):3
use changeBar 1984 1959 100 0 changeBar#53
xform 0 2336 2000
use changeBar 1984 2023 100 0 changeBar#49
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#50
xform 0 2336 2032
use changeBar 1984 1927 100 0 changeBar#56
xform 0 2336 1968
use inhier -128 1040 100 0 START
xform 0 -32 1040
use inhier -144 1184 100 0 ENABLE
xform 0 -32 1184
use hwin 896 1399 100 0 hwin#54
xform 0 992 1440
p 899 1432 100 0 -1 val(in):3
use hwin 976 1111 100 0 hwin#18
xform 0 1072 1152
p 944 1184 100 0 -1 val(in):$(CAD_START)
use estringouts 1280 1239 100 0 estringouts#52
xform 0 1408 1312
p 1344 1200 100 0 1 OMSL:closed_loop
p 1344 1232 100 0 1 name:$(is)powerUpdate
p 1536 1296 75 768 -1 pproc(OUT):PP
use estringouts 1280 1495 100 0 estringouts#51
xform 0 1408 1568
p 1344 1456 100 0 1 OMSL:closed_loop
p 1344 1488 100 0 1 name:$(is)tempUpdate
p 1536 1552 75 768 -1 pproc(OUT):PP
use outhier 2176 704 100 0 FLNK
xform 0 2128 704
use efanouts 208 903 100 0 efanouts#24
xform 0 328 1056
p 224 896 100 768 1 name:$(is)tempUpdateF
use elongouts 1280 967 100 0 elongouts#14
xform 0 1408 1056
p 1344 928 100 0 1 OMSL:closed_loop
p 1344 960 100 0 1 name:$(is)powerCommand
p 1536 1024 75 768 -1 pproc(OUT):PP
use hwout 1632 1255 100 0 hwout#16
xform 0 1728 1296
p 1856 1296 100 0 -1 val(outp):$(top)ec:tempSet.B
use hwout 1632 983 100 0 hwout#13
xform 0 1728 1024
p 1856 1024 100 0 -1 val(outp):$(top)ec:apply.DIR PP NMS
use hwout 1632 1511 100 0 hwout#4
xform 0 1728 1552
p 1856 1552 100 0 -1 val(outp):$(top)ec:tempSet.A
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamIsEcUpdate.sch,v 1.1 2006/09/14 22:27:50 gemvx Exp $
