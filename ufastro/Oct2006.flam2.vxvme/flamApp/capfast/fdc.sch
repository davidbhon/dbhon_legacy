[schematic2]
uniq 1311
[tools]
[detail]
w 3644 1611 100 0 n#1310 ecad20.ecad20#1170.VALD 3136 1472 3328 1472 3328 1600 4032 1600 estringouts.estringouts#844.DOL
w 4620 1355 100 0 n#1308 estringouts.estringouts#844.OUT 4288 1552 4512 1552 4512 1344 4800 1344 egenSubFLAM.egenSubFLAM#1212.D
w 3860 1547 100 0 n#1306 estringouts.estringouts#1305.FLNK 3808 1536 3984 1536 3984 1568 4032 1568 estringouts.estringouts#844.SLNK
w 2708 1579 100 0 n#1304 hwin.hwin#1303.in 2672 1568 2816 1568 ecad20.ecad20#1170.INPB
w 1596 811 100 0 n#1300 hwin.hwin#1251.in 1728 608 1792 608 1792 800 1472 800 1472 1344 1632 1344 eapply.eapply#1243.INMC
w 1564 779 100 0 n#1299 hwin.hwin#1250.in 1728 672 1760 672 1760 768 1440 768 1440 1376 1632 1376 eapply.eapply#1243.INPC
w 1980 811 100 0 n#1298 hwout.hwout#1249.outp 1920 608 1856 608 1856 800 2176 800 2176 1344 2016 1344 eapply.eapply#1243.OCLC
w 2012 779 100 0 n#1297 hwout.hwout#1248.outp 1920 672 1888 672 1888 768 2208 768 2208 1376 2016 1376 eapply.eapply#1243.OUTC
w 1564 331 100 0 n#1296 ecad4.ecad4#1265.MESS 1856 160 1952 160 1952 320 1248 320 1248 1472 1632 1472 eapply.eapply#1243.INMA
w 1532 299 100 0 n#1295 ecad4.ecad4#1265.VAL 1856 192 1920 192 1920 288 1216 288 1216 1504 1632 1504 eapply.eapply#1243.INPA
w 1868 427 100 0 n#1294 eapply.eapply#1243.OCLA 2016 1472 2400 1472 2400 416 1408 416 1408 160 1536 160 ecad4.ecad4#1265.ICID
w 1900 395 100 0 n#1293 eapply.eapply#1243.OUTA 2016 1504 2432 1504 2432 384 1440 384 1440 192 1536 192 ecad4.ecad4#1265.DIR
w 2428 -325 100 0 n#1292 estringouts.estringouts#1283.OUT 2432 -336 2496 -336 2496 -608 2560 -608 hwout.hwout#1263.outp
w 2620 1355 100 0 n#1291 estringouts.estringouts#1282.OUT 2432 -112 2560 -112 2560 256 2496 256 2496 1344 2816 1344 ecad20.ecad20#1170.F
w 2604 1675 100 0 n#1290 estringouts.estringouts#1281.OUT 2432 112 2528 112 2528 224 2464 224 2464 1664 2816 1664 ecad20.ecad20#1170.A
w 2252 -5 100 0 n#1289 estringouts.estringouts#1282.FLNK 2432 -80 2496 -80 2496 -16 2080 -16 2080 128 2176 128 estringouts.estringouts#1281.SLNK
w 2252 -229 100 0 n#1288 estringouts.estringouts#1283.FLNK 2432 -304 2496 -304 2496 -240 2080 -240 2080 -96 2176 -96 estringouts.estringouts#1282.SLNK
w 1932 -405 100 0 n#1287 ecad4.ecad4#1265.STLK 1856 -416 2080 -416 2080 -320 2176 -320 estringouts.estringouts#1283.SLNK
w 1916 -117 100 0 n#1286 ecad4.ecad4#1265.VALC 1856 -128 2048 -128 2048 -288 2176 -288 estringouts.estringouts#1283.DOL
w 1916 11 100 0 n#1285 ecad4.ecad4#1265.VALA 1856 0 2048 0 2048 160 2176 160 estringouts.estringouts#1281.DOL
w 1980 -53 100 0 n#1284 ecad4.ecad4#1265.VALB 1856 -64 2176 -64 estringouts.estringouts#1282.DOL
w 3692 -549 100 0 n#1280 hwin.hwin#1277.in 3664 -560 3792 -560 estringouts.estringouts#1274.DOL
w 4060 -605 100 0 n#1279 estringouts.estringouts#1274.OUT 4048 -608 4144 -608 hwout.hwout#1278.outp
w 3692 -589 100 0 n#1276 estringins.estringins#1273.FLNK 3616 -656 3664 -656 3664 -592 3792 -592 estringouts.estringouts#1274.SLNK
w 3676 -349 100 0 n#1271 elongins.elongins#1272.FLNK 3632 -352 3792 -352 elongouts.elongouts#1269.SLNK
w 3708 -317 100 0 n#1270 elongins.elongins#1272.VAL 3632 -384 3696 -384 3696 -320 3792 -320 elongouts.elongouts#1269.DOL
w 4060 -381 100 0 n#1268 elongouts.elongouts#1269.OUT 4048 -384 4144 -384 hwout.hwout#1267.outp
w 2284 2027 100 0 n#1262 ecad20.ecad20#1170.MESS 3136 1824 3200 1824 3200 2016 1440 2016 1440 1408 1632 1408 eapply.eapply#1243.INMB
w 2284 1995 100 0 n#1261 ecad20.ecad20#1170.VAL 3136 1856 3168 1856 3168 1984 1472 1984 1472 1440 1632 1440 eapply.eapply#1243.INPB
w 2476 1835 100 0 n#1260 eapply.eapply#1243.OCLB 2016 1408 2208 1408 2208 1824 2816 1824 ecad20.ecad20#1170.ICID
w 2460 1867 100 0 n#1259 eapply.eapply#1243.OUTB 2016 1440 2176 1440 2176 1856 2816 1856 ecad20.ecad20#1170.DIR
w 5244 1091 100 0 n#1256 egenSubFLAM.egenSubFLAM#1212.OUTL 5088 1088 5472 1088 5472 1056 5504 1056 hwout.hwout#1237.outp
w 5260 1123 100 0 n#1242 egenSubFLAM.egenSubFLAM#1212.OUTK 5088 1120 5504 1120 hwout.hwout#1236.outp
w 3884 -125 100 0 n#1210 ecad20.ecad20#1170.VALO 3136 768 3232 768 3232 -128 4608 -128 4608 64 4704 64 ecalcs.ecalcs#303.SDIS
w 6076 810 120 0 n#1208 estringouts.estringouts#839.FLNK 3872 2112 3968 2112 3968 2176 4640 2176 4640 1824 6080 1824 6080 -192 4128 -192 4128 64 4160 64 flamLinkEnable.flamLinkEnable#861.ENABLE
w 3725 1308 120 0 n#1226 efanouts.efanouts#863.LNK1 3584 128 3680 128 3680 608 3520 608 3520 1296 4016 1296 estringouts.estringouts#843.SLNK
w 4108 1419 100 0 n#1201 estringouts.estringouts#1305.OUT 3808 1504 3808 1408 4480 1408 4480 1312 4800 1312 egenSubFLAM.egenSubFLAM#1212.E
w 4204 1699 100 0 n#1199 estringouts.estringouts#841.OUT 3872 1792 3936 1792 3936 1696 4544 1696 4544 1376 4800 1376 egenSubFLAM.egenSubFLAM#1212.C
w 4572 1627 100 0 n#1198 estringouts.estringouts#840.OUT 4256 1856 4576 1856 4576 1408 4800 1408 egenSubFLAM.egenSubFLAM#1212.B
w 4204 2083 100 0 n#1197 estringouts.estringouts#839.OUT 3872 2080 4608 2080 4608 1440 4800 1440 egenSubFLAM.egenSubFLAM#1212.A
w 3228 1891 100 0 n#1190 ecad20.ecad20#1170.VALA 3136 1664 3232 1664 3232 2128 3616 2128 estringouts.estringouts#839.DOL
w 3596 1907 100 0 n#1189 ecad20.ecad20#1170.VALB 3136 1600 3264 1600 3264 1904 4000 1904 estringouts.estringouts#840.DOL
w 3420 1843 100 0 n#1188 ecad20.ecad20#1170.VALC 3136 1536 3296 1536 3296 1840 3616 1840 estringouts.estringouts#841.DOL
w 3212 1419 100 0 n#1186 ecad20.ecad20#1170.VALE 3136 1408 3360 1408 3360 1552 3552 1552 estringouts.estringouts#1305.DOL
w 3436 1355 100 0 n#1185 estringouts.estringouts#843.DOL 4016 1328 3808 1328 3808 1344 3136 1344 ecad20.ecad20#1170.VALF
w 4500 1291 100 0 n#1184 estringouts.estringouts#843.OUT 4272 1280 4800 1280 egenSubFLAM.egenSubFLAM#1212.F
w 3916 1883 100 0 n#1183 estringouts.estringouts#841.FLNK 3872 1824 3904 1824 3904 1872 4000 1872 estringouts.estringouts#840.SLNK
w 3868 1963 100 0 n#1182 estringouts.estringouts#840.FLNK 4256 1888 4288 1888 4288 1952 3520 1952 3520 2096 3616 2096 estringouts.estringouts#839.SLNK
w 3916 1675 100 0 n#1180 estringouts.estringouts#844.FLNK 4288 1584 4384 1584 4384 1664 3520 1664 3520 1808 3616 1808 estringouts.estringouts#841.SLNK
w 3868 1387 100 0 n#1179 estringouts.estringouts#843.FLNK 4272 1312 4288 1312 4288 1376 3520 1376 3520 1520 3552 1520 estringouts.estringouts#1305.SLNK
w 4572 379 100 0 n#1196 flamLinkEnable.flamLinkEnable#861.FLINK 4480 32 4576 32 4576 736 4800 736 egenSubFLAM.egenSubFLAM#1212.SLNK
w 3260 131 100 0 n#1227 ecad20.ecad20#1170.STLK 3136 224 3264 224 3264 48 3344 48 efanouts.efanouts#863.SLNK
w 3954 3 100 0 n#862 efanouts.efanouts#863.LNK2 3584 96 3808 96 3808 0 4160 0 flamLinkEnable.flamLinkEnable#861.SLINK
w 4786 547 100 0 n#650 junction 5024 288 5024 544 4608 544 4608 480 4704 480 ecalcs.ecalcs#303.INPA
w 5090 227 100 0 n#650 ecalcs.ecalcs#303.VAL 4992 288 5024 288 5024 224 5216 224 elongouts.elongouts#304.DOL
w 5500 651 100 0 n#1234 elongouts.elongouts#316.OUT 5472 640 5600 640 5600 -32 5696 -32 hwout.hwout#319.outp
w 5298 483 100 0 n#635 elongins.elongins#272.VAL 5472 384 5536 384 5536 480 5120 480 5120 704 5216 704 elongouts.elongouts#316.DOL
w 5330 515 100 0 n#636 elongins.elongins#272.FLNK 5472 416 5568 416 5568 512 5152 512 5152 672 5216 672 elongouts.elongouts#316.SLNK
w 4994 1539 100 0 n#341 egenSubFLAM.egenSubFLAM#1212.OUTE 5088 1312 5376 1312 5376 1536 4672 1536 4672 1696 4752 1696 hwout.hwout#338.outp
w 4962 1507 100 0 n#340 egenSubFLAM.egenSubFLAM#1212.OUTD 5088 1344 5344 1344 5344 1504 4640 1504 4640 1760 4752 1760 hwout.hwout#337.outp
w 5084 251 100 0 n#1230 ecalcs.ecalcs#303.FLNK 4992 320 5088 320 5088 192 5216 192 elongouts.elongouts#304.SLNK
w 5298 291 100 0 n#312 elongouts.elongouts#304.OUT 5472 160 5536 160 5536 288 5120 288 5120 400 5216 400 elongins.elongins#272.SLNK
s 5168 1248 120 0 dither
s 4615 922 100 0 Exposure time
s 4679 954 100 0 obs_mode
s 5161 1098 100 0 Exposure time
s 5160 1127 100 0 Total time
s 5163 1162 100 0 obs_mode
s 5164 1194 100 0 Frame Count
s 2659 1670 120 0 command
s 2675 1594 120 0 obs ID
s 2640 1520 120 0 DHS write
s 5184 -416 220 0 mce4_cont-18.sch 12/10/2001 4:15 PM EST
s 2640 1360 120 0 sim mode
s 5161 1387 100 0 command
s 5161 1350 100 0 obs ID
s 5163 1317 100 0 DHS write
s 5161 1451 100 0 sim mode
s 5168 1280 100 0 local archive
s 5161 1419 100 0 socket number
s 2656 1408 120 0 dither
s 2656 1472 120 0 archive
[cell use]
use estringouts 3616 2023 100 0 estringouts#839
xform 0 3744 2096
p 3648 1984 100 0 1 OMSL:closed_loop
p 3648 2016 100 768 -1 name:$(dc)commW
use estringouts 4000 1799 100 0 estringouts#840
xform 0 4128 1872
p 4016 1760 100 0 1 OMSL:closed_loop
p 4016 1792 100 768 -1 name:$(dc)obsIDW
use estringouts 3616 1735 100 0 estringouts#841
xform 0 3744 1808
p 3632 1696 100 0 1 OMSL:closed_loop
p 3632 1728 100 768 -1 name:$(dc)DHSwrtW
use estringouts 4016 1223 100 0 estringouts#843
xform 0 4144 1296
p 4032 1184 100 0 1 OMSL:closed_loop
p 4032 1216 100 768 -1 name:$(dc)SmModeW
use estringouts 4032 1495 100 0 estringouts#844
xform 0 4160 1568
p 4048 1456 100 0 1 OMSL:closed_loop
p 4048 1488 100 768 -1 name:$(dc)lclArchvW
use estringouts 3792 -665 100 0 estringouts#1274
xform 0 3920 -592
p 3840 -704 100 0 1 OMSL:closed_loop
p 3840 -672 100 768 -1 name:$(dc)dhsStatusOut
p 4048 -608 75 768 -1 pproc(OUT):PP
use estringouts 2176 55 100 0 estringouts#1281
xform 0 2304 128
p 2192 16 100 0 1 OMSL:closed_loop
p 2192 48 100 768 -1 name:$(dc)InitPushVala
use estringouts 2176 -169 100 0 estringouts#1282
xform 0 2304 -96
p 2192 -208 100 0 1 OMSL:closed_loop
p 2192 -176 100 768 -1 name:$(dc)InitPushValb
use estringouts 2176 -393 100 0 estringouts#1283
xform 0 2304 -320
p 2192 -432 100 0 1 OMSL:closed_loop
p 2192 -400 100 768 -1 name:$(dc)InitPushValc
use estringouts 3552 1447 100 0 estringouts#1305
xform 0 3680 1520
p 3568 1408 100 0 1 OMSL:closed_loop
p 3568 1440 100 768 -1 name:$(dc)ditherW
use hwin 1536 567 100 0 hwin#1251
xform 0 1632 608
p 1280 592 100 0 -1 val(in):$(dc)obsControl.MESS
use hwin 1536 631 100 0 hwin#1250
xform 0 1632 672
p 1296 656 100 0 -1 val(in):$(dc)obsControl.VAL
use hwin 3472 -601 100 0 hwin#1277
xform 0 3568 -560
p 3248 -576 100 0 -1 val(in):$(dc)dhsStatus.VAL
use hwin 2480 1527 100 0 hwin#1303
xform 0 2576 1568
p 2483 1560 100 0 -1 val(in):$(top)observe.VALA
use hwout 2560 -649 100 0 hwout#1263
xform 0 2656 -608
p 2768 -624 100 0 -1 val(outp):$(dc)obsControl.A NPP NMS
use hwout 1920 567 100 0 hwout#1249
xform 0 2016 608
p 2128 592 100 0 -1 val(outp):$(dc)obsControl.ICID
use hwout 1920 631 100 0 hwout#1248
xform 0 2016 672
p 2128 656 100 0 -1 val(outp):$(dc)obsControl.DIR
use hwout 5696 -73 100 0 hwout#319
xform 0 5792 -32
p 5895 -40 100 0 -1 val(outp):$(sad)dcHeartbeat.VAL PP NMS
use hwout 4752 1655 100 0 hwout#338
xform 0 4848 1696
p 4955 1686 100 0 -1 val(outp):$(sad)dataMode.VAL PP NMS
use hwout 4752 1719 100 0 hwout#337
xform 0 4848 1760
p 4955 1753 100 0 -1 val(outp):$(sad)obsID.VAL PP NMS
use hwout 5504 1079 100 0 hwout#1236
xform 0 5600 1120
p 5712 1104 100 0 -1 val(outp):$(sad)exposed.VAL PP NMS
use hwout 5504 1015 100 0 hwout#1237
xform 0 5600 1056
p 5712 1040 100 0 -1 val(outp):$(sad)elapsed.VAL PP NMS
use hwout 4144 -425 100 0 hwout#1267
xform 0 4240 -384
p 4240 -393 100 0 -1 val(outp):
use hwout 4144 -649 100 0 hwout#1278
xform 0 4240 -608
p 4240 -617 100 0 -1 val(outp):
use estringins 3360 -729 100 0 estringins#1273
xform 0 3488 -656
p 3472 -736 100 1024 -1 name:$(dc)dhsStatus
use elongins 5216 327 100 0 elongins#272
xform 0 5344 400
p 5005 601 100 0 0 DESC:Detector Controller Heartbeat
p 5328 320 100 1024 -1 name:$(dc)heartbeat
use elongins 3376 -441 100 0 elongins#1272
xform 0 3504 -368
p 3488 -448 100 1024 -1 name:$(dc)dhsHeartbeat
use elongouts 5216 583 100 0 elongouts#316
xform 0 5344 672
p 5101 1017 100 0 0 DESC:DC Heartbeat Longout
p 5216 544 100 0 1 OMSL:closed_loop
p 5216 576 100 768 -1 name:$(dc)heartbeatLO
p 5472 640 75 768 -1 pproc(OUT):PP
use elongouts 5216 103 100 0 elongouts#304
xform 0 5344 192
p 5101 537 100 0 0 DESC:DC Heartbeat is SIM FAST Mode
p 5228 68 100 0 1 OMSL:closed_loop
p 5328 96 100 1024 -1 name:$(dc)simHeartbeat
p 5472 160 75 768 -1 pproc(OUT):PP
use elongouts 3792 -441 100 0 elongouts#1269
xform 0 3920 -352
p 3840 -480 100 0 1 OMSL:closed_loop
p 3840 -448 100 768 -1 name:$(dc)dhsHeartbeatOut
p 4048 -384 75 768 -1 pproc(OUT):PP
use ecad4 1536 -505 100 0 ecad4#1265
xform 0 1696 -128
p 1472 -736 100 0 0 FTVB:STRING
p 1472 -768 100 0 0 FTVC:STRING
p 1472 -1056 100 0 0 SNAM:DCInitCommand
p 1648 -512 100 1024 -1 name:$(dc)init
p 1856 -160 75 768 -1 pproc(OUTC):NPP
use eapply 1632 967 100 0 eapply#1243
xform 0 1824 1328
p 1744 960 100 1024 -1 name:$(dc)apply
use flamDcPreAmp 2848 -569 100 0 flamDcPreAmp#1233
xform 0 2944 -448
use flamDcBias 2864 -329 100 0 flamDcBias#1232
xform 0 2944 -224
use egenSubFLAM 4800 647 100 0 egenSubFLAM#1212
xform 0 4944 1072
p 4577 421 100 0 0 FTA:STRING
p 4577 421 100 0 0 FTB:STRING
p 4577 389 100 0 0 FTC:STRING
p 4577 357 100 0 0 FTD:STRING
p 4577 325 100 0 0 FTE:STRING
p 4577 261 100 0 0 FTF:STRING
p 4577 261 100 0 0 FTG:STRING
p 4577 229 100 0 0 FTH:STRING
p 4577 197 100 0 0 FTI:STRING
p 4577 165 100 0 0 FTJ:STRING
p 4577 165 100 0 0 FTK:STRING
p 4577 165 100 0 0 FTL:STRING
p 4577 165 100 0 0 FTM:STRING
p 4577 165 100 0 0 FTN:STRING
p 4577 165 100 0 0 FTO:STRING
p 4577 165 100 0 0 FTP:STRING
p 4577 165 100 0 0 FTQ:STRING
p 4577 165 100 0 0 FTR:STRING
p 4577 165 100 0 0 FTS:STRING
p 4577 165 100 0 0 FTT:STRING
p 4577 165 100 0 0 FTU:STRING
p 4577 421 100 0 0 FTVA:LONG
p 4577 421 100 0 0 FTVB:LONG
p 4577 389 100 0 0 FTVC:STRING
p 4577 357 100 0 0 FTVD:STRING
p 4577 325 100 0 0 FTVE:STRING
p 4577 261 100 0 0 FTVF:STRING
p 4577 261 100 0 0 FTVG:STRING
p 4577 229 100 0 0 FTVH:STRING
p 4577 197 100 0 0 FTVI:STRING
p 4577 165 100 0 0 FTVJ:STRING
p 4577 165 100 0 0 FTVK:STRING
p 4577 165 100 0 0 FTVL:STRING
p 4577 165 100 0 0 FTVM:STRING
p 4577 165 100 0 0 FTVN:STRING
p 4577 165 100 0 0 FTVO:STRING
p 4577 165 100 0 0 FTVP:STRING
p 4577 165 100 0 0 FTVQ:STRING
p 4577 165 100 0 0 FTVR:STRING
p 4577 165 100 0 0 FTVS:STRING
p 4577 165 100 0 0 FTVT:STRING
p 4577 165 100 0 0 FTVU:STRING
p 4512 1054 100 0 0 INAM:ufacqControlGinit
p 4512 1022 100 0 0 SNAM:ufacqControlGproc
p 4912 640 100 1024 -1 name:$(dc)acqControlG
use flamDcConfig 2848 -121 100 0 flamDcConfig#1209
xform 0 2944 0
use ecad20 2816 135 100 0 ecad20#1170
xform 0 2976 1024
p 2912 1152 100 0 0 FTVO:LONG
p 2912 736 100 0 0 SNAM:dcAcqCommand
p 2928 128 100 1024 -1 name:$(dc)acqControl
p 2784 1568 75 1280 -1 pproc(INPB):NPP
p 3136 234 75 0 -1 pproc(STLK):PP
use efanouts 3344 -89 100 0 efanouts#863
xform 0 3464 64
p 3456 -96 100 1024 -1 name:$(dc)cmFanout
use flamLinkEnable 4160 -89 100 0 flamLinkEnable#861
xform 0 4320 32
p 4160 -96 100 0 1 setSystem:system $(dc)acq
use ecars 5520 1255 100 0 ecars#305
xform 0 5680 1424
p 5616 1472 100 0 0 DESC:Observation Status CAR
p 5632 1248 100 1024 -1 name:$(dc)observeC
use ecalcs 4704 7 100 0 ecalcs#303
xform 0 4848 272
p 4704 -32 100 0 1 CALC:A+1
p 4497 438 100 0 0 DESC:Heartbeat Simulator
p 4416 254 100 0 0 DISV:0
p 4704 -64 100 0 1 SCAN:1 second
p 4816 0 100 1024 -1 name:$(dc)HeartbeatCALC
[comments]
RCS: "$Name:  $ $Id: fdc.sch,v 1.1 2006/09/14 22:27:42 gemvx Exp $"
