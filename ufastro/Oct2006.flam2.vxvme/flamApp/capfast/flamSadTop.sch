[schematic2]
uniq 28
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos SAD Top Level
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/03
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamSadTop.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 YYYY/MM/DD
[cell use]
use flamSadMain 608 807 100 0 flamSadMain#27
xform 0 1024 1136
p 608 736 100 0 1 setCc:dc flam:cc:
p 608 704 100 0 1 setDc:dc flam:dc:
p 608 768 100 0 1 setTop:top flam:sad:
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadTop.sch,v 1.1 2006/09/14 22:28:01 gemvx Exp $
