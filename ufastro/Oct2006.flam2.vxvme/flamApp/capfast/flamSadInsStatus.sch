[schematic2]
uniq 63
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos Instrument Status
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/11
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamSadInsStatus.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2000/11/11
[cell use]
use esirs 528 -25 100 0 esirs#62
xform 0 736 128
p 592 -64 100 0 1 SCAN:Passive
p 688 256 100 1024 -1 name:$(top)DATUMCNT
use esirs -192 423 100 0 esirs#30
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)issPort
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)historyLog
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)heartbeat
use esirs -192 1671 100 0 esirs#27
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)health
use esirs 544 1671 100 0 esirs#32
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)state
use esirs 1280 1255 100 0 esirs#31
xform 0 1488 1408
p 1344 1216 100 0 1 SCAN:Passive
p 1440 1248 100 1024 -1 name:$(top)name
use esirs 544 423 100 0 esirs#46
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)Simulation
use esirs 544 1255 100 0 esirs#48
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)reconfigTimeout
use esirs 1280 423 100 0 esirs#49
xform 0 1488 576
p 1344 384 100 0 1 SCAN:Passive
p 1440 416 100 1024 -1 name:$(top)initialized
use esirs 544 839 100 0 esirs#53
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)datummed
use esirs 1280 1671 100 0 esirs#57
xform 0 1488 1824
p 1344 1632 100 0 1 SCAN:Passive
p 1440 1664 100 1024 -1 name:$(top)parked
use esirs 1280 839 100 0 esirs#61
xform 0 1488 992
p 1216 544 100 0 0 FTVL:LONG
p 1472 640 100 0 0 SNAM:ufSetDatumCnt
p 1392 832 100 1024 1 name:$(top)DatumCnt
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadInsStatus.sch,v 1.1 2006/09/14 22:28:00 gemvx Exp $
