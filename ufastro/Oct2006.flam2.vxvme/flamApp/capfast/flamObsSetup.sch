[schematic2]
uniq 282
[tools]
[detail]
w 898 779 100 0 n#273 ecad20.ecad20#227.PLNK 192 32 352 32 352 768 1504 768 egenSub.egenSub#130.SLNK
w 1938 1259 100 0 n#271 egenSub.egenSub#130.OUTD 1792 1248 2144 1248 hwout.hwout#272.outp
w 818 1259 100 0 n#264 ecad20.ecad20#227.VALD 192 1248 1504 1248 egenSub.egenSub#130.INPD
w 482 -245 100 0 n#260 ecad20.ecad20#227.VALR 192 352 288 352 288 -256 736 -256 736 -96 800 -96 flamIsEcUpdate.flamIsEcUpdate#194.ENABLE
w 818 1323 100 0 n#249 ecad20.ecad20#227.VALC 192 1312 1504 1312 egenSub.egenSub#130.INPC
w 818 1387 100 0 n#252 ecad20.ecad20#227.VALB 192 1376 1504 1376 egenSub.egenSub#130.INPB
w 818 1451 100 0 n#242 ecad20.ecad20#227.VALA 192 1440 1504 1440 egenSub.egenSub#130.INPA
w 274 11 100 0 n#255 ecad20.ecad20#227.STLK 192 0 416 0 flamSubSysCommand.flamSubSysCommand#57.START
w 1938 1323 100 0 n#212 egenSub.egenSub#130.OUTC 1792 1312 2144 1312 hwout.hwout#213.outp
w 1090 -21 100 0 n#202 flamIsEcUpdate.flamIsEcUpdate#194.FLNK 1088 -32 1152 -32 flamSubSysCommand.flamSubSysCommand#197.START
w 1106 203 100 0 n#201 flamSubSysCommand.flamSubSysCommand#57.CAR_IMSS 704 32 768 32 768 192 1504 192 1504 0 junction
w 1490 11 100 0 n#201 flamSubSysCommand.flamSubSysCommand#197.CAR_IMSS 1440 0 1600 0 ecars.ecars#53.IMSS
w 1106 235 100 0 n#200 flamSubSysCommand.flamSubSysCommand#57.CAR_IVAL 704 96 736 96 736 224 1536 224 1536 64 junction
w 1490 75 100 0 n#200 flamSubSysCommand.flamSubSysCommand#197.CAR_IVAL 1440 64 1600 64 ecars.ecars#53.IVAL
w 722 -21 100 0 n#191 flamSubSysCommand.flamSubSysCommand#57.DONE 704 -32 800 -32 flamIsEcUpdate.flamIsEcUpdate#194.START
w 1938 1387 100 0 n#129 egenSub.egenSub#130.OUTB 1792 1376 2144 1376 hwout.hwout#132.outp
w 1938 1451 100 0 n#128 egenSub.egenSub#130.OUTA 1792 1440 2144 1440 hwout.hwout#131.outp
w -112 1739 -100 0 c#89 ecad20.ecad20#227.DIR -128 1632 -192 1632 -192 1728 16 1728 16 1920 -96 1920 inhier.DIR.P
w -144 1771 -100 0 c#90 ecad20.ecad20#227.ICID -128 1600 -224 1600 -224 1760 -16 1760 -16 1856 -96 1856 inhier.ICID.P
w 136 1739 -100 0 c#91 ecad20.ecad20#227.VAL 192 1632 256 1632 256 1728 64 1728 64 1920 192 1920 outhier.VAL.p
w 168 1771 -100 0 c#92 ecad20.ecad20#227.MESS 192 1600 288 1600 288 1760 96 1760 96 1856 192 1856 outhier.MESS.p
s 288 1264 100 0 Bias Mode
s 288 1328 100 0 Readout mode
s 1904 1392 100 0 number of reads
s 1904 1456 100 0 exposure time
s 288 1392 100 0 number of reads
s 304 1456 100 0 exposure time
s -416 1312 100 0 Readout mode
s 2512 -240 100 1792 flamObsSetup.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2003/02/18
s 2320 -240 100 1792 Rev: H
s 2432 -192 100 256 Flamingos observationSetup Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s -416 1424 100 0 exposure time
s -416 1360 100 0 number of reads
s -416 1248 100 0 Bias Mode
s 1904 1264 100 0 Bias Mode
s 1904 1328 100 0 Readout mode
[cell use]
use hwout 2144 1207 100 0 hwout#272
xform 0 2240 1248
p 2368 1248 100 0 -1 val(outp):$(top)dc:obsControl.E
use hwout 2144 1271 100 0 hwout#213
xform 0 2240 1312
p 2368 1312 100 0 -1 val(outp):$(top)dc:obsControl.D
use hwout 2144 1335 100 0 hwout#132
xform 0 2240 1376
p 2368 1376 100 0 -1 val(outp):$(top)dc:obsControl.C
use hwout 2144 1399 100 0 hwout#131
xform 0 2240 1440
p 2368 1440 100 0 -1 val(outp):$(top)dc:obsControl.B
use ecad20 -128 -89 100 0 ecad20#227
xform 0 32 800
p -32 1376 100 0 0 FTVA:STRING
p -32 1344 100 0 0 FTVB:STRING
p -32 1312 100 0 0 FTVC:STRING
p -32 1280 100 0 0 FTVD:STRING
p -32 1248 100 0 0 FTVE:STRING
p -32 1184 100 0 0 FTVG:STRING
p -32 1152 100 0 0 FTVH:STRING
p -32 1120 100 0 0 FTVI:STRING
p -32 1088 100 0 0 FTVJ:STRING
p -32 1024 100 0 0 FTVL:STRING
p -32 928 100 0 0 FTVO:STRING
p -32 832 100 0 0 FTVR:STRING
p -64 -128 100 0 1 INAM:flamIsNullInit
p -64 -160 100 0 1 SNAM:flamIsObsSetupProcess
p -64 -96 100 0 1 name:$(top)observationSetup
use flamSubSysCommand 416 -185 100 0 flamSubSysCommand#57
xform 0 560 0
p 416 -224 100 0 1 setCommand:cmd obsSetup
p 416 -192 100 0 1 setSystem:sys dc
use flamSubSysCommand 1152 -217 100 0 flamSubSysCommand#197
xform 0 1296 -32
p 1152 -256 100 0 1 setCommand:cmd obsSetup
p 1152 -224 100 0 1 setSystem:sys ec
use flamIsEcUpdate 800 -217 100 0 flamIsEcUpdate#194
xform 0 944 -32
use egenSub 1504 679 100 0 egenSub#130
xform 0 1648 1104
p 1281 453 100 0 0 FTA:STRING
p 1281 453 100 0 0 FTB:STRING
p 1281 421 100 0 0 FTC:STRING
p 1281 389 100 0 0 FTD:STRING
p 1281 357 100 0 0 FTE:STRING
p 1281 293 100 0 0 FTF:STRING
p 1281 293 100 0 0 FTG:STRING
p 1281 261 100 0 0 FTH:STRING
p 1281 229 100 0 0 FTI:STRING
p 1281 197 100 0 0 FTJ:STRING
p 1281 453 100 0 0 FTVA:STRING
p 1281 453 100 0 0 FTVB:STRING
p 1281 421 100 0 0 FTVC:STRING
p 1281 389 100 0 0 FTVD:STRING
p 1281 357 100 0 0 FTVE:STRING
p 1281 293 100 0 0 FTVF:STRING
p 1281 293 100 0 0 FTVG:STRING
p 1281 261 100 0 0 FTVH:STRING
p 1281 229 100 0 0 FTVI:STRING
p 1281 197 100 0 0 FTVJ:STRING
p 1568 640 100 0 1 INAM:flamIsNullGInit
p 1568 608 100 0 1 SNAM:flamIsCopyGProcess
p 1568 672 100 768 1 name:$(top)obsSetupPreset2G
use outhier 208 1920 100 0 VAL
xform 0 176 1920
use outhier 208 1856 100 0 MESS
xform 0 176 1856
use inhier -160 1920 100 0 DIR
xform 0 -96 1920
use inhier -176 1856 100 0 ICID
xform 0 -96 1856
use ecars 1600 -217 100 0 ecars#53
xform 0 1760 -48
p 1760 -224 100 1024 1 name:$(top)observationSetupC
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamObsSetup.sch,v 1.1 2006/09/14 22:27:54 gemvx Exp $
