package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Infrared Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;
import TelescopeServer.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;

public class DataDisplayFrame extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: DataDisplayFrame.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    IndexColorModel colorModel;
    ZoomPanel zoomPanel;
    HistogramPanel histoPanel;
    ColorMapDialog colorMapDialog;
    TelescopePanel telescopeControl;
    UFobsMonitor obsMonitor;

    DataAccessPanel dataAccess;
    ImagePanel[] imagePanels;

    ButtonGroup countsModeBuGrp = new ButtonGroup();;

    final int NpanRow = 2, NpanCol = 2;
    final int X_SIZE = 1250, Y_SIZE = 940;

    public DataDisplayFrame(String dasHost, int dasPort, String tcsHost, String tcsPort, String args[])
    {
        this.setTitle("JDD:  Java  Data  Display  Tool  (version 2005/04/28)");
        System.out.println("Initializing JDD components... ");
	setupMenuBar();
	//	String[] defaultBuffers = {"dif1","dif2","sig","acc(sig)"};
	String[] defaultBuffers = {"dif1","acc(dif1)","src1","ref1"};

        colorMapDialog = new ColorMapDialog(this);
        colorModel = colorMapDialog.colorModel;
	histoPanel = new HistogramPanel( colorMapDialog, 30, 110 );

	int Npanels = NpanRow * NpanCol;
	imagePanels = new ImagePanel[Npanels]; //need this pointer first for dataDisplay

	//connect to Frame Data Acq. Server
	dataAccess = new DataAccessPanel( dasHost, dasPort, imagePanels );

        zoomPanel = new ZoomPanel( colorModel, histoPanel );

	JPanel imPanelsHolder = new JPanel(); //for scrollPane of imagePanels.
	int minW = NpanRow * dataAccess.frameConfig.width;
	int minH = NpanCol * dataAccess.frameConfig.height + 220;
	imPanelsHolder.setMinimumSize(new Dimension( minW, minH ));
	imPanelsHolder.setLayout(new GridLayout(NpanRow, NpanCol));

	for( int i=0; i < imagePanels.length; i++ ) {
	    imagePanels[i] = new ImagePanel( imagePanels, colorModel, dataAccess, zoomPanel );
	    imPanelsHolder.add( imagePanels[i] );
	    imagePanels[i].controlPanel.displayBuffer( defaultBuffers[i] );
	}

        JScrollPane scrollPane = new JScrollPane( imPanelsHolder );
        scrollPane.setBounds(0, 0, 650, 730);
        zoomPanel.setBounds( 652, 60, zoomPanel.xSize, zoomPanel.ySize );
        histoPanel.setBounds( 660 + zoomPanel.xSize, 60, histoPanel.xSize, histoPanel.ySize );

        dataAccess.connectPanel.setBounds( 680, zoomPanel.ySize + 70, 300, 650 - zoomPanel.ySize );
	dataAccess.pauseUpdateButton.setBounds( 660 + zoomPanel.xSize, zoomPanel.ySize, 150, 50 );

        JButton colorMapButton = new JButton("Color  Map  Control");
        JButton telescopeConButton = new JButton("Telescope  Offset  Control");
        telescopeConButton.setBounds(710, 10, 230, 40);
        colorMapButton.setBounds(1020, 10, 200, 40);

	JRadioButton countsPerFrmTime = new JRadioButton("Counts Per Frame Time");
	JRadioButton countsTotal = new JRadioButton("Total  Counts");
        countsModeBuGrp.add( countsPerFrmTime );
        countsModeBuGrp.add( countsTotal );
	JPanel radioButtons = new JPanel();
	radioButtons.setLayout(new GridLayout(2,1));
	radioButtons.add( countsPerFrmTime );
	radioButtons.add( countsTotal );
	radioButtons.setBounds(1020, zoomPanel.ySize + 70, 200, 650 - zoomPanel.ySize );

        dataAccess.setLayout(null);
        dataAccess.add( scrollPane );
        dataAccess.add( zoomPanel );
        dataAccess.add( telescopeConButton );
        dataAccess.add( colorMapButton );
        dataAccess.add( histoPanel );
        dataAccess.add( dataAccess.connectPanel );
        dataAccess.add( dataAccess.pauseUpdateButton );
        dataAccess.add( radioButtons );
        dataAccess.setBounds( 0, 0, X_SIZE-14, 730 );

	//connect to Frame Data Acq. Server notification monitor and hook to dataAccess panel
	// for automatic update of image panels with frame buffer updates:

        obsMonitor = new UFobsMonitor( dasHost, dasPort, dataAccess );
        obsMonitor.setBounds( 0, 733, X_SIZE-14, Y_SIZE-733-62 );

        this.getContentPane().setLayout(null);
        this.getContentPane().add( dataAccess );
        this.getContentPane().add( obsMonitor );

        this.setLocation( 20, 15 );
        this.setSize( X_SIZE, Y_SIZE );
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        RadioListener radioListener = new RadioListener();
        countsTotal.addActionListener(radioListener);
        countsPerFrmTime.addActionListener(radioListener);
        countsModeBuGrp.setSelected( countsPerFrmTime.getModel(), true );

	colorMapButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                colorMapDialog.show();
		colorMapDialog.setState( Frame.NORMAL );
            }
        });

        telescopeControl = new TelescopePanel(tcsHost, tcsPort, args);

        telescopeConButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                telescopeControl.show();
		telescopeControl.setState( Frame.NORMAL );
            }
        });
    }
//-------------------------------------------------------------------------------

    private void setupMenuBar() {
	JMenuBar menuBar = new JMenuBar();
	JMenu menuFile = new JMenu("File");
	JMenuItem menuFileExit = new JMenuItem("Exit");
        menuFile.add( menuFileExit );
	JMenu menuHelp = new JMenu("Help");
	JMenuItem menuHelpAbout = new JMenuItem("About");
        menuHelp.add( menuHelpAbout );
        
        menuFileExit.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    System.out.println("JDD exiting...");
		    System.exit(0);
		}
	    });
        
        JMenu lnfMenu = new JMenu("Look & Feel");
	String[] LnF = {"Motif Look","Metal Look"};

        for( int i=0; i < LnF.length; i++ ) {
            JMenuItem menuItem = new JMenuItem(LnF[i]);
            lnfMenu.add(menuItem);
            menuItem.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { change_look(e); }
		});
        }
        
        menuHelpAbout.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { helpAbout_action(e); }
	    });

        menuBar.add(menuFile);
        menuBar.add(lnfMenu);
        menuBar.add(menuHelp);
        this.setJMenuBar( menuBar );
    }

    public void helpAbout_action(ActionEvent e) {
        // Do nothing right now
        // To be added later
    }
    
    public void changeColorModel(IndexColorModel colorModel) {
        //System.out.println("Color Model updating...");
        this.colorModel = colorModel;
        zoomPanel.zoomImage.updateColorMap( colorModel );
        histoPanel.colorBar.updateColorMap( colorModel );
        for( int i=0; i < imagePanels.length; i++ ) imagePanels[i].imageDisplay.updateImage( colorModel );
    }

    void change_look(ActionEvent e) {
        if (e.getActionCommand()=="Motif Look")
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
        
        if (e.getActionCommand()=="Metal Look")
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
    }
//------------------------------------------------------------------------------------------------------------

    class RadioListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
           
            String countsMode = (String)((JRadioButton)getSelection( countsModeBuGrp )).getText();

	    if( countsMode.toUpperCase().indexOf("TOTAL") >= 0 ) {
		zoomPanel.zoomImage.useScaleFactor( false );
		zoomPanel.zoomImage.showPixelValue();
		zoomPanel.adjustZoom.useScaleFactor( false );
		zoomPanel.adjustZoom.updateMinMaxVal();
		for( int i=0; i < imagePanels.length; i++ ) {
		    imagePanels[i].adjustPanel.useScaleFactor( false );
		    imagePanels[i].adjustPanel.updateMinMaxVal();
		}
	    }
	    else {
		zoomPanel.zoomImage.useScaleFactor( true );
		zoomPanel.zoomImage.showPixelValue();
		zoomPanel.adjustZoom.useScaleFactor( true );
		zoomPanel.adjustZoom.updateMinMaxVal();
		for( int i=0; i < imagePanels.length; i++ ) {
		    imagePanels[i].adjustPanel.useScaleFactor( true );
		    imagePanels[i].adjustPanel.updateMinMaxVal();
		}
	    }

	}

	JRadioButton getSelection(ButtonGroup group) {
	    for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
		JRadioButton b = (JRadioButton)e.nextElement();
		if( b.getModel() == group.getSelection() )
		    return b;
	    }
	    return null;
	}
    }
}
