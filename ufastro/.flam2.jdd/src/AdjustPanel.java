package ufjdd;
/**
 * Title:       AdjustPanel.java
 * Version:     (see rcsID)
 * Authors:     Ziad Saleh and Frank Varosi
 * Created:     March 19, 2004, 12:49 AM
 * Company:     University of Florida
 * Description: Object for adjusting the display scaling of image in an ImagePanel object.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import javaUFLib.*;

public class AdjustPanel extends JPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: AdjustPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    private ImageDisplayPanel imgdp;
    protected String className = this.getClass().getName();
    int CoAdds = 1;
    double scaleFactor = 1.0;
    boolean useScaleFactor = true;
    float fmin, fmax;
    int imin, imax;
    String scaleMode = "Linear";
    JButton origMinButton;
    JButton origMaxButton;
    public JLabel minValLabel;
    public JLabel maxValLabel;
    JButton newMinButton;
    JButton newMaxButton;
    JTextField newMinTextField;
    JTextField newMaxTextField;
    ButtonModel buLinModel;
    JTextField thresholdTextField, powerTextField;
    ButtonGroup scalingModeBuGrp;
    
    public AdjustPanel(ImageDisplayPanel imgdp)
    {
        this.imgdp = imgdp;
        
        origMinButton = new JButton("Min *");
        origMinButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        origMaxButton = new JButton("Max *");
        origMaxButton.setAlignmentX(JButton.LEFT_ALIGNMENT);
        minValLabel = new JLabel("", JLabel.LEFT);
        minValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        maxValLabel = new JLabel("", JLabel.LEFT);
        maxValLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        newMinTextField = new JTextField();
        newMaxTextField = new JTextField();
        minValLabel.setText("" + 0);
        maxValLabel.setText("" + 0);
        
        newMinButton = new JButton("nMin");
        newMaxButton = new JButton("nMax");
        
        thresholdTextField = new JTextField("0.1");
        thresholdTextField.setEditable(false);
        powerTextField = new JTextField("0.33");
        powerTextField.setEditable(false);
        
        
        JRadioButton LinScaleModeButton = new JRadioButton("Linear");
	JRadioButton LogScaleModeButton = new JRadioButton("Log");
	JRadioButton PowScaleModeButton = new JRadioButton("Power");

        scalingModeBuGrp = new ButtonGroup();
        scalingModeBuGrp.add(LinScaleModeButton);
        scalingModeBuGrp.add(LogScaleModeButton);
        scalingModeBuGrp.add(PowScaleModeButton);

        RadioListener scalingModeListener = new RadioListener();
        LinScaleModeButton.addActionListener(scalingModeListener);
        LogScaleModeButton.addActionListener(scalingModeListener);
        PowScaleModeButton.addActionListener(scalingModeListener);
        
        buLinModel = LinScaleModeButton.getModel();
        scalingModeBuGrp.setSelected(buLinModel, true);
        
        origMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMinButton.setText("Min *");
                newMinButton.setText("nMin");
                applySetting();
            }
        });
        
        origMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                origMaxButton.setText("Max *");
                newMaxButton.setText("nMax");
                applySetting();
            }
        });
        
        newMinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if( newMinTextField.getText().trim().length() > 0 ) {
                    origMinButton.setText("Min");
                    newMinButton.setText("nMin *");
                    applySetting();
                }
            }
        });
        
        newMaxButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if( newMaxTextField.getText().trim().length() > 0 ) {
                    origMaxButton.setText("Max");
                    newMaxButton.setText("nMax *");
                    applySetting();
                }
            }
        });
        
        newMinTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if( c == KeyEvent.VK_ENTER ) {
		    if( newMinTextField.getText().trim().length() > 0 ) {
			origMinButton.setText("Min");
			newMinButton.setText("nMin *");
			applySetting();
		    }
		}
		else newMinTextField.setBackground( Color.white );
            }
        });
        
        newMaxTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if( c == KeyEvent.VK_ENTER ) {
		    if( newMaxTextField.getText().trim().length() > 0 ) {
			origMaxButton.setText("Max");
			newMaxButton.setText("nMax *");
			applySetting();
		    }
                }
		else newMaxTextField.setBackground( Color.white );
            }
        });
        
        thresholdTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                if( e.getKeyChar() == KeyEvent.VK_ENTER ) {
                    applySetting();
                }
		else thresholdTextField.setBackground( Color.white );
            }
        });
        
        powerTextField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                 if( e.getKeyChar() == KeyEvent.VK_ENTER ) {
                    applySetting();
                }
		else powerTextField.setBackground( Color.white );
            }
        });
        
        this.setLayout(new RatioLayout());
        this.add("0.00, 0.0; 0.25, 0.32", origMinButton);
        this.add("0.25, 0.0; 0.25, 0.32", minValLabel);
        this.add("0.50, 0.0; 0.25, 0.32", origMaxButton);
        this.add("0.75, 0.0; 0.25, 0.32", maxValLabel);
        
        this.add("0.00, 0.33; 0.25, 0.32", newMinButton);
        this.add("0.25, 0.33; 0.25, 0.32", newMinTextField);
        this.add("0.50, 0.33; 0.25, 0.32", newMaxButton);
        this.add("0.75, 0.33; 0.25, 0.32", newMaxTextField);
        
        this.add("0.00, 0.67; 0.25, 0.32", LinScaleModeButton);
        this.add("0.25, 0.67; 0.20, 0.32", LogScaleModeButton);
        this.add("0.45, 0.67; 0.05, 0.32", new JLabel(">"));
        this.add("0.50, 0.67; 0.10, 0.32", thresholdTextField);
        this.add("0.62, 0.67; 0.24, 0.32", PowScaleModeButton);
        this.add("0.86, 0.67; 0.04, 0.32", new JLabel("="));
        this.add("0.90, 0.67; 0.10, 0.32", powerTextField);
        
        setBorder(BorderFactory.createTitledBorder(""));
    }
    
    public void updateMinMaxVal(ImageBuffer imgBuffer) {
        this.CoAdds = imgBuffer.CoAdds;
        this.scaleFactor = imgBuffer.scaleFactor;
	if( CoAdds <= 0 ) CoAdds = 1;
	if( scaleFactor <= 0 ) scaleFactor = 1;
	this.imin = imgBuffer.min;
	this.imax = imgBuffer.max;
	this.fmin = imgBuffer.s_min;
	this.fmax = imgBuffer.s_max;
	updateMinMaxVal();
    }

    public void updateMinMaxVal()
    {
	if( useScaleFactor ) {
	    minValLabel.setText( UFLabel.truncFormat( fmin * scaleFactor ) );
	    maxValLabel.setText( UFLabel.truncFormat( fmax * scaleFactor ) );
	}
	else {
	    if( className.indexOf("Zoom") > 0 ) {
		minValLabel.setText( UFLabel.truncFormat( fmin ) );
		maxValLabel.setText( UFLabel.truncFormat( fmax ) );
	    }
	    else {
		minValLabel.setText( Integer.toString( imin ) );
		maxValLabel.setText( Integer.toString( imax ) );
	    }
	}
    }

    void useScaleFactor( boolean useit ) { useScaleFactor = useit; }

    public void reset() {        
        newMinTextField.setText("");
        newMaxTextField.setText("");
        origMinButton.setText("Min *");
        origMaxButton.setText("Max *");
        newMinButton.setText("nMin");
        newMaxButton.setText("nMax");
        scalingModeBuGrp.setSelected(buLinModel, true);
        thresholdTextField.setEditable(false);
        thresholdTextField.setText("1");
    }
    
    public void applySetting( String scaleMode )
    {
	this.scaleMode = scaleMode;
	applySetting();
    }

    public void applySetting()
    {
        float temp_min = 0, temp_max = 0;
        
        if( scaleMode.equalsIgnoreCase("Linear") ) {

            if( origMinButton.getText().indexOf("*") > 0 && origMaxButton.getText().indexOf("*") > 0 ) {
                reDrawImage();
            }
            else {
		temp_min = Float.parseFloat( minValLabel.getText().trim() );
		temp_max = Float.parseFloat( maxValLabel.getText().trim() );

		if( newMinButton.getText().indexOf("*") > 0 ) {
		    if( newMinTextField.getText().trim().length() == 0 ) {
			origMinButton.setText("Min *");
			newMinButton.setText("nMin");
		    }
		    else {
			try{ temp_min = Float.parseFloat( newMinTextField.getText().trim() ); }
			catch(NumberFormatException nfe) {
			    origMinButton.setText("Min *");
			    newMinButton.setText("nMin");
			    newMinTextField.setBackground( Color.yellow );
			    Toolkit.getDefaultToolkit().beep();
			}
		    }
                }

		if( newMaxButton.getText().indexOf("*") > 0 ) {
		    if( newMaxTextField.getText().trim().length() == 0 ) {
			origMaxButton.setText("Max *");
			newMinButton.setText("nMax");
		    }
		    else {
			try{ temp_max = Float.parseFloat( newMaxTextField.getText().trim() ); }
			catch(NumberFormatException nfe) {
			    origMaxButton.setText("Max *");
			    newMaxButton.setText("nMax");
			    newMaxTextField.setBackground( Color.yellow );
			    Toolkit.getDefaultToolkit().beep();
			}
		    }
		}

                reDrawImage( temp_min, temp_max );
            }
        }
        else if( scaleMode.equalsIgnoreCase("Power") ) {
	    try {
		double threshHold = Double.parseDouble( thresholdTextField.getText() );
		try {
		    double power = Double.parseDouble( powerTextField.getText() );
		    reDrawImage( threshHold, power );
		}
		catch(NumberFormatException nfe) {
		    powerTextField.setBackground( Color.yellow );
		    Toolkit.getDefaultToolkit().beep();
		}
	    }
	    catch(NumberFormatException nfe) {
		thresholdTextField.setBackground( Color.yellow );
		Toolkit.getDefaultToolkit().beep();
	    }
	}
        else {
	    try {
		double threshHold = Double.parseDouble( thresholdTextField.getText() );
		reDrawImage( threshHold );
	    }
	    catch(NumberFormatException nfe) {
		thresholdTextField.setBackground( Color.yellow );
		Toolkit.getDefaultToolkit().beep();
	    }
        }
    }

    protected void reDrawImage() { imgdp.drawOriginalImage(); }

    protected void reDrawImage(float min, float max)
    {
	if( useScaleFactor )
	    imgdp.updateImage( (int)Math.round( min * CoAdds ), (int)Math.round( max * CoAdds ) );
	else
	    imgdp.updateImage( (int)Math.round( min ), (int)Math.round( max ) );
    }

    protected void reDrawImage(double threshHold)
    {
	if( useScaleFactor )
	    imgdp.applyLogScale( threshHold * CoAdds );
	else
	    imgdp.applyLogScale( threshHold );
    }

    protected void reDrawImage(double threshHold, double power)
    {
	if( useScaleFactor )
	    imgdp.applyPowerScale( threshHold * CoAdds, power );
	else
	    imgdp.applyPowerScale( threshHold, power );
    }
//------------------------------------------------------------------------------------------------------------

    class RadioListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e) {
           
            String scaleMode = (String)((JRadioButton)getSelection( scalingModeBuGrp )).getText();

            if( scaleMode.equalsIgnoreCase("Linear") ) {
                thresholdTextField.setEditable(false);
                powerTextField.setEditable(false);
                newMinTextField.setEditable(true);
                newMaxTextField.setEditable(true);
                newMinButton.setEnabled(true);
                newMaxButton.setEnabled(true);
            }
	    else if( scaleMode.equalsIgnoreCase("Power") ) {
                thresholdTextField.setEditable(true);
                powerTextField.setEditable(true);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
                newMinButton.setEnabled(false);
                newMaxButton.setEnabled(false);
	    }
            else {
                thresholdTextField.setEditable(true);
                powerTextField.setEditable(false);
                newMinTextField.setEditable(false);
                newMaxTextField.setEditable(false);
                newMinButton.setEnabled(false);
                newMaxButton.setEnabled(false);
            }
            
            applySetting( scaleMode );
        }

	JRadioButton getSelection(ButtonGroup group) {
	    for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
		JRadioButton b = (JRadioButton)e.nextElement();
		if( b.getModel() == group.getSelection() )
		    return b;
	    }
	    return null;
	}
    }
}


