package ufjdd;
/**
 * Title:        ImageBuffer.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh and Frank Varosi
 * Company:      University of Florida
 * Description:  Object for storing frameConfig and frameInts objects from Data Acq. Server.
 */
import javaUFProtocol.*;

public class ImageBuffer {
    
    public static final
	String rcsID = "$Name:  $ $Id: ImageBuffer.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    UFFrameConfig frameConfig;
    String name;
    public int width;
    public int height;
    public int min =  2147483647;
    public int max = -2147483648;
    public int CoAdds = 1;
    public double scaleFactor = 1.0;
    public float s_min=0;
    public float s_max=0;
    public int pixels[];
    public int zoomFactor = 1;
    public int image[][];

    public ImageBuffer( UFFrameConfig frameConfig, UFInts frameData )
    {
	frameData.flipFrame( frameConfig ); //to accomodate Java coordinate sys of (Left,Top) = (0,0)
        this.frameConfig = frameConfig;
        this.width = frameConfig.width;
        this.height = frameConfig.height;
	frameData.calcMinMax();
        this.min = frameData.minVal();
        this.max = frameData.maxVal();
	this.CoAdds = frameConfig.chopCoadds * frameConfig.frameCoadds * frameConfig.coAdds;
        this.scaleFactor = 1.0/(double)CoAdds;
        s_min = min;
        s_max = max;
        name = frameData.name();
	if( name.indexOf("||") > 0 ) name = name.substring( 0, name.indexOf("||") );
        pixels = frameData.values();
    }

    public ImageBuffer( ImageBuffer imgBuffer, int[][] image, int zoomFactor )
    {
        this.image = image; //for holding zoom pixels.
	this.zoomFactor = zoomFactor;
        this.frameConfig = imgBuffer.frameConfig;
        this.name = imgBuffer.name;
        this.width = imgBuffer.width;
        this.height = imgBuffer.height;
	this.CoAdds = imgBuffer.CoAdds;
	this.scaleFactor = imgBuffer.scaleFactor;

	for( int i=0; i < image.length; i++ ) {
	    for( int j=0; j < image[i].length; j++ ) {
		int data = image[i][j];
		if( data < min ) min = data;
		if( data > max ) max = data;
	    }
	}

        s_min = min;
        s_max = max;
    }
}



