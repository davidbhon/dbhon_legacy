/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        ***
 * (c) <2005>                       (c) <2005>
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                    
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * recDeviceControl.c
 *
 * PURPOSE:
 * EPICS Record Support code for the deviceControl record.
 *
 * FUNCTION NAME(S)
 * abortingState        - Abort any motion and return to idle state.
 * alarm                - Raise an alarm on the VAL or MPOS fields.
 * calculateTimeout     - Predict the motion time (to be used for timeout).
 * checkVals            - Convert the string VALS field to a position.
 * depoweringState      - Turn off motor power.
 * getAlarmDouble       - Fill alarm double structure.
 * getControlDouble     - Fill control double structure.
 * getGraphicsDouble    - Fill graphics double structure.
 * getPrecision         - Return display precision.
 * getUnits             - Return engineering units for display.
 * getValue             - Return contents of the value (VAL) field.
 * holdingState         - Keep motor powered waiting for new tracking position.
 * idleState            - Wait for new motion request.
 * initLinks            - Initialize EPICS input and output database links.
 * initRecord           - Initialize an instance of the deviceControl record.
 * initState            - Re-initialize operating parameters.
 * lockingState         - Apply brake.
 * monitor              - Raise monitors on changed fields.
 * movingState          - Monitor motion in progress.
 * poweringState        - Turn on motor power.
 * process              - Main record processing function.
 * processDirective     - Process the directive field.
 * processState         - Process the current operating state.
 * readInputLinks       - Read values from EPICS input links.
 * special              - Perform special pre/post processing on selected fields.
 * startingState        - Configure and start a motion.
 * stoppingState        - Bring the motor to a controlled stop.
 * unlockingState       - Release the brake.
 * writeOutputLinks     - Write values on the EPICS output links.
 *
 *INDENT-OFF*
 * $Log: recDeviceControl.c,v $
 * Revision 0.1  2005/07/01 17:47:14  drashkin
 * *** empty log message ***
 *
 * Revision 1.6  2005/06/09  bmw
 * Fixed encoder jump check.
 *
 * Revision 1.5  2005/05/19  bmw
 * Added revision info.
 *
 * Revision 1.4  2005/04/26  bmw
 * Removed sections in calculateTimeout() that were commented out.
 * Removed sections in initLinks() that were commented out.
 * Removed line in initState() that was commented out.
 * Removed sections in startingState() that were commented out.
 *
 * Revision 1.3  2004/11/02  bmw
 * Added comments, added home position offset.
 *
 * Revision 1.2  2004/09/20  bmw
 * Initial F2 revision, copy of Gemini GMOS deviceControlRecord.c rev 1.1
 *
 * Revision 1.1  2002/04/24 05:18:14  ajf
 * Changes for epics3.13.4GEM8.4.
 *
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include    <stdioLib.h>
#include    <stdlib.h>
#include    <string.h>
#include    <math.h>

#include    <vxWorks.h>
#include    <logLib.h>
#include    <tickLib.h>
#include    <ellLib.h>
#include    <taskLib.h>

#include    <dbDefs.h>
#include    <dbAccess.h>
#include    <dbFldTypes.h>
#include    <dbEvent.h>
#include    <devSup.h>
#include    <recSup.h>

#include    <drvOmsVme58.h>           /* index velocity constants */
#include    <devDcOms58.h>            /* access to device control DSET functions */
#include    <deviceControl.h>         /* access to device ctrl private structure */
#define GEN_SIZE_OFFSET
#include    <deviceControlRecord.h>   /* auto-created from *.dbd */
#undef GEN_SIZE_OFFSET

#include    <ddrMessageLevels.h>      /* Device record message level definitions. */


/*
 *  Local Defines
 */

#define VERSION 3.0                   /* Code version number                */

#define DDR_INIT            0         /* initializing                       */
#define DDR_IDLE            1         /* idle, awaiting commmand            */
#define DDR_POWERING        2         /* applying power                     */
#define DDR_UNLOCKING       3         /* removing brake                     */
#define DDR_STARTING        4         /* starting motion                    */
#define DDR_MOVING          5         /* moving happily                     */
#define DDR_STOPPING        6         /* stopping motion                    */
#define DDR_HOLDING         7         /* holding position with power on     */
#define DDR_ENGAGING        8         /* engaging brake                     */
#define DDR_DEPOWERING      9         /* removing power                     */
#define DDR_FAILING         10        /* aborting motion                    */

#define DDR_ACKNOWLEDGE_TIME      5   /* BUSY time for acknowledgement 0.1s */
#define DDR_START_TIMEOUT        25   /* max time for motion to start  0.1s */
#define DDR_INIT_HOLDOFF         10   /* record initialization holdoff 0.1s */
#define DDR_LUT_MAX_NAME         16   /* maximum length of name string      */
#define DDR_MOTION_OVERHEAD_TIME 25   /* motion setup overhead, x0.1 sec    */
#define DDR_INDEX_OVERHEAD_TIME  25   /* indexing overhead, x0.1 sec        */

/*
 * Bitmap mnemonics for EPICS record filds to have monitors raised
 * or output links processed
 */

#define RECORD_DIR        0x00000001        /*  Directive field             */
#define RECORD_MODE       0x00000002        /*  Operating mode field        */
#define RECORD_VELO       0x00000004        /*  Slew velocity field         */
#define RECORD_ACCL       0x00000008        /*  Acceleration rate field     */
#define RECORD_IALG       0x00000010        /*  Index algorithm field       */
#define RECORD_BSTA       0x00000020        /*  Use brake status bit field  */
#define RECORD_PSTA       0x00000040        /*  Use power status bit field  */
#define RECORD_FLT        0x00000080        /*  Fault (interlock) field     */
#define RECORD_DBUG       0x00000100        /*  Debugging level field       */
#define RECORD_SIMM       0x00000200        /*  Simulation level field      */
#define RECORD_MDBD       0x00000400        /*  Motor deadband field        */
#define RECORD_ACK        0x00000800        /*  Command acknowledge field   */
#define RECORD_BUSY       0x00001000        /*  Action state field          */
#define RECORD_MESS       0x00002000        /*  Error message field         */
#define RECORD_PWR        0x00004000        /*  Power control bit field     */
#define RECORD_BRK        0x00008000        /*  Brake control bit field     */
#define RECORD_MIP        0x00010000        /*  Motion in progress field    */
#define RECORD_MPOS       0x00020000        /*  Motor position field        */
#define RECORD_RPOS       0x00040000        /*  Raw motor position field    */
#define RECORD_RRBV       0x00080000        /*  Raw readback value field    */
#define RECORD_RVEL       0x00100000        /*  Raw velocity field          */
#define RECORD_RENC       0x00200000        /*  Raw encoder field           */
#define RECORD_MSTA       0x00400000        /*  Motor status field          */
#define RECORD_OSTA       0x00800000        /*  Operating state field       */
#define RECORD_LVIO       0x01000000        /*  Limit violation field       */
#define RECORD_LSWA       0x02000000        /*  Limit switch active field   */
#define RECORD_HPVL       0x04000000        /*  Home position valid field   */
#define RECORD_HSWA       0x08000000        /*  Home switch active field    */
#define RECORD_VALS       0x10000000        /*  Named position input field  */


/*
*************************************************************************
 * Macros that allow the record and device support code to
 * control which fields have monitors raised or output links processed
 * whenever a record processes.
 *
 * MONITOR(RECORD_XXX) will cause the record support code to raise
 * a monitor on the given field when the record next processes.
 * 
 * TRIGGER(M_XXX) will cause the record support code to trigger the
 * output link for the given field when the record processes.
 *
 *    --- NOTE ---
 *    The macros below assume that the variable "pdr" exists and points
 *    to a deviceControl record, like so:
 *        DEVICE_CONTROL_RECORD *pdr;
 *    No check is made in this code to ensure that this really is true.
 *************************************************************************
 */

#define MONITOR(a)          pdr->mmap |= (a);   /* set monitor bit          */
#define MONITORED(a)        pdr->mmap  & (a)    /* is monitor bit set?      */
#define UNMONITOR(a)        pdr->mmap &= ~(a);  /* clear monitor bit        */
#define UNMONITOR_ALL       pdr->mmap = 0;      /* clear all monitor bits   */

#define TRIGGER(a)          pdr->lmap |= (a);   /* set write link bit       */
#define TRIGGERED(a)        pdr->lmap  & (a)    /* is write link bit set?   */
#define UNTRIGGER(a)        pdr->lmap &= ~(a);  /* clear write link bit     */
#define UNTRIGGER_ALL       pdr->lmap = 0;      /* clear all write link bits*/


/*
 *  Definitions for EPICS support functions not implemented for this record
 */

#define report              NULL      /* no function supplied               */
#define initialize          NULL      /* no function supplied               */
#define cvtDbaddr           NULL      /* no function supplied               */
#define getArrayInfo        NULL      /* no function supplied               */
#define putArrayInfo        NULL      /* no function supplied               */
#define getEnumStr          NULL      /* no function supplied               */
#define getEnumStrs         NULL      /* no function supplied               */
#define putEnumStr          NULL      /* no function supplied               */


/*
 *  Prototypes for EPICS support functions implemented for this record
 */

static void     alarm(DEVICE_CONTROL_RECORD *);
static long     checkVals(DEVICE_CONTROL_RECORD *, double *, long *);
static long     getAlarmDouble(struct dbAddr *, struct dbr_alDouble *);
static long     getControlDouble(struct dbAddr *, struct dbr_ctrlDouble *);
static long     getGraphicDouble(struct dbAddr *, struct dbr_grDouble *);
static long     getPrecision(struct dbAddr *, long *);
static long     getUnits(struct dbAddr *, char *);
static long     getValue(DEVICE_CONTROL_RECORD *, struct valueDes *);
static long     initLinks(DEVICE_CONTROL_RECORD *);
static long     initRecord(DEVICE_CONTROL_RECORD *, int);
static void     monitor(DEVICE_CONTROL_RECORD *);
static long     process(DEVICE_CONTROL_RECORD *);
static long     readInputLinks(DEVICE_CONTROL_RECORD *, int );
static long     special(struct dbAddr *, int);
static long     writeOutputLinks (DEVICE_CONTROL_RECORD *);


/* 
 *  Prototypes for local internal processing functions
 */

static long     calculateTimeout (double, double, double, double);
static long     processDirective (DEVICE_CONTROL_RECORD *);
static long     processState (DEVICE_CONTROL_RECORD *);


/* 
 *  Prototypes for local state machine state functions
 */

static long     abortingState (DEVICE_CONTROL_RECORD *);
static long     depoweringState (DEVICE_CONTROL_RECORD *);
static long     holdingState (DEVICE_CONTROL_RECORD *);
static long     idleState (DEVICE_CONTROL_RECORD *);
static long     initState (DEVICE_CONTROL_RECORD *);
static long     lockingState (DEVICE_CONTROL_RECORD *);
static long     movingState (DEVICE_CONTROL_RECORD *);
static long     poweringState (DEVICE_CONTROL_RECORD *);
static long     startingState (DEVICE_CONTROL_RECORD *);
static long     stoppingState (DEVICE_CONTROL_RECORD *);
static long     unlockingState (DEVICE_CONTROL_RECORD *);


/*
 *  Create record interface function access structure and initialize
 *  all function addresses.
 */

struct rset deviceControlRSET = {
    RSETNUMBER,
    report,
    initialize,
    initRecord,
    process,
    special,
    getValue,
    cvtDbaddr,
    getArrayInfo,
    putArrayInfo,
    getUnits,
    getPrecision,
    getEnumStr,
    getEnumStrs,
    putEnumStr,
    getGraphicDouble,
    getControlDouble,
    getAlarmDouble
};


/*
 *  Define the translation lookup table linked list node structure.
 */

typedef struct {
    ELLNODE     node;                       /* pointer to next node */
    char        name[DDR_LUT_MAX_NAME + 1]; /* position name */
    double      target;                     /* position target*/
    long        index;                      /* index algorithm */
    } TRANSLATION_NODE;

   
/*
 *  Define a macro to print debugging information to the VxWorks logging
 *  system.  If the current debugging level set by the DBUG field is
 *  greater than or equal to the debugging threshold given to the macro then 
 *  the given information message string is sent to the logging task.
 *
 *  The string consists of the system tick counter followd by the
 *  name of the record and then a formatted string containing one
 *  integer variable.   For example:
 *
 *  DEBUG(DDR_MSG_MAX,
 *        "<%ld> %s:movingState: encoder check...deadband:%d\n", pdr->edbd);
 *  
 *  Would result in the following log message if debugging is set to MAX:
 *
 *  <312456> gm:cc:mskExtDevice:movingState: encoder check...deadband:10
 */

#define DEBUG(l,FMT,V) if (l <= pdr->dbug)                     \
                            printf  ("%s: "FMT,                \
                                    taskName(0),               \
                                    tickGet(),                 \
                                    pdr->name,                 \
                                    V);

/*
 *  Define a macro to save the first (root) command failure error message 
 *  and prevent it from being overwritten by subsequent error messages
 *  (a result of the root failure).   The message will remain until
 *  cleared by starting a new command.
 */

#define SET_ERR_MSG(MSG)                                         \
{                                                                \
    if (!strlen (pPriv->actionErrMess))                          \
        strncpy (pPriv->actionErrMess, MSG, MAX_STRING_SIZE - 1);\
}

/*
 *  Macro for writing to the MESS field.  Will overwrite
 *  any previous value.
 */

#define SET_MESS(MSG)                                            \
{                                                                \
    strncpy (pdr->mess, MSG, MAX_STRING_SIZE - 1);               \
}

/*
 *  Macro to save command rejection error message.  Will overwrite
 *  any previous value.
 */

#define SET_REJ_MSG(MSG)                                         \
{                                                                \
    strncpy (pPriv->rejectErrMess, MSG, MAX_STRING_SIZE - 1);    \
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * abortingState
 *
 * INVOCATION:
 * status = abortingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)    deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) Record processing success code
 *
 * PURPOSE:
 * Immediately abort any motion in progress and return to the idle state
 *
 * DESCRIPTION:
 * This function is called whenever a serious error or fault is detected 
 * in another state and every time the record processes until we leave this 
 * state for the idle state.   It will immediately abort any command in 
 * progress via the following algorithm:
 *
 *      If we were called from another state (entering aborting state):
 *      {
 *          Switch state machine into aborting state.
 *          Stop all device motion immediately.
 *          If controlling brake:
 *          {
 *              set the brake control bit to energize the brake.
 *          }
 *          If controlling power:
 *          {
 *               clear power control bit and call device support to kill power.
 *          }
 *          Indicate that the motion failed and we lost our home reference.
 *
 *          If controlling brake or power:
 *          {
 *               Request a callback to check brake/power state.
 *          }
 *
 *          Otherwise consider motion aborted and exit to the idle state.
 *      }
 *
 *      If the status bits say the power is off and brake is on:
 *      {
 *          Exit to the idle state.
 *      }
 *
 *      If this is the callback requested above:
 *      {
 *          If power is not off then try to turn it off again once.
 *          If brake is not on then generate an error message.
 *          Exit to the idle state.
 *      }
 *
 *      Remain in aborting state until the record is processed again.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 *  bmw - checking power status is not really valid
 *  because it may be a case where a kill switch or hard limit
 *  has been set, then unset later.  A pass through here may
 *  indicate the power has gone off, but if the aux bit is still
 *  set, then when the cause for the abort is removed (as in
 *  kill switch released), the power will come back on.
 *-
 ************************************************************************
 */

static long abortingState
(
    DEVICE_CONTROL_RECORD    *pdr   /* (in)  Ptr. to device rec.            */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
        *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
        *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long delay = 0;                 /* delay time in 0.1s units             */
    long status = 0;                /* function status return               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s: abortingState:entry%c\n", ' ');


    /*
     *  Check the current operating state.  If we were in some other
     *  state then change the operating state to FAILING and try to abort
     *  the action in progress.   
     */
    
    if ( pdr->osta != DDR_FAILING )
    {
        DEBUG(DDR_MSG_FULL,
                 "<%ld> %s: abortingState: from osta:%d\n", pdr->osta);
        pdr->osta = DDR_FAILING;

        /*
         *  Abort any motion in progress by sending an abort command
         *  to the device driver.  Cancel the current motion timeout.
         */
                
        (*pdset->setDelay) (pPriv, 0);  
        (*pdset->controlMotion) (pPriv, DDR_MOVE_ABORT);


        /*
         *  If we are controlling a motor brake (brake timeout is set)
         *  then apply the brake by setting the brake control bit and 
         *  requesting a write to the brake control output link.
         *  Load the callback timer delay to allow us to 
         *  check to confirm that the brake was applied.
         */
 
        if( pdr->btmo )
        {         
            pdr->brk = TRUE;
            MONITOR(RECORD_BRK);
     
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_BRK);       
            }

            delay = pdr->btmo;
        }

        /*
         *  Are we are controlling the motor power.  If power timeout is set
	 *  to a non-zero value then we are, so shut off the power. 
         */

        if ( pdr->ptmo )
        { 
            /*
             *  Shut off the motor power by clearing the power control bit
             *  and requesting a write to the power control output link.
             */

            pdr->pwr = FALSE; 
            MONITOR(RECORD_PWR);

            /*   Trigger the link unless we're simulating. */ 
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_PWR);
            }

            /*  Are we using the auxiliary power bit? */

            if (pdr->uapb)
            {
                /*  Call the device support power control function.*/
                status = (*pdset->controlPower) (pPriv, FALSE);
                if (status)
                {
                    DEBUG(DDR_MSG_ERROR, 
                          "<%ld> %s:abortingState: power control fault%c\n",
                          ' ');
                    SET_ERR_MSG( pPriv->errorMessage);
                }
            }

           /*  
            *  Load the callback timer delay with the longer of the power timeout 
            *  and brake timeout delays.
            */
     
           if (delay < pdr->ptmo)
            {
                delay = pdr->ptmo;
            }
        }  


        /*
         *  In simulation mode fake the power and brake action by setting
         *  the associated input status bits directly.
         */

        if (pPriv->simulation)
        {
            pdr->psta = FALSE;
            MONITOR(RECORD_PSTA);
            pdr->bsta = TRUE;
            MONITOR(RECORD_BSTA);
        }

        /*
         *  Clear any motion modifiers, indicate the motion failed and we
         *  lost our home reference then terminate the command.
         */
       
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->move_while_busy = FALSE;
        pPriv->backlashMotion = FALSE;
        semGive (pPriv->mutexSem);

        pdr->dir = DDR_DIR_STOP;
     
        pdr->mip = DDR_MIP_ERROR;
        MONITOR(RECORD_MIP); 
       
        pdr->hpvl = FALSE;
        MONITOR(RECORD_HPVL);
     

        /*
         *  If a callback is required to allow time for power/brake
         *  control to take effect then start the timeout timer here.
         */

        if (delay > 0)
        {
            (*pdset->setDelay) (pPriv, delay);
            DEBUG(DDR_MSG_FULL,
                   "<%ld> %s:abortingState: setting timeout:%ld\n",
                   delay);
            return (status);
        }


        /*
         *  Otherwise consider the motion aborted and return to the
         *  idle state to await a new command.
         */

        else
        {
            return idleState (pdr);          /* sequence is complete */
        }

    }  /* end of entering state processing */
     

    /*
     *  Calls to this function while already in FAILING state will check for 
     *  the action to stop in response to the abort commands issued in the 
     *  first call.
     *
     *  If the external status bits indicate that the things have
     *  been shut down successfully then proceed to the idle state and wait
     *  for another command.
     */

    if (((pdr->upsb && pdr->ubsb) && (!pdr->psta && pdr->bsta)) ||
        ((pdr->upsb && !pdr->ubsb) && !pdr->psta) ||               
        ((!pdr->upsb && pdr->ubsb) && pdr->bsta) ||
         (!pdr->upsb && !pdr->ubsb))
    {
        (*pdset->setDelay) (pPriv, 0);    
        DEBUG(DDR_MSG_MAX,
                "<%ld> %s:abortingState: pwr/brk OK cancel timeout%c\n", ' ');

        return idleState (pdr);   
    }


    /*
     *  Is this the callback requested in the first call to this function?
     *  If so, check the state of the input status bits. 
     */

    if (pPriv->timeout)
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);

        /*
         *  If we have power state feedback make sure that the power
         *  has indeed been turned off.  If it hasn't then try to turn
         *  it off again.
         */

        if ( pdr->upsb && pdr->psta )
        {
            SET_ERR_MSG( "motor didn't power off in time");
            DEBUG(DDR_MSG_FULL,
                  "<%ld> %s:abortingState: motor didn't power off, retry%c\n",
                  ' ');
            if (pdr->uapb == 1)
            {
                status = (*pdset->controlPower) (pPriv, FALSE);
                DEBUG(DDR_MSG_ERROR, "<%ld> %s:abortingState: Power fault%c\n",
                ' ');
                SET_ERR_MSG( pPriv->errorMessage );
            }
        }

        /*
         *  If we have brake state feedback make sure that the brake
         *  has indeed been applied.  If it hasn't then generate an
         *  error message.
         */

        if ( pdr->ubsb && !pdr->bsta )
        {
            SET_ERR_MSG( "brake did not engage in time");
            DEBUG(DDR_MSG_ERROR,
               "<%ld> %s:abortingState: brake did not engage in time%c\n", ' ');
        }

        return idleState (pdr);
    }

    /* 
     *  Otherwise keep waiting ...
     */

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * alarm
 *
 * INVOCATION:
 * alarm (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)     deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (void)
 *
 * PURPOSE:
 * Raise an alarm on the VAL or MPOS fields
 *
 * DESCRIPTION:
 * Raise an alarm on the VAL or MPOS fields.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void alarm
(
    DEVICE_CONTROL_RECORD    *pdr    /* deviceControl record structure      */
)
{
    short rangeFlag;

    /*
     *  Set the undefined value alarm if the UDF field is set.
     */

    if (pdr->udf == TRUE)
    {
        recGblSetSevr(pdr, UDF_ALARM, INVALID_ALARM);
        return;
    }

    /*
     *  Raise range alarms if motor position or target position value exceeds
     *  limits.
     */

    if ((pdr->val > pdr->hihi || pdr->mpos > pdr->hihi) &&
        recGblSetSevr(pdr, HIHI_ALARM, pdr->hhsv))
    {
        return;
    }

    if ((pdr->val > pdr->high || pdr->mpos > pdr->high) &&
        recGblSetSevr(pdr, HIGH_ALARM, pdr->hsv))
    {
        return;
    }

    if ((pdr->val < pdr->low || pdr->mpos < pdr->low) &&
        recGblSetSevr(pdr, LOW_ALARM, pdr->lsv))
    {
        return;
    }

    if ((pdr->val < pdr->lolo || pdr->mpos < pdr->lolo) &&
        recGblSetSevr(pdr, LOLO_ALARM, pdr->llsv))
    {
        return;
    }


    /*
     *  Set the limit violation field if the target position value or 
     *  motor position fields are outside limits.
     */

    rangeFlag = (pdr->val > pdr->phlm || pdr->val < pdr->pllm) ? TRUE : FALSE;

    if (rangeFlag ^ pdr->lvio)
    {
        pdr->lvio = rangeFlag;
        MONITOR(RECORD_LVIO);
    }

    return;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * calculateTimeout
 *
 * INVOCATION:
 * timeout = calculateTimeout (start, end, velo, accel);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) start    (double)        current position
 * (>) end      (double)        target position
 * (>) velo     (double)        motion velocity
 * (>) accel    (double)        acceleration profile
 *
 * FUNCTION VALUE:
 * (long) maximum motion time in units of 0.1 seconds
 *
 * PURPOSE:
 * Predict the motion time (to be used for timeout)
 *
 * DESCRIPTION:
 * Calculate motion timeout based on distance to travel, acceleration rate
 * and motion velocity. Timeout is calculated in units of 0.1 seconds - 
 * partly because that is what setDelay uses but also because the force
 * to long loses some resolution. So what it does is calculate the actual 
 * timeout in seconds, multiply by 10 to get in the right units for setDelay
 * then cast as a long.  Before the timeouts are passed to setDelay a 
 * default overhead number is added.
 * 
 * Ramp up time = ramp down time =  velocity/acceleration
 * Ramp up distance = ramp down distance = ramp up time * velocity / 2
 * Therefore 
 *       ramp up plus ramp down distance  = velocity * velocity / acceleration
 *       ramp up plus ramp down time = 2 * velocity /acceleration
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * - Assumes a linear ramp up/down.
 * - Assumes ramp up starts at velo of 0.
 * - Does not account for changes of velocity sent to motor for indexing.
 * - Always assumes motor is already moving but in the opposite direction
 * and adds an initial ramp down (i.e. ramp down to VBAS then switch 
 * direction and ramp up from VBAS). Within that assumption is another 
 * assumption which is that the ramp down is from the same velocity
 * as the new target.
 *-
 ************************************************************************
 */

static long calculateTimeout
(
    double start,                   /* motion starting point                */
    double end,                     /* motion ending point                  */
    double velo,                    /* motion velocity                      */
    double accel                    /* acceleration/deceleration rate       */
)
{
    long   timeout;                 /* calculated motion timeout in 0.1sec  */
    double rampTime;                /* time in seconds to ramp up or down   */
    double rampDistance;            /* distance in EGUs of ramp up or down  */

    /*
     *  Acceleration and velocity must be set to positive values
     */

    if (accel <= 0.0 || velo <= 0.0)
    {
        logMsg("deviceControlRecord:calculateTimeout: accel=%d, velo=\n", 
               accel,velo,0,0,0,0);
        return 0;
    }

    /*
     *  Calculate ramp time and distance here to make timeout calculation
     *  easier to read. Assumes a linear ramp.
     */

    rampTime     = (velo / accel);
    rampDistance = ( rampTime * velo / 2); 

    /*
     *  If the move will happen within the ramp up/down distance, then 
     *  calculate the short move timeout, which is the full ramp up/down 
     *  time multiplied by the square root of the ratio of the actual move 
     *  distance to the full ramp up/down distance.  Add also another full
     *  ramp down in case the motor is already  moving but in the opposite 
     *  direction.
     */

    if (fabs (end - start) < (2 * rampDistance))
    {
        timeout = (long)(ceil (10.0 * (rampTime + (2.0 * rampTime) * 
                     sqrt( fabs(end - start) / (2 * rampDistance)))));
    }

    /*
     *  Otherwise calculate the long move timeout, which is the time to
     *  fully ramp up & down plus the time between at full velocity. 
     *  Add also another full ramp down in case the motor is already 
     *  moving but in the opposite direction.
     */

    else
    {
        timeout = (long) (ceil (10.0 * ((3.0 * rampTime) + 
                  ((fabs (end - start) - (2 * rampDistance) ) / velo))));
    }

    return timeout;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * checkVals
 *
 * INVOCATION:
 * status = checkVals (pdr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)    deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code, always (found).
 *
 * PURPOSE:
 * Convert the contents of the string value (VALS) field to a position
 *
 * DESCRIPTION:
 * Return a conversion success flag using the following algorithm:
 *
 *      If the VALS field is not empty:
 *      {   
 *          Try to convert the string to a double value.
 *          If the conversion was successful:
 *          {
 *              Set position to converted value.
 *              Set the success flag.
 *          }
 *
 *          Else it was not a number, so:
 *          {
 *              Scan each entry in the translation table for a matching name.
 *              If the names match then:
 *              {
 *                  Set position to value in table matching string.
 *                  Set index to value in table.
 *                  Set the success flag.
 *                  Skip the remainder of the table.
 *              }
 *          }
 *      }
 *
 *      Else the VALS field was empty, so:
 *      {
 *          Set the success flag without changing the position value.
 *      }
 *
 *       
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long checkVals
(
    DEVICE_CONTROL_RECORD   *pdr,       /* (in) Record pointer.             */
    double                  *position,  /* position found                   */
    long                    *index      /* index mode found                 */          
)
{
    TRANSLATION_NODE *pNode;            /* Translation table node structure */
    int              found = FALSE;     /* Flag to say we found a value.    */
    char             *end;              /* End of string conversion pointer */


    /*
     *  If there is a string in the VALS field then we need to analyze it.
     */

    if ( strlen( pdr->vals ) )
    {
        DEBUG(DDR_MSG_MAX, "<%ld> %s:checkVals: vals=%s\n", pdr->vals );


        /*
         *  Try to convert the string to a double value.
         */

        errno = 0;
        *position = strtod (pdr->vals, &end );

        /*
         *  If the translation ended with the string termination then the
         *  conversion was successful and the vals field held a double value.
         */

        if (*end == '\0')
        {
            DEBUG(DDR_MSG_MAX, 
                  "<%ld> %s:checkVals: decodes as %f\n",
                  *position );
            found = TRUE;
        }


        /*
         *  Otherwise it was not a number.   Look instead for one of the
         *  predefined named positions contained in the lookup table.
         */

        else
        {
            pNode = (TRANSLATION_NODE *) ellFirst (pdr->lthp);


            /*
             *  Check the name field of each element of the translation
             *  table linked list for a match with the contents of VALS.
             *  If a match is found then recover the position associated
             *  with that name and skip the remaining entries.
             */

            while (pNode)
            {
                if (strcmp(pNode->name, pdr->vals) == 0)
                {
                    found = TRUE;

                    *position = pNode->target;
                    *index = pNode->index;

                    DEBUG(DDR_MSG_MAX,
                          "<%ld> %s:checkVals: translates to %f\n",
                          *position );
                    DEBUG(DDR_MSG_MAX, 
                          "<%ld> %s:checkVals: index mode %ld\n",
                          *index );
                    break;
                }

                pNode = (TRANSLATION_NODE *) ellNext (&pNode->node);
            }
        }
    }


    /*
     *  Otherwise there was nothing in the VALS field so just use the
     *  VAL field directly.
     */

    else
    {
        found = TRUE;
    }

    return ( found );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * depoweringState
 *
 * INVOCATION:
 * status = depoweringState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)     deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Turn off motor power.
 *
 * DESCRIPTION:
 * This function is called when the locking state exits and every time the 
 * record processes thereafter until we leave this state for the idle or 
 * aborting states.   It will de-energize the motor power via the following 
 * algorithm:
 * 
 *      If we were called from another state (entering depowering state):
 *      {
 *          Switch state machine to depowering state.
 *
 *          If we can control the power:
 *          {
 *              Clear power control bit.
 *              Request that the power control link be written.
 *
 *              If controlling power via device support:
 *              {
 *                  Call device suppport power control function.
 *              }
 *
 *              Set a callback timeout so that we can check the power.
 *              Return and wait for the record to be processed again.
 *          }
 *
 *          Otherwise there is nothing to do so:
 *          {
 *              Exit to the idle state.
 *          }
 *      }
 *
 *      Abort the operation immediately if the moving flag comes active.
 *
 *      If we are using the power status bit and the power is now off:
 *      {
 *              Cancel the callback timeout.
 *              Exit to the idle state.
 *      }
 *
 *      If this is the callback requested above:
 *      {
 *          If we are not using the power status bit:
 *          {
 *              Exit to the idle state.
 *          }
 *          If we are in simulation mode:
 *          {
 *              Clear the power status bit to make it look like power is off.
 *              Exit to the idle state.
 *          }
 *          Otherwise there must be a problem because power is still on:
 *          {
 *              Generate an error message.
 *              Exit to the aborting state.
 *          }
 *      } 
 *
 *      Remain in depowering state until the record is processed again.
 *
 * DESCRIPTION:
 *  
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long depoweringState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
            *pPriv = pdr->dpvt;         
    DEVICE_CONTROL_DSET             /* device support function structure    */     
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long    status = 0;             /* Return function status.              */


    DEBUG(DDR_MSG_MAX, "<%ld> %s:depoweringState:entry%c\n", ' ');


    /*
     *  Turn off the power when first entering depowering state.
     */
    
    if ( pdr->osta != DDR_DEPOWERING )
    {
        DEBUG(DDR_MSG_FULL,
              "<%ld> %s:depoweringState: from osta:%d\n", pdr->osta);
        pdr->osta = DDR_DEPOWERING;


        /*
         *  If we are controlling the power (power timeout is set) then
         *  command the power to go off.
         */

        if ( pdr->ptmo )
        {
            /*
             *  Clear power control bit output field and request that the
             *  associated output link be written when processing ends.
             */

            pdr->pwr = FALSE;
            MONITOR(RECORD_PWR);
                    
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_PWR);
            }
           

            /*
             *  If we are also using the device support power control (use
             *  auxiliary power bit is set) then call the associated device
             *  support function to turn off the power this way.
             */

            if (pdr->uapb)
            {
                status = (*pdset->controlPower) (pPriv, FALSE);
                if (status)
                {
                    DEBUG(DDR_MSG_ERROR,
                          "<%ld> %s:depoweringState: Power control fault%c\n",
                          ' ');
                    SET_ERR_MSG( pPriv->errorMessage);
                    return abortingState (pdr);  /* command fails here      */
                }
            }


            /*
             *  Request a timeout after the power-off delay so that we
             *  chan check to ensure the power really did go off then
             *  return to wait for the timeout.
             */

            (*pdset->setDelay) (pPriv, pdr->ptmo);
            return ( status );
        }


        /*
         *  Otherwise we are not controlling the power so command is now
         *  finished.  Move directly on to idle state to await a new command.
         */

        else
        {
            return idleState (pdr);
        }
    }


    /*
     *  Abort the operation immediately if the moving flag suddenly comes alive.
     */

    if ( pPriv->moving )
    {
        SET_ERR_MSG("Unexpected motion while depowering");
        DEBUG(DDR_MSG_ERROR, 
              "<%ld> %s:depoweringState:moving flag set after powering down%c\n", ' ');
        return abortingState (pdr);
    }


    /*
     *  Second or subsequent pass through depowering state.   Check to see
     *  if the power was successfully turned off.   If the power is off then
     *  the command is finished so cancel the callback and move directly on 
     *  to the idle state to wait for a new command.
     */
    
    if (pdr->upsb && !pdr->psta)
    {       
        (*pdset->setDelay) (pPriv, 0);
        return idleState (pdr);
    }


    /*
     * A timeout means that this is the callback requested above.
     */
    
    if (pPriv->timeout)
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);


        /*
         *  If we do not have power status feedback (use power status bit 
         *  has not been set) then there is nothing to check.  Command is 
         *  finished, move on to idle state.  This is just a delay.
         */

        if (!pdr->upsb)        
        {
            return idleState (pdr);
        }


        /*
         *  If we are simulating motion then fake the power off by 
         *  setting the power status bit directly.  Command is finished,
         *  move on to idle state.
         */

        else if (pPriv->simulation)    
        {
            pdr->psta = FALSE;
            MONITOR(RECORD_PSTA);
            return idleState (pdr);
        }


        /*
         *  Othewise the timeout expired and the power did not turn off.
         *  Something went wrong so abort the command immediately at this 
         *  point.
         */

        else                   
        {
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:depoweringState, power still on after timeout%c\n",
                  ' ');
            SET_ERR_MSG( "motor did not power off in time");
            return abortingState (pdr);
        }
    }

     
    /*
     * Otherwise wait to be called again...
     */

    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getAlarmDouble
 *
 * INVOCATION:
 * status = getAlarmDouble (paddr, pad);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *)     Pointer to database address structure.
 * (>) pad (struct dbr_alDouble *)  Alarm double stuff is returned here.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Return the alarm double information for display
 *
 * DESCRIPTION:
 * Return the state of the alarms if the dbAddr points to the VAL or MPOS
 * fields otherwise return the generic alarm structure.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long getAlarmDouble
(
    struct dbAddr *paddr,           /* Database field address structure     */
    struct dbr_alDouble *pad        /* Alarm double structure               */
)
{
    /*
     *  Recover the record structure from the field address structure
     */

    DEVICE_CONTROL_RECORD *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;

    DEBUG(DDR_MSG_MAX, "<%ld> %s:getAlarmDouble entry%c\n", ' ');


    /*
     *  If this is either the VAL or the MPOS fields copy the alarm
     *  information into the calling structure.
     */

    if (paddr->pfield == (void *) &pdr->val ||
        paddr->pfield == (void *) &pdr->mpos)
    {
        pad->upper_alarm_limit = pdr->hihi;
        pad->upper_warning_limit = pdr->high;
        pad->lower_warning_limit = pdr->low;
        pad->lower_alarm_limit = pdr->lolo;
    }


    /*
     *  Otherwise use the generic alarm structure.
     */

    else
    {
        recGblGetAlarmDouble(paddr, pad);
    }

    return (0);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getControlDouble
 *
 * INVOCATION:
 * status = getControlDouble (paddr, pcd);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *) Pointer to database address structure.
 * (<) pcd (dbr_ctrlDouble *)   Control double stuff is returned here.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Update the control double information for display
 *
 * DESCRIPTION:
 * Copy the high and low position limits into the control double structure
 * if the dbAddr structure points to the VAL or MPOS fields otherwise use
 * the generic control double information.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long getControlDouble
(
    struct dbAddr *paddr,           /* database field address structure     */ 
    struct dbr_ctrlDouble *pcd      /* control double structure             */
)
{
    /*
     *  Recover the record structure from the field address structure
     */

    DEVICE_CONTROL_RECORD *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;

    DEBUG(DDR_MSG_MAX, "<%ld> %s:getControlDouble entry%c\n", ' ');


    /*
     *  If this is the VAL or MPOS fields copy the high and low position 
     *  limits into the control double structure.
     */

    if (paddr->pfield == (void *) &pdr->val ||
        paddr->pfield == (void *) &pdr->mpos )
    {
        pcd->upper_ctrl_limit = pdr->phlm;
        pcd->lower_ctrl_limit = pdr->pllm;
    }


    /*
     *  Otherwise use the generic control double information.
     */

    else
    {
        recGblGetControlDouble(paddr, pcd);
    }
   
    return (0);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getGraphicsDouble
 *
 * INVOCATION:
 * status = getGraphicsDouble (paddr, pgd);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *) Pointer to database address structure.
 * (<) pcd (dbr_grDouble *)     Graphics double stuff is returned here.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Update the graphics double information for display
 *
 * DESCRIPTION:
 * Copy the high and low position limits into the graphics double structure
 * if the dbAddr structure points to the VAL or MPOS fields otherwise use
 * the generic graphics double information.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long getGraphicDouble
(
    struct dbAddr *paddr,           /* database field address structure     */ 
    struct dbr_grDouble *pgd        /* display graphics double structure    */
)
{
    /*
     *  Recover the record structure from the field address structure
     */

    DEVICE_CONTROL_RECORD *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;

    DEBUG(DDR_MSG_MAX, "<%ld> %s:getGraphicDouble entry%c\n", ' ');


    /*
     *  If this is the VAL or MPOS fields copy the high and low position 
     *  limits into the graphics double structure.
     */

    if (paddr->pfield == (void *) &pdr->val ||
        paddr->pfield == (void *) &pdr->mpos ) 
    {
        pgd->upper_disp_limit = pdr->phlm;
        pgd->lower_disp_limit = pdr->pllm;
    }

    
    /*
     *  Otherwise use the generic control double information.
     */

    else 
    {
        recGblGetGraphicDouble(paddr, pgd);
    }

    return (0);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getPrecision
 *
 * INVOCATION:
 * status = getPrecision (paddr, precision);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *) Pointer to database address structure.
 * (<) precision (long *)       Precision is returned here
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Return the precision level of selected fields for display
 *
 * DESCRIPTION:
 * For all fields that contain values in user (real) units return the
 * value of the display precision (PREC) field.   Otherwise return the
 * generic display precision value.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long getPrecision
(
    struct dbAddr *paddr,       /* database field address structure         */ 
    long *precision             /* number of decimal places to display      */
)
{
    /*
     *  Recover the record structure from the field address structure
     */

    DEVICE_CONTROL_RECORD *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;

    DEBUG(DDR_MSG_MAX, "<%ld> %s:getPrecision entry%c\n", ' ');


    /*
     *  For all fields that contain position and velocity values,
     *  set the display precision to the value of the PREC field.
     */

    if ( (paddr->pfield == (void *) &pdr->val)  ||
         (paddr->pfield == (void *) &pdr->mpos) ||
         (paddr->pfield == (void *) &pdr->velo) ||
         (paddr->pfield == (void *) &pdr->vbas) ||
         (paddr->pfield == (void *) &pdr->accl) ||
         (paddr->pfield == (void *) &pdr->vhlm) ||
         (paddr->pfield == (void *) &pdr->vllm) ||
         (paddr->pfield == (void *) &pdr->phlm) ||
         (paddr->pfield == (void *) &pdr->pllm) ||
         (paddr->pfield == (void *) &pdr->blco) ||
         (paddr->pfield == (void *) &pdr->hpo) ||
         (paddr->pfield == (void *) &pdr->fivl))
      {
            *precision = pdr->prec;
      }


    /*
     *  Otherwise return the generic display precision setting.
     */

    else
    {
        recGblGetPrec(paddr, precision);
    }

    return (0);
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getUnits
 *
 * INVOCATION:
 * status = getUnits (paddr, units);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *) Pointer to database address structure.
 * (<) units (char *)           Engineering units are returned here.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Return the engineering units field (EGU) string for display
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * Returns engineering units for all strings including those not 
 * expressed in engineering units!
 *-
 ************************************************************************
 */

static long getUnits
(
    struct dbAddr *paddr,           /* database field address structure     */ 
    char *units                     /* units string to display              */ 
)
{
    /*
     *  Recover the record structure from the field address structure
     */

    DEVICE_CONTROL_RECORD *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;

    DEBUG(DDR_MSG_MAX, "<%ld> %s:getUnits entry%c\n", ' ');


    /*
     *  For ALL fields return the engineering units string
     */

    strncpy (units, pdr->egu, DB_UNITS_SIZE);
    units[DB_UNITS_SIZE-1] = '\0';

    return (0);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * getValue
 *
 * INVOCATION:
 * status = getValue (pdr, pvdes);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)   Pointer to deviceControl
 *                           record structure.
 * (<) pvdes (struct valueDes *)    Value field stuff goes here
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Return EPICS value field information
 *
 * DESCRIPTION:
 * Copy value field information into the record value description structure.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long getValue
(
    DEVICE_CONTROL_RECORD *pdr,     /* deviceControl record structure       */ 
    struct valueDes *pvdes          /* value field description structure    */
)
{
    DEBUG(DDR_MSG_MAX, "<%ld> %s:getValue: entry%c\n", ' ');

    /*
     *  Load value field information into the description structure.
     */

    pvdes->field_type = DBF_DOUBLE;
    pvdes->no_elements = 1;
    pvdes->pvalue = &pdr->val;

    return (0);
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * holdingState
 *
 * INVOCATION:
 * status = holdingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Hold motor on while waiting for a new motion track command
 *
 * DESCRIPTION:
 * The holding state is used to keep the motor active (brake off and
 * power on) between position commands when in TRACK mode.  This function
 * is called when the system is in TRACK mode and the moving state exits 
 * and every time the record processes thereafter until we leave this state
 * for the starting, moving, locking or aborting states.  
 *
 * It waits for position updates via the following algorithm:
 *
 *      If we were called from another state (entering holding state):
 *      {
 *          Set the pp flag to indicate last motion has completed.
 *          Switch state machine into holding state.
 *      }
 *
 *      If the directive field has changed to GO:
 *      {
 *          If the requested motion is larger than the motion deadband:
 *          {
 *              Set the directive field to CHECK.
 *              Exit to the starting state (begin a new motion).
 *          }
 *
 *          Otherwise:
 *          {
 *              Set the directive field to CHECK.
 *              Exit to the moving state (force re-entry into holding state).
 *          }
 *      }
 *
 *      If the directive field has changed to STOP:
 *      {
 *          Exit to the locking state (shut down motor).
 *      }
 *
 *      Remain in holding state until record is processed again. 
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * Does not check for unwanted motion or limit switches while waiting
 * for commands.
 *-
 ************************************************************************
 */

static long holdingState 
(
    DEVICE_CONTROL_RECORD *pdr      /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
        *pPriv = pdr->dpvt;
    long status = 0;                /* function return status               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:holdingState:entry%c\n", ' ');


    /*
     *  If entering the holding state from another state (first pass through
     *  the function), update the state status fields.   If we are in tracking
     *  mode then the motion is considered finished at this point so set the 
     *  pp flag to end the command.  Note that since this state is only
     *  entered while in TRACK mode the pp flag will always be set!
     */

    if (pdr->osta != DDR_HOLDING)
    {
        DEBUG(DDR_MSG_FULL,
               "<%ld> %s: holdingState: from osta:%d\n", pdr->osta);
        if ( pPriv->mode == DDR_MODE_TRACK )
        {
            pdr->pp = TRUE;
        }

        pdr->osta = DDR_HOLDING;
        pdr->mip = DDR_MIP_HOLDING;
        MONITOR(RECORD_MIP);
    }
     
   
    /*
     *  If the current directive is GO then there is a new position 
     *  target available.   Check to see if this requires motion or not.
     */

    if (pdr->dir == DDR_DIR_GO)
    {
        /*
         *  If the new target position is outside the motion deadband
         *  then go back to starting state to begin the new motion.
         *  Setting the directive field to DDR_CHECK without raising a
         *  monitor is a somewhat kludgey way of preventing the record
         *  from responding to changes in the VAL field until the GO
         *  directive is issued again.
         */

        if (fabs(pdr->val - pdr->mpos) > pdr->mdbd)
        {
            pdr->dir = DDR_DIR_CHECK;
            status = startingState (pdr);    
        }


        /*
         *  Otherwise the new position is within the motion deadband.
         *  Switch back to moving state to acknowledge the request without
         *  actually starting a motion.  Set the directive to check as
         *  in the previous test.
         */

        else
        {
            pdr->dir = DDR_DIR_CHECK;
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->moving = TRUE;
            semGive (pPriv->mutexSem);
            DEBUG(DDR_MSG_FULL,
                  "<%ld> %s:holdingState:motion < motor deadband%c\n", ' ');
            status = movingState (pdr);
        }

        return (status);
    }


    /*
     * Stop takes us out of holding mode
     */
      
    if (pdr->dir == DDR_DIR_STOP)
    {
        status = lockingState (pdr);
    }   

         
    /*
     * Otherwise just hang out until something more interesting happens
     */ 
    
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * idleState
 *
 * INVOCATION:
 * status = idleState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) Pointer to deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Wait for new motion command
 *
 * DESCRIPTION:
 * The idle state is used to monitor the system between repositioning commands.
 * It is called when the depowering or aborting states exit and every time the
 * record processes thereafter until we leave this state for the powering
 * state. 
 * It waits for position updates via the following algorithm:
 *
 *      If we were called from another state (entering idle state):
 *      {
 *          Switch state machine into idle state. 
 *          If a command requiring movement was received before reaching idle:
 *          {
 *              Exit to the powering state (start a motion sequence).
 *          }
 *
 *          If the motor power is on:
 *          {
 *              Call device support to turn off motor power.
 *          }
 *
 *          If error free, set motor state fields to indicate motion has finished.
 *          Set the post processing flag to indicate that command has finished.
 *          Return and wait for the record to be processed again.
 *      }
 * 
 *      If timeout flag is set this is an "actionless" command acknowledgement:         
 *      {
 *          If the mode field is set to TEST then test the record by:
 *          {
 *              Checking to see if the motor power is off.
 *              Checking to see if the interlock flag is set.
 *          }
 *          Set post processing flag to indicate that command has finished.
 *      } 
 *
 *      If the directive field is GO then a new motion command has been issued:
 *      {
 *          If new target is outside of the motion deadband:
 *          {
 *              Exit to the powering state to begin the motion.
 *          }
 *      }
 *
 *      If the directive field is STOP and the motor is moving:
 *      {
 *          Exit to the stopping state to terminate the motion.
 *      }
 *
 *      If using an encoder check it ... if the device has moved:
 *      {
 *          Switch the motor state to ERROR.
 *          Clear the home position valid field to force re-indexing device.
 *          Generate an error message.
 *      }
 *
 *      Remain in idle state until the record is processed again.
 *               
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long idleState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE 
        *pPriv = pdr->dpvt;         /* internal control structure           */
    DEVICE_CONTROL_DSET             /* device support function structure    */
        *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long    status = 0;             /* function status return               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:idleState:entry%c\n", ' ');


    /*
     *  If we are moving into idle state from some other state (first
     *  pass) then do the state switching stuff here.
     */

    if (pdr->osta != DDR_IDLE)
    {

        DEBUG(DDR_MSG_FULL,
             "<%ld> %s:idleState: from osta:%d\n", pdr->osta);
        pdr->osta = DDR_IDLE;          
       
        /*
         * If one of the motion parameters change while we were shutting
         * down the motor then proceed immediately to execute the new
         * command.
         */
   
        if ( pPriv->move_while_busy )
        {
            DEBUG(DDR_MSG_FULL,
                  "<%ld> %s:idleState: Motion parameter change while moving%c\n",
                  ' ');
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->move_while_busy = FALSE;
            semGive (pPriv->mutexSem);

            pdr->dir = DDR_DIR_CHECK;
            return ( poweringState (pdr) );   
        }

        /*
         *  No new command to execute so ensure that the motor power is off.
         *  Some motion control cards are notorious for ignoring or losing
         *  power shut-off commands.... so try again once if the power is 
         *  still on.
         */

        if (pdr->uapb)
        {
            DEBUG(DDR_MSG_FULL,
                   "<%ld> %s:idleState: turning power off%c\n", ' ');
            status = (*pdset->controlPower) (pPriv, FALSE);
            if (status)
            {
                DEBUG(DDR_MSG_ERROR, 
                      "<%ld> %s:idleState: Cannot turn off power:%c\n", ' ');
                SET_ERR_MSG( pPriv->errorMessage);
            }
        }


        /*
         *  If the last command completed successfully then set the motion
         *  status field to the STOPPED state (idle).
         */

        if (pdr->mip != DDR_MIP_ERROR)
        {
            pdr->mip = DDR_MIP_STOPPED;
            MONITOR(RECORD_MIP);
        }


        /*
         *  Clear the timeout flag if it was still set.
         */
       
        if (pPriv->timeout)
        {
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->timeout = FALSE;
            semGive (pPriv->mutexSem);
        }


        /*
         *  Overwrite the current directive field with the stop directive.
         *  Doing this without raising a monitor is a somewhat kludgey way
         *  of forcing the system to recognize when a new GO directive has
         *  been issued.
         */

        pdr->dir = DDR_DIR_STOP;


        /*
         *  Set the post processing flag to indicate that the last command
         *  finished and then exit and wait to be called again.
         */
       
        pdr->pp = TRUE;
        return status;
    }


    /* 
     *  This is the second or subsequent call to this function (already
     *  in idle state) so check to see why we were called.
     */


    /*
     * A timeout in idle state is the record's way of leaving the busy
     * field set to BUSY for a period of time to let the world see that
     * a command that required no action was received and processed.
     */


    if (pPriv->timeout)
    {
        /*
         *  If this is a request to test the device then make sure that
         *  the power is off and that there are no interlocks active.
         */

        if (pPriv->mode == DDR_MODE_TEST && pPriv->simulation == DDR_SIM_NONE )
        {
            if (pdr->uapb)
            {
                status = (*pdset->controlPower) (pPriv, FALSE);
                if (status)
                {
                    DEBUG(DDR_MSG_ERROR, 
                          "<%ld> %s:idleState:Cannot turn off power:%c\n",' ');
                    SET_ERR_MSG(pPriv->errorMessage);
                }
            }

            if (pdr->flt)
            {
                DEBUG(DDR_MSG_ERROR, 
                     "<%ld> %s:idleState:Interlock active:%c\n",' ');
                SET_ERR_MSG("Interlock line active");
            }       
        }


        /*
         *  Set the post processing flag to indicate that the command
         *  has completed processing.
         */
 
        DEBUG(DDR_MSG_MAX, 
              "<%ld> %s:idleState:timeout true, setting PP%c\n", ' ');
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);
        pdr->pp = TRUE;
        return status;
    }


    /*
     *  If the directive field has changed to GO then a new command has
     *  been recived.  Analyze the val field to see if motion is required.
     */

    if (pdr->dir == DDR_DIR_GO)
    {
        /*
         *  If this is an index request or the target position is outside of
         *  the motion deadband then we need to adjust the motor position.
         */

        if ((pPriv->mode == DDR_MODE_INDEX) ||
            (pPriv->mode == DDR_MODE_MOVE &&
             fabs(pdr->val - pdr->mpos) > pdr->mdbd) ||
            (pPriv->mode == DDR_MODE_TRACK &&
             fabs(pdr->val - pdr->mpos) > pdr->mdbd))
        {
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:idleState:DIR=Go, motion required in mode=%ld\n",
                  pPriv->mode );

            pdr->dir = DDR_DIR_CHECK;
            return ( poweringState (pdr) );    
        }


        /*
         *  Otherwise the target is unchanged or within the motion
         *  deadband.
         */

        else
        {
            DEBUG(DDR_MSG_FULL,
                  "<%ld> %s:idleState:DIR=GO no motion required, mode=%ld\n",
                  pPriv->mode );
        }
    }

    /*
     *  If a STOP directive was received then bring the device to a controlled 
     *  stop by switching to the stopping state.  This will happen if the motor
     *  has runaway or is being controlled by some means other than this driver.
     */
    
    if ((pdr->dir == DDR_DIR_STOP) && (pPriv->moving == TRUE))
    {
        return ( stoppingState (pdr) );
    }

    /*
     *  Nothing should be moving so keep an eye out for spontaneous
     *  device motion...
     */

    if (pdr->ueip && (MONITORED(RECORD_RENC)) && 
       !pPriv->insideDeadband && !pdr->simm)
    {
        /*
         *  This is a serious error since we can no longer trust our
         *  index position.   Set the motor state to ERROR and clear
         *  the home position valid flag to force re-indexing before the
         *  next move can be made.
         */

        DEBUG(DDR_MSG_ERROR, 
              "<%ld> %s:idleState:spontaneous device motion%c\n", ' ');

        pdr->mip = DDR_MIP_ERROR;
        MONITOR(RECORD_MIP); 
       
        pdr->hpvl = FALSE;
        MONITOR(RECORD_HPVL);

        SET_MESS( "Unexpected device motion!!");
        MONITOR(RECORD_MESS);
    }
         
    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * initLinks
 *
 * INVOCATION:
 * status = initLinks (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) Pointer to deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code, always 0.
 *
 * PURPOSE:
 * Initialize the epics input and output database links
 *
 * DESCRIPTION:
 * The input links (associated with the VAL, SIMM and DBUG fields) are
 * initialized to point to the source of the values for these fields.
 * The output links (associated with the BRK, PWR, BUSY and MESS fields) are
 * initialized to point to the destination for the values of these fields.
 * Each field is checked for an active link via the following algorithm:
 *
 *      If the desired output location field (DOL) is of type CONSTANT:
 *      {
 *          Initialize the value field with the value of the DOL field.
 *      }
 *      
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long initLinks 
(
DEVICE_CONTROL_RECORD *pdr          /* deviceControl record structure       */
)
{
    long status = 0;                /* function return status               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:initLinks: entry%c\n", ' ');


    /*
     *  If the VAL field has not been linked (via the DOL field) to another 
     *  epics record field at startup then its type will be read as CONSTANT.
     *  Set the VAL field to the value of the DOL field in case it is being 
     *  used to initialize the VAL field.
     */

    if (pdr->dol.type == CONSTANT)
    {
        pdr->udf = FALSE;
	recGblInitConstantLink(&pdr->dol, DBF_DOUBLE, &pdr->val);
    }

    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * initRecord
 *
 * INVOCATION:
 * initRecord (pdr, pass);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) Pointer to deviceControl record structure.
 * (>) pass (int) Pass number (function called twice during initialization)
 *
 * FUNCTION VALUE:
 * (long) initialization pass success code.
 *
 * PURPOSE:
 * Initialize an instance of the deviceControl record and device support
 *
 * DESCRIPTION:
 * This function is called twice by the EPICS system during database
 * initialization.   The first time is before any of the fields have
 * been set up, the second to allow database links to be created.
 * All processing is done during the second pass.  The following algorithm
 * is used to initialize an instance of the record:
 *
 *      If this is the first pass then return immediately.
 *
 *      If the device support access structure is missing abort immediately.
 *      If critical device support functions are missing abort immediately.
 *
 *      Setup input and output database links to other records. 
 *
 *      Create an internal control structure for this record.
 *      Create a mutual exclusion semaphore to protect this structure.
 *      Initialize the structure to a safe starting state.
 *
 *      Initialize the critical record database fields to a startup state.
 *      Create a lookup table linked list in case a lookup table is required.
 *      Initialize the device support package.
 *
 *      Request the device support to re-process the record after a
 *      short delay.  This prevents any file I/O from blocking the system.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long initRecord 
(
    DEVICE_CONTROL_RECORD *pdr,     /* deviceControl record structure       */
    int pass                        /* initialization phase flag            */
)
{
    DEVICE_CONTROL_PRIVATE *pPriv;  /* internal control structure           */
    DEVICE_CONTROL_DSET *pdset;     /* device support layer function struct */
    long status = 0;                /* function return status               */
    ELLLIST *x = NULL;              /* lookup table entry linked list       */

    DEBUG(DDR_MSG_MIN, "<%ld> %s:initRecord pass = %d\n", pass);


    /*
     *  The first initialization phase is ignored, all processing takes
     *  place on the second phase after all of the other records have been
     *  set up.
     */

    if (pass == 0)
    {
        return (0);
    }


    /*
     *  A device support function access structure must have been
     *  defined for this record for the record to work.
     */

    if (!(pdset = (DEVICE_CONTROL_DSET *) (pdr->dset)))
    {
        recGblRecordError(S_dev_noDSET, (void *) pdr,
                          "deviceControl: initRec");
        return (S_dev_noDSET);
    }


    /*
     *  Ensure that interface functions
     *  have been defined for the support this record requires.
     */

    if ((pdset->number < 9) ||
        (pdset->configureDrive == NULL) ||
        (pdset->controlPower == NULL) ||
        (pdset->controlMotion == NULL) ||
        (pdset->setDelay == NULL) ||
        (pdset->initDeviceSupport == NULL) ||
        (pdset->setPosition == NULL))
    {
        recGblRecordError(S_dev_missingSup, (void *) pdr,
                          "deviceControl: initRec");
        return (S_dev_missingSup);
    }


    /*
     *  Set up all of the input and output database links.
     */

    status = initLinks (pdr);
    if (status) return (status);


    /*
     *  Create an internal control structure for this instance of the
     *  deviceControl record and store that structure address in the
     *  record's device private (DPVT) field.  This structure is also
     *  used to pass parameters to and from the device support code.
     */

    pPriv = malloc (sizeof(DEVICE_CONTROL_PRIVATE));
    if (!pPriv) {
        status = -1;
        recGblRecordError (status, pdr, __FILE__
                          ":no room for device private");
        return status;
    }

    pdr->dpvt = (void *) pPriv;


    /*
     *  Create a mutual exclusion semaphore to protect the control structure
     *  from corruption during asynchronous access.
     */
   
    pPriv->mutexSem = NULL;
    if ( ( pPriv->mutexSem = semMCreate (SEM_Q_PRIORITY | SEM_INVERSION_SAFE) )
        == NULL )
    {
        status = -1;
        recGblRecordError (status, pdr, __FILE__ ":mutexSem alloc failed");
        return status;
    }


    /*
     *  Initialize the internal control structure for this device
     */

    semTake (pPriv->mutexSem, WAIT_FOREVER);
    *pPriv->errorMessage = '\0';
    *pPriv->actionErrMess = '\0';
    *pPriv->rejectErrMess = '\0';
    pPriv->mode = pdr->mode;
    pPriv->simulation = DDR_SIM_NONE;
    pPriv->debug = DDR_DBUG_NONE;
    pPriv->simmChange = 0;
    pPriv->simmHpvl = FALSE;
    pPriv->position = 0;
    pPriv->homeOffset = 0;
    pPriv->target = 0;
    pPriv->fault = FALSE;
    pPriv->faultChange = FALSE;
    pPriv->callback = FALSE;
    pPriv->timeout = FALSE;
    pPriv->encoder = 0;
    pPriv->badRead = FALSE;
    pPriv->encoderDeadband = 1;
    pPriv->initializing = TRUE;
    pPriv->status = 0;
    pPriv->moving = FALSE;
    pPriv->move_while_busy = FALSE;
    pPriv->backlashMotion = FALSE;
    pPriv->checkLimits = 1;
    pPriv->highLimit = 0;
    pPriv->lowLimit = 0;
    pPriv->homeSwitch = 0;
    pPriv->index = pdr->ialg;
    pPriv->stalled_times= 0;
    pPriv->rejectAck = TRUE;
    pPriv->encoderResolution = 0;
    pPriv->motorResolution = 0;
    semGive (pPriv->mutexSem);


    /*
     * Initialize the record database fields for this device
     */

    pdr->busy = DDR_CMD_IDLE;
    pdr->hpvl = FALSE;
    pdr->udf = TRUE;
    pdr->vers = VERSION;
    pdr->mmap = 0;
    pdr->lmap = 0;
    pdr->pp = FALSE;
    pdr->osta = DDR_INIT;
    pdr->mip = DDR_MIP_STOPPED;
    pdr->lswa = 0;
    MONITOR(RECORD_LSWA);
    pdr->hswa = 0;
    MONITOR(RECORD_HSWA);


    /*
     *  Create and initialize the lookup table linked list.   Store the
     *  address of the list in the record's lookup table head pointer (LTHP)
     *  field.
     */

    x = (ELLLIST *) malloc (sizeof(ELLLIST));
    if ( x == NULL )
    {
        status = -1;
        recGblRecordError (status, pdr, __FILE__ ":no room for Link Ptr");
        return status;
    }

    pdr->lthp = (void *) x; 
    ellInit (pdr->lthp);


    /*
     * Then initialize device support via the initialization function
     */

    if (pdset->initDeviceSupport)
    {
        DEBUG(DDR_MSG_MIN, "<%ld> %s:initRecord:call initDeviceSupport%c\n",
              ' ');
        status = (*pdset->initDeviceSupport) (pdr);
        DEBUG(DDR_MSG_MIN, "<%ld> %s:initRecord:initDeviceSupport returns %ld\n",
              status);
        if (status)
        {
            SET_ERR_MSG( pPriv->errorMessage);
            return status;
        }
    }


    /*
     *  Set up a callback.   After the holdoff interval the record will
     *  automatically re-process.  Since the operating state (OSTA) field
     *  has been set to initializing then the initState function will
     *  will be called at this time to complete the record initialization.
     *  This is done to allow the system initialization to complete before
     *  doing any file I/O which seems to block the system.
     */

    (*pdset->setDelay) (pPriv, DDR_INIT_HOLDOFF);

    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * initState
 *
 * INVOCATION:
 * status = initState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Initialize (or re-initialize) the state machine
 *
 * DESCRIPTION:
 * This function is either called from the initRecord function at system
 * startup or when an INIT mode command is executed.   It re-initializes
 * the record using the following algorithm:
 * 
 *      Ensure that the final index velocity (FIVL) field is valid.
 *      Re-initialize the internal control structure.
 *      Re-initialize the record status and control fields.
 *      If using a brake, apply it now.
 *      If using auxiliary power bit ensure that power is off.
 *
 *      If a lookup table file has been specified:
 *      {
 *          If the file can be opened:
 *          {
 *              Skip blank and comment lines.
 *              Add data lines to the lookup table linked list.
 *          }
 *      }
 *
 *      Exit to the idle state via a 0.1 second callback.
 * 
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long initState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */ 
        *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    TRANSLATION_NODE *pNode;        /* lookup table node                    */
    FILE    *fp;                    /* lookup table file pointer            */
    char    scratchBuf[256];        /* character scratch buffer             */
    long    status = 0;             /* function return status               */


    DEBUG(DDR_MSG_FULL, "<%ld> %s:Entering Init state%c\n", ' ');

   
    /*
     *  If the final index velocity has not been defined then use the
     *  maximum safe indexing velocity.
     */

    if (pdr->fivl <= 0.0)
    {
        pdr->fivl = DDR_MAX_INDEX_VEL / pdr->mres;
    }
 

    /*
     * Re-initialize internal control structure
     */

    semTake (pPriv->mutexSem, WAIT_FOREVER);
    pPriv->initializing = TRUE;
    pPriv->velocity = pdr->velo * pdr->mres;
    pPriv->indexVelocity = pdr->fivl * pdr->mres;
    pPriv->acceleration = pdr->accl * pdr->mres;
    pPriv->homeOffset = pdr->hpo * pdr->mres;
    pPriv->motorResolution = pdr->mres;
    pPriv->encoderResolution = pdr->eres;

    pPriv->index = pdr->ialg;
    pPriv->encoderDeadband = pdr->edbd;
    pPriv->status = 0;

    pPriv->rejectAck = TRUE;

    *pPriv->actionErrMess = '\0';
    *pPriv->rejectErrMess = '\0';

    semGive (pPriv->mutexSem);   


    /*
     *  Update the debugging and simulation states
     */

    status = readInputLinks( pdr, FALSE );
    if (status)
    {
        return (status);
    }
    DEBUG(DDR_MSG_MAX, "<%ld> %s:initState, simulation=%d\n",
          pPriv->simulation );
  

    /*
     *  If we are controlling a motor brake (brake timeout is set)
     *  then apply the brake by setting the brake control bit and 
     *  requesting a write to the brake control output link.
     *  We're not requesting a callback after a brake delay -
     *  should we do that?
     */

    if( pdr->btmo )
    { 
        pdr->brk = TRUE;
        MONITOR(RECORD_BRK);
        if (!pPriv->simulation)
        {
            TRIGGER(RECORD_BRK);
        }
    }

    pdr->udf = TRUE;
    pdr->vers = VERSION;
    pdr->pp = FALSE;

    *pdr->mess = '\0';
    MONITOR(RECORD_MESS);
    pdr->vals[0] = '\0';
    MONITOR(RECORD_VALS);

    if (pdr->mip == DDR_MIP_ERROR)
    {
        pdr->mip = DDR_MIP_STOPPED;
        MONITOR(RECORD_MIP);
    }

    /*
     *  If we are using the auxiliary power bit to control the motor
     *  power then call the device support power control function to
     *  ensure that the power is off.
     */
   
    if (pdr->uapb)
    {
        status = (*pdset->controlPower) (pPriv, FALSE);
        if (status)
        {
            DEBUG(DDR_MSG_ERROR, "<%ld> %s:initState: Power control fault%c\n",
                  ' ');
            SET_ERR_MSG( pPriv->errorMessage);
            return abortingState (pdr);
        }
    }


    /*
     *  Clear any existing lookup table entries.
     */

    ellFree(pdr->lthp);


    /*
     *  If a file name and directory have been specified for a lookup
     *  table then load the table from the given file.
     */

    if (strlen(pdr->tfil) && strlen( pdr->tdir ))
    {          
        DEBUG(DDR_MSG_FULL, 
              "<%ld> %s:initState:opening file: %s\n", pdr->tfil);

        /*
         *  Create a full path name from the directory and file fields
         *  and then try to open the file.  If the file opened successfully
         *  then read the contents. 
         */

        sprintf (scratchBuf, "%s/%s", pdr->tdir, pdr->tfil);
        if ((fp = fopen(scratchBuf, "r")) != NULL)
        {

            pNode = (TRANSLATION_NODE *) ellFirst (pdr->lthp);
       
       
            /*
             * Process one entry at a time
             */
                         
            while (fgets(scratchBuf, sizeof( scratchBuf ), fp ) != NULL )
            {

                DEBUG(DDR_MSG_FULL, 
                      "<%ld> %s:initState:got a line: %c\n",' ');

                /*
                 *  Discard comment and blank lines.
                 */

                if (*scratchBuf == '#' || *scratchBuf == '\n' )
                { 
                         continue;
                }


                /*
                 *  Otherwise it is a data line.  Add it to the table
                 */

                else       
                {
                    /*
                     *  Create a new linked list node to hold the lookup
                     *  table entry.
                     */

                    if (!pNode)
                    {
                        if (!(pNode = malloc (sizeof(TRANSLATION_NODE))))
                        {
                            recGblRecordError(-1, (void *) pdr, 
                                              "translation node create fail");
                            break;
                        }
                        ellAdd ((ELLLIST *)pdr->lthp, &pNode->node);     
                    }

   
                    /*
                     *  Read the first three items into the table.  If
                     *  they can not all be converted then we have a
                     *  corrupted file.  Kill the partially built table
                     *  and return an error.
                     */
                
                    if (sscanf(scratchBuf,
                               "%s %lf %ld",
                               pNode->name,
                               &pNode->target,
                               &pNode->index) != 3)            
                    {
                        ellFree(pdr->lthp);
                         DEBUG(DDR_MSG_ERROR,
                               "<%ld> %s:initState: line format error: %s\n",
                               scratchBuf );
                        SET_ERR_MSG("LUT file corrupted");
                        status = -6;
                        break;
                    }

                    /*
                     *  Print the three entries if full debugging is on.
                     */

                     DEBUG(DDR_MSG_FULL,
                           "<%ld> %s:initState: LUT name: %s\n",
                           pNode->name);
                     DEBUG(DDR_MSG_FULL,
                           "<%ld> %s:initState: LUT target: %f\n",
                           pNode->target);
                     DEBUG(DDR_MSG_FULL,
                           "<%ld> %s:initState: LUT index: %ld\n",
                           pNode->index);

                    /*
                     * And point to the next node
                     */
               
                    pNode = (TRANSLATION_NODE *) ellNext (&pNode->node);

                }   /* end of reading data line */                 
 
           }   /* end of reading file data */
       
            fclose (fp);
       
        }   /* end of if file can be opened */
      

        /*
         *  Otherwise the file name is bad....
         */

        else
        {
            DEBUG(DDR_MSG_ERROR, 
                  "<%ld> %s:initState: open %s failed\n", scratchBuf);
            SET_ERR_MSG("LUT file not found");
        }

    }   /* end of if file name and directory are specified */


    /*
     *  All initialization is complete.  Switch to idle state via a 
     *  0.1 second timeout.  This is done to force a callback from the
     *  device support code which will update all of the motor status
     *  information.
     */

    semTake (pPriv->mutexSem, WAIT_FOREVER);
    pPriv->initializing = FALSE;
    semGive (pPriv->mutexSem);

    pdr->osta = DDR_IDLE;          
    (*pdset->setDelay) (pPriv, 1);
    return ( status );
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * lockingState
 *
 * INVOCATION:
 * status = lockingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Engage the brake or locking mechanism
 *
 * DESCRIPTION:
 * This function is called by moving or holding states when a
 * motion is complete and the motor is to be shut down.  It applies
 * the brake via the following algorithm:
 *
 *      If we were called from another state (entering locking state):
 *      {
 *          Switch state machine to locking state.
 *
 *          If we can control the brake:
 *          {
 *              Set the break control bit.
 *              Request that the brake control link be written.
 *              Set a callback timeout so that we can check the brake.
 *              Return and wait for the record to be processed again.
 *          }
 *
 *          Otherwise there is nothing to do so exit to the depowering state.
 *      }
 *
 *      Abort the operation immediately if we see motion startup.
 *
 *      If we are using the brake status bit and the brake is now off:
 *      {
 *              Cancel the callback timeout.
 *              Exit to the depowering state.
 *      }
 *
 *      If this is the callback requested above:
 *      {
 *          If we are not using the brake status bit:
 *          {
 *              Exit to the depowering state.
 *          }
 *          If we are in simulation mode:
 *          {
 *              Set the brake status bit to make it look like brake is on.
 *              Exit to the depowering state.
 *          }
 *          Otherwise there must be a problem because brake is still off:
 *          {
 *              Generate an error message.
 *              Exit to the aborting state.
 *          }
 *      } 
 *
 *      Remain in locking state until the record is processed again.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long lockingState 
(
    DEVICE_CONTROL_RECORD *pdr      /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */ 
        *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* dev support function structure       */
        *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long status = 0;                /* function return status   */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:lockingState:entry%c\n", ' ');

    
    /*
     *  If we are moving into locking state (first pass) from some other
     *  state then apply the brake.
     */
    
    if (pdr->osta != DDR_ENGAGING)
    {
        DEBUG(DDR_MSG_FULL,
              "<%ld> %s: lockingState: from osta:%d\n", pdr->osta);

        /*
         *  Switch the state machine into locking state and indicate
         *  that the move has finished and the device is shutting down.
         */

        pdr->osta = DDR_ENGAGING;

        pdr->mip = DDR_MIP_ENDING;
        MONITOR(RECORD_MIP);


        /*
         *  If we are controlling a brake (brake timeout has been set)
         *  then set the brake control bit and request a callback after
         *  the brake engaging time so that we can check the brake status.
         */

        if( pdr->btmo )
        {         
            pdr->brk = TRUE;
            MONITOR(RECORD_BRK);
                    
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_BRK);
            }
           
            (*pdset->setDelay) (pPriv, pdr->btmo);
            return status;                      
        }

        /*
         *  Otherwise there is no brake to control so exit the locking
         *  state and move on to the depowering state.
         */

        else
        {
            return depoweringState (pdr);
        }

    }

    /*
     *  Abort the operation immediately if the moving flag suddenly comes alive.
     */

    if ( pPriv->moving )
    {
        SET_ERR_MSG("Unexpected motion while braking");
        DEBUG(DDR_MSG_ERROR, 
              "<%ld> %s:lockingState:moving flag set after brakes applied%c\n", ' ');
        return abortingState (pdr);
    }

    /*
     *  If we have brake status feedback (use brake status bit set) and
     *  the brake status indicates that the brake is now on then
     *  clear the callback timer and exit the locking state and go on to the
     *  depowering state.
     */
    
    if (pdr->ubsb && pdr->bsta)
    {        
        (*pdset->setDelay) (pPriv, 0);
        status = depoweringState (pdr);
        return (status);
    }


    /*
     *  If the timeout flag is set then this is the callback requested above.
     */
    
    if (pPriv->timeout)
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);


        /*
         *  If we are not using the brake status bit then this was just
         *  a delay to allow the brake to enegage so exit the locking
         *  state and go on to the depowering state.
         */

        if( !pdr->ubsb )
        {
            status = depoweringState (pdr);
        }


        /*
         *  If we are in simulation mode fake the brake status bit by
         *  setting the input field directly.
         */

        else if (pPriv->simulation)
        {
            pdr->bsta = TRUE;
            MONITOR(RECORD_BSTA);
            status = depoweringState (pdr);
        }

        /*
         *  Othewise the bit should have been set but it is not.
         *  Exit immediately for the abortingState...
         */

        else
        {
            SET_ERR_MSG( "brake did not engage in time");
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:lockingState: brake did not engage in time%c\n",
                  ' ');
            status = abortingState (pdr);
        }
    }


    /*
     *  Otherwise just hang out until something more interesting happens
     */

    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * monitor
 *
 * INVOCATION:
 * monitor (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)   deviceControl record structure.
 *
 * FUNCTION VALUE:
 * none.
 *
 * PURPOSE:
 * Raise monitors on selected EPICS record fields
 *
 * DESCRIPTION:
 * Raise monitors on the val field if the field has changed by more than
 *   the monitor or alarm deadbands.
 * Raise monitors on all input fields that have been marked as changed
 * Raise monitors on all output fields that have been marked as changed.
 * 
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void monitor
(
    DEVICE_CONTROL_RECORD    *pdr        /* deiveControl record structure   */
)
{
    unsigned short  monitorMask;        /* monitor modifier mask            */
   
    DEBUG(DDR_MSG_MAX, "<%ld> %s:monitor:entry%c\n", ' ');


    /*
     *  Start by clearing the monitor mask
     */

    monitorMask = recGblResetAlarms(pdr);


    /*
     *  If the value field has changed by more then the monitor or alarm
     *  deadbands then raise the appropriate monitor on the VAL field.
     */
    
    if (fabs(pdr->val - pdr->mlst) > pdr->mdel)
    {
        monitorMask |= DBE_VALUE;
        pdr->mlst = pdr->val;
    }

    if (fabs(pdr->val - pdr->alst) > pdr->adel)
    {
        monitorMask |= DBE_LOG;
        pdr->alst = pdr->val;
    }

    if (monitorMask)
    {
        db_post_events(pdr, &pdr->val, monitorMask);
    }


    /*
     *  Raise monitors on the input fields if the appropriate bits in the
     *  monitor mask word have been set.  Use the MONITORED macro to
     *  test the bit for each field.
     */

    if (MONITORED(RECORD_DIR))  db_post_events(pdr, &pdr->dir,  DBE_VALUE);
    if (MONITORED(RECORD_MODE)) db_post_events(pdr, &pdr->mode, DBE_VALUE);
    if (MONITORED(RECORD_VELO)) db_post_events(pdr, &pdr->velo, DBE_VALUE);
    if (MONITORED(RECORD_ACCL)) db_post_events(pdr, &pdr->accl, DBE_VALUE);
    if (MONITORED(RECORD_IALG)) db_post_events(pdr, &pdr->ialg, DBE_VALUE);
    if (MONITORED(RECORD_PSTA)) db_post_events(pdr, &pdr->psta, DBE_VALUE);
    if (MONITORED(RECORD_BSTA)) db_post_events(pdr, &pdr->bsta, DBE_VALUE);
    if (MONITORED(RECORD_FLT))  db_post_events(pdr, &pdr->flt,  DBE_VALUE);
    if (MONITORED(RECORD_DBUG)) db_post_events(pdr, &pdr->dbug, DBE_VALUE);
    if (MONITORED(RECORD_SIMM)) db_post_events(pdr, &pdr->simm, DBE_VALUE);
    if (MONITORED(RECORD_MDBD)) db_post_events(pdr, &pdr->mdbd, DBE_VALUE);
    if (MONITORED(RECORD_VALS)) db_post_events(pdr, &pdr->vals, DBE_VALUE);


    /*
     *  Raise monitors on the output fields if the appropriate bits in the
     *  monitor mask word have been set.  Use the MONITORED macro to
     *  test the bit for each field.  Set the MESS and BUSY fields last
     *  ensure that everything else has been seen before the busy field
     *  changes.
     */

    if (MONITORED(RECORD_ACK))  db_post_events(pdr, &pdr->ack, DBE_VALUE);
    if (MONITORED(RECORD_PWR))  db_post_events(pdr, &pdr->pwr, DBE_VALUE);
    if (MONITORED(RECORD_BRK))  db_post_events(pdr, &pdr->brk, DBE_VALUE);

    if (MONITORED(RECORD_MIP))  db_post_events(pdr, &pdr->mip, DBE_VALUE);
    if (MONITORED(RECORD_MPOS)) db_post_events(pdr, &pdr->mpos, DBE_VALUE);
    if (MONITORED(RECORD_RPOS)) db_post_events(pdr, &pdr->rpos, DBE_VALUE);
    if (MONITORED(RECORD_RRBV)) db_post_events(pdr, &pdr->rrbv, DBE_VALUE);
    if (MONITORED(RECORD_RVEL)) db_post_events(pdr, &pdr->rvel, DBE_VALUE);
    if (MONITORED(RECORD_RENC)) db_post_events(pdr, &pdr->renc, DBE_VALUE);
    if (MONITORED(RECORD_MSTA)) db_post_events(pdr, &pdr->msta, DBE_VALUE);
    if (MONITORED(RECORD_OSTA)) db_post_events(pdr, &pdr->osta, DBE_VALUE);
    if (MONITORED(RECORD_LVIO)) db_post_events(pdr, &pdr->lvio, DBE_VALUE);
    if (MONITORED(RECORD_LSWA)) db_post_events(pdr, &pdr->lswa, DBE_VALUE);
    if (MONITORED(RECORD_HPVL)) db_post_events(pdr, &pdr->hpvl, DBE_VALUE);
    if (MONITORED(RECORD_HSWA)) db_post_events(pdr, &pdr->hswa, DBE_VALUE);
   
    if (MONITORED(RECORD_MESS)) db_post_events(pdr, &pdr->mess, DBE_VALUE);
    if (MONITORED(RECORD_BUSY)) db_post_events(pdr, &pdr->busy, DBE_VALUE);


    /*
     *  Clear the monitor bitmask to acknowledge that monitors were raised.
     */

    UNMONITOR_ALL;
   
    return;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * movingState
 *
 * INVOCATION:
 * status = movingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)   deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Monitor motor while moving
 *
 * DESCRIPTION:
 * Monitor a motion in progress via the following algorithm:
 *
 *      If we were called from another state (entering moving state):
 *      {
 *          Switch state machine to moving state.
 *          Change device state output to moving.
 *          Calculate timeout based on distance, velocity & acceleration.
 *          Set motion timeout.
 *          Return and wait for record to be processed again.
 *      }
 *
 *      If new motion parameters have been received while moving:
 *      {
 *          Clear move_while_busy and backlashMotion flags
 *          Call controlMotion to STOP current motion
 *          Exit to starting state to begin next motion.
 *      }
 *
 *      If the motor has stopped moving:
 *      {
 *          If we hit a limit switch:
 *          {
 *              If indexing on the limit switch just hit (good):
 *              {
 *                  Cancel motion timeout timer.
 *                  Set the position counter to the homeOffset.
 *                  Set the home position valid flag.
 *                  Exit to the locking state.
 *              }
 *              Else this is a device fault (bad):
 *              {
 *                  Generate an error message.
 *                  Exit to aborting state.
 *              }
 *          }
 *          Else device thinks it reached the target position:
 *          {
 *              Cancel motion timeout timer.
 *              If not indexing
 *              {
 *                  If target does not agree with the position (bad):
 *                  {
 *                      Generate an error message.
 *                      Exit to aborting state.
 *                  }
 *                  If using encoders don't agree with the position (bad):
 *                  {
 *                      Generate an error message.
 *                      Exit to aborting state.
 *                  }
 *              }
 *              Else must have successfully finished indexing (good)
 *              {
 *                  Set the position counter to the home offset.
 *                  Set the home position valid flag.
 *                  Exit to the locking state.
 *              }
 *
 *              If this is the first part of a two-part backlash comp move:
 *              {
 *                  Set the backlash motion flag.
 *                  Exit to starting state to execute the second half.
 *              }
 *              Else everything was OK so we are done:
 *              {
 *                  Clear backlash motion flag
 *                  If tracking, exit to holding state
 *                  Else exit to locking state.
 *              }
 *          }
 *      }
 *
 *      If the STOP command was received exit to the stopping state.
 *
 *      If using encoders and the readings were the same 3x in a row:
 *      {
 *          Motor has stalled so generate an error message.
 *          Exit to aborting state.
 *      }
 *
 *      If using power status bit and power is off then we hit an interlock:
 *      {
 *          Generate an error message.
 *          Exit to aborting state.
 *      }
 *
 *      If the timeout flag is set then the motion timed out:
 *      {
 *          Generate an error message.
 *          Exit to aborting state.
 *      }
 *
 *      Remain in moving state until the record is processed again.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * Doesn't check to see if calculateTimeout() returns 0.
 *-
 ************************************************************************
 */

static long movingState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
            *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long    timeout;                /* motion timeout                       */
    long    status = 0;             /* function return status               */


    DEBUG(DDR_MSG_MAX, "<%ld> %s:movingState:entry%c\n", ' ');

    /*
     *  If entering this state from another state (first pass) then
     *  set the motion timeout.
     */
    
    if (pdr->osta != DDR_MOVING)
    {
        DEBUG(DDR_MSG_FULL, 
              "<%ld> %s:movingState: from osta:%d\n", pdr->osta);

        /*
         *  Switch the state machine into moving state and indicate
         *  that startup has completed and that the motor is moving.
         */

        pdr->osta = DDR_MOVING;
        pdr->mip = DDR_MIP_MOVING;
        MONITOR(RECORD_MIP);


        /*
         *  If we are in indexing the device then calculate the timeout for
         *  the longest possible move.
         */
       
        if (pdr->mode == DDR_MODE_INDEX)
        {
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:movingState: calculate timeout for index%c\n", ' ');
            timeout =  calculateTimeout (pdr->pllm, 
                                         pdr->phlm, 
                                         pdr->velo, 
                                         pdr->accl);

            /*
             * Add an overhead time to account for the extra short moves made
             * when indexing on a home switch.
             */

            timeout = timeout + DDR_INDEX_OVERHEAD_TIME;
        }


        /*
         *  Otherwise it is a normal motion so calculate the timeout based on
         *  the distance to move and the velocity of motion.
         */

        else
        {
            /*
             *  If this is a normal point-to-point motion then calcualte
             *  timeout based on the distance to move and the normal
             *  motion velocity.
             */

            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:movingState: calculate timeout for a non-index move%c\n", ' ');
            timeout =  calculateTimeout (pPriv->position, 
                                         pPriv->target, 
                                         pdr->velo, 
                                         pdr->accl);
        }


        /*
         *  Set the callback timer to the calculated timeout interval
         *  plus the starting overhead (both in units of 0.1 seconds).
         */

        timeout = timeout + DDR_MOTION_OVERHEAD_TIME;

        DEBUG(DDR_MSG_FULL, "<%ld> %s:Motor timeout=%ld\n", timeout);
        (*pdset->setDelay) (pPriv, timeout);

        return ( status );                     
    }


    /*
     *  If new motion parameters have been received while still moving
     *  then return to the starting state to re-load them into the 
     *  motion control card.
     */
    
    if ( pPriv->move_while_busy )
    {
        DEBUG(DDR_MSG_FULL,
               "<%ld> %s: movingState: attribute update, reload%c\n", ' ');

        pdr->dir = DDR_DIR_CHECK;
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->move_while_busy = FALSE;
        pPriv->backlashMotion = FALSE;
        semGive (pPriv->mutexSem);
        status = (*pdset->controlMotion) (pPriv, DDR_MOVE_STOP);
        if (status)
        {
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:movingState: controlMotion call failed%c\n",
                  ' ');
            SET_ERR_MSG( pPriv->errorMessage);
            return abortingState (pdr);
        }
        (*pdset->setDelay) (pPriv, 0);   /* cancel the timeout */

        return ( startingState (pdr) );
    }

    /*
     *  If the motor has stopped moving then check to see why...
     */
        
    if (!pPriv->moving)
    {

        /*
         *  If we hit a soft limit switch then check to see if this
         *  is a bad thing.
         */

        if (pdr->lswa)
        {

            /*
             *  If we are indexing on the soft limit switch then
             *  this is not a bad thing.   Cancel the motion timeout,
             *  set the position counter to zero and the home position
             *  valid flag then shut down motion by going on to the
             *  locking state.
             */

            if ((pPriv->index == DDR_INDEX_LLSW && pPriv->lowLimit) ||
                (pPriv->index == DDR_INDEX_ULSW && pPriv->highLimit))
            {

                (*pdset->setDelay) (pPriv, 0);
                if (pPriv->homeOffset)
                {
                    status = (*pdset->setPosition) (pPriv, pPriv->homeOffset);    
                    if (status)
                    {
                        DEBUG(DDR_MSG_ERROR,
                            "<%ld> %s:movingState: setPosition call failed%c\n",' ');
                        SET_ERR_MSG( pPriv->errorMessage);
                        return abortingState (pdr);
                    }
                }
                DEBUG(DDR_MSG_FULL,"<%ld> %s:movingState: Successful index, setting current position to: %.2f\n", pPriv->homeOffset / pdr->mres );
                pdr->hpvl = TRUE;
                MONITOR(RECORD_HPVL);
                return ( lockingState(pdr) );
            }


            /*
             *  Otherwise it is a bad thing.  Generate an error message
             *  then abort the motion immediately by going to the 
             *  aborting state.
             */

            else
            {
                SET_ERR_MSG( "Device hit a soft limit");
                DEBUG(DDR_MSG_ERROR,
                      "<%ld> %s: movingState: device hit a soft limit%c\n", ' ');
                return( abortingState(pdr) );
            }

        }   /* End of motion stopped by a limit switch */

        /*  Otherwise the motion completed successfully.  Clean up the details. */

        else
        {
            /* 
             *  Cancel the motion timeout.
             */

            (*pdset->setDelay) (pPriv, 0);


            /* 
             *  If this is not an index motion then confirm 
             *  that we got where we were supposed to go. 
             */

            if (pPriv->mode != DDR_MODE_INDEX)
            {

                /*
                 *  Compare target and current position (this checks for the case
                 *  where the motor is stopped by the motor controller after an 
                 *  apparent limit but the omsScanTask did not read a limit).
                 */

                if ( pPriv->target != pPriv->position )
                {
                    SET_ERR_MSG("Motor didn't reach target");
                    DEBUG(DDR_MSG_ERROR, "<%ld> %s:movingState:motor didn't reach target (limit switch bounce?)%c\n", ' ');
                    return abortingState(pdr);
                }

                /* 
                 *  If we are using encoders then make sure the encoder count 
                 *  agrees with the motor count.
                 */

                if (pdr->ueip)
                {
                    DEBUG(DDR_MSG_FULL, "<%ld> %s:movingState: encoder check, missed by:%d count(s)\n", abs((long)(((double)pdr->rrbv) *  pdr->eres/pdr->mres) - pdr->renc));

                    /*
                     *  If the position returned by the encoders is out by more
                     *  than the encoder deadband value then generate an error
                     *  message and abort the motion immediately.
                     */

                    if (abs((long)(((double) pdr->rrbv) * pdr->eres/pdr->mres) - 
                            pdr->renc) > pdr->edbd)
                    {
                        SET_ERR_MSG("Encoder doesn't agree with motor count");
                        DEBUG(DDR_MSG_ERROR, "<%ld> %s:movingState: encoder & position disagree by more than encoder deadband (EDBD=%ld)\n", pdr->edbd);
                        return abortingState(pdr);
                    }
                }
            }

            /*
             *  Must be an indexing sequence finishing.  
             *  Unless IALG is set to NONE, define current position with 
             *  home offset.
             *  Set home position valid (HPVL)
             */

            else
            {
                if ((pPriv->homeOffset != 0) && (pPriv->index != DDR_INDEX_NONE))
                {
                    status = (*pdset->setPosition) (pPriv, pPriv->homeOffset);    
                    if (status)
                    {
                        DEBUG(DDR_MSG_ERROR,
                            "<%ld> %s:movingState: setPosition call failed%c\n",' ');
                        SET_ERR_MSG( pPriv->errorMessage);
                        return abortingState (pdr);
                    }

                    DEBUG(DDR_MSG_FULL,"<%ld> %s:movingState: Successful index, setting current position to: %.2f\n", pPriv->homeOffset / pdr->mres );
                }
                if (!pdr->hpvl)
                {
                    pdr->hpvl = TRUE;
                    MONITOR(RECORD_HPVL);
                }
            }
            /*
             *  If this is the end of the approach portion of a backlash
             *  corrected motion then set the backlash motion flag and 
             *  go back to starting state to trigger the short anti-backlash
             *  motion.
             */

            if ( (pPriv->mode != DDR_MODE_INDEX) && 
                 (pdr->blco != 0 )               && 
                 (pPriv->backlashMotion == TRUE)      )
            {
                return startingState (pdr);
            }

            /*
             *  Otherwise the device managed to get where it was supposed
             *  to go.   If we are in TRACK mode then switch to the
             *  holding state to keep the device alive until a new target
             *  is recieved, otherwise switch to locking state to shut 
             *  the device down.
             */

            else
            {
                semTake (pPriv->mutexSem, WAIT_FOREVER);
                pPriv->backlashMotion = FALSE;
                semGive (pPriv->mutexSem);
                return ((pPriv->mode == DDR_MODE_TRACK) ? holdingState (pdr) :
                        lockingState (pdr) );
            }
        }   /* end of motion stopped normally */
    }   /* end of motor stopped moving */


    /*
     *  If a STOP directive was received then bring the device to a controlled 
     *  stop by switching to the stopping state.
     */
    
    if (pdr->dir == DDR_DIR_STOP)
    {
        return ( stoppingState (pdr) );
    }


    /*
     *  If we are using the encoder then it is possible to check for
     *  motor stall.   If the encoder has not changed since the last
     *  time the record was processed (a monitor is raised in the process 
     *  function if the encoder has changed) check to see if this means
     *  a motor stall.
     */

    if (pdr->ueip && !(MONITORED(RECORD_RENC)))
    {
        /*
         *  If we get two identical readings in a row while the motor
         *  thinks it is moving then the device has stalled.
         *  Abort the motion immediately.
         */
        
        if ( pPriv->stalled_times > 0)
        {
            SET_ERR_MSG( "Motor stalled");
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:movingState: motor stalled (encoder values not changing)%c\n", ' ');
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->stalled_times = 0;
            semGive (pPriv->mutexSem);
            return abortingState (pdr);
        }


        /*
         *  Otherwise this is the first identical reading.  Flag it
         *  so that if the encoder still has not changed when the record next
         *  processes we can detect a stall.
         */

        else
        {     
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->stalled_times++ ;
            semGive (pPriv->mutexSem);
        }
    }


    /*
     *  Othewise the encoder reading has changed.  Clear the identical
     *  reading flag in case it was set last time the record processed.
     */

    else
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->stalled_times = 0;
        semGive (pPriv->mutexSem);
    }

    /*
     *  If we can read the motor power state and the power is off then
     *  we have either hit a limit or an interlock was triggered.  Generate
     *  an error message and abort the motion immediately!
     */
         
    if (pdr->upsb && !pdr->psta)
    {        
        SET_ERR_MSG( "Power failed while moving, hard limit?");
        DEBUG(DDR_MSG_ERROR, 
              "<%ld> %s:movingState:Power failed while moving, hard limit?%c\n",
              ' ');
        return abortingState (pdr);
    }

    /*
     *  If the timeout flag is set then the motion timeout set when we 
     *  entered the moving state has expired.  This is not good news
     *  since it indicates that something has gone wrong with the
     *  device.  Abort the motion immediately.
     */
    
    if (pPriv->timeout)
    {
        SET_ERR_MSG( "Motion timeout!!!");
        DEBUG(DDR_MSG_ERROR,
              "<%ld> %s:movingState: Motor didn't arrive at target in time, moving=%d \n",
              pPriv->moving );
        return ( abortingState (pdr) );
    }


    /*
     * Otherwise just hang out until something more interesting happens
     */ 
              
   return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * poweringState
 *
 * INVOCATION:
 * status = poweringState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)   deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Enable motor power
 *
 * DESCRIPTION:
 * This function is called from the idle state when a new motion command
 * has been recieved.  It will turn on the motor power via the following
 * algorithm:
 *
 *      If we were called from another state (entering powering state):
 *      {
 *          Switch state machine to powering state.
 *
 *          If we can control the power:
 *          {
 *              Set power control bit.
 *              Request that the power control link be written.
 *
 *              If controlling power via device support:
 *              {
 *                  Call device suppport power control function.
 *              }
 *
 *              Set a callback timeout so that we can check the power.
 *              Return and wait for the record to be processed again.
 *          }
 *
 *          Otherwise there is nothing to do so exit to the unlocking state.
 *      }
 *
 *      If the stop directive has been received:
 *      {
 *          exit to the depowering state
 *      }

 *      If we are using the power status bit:
 *      {
 *          If the power is now on:
 *          {
 *              Cancel the callback timeout.
 *              Exit to the unlocking state.
 *           }
 *      }
 *
 *      If this is the callback requested above:
 *      {
 *          If we are not using the power status bit:
 *          {
 *              Exit to the unlocking state.
 *          }
 *          If we are in simulation mode:
 *          {
 *              Set the power status bit to make it look like power is on.
 *              Exit to the unlocking state.
 *          }
 *          Otherwise there must be a problem because power is still off:
 *          {
 *              Generate an error message.
 *              Exit to the aborting state.
 *          }
 *      } 
 *
 *      Remain in powering state until the record is processed again.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long poweringState
(
    DEVICE_CONTROL_RECORD    *pdr        /* deviceControl record structure  */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
            *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long    status = 0;             /* function return status               */


    DEBUG(DDR_MSG_MAX, "<%ld> %s:poweringState:entry%c\n", ' ');


    /*
     *  If entering this state from another state (first pass through
     *  the function) start the motion sequence.
     */
    
    if (pdr->osta != DDR_POWERING)
    {
        DEBUG(DDR_MSG_FULL, "<%ld> %s: poweringState: from osta:%d\n",
                             pdr->osta );

        /*
         *  Switch state machine into powering state and set the device
         *  status output to indicate that a motion is starting.
         */

        pdr->osta = DDR_POWERING;
                        
        pdr->mip = DDR_MIP_BEGINNING;
        MONITOR(RECORD_MIP);

        /*
         * If we are controlling the motor power (power timeout is set)
         * then turn on the motor power.
         */

        if ( pdr->ptmo )
        {        
            /*
             *  Set the power control output field and request that the
             *  associated output link be triggered when processing finishes.
             */

            pdr->pwr = TRUE;
            MONITOR(RECORD_PWR);
                    
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_PWR);
            }           


            /*
             *  If we are also controlling the power via device support (use
             *  auxiliary power bit field is set) then call device support to
             *  turn the power on that way as well.
             */
          
            if ( pdr->uapb )
            {
                status = (*pdset->controlPower) (pPriv, TRUE);
                if (status)
                {
                    DEBUG(DDR_MSG_ERROR, 
                          "<%ld> %s:poweringState:Power control fault%c\n",' ');
                    SET_ERR_MSG( pPriv->errorMessage);
                    return abortingState (pdr);
                }
            }

            /*
             *  Request a callback after the power on timeout interval to
             *  allow us to confirm that the power came on then return
             *  to wait for the callback.
             */
           
            (*pdset->setDelay) (pPriv, (long) pdr->ptmo);
            return status;
        }


        /*
         *  Otherwise we are not controlling the power so proceed directly
         *  to the unlocking state to remove the brake.
         */

        else
        {
            return (unlockingState (pdr));
        }
    }

  
    /*
     *  If the STOP directive was received (operator cancelled the motion)
     *  then proceed directly to the depowering state to shut the power
     *  off again.
     */
    
    if (pdr->dir == DDR_DIR_STOP)
    {
        return( depoweringState (pdr) );
    }


    /*
     *  If the power status bit is on then we were successful.  Cancel the
     *  timeout and proceed directly to the unlocking state to remove
     *  the brake.
     */
    
    if ( pdr->upsb && pdr->psta )
    {        
        (*pdset->setDelay) (pPriv, 0);
        return( unlockingState (pdr) );
    }
   
       
    /*
     *  If the timeout flag is set then this is the callback requested above.
     */
      
    if (pPriv->timeout)
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);


        /*
         *  If we are not using the power status bit (use power status bit
         *  field is not set) then this was just a power-on delay.  Proceed
         *  directly to the unlocking state to remove the brake.
         */
       
        if (!pdr->upsb)        
        {        
            return unlockingState (pdr);
        }


        /*
         *  If we are in simulation mode then fake the power on state by 
         *  setting the power status input field directly and then proceed
         *  to the unlocking state to remove the brake. 
         */

        else if (pPriv->simulation)
        {
            pdr->psta = TRUE;
            MONITOR(RECORD_PSTA);
            return ( unlockingState (pdr) );
        }


        /*
         *  Otherwise the power did not come on for some reason... 
         *  Abort the move immediately.
         */

        else   
        {
            SET_ERR_MSG( "no motor power ... interlocked?");
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s: poweringState: No motor power, interlock?%c\n",
                  ' ');
            return( abortingState (pdr) );
        }
    }
       
    /*
     * Otherwise just hang out until something more interesting happens
     */
    
    return( status );
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * process
 *
 * INVOCATION:
 * status = process (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)    deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Process the device control record
 *
 * DESCRIPTION:
 * Process is the function called by the EPICS code to "process" the record
 * for one of the following reasons:
 *
 * 1) Someone poked our .proc field or any field except FLT that is marked
 *    'process-passive'.  In this case, we read the input links and process 
 *    the current directive.
 *
 * 2) Device support triggered an asynchronous callback.   This is done so 
 *    that the fields changed during device processing are posted as they 
 *    change.  Input links are not read during callback processing.
 *
 * 3) A fault was registered on the FLT field.   This is treated like
 *    a callback in that it was not directly requested by the operator.
 *
 * Processing is controlled by the following algorithm:
 *
 *      If the processing active flag is set:
 *      {
 *          Return immediately without doing anything.
 *      }
 *
 *      Set the processing active flag.
 *
 *      If the fault field has just been written to:
 *      {
 *          If the fault field is set:
 *          {
 *              If there is a command in progress or motion happening:
 *              {
 *                  abort it immediately
 *              }
 *
 *              Otherwise the record is idle:
 *              {
 *                  set the device state to ERROR
 *              }
 *          }
 *
 *          Otherwise the fault field is clear:
 *          { 
 *              clear the internal fault flag
 *          }
 *      }
 *
 *      If this is an internal callback:
 *      {
 *          If there has been a change in home or limit switch state:
 *          {
 *              update the record home or limit switch active field.
 *          }
 *
 *          If raw motor positon has changed:
 *          {
 *              update the raw motor position.
 *              If not using encoders, update engineering unit motor position.
 *              If we are in simulation mode:
 *              {
 *                  fake the encoder reading value
 *              }
 *          }
 *
 *          If we are using the encoders and not simulated and 
 *            raw encoder changed:
 *          {
 *              If the encoder has jumped by an unrealistic amount:
 *              {   
 *                  raise a warning in all debug modes.
 *              }
 *
 *              If change in encoder is greater than the encoder deadband:
 *              {
 *                  clear the insideDeadband flag
 *              }
 *              Raise encoder update monitor unless changing simulation modes
 * 
 *              update the raw encoder value and engineering positions.
 *          }
 *
 *          If motor status has changed:
 *          {
 *              then update the record's msta status field.
 *          }
 *
 *          Determine and process the currently active machine state.
 *      }
 *
 *      Otherwise the record is being processed from the outside:       
 *      {
 *          process the action directive
 *      }
 *
 *      If a command has completed:
 *      {
 *          If an error message has been generated during processing:
 *          {
 *              set the command state (busy) output to ERROR
 *          }
 *          Else If a command was rejected
 *          {
 *              Set the error message
 *          }
 *          Else If leftover rejection message
 *          {
 *              Ignore the message and set to IDLE
 *          }
 *          Otherwise the command completed successfully so:
 *          {
 *              set the command state (busy) output to IDLE
 *          }
 *      }
 *
 *      Write data to all marked output links
 *
 *      Raise alarms
 *      Trigger monitors 
 *      Clear the processing active flag to allow others to process the record
 *      
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long process
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control struct              */ 
        *pPriv = pdr->dpvt;
    long    status = 0;             /* function return status               */


    /*
     *  If the processing active flag is set then someone else is processing
     *  this record.   Return immediately.
     */

    if (pdr->pact)
    {
        DEBUG(DDR_MSG_MAX,
              "<%ld> %s: process: attempt to process with pact=TRUE%c\n", ' ');
        return (0);
    }


    /*
     *  Set the processing active flag to indicate that the record is being
     *  processed.
     */

    DEBUG(DDR_MSG_MAX,
            "<%ld> %s: process: normal record processing begins .....%c\n", ' ');
    pdr->pact = TRUE;


    /*
     *  If the private fault flag has been set then the interlock (FLT) field
     *  has changed state.
     */

    if (pPriv->faultChange)
    {
        DEBUG(DDR_MSG_MAX, "<%ld> %s:process: fault line change%c\n", ' ');

        /*
         *  If the fault field is set then a new fault has been detected.
         */

        if (pdr->flt)
        {
            DEBUG(DDR_MSG_LOG, "<%ld> %s:process: fault detected%c\n", ' ');


            /*
             *  If there is a command in progress or the motor is in motion
             *  (controlled from a different source) abort it immediately.
             */

            if ((pdr->busy == DDR_CMD_BUSY) || (pPriv->moving == TRUE))
            {
                DEBUG(DDR_MSG_ERROR,
                      "<%ld> %s:process: aborting command in progress%c\n",
                      ' ');
                SET_ERR_MSG("Interlock Detected");
                status = abortingState (pdr);
            }

            /*
             *  Otherwise the device was idle when the fault occurred.
             *  This code is accessed on the first command after the 
             *  fault.  Set the device state to ERROR.  Don't write an
             *  error message here though.  A command rejection will
             *  happen later but only if the command requires motion.
             *
             *  Note that because the device was not BUSY when the
             *  fault occurred, the index is NOT lost.
             */

            else
            {
                DEBUG(DDR_MSG_LOG,
                      "<%ld> %s:process: Processing interlock that occurred while IDLE%c\n",
                      ' ');

                pdr->mip = DDR_MIP_ERROR;
                MONITOR(RECORD_MIP);
            }
        }

        /*
         *  If the fault field is clear then the fault has been cleared.
         */

        else
        {
            DEBUG(DDR_MSG_MIN, "<%ld> %s:process: fault cleared%c\n", ' ');
        }

        /*
         *  Clear the internal faultChange flag to indicate that the
         *  change of fault states has been acknowledged.
         */

        DEBUG(DDR_MSG_MAX, "<%ld> %s:process: clear faultChange flag%c\n", ' ');

        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->faultChange = FALSE;
        semGive (pPriv->mutexSem);
    }

    /*
     *  If the callback flag has been set then this is an internal
     *  re-processing request.  Check device support return status and
     *  process the state machine instead of looking for a new command.
     */

    if ( pPriv->callback )
    {       
        DEBUG(DDR_MSG_MAX, "<%ld> %s:process: callback true%c\n", ' ');

        semTake (pPriv->mutexSem, WAIT_FOREVER);

        /*
         *  If there has been a change in limit switch state update the
         *  record limit switch active field.
         */

        if (pdr->lswa != (pPriv->lowLimit || pPriv->highLimit))
        {
            pdr->lswa = (pPriv->lowLimit || pPriv->highLimit);
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:process:Limit switch change, LSWA=%d\n", pdr->lswa );
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:process:lowLimit=%d\n", pPriv->lowLimit);
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:process:highLimit=%d\n", pPriv->highLimit);

            MONITOR(RECORD_LSWA);
        }

        /*
         *  If there has been a change in home switch state update the
         *  home switch active status and raise a monitor on the field.
         */

        if (pdr->hswa != pPriv->homeSwitch)
        {
            pdr->hswa = pPriv->homeSwitch;
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s:process:Home switch change, HSWA=%d\n", pdr->hswa );
            MONITOR(RECORD_HSWA);
        }

        /*
         *  If raw motor position (motor counts) as returned by the device 
         *  support has changed then update the raw readback value.
         */

        if (pdr->rrbv != pPriv->position)
        {
            DEBUG(DDR_MSG_MAX, 
                  "<%ld> %s:process: raw pos!=position%c\n", ' ');
            pdr->rrbv = pPriv->position;
            MONITOR(RECORD_RRBV);

            /*
             *  Not using encoder.  Update engineering motor position with 
             *  motor count value returned by device support.
             */

            if (!pdr->ueip)
            {
                pdr->mpos = (double)(pPriv->position) / pdr->mres;
                MONITOR(RECORD_MPOS);
            }
            /*
             *  Simulating so encoder was previously set to 0.  Fake it 
             *  by updating raw encoder and engineering motor position  
             *  using motor count value returned by device support.
             */

            else if (pdr->ueip && pPriv->simulation)
            {
                pPriv->encoder = (long)((double)(pPriv->position) * 
                                       pdr->eres / pdr->mres);
                pdr->renc = pPriv->encoder;
                MONITOR(RECORD_RENC);

                pdr->mpos = (double)(pPriv->position) / pdr->mres;
                MONITOR(RECORD_MPOS);
            }

        }

        /*
         *  Using encoder and its unsimulated value has changed.
         */

        if (pdr->ueip && !pPriv->simulation  &&  pdr->renc != pPriv->encoder )
        {
            DEBUG(DDR_MSG_MAX, "<%ld> %s:process: raw enc!=encoder=%c\n", ' ');

            /*
             * Encoders shouldn't change by more than what they could have travelled
             * at Max Velocity during one scanTask loop.
             */

	    if ( fabs(pPriv->encoder - pdr->renc) > 
                     (pdr->vhlm * pdr->eres / DDR_OMS_SCAN_TASK_RATE) + 10  )
            {
                DEBUG(DDR_MSG_WARNING,
                      "<%ld> %s:process: encoder value jumped by %d\n",
                      abs(pPriv->encoder - pdr->renc));
            }

            /*
             *  If the change in encoder value (from where the motor should 
             *  be) is less than the encoder deadband then set the 
             *  insideDeadband flag.   This will prevent the idle state
             *  from generating a spontaneous motion error due to encoder
             *  noise.
             */

            if (abs( pPriv->encoder - 
                     (long)((double)(pPriv->position) * pdr->eres / pdr->mres) ) <
                   pdr->edbd)
            {
                pPriv->insideDeadband = 1;
            }
            else
            {
                pPriv->insideDeadband = 0;
            }


            /*
             *  If we are changing out of simulation mode do not raise
             *  a monitor otherwise the idle state will generate a 
             *  spontaneous motion error.
             */

            if (pPriv->simmChange)
            {
                pPriv->simmChange = 0;
            }       
            else 
            {
                MONITOR(RECORD_RENC);
            }

            /*
             *  Update motor position with actual encoder generated position.
             */

            pdr->mpos = (double)((long)(pPriv->encoder) / pdr->eres);
            MONITOR(RECORD_MPOS);

            /*
             *  Update raw encoder with encoder count value.
             */

            pdr->renc = pPriv->encoder;

        } /* end of encoder stuff */

        semGive (pPriv->mutexSem);


        /*
         *   If there has been a change in motor status then update the
         *   record status field.
         */

        if ( pdr->msta != pPriv->status )
        {
            pdr->msta = pPriv->status;
            MONITOR(RECORD_MSTA);
        }

        
        /*
         *  Determine and process the currently active machine state.
         */
        
        status = processState (pdr);
        DEBUG(DDR_MSG_MAX, "<%ld> %s:process: clearing callback flag%c\n", ' ');
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->callback = FALSE;       
        semGive (pPriv->mutexSem);
    }


   
    /*
     * If the callback flag has not been set then the request
     * for processing came from the real world.
     * Process the action directive.
     */

    else
    {
        status = processDirective(pdr);
    }


    /*
     *  If a command has completed set the output to either idle or
     *  error depending on the presence of an error message.
     */

    if (pdr->pp) 
    {
        if ( strlen(pPriv->actionErrMess) && pdr->busy == DDR_CMD_BUSY )
        {
	  /* Command failed so this error message takes precedence */
            DEBUG(DDR_MSG_ERROR, "<%ld> %s:process: Command failed, setting BUSY to ERR%c\n", ' ');
            SET_MESS( pPriv->actionErrMess );
            pdr->busy = DDR_CMD_ERROR;
        }
        else if ( pPriv->rejectAck == FALSE && strlen(pPriv->rejectErrMess) ) 
        {
         /* Command rejected, so write to MESS */
            DEBUG(DDR_MSG_ERROR, "<%ld> %s:process: Command rejected.%c\n", ' ');
            SET_MESS( pPriv->rejectErrMess );
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->rejectAck = TRUE;
            semGive (pPriv->mutexSem);
        }
        else if ( strlen(pPriv->rejectErrMess) && pdr->busy == DDR_CMD_BUSY )
        {
	  /*  
           *  Left over command rejection message. Clear the MESS field
           *  here if you want a successful command completion to erase
           *  any command rejection messages.
           */
            DEBUG(DDR_MSG_MIN, "<%ld> %s:process: Move while moving command rejected, but this finished, setting BUSY to IDLE%c\n", ' ');
            pdr->busy = DDR_CMD_IDLE;
        }
        else if ( pdr->busy == DDR_CMD_BUSY )
        {
	  /* Command finished successfully */
            DEBUG(DDR_MSG_FULL, "<%ld> %s:process: Command finished, setting BUSY to IDLE%c\n", ' ');
            pdr->busy = DDR_CMD_IDLE;
        }
        MONITOR(RECORD_MESS);
        MONITOR(RECORD_BUSY);
    }               


    /*
     * Write data to all marked output links
     */

    if (MONITORED(RECORD_BUSY))
    {
        TRIGGER(RECORD_BUSY);     /* always write busy changes */
    }
    if (MONITORED(RECORD_MESS))
    {
        TRIGGER(RECORD_MESS);     /* always write message changes */
    }

    if (pdr->lmap)
    {
        status = writeOutputLinks (pdr);
        if (status) return ( status );
    }


    /*
     * If the command has completed clean up last epics stuff
     */

    if (pdr->pp)
    {
        DEBUG(DDR_MSG_MAX, "<%ld> %s:process: post process entry%c\n", ' ');
        pdr->pp = FALSE;
        recGblFwdLink( pdr );
        recGblGetTimeStamp( pdr );
    }


    /*
     *  Raise alarms and trigger monitors then clear the processing active
     *  flag to allow others to process the record.
     */

    alarm( pdr );
    monitor( pdr );

    pdr->pact = FALSE;
    DEBUG(DDR_MSG_MAX,
            "<%ld> %s: process: record processing ends .....%c\n\n", ' ');

    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * processDirective
 *
 * INVOCATION:
 * status = processDirective (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 *
 * PURPOSE:
 * Control command processing via the DIRective field
 *
 * DESCRIPTION:
 *
 * Handle an external processing request according to the
 * directive being issued via the following algorithm:
 *
 *      Clear the message fields
 *      Read the input links to update the operating parameters.
 *
 *      If the command directive is CHECK or GO:
 *      {
 *          If this is an INIT or TEST command:
 *          {
 *              reject the command if an interlock is present 
 *                or the record is busy
 *          }
 *
 *          If this is an INDEX command:
 *          {
 *              Reject command if:
 *                 Interlock is present
 *                 record is busy
 *                 indexing mode is not supported
 *                 velocities are outside limits
 *          }
 *
 *          If this is a MOVE command:
 *          {
 *              If the string value (vals) field contains something:
 *              {
 *                  reject command if it is not numeric or a named position
 *                  otherwise save the translated value for later use
 *              }
 *          }
 *
 *          If this is a MOVE or TRACK command:
 *          {
 *              reject command if:
 *                  fault field is set
 *                  index position is invalid
 *                  target position is outside limits
 *                  velocity is outside limits
 *                  indexing algorithm is invalid
 *          }
 *      }
 *      
 *      If the command is GO then:
 *      {
 *          Clear VALS field
 *          start command execution
 *          set busy field to BUSY to indicate that the record is busy
 *          set a callback timer to ensure busy state is seen by outside
 *
 *          If we are in virtual simulation mode:
 *          {
 *              return and wait for the record to be processed again
 *          }
 *
 *          If the target position or velocity fields changed while moving:
 *          {
 *              set move-while-moving flag
 *          }
 *
 *          If this is an INIT command:
 *          {
 *              Set the state machine to init
 *          }
 *
 *          Otherwise it is an action command:
 *          {
 *              Call the state machine function to execute the command
 *          }
 *      }
 *
 *      If the command is STOP then:
 *      {
 *          If the record is not executing a command:
 *          {
 *              set the busy field to busy
 *              request a callback after an acknowledgement interval
 *                which allows the BUSY/IDLE trransition to be seen
 *          }
 *
 *          Process the current state machine state
 *      }
 *
 *      If any of the above actions generated an error message
 *      {
 *          reject the command
 *      }
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


static long processDirective
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control sturcture           */
            *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    double  position;               /* target position for move             */
    long    index;                  /* indexing mode for move               */
    char    tempstr[100];           /* scratch buffer                       */
    long    status = 0;             /* function return status               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:processDirective:entry%c\n", ' ');


    /*
     * Clear the errror message field.
     */

    *pdr->mess = '\0';
    MONITOR(RECORD_MESS);
   
    *pPriv->errorMessage = '\0';
    *pPriv->actionErrMess = '\0';
    *pPriv->rejectErrMess = '\0';


    /*
     *  Read the input links to update the operating parameters.   If
     *  a read error occurred return immediately.
     */
    
    if ( ( status = readInputLinks (pdr, TRUE) ) )
    {
        return (status);
    }


    /*
     *  Process according to the command directive received.
     */

    switch (pdr->dir)
    {

        /*
         *  CHECK will check the validity of the input arguments.
         *  GO will check the validity of the input arguments then execute
         *  the command.
         */

        case DDR_DIR_CHECK:
        case DDR_DIR_GO:

            position = pdr->val;
            index = DDR_INDEX_NONE;
           
            DEBUG(DDR_MSG_FULL,
                      "<%ld> %s:processDirective: dir:%d\n", pdr->dir);

            /*
             *  Perform specialized validity checking based on the current
             *  operating mode.
             */

            switch (pdr->mode)
            {

                /*
                 *  Initialization can only be done if no other command
                 *  is being executed at the time.
                 */

                case DDR_MODE_INIT:

                    if (pdr->flt)
                    {
                        SET_REJ_MSG("Cannot init while fault line is active");
                        DEBUG(DDR_MSG_ERROR, 
                             "<%ld> %s:processDirective:init while interlocked%c\n",
                              ' ');
                    }
                    else if (pdr->mip != DDR_MIP_STOPPED &&
                             pdr->mip != DDR_MIP_HOLDING &&
                             pdr->mip != DDR_MIP_ERROR)
                    {
                        SET_REJ_MSG( "Cannot initialize while active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:init while busy%c\n",
                              ' ');
                    }
                    break;

                /*
                 *  Testing can only be done if no other command
                 *  is being executed at the time.
                 */

                case DDR_MODE_TEST:
                    if (pdr->flt)
                    {
                        SET_REJ_MSG("Test would fail - fault line is active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:test while interlocked%c\n",
                              ' ');
                    }
                    else if (pdr->mip != DDR_MIP_STOPPED &&
                             pdr->mip != DDR_MIP_HOLDING &&
                             pdr->mip != DDR_MIP_ERROR)
                    {
                        SET_REJ_MSG( "Cannot test while active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:test while busy%c\n",
                              ' ');
                    }
                    break;

                /*
                 *  The device can only be re-indexed if no other command
                 *  is being executed at the time and the indexing mode
                 *  and velocities are within limits.
                 */

                case  DDR_MODE_INDEX:
                    if (pdr->flt)
                    {
                        SET_REJ_MSG("Cannot index while fault line is active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:index while interlocked%c\n",
                              ' ');
                    }

                    else if (pdr->mip != DDR_MIP_STOPPED &&
                             pdr->mip != DDR_MIP_HOLDING &&
                             pdr->mip != DDR_MIP_ERROR     )
                    {
                        SET_REJ_MSG( "Cannot index while active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:index while moving%c\n",
                              ' ');
                    }


                    else if (pdr->velo > pdr->vhlm || pdr->velo < pdr->vllm)
                    {
                        SET_REJ_MSG( "Velocity out of range");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:index with bad velocity%c\n",
                              ' ');
                    }

                    else if (pdr->fivl * pdr->mres > DDR_MAX_INDEX_VEL ||
                             pdr->fivl * pdr->mres < DDR_MIN_INDEX_VEL)
                    {
                        SET_REJ_MSG( "Final Index Velocity out of range");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:index with bad fivl%c\n",
                              ' ');
                    }

                    index = pdr->ialg;

                    break;

                /*
                 *  In MOVE mode the string position field is checked first
                 *  to see if it contains a target string.   If it does then
                 *  it must contain a valid named or numeric target position.
                 */

                case DDR_MODE_MOVE:
                    if ( ( !checkVals( pdr, &position, &index) ) )
                    {
                        SET_REJ_MSG( "Cannot translate named position");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:move mode: bad VALS string%c\n",
                              ' ');
                        break;
                    }


                /*
                 *  MOVE or TRACK commands can only be executed if the fault
                 *  line is clear, we have not lost our index position and the
                 *  target position and motion velocity are within limits.
                 */

                case DDR_MODE_TRACK:
                    if (pdr->flt)
                    {
                        SET_REJ_MSG("Cannot move while fault line is active");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:move while interlocked%c\n",
                              ' ');
                    }
            
                    else if (!index && !pdr->hpvl)
                    {
                        SET_REJ_MSG( "Must re-index device before moving");
                        DEBUG(DDR_MSG_ERROR,
                              "<%ld> %s:processDirective:move after lost index%c\n",
                              ' ');
                    }

                    else if (position > pdr->phlm || position < pdr->pllm)
                    {
                        SET_REJ_MSG( "Target position out of range");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:move with bad target%c\n",
                              ' ');
                    }

                    else if ((position + pdr->blco) > pdr->phlm ||
                             (position + pdr->blco) < pdr->pllm)
                    {
                        SET_REJ_MSG( "Backlash correction moves out of range");
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:move with backlash out of range%c\n",
                        ' ');
                    }

                    else if (pdr->velo > pdr->vhlm || pdr->velo < pdr->vllm)
                    {
                        SET_REJ_MSG( "Velocity out of range");
                        DEBUG(DDR_MSG_ERROR,
                              "<%ld> %s:processDirective:move with bad velocity%c\n",
                              ' ');
                    }

                    else if ( index < DDR_INDEX_NONE ||
                              index > DDR_INDEX_ULSW )
                    {
                        sprintf(tempstr,
                              "Invalid index <pPriv->index=%d>",
                              pPriv->index );
                        SET_REJ_MSG( tempstr );
                        DEBUG(DDR_MSG_ERROR, 
                              "<%ld> %s:processDirective:move with bad index=%d\n",
                              pPriv->index );
                    }           
                    break;

                /*
                 *  All other settings of the mode field are rejected as
                 *  unsupported.
                 */

                default:
                    SET_REJ_MSG("Unsupported mode requested");
                    DEBUG(DDR_MSG_ERROR, 
                          "<%ld> %s:processDirective: Invalid mode=%d\n",
                          pdr->mode );
                    break;

            }   /* end validation according to mode*/
             

            /*
             *  If we were just checking the inputs or if any of the checks
             *  failed then exit here before the command is executed.
             */

            if (pdr->dir == DDR_DIR_CHECK || strlen (pPriv->rejectErrMess))
            {
                break;
            }


            DEBUG(DDR_MSG_FULL,
                    "<%ld> %s:processDirective: GO directive%c\n", ' ');

            /*
             *  VALS conversion did not fail so erase the contents of the
             *  VALS field.
             */
           
            pdr->vals[0] = '\0';
            MONITOR(RECORD_VALS);

            /*
             *  Start command execution... make sure that the BUSY field is
             *  set to busy to indicate that command processing has started.
             */

            if (pdr->busy != DDR_CMD_BUSY)
            {
                pdr->busy = DDR_CMD_BUSY;
                MONITOR(RECORD_BUSY);                           
            
                /*
                 *  Set the callback timer first so that the busy field will remain 
                 *  set long enough for the outside world to see it.
                 */

                (*pdset->setDelay) (pPriv, DDR_ACKNOWLEDGE_TIME);

                /*
                 *  In virtual simulation mode we do not do anything except
                 *  set the busy field so exit here.  Set the "home position valid" flag 
                 *  if indexing
                 */

                if ( pPriv->simulation == DDR_SIM_VSM )
                {
                    if ( pdr->mode == DDR_MODE_INDEX )
                    {
                        pdr->hpvl = TRUE;
                        MONITOR(RECORD_HPVL);
                    }
                    break;
                }
            }


            /*
             *  If the target position or motion velocity has changed
             *  while a move was already in progress then set the
             *  move_while_busy flag so that the device support can
             *  update the motion parameters.
             */

            if ( (pdr->dir == DDR_DIR_GO) &&
                (pdr->osta != DDR_IDLE) &&
                ( (fabs (position - (double)(pPriv->target / pdr->mres)) > pdr->mdbd) ||
                (pPriv->velocity != (long)(pdr->velo * pdr->mres)) ) )
            {
                semTake (pPriv->mutexSem, WAIT_FOREVER);
                pPriv->move_while_busy = TRUE;
                semGive (pPriv->mutexSem);
                DEBUG(DDR_MSG_FULL, 
                      "<%ld> %s:processDirective: motion update while busy%c\n",' ');
            }


            /*
             *  Update the local mode field.  If the mode is INIT
             *  then set the state machine to the initializing state.
             *  Note that the initialization state will only be processed
             *  after the acknowledgement delay set earlier!!!
             */

            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->mode = pdr->mode;
            pPriv->backlashMotion = FALSE;
            semGive (pPriv->mutexSem);

            if (pdr->mode == DDR_MODE_INIT)
            {
                pdr->osta = DDR_INIT;
            }


            /*
             *  Otherwise this is an action of some sort.  Call the state 
             *  machine processing function to execute the command.  Note
             *  that if the index algorithm is set then the local
             *  operating mode is forced to be index as well.
             */

            else
            {
                pdr->val = position;
                semTake (pPriv->mutexSem, WAIT_FOREVER);
                if (index)
                {
                    pPriv->mode = DDR_MODE_INDEX;
                }
                pPriv->index = index;
                semGive (pPriv->mutexSem);
                status = processState(pdr);             
            }

            break;


        /*
         *  STOP directive will stop any command in progress.
         */
                 
        case DDR_DIR_STOP:
   
             DEBUG(DDR_MSG_MAX,
                  "<%ld> %s: processDirective: process STOP directive%c\n", ' ');

            /*  
             *  If the record is not executing a command then set the 
             *  BUSY field to indicate that the command was received and
             *  request a callback after the acknowledgement interval to
             *  clear it.  This will keep the busy flag set long enough
             *  for the outside world to see it. 
             */ 
             
             if (pdr->busy != DDR_CMD_BUSY)
            {
                pdr->busy = DDR_CMD_BUSY;
                MONITOR(RECORD_BUSY);                           
               (*pdset->setDelay) (pPriv, DDR_ACKNOWLEDGE_TIME);
            }


            /*
             *  Process the currently active state.   State processing
             *  will see the stop command and shut things down.
             */
          
            status = processState (pdr);
           
            break;


        /*
         * Anything else is invalid and will be rejected
         * Set DIR to CHECK so the rejection will be noticed.
         */

        default:
            SET_REJ_MSG("Unsupported directive requested");
            DEBUG(DDR_MSG_ERROR,
                   "<%ld> %s: processDirective: Invalid directive=%d\n", 
                    pdr->dir);
            pdr->dir = DDR_DIR_CHECK;
            MONITOR(RECORD_DIR);
            break;
           
    }   /* end processing according to directive */


    /*
     *  If an error message has been generated then the command has been
     *  rejected for some reason.  Set the acknowlege (ack) field to 
     *  indicate this before returning.
     */
    
    if (strlen (pPriv->rejectErrMess))
    {
        pPriv->rejectAck = FALSE;
        pdr->ack = -1;
        pdr->pp = TRUE;
    }
    else
    {
        pdr->ack = 0;
    }
    MONITOR(RECORD_ACK);
                                             
    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * processState
 *
 * INVOCATION:
 * status = processState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 *
 * PURPOSE:
 * Direct processing to the current state machine state
 *
 * DESCRIPTION:
 * Call the state machine function for the current operating state stored
 * in the record "operating state" OSTA field.  Abort the command if the
 * local status field had been set.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long processState
(
    DEVICE_CONTROL_RECORD    *pdr       /* deviceControl Record structure   */
)
{
    DEVICE_CONTROL_PRIVATE              /* internal control structure       */
            *pPriv = pdr->dpvt;
    long    status = 0;                 /* function return status           */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:processState: entry%c\n", ' ');


    /*
     *  If the local status field has been set then a device support error
     *  of some sort has been detected.   Ensure that the command is
     *  terminated by calling the aborting state directly at this point.
     */

    if (pPriv->status && pdr->mip != DDR_MIP_ERROR)
    {
        SET_ERR_MSG( pPriv->errorMessage);
        DEBUG(DDR_MSG_ERROR,
              "<%ld> %s:processState: device layer failure, status = %ld\n",
              pPriv->status);
        status = abortingState (pdr);   /* indicate the motion failed */
    }


    /*
     *  Otherwise it is safe to process the current state of the state
     *  machine.  Call the appropriate state function as selected by the
     *  record operating state (osta) field.
     */

    else
    {
        DEBUG(DDR_MSG_MAX, "<%ld> %s:processState: osta=%d\n", pdr->osta );
        switch (pdr->osta) {
            case DDR_INIT:
                status = initState (pdr);
                break;
            case DDR_IDLE:
                status = idleState (pdr);
                break;
            case DDR_POWERING:
                status = poweringState (pdr);
                break;
            case DDR_UNLOCKING:
                status = unlockingState (pdr);
                break;
            case DDR_STARTING:
                status = startingState (pdr);
                break;
            case DDR_MOVING:
                status = movingState (pdr);
                break;
            case DDR_STOPPING:
                status = stoppingState (pdr);
                break;
            case DDR_HOLDING:
                status = holdingState (pdr);
                break;
            case DDR_ENGAGING:
                status = lockingState (pdr);
                break;
            case DDR_DEPOWERING:
                status = depoweringState (pdr);
                break;
            case DDR_FAILING:
                DEBUG(DDR_MSG_LOG,
                      "<%ld> %s:processState: aborting%c\n", ' ');
                status = abortingState (pdr);
                break;
        }                         
    }
    
   
    /*
     *  If an error was detected in state processing then set the post
     *  processing flag to ensure that the command is completed at this
     *  point.
     */
    
    if (status) 
    {
        pdr->pp = TRUE;
    }

    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * readInputLinks
 *
 * INVOCATION:
 * status = readInputLinks (pdr, all);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 * (>) readDol  (int)  update VAL field as well by reading the DOL link.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Read values from EPICS input links
 *
 * DESCRIPTION:
 * Update record fields that have input links associated with them.
 * If the DOL link is connected update the VAL field.
 * If the SIML link is connected update the SIMM field.
 * If the DBGL link is connected update the DBUG field.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long readInputLinks
(
    DEVICE_CONTROL_RECORD *pdr,         /* deviceControl record structure   */
    int readDol                         /* read the .dol field as well      */
)
{
    DEVICE_CONTROL_PRIVATE              /* internal control structure       */
            *pPriv = pdr->dpvt;
    unsigned    short sval;             /* value read from DBR_SHORT link   */
    double    dval;                     /* value read from DBR_DOUBLE link  */
    long    status = 0;                 /* function return status           */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:readInputLinks: entry%c\n", ' ');


    /*
     *  If the readDol flag is set and something is attached to the DOL field 
     *  then update the val field by reading the desired output location 
     *  link.
     */

    if ( readDol )
    {

        if (pdr->dol.type != CONSTANT)
        {
	    status = dbGetLink(&(pdr->dol), DBR_DOUBLE, &dval, 0, 0);
            if (status) return (status);

            if (pdr->val != dval)
            {
                DEBUG(DDR_MSG_MAX,
                      "<%ld> %s:readInputLinks: Updating VAL field%c\n", ' ');
                pdr->val = dval;
            }
        }
    }


    /*
     *  If something is attached to the SIML link then update the current
     *  simulation level (simm) field by reading the siml link.
     */
 
    if (pdr->siml.type != CONSTANT)
    {
	status = dbGetLink(&(pdr->siml), DBR_USHORT, &sval, 0, 0);
        if (status) return (status);

        if (pdr->simm != sval)
        {
            DEBUG(DDR_MSG_MAX,
                  "<%ld> %s: readInputLinks: updating simm to %d\n", sval);
            pdr->simm = sval;
            MONITOR(RECORD_SIMM);
        }
    }


    /*
     *  The internal simulation level can only be changed if the
     *  record is not executing a command.
     */

    if (pdr->busy != DDR_CMD_BUSY && pPriv->simulation != pdr->simm )
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->simulation = pdr->simm;
        DEBUG(DDR_MSG_MAX,
              "<%ld> %s: readInputLinks: Not busy, updating simulation to %d\n",
              pPriv->simulation);
        pPriv->simmChange = 1;

        /*
         *  Restore the real motor status and target information
         *  output fields whenever switching simulation modes.
         */

        pPriv->target = pPriv->position;
        pdr->rpos = pPriv->target;

        if (pPriv->simulation == DDR_SIM_NONE) 
        {
            pPriv->simmHpvl = pdr->hpvl;
        }
        else
        {
            pdr->hpvl = pPriv->simmHpvl;
            MONITOR(RECORD_HPVL);
        }
        semGive (pPriv->mutexSem);
    }


    /*
     *  If something is attached to the DBGL link then update the current
     *  debug level (dbug) field by reading the dbgl link.
     */
 
    if (pdr->dbgl.type != CONSTANT)
    {
	status = dbGetLink(&(pdr->dbgl), DBR_USHORT, &sval, 0, 0);
        if (status) return (status);

        if (pdr->dbug != sval)
        {
            DEBUG(DDR_MSG_MAX,
                  "<%ld> %s: readInputLinks: updating DDR_DEBUG to %d\n",sval);
            pdr->dbug = sval;
            MONITOR(RECORD_DBUG);
        }
    }

    semTake (pPriv->mutexSem, WAIT_FOREVER);
    pPriv->debug = pdr->dbug;
    semGive (pPriv->mutexSem);

    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * special
 *
 * INVOCATION:
 * status = special (paddr, after);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) paddr  (struct dbAddr *) record field address structure.
 * (>) after  (int)             Pre/post processing flag.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Perform special pre/post processing on selected fields
 *
 * DESCRIPTION:
 * Currently only the fault line is tagged for special processing.
 * Set the internal fault change flag after the field has been written
 * to identify the subsequent processing as a fault change call.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long special
(
    struct dbAddr    *paddr,        /* record field address structure       */
    int              after          /* pre/post processing flag             */ 
)
{
    DEVICE_CONTROL_RECORD           /* deviceControl record structure       */
        *pdr = (DEVICE_CONTROL_RECORD *) paddr->precord;
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
        *pPriv = pdr->dpvt;


    /*
     *  Ignore the call made to this function before the database field has
     *  been written to.
     */
   
    if ( !after )
    {
        return (0);
    }
   

    /*
     *  Trap writes to the fault line here.   Since the fault field is
     *  tagged as passive-process the record will be processed after
     *  this field has been written.   Setting the internal fault flag
     *  identifies the processing as belonging to a fault field access.
     *  Note that this function is called EVERY TIME the FLT field is
     *  written to, not just when FLT changes.  So if you are
     *  continuously writing a "0" to clear the fault, this will cause
     *  the record to process each time.
     */
    
    if (paddr->pfield == (void *) &pdr->flt)
    {
        DEBUG(DDR_MSG_MAX, "<%ld> %s:special: fault <%ld>\n", pdr->flt);
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        if (pPriv->fault != pdr->flt)
        {
            DEBUG(DDR_MSG_MIN, "<%ld> %s:special: new fault state, was: <%d>\n", 
                pPriv->fault);
            pPriv->faultChange = TRUE;
            pPriv->fault = pdr->flt;
        }
        semGive (pPriv->mutexSem);
        MONITOR(RECORD_FLT);
    }

    return (0);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * startingState
 *
 * INVOCATION:
 * status = startingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Set up motion parameters and start the motor moving
 *
 * DESCRIPTION:
 *      If we were called from another state (entering starting state):
 *      {
 *          Switch state machine to starting state
 *          Clear the move_while_busy flag if set
 *          Convert velocity and acceleration from user units to steps
 *          Ensure that the velocity is always larger than the base velocity.
 *          Update encoder deadband
 *          Store values in the internal control structure
 *          Call the device support setup function load them into the hardware
 *
 *          If simulating index:
 *          {
 *              set internal index mode to none (index without hardware)
 *              set target to 0
 *          }
 *
 *          Update the internal target position
 *
 *          If backlash correction is enabled:
 *          {
 *              If this is the initial motion:
 *              {
 *                  convert target plus backlash overshoot to steps and save
 *              }
 *
 *              Otherwise this is the second (anti-backlash) motion:
 *              {
 *                  restore the original position
 *              }
 *          }
 *
 *          Otherwise no backlash motion is required:
 *          {
 *              convert target position to steps and save
 *          }
 *
 *          If the motor rounding is requested:
 *          {
 *              round target position in steps down to multiple of value
 *          }
 *
 *          Call device support to start the motion using saved values
 *          Set check limits flag to update the limit switch state
 *
 *          If device was already at the target position:
 *          {
 *              call moving state function directly
 *          }    
 *
 *          Request a callback to process record later to check motor started
 *      }
 *
 *      If stop directive received before the motor started:
 *      {
 *          Exit to stopping state
 *      }
 *
 *      If the device has started moving we were successful:
 *      {
 *          Cancel starting timeout
 *          Exit to moving state
 *      }
 *
 *      If we are trying to move into a limit switch:
 *      {
 *          If we are indexing on the soft limit switch (okay):
 *          {
 *              Set the current position equal to the homing offset
 *              Set home position valid field
 *              Exit to the lockingState
 *          }
 *
 *          Otherwise this is an error:
 *          {
 *              Generate an error message
 *              Exit to the aborting state
 *          }
 *
 *      If the timeout flag is set then the device did not start in time:
 *      {
 *           Generate an error message
 *           Exit to the aborting state.
 *      }
 *
 *      Remain in starting state until the record is processed again.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long startingState
(
    DEVICE_CONTROL_RECORD    *pdr   /* device control record structure      */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
            *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long    status = 0;             /* function return status               */


    DEBUG(DDR_MSG_MAX, "<%ld> %s:startingState:entry%c\n", ' ');

    /*
     *  If entering the starting state from another state (first pass) the
     *  setup and start the motion.
     */
    
    if (pdr->osta != DDR_STARTING)
    {
        DEBUG(DDR_MSG_FULL,
               "<%ld> %s: startingState: from osta:%d\n", pdr->osta);

        /*
         *  switch the state machine into starting state.
         */

        pdr->osta = DDR_STARTING;

        semTake (pPriv->mutexSem, WAIT_FOREVER);

        /*
         *  If we are changing motion parameters on the fly then
         *  acknowledge that this has been done.
         *  ???Can we guarantee that osta != DDR_STARTING so that
         *  this code will get executed????
         */

        if ( pPriv->move_while_busy )
        {
            DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, unsetting move_while_busy%c\n", ' ');
            pPriv->move_while_busy = FALSE;
            pPriv->backlashMotion = FALSE;
        }


        /*
         *  Update the internal (device step based units) velocity fields
         *  with the current state of the record input fields.
         */

        pPriv->velocity = (long)(pdr->velo * pdr->mres);
        pPriv->indexVelocity = (long)(pdr->fivl * pdr->mres);
        pPriv->baseVelocity = (long)(pdr->vbas * pdr->mres);
        pPriv->acceleration = (long)(pdr->accl * pdr->mres);


        /*
         *  Ensure that the velocity is always larger than the base velocity.
         */

        if (pPriv->baseVelocity >= pPriv->velocity)
        {
            pPriv->velocity = (pPriv->baseVelocity + 1);
            DEBUG(DDR_MSG_FULL, 
                  "<%ld> %s: startingState: increasing velocity to %ld steps/sec\n",
                  pPriv->velocity );
        }

        /*
         *  Update the encoder deadband region.
         */

        pPriv->encoderDeadband = pdr->edbd;
        DEBUG(DDR_MSG_MAX,
                "<%ld> %s:startingState: encoder Deadband=%ld\n",
                pPriv->encoderDeadband );
        semGive (pPriv->mutexSem);


        /*
         *  Load the updated motion parameters into the motion control
         *  hardware.  Since this is hardware specific, let the device 
         *  support code worry about how this is done.....
         */

        status = (*pdset->configureDrive) (pPriv);
        if (status)
        {
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:startingState: configureDrive call failed%c\n",
                  ' ');
            SET_ERR_MSG( pPriv->errorMessage);
            return abortingState (pdr);
        }

        /*
         *  If currently simulating then set the internal index mode to 
         *  none (index without hardware switches)and set the target
         *  position to 0.
         */

        semTake (pPriv->mutexSem, WAIT_FOREVER);

            DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, current mode is: %ld\n",pPriv->mode );

        if ( pPriv->simulation && pPriv->mode == DDR_MODE_INDEX )
        {
            DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, setting IALG to NONE and target position to 0%c\n", ' ');
            pPriv->index = DDR_INDEX_NONE;
            pPriv->target = 0;
        }


        /*
         *  Update the internal target position (device step units) with
         *  the current value of the val field.  
         */
 
        pPriv->target = (long)(pdr->val * pdr->mres);

        /*
         *  If we are doing a backlash corrected move, then handle the 
         *  two target positions here.
         */

        if (  (pPriv->backlashMotion == FALSE) &&
              ( (pdr->blco < 0 && ((pPriv->target - pPriv->position) < 0))  ||
                (pdr->blco > 0 && ((pPriv->target - pPriv->position) > 0)) )  )
        {

            pPriv->backlashMotion = TRUE;

            /*
             *  Overshoot or undershoot the target position as necessary to 
             *  allow the final backlash motion to take up the mechanical 
             *  slack (software to the rescue again!).
             */

            pPriv->target = (long)((pdr->val + pdr->blco) * pdr->mres);
            DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, backlash overshoot, target: %ld\n",
                pPriv->target);
        }
        else if (pPriv->backlashMotion == TRUE)
        {
            DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, backlash correction, target: %ld\n",
                pPriv->target);
            pPriv->backlashMotion = FALSE;
         }
         else
         {   DEBUG(DDR_MSG_FULL,
                "<%ld> %s:startingState, non-backlash move, target: %ld\n",
                 pPriv->target);
         }


        /*
         *  If the value in the motor rounding field is greater than one then
         *  we have to round the internal target position down to the nearest
         *  multiple of the mrnd number.  This is done to handle the case
         *  where stepper motors are driven in microstep mode to smooth the
         *  motion but there is no brake to hold a micro step position between
         *  motor detents.   This is a hack to try and tune the target 
         *  position to always land on a full-step position.
         */

        if (pdr->mrnd > 1)
        {
            pPriv->target = pPriv->target - (pPriv->target % pdr->mrnd);
        }

        semGive (pPriv->mutexSem);


        /*
         *  Start the motor moving to the target position calculated above.
         */

        status = (*pdset->controlMotion) (pPriv, DDR_MOVE_GO);
        if (status)
        {
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:startingState: controlMotion call failed%c\n",
                  ' ');
            SET_ERR_MSG( pPriv->errorMessage);
            return abortingState (pdr);
        }


        /*
         *  Set the checkLimits flag to ask the device support to
         *  check the state of the limit switches after the motion has
         *  started.   Some interface hardware only looks for limits in
         *  the direction it thinks the motor is travelling and they will
         *  not detect a limit if the last move was in the opposite direction.
         */

        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->checkLimits = 1;
        semGive (pPriv->mutexSem);


        /*
         *  If the device was already in the target position then there
         *  will be no device motion.   Handle this by switching to the
         *  moving state directly (rather than after the motor starts moving).
         */ 

        if ( pPriv->index == DDR_INDEX_NONE &&
             pPriv->target == pPriv->position )
        {
            semTake (pPriv->mutexSem, WAIT_FOREVER);
            pPriv->moving = TRUE;
            semGive (pPriv->mutexSem);
            DEBUG(DDR_MSG_FULL,
                "<%ld> %s: startingState: We're there, so fake moving%c\n", ' ');
            status = movingState (pdr);
        }

        /*
         *  Request a callback after the start timeout to trap the case
         *  where the motor does not start then return and wait for the
         *  record to be processed again.
         */

        (*pdset->setDelay) (pPriv, DDR_START_TIMEOUT);

        return ( status );
    }


    /*
     *  STOP directive received meaning that the operator has cancelled the 
     *  motion.  Shut things down by moving directly to the stopping state.
     */
    
    if ( pdr->dir == DDR_DIR_STOP )
    {
        (*pdset->setDelay) (pPriv, 0);
        status = stoppingState (pdr);
    }


    /*
     *  If the device has started moving then we have been successful.  Cancel
     *  the starting timeout and move on to the moving state.
     */
    
    if ( pPriv->moving )
    {
        (*pdset->setDelay) (pPriv, 0);   /* cancel the timeout */
        status = movingState (pdr);      /* and monitor the motion */
        return (status);
    }   
   

    /*
     *  The checkLimits flag caused the limit switches to be read and it looks
     *  like we are trying to move into a limit.
     */

    if (pdr->lswa)
    {

        /*
         *  If we are indexing using the soft limit as an index switch
         *  then this is a good thing.  Zero the position counter, set the
         *  home position valid flag and shut down the motion by moving
         *  on to the locking state.
         */

        if (pPriv->index &&
	    ((pPriv->index == DDR_INDEX_LLSW && pPriv->lowLimit) ||
	     (pPriv->index == DDR_INDEX_ULSW && pPriv->highLimit)) )
        {
            if (pPriv->homeOffset)
            {
                status = (*pdset->setPosition) (pPriv, pPriv->homeOffset);
                if (status)
                {
                    DEBUG(DDR_MSG_ERROR,
                          "<%ld> %s:startingState: setPosition call failed%c\n",' ');
                    SET_ERR_MSG( pPriv->errorMessage);
                    return abortingState (pdr);
                }
            }

            pdr->hpvl = TRUE;
            MONITOR(RECORD_HPVL);
            return ( lockingState(pdr) );
        }


        /*
         *  Otherwise it is a bad thing.  Generate an error message
         *  and abort the motion immediately.
         */

        else
        {
            SET_ERR_MSG( "Cannot move into a soft limit");
            DEBUG(DDR_MSG_ERROR, 
                  "<%ld> %s: startingState: can't move into a limit%c\n", ' ');
            return ( abortingState (pdr) );
        }
    }


    /*
     *  If the timeout flag is set then this is the callback requested above.
     *  The device did not start after receiving a start command.   Generate
     *  an error message and abort the command at this point.
     */
    
    if ( pPriv->timeout )
    {
        if ( pPriv->checkLimits )
        {
            DEBUG(DDR_MSG_MIN,
                  "<%ld> %s:startingState: scanTask did not check limits!%c\n",
                  ' ');
        }

        SET_ERR_MSG( "Motion did not start in time");
        DEBUG(DDR_MSG_ERROR,
               "<%ld> %s: startingState: Motion did not start in time%c\n", ' ');
        return ( abortingState (pdr) );
    }
        

    /*
     * Otherwise just hang out until something more interesting happens
     */ 
    
    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * stoppingState
 *
 * INVOCATION:
 * status = stoppingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)   deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Bring the motor to a controlled stop
 *
 * DESCRIPTION:
 * Stop the motor via the following algorithm:
 *
 *      If we were called from another state (entering stopping state):
 *      {
 *          Switch state machine to stopping state.
 *
 *          Call the device support motor control function to perform
 *            a controlled stop.
 *
 *          Set a callback timeout so that we can check that it has stopped.
 *          Return and wait for the record to be processed again.
 *      }
 *
 *      If the motor has stopped
 *      {
 *          Cancel the callback timeout.
 *          Exit to the locking state.
 *      }
 *
 *      If this is the callback requested above then the motor did not stop:
 *      {
 *          Generate an error message.
 *          Exit to the aborting state.
 *      } 
 *
 *      Remain in stopping state until the record is processed again.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long stoppingState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control sturcture           */ 
        *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
        *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long timeout;                   /* callback delay value                 */
    long status = 0;                /* function return status               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:stoppingState:entry%c\n", ' ');



    /*
     *  If entering the stopping state from another state then call the
     *  device support motor stopping function and request a callback
     *  in case the motor does not stop.
     */
         
    if (pdr->osta != DDR_STOPPING)
    {
        DEBUG(DDR_MSG_FULL,
              "<%ld> %s: stoppingState: from osta:%d\n", pdr->osta);

        /*
         *  switch the state machine into the stopping state.
         */

        pdr->osta = DDR_STOPPING;


        /*
         *  tell the device to come to a controlled stop.
         */
       
        status = (*pdset->controlMotion) (pPriv, DDR_MOVE_STOP);
        if (status)
        {
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s:stoppingState: controlMotion call failed%c\n",
                  ' ');
            SET_ERR_MSG( pPriv->errorMessage);
            return abortingState (pdr);
        }


        /*
         *  Request a callback after the deceleration period (in seconds).
         *  Since setDelay is expecting units of 0.1, multiply by 10 before
         *  passing it on and add the  motion overhead constant(already
         *  in units of 0.1 seconds). 
         *
         *  This will allow the record to generate an error if the motor 
         *  does not stop.  Following this, return and wait for the record 
         *  to be processed again.
         */

        timeout = (pPriv->acceleration == 0 ) ? 0 :
              ( DDR_MOTION_OVERHEAD_TIME + 
                (long) ceil (10 * pPriv->velocity / pPriv->acceleration ));
        if (timeout < DDR_START_TIMEOUT) timeout = DDR_START_TIMEOUT;
        DEBUG(DDR_MSG_FULL,
              "<%ld> %s: stoppingState: timeout:%ld\n", timeout);
        (*pdset->setDelay) (pPriv, timeout);
       
        return ( status );
    }


    /*
     *  If the device is no longer moving then we have been successful in 
     *  stopping the motion.  Move on to the locking state to apply the brake
     *  and finish the shutdown sequence.
     */
    
    if (!pPriv->moving)
    {
        (*pdset->setDelay) (pPriv, 0);   /* cancel the timeout */

        if (pdr->lswa)
        {
            SET_ERR_MSG( "Device hit a soft limit");
            DEBUG(DDR_MSG_ERROR,
                  "<%ld> %s: stoppingState: Device hit a soft limit%c\n", ' ');
            return ( abortingState(pdr) );
        }
      
        return ( lockingState (pdr) );
    }   
    

    /*
     *  If the timeout flag is set then we did not stop even though told to.
     *  Generate an error and abort the command immediateldy.
     */
    
    if (pPriv->timeout)
    {
        SET_ERR_MSG( "motion did not stop in time");
        DEBUG(DDR_MSG_ERROR,
              "<%ld> %s: stoppingState: Motion did not stop in time%c\n", ' ');
        return ( abortingState (pdr) );
    }


    /*
     *  Otherwise hang out and wait for something interesting to happen.
     */

    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * unlockingState
 *
 * INVOCATION:
 * status = unlockingState (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Release the brake or locking mechanism
 *
 * DESCRIPTION:
 * Release the motor brake using the following algorithm:
 *
 *      If we were called from another state (entering unlocking state):
 *      {
 *          Switch state machine to unlocking state.
 *
 *          If we can control the brake:
 *          {
 *              Clear brake control bit.
 *              Request that the brake control link be written.
 *              Set a callback timeout so that we can check the brake.
 *              Return and wait for the record to be processed again.
 *          }
 *
 *          Otherwise there is nothing to do so:
 *          {
 *              Exit to the starting state.
 *          }
 *      }
 *
 *      If the stop command has been received:
 *      {
 *          Exit to the locking state.
 *      }
 *
 *      If we are using the brake status bit and the brake is now off:
 *      {
 *              Cancel the callback timeout.
 *              Exit to the starting state.
 *      }
 *
 *      If this is the callback requested above:
 *      {
 *          If we are not using the brake status bit:
 *          {
 *              Exit to the starting state.
 *          }
 *
 *          If we are in simulation mode:
 *          {
 *              Clear the brake status bit to make it look like brake is off.
 *              Exit to the starting state.
 *          }
 *
 *          Otherwise there must be a problem because brake is still on:
 *          {
 *              Generate an error message.
 *              Exit to the aborting state.
 *          }
 *      } 
 *
 *      Remain in unlocking state until the record is processed again.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long unlockingState
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    DEVICE_CONTROL_PRIVATE          /* internal control structure           */
            *pPriv = pdr->dpvt;
    DEVICE_CONTROL_DSET             /* device support function structure    */
            *pdset = (DEVICE_CONTROL_DSET *) (pdr->dset);
    long     status = 0;            /* function return status               */


    DEBUG(DDR_MSG_MAX, "<%ld> %s:unlockingState:entry%c\n", ' ');


    /*
     *  If entering the unlocking state from another state (first pass) then
     *  switch the state machine into unlocking state and remove the brake.
     */
    
    if (pdr->osta != DDR_UNLOCKING)
    {
        DEBUG(DDR_MSG_FULL,
              "<%ld> %s: unlockingState: from osta:%d\n", pdr->osta);

        pdr->osta = DDR_UNLOCKING;

        /*
         *  If we are controlling the brake (brake timeout is set) then
         *  clear the brake control output field and request a callback
         *  after the brake timeout interval to allow is to check the
         *  state.
         */

        if (pdr->btmo)
        {         
            pdr->brk = FALSE;
            MONITOR(RECORD_BRK);
                    
            if (!pPriv->simulation)
            {
                TRIGGER(RECORD_BRK);
            }           

            (*pdset->setDelay) (pPriv, pdr->btmo);
                      
        }

        /*
         *  Otherwise there is no brake to control so move right on to
         *  the starting state.
         */

        else
        {
            status = startingState (pdr);
        }


        /*
         *  return and wait for the record to be processed again.
         */

        return ( status );
    }


    /*
     *  If the STOP directive has been received then the operator has
     *  cancelled the motion.  Proceed directly to the locking state to
     *  put the brake back on.
     */
    
    if (pdr->dir == DDR_DIR_STOP)
    {
        return( lockingState (pdr) );
    }


    /*
     *  If we can sense the state of the brake (use brake status field is set)
     *  and the brake is off then cancel the callback and proceed directly
     *  to the starting state.
     */
    
    if (pdr->ubsb && !pdr->bsta)
    {
        (*pdset->setDelay) (pPriv, 0);
        status = startingState (pdr);
        return (status);
    }
   
   
    /*
     *  If the timeout flag is set then this is the callback requested
     *  above.
     */
    
    if (pPriv->timeout)
    {
        semTake (pPriv->mutexSem, WAIT_FOREVER);
        pPriv->timeout = FALSE;
        semGive (pPriv->mutexSem);

        /*
         *  If we are not reading the brake status then the callback
         *  was just to give the brake time to react.   Proceed to the
         *  starting state since the brake should be off by now.
         */

        if (!pdr->ubsb)
        {
            status = startingState (pdr);
        }


        /*
         *  In simualtion mode fake the brake status by writing directly
         *  to the the status bit before proceeding to the starting state.
         */

        else if (pPriv->simulation)
        {
            pdr->bsta = FALSE;
            MONITOR(RECORD_BSTA);
            status = ( startingState (pdr) );
        }


        /*  Otherwise we are reading it and something has gone wrong because
         *  the brake did not come off.   Can't move the motor with the
         *  brake on so abort the command here.
         */

        else
        {
            SET_ERR_MSG( "brake did not release in time");
            DEBUG(DDR_MSG_ERROR,
                 "<%ld> %s: unlockingState: Brake did not release in time%c\n",
                 ' ');
            return ( abortingState (pdr) );
        }
    }
       
    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * writeOutputLinks
 *
 * INVOCATION:
 * status = writeOutputLinks (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *)  deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) record processing success code.
 *
 * PURPOSE:
 * Write selected EPICS output links
 *
 * DESCRIPTION:
 * Each output link that is connected and has been selected for writing by 
 * the TRIGGER macro will write the current value of the associated data 
 * field to the output link.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long writeOutputLinks
(
    DEVICE_CONTROL_RECORD    *pdr   /* deviceControl record structure       */
)
{
    long status = 0;                /* function return status               */

    DEBUG(DDR_MSG_MAX, "<%ld> %s:writeOutputLinks: %lx\n", pdr->lmap);

    /*
     *  If a field has been marked by writing (done by setting the matching
     *  bit in the record's lmap field via the TRIGGER macro) during processing
     *  then write the field via the associated output link field.
     *  Write the busy field last to ensure that the other fields have been
     *  seen before the higher level code recognizes that the command is done.
     */

    if (TRIGGERED(RECORD_BRK))
    {
	status = dbPutLink(&(pdr->brkl), DBR_LONG, &(pdr->brk), 1);
        if (status) return status;
    }

    if (TRIGGERED(RECORD_PWR))
    {
       status = dbPutLink(&(pdr->pwrl), DBR_LONG, &(pdr->pwr), 1);
       if (status) return status;
    }

    if (TRIGGERED(RECORD_MESS))
    {
        status = dbPutLink(&(pdr->msgl), DBR_STRING, pdr->mess, 1);
        if (status) return status;
    }

    if (TRIGGERED(RECORD_BUSY))
    {
        status = dbPutLink(&(pdr->bsyl), DBR_USHORT, &(pdr->busy), 1);
        if (status) return status;
    }


    /*
     *  Clear the link bitmap field via the untrigger macro before returning.
     */

    UNTRIGGER_ALL;
   
    return status;
}
