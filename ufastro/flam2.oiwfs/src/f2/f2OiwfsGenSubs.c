/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <2005>                           (c) <2005>
 * National Research Council            Conseil national de recherches
 * Ottawa, Canada, K1A 0R6              Ottawa, Canada, K1A 0R6
 * All rights reserved                  Tous droits reserves
 *                                      
 * NRC disclaims any warranties,        Le CNRC denie toute garantie
 * expressed, implied, or statu-        enoncee, implicite ou legale,
 * tory, of any kind with respect       de quelque nature que se soit,
 * to the software, including           concernant le logiciel, y com-
 * without limitation any war-          pris sans restriction toute
 * ranty of merchantability or          garantie de valeur marchande
 * fitness for a particular pur-        ou de pertinence pour un usage
 * pose.  NRC shall not be liable       particulier.  Le CNRC ne
 * in any event for any damages,        pourra en aucun cas etre tenu
 * whether direct or indirect,          responsable de tout dommage,
 * special or general, consequen-       direct ou indirect, particul-
 * tial or incidental, arising          ier ou general, accessoire ou
 * from the use of the software.        fortuit, resultant de l'utili-
 *                                      sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * f2OiwfsGenSubs.c
 *
 * PURPOSE:
 * Gensub record support for Flamingo-2 OIWFS probe controller.
 *
 * FUNCTION NAME(S)
 * oiAngles2Position - Convert base and pickoff angles to X,Y position.
 * oiFollowA         - Coordinate OIWFS move commands from the TCS.
 * oiInterlock       - Decode the limit switches to determine interlock.
 * oiOpcRebootInit   - Make VMEbus reset happen whenever reboot is used.
 * oiProbeOffset     - Make Offsets (tracking position errors available
 *
 *
 *INDENT-OFF*
 * $Log: f2OiwfsGenSubs.c,v $
 * Revision 0.1  2005/07/01 17:47:53  drashkin
 * *** empty log message ***
 *
 * Revision 2.0  2005/06/22  bmw
 * Added GIS output to InterlockDecode GenSub
 *
 * Revision 1.9  2005/06/10  bmw
 * Added output to CalcPosition Gensub
 *
 * Revision 1.8  2005/06/06  bmw
 * Added GUI E-stop inputs to oiInterlock
 *
 * Revision 1.7  2005/06/01  bmw
 * Added oiInterlock
 *
 * Revision 1.6  2005/05/20  bmw
 * Added oiRebootInit()
 *
 * Revision 1.5  2005/05/19  bmw
 * Added comments and revision info.
 *
 * Revision 1.4  2005/04/27  bmw
 * Changed FOLLOW_RECENTER from 20 to 200.  Recenter probe while following 
 * once every 200 stream updates (stream updates are at 20 Hz so recenter
 * at 0.1 Hz.
 *
 * Revision 1.3  2004/09/27  bmw
 * Initial F2 revision, copy of Gemini GMOS rev 1.2
 * Changed names to F2, removed *CalcAbsAngles(), revised target checking.
 *
 * Revision 1.2  2003/01/31 14:12:46  gemvx
 * Merged gmos-south
 *
 *INDENT-ON*
 *
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include <stdioLib.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <vxWorks.h>    /* standard VxWorks include */
#include <sysLib.h>     /* for sysReset()           */
#include <rebootLib.h>  /* for rebootHookAdd()      */


#include <dbEvent.h>
#include <dbDefs.h>
#include <recSup.h>
#include <logLib.h>
#include <tickLib.h>
#include <taskLib.h>

#include <genSubRecord.h>

#include <f2OiwfsCalc.h>


/****************************************************************************
 *  --- FollowA gensub record --- 
 *  input field access mnemonics 
 */

#define IN_X_TARGET  *(double *)pgs->a    /* X Position Target              */
#define IN_Y_TARGET  *(double *)pgs->b    /* Y Position Target              */
#define IN_CURPOS    ((double *)pgs->c)   /*(X,Y,Z,R) pos from CalcPosition */

#define IN_FOLLOW      *(long *)pgs->h    /* Current follow Mode            */
#define IN_TOLERANCE *(double *)pgs->i    /* Tolerance (tracking deadband)  */
#define IN_STREAM    ((double *)pgs->j)   /* Input target stream to follow  */

/*
 *  --- FollowA gensub record ---
 *  output field access mnemonics
 */

#define OUT_REJECT      *(long *)pgs->vala  /* Inhibit assembly directive   */
#define OUT_FOLLOW_S    *(long *)pgs->valb  /* Flag to drive followS in SAD */
#define OUT_NEW_TRACK   *(long *)pgs->valc  /* Flag for new trackID         */
#define OUT_REJ_OFFSET  *(long *)pgs->vald  /* Inhibit probe offsets        */
#define OUT_X_MASK    *(double *)pgs->vale  /* X target in Mask frame       */
#define OUT_Y_MASK    *(double *)pgs->valf  /* Y target in Mask frame       */
#define OUT_IN_POSITION *(long *)pgs->valg  /* In Position flag for the SAD */
#define OUT_ARRAY_S     *(long *)pgs->valh  /* Array valid flag for the SAD */
#define OUT_STREAM    ((double *)pgs->valj) /* Probe offset stream out      */


/****************************************************************************
 *  --- probeOffset gensub record --- 
 *  input field access mnemonics
 */

#define IN_OFFSET    ((double *)pgs->a)     /* Probe offset structure in   */

/*
 *  --- probeOffset gensub record --- 
 *  output field access mnemonics
 */

#define OUT_X_OFF    *(double *)pgs->vala   /* X offset in mm               */
#define OUT_Y_OFF    *(double *)pgs->valb   /* Y offset in mm               */
#define OUT_Z_OFF    *(double *)pgs->valc   /* Z offset in mm               */
#define OUT_R_OFF    *(double *)pgs->vald   /* angle offset in mm           */
#define OUT_TRACK_ID *(double *)pgs->vale   /* track ID                     */
#define OUT_OFFSET   ((double *)pgs->valj)  /* Probe offset stream out      */



/****************************************************************************
 *  --- probeInterlockDecode gensub record --- 
 *  input field access mnemonics
 */

#define IN_BAS_P_LIM   *(long *)pgs->a       /* Base positive limit         */
#define IN_BAS_N_LIM   *(long *)pgs->b       /* Base negative limit         */
#define IN_PKO_P_LIM   *(long *)pgs->c       /* Pickoff positive limit      */
#define IN_PKO_N_LIM   *(long *)pgs->d       /* Pickoff negative limit      */
#define IN_PROBE_ESTOP *(long *)pgs->e       /* Probe ESTOP screen  button  */
#define IN_BAS_ESTOP   *(long *)pgs->f       /* Base ESTOP screen button    */
#define IN_PKO_ESTOP   *(long *)pgs->g       /* Pickoff ESTOP screen button */
#define IN_PROBE_ILCK  *(long *)pgs->h       /* Probe interlock             */
#define IN_BAS_ILCK    *(long *)pgs->i       /* Base interlock              */
#define IN_PKO_ILCK    *(long *)pgs->j       /* Pickoff interlock           */

/*
 *  --- probeInterlockDecode gensub record --- 
 *  output field access mnemonics
 */

#define OUT_ILCK_MESS   (char *)pgs->vala    /* Interlock error message     */
#define OUT_ILCK_CODE  *(long *)pgs->valb    /* Interlock error code        */
#define OUT_PROBE_ILCK *(long *)pgs->valc    /* Probe interlock (ILCK) drive*/
#define OUT_PROBE_SDIS *(long *)pgs->vald    /* Probe interlock SDIS        */
#define OUT_BAS_ILCK   *(long *)pgs->vale    /* Base interlock (FLT) drive  */
#define OUT_BAS_SDIS   *(long *)pgs->valf    /* Base interlock SDIS         */
#define OUT_PKO_ILCK   *(long *)pgs->valg    /* Pickoff interlock drive     */
#define OUT_PKO_SDIS   *(long *)pgs->valh    /* Pickoff interlock SDIS      */
#define OUT_GIS        *(long *)pgs->vali    /* Kill switch or GIS activated*/



/****************************************************************************
 *  --- CalcPosition genSub record ---
 *  input field access mnemonics
 */ 

 /* Current angles from deviceControl records                               */
#define IN_B_ENCODER  *(double *)pgs->a    /* Base Angle                    */
#define IN_P_ENCODER  *(double *)pgs->b    /* Pickoff Angle                 */
 /* Large offsets to convert angles from encoder to instrument frame of ref */
#define IN_B_OFFSET   *(double *)pgs->c    /* Base Angle Offset             */
#define IN_P_OFFSET   *(double *)pgs->d    /* Pickoff Angle Offset          */
 /* Correction offsets to convert positions to instrument frame of ref      */
#define IN_X_OFFSET   *(double *)pgs->e    /* X Position Offset             */
#define IN_Y_OFFSET   *(double *)pgs->f    /* Y Position Offset             */
#define IN_Z_OFFSET   *(double *)pgs->g    /* Focal plane focus offset      */

/*
 *  --- CalcPosition genSub record ---
 *  output field access mnemonics
 */

#define OUT_X_CURRENT   *(double *)pgs->vala    /* Current X Position       */
#define OUT_Y_CURRENT   *(double *)pgs->valb    /* Current Y Position       */
#define OUT_Z_CURRENT   *(double *)pgs->valc    /* Current Calculated Focus */
#define OUT_R_CURRENT   *(double *)pgs->vald    /* Current Rotation         */
#define OUT_CURPOS      ((double *)pgs->vale)   /* (X,Y,Z,R)                */
#define OUT_BAS_CURRENT *(double *)pgs->valf    /* Current base stage angle */
#define OUT_PKO_CURRENT *(double *)pgs->valg  /* Current pickoff stage ang. */
#define OUT_XY_INFIELD    *(long *)pgs->valh    /* Is current posn in FOV   */ 


/*
 *  Local Defines
 */

#define OI_PRESET_ANGLE   -2.0              /* target angle for final index */ 
#define CALC_SUCCESS      0                 /* calculation results valid    */
#define CALC_FAILURE      -1                /* calculation results invalid  */   
#define D2R              (M_PI/180.0)       /* Convert degrees to radians   */
#define R2D              (1.0/D2R)          /* Convert radians to degrees   */
#define ARRAY_GOOD        0                 /* Demand stream achievable     */ 
#define ARRAY_INVALID     1                 /* Demand stream not achievable */
#define ARRAY_LATE        2                 /* Demand stream is late        */
#define FOLLOW_STOP       0                 /* Stop following/Moving        */
#define FOLLOW_MOVE       1                 /* Move mode - send target      */
#define FOLLOW_FOLLOW     2                 /* Follow mode                  */
#define FOLLOW_MOVE_R     3                 /* Move mode - read position    */
#define SCAN_DISABLE     -1                 /* Disable record scanning      */
#define SCAN_ENABLE       0                 /* Enable record scanning       */

    /*  
     * The demand stream from the TCS will come into input J of the
     * followA genSub and is expected to be at a rate of about 20Hz.
     * The next two definitions are magic numbers that are assigned
     * to stream_count.  FOLLOW_RECENTER is the number used to
     * reduce number of updates while following and FOLLOW_CENTER is
     * used to show that the previous stream was not used.
     */

#define FOLLOW_RECENTER  200       /* When out of tolerance, recenter once 
                                        every FOLLOW_CENTER stream updates */
#define FOLLOW_RESUME    -1        /* Resume following target from
                                        previous trackID                   */

/*
 *  Misc function prototypes
 */
extern sysReset (void);   /* This should be declared in VxWorks install */
int  oiOpcRebootInit (void);


/*
 *  Gensub record support function prototypes
 */

long oiAngles2Position (genSubRecord *);
long oiFollowA (genSubRecord *);
long oiInterlock (genSubRecord *);
long oiProbeOffset(genSubRecord *);

/*
 *  Global Variables
 */
long    prev_follow;            /* previous follow mode                     */
double  prev_x;                 /* previous x target                        */
double  prev_y;                 /* previous y target                        */
long    stream_count;           /* number of targets since last start cmd   */
int     f2OiwfsFollowDebug = 0; /* debug output level flag                  */
long    newTrack = TRUE;        /* indicates 1st pass with new trackID      */
int     f2IlckDebug = 0;        /* debug output flag                        */

/*
 *  Debug macro to control debugging information output
 */

#define DEBUG(FMT,V)                                                        \
{                                                                           \
        if (f2OiwfsFollowDebug)                                             \
             printf ("%s: "FMT, taskName(0), tickGet(), pgs->name, V);                         \
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiAngles2Position
 *
 * INVOCATION:
 * status = oiAngles2Position (pgs);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pgs (struct genSubRecord *)  Pointer to calling genSub record structure
 *
 * FUNCTION VALUE:
 * (long) processing status.
 *
 * PURPOSE:
 * Convert base and pickoff angles to x/y position 
 *
 * DESCRIPTION:
 * Calculate the actual probe position in the instrument frame of reference using
 * the current base and pickoff stage angles. 
 *
 * All calculation offset parameters are updated first in case they have been
 * adjusted since the last calculation.
 * 
 * Gensub Inputs:
 *    A -> Current Base Angle from deviceControl record
 *    B -> Current Pickoff Angle from deviceControl record
 *    C -> Base Angle Offset from index point to probe coordinate origin 
 *    D -> Pickoff Angle Offset from index point to probe coordinate origin
 *    E -> X Position Offset from index point to probe coordinate origin
 *    F -> Y Position Offset from index point to probe coordinate origin
 *    G -> Z (virtual focus) Offset from index point to probe coordinate origin
 *
 * Gensub outputs:
 *    VALA ->  Current probe X Position in instrument frame of reference 
 *    VALB ->  Current probe Y Position in instrument frame of reference
 *    VALC ->  Current (virtual) focus offset (Z axis)
 *    VALD ->  Current probe frame of reference rotation
 *    VALE ->  Probe position structure (X,Y,Z,R) from the above
 *    VALF ->  Current base stage angle
 *    VALG ->  Current pickoff stage angle
 *    VALH ->  Flag to indicate whether current position is in (0) or out (-1)
 *             of the target field coverage
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * f2OiwfsCalc.c/f2OiwfsSetOffsets()
 * capfast/f2OiwfsCalcPos.sch
 * CP/pv/f2Opc.pv
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiAngles2Position 
(
    struct genSubRecord *pgs        /* calling record structure             */
)
{
    long status;                    /* function return status               */
    double xPos;                    /* calculated probe X position          */
    double yPos;                    /* calculated probe Y position          */
    double zPos;                    /* calculated virtual Z position        */
    double angle;                   /* calculated probe axis rotation angle */


    /*
     *  Update offsets for future oiwfs calculations
     */

    f2OiwfsSetOffsets ( IN_B_OFFSET * D2R, 
                        IN_P_OFFSET * D2R, 
                        IN_X_OFFSET, 
                        IN_Y_OFFSET,
                        IN_Z_OFFSET );

    /*
     *  Reconstruct the current probe position from the base and pickoff
     *  angles reported by the incremental shaft encoders.
     */

    status = f2OiwfsCalculateProbePosition (IN_B_ENCODER * D2R,
                 IN_P_ENCODER * D2R, &xPos, &yPos, &zPos, &angle);


    /*
     *  If the calculation was successful then update the position 
     *  output fields.
     */

    if (status == CALC_SUCCESS);
    {
        OUT_X_CURRENT = xPos;
        OUT_Y_CURRENT = yPos;
        OUT_Z_CURRENT = zPos;
        OUT_R_CURRENT = angle * R2D;
        OUT_BAS_CURRENT = IN_B_ENCODER;
        OUT_PKO_CURRENT = IN_P_ENCODER;

        /*
         * Update the probe position structure.
         */

        OUT_CURPOS[0] = xPos;
        OUT_CURPOS[1] = yPos;
        OUT_CURPOS[2] = zPos;
        OUT_CURPOS[3] = angle;

        /*
         * Is the new current position in an valid position?
         */

        OUT_XY_INFIELD = f2OiwfsCheckTargets (xPos, yPos);
    }

    return (status); 
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiFollowA
 *
 * INVOCATION:
 * status = oiFollowA (pgs);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pgs (struct genSubRecord *)  Pointer to calling genSub record structure
 *
 * FUNCTION VALUE:
 * (long) processing status.
 *
 * PURPOSE:
 * Coordinate OIWFS move commands from the TCS.
 *
 * DESCRIPTION: 
 * Coordinates OIWFS moves using the following algorithm:
 *
 * Read the actual positions
 * For MOVE follow mode:
 *     Change follow mode to MOVE_R
 *     Reject if move targets are invalid
 *     Otherwise send targets to assembly and set up move command
 * For MOVE_R follow mode:
 *     Set inPosition flag if moved to within tolerance of target
 * For FOLLOW follow mode
 *     Read the TCS demand stream
 *     Copy X,Y targets to inputs A & B of GenSub
 *     Note if new track ID
 *     Calculate the current position errors (actuals - targets) 
 *     Reject if X,Y follow targets are invalid
 *     If we're not aleady at the target position (within the tolerance): 
 *         Send the targets to the assembly and set up a move if:
 *             New track ID, or
 *             Resuming tracking, or
 *             Time to recentre.
 *         Otherwise target is valid and still out of position but
 *         no need to drive the assemblyControl record yet.
 *     Else we're already close enough so: 
 *         no move required,
 *         set inPosition flag
 * For any other follow mode (including STOP):
 *     Disable assembly record
 *
 *
 * This gensub function is the main driving engine of the oiwfs probe
 * assembly.  It is processed at a 20 Hz interval by the incoming TCS 
 * target stream.  The record is processed in two ways: 1. when it 
 * receives new a TCS target stream update and 2. when it receives a
 * new move target (specifically the Y position).  The record
 * processes regardless of the following state, however while following 
 * a stream, it initiates motion at most every FOLLOW_RECENTER passes.
 * 
 * The X-Y target (from either the TCS target stream or the explicit move
 * command) is sent in the instrument frame of reference.
 *
 * 
 * Gensub input fields:
 *    A -> X Position Target in the instrument frame of reference
 *    B -> Y Position Target in the instrument frame of reference
 *    C -> Current probe position structure (X,Y,Z,R) from CalcPosition gensub
 *    D -> n/a
 *    E -> n/a
 *    F -> n/a
 *    G -> n/a
 *    H -> Current following mode
 *    I -> Current tolerance (tracking deadband)
 *    J -> Stream of probe target structures generated by the TCS system
 *
 * Gensub output fields:
 *    VALA -> Probe assemblyControl record inhibit flag
 *    VALB -> followS flag (0 == not following; 1 == following)
 *    VALC -> New trackId detected flag (1 = new trackId; 0 = same trackId)
 *    VALD -> Probe offset update inhibit flag
 *    VALE -> X target for oiwfs probe assembly
 *    VALF -> Y target for oiwfs probe assembly
 *    VALG -> Boolean "in-position" flag for the SAD
 *    VALH -> Boolean "target stream valid" flag for the SAD
 *    VALI -> n/a
 *    VALJ -> Probe offset structure out to A&G system
 *
 * EXTERNAL VARIABLES:
 *    prev_follow  - previous follow mode
 *    prev_x       - previous x target
 *    prev_y       - previous y target
 *    stream_count - number of targets since last start cmd
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * capfast/f2OiwfsFollowA.sch
 *
 * DEFICIENCIES:
 * Subsequent moves to the same position are not acted upon unless
 * the follow mode has changed (activeC will not go BUSY/IDLE).
 *-
 ************************************************************************
 */


long oiFollowA
(
    struct genSubRecord *pgs              /* calling record structure       */
)
{
    long status       = CALC_SUCCESS;     /* function return status         */
    long offsetCtrl   = SCAN_DISABLE;     /* disable position updates       */
    long assemblyCtrl = SCAN_DISABLE;     /* disable assemblyControl link   */
    long inPosition   = FALSE;            /* in position flag               */
    long tcsArray     = ARRAY_INVALID;    /* array valid flag               */
    long followMode;                      /* follow mode                    */

    double tcsDemands[6];                 /* demand stream from TCS         */
    double tAppl;                         /* time demand applies            */
    double trackId;                       /* trackID of demand              */
    double xTarget;                       /* target X                       */ 
    double yTarget;                       /* target Y                       */
    double zTarget;                       /* target Z                       */

    double probePosition[4];              /* current probe positions        */
    double xCurrent;                      /* current x position             */
    double yCurrent;                      /* current Y position             */
    double zCurrent;                      /* current Z position             */
    double rCurrent;                      /* current R position             */

    /*
     *  Make a local copy of the current position structure
     */

    memcpy(probePosition, IN_CURPOS, pgs->noc * sizeof(double));

    xCurrent = probePosition[0];
    yCurrent = probePosition[1];
    zCurrent = probePosition[2];
    rCurrent = probePosition[3];


    /*
     *  Further processing depends on the current following mode,
     *  which can be:
     *               0  FOLLOW_STOP    stop following or moving
     *               1  FOLLOW_MOVE    move to given probe target
     *               2  FOLLOW_FOLLOW  follow TCS stream of targets
     *               3  FOLLOW_MOVE_R  check to see if move is in position
     */
    
    followMode = IN_FOLLOW;

    switch (followMode)
    {

        /*
         *  In MOVE mode we are being asked to re-position the probe to
         *  a given X-Y target in the telescope frame of reference.   This
         *  target is given directly - the TCS target stream is ignored.  
         */

        case FOLLOW_MOVE:

            if (prev_follow != FOLLOW_MOVE)
            {
                DEBUG("<%ld> %s: oiFollowA: Move mode%c\n",' ');
            }

            /*
             *  Change to MOVE_R follow mode for subsequent passes as
             *  this first pass checks if the target is valid.
             */

            followMode = FOLLOW_MOVE_R;


            /* 
             *  Set stream count to "resume" on next follow.
             */

            stream_count = FOLLOW_RESUME;



            /*
             *  Is the target within motion limits?  This is more complicated than
             *  simply a rectangular box.
             */
 
            status = f2OiwfsCheckTargets (IN_X_TARGET, IN_Y_TARGET);
            if (status)
            {
                DEBUG("<%ld> %s:oiFollowA: target out of range%c\n", ' ');
                status = CALC_FAILURE;
            }
            else
            {
                /*  New target is within limits  */
                status = CALC_SUCCESS;
            }

            /*
             *  If the transformation was successful (probe can physically
             *  reach the target position) send the transformed target
             *  position to the oiwfs assemblyControl record as its target
             *  position.   Enable the assemblyControl record output link.
             */
                   
            if (status == CALC_SUCCESS)
            {
                DEBUG("<%ld> %s: oiFollowA: Move to new target %c\n",' ');
                OUT_X_MASK = IN_X_TARGET;
                OUT_Y_MASK = IN_Y_TARGET;
                assemblyCtrl = SCAN_ENABLE;
            }

            break;


        /*
         *  In MOVE_R mode the probe position is read to determine if it is
         *  in position for a MOVE command.
         */

        case FOLLOW_MOVE_R:

            /*
             *  If the motion vector is less than the tolerance value then
             *  set the in-position flag.
             */

            if (sqrt((IN_X_TARGET - xCurrent) * (IN_X_TARGET - xCurrent) + 
                          (IN_Y_TARGET - yCurrent) * (IN_Y_TARGET - yCurrent)) < 
                     IN_TOLERANCE                         )
            {
                inPosition = TRUE;
                if (OUT_IN_POSITION == FALSE)  
                    DEBUG("<%ld> %s: oiFollowA: Moved to within tolerance of target%c\n",' ');
            }

            break;

        /*
         *  In FOLLOW mode the probe position is constantly updated to
         *  follow the target stream generated by the TCS system.  Note that
         *  the probe is only physically moved if the position offset is
         *  outside of the current deadband.
         */

        case FOLLOW_FOLLOW:

            if (prev_follow != FOLLOW_FOLLOW)
            {
                DEBUG("<%ld> %s: oiFollowA: Follow mode%c\n",' ');
            }

            /*
             *  Make a local copy of the demand stream now so that it is only 
             *  read once and all members belong to the same array.
             */

            memcpy(tcsDemands, IN_STREAM, pgs->noj * sizeof(double));

            tAppl   = tcsDemands[0];
            trackId = tcsDemands[2];
            xTarget = tcsDemands[3];
            yTarget = tcsDemands[4];
            zTarget = tcsDemands[5];

              /*
              *  Copy the follow targets to the move targets inputs to
        .     *  show updates on the engineering screens.
              */
 
             IN_X_TARGET = xTarget;
             IN_Y_TARGET = yTarget;
             db_post_events(pgs, &IN_X_TARGET, DBE_VALUE);
             db_post_events(pgs, &IN_Y_TARGET, DBE_VALUE);


            /*
             *  Set new track ID flag if new ID differs from old ID
             */

            if ( OUT_STREAM[1] != trackId )
            {
                newTrack = TRUE;
            }

            /* 
             *  Check the newTrack flag here and set output if 
             *  not already done.
             */ 

            if (newTrack == TRUE && OUT_NEW_TRACK == FALSE)
            {
                if (f2OiwfsFollowDebug)
                    printf("oiFollowA: New TrackID:%f\n", trackId);
                OUT_NEW_TRACK = TRUE;
            }

            /*
             *  Calculate the probe offsets by subtracting the current probe
             *  encoder positions from the desired probe position requested by 
	     *  the TCS.  The offsets are then sent to the A&G system as a 
             *  single structure.
             */

            OUT_STREAM[0] = tAppl;
            OUT_STREAM[1] = trackId;
            OUT_STREAM[2] = rCurrent;
            OUT_STREAM[3] = xCurrent - xTarget;
            OUT_STREAM[4] = 0;
            OUT_STREAM[5] = yCurrent - yTarget;
            OUT_STREAM[6] = 0;
            OUT_STREAM[7] = zCurrent;
            OUT_STREAM[8] = 0;

            /* 
             *  Probe offset information is always updated in follow
             *  mode.
             */ 

            offsetCtrl = SCAN_ENABLE;


            /*
             *  Test the target position against the motion limits.  If
             *  requested position is outside of the safe operating area 
             *  then set status to FAIL & stream count to resume on next follow
             */
            status = ( f2OiwfsCheckTargets (xTarget, yTarget) ||
                       zTarget != 0  );
            if (status)
            {
                DEBUG("<%ld> %s:oiFollowA: target out of range%c\n", ' ');
                status = CALC_FAILURE;
		stream_count = FOLLOW_RESUME;
            }

            /*
             *  Target is within limits, so check to see if current position
             *  is within tolerance value.   If it is outside the deadband 
             *  or if the trackID has changed, then move the probe to the 
             *  current target position.
             */

            else if ( ( sqrt((xTarget - xCurrent) * (xTarget - xCurrent) +
                             (yTarget - yCurrent) * (yTarget - yCurrent)  ) > 
                        IN_TOLERANCE) ||
                       newTrack == TRUE                                        )
            {

                /*
                 * Indicate the target stream is valid but, since the probe needs to
                 * be slewed to the target it is likely the probe will be late.
                 */

                tcsArray = ARRAY_LATE;

                /*
                 *  In order to reduce the number of probe motions the target
                 *  is only updated under these conditions:
                 *       o  once every (MAGIC NUMBER) of target stream updates
                 *       o  on first motion request after follow mode has started
                 *       o  when trackID has changed
                 */

                if ( stream_count >= FOLLOW_RECENTER || 
                     stream_count == FOLLOW_RESUME   ||
                     newTrack     == TRUE               )
                {
                    if (newTrack == TRUE)
                    {
                        newTrack = FALSE;
                        DEBUG("<%ld> %s: oiFollowA: Slewing to new trackID target%c\n", ' ');
                    }

                    else if (stream_count == FOLLOW_RESUME)
                    {
                        DEBUG("<%ld> %s: oiFollowA: Slewing to previous trackID target%c\n", ' ');
                    }                    

                    else
                    {
                        DEBUG("<%ld> %s: oiFollowA: Recentering on new target%c\n", ' ');
                    }

                    
                    /*
                     *  Send the target positions to the oiwfs assembly record.
                     *  Enable the assemblyControl record output link
                     *  and reset stream count.
                     */

                    OUT_X_MASK = xTarget;
                    OUT_Y_MASK = yTarget;

                    assemblyCtrl = SCAN_ENABLE;

                    stream_count = 0;
                }

                else 
                {
                    /*
                     *  Target is valid and still out of position but
                     *  no need to drive the assemblyControl record yet, 
                     *  so keep it disabled and increment stream count.
                     */

                    stream_count ++; 
                }
            }


            /*
             *  Otherwise we are in the tolerance region.  Disable the
             *  assemblyControl link. Indicate that we are in position, 
             *  that the target stream is valid and clear the new track output.
             */

            else 
            {
                inPosition = TRUE;
                tcsArray = ARRAY_GOOD;
                if (OUT_NEW_TRACK == TRUE)
                {
                    DEBUG("<%ld> %s: oiFollowA: Following within tolerance%c\n",' ');
                    OUT_NEW_TRACK = FALSE;
                }
            }

            break;

        /*
         *  In STOP mode we do not follow the TCS target stream or update the 
         *  probe offset information.   This is an idle state that waits for
         *  a request to move or to resume TCS position tracking.  Treat any
         *  invalid follow modes as a STOP.
         */

        case FOLLOW_STOP:

            if (prev_follow != FOLLOW_STOP)
            {
                DEBUG("<%ld> %s: oiFollowA: Stop following/moving%c\n",' ');
            }

        default:

            if (followMode != FOLLOW_STOP && followMode != prev_follow)
            {
                DEBUG("<%ld> %s: oiFollowA: ERROR ... invalid follow mode, stop following/moving%c\n",' ');
            }

           /* 
            *  Set stream count to "resume" on next follow.
            */

            stream_count = FOLLOW_RESUME;

            break;
    }       


    /*
     *  Update output flags if they've changed
     */

    if (OUT_IN_POSITION != inPosition)
        OUT_IN_POSITION = inPosition;
    if (OUT_ARRAY_S != tcsArray)
        OUT_ARRAY_S = tcsArray;
    if (OUT_FOLLOW_S != (followMode == 2))
        OUT_FOLLOW_S = (followMode == 2);

    /*
     *  Update scanning control outputs if they've changed
     */

    if (OUT_REJ_OFFSET != offsetCtrl) 
        OUT_REJ_OFFSET = offsetCtrl;
    if (OUT_REJECT != assemblyCtrl)  
        OUT_REJECT = assemblyCtrl;

    /*
     *  Update the input comparison fields so that we know if they have
     *  changed when the next periodic processing happens.
     */

    prev_follow = followMode;
    prev_x = IN_X_TARGET;
    prev_y = IN_Y_TARGET;

    /*
     *  Change the input follow mode if followMode was changed
     *  after a move command.
     */

    if (IN_FOLLOW != followMode)
        IN_FOLLOW  = followMode;

    return (status); 
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiInterlock
 *
 * INVOCATION:
 * status = oiInterlock (pgs);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pgs (struct genSubRecord *)  Pointer to calling genSub record structure
 *
 * FUNCTION VALUE:
 * (long) processing status, always 0
 *
 * PURPOSE:
 * Decode limit switch states to determine interlock status 
 *
 * DESCRIPTION:
 * Reads the current states of the base and pickoff limits (as well as 
 * the ESTOP demands from the user interface screens) to determine
 * what type of interlock if any is present, following this algorithm:
 * 
 * If any of the ESTOP buttons on the user interface have been pressed
 * then set the appropriate interlocks (Probe ESTOP interlocks all Probe
 * as well as Base and Pickoff. 
 *
 * If all four limits are active, assume an interlock from GIS or Kill
 * switch and interlock probe assembly and both stages.
 *
 * If both limits active from either stage, assume thermal interlock
 * (pickoff only) or shorted motor windings and interlock probe assembly
 * and the affected stage (the other is still useable).
 * 
 * All other combinations are simple limits or interlock switches activated.
 * 
 * Write an appropriate interlock error message if ANY switches are active.
 * 
 * Gensub Inputs:
 *    A -> State of Base negative limit (0 okay, 1 limit)
 *    B -> State of Base positive limit (0 okay, 1 limit)
 *    C -> State of Pickoff negative limit (0 okay, 1 limit)
 *    D -> State of Pickoff positive limit (0 okay, 1 limit)
 *    E -> Probe ESTOP screen button (0 okay, 1 interlock)
 *    F -> Base ESTOP screen button (0 okay, 1 interlock)
 *    G -> Pickoff ESTOP screen button (0 okay, 1 interlock)
 *
 * Gensub outputs:
 *    VALA ->  Interlock error message string
 *    VALB ->  Interlock error code
 *    VALC ->  Drives probe assemblyControl record interlock input (ILCK)
 *    VALD ->  Probe interlock scan disable
 *    VALE ->  Drives probeBas deviceControl record interlock input (FLT)
 *    VALF ->  Base interlock scan disable
 *    VALG ->  Drives probePko deviceControl record interlock input (FLT)
 *    VALH ->  Pickoff interlock scan disable
 *    VALI ->  Dedicated output to indicate GIS demand or activated Kill switch
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * Refer electrical schematics to see how the limit states are generated. 
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiInterlock 
(
    struct genSubRecord *pgs        /* calling record structure             */
)
{
    long status = 0;                /* function return status               */
    char interlockMess[MAX_STRING_SIZE]; /* Interlock error message         */
    long interlockErrorCode = 0;    /* Interlock error code                 */
    long probeInterlock = 0;        /* Probe interlock drive                */
    long basInterlock = 0;          /* Base interlock drive                 */
    long pkoInterlock = 0;          /* Pickoff interlock drive              */
    long gisDemand = 0;             /* GIS demand or kill switch activation */

    strncpy ( interlockMess, "", MAX_STRING_SIZE-1 );

    db_post_events(pgs, &IN_BAS_ESTOP, DBE_VALUE);
    db_post_events(pgs, &IN_PKO_ESTOP, DBE_VALUE);
    db_post_events(pgs, &IN_BAS_P_LIM, DBE_VALUE);
    db_post_events(pgs, &IN_BAS_N_LIM, DBE_VALUE);
    db_post_events(pgs, &IN_PKO_P_LIM, DBE_VALUE);
    db_post_events(pgs, &IN_PKO_N_LIM, DBE_VALUE);


    /*
     *  Check the input combinations in a specific order to determine
     *  interlock status
     */
    if ( IN_BAS_P_LIM && IN_BAS_N_LIM && IN_PKO_P_LIM && IN_PKO_N_LIM )
    {
        /*
         *  If all four limits are active, assume a GIS demand or "Kill" 
         *  switch activation.
         */
        interlockErrorCode = 1;
        strncpy ( interlockMess, "Kill switch or GIS demand activated", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and both stages.
         */
        probeInterlock = 1;
        basInterlock =1;
        pkoInterlock =1;

        /*
         *  Set GIS Demand output (used by test scripts).
         */
        gisDemand = 1;
    }
    else if (IN_PROBE_ESTOP )
    {
        /*
         *  The E-Stop button on the Probe assembly GUI has been pressed.
         */
        interlockErrorCode = 2;
        strncpy ( interlockMess, "E-Stop activated on Probe assembly GUI", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and both stages.
         */
        probeInterlock = 1;
        basInterlock =1;
        pkoInterlock =1;
    }
    else if ( IN_PKO_P_LIM && IN_PKO_N_LIM )
    {
        /*
         *  If both Pickoff stage limits are active, assume thermal interlock
         *  or shorted motor windings.
         */
        interlockErrorCode = 3;
        strncpy ( interlockMess, "Pickoff thermal overload or shorted", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and Pickoff stage (Base is still useable).
         */
        probeInterlock = 1;
        pkoInterlock =1;
    }
    else if ( IN_BAS_P_LIM && IN_BAS_N_LIM )
    {
        /*
         *  If both Base stage limits are active, assume shorted motor windings.
         */
        interlockErrorCode = 4;
        strncpy ( interlockMess, "Base motor windings shorted", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and Base stage (Pickoff is still useable).
         */
        probeInterlock = 1;
        basInterlock =1;
    }
    else if ( IN_BAS_ESTOP && IN_PKO_ESTOP )
    {
        /*
         *  The E-Stop buttons on the Base & Pickoff stage GUIs have been pressed.
         */
        interlockErrorCode = 5;
        strncpy ( interlockMess, "E-Stop activated on both stage GUIs", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and both stages.
         */
        probeInterlock = 1;
        basInterlock =1;
        pkoInterlock =1;
    }
    else if ( IN_BAS_ESTOP )
    {
        /*
         *  The E-Stop button on the Base stage GUI has been pressed.
         */
        interlockErrorCode = 6;
        strncpy ( interlockMess, "E-Stop activated on Base stage GUI", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and Base stage (Pickoff is still useable).
         */
        probeInterlock = 1;
        basInterlock =1;
    }
    else if ( IN_PKO_ESTOP )
    {
        /*
         *  The E-Stop button on the Pickoff stage GUI has been pressed.
         */
        interlockErrorCode = 7;
        strncpy ( interlockMess, "E-Stop activated on Pickoff stage GUI", 
                  MAX_STRING_SIZE-1 );
        /*
         *  Interlock probe assembly and Pickoff stage (Base is still useable).
         */
        probeInterlock = 1;
        pkoInterlock =1;
    }
        /*
         *  If one or both stages have a single limit active, create an 
         *  appropriate message but no need to interlock anything.
         */
    else if ( ( IN_BAS_P_LIM || IN_BAS_N_LIM ) && 
              ( IN_PKO_P_LIM || IN_PKO_N_LIM ) )
    {
        interlockErrorCode = 8;
        strncpy ( interlockMess, "Both stages in limits or interlock", 
                  MAX_STRING_SIZE-1 );
    }
    else if ( IN_BAS_P_LIM )
    {
        interlockErrorCode = 9;
        strncpy ( interlockMess, "Base stage in positive limit", 
                  MAX_STRING_SIZE-1 );
    }
    else if ( IN_BAS_N_LIM )
    {
        interlockErrorCode = 10;
        strncpy ( interlockMess, "Base in negative limit or interlock", 
                  MAX_STRING_SIZE-1 );
    }
    else if ( IN_PKO_P_LIM )
    {
        interlockErrorCode = 11;
        strncpy ( interlockMess, "Pickoff stage in positive limit", 
                  MAX_STRING_SIZE-1 );
    }
    else if ( IN_PKO_N_LIM )
    {
        interlockErrorCode = 12;
        strncpy ( interlockMess, "Pickoff in negative limit or interlock", 
                  MAX_STRING_SIZE-1 );
    }

    /*
     *  Update the error outputs if error has changed since last pass.
     */

    if (OUT_ILCK_CODE != interlockErrorCode)
    {
        if (f2IlckDebug ) printf("oiInterlock: New error code:%ld\n", interlockErrorCode);
        /*  Update interlock error code  */
        OUT_ILCK_CODE = interlockErrorCode; 

        /*  Update interlock error message  */
        strncpy ( OUT_ILCK_MESS, interlockMess,  MAX_STRING_SIZE-1 );

        /*
         *  Time to write to the GenSub outputs (but only if they've
         *  changed - to avoid unnecessary record processing at the
         *  assembly and devices).
         */

        if (OUT_PROBE_ILCK != probeInterlock)
        { 
            OUT_PROBE_SDIS = SCAN_ENABLE;    /* Enable Probe interlock drive   */
            if (f2IlckDebug ) 
                printf("oiInterlock: New ILCK state:%ld\n", probeInterlock);
            OUT_PROBE_ILCK = probeInterlock; /* Probe interlock drive          */
        } 
        else OUT_PROBE_SDIS = SCAN_DISABLE;  /* Disable Probe interlock drive  */

        if (OUT_BAS_ILCK != basInterlock)
        {
            OUT_BAS_SDIS = SCAN_ENABLE;      /* Enable Base interlock drive    */
            if (f2IlckDebug ) 
                printf("oiInterlock: New base FLT state:%ld\n", basInterlock);
            OUT_BAS_ILCK = basInterlock;     /* Base interlock drive           */
        }
        else OUT_BAS_SDIS = SCAN_DISABLE;    /* Disable Base interlock drive   */

        if (OUT_PKO_ILCK != pkoInterlock)
        {
            OUT_PKO_SDIS = SCAN_ENABLE;      /* Enable Pickoff interlock drive */
            if (f2IlckDebug ) 
                printf("oiInterlock: New pickoff FLT state:%ld\n", pkoInterlock);
            OUT_PKO_ILCK = pkoInterlock;     /* Pickoff interlock drive        */
        }
        else OUT_PKO_SDIS = SCAN_DISABLE;    /* Disable Pickoff interlock drive*/

        if (OUT_GIS != gisDemand)
        {
            if (f2IlckDebug ) 
                printf("oiInterlock: New GIS demand state:%ld\n", gisDemand);
            OUT_GIS = gisDemand;
        }
    }
    else
    {
        OUT_PROBE_SDIS = SCAN_DISABLE;  /* Disable Probe interlock drive  */
        OUT_BAS_SDIS = SCAN_DISABLE;    /* Disable Base interlock drive   */
        OUT_PKO_SDIS = SCAN_DISABLE;    /* Disable Pickoff interlock drive*/
    }
    return (status); 
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiOpcRebootInit
 *
 * INVOCATION:
 * status = oiOpcRebootInit;
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * 
 *
 * FUNCTION VALUE:
 * (long) processing status: 0, okay  -1, failed
 *
 * PURPOSE:
 * Make a VMEbus reset happen whenever the VxWorks reboot command is used. 
 *
 * DESCRIPTION:
 * Uses the rebootHookAdd function to add sysReset() routine to be called
 * at reboot. 
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * None.
 *
 * DEFICIENCIES:
 * The sysReset() function was added to VxWorks config/mv2700/sysLib.c
 * but was never added to sysLib.h which is why it's declared in this file.
 *-
 ************************************************************************
 */
int oiOpcRebootInit
(
    void
)
{
    if (rebootHookAdd((FUNCPTR)sysReset) != OK)
    {
        return -1;
    }
    return 0;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiProbeOffset
 *
 * INVOCATION:
 * status = oiProbeOffset (pgs); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pgs  (genSub *) Pointer to genSub record structure.
 *
 *
 * FUNCTION VALUE:
 * (long) function return status.
 *
 * PURPOSE:
 * Make Offset info (tracking position errors) available for engineering.
 *
 * DESCRIPTION:
 * Control writing of the probe offset information (the tracking position 
 * errors) into the  A&G offset record.  Processing of this record is 
 * controlled by the FollowA record.  Processing occurs every time a new 
 * offset stream is written, which should be at the same 20 Hz rate the 
 * TCS sends demands to FollowA.  However processing is enabled only when 
 * the oiwfs is actually following the stream.
 *
 * This record also decomposes the probe offset structure into its 
 * component parts so that they can be displayed on engineering screens.
 *
 * Gensub input fields:
 *    A -> Probe offset structure in from followA gensub.
 *
 * Gensub output fields:
 *    VALA -> X offset in mm in instrument frame of reference
 *    VALB -> Y offset in mm in instrument frame of reference
 *    VALC -> Z offset in mm in instrument frame of reference
 *    VALD -> probe angle offset in mm
 *    VALE -> current tracking stream ID
 *    VALJ -> Probe offset structure out
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * capfast/f2OiwfsFollowA.sch
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiProbeOffset
(
    genSubRecord *pgs               /* gensub record structure              */
)
{

    /*
     *  Update the output field values with the current values from
     *  the offset structure.
     */

    OUT_X_OFF = IN_OFFSET[3];
    OUT_Y_OFF = IN_OFFSET[5];
    OUT_Z_OFF = IN_OFFSET[7];
    OUT_R_OFF = IN_OFFSET[2];
    OUT_TRACK_ID = IN_OFFSET[1];

    /*
     *  And copy the whole input structure to the output structure to be
     *  sent to the A&G system.
     */

    OUT_OFFSET[0] = IN_OFFSET[0];
    OUT_OFFSET[1] = IN_OFFSET[1];
    OUT_OFFSET[2] = IN_OFFSET[2];
    OUT_OFFSET[3] = IN_OFFSET[3];
    OUT_OFFSET[4] = IN_OFFSET[4];
    OUT_OFFSET[5] = IN_OFFSET[5];
    OUT_OFFSET[6] = IN_OFFSET[6];
    OUT_OFFSET[7] = IN_OFFSET[7];
    OUT_OFFSET[8] = IN_OFFSET[8];

    return 0;
}
