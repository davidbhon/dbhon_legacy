/*
************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <2005>                       (c) <2005>
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                     
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * <f2OiwfsCadSupport.c>
 *
 * PURPOSE:
 * Provide oiwfs Command Action Directive (CAD) Record support
 *
 * FUNCTION NAME(S)
 * checkBuffer          - evaluate string buffer to determine value type
 * f2SeqCadReboot       - NULL routine (CAD triggers sub record)
 * f2SeqReboot          - Reboots the F2 OPC IOC
 * oiwfsdatumCad        - datum command CAD support 
 * oiwfsDebugCad        - debug command CAD support
 * oiwfsFollowCad       - follow command CAD support       
 * oiwfsinitCad         - init command CAD support
 * oiwfsMoveCad         - move command CAD support
 * oiwfsParkCad         - park command CAD support
 * oiwfsStopCad         - stop command CAD support
 * oiwfstestCad         - test command CAD support
 * oiwfsToleranceCad    - tolerance command CAD support 
 *
 *INDENT-OFF*
 * $Log: f2OiwfsCadSupport.c,v $
 * Revision 0.1  2005/07/01 17:47:53  drashkin
 * *** empty log message ***
 *
 * Revision 1,7  2005/06/29  bmw
 * Changed rejection criteria of park command to more closely match index command.
 *
 * Revision 1.6  2005/06/28  bmw
 * Added f2SeqCadReboot and f2SeqReboot,
 * removed references to probe state (ASTA) of "ERROR"
 *
 * Revision 1.5  2005/05/19  bmw
 * Add comments, revision info, removed oiwfsSimulateCad().
 *
 * Revision 1.4  2004/11/02  bmw
 * Initial F2 revision, copy of Gemini GMOS gmOiwfsCadSupport.c rev 1.3
 * Added oiwfsSimulateCad(), removed masterEnable, changed the way targets
 * are checked.
 *
 * Revision 1.3  2002/04/24 05:14:05  ajf
 * Changes for 3.13.4GEM8.4.
 *
 *
 *INDENT-ON*
 *
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <float.h> 
#include <limits.h>
#include  <rebootLib.h>           /* Access to reboot command               */
#include  <taskLib.h>             /* Access to taskDelay                    */

#include <dbEvent.h>
#include <cadRecord.h>
#include <cad.h>
#include <carRecord.h>
#include  <subRecord.h>           /* For reboot subroutine record           */

#include <assemblyControlRecord.h>
#include <assemblyControl.h>
#include <f2OiwfsCalc.h>          /* Access to f2OiwfsCheckTargets          */

/*
 *  Definitions to provide access to the CAD record input fields
 */

#define STRING_A    (char *) pcr->a        /* CAD A field string value      */
#define STRING_B    (char *) pcr->b        /* CAD B field string value      */
#define STRING_C    (char *) pcr->c        /* CAD C field string value      */
#define STRING_D    (char *) pcr->d        /* CAD D field string value      */
#define STRING_E    (char *) pcr->e        /* CAD E field string value      */
#define STRING_F    (char *) pcr->f        /* CAD F field string value      */
#define STRING_G    (char *) pcr->g        /* CAD G field string value      */
#define STRING_H    (char *) pcr->h        /* CAD H field string value      */
#define STRING_I    (char *) pcr->i        /* CAD I field string value      */
#define STRING_J    (char *) pcr->j        /* CAD J field string value      */
#define STRING_K    (char *) pcr->k        /* CAD K field string value      */


/*
 *  Definitions to provide access to the CAD record output fields
 */

#define STRING_VALA   (char *) pcr->vala   /* CAD VALA field string value   */ 
#define LONG_VALA    *(long *) pcr->vala   /* CAD VALA field long value     */
#define DOUBLE_VALA  *(double *) pcr->vala /* CAD VALA field double value   */

#define STRING_VALB   (char *) pcr->valb   /* CAD VALB field string value   */
#define LONG_VALB    *(long *) pcr->valb   /* CAD VALB field long value     */
#define DOUBLE_VALB  *(double *) pcr->valb /* CAD VALB field double value   */

#define STRING_VALC   (char *) pcr->valc   /* CAD VALC field string value   */
#define LONG_VALC    *(long *) pcr->valc   /* CAD VALC field long value     */
#define DOUBLE_VALC  *(double *) pcr->valc /* CAD VALC field double value   */

#define STRING_VALD   (char *) pcr->vald   /* CAD VALD field string value   */
#define LONG_VALD    *(long *) pcr->vald   /* CAD VALD field long value     */
#define DOUBLE_VALD  *(double *) pcr->vald /* CAD VALD field double value   */

#define LONG_VALE    *(long *) pcr->vale   /* CAD VALE field long value     */
#define DOUBLE_VALE  *(double *) pcr->vale /* CAD VALE field double value   */

#define LONG_VALF    *(long *) pcr->valf   /* CAD VALF field long value     */
#define DOUBLE_VALF  *(double *) pcr->valf /* CAD VALF field double value   */


/*
 *  Local definitions
 */

#define STRTOL_BASE         10      /* conversion base for strtol function  */
#define SCRATCH_BUF_SIZE    64      /* error message formatting buffer size */

#define CMD_IDLE            0       /* System is idle                       */
#define CMD_BUSY            1       /* System is busy executing a command   */
#define CMD_ERROR           2       /* Last command executed failed         */

#define FOLLOW_MODE         2       /* Follow TCS target positions          */
#define MOVE_MODE           1       /* Stop following and move to a position*/
#define STOP_MODE           0       /* Stop following without moving probe  */

#define STOP_ENABLE         0       /* Allow CAD to Stop following          */
#define STOP_DISABLE       -1       /* Don't allow CAD to Stop following    */


/* For CAD input checking */

#define ALL_BLANKS     1       /* all white space                           */
#define SHORT_INTEGER  2       /* short integer                             */
#define LONG_INTEGER   3       /* long integer                              */
#define FLOAT_VALUE    4       /* floating number                           */
#define DOUBLE_VALUE   5       /* double precision number                   */
#define BAD_VALUE      6       /* not fit any of previous 5 classifications */

/*
 *  Function prototypes
 */

long oiwfsDatumCad (struct cadRecord *);
long oiwfsDebugCad (struct cadRecord *);
long oiwfsFollowCad (struct cadRecord *);
long oiwfsInitCad (struct cadRecord *);
long oiwfsMoveCad (struct cadRecord *);
long oiwfsParkCad (struct cadRecord *);
long oiwfsStopCad (struct cadRecord *);
long oiwfsTestCad (struct cadRecord *);
long oiwfsToleranceCad (struct cadRecord *);

/*
 * Internal function prototypes
 */

static long checkBuffer (char *);

/*
 *  Macros
 *
 *  ERROR_MESSAGE transfers the given string, up to the maximum allowable
 *  number of characters, to the CAD record error message (MESS) field. 
 */

#define ERROR_MESSAGE(f)   strncpy(pcr->mess, f, MAX_STRING_SIZE-1)

/*
 *  Public debug control word
 */

int oiCadDebug = 0;             /* enable debug messages (T/F)          */



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * checkBuffer
 *
 * INVOCATION:
 * status = checkBuffer ();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)  
 * (>) null terminated string to check
 *      
 * FUNCTION VALUE:
 * status  (long) 
 *                      ALL_BLANKS, if all white space
 *                      SHORT_INTEGER
 *                      LONG_INTEGER
 *                      FLOAT_VALUE
 *                      DOUBLE_VALUE
 *                      BAD_VALUE, if cannot be converted
 * 
 * PURPOSE:
 * Evaluate buffer to determine value type.
 *
 * DESCRIPTION:
 * Checks the contents of the character buffer passed to see if it
 * holds an integer, double, real, string or is blank.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * limits.h, float.h for limit checking and ctype.h for isspace
 *
 * SEE ALSO:
 * None 
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long checkBuffer( char *buf )
{
    char   *startP;
    char   *endP;
    long   lval;
    double dval;

    startP = buf;

    while (isspace(*startP) )
        startP++;         /* startP refers to first non-whitespace character */
    if (*startP == '\0')
    {
        if (oiCadDebug)
            printf("checkBuffer: CAD input has all blanks\n");
        return ALL_BLANKS;          /* Nothing in the buffer but whitespace  */
    }
    else
    {
        lval = strtol(startP, &endP, STRTOL_BASE);

        if (lval > -LONG_MAX && lval < LONG_MAX && *endP == '\0')
        {
            if (lval >= -INT_MAX  && lval <= INT_MAX)
                return SHORT_INTEGER;          /* buffer holds short integer */
            else
                return LONG_INTEGER;            /* buffer holds long integer */
        }
        else
        {
            dval = strtod (startP, &endP);
            if (dval >= -DBL_MAX && dval <= DBL_MAX && *endP == '\0')
            {
                if (dval >= -FLT_MAX && dval <= FLT_MAX)
                    return FLOAT_VALUE;  /* buffer holds floating-point real */
                else
                    return DOUBLE_VALUE;              /* buffer holds double */
            }
            else
            {
                if (oiCadDebug)
                    printf("checkBuffer: CAD input does not hold integer or real\n");
                return BAD_VALUE;    /* buffer does not hold integer or real */
            }
        }
    }
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * f2SeqCadReboot
 *
 * INVOCATION:
 * status = f2SeqCadReboot (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Reboot the Flamingos-2 OPC IOC
 *
 * DESCRIPTION:
 * Null routine called by reboot CAD record - actual reboot
 * is done by a subroutine record which calls f2SeqReboot().
 * 
 * CAD input field assignments....
 *      A -> 
 *
 * CAD output field assignments
 *      VALA -> 
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * src/f2/f2SeqReboot
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long f2SeqCadReboot (struct cadRecord *pcr)
{

    if (pcr->dir == menuDirectiveMARK)
    {
        printf ("\nThis IOC is preparing to reboot.\n");
    }
    else if (pcr->dir == menuDirectiveCLEAR)
    {
        if (pcr->mark == 1)
	{
            printf ("Reboot canceled.\n");
        }
    }
    else if (pcr->dir == menuDirectiveSTART)
    {
        printf ("Rebooting in 3 seconds...\n");
    }
    return CAD_ACCEPT;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * f2SeqReboot
 *
 * INVOCATION:
 * status = f2SeqReboot (psub); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (!)   psub   (struct subRecord*)  Pointer to subroutine record structure
 *
 * FUNCTION VALUE:
 * (long) command accept flag.
 *
 * PURPOSE:
 * Reboot the Flamingos-2 OPC IOC
 *
 * DESCRIPTION:
 * Called by subroutine record to reboot F2 OPC IOC
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * (VxWorks rebootLib routine) reboot
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long f2SeqReboot (struct subRecord *psub)
{ 
   printf ("*** REBOOTING ***\n");
   taskDelay(60);

   reboot(BOOT_QUICK_AUTOBOOT);
   return CAD_ACCEPT;
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsdatumCad
 *
 * INVOCATION:
 * status = oiwfsdatumCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Index the OIWFS stages
 *
 * DESCRIPTION:
 * For PRESET or START directive
 * {
 *     Convert system action and following states
 *     Reject if:
 *        probe is interlocked,
 *        probe has not been initialised,
 *        Invalid system action State
 *        Invalid system follow State
 *        Busy doing something else (must stop first)
 *        IN follow mode tracking a target stream
 * }
 * For START directive
 * {
 *     Initiate the index by sending an INDEX command 
 *       to the oiwfs assemblyControl record.  
 *     Set the following state to disable following.
 * }
 * 
 * CAD input field assignments....
 *      A -> activeC.VAL
 *      B -> following_mode
 *      C -> OIWFS probe assembly initialized flag (0/1)
 *      D -> OIWFS probe assembly indexed flag (0/1)
 *      E -> OIWFS probe assembly interlocked flag (0/1)
 *      F -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      VALA -> oiwfs assemblyControl record mode field
 *      VALB -> oiwfs assemblyControl record directive field
 *      VALC -> enable STOP_MODE 
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * Not actually capable of stopping the FOLLOW mode, since the command
 * is rejected when following.  It will also not stop a MOVE mode command
 * because the probe will most certainly be BUSY.
 *-
 ************************************************************************
 */

long oiwfsdatumCad 
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char *pEnd;                     /* First non-translatable character     */
    long  actionState;              /* Current system action state          */
    long  followState;              /* Current system following state       */
    long  probeInit;                /* Has probe been initialized?          */
    long  probeIlock;               /* Has probe been interlocked?          */
    char *probeState;               /* Probe assembly state.                */
    char  errorMessage[SCRATCH_BUF_SIZE];  /* Error message                 */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /* 
         *  Preset and start reject the command if it is not safe to execute 
         *  a datum action at this time.
         */
 
        case menuDirectivePRESET:
        case menuDirectiveSTART:

            /*
             * Reject if the probe is interlocked.
             */

            probeIlock = strtol (STRING_E, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }


            /*
             * Reject if the probe has not been initialised.
             */

            probeInit = strtol (STRING_C, &pEnd, STRTOL_BASE);
            if ( probeInit != 1 )
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, probe assembly not initialized.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not initialized");
                return CAD_REJECT;
            }

            /*
             *  Convert system action and following states from string
             *  to long. 
             */

            if (strcmp(STRING_A,"IDLE") == 0)
            {
                actionState = 0;
            }

            else if (strcmp(STRING_A,"BUSY") == 0) 
            {
                actionState = 1;
            }

            else if (strcmp(STRING_A,"ERROR") == 0) 
            {
                actionState = 2;
            }

            else 
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, CAD action state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system action State");
                return CAD_REJECT;
            }

            followState = strtol (STRING_B, &pEnd, STRTOL_BASE);
            if (*pEnd != '\0')
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, CAD follow state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system follow State");
                return CAD_REJECT;
            }


            /*
             * Reject the command if any other command is being 
             * executed or following mode is active.
             */

            if (actionState == CMD_BUSY)
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, Cannot datum while action state BUSY\n", pcr->name);
                ERROR_MESSAGE ("Cannot datum while busy");
                return CAD_REJECT;
            }

            if (followState == 2)
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, Cannot datum while follow state FOLLOWING\n", pcr->name);
                ERROR_MESSAGE ("Cannot datum while following");
                return CAD_REJECT;
            }

            /*
             * Reject if the above checks have been passed but the probe is still BUSY.
             */

            probeState = STRING_F;
            if ( strcmp( probeState, "IDLE") )
            {
                if (oiCadDebug)
                    printf("oiwfsdatumCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }

            /*
             * Preset bails out here before indexing anything.
             */

            if (pcr->dir == menuDirectivePRESET)
            {
                return CAD_ACCEPT;
            }


            /*
             *  Start will re-index the system by sending an INDEX command 
             *  to the oiwfs assemblyControl record.  It will also allow
             *  following state to be stopped.
             */
 
            LONG_VALA = DAR_MODE_INDEX;
            LONG_VALB = DAR_DIR_START;
            LONG_VALC = STOP_ENABLE;

            break;

        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf("oiwfsdatumCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
    
    return CAD_ACCEPT;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsDebugCad
 *
 * INVOCATION:
 * status = oiwfsDebugCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Set the global debug level.
 *
 * DESCRIPTION:
 * For PRESET or START directives:
 * {
 *     Evaluate target debug level string
 *     Reject command if:
 *        string invalid
 *        level out of range
 * }
 * For START directive
 *     Write debug level to output field
 *
 * 
 * CAD input field assignments....
 *      A -> requested target debug level
 *
 * CAD output field assignments
 *      OUTA -> output DBUG level
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfsDebugCad 
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    long debugLevel=DAR_DBUG_NONE;  /* Requested debugging level        */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveSTOP:

            return CAD_ACCEPT;


       /*
        *  CLEAR directive should clear target input
        */

        case menuDirectiveCLEAR:

            strcpy(pcr->a, "");
            db_post_events(pcr, &pcr->a, 1);
            return CAD_ACCEPT;

        /* 
         *  Preset and start reject the command if the requested debugging 
         *  level is not valid. Will accept numbered levels or named in caps.
         */
 
        case menuDirectivePRESET:
        case menuDirectiveSTART:
            
            if ( checkBuffer(pcr->a) == SHORT_INTEGER )
            {
                debugLevel = atoi(pcr->a);
                if (debugLevel < DAR_DBUG_QUIET || debugLevel > DAR_DBUG_MAX)
                {
                    if (oiCadDebug)
                        printf( "oiwfsDebugCad: %s, requested debug level out of range:%ld\n",
                                pcr->name, debugLevel );
                    ERROR_MESSAGE ("Invalid Debug Level - out of range");
                    return CAD_REJECT;
                }
            }
            else if ( strcmp(STRING_A,"QUIET") == 0) 
            {
                debugLevel = DAR_DBUG_QUIET;
            }

            else if ( strcmp(STRING_A,"NONE") == 0) 
            {
                debugLevel = DAR_DBUG_NONE;
            }

            else if (strcmp(STRING_A,"MIN") == 0) 
            {
                debugLevel = DAR_DBUG_MIN;
            }

            else if (strcmp(STRING_A,"FULL") == 0) 
            {
                debugLevel = DAR_DBUG_FULL;
            }

            else if (strcmp(STRING_A,"MAX") ==0 ) 
            {
                debugLevel = DAR_DBUG_MAX;
            }

            else 
            {
                if (oiCadDebug)
                    printf( "oiwfsDebugCad: %s, requested debug level contains bad string:\"%s\"\n", pcr->name, STRING_A);
                ERROR_MESSAGE ("Invalid Debug Level - bad string");
                return CAD_REJECT;
            }
 
            /*
             * Preset bails out here before setting debug level.
             */

            if (pcr->dir == menuDirectivePRESET)
            {
                return CAD_ACCEPT;
            }


            /* 
             *  Start writes the debugging level decoded above to the output 
             *  field VALA.
             */
 
            LONG_VALA = debugLevel;
            return CAD_ACCEPT;


        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf( "oiwfsDebugCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsFollowCad
 *
 * INVOCATION:
 * status = oiwfsFollowCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Enable automatic position tracking
 *
 * DESCRIPTION:
 * For PRESET directive
 * {
 *     Reject if:
 *        Interlocked,
 *        Not initialized,
 *        Not Indexed,
 *        Following not allowed from current state.
 * }
 * For START directive
 *     Set output to enable following
 * 
 * 
 * CAD input field assignments....
 *      A -> OIWFS probe assembly initialized flag (0/1)
 *      B -> OIWFS probe assembly indexed flag (0/1)
 *      C -> OIWFS probe assembly interlocked flag (0/1)
 *      D -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      OUTA -> output following mode
 *      
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfsFollowCad 
(
    struct cadRecord *pcr           /* CAD record structure                 */
)
{
    char *pEnd;                     /* First non-translatable character     */
    long  probeInit;                /* Has probe been initialized?          */
    long  probeIndexed;             /* Has probe been indexed?              */
    long  probeIlock;               /* Has probe been interlocked?          */
    char *probeState;               /* Probe assembly state.                */
    char  errorMessage[SCRATCH_BUF_SIZE];  /* Error message                 */

    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /*
         *  Preset rejects if the probe has not been initialised or indexed, or if
         *  the probe has been interlocked.
         */

        case menuDirectivePRESET:

            /*
             * Reject if the probe has been interlocked.
             */

            probeIlock = strtol (STRING_C, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf( "oiwfsFollowCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }


            /*
             * Reject if the probe has not been initialized.
             */

            probeInit = strtol (STRING_A, &pEnd, STRTOL_BASE);
            if ( probeInit != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsFollowCad: %s, probe assembly not initialized.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not initialized");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe has not been indexed.
             */

            probeIndexed = strtol (STRING_B, &pEnd, STRTOL_BASE);
            if ( probeIndexed != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsFollowCad: %s, probe assembly not indexed.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not indexed");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe is not in state that allows following to start
             * (e.g. following cannot be started while the probe is indexing).
             */

            probeState = STRING_D;
            if ( strcmp( probeState, "IDLE")     &&
                 strcmp( probeState, "MOVING")   &&
                 strcmp( probeState, "TRACKING") &&
                 strcmp( probeState, "STARTING")    )
            {
                if (oiCadDebug)
                    printf( "oiwfsFollowCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }

            break;

        /*
         *  Start sets the VALA field to the tracking enable code.
         */ 

        case menuDirectiveSTART:

            LONG_VALA = FOLLOW_MODE;
            break;

        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf("oiwfsFollowCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
    
    return CAD_ACCEPT;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsinitCad
 *
 * INVOCATION:
 * status = oiwfsinitCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Initialize the OIWFS components
 *
 * DESCRIPTION:
 * For PRESET or START directives:
 * {
 *     Translate action and following states
 *     Reject if:
 *        Interlocked,
 *        Action state is invalid
 *        Currently following
 * }
 * For START directive:
 * {
 *     Initiate initialization by sending INIT command to assembly
 *     Don't disable following
 * }
 * 
 * CAD input field assignments....
 *      A -> activeC.VAL
 *      B -> following_mode
 *      C -> OIWFS probe assembly initialized flag (0/1)
 *      D -> OIWFS probe assembly indexed flag (0/1)
 *      E -> OIWFS probe assembly interlocked flag (0/1)
 *      F -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      VALA -> oiwfs assemblyControl record mode field
 *      VALB -> oiwfs assemblyControl record directive field
 *      VALC -> disable STOP_MODE
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfsinitCad 
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char *pEnd;                     /* First non-translatable character     */
    long  actionState;              /* Current system action state          */
    long  followState;              /* Current system following state       */
    long  probeIlock;               /* Has probe been interlocked?          */
    char *probeState;               /* Probe assembly state.                */
    char  errorMessage[SCRATCH_BUF_SIZE];  /* Error message                 */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
 
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /*
         *  Preset and start will reject the command if the system is busy
         */

        case menuDirectivePRESET:
        case menuDirectiveSTART:

            /*
             * Reject if the probe has been interlocked.
             */

            probeIlock = strtol (STRING_E, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }

            /*
             *  Translate action and following states
             */

            if (strcmp(STRING_A,"IDLE") == 0)
            {
                actionState = 0;
            }

            else if (strcmp(STRING_A,"BUSY") == 0) 
            {
                actionState = 1;
            }

            else if (strcmp(STRING_A,"ERROR") == 0) 
            {
                actionState = 2;
            }
            else 
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, CAD action state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system action State");
                return CAD_REJECT;
            }

            followState = strtol (STRING_B, &pEnd, STRTOL_BASE);
            if (*pEnd != '\0')
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, CAD follow state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system follow State");
                return CAD_REJECT;
            }


            /*
             * Reject the command if the system is busy or is in following 
             * mode
             */

            if (actionState == CMD_BUSY)
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, Cannot init while action state BUSY\n", pcr->name);
                ERROR_MESSAGE ("Cannot init while busy");
                return CAD_REJECT;
            }

            else if (followState == 2)
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, Cannot init while follow state FOLLOWING\n", pcr->name);
                ERROR_MESSAGE ("Cannot init while following");
                return CAD_REJECT;
            }

            /*
             * Reject if the above checks have been passed but the probe is still BUSY.
             */

            probeState = STRING_F;
            if ( strcmp( probeState, "IDLE") )
            {
                if (oiCadDebug)
                    printf("oiwfsinitCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }


            /*
             * Preset bails out here before initializing anything.
             */

            if (pcr->dir == menuDirectivePRESET)
            {
                return CAD_ACCEPT;
            }


            /*
             *  Start will initialize the system by sending an INIT command 
             *  to the oiwfs assemblyControl record.
             */

            LONG_VALA = DAR_MODE_INIT;
            LONG_VALB = DAR_DIR_START;
            LONG_VALC = STOP_DISABLE;

            break;

        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf("oiwfsinitCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsMoveCad
 *
 * INVOCATION:
 * status = oiwfsMoveCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Position the OIWFS probe to the given coordinates
 *
 * DESCRIPTION:
 * For PRESET or START directive
 * {
 *     Reject if:
 *        Interlocked,
 *        Not Initialized,
 *        Not Indexed,
 *        Invalid X or Y targets.
 * }
 * For START directive
 * {
 *     Send MOVE command to assembly
 *     Put followState in MOVE mode.
 * }
 * 
 *
 * CAD input field assignments....
 *      A -> requested X position
 *      B -> requested Y position
 *
 *      G -> OIWFS probe assembly initialized flag (0/1)
 *      H -> OIWFS probe assembly indexed flag (0/1)
 *      I -> OIWFS probe assembly interlocked flag (0/1)
 *      J -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      VALA -> target object X position
 *      VALB -> target object Y position
 *      VALC -> following mode
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfsMoveCad 
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char  *pEnd;                    /* First non-translatable character     */
    double x_target;                /* Translated X target position         */
    double y_target;                /* Translated Y target position         */

    long   probeInit;               /* Has probe been initialized?          */
    long   probeIndexed;            /* Has probe been indexed?              */
    long   probeIlock;              /* Has probe been interlocked?          */
    char  *probeState;              /* Probe assembly state.                */
    char   errorMessage[SCRATCH_BUF_SIZE];  /* Error message                */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveSTOP:
            return CAD_ACCEPT;

       /*
        *  CLEAR directive should clear target inputs
        */

        case menuDirectiveCLEAR:

            strcpy(pcr->a, "");
            db_post_events(pcr, &pcr->a, 1);
            strcpy(pcr->b, "");
            db_post_events(pcr, &pcr->b, 1);
            return CAD_ACCEPT;

        /*
         *  Preset or start will reject the command if the input arguments 
         *  can not be parsed into double values or if the requested probe 
         *  position is out of range.
         */

        case menuDirectivePRESET:
        case menuDirectiveSTART:

            /*
             * Reject if the probe has been interlocked.
             */

            probeIlock = strtol (STRING_I, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }


            /*
             * Reject if the probe has not been initialized.
             */

            probeInit = strtol (STRING_G, &pEnd, STRTOL_BASE);
            if ( probeInit != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, probe assembly not initialized.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not initialized");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe has not been indexed.
             */

            probeIndexed = strtol (STRING_H, &pEnd, STRTOL_BASE);
            if ( probeIndexed != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, probe assembly not indexed.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not indexed");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe is not in state that allows it to be moved.
             */

            probeState = STRING_J;
            if ( strcmp( probeState, "IDLE")     &&
                 strcmp( probeState, "MOVING")   &&
                 strcmp( probeState, "TRACKING") &&
                 strcmp( probeState, "STARTING")    )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }

            /*
             * Convert all input attributes to double values.
             */

            /* Check X target attribute */ 
            if ( checkBuffer(pcr->a)== BAD_VALUE || 
                 checkBuffer(pcr->a) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, requested X target contains bad string:\"%s\"\n", pcr->name, STRING_A );
                ERROR_MESSAGE ("Invalid X target - bad string");
                return CAD_REJECT;
            }
            else
	    {
                x_target = strtod (STRING_A, &pEnd);
            }

            /* Check Y target attribute */ 
            if ( checkBuffer(pcr->b)== BAD_VALUE || 
                 checkBuffer(pcr->b) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsMoveCad: %s, requested Y target contains bad string:\"%s\"\n", pcr->name, STRING_B );
                ERROR_MESSAGE ("Invalid Y target - bad string");
                return CAD_REJECT;
            }
            else
	    {
                y_target = strtod (STRING_B, &pEnd);
            }




            /*
             * Make sure the targets are within the limits
             */

            if (f2OiwfsCheckTargets (x_target, y_target))
            {
                if (oiCadDebug)
                      printf("oiwfsMoveCad: %s, probe target out of range\n", pcr->name);
                ERROR_MESSAGE ("Probe target out of range");
                return CAD_REJECT;
            }

            /*
             * Preset bails out here before moving anything.
             */

            if (pcr->dir == menuDirectivePRESET)
            {
                return CAD_ACCEPT;
            }


            /*
             * Start outputs the target position and sets the following mode
             * to the move code.
             */

            DOUBLE_VALA = x_target;
            DOUBLE_VALB = y_target;
            LONG_VALC = MOVE_MODE;

            return CAD_ACCEPT;


        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf( "oiwfsMoveCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
    
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsParkCad
 *
 * INVOCATION:
 * status = oiwfsParkCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long) command accept/reject flag.
 *
 * PURPOSE:
 * Place the probe in a safe position for shutdown
 *
 * DESCRIPTION:
 * For PRESET directive:
 * {
 *     Reject if:
 *        Interlocked,
 *        Not Initialized,
 *        Not Indexed,
 *        Cannot park from current state,
 * }
 * For START directive
 * {
 *     Send PARK command to assembly
 *     Stop following.
 * }
 *
 * 
 * CAD input field assignments....
 *      A -> OIWFS probe assembly initialized flag (0/1)
 *      B -> OIWFS probe assembly indexed flag (0/1)
 *      C -> OIWFS probe assembly interlocked flag (0/1)
 *      D -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      VALA -> oiwfs assemblyControl record mode field
 *      VALB -> oiwfs assemblyControl record directive field
 *      VALC -> following mode
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None known.
 *
 *-
 ************************************************************************
 */

long oiwfsParkCad 
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char *pEnd;                     /* First non-translatable character     */
    long  followState;              /* Current system following state       */
    long  probeInit;                /* Has probe been initialized?          */
    long  probeIndexed;             /* Has probe been indexed?              */
    long  probeIlock;               /* Has probe been interlocked?          */
    char *probeState;               /* Probe assembly state.                */
    char  errorMessage[SCRATCH_BUF_SIZE];  /* Error message                 */

    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /*
         *  Preset rejects if the probe has not been initialised or indexed, or if
         *  the probe has been interlocked.
         */

        case menuDirectivePRESET:

            /*
             * Reject if the probe has been interlocked.
             */

            probeIlock = strtol (STRING_C, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf( "oiwfsParkCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }


            /*
             * Reject if the probe has not been initialized.
             */

            probeInit = strtol (STRING_A, &pEnd, STRTOL_BASE);
            if ( probeInit != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsParkCad: %s, probe assembly not initialized.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not initialized");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe has not been indexed.
             *
             * This is not really necessary in the current implementation
             * of the assembly code where a PARK command is actually
             * translated into an INDEX command, but we'll leave it in
             * because a Park will be rejected at the assembly level 
             * unless indexed. Rather than make a special case in the
             * assembly code to allow a park, we'll just reject it here.
             */

            probeIndexed = strtol (STRING_B, &pEnd, STRTOL_BASE);
            if ( probeIndexed != 1 )
            {
                if (oiCadDebug)
                    printf( "oiwfsParkCad: %s, probe assembly not indexed.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe not indexed");
                return CAD_REJECT;
            }

            /*
             * Read the current following mode and reject if invalid
             */
 
            followState = strtol (STRING_E, &pEnd, STRTOL_BASE);
            if (*pEnd != '\0')
            {
                if (oiCadDebug)
                    printf("oiwfsParkCad: %s, CAD follow state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system follow State");
                return CAD_REJECT;
            }


            /*
             * Reject the command if Prove Tracking Controller is in following mode.
             */

            if (followState == 2)
            {
                if (oiCadDebug)
                    printf("oiwfsParkCad: %s, Cannot Park while follow state FOLLOWING\n", pcr->name);
                ERROR_MESSAGE ("Cannot park while following");
                return CAD_REJECT;
            }

            /*
             * Reject if the probe is not in state that allows it to be parked.
             */

            probeState = STRING_D;
            if ( strcmp( probeState, "IDLE")  )
            {
                if (oiCadDebug)
                    printf( "oiwfsParkCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start parks the stage by issuing the PARK command to the
         *  oiwfs assemblyControl record.  The stop following command
         *  is issued as well to ensure that following mode is cancelled.
         */

        case menuDirectiveSTART:

            LONG_VALA = DAR_MODE_PARK;
            LONG_VALB = DAR_DIR_START;
            LONG_VALC = STOP_MODE;

            break;


        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf( "oiwfsParkCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsStopCad
 *
 * INVOCATION:
 * status = oiwfsStopCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long)  command accept/reject flag.
 *
 * PURPOSE:
 * Stop any current motion (track, index, park or move).
 *
 * DESCRIPTION:
 * For START directive:
 * {
 *     Send Stop to followState
 * }
 *
 * 
 * CAD input field assignments
 *      None
 *
 * CAD output field assignments
 *      VALA -> following mode
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfsStopCad
(
    struct cadRecord *pcr           /* CAD record structure */
)
{

    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /*
         *  Preset always accepts a park request!!!
         */

        case menuDirectivePRESET:

            break;


        /*
         *  Start cancels the following action by sending the STOP command
         *  to the following system.
         */

        case menuDirectiveSTART:

            LONG_VALA = STOP_MODE;

            break;


        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf( "oiwfsStopCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
    
    return CAD_ACCEPT;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfstestCad
 *
 * INVOCATION:
 * status = oiwfstestCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long)  command accept/reject flag.
 *
 * PURPOSE:
 * Check to ensure that all sub-systems are functioning normally without
 * actually moving anything.
 *
 * DESCRIPTION:
 * For PRESET or START directive:
 * {
 *     Reject if:
 *        Interlocked
 *        Invalid action state
 *        Invalid follow state
 *        Busy following
 *        Busy doing something else
 * }
 * For START directive
 *     Send the TEST command to the assembly
 *     Don't prevent following
 *
 * 
 * CAD input field assignments....
 *      A -> activeC.VAL
 *      B -> following_mode 
 *      C -> OIWFS probe assembly initialized flag (0/1)
 *      D -> OIWFS probe assembly indexed flag (0/1)
 *      E -> OIWFS probe assembly interlocked flag (0/1)
 *      F -> OIWFS probe assembly state (IDLE, INITIALIZING, MOVING, etc.)
 *
 * CAD output field assignments
 *      VALA -> oiwfs assemblyControl record mode field
 *      VALB -> oiwfs assemblyControl record directive field
 *      VALC -> STOP_MODE
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long oiwfstestCad
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char *pEnd;                     /* First non-translatable character     */
    long  actionState;              /* Current system action state          */
    long  followState;              /* Current system following state       */
    long  probeIlock;               /* Has probe been interlocked?          */
    char *probeState;               /* Probe assembly state.                */
    char  errorMessage[SCRATCH_BUF_SIZE];  /* Error message                 */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, CLEAR & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveCLEAR:
        case menuDirectiveSTOP:
            break;

        /* 
         *  Preset and start reject the command if it is not safe to execute 
         *  a test action at this time.
         */
 
        case menuDirectivePRESET:
        case menuDirectiveSTART:

            /*
             * Reject if the probe has been interlocked.
             */

            probeIlock = strtol (STRING_E, &pEnd, STRTOL_BASE);
            if ( probeIlock != 0 )
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, probe assembly interlocked.\n", pcr->name);
                ERROR_MESSAGE ("OIWFS probe interlocked");
                return CAD_REJECT;
            }

            /*
             *  Convert system action and following states from 
             *  string to long. 
             */

            if (strcmp(STRING_A,"IDLE") == 0)
            {
                actionState = 0;
            }

            else if (strcmp(STRING_A,"BUSY") == 0) 
            {
                actionState = 1;
            }

            else if (strcmp(STRING_A,"ERROR") == 0) 
            {
                actionState = 2;
            }

            else 
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, CAD action state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system action State");
                return CAD_REJECT;
            }

            followState = strtol (STRING_B, &pEnd, STRTOL_BASE);
            if (*pEnd != '\0')
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, CAD follow state input is invalid\n", pcr->name);
                ERROR_MESSAGE ("Invalid system follow State");
                return CAD_REJECT;
            }


            /*
             *  Reject the command if any other command is being 
             *  executed or following mode is active.
             */

            if (actionState == CMD_BUSY)
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, Cannot test while action state BUSY\n", pcr->name);
                ERROR_MESSAGE ("Cannot test while busy");
                return CAD_REJECT;
            }

            if (followState == 2)
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, Cannot test while follow state FOLLOWING\n", pcr->name);
                ERROR_MESSAGE ("Cannot test while following");
                return CAD_REJECT;
            }

            /*
             * Reject if the above checks have been passed but the probe is still BUSY.
             */

            probeState = STRING_F;
            if ( strcmp( probeState, "IDLE") )
            {
                if (oiCadDebug)
                    printf("oiwfstestCad: %s, probe assembly is %s.\n", pcr->name, probeState);
                sprintf( errorMessage, "OIWFS probe is %s", probeState);
                ERROR_MESSAGE (errorMessage);
                return CAD_REJECT;
            }

            /*
             * Preset bails out here before testing anything.
             */

            if (pcr->dir == menuDirectivePRESET)
            {
                return CAD_ACCEPT;
            }


            /* 
             *  Start will test the system by sending a TEST command to 
             *  the oiwfs assemblyControl record.  
             */
 
            LONG_VALA = DAR_MODE_TEST;
            LONG_VALB = DAR_DIR_START;
            LONG_VALC = STOP_DISABLE;

            break;

        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf("oiwfstestCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
      }

    return CAD_ACCEPT;
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiwfsToleranceCad
 *
 * INVOCATION:
 * status = oiwfsToleranceCad (pcr); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pcr  (struct cadRecord *) Pointer to calling CAD record.
 *
 * FUNCTION VALUE:
 * (long)  command accept/reject flag.
 *
 * PURPOSE:
 * Set the probe motion deadband.
 *
 * DESCRIPTION:
 * For PRESET directive:
 * {
 *     Reject if
 *        Invalid X/Y/Z tolerance values
 *        Invalid tolerance range
 *     Output new tolerance (controlled by start link in CapFast)
 * }
 * 
 * 
 * CAD input field assignments....
 *      A -> target X & Y tolerance
 *      B -> target Z tolerance
 *      C -> minimum allowable tolerance
 *      D -> maximum allowable tolerance
 *
 * CAD output field assignments
 *      VALA -> output tolerance
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * Attribute B (target Z tolerance) is always assumed to be 0
 * and is not checked in any way.
 *-
 ************************************************************************
 */

long oiwfsToleranceCad
(
    struct cadRecord *pcr           /* CAD record structure */
)
{
    char *pEnd;                     /* First non-translatable character */
    double deadband;                /* oiwfs X/Y tolerance              */
    double minD;                    /* minimum allowable                */
    double maxD;                    /* maximum allowable                */
    double deadbandZ;               /* oiwfs Z tolerance                */


    /*
     *  Processing depends on the state of the directive field as follows:
     */

    switch (pcr->dir)
    {
       /*
        *  Accept MARK, START & STOP directives, but do nothing
        */

        case menuDirectiveMARK:
        case menuDirectiveSTART:
        case menuDirectiveSTOP:
            break;

       /*
        *  CLEAR directive should clear target inputs
        */

        case menuDirectiveCLEAR:

            strcpy(pcr->a, "");
            db_post_events(pcr, &pcr->a, 1);
            strcpy(pcr->b, "");
            db_post_events(pcr, &pcr->b, 1);
            return CAD_ACCEPT;

        /* 
         *  Preset or start rejects the command if the inputs are not valid.
         */
 
        case menuDirectivePRESET:

            /*
             *  Reject the command if any of the inputs can not be converted
             *  from sting to double values.
             */

            /* Check X/Y Tolerance Target  attribute */ 
            if ( checkBuffer(pcr->a)== BAD_VALUE || 
                 checkBuffer(pcr->a) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, requested X/Y tolerance contains bad string:\"%s\"\n", pcr->name, STRING_A );
                ERROR_MESSAGE ("Invalid X/Y tolerance - bad string");
                return CAD_REJECT;
            }
            else
	    {
                deadband = strtod (STRING_A, &pEnd);
            }

            /* Check Z Tolerance attribute */ 
            if ( checkBuffer(pcr->b)== BAD_VALUE || 
                 checkBuffer(pcr->b) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, requested Z target contains bad string:\"%s\"\n", pcr->name, STRING_B );
                ERROR_MESSAGE ("Invalid Z tolerance - bad string");
                return CAD_REJECT;
            }
            else
	    {
                deadbandZ = strtod (STRING_B, &pEnd);
            }


            /* Check X/Y minimum tolerance range attribute */ 
            if ( checkBuffer(pcr->c)== BAD_VALUE || 
                 checkBuffer(pcr->c) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, requested X/Y minimum range contains bad string:\"%s\"\n", pcr->name, STRING_C );
                ERROR_MESSAGE ("Invalid X/Y Tolerance Min - bad string");
                return CAD_REJECT;
            }
            else
	    {
                minD = strtod (STRING_C, &pEnd);
            }

            /* Check X/Y maximum tolerance range attribute */ 
            if ( checkBuffer(pcr->d)== BAD_VALUE || 
                 checkBuffer(pcr->d) == ALL_BLANKS )
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, requested X/Y maximum range contains bad string:\"%s\"\n", pcr->name, STRING_D );
                ERROR_MESSAGE ("Invalid X/Y Tolerance Max - bad string");
                return CAD_REJECT;
            }
            else
	    {
                maxD = strtod (STRING_D, &pEnd);
            }


            /*
             * Reject the command if the given deadband is outside limits
             */

            if ((deadband < minD) || (deadband > maxD))
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, requested Tolerance out of range:%f\n", pcr->name, deadband );
                ERROR_MESSAGE ("X/Y Tolerance out of range");
                return CAD_REJECT;
            }

            /*
             * Give warning if the given Z deadband is not == 0
             */

            if (deadbandZ != 0)
            {
                if (oiCadDebug)
                    printf( "oiwfsToleranceCad: %s, Warning - Z Tolerance assumed to be 0:%f\n", pcr->name, deadbandZ );
            }


            /*
             *  All is okay so update the following system deadband value.
             *
             *  Keep in mind that the routing of this output must be
             *  controlled by the STLK link.  See the CapFast schematic
             *  f2OiwfsTolCad.sch to see why this CAD is handled differently
             *  from the rest.
             */

            DOUBLE_VALA = deadband;

            break;

        /*
         *  Invalid directives are rejected.
         */

        default:

            if (oiCadDebug)
                printf( "oiwfsToleranceCad: %s, invalid directive: %d\n", pcr->name, pcr->dir);
            ERROR_MESSAGE ("Invalid directive");
            return CAD_REJECT;
    }
    
    return CAD_ACCEPT;
}
