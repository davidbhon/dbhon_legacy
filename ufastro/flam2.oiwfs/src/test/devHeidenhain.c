/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2000                         (c) 2000
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME: devHeidenhain.c
 *
 * PURPOSE: Performs an initialisation of and output of
 *          a stream of encoder values of the Heidenhainn
 *          encoder readout.
 *
 *          There are three sensors that can be attached to
 *          the readout. A serial port permits an external
 *          device to request the three current encoder positions.
 *
 *          It is up to the user to configure the readout to have
 *          some basic parameters set. These include:
 *             1) The baud rate of 19200
 *
 *INDENT-OFF*
 * $Log: devHeidenhain.c,v $
 * Revision 0.1  2005/07/01 17:48:40  drashkin
 * *** empty log message ***
 *
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#include "vxWorks.h"
#include "stdio.h"
#include "ioLib.h"
#include "sioLib.h"
#include "taskLib.h"
#include "sysLib.h"
#include "timers.h"
#include "devBc635.h"	/* for the high-speed time access */
#include "devHeidenhain.h"


/*
 *  Local Defines
 */

#define HEIDEN_DEBUG     /* undef to stop debugging info to console */
#define HEIDEN_BAUD_RATE     38400
#define HEIDEN_GET_BYTE      "\02"



/*
 *  Data Structures
 */

static int  serialPortD = 0;        /* handle to the serial port */
static int  heidenInBytes = 0;    /* counter for input data size */
static int  heidenInitF = FALSE;  /* initialised flag */


/*
 *  Functions
 */

int heidenInit(int);
int heidenRd(int, int);
float heidenRdOne(int);
int heidenRdAll(double *, double *, double *, double *);



/*
 ************************************************************************
 *+
 * EXPORTED FUNCTION: heidenInit(portNum)
 *
 * RETURNS: int, 0 for OK
 *
 * DESCRIPTION: Call initialises the serial port and checks to make
 *              sure that a reasonable response is received.
 *
 * NOTES:       The baud rate must be set on the readout unit to
 *              19200. If not, this call will fail
 *-
 ************************************************************************
 */

int heidenInit
(
    int		inPort         /* serial port the readout connected to */
)
{
    char    portName[20];       /* buffer to construct serial port name */
    int     status;             /* function return codes */
    char    inBuf[120] = " ";   /* buffer for input data */

/*
 * First construct then initialise the serial port.
 */

    if ((inPort < 1) || (inPort > 3))
    {
        return(HEIDEN_INVLD_PORT);
    }

    sprintf(portName, "/tyCo/%i", (char)inPort);    

    if ((serialPortD = open(portName, O_RDWR, 0)) == ERROR)
    {
        return(HEIDEN_PORT_OPEN_ERR);
    }


/*
 * Set the modes of the serial port. Raw,
 * 9600, 7 data, 1 start, 2 stop, even parity
 */

    status = ioctl(serialPortD, FIOSETOPTIONS, OPT_RAW);
    if (status !=  0)
    {
        return(HEIDEN_SET_PORT_ERR);
    }

    status = ioctl(serialPortD, FIOBAUDRATE, HEIDEN_BAUD_RATE);
    if (status !=  0)
    {
        return(HEIDEN_SET_BAUD_ERR);
    }

    status = ioctl(serialPortD, 
                SIO_HW_OPTS_SET, 
                CREAD | CS7 | STOPB | PARENB);
    if (status !=  0)
    {
        return(HEIDEN_SET_OPTS_ERR);
    }

   
/*
 * Empty out input/output buffers and retrieve a test reading
 */ 

    status = ioctl(serialPortD, FIOFLUSH, 0);
    if (status !=  0)
    {
        return(HEIDEN_FLUSH_ERR);
    }

    if (write(serialPortD, HEIDEN_GET_BYTE, 1) != 1)
    {
        return(HEIDEN_WRITE_ERR);
    }

    taskDelay(sysClkRateGet() / 10);
    status = ioctl(serialPortD, FIONREAD, (int)&heidenInBytes);
    if ((status !=  0) || (heidenInBytes <= 0))
    {
        return(HEIDEN_GET_BYTE_ERR);
    }
    
    if (read(serialPortD, inBuf, heidenInBytes) != heidenInBytes)
    {
        return(HEIDEN_READ_ERR);
    }

#ifdef HEIDEN_DEBUG
    printf("%s\n", inBuf);
#endif


/*
 * Initialise the Bancomm time card
 */

    if (bc635Init(BC635_SOURCE_HW, NULL) != BC635_SOURCE_HW)
    {
        return(HEIDEN_TIME_ERR);
    }

    heidenInitF = TRUE;
    return(OK);
}




/*
 ************************************************************************
 *+
 * EXPORTED FUNCTION: heidenRd(sampleCnt, sampleRate)
 *
 * RETURNS: int, number of successfull iterations, -tive for errors
 *
 * DESCRIPTION: Take this many samples at this rate
 *
 *-
 ************************************************************************
 */

int heidenRd
(
    int		sampleCnt,      /* take this many samples */
    int     sampleRate      /* at this rate */
)
{
int     sampleNum;      /* loop index */
char    inBuf[120];     /* Buffer for returned data */    
int     delayCnt;       /* how many OS ticks to delay between reads */
int     inCnt;          /* how many bytes actually read */
double  xVal, yVal, zVal;   /* the interpreted values */
double	bcTime;         /* Time from the Bancomm card */
char    *tmpP;          /* tmp pointer while extracting X, Y & Z */


/*
 * Make sure module initialised
 */
    if (heidenInitF != TRUE)
    {
        return(HEIDEN_NOT_INIT_ERR);
    }


/*
 * figure out how many ticks to wait each time
 */

    delayCnt = sysClkRateGet() / sampleRate;

/*
 * Just loop through. Send 'sample command', read time, pause for
 * appropriate delay, then read reply 
 */

    for (sampleNum = 0; sampleNum < sampleCnt; sampleNum++)
    {
        /* 
	 * send 'sample command', sample Bancomm time, then delay and
	 * get bytes.
	 */

        if (write(serialPortD, HEIDEN_GET_BYTE, 1) != 1)
        {
            return(HEIDEN_WRITE_ERR);
        }

	bcTime = bc635RawTime();
        
	taskDelay(delayCnt);

        if ((inCnt = read(serialPortD, inBuf, heidenInBytes)) != heidenInBytes)
        {
            return(sampleNum);
        }

        /*
         * format the values
         */

        tmpP = inBuf;
        while (*tmpP != 'X') tmpP++;
        tmpP += 3;
        xVal = atof(tmpP);

        tmpP = inBuf;
        while (*tmpP != 'Y') tmpP++;
        tmpP += 3;
        yVal = atof(tmpP);

        tmpP = inBuf;
        while (*tmpP != 'Z') tmpP++;
        tmpP += 3;
        zVal = atof(tmpP);

        printf("%4i %12.2f %7.3f %7.3f %7.3f\n", 
				sampleNum, bcTime, xVal, yVal, zVal);


    } /* for() */


    return(sampleNum);
        
} /* heidenRd */



/*
 ************************************************************************
 *+
 * EXPORTED FUNCTION: heidenRdOne(axis)
 *
 * RETURNS: int, number of successfull iterations, -tive for errors
 *
 * DESCRIPTION: Return the encoder value for a single encoder
 *
 *-
 ************************************************************************
 */

float heidenRdOne
(
    int	    axis      /* return value for this axis */
)
{
char    inBuf[120];     /* Buffer for returned data */    
int     inCnt;          /* how many bytes actually read */
double  xVal, yVal, zVal;   /* the interpreted values */
double	bcTime;         /* Time from the Bancomm card */
char    *tmpP;          /* tmp pointer while extracting X, Y & Z */
int     sign;


/*
 * Make sure module initialised
 */
    if (heidenInitF != TRUE)
    {
        return(HEIDEN_NOT_INIT_ERR);
    }


    /* 
     * send 'sample command', sample Bancomm time, then delay and
     * get bytes.
     */
    
    if (write(serialPortD, HEIDEN_GET_BYTE, 1) != 1)
      {
        return(HEIDEN_WRITE_ERR);
      }
    
    bcTime = bc635RawTime();
    
    taskDelay(5);
    
    if ((inCnt = read(serialPortD, inBuf, heidenInBytes)) != heidenInBytes)
      {
        return(0);
      }
    
    /*
     * format the values
     */
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'X') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    xVal = sign*atof(tmpP);
    
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'Y') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    yVal = sign*atof(tmpP);
    
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'Z') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    zVal = sign*atof(tmpP);
    
    switch (axis)
      {
      case 0:
           return(xVal);
      case 1:
           return(yVal);
      case 2:
           return(zVal);
      default:
           printf("heidenRdOne: invalid axis");
      }
    
    return(0);
    
} /* heidenRdOne */


/*
 ************************************************************************
 *+
 * EXPORTED FUNCTION: heidenRdAll(axis)
 *
 * RETURNS: int, number of successfull iterations, -tive for errors
 *
 * DESCRIPTION: Return the time and all encoder values
 * Temporarily, this uses the system clock.  Ultimately all the records
 * will use the bancomm time.
 *
 *-
 ************************************************************************
 */

int heidenRdAll
(
   double * bcTime, 
   double * xVal,
   double * yVal,
   double * zVal   /*return values*/
)
{
  char    inBuf[120];     /* Buffer for returned data */    
  int     inCnt;          /* how many bytes actually read */
  char    *tmpP;          /* tmp pointer while extracting X, Y & Z */
  int     sign;
  int     j;
  struct timespec tspec;


/*
 * Make sure module initialised
 */
    if (heidenInitF != TRUE)
    {
        return(HEIDEN_NOT_INIT_ERR);
    }

    /* 
     * send 'sample command', sample Bancomm time, then delay and
     * get bytes.
     */
    
    if (write(serialPortD, HEIDEN_GET_BYTE, 1) != 1)
      {
        return(HEIDEN_WRITE_ERR);
      }
    
    /* *bcTime = bc635RawTime(); */
    if ( (j = clock_gettime(CLOCK_REALTIME, &tspec )) ) return j;
    *bcTime = tspec.tv_sec + (double)tspec.tv_nsec / 1000000000.0;
   
    taskDelay(5);
    
    if ((inCnt = read(serialPortD, inBuf, heidenInBytes)) != heidenInBytes)
      {
        return(0);
      }
    
    /*
     * format the values
     */
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'X') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    *xVal = sign*atof(tmpP);
    
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'Y') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    *yVal = sign*atof(tmpP);
    
    sign = 1;
    tmpP = inBuf;
    while (*tmpP != 'Z') tmpP++;
    tmpP += 2;
    if (*tmpP == '-') sign = -1;
    tmpP+=1;
    *zVal = sign*atof(tmpP);
        
    return(0);
    
} /* heidenRdAll */
