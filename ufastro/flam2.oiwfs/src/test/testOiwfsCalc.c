/*
 *  Local Defines
 */

#include <stdio.h>
#include <stdlib.h>
#include <taskLib.h>
#include <sysLib.h>

#include <vxWorks.h>
#include <stdio.h>
#include <ioLib.h>
#include <tickLib.h>
#include <sioLib.h>
#include <module_types.h>

#include <math.h>
#include <f2OiwfsCalc.h>
/*
 *  Local Types
 */

/*
 * Dummy Function Prototypes
 */

long angle2pos (double, double);
long pos2angle (double, double);
long p2atest (void);

long angle2pos 
(
    double bAngle, /* base angle in degrees */
    double pAngle  /* pickoff angle in degrees */

)
{
    long status;
    double xPos, yPos, zPos, prAngle;

    printf("Base angle: %f\n", bAngle);
    printf("Pickoff angle: %f\n", pAngle);

    status = f2OiwfsCalculateProbePosition ((bAngle*3.1416/180), 
                      (pAngle*3.1416/180),
                      &xPos, &yPos, &zPos, &prAngle);
    if (status==0)
    {
        printf("X position: %f\n", xPos);
        printf("Y position: %f\n", yPos);
        printf("Z position: %f\n", zPos);
        printf("Probe angle: %f\n", (prAngle*180/3.1416));

    }
    return status;
}

long pos2angle 
( 
    double xPos, /* X position in mm */
    double yPos  /* Y position in mm */
)
{
    long status;
    double bAngle, pAngle, prAngle;

    printf("X position: %.2f\n", xPos);
    printf("Y position: %.2f\n", yPos);

    status = f2OiwfsCalculateProbeAngles (xPos, yPos, 
                      &bAngle, &pAngle, &prAngle);
    
    if (status==0)
    {
        printf("Base angle: %.2f\n", (bAngle*180/3.1416));
        printf("Pickoff angle: %.2f\n", (pAngle*180/3.1416));
        printf("Probe angle: %.2f\n", (prAngle*180/3.1416));

    }
    return status;
}

long p2atest ( void )
{

    printf("epsilon = \n  for x,y%.2f\n",(atan2 (202, -100)));
    
    return 0;
}
