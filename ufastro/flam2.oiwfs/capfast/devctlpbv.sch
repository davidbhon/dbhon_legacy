[schematic2]
uniq 97
[tools]
[detail]
w -444 -437 100 2 n#96 hwin.hwin#95.in -448 -432 -448 -432 ebis.NegLim.INP
w -444 -149 100 2 n#94 hwin.hwin#93.in -448 -144 -448 -144 ebis.PosLim.INP
w -444 171 100 2 n#33 hwin.hwin#29.in -448 176 -448 176 ebis.Home.INP
w -232 363 100 0 dbug inhier.dbug.P -736 352 320 352 320 416 576 416 edevctlm.Device.DBUG
w -104 491 100 0 simm inhier.simm.P -736 480 576 480 edevctlm.Device.SIMM
w 1288 491 100 0 lswa edevctlm.Device.LSWA 896 480 1728 480 outhier.lswa.p
w 1294 523 100 0 mip edevctlm.Device.MIP 896 512 1728 512 outhier.mip.p
w -162 1035 100 0 val inhier.val.P -736 1024 448 1024 448 352 576 352 edevctlm.Device.SLNK
w 1294 811 100 0 ack edevctlm.Device.ACK 896 800 1728 800 outhier.ack.p
w 1288 555 100 0 mpos edevctlm.Device.MPOS 896 544 1728 544 outhier.mpos.p
w 1096 11 100 0 n#62 edevctlm.Device.BRKL 896 608 960 608 960 0 1280 0 ebos.Brake.SLNK
w 1000 875 100 0 n#59 hwout.hwout#18.outp 1152 864 896 864 edevctlm.Device.OUT
w 1240 747 100 0 message_link edevctlm.Device.MSGL 896 736 1728 736 outhier.msgl.p
w 1258 779 100 0 busy_link edevctlm.Device.BSYL 896 768 1728 768 outhier.bsyl.p
w -184 939 100 0 velo inhier.velo.P -736 928 416 928 416 704 576 704 edevctlm.Device.VELO
w -136 1227 100 0 mode inhier.mode.P -736 1216 512 1216 512 768 576 768 edevctlm.Device.MODE
w -114 1323 100 0 dir inhier.dir.P -736 1312 544 1312 544 800 576 800 edevctlm.Device.DIR
w 1512 -21 100 0 n#27 ebos.Brake.OUT 1536 -32 1536 -32 hwout.hwout#32.outp
w 1202 35 100 0 n#25 hwin.hwin#28.in 1184 64 1184 32 1280 32 ebos.Brake.DOL
s 512 -256 100 0 on this Capfast schematic.
s 512 -224 100 0 all its properties defined correctly
s 512 -192 100 0 above record does not necessarily have
s 512 -160 100 0 are initialised using pvload, so the
s 512 -128 100 0 NOTE: Many of the device record properties
[cell use]
use hwin -640 -473 100 0 hwin#95
xform 0 -544 -432
p -672 -400 100 0 -1 val(in):#<$(negLim)>
use hwin -640 -185 100 0 hwin#93
xform 0 -544 -144
p -656 -112 100 0 -1 val(in):#<$(posLim)>
use hwin 992 23 100 0 hwin#28
xform 0 1088 64
p 995 56 100 0 -1 val(in):1
use hwin -640 135 100 0 hwin#29
xform 0 -544 176
p -656 208 100 0 -1 val(in):#<$(home)>
use ebis -192 -400 100 0 NegLim
xform 0 -320 -464
p -384 -544 100 0 1 DTYP:$(xycom)
p -384 -640 100 0 1 ONAM:ON
p -208 -400 100 512 1 PV:$(top)$(assy)$(dev)
p -384 -576 100 0 1 SCAN:$(iointr)
p -384 -608 100 0 1 ZNAM:OFF
use ebis -192 -112 100 0 PosLim
xform 0 -320 -176
p -384 -256 100 0 1 DTYP:$(xycom)
p -384 -352 100 0 1 ONAM:ON
p -208 -112 100 512 1 PV:$(top)$(assy)$(dev)
p -384 -288 100 0 1 SCAN:$(iointr)
p -384 -320 100 0 1 ZNAM:OFF
use ebis -192 208 100 0 Home
xform 0 -320 144
p -384 64 100 0 1 DTYP:$(xycom)
p -384 -32 100 0 1 ONAM:ON
p -208 208 100 512 1 PV:$(top)$(assy)$(dev)
p -384 32 100 0 1 SCAN:$(iointr)
p -384 0 100 0 1 ZNAM:OFF
use inhier -768 1328 100 512 dir
xform 0 -736 1312
use inhier -768 1232 100 512 mode
xform 0 -736 1216
use inhier -768 1040 100 512 val
xform 0 -736 1024
use inhier -768 944 100 512 velo
xform 0 -736 928
use inhier -768 496 100 512 simm
xform 0 -736 480
use inhier -768 368 100 512 dbug
xform 0 -736 352
use outhier 1760 480 100 0 lswa
xform 0 1712 480
use outhier 1760 512 100 0 mip
xform 0 1712 512
use outhier 1760 800 100 0 ack
xform 0 1712 800
use outhier 1760 768 100 0 bsyl
xform 0 1712 768
use outhier 1760 736 100 0 msgl
xform 0 1712 736
use outhier 1760 544 100 0 mpos
xform 0 1712 544
use edevctlm 832 912 100 0 Device
xform 0 736 608
p 672 464 100 0 0 MRND:1
p 768 128 100 0 0 PHLM:181.0
p 768 96 100 0 0 PLLM:-181.0
p 816 912 100 512 1 PV:$(top)$(assy)$(dev)
p 640 32 100 0 1 SIMM:$(devSimm)
p 640 224 100 0 1 TDIR:$(data_dir)
p 640 192 100 0 1 TFIL:$(assy)$(dev).lut
p 752 160 100 0 0 UEIP:YES
p 640 96 100 0 1 UPSB:NO
p 768 32 100 0 0 VBAS:0.00000000e+00
p 768 0 100 0 0 VHLM:20.0
p 768 -32 100 0 0 VLLM:0.0
use f2BorderC -1120 -825 100 0 f2BorderC#40
xform 0 560 480
p 2132 -696 100 512 1 File:devctlpbv.sch
p 1540 -652 150 0 1 Rev:A
p 1820 -576 120 256 -1 Title:FL-2 Device With Brake (VAL)
p 1860 -640 100 1024 -1 author:B.Wooff
p 1860 -672 100 1024 -1 date:June 3, 2005
use hwout 1152 823 100 0 hwout#18
xform 0 1248 864
p 1216 896 100 0 -1 val(outp):#<$(motor)>
use hwout 1536 -73 100 0 hwout#32
xform 0 1632 -32
p 1632 0 100 0 -1 val(outp):#<$(brake)>
use ebos 1456 80 100 0 Brake
xform 0 1408 0
p 1344 -96 100 0 1 DTYP:$(xycom)
p 1344 -128 100 0 1 OMSL:supervisory
p 1344 -192 100 0 1 ONAM:ON
p 1456 80 100 512 1 PV:$(top)$(assy)$(dev)
p 1344 -160 100 0 1 ZNAM:OFF
[comments]
