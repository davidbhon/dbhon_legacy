[schematic2]
uniq 3
[tools]
[detail]
s 832 1904 500 256 Flamingos 2 - Mauna Kea
s 832 1760 500 256 OIWFS Probe Controller
[cell use]
use f2OpcCfg 640 1223 100 0 f2OpcCfg#1
xform 0 800 1384
p 576 1088 100 0 -1 set0001:top f2:wfs:
p 576 1056 100 0 -1 set0002:sadtop f2:wfs:
p 576 1024 100 0 -1 set0003:dev probe
p 576 992 100 0 -1 set0004:assySimm NONE
p 576 960 100 0 -1 set0005:upsb YES
p 576 928 100 0 -1 set0006:xycom XYCOM-240
p 576 896 100 0 -1 set0007:iointr I/O Intr
p 576 832 100 0 -1 set0008:agProbeOffset ag:wfs:oi:probeOffset.J
p 800 992 100 0 -1 set0009:devSimm NONE
use f2BorderC -896 -265 100 0 f2BorderC#0
xform 0 784 1040
p 2356 -136 100 512 1 File:f2OpcMK.sch
p 2052 48 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 128 -80 100 1792 -1 Rev:0.1
p 2044 -16 120 256 -1 Title:Probe Controller top level database
p 2084 -80 100 1024 -1 author:B.Wooff
p 2224 -112 100 1280 -1 date:September 20, 2004
[comments]
