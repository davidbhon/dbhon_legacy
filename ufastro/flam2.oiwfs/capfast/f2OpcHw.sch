[schematic2]
uniq 113
[tools]
[detail]
[cell use]
use f2OPC -64 455 100 0 f2OPC#112
xform 0 1296 1216
p 1056 1728 100 512 -1 set0010:pkoMotor C2 S0
p 1056 1696 100 512 -1 set0011:basMotor C2 S1
p 1392 1728 100 512 -1 set0020:pkoPosLim C0 S24
p 1392 1696 100 512 -1 set0021:pkoNegLim C0 S25
p 1392 1664 100 512 -1 set0022:basPosLim C0 S26
p 1392 1632 100 512 -1 set0023:basNegLim C0 S27
p 1744 1664 100 512 -1 set0062:pkoBrake C0 S26
p 1744 1632 100 512 -1 set0063:basBrake C0 S27
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 2836 -24 100 512 1 File:f2OPChw.sch
p 2532 160 120 256 -1 Project:Flamingos
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:Flamingos2 OPC Hardware Addresses
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:September 10, 2004
[comments]
