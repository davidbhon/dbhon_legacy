[schematic2]
uniq 332
[tools]
[detail]
w 298 1691 100 0 n#331 elongouts.JogDirection.FLNK 240 1680 416 1680 416 1408 576 1408 ecalcs.JogTarget.SLNK
w 962 1579 100 0 n#330 ecalcs.JogTarget.FLNK 864 1632 928 1632 928 1568 1056 1568 eaos.TrackVal.SLNK
w 418 1771 100 0 n#328 elongouts.JogDirection.VAL 240 1648 320 1648 320 1760 576 1760 ecalcs.JogTarget.INPB
w 250 1915 100 0 n#327 eaos.JogDistance.VAL 144 1904 416 1904 416 1792 576 1792 ecalcs.JogTarget.INPA
w 930 1611 100 0 n#324 ecalcs.JogTarget.VAL 864 1600 1056 1600 eaos.TrackVal.DOL
w 1970 1515 100 0 n#307 elongouts.TrackTarget.FLNK 1952 1504 2048 1504 junction
w 1970 907 100 0 n#307 elongouts.IndexMode.FLNK 1952 896 2048 896 2048 1184 junction
w 1970 1771 100 0 n#307 elongouts.ModeTarget.FLNK 1952 1760 2048 1760 2048 1184 junction
w 2098 1195 100 0 n#307 elongouts.MoveMode.FLNK 1952 1184 2304 1184 eseqs.DirSeq.SLNK
w 1522 1483 100 0 n#322 eaos.TrackVal.FLNK 1312 1600 1408 1600 1408 1472 1696 1472 elongouts.TrackTarget.SLNK
w 1700 1499 100 2 n#320 hwin.hwin#319.in 1696 1504 1696 1504 elongouts.TrackTarget.DOL
w 1090 843 100 0 n#316 eaos.ValIndx.FLNK 992 832 1248 832 elongouts.Indx0.SLNK
w 1570 875 100 0 n#315 elongouts.Indx0.FLNK 1504 864 1696 864 elongouts.IndexMode.SLNK
w 1252 859 100 2 n#314 hwin.hwin#313.in 1248 864 1248 864 elongouts.Indx0.DOL
w 1700 891 100 2 n#312 hwin.hwin#311.in 1696 896 1696 896 elongouts.IndexMode.DOL
w 1474 1163 100 0 n#302 eaos.ValTarget.FLNK 1312 1152 1696 1152 elongouts.MoveMode.SLNK
w 1330 1355 100 0 n#302 estringouts.ValsTarget.FLNK 1312 1344 1408 1344 1408 1152 junction
w 2308 1499 100 2 n#306 hwin.hwin#299.in 2304 1504 2304 1504 eseqs.DirSeq.DOL1
w 1700 1179 100 2 n#305 hwin.hwin#300.in 1696 1184 1696 1184 elongouts.MoveMode.DOL
s 496 1792 100 512 Jog Target
s 1024 896 100 0 Set the IALG to 0
[cell use]
use eaos 48 1984 100 0 JogDistance
xform 0 16 1904
p -32 1856 100 0 1 PREC:4
p 48 1984 100 512 -1 PV:$(top)$(assy)$(dev)
use eaos 1216 1648 100 0 TrackVal
xform 0 1184 1568
p 1120 1472 100 0 1 OMSL:closed_loop
p 1136 1520 100 0 1 PREC:4
p 1216 1648 100 512 -1 PV:$(top)$(assy)$(dev)
p 1232 1504 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.VAL
use eaos 896 880 100 0 ValIndx
xform 0 864 800
p 896 880 100 512 -1 PV:$(top)$(assy)$(dev)
p 928 736 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.VAL
use eaos 1216 1200 100 0 ValTarget
xform 0 1184 1120
p 1120 1024 100 0 1 OMSL:supervisory
p 1184 1072 100 256 1 PREC:4
p 1216 1200 100 512 -1 PV:$(top)$(assy)$(dev)
p 1248 1056 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.VAL
use elongouts 144 1728 100 0 JogDirection
xform 0 112 1648
p 144 1728 100 512 -1 PV:$(top)$(assy)$(dev)
use elongouts 1856 1552 100 0 TrackTarget
xform 0 1824 1472
p 1760 1376 100 0 1 OMSL:supervisory
p 1856 1552 100 512 -1 PV:$(top)$(assy)$(dev)
p 1888 1424 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.MODE
use elongouts 1856 944 100 0 IndexMode
xform 0 1824 864
p 1856 944 100 512 -1 PV:$(top)$(assy)$(dev)
p 1888 800 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.MODE
use elongouts 1408 912 100 0 Indx0
xform 0 1376 832
p 1408 912 100 512 -1 PV:$(top)$(assy)$(dev)
p 1440 768 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.IALG
use elongouts 1856 1232 100 0 MoveMode
xform 0 1824 1152
p 1760 1056 100 0 1 OMSL:supervisory
p 1856 1232 100 512 -1 PV:$(top)$(assy)$(dev)
p 1888 1088 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.MODE
use elongouts 1856 1808 100 0 ModeTarget
xform 0 1824 1728
p 1760 1632 100 0 1 OMSL:supervisory
p 1856 1808 100 512 -1 PV:$(top)$(assy)$(dev)
p 1888 1664 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.MODE
use ecalcs 720 1840 100 0 JogTarget
xform 0 720 1584
p 736 1776 70 0 1 CALC:(B=0)?(C+A):(C-A)
p 720 1840 100 512 -1 PV:$(top)$(assy)$(dev)
p 608 1344 70 512 1 def(INPC):$(top)$(assy)$(dev)Device.VAL
use ecalcs 432 576 100 0 CalcEncErr
xform 0 464 336
p 448 528 100 0 1 CALC:B - ( A * D / C )
p 432 576 100 512 -1 PV:$(top)$(assy)$(dev)
p 480 144 100 0 1 SCAN:1 second
p 208 544 100 512 1 def(INPA):$(top)$(assy)$(dev)Device.RRBV
p 208 512 100 512 1 def(INPB):$(top)$(assy)$(dev)Device.RENC
p 208 480 100 512 1 def(INPC):$(top)$(assy)$(dev)Device.MRES
p 208 448 100 512 1 def(INPD):$(top)$(assy)$(dev)Device.ERES
use hwin 1504 1463 100 0 hwin#319
xform 0 1600 1504
p 1507 1496 100 0 -1 val(in):$(D_TRACK)
use hwin 1056 823 100 0 hwin#313
xform 0 1152 864
p 1059 856 100 0 -1 val(in):0
use hwin 1504 855 100 0 hwin#311
xform 0 1600 896
p 1507 888 100 0 -1 val(in):$(D_INDEX)
use hwin 1504 1143 100 0 hwin#300
xform 0 1600 1184
p 1507 1176 100 0 -1 val(in):$(D_MOVE)
use hwin 2112 1463 100 0 hwin#299
xform 0 2208 1504
p 2115 1496 100 0 -1 val(in):$(D_GO)
use estringouts 1216 1392 100 0 ValsTarget
xform 0 1184 1328
p 1088 1248 100 0 1 OMSL:supervisory
p 1216 1392 100 512 -1 PV:$(top)$(assy)$(dev)
p 1264 1280 70 0 1 def(OUT):$(top)$(assy)$(dev)Device.VALS
use eseqs 2496 1584 100 0 DirSeq
xform 0 2464 1344
p 2416 1120 100 0 0 DLY1:0.5
p 2416 1072 100 0 0 DLY2:0.0
p 2480 1584 100 512 -1 PV:$(top)$(assy)$(dev)
p 2448 1088 70 0 1 def(LNK1):$(top)$(assy)$(dev)Device.DIR
p 2720 1472 70 0 0 def(LNK2):0.0
p 2640 1504 75 1024 -1 pproc(LNK1):PP
p 2640 1472 75 1024 -1 pproc(LNK2):NPP
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 2840 -24 100 512 1 File:f2DeviceEng.sch
p 1216 1088 100 0 0 IO:
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:Engineering Driver for deviceControl
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:May 31, 2005
p 1152 1056 100 0 0 model:
p 1152 1024 100 0 0 revision:
[comments]
