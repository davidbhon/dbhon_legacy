[schematic2]
uniq 217
[tools]
[detail]
w 384 1104 100 0 current_probe_position f2OiwfsCommands.f2OiwfsCommands#174.CUR_POS 32 1504 0 1504 0 1088 1024 1088 1024 1472 992 1472 f2OiwfsAssembly.f2OiwfsAssembly#173.CUR_POS
w 594 1515 100 0 n#194 f2OiwfsCommands.f2OiwfsCommands#174.DEBUG 576 1504 672 1504 f2OiwfsAssembly.f2OiwfsAssembly#173.DBUG
w 594 1803 100 0 n#193 f2OiwfsCommands.f2OiwfsCommands#174.Y_M 576 1792 672 1792 f2OiwfsAssembly.f2OiwfsAssembly#173.Y_M
w 594 1835 100 0 n#192 f2OiwfsCommands.f2OiwfsCommands#174.X_M 576 1824 672 1824 f2OiwfsAssembly.f2OiwfsAssembly#173.X_M
w 594 1899 100 0 n#165 f2OiwfsCommands.f2OiwfsCommands#174.MODE 576 1888 672 1888 f2OiwfsAssembly.f2OiwfsAssembly#173.MODE
w 594 1931 100 0 n#164 f2OiwfsCommands.f2OiwfsCommands#174.DIR 576 1920 672 1920 f2OiwfsAssembly.f2OiwfsAssembly#173.DIR
s 1584 2032 100 0 Not a true SAD, so included in main database.
[cell use]
use f2OpcReboot -352 1559 100 0 f2OpcReboot#216
xform 0 -240 1776
p -352 1536 100 0 -1 set001:top $(top)
use f2OiwfsInterlock 1248 1863 100 0 f2OiwfsInterlock#215
xform 0 1360 1936
p 1248 1856 100 0 -1 set001:top $(top)
p 1248 1824 100 0 -1 set002:assy $(assy1)
p 1248 1792 100 0 -1 set003:dev1 $(dev1)
p 1248 1760 100 0 -1 set004:dev2 $(dev2)
p 1248 1728 100 0 -1 set005:sadtop $(sadtop)
use f2OiwfsAssyEng 1248 1575 100 0 f2OiwfsAssyEng#213
xform 0 1360 1648
p 1248 1568 100 0 -1 set001:top $(top)
p 1248 1536 100 0 -1 set002:assy $(assy1)
p 1248 1504 100 0 -1 set003:dev1 $(dev1)
p 1248 1472 100 0 -1 set004:dev2 $(dev2)
use f2BorderC -864 247 100 0 f2BorderC#211
xform 0 816 1552
p 2388 376 100 512 1 File:f2OPC.sch
p 1796 420 150 0 1 Rev:1.0
p 2076 496 120 256 -1 Title:OIWFS Probe Controller
p 2116 432 100 1024 -1 author:B.Wooff
p 2116 400 100 1024 -1 date:June 28, 2005
use f2OiwfsSad 1696 1735 100 0 f2OiwfsSad#202
xform 0 1824 1872
p 1696 1728 100 0 -1 set001:sadtop $(sadtop)
p 1696 1696 100 0 -1 set002:top $(top)
p 1696 1664 100 0 -1 set003:assy $(assy1)
use f2OiwfsCommands 32 1407 100 0 f2OiwfsCommands#174
xform 0 304 1704
p 96 1408 100 0 -1 set001:top $(top)
p 96 1376 100 0 -1 set002:assy $(assy1)
use f2OiwfsAssembly 672 1399 100 0 f2OiwfsAssembly#173
xform 0 832 1696
[comments]
