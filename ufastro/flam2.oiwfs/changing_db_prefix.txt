source ~f2wfs/f2oiwfs.cshrc
make release

...

follow instructions in F2_OPC_Configuration.pdf
       -- make sure that uf2OpcInstall script is run
          from INSTALL DIRECTORY !!!! (not source dir !!!)
	  (actually, INSTALL DIR == SOURCE DIR !!!!)


change startup/resource.def:

2,3c2,3
< EPICS_IOC_LOG_INET    DBF_STRING 192.168.111.222 (or .170 if we get
capfast liscence working)
< EPICS_TS_NTP_INET     DBF_STRING 192.168.111.222
---
> EPICS_IOC_LOG_INET    DBF_STRING 172.17.2.10
> EPICS_TS_NTP_INET     DBF_STRING 172.17.2.10



edit startup/local.vws

(comment everything out except last 'cd'
 for sim version, don't do the hostAdd baytech thing)


edit startup/f2StartupSim

comment out f2Rpc3(2,0)

CHANING NAME

edit capfast/f2OpcCP.sch (change f2:wfs to something else)
     capfast/f2OpcCPsim.sch

edit adl/f2OpcStartGUI

edit startup/f2Startup.vws and startup/f2StartupSim.vws


edit test/wfsReset
     test/configFiles/wfsCommands.config
     test/configFiles/baseDevice.config
     test/configFiles/pickoffDevice.config     
     test/configFiles/wfsAssembly.config