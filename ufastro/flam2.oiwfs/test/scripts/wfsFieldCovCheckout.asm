Flamingos-2 OIWFS Probe Field Coverage Checkout.

THIS SCRIPT TAKES ABOUT 10 MINUTES TO COMPLETE.

Assumptions are ....

        * The IOC has been depowered and re-started cold, or the wfs
          assembly has been reset to its startup state.

        * The Flamingos-2 components control database has been loaded.

        * There is nothing obstructing the OIWFS probe arm.

	If so, it should be safe to move the probe arm....
