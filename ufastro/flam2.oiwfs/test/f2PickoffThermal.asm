
Carry out automated pickoff stage thermal check 
--------------------------------------------------------------

This script makes the following assumptions:

* The Flamingos-2 OIWFS Probe Controller database is loaded and ready for use.

* It is safe to move all the OIWFS Probe.


NOTE: This scripts runs -auto mode and does not require any user input
after this prompt. 

This script will take about 20 minutes to complete. 

